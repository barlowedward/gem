#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Std;
use Repository;
use File::Find;

$| = 1;

sub usage
{
	print @_;
	print "find_win32_files_for_monitoring.pl -i sourcedir -A -H

	-r reformat in filechecker format
   -d debug mode
   -S system optional (outputs --systemname=opt_S)
\n";
   return "\n";
}

use vars qw($opt_i $opt_A $opt_H $opt_d $opt_r $opt_S);

die usage("Bad Parameter List\n") unless getopts('ri:AHdS:');

$opt_S=' --systemname='.$opt_S if $opt_S;

if( ! defined $opt_i ) {
	my($gr_dir)=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gr_dir;
	$opt_i = "$gr_dir/data/system_information_data";
	die "No Input File $opt_i" unless -d $opt_i;
}

$opt_d=1 if defined $opt_d;

die usage("Must Pass Input Directory\n") unless defined $opt_i;
$opt_i=~s/\s*$//;
$opt_i=~s/\/$//;

my($starttime)=time;
if( $opt_i =~ /,/ ) {
	foreach my $dir ( split( /,/,$opt_i ) ) {
		die usage("Output Directory $dir does not exist\n") unless -d $dir;
		print "Reading directory $dir\n" unless $opt_r;
		find(\&wanted,$dir);
		print "	 --> file parse completed\n" unless $opt_r;
	}
} else {
	die usage("Input Directory $opt_i does not exist\n") unless -d $opt_i;
	print "Reading Input directory $opt_i\n" unless $opt_r;
	find(\&wanted,$opt_i);
	print "	 --> file parse completed\n" unless $opt_r;
}

exit(0);

my(%printed);	# for duplicates
sub wanted
{
	return if -d $File::Find::name;
	if( $opt_r ) {
		my($dir)=$File::Find::dir;
		return if /\.trn$/i or /\.log$/i or /logdump/ or /trandump/ or /_restore\./i or /archive_backup/i or /model_backup/i;
		my($f)=$_;
		$f =~ s/\d//g;
		return if $printed{$f};
		return if $dir=~/RECYCLER/ or $dir=~/System Volume Information/ or /^\.$/;
		my($days) = -M $File::Find::name;
		$f=~s/^\s+//;
		$f=~s/\s+$//;
		$printed{$f}=1;
		if( $f =~ /\s/ ) {
		} elsif( $days>30 ) {
			# ignore
		} elsif( $days>4 ) {
			print "--DIR=$dir --DAYS=7 --FILE=$f $opt_S\n";
		} elsif( $days>1 ) {
			print "--DIR=$dir --DAYS=4 --FILE=$f $opt_S\n";
		} else {
			print "--DIR=$dir --DAYS=2 --FILE=$f $opt_S\n";
		}
	} else {
		return if $printed{$File::Find::name};
		print $File::Find::name,"\n";
	}

	# return unless /^dbspace/;
}

__END__

=head1 NAME

find_win32_files_for_monitoring.pl - find files for monitoring*/

=head2 USAGE

find_win32_files_for_monitoring.pl -i sourcedir,dir,dir

   -r - format output for purgefiles

=cut
