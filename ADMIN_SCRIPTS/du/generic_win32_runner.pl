#!/apps/perl/linux/perl-5.8.2/bin/perl

# copyright (c) 2006-7 by SQL Technologies.  All Rights Reserved.

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use MlpAlarm;
use CommonFunc;
use File::Basename;
use Getopt::Long;
use vars qw( $OUTFILE $ERRORFILE $HTML $NOEXEC $SYSTEM $DEBUG $LOGEVENTS $LOOPSLEEPTIME $BATCHNAME);
use Repository;

$|=1;

# changedir to directory in which the executable exists
my($curdir)=dirname($0);
$curdir =~ s?\\?/?g;
chdir $curdir or error_out( "Cant chdir to $curdir : $! \n" );

sub usage {
	print @_,"\n";
 	return "Usage: generic_win32_runner.pl --OUTFILE=file [--HEML] --ERRORFILE=file --SYSTEM=system[,system] [--DEBUG] [--LOGEVENTS] [--LOOPSLEEPTIME] --BATCHNAME=name";
}

sub error_out {
   my($msg)=join("",@_);
   MlpBatchJobErr($msg) if $BATCHNAME;
   die $msg;
}

my($COMMAND_RUN)=$0." ".join(" ",@ARGV);

#die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
    GetOptions(
    	"OUTFILE=s"=>\$OUTFILE,
    	"ERRORFILE=s"=>\$ERRORFILE,
    	"SYSTEM=s"=>\$SYSTEM,
    	"HTML"=>\$HTML,
    	"DEBUG"=>\$DEBUG,
    	"NOEXEC"=>\$NOEXEC,
    	"LOGEVENTS"=>\$LOGEVENTS,
		"BATCHNAME=s"=>\$BATCHNAME,
    	"LOOPSLEEPTIME=i"=>\$LOOPSLEEPTIME );

if( defined $LOOPSLEEPTIME ) {
	cd_home();
	if( Am_I_Up(-prefix=>$BATCHNAME) ) {  print "Program Running - Aborting\n"; exit(0); }
	I_Am_Up(-prefix=>$BATCHNAME);
}

my($NL)="\n";
$NL="<br>\n" if $HTML;

if( $BATCHNAME ) {
	write_outputmsg( $BATCHNAME,$NL );
} else {
	write_outputmsg( "generic_win32_runner.pl",$NL );
}
write_outputmsg( "started at ".localtime(time).$NL);
write_outputmsg( "program called in debug mode",$NL ) if defined $DEBUG;

my(@systems);
if( defined $SYSTEM and $SYSTEM ne "ALL" ) {
	@systems= split(/,/,$SYSTEM);
} else {
	@systems=get_password(-type=>"win32servers");
}

while(1) {
	foreach my $sys (sort @systems) {
		do_one_system($sys);
	}
	last unless $LOOPSLEEPTIME;
	I_Am_Up(-prefix=>$BATCHNAME);
	write_outputmsg( "Sleeping for $LOOPSLEEPTIME at ".localtime(time).$NL) if defined $DEBUG;
	sleep $LOOPSLEEPTIME;
	I_Am_Up(-prefix=>$BATCHNAME);

}
exit(0);

my($errorfile_open)="FALSE";
sub write_errormsg
{
	my($msg)=join("",@_);
	write_outputmsg($msg);
	if( defined $ERRORFILE ) {
		if($errorfile_open eq "FALSE" ) {
			$errorfile_open="TRUE";
			unlink $ERRORFILE if -w $ERRORFILE;
			open(ERRF,"> $ERRORFILE") or error_out( "Cant Write To $ERRORFILE : $!\n" );
		}
		print ERRF $msg;
	}
}

my($OUTFILE_open)="FALSE";
sub write_outputmsg
{
	my($msg)=join("",@_);
	if( defined $OUTFILE ) {
		if($OUTFILE_open eq "FALSE" ) {
			$OUTFILE_open="TRUE";
			unlink $OUTFILE if -w $OUTFILE;
			open(OUTF,"> $OUTFILE") or error_out( "Cant Write To $OUTFILE : $!\n" );
		}
		print OUTF $msg;
	}
	print $msg;
}

sub do_one_system
{
	my($sys)=@_;
	write_outputmsg( "Working on system $sys",$NL );
	MlpBatchJobStart( -BATCH_ID=>$BATCHNAME, -SUBKEY=>$sys) if defined $LOGEVENTS;

	my %info = get_password_info(-name=>$sys, -type=>'win32servers');
	my(@list_of_disks)=split(/\s+/,$info{DISKS});

	foreach( @list_of_disks ) {
		my($cmd)="$curdir/du.exe -l 3 \\\\$sys\\$_";
		write_outputmsg("-- Disk $_ : $cmd",$NL);
		open(OUT,"$cmd |") or error_out("Cant run $cmd\n");
		while(<OUT>) {
			chomp;
			write_outputmsg( "-- > $_",$NL );
		}
		close(OUT);
	}
	MlpBatchJobEnd() if defined $LOGEVENTS;
}

__END__

=head1 NAME

win32_eventlog.pl - NT Diskspace Report

=head2 DESCRIPTION

Uses Win32 Libraries to check out eventlogs on remote NT servers.  Output
is saved using alarming functions.

=head2 USAGE

  Usage: win32_eventlog.pl --OUTFILE=file --ERRORFILE=file --SYSTEM=system
   --HOURS=hours --SERVICE=service[,service] [--CONFIGFILE=filename]
   [--SHOWINFOMSGS] [--DEBUG] [--LOGEVENTS]  [--LOOPSLEEPTIME]

 OUTFILE:   	Output Report File
 ERRORFILE:   	Output Report for just errors
 SYSTEM:    	List of systems to work on - comma separated
 HOURS:
 DEBUG:   		Debug
 LOGEVENTS:   	Save Heartbeats using MlpAdmin
 LOOPSLEEPTIME

=head2 CONFIG

Reads Repository data file which looks like:

=head2 IGNORE LIST

The program reads the __DATA__ section at the bottom of the program
to find out messages that it can ignore by service, severity, and
error number.  Each service can specify by Error/Warning/Info whether
it wants to ignore all messages (wildcard %) or a specific list of
messages to ignore.

=cut
