#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# Release Manager By Ed Barlow
# Documentation At End Of File in Pod Format

use strict;
use Getopt::Std;
use File::Basename;

sub usage {
	my($msg)=@_;
	print "ERROR: $msg\n" if defined $msg;

	print
"Usage: $0 -UUSER -PPASSWD -SSVR -DDB [ FLAGS ] -VDIR/-c CTLFILE/PROCEDUREs

Installs scripts into your server.  Preserves data for tables.  Many flags to control behavior.  The scripts to be installed are either based on a control file (default for -c is do_release.ctl). If neither -V and -c procedures are read from the command line.  The DB parameter can contain sybase wildcards.

  -s# - start at proc # (between 1 & 999)
  -e# - end at proc #  (between 1 & 999)
  -n  - noexec do not run the isql
  -r  - ignore rules (.rul or type=R) and defaults (.def or type=D)
  -T  - install stored procedures only (.prc or type=P)...
  -b  - batch mode - no waits or delays (and dont die on error)
  -N  - New Objects Only
  -s  - Dont save data
  -x  - batch mode - no waits or delays (but do die on error)\n";

  exit(1);
}

use vars qw($opt_d $opt_s $opt_N $opt_x $opt_U $opt_S $opt_P $opt_D $opt_c $opt_V $opt_s $opt_e $opt_b $opt_n $opt_r $opt_T);

# ================================
# TEST ARGUMENTS
# ================================
usage() if $#ARGV<0;
usage("Bad Parameter List") unless getopts('dU:ND:S:P:c:V:s:e:bxnrT');
usage("Must pass database\n" )  unless defined $opt_D;
usage("Must pass password\n" )  unless defined $opt_P;
usage("Must pass server\n")	  unless defined $opt_S;
usage("Must pass username\n" )  unless defined $opt_U;

my($curdir)=dirname($0);
use Sybase::Release;

my $objects=join(" ",@ARGV);
undef $objects if $objects=~ /^\s*$/;

if( defined $opt_V and ! defined $objects ) {
   opendir(DIR,$opt_V) || die("Can't open directory $opt_V for reading\n");
   my(@dirlist) = grep(!/^\./,readdir(DIR));
	$objects=join(" ",@dirlist);
   closedir(DIR);
};

do_release(    batch_die => $opt_x,
				   debug=>$opt_d,
					username => $opt_U,
					servername => $opt_S,
					password => $opt_P,
					dbname => $opt_D,
					ctlfile => $opt_c,
					dir => $opt_V,
					start_at_fileno => $opt_s,
					end_at_fileno => $opt_e,
					batch_nodie => $opt_b,
					noexec => $opt_n,
					norules => $opt_r,
					nosave => $opt_s,
					procs_only => $opt_T,
					new_obj_only => $opt_N,
					objects => $objects,
	);

__END__

=head1 NAME

do_release.pl - release management utility

=head2 DESCRIPTION

Release Manager - Loads database based on ddl files.

This is a nice way to install your ddl while preserving table data.  Behavior
is dependent on file name (tables, for example, should have .tbl). Unknown
extensions get treated as generic sql scripts.  You may pass the objects in
to the script or can list them (in order) in a control file (defined with -c).

The utility manages tables by archiving the data and then copying it back.
This is a great tool to manage upgrades of databases.

=head2 USAGE

Usage: do_release.pl -UUSER -PPASSWD -SSVR -DDB [ FLAGS ] -VDIR/-c CTLFILE/PROCEDUREs

Installs scripts into your server, preserving data for tables.  The scripts to be installed are based on a control file located in the same directory that contains the scripts (default for -c is do_release.ctl). If neither -V and -c procedures are read from the command line.  The DB parameter can contain sybase wildcards.

B<-s#> - start at proc # (between 1 & 999)

B<-e#> - end at proc #  (between 1 & 999)

B<-N> - New Objects Only

B<-n> - noexec do not run the isql

B<-r> - ignore rules (.rul or type=R) and defaults (.def or type=D)

B<-T> - install stored procedures only (.prc or type=P)...

B<-b> - batch mode - no waits or delays (and dont die on error)

B<-x> - batch mode - no waits or delays (but do die on error)

B<-s> - Dont Save Table Data

=head2 NOTES

If u do a use XXX statement as a batch, your db will be set to that database

=cut
