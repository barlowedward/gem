#!/usr/local/bin/perl-5.6.1
use lib qw(/apps/sybmon/dist/lib /apps/sybmon/dist/ADMIN_SCRIPTS/lib /apps/sybmon/dist/plugins /apps/sybmon/dist/cpan);

use subs qw(debug logdie infof info warning alert);
use strict;
use Repository;
use Net::Ping::External qw(ping);
use vars qw( $login $opt_S $passwd $opt_d );
use Sys::Hostname;
use MlpAlarm;

# Copyright (c) 2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

$| = 1;

my(@systemlist)=get_password(-type=>"pcdisks");
push @systemlist,get_password(-type=>"unix");
my($numalive,$numdead)=(0,0);
MlpMonitorStart( -monitor_program=>"PingSystems" );

foreach (@systemlist) {
	print "Pinging ... $_... ";
	my $alive = ping(host=>$_, timeout=>5);
	if( $alive ) {
		print "alive!\n";
		$numalive++;
		MlpHeartbeat( -monitor_program=>"PingSystems",
			-system=>$_,
			-subsystem=>"ping",
			-message_text=>"ping succeeded",
			-state=>'OK' );

	} else {
		print "*** dead! ***\n";
		$numdead++;
		MlpHeartbeat( -monitor_program=>"PingSystems",
			-system=>$_,
			-subsystem=>"ping",
			-message_text=>"Ping from ".hostname()." Failed",
			-state=>'WARNING' );

	}
}
print $numalive," hosts are alive!\n";
print $numdead," hosts are dead!\n";

MlpBatchDone();
