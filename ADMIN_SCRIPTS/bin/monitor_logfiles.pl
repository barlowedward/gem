#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Sys::Hostname;
use CommonFunc;
use Repository;
use MlpAlarm;

use vars qw( $DIR $HOST $NOCOPY $CTLFILE $BATCH_ID $DEBUG $FILEROOT $MAX_NORUN_DAYS $LOGEXT $ERREXT );
die usage("") if $#ARGV<0;
$|=1;
my($curhost)=hostname();

sub usage {
	print @_;
	print "usage $0

--CTLFILE=<file>

-or-

--BATCH_ID=
--HOST=<hostname for file>
--NOCOPY
--DIR=<dir>
--FILEROOT=<filename without .log or .err extension>
--BATCH_SUBKEY<key>
--MAX_NORUN_DAYS=<num>
--LOGEXT=.log
--ERREXT=.err
\n";
	exit(0);
}

die usage("Bad Parameter List\n") unless GetOptions(
	"dir=s" 				=> \$DIR,
	"host=s"				=> \$HOST,
	"nocopy"				=> \$NOCOPY,
   "ctlfile=s"			=> \$CTLFILE,
   "batch_id=s"		=> \$BATCH_ID,
	"logext=s"			=> \$LOGEXT,
	"errext=s"			=> \$ERREXT,
	"debug"				=>	\$DEBUG,
	"fileroot=s" 		=> \$FILEROOT,
	"MAX_NORUN_DAYS=s" => \$MAX_NORUN_DAYS
);

die "Error - LOGEXT should not start with a period (.)\n" if $LOGEXT=~/^\./;
die "Error - ERREXT should not start with a period (.)\n" if $ERREXT=~/^\./;
print "monitor_logfiles.pl - version 1.0\n";
MlpBatchJobStart( -BATCH_ID=>$BATCH_ID )  if defined $BATCH_ID;

my($ROOTDIR)=get_gem_root_dir();
die "ROOT DIRECTORY $ROOTDIR does not exist\n" unless -d $ROOTDIR;

if( defined $CTLFILE) {
	$CTLFILE="$ROOTDIR/conf/$CTLFILE" if ! -e $CTLFILE and -e "$ROOTDIR/conf/$CTLFILE";
	die "control file $CTLFILE does not exist\n" 	unless -e $CTLFILE;
	die "control file $CTLFILE is not readable\n" 	unless -r $CTLFILE;
   print "Reading control file $CTLFILE\n";

	my(@dat);
	open(X,"$CTLFILE") or die "Cant open $CTLFILE $!\n";
	while(<X>) {
		next if /^#/ or /^\s*$/;
		chomp;chomp;chomp;
		s/^\s+//;
		s/\s+$//;
		push @dat,$_;
	}
	close(X);

	foreach (@dat) {
		my(@valpairs)=split;
		my( $host, $dir, $fileroot, $max_norun_days,$logext,$errext, $nocopy);
		foreach my $kv (@valpairs) {
			if( $kv=~/^\s*\-+host=/i ) {
				$kv=~s/^\s*\-+host=//gi;
				$host=$kv;
			} elsif( $kv=~/^\s*\-+dir=/i ) {
				$kv=~s/^\s*\-+dir=//gi;
				$dir=$kv;
			} elsif( $kv=~/^\s*\-+nocopy/i ) {
				$nocopy=1;
			} elsif( $kv=~/^\s*\-+fileroot=/i ) {
				$kv=~s/^\s*\-+fileroot=//gi;
				$fileroot=$kv;
			} elsif( $kv=~/^\s*\-+max_norun_days=/i ) {
				$kv=~s/^\s*\-+max_norun_days=//gi;
				$max_norun_days=$kv;
			} elsif( $kv=~/^\s*\-+logext=/i ) {
				$kv=~s/^\s*\-+logext=//gi;
				$logext=$kv;
			} elsif( $kv=~/^\s*\-+errext=/i ) {
				$kv=~s/^\s*\-+errext=//gi;
				$errext=$kv;
			} else {
				die "UNKNOWN KV PAIR $kv\n";
			}
		}

		print "monitor_it( $host, $nocopy, $dir, $fileroot, $max_norun_days, $logext, $errext )\n" if $DEBUG;
		monitor_it( $host, $nocopy, $dir, $fileroot, $max_norun_days, undef, $logext, $errext );
		print "\n";
	}
} else {
	monitor_it( $HOST, $NOCOPY, $DIR, $FILEROOT, $MAX_NORUN_DAYS, undef, $LOGEXT, $ERREXT);
}
MlpBatchJobEnd( ) if defined $BATCH_ID;
exit(0);

sub monitor_it {
	my($host, $nocopy, $dir, $fileroot, $max_norun_days, $noheader, $logext, $errext)=@_;
	$dir =~ s.\\.\/.g;
	$max_norun_days=2 unless $max_norun_days;

	$logext="log" unless $logext;
	$errext="err" unless $errext;

	unless($noheader) {
		print "####### Processing Directive #######\n" if $CTLFILE;
		print "#   Run Time: ",(scalar localtime(time))."\n";
	   print "#   Host=$host \n" if $host;
	   print "#   Directory=$dir\n";
	   print "#   Fileroot=$fileroot\n";
	   print "#   Days=$max_norun_days\n";
	   print "#\n";
	}

 	die "Must Pass -dir\n" 					unless defined $dir;
	die "Must Pass -fileroot\n" 			unless defined $fileroot;;

   if( $host and ! $noheader and ! $nocopy ) {
   	die "Cant run --host on win32()\n" if is_nt();
   	print "Fetching Files From $host\n";

   	die "WOAH - $ROOTDIR/data/cronlogs/$fileroot.log does not exist\n" unless -d "$ROOTDIR/data/cronlogs";

   	unlink("$ROOTDIR/data/cronlogs/$fileroot.$logext");
   	unlink("$ROOTDIR/data/cronlogs/$fileroot.$errext");

   	my($cmd)="scp -p $host:$dir/$fileroot.$logext $ROOTDIR/data/cronlogs/${host}_${fileroot}.${logext} 2>&1\n";
   	print "\n".$cmd."\n";
   	open(C1,$cmd."|") or die "Cant run $cmd $!\n";
   	while(<C1>) {
   		print "#\n# ERROR: ".$_."#\n";
   	}
   	close(C1);
   	# print "\n";

   	my($cmd)="scp -p $host:$dir/$fileroot.$errext $ROOTDIR/data/cronlogs/${host}_${fileroot}.${errext} 2>&1\n";
   	print "\n".$cmd."\n";
   	open(C2,$cmd.' |') or die "Cant run $cmd $!\n";
   	while(<C2>) {
   		print "#\n# ERROR: ".$_."#\n";
   	}
   	close(C2);
   	# system($cmd);
   	# print "\n";

   	monitor_it($host, 1, "$ROOTDIR/data/cronlogs", $host.'_'.$fileroot, $max_norun_days, 1, $logext, $errext );

   } else {
   	my($errorfile)=$dir."/".$fileroot.'.'.$errext;
   	my($logfile)=$dir."/".$fileroot.'.'.$logext;

   	print "#   $fileroot.$errext";
   	my($state,$msg);
   	if( -e $errorfile ) {
   		my($size) = -s $errorfile;
   		if( $size ) {
   			print " EXISTS size=$size (ERROR)\n";
   			$state='ERROR';
   			$msg.="<b>file $errorfile</b><br>\n";
   			open(E,$errorfile) or die "Cant read $errorfile\n";
   			my($cnt)=0;
   			while(<E>) {
   				$msg.=$_;
   				last if $cnt++ > 5;
   			}
   			$msg.="\n";
   			close(E);
	   	} else {
	   		print " EXISTS (OK - zero size)\n";
	   		$state="OK" unless $state;
	   	}
   	} else {
   		print " Does NOT Exist (OK)\n";
   		$state="OK" unless $state;
   	}

   	print "#   $fileroot.$logext";
   	if( -e $logfile ) {
   		my($age) = -M $logfile;
   		if( $age > $max_norun_days ) {
   			$age=int($age);
   			print " EXISTS age=$age (ERROR)\n";
   			$state='ERROR';
   			$msg.="$logfile has age $age days\n";
	   	} else {
	   		print " EXISTS (OK)\n";
	   		$state="OK" unless $state;
	   	}
   	} else {
   		print " Does NOT Exist (ERROR)\n";
   		$msg.="$logfile does not exist\n";
   		$state='ERROR';
   	}
   	set_alert($host,$fileroot,$state,$msg);
   }
}

sub set_alert {
	my($system,$subsys,$state,$msg) = @_;
	$msg="Files Look Ok\n" unless $msg;
	$system=$curhost unless $system;
	MlpHeartbeat(
						-monitor_program	=>	$BATCH_ID,
						-subsystem			=>	$subsys,
						-system				=>	$system,
						-state				=>	$state,
						-message_text		=>	$msg	) if $BATCH_ID;
	print "\n[$state]\n";
	print "[$state] $system:$subsys";
	print "\nALERT=".join( "\nALERT=",split("\n",$msg)) if $state eq "ERROR";
	#print "[$state] $msg";
	print "\n[$state]\n";
}

__END__

=head1 NAME

monitor_logfiles.pl - generic log file monitor

=head2 DESCRIPTION

This program monitors logfiles.  Logfiles are produced when batch scripts run using the following format

PROGRAM > program.log 2> program.err

The program tests the .log file datestamp and if it is >

=head2 USAGE

monitor_logfiles.pl --CTLFILE=FILE

-or-

monitor_logfiles.pl --DIR=  --FILEROOT=  --BATCH_SUBKEY=  --MAX_NORUN_DAYS= --LOGEXT= --ERREXT

--HOST=				(fetch from this host - must be on unix)
--DIR=
--FILEROOT=
--BATCH_SUBKEY
--MAX_NORUN_DAYS	(alert if logfile age > DAYS)
--NOCOPY				(dont copy files - assumes --DIR available on curhost and --HOST)
--LOGEXT				(extension for error files = dflt=.log)
--ERREXT				(extension for error files = dflt=.err)

--ctlfile=file : A control file can be used to automate multiple systems.  See conf/win32_cleanup.dat.sample.
The controlfile basically contains the arguments to this program

=head2 BUGS

-host only works on unix

=cu
