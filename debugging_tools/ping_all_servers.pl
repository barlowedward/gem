#!/usr/local/bin/perl-5.6.1
use lib qw(/apps/sybmon/dist/lib /apps/sybmon/dist/ADMIN_SCRIPTS/lib /apps/sybmon/dist/plugins /apps/sybmon/dist/cpan);

use subs qw(debug logdie infof info warning alert);
use File::Basename;
use strict;
use Getopt::Std;
use Sybase::SybFunc;
use Repository;
use Sybase::SybAdminDb;
use Net::myFTP;
use Time::Local;
use vars qw( $login $opt_S $passwd $opt_d );
use Sys::Hostname;

my($UNIX_COMMAND)='id';
#my($PS_FIELD)=9;

BEGIN { $ENV{SYBASE}="/opt/sybase" unless defined $ENV{SYBASE}; }

my $curhostname = hostname();

# Copyright (c) 2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# cd to the right directory
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

$| = 1;

sub usage
{
	print @_;
	print "ping_all_servers.pl -s  [-SSERVER]

	-S SERVER - only check given server
	-s silent - only print problems
\n";
   return "\n";
}

use vars qw($opt_s);

die usage("Bad Parameter List\n") unless getopts('sd');

my($passfile)=get_root_dir()."/pass_db";

die "Error: dbm data file $passfile does not exist.  \nRun discover.pl to create and populate this file.\n" 
	unless (-e $passfile.".dir" and -e $passfile.".pag")
		 or  -e $passfile.".db"
		 or  -e $passfile;

# INITIALIZATION
my(@serverlist);
my(@systemlist);
my(@bserverlist);

# GET SERVER LISTS
db_open(-dbmname=>$passfile);

my $t =localtime(db_param(-name=>"unix_discover_date",-type=>'G'));
print "########################################\n";
print "Connectivity Report \n";
my $ct  = localtime(time);
print "Run At         ",$ct,"\n";
print "Discovery Process Run At ",$t,"\n";
print "########################################\n";

@bserverlist = split(" ",db_param(-name=>"backup_servers"));
@systemlist  = get_password(-type=>"unix");
# @serverlist  = split(" ",db_param(-name=>"sybase_servers"));
@serverlist=get_password(-type=>"sybase");
db_close();

# CALCULATE LENGTHS
my($lengthof)=0;
foreach (@serverlist)  { $lengthof=length if length>$lengthof; }
foreach (@bserverlist) { $lengthof=length if length>$lengthof; }
foreach (@systemlist)  { $lengthof=length if length>$lengthof; }

my($numbad)=0;
my($totalnumbad)=0;

############# UNIX SERVERS ############################
print "########################################\n";
print "=>>> unix systems\n";
print "########################################\n";

foreach my $host ( @systemlist ) {
	my($numbad)=0;
	print "(",__LINE__," ",$numbad,")"  if defined $opt_d;
   my($login,$password)=get_password(-type=>"unix",-name=>$host);
   my(%args)=get_password_info(-type=>"unix",-name=>$host);
	# foreach (keys %args) { print "--> $_ $args{$_} \n"; }

	if( $args{SERVER_TYPE} ne "PRODUCTION"
	and $args{SERVER_TYPE} ne "DEVELOPMENT"
	and $args{SERVER_TYPE} ne "DR"
	and $args{SERVER_TYPE} ne "QA" ) {
		if( ! defined $args{SERVER_TYPE} ) {
			print "\nWARNING:  UNDEFINED SERVER_TYPE IN password file\n.";
		} else {
			print "\nWARNING:  SERVER TYPE ($args{SERVER_TYPE} MUST BE PRODUCTION or DEVELOPMENT or QA or DR.\n";
		}
		$numbad++;
	}

	printf("%".$lengthof."s ", $host);

   my($ftp) = Net::myFTP->new($host,{Debug=>0,Timeout=>5});
   if( ! $ftp )  {
   	print "\nWARNING: Not Reachable $!";
		$numbad++;
   } else {
		print "(",__LINE__," ",$numbad,")"  if defined $opt_d;
      if( ! $ftp->login($login,$password) ) {
			print "\nWARNING: Login/Password Does Not Work $!";
			$numbad++;
		} else {
			print "testing ftp ";
      	$ftp->ascii();
      	my(@rc)= $ftp->fspec('/');
			if( $#rc < 0 ) {
				print "\nWARNING: cant stat / $!";
				$numbad++;
			} else {
				my(@sybase_dirs)=split(/\s/,$args{SYBASE});
				my(@more_sybase_dirs)=split(/\s/,$args{SYBASE_CLIENT})
				if defined $args{SYBASE_CLIENT} and $args{SYBASE_CLIENT} !~ /^\s*$/;
				push @sybase_dirs,@more_sybase_dirs;
				foreach my $file_dir (@sybase_dirs) {
					next if $file_dir =~ /^\s*$/;
  					if( ! $ftp->cwd($file_dir) ) {
      				print("\nWARNING: Cant Chdir To $file_dir on machine $host - check your password file");
						$numbad++;
      				next;
   				}
   				my(@files)=$ftp->ls();
					my $found = "NO";
					foreach (@files) { 
						if( $_ eq "interfaces" ) { $found="OK"; last; } 
						if( $_ =~ /^ini$/i ) {     $found="OK"; last; } 
					} 
					if( $found eq "NO" ) {
      				print("\nWARNING: Directory $file_dir does not contain interfaces file on machine $host - check your password file");
						$numbad++;
					} else {
						print "ok ";
					}
				}
			}
      	$ftp->close();
		}
	}
   $ftp->quit() if defined $ftp;

#####
	print "\tTesting rsh to $host\n";
	my($dat)=do_rsh(-login=>$login, -hostname=>$host, 
			-command=>"echo helloworld",
			-debug=>$DEBUG);
	if( $dat =~ /^failure/ ) {
		
		push @warnings, "rsh to $host - ".$dat;
		print " $dat\n";
		next;
	} elsif( $dat eq "helloworld" ) {
		print " test ok\n" if $DEBUG;
	} else {
		die "HOW ODD - THE COMMAND RETURNED $dat\n";
	}
	print " ok\n" if $DEBUG;
#########
	print "(",__LINE__," ",$numbad,")"  if defined $opt_d;
	if( defined $args{RSH_OK} 
	and ($args{RSH_OK} eq "Y" or $args{RSH_OK} eq 'y') ) {
		my($dat)=do_rsh(-command=>$UNIX_COMMAND,
						-hostname=>$host, -login=>$login);
		#my $dat;
		#print "testing rsh ";
		#my($cmd);
		#if( $host eq $curhostname ) {
			#$cmd = "$UNIX_COMMAND 2>&1";
		#} else {
			#$cmd = "rsh -l $login $host $UNIX_COMMAND 2>&1";
		#}
		#$dat=`$cmd`;
		if( $dat=~ /^failure/ ) {
			print "\nWARNING: $cmd returned $dat" ;
			$numbad++;
		} elsif( $dat=~ /not found$/ ) {
			print "\nWARNING: $cmd returned no data or command not found" ;
			$numbad++;
		} elsif( $dat=~ /permission denied$/ ) {
			print "\nWARNING: $cmd returned $dat" ;
			$numbad++;
		} elsif( $dat=~/uid/ and $dat=~/gid/ ) {
				print "(ok) ";
		} else {
			print "\nWARNING: odd format... ($cmd) returned ($dat)\n";
			$numbad++;
		}
	}
	print "(",__LINE__," ",$numbad,")"  if defined $opt_d;
	print " ***all ok***" if $numbad==0;
	print "FAILCOUNT=($numbad)" if $numbad ne "0";
	$totalnumbad++ if $numbad ne "0";
	print "\n";
	print "(",__LINE__," ",$numbad,")"  if defined $opt_d;
}

############# BACKUP SERVERS ############################
print "########################################\n";
print "=>>> backup servers\n";
print "########################################\n";
foreach my $server (@bserverlist) {
	printf("%".$lengthof."s ", $server);
	
	my($rc) = db_syb_ping($server,'sa','abc');
	if( $rc eq "0" or $rc eq "FAILED: REASON: LOGIN INCORRECT" ) {
		print " ok...\n";
	} else {
		$numbad++;
		print " Error Connecting to $server: $rc\n";
	}
}

############# SYBASE SERVERS ############################
print "########################################\n";
print "=>>> sybase servers\n";
print "########################################\n";
foreach my $server (@serverlist) {
	printf("%".$lengthof."s ", $server);

	($login,$passwd)=get_password(-type=>"sybase",-name=>$server);
	warn "Must pass username\n"	  	unless defined $login;
	warn "Must pass password\n"	  	unless defined $passwd;
	next unless defined $login and defined $passwd;
	
	my($rc) = db_syb_ping($server,$login,$passwd);
	if( $rc eq "0" ) {
		print " ok...\n";
	} else {
		$numbad++;
		print " Error Connecting to $server: $rc\n";
	}
}

exit($numbad);

__END__

=head1 NAME 

ping_server.pl - utility to ping databases

=head2 USAGE

	ping_server.pl -SSERVER 

=head2 SYNOPSIS

pings all servers in your password files.  Returns number of servers
that cant be connected to.  Prints status of your servers.  Has no 
embedded error handling.

=cut

