# copyright (c) 2003-2008 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

package CommonFunc;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use File::Basename;
use Cwd;
use Sys::Hostname;
use Repository;

my($LOCK_DIAG);			# set if you want lock file diagnostics

BEGIN {
	if( $] < 5.008001 and defined $ENV{PROCESSOR_LEVEL} ) {
		if( defined $^V and $^V!~/\s*$/) {
			die "Error : You must have perl 5.8.1 installed under Win32.  You are running $^V \n";
		}
		my($x)=$];
		$x=~s/0+/\./g;
		$x=~s/\.\.+/\./g;
		die "Error : You must have perl 5.8.1 installed.  You are running $x ($])\n";
	}
}

$VERSION= 1.0;

@ISA      = (	'Exporter'	);
@EXPORT   = ( 'get_version','is_nt','send_mail_message','cd_home','today','get_tempdir','get_job_list','read_configfile',
	'get_tempfile', 'is_up', 'reword','mark_up','is_interactive','I_Am_Up','Am_I_Up', "I_Am_Down","GetLockfile","__debugmsg","__statusmsg",
	'read_gemconfig', 'close_gemconfig', 'write_gemconfig', 'read_gemconfig_quick', 'find_fullspec' );

sub find_fullspec {		# take a path and return the full spec path
	my($inpath)=@_;
	my($outpath);

	open(CMD,"net use |") or die "Cant Run net use";
	my($ok)="FALSE";
	
	if( substr($inpath,1,1) ne ":" ) {
		warn "Shouldnt Be Parsing Unix Path $inpath on Windows\n";
		return $inpath;
	}
	
	my($a)=substr($inpath,0,2);
	while(<CMD>) {
		next if /^\s*$/ or /^New connections/;
		if( /^Status\s*Local\s*Remote\s*Network/ ) {
			$ok="TRUE";		
			next;
		}	
		next if /-----------------------/ or /The command compl/;
		# next unless /^OK/;
		chomp;
	
		my($status,$localdr,$remote,$network)=split;		
		if( $localdr eq uc($a) ) {		
			$outpath = $remote.substr($inpath,2)
		}
	}
	close(CMD);
	die "Unable to parse 'net use' for base directory. pls seek help" unless $ok eq "TRUE";
	$outpath =~ s.\/.\\.g;
	return $outpath;	
}

sub send_mail_message {
	my($to,$from,$smtpserver,$subject,$msgtxt)=@_;
	die "[send_mail_message] FATAL: NO mail host passed to send_mail_message" if $^O =~ /MSWin32/ and ! $smtpserver;
	
	if( ! $to ) {			# no send to??? send to me! = stupid hack but at least i can see the message
		print "[send_mail_message] FAIL - NO SUBJECT PASSED TO send_mail_message\n";
		$to = "ebarlow\@mlp.com";
		$from="FAIL\@mlp.com";
		$subject="FAIL - $subject";
	}
	
	die "MUST PASS From Address to send_mail_message - $to,$from,$subject,$msgtxt" unless $from;
	
	print "[send_mail_message] To $to \n";
	print "[send_mail_message] From $from \n";
	print "[send_mail_message] Host $smtpserver \n";
	print "[send_mail_message] Subj $subject \n";

	use MIME::Lite;
	my($msg)=MIME::Lite->new(
				To=>	$to,
				From=>$from,
				Subject=>$subject,
				Type=>'multipart/related' );
	return ("ERROR: Mail Test Fails") unless $msg;
	$msg->attach(Type=>'text/html', Data=>$msgtxt );
	my($rc);
	eval {
		if( $^O =~ /MSWin32/ ) {
			print "[send_mail_message] Test - sending Mail via Mailhost '",$smtpserver,"'\n";
			$rc = $msg->send("smtp",$smtpserver);
		} else {
			print "[send_mail_message] Test - sending Unix Mail\n";
			$rc = $msg->send();
		}
	};
	my($err)=join("",$@) if $@;
	print "[send_mail_message] ERROR: $err\n" if $err;
	print "[send_mail_message] SUCCESSFUL: MAIL SENT ($rc=$rc)\n";
}

sub reword {
	my($txt,$wantlen,$maxlen)=@_;

	if( length($txt)>=$maxlen ) {
	   	$txt=~s/\s+/ /g;
			my(@words)=split(/ /,$txt);
	   	my($curlen)=0;
   		$txt="";
   		foreach(@words) {
      			if( $curlen+length() > $wantlen ) {
   				if( $curlen+length()>$maxlen ) {
   					$txt.="\n".$_;
   					$curlen=length();
	   			} else {
   					$txt.=" ".$_."\n";
   					$curlen=0;
   				}
   			} else {
   				$curlen+=length()+1;
   				$txt.=" ".$_;
   			}
   		}
   		$txt=~s/\n\s+/\n/g;
			$txt=~s/^\s+//;
			$txt=~s/\s+$//;
   	}
   	return $txt;
}

sub get_version {
	my($file)=@_;
	my($version_file, $VERSION);
	$version_file=$file if defined $file and -r $file;
	$version_file="version" if -e "version" and ! defined $version_file;
	$version_file="VERSION" if -e "VERSION" and ! defined $version_file;
	$version_file="../version" if ! defined $version_file and -e "../version";
	$version_file="../VERSION" if ! defined $version_file and -e "../VERSION";
	my($codedir)=dirname($0);
	$version_file=$codedir."/VERSION" if ! defined $version_file and -e $codedir."/VERSION";
	$version_file=$codedir."/version" if ! defined $version_file and -e $codedir."/version";
	die "Cant find a file named VERSION anywhere\nThis file should be located in . or .. or $codedir directories\n" unless defined $version_file;

	open (VER,$version_file) or die "Cant read file $version_file...\n";
	while (<VER>) { chomp; next if /^\s*$/; $VERSION=$_; }
	close VER;

	my($sht_version)=$VERSION;
	$sht_version=~s/\(.+$//;
	$sht_version=~s/\s+$//;
	$sht_version=~s/^[\D\s]+//g;

	return($VERSION, $sht_version);
}

my($sv_is_nt);
sub is_nt {				# set something if you are on NT
	return $sv_is_nt if defined $sv_is_nt;
	$sv_is_nt=0;

	# ENV{PROCESSOR_LEVEL} worked well for a while but got confused at aleast at one client bummer
	$sv_is_nt=1 if $^O =~ /MSWin32/;
	
	return $sv_is_nt;
}

sub cd_home {
	# change to this directory if run from somewhere else
	my  $curdir=dirname($0);
	chdir($curdir) or die "Cant cd to $curdir: $!\n";
	return cwd();
}

sub today
{
	my(@t)=localtime(time);
	$t[4]++;
	$t[5]+=1900;
	substr($t[3],0,0)="0" if length($t[3])==1;
	substr($t[4],0,0)="0" if length($t[4])==1;
	substr($t[5],0,0)="0" if length($t[5])==1;
	substr($t[2],0,0)="0" if length($t[2])==1;
	substr($t[1],0,0)="0" if length($t[1])==1;
	substr($t[0],0,0)="0" if length($t[0])==1;
	return $t[5].$t[4].$t[3].".".$t[2].$t[1].$t[0];
}

# Check for Lock File and Dont Restart unless it exists
# Args: -threshtime , -filename
sub is_up { 	return Am_I_Up(@_); }
sub mark_up { 	return I_Am_Up(@_); }

sub get_job_list
{
	my(%config)=@_;
	die "Must pass %config" unless %config;

	my(@keys)=qw(
	SERVER_NAME SERVER_DIRECTORY IGNORE_SERVER NUM_DAYS_TO_KEEP NUM_BACKUPS_TO_KEEP
	NUMBER_OF_STRIPES DUMP_FILES_PER_SUBDIR SYBASE_COMPRESSION_LEVEL COMPRESS UNCOMPRESS DATABASE_IGNORE_LIST DBCC_IGNORE_DB
	MAIL_TO DO_EXTERNAL_COMPRESS COMPRESS_DEST COMPRESS_LATEST DO_UPDSTATS DO_AUDIT AUDIT_PURGE_DAYS SUCCESS_MAIL_TO
	DO_DBCC DO_BCP BCP_COMMAND BCP_TABLES DO_PURGE DO_INDEXES DO_REORG REORG_ARGS UPD_STATS_FLAGS
	IS_REMOTE SERVER_HOSTNAME SERVER_HOST_LOGIN SERVER_HOST_PASSWORD DO_DUMP DO_CLEARLOGSBEFOREDUMP
	DO_LOAD XFER_TO_HOST XFER_SCRATCH_DIR XFER_TO_SERVER XFER_TO_DB
	XFER_FROM_DB XFER_TO_DIR XFER_BY_FTP CLIENT_PATH_TO_DIRECTORY MAIL_HOST SERVER_DUMP_DIRECTORY SERVER_LOG_DIRECTORY
	CLIENT_PATH_TO_DUMP_DIR CLIENT_PATH_TO_LOG_DIR);

	my(%keyhash);
	my(%jobhash);
	foreach (@keys) { $keyhash{$_}=1; }

	foreach my $k ( keys %config ) {

		next if defined $keyhash{$k};
		next if $k !~ /\_/;
		my($job,@parts)=split(/\_/,$k);
		my(@permuted);
		while(@parts) {
			my($v)=join("_",@parts);
			if( defined $keyhash{ $v } ){
				#print "Job $job found From $k\n" unless defined $jobhash{$job};
				$jobhash{$job}=1;
				last;
			}
			$job.="_".shift @parts;
		}
	}
	return(keys %jobhash);
}

# returns a temporary file name
sub get_tempdir {
	if(  -d "C:/tmp" and -w "C:/tmp" ) {
		return "C:/tmp";
	} elsif( -d "C:/temp" and -w "C:/temp") {
		return "C:/temp";
	} elsif( -d "/tmp" ) {
		return "/tmp";
	} else {
		die "Cant find temporary file to return\n";
	}
}

# returns a temporary file name
sub get_tempfile {
	my($base)=@_;
	if( defined $base ) {
		return $base.".".$$;
	} elsif(  -d "C:/tmp" and -w "C:/tmp" ) {
		return "C:/tmp/".basename($0).".".$$;
	} elsif( -d "C:/temp" and -w "C:/temp") {
		return "C:/temp/".basename($0).".".$$;
	} elsif( -d "/tmp" ) {
		return "/tmp/".basename($0).".".$$;
	} elsif( -w dirname($0) ) {
		return dirname($0)."/".basename($0).".".$$;
	} else {
		die "Cant find temporary file to return\n";
	}
}

my(%GemConfigHash);
my($GemConfig_Filename);
#use Config::Simple;

use Fcntl;

sub write_gemconfig
{
	my(%args)=@_;
	
	print "Writing gem.dat\n";
	my(%saved_values);
	foreach ( keys %GemConfigHash ) { 	$saved_values{$_} = $GemConfigHash{$_}; 	}
	# tied(%GemConfig)->write();
	close_gemconfig();
	print "Sleeping...\n";
	sleep(1);
	%GemConfigHash  = undef;
	
	return undef if $args{-refresh_GemConfig} eq "FALSE";
	
	print "ReReading $GemConfig_Filename...\n";
	my($a) = read_gemconfig( -filename=>$GemConfig_Filename );
	my($c)=0;
	foreach ( keys %saved_values ) {
		next  if $saved_values{$_} eq $a->{$_} or $_ =~ /^\_/;
		print __FILE__," Line=",__LINE__," WARNING Key $_ old=$saved_values{$_} new=$a->{$_} \n";
		$a->{$_}  = $saved_values{$_};
		$c++;
	}
	print "WARNING... FOUND $c ERRORS\n" if $c;	
	return $a;
}

sub close_gemconfig { untie %GemConfigHash; }

sub read_gemconfig
{
	my(%args)=@_;

	my $filename=$args{-filename} if $args{-filename} and -e $args{-filename};
	$filename=get_conf_dir()."/gem.dat" if  ! defined $filename and -r get_conf_dir()."/gem.dat";
	$filename = "gem.dat"		if ! defined $filename and -r "gem.dat";
	$filename="../gem.dat"		if ! defined $filename and -r "../gem.dat";
	
	return undef if $args{-isconfigured} and ! defined $filename;

	die("No Configuration File gem.dat Found.  Searched . .. and ".get_conf_dir()) 
		unless defined $filename;
# 	print __FILE__," ",__LINE__,"\nDIAG CommonFunc.pm: read_gemconfig(file->$filename) at ".scalar(localtime(time))."\n"; 
	print "CommonFunc.pm: read_gemconfig(file->$filename) at ".scalar(localtime(time))."\n" 
		if defined $args{-debug};

	# my $cfg = new Config::Simple($filename);
	# tie %GemConfigHash, "Config::Simple",$filename;
	$GemConfig_Filename = $filename;
	use Tie::Config;
	if( $args{READONLY} or $args{-READONLY} ) {
		tie %GemConfigHash, 'Tie::Config', $filename, O_RDONLY;
	} else {
		die "$filename is not writeable" unless -w $filename;
		tie %GemConfigHash, 'Tie::Config', $filename, O_RDWR;
	}
	
	if( $args{-debug} ){
		print "Starting Print $filename : GemConfig\n";
		foreach ( keys %GemConfigHash ) { 
			print "   ",$_, " " , $GemConfigHash{$_} , "\n" unless /^\_/; 
		}
		print "Done Print of GemConfig\n";
	}

	print "CommonFunc.pm: returning configuration info at ".scalar(localtime(time))."\n" 
		if defined $args{-debug};
	return(\%GemConfigHash);
}

sub read_gemconfig_quick
{
	my(%args)=@_;
	my $filename=$args{-filename} if $args{-filename};
	$filename = "gem.dat"		if ! defined $filename and -r "gem.dat";
	$filename="../gem.dat"		if ! defined $filename and -r "../gem.dat";
	$filename=get_conf_dir()."/gem.dat"
		if ! defined $filename and -r get_conf_dir()."/gem.dat";
	die("No Configuration File gem.dat Found.  Searched . .. and ".get_conf_dir()) unless defined $filename;
	die "$filename is not writeable" unless -w $filename;
	print "CommonFunc.pm: read_gemconfig(file->$filename) at ".scalar(localtime(time))."\n" if defined $args{-debug};

	open( F, $filename ) or die "Cant read $filename\n";
	while(<F>) {
		chop;chomp;
		next if /^\s*#/;
		s/\s*=\s*/=/;
		my($k,$v)=split(/\s*=\s*/);
		$GemConfigHash{$k} = $v;
	}
	close(F);
	print "CommonFunc.pm: returning configuration info at ".scalar(localtime(time))."\n" if defined $args{-debug};
	return(\%GemConfigHash);
}

sub read_configfile
{
	my(%args)=@_;
	my($opt_J)= $args{-job} || "UNKNOWN";
	my($opt_S)= $args{-srv};
	my(@errors);

	print __FILE__," CommonFunc.pm: read_configfile() at ".scalar(localtime(time))."\n" if defined $args{-debug};

	my(%OUTPUT);
	
	my $filename;	
	$filename=$args{-filename} if $args{-filename} and -r  $args{-filename};	
	die "File $filename exists but is not readable" if $args{-filename} and -e  $args{-filename} and ! -r  $args{-filename};
	
	$args{-filename}="backup_plans.dat" unless $args{-filename};
	$filename = get_conf_dir()."/".basename($args{-filename}) 
		if ! $filename and -r get_conf_dir()."/".basename($args{-filename});
	
	$args{-filename}="configure.cfg" unless $filename;
	$filename = get_conf_dir()."/configure.cfg" 
		if ! $filename and -r get_conf_dir()."/configure.cfg" ;
	
	#$filename="configure.cfg"		if ! $filename and -r "configure.cfg";
	#$filename="../configure.cfg"		if ! defined $filename and -r "../configure.cfg";
	#$filename=get_conf_dir()."/configure.cfg"
	#	if ! defined $filename and -r get_conf_dir()."/configure.cfg";

	die("No Configuration File Found.  Searched . .. and ".get_conf_dir()) unless defined $filename;
	
	my($errstr)="Error In Configuration File $filename\n";
	my(%C);
	print "CommonFunc.pm: file=$filename at ".scalar(localtime(time))."\n" if defined $args{-debug};

	#
	# read the config file
	#
	open(CFGFILE,$filename) or die("Cant Read $filename: $!\n");
	#my($cur_vbl)="";
	while (<CFGFILE>) {
		next if /^\s*$/;
		s/\s+$//;
		chomp;
		next if /^\s*#/;
  		s/^\s+//;
  		s/\s+$//;

		my($key,$value)=split(/=/,$_,2);

		$key=~s/DSQUERY$/SERVER_NAME/;
		$key=~s/REMOTE_DIR$/SERVER_DIRECTORY/;
		$key=~s/DO_COMPRESS$/DO_EXTERNAL_COMPRESS/;
		$key=~s/REMOTE_DUMP_DIR$/SERVER_DUMP_DIRECTORY/;
		$key=~s/REMOTE_LOG_DIR$/SERVER_LOG_DIRECTORY/;
		$key=~s/LOCAL_DIR$/CLIENT_PATH_TO_DIRECTORY/;
		$key=~s/LOCAL_DUMP_DIR$/CLIENT_PATH_TO_DUMP_DIRECTORY/;
		$key=~s/LOCAL_LOG_DIR$/CLIENT_PATH_TO_LOG_DIRECTORY/;

		$key=~s/REMOTE_HOST$/SERVER_HOSTNAME/;
		$key=~s/REMOTE_ACCOUNT$/SERVER_HOST_LOGIN/;
		$key=~s/REMOTE_PASSWORD$/SERVER_HOST_PASSWORD/;


		if( defined $value and $value !~ /^\s*$/) {
			$C{$key}=$value;
		} else {
			if( $key=~/\_SYBASE\_COMPRESSION\_LEVEL/ ) {
				s/_SYBASE_COMPRESSION_LEVEL//;
				my $msg=$errstr."SYBASE_COMPRESSION_LEVEL must be defined for server $_ - it is set to empty string - either remove this variable or set it to level 0 (no compression)\n";
				if( defined $args{-nodie} ) {
					push @errors,$msg;
				} else {
					die $msg;
				}
			}
		}
	}
	close CFGFILE;

	my($filename2)=$filename;
	$filename2=~s/configure.cfg/gem.dat/g;
	if( -r $filename2 ) {
		print "CommonFunc.pm: file=$filename2 at ".scalar(localtime(time))."\n" if defined $args{-debug};
		open(CFGFILE2,$filename2) or die("Cant Read $filename2: $!\n");
		while (<CFGFILE2>) {
			next if /^\s*$/;
			s/\s+$//;
			chomp;
			next if /^\s*#/;
	  		s/^\s+//;
	  		s/\s+$//;
			my($key,$value)=split(/=/,$_,2);
			if( defined $value and $value !~ /^\s*$/) {
				$C{$key}=$value;
			} else {
				if( $key=~/\_COMPRESSION\_LEVEL/ ) {
					s/_SYBASE_COMPRESSION_LEVEL//;
					my $msg=$errstr."SYBASE_COMPRESSION_LEVEL must be defined for server $_ - it is set to empty string - either remove this variable or set it to level 0 (no compression)\n";
					if( defined $args{-nodie} ) {
						push @errors,$msg;
					} else {
						die $msg;
					}
				}
			}
		}
		close CFGFILE2;
	}
	print "CommonFunc.pm: validating data at ".scalar(localtime(time))."\n" if defined $args{-debug};

	$OUTPUT{JOBNAME}= $opt_J || $opt_S;
	$OUTPUT{SYBASE} = $C{"SYBASE"} || $ENV{SYBASE};
	$OUTPUT{SERVER_NAME}= $C{$opt_J."_SERVER_NAME"} || $opt_J;
	$OUTPUT{BASE_BACKUP_DIR} =  $C{$opt_J."_BASE_BACKUP_DIR"} || $C{BASE_BACKUP_DIR};
	$OUTPUT{CLIENT_PATH_TO_DIRECTORY}
		=	$C{$opt_J."_CLIENT_PATH_TO_DIRECTORY"} || $C{CLIENT_PATH_TO_DIRECTORY} || $OUTPUT{BASE_BACKUP_DIR};
	$OUTPUT{SERVER_DIRECTORY}
		=	$C{$opt_J."_SERVER_DIRECTORY"} || $C{SERVER_DIRECTORY} || $OUTPUT{CLIENT_PATH_TO_DIRECTORY} || $OUTPUT{BASE_BACKUP_DIR};
	$OUTPUT{SERVER_DUMP_DIRECTORY} = $C{$opt_J."_SERVER_DUMP_DIRECTORY"};
	$OUTPUT{SERVER_LOG_DIRECTORY}  = $C{$opt_J."_SERVER_LOG_DIRECTORY"};
	$OUTPUT{CLIENT_PATH_TO_DUMP_DIR} = $C{$opt_J."_CLIENT_PATH_TO_DUMP_DIR"};
	$OUTPUT{CLIENT_PATH_TO_LOG_DIR}  = $C{$opt_J."_CLIENT_PATH_TO_LOG_DIR"};
	$OUTPUT{IGNORE_SERVER}
		=	$C{$opt_J."_IGNORE_SERVER"} || $C{IGNORE_SERVER};
	$OUTPUT{NUM_DAYS_TO_KEEP}
		= $C{$opt_J."_NUM_DAYS_TO_KEEP"} || $C{NUM_DAYS_TO_KEEP};
	if( ! defined $C{$opt_J."_NUM_BACKUPS_TO_KEEP"} or length($C{$opt_J."_NUM_BACKUPS_TO_KEEP"})==0 ) {
		$OUTPUT{NUM_BACKUPS_TO_KEEP} = $C{NUM_BACKUPS_TO_KEEP};
	} else {
		$OUTPUT{NUM_BACKUPS_TO_KEEP} =  $C{$opt_J."_NUM_BACKUPS_TO_KEEP"};
	}
	$OUTPUT{NUMBER_OF_STRIPES}
		= defined($C{$opt_J."_NUMBER_OF_STRIPES"}) ? $C{$opt_J."_NUMBER_OF_STRIPES"} : $C{NUMBER_OF_STRIPES};
	$OUTPUT{DUMP_FILES_PER_SUBDIR}
		= defined($C{$opt_J."_DUMP_FILES_PER_SUBDIR"}) ? $C{$opt_J."_DUMP_FILES_PER_SUBDIR"} : $C{DUMP_FILES_PER_SUBDIR};

	# pretty lame... must do this if value is 0 doh...
	$OUTPUT{SYBASE_COMPRESSION_LEVEL}
		= 	length($C{$opt_J."_SYBASE_COMPRESSION_LEVEL"}) ? $C{$opt_J."_SYBASE_COMPRESSION_LEVEL"} : $C{SYBASE_COMPRESSION_LEVEL};

	$OUTPUT{COMPRESS}			= 	$C{$opt_J."_COMPRESS"} || $C{COMPRESS};
	$OUTPUT{COMPRESS_DEST}	= 	$C{$opt_J."_COMPRESS_DEST"} || $C{COMPRESS_DEST};
	$OUTPUT{UNCOMPRESS }		= 	$C{$opt_J."_UNCOMPRESS"} || $C{UNCOMPRESS};
	$OUTPUT{DATABASE_IGNORE_LIST}		=	$C{$opt_J."_DATABASE_IGNORE_LIST"} || $C{DATABASE_IGNORE_LIST};
	$OUTPUT{DBCC_IGNORE_DB}	=	$C{$opt_J."_DBCC_IGNORE_DB"} || $C{DBCC_IGNORE_DB};
	$OUTPUT{MAIL_FROM}		=	$C{$opt_J."_MAIL_FROM"} || $C{MAIL_FROM};
	$OUTPUT{SUCCESS_MAIL_TO}=	$C{$opt_J."_SUCCESS_MAIL_TO"} || $C{SUCCESS_MAIL_TO};
	$OUTPUT{MAIL_TO}			=	$C{$opt_J."_MAIL_TO"} || $C{MAIL_TO};
	$OUTPUT{MAIL_HOST}		=	 $C{MAIL_HOST};
	$OUTPUT{DO_EXTERNAL_COMPRESS}		=	lc($C{$opt_J."_DO_EXTERNAL_COMPRESS"}||$C{DO_EXTERNAL_COMPRESS})||"n";
	$OUTPUT{DO_ONLINEDB}		=	lc($C{$opt_J."_DO_ONLINEDB"}||$C{DO_ONLINEDB})||"n";
	$OUTPUT{COMPRESS_LATEST}=	lc($C{$opt_J."_COMPRESS_LATEST"}||$C{COMPRESS_LATEST})||"n";
	$OUTPUT{UPD_STATS_FLAGS}=	$C{$opt_J."_UPD_STATS_FLAGS"} || $C{UPD_STATS_FLAGS} || "";
	$OUTPUT{DO_UPDSTATS}		=	lc($C{$opt_J."_DO_UPDSTATS"} || $C{DO_UPDSTATS}) || "n";
	$OUTPUT{DO_AUDIT}			=	lc($C{$opt_J."_DO_AUDIT"} || $C{DO_AUDIT}) || "n";
	$OUTPUT{AUDIT_PURGE_DAYS}	=	lc($C{$opt_J."_AUDIT_PURGE_DAYS"} || $C{AUDIT_PURGE_DAYS}) || "30";
	$OUTPUT{DO_DBCC}			=	lc($C{$opt_J."_DO_DBCC"} || $C{DO_DBCC}) || "n";
	$OUTPUT{DO_BCP}			=	lc($C{$opt_J."_DO_BCP"} || $C{DO_BCP}) || "n";
	$OUTPUT{BCP_COMMAND}		=	$C{$opt_J."_BCP_COMMAND"} || $C{BCP_COMMAND} || "";
	$OUTPUT{BCP_TABLES}		=	$C{$opt_J."_BCP_TABLES"} || $C{BCP_TABLES};
	$OUTPUT{DO_PURGE}			=	lc($C{$opt_J."_DO_PURGE"}||$C{DO_PURGE})||"n";

	$OUTPUT{DO_INDEXES}		=  lc($C{$opt_J."_DO_INDEXES"}||$C{DO_INDEXES})||"n";
	$OUTPUT{DO_REORG}			=  lc($C{$opt_J."_DO_REORG"} || $C{DO_REORG}) || "n";
	$OUTPUT{REORG_ARGS}		=  $C{$opt_J."_REORG_ARGS"}||$C{REORG_ARGS}||"--REORG_COMPACT";
	$OUTPUT{IS_REMOTE}		=	lc($C{$opt_J."_IS_REMOTE"} || $C{IS_REMOTE}) || "n";

	$OUTPUT{SERVER_HOSTNAME} =	$C{$opt_J."_SERVER_HOSTNAME"}
						|| $C{$opt_J."_RSH_HOST"}
						|| $C{SERVER_HOSTNAME}
						|| $C{RSH_HOST};

	$OUTPUT{SERVER_HOST_PASSWORD} =	$C{$opt_J."_SERVER_HOST_PASSWORD"}
						|| $C{SERVER_HOST_PASSWORD};

	$OUTPUT{SERVER_HOST_LOGIN} =	$C{$opt_J."_SERVER_HOST_LOGIN"}
						|| $C{$opt_J."_RSH_ACCOUNT"}
						|| $C{SERVER_HOST_LOGIN}
						|| $C{RSH_ACCOUNT};

	$OUTPUT{DO_DUMP}		=	lc($C{$opt_J."_DO_DUMP"}||$C{DO_DUMP})||"n";
	$OUTPUT{DO_LOAD}		=	lc($C{$opt_J."_DO_LOAD"} || $C{DO_LOAD}) || "n";
	$OUTPUT{DO_CLEARLOGSBEFOREDUMP}	=	lc($C{$opt_J."_DO_CLEARLOGSBEFOREDUMP"}||$C{DO_CLEARLOGSBEFOREDUMP})||"y";

	$OUTPUT{XFER_TO_SERVER} 		= $C{$opt_J."_XFER_TO_SERVER"};
	$OUTPUT{XFER_TO_DB} 				= $C{$opt_J."_XFER_TO_DB"};
	$OUTPUT{XFER_FROM_DB} 			= $C{$opt_J."_XFER_FROM_DB"};
	$OUTPUT{XFER_TO_DIR} 			= $C{$opt_J."_XFER_TO_DIR"};
	$OUTPUT{XFER_TO_DIR_BY_TARGET}= $C{$opt_J."_XFER_TO_DIR_BY_TARGET"};
	$OUTPUT{XFER_BY_FTP} 			= lc($C{$opt_J."_XFER_BY_FTP"});
	$OUTPUT{XFER_TO_HOST} 			= $C{$opt_J."_XFER_TO_HOST"};
	$OUTPUT{XFER_SCRATCH_DIR} 		= $C{$opt_J."_XFER_SCRATCH_DIR"};

	$OUTPUT{SRVR_DIR_BY_BKSVR} 	= $OUTPUT{SERVER_DIRECTORY}."/".$OUTPUT{SERVER_NAME};
	$OUTPUT{DUMP_DIR_BY_BKSVR}		= $OUTPUT{SERVER_DIRECTORY}."/".$OUTPUT{SERVER_NAME}."/dbdumps";
	$OUTPUT{TRAN_DIR_BY_BKSVR} 	= $OUTPUT{SERVER_DIRECTORY}."/".$OUTPUT{SERVER_NAME}."/logdumps";

	$OUTPUT{NT_PERL_LOCATION}		= 	$C{NT_PERL_LOCATION};
	$OUTPUT{NT_CODE_LOCATION}		= 	$C{NT_CODE_LOCATION};
	$OUTPUT{UNIX_PERL_LOCATION}	=	$C{UNIX_PERL_LOCATION};
	$OUTPUT{UNIX_CODE_LOCATION} 	=	$C{UNIX_CODE_LOCATION};
	$OUTPUT{CODE_LOCATION_ALT1} 	=	$C{CODE_LOCATION_ALT1};
	$OUTPUT{CODE_LOCATION_ALT2} 	=	$C{CODE_LOCATION_ALT2};

	print "CommonFunc.pm: copying data at ".scalar(localtime(time))."\n" if defined $args{-debug};
	if( ! defined $args{-job} and ! defined $opt_S ) {
		foreach( keys %C ) {
			$OUTPUT{$_}=$C{$_} unless defined $OUTPUT{$_};
		}
	}

	print "CommonFunc.pm: final validation at ".scalar(localtime(time))."\n" if defined $args{-debug};
	if( $OUTPUT{XFER_BY_FTP} eq "y" ) {
		unless( defined $OUTPUT{XFER_TO_HOST}
			and 	$OUTPUT{XFER_TO_HOST} !~ /^\s*$/ ){
				my $msg=$errstr."IF XFER_BY_FTP is defined for $opt_J, then XFER_TO_HOST must also be\n";
				if( defined $args{-nodie} ) {
					push @errors,$msg;
				} else {
					die $msg;
				}
		}
		unless ( defined $OUTPUT{XFER_SCRATCH_DIR} and $OUTPUT{XFER_SCRATCH_DIR} !~ /^\s*$/ ){
			my $msg=$errstr."IF XFER_BY_FTP is defined for $opt_J, then XFER_SCRATCH_DIR must also be";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
					die $msg;
			}
		}
	}

#	unless ( $OUTPUT{NUM_BACKUPS_TO_KEEP}>=1 ) {
#			my $msg=$errstr."NUM_BACKUPS_TO_KEEP MUST BE >=1";
#			if( defined $args{-nodie} ) {
#				push @errors,$msg;
#			} else {
#				die $msg;
#			}
#	}
	if( defined $OUTPUT{SYBASE} and ! -d $OUTPUT{SYBASE} ){
			my $msg=$errstr."SYBASE ($OUTPUT{SYBASE}) MUST BE A DIRECTORY";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				warn $msg;
			}
	}

	$OUTPUT{NT_PERL_LOCATION}		=~ s.\\.\/.g;
	$OUTPUT{NT_CODE_LOCATION}		=~ s.\\.\/.g;
	$OUTPUT{UNIX_PERL_LOCATION}	=~ s.\\.\/.g;
	$OUTPUT{UNIX_CODE_LOCATION} 	=~ s.\\.\/.g;
	$OUTPUT{CODE_LOCATION_ALT1} 	=~ s.\\.\/.g if defined $OUTPUT{CODE_LOCATION_ALT1};
	$OUTPUT{CODE_LOCATION_ALT2} 	=~ s.\\.\/.g if defined $OUTPUT{CODE_LOCATION_ALT2};
	$OUTPUT{BASE_BACKUP_DIR} 		=~ s.\\.\/.g;

	# first we try to replace base backup directory
	print "CommonFunc.pm: checking base backup dir at ".scalar(localtime(time))."\n"
		if defined $args{-debug};

	#
	# IF NO BASE_BACKUP_DIR make one up from CODE_LOCATION/data/BACKUP_LOGS
	#
	unless( defined $OUTPUT{BASE_BACKUP_DIR} ){
		if( $OUTPUT{UNIX_CODE_LOCATION} !~ /^\s*$/ and -d $OUTPUT{UNIX_CODE_LOCATION} ) {
				$OUTPUT{BASE_BACKUP_DIR}=$OUTPUT{UNIX_CODE_LOCATION}."/data/BACKUP_LOGS";
		} elsif(  $OUTPUT{NT_CODE_LOCATION} !~ /^\s*$/	and -d $OUTPUT{NT_CODE_LOCATION} ) {
				$OUTPUT{BASE_BACKUP_DIR}=$OUTPUT{NT_CODE_LOCATION}."/data/BACKUP_LOGS";
		} elsif(  $OUTPUT{CODE_LOCATION_ALT1} !~ /^\s*$/ and -d $OUTPUT{CODE_LOCATION_ALT1} ) {
				$OUTPUT{BASE_BACKUP_DIR}=$OUTPUT{CODE_LOCATION_ALT1}."/data/BACKUP_LOGS";
		} elsif(  $OUTPUT{CODE_LOCATION_ALT2} !~ /^\s*$/ and -d $OUTPUT{CODE_LOCATION_ALT2} ) {
				$OUTPUT{BASE_BACKUP_DIR}=$OUTPUT{CODE_LOCATION_ALT2}."/data/BACKUP_LOGS";
		}
	}

	#
	# TEST BASE_BACKUP_DIR for existence
	#   if it does NOT exist, get $dir = basename(BASE_BACKUP_DIR) and swap em around
	#
	unless ( -d $OUTPUT{BASE_BACKUP_DIR} ){
		my($found)="TRUE";
		my($dir)=$OUTPUT{BASE_BACKUP_DIR};
		if( $OUTPUT{UNIX_CODE_LOCATION} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{UNIX_CODE_LOCATION}/ ) {
				$dir=~s/$OUTPUT{UNIX_CODE_LOCATION}//;
		} elsif(  $OUTPUT{NT_CODE_LOCATION} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{NT_CODE_LOCATION}/ ) {
				$dir=~s/$OUTPUT{NT_CODE_LOCATION}//;
		} elsif(  $OUTPUT{CODE_LOCATION_ALT1} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{CODE_LOCATION_ALT1}/ ) {
				$dir=~s/$OUTPUT{CODE_LOCATION_ALT1}//;
		} elsif(  $OUTPUT{CODE_LOCATION_ALT2} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{CODE_LOCATION_ALT2}/ ) {
				$dir=~s/$OUTPUT{CODE_LOCATION_ALT2}//;
		} else {
			$found="FALSE";
		}

		if( $found eq "TRUE" ) {
			$dir =~ s/^\///;
			if( -d "$OUTPUT{UNIX_CODE_LOCATION}/$dir" ) {
				# print "Setting to $dir in UNIX $OUTPUT{UNIX_CODE_LOCATION} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{UNIX_CODE_LOCATION}/$dir";
			} elsif( -d "$OUTPUT{NT_CODE_LOCATION}/$dir" ) {
				# print "Setting to $dir in NT $OUTPUT{NT_CODE_LOCATION} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{NT_CODE_LOCATION}/$dir";
			} elsif( -d "$OUTPUT{CODE_LOCATION_ALT1}/$dir" ) {
				# print "Setting to $dir in ALT1 $OUTPUT{CODE_LOCATION_ALT1} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{CODE_LOCATION_ALT1}/$dir";
			} elsif( -d "$OUTPUT{CODE_LOCATION_ALT2}/$dir" ) {
				# print "Setting to $dir in ALT2 $OUTPUT{CODE_LOCATION_ALT2} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{CODE_LOCATION_ALT2}/$dir";
			}
			print "CommonFunc.pm: BASE_BACKUP_DIR is now $OUTPUT{BASE_BACKUP_DIR}\n"
				if -d $OUTPUT{BASE_BACKUP_DIR} and defined $args{-debug};
		}

		unless( defined $OUTPUT{BASE_BACKUP_DIR} ){
			my $msg=$errstr."BASE_BACKUP_DIR MUST BE DEFINED\nBackup Software Has Not Been Initialized.";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}

		if( defined $OUTPUT{BASE_BACKUP_DIR} and ! -d $OUTPUT{BASE_BACKUP_DIR} ){
			my $msg=$errstr."BASE_BACKUP_DIR  $OUTPUT{BASE_BACKUP_DIR} IS NOT A DIRECTORY.\n\nThe BASE_BACKUP_DIR is the location of all the logs and output files\nfrom the backup scripts.\n";
			$msg .= "Diagnostics: FOUND=$found DIR=$dir\n";
			$msg .= "BASE_BACKUP_DIR IS NOT DEFINED.\nBackup Software Has Not Been Initialized.\n"
				if ! defined $OUTPUT{BASE_BACKUP_DIR} ;
			$msg .= "Alternate Location FROM UNIX_CODE_LOCATION = $OUTPUT{UNIX_CODE_LOCATION}\n"
				if $OUTPUT{UNIX_CODE_LOCATION};
			$msg .= "Alternate Location FROM NT_CODE_LOCATION =   $OUTPUT{NT_CODE_LOCATION}\n"
				if $OUTPUT{NT_CODE_LOCATION};
			$msg .= "Alternate Location FROM CODE_LOCATION_ALT1 = $OUTPUT{CODE_LOCATION_ALT1}\n"
				if $OUTPUT{CODE_LOCATION_ALT1};
			$msg .= "Alternate Location FROM CODE_LOCATION_ALT2 = $OUTPUT{CODE_LOCATION_ALT2}\n"
				if $OUTPUT{CODE_LOCATION_ALT2};

			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}
	}

	if( $OUTPUT{IS_REMOTE} eq "y" and defined $opt_J ) {
		unless ( defined $OUTPUT{SERVER_HOSTNAME} ) {
			my $msg=$errstr."Configuration Error: IS_REMOTE is set in $filename but SERVER_HOSTNAME is not.  Please seek help.  If the dump directory for server ".$OUTPUT{SERVER_NAME}." can not be viewed as a local filesystem (via nfs or windows networking), you must define the configuration value for SERVER_HOSTNAME.  ";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}

		unless ( defined $OUTPUT{SERVER_DIRECTORY} ) {
			my $msg=$errstr."Configuration Error: IS_REMOTE is set in $filename but SERVER_DIRECTORY is not.  Please seek help.  If the dump directory for server ".$OUTPUT{SERVER_NAME}." can not be viewed as a local filesystem (via nfs or windows networking), you must define the configuration value for SERVER_DIRECTORY.  ";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}


		if( ! defined $OUTPUT{SERVER_HOSTNAME} or $OUTPUT{SERVER_HOSTNAME} =~ /^\s*$/ ) {
			my $msg=$errstr."SERVER_HOSTNAME Variable Must Be Set For this Job (in configure.cfg)\n";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}
	}
	$OUTPUT{ERRORS}=\@errors if $#errors>=0;
	print "CommonFunc.pm: returning configuration info at ".scalar(localtime(time))."\n" if defined $args{-debug};
	return(%OUTPUT);
}

sub is_interactive {
	# returns 1/0 if you are interactive
	return -t STDIN && -t STDOUT;
}

# Check for Lock File and Dont Restart unless it exists
# Args: -threshtime , -filename
my($m_lockfile_text);
sub Am_I_Up
{
	my(%args)=@_;
	print "Am_I_Up(@_) called\n" if defined $args{-debug} or $LOCK_DIAG;
	$m_lockfile_text = $args{-lockfiletext} if $args{-lockfiletext};

	my($filename,$filetimesecs,$threshtime)=GetLockfile(%args);
	print "File=$filename Ping=$filetimesecs Thres=$threshtime\n" if $args{-debug} or $LOCK_DIAG;
	my($rc)=0;
	if( defined $filetimesecs ) {
		if( $filetimesecs > $threshtime ) {
			$rc=0;
		} else {
			$rc=1;
			print "Am_I_Up() returning $rc (not touching file)\n" if defined $args{-debug} or $LOCK_DIAG;
			return $rc;
		}
	} else {
		print "Am_I_Up() file does not exists\n" if defined $args{-debug} or $LOCK_DIAG;
	}
	$args{-filename}=$filename;
	I_Am_Up(%args);
	return $rc;
}

sub I_Am_Up
{
	my(%args)=@_;
	$m_lockfile_text = $args{-lockfiletext} if $args{-lockfiletext};
	print "I_Am_Up(@_) called\n" if defined $args{-debug} or $LOCK_DIAG;
	my($filename,$filetimesecs,$threshtime)=GetLockfile(%args);
	print "File=$filename Time=$filetimesecs Thres=$threshtime\n" if $args{-debug} or $LOCK_DIAG;
	my($rc)=0;
	if( defined $filetimesecs ) {
		if( $filetimesecs > $threshtime ) {
			$rc=0;
		} else {
			$rc=1;
		}
	}

	my($lockdir)=dirname($filename);
	if( ! -d $lockdir ) { # hmm wth? what happened to the directory... a network outage?
		sleep(2);
		if ( ! -d $lockdir ) {
			warn "CANT WRITE $filename - DIrectory $lockdir does not exist" ;
			return 0;
		}
	}
	print "I_Am_Up() creating $filename\n" if defined $args{-debug} or $LOCK_DIAG;
	open(OUT,">$filename") or die "Cant write $filename: $!";
	#print "LOCK FILE TEXT=$m_lockfile_text\n";
	#print "PRINTED\n" if  $m_lockfile_text;
	if( $m_lockfile_text ) {
		print OUT $m_lockfile_text."\n";
	} else {
		print OUT "Lock File Written\n";
	}
	close(OUT);
	return $rc;
}

sub I_Am_Down
{
	my(%args)=@_;
	$m_lockfile_text = $args{-lockfiletext} if $args{-lockfiletext};

	print "I_Am_Down(@_) called\n" if defined $args{-debug} or $LOCK_DIAG;
	my($filename,$filetimesecs,$threshtime)=GetLockfile(%args);

	#if( defined $args{-filename} ) {
	#	$filename = $args{-filename};
	#} elsif( defined $args{-dir} ) {
	#	$filename= $args{-dir}."/".hostname()."_".basename($0).".lck";
	#} else {
	#	my($root)=get_gem_root_dir();
#		if( -d "$root/data/lockfiles" ) {
	#		$filename=$root."/data/lockfiles/".hostname()."_".basename($0).".lck";
	#	} else {
	#		print "Am_I_Up():  o $root/data/lockfiles directory\n" if defined $args{-debug} or $LOCK_DIAG;
	#		$filename=hostname()."_".basename($0).".lck";
	#	}
	#}

	unlink $filename if -r $filename;
}

my($saved_lockfile_name);

sub GetLockfile {
	my(%args)=@_;
	my($filename,$threshtime);

	my($prefix);
	if( $saved_lockfile_name ) {
		$filename=$saved_lockfile_name;
	} else {
		if( defined $args{-prefix} ) {
			$prefix = $args{-prefix}."_";
		} else {
			$prefix="";
		};
		if( defined $args{-filename} ) {
			$filename = $args{-filename};
		} elsif( defined $args{-dir} ) {
			$filename= $args{-dir}."/".$prefix.hostname()."_".basename($0).".lck";
		} else {
			my($root)=get_gem_root_dir();
			if( -d "$root/data/lockfiles" ) {
				$filename=$root."/data/lockfiles/".$prefix.hostname()."_".basename($0).".lck";
			} else {
				$filename=$prefix.hostname()."_".basename($0).".lck";
			}
		}
		$saved_lockfile_name=$filename if $filename;
	}

	my($rc);
	if( -e $filename ) {
		die "Lock File Exists But Is UnWritable ($filename) host=".hostname()
			unless -w $filename;
		my($filetimesecs)= (-M $filename)*24*60*60;
		if( $args{-threshtime} ) {
			$threshtime = $args{-threshtime};
		} else {
			$threshtime=1200;	# twenty minutes
		}
		open(F,$filename) or die "Cant Read $filename\n";
		my($text)='';
		while(<F>) { chop; chomp; $text.=$_; }
		close(F);
		return $filename, $filetimesecs, $threshtime, $text;
	} else {
		return $filename,undef, undef, undef;
	}
}

my($saved_semaphore_filename);

# set/get an application semaphore - this could represent the last work actually processed
sub set_semaphore
{
	my(%args)=@_;
}

sub get_semaphore
{
	my(%args)=@_;

}

sub __debugmsg {
	my($msg)=join("",@_);
	if( defined $main::DEBUG ) {
		$msg=~s/\] /\]\t\t\<dbg\> /;
		__statusmsg($msg);
	}
}

sub __statusmsg {
    my $msg = join("",@_);
  	 print $msg;
}

1;

__END__

=head1 NAME

CommonFunc.pm - Common Function Library

=head2 DESCRIPTION

Standard/Miscellaneous functions

=head2 SUMMARY

 get_version() - returns version of the gem ($version,$sht_version)
 is_nt()       - return 1/0 if you are on NT/Windows
 cd_home()     - cd to the directory that contains the main code and return that directory
 today()       - returns date string yyyymmdd.hhmiss
 is_up()       - writes lock file useful to determine if program is allready running

