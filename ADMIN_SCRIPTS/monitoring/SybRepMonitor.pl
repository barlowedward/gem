#!/apps/perl/linux/perl-5.8.2/bin/perl
#
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use MlpAlarm;
use Sys::Hostname;
use CommonFunc;
use CommonHeader;
use DBIFunc;
use Repository;

my($SOURCE_TABLE)="gem_rep_status";
my $query_cnt = 1;
my $current_server;	# for prints
my(%done_sources);

use vars qw( $BATCHID $BUCKETSIZE $DEBUG $OUTFILE $ITERATIONS $NOLOCK $ALARM_MINS $HTML $ALARM_MAILTO $THRESHFILE
		$NOLOG $SERVER $REPORT_THRESH $FREQUENCY $REPORT_HOURS $ACTION $REBUILD $CRIT_MINS );

sub usage {
	die @_."\nUsage: $0 --ACTION=INSTALL|UNINSTALL|MONITOR|REPORT|CHECK|COMPARE --BATCHID=id
	[--SERVER=xxx] [--DEBUG] [--OUTFILE=file] [--NOLOCK]

	directives stored in replication.dat

	if --ACTION=INSTALL will install
		--REBUILD redoes table creates
	if --ACTION=CHECK will check latency vs --ALARM_MINS
		- mail to --ALARM_MAILTO as needed
	if --ACTION=REPORT will create a report
		- will print data for --REPORT_HOURS hours
		- will only show rows > --REPORT_THRESH seconds
		- will aggregate based on --BUCKETSIZE
		- will print HTML output if --HTML is passed
	if --ACTION=MONITOR will insert rows into all source databases
		- will insert --ITERATIONS rows
		- sleeps --FREQUENCY secs between each insert
	\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
  		"ACTION=s"			=> \$ACTION,

		"ITERATIONS=s"		=> \$ITERATIONS,
		"FREQUENCY=s"		=> \$FREQUENCY,
		"BATCHID=s"			=> \$BATCHID,
		"THRESHFILE=s"		=> \$THRESHFILE,

		"REBUILD"				=> \$REBUILD,
		"HTML"				=> \$HTML,
		"REPORT_THRESH=s"	=> \$REPORT_THRESH,
		"BUCKETSIZE=s"		=> \$BUCKETSIZE,
		"REPORT_HOURS=s"	=> \$REPORT_HOURS,

		"NOLOG=s"			=> \$NOLOG,
		"NOLOCK"			   => \$NOLOCK,
		"CRIT_MINS=s"		=> \$CRIT_MINS,

		#"ALARM_SECS=s"	=> \$ALARM_SECS,
		"ALARM_MINS=s"		=> \$ALARM_MINS,
		"ALARM_MAILTO=s"	=> \$ALARM_MAILTO,

		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE,
		"DEBUG"				=> \$DEBUG );

die usage() unless $ACTION;

$ITERATIONS=20 		unless $ITERATIONS;
usage( "ACTION=INSTALL|UNINSTALL|MONITOR|REPORT|CHECK|COMPARE" )
		unless $ACTION eq "INSTALL"
		or  $ACTION eq "UNINSTALL"
		or  $ACTION eq "COMPARE"
		or  $ACTION eq "MONITOR"
		or  $ACTION eq "CHECK"
		or  $ACTION eq "REPORT";

my($NL)="\n";
$NL="<br>\n" if $HTML;
my($OUTFILESTRING)="";		# Written to $OUTFILE

if( ! $NOLOCK ) {
	print "NOLOCK IS NOT DEFINED!\n" if $DEBUG;
	if( Am_I_Up(-debug=>$DEBUG,-prefix=>$BATCHID,-threshtime=>120) ) {
		my($fname,$secs,$thresh)=GetLockfile(-prefix=>$BATCHID,-threshtime=>120);
		print "Cant Start SybRepMonitor.pl - it is apparently allready running\nUse --NOLOCK to ignore this\nLock File=$fname secs=$secs thresh=$thresh secs\n";
		exit(0);
	}
} else {
	print "NOLOCK IS DEFINED!\n" if $DEBUG;
}

output($BATCHID." (SybRepMonitor.pl --ACTION=$ACTION) run at ".localtime(time)." on host ".hostname().$NL);

my($reproute_ptr,$repserver_ptr)=read_rep_configfile();
if( $DEBUG ) {
	use Data::Dumper;
	print "debug mode: Dumping rep configuration\n";
	print Dumper $reproute_ptr;
	print Dumper $repserver_ptr;
	I_Am_Down();
	die if $ACTION eq "TEST";
}

$|=1;


if( $ACTION eq "CHECK" ) {
	$BATCHID="SybRepMonCheck" unless $BATCHID;
	MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor');

	my($warnth,$alarmth,$critth);
	if( defined $THRESHFILE ) {
		($warnth,$alarmth,$critth)=read_threshfile($THRESHFILE,"SybRepMonCheck");
	}

	my($as)=$ALARM_MINS || 15;
	my($Cs)=$CRIT_MINS  || 30;

#	my($as)=$ALARM_SECS;
#	$as=0 unless $ALARM_SECS;
#	$as+=60*$ALARM_MINS if $ALARM_MINS;
#	$as=60 if $as == 0;
#	my($Cs)=$CRIT_SECS;
#	$Cs=0 unless $CRIT_SECS;
#	$Cs+=60*$CRIT_MINS if $CRIT_MINS;

	#my($asstr)="$ALARM_MINS min " if $ALARM_MINS;
	#$asstr .= "$ALARM_SECS secs" if $ALARM_SECS;

	# get the target server/db's
	my(%done_targets);
	#output( "DBG DBG : Sybase is ".$ENV{SYBASE}.$NL );

	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		next if defined $SERVER and $$hshptr{targetsvr} ne $SERVER;
		next if $done_targets{$$hshptr{targetsvr}."|".$$hshptr{targetdb}};
		$done_targets{$$hshptr{targetsvr}."|".$$hshptr{targetdb}} = 1;

		# connect to the server and do it!
		output( "Target $$hshptr{targetsvr} $$hshptr{targetdb}",$NL );
		my($svrptr)=$$repserver_ptr{$$hshptr{targetsvr}};
		flush_and_die( "Server $$hshptr{targetsvr} not defined!" ) unless $svrptr;

		if( ! dbi_connect( -srv=>$$hshptr{targetsvr}, -login=>$$svrptr{login}, -password=>$$svrptr{password} )) {
			warn "Cant connect to $$hshptr{targetsvr}\n";
			next;
   	}
   	$current_server=$$hshptr{targetsvr};

   	output( "*** Connected to $$hshptr{targetsvr} as $$svrptr{login} at ".localtime(time)."",$NL );
		my(@rc)=dbi_query(
			-db=>$$hshptr{targetdb},
			-query=>"select distinct datediff(mi,max(irs_source_date),getdate()),max(irs_source_date), irs_source_server from gem_rep_status" );

		#$dat[2]=~s/\s+$//;	# hmmm wonder why there was a trailing space...  well this fixes it...

		foreach ( @rc ) {
			my($MIN,$DATE,$SOURCE)=dbi_decode_row($_);
			$SOURCE=~s/\s+$//;
			output( "SOURCE=[$SOURCE] " );
			output( "DELAY=[".$MIN." MINUTES] " );
			output( "LAST SOURCE TIME=[".$DATE."]".$NL );

			if( defined $THRESHFILE) {
				$as=$ALARM_MINS || 15;
				$Cs=$CRIT_MINS  || 30;
				#use Data::Dumper;
				#print Dumper $alarmth,"\n====================\n";
				#print Dumper $critth,"\n====================\n";
				$as=$$alarmth{$SOURCE} 	if $$alarmth{$SOURCE};
				$Cs=$$critth{$SOURCE} 	if $$critth{$SOURCE};
				$as=$$alarmth{$SOURCE.":".$$hshptr{targetsvr}} 	if $$alarmth{$SOURCE.":".$$hshptr{targetsvr}};
				$Cs=$$critth{$SOURCE.":".$$hshptr{targetsvr}} 	if $$critth{$SOURCE.":".$$hshptr{targetsvr}};
				output( "\tTHRESHOLDs for ".$SOURCE.":".$$hshptr{targetsvr}." are ".$as."/".$Cs." Minutes",$NL);
			}

			my($state)="OK";
			$state="ERROR" if $MIN>$as;
			$state="CRITICAL" if $MIN>$Cs and $Cs>0;
			output( "\tREP STATE=$state (alarm thresh=$as Min crit thresh=$Cs Min)".$NL );

			MlpHeartbeat(
				-monitor_program => $BATCHID,
				-system => $$hshptr{targetsvr},
				-subsystem=>"Source: $SOURCE",
				-state => $state,
				-debug=>$DEBUG,
				-message_text=> "Replication Delay=$MIN Minutes from $SOURCE. SourceTime=$DATE. "
			 ) unless defined $NOLOG;
		}
		dbi_disconnect();
	}
	MlpBatchJobEnd();
	output( "*** Completed Replication Check",$NL );

} elsif( $ACTION eq "MONITOR" ) {
	output("*** Started Replication Monitoring",$NL );

	$BATCHID="SybRepMonAgent" unless $BATCHID;
	MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor');

	# get the source server/db's

	$ITERATIONS=1 unless $ITERATIONS;
	$FREQUENCY=10 if $ITERATIONS>1 and ! $FREQUENCY;

	my(@unique_sources);
	foreach ( keys %$reproute_ptr ) 	{
			my($hshptr)=$$reproute_ptr{$_};
			next if $done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}};
			$done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}} = 1;
			push @unique_sources,$$hshptr{sourcesvr}."|".$$hshptr{sourcedb};
			output( "Unique Source $$hshptr{sourcesvr} ",$NL );
	}

	for my $cnt (1..$ITERATIONS) {
		foreach ( @unique_sources ) 	{
			my($s,$d)=split(/\|/,$_);

			# connect to the server and do it!
			#print "Source $$hshptr{sourcesvr} $$hshptr{sourcedb}",$NL;
			#print "Source $s Db $d",$NL;
			my($svrptr)=$$repserver_ptr{$s};
			flush_and_die( "Server $s not defined!" ) unless $svrptr;
			#print "Connecting as $$svrptr{login} / $$svrptr{password}",$NL;

			if( ! dbi_connect(-srv=>$s,-login=>$$svrptr{login},-password=>$$svrptr{password} ) ) {
				warn "Cant connect to $s\n";
				next;
	  		}
	  		output( "*** Connected to $s db $d as $$svrptr{login}".$NL );
			$current_server=$s;

			my($q) = "insert gem_rep_status select getdate(),getdate(),'".$s."'\n";
			pr_query($d,$q);
			my(@rc)=dbi_query(
				-db=>$d,
				-query=>$q,
				-no_results=>1
			);
			dbi_disconnect();
		}
		output( "Completed Iteration $cnt/$ITERATIONS) Sleeping $FREQUENCY".$NL) if $FREQUENCY;
		sleep($FREQUENCY) if $FREQUENCY;
		I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;
	}

	MlpBatchJobEnd();
	output( "*** Completed Monitoring".$NL );

} elsif( $ACTION eq "REPORT" ) {
	$REPORT_HOURS=48 unless $REPORT_HOURS;

	$BATCHID="SybRepMonReport" unless $BATCHID;
	MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor');

	# get the target server/db's
	my(%done_targets);
	my(%timestamps);
	my(%peak_times, %avg_times, %int_peak_times);

	output("Report Only Includes Delays Over $REPORT_THRESH Seconds".$NL)
		if $REPORT_THRESH;
	output("Report Only Includes Data Collected within the past $REPORT_HOURS Hours".$NL)
		if $REPORT_HOURS;

	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		next if defined $SERVER and $$hshptr{targetsvr} ne $SERVER;
		next if $done_targets{$$hshptr{targetsvr}."|".$$hshptr{targetdb}};
		$done_targets{$$hshptr{targetsvr}."|".$$hshptr{targetdb}} = 1;

		# connect to the server and do it!
		my($outstr)="Target $$hshptr{targetsvr} $$hshptr{targetdb} ";
		my($svrptr)=$$repserver_ptr{$$hshptr{targetsvr}};
		flush_and_die( "Server $$hshptr{targetsvr} not defined!" ) unless $svrptr;
		if( ! dbi_connect(-srv=>$$hshptr{targetsvr},-login=>$$svrptr{login},-password=>$$svrptr{password} ) ) {
			output($outstr."SKIPPING - CANT CONNECT".$NL);
			warn "Cant connect to $$hshptr{targetsvr}\n";
			next;
   	}
	   $outstr.="[ Connected as $$svrptr{login} ] " if $DEBUG;
	   $current_server=$$hshptr{targetsvr};

		# DBG DBG - WHOLE SECTION
		#my(@rc)=dbi_query(-db=>"master",-query=>"exec sp__id" );
		#output( "DBG DBG:  " );
		#foreach ( @rc ) {
		#	my(@dat)=dbi_decode_row($_);
		#	output( join("\t",@dat) );
		#}
		#output( $NL );
		# DBG DBG - WHOLE SECTION

		my(@rc)=dbi_query(
			-db=>$$hshptr{targetdb},
			-query=>"select max(irs_source_date) from gem_rep_status" );
		$outstr.= "Most Recent Data Date [ ";
		foreach ( @rc ) {
			my(@dat)=dbi_decode_row($_);
			$outstr.= join("\t",@dat);
		}
		output( $outstr." ]".$NL );
		$REPORT_THRESH=0 unless $REPORT_THRESH;
		$BUCKETSIZE=5 unless $BUCKETSIZE;

		my($q)='
CREATE TABLE  #gem_rep_status
(
 day              char(8) NULL,
 mhour            int NULL,	-- bucket
 mminute          int NULL, -- bucket
 peak_secs		  int NULL,
 latency_secs     int NULL
)

insert 	#gem_rep_status (day,mhour,mminute,peak_secs,latency_secs)
select  convert(char(8),irs_source_date,112),
	datepart(hh,irs_source_date),
	datepart(mi,irs_source_date)/'.$BUCKETSIZE.',
	datediff(ss,irs_source_date,irs_destination_date),
	datediff(ss,irs_source_date,irs_destination_date)
from 	'.$SOURCE_TABLE.'
where 	datediff(hh,irs_source_date,getdate())<='.$REPORT_HOURS.'

select 	day, mhour, mminute=mminute*'.$BUCKETSIZE.',
	average_latency=avg(latency_secs),
	average_peak=max(latency_secs)	,
	latency_timestr=convert(varchar(20),""),
	peak_timestr=convert(varchar(20),"")
into 	#tmp
from 	#gem_rep_status
group by day, mhour, mminute

update 	#tmp
set 	latency_timestr=convert(varchar,round((average_latency)/60,0))+" Minutes"
where 	average_latency>300

update 	#tmp
set 	latency_timestr=latency_timestr+" "+
		convert(varchar,average_latency-60*round((average_latency)/60,0))+
		" Seconds"
--where latency_timestr>300

update 	#tmp
set 	peak_timestr=convert(varchar,round((average_peak)/60,0))+" Minutes"
where 	average_latency>300

update 	#tmp
set 	peak_timestr=peak_timestr+" "+
		convert(varchar,average_peak-60*round((average_peak)/60,0))+
		" Seconds"

select 	TStamp= day + " "+convert(char(2),mhour)+":"+convert(char(2),mminute),
		Peak_String 	= ltrim(peak_timestr),
		--Average_Seconds	= average_latency,
		Average_String  = ltrim(latency_timestr),
		Peak_Seconds	= average_peak
from 	#tmp
where   average_peak>'.$REPORT_THRESH.'
order by day,mhour,mminute

drop table #gem_rep_status
drop table #tmp
';

		pr_query($$hshptr{targetdb},$q) if $DEBUG;
		my(@rc)=dbi_query(
			#-print_hdr=>1,
			-db=>$$hshptr{targetdb},
			-query=>$q );

		if( $#rc<0 ) {
			output( "<TABLE BORDER=1>\n");
			output( "<TR><TD BGCOLOR=RED>NO TIMING INFO FOR $$hshptr{targetsvr} $$hshptr{targetdb} FOUND FOR TODAY!</TD></TR>\n" );
			output( "</TABLE>");
		} else {
			foreach ( @rc ) {
				my($TSTAMP,$PK,$AVG,$PK_INT)=dbi_decode_row($_);

				$TSTAMP=~s/:(\d)$/:0$1/;
				$TSTAMP=~s/(\d)\s:/0$1:/;
				$timestamps{$TSTAMP}=1;
				$peak_times{$$hshptr{targetsvr}."|".$$hshptr{targetdb}."|".$TSTAMP}=$PK;
				$avg_times{$$hshptr{targetsvr}."|".$$hshptr{targetdb}."|".$TSTAMP}=$AVG;
				$int_peak_times{$$hshptr{targetsvr}."|".$$hshptr{targetdb}."|".$TSTAMP}=$PK_INT;
			}
		}
		dbi_disconnect();
	}

	# print the header row
	my($header_row)="";
	if( $HTML ) {
		output( "<TABLE BORDER=1>\n" );
		$header_row="<TR BGCOLOR=BEIGE><TH>TIME</TH>";
	} else {
		output("TIME\t");
	}
	foreach ( sort keys %done_targets ) 	{
		my($s,$d)=split(/\|/,$_);
		if( $HTML ) {
			$header_row.=( "<TH COLSPAN=2>$s / $d PEAK/AVG SECS</TH>");
		} else {
			output( "$s / $d PEAK\t");
			output( "$s / $d AVG\t");
		}
	}
	$header_row.="</TR>" if $HTML;

	my($tstamp_8char)="";
	foreach my $ts ( reverse sort keys %timestamps ) {
		if( $tstamp_8char ne substr($ts,0,8) ) {
			output($header_row);
			$tstamp_8char = substr($ts,0,8);
		}
		output("<TR><TD>$ts</TD>\n");
		foreach ( sort keys %done_targets ) 	{
			my($a)=$peak_times{$_."|".$ts} || "&nbsp;";
			my($b)=$avg_times{$_."|".$ts}  || "&nbsp;";
			my($bgcolor)="";
			$bgcolor="BGCOLOR=PINK" if $int_peak_times{$_."|".$ts}>600;
			output("\t<TD $bgcolor>".$a."</TD>"."<TD $bgcolor>".$b."</TD>\n");
		}
		output("</TR>\n");
	}

#			if( $HTML ) {
#				output( "<TR BGCOLOR=BEIGE><TH>SERVER</TH><TH>DB</TH><TH>",
#					join("</TH>\n\t<TH>","TIME","Peak(secs)","Average(secs)"),"</TH></TR>\n" );
#			} else {
#				output( join("\t","TIME","Peak(secs)","Average(secs)"),"\n" );
#			}
#			if( defined $HTML ) {
#					my($bgcolor)="BGCOLOR=PINK"  if $dat[1]>600 or $dat[3]>600;
#					$bgcolor="BGCOLOR=RED" if $dat[1]>6000 or $dat[3]>6000;
#					output( "<TR $bgcolor><TD>$$hshptr{targetsvr}</TD>
#						<TD>$$hshptr{targetdb}</TD>
#						<TD>",join("</TD>\n\t<TD>",@dat),"</TD></TR>\n" );
#				} else {
#					output( join("\t",@dat),"\n" );
#				}

	output("</TABLE>\n") if $HTML;
	MlpBatchJobEnd();

} elsif( $ACTION eq "INSTALL" ) {
	output( "*** Starting the installation".$NL );
	dbi_set_mode("INLINE");

	#
	# SET UP THE TABLES
	#
	my(%done_sources);
	my($cnt)=0;
	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		next if $done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}};
		$cnt++;
		$done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}} = $cnt;
		output( "*** Creating Tables On $$hshptr{sourcesvr} $$hshptr{sourcedb} ".$NL );
		my($svrptr)=$$repserver_ptr{$$hshptr{sourcesvr}};
		flush_and_die( "Server $$hshptr{sourcesvr} not defined!" ) unless $svrptr;
		if( ! dbi_connect(-srv=>$$hshptr{sourcesvr},-login=>$$svrptr{login},-password=>$$svrptr{password} ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{sourcesvr}".$NL );
   	}
   	output( "*** Connected to $$hshptr{sourcesvr} as $$svrptr{login}".$NL );

		$current_server=$$hshptr{sourcesvr};
		create_tables($$hshptr{sourcesvr},$$hshptr{sourcedb});
		dbi_disconnect();

		# SET UP THE REP DEF
		output( "*** Connecting to $$hshptr{repserver}".$NL );
		my($repptr)=$$repserver_ptr{$$hshptr{repserver}};

		if( ! dbi_connect(-srv=>$$hshptr{repserver},-login=>$$repptr{login},-password=>$$repptr{password},-debug=>$DEBUG ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{repserver} as $$repptr{login} $$repptr{password}".$NL );
   		}
   	output( "*** Connected to $$hshptr{repserver} as $$repptr{login}".$NL );
		$current_server=$$hshptr{repserver};

		my($q)='CREATE REPLICATION DEFINITION
	           "gem_rep_status_m0'.$cnt.'"
	WITH PRIMARY AT
	           XXXXX
	WITH ALL TABLES NAMED
	           "gem_rep_status"
	           (
	              "irs_source_date"             datetime,
	              "irs_destination_date"        datetime,
	              "irs_source_server"           char(30)
	           )
	PRIMARY KEY  (   "irs_source_date"            )
	REPLICATE ALL COLUMNS';

		$q=~s/XXXXX/$$hshptr{sourcesvr}\.$$hshptr{sourcedb}/;
		pr_query("",$q);
		foreach ( dbi_query( -db=>"", -query=>$q ) ) {
			my(@dat)=dbi_decode_row($_);
			output( ">>>",join("\t",@dat),$NL );
		}

$q='ALTER FUNCTION STRING
           gem_rep_status_m0'.$cnt.'.rs_insert
      FOR  rs_sqlserver_function_class
OUTPUT LANGUAGE
           \'   INSERT  gem_rep_status
                       (
                          irs_source_date,
                          irs_destination_date,
                          irs_source_server
                       )
               VALUES  (
                          ?irs_source_date!new?,
                          GETDATE(),
                          ?irs_source_server!new?
                       )\'';
         pr_query("",$q);
         foreach ( dbi_query( -db=>"", -query=>$q ) ) {
				my(@dat)=dbi_decode_row($_);
				output( ">>>",join("\t",@dat),$NL );
			}

$q='ALTER FUNCTION STRING
           gem_rep_status_m0'.$cnt.'.rs_update
      FOR  rs_sqlserver_function_class
OUTPUT LANGUAGE
           \'   UPDATE  gem_rep_status
                  SET  irs_source_date       = ?irs_source_date!new?,
                       irs_destination_date  = GETDATE(),
                       irs_source_server     = ?irs_source_server!new?
                WHERE  irs_source_date = ?irs_source_date!old?\'';
               pr_query("",$q);
               foreach ( dbi_query( -db=>"", -query=>$q ) ) {
			my(@dat)=dbi_decode_row($_);
			output( ">>>",join("\t",@dat),$NL);
		}

		dbi_disconnect();

		if( ! dbi_connect(-srv=>$$hshptr{sourcesvr},-login=>$$svrptr{login},-password=>$$svrptr{password} ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{sourcesvr}".$NL );
   	}
   	$current_server=$$hshptr{sourcesvr};

   	output( "*** Connected to $$hshptr{sourcesvr} as $$svrptr{login}".$NL );
		$q='EXECUTE sp_setreptable "gem_rep_status", TRUE';
		pr_query($$hshptr{sourcedb},$q);
	   foreach ( dbi_query( -db=>$$hshptr{sourcedb}, -query=>$q ) ) {
			my(@dat)=dbi_decode_row($_);
			output( ">>>",join("\t",@dat),$NL );
		}

		dbi_disconnect();
	}
	output( "\nSleeping for 60 seconds to let subscriptions be created... ".$NL );
	sleep(60);
	I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;
	output( "Sleep completed".$NL );


	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		next if $done_sources{$$hshptr{targetsvr}."|".$$hshptr{targetdb}};
		$done_sources{$$hshptr{targetsvr}."|".$$hshptr{targetdb}} = 0;
		output( "Source $$hshptr{targetsvr} $$hshptr{targetdb} ".$NL );
		my($svrptr)=$$repserver_ptr{$$hshptr{targetsvr}};
		flush_and_die( "Server $$hshptr{targetsvr} not defined!" ) unless $svrptr;
		if( ! dbi_connect(-srv=>$$hshptr{targetsvr},-login=>$$svrptr{login},-password=>$$svrptr{password} ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{targetsvr}".$NL );
   	}
   	$current_server=$$hshptr{targetsvr};

   	output( "*** Connected to $$hshptr{targetsvr} as $$svrptr{login}".$NL );
		create_tables($$hshptr{targetsvr},$$hshptr{targetdb});
		dbi_disconnect();
	}

	# now lets subscribe to each of the sources
	my($a)=1;
	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};

		# ok subscribe
my($q)='CREATE SUBSCRIPTION
           gem_rep_status_m'.$a.'cms
      FOR  AAAAA
WITH REPLICATE AT
           XXXXX
WITHOUT MATERIALIZATION
SUBSCRIBE TO TRUNCATE TABLE';

		$q=~s/XXXXX/$$hshptr{targetsvr}\.$$hshptr{targetdb}/;
		my($zz)="gem_rep_status_m0".$done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}};
		$q=~s/AAAAA/$zz/;

		my($repptr)=$$repserver_ptr{$$hshptr{repserver}};
		if( ! dbi_connect(-srv=>$$hshptr{repserver},-login=>$$repptr{login},-password=>$$repptr{password},-debug=>$DEBUG ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{repserver} as $$repptr{login} $$repptr{password}".$NL );
   	}
   	$current_server=$$hshptr{repserver};

   	output( "*** Connected to $$hshptr{repserver} as $$repptr{login}".$NL );
   	pr_query("",$q);

		foreach ( dbi_query( -db=>"", -query=>$q ) ) {
			my(@dat)=dbi_decode_row($_);
			output( ">>>",join("\t",@dat),$NL );
		}
		dbi_disconnect();
		$a++;
	}



#
#CREATE SUBSCRIPTION
#           gem_rep_status_mDRcms
#      FOR  gem_rep_status_m01cmd
#WITH REPLICATE AT
#           IMAGSYB1DR.clientmlp
#WITHOUT MATERIALIZATION
#SUBSCRIBE TO TRUNCATE TABLE

} elsif( $ACTION eq "UNINSTALL" ) {
	output( "*** Starting un - installation".$NL );
	dbi_set_mode("INLINE");
	my($cnt)=0;
	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		next if $done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}};
		$cnt++;
		$done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}} = $cnt;
	}

	my($a)=1;
	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};

		# ok subscribe
my($q)='DROP SUBSCRIPTION
           gem_rep_status_m'.$a.'cms
      FOR  AAAAA
WITH REPLICATE AT
           XXXXX
WITHOUT PURGE';

		$q=~s/XXXXX/$$hshptr{targetsvr}\.$$hshptr{targetdb}/;
		my($zz)="gem_rep_status_m0".$done_sources{$$hshptr{sourcesvr}."|".$$hshptr{sourcedb}};
		$q=~s/AAAAA/$zz/;

		my($repptr)=$$repserver_ptr{$$hshptr{repserver}};
		if( ! dbi_connect(-srv=>$$hshptr{repserver},-login=>$$repptr{login},-password=>$$repptr{password},-debug=>$DEBUG ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{repserver} as $$repptr{login} $$repptr{password}".$NL );
   		}
   	$current_server=$$hshptr{repserver};

   	output( "*** Connected to $$hshptr{repserver} as $$repptr{login}".$NL );
   	pr_query("",$q);

		foreach ( dbi_query( -db=>"", -query=>$q ) ) {
			my(@dat)=dbi_decode_row($_);
			output( ">>>",join("\t",@dat),$NL );
		}
		dbi_disconnect();
		$a++;
	}
	output( "Sleeping for 60 seconds".$NL );
	sleep(60);
	output( "Sleep completed".$NL );
	I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;

	my($cnt)=1;
	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		my($repptr)=$$repserver_ptr{$$hshptr{repserver}};
		if( ! dbi_connect(-srv=>$$hshptr{repserver},-login=>$$repptr{login},-password=>$$repptr{password},-debug=>$DEBUG ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{repserver} as $$repptr{login} $$repptr{password}".$NL );
   	}
   	$current_server=$$hshptr{repserver};

   	for $cnt (0..10) {
			my($q)='DROP REPLICATION DEFINITION gem_rep_status_m0'.$cnt;
			output( "*** Connected to $$hshptr{repserver} as $$repptr{login}".$NL );
   		pr_query("",$q);
			foreach ( dbi_query( -db=>"", -query=>$q ) ) {
				my(@dat)=dbi_decode_row($_);
				next if $dat[0]=~/15020/ and $dat[0]=~/doesn't exist/;
				output( ">>>",join("\t",@dat),$NL );
			}
		}
		dbi_disconnect();
	}

	my(%done);
	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		my($svrptr)=$$repserver_ptr{$$hshptr{sourcesvr}};
		next if $done{$$hshptr{sourcesvr}};
		if( ! dbi_connect(-srv=>$$hshptr{sourcesvr},-login=>$$svrptr{login},-password=>$$svrptr{password} ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{sourcesvr}".$NL );
   		}
   	$current_server=$$hshptr{sourcesvr};

   	output( "*** Connected to $$hshptr{sourcesvr} as $$svrptr{login}".$NL );
   	my($q)="if exists ( select 1 from sysobjects where name='gem_rep_status' )  drop table gem_rep_status";
   	pr_query("",$q);
		foreach ( dbi_query( -db=>"", -query=>$q ) ) {
			my(@dat)=dbi_decode_row($_);
			output( ">>>",join("\t",@dat),$NL );
		}
		$done{$$hshptr{sourcesvr}}=1;
		dbi_disconnect();
	}
	foreach ( keys %$reproute_ptr ) 	{
		my($hshptr)=$$reproute_ptr{$_};
		my($svrptr)=$$repserver_ptr{$$hshptr{targetsvr}};
		next if $done{$$hshptr{targetsvr}};
		if( ! dbi_connect(-srv=>$$hshptr{targetsvr},-login=>$$svrptr{login},-password=>$$svrptr{password} ) ) {
			flush_and_die( "Fatal Error: Cant connect to $$hshptr{targetsvr}".$NL );
   	}
   	$current_server=$$hshptr{targetsvr};

		output( "*** Connected to $$hshptr{targetsvr} as $$svrptr{login}".$NL );
		my($q)="if exists ( select 1 from sysobjects where name='gem_rep_status' )  drop table gem_rep_status";
		pr_query("",$q);
		foreach ( dbi_query( -db=>"", -query=>$q ) ) {
			my(@dat)=dbi_decode_row($_);
			output( ">>>",join("\t",@dat),$NL );
		}
		$done{$$hshptr{targetsvr}}=1;
		dbi_disconnect();
	}
} elsif( $ACTION eq "COMPARE" ) {
		my($rdir)=get_gem_root_dir();
		foreach ( keys %$reproute_ptr ) 	{
			my($hshptr)=$$reproute_ptr{$_};

			my($svrptr)=$$repserver_ptr{$$hshptr{targetsvr}};
			flush_and_die( "Server $$hshptr{targetsvr} not defined!" ) unless $svrptr;

			my($tgtptr)=$$repserver_ptr{$$hshptr{sourcesvr}};
			flush_and_die( "Server $$hshptr{sourcesvr} not defined!" ) unless $tgtptr;

			print "SOURCE SERVER $$hshptr{sourcesvr} : login=$$svrptr{login} password=$$svrptr{password}",$NL;
			print "TARGET SERVER $$hshptr{targetsvr} : login=$$tgtptr{login} password=$$tgtptr{password}",$NL;

			output("COMPARING DDL FROM $$hshptr{sourcesvr} to $$hshptr{targetsvr}",$NL);
			output("COMPARING DATA FROM $$hshptr{sourcesvr} to $$hshptr{targetsvr}",$NL);

			output("CREATING $rdir/data/html_output/Data_".$$hshptr{sourcesvr}."_".$$hshptr{targetsvr}.".html");
#			my($cmd)="$^X $rdir/ADMIN_SCRIPTS/bin/datacompare.pl -B$BATCHID -U$$svrptr{login} -S$$hshptr{sourcesvr} -P$$svrptr{password} -u$$tgtptr{login} -s$$hshptr{targetsvr} -p$$tgtptr{password} -h > $rdir/data/html_output/Data_".$$hshptr{sourcesvr}."_".$$hshptr{targetsvr}.".html 2>&1";
			my($cmd)="$^X $rdir/ADMIN_SCRIPTS/bin/datacompare.pl -SOURCELOGIN=$$svrptr{login} -SOURCESERVER=$$hshptr{sourcesvr} -SOURCEPASS=$$svrptr{password} -TARGETSERVER=$$hshptr{targetsvr} --TARGETLOGIN=$$tgtptr{login} --TARGETPASS=$$tgtptr{password} -HTML --GEMREPORT --BATCHID=$BATCHID 2>&1";

			output("Executing $cmd",$NL);
			system($cmd);

			output("CREATING $rdir/data/html_output/DDL_".$$hshptr{sourcesvr}."_".$$hshptr{targetsvr}.".html");
			$cmd="$^X $rdir/ADMIN_SCRIPTS/bin/dbcompare.pl -B$BATCHID -U$$svrptr{login} -S$$hshptr{sourcesvr} -P$$svrptr{password} -u$$tgtptr{login} -s$$hshptr{targetsvr} -p$$tgtptr{password} -h > $rdir/data/html_output/DDL_".$$hshptr{sourcesvr}."_".$$hshptr{targetsvr}.".html 2>&1";
			output("Executing $cmd",$NL);
			system($cmd);
		}
} else {
	flush_and_die( "ACTION=$ACTION UNIMPLEMENTED" );
}

output("SybRepMonitor.pl Completed at ".localtime(time)." on host ".hostname().$NL);
flush_outfile();
I_Am_Down();

exit(0);
############################################
sub flush_and_die {
	output(@_);
	output("ABNORMAL PROGRAM TERMINATION");
	flush_outfile();
	I_Am_Down();
}

sub flush_outfile {
	if( $OUTFILE ) {
		print "WRITING TO OUTPUT FILE $OUTFILE",$NL;
		open(OUT,">$OUTFILE") or die "Cant write $OUTFILE in source directory\n";
		print OUT $OUTFILESTRING;
		close(OUT);
	}
}
sub output {
	print @_;
	$OUTFILESTRING.=join("",@_);
	# print OUT @_ if $OUTFILE;
}

# this func is in more than one configfile - should be moved to a librar
sub read_rep_configfile {
	my($dir)=get_conf_dir();
	die "replication.dat does not exist in $dir".$NL unless -r "$dir/replication.dat";
	my(%routes, %servers);
	open( CONF,"$dir/replication.dat" ) or die "cant read $dir/replication.dat $!".$NL;
	while(<CONF>) {
		next if /^\s*#/ or /^\s*$/;
		chomp;chomp;
		my(@dat)=split(/\s+/,$_);
		die "Column 1 of replication.dat must be REPSERVER or REPROUTE"
			unless $dat[0] eq "REPSERVER" or $dat[0] eq "REPROUTE";
		if( $dat[0] eq "REPSERVER" ) {
			my(%hash);
			$hash{login}=$dat[2];
			$hash{password}=$dat[3];
			$hash{threshold}=$dat[4];
			$hash{type}="repserver";
			$servers{$dat[1]} = \%hash;
			print "Replication RepServer ".$dat[1].$NL;	#DBG DBG if $DEBUG;
		} elsif( $dat[0] eq "REPROUTE" ) {
			my(%hash);
			$hash{sourcesvr}=$dat[1];
			$hash{sourcedb} =$dat[2];
			$hash{targetsvr}=$dat[3];
			$hash{targetdb} =$dat[4];
			$hash{repserver}=$dat[5];
			$routes{$dat[1].".".$dat[2].".".$dat[3].".".$dat[4]} = \%hash;
			print "Replication Route ".$dat[1].".".$dat[2].".".$dat[3].".".$dat[4].$NL;	#DBG DBG if $DEBUG;

			if( ! defined $servers{$dat[1]} ) {
				# grab source password
				output( "Fetching Source Password Info For $dat[1] $NL") if $DEBUG;
				my($l,$p)=get_password(-type=>"sybase", -name=>$dat[1] );
				die "No password found for $dat[1]".$NL unless $l and $p;
				my(%x);
				($x{login},$x{password})=($l,$p);
				$x{type}="dataserver";
				$servers{$dat[1]}=\%x;
			}

			if( ! defined $servers{$dat[3]} ) {
				# grab source password
				output( "Fetching Dest Password Info For $dat[3] $NL" ) if $DEBUG;
				my(%x);
				my($l,$p)=get_password(-type=>"sybase", -name=>$dat[3] );
				die "No password found for $dat[3]".$NL unless $l and $p;
				($x{login},$x{password})=($l,$p);
				$x{type}="dataserver";
				$servers{$dat[3]}=\%x;
			}
		}
	}
	close(CONF);
	return( \%routes, \%servers);
}

# create table for replication monitoring
my(%done_tables);
sub create_tables {
	my($svr,$db)=@_;
	next if $done_tables{$svr.$db};
	$done_tables{$svr.$db} = 1;
   print  "*** Creating table gem_rep_status on Server=$svr Db=$db".$NL;

   my($does_exist)='SELECT  "EXISTS"
         FROM  sysobjects
        WHERE  name           = "gem_rep_status"
          AND  type           = "U"
          AND  USER_NAME(uid) = "dbo"';

   pr_query( $db, $does_exist );
   my($found_table)="FALSE";
	foreach ( dbi_query( -db=>$db, -query=>$does_exist ) ) {
		my(@c)=dbi_decode_row($_);
		if( $c[0] ne "EXISTS" ) {
			output("WARNING QUERY RETURNED $c[0]".$NL );
			next;
		}
		output("FOUND EXISTING TABLE gem_rep_status".$NL);
		$found_table="TRUE";
	}

	my(@q);
	my($cnt)=0;
	$q[$cnt++]='IF  EXISTS (   SELECT  1
            FROM  sysobjects
           WHERE  name           = "gem_rep_status"
             AND  type           = "U"
             AND  USER_NAME(uid) = "dbo")
           BEGIN
                DROP TABLE gem_rep_status
           END' if $found_table eq "TRUE" and $REBUILD;

	$q[$cnt++]='CREATE TABLE  gem_rep_status
(
   irs_source_date              datetime    NOT NULL,
   irs_destination_date         datetime    NOT NULL,
   irs_source_server            char(30)    NOT NULL
)' if $REBUILD or $found_table eq "FALSE";

	$q[$cnt++]='CREATE UNIQUE CLUSTERED INDEX
           irs_source_date_ind
       ON  gem_rep_status ( irs_source_date )' if $REBUILD or $found_table eq "FALSE";

	$q[$cnt++]='GRANT SELECT,INSERT,UPDATE,DELETE ON gem_rep_status TO public';

	foreach my $q (@q) {
		next unless $q;
		pr_query( $db, $q );
		dbi_query(		-db=>$db,		-query=>$q,		-no_results=>1		);
	}
	@q=();

	$q[$cnt++] = "drop procedure sp__repstat_$db";
	$q[$cnt++] = "create procedure sp__repstat_$db as

select delay=datediff(mi,$db..gem_rep_status.irs_source_date,$db..gem_rep_status.irs_destination_date),
	srctm=$db..gem_rep_status.irs_source_date,
	desttm=$db..gem_rep_status.irs_destination_date,
	srcsvr=$db..gem_rep_status.irs_source_server
	from $db..gem_rep_status
	where datediff(hh,irs_source_date,getdate())<=1
	order by irs_source_date desc
";
	$q[$cnt++] = "grant execute on sp__repstat_$db to public";

	foreach my $q (@q) {
		next unless $q;
		pr_query( "sybsystemprocs",$q );
		dbi_query(		-db=>"sybsystemprocs",		-query=>$q,		-no_results=>1		);
	}

	print  "*** Table, Index, Permissions Completed on Server=$svr Db=$db".$NL;
}

sub pr_query {
	my($db)=shift;
	my($a)=join("\n",@_)."\n";
	my(@v)=split(/\n/,$a);
	output $NL;
	output "[ Query #".$query_cnt." ] /*** Server=".$current_server." DB=".$db." ***/".$NL;
	foreach (@v) {
		next if /^\s*$/;
		output "[ Query #".$query_cnt." ] ".$_.$NL;
	}
	output $NL;

	$query_cnt++;
}


__END__

=head1 NAME

SybRepMonitor.pl - Complete REplication Monitoring SYstem

=head2 DESCRIPTION

The sybase replication monitor is a stand alone system that permits replication
monitoring.  Data is inserted into your replication sources (one row per minute)
and is replicated to your replication destinations.  That latency is timed and
stored for later usage.

=head2 SYNTAX

Usage: SybRepMonitor.pl --ACTION=INSTALL|UNINSTALL|MONITOR|REPORT|CHECK
	[--SERVER=xxx] [--DEBUG] [--OUTFILE=file]

	if --ACTION=INSTALL, it will install the monitoring
		--REBUILD - rebuild tables (loses data). By default table is preserved.
		            this option is primarily used if there is a table ddl change.
		            if the table does *not* exist, it will always be built

	if --ACTION=CHECK will check latency vs --ALARM_MINS
		- mail to --ALARM_MAILTO as needed

	if --ACTION=REPORT will create a report
		- will print data for --REPORT_HOURS hours
		- will only show rows > --REPORT_THRESH seconds
		- will aggregate based on --BUCKETSIZE
		- will print HTML output if --HTML is passed

	if --ACTION=MONITOR will insert rows into all source databases
		- will insert --ITERATIONS rows
		- sleeps --FREQUENCY secs between each insert

=head2 DETAILS

A table called gem_rep_status is created in each of your replication db's - both the
source and target.  A procedure is created in sybsystemprocs named sp__repstatus_$dbname.
This procedure will tell you the replication status of the dbname in question.  When you
run with --ACTION=MONITOR it will insert rows into primary db's.  The --ACTION=REPORT function
creates a report on the latencies of the rows you inserted with --ACTION=MONITOR.

