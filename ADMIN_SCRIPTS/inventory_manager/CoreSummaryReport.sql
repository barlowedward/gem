create table #TMP (
  system_name   varchar(255),
  contact		varchar(255) null,
  cpucount 		int null
)

insert #TMP select distinct system_name,null,null from INV_system where system_type='unixinv'

update #TMP set contact = attribute_value from #TMP t, INV_system_attribute a
where t.system_name=a.system_name and a.system_type='unixinv' and a.attribute_name='contact'

update #TMP set cpucount = convert(int,attribute_value) from #TMP t, INV_system_attribute a
where t.system_name=a.system_name and a.system_type='unixinv' and a.attribute_name='cpucount'

select top 1000 * from #TMP

select contact, count(*), sum(cpucount)
from #TMP
group by contact
order by contact

