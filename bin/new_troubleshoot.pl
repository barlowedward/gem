use strict;
use Tk;
use Tk::DialogBox;
use Tk::ROText;
use Tk::LabFrame;
use Repository;
use Tk::JComboBox;
use RunCommand;
use Sys::Hostname;
use Carp;
use vars qw/$top/;

# Copyright (c) 2001-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

$|=1;
# GLOBAL VARIALBES
my(%var);
my($login,$password,$server);
my(@perl_includes);
my($lineno);
my($save_print_string);
my($cur_tag)="t_normal";
my($textObject);

our($install_state);
my(%test_status,%endtime,%starttime);	# these are 
my($gemrootdir);
my($GemConfig,%CONFIG);
my(%graphics_object);
my(@ListOfTests,%TestCommands);
my(%CommandResults);					# values are array refs
my(@saved_prints);

# USER DEFINABLE CONFIGURABLE ITEMS
my($NUM_BUTTONS_PER_COL)=3;

die "ERROR: DISPLAY VARIABLE IS NOT SET\n" if ! defined $ENV{DISPLAY} and ! defined $ENV{PROCESSOR_LEVEL};

use vars qw( $TESTALL $TESTSQLSVR $TESTDATABASE $TESTUNIX $DEBUG $NOPAUSE );

my($curdir)=dirname(dirname($0));		# REMEMBER THE FILE IS ACTUALLY IN bin
chdir $curdir or die "Cant chdir to $curdir : $! \n";

mkdir('logs', 0755) unless -d "logs";

test_initialize();
if( $install_state ne "INSTALLED" ) {
	myprint( "**************************\n");
	myprint( "* Please Run $^X configure.pl\n" );
	myprint( "**************************\n");
	exit(0);
}

myprint( "USE SYBASE=",  $GemConfig->{USE_SYBASE_ASE}, 
			" USE SQLSVR=", $GemConfig->{USE_SQLSERVER}, 
			" USE ORACLE=", $GemConfig->{USE_ORACLE},"\n" );
myprint(	"INSTALLSTATE=",$GemConfig->{INSTALLSTATE},
			" INSTALLTYPE=",$GemConfig->{INSTALLTYPE},"\n" ); 	
myprint( " \n" );

my($PRODUCT_ARGS)="";
$PRODUCT_ARGS.=" --NO_SYBASE" if $GemConfig->{USE_SYBASE_ASE} eq "N";
$PRODUCT_ARGS.=" --NO_SQLSVR" if $GemConfig->{USE_SQLSERVER} eq "N";
$PRODUCT_ARGS.=" --NO_ORACLE" if $GemConfig->{USE_ORACLE} eq "N";

myprint("DISPLAY VARIABLE=$ENV{DISPLAY}  [ required on unix ]\n") unless is_nt(); 
myprint("Building TK Windows... (If this fails, something is wrong witn your x setup)\n") unless is_nt();

my $MW 	  = MainWindow->new(-title=>'GEM Diagnostic Utility');
myprint("Scaling TK Frames...\n");
$MW->scaling(2) if $MW->scaling() > 2;
myprint("Building TK Frames...\n");
my $FRMA   = $MW->LabFrame(-relief=>'sunken', -label=>"Individual Server Connectivity", -labelside=>'acrosstop'	)->pack(qw/-side top -fill both -expand 1/);
my $FRMC   = $MW->LabFrame(-relief=>'sunken', -label=>"Test GEM Setup", -labelside=>'acrosstop'	)->pack(qw/-side top -fill both -expand 1/);
my $FRMD   = $MW->LabFrame(-relief=>'sunken', -label=>"Show Test Results", -labelside=>'acrosstop'	)->pack(qw/-side top -fill both -expand 1/);
my $FRMR   = $MW->LabFrame(-relief=>'raised', -label=>"Results", -labelside=>'acrosstop'	)->pack(qw/-side top -fill both -expand 1/);

$MW->fontCreate('header', 	-family=>'Ariel', -size=>'12', -weight=>'bold');
$MW->fontCreate('large', 	-family=>'Ariel', -size=>'12' );
$MW->fontCreate('bold', 	-family=>'Ariel', -size=>'10', -weight=>'bold');
$MW->fontCreate('code', 	-family=>'Courier', -size=>'10');
$MW->fontCreate('norm', 	-family=>'Ariel', -size=>'10');
$MW->fontCreate('small', 	-family=>'Ariel', -size=>'9');
$MW->fontCreate('smbold', 	-family=>'Ariel', -size=>'9', -weight=>'bold');
$MW->fontCreate('tiny', 	-family=>'Ariel', -size=>'6');

my($row)=1;
$var{"DB.DEBUG"}='N';
$FRMA->Checkbutton(	-font=>"large",-variable=>\$var{"DB.DEBUG"},-onvalue=>'Y',-offvalue=>'N',-text=>"Debug Mode")->grid(-column=>4, -row=>$row, -sticky=>'w');
$var{"DB.ALLSERVERS"}='N';

$graphics_object{'SYBASE CONNECT'}=$FRMA->Button(-text=>'Sybase Connectivity',	-command=> sub { 
			if( $var{"DB.SYBASENAME"} =~ /^\s*$/ ) {
					$MW->messageBox(
							-title => 	'No Server',
							-message =>	"Must Select Server For Connectivty Test",
							-type => "OK" );
					return;
			}
			my($cmd)=$gemrootdir."/bin/test_dbi_connectivity.pl ";
			#if( $var{"DB.ALLSERVERS"} eq "N" and $var{"DB.SYBASENAME"}!~/^\s*$/ ) {
			my($login,$password)=get_password(-type=>"sybase",-name=>$var{"DB.SYBASENAME"});
			$cmd .= "--SERVER=".$var{"DB.SYBASENAME"}." --LOGIN=$login --PASSWORD=$password ";
			#} else {
			#		# no server
			#		$cmd .= "--ALL=sybase ";
			#}
			$cmd .= "--DEBUG " if $var{"DB.DEBUG"} eq "Y";		
			ClearResults();		
			RunOperation('SYBASE CONNECT',$cmd); 
	}, 
	-background=>'yellow')->grid(-column=>1,-row=>$row, -padx=>5, -pady=>2, -sticky=>'nsew');
		
myprint("Fetching Server Config Info...\n");
my @sybservers = get_password(-type=>"sybase") if $install_state ne "UNINSTALLED";
$var{"DB.SYBASENAME"}="";
my($syb_c)=$FRMA->JComboBox(
   	-choices=> \@sybservers,
   	-relief=>'sunken',
   	-background=>'white',   	
   	-mode => "editable",
   	-entrywidth=>25,
   	-textvariable=>\$var{"DB.SYBASENAME"},
   	-maxrows=>4
   )->grid(-column=>2, -row=>$row, -padx=>5, -sticky=>'nsew');	
   
if( $GemConfig->{USE_SYBASE_ASE} eq "N") {	  
 	 $FRMA->Label(-text=>"SYBASE ASE DISABLED", -font=>"large", -justify=>'right')->grid(-column=>3, -row=>$row);
	 $graphics_object{'SYBASE CONNECT'}->configure(-state=>"disabled", -background=>'grey');	
	 $syb_c->configure(-state=>"disabled", -background=>'grey');	
}
$row++;

$graphics_object{'SQL SERVER CONNECT'}=$FRMA->Button(-text=>'Sql Server Connectivity',	-command=> sub { 	
			if( $var{"DB.SQLSERVERNAME"} =~ /^\s*$/ ) {
					$MW->messageBox(
							-title => 	'No Server',
							-message =>	"Must Select Server For Connectivty Test",
							-type => "OK" );
					return;
			}
			my($cmd)=$gemrootdir."/bin/test_dbi_connectivity.pl ";			
			my($login,$password)=get_password(-type=>"sqlsvr",-name=>$var{"DB.SQLSERVERNAME"});
			$cmd .= "--SERVER=".$var{"DB.SQLSERVERNAME"}." --LOGIN=$login --PASSWORD=$password ";			
			$cmd .= "--DEBUG " if $var{"DB.DEBUG"} eq "Y";		
			ClearResults();		
			RunOperation('SQL SERVER CONNECT',$cmd); 
	 }, 
	-background=>'yellow')->grid(-column=>1,-row=>$row, -padx=>5, -pady=>5, -sticky=>'nsew');
my @dbservers = get_password(-type=>"sqlsvr") if $install_state ne "UNINSTALLED";
my($sql_c)=$FRMA->JComboBox(
   	-choices=> \@dbservers,
   	-relief=>'sunken',
   	-background=>'white',   	
   	-mode => "editable",
   	-entrywidth=>25,
   	-textvariable=>\$var{"DB.SQLSERVERNAME"},
   	-maxrows=>4
   )->grid(-column=>2, -row=>$row, -padx=>5, -sticky=>'nsew');	
 
if( $GemConfig->{USE_SQLSERVER} eq "N" or ! is_nt() ) {	  
 	 $FRMA->Label(-text=>"SQL SERVER DISABLED", -font=>"large", -justify=>'right')->grid(-column=>3, -row=>$row);
	 $graphics_object{'SQL SERVER CONNECT'}->configure(-state=>"disabled", -background=>'grey');	
	 $sql_c->configure(-state=>"disabled", -background=>'grey');	
}	

$row++;
$graphics_object{'TEST UNIX CONNECTIVITY TESTS'}=$FRMA->Button(-text=>'Unix Connectivity',-command=> sub { 
		ClearResults();		
		my($cmd)=$gemrootdir."/bin/test_ping_systems.pl ";	
		$cmd .= "--SYSTEM=".$var{"HOST.UNIXSERVERNAME"};
		$cmd .= "--DEBUG " if $var{"DB.DEBUG"} eq "Y";				
		RunOperation('TEST UNIX CONNECTIVITY TESTS',$cmd); 
		}, 
	-background=>'yellow')->grid(-column=>1,-row=>$row, -padx=>5, -pady=>5, -sticky=>'nsew');
my @unixservers = get_password(-type=>"unix") if $install_state ne "UNINSTALLED";
my($unix_c)=$FRMA->JComboBox(
   	-choices=> \@unixservers,
   	-relief=>'sunken',
   	-background=>'white',
   	-mode => "editable",-entrywidth=>25,
   	-textvariable=>\$var{"HOST.UNIXSERVERNAME"},
   	-maxrows=>4
   )->grid(-column=>2, -row=>$row, -padx=>5, -sticky=>'nsew');	
if( $GemConfig->{INSTALLTYPE} eq "WINDOWS" ) {	  
 	 $FRMA->Label(-text=>"UNIX DISABLED", -font=>"large", -justify=>'right')->grid(-column=>3, -row=>$row);
	 $graphics_object{'TEST UNIX CONNECTIVITY TESTS'}->configure(-state=>"disabled", -background=>'grey');	
	 $unix_c->configure(-state=>"disabled", -background=>'grey');	
}
$row++;

$graphics_object{'TEST WIN32 CONNECTIVITY TESTS'}=$FRMA->Button(-text=>'Win32 Connectivity',-command=> sub { 
	ClearResults();		
	RunOperation('TEST WIN32 CONNECTIVITY TESTS'); }, 
	-background=>'yellow')->grid(-column=>1,-row=>$row, -padx=>5, -pady=>5, -sticky=>'nsew');
my @win32servers = get_password(-type=>"win32servers") if $install_state ne "UNINSTALLED";
my($win_c)=$FRMA->JComboBox(
   	-choices=> \@win32servers,
   	-relief=>'sunken',
   	-background=>'white',   	
   	-mode => "editable",-entrywidth=>25,
   	-textvariable=>\$var{"HOST.WIN32SERVERNAME"},
   	-maxrows=>4
   )->grid(-column=>2, -row=>$row, -padx=>5, -sticky=>'nsew');	
if( $GemConfig->{INSTALLTYPE} eq "UNIX" or ! is_nt() ) {	  
 	 $FRMA->Label(-text=>"WIN32 DISABLED", -font=>"large", -justify=>'right')->grid(-column=>3, -row=>$row);
	 $graphics_object{'TEST WIN32 CONNECTIVITY TESTS'}->configure(-state=>"disabled", -background=>'grey');	
	 $win_c->configure(-state=>"disabled", -background=>'grey');	
}
$row++;

$FRMC->Button(-text=>'Run All Tests', 
	-background=>'yellow',
	-command=> sub { 
			ClearResults();
			my($rc)=test_dir_structure(); 
			$graphics_object{'Directory Structure'}->configure(-bg=>"white") 	if $rc==0; 			
			ClearResults();
			$rc=test_configuration_files();
			$graphics_object{'Configuration Files'}->configure(-bg=>"white") 	if $rc==0;  
			ClearResults();
			$rc=test_include_path();
			$graphics_object{'Perl Include Path'}->configure(-bg=>"white") 			if $rc==0;  
			foreach my $tst ( @ListOfTests ) { 				
				next if(( $GemConfig->{USE_SQLSERVER} eq "N" or ! is_nt() ) and $tst =~ /SqlSvr/);
				ClearResults();
				RunOperation($tst,$TestCommands{$tst}); 
				$textObject->update();
			}
		})->grid(-column=>0,-row=>$row, -columnspan=>3,-padx=>5, -pady=>5, -sticky=>'nsew');
$row++;

myprint("Building Test Buttons...\n");
my($col)=0;
sub test_btn
{
	my($txt,$subroutine, $file)=@_;
	$test_status{$txt}="NOT RUN";
	if( $subroutine) {
		$graphics_object{$txt}=	$FRMC->Button(-width=>22,
			-text=>$txt,
			-background=>'yellow',
			-command=> $subroutine )->grid(-column=>$col++,-row=>$row, -padx=>5, -pady=>5);
	} else {
		push @ListOfTests,$txt;
		$TestCommands{$txt} = $file;
		$graphics_object{$txt}=	$FRMC->Button(-width=>22,
			-text=>$txt,
			-background=>'yellow',
			-command=>sub { 
				return if is_busy();	
				busy();
				ClearResults();
				RunOperation($txt,$file);
				unbusy(); } )->grid(-column=>$col++,-row=>$row, -padx=>5, -pady=>5);
	}
	if( $col>=$NUM_BUTTONS_PER_COL ) { $col=0; $row++; }
}

$test_status{"Directory Structure"}="NOT RUN";
$test_status{"Configuration Files"}="NOT RUN";
$test_status{"Perl Include Path"}="NOT RUN";

test_btn('Directory Structure',	sub { ClearResults();my($rc)=test_dir_structure();	$graphics_object{'Directory Structure'}->configure(-bg=>"white") if $rc==0; });
test_btn('Configuration Files', 	sub { ClearResults();my($rc)=test_configuration_files();	$graphics_object{'Configuration Files'}->configure(-bg=>"white") if $rc==0;  });
test_btn('Perl Include Path',			sub { ClearResults();my($rc)=test_include_path();$graphics_object{'Perl Include Path'}->configure(-bg=>"white") if $rc==0;  });
test_btn('Required Commands',		undef,	$gemrootdir.'/bin/test_01_required_commands.pl' );
test_btn('Alarm System', 			undef,	$gemrootdir.'/bin/test_alarms.pl' );
test_btn('DBI/DBD Setup', 			undef,	$gemrootdir.'/bin/test_dbi.pl'.$PRODUCT_ARGS );
test_btn('Sybase Connectivity',	undef,	$gemrootdir.'/bin/test_ping_databases.pl --TYPE=SYBASE' );
test_btn('SqlSvr Connectivity',	undef,	$gemrootdir.'/bin/test_ping_databases.pl --TYPE=SQLSVR' );
test_btn('Oracle Connectivity',	undef,	$gemrootdir.'/bin/test_ping_databases.pl --TYPE=ORACLE' );
test_btn('Ping All Hosts', 	undef,	$gemrootdir.'/bin/test_ping_systems.pl' );
test_btn('Repository',				undef,	$gemrootdir.'/bin/test_repository.pl' );
test_btn('Unix Configuration',	undef,	$gemrootdir.'/bin/test_unix_config.pl' );

if( $GemConfig->{USE_SQLSERVER} eq "N" or ! is_nt() ) {	  
	 $graphics_object{'SqlSvr Connectivity'}->configure(-state=>"disabled", -background=>'grey');	
}	

if( $GemConfig->{INSTALLTYPE} eq "WINDOWS" ) {	  
 	 $graphics_object{'Unix Configuration'}->configure(-state=>"disabled", -background=>'grey');	
	 $graphics_object{'Unix Configuration'}->configure(-state=>"disabled", -background=>'grey');	
}

$FRMD->Label(-text=>"Installation State: $install_state", -font=>"bold", -justify=>'left')->grid(-column=>0, -row=>$row, -sticky=>'w');
$FRMD->Label(-text=>"Installation State: $install_state", -font=>"bold", -justify=>'left')->grid(-column=>0, -row=>$row, -sticky=>'w');

$FRMD->Button(-text=>'SHOW TEST RESULTS',
	-background=>'white',
	-command=> sub { 
	ClearResults();
	set_tag("t_hdr");
	myprint( "TEST RESULTS\n" );
	myprint(sprintf( "%7s %22s %4s %s\n","Status",'Test Name','Secs',"Results" ));
	clear_tag();	
	foreach my $t ( sort keys %test_status ) {		
		set_tag("t_normal");
		my($rows)=0;
		set_tag("t_error") if $test_status{$t} ne "OK";
		foreach ( @{$CommandResults{$t}} ) {
			chomp;chomp;
			if( $rows==0 ) {
				myprint(sprintf( "%7s %22s %4s %s",$test_status{$t},$t,$endtime{$t}-$starttime{$t},$_ ));
			} else {
				myprint(sprintf("%35s %s","",$_)."\n" );
			}
			$rows++;
		}	
		if( $rows==0 ) {
			myprint(sprintf( "%7s %22s %4s\n",$test_status{$t},$t,$endtime{$t}-$starttime{$t} ));
		} 
	}
 })->grid(-column=>1,-row=>$row, -padx=>5, -pady=>5, -sticky=>'e');

$FRMD->Button(-text=>'SHOW ERRORS',
	-background=>'white',
	-command=> sub { 
	ClearResults();
	set_tag("t_hdr");
	myprint( "TEST ERRORS\n" );
	myprint(sprintf( "%7s %22s %4s %s\n","Status",'Test Name','Secs',"Results" ));
	clear_tag();	
	foreach my $t ( sort keys %test_status ) {		
		next unless $test_status{$t} ne "OK";
		my($rows)=0;
		set_tag("t_error");
		foreach ( @{$CommandResults{$t}} ) {
			chomp;chomp;
			if( $rows==0 ) {
				myprint(sprintf( "%7s %22s %4s %s",$test_status{$t},$t,$endtime{$t}-$starttime{$t},$_ ));
			} else {
				myprint(sprintf("%35s %s","",$_)."\n" );
			}
			$rows++;
		}	
		if( $rows==0 ) {
			myprint(sprintf( "%7s %22s %4s\n",$test_status{$t},$t,$endtime{$t}-$starttime{$t} ));
		} 
	}
 })->grid(-column=>2,-row=>$row, -padx=>5, -pady=>5, -sticky=>'e');

$FRMD->Button(-text=>'INITIALIZATION MESSAGES',
	-background=>'white',
	-command=> sub { 
	ClearResults();
	foreach ( @saved_prints ) { myprint($_); }
 })->grid(-column=>3,-row=>$row, -padx=>5, -pady=>5, -sticky=>'e');

$FRMD->Button(-text=>'TIMINGS',
	-background=>'white',
	-command=> sub { 
	ClearResults();
	ShowTimings();
 })->grid(-column=>4,-row=>$row, -padx=>5, -pady=>5, -sticky=>'e');
  
$row++;

$textObject = $FRMR->Scrolled("ROText",
			-relief => "sunken", 
			-wrap=>'none', -width=>120,
      	-bg=>'white',
      	-scrollbars => "osoe")->grid(-column=>1, -row=>$row, -sticky=>'ew',-columnspan=>3, -rowspan=>20);

myprint("Building Fonts...\n");
$textObject->tagConfigure("t_hdr",   -foreground=>'black', -relief=>'raised', -background=>'beige');
$textObject->tagConfigure("t_blue",  -foreground=>'blue');
$textObject->tagConfigure("t_error",   -foreground=>'red');
$textObject->tagConfigure("t_debug", -foreground=>'green');
$textObject->tagConfigure("t_normal", -foreground=>'black');

$textObject->insert("end",$save_print_string);
myprint("Passing Control To Tk Main Loop\n");
$save_print_string="";

MainLoop();
close(LOG);
exit(0);

sub set_tag {		$cur_tag=@_;	}
sub clear_tag {	$cur_tag="t_normal";	}

sub myprint_withsave {
	push @saved_prints,join("",@_);
	myprint(@_);
}
	
sub myprint 
{
		$lineno=0 unless $lineno;
		my($line)=join("",@_);
		# print "<START LINE>=$line <END LINE>\n";
	   foreach my $lx ( split(/\n/,$line)) {
			# print "<STARTLX>=$lx<END LX>\n";
			$lineno++;
			my($l)=sprintf("%-4s",$lineno);
			#$l="    " if $lx=~/^\s*$/;
			print $l." : ".$lx,"\n";
			print LOG $l." : ".$lx,"\n" if defined $install_state;		
			if( ! defined $textObject ) {
				$save_print_string .= $l." : ".$lx."\n";
				return;
			} else {
				$textObject->insert("end",$l." : ","t_blue");
				if( $lx=~/^debug/ ) {
					$textObject->insert("end",$lx."\n","t_debug");
				} else {
					$textObject->insert("end",$lx."\n",$cur_tag);
				}
				$textObject->update();
			}
		}
}

sub ClearResults{	$lineno=0; $textObject->delete("1.0",'end'); }

sub RunOperation {
	my($op_name, $op_command)=@_;
	die "NO Command Passed To RunOperation!\n" unless $op_command;
	
	start_test($op_name);	
	my($str)="$^X -I".join(" -I",@perl_includes). " $op_command";
	myprint("Running: ".	$str);
	my($rc,$outptr)=RunCommand(
		-cmd=>$str,
		-mainwindow=>$MW,
		-statusfunc=>\&myprint,
		-printhdr  => "[$op_name] : ",
		-printfunc =>\&myprint,
		-debug=>$DEBUG
	);	
	
	myprint("debug() RETURN CODE=$rc\n" ) if $DEBUG;
	my(@error_messages);
	my($vstate)="INCOMPLETE";
	foreach ( @$outptr ) {
		chomp;
		myprint("debug() $_\n") if $DEBUG;
		if( /ERROR:/ or /WARNING:/ ) {
			push @error_messages,$_;
			$vstate="ERROR";			
			$graphics_object{$op_name}->configure(-bg=>"pink");
		} elsif( /SUCCESSFUL:/ ) {
			push @error_messages,$_;
			$vstate="OK";
			$graphics_object{$op_name}->configure(-bg=>"white");
		} elsif( /SKIPPED:/ ) {
			push @error_messages,$_;
			$vstate="SKIPPED";
			$graphics_object{$op_name}->configure(-bg=>"white");
		}
	}
	$CommandResults{$op_name}=\@error_messages;
	if( $vstate eq "INCOMPLETE" ) {
		myprint( "$op_name Did Not Return A Line with ERROR: or WARNING: or SUCCESSFUL: or SKIPPED: - test failed\n\n" );	
	} else {
		myprint( "COMPLETED ($vstate): $op_name\n\n" );	
	}
	end_test($vstate);	
	return;
}

BEGIN {
	use Getopt::Long;
	
	die "Bad Parameter List $!\nAllowed arguments to are --TESTSQLSERVER --TESTDATABASE --TESTUNIX\nor --TESTALL" unless
   GetOptions(  "TESTSQLSVR"	=> \$TESTSQLSVR,
		"TESTDATABASE"				=> \$TESTDATABASE,
		"TESTALL"					=> \$TESTALL,
		"NOPAUSE"					=> \$NOPAUSE,
		"DEBUG"						=> \$DEBUG,
		"TESTUNIX"					=> \$TESTUNIX	);

	$starttime{"BEGIN BLOCK"}=time;
	myprint_withsave("troubleshoot.pl - Generic Enterprise Manager troubleshooter\n");
	myprint_withsave(" \n");
	select STDOUT;$|=1;

	myprint_withsave("Testing Perl & Perl Library Setup\n");
	myprint_withsave("   Your Perl version is $]\n");

	if( $] < 5.008001 and defined $ENV{PROCESSOR_LEVEL} ) {
		if( defined $^V and $^V!~/\s*$/) {
			die "Error : You must have perl 5.8.1 installed under Win32.  \nYou are running $^V \n";
		}
		my($x)=$];
		$x=~s/0+/\./g;
		$x=~s/\.\.+/\./g;
		die "Error : You must have perl 5.8.1 installed.  \nYou are running $x ($])\n";
	}

	if( ! defined $ENV{DISPLAY} and ! defined $ENV{PROCESSOR_LEVEL} ) {
		die "Error : DISPLAY environment variable not set!\n";
	}

	my(%badpackage)=();
	sub testit {
		my($pkg_arryref, $msgbad, $msgok)=@_;
		my $err;
		my $numbad=0;
		foreach my $str (@$pkg_arryref) {

			myprint_withsave("Testing $str\n") if $DEBUG;
			if ( eval "require $str" ) {
				$str->import();
			} else {
				my $err = $@;
				chomp $err;
				$numbad++;
				myprint_withsave("--$str Results $err\n") if $DEBUG;
				$badpackage{$str}=$err;
			}
		}

		if ($numbad ) {
			print $msgbad;
			foreach ( keys %badpackage) {
				myprint_withsave("Package Did Not Load: $_ \n");
				if( defined $ENV{PROCESSOR_LEVEL} ) {
					myprint_withsave("\tplease run: ppm install DBI\n")	if $_ eq "DBI";
					myprint_withsave("\tplease run: ppm install DBD-ODBC\n") if $_ eq "DBD::ODBC";

					if(  $]<5.008 ) {
						myprint_withsave("\tplease run: ppm install Win32-TaskScheduler --location=http://taskscheduler.sourceforge.net/perl/\n")
							if $_ eq "Win32::TaskScheduler";
					} else {
						myprint_withsave("\tplease run: ppm install http://taskscheduler.sourceforge.net/perl58/Win32-TaskScheduler.ppd\n")
							if $_ eq "Win32::TaskScheduler";
					}
				}

				foreach ( split( /\n/,$badpackage{$_} )) {
					next if /^Can't locate /;
					myprint_withsave("\t=> $_\n");
				}
			}

			myprint_withsave("The package(s) listed above MUST be installed before you may proceed.\n\n");
			exit(1);
		} else {
			myprint_withsave( $msgok );
		}
	}

	my @std_packages=qw(CGI Getopt::Std Carp DBM::Deep File::Copy File::Basename 
			Sys::Hostname Net::FTP Data::Dumper File::Find File::Path LWP::Simple
			Socket Tk URI::Escape Log::Dispatch Log::Dispatch::Screen
			Log::Dispatch::File Fcntl Getopt::Long Data::Dumper );
	my @dbi_packages=qw(DBI);
	if( defined $ENV{PROCESSOR_LEVEL}) {
		push @dbi_packages,"DBD::ODBC";
	} else {
		push @dbi_packages,"DBD::Sybase";
	}

	my @tk_packages=qw(Tk::HList Tk::Tree Tk::DialogBox Tk::ItemStyle Tk::NoteBook Tk::BrowseEntry Tk::Listbox
				Tk::ErrorDialog Tk::Menu Tk::ROText Tk::Scrollbar Tk::Pixmap Tk::Label Tk::Balloon );

	if( defined $ENV{PROCESSOR_LEVEL}) {
		push @dbi_packages,"Win32::TaskScheduler";
	}

	my @gem_packages=qw(CommonFunc);
	my @my_packages=qw(Net::myFTP Net::myRSH Repository DBIFunc Do_Time MlpAlarm CommonFunc Do_Time RunCommand LogFunc);

	myprint_withsave("   => Testing Perl Builtin Package Integrity\n      ");
	testit( \@std_packages,
		"\nERROR: At least one pre-requisite required package was not found!\nThese are required parts of perl and must be available\nContact your system administrator\n",
		"   OK: All Required Builtin Perl Packages Found!!\n" );

	myprint_withsave("   => Testing Perl Database Connectivity Package Integrity\n      ");
	testit( \@dbi_packages,
		"\nERROR: Database Tests FAILED\n",
		"   OK: All Required Perl/DBI Packages Found!!\n" );

	if( ! defined $badpackage{DBI}) {
		if( $DBI::VERSION < 1.0 ) {
			myprint_withsave("   ERROR - DBI Version ($DBI::VERSION) Must be at 1.0 or later");
		} else {
			myprint_withsave("         OK: DBI Version is ($DBI::VERSION)\n");
		}
	}

	if( defined $ENV{PROCESSOR_LEVEL}) {
		myprint_withsave( "   DBD::ODBC Version ($DBD::ODBC::VERSION) Should be at 1.0 or later" )
			if $DBD::ODBC::VERSION < 1.0;
		myprint_withsave("         OK: DBD::ODBC Version is ($DBD::ODBC::VERSION)\n");
	} else {
	   if( $DBD::Sybase::VERSION < 1.0 ) {
		   myprint_withsave("   ***************************************\n");
		   myprint_withsave("   * WARNING - DBD::Sybase Version ($DBD::Sybase::VERSION) Should Probably be at 1.0 or later\n");
		   myprint_withsave("   * This will not stop your install but a more stable version is better\n");
		   myprint_withsave("   ***************************************\n");
	   } else {
		   myprint_withsave("         OK: DBD::Sybase Version is ($DBD::Sybase::VERSION)\n");
	   };
	}

	myprint_withsave("   => Testing Perl/Tk Package Integrity\n   ");
	testit( \@tk_packages,
		"\nERROR: At least one Tk Package did not load found!\nThese are usually shipped with Tk - Is your version recent?\nYour Perl Tk Version is $Tk::VERSION",
		"   OK: All Required Perl/Tk Packages Found!!\n" );
	if( $Tk::VERSION < 800.000 ) {
		   myprint_withsave("   ***************************************\n");
		   myprint_withsave("   * ERROR - Tk Version ($Tk::VERSION) MUST be 8.24 or later\n");
		   myprint_withsave("   ***************************************\n");
	} elsif( $Tk::VERSION < 800.024 ) {
		   myprint_withsave("   ***************************************\n");
		   myprint_withsave("   * WARNING - Tk Version ($Tk::VERSION) Should Probably be at 8.24 or later\n");
		   myprint_withsave("   * This will not stop your install but a more stable version is better\n");
		   myprint_withsave("   ***************************************\n");
	   } else {
		   myprint_withsave("   OK: Tk Version is ($Tk::VERSION)\n" );
		}

	myprint_withsave("   => Testing Gem Add On Module Integrity\n      " );
	testit( \@gem_packages,
		"\nERROR: At least one gem module did not load!\n",
		"   OK: All Gem Packages Found!!\n" );

	myprint_withsave("   => Testing Toolkit Built In Module Integrity\n      " );
	testit( \@my_packages,
		"\nERROR: At least one gem module did not load!\n",
		"   OK: All Builtin Packages Found!!\n" );
	$endtime{"BEGIN BLOCK"}=time;
}

my($current_test);
sub start_test {
	my($id,$funcname)=@_;
	$current_test=$id;
	
	my($s)= "Testing $id ";
	$s.= "- $funcname"  if $funcname;
	$s.= "\n";
	myprint( $s );
	
	$starttime{$id}=time;
	myprint("debug() start_test called\n") if $DEBUG;
}
sub end_test {
	my($end_state)=@_;
	
	if( $end_state eq "SKIPPED" ) {
		myprint( "This Test was SKIPPED\n" );
	} else {
		myprint( "Test '".$current_test."' returned status=$end_state\n" );		
	}
	myprint("==========================================\n");
	$endtime{$current_test}=time;
	$test_status{$current_test}=$end_state;	
	myprint("debug() end_test called - $current_test set to $end_state\n") if $DEBUG;
}

sub test_include_path 
{
	return if is_busy();	
	busy();
	start_test("Perl Include Path","test_include_path()");	
	die "\nERROR Cant calculate module perl path - no gem libraries in \@INC\nThe program searches for $gemrootdir in\n".join("\n\t",@INC)."\n"
		if $#perl_includes<0;
	my($rc)="OK";
	my(@results);
	foreach(@INC) {
		myprint("$_\n");
		next if -d $_;
		push @results,"Include Directory $_ Not Found\n";
		myprint("Include Directory $_ Not Found\n");
	}		
	push @results,"SUCCESSFUL: Perl Include Path OK" if $#results<0;
	$CommandResults{"Perl Include Path"}=\@results;
	end_test($rc);
	unbusy();
	return 0;
}

sub test_configuration_files {
	return if is_busy();	
	busy();
	my(@results);
	my($rc)="OK";
	start_test("Configuration Files","test_configuration_files()");
	
	myprint("Testing $gemrootdir/conf/configure.cfg");
	if( ! -r "$gemrootdir/conf/configure.cfg" )  {
		myprint( "Configuration file configure.cfg is missing from the conf subdirectory.\n" );
		myprint( "This is normal: please run the GEM setup utility configure.pl.\n" );	
		unbusy();
		end_test("SKIPPED");
		$CommandResults{"Configuration Files"}=\("SKIPPED: GEM not initialized");
		return 1;
	}
	
	myprint("Testing $gemrootdir/conf/gem.dbm");
	if( ! -r "$gemrootdir/conf/gem.dbm" )  {
		myprint( "Configuration file gem.dbm is missing from the conf subdirectory.\n" );
		myprint( "This is normal: please run the GEM setup utility configure.pl.\n" );	
		unbusy();		
		end_test("SKIPPED");
		$CommandResults{"Configuration Files"}=\("SKIPPED: GEM not initialized");
		return 1;
	}		
	
	foreach (qw(unix_passwords.dat sybase_passwords.dat oracle_passwords.dat sqlsvr_passwords.dat win32_passwords.dat)) {
		myprint("Testing $gemrootdir/conf/$_");
		if( ! -r "$gemrootdir/conf/$_" )  {
			my($str)="Warning: File $_ Not Found - This will not stop your install but may be of concern\n";
			myprint( $str );
			$rc="ERROR";
			push @results,$str;
		}		
	}	
	
	foreach (qw(console.dat win32_cleanup.dat win32_service.dat port_monitor.dat threshold_overrides.dat)) {
		myprint("Testing $gemrootdir/conf/$_");
		if( ! -r "$gemrootdir/conf/$_" )  {
			my($str)="Note: Extended Functionality Configuration File $_ Not Found\nYou may wish to set up this file by hand to add GEM functionality\n";
			push @results,$str;
			$rc="WARNING";
			myprint( $str );
		}		
	}
	end_test($rc);
	push @results,"SUCCESSFUL: Config Files OK" if $#results<0;
	$CommandResults{"Configuration Files"}=\@results;
	unbusy();
	return 0;
}

#
# Create Directories If Needed
#
# fix the directory structure if necessary - winzip apparently does not expand null files
sub md {
	my($dirtomake)=@_;
	if( -d $dirtomake ) {
		if( ! -r $dirtomake ) {			
			return 1, "WARNING: Directory $dirtomake is not readable\n";
		}
		if( ! -w $dirtomake ) {			
			return 1, "WARNING: Directory $dirtomake is not writable\n";
		}			
		return 0,"OK $gemrootdir/$_ ";
	#} else {
		#myprint( "Making Directory $dirtomake" );
		#mkdir( $dirtomake, 0755 ) or die "Cant mkdir $dirtomake : $!\n";
	}

	if( ! -d "$gemrootdir/$_" ) {			
		return 1,"\tDirectory $gemrootdir/$_ not found\n";
	} elsif( ! -r "$gemrootdir/$_" ) {			
		return 1,"\tDirectory $gemrootdir/$_ is not readable\n";
	} elsif( ! -w "$gemrootdir/$_" ) {			
		return 1, "\tDirectory $gemrootdir/$_ is not writable\n";
	}
	return 0,"OK $gemrootdir/$_ ";
}

sub test_dir_structure {
	return if is_busy();	
	busy();
	start_test("Directory Structure","test_dir_structure()");
	chdir($gemrootdir);	
	my($rootdir)=$gemrootdir;
	my(@results);
	my($retcode)="OK";
	my(@dirs)= qw?conf data ADMIN_SCRIPTS doc plugins Win32_perl_lib_5_8 bin
		win32_batch_scripts win32_batch_scripts/interactive win32_batch_scripts/batch win32_batch_scripts/plan_interactive
		win32_batch_scripts/plan_batch data lib unix_batch_scripts unix_batch_scripts/interactive
		unix_batch_scripts/batch unix_batch_scripts/plan_interactive unix_batch_scripts/plan_batch
		build_tools?;

	my(@data_dirs)=qw?BACKUP_LOGS cronlogs misc CONSOLE_REPORTS depends patern_files
			html_output system_information_data misc
				system_information_data/schedlgu system_information_data/hosts 
				lockfiles GEM_BATCHJOB_LOGS?;

	my($num_errors)=0;
	
	myprint ( "\tTesting... $rootdir\n" );	
	foreach (@dirs)      {
		my($rc,$msg)=md("$rootdir/$_");
		$num_errors+=$rc;
		next unless $rc == 1 or $DEBUG;
		push @results,$msg;
		myprint( $msg );
	}
	myprint ( "\tTesting... $rootdir/data\n" );	
	foreach (@data_dirs) {
		my($rc,$msg)=md("$rootdir/data/$_");
		$num_errors+=$rc;		
		next unless $rc == 1 or $DEBUG;
		push @results,$msg;
		myprint( $msg );
	}	
	myprint ( "\tGEM Root Directory = $gemrootdir\n" );	
	if( $num_errors) {		
		if( $install_state ne "INSTALLED" ) {
			myprint( "**************************\n");
			myprint( "* GEM Has Not Been Installed Yet\n" );	
			myprint( "* Next step is $^X configure.pl\n" );		
			myprint( "* Current system state=$install_state\n" );
			myprint( "**************************\n");
			$CommandResults{"Directory Structure"}=\("GEM Not Installed\n");
			end_test("SKIPPED");		
			unbusy();			
			return 0;
		}
		$retcode="ERROR";
		myprint( "Exiting - The directory structure contains errors (above).\n" );
		myprint( "This indicates that you have not successfully run the GEM Configuration Utility.\nIf you have not run configure.pl, run it now.\n" );	
		unbusy();		
	}
	end_test($retcode);
	push @results,"SUCCESSFUL: Directory Structure OK" if $#results<0;
	$CommandResults{"Directory Structure"}=\@results;
	unbusy();
	return 0;
}

sub ShowTimings {
	myprint(sprintf( "%7s %22s %4s %s","STATUS","TEST NAME","DURATION (Secs)","NOTES")); 
	foreach my $t ( sort keys %test_status ) {		
		set_tag("t_normal");
		set_tag("t_error") if $test_status{$t} ne "OK";
		myprint(sprintf( "%7s %22s %4s %s",$test_status{$t},$t,$endtime{$t}-$starttime{$t},$_ )); 
	}
}

sub test_initialize {
	my($logfilename)=hostname();
	$logfilename .= ".".$ENV{USER} if $ENV{USER};
	$logfilename .= ".troubleshoot.log";
	$logfilename = "logs/".$logfilename;
	open(LOG,">".$logfilename) or die "Cant write file ".$logfilename."\n";
	
	myprint( " \n" );
	myprint( "OUTPUT IS LOGGED TO ".$logfilename."\n" );	
	myprint( " \n" );	
	
	$gemrootdir=get_gem_root_dir();
	if( ! -d $gemrootdir ) {
		myprint( "ERROR: Something is wrong - can not parse application root directory from its include path\n" );	
		exit(0);
	}
	myprint( "Gem Root Directory = $gemrootdir \n" );
	
	my($dir)=get_conf_dir();	
	myprint( "Gem Configuration Directory = $dir \n" );
	
	if( -r $dir."/gem.dat" and -r $dir."/configure.cfg" ) {
		$install_state="INSTALLED";
		%CONFIG = read_configfile();
		$GemConfig=read_gemconfig();
	} else {
		$install_state="UNINSTALLED";
		myprint( "GEM is NOT installed\n" );
	}
	
	foreach (@INC) { chomp; push @perl_includes, $_ if /$gemrootdir/; }
	die "\nERROR Cant calculate module perl path - no gem libraries in \@INC\nThe program searches for $gemrootdir in\n".join("\n\t",@INC)."\n"
		if $#perl_includes<0;
		
	myprint(" \n");	
}

my($busy_count)=0;

sub is_busy
{
	myprint("[BUSY] skipping button click as application is busy\n" ) if $busy_count;
	return $busy_count;
}
sub busy
{
	$busy_count++;
	$MW->configure(-cursor=>'watch');
}

sub unbusy
{
	my($clear)=@_;
	$busy_count-- if $busy_count>0;
	$busy_count=0 if $clear;
	$MW->configure(-cursor=>'arrow') if $busy_count==0;
}

1;
__END__

=head1 NAME

new_troubleshoot.pl - Rules based troubleshooter

=head2 USAGE

perl new_troubleshoot.pl

=head2 DESCRIPTION

Troubleshoot.pl is a graphical troubleshooter for GEM.

=head2 EXTERNAL COMMAND EXECUTION

Currently the layout of this command is:

'Directory Structure',	sub { ClearResults();my($rc)=test_dir_structure();	$graphics_object{'Directory Structure'}->configure(-bg=>"white") if $rc==0; });
'Configuration Files', 	sub { ClearResults();my($rc)=test_configuration_files();	$graphics_object{'Configuration Files'}->configure(-bg=>"white") if $rc==0;  });
'Perl Include Path',		sub { ClearResults();my($rc)=test_include_path();$graphics_object{'Perl Include Path'}->configure(-bg=>"white") if $rc==0;  });
'Required Commands',			$gemrootdir.'/bin/test_01_required_commands.pl' );
'Alarm System', 				$gemrootdir.'/bin/test_alarms.pl' );
'DBI/DBD Setup', 				$gemrootdir.'/bin/test_dbi.pl'.$PRODUCT_ARGS );
'Sybase Connectivity',	$gemrootdir.'/bin/test_ping_databases.pl --TYPE=SYBASE' );
'SqlSvr Connectivity',	$gemrootdir.'/bin/test_ping_databases.pl --TYPE=SQLSVR' );
'Oracle Connectivity',	$gemrootdir.'/bin/test_ping_databases.pl --TYPE=ORACLE' );
'Ping All Hosts', 		$gemrootdir.'/bin/test_ping_systems.pl' );
'Repository',					$gemrootdir.'/bin/test_repository.pl' );
'Unix Configuration',		$gemrootdir.'/bin/test_unix_config.pl' );

The above commands are run as follows:

	my($str)="$^X -I".join(" -I",@perl_includes). " $op_command";
	my($rc,$outptr)=RunCommand(-cmd=>$str,	-statusfunc=>\&myprint,		-printhdr  => "[$op_name] : ",-printfunc =>\&myprint	);	
	foreach ( @$outptr ) {
		if( /ERROR:/ or /WARNING:/ ) {
			push @CommandResults,$_;
			$state="ERROR";			
			$graphics_object{$op_name}->configure(-bg=>"pink");
		} elsif( /SUCCESSFUL:/ ) {
			push @CommandResults,$_;
			$state="OK";
			$graphics_object{$op_name}->configure(-bg=>"white");
		} elsif( /SKIPPED:/ ) {
			push @CommandResults,$_;
			$state="SKIPPED";
			$graphics_object{$op_name}->configure(-bg=>"white");
		}
	}
	
=cut
