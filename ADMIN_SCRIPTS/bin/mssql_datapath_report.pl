#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use MlpAlarm;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Repository;

use vars qw( $SYSTEMS $DATABASE $NOLOG $DEBUG $TYPE $HTML $PRODONLY );

my($VERSION)=1.0;

$|=1;

my($query)='DECLARE @data_path nvarchar(512)
    DECLARE @sql_path nvarchar(512)
DECLARE @retcode int

 exec @retcode = master.dbo.sp_MSget_setup_paths
  @sql_path output,
  @data_path output
select @sql_path, @data_path';

sub usage
{
		# TYPE=sybase|sqlsvr
		#
	return "Usage: mssql_datapath_report.pl --HTML --DEBUG --SYSTEMS=system[,system] --PRODONLY\n";
}

#die usage() if $#ARGV<0;
die usage() unless
   GetOptions(  #"DATABASE=s"	=> \$DATABASE,
		"SERVER=s"	=> \$SYSTEMS,
		"SYSTEMS=s"	=> \$SYSTEMS,
		#"TYPE=s"	=> \$TYPE,
		"HTML"		=> \$HTML,
		"PRODONLY"	=> \$PRODONLY,
		"DEBUG"		=> \$DEBUG,
		"NOLOG"		=> \$NOLOG,
		);

$SYSTEMS="ALL" unless $SYSTEMS;
#$TYPE="sybase" unless is_nt();
cd_home();

my($nl)="\n";
$nl="<br>\n" if $HTML;
dbi_set_web_page(1) if $HTML;

print "<h1>" if $HTML;
print "Simple Audit Report",$nl;
print "</h1>" if $HTML;
print "Created by mssql_datapath_report.pl at ".localtime(time).$nl;
print "This report lists data paths for your sql servers",$nl;
print "This report will run only on production servers",$nl if $PRODONLY;

MlpBatchJobStart(-BATCH_ID => "DataPathReport");
my(@outdat);

if( $SYSTEMS eq "ALL" ) {
	if( ! $TYPE or $TYPE eq "sqlsvr" ) {
		foreach my $srv ( sort (get_password(-type=>'sqlsvr'))) {
			do_a_server($srv,'sqlsvr');
		}
	}
#	if( ! $TYPE or $TYPE eq "sybase" ) {
#
#		foreach my $srv (get_password(-type=>'sybase')) {
#			do_a_server($srv,'sybase');
#		}
#	}
} else {
	foreach my $srv ( split(/,/,$SYSTEMS)) {
		do_a_server($srv,undef);
	}
}

print "<TABLE BORDER=1><TR BGCOLOR=BEIGE><TH>SERVER</TH><TH>SQL Path</TH><TH>Data Paths</TH></TR>\n"
	if $HTML;
print "SERVER\tDB\tSQL PATH\tData Path\n"
	unless $HTML;
foreach (@outdat) { print $_; }
print "</TABLE>" if $HTML;

MlpBatchJobEnd();
exit(0);

sub do_a_server {
	my($srv,$type)=@_;
	print "Working on $srv",$nl if $DEBUG;
	my($login,$pass,$cmd);

	if( $SYSTEMS eq "ALL" and $PRODONLY ) {
		my %info = get_password_info(-name=>$srv, -type=>$type);
		if( $info{SERVER_TYPE} ne "PRODUCTION" and $info{SERVER_TYPE} ne "CRITICAL" and $info{SERVER_TYPE} ne "QA"  ) {
			print "Skipping Server $srv As It is ",$info{SERVER_TYPE},$nl if $DEBUG;
			next;
		}
	}
	print "Processing Server $srv",$nl if $DEBUG;

	if( defined $type ) {
		($login,$pass)=get_password(-type=>$type,-name=>$srv);
	} else {
		($login,$pass)=get_password(-type=>'sqlsvr',-name=>$srv);
		#($login,$pass)=get_password(-type=>'sybase',-name=>$srv) if ! $pass;
	}
   	print "Connecting to $srv\n" if $DEBUG;
   	my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass);
	if( ! $rcx ) {
      		print "Cant connect to $srv as $login",$nl;
		next;
	}

	my($DBTYPE,$DBVERSION,$VNO)=dbi_db_version();
	#print "SERVER=$srv TYPE=$DBTYPE VERSION=$DBVERSION X=$x\n";
	if( $VNO >= 9 ) {
		if( $HTML ) {
			push @outdat,"<TR><TD>$srv</TD><TD>Unavailable in 2005</TD><TD>Unavailable in 2005</TD></TR>\n";
		} else {
			push @outdat,"$srv\tUnavailable in 2005\n";
		}
	} elsif( $VNO < 8 ) {
		if( $HTML ) {
			push @outdat,"<TR><TD>$srv</TD><TD>Unavailable in $DBVERSION</TD><TD>Unavailable in $DBVERSION</TD></TR>\n";
		} else {
			push @outdat,"$srv\tUnavailable in $DBVERSION\n";
		}

	} else {
		my(@d)=dbi_query(-db=>'master',-query=>$query, -debug=>$DEBUG);
		foreach ( @d ) {
			my($sqlpath, $datapath)=dbi_decode_row($_);
			if( $HTML ) {
				push @outdat,"<TR><TD>$srv</TD><TD>$sqlpath</TD><TD>$datapath</TD></TR>\n";
			} else {
				push @outdat,"$srv\t$sqlpath\t$datapath\n";
			}
		}
	}
   	dbi_disconnect();
	print "Completed Work on Server $srv",$nl if defined $DEBUG;
}

__END__

=head1 NAME

mssql_datapath_report.pl - paths for your sql server

=head2 DESCRIPTION

TBD

=head2 USAGE

Usage: mssql_datapath_report.pl --SYSTEMS=system[,system]

=cut

