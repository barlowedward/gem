#!/bin/sh
. /export/home/sybase/SYBASE.sh
cd /apps/sybmon/dist/ADMIN_SCRIPTS/monitoring

dotest() {
	VAL=`grep $1 /tmp/$0.out`
	if [ -n "$VAL" ]
	then
		echo $1 UP
		MlpHeartbeat.pl --MONITOR_PROGRAM=$0 --STATE=OK --SYSTEM=$1 --SUBSYSTEM="HistReporting" --MESSAGE_TEXT="Historical/Monitor Server Batch Ok"
	else
		echo "$1 DOWN"
		MlpHeartbeat.pl --MONITOR_PROGRAM=$0 --STATE=ERROR --SYSTEM=$1 --SUBSYSTEM="HistReporting" --MESSAGE_TEXT="Historical/Monitor Server Batch Not Running"
	fi
}

# rm -f /tmp/$0.out
# isql -Usa -Pabc -SSYBHIST -o/tmp/$0.out << EOF
# hs_list sessions,active
# go
# exit
# EOF

dotest IMAGSYB1_MON
dotest MLPSYB_MON
dotest IMAGSYB2_MON
dotest SSDB_MON
dotest RISKDB_MON
