: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

# ok test the run command library
PASSWORD=

my(%CURRENT_CONFIG);
use strict;
use IO::Socket;
use Posix;
use RunCommand;
use Tk;

$|++;      # Unbuffered output to STDOUT

my($mw)=MainWindow->new(-title=>'Test Box');
$mw->scaling(2) if $mw->scaling() > 2;
my($b)=$mw->Button(-text=>'go', -command=>\&gogogo)->pack;
MainLoop;

sub gogogo {
	#print "GO GO GO\n";
	#print _get_url();
	#return;
my($str)='C:\Perl\bin\perl.exe -IG:/dev/Win32_perl_lib_5_6 -IG:/dev/lib -IG:/dev/plugins G:/dev/ADMIN_SCRIPTS/procs/configure.pl --SERVER=IMAGSYB1DR --USER=sa --PASSWORD=$PASSWORD --TYPE=Sybase --MINVERSION="6.63"';

my($rc,$datptr)=RunCommand(
		-debug=>1,
		-cmd=>$str,
		-mainwindow=>$mw,
#		-statusfunc=>\&myprint,
#		-printfunc =>\&myprint,
		-printhdr  => "[installer] ",
		-showinwin => "Windows Discovery",
		#-debug=>1
	);

foreach (@$datptr) {
	print "RESULTS: $_\n";
}
}

sub myprint { print @_; }

sub _get_url {

    # The parent opens a listen socket on a well known port on the
    # localhost, and then starts a second process to run lwp-request.
    # When the parent receives a connect it reads all the HTML sent
    # by the child.

    use IO::Socket;
    use POSIX;

#    my $url = shift;
    my $port = 9917;
    my($pid, $handle);

    my $server = IO::Socket::INET->new(
        LocalHost => 'localhost',
        LocalPort => $port,
        Proto     => 'tcp',
        Listen    => SOMAXCONN,
        Reuse => 1,
    );
    die "Parent socket open failure: $!" unless defined $server;

    die "fork failure: $!" unless defined($pid = fork);
    
    if ($pid) {		# parent
	$handle = $server->accept;
#	binmode $handle;
	$handle->autoflush(1);

        my( $eof) = 0;
        my $content;
        $mw->fileevent($handle, 'readable' => sub {
	    my($stat, $data);
	    while ($stat = sysread $handle, $data, 4096) {
	        $content .= $data;
	    }
	    die "sysread error:  $!" unless defined $stat;
	    $eof = 1 if $stat == 0;
        });
        $mw->waitVariable(\$eof);
        $mw->fileevent($handle, 'readable' => '');
        close $handle;
        $pid = undef;
        return $content;

    } else {		# child
	$handle = IO::Socket::INET->new(
	    PeerAddr => 'localhost',
	    PeerPort => $port,
	    Proto    => 'tcp',
        );
	die "Child socket open failure: $!" unless defined $handle;
#	binmode $handle;
	$handle->autoflush(1);

#        use LWP::UserAgent;
#        my $ua = LWP::UserAgent->new;
#        $ua->timeout(20);
#        my $req_object = HTTP::Request->new('GET' => $url);
#        my $res_object = $ua->request($req_object);
#        die "request failed" if $res_object->is_error;
#        foreach my $response (keys %{$res_object->headers}) {
#            print $handle "$response: ", $res_object->headers->{$response}, "\n";
#        }
#        print $handle "\n";
#        
        #print $handle $res_object->content;
	my($i)=0;
	while($i<100) {
		print $handle $i."\n";
		sleep 1;
	}


	close $handle;
        POSIX::_exit(0);

    } # ifend fork

} # end tcp_socket_fileevent
