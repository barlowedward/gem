#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Std;
use File::Copy;

my($tmpdir)="/tmp";
$tmpdir = "C:/temp"     unless -d $tmpdir;
$tmpdir = "D:/temp"     unless -d $tmpdir;
$tmpdir = "C:/tmp"      unless -d $tmpdir;
print "TEMP DIR=$tmpdir \n";

# opt_F is file opt_D is directory
use vars qw($opt_F $opt_D $opt_r);
die "usage: $0 [-F FILE -D DIR -r] : -r for recursive"
		unless getopts('F:D:r');

if( defined $opt_F ) {
        print "Operating on File $opt_F\n";
        die "File Not Found\n" unless -f $opt_F;
        process_file($opt_F);
} elsif( defined $opt_D ) {
        print "Operating on Dir $opt_D\n";
        die "Dir Not Found\n" unless -d $opt_D;
        process_file($opt_D);
} else {
        print "Operating on Current Dir\n";
        process_file(".");
}

sub process_file()
{
        my($file)=@_;
        if( -d $file ) {
                opendir(DIR,$file) || die("Can't open directory $file for reading\n");
        my(@dirlist) = grep(!/^\./,readdir(DIR));
        closedir(DIR);
                foreach (@dirlist) {
                        process_file($file."/".$_)
                                if defined $opt_r or ! -d $file."/".$_;
                }
        } elsif( ! -w $file ) {
                print " $file skipped (not writable)\n";
        } elsif( -T $file ) {
                # remove ctrl m from file
                print $file;
                open(DIR,"$file") or die "Cant open $file: $!\n";
                open(OUT,"> $tmpdir/file.$$") or die "Cant open temp file $tmpdir/file.$$: $!\n";
                while( <DIR> ) {
                        chop;
                        s/
+$//;
                        print OUT $_,"\n";
                }
                close DIR;
                close OUT;
                copy("$tmpdir/file.$$",$file)
                        or die "Cant Rename File $tmpdir/file.$$ to $file: $!";
                unlink("$tmpdir/file.$$")
                        or die("cant remove file $tmpdir/file.$$");
                print " cleaned\n";
        } else {
                print "$file: not plain text file\n";
        }
}

__END__

=head1 NAME

clean_ctrl_M.pl - parse off ctrl-M characters

=head2 DESCRIPTION

Filter that removes new lines that can be created when moving DOS-UNIX.  Can
be used to operate on a file, directory (with subdirectories), or stdin.

=head2 USAGE

        cat file | clean_ctrl_M
        clean_ctrl_M -FFile
        clean_ctrl_M -DDirectory

=head2 NOTES

The program is simple.

=cut

