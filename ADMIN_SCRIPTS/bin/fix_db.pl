#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "Usage: fix_db.pl -USA_USER -SSERVER -PSA_PASS -Tsybase|sqlsvr -x
-x debug mode - print queries only dont really fix
-d debug mode

If no server is passed it works on all servers in your password file\n";
        return "\n";
}

use strict;
use Getopt::Std;
use Repository;
use CommonFunc;
use File::Basename;
use Sys::Hostname;
use DBIFunc;

use vars qw($opt_d $opt_U $opt_S $opt_P $opt_x $opt_D $opt_T);
#die usage() if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('U:S:P:dAxD:T:');

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

dbi_msg_exclude("Password correctly set.");
dbi_msg_exclude("Account unlocked.");
dbi_msg_exclude("New login created.");
dbi_msg_exclude("New user added.");
dbi_msg_exclude("A user with the specified login name already exists.");
dbi_msg_exclude("User has been dropped from current database.");
dbi_msg_exclude("Account locked.");
dbi_msg_exclude("Login dropped.");

my($q,@rc);
if( defined $opt_S ) {
	die "Must Pass -U if you pass -S" unless defined $opt_U;
	die "Must Pass -P if you pass -S" unless defined $opt_P;
	fix_database($opt_S,$opt_U,$opt_P);
	exit(0);
}

print "fix_db.pl : Database Configuration Fixer : Run at ".localtime(time)." : On System ".hostname()."\n\n";

print "This output was created in noexecute mode ( the -x flag was passed ).
This will list potential configuration problems but will not actually fix them.
To fix the listed problems, run \'perl $curdir/fix_db.pl\'
GEM will NOT automatically do this - you need to do run it by hand.\n"
	if $opt_x;

my(@serverlist);

if( ! $opt_T or $opt_T eq "sybase" ) {
	@serverlist=get_password(-type=>"sybase",-name=>undef);
	foreach my $server (@serverlist) {
		($opt_U,$opt_P)=get_password(-type=>"sybase",-name=>$server);
		$opt_S=$server;
		fix_database($opt_S,$opt_U,$opt_P);
	}
}

if( is_nt() and (! $opt_T or $opt_T eq "sqlsvr" )) {
      @serverlist=get_password(-type=>"sqlsvr",-name=>undef);
      foreach my $server (@serverlist) {
			($opt_U,$opt_P)=get_password(-type=>"sqlsvr",-name=>$server);
			$opt_S=$server;
			fix_database($opt_S,$opt_U,$opt_P);
		}
}
exit(0);

sub  fix_database {
	my($opt_S, $opt_U, $opt_P)=@_;

	die usage("Must pass sa password\n" )          unless defined $opt_P;
	die usage("Must pass server\n")                unless defined $opt_S;
	die usage("Must pass sa username\n" )          unless defined $opt_U;

	print "\nConnecting To $opt_S\n";

	#
	# CONNECT TO DB
	#
	my($rc)=dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P);
	if( ! $rc ) {
	   warn "Cant connect to $opt_S as $opt_U\n";
	   return;
	}

	# GET DB INFO
	my($server_type, @databaselist) = dbi_parse_opt_D( $opt_D, 1,"Y");
	print "server type=$server_type\ndatabase list=",join(" ",@databaselist),"\n" if $opt_d;

	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
#	die $DBTYPE,"\n",$DBVERSION,"\nPATCH=",$PATCHLEVEL;

	dbi_msg_exclude('Run the CHECKPOINT command in the database that was changed.');
	dbi_msg_exclude('Database option ');
	dbi_msg_exclude('Configuration option changed. ');

	my($SERVERNAME);
	foreach( dbi_query(-db=>"master",	-query=>"select \@\@SERVERNAME")) {
		($SERVERNAME)=dbi_decode_row($_);
	}
 	if( ! $SERVERNAME ) {
 		my($q)="sp_addserver '".$opt_S."',local";
 		print "=>",$q;
 		dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
  }

		  #
        # GET DATABASES WITH DATA & LOG ON SAME DEVICE
        #
        my(%mixdb);
        if( $server_type eq "SYBASE" ) {
	        foreach( dbi_query(-db=>"master",
	        	-query=>"select distinct db_name(dbid) from master..sysusages where segmap&7=7")) {
	        		my($db)=dbi_decode_row($_);
	            $mixdb{$db} = 1;
	        }
        }

        my(%si,%tl);
        foreach( dbi_query(-db=>"master",-query=>"select name,si=status&4,tl=status&8 from master..sysdatabases")) {
                my @dat=dbi_decode_row($_);
                $si{$dat[0]}=1 if $dat[1] == 4;
                $tl{$dat[0]}=1 if $dat[2] == 8;
        }

        my($allow_updates)=0;
        foreach( dbi_query(-db=>"master",-query=>"exec sp_configure 'allow updates'")) {
                my @dat=dbi_decode_row($_);
                $allow_updates=$dat[3];
                $allow_updates=~s/\s//g;
        }
	#print "Allow updates is $allow_updates\n";

        $q='exec sp_configure "allow updates",1';
        #print "=>",$q,"\n";
        dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;

	if( $server_type eq "SYBASE" ) {
	        $q="select count(*) from sysremotelogins where suser_name(suid) is null";
	        foreach ( dbi_query(-db=>"master",-query=>$q) ) {
	        	my($num)=dbi_decode_row($_);
	        	if( $num != 0 ) {
	                	$q="delete sysremotelogins where suser_name(suid) is null\n";
	                	print "=>",$q;
	                	dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
	                }
	        }
        }

	if( $server_type eq "SYBASE" ) {
	        $q="select count(*) from sysloginroles where suser_name(suid) is null";
	        foreach ( dbi_query(-db=>"master",-query=>$q) ) {
	        	my($num)=dbi_decode_row($_);
	        	if( $num != 0 ) {
	                	$q="delete sysloginroles where suser_name(suid) is null\n";
	                	print "=>",$q;
	                	dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
	        	}
	        }
        }
        $q="select name from syslogins where name='tmp_delete_immediately'";
        my($dodrop)=0;
        foreach ( dbi_query(-db=>'master',-query=>$q) ) {         	$dodrop=1;        	        }
        $q="select name from sysusers where name='tmp_delete_immediately'";
        if( $dodrop ) {
	        foreach my $db (@databaselist) {
	        	next if     $db eq 'model'
	                        or $db eq 'sybsecurity'
	                        or $db eq 'sybsyntax'
	                        or $db eq 'sybsystemprocs'
	                        or $db eq 'master'
	                        or $db eq 'msdb'
	                        or $db eq 'tempdb';
	                 $dodrop=0;
	                 foreach ( dbi_query(-db=>$db,-query=>$q) ) { $dodrop=1; }
	                 if( $dodrop ) {
	                 	print "=> drop user tmp_delete_immediately\n";
	          ##       	dbi_query(-db=>$db,-query=>"drop user tmp_delete_immediately");
	                 }
	        }
	        $q="sp_droplogin tmp_delete_immediately";
	        print "=> $q\n";
	        ##dbi_query(-db=>'master',-query=>$q);

        }

		foreach my $db (@databaselist) {

			# CHECK FOR TROJAN SP__
			if( $server_type eq "SYBASE" and $db ne 'sybsystemprocs' ) {
		   	foreach( dbi_query(-db=>"master",
		        	-query=>"select o1.name from $db..sysobjects o1, sybsystemprocs..sysobjects o2
		        				where o1.name=o2.name and o1.name!='sp_thresholdaction'
		        				and o1.name like 'sp_%' and o1.type='P'")) {

		        		my(@dat)=dbi_decode_row($_);
		        		next if $dat[0]=~/^\s*$/;
                	if( $dat[0] =~ /^sp\_\_/ ) {
             			$q="drop procedure $dat[0]\n";
             			print "=>($db) ",$q;
             			#dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;
                  } else {
                  	print "=>($db) possible trojan horse $dat[0] (not removing)\n";
                  }
		        }
	   	}

			if( $server_type ne "SYBASE" and $db ne 'master' ) {
		   	foreach( dbi_query(-db=>"master",
		        	-query=>"select o1.name from $db..sysobjects o1, master..sysobjects o2
		        				where o1.name=o2.name and o1.name!='sp_thresholdaction'
		        				and o1.name like 'sp_%' and o1.type='P'")) {

		        		my(@dat)=dbi_decode_row($_);
		        		next if $dat[0]=~/^\s*$/;
                	if( $dat[0] =~ /^sp\_\_/ ) {
             			$q="drop procedure $dat[0]\n";
             			print "=>($db) ",$q;
             			#dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;
                  } else {
                  	print "=>($db) possible trojan horse $dat[0] (not removing)\n";
                  }
		        }
	   	}

         next if     $db eq 'model'
               or $db eq 'sybsecurity'
               or $db eq 'sybsyntax'
               or $db eq 'sybsystemprocs'
               or $db eq 'master'
               or $db eq 'msdb'
               or $db eq 'tempdb';

          print "validating server $opt_S database $db\n" if defined $opt_d;

          # ok if mix then must have si and tl else si=tl
          if ( defined $mixdb{$db}
          or ( defined $si{$db} and ! defined $tl{$db} ) ) {

                  if(! defined $tl{$db}) {
                  	print "**************************\n* Warning - db $db Options InAppropriate.  \n* Turn off select/into or turn on truncate log on chkpt\n**************************\n";
                          #$tl{$db}=1;
                          #$q="exec sp_dboption \"$db\",\"trunc log on chkpt\",true\n";
                          #print "=>",$q;
                          #dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
                          #print "=>checkpoint $db\n";
                          #dbi_query(-db=>$db,-query=>"checkpoint") unless defined $opt_x;
                  }

                  #if(! defined $si{$db}) {
                  #        $si{$db}=1;
                  #        $q="exec sp_dboption \"$db\",\"select into/bulkcopy\",true\n";
                  #        print "=>",$q;
                  #        dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
                  #        print "=>checkpoint $db\n";
                  #        dbi_query(-db=>$db,-query=>"checkpoint") unless defined $opt_x;
                  #}
         }

         if( $server_type eq "SQLSERVER" ) {
				$q="select DatabaseProperty('$db','IsAutoUpdateStatistics'),
						 DatabaseProperty('$db','IsAutoCreateStatistics'),
						 DatabaseProperty('$db','IsAutoShrink')";
				#print "$db Running $q\n";
				foreach ( dbi_query(-db=>"master",-query=>$q) ) {
					my(@dat)=dbi_decode_row($_);
					#print "DB $db Returned ",join(",",@dat),"\n";
					if( $dat[0] == 0 ) {
						$q="exec sp_dboption \"$db\",\"auto update statistics\",true\n";
						print "=>",$q;
						dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
				   }
				   if( $dat[1] == 0 ) {
						$q="exec sp_dboption \"$db\",\"auto create statistics\",true\n";
						print "=>",$q;
						dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
				              }
				              if( $dat[2] == 0 ) {
						$q="exec sp_dboption \"$db\",\"autoshrink\",true\n";
						print "=>",$q;
						dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
				              }
				              if( $dat[0] == 0 or $dat[1] == 0 or $dat[2] == 0) {
				              	print "=>checkpoint $db\n";
				              	dbi_query(-db=>$db,-query=>"checkpoint") unless defined $opt_x;
				             	}
				}
         }


         if( $server_type eq "SYBASE" ) {
            $q="select name from sysusers
            	where suser_name(suid) is null and uid!=gid and (suid>0 or (suid=99 and name='sa'))";
            foreach ( dbi_query(-db=>$db,-query=>$q) ) {
            	my($num)=dbi_decode_row($_);
                    if( $num >= 0 ) {
                            foreach (@rc) {
                                    print "-- Deleting User $_\n";
                                    $q="sp_dropuser $_\n";
                                    print "=>",$q;
                                    dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;
                            };
                    }
            }

             $q="select suid from sysalternates where suser_name(suid) is null or suser_name(altsuid) is null ";
                  foreach ( dbi_query(-db=>$db,-query=>$q) ) {
                  	my($num)=dbi_decode_row($_);
                          #print "-- Deleting Alias For Suid $num\n";
                          $q="delete sysalternates where suid=$num\n";
                          print "$db =>",$q;
                          dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;
                  }
          }

          $q="select uid,name,type from sysobjects where uid not in (select uid from sysusers)";
			 if( $PATCHLEVEL =~ /^9/ ) {		# 2005
				$q.=" and uid not in ( select schema_id from sys.schemas )";
			 }
          my($init)="FALSE";
          #my($schemainit)="FALSE";
          my(@dat) = dbi_query(-db=>$db,-query=>$q);
          #my(%schemas);

          foreach ( @dat ) {
#					if( $schemainit eq "FALSE" ) {
#		        			$schemainit="TRUE";
#		        			if( $DBTYPE eq "SQL SERVER" and $DBVERSION>9 ) {
#							foreach( dbi_query(-db=>$db,-query=>"select schema_id from sys.schemas")) {
#		        					my($id)=dbi_decode_row($_);
#					            $schemas{$id} = 1;
#					        	}
#				        	}
#		        	}

               my($u,$nm,$type)=dbi_decode_row($_);
               # 	if( $schemas{$u} ) {
               # 		#print "==> found schema $u\n";
               # 		next;
               # 	}

                  print "[$opt_S,$db]==> found invalid object $nm owned by uid $u\n";

                	if( $init eq "FALSE" ) {

                	  $init="TRUE";
                    $q='exec sp_addlogin "tmp_delete_immediately","xxxyyy"';
                    print "[$opt_S]=>",$q,"\n";
                    dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;

                    $q='exec sp_adduser "tmp_delete_immediately"';
                    print "[$opt_S,$db]=>",$q,"\n";
                    dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;
		            }


                        $q="update sysusers set uid=$u where name=\'tmp_delete_immediately\'";
                        print "=>",$q,"\n";
                        dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;

                        $type=~s/\s//g;
                        if( $type eq "U" ) {
                                $q="drop table tmp_delete_immediately.$nm";
                        } elsif( $type eq "P" ) {
                                $q="drop procedure tmp_delete_immediately.$nm";
                        } elsif( $type eq "R" ) {
                                $q="drop rule tmp_delete_immediately.$nm";
                        } elsif( $type eq "D" ) {
                                $q="drop default tmp_delete_immediately.$nm";
                        } elsif( $type eq "V" ) {
                                $q="drop view tmp_delete_immediately.$nm";
                        }
                        print "=>",$q,"\n";
                        dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;
                };
                if( $init eq "TRUE" ) {
                	$q='exec sp_dropuser "tmp_delete_immediately"';
                        print "=>",$q,"\n";
                        dbi_query(-db=>$db,-query=>$q) unless defined $opt_x;

                        $q='exec sp_droplogin "tmp_delete_immediately"';
                        print "=>",$q,"\n";
                        dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;
                }
        }

		  if( $allow_updates ) {
		   # print "allow updates=$allow_updates\n";
        	print "=> exec sp_configure 'allow updates',0\n";
	        dbi_query(-db=>"master",-query=>"exec sp_configure 'allow updates',0") unless $opt_x;
        }

        $q='exec sp_configure "allow updates",0';
        #print "=>",$q,"\n";
        dbi_query(-db=>"master",-query=>$q) unless defined $opt_x;

        dbi_disconnect();
}

__END__

=head1 NAME

fix_db.pl - fix up your databases

=head2 DESCRIPTION

Fixes up your databases.  Right now it will perform the following operations
which i believe should be non destructive.

 If data and log on same device         -> set truncate log on checkpoint
 If Select Into/Bulk Copy set   -> set truncate log on checkpoint
 Turns off sp_configure allow updates if set
 removes objects  owned by users with no uid
 Deletes  sysusers where there is no suid ( and removes their objects )

I believe these rules are non destructive

=head2 USAGE

        ./fix_db -USA_USER -SSERVER -PSA_PASS

or

        ./fix_db -A

if you have your password file defined to go through all your servers

 -x noexec

=cut
