-- report_key=RESTOREHISTORY
-- report_type=HTML_AGGREGATE
-- run_frequency=daily
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_min_version=
-- server_max_version=
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=This Report Lists When Databases Were Restored
-- report_footer=
-- enabled=Y
-- outfile=Restore_History

select 	A.DB as 'Database',
			LastRestoreDate=B.LastRestoreDate,
			A.DumpFileName,
			backup_size
from
(
select
	dbs.name as [DB],
	rh.restore_date as [RestoreDate],
	bmf.physical_device_name as [DumpFileName],
	bs.backup_size
from master.dbo.sysdatabases as dbs
	inner join msdb.dbo.restorehistory as rh
		on dbs.name = rh.destination_database_name
	inner join msdb.dbo.backupset as bs
		on rh.backup_set_id = bs.backup_set_id
	inner join msdb.dbo.backupmediafamily bmf
		on bs.media_set_id = bmf.media_set_id
where databasepropertyex(dbs.name,'Status') = 'RESTORING'
) as A
inner join
(
	select destination_database_name as DB, max(restore_date) as LastRestoreDate
	from msdb.dbo.restorehistory
	group by destination_database_name
) as B
on A.DB = B.DB and A.[RestoreDate] = B.LastRestoreDate
order by A.DB
