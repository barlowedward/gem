#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use MlpAlarm;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Repository;

use vars qw( $MAXSIZE $DATATOO $TRUNCATEONLY $EXCLUDESYSTEMS $SYSTEMS $DOSHRINK $DATABASE $STDALARM $DUMPTRANNOLOG $BATCHID $DEBUG );

my($VERSION)=1.0;

$|=1;

sub usage
{
	return "Usage: mssql_shrinklogs.pl --TRUNCATEONLY --DATATOO --EXCLUDESYSTEMS=a,b --BATCHID --STDALARM --DEBUG --SYSTEMS=system[,system] --DOSHRINK --DUMPTRANNOLOG --MAXSIZE=MB --DATABASE=DBLIST\n";
}

die usage() if $#ARGV<0;
die "Bad Parameter List $!\n".usage() unless
   GetOptions(
   	"STDALARM"				=> \$STDALARM,
		"SYSTEMS=s"				=> \$SYSTEMS,
		"SYSTEM=s"				=> \$SYSTEMS,
		"SERVER=s"				=> \$SYSTEMS,
		"EXCLUDESYSTEMS=s"	=> \$EXCLUDESYSTEMS,
		"MAXSIZE=s"				=> \$MAXSIZE,
		"TRUNCATEONLY"			=> \$TRUNCATEONLY,
		"DATABASE=s"			=> \$DATABASE,
		"BATCHID=s"				=> \$BATCHID,
		"DEBUG"					=> \$DEBUG,
		"DATATOO"				=> \$DATATOO,
		"DUMPTRANNOLOG"		=> \$DUMPTRANNOLOG,
		"DOSHRINK"				=> \$DOSHRINK);

my(%EXCLUDES);
if( $EXCLUDESYSTEMS ) {
	foreach ( split(/,/,$EXCLUDESYSTEMS) ) {
		$EXCLUDES{$_}=1;
	}
}

cd_home();
my(@srv);
if( $SYSTEMS eq "ALL" ) {
	@srv=get_password(-type=>'sqlsvr');
} else {
	@srv=split(/,/,$SYSTEMS);
}
die "No Systems Defined" if $#srv<0;

$BATCHID="MssqlShrinklogs" unless $BATCHID;
MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor');
print "\nMssql_Shrinklogs.pl started processing at ".localtime(time)."\n";

my($login,$pass,$ctype,$srv,$cmd);
foreach $srv (@srv) {
	next if $EXCLUDES{$srv};
	($login,$pass,$ctype)=get_password(-type=>'sqlsvr',-name=>$srv);
   print "Connecting to $srv at ".localtime(time)."\n";

   my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
	if( ! $rcx ) {
      		print "Cant connect to $srv as $login\n";
		next;
	}

	print "Getting Database List\n" if defined $DEBUG;
	$DATABASE="%" unless $DATABASE;
	my($server_type,@okdb)=dbi_parse_opt_D($DATABASE,1);

#   my(@rc)=dbi_query(-db=>"master",-query=>"select name from master..sysdatabases", -debug=>$DEBUG);
#	foreach ( @rc ) {}
#		my($db)=dbi_decode_row($_);

my(%savings,%todo);	# key=server.db

foreach my $db (@okdb) {
		next if $db eq "master"
			or $db eq "model"
			or $db eq "tempdb"
			or $db eq "msdb"
			or $db eq "Northwind"
			or $db eq "pubs";

		print "Working on Database $db\n" if defined $DEBUG;

		print "Using Database $db\n" if defined $DEBUG;
		dbi_use_database($db);
		print "Getting Files In $db\n" if defined $DEBUG;
		my($query)= "select name,size from sysfiles";
		$query.=" where 1=1";
		$query.=" and status&0x40=0x40" unless $DATATOO;
		$query.=" and size < 127*$MAXSIZE" if $MAXSIZE;
   	my(@fname)=dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG);
   	foreach ( @fname ) {
			my($logfile,$startsize)=dbi_decode_row($_);
			$logfile=~s/\s//g;
			if( $startsize<=128 ) {	# minimum size...
				print "Too Small : DB=$db File=$logfile\n";
				next;
			}

			print localtime(time)." Server=$srv DB=$db File=$logfile\n";

			# run a checkpoint
			my($query)="checkpoint";
			print "$query\n" if defined $DEBUG;
   		my(@rc)=dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG);
   		foreach (@rc) {
				my(@x)=dbi_decode_row($_);
				die join(" ",@x);
			}

			# run a checkpoint
			my($query)="checkpoint";
			print "$query\n" if defined $DEBUG;
   		my(@rc)=dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG);
   		foreach (@rc) {
				my(@x)=dbi_decode_row($_);
				die join(" ",@x);
			}

			if( defined $DUMPTRANNOLOG ) {
				print "Dumping Transaction Log\n";
				my($query)="dump tran $db with no_log";
				print "Reformatting Results\n" if defined $DEBUG;
   			my(@rc)=dbi_reformat_results(dbi_query(-db=>$db,-query=>$query,-print_hdr=>1, -debug=>$DEBUG));
   			foreach (@rc) {
					my(@x)=dbi_decode_row($_);
					print "$srv ->",join(" ",@x),"\n";
				}
			}
			if( defined $DOSHRINK ) {
				my($query);
				if( $TRUNCATEONLY ) {
					$query="dbcc shrinkfile( '".$logfile."',TRUNCATEONLY )";
				} else {
					$query="dbcc shrinkfile( '".$logfile."' )";
				}
   			print "Shrinking: $query\n";
				my(@rc)=dbi_query(-db=>$db,-query=>$query,-print_hdr=>1, -debug=>$DEBUG);
				print "Reformatting Results From Shrinking\n" if defined $DEBUG;
				my($endsize,$bestsize);
				foreach (@rc) {
					my(@c)=dbi_decode_row($_);
					$endsize=$c[2];
					$bestsize=$c[4];
					$startsize=$endsize if $startsize<$endsize;
				}
   			my(@rc2)=dbi_reformat_results(@rc);
   			foreach (@rc2) {
   				print "$srv ->",$_,"\n";
   			}
   			print "$srv -> STARTSIZE=$startsize SAVINGS=",($startsize-$endsize)," pages. FRAGMENTATION=",($endsize-$bestsize)," pages\n";
   			if( defined $savings{$srv.".".$db} ) {
   				$savings{$srv.".".$db} += $startsize-$endsize;
   				$todo{$srv.".".$db}    += $endsize - $bestsize;
   			} else {
   				$savings{$srv.".".$db} = $startsize-$endsize;
   				$todo{$srv.".".$db}    = $endsize - $bestsize;
   			}
			}
		}
	}

	printf( "\nSPACESAVINGS\n%40s %10s %10s\n","SERVER.DB","SAVINGS","EMPTYSPACE" );
	my($sums,$sumt)=(0,0);
	foreach ( sort keys %savings) {
		$sums += $savings{$_};
		$sumt += $todo{$_};
		printf "%40s %10s %10s\n",$_, $savings{$_}, $todo{$_};
	}
	printf "%40s %10s %10s\n","---------", "----------","----------";
	printf("%40s %10s %10s\n","TOTAL", $sums, $sumt);
   dbi_disconnect();
	print "Completed Work on Server $srv at ".localtime(time)."\n";
}

MlpBatchJobEnd();
exit(0);
__END__

=head1 NAME

mssql_shrinklogs.pl - shrink data/log files on sql server

=head2 DESCRIPTION

Shrinks your data/log files on your sql servers.  This is a much faster shrink
than you would get if you were using a shrink database.  The program also allows
you to shrink your data files if you pass --DATATOO.

You may specify all systems from the pc config file or specifiy the systems
to use.  Be careful tho.  This will confuse the tran log dump/load programs
if you pass the --DUMPTRANNOLOG option to truncate the log.

=head2 USAGE

Usage: mssql_shrinklogs.pl --DATATOO --STDALARM --DEBUG --SYSTEMS=system[,system] -
 -DOSHRINK --DUMPTRANNOLOG --MAXSIZE=MB --DATABASE=DBLIST

=head2 OPTIONS

 --DUMPTRANNOLOG : dump tran with nolog prior to shrinking (dangerous?)
 --DOSHRINK      : do the shrink
 --SYSTEMS       : comma separated list of systems.  Otherwise does em all
 --MAXSIZE		  : maximum file size for files to shrink (does not to larger ones)

=cut

