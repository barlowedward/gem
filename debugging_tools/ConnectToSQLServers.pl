#!/usr/local/bin/perl-5.6.1

# THIS WRITES AN ERROR TO THE ALARM DB AND THEN DELETES IT!

use lib qw(/apps/sybmon/dev/lib);

use strict;
use Repository;
use DBIFunc;
use MlpAlarm;
use Sys::Hostname;

foreach my $server ( get_password(-type=>'sqlsvr') ) {
	print "CONNECTING TO $server\n";
	my($login,$password)=get_password(-type=>'sqlsvr',-name=>$server);

	my($rc) = dbi_connect(-srv=>$server, 
		-login=>$login, 
		-password=>$password,
		-die_on_error=>0);
	if( ! $rc ) {
		print "dbi_connect() Failed\n";
		print "Error: Cant Connect To Server -  Marking Connection Failed\nUnable to connect to $server as login $login\nHost=".hostname()."\nPERL=$^X \nSYBASE=$ENV{SYBASE}\nerror number=".$DBI::err."\nerrstr=".$DBI::errstr,"\n";
		print "Unable to connect to server $server: err=".$DBI::err." errstr=".$DBI::errstr,"\n";
		next;
	};
	
	my($typ,@db)=dbi_parse_opt_D(undef,0,0,1);
	print "DATABASES:",join(" ",@db),"\n";
	dbi_disconnect();
}
