#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# GENERIC REPOSITORY RUN
# Copyright (c) 2009-2011 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use Repository;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME."

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

#die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> 1 ,
	DOALL				=> "sqlsvr" ,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;

	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	if( $DBTYPE eq "SQL SERVER" ) {
		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";

	#my($rc)=dbi_connect(-srv=>$opt_S,-login=>'sa',-password=>'xxx');
	#if( ! $rc ) {
	#   warn "Cant connect to $opt_S \n";
	#   return;
	#}

		# GET DB INFO
		my($server_type, @databaselist) = dbi_parse_opt_D( '%', 1,"Y");
		print "server type=$server_type\ndatabase list=",join(" ",@databaselist),"\n" if $DEBUG;

		my %info = get_password_info(-name=>$cursvr, -type=>'sqlsvr');
		my($Is_Production)=0;
		$Is_Production=1 if $info{SERVER_TYPE} eq "PRODUCTION" or $info{SERVER_TYPE} eq "CRITICAL";

		my($n)="exec sp_change_users_login 'Auto_Fix', ";
		foreach my $db (@databaselist) {
			print "DB=> $db\n" if $DEBUG;
			my(@rc)=dbi_query(-db=>$db,-query=>"sp_change_users_login 'Report'");
			foreach (@rc) {
				my($usr,$sid)=dbi_decode_row($_);
				next if $usr eq "dbo";

				my($q)="select * from master..syslogins where name='".$usr."'";
				my(@rcl)=dbi_query(-db=>$db,-query=>$q);
				if( $#rcl < 0 ) {
					printf "%-12s %-35s NO LOGIN %-20s\n", $info{SERVER_TYPE}, $cursvr.".".$db, $usr;
				} else {
					printf "%-12s %-35s FIXING %-s \n", $info{SERVER_TYPE}, $cursvr.".".$db, $usr;
					printf "%12s Executing: $n $usr\n"," ";
					my(@rc2)=dbi_query(-db=>$db,-query=>"$n $usr");
					foreach (@rc) {
						print "   ->",join(" ",dbi_decode_row($_)),"n";
					}
				}
			}
		}

		return;
	} else {
		die "NOT A SQL SERVER?";

		# get current locked/expired status from the repository
		#my($q)="select credential_name, attribute_key, attribute_value
		#	print "(noexec)" if $NOEXEC and $DEBUG;
		#	print $q,"\n" if $DEBUG;
		#	MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
		#  my(@lastresults)=MlpAlarm::_querywithresults($q,1)  unless $NOEXEC
		#  foreach (@lastresults) {
		#		my(@vals) = dbi_decode_row($_);
	}
}

__END__

=head1 NAME

mssql_fix_orphan_users.pl - fix up your databases users

=head1 SYNOPSIS

run sp_change_users_login 'Report' in all your servers

=head2 DESCRIPTION

Fixes orphaned users in your databases. checks for them via

sp_change_users_login 'Report'

and then fixes those ones with logins available via

exec sp_change_users_login 'Auto_Fix',login

=cut
