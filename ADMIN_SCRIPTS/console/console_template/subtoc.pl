use strict;
use File::Basename;
use CGI qw( :standard escapeHTML escape);
my($curdir)=dirname($0);
my($INTERACTIVE);
my($DEBUG);

my($show_timings)=1;			# diagnostics
my($starttime)=time if $show_timings;

my(@timings);
sub do_timings {
	my($line,$lbl)=@_;
	push @timings, $line.":".time().":".$lbl;
}

sub show_timings {
	my($lasttime);
	foreach ( @timings ) {
		my($l,$t,$lbl)=split(/:/,$_,3);
		$lasttime=$starttime unless $lasttime;
		print "   ",$l," ($lbl) => ",($t-$lasttime),br,"\n";
		$lasttime = $t;
	}
	print "   Total = ",(time-$starttime),br,"\n";
}
print "Content-Type: text/html; charset=ISO-8859-1

<HTML><HEAD><TITLE>Server Reports</TITLE></HEAD>
<BODY TEXT=BLACK BGCOLOR=WHITE>";

do_timings(__LINE__,"Startup") if $show_timings;

my(%word1,%word2,%word3);
if( ! -d $curdir ) {

	print "ERROR: No directory<br>";

} elsif( opendir(DIR,$curdir) ) {

	my(@dirlist)=readdir(DIR);
	closedir(DIR);
	do_timings(__LINE__,"Post DirRead") if $show_timings;

	my($txt)='';
	my($filecount)=0;
	foreach (@dirlist) {
		next if /^\./ or /^subtoc.pl/ or /^index.htm$/ or /^readme.htm/ or /^divider.jpg/ or /\.pl$/;
		my($filename)=$_;

		my($dl)=$_;
		$dl =~ s/\.html*$//g;
		my(@words)=split(/\_/,$dl);
		$word1{ $words[0] } =1;
	}

	print start_form();
	my(@w1)=sort keys %word1;
	if( $#w1 >= 0 ) {
		print submit('SELECT SERVER'),"<p>";
		print popup_menu(-name=>'w1', -values=>\@w1),"<p>";
	} else {
		print "No Static Reports Found\n";
	}
	print end_form(),"<hr>";
	do_timings(__LINE__,"Post Drop Down") if $show_timings;

	my($server) = param('w1') || "SQLPROD2";
	$server = "SQLPROD2" if $INTERACTIVE;
	if( defined param('SELECT SERVER') or $INTERACTIVE ) {
		print b("Server: $server\n"),hr,p;

		print br,b("Precompiled Reports"),p;

		foreach (@dirlist) {
			# print "FOUND $_\n",br;
			next if /^\./ or /^subtoc.pl/ or /^index.htm$/
					or /^readme.htm/ or /^divider.jpg/ or /^performance.pl/;
			next unless /^$server/;
			next unless /\.html*$/ or /\.pl$/;
			my($file)=$_;
			$file =~ s/\.html*$//;
			$file =~ s/^${server}\s*//;
			$file = reword($file);
			print "<A TARGET=sub_basefrm HREF=$_>$file</A><br>";
		}
		print p;

		foreach ( @dirlist ) {
			do_timings(__LINE__,"File $_") if $show_timings;
			next if /^\./ or /^subtoc.pl/;
			next unless /\.pl$/;
			if( $_ eq "performance.pl" ) {
				use DBIFunc;
				use MlpAlarm;
				my($q)="select distinct perf_key from PER_summary where system_name='".$server."' order by perf_key";
				print $q,p if $DEBUG;
				my(@rc) = _querywithresults($q,1);
				#use Data::Dumper;
				#print "<pre>";
				#print "RESULTS:";
				#print Dumper \@rc;
				#print "</pre>";
				print br,b("Performance Reports"),p if $#rc>=0;
				foreach (@rc) {
					my(@kys)=dbi_decode_row($_);

					next if $kys[0]=~/index\_usage/i;
					# print "Retrieved ",@kys,br;
					my($title)=reword($kys[0]);
					my($prefix)="(in dev) " unless $title=~/Space Monitor/ or $title=~/Diskspace/;
					print "<A TARGET=sub_basefrm HREF=performance.pl?servername=\"".escape($server).
								"\"&metric=\"".escape($kys[0])."\">",$prefix, $title, "</A><br>";

					if( $kys[0] eq "io_usage" ) {
						print "<A TARGET=sub_basefrm HREF=performance.pl?servername=\"".escape($server).
								"\"&specialk=reads&metric=\"".escape($kys[0])."\">$prefix Io Reads</A><br>";
						print "<A TARGET=sub_basefrm HREF=performance.pl?servername=\"".escape($server).
								"\"&specialk=writes&metric=\"".escape($kys[0])."\">$prefix Io Writes</A><br>";
					}
				}
			} else {
				my($url)=$_;
				$url.= "?servername=".param('w1');
				my($file)=$_;
				$file=~s/\.pl$//;
				$file=reword($file);
				print "<A TARGET=sub_basefrm HREF=$url>$file</A><br>";
			}
		}
	}
	print "<br>",$txt;
} else {
	print "Cant Open Directory $curdir For Listing\n";
}
do_timings(__LINE__,"Done") if $show_timings;

print "</body></html>\n";
exit();

sub reword {
	my($word)=@_;
	my($rc);
	foreach( split(/[\.\_]/,$word) ) {
		$rc .= ucfirst($_)." ";
	}
	$rc =~ s/^\s+//;
	$rc=~s/\s$//;
	return $rc;
}
