echo "RUNNING WIN32 BATCH OF INVENTORY SYSTEM"
hostname

rem perl INV_load_configfiles.pl
echo "#####################"
echo starting INV_dsget_computer.pl
echo "#####################"
perl INV_dsget_computer.pl --BATCH_ID=INV_dsget_computer

echo "#####################"
echo starting INV_database_auditor.pl sqlsvr
echo "#####################"
perl INV_database_auditor.pl --DOALL=sqlsvr --BATCH_ID=INV_database_auditor

echo "#####################"
echo starting INV_database_auditor.pl oracle
echo "#####################"
perl INV_database_auditor.pl --DOALL=oracle --BATCH_ID=INV_database_auditor

echo "#####################"
echo starting INV_win_getsystemusers.pl
echo "#####################"
perl INV_win_getsystemusers.pl --BATCH_ID=INV_win_getsystemusers

echo "#####################"
echo starting INV_dsget.pl to fetch users
echo "#####################"
perl INV_dsget.pl

echo "#####################"
echo starting INV_mapper.pl
echo "#####################"
perl INV_mapper.pl --BATCH_ID=INV_mapper
