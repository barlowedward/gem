#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Repository;
use Sys::Hostname;
use CommonFunc;
use vars qw( $opt_S $opt_d $opt_A $MAXHOPS $MAXTIME);

# Copyright (c) 2005 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

$| = 1;
sub usage {
	return "Usage: build_hosts.pl --SYSTEM=server --DEBUG\n";
}
die usage("Bad Parameter List\n") unless GetOptions(
                "SYSTEM=s"      => \$opt_S,
                "DEBUG"     => \$opt_d );

my(@systemlist);
if( defined $opt_S ) {
	push @systemlist,$opt_S;
} else {
	@systemlist=get_password(-type=>"win32servers");
	push @systemlist,get_password(-type=>"unix");
}

print "build_hosts.pl version 1.0 run on ",hostname(),"\n\n";

my(%nameservers);
my(%ipaddresses);
foreach my $system (sort @systemlist) {
	$ipaddresses{$system}="";
	my $cmd;
	if( is_nt() ) {
		$cmd="nslookup $system";
	} else {
		if( -x "/usr/local/bin/nslookup" ) {
			$cmd="/usr/local/bin/nslookup $system 2>&1";
		} elsif( -x "/usr/sbin/nslookup" ) {
			$cmd="/usr/sbin/nslookup $system 2>&1";
		} else {
			$cmd="nslookup $system 2>&1";
		}
	}
	print "executing $cmd ...\n" if $opt_d;
	my($starttime)=time;
	open(CMD,$cmd." |")
		or die "build_hosts.pl: hostname=",hostname()," Cant Run $cmd\n";
	my(@rc);
	while(<CMD>) {
		chomp;chomp; print ">".$_ if $opt_d;push @rc,$_;
	}
	close(CMD);
	die "host ".hostname()." ".$rc[0] if $#rc==0 and $rc[0]=~/not found/;
	die "host ".hostname()." ".$rc[0] if $#rc==0 and $rc[0]=~/is not recognized as an inter/;

	my($count)=0;
	foreach ( @rc ) {
		chomp;
		if( /^Server:/ ) {
			s/^Server:\s+//;
			$nameservers{$_}=1;
			next;
		}
		if( /^Address:/ ) {
			if( $count==0 ) {
				$count++;
			} else {
				s/^Address:\s+//;
				$ipaddresses{$system}=$_;
			}
		}
	}
}
if( is_nt() ) {
	my($file);
	$file="C:/winnt/system32/drivers/etc/hosts";
	$file="C:/windows/system32/drivers/etc/hosts" unless -r $file;
	if( -r $file ) {
		open(F, "C:/winnt/system32/drivers/etc/hosts" )
				or die "Cant open hosts file\n";
		while( <F> ) {
			chomp;
			chomp;
			print $_,"\n" if /^#/;
		}
		print "\n127.0.0.1	     localhost\n\n";
	}
}
foreach ( sort keys %ipaddresses ) {
	if( $ipaddresses{$_} eq "" ) {
		warn "Cant find ip address for $_\n";
		printf "### NO IP ADDRESS FOUND FOR %s\n", $_;
		next;
	}

	printf "%-20s %s\n", $ipaddresses{$_},$_;
}
print "\n" if is_nt();
exit(0);
