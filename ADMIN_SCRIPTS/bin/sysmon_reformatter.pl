#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# ==========================
# REFORMATS A DIRECTORY OF SYSMON OUTPUT TO CSV FILE FOR PLOTTING
# ==========================

use     strict;
use     Getopt::Std;
use     File::Basename;

use vars qw($opt_D $opt_o);

sub usage
{
	print @_ if $#_>=0;
   print "sysmon_reformatter.pl -Ddir -ooutfile

Reads all sysmon output files and condenses them into csv file

\n";

	exit();
};

usage() if $#ARGV<0;
usage( "Incorrect Usage" ) unless getopts('D:o:');

usage("$D is not a directory") unless -d $opt_D;
usage("Must pass -D and -o options") unless defined $opt_D and defined $opt_o;


opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
my(@files)=readdir(DIR);
closedir(DIR);

foreach (@files) {
	next if /^\./;
	read_file($_);
}

open(OUTFILE,"> $opt_o") or die "Cant write to $opt_o\n";
close(OUTFILE);

sub read_file
{
	my($file)=@_;
	open(IN,$file) or die "Cant read $file";
	my($rundate,$runtime);
	while( <IN> ) {
		if( ! defined $rundate ) {
			next unless /^Run Date:/;
			s/^Run Date:\s*//;
			s/\s+$//;
			$rundate=$_;
		}
		if( ! defined $runtime ) {
			next unless /^Statistics Sampled at:/;
			s/^Statistics Sampled at:\s*//;
			s/\s+$//;
			$runtime=$_;
		}
	}
	close(IN);
}

