#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# generic cd to the right directory
use File::Basename;
my($curdir)=dirname($0);

my($usage)="$0 [-dx] -USA_USER -SSERVER -PSA_PASS -DDB\n-d debug mode\n-x remove stuff only\n\nnote database can be a wild card \n";

use strict;
use Getopt::Std;
use Sybase::SybFunc;

use vars qw($opt_d $opt_D $opt_P $opt_x $opt_U $opt_S);
die $usage unless getopts('dP:D:xU:S:');
die $usage if ! defined $opt_D and ! defined $opt_P and ! defined $opt_S and ! defined $opt_U;

# IF ONLY HAVE opt_S get login & password from password file
if( defined $opt_S and ! defined $opt_U ) {
        use Repository;
   ($opt_U,$opt_P)=get_password(-type=>"sybase",-name=>$opt_S);
}

$opt_D="%"
        unless defined $opt_D;
die "Must pass password\n"
        unless defined $opt_P;
die "Must pass server\n"
        unless defined $opt_S;
die "Must pass username\n"
        unless defined $opt_U;

use vars qw($DEBUG);
$DEBUG=1 if defined $opt_d;

#
# CONNECT TO DB
#
db_connect($opt_S,$opt_U,$opt_P)
        or die "Cant connect to $opt_S as $opt_U\n";

# GET DB INFO
my(@databaselist)=db_query("master","select name from sysdatabases where name like \"$opt_D\"  and status & 0x1120 = 0 and status2&0x0030=0");

my($db, @procs,@tbls,@views);

foreach $db (@databaselist) {
        die "Cant Use Database $db\n" unless db_use_database($db);
        my(@uname)=db_query($db,"select name from sysusers where uid=gid and name=\"super_user_group\"");
        if( $#uname >= 0 ) {
                printf( "%30s:\t",$db );
                my(@rc)=db_query($db,"exec sp_dropgroup super_user_group");
                print "\n";
                foreach (@rc) { print "MSG>",$_,"\n"; }
        }
        @uname=db_query($db,"select name from sysusers where uid=gid and name=\"read_only_group\"");
        if( $#uname >= 0 ) {
                printf( "%30s:\t",$db );
                my(@rc)=db_query($db,"exec sp_dropgroup read_only_group");
                print "\n";
                foreach (@rc) { print "MSG>",$_,"\n"; }
        }
}

exit if defined $opt_x;

foreach $db (@databaselist) {
        next if $db eq "tempdb" or $db eq "sybsystemprocs" or $db eq "master"
                or $db eq "model" or $db eq "sybsecurity";
        die "Cant Use Database $db\n" unless db_use_database($db);

        printf( "%30s:\t",$db );
        db_query($db,"exec sp_addgroup super_user_group");
        db_query($db,"exec sp_addgroup read_only_group");
        print "\n";

        @procs=db_query($db,"select name from sysobjects where type=\"P\" and uid=1");
        print "PROCS: " if defined $DEBUG;
        foreach (@procs) {
                db_query($db, "grant execute on $_ to super_user_group\n");
                print "." if defined $DEBUG;
        }
        print "\n" if defined $DEBUG;

        print "TABLES: " if defined $DEBUG;
        @tbls =db_query($db,"select name from sysobjects where type=\"U\" and uid=1");
        foreach (@tbls) {
                db_query($db, "grant all on $_ to super_user_group\n");
                db_query($db, "grant select on $_ to read_only_group\n");
                print "." if defined $DEBUG;
        }
        print "\n" if defined $DEBUG;

        print "VIEWS: " if defined $DEBUG;
        @views =db_query($db,"select name from sysobjects where type=\"V\" and uid=1");
        foreach (@views) {
                db_query($db, "grant all on $_ to super_user_group\n");
                db_query($db, "grant select on $_ to read_only_group\n");
                print "." if defined $DEBUG;
        }
        print "\n" if defined $DEBUG;
        printf( "%30s:\t",$db );
        print $#views+$#tbls+$#procs+3," Objects Permissions Modified\n"
                unless defined $DEBUG;
}

db_close_sybase();

__END__

=head1 NAME

create_dflt_groups.pl - create read_only_group and super_user_group in your server

=head2 DESCRIPTION

This very simple perl script creates a group named super_user_group
and a group read_only_group in all your databases in a server.  The
super_user_group is granted select,insert,update,delete on all
tables and views, execute on all procs.  The read_only_group is
granted select on tables only.

It is kind of nice to have these groups around and then you can manage
who has access to what based on these things.  Of course, if you change
ddl (drop/add stuff) then the permissions will be lost and you will need
to add them back by rerunning this proc.

=head2 USAGE

        create_dflt_groups.pl [-dx] -USA_USER -SSERVER -PSA_PASS -DDB
        -d debug mode
        -x remove stuff only

note database can be a wild card

=cut

