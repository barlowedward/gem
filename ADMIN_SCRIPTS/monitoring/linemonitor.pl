#!/apps/perl/linux/perl-5.8.2/bin/perl

#
# Based On Generic Log File Filteration Script
#
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use 		strict;
require 	5.002;
use     	strict;
use     	File::Basename;
use 		Getopt::Long;
use		CommonFunc;
use 		MlpAlarm;
use CommonHeader;

my($VERSION)=1.0;
my($PROGNAME)="linemonitor.pl";

use vars qw( $DEBUG $NOLOG );

my($headercount)=20;

sub usage
{
	return "Usage: $PROGNAME --DEBUG --NOLOG\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
		"NOLOG" 		=> \$NOLOG,
		"DEBUG"		=> \$DEBUG );

$|=1;

my($cwd)=cd_home();
my($rc)=is_up();
if( $rc ) {
   print "ERROR: $0 is allready running\n"
      if is_interactive();
   exit(0);
}
mark_up();

print $PROGNAME," version $VERSION\n";

MlpBatchJobStart(-BATCH_ID=>$PROGNAME,-AGENT_TYPE=>'Gem Monitor') unless defined $NOLOG;

my($command)="/home/wombat/bin/mdrvlisten.sh -s MONITOR.ALLSTATUS |";
open(INPUT,$command ) or die "Cant Run Command $command \n";
my($lineno,$batchno)=(0,0);

my(%curitem);
while(<INPUT>) {
	chomp;
	$lineno++;
	if( $lineno%100==0 ){
		# MlpBatchJobStep() unless defined $NOLOG;
		mark_up();
	}
	print $_,"\n" if defined $DEBUG;
	if( /^\s/ ) {
		my(@d)=split(/\|/);
		$d[0]=~s/\s//g;
		$d[3]=~s/\s//g;
		$curitem{$d[0]}=$d[3];
	} else {
		next unless /^MONITOR/;
		next unless defined $curitem{wSeqNum};
		$batchno++;
		my($state);
		if( $curitem{wFeedMhMode}=~/normal/i or $curitem{wLineStatus}=~/up/i ) {
			$state="OK";
		} else {
			$state="ERROR";
		}
		# } elsif( $curitem{wFeedMhMode}=~/down/i or $curitem{wLineStatus}=~/down/i ) {
		# } else {
			# die "Unknown value ($curitem{wFeedMhMode} / $curitem{wLineStatus})";
		printf( "%24s %20s %6s %20s\n","System","Subsystem","State","Host")
			if $batchno%$headercount==1;

		printf( "%24s %20s %6s %20s\n",$curitem{wFeedId},$curitem{wFeedFtMode},$state,$curitem{wFeedHostName});
		MlpHeartbeat(
			-monitor_program=> $PROGNAME,
			-system	=> $curitem{wFeedId},
			-subsystem	=> $curitem{wFeedFtMode},
			-state   => $state,
			-message_text => "Host=$curitem{wFeedHostName} FeedMhMode=$curitem{wFeedMhMode} LineStatus=$curitem{wLineStatus}"
		);
		%curitem = ();
	}
}

close(INPUT);
MlpBatchJobEnd();
