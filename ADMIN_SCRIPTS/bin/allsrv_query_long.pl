#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2003 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Basename;
use DBIFunc;
use CommonFunc;
use Getopt::Long;
use Repository;

use vars qw( $QUERY $QUERYFILE $COLUMNS $TYPE $DEBUG $HTML $HEADER $SHOWQUERIES $SHOWSERVER $PRINT_HDR $SHOWALLSERVERS );
$|=1;

sub usage
{
        print @_;
        print "Usage: allsqlsrv_query_long.pl --QUERY=Query --COLUMNS=1,3,5 --SHOWALLSERVERS --SHOWQUERIES --PRINT_HDR --TYPE=SQLSVR|SYBASE --DEBUG --HTML --HEADER=col1,col2 --SHOWSERVER

Generic program to run a query on all servers.  If you type
        \"$0 Query\"
you will end up executing
        Query
on all your servers from the master database.
\n";

        return "\n";
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
   GetOptions( "HEADER=s"=>\$HEADER,"COLUMNS=s"=>\$COLUMNS,"TYPE=s"=>\$TYPE,
   	"SHOWQUERIES"	=>	\$SHOWQUERIES,
   	"QUERY=s"		=>	\$QUERY,
   	"QUERYFILE=s"		=>	\$QUERYFILE,
   	"DEBUG"			=>	\$DEBUG,
   	"SHOWSERVER"	=>	\$SHOWSERVER,
   	"SHOWALLSERVERS"	=>	\$SHOWALLSERVERS,
   	"PRINT_HDR"		=>	\$PRINT_HDR,
   	"HTML"			=>	\$HTML );

die usage("Must pass --QUERY") unless defined $QUERY or $QUERYFILE;

#$TYPE="SYBASE" unless defined $TYPE;
$TYPE=uc($TYPE);
die "TYPE must be SQLSVR or SYBASE" if $TYPE and $TYPE ne "SQLSVR" and $TYPE ne "SYBASE";

my(%cols);
if( defined $COLUMNS ) {
	$COLUMNS=~s/\s//g;
	foreach(split(/,/,$COLUMNS)){ $cols{$_}=1; }
}

print "QueryFile is $QUERYFILE\n" if $QUERYFILE;
if( $QUERYFILE ) {
	open(FILE,$QUERYFILE) or die "CANT OPEN $QUERYFILE $!\n";
	while(<FILE>) {
		chomp;
		$QUERY.=$_."\n";
	}
	close(FILE);
}

print "Query is $QUERY\n" if $QUERY;
print "HTML is defined\n" if defined $HTML;
print "HTML is not defined\n" unless defined $HTML;

cd_home();

my(@srv,%loginh,%passh);
if( ! $TYPE or $TYPE eq "SYBASE" ){
	foreach ( get_password(-type=>"sybase",-name=>undef) ) {
		push @srv,$_;
		($loginh{$_},$passh{$_})= get_password(-type=>"sybase",-name=>$_);
	}
}

if( is_nt() ) {
	if( ! $TYPE or $TYPE eq "SQLSVR" ){
		foreach ( get_password(-type=>"sqlsvr",-name=>undef) ) {
			push @srv,$_;
			($loginh{$_},$passh{$_})= get_password(-type=>"sqlsvr",-name=>$_);
		}
	}
}

my($login,$pass,$srv,$cmd);
print "<TABLE BORDER=1>\n" if defined $HTML;
if( defined $HEADER ) {
	print "<TR>" if defined $HTML;
	foreach (split(/,/,$HEADER) ) {
		if( defined $HTML ) {
			print "<TH>$_</TH>" if defined $HTML;
		} else {
			print "$_ ";
		}
	}
	print "</TR>" if defined $HTML;
}

my($query_cols)=-1;
foreach $srv (@srv) {
   $login=$loginh{$srv};
   $pass =$passh{$srv};
   print "Connecting to $srv\n" if $DEBUG;
   #my($rc)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
   my($rc)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass);
   if( ! $rc ){
   	 print "********************************\n";
       print "* Cant connect to $srv as $login\n";
       print "********************************\n";
       next;
     }

	if( $SHOWQUERIES ) {
		print "<TR><TD COLSPAN=20>" 	if $HTML;
		print "srv=$srv qry=$QUERY\n";
		print "</TD></TR>" 				if $HTML;
	}

	print "SRV=$srv SHOWSERVER=$SHOWSERVER QUERY=$QUERY \n" if $DEBUG;
   my(@rc)=dbi_query(-db=>"master",-query=>$QUERY, -print_hdr=>$PRINT_HDR);
   if( $#rc < 0 and $SHOWALLSERVERS ) {
   	my(@z);
   	for (1..$query_cols) { push @z,""; }
   	push @rc, dbi_encode_row(@z);
   }

   foreach my $row (@rc) {
    	my(@x)=dbi_decode_row($row);
    	$query_cols = $#x  unless $query_cols;

		print "$#x COLUMNS RETURNED\n" if defined $DEBUG;
      if( defined $COLUMNS ) {
			my($colid)=0;
		   # print "->",$x[0],$x[1],$x[2],"\n";
			print "<TR>" if defined $HTML;

			if( $SHOWSERVER ) {
				die "OOPS";
				if( $HTML ) {
					print "<TD>$srv</TD>\n";
				} else {
					print "SHOWSERVER\n";
					printf "%-14s", $srv;
				}

			}
			foreach my $coldat (@x) {
				if( defined $cols{$colid} ) {
					$coldat=~s/\s/-/g;
					if( defined $DEBUG ) {
						print "\tCOL $colid=$coldat\n" ;
					} elsif( defined $HTML ) {
						print "   <TD>",$coldat,"</TD>\n";
					} else {
						print "$coldat ";
					}
				}
				$colid++;
			}
			print "</TR>" if defined $HTML;
			print "\n";
      } else {
      	print "<TR>" if $HTML;
      	print "->" unless $HTML;

      	if( $SHOWSERVER ) {
				if( $HTML ) {
					print "<TD>$srv</TD>\n";
				} else {
					printf "%-14s", $srv;
				}
			}

      	if( $HTML ) {
      		print "<TD>",join("</TD>\n\t<TD>",@x),"</TD></TR>\n";
      	} else {
      		print join(" ",@x),"\n";
      	}
      }
   }

   dbi_disconnect();
}
print "</TABLE>\n" if defined $HTML;

__END__

=head1 NAME

allsrv_query_long.pl - loop through your servers and run a query

=head1 SYNOPSIS

$ perl allsrv_query_long.pl

Usage: allsqlsrv_query_long.pl --QUERY=Query --COLUMNS=1,3,5 --SHOWQUERIES --PRINT_HDR --TYPE=SQLSVR|SYBASE --DEBUG --HTML --HEADER=col1,col2 --SHOWSERVER

Generic program to run a query on all servers.  If you type
        "allsrv_query_long.pl Query"
you will end up executing
        Query
on all your servers from the master database.

=head2 DESCRIPTION

This is the long arguments version of allsrv_query.pl (and should be used in preference).  This program loops through all
sybase and/or sql servers in your config files and runs a single query in them - formatting output appropriately.  You can
control the columns to show, whether its html output etc...

This is quite useful to get administrative values (like procedure cache size) from your servers or to audit them

=cut
