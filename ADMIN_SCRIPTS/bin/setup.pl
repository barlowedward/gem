#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
require 5.004;
use File::Compare;
use File::Copy;

# SETS UP YOUR SYSTEM
#  - copy .pm from ../lib to first spot in include list if different
#  - copy .pm from . if different

opendir(DIR,'.') || croak("Can't open directory . for reading\n");
my %LibModules;
foreach (grep(!/^\./,readdir(DIR))) {
        next unless /\.pm$/;
        $LibModules{$_}=".";
}
closedir(DIR);

if( -d "../lib" ) {
        opendir(DIR,'../lib') || croak("Can't open directory ../lib for reading\n");
        foreach (grep(!/^\./,readdir(DIR))) {
                next unless /\.pm$/;
                next if defined $LibModules{$_} and compare("../lib/".$_,"./$_")==0;
                $LibModules{$_}="../lib";
        }
        closedir(DIR);
}

my($outdir)=$INC[0];
print "OUTDIR = $outdir\n";

foreach ( keys %LibModules ) {
        print $_," ";

        if( compare($LibModules{$_}."/".$_,"$outdir/$_")==0 ) {
                print "OK\n";
        } else {
                print "NOT OK\n";
        }
}
