#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Long;
use DBIFunc;
use MlpAlarm;
use Inventory;

use vars qw( $FROMDB $FROMTBL $DEBUG $ORDERBY $NORMALEXCLUDE );

my($VERSION)=1.0;

$|=1;

sub usage { 	return "usage: copy_syslogins.pl -FROMTBL=tbl -FROMDB=db --ORDERBY=orderby --NORMALEXCLUDE\n";  }

die usage() if $#ARGV<0;
die "Bad Parameter List $!\n".usage() unless GetOptions(  "FROMTBL=s"	=> \$FROMTBL,
	"ORDERBY=s" => \$ORDERBY,
	"NORMALEXCLUDE" => \$NORMALEXCLUDE,
	"FROMDB=s"	=> \$FROMDB, "DEBUG"=>\$DEBUG);
die "NO FROMDB" unless $FROMDB;
die "NO FROMTBL" unless $FROMTBL;

my(@colname);
foreach  ( _querywithresults( "exec sp__helpcolumn $FROMTBL", undef, $FROMDB) ) {
		my(@rc)=dbi_decode_row($_);
		#print join("|",@rc),"\n";
		$rc[0]=~s/\s+$//;
		push @colname,$rc[0] unless $rc[-1]==1;
}
unshift @colname;

_querynoresults("delete INV_system where system_type='DCM'");
_querynoresults("delete INV_system_attribute where system_type='DCM'");

my($count)=0;
my(%unique_lc_machnames);
foreach  ( _querywithresults( "select * from $FROMTBL $ORDERBY", undef, $FROMDB) ) {
		my($nm,@rc)=dbi_decode_row($_);
		next if $nm eq "Name" or $nm =~ /^\s*$/; 	# cleanup stupid crappy data
		if( $NORMALEXCLUDE ) {
			if( $nm =~ /^wire manager\s*$/i
						or $nm=~/^clear cube\s*$/i
						or $nm=~/^patch panel\s*$/i
						or $nm=~/^fiber panel\s*$/i
						or $nm=~/^tie panel\s*$/i
						or $nm=~/^telco\s*$/i
						or $nm=~/^panel\s*$/i
						or $nm=~/^empty\s*$/i
						or $nm=~/^not available\s*$/i
						or $nm=~/^lan drop/i
						or $nm=~/^ipatch/i
						or $nm=~/^shelf/i
						or $nm=~/\schasis$/i
						or $nm=~/^spare\s*$/i
						or $nm=~/^power\s*$/i
						or $nm=~/^sentry\s*$/i
						or $nm=~/^storage\s*$/i
						or $nm=~/^no name\s*$/i
						or $nm=~/\-bc\-[\d\.]+\-[\d\.]+$/i
						or $nm=~/\-bc\-[\d\.]+\-[\d\.]+\-[\d\.]+$/i ) {
							print "\tSkipping $nm\n" if $DEBUG;
							next;
					}
		}

		print join("|",@rc),"\n" if $DEBUG;
		if( $unique_lc_machnames{lc($nm)} ) {
			$nm=$nm." ".$rc[12];
			print "\tNew Name = $nm\n";
		} else {
			$unique_lc_machnames{lc($nm)}=1;
		}

		my(%attributes);
		my($i)=0;
		foreach (@rc) {
			s/\s+$//;
			if( $_ ne "" ) {
				print "Set host=$nm key=$colname[$i] value=$_\n" if $DEBUG;
				$attributes{$colname[$i]} = $_;
			}
			$i++;
		}
		print "Running InvUpdSystem( -system_name => $nm )\n" if $DEBUG;
		InvUpdSystem(
				-system_name => $nm,
				-system_type=>'DCM',
				-clearattributes=>1,		# UNNEEDED
				-attribute_category=>'DCM',
				-attributes => \%attributes );
		$count++;
	   print "$count rows inserted" if $count%500==0;
}

