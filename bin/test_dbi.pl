#
# standard plugin header
#
use strict;
use Getopt::Long;
use vars qw( $DEBUG $NO_ORACLE $NO_SYBASE $NO_SQLSVR);
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless
	GetOptions(  	"DEBUG"			=> \$DEBUG,
					   "NO_SYBASE"		=> \$NO_SYBASE,
						"NO_SQLSVR"		=> \$NO_SQLSVR,
						"NO_ORACLE"		=> \$NO_ORACLE   
   );   

my(@errors);
#
# end standard header
#

use DBI;
use Socket;			 # needed for gethostbyaddr
use Repository;
$|=1;

if( is_nt() ) {
	print "TESTING YOUR PERL ODBC CONFIGURATION!\n";
} else {
	print "TESTING YOUR PERL DBI CONFIGURATION!\n";
}

my(@sybase_servers)=get_password(-type=>"sybase") unless $NO_SYBASE;
my(@sqlsvr_servers)=get_password(-type=>"sqlsvr") unless $NO_SQLSVR or ! is_nt();
my(@oracle_servers)=get_password(-type=>"oracle") unless $NO_ORACLE;

my($IF_FILE);
my(%avail_drivers);
my($num_errors)=0;
foreach ( DBI->available_drivers() ) { $avail_drivers{$_}=1; print "\tDriver $_ Available\n"; }

if( ! is_nt() and ! defined $avail_drivers{Oracle} and $#oracle_servers>=0 ) {
	print "ERROR: DBD::Oracle Driver Unavailable\n";
	$#oracle_servers=-1;
	$num_errors++;
}
if( ! is_nt() and ! defined $avail_drivers{Sybase} and $#sybase_servers>=0 ) {
	print "ERROR: DBD::Sybase Driver Unavailable\n";
	$#sybase_servers=-1;
	$num_errors++;
}
if( is_nt() and ! defined $avail_drivers{ODBC}) {
	print "ERROR: Oracle DBD::ODBC Driver Unavailable\n";
	exit(0);
}

my(@warnings);
if( $#sybase_servers>=0 ) {
	print "Testing Sybase\n" if $DEBUG;
	if( ! defined $ENV{SYBASE} ) {
		print "ERROR: SYBASE Environment variable \$SYBASE is not set\n";
		$num_errors++;
	} elsif( ! -d $ENV{SYBASE} ) {
		print "ERROR: SYBASE DIRECTORY (\$SYBASE) is not a valid directory\n";
		$num_errors++;
	} else {
	        $IF_FILE	= "$ENV{SYBASE}/interfaces"	if -r "$ENV{SYBASE}/interfaces";
	        $IF_FILE	= "$ENV{SYBASE}/ini/sql.ini"	if -r "$ENV{SYBASE}/ini/sql.ini";

	        if( ! -r $IF_FILE ) {
	        	print "ERROR: SYBASE Interfaces file does not exist\n";
	        	$num_errors++;
	        }

		if( ! is_nt() ) {
			my($found_svr_ptr)=dbi_get_interfaces($IF_FILE);
			foreach (@sybase_servers) {
				next if $$found_svr_ptr{$_};
				print "WARNING Registered Sybase Server $_ Not In Interfaces File\n";
			}
		}
	}
}

if( is_nt()) {
    my(%dsn_list);
    eval {  DBI->data_sources("ODBC");	};
    if( $@ ) {
	     	print "ERROR: can not load DBD::ODBC : $@\n";
        	exit(1);
    } else {
     	my(@dat)= grep(s/DBI:ODBC://i,DBI->data_sources("ODBC"));
     	foreach (@dat) { $dsn_list{$_}=1;    	}
     	foreach( @sybase_servers,@sqlsvr_servers,@oracle_servers) {
     		next if  $dsn_list{$_};
     		print "ERROR: Server $_ Is Registered But Does Not Have an ODBC Listing\n";
     		$num_errors++;		
		}
   }
}

if( $num_errors ) {
	print "ERROR: DBI Setp tests generated $num_errors error messages\n";
} else {
	print "SUCCESSFUL: DBI tests generated no errors\n";
}
exit($num_errors);

#
## ok now connect to them all!!!
#foreach my $server ( @sybase_servers ) {
#	my($dsn);
#	if( is_nt() ) {
#		next unless $avail_drivers{ODBC};
#		$dsn="dbi:ODBC:$server";
#	} else {
#		next unless $avail_drivers{Sybase};
#		$dsn="dbi:Sybase:$server";
#	}
#	my($login,$passwd,$conntype)=get_password(-name=>$server,-type=>"sybase");
#	if( ! defined $login or ! defined $passwd ) {
#		push @warnings, "No Login/Password Found For $server";
#		next;
#	}
#        my($dbh)= DBI->connect($dsn,$login,$passwd,{RaiseError=>0,PrintError=>0,AutoCommit=>1});
#        if( defined $dbh ) {
#        	print "CONNECTED to $server\n"  if $DEBUG;
#        	my($ary_ref)=$dbh->selectall_arrayref("select count(*) from master..sysobjects where name='sysprocesses'");
#        	my($ok)=0;
#        	foreach my $row (@$ary_ref) {
#        		$ok++ if $$row[0] == 1;
#        	}
#        	if( $ok == 0 ) {
#        		push @warnings, "Connect ok but cant select from catalog?";
#        		foreach my $row (@$ary_ref) {
#        			push @warnings, "--->",join(" ",@$row)," \n";
#        		}
#        	} else {
#        		print "Testing Completed Successfully\n" if $DEBUG;
#        	}
#        	$dbh->disconnect();
#        } else {
#        	print "FAILED TO CONNECT to $server\n" if $DEBUG;
#        	push @warnings, "FAILED TO CONNECT to $server";
#        }
#}
#
#if( is_nt() ) {
#foreach my $server ( @sqlsvr_servers ) {
#	my($dsn);
#	next unless $avail_drivers{ODBC};
#	$dsn="dbi:ODBC:$server";
#	my($login,$passwd,$conntype)=get_password(-name=>$server,-type=>"sqlsvr");
#	if( ! defined $login or ! defined $passwd ) {
#		push @warnings, "No Login/Password Found For $server";
#		next;
#	}
#        my($dbh)= DBI->connect($dsn,$login,$passwd,{RaiseError=>0,PrintError=>0,AutoCommit=>1});
#        if( defined $dbh ) {
#        	print "CONNECTED to $server\n" if $DEBUG;
#        	my($ary_ref)=$dbh->selectall_arrayref("select count(*) from master..sysobjects where name='sysprocesses'");
#        	my($ok)=0;
#        	foreach my $row (@$ary_ref) {
#        		$ok++ if $$row[0] == 1;
#        	}
#        	if( $ok == 0 ) {
#        		push @warnings, "Connect ok but cant select from catalog?";
#        		foreach my $row (@$ary_ref) {
#        			push @warnings, "--->",join(" ",@$row)," \n";
#        		}
#        	} else {
#        		print "Testing Completed Successfully\n" if $DEBUG;
#        	}
#
#	        $dbh->disconnect();
#        } else {
#        	print "FAILED TO CONNECT to $server\n" if $DEBUG;
#        	push @warnings, "FAILED TO CONNECT to $server";
#        }
#}
#
#foreach my $server ( @oracle_servers ) {
#	my($dsn);
#	next unless $avail_drivers{ODBC};
#	$dsn="dbi:ODBC:$server";
#	my($login,$passwd,$conntype)=get_password(-name=>$server,-type=>"oracle");
#	if( ! defined $login or ! defined $passwd ) {
#		push @warnings, "No Login/Password Found For $server";
#		next;
#	}
#        my($dbh)= DBI->connect($dsn,$login,$passwd,{RaiseError=>0,PrintError=>0,AutoCommit=>1});
#        if( defined $dbh ) {
#        	print "CONNECTED to $server\n" if $DEBUG;
#	        $dbh->disconnect();
#        } else {
#        	print "FAILED TO CONNECT to $server\n" if $DEBUG;
#        	push @warnings, "FAILED TO CONNECT to $server";
#        }
#}
#}
#
#foreach (@warnings) { chomp; print "ERROR: $_\n"; }
#exit( $#warnings+1);

sub error_out {
	my($str)=join("",@_);
	$str=~s/\n/\n* /;
	die "********************************************\n* ",$str,"********************************************\n";
}

############################# SUBROUTINES ###########################
sub is_nt {
	my($is_nt)=0;
	$is_nt=1 if defined $ENV{PROCESSOR_LEVEL};
	return $is_nt;
}

my(%found_name);
sub dbi_get_interfaces
{
	my($filename)=@_;
	die "ERROR: SYBASE ENV not defined" unless defined $ENV{"SYBASE"};

	my(@srvlist,%port,%host,%found_svr);

	if( defined $filename and ! -e $filename ) {
		print "Warning: File $filename Not Found\n";
	}

	if( -e $ENV{"SYBASE"}."/interfaces" or -e $filename ) {
		$filename=$ENV{"SYBASE"}."/interfaces" unless defined $filename;

		# UNIX
		open FILE,$filename or die "ERROR: Cant open $filename : $!";
		my($srv,$dummy);
		while( <FILE> ){
			next if /^#/;
			chomp;

			if( /^\s/ ) {
				next unless /query/;
				next if $srv eq "";
				chomp;

				my(@vals)=split;

				# Sun Binary Format
				if( $vals[4] =~ /^\\x/ ) {
					#format \x
					$vals[4] =~ s/^\\x0002//;

					my($p) = hex(substr($vals[4],0,4));
					my($pk_ip) = pack('C4',
						hex(substr($vals[4],4,2)),
						hex(substr($vals[4],6,2)),
						hex(substr($vals[4],8,2)),
						hex(substr($vals[4],10,2)));

					my($name,$aliases,$addrtype,$len,@addrs);
					if( defined $found_name{$pk_ip} ) {
						$name=$found_name{$pk_ip};
					} else {
						($name,$aliases,$addrtype,$len,@addrs) = gethostbyaddr($pk_ip,AF_INET);
						$found_name{$pk_ip}=$name if defined $name and $name !~ /^\s*$/;
					}

					my($h) =$name ||
						hex(substr($vals[4],4,2)).".".
						hex(substr($vals[4],6,2)).".".
						hex(substr($vals[4],8,2)).".".
						hex(substr($vals[4],10,2));

					if( defined $host{$srv}  ) {
						$host{$srv}.="+".$h;
						$port{$srv}.="+".$p;
					} else {
						$host{$srv}=$h;
						$port{$srv}=$p;
					}
				} else {
					$port{$srv} = $vals[4];
					$host{$srv} = $vals[3];
				}

				push @srvlist,$srv unless defined $found_svr{$srv};
				$found_svr{$srv} = 1;
				# $srv="";
			} else {
				($srv,$dummy)=split;
			}
		}
		close FILE;

	} elsif( -e $ENV{"SYBASE"}."/ini/sql.ini"  ) {

		# DOS
		# fix up sybase env vbl
		$ENV{"SYBASE"} =~ s#\\#/#g;
		open FILE,$ENV{"SYBASE"}."/ini/sql.ini"
			or die "ERROR: Cant open file ".$ENV{"SYBASE"}."/ini/sql.ini: $!\n";
		my($cursvr)="";
		while( <FILE> ){
			next if /^#/;
			next if /^;/;
			next if /^\s*$/;
			chop;

	#
	# MUST ALSO HANDLE BIZARROS LIKE THE FOLLOWING
	#
	#[NYSISW0035]
	#$BASE$00=NLMSNMP,\pipe\sybase\query
	#$BASE$01=NLWNSCK,nysisw0035,5000
	#MASTER=$BASE$00;$BASE$01;
	#$BASE$02=NLMSNMP,\pipe\sybase\query
	#$BASE$03=NLWNSCK,nysisw0035,5000
	#QUERY=$BASE$02;$BASE$03;

			if( /^\[/ ) {
				# IT A SERVER LINE
				s/^\[//;
				s/\]\s*$//;
				s/\s//g;
				$cursvr=$_;
				push @srvlist,$_ unless defined $found_svr{$_};
				$found_svr{$_} = 1;
			} else {
				# IT A DATA LINE
				next if /^master=/i;
				next if /QUERY=\$BASE/i;
				next if /\,.pipe/i;
				next if /NLMSNMP/i;
				(undef,$host{$cursvr},$port{$cursvr})=split /,/;
			}
		}
		close FILE;
	} elsif( defined $filename ) {
		die "ERROR: $filename error";
	} else {
		die "ERROR: UNABLE TO DETERMINE INTERACE FILE";
	}
	return \%found_svr;
#	my(@rc);
#	foreach (sort @srvlist ) {
#		if(defined $search_string) {
#			next unless /$search_string/i
#				or $host{$_} =~ /$search_string/i
#				or $port{$_} =~ /$search_string/i;
#		}
#		push @rc,[ $_,$host{$_},$port{$_} ];
#	}
#	return @rc;

}
