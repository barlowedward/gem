#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#DOESNT WORK RIGHT - NEEDS TO RUN ON LOCAL SYSTEM TO GET APPROPARIATE VERSIONING SIGH

use strict;
use Win32::EventLog;
use Repository;
use Getopt::Long;
use MlpAlarm;

use vars qw( $BATCHID $DAYS $DEBUG $LISTEVENTS $SERVER);

sub usage
{
	print "usage: win32_getversion.pl --BATCHID=id --DEBUG --SERVER=svr --LISTEVENTS --DAYS=days\n";
   	return "\n";
}

$| =1;		# dont buffer standard output

die usage() unless GetOptions(
		"SERVER=s" 		=> \$SERVER,
		"BATCHID=s" 		=> \$BATCHID ,
		"DAYS=s" 		=> \$DAYS ,
		"LISTEVENTS"		=> \$LISTEVENTS,
		"DEBUG"     		=> \$DEBUG );

$DAYS=30 unless $DAYS;
$BATCHID="PcGetVersion" unless $BATCHID;

$|=1;
my($VERSION)="1.0";

#foreach ( get_password(-type=>'sybase') ) { 	$servertype{$_}="sybase"; }
MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Gem Monitor');

my(@sqlsvrs)= get_password(-type=>'win32servers');

if( $SERVER ) {
	@sqlsvrs=split(/,/,$SERVER);
}

my(%BOOTRECORDS);	# key = server, time
my(%OSVERSION);		# key = server
my(%SHORTOSVERSION);		# key = server

print "win32_getversion.pl v1.0: Run at ".localtime(time)."\n";
print "Fetching version and boot times for windows servers\n";
$DAYS*=60*60*24;
$DAYS=time-$DAYS;

foreach my $SYSTEM ( @sqlsvrs ) {
	print "analyzing... $SYSTEM\n" if $DEBUG;
	print "." unless $DEBUG;
        my $handle = Win32::EventLog->new("System",$SYSTEM );
        if( ! $handle ) {
        	warn "Cant open Application Eventlog on $SYSTEM";
        	next;
        }

        my($base,$recs);
        $handle->GetOldest($base);
        $handle->GetNumber($recs);
        $Win32::EventLog::GetMessageText=1;		# formatted messages

        my($recno)=1;
        my($hashRef);
 #       my($numrecread)=0;

        while( $recno++<$recs ) {
        	# read sequentially from most recent
        	$handle->Read(EVENTLOG_BACKWARDS_READ|EVENTLOG_SEQUENTIAL_READ,undef,$hashRef) or next;
	#	$numrecread++;
        #	print "\t[$numrecread event records read]\n" if $numrecread%1000==0;

		my($id) = $$hashRef{EventID} & 0xffff;

		if( $id == 6009 ) {

			last if $$hashRef{TimeGenerated}<$DAYS and defined $OSVERSION{$SYSTEM};
			print "[$SYSTEM] $$hashRef{Message}" if $DEBUG;
			print "[$SYSTEM] $$hashRef{TimeGenerated} ".localtime($$hashRef{TimeGenerated})."\n"
				if $LISTEVENTS;
			#$BOOTRECORDS{$SYSTEM.":".$$hashRef{TimeGenerated}.":".localtime($$hashRef{TimeGenerated}).":".$$hashRef{Message}}=1;
			$BOOTRECORDS{$SYSTEM.":".$$hashRef{TimeGenerated}}=1;
			$$hashRef{'Strings'} =~ tr/\0/\n/;
			print "Strings=",$$hashRef{'Strings'} if $DEBUG;
			print "[$id] $$hashRef{Message}" if $DEBUG;
			$OSVERSION{$SYSTEM}=$$hashRef{Message};
			$SHORTOSVERSION{$SYSTEM}=$$hashRef{'Strings'};
		# print out the keys stuff
	        #	foreach ( keys %$hashRef ) {
	        #		print "  key $_ value $$hashRef{$_}\n";
	        #	}

		        last if $$hashRef{TimeGenerated}<$DAYS;
		}
	}

	$handle->Close();
};

foreach my $SYSTEM ( sort @sqlsvrs ) {
	print "SERVER: $SYSTEM \n";
	$SHORTOSVERSION{$SYSTEM}=~s/\n/\//g;
	chomp $SHORTOSVERSION{$SYSTEM};
	chomp $OSVERSION{$SYSTEM};
	print "  VER: $SHORTOSVERSION{$SYSTEM}\n";
	print "   OS: $OSVERSION{$SYSTEM}\n";
	foreach ( keys %BOOTRECORDS ) {
		next unless /^$SYSTEM:/;
		s/^$SYSTEM://;
		print "   Rebooted at ".localtime($_)."\n";
	}
	print "\n";
}
print "Completed read from eventlogs\n" if defined $DEBUG;
MlpBatchJobEnd();
exit(0);


