#!/usr/local/bin/perl-5.6.1
use lib qw(/apps/sybmon/dist/lib /apps/sybmon/dist/ADMIN_SCRIPTS/lib /apps/sybmon/dist/plugins /apps/sybmon/dist/cpan);

use strict;
use MlpAlarm;
use DBI;

$|=1;

print "======================================================\n";
print "ADDING AN EVENT\n";
MlpEvent( -severity=>"INFORMATION", -message_text=>"Test Message", -debug=>1);
print "======================================================\n";

print "ADDING A HEARTBEAT\n";
DBI->trace(2);
MlpHeartbeat( -state=>"OK", -message_text=>"Test Message", -debug=>1);
print "======================================================\n";

