#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        return join("",@_).  "./data_mine.pl [-dx] -USA_USER -SSERVER -PSA_PASS -DDB -ooutfile -iinfile -f
       -wwhereclause -ssearchclause
       -d debug mode\n";
}

use strict;
use Getopt::Std;
use File::Basename;
use Sybase::SybFunc;

use vars qw($opt_d $opt_U $opt_S $opt_P  $opt_D $opt_w $opt_s  $opt_o $opt_i $opt_l $opt_f);
die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('dU:D:S:P:w:s:i:o:l:f');

die usage("Must pass username\n" ) unless defined $opt_U;
die usage("Must pass password\n" ) unless defined $opt_P;
die usage("Must pass server\n" ) unless defined $opt_S;
die usage("Must pass database\n" ) unless defined $opt_D;
die usage("Must pass where clause\n" ) unless defined $opt_w or defined $opt_i;
die usage("Must pass -o or -i \n" ) unless defined $opt_o or defined $opt_i;
die usage("Cant pass -i without -s\n" ) if defined $opt_i and ! defined $opt_s;
die usage("Cant pass -o and -s\n" ) if defined $opt_o and defined $opt_s;

print "Connecting to sybase\n" if defined $opt_d;
db_connect($opt_S,$opt_U,$opt_P)
        or die "Cant connect to $opt_S as $opt_U\n";

my(%SEARCH_KEYS);

# manage where clause
$opt_w=~s/^\s*where\s*//i;
$opt_w=~s/\n/ /i;
$opt_w=~s/\s+/ /g;
my(@where) = split(/[aA][nN][dD]\s*/,$opt_w);
foreach (@where) {
	print "Where: $_ \n";
	my($k,$v)=split /=/;
	$k=~s/\s//g;
	$SEARCH_KEYS{$k}=$v;
}

my(@file_data);
if( ! defined $opt_i ) {
	# NO INPUT FILE - CREATE ONE
	my($sql)="select c.name,o.name
		from syscolumns c, sysobjects o
		where o.id = c.id
		and   o.type = 'U'
		and   o.uid = 1
		and   c.name in ('".join("','",keys %SEARCH_KEYS)."')";
	my(@coldata)=db_query($opt_D,$sql);

	my(%keys_by_object);
	print "Syscolumns Query Returned ",($#coldata+1)," Rows\n" if defined $opt_d;
	foreach (@coldata) {
		my($cnm,$onm) = db_decode_row($_);
		if( defined $keys_by_object{$onm} ) {
			my($x)= $keys_by_object{$onm};
			push @$x,$cnm;
		} else {
			my(@v);
			push @v,$cnm;
			$keys_by_object{$onm} = \@v;
		}
	}

	open(OUTFILE,">".$opt_o) or die "Cant write to $opt_o : $! \n";
	foreach my $table (keys %keys_by_object) {
		my($x)= $keys_by_object{$table};
		my($tblstr)= "TABLE: ".$table."\n";

		push  @file_data, $tblstr;

		if( $#where == $#$x ) {
			$sql = "select * from $table where $opt_w";
			#print "Starting Query $sql\n";
			my(@data)=db_query($opt_D,$sql,1,1);
			next if $#data<0;
			printf "%30.30s -> COMPLETE %d ROWS FOUND\n",$table,($#data+1);
			#print "Done Query \n";
			print OUTFILE $tblstr;
			foreach (@data) {
				print OUTFILE $_,"\n";
				push @file_data,$_;
			}
		} else {
			my($twhere)="WHERE ";
			foreach (@$x) {
				$twhere .= $_ . " = " .$SEARCH_KEYS{$_} ." and ";
			}
			$twhere=~s/ and $/\n/;
			$sql = "select count(*) from $table $twhere";
			# print "DBG: $sql";
			my(@data)=db_query($opt_D,$sql);
			next if $data[0] == 0;
			printf "%30.30s -> INCOMPLETE %s ELEMENTS\n" ,$table,$data[0]
				if defined $opt_d;
			$sql = "select * from $table $twhere";
			my(@data)=db_query($opt_D,$sql,1,1);
			print OUTFILE $tblstr;
			foreach (@data) {
				print OUTFILE $_,"\n";
				push @file_data,$_;
			}
		}
	}
	close(OUTFILE);
} else {
	open(IN,$opt_i) or die "Cant read $opt_i :$! \n";
	foreach (<IN>) {
		chop;
		push @file_data,$_;
	}
	close(IN);
}

exit(0) unless defined $opt_s;

# ok here we have @file_data
#  which is format
#   TABLE: name
#     <rowheader>
#     0+ data rows
#
my($srchkey,$srchval);
if( $opt_s =~ /\=/ ) {
	($srchkey,$srchval)=split(/=/,$opt_s);
	print "SEARCHING COLUMN $srchkey for ($srchval)\n";
} else {
	($srchkey,$srchval)=("",$opt_s);
	print "SEARCHING for ($srchval)\n";
}

my($srchcol)= undef;
$srchkey=~s/\s//;
my($hdrflag)=0;
print ".... SEARCHING ... \n";
my($curtbl)="";
my(@colnames);
foreach my $row (@file_data) {
	if( $row =~ /^TABLE:/ ) {
		@colnames=();
		$hdrflag=1;
		#print $row,"\n";
		$row =~ s/^TABLE://;
		$curtbl = $row;
	} elsif ( $hdrflag==1 )  {
		# parse header to find column
		$hdrflag=0;
		next if defined $srchcol;
		@colnames=db_decode_row($row);
		next if $srchkey eq "";
		$srchcol=0;
		foreach (@colnames) { last if $_ eq $srchkey; $srchcol++; }
		print $curtbl," : COLUMN FOUND IN POSITION $srchcol\n";
	} else {
		next unless defined $srchcol or $srchkey eq "";
		my(@d)=db_decode_row($row);
		if( $srchkey eq "" ) {
			# print "Warning Line $row Contains Key" if $row =~ /$srchval/;
			my($c)= -1;
			foreach (@d) {
				$c++;
				next unless $_ eq $srchval;
				print $curtbl.".".$colnames[$c],"\n" if defined $opt_f;
				print "$curtbl : $colnames[$c] : $row \n" unless defined $opt_f;
				last;
			}
		} else {
			print "$curtbl : $row \n" if ! defined $opt_f and $d[$srchcol] eq $srchkey;
			print "$curtbl.$row \n" if defined $opt_f and $d[$srchcol] eq $srchkey;
		}
	}
}
print ".... DONE SEARCHING ... \n";

=head1 NAME

data_mine.pl - search data on tables

=head2 DESCRIPTION

parses where clause and builds data extract of table data which it searches.
so... you pass whereclause.  This gets parsed and tables with either all the
appropriate keys are extracted UNION tables with one row after a subset
of the keys is checked.  This data is then searched for column info with
the preset values

=head2 USAGE

   ./data_mine.pl [-dx] -USA_USER -SSERVER -PSA_PASS -DDB -ooutfile -iinfile
            -Wwhereclause -Ssearchclause

            -d debug mode

	you can create output file by putting -o (and no -s)
	you can search with -s and -i

=head2 BUGS

im going to rely on decent design - if you have int columns with the same
layout as your char data - its gonna bomb (why do you have columns named
the same thing with 2 different data types anyway).

well data mining is kind of brute force

=cut
