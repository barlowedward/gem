: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use strict;
use File::Basename;
use Cwd;
use Getopt::Long;

if( $] < 5.008001 ) {
	if( defined $^V and $^V!~/\s*$/) {
		die "Error : You must have perl 5.8.1 or later installed.  You are running $^V \n";
	} 
	my($x)=$];
	$x=~s/0+/\./g;
	$x=~s/\.\.+/\./g;
	die "Error : You must have perl 5.8.1 or later installed.  You are running $x ($])\n";				
}
if( ! defined $ENV{DISPLAY} and ! defined $ENV{PROCESSOR_LEVEL} ) {
	die "Error : DISPLAY environment variable not set!\n";
}


use vars qw($NOPAUSE $DBG);
foreach ( @ARGV ) { $NOPAUSE=1 if /NOPAUSE/; $DBG="-d " if /DEBUG/ }

my  $curdir=dirname($0);
chdir($curdir) or die "Cant cd to $curdir: $!\n";

my($dir)=cwd();
my($includes)=" -I$dir/lib -I$dir/plugins ";
$includes=" -I$dir/Win32_perl_lib_5_8".$includes if defined $ENV{PROCESSOR_LEVEL} and $]>=5.008;
$includes .= $ENV{perlargs}." " if defined $ENV{perlargs};

print "$^X  $DBG $includes $dir/bin/new_troubleshoot.pl ".join(" ",@ARGV),"\n";
system("$^X $DBG $includes $dir/bin/new_troubleshoot.pl ".join(" ",@ARGV));

if( defined $ENV{PROCESSOR_LEVEL} and ! defined $NOPAUSE ) {
         print "<Hit Enter Key To Continue>";
         <STDIN>;
}

__END__

=head1 NAME

troubleshoot.pl - Rules based troubleshooter

=head2 USAGE

perl troubleshoot.pl

=head2 DESCRIPTION

Troubleshoot.pl is a command line program designed to
troubleshoot your installation of the GEM.
For now all it does is verify and validate your installation of
perl. Run this prior to running any of
the other scripts.

=head2 TROUBLESHOOT OUTPUT

/apps/sybmon/perl/bin/perl  -I/apps/sybmon/dev/lib -I/apps/sybmon/dev/plugins  /apps/sybmon/dev/bin/troubleshoot.pl
troubleshoot.pl - perl package troubleshooter
   Your perl version is 5.008004
   Your Perl Library Include Path is
        /apps/sybmon/dev/lib
        /apps/sybmon/dev/plugins
        /apps/sybmon/perl/lib/5.8.4/sun4-solaris-thread-multi
        /apps/sybmon/perl/lib/5.8.4
        /apps/sybmon/perl/lib/site_perl/5.8.4/sun4-solaris-thread-multi
        /apps/sybmon/perl/lib/site_perl/5.8.4
        /apps/sybmon/perl/lib/site_perl
        .
   Testing Perl Builtin Package Integrity
         OK: All Required Builtin Perl Packages Found!!
   Testing Perl Database Connectivity Package Integrity
         OK: All Required Perl/DBI Packages Found!!
         OK: DBI Version is (1.46)
   OK: DBI Version is (1.04)
   Testing Perl/Tk Package Integrity
         OK: All Required Perl/Tk Packages Found!!
         OK: Tk Version is (804.027)
   Testing Gem Add On Module Integrity
         OK: All Gem Packages Found!!
   Testing Toolkit Built In Module Integrity
         OK: All Builtin Packages Found!!

=cut

