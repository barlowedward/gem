#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
	  //samba/sybmon/dev/lib
	  //samba/sybmon/dev/Win32_perl_lib_5_8);

BEGIN {
  $ENV{SYBASE}="/apps/sybase";
}

use strict;
use lib qw(/dat/apacheconf/lib);
use DBI;
use File::Basename;
use DBIFunc;

our $SERVER;
our $DATABASE;
our $TABLE;
our $USER;
our $PASSWORD;
our $DBSERVER;

$PASSWORD = shift @ARGV;
$SERVER	  = shift @ARGV;
$DATABASE = shift @ARGV;
$TABLE    = shift @ARGV;

if (! (  defined($PASSWORD)
      && defined($SERVER)
      && defined($DATABASE)
      && defined($TABLE)
      && @ARGV >= 2))
{
  usage(__LINE__);
}

my $query;
my @rc;
my $result;

$USER = "sa";

$SERVER = uc($SERVER);

if ($SERVER eq "COMPLIANCEDB")
{
}
elsif ($SERVER eq "SYBMON")
{
  $USER = "lschrieber";
}
else
{
  usage(__LINE__);
}

$DBSERVER = dbi_connect(-srv			=>$SERVER,
			-login			=>$USER,
			-password		=>$PASSWORD,
			-nosybaseenvcheck	=>1,
			-connection		=>1)
	or die "Cant connect to $SERVER as director\n";

dbi_set_mode("INLINE");

run_query(0, "
use $DATABASE
");

for (my $argn = 1; $argn < @ARGV; $argn += 2)
{
  my $privilege = $ARGV[$argn-1];
  my $class     = $ARGV[$argn  ];

  run_query(1, "
  grant $privilege on $TABLE to $class
");
}

The_End:
dbi_disconnect(-connection=>$DBSERVER);
exit(0);

# end of main

sub run_query
{
  my $include = $_[0];
  my $query   = $_[1];

  my $result;

  if ($include == 0) {return;}

print STDERR $query, "\n";

print STDERR "Press y to proceed >";

while (<STDIN>)
{
  chomp;
  if ($_ eq 'y')
  {
    last;
  }
}
print STDERR "\nrunning the query ...\n\n";

  my @rc = dbi_query(-query		=>$query,
		     -db		=>$DATABASE,
		     -connection	=>$DBSERVER);

  my $row = 0;

  foreach (@rc)
  {
    my(@v) = dbi_decode_row($_);

    $row += 1;

#   print $row, " >", join("< >", @v), "<\n";
    print join("\t", @v), "\n";
    $result = join(" ", @v);
  }

  return $result;
}

sub usage
{
  my $line = $_[0];

  print "Usage($line): $0 password server database table [privilege class [...]]\n";
  exit;
}
# end of module
