#!/bin/ksh

if [ -z "$1" ]
then
	echo "Usage $0 Hostname User Pass"
	exit 1
fi

if [ -z "$2" ]
then
	echo "Usage $0 Hostname User Pass"
	exit 1
fi

if [ -z "$3" ]
then
	echo "Usage $0 Hostname User Pass"
	exit 1
fi

HOST=$1
USER=$2
PASS=$3

ftp -nv $HOST << EOF
user $USER $PASS
mkdir .ssh
chmod 700 .ssh
cd .ssh
prompt
put authorized_keys2 authorized_keys
put authorized_keys2 authorized_keys2
chmod 644 *
EOF
