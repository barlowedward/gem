#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use Win32::EventLog;
use Getopt::Long;
use Repository;
use DBI;
use MlpAlarm;
use DBIFunc;
use Do_Time;
use vars qw( $BATCHID $HTMLDETAIL $OUTFILE $HTMLFILE $SYSTEM $HOURS $USER $PASSWORD $SRVNAME $NOSTDOUT $REPORT $PRIMARY $DEBUG $LISTEVENTS );

$|=1;
my($VERSION)="1.0";

# changedir to directory in which the executable exists
my($curdir)=dirname($0);
chdir $curdir or die "Cant chdir to $curdir : $! \n";

sub usage {
	print @_,"\n";
	return "Usage: win32_backupreport.pl --OUTFILE=file --HTMLFILE=file --SYSTEM=system --HOURS=hours --USER=sa --PASSWORD=password --SRVNAME=ODBCID --NOSTDOUT --BATCHID=name --REPORT=<backup|restore> --PRIMARY=<primary host if restore> --DEBUG --HTMLDETAIL=file --LISTEVENTS";
}

sub error_out {
   my($msg)=join("",@_);
   $msg = "BATCH ".$BATCHID.": ".$msg if $BATCHID;
   MlpBatchJobEnd(-EXIT_CODE=>"ERROR", -EXIT_NOTES=>$msg);

   MlpEvent(
      -monitor_program=> "BackupRpt",
      -system=> $BATCHID,
      -message_text=> $msg,
      -severity => "ERROR"
   );
   die $msg;
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
	GetOptions( "OUTFILE=s"=>\$OUTFILE,
			"HTMLFILE=s"=>\$HTMLFILE,
			"SYSTEM=s"=>\$SYSTEM,
			"HOURS=s"=>\$HOURS,
			"USER=s"=>\$USER,
			"PASSWORD=s"=>\$PASSWORD,
			"SRVNAME=s"=>\$SRVNAME,
			"NOSTDOUT"=>\$NOSTDOUT,
			"LISTEVENTS"=>\$LISTEVENTS,
			"PRIMARY=s"=>\$PRIMARY,
			"REPORT=s"=>\$REPORT,
			"BATCHID=s"=>\$BATCHID,
			"DEBUG"=>\$DEBUG,
			"HTMLDETAIL=s"=>\$HTMLDETAIL );

my($runby)=$BATCHID;
$BATCHID="BackupRpt $SYSTEM" unless $BATCHID;

error_out( "HTMLDETAIL and HTMLFILE must be in same directory\n" )
	if defined $HTMLDETAIL and defined $HTMLFILE
		and dirname($HTMLDETAIL) ne dirname($HTMLFILE);

$REPORT="backup" unless defined $REPORT;
#die usage("Must pass --USER") 		unless defined $USER;
#die usage("Must pass --PASSWORD") 	unless defined $PASSWORD;
error_out("Must pass --PRIMARY if REPORT=restore\n")
	if $REPORT eq "restore" and ! defined $PRIMARY;
error_out("Must not pass --PRIMARY if REPORT=backup\n")
	if $REPORT eq "backup" and defined $PRIMARY;

$SRVNAME=$SYSTEM unless defined $SRVNAME;

# REPORT VARIABLES
# stuff gets stored into these - parsed from the error log
my( %V_BACKUP_TIME, %V_BACKUP_FILE, %V_BACKUP_LSN, %V_TRANDUMP_TIME, %V_TRANDUMP_FILE, %V_TRANDUMP_LSN, %V_RESTORE_TIME, %V_RESTORE_FILE, %V_RESTORE_LSN, %V_TRANLOAD_TIME, %V_TRANLOAD_FILE, %V_TRANLOAD_FILETIME, %V_TRANLOAD_LSN, %V_LOGTRUNCATED );
#my( %ALL_BACKUP_TIME, %ALL_BACKUP_FILE, %ALL_BACKUP_LSN, %ALL_TRANDUMP_TIME, %ALL_TRANDUMP_FILE, %ALL_TRANDUMP_LSN, %ALL_RESTORE_TIME, %ALL_RESTORE_FILE, %ALL_RESTORE_LSN, %ALL_TRANLOAD_TIME, %ALL_TRANLOAD_FILE, %ALL_TRANLOAD_FILETIME, %ALL_TRANLOAD_LSN, %ALL_LOGTRUNCATED );

my(@databases)=get_db($SYSTEM);
my(@systems)=($SYSTEM);
push @systems,$PRIMARY if defined $PRIMARY;
MlpBatchJobStart(-BATCH_ID=>$BATCHID,-SUBKEY=>$SYSTEM);

foreach my $system  (@systems) {
print "Working on system $system\n" if defined $DEBUG;
foreach my $logtype ("Application") {
	print "Working on log type $logtype\n" if defined $DEBUG;
	my($alarmlvl)="ok";
	my($alarmmsg)="";

	my $handle = Win32::EventLog->new($logtype,$system )
		or error_out( "Cant open Application Eventlog on $system" );

	my($base,$recs);
	$handle->GetOldest($base);
	$handle->GetNumber($recs);
	$Win32::EventLog::GetMessageText=1;		# formatted messages

	my($recno)=1;
	my($hashRef);
	my($numrecread)=0;
	while( $recno++<$recs ) {
		# read sequentially from most recent
		$handle->Read(EVENTLOG_BACKWARDS_READ|EVENTLOG_SEQUENTIAL_READ,undef,$hashRef) or next;

		# only sql server messages
		next unless $$hashRef{Source} eq "MSSQLSERVER";

		# when the time limit has been reached... quit
		last if defined $HOURS and time - $$hashRef{TimeGenerated} > $HOURS*60*60;
		$numrecread++;
		print "[$numrecread event records read]\n" if defined $DEBUG and $numrecread%1000==0;

		my($tag); 		# tag will be ERROR, WARNING, INFORMATION
		if( $$hashRef{EventType} == EVENTLOG_INFORMATION_TYPE ) {
			$tag="INFORMATION,";
		} elsif( $$hashRef{EventType} == EVENTLOG_ERROR_TYPE ) {
			$tag="ERROR,";
		} else {
			print "Unknown Event Type ";
			foreach ( keys %$hashRef ) {
				print "\t$_\t $$hashRef{$_} \n";
			}
			#$tag="WARNING,";
			next;
		}
		my($msgstring)=$$hashRef{Message};
		$msgstring=$$hashRef{Strings} if ! defined $msgstring or $msgstring=~/^\s*$/;

		# added this - was required for some reason... sigh
		my($p)=chr(0);
		$msgstring=~s/$p/ /g;

		my($m,$s)= split(/\s/,$msgstring,2);
		$s=~ s/\r//g;				# remove ctrl m
		$s=~ s/\)\.\s*$//;		# remove ). from end of line

		print "Error: Cant get errno for $msgstring \n"
			unless defined $m and $m == int($m);

#foreach ( keys %$hashRef ) { print "K=$_ V=$$hashRef{$_}\n"; }
# print "msgstring=$msgstring\n" if $DEBUG;
#print "[$m] ",substr($s,0,30)," ...\n" if $DEBUG;

		next unless int($m) eq $m;
		if( $tag eq "ERROR," and $m != 18278  ) {
			print "ERROR $m: $s\n" if defined $LISTEVENTS;
			next;
		} elsif( $m >= 18264 and $m<=18271 ) {
			my($op,$db,$crdt,$firstlsn,$lastlsn,$device,$filetime,$pages)=parse_dump_line($m,$s);
			output( sprintf("(%s) %20s - %20s : %s\n",$m,$op,$db,$filetime )) if defined $DEBUG or defined $LISTEVENTS;

			if( $m == 18264 ) {			# db backup
				if( ! defined $V_BACKUP_TIME{$db} ) {
					next if $REPORT eq "restore" and $system eq $SYSTEM;
					#$ALL_BACKUP_TIME{$device} = $$hashRef{TimeGenerated};
					#$ALL_BACKUP_LSN{$device}  = $lastlsn;
					$V_BACKUP_TIME{$db} = $$hashRef{TimeGenerated};
					$V_BACKUP_FILE{$db} = $device;
					$V_BACKUP_LSN{$db}  = $lastlsn;
				}
			} elsif( $m == 18265 ) {	# log backup
				if( ! defined $V_TRANDUMP_TIME{$db} ) {
					next if $REPORT eq "restore" and $system eq $SYSTEM;
					#$ALL_TRANDUMP_TIME{$device} = $$hashRef{TimeGenerated};
					#$ALL_TRANDUMP_LSN{$device}  = $lastlsn;
					$V_TRANDUMP_TIME{$db} = $$hashRef{TimeGenerated};
					$V_TRANDUMP_FILE{$db} = $device;
					$V_TRANDUMP_LSN{$db}  = $lastlsn;
				}
			} elsif( $m == 18266 ) {	# db file backup
			} elsif( $m == 18269 ) {	# file restore
			} elsif( $m == 18267 ) {	# db restored
				if( ! defined $V_RESTORE_TIME{$db} ) {
					next if $REPORT eq "restore" and $system eq $PRIMARY;
					#$ALL_RESTORE_TIME{$device} = $$hashRef{TimeGenerated};
					#$ALL_RESTORE_LSN{$device}  = $lastlsn;
					$V_RESTORE_TIME{$db} = $$hashRef{TimeGenerated};
					$V_RESTORE_FILE{$db} = $device;
					$V_RESTORE_LSN{$db}  = $lastlsn;
				}
			} elsif( $m == 18268 ) {	# log restored
				if( ! defined $V_TRANLOAD_TIME{$db} ) {
					next if $REPORT eq "restore" and $system eq $PRIMARY;

					if( $DEBUG ) {
						output("New Restore of $db: \n\tFile=$device\n\tAt Time=$filetime (".localtime($filetime).")\n");
						print "New Restore of $db: File=$device\n";
					}
					#$ALL_TRANLOAD_TIME{$device} 		= $$hashRef{TimeGenerated};
					#$ALL_TRANLOAD_FILETIME{$device} 	= $filetime;
					#$ALL_TRANLOAD_LSN{$device}  		= $lastlsn;

					$V_TRANLOAD_TIME{$db} = $$hashRef{TimeGenerated};
					$V_TRANLOAD_FILE{$db} = $device;
					$V_TRANLOAD_FILETIME{$db} = $filetime;
					$V_TRANLOAD_LSN{$db}  = $lastlsn;
				}
			} elsif( $m == 18270 ) {	# diff backup
			} elsif( $m == 18271 ) {	# db diff restore
			}
		} elsif( $m == 18278 ) {	# log truncated
			$s=~s/[\r\n]//g;
			$m=~s/[\r\n]//g;
			output( "($m) $tag $s\n" ) if defined $DEBUG;
			$s=~ s/:Database log truncated: Database: //;
			$s=~ s/\s//g;
			$s=~ s/\.//g;
			# ok well
			$V_LOGTRUNCATED{$s} = $$hashRef{TimeGenerated};
			output( "($m) $s\n" ) if defined $LISTEVENTS;
		} elsif( $m == 18204 ) {
			output( "($m) $s\n" ) if defined $LISTEVENTS;
		} elsif( $m == 17550 or  $m==17551 ) {
			# dbcc traceon
		} elsif( $m >= 18450 and $m<=18470 ) {	# Login Succeeded/fail
		} elsif( $m == 18100 ) {	# user killed
		} elsif( $s =~ /^\s*$/ ) {
			output( "(no msgno) $m\n" ) if defined $LISTEVENTS;
		} elsif( $m >= 18000 and $m<19000 ) {
			output( "($m) $s\n" );
		} elsif( $m >= 8900 and $m<8999 ) {	# dbcc error
		} else {
			# my(%h)=%$hashRef;
			# foreach (keys %h) { print "DBG--> $_  $h{$_} \n"; }
			my($msg)="\n\tTime:".(scalar localtime($$hashRef{TimeGenerated}))."\n\tComputer:".
					$$hashRef{Computer}."\n\tTag:".
					$tag.",".
					$$hashRef{Source}."\n\tSource:".
					$s;
					#$$hashRef{Message};
			#$msg =~ s/[\r\n]//g;
			#$msg =~ s/\s$//g;
			# output( $msg."\n" );
			output( "*** unknown message $m\n\t  $msg\n" );
		}
	}
	print "[completed server read: $numrecread event records]\n" if defined $DEBUG;

	$handle->Close();
};
};

print "Completed read from eventlogs\n" if defined $DEBUG;
print "OUTPUT FILE is $OUTFILE\n" if defined $OUTFILE;
print "HTML Report in $HTMLFILE - Details in $HTMLDETAIL\n" if defined $HTMLFILE;

unlink $HTMLFILE 	if -e $HTMLFILE;
unlink $HTMLDETAIL 	if -e $HTMLDETAIL;
unlink $OUTFILE 	if -e $OUTFILE;

open(HTML,">$HTMLFILE")
	or error_out( "Cant Write $HTMLFILE: $!") if defined $HTMLFILE;
open(HTMLDTL,">$HTMLDETAIL")
	or error_out( "Cant Write $HTMLDETAIL: $!")  if defined $HTMLDETAIL;
open(OUTF,">$OUTFILE")
	or error_out( "Cant Write $OUTFILE: $!" ) if defined $OUTFILE;

# ok what do we have now
#  - we have a bunch of hashes by database describing primary and secondary

# GENERATE REPORTS
if( $REPORT eq "backup" ) {
	print "Creating Backup Report\n" if defined $DEBUG;
	output( "Backup Report For System $SYSTEM\n" );

	output( "Run from Batch Id $BATCHID\n" ) if defined $BATCHID;
	output( "Created by win32_backupreport.pl version $VERSION\n" );
	output( "Run At ".(scalar localtime)."\n" );

	print HTML "<h1>Backup Report For System $SYSTEM</h1>\n"
		if defined $HTMLFILE;
	print HTML "Created by win32_backupreport.pl version $VERSION<br>\n" if defined $HTMLFILE;
	print HTML "Run At ".(scalar localtime),"<br>\n" if defined $HTMLFILE;

	output( sprintf( "%20s %s %s %s %s %s\n","Database", "Last Backup", "Last TranDump","Log Truncation","Last Lsn"));
	print HTML "<TABLE BORDER=1><TR><TH>Database</TH><TH>Last Backup</TH><TH>Last TranDump</TH><TH>Last Truncation</TH><TH>Last Lsn</TH></TR>\n" if defined $HTMLFILE;
	foreach ( sort @databases ) {
		my($lsn)= $V_BACKUP_LSN{$_};
		$lsn    = $V_TRANDUMP_LSN{$_} if $V_TRANDUMP_LSN{$_} ne "";
		my($alarmmsg,$rowcolor,$trandumptime,$lasttrunctime,$bkago,$trago)= (" "," "," "," "," "," ");

		# if dump > 24 hours or tranlog > 1 hour
		# if you truncated log > last backup time
		if( defined $V_LOGTRUNCATED{$_} and defined $V_BACKUP_TIME{$_} and $V_LOGTRUNCATED{$_} > $V_BACKUP_TIME{$_} ) {
			$rowcolor=" BGCOLOR=pink";
			$lasttrunctime=do_time(-fmt=>"mm/dd hh:mi",-nullonnull=>1,-time=>$V_LOGTRUNCATED{$_} );
			$alarmmsg="Log Trunc";
		} else { $lasttrunctime="n.a."; }

		# if you dumped tran > last backup time
		if( defined $V_TRANDUMP_TIME{$_} and defined $V_BACKUP_TIME{$_} and $V_TRANDUMP_TIME{$_} > $V_BACKUP_TIME{$_} ) {
			$trandumptime=do_time(-fmt=>"mm/dd hh:mi",-nullonnull=>1,-time=>$V_TRANDUMP_TIME{$_} );
		} elsif( defined $V_TRANDUMP_TIME{$_} and ! defined $V_BACKUP_TIME{$_} ){
			$trandumptime=do_time(-fmt=>"mm/dd hh:mi",-nullonnull=>1,-time=>$V_TRANDUMP_TIME{$_} );
			$rowcolor=" BGCOLOR=beige" ;
			$alarmmsg="No Full Dump";
		} else {
			$trandumptime=do_time(-fmt=>"mm/dd hh:mi",-nullonnull=>1,-time=>$V_TRANDUMP_TIME{$_} );
			if( defined $V_TRANDUMP_TIME{$_}
				and  $V_TRANDUMP_TIME{$_} ne "" ) {
				$rowcolor=" BGCOLOR=pink" ;
				$alarmmsg="TrDump Before LastBackup";
			}
		}

		if( defined $V_TRANDUMP_TIME{$_} and $trandumptime ne "n.a.") {
			$trago = to_dhm(time-$V_TRANDUMP_TIME{$_});
			$rowcolor=" BGCOLOR=pink"
				if (time-$V_TRANDUMP_TIME{$_}) > 60*60;
		}

		if( defined $V_BACKUP_TIME{$_} ) {
			$bkago = to_dhm(time-$V_BACKUP_TIME{$_});
			if( (time-$V_BACKUP_TIME{$_}) > 24*60*60 ) {
				$rowcolor=" BGCOLOR=pink" ;
				$alarmmsg=" Backup>24Hours";
			}
		}

		htmldetail($_) if defined $HTMLDETAIL;

		my($bktime)=do_time(-fmt=>"mm/dd hh:mi",-nullonnull=>1,-time=>$V_BACKUP_TIME{$_} );
		$bktime="n.a." unless defined $bktime;

		output(sprintf( "%20s %s %s %s %s %s %s\n",$_,
				$bktime,
				$bkago,
				$trandumptime,
				$trago,
				$lasttrunctime,
				$lsn));
		my($db)=$_;
		$db = "<A HREF=".basename($HTMLDETAIL)."#$_>$_</A>"
			if defined $HTMLDETAIL;

		print HTML "<TR $rowcolor><TD>",$db, "</TD><TD>",
				$bktime, "<b>$bkago</b></TD><TD>",
				$trandumptime, "<b>$trago</b></TD><TD>",
				$lasttrunctime, "</TD><TD>$alarmmsg</TR></TR>\n" if defined $HTMLFILE;
	}
	print HTML "</TABLE>" if defined $HTMLFILE;
} else {
	print "Creating Restore Report\n" if defined $DEBUG;

	if( defined $PRIMARY ) {
		print "Log Shipping Report From $PRIMARY To $SYSTEM\n" if defined $DEBUG;
		output( "Log Shipping Report From $PRIMARY To $SYSTEM\n");
		print HTML "<h1>Log Shipping Report From $PRIMARY To $SYSTEM</h1>\n"
			if defined $HTMLFILE;
	} else {
		print "Log Shipping Report For System $SYSTEM\n"  if defined $DEBUG;
		output( "Log Shipping Report For System $SYSTEM\n");
		print HTML "<h1>Log Shipping Report For System $SYSTEM</h1>\n"
			if defined $HTMLFILE;
	}

	output( "Created by win32_backupreport.pl version $VERSION\n");
	print HTML "Created by win32_backupreport.pl version $VERSION<br>\n" if defined $HTMLFILE;
	output( "Run At ".(scalar localtime)."\n");
	print HTML "Run At ".(scalar localtime),"<br>\n" if defined $HTMLFILE;

	output( sprintf( "%20s %s %s %s %s\n","Database", "LAST FULL LOAD", "LAST TRAN LOAD","LAST TRAN DUMP","LAST LSN"));
	print HTML "<TABLE BORDER=1><TR><TH>Database</TH><TH>Full Load</TH><TH>Tran Load</TH><TH>Behind</TH><TH>Dump Lsn</TH><TH>Load Lsn</TH><TH>Alarm</TH></TR>\n" if defined $HTMLFILE;

	if( defined $DEBUG ) {
		use Data::Dumper;
		print Dumper \%V_TRANDUMP_TIME;
		print Dumper \%V_TRANDUMP_FILE;
		print Dumper \%V_TRANDUMP_LSN;

		print Dumper \%V_TRANLOAD_TIME;
		print Dumper \%V_TRANLOAD_FILE;
		print Dumper \%V_TRANLOAD_LSN;
	}
#my( %V_BACKUP_TIME, %V_BACKUP_FILE, %V_BACKUP_LSN, %V_TRANDUMP_TIME, %V_TRANDUMP_FILE,
#%V_TRANDUMP_LSN, %V_RESTORE_TIME, %V_RESTORE_FILE, %V_RESTORE_LSN, %V_TRANLOAD_TIME, %V_TRANLOAD_FILE,
# %V_TRANLOAD_FILETIME, %V_TRANLOAD_LSN, %V_LOGTRUNCATED );
#my( %ALL_BACKUP_TIME, %ALL_BACKUP_FILE, %ALL_BACKUP_LSN, %ALL_TRANDUMP_TIME, %ALL_TRANDUMP_FILE,
#%ALL_TRANDUMP_LSN, %ALL_RESTORE_TIME, %ALL_RESTORE_FILE, %ALL_RESTORE_LSN, %ALL_TRANLOAD_TIME,
#%ALL_TRANLOAD_FILE, %ALL_TRANLOAD_FILETIME, %ALL_TRANLOAD_LSN, %ALL_LOGTRUNCATED );

	foreach ( sort @databases ) {
		my($full_dump_state,$tran_dump_state,$last_sync_time,$dump_lsn,$load_lsn,$color,$alarmmsg);

		# set status  - tran_dump_state
		#
		my($bkfile, $bklog, $loadfile, $loadlog)
			= (basename($V_BACKUP_FILE{$_}),basename($V_TRANDUMP_FILE{$_}),
				basename($V_RESTORE_FILE{$_}),basename($V_TRANLOAD_FILE{$_}));

		$bkfile		= "" unless defined $bkfile;
		$bklog		= "" unless defined $bklog;
		$loadfile	= "" unless defined $loadfile;
		$loadlog	= "" unless defined $loadlog;

		htmldetail($_) if defined $HTMLDETAIL;

		# COMPARE FULL BACKUPS
		$last_sync_time = $V_RESTORE_TIME{$_} || 0;
		if( $bkfile ne "" and $loadfile ne "") {
			if($bkfile eq $loadfile) {
				$full_dump_state="Synced ".  to_dhm(time - $V_BACKUP_TIME{$_}) ;
				$last_sync_time  = $V_BACKUP_TIME{$_};
			} else {
				# ok the files are different cool... check out the tran log time
				if( $V_TRANLOAD_TIME{$_} > $V_RESTORE_TIME{$_} ) {
					$full_dump_state="(see tranlog)";
				} else {
					$full_dump_state="Not Synched";
				}
			}
		} elsif( $bkfile ne "" ) {
			$full_dump_state = "Dump Only";
		} else {
			$full_dump_state = "N.A.";
		}

		my($time_behind)=0;
		if( $bklog ne "" and $loadlog ne "") {
			if($bklog eq $loadlog) {
				$tran_dump_state="Synced ".  to_dhm(time - $V_TRANDUMP_TIME{$_}) ;
				$last_sync_time  = $V_TRANDUMP_TIME{$_}
					 if ! defined $last_sync_time
						or 	$V_TRANDUMP_TIME{$_} > $last_sync_time;
			} else {
				$last_sync_time = $V_TRANLOAD_TIME{$_};
				if( $V_TRANDUMP_LSN{$_} eq $V_TRANLOAD_LSN{$_} ) {
					$tran_dump_state="InSync";
				} else {
					if( time - $V_TRANLOAD_TIME{$_} < 60*60 ) {
						$tran_dump_state="Ok"
					} else {
						$alarmmsg="Old Transync";
						$tran_dump_state="Unsync";
						$color="BGCOLOR=PINK";
					};
				}
			}
		} elsif( $bklog ne "" ) {
			$tran_dump_state = "Dumped Only";
		} else {
			$tran_dump_state = "N.A.";
		}

		if( defined $last_sync_time ) {
			$time_behind = time-$last_sync_time;
			$last_sync_time= to_dhm($time_behind);
		} elsif( $_ eq "master" or $_ eq "model" or $_ eq "tempdb"
				or $_ eq "msdb" or $_ eq "pubs" or $_ eq "Northwind" ) {
			$last_sync_time= "";
		} else {
			$last_sync_time= "";
			$color="BGCOLOR=BEIGE";
			$alarmmsg="Not Synced";
		}


		$dump_lsn= $V_TRANDUMP_LSN{$_} || $V_BACKUP_LSN{$_};
		$load_lsn= $V_TRANLOAD_LSN{$_} || $V_RESTORE_LSN{$_};

		if(! defined $dump_lsn or ! defined $load_lsn ) {
			$dump_lsn="&nbsp;" unless defined $dump_lsn;
			$load_lsn="&nbsp;" unless defined $load_lsn;
			if( $_ ne "master" and $_ ne "model" and $_ ne "tempdb" and $_ ne "msdb" and $_ ne "pubs" and $_ ne "Northwind" ) {
				$color="BGCOLOR=RED" ;
				if(! defined $dump_lsn and ! defined $load_lsn ) {
					$alarmmsg="No LSN";
				} elsif( ! defined $load_lsn ) {
					$alarmmsg="No Dest LSN";
				} elsif( ! defined $dump_lsn ) {
					$alarmmsg="No Src LSN";
				}
			}
		} else {
			if( $dump_lsn ne $load_lsn and $time_behind>60*60*4 ) {
				$color="BGCOLOR=PINK";
				$alarmmsg="LSN out of sync";
			}
		}

		my($db)=$_;
		$db = "<A HREF=".basename($HTMLDETAIL)."#$_>$_</A>"
			if defined $HTMLDETAIL;

		$alarmmsg="&nbsp;" unless defined $alarmmsg;

		print HTML "<TR $color><TD>",$db, "</TD><TD>",
			$full_dump_state, "</TD><TD>",
			$tran_dump_state, "</TD><TD>",
			$last_sync_time, "</TD><TD>",
			$dump_lsn, "</TD><TD>",
			$load_lsn, "</TD><TD>",
			$alarmmsg,"</TD></TR>\n" if $HTMLFILE;
		output(sprintf( "%20s %s %s %s %s\n",$_,
			$full_dump_state,
			$tran_dump_state,
			$last_sync_time,
			$dump_lsn,
			$load_lsn ));
	}
	print HTML "</TABLE>" if defined $HTMLFILE;
}
close(HTML) if defined $HTMLFILE;
close(HTMLDTL) if defined $HTMLDETAIL;
close(OUTF) if defined $OUTFILE;

print "Program Completed\n" if defined $DEBUG;
MlpBatchJobEnd();
exit(0);

sub htmldetail
{
	my($db)=@_;
	my($dbref)= "<A HREF=".basename($HTMLFILE)."#$db>$db</A>";
	print HTMLDTL "<H1><A NAME=$db></A>$dbref</H1>\n";
	print HTMLDTL "Run At ".(scalar localtime),"<br>\n";
	print HTMLDTL "<TABLE BORDER=1><TR><TH>DB</TH><TH>STAT</TH><TH>VALUE</TH></TR>";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_BACKUP_TIME</TD><TD>$V_BACKUP_TIME{$db} ",to_dhm(time-$V_BACKUP_TIME{$db})," </TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_BACKUP_FILE</TD><TD>$V_BACKUP_FILE{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_BACKUP_LSN</TD><TD>$V_BACKUP_LSN{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_TRANDUMP_TIME</TD><TD>$V_TRANDUMP_TIME{$db} :",localtime($V_TRANDUMP_TIME{$db})," : ",to_dhm(time-$V_TRANDUMP_TIME{$db}),"</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_TRANDUMP_FILE</TD><TD>$V_TRANDUMP_FILE{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_TRANDUMP_LSN</TD><TD>$V_TRANDUMP_LSN{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_RESTORE_TIME</TD><TD>$V_RESTORE_TIME{$db} ",to_dhm(time-$V_RESTORE_TIME{$db}),"</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_RESTORE_FILE</TD><TD>$V_RESTORE_FILE{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_RESTORE_LSN</TD><TD>$V_RESTORE_LSN{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_TRANLOAD_TIME</TD><TD>$V_TRANLOAD_TIME{$db} :",localtime($V_TRANLOAD_TIME{$db})," : ",to_dhm(time-$V_TRANLOAD_TIME{$db}),"</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_TRANLOAD_FILE</TD><TD>$V_TRANLOAD_FILE{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_TRANLOAD_FILETIME</TD><TD>$V_TRANLOAD_FILETIME{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_TRANLOAD_LSN</TD><TD>$V_TRANLOAD_LSN{$db}</TD></TR> \n";
	print HTMLDTL "<TR><TD>$dbref</TD><TD> V_LOGTRUNCATED</TD><TD>$V_LOGTRUNCATED{$db}</TD></TR> \n";

	# print HTMLDTL "<TR><TD>$dbref</TD><TD> bkfile</TD><TD>$bkfile \n";
	# print HTMLDTL "<TR><TD>$dbref</TD><TD> bklog</TD><TD>$bklog \n";
	# print HTMLDTL "<TR><TD>$dbref</TD><TD> loadfile</TD><TD>$loadfile \n";
	# print HTMLDTL "<TR><TD>$dbref</TD><TD> loadlog</TD><TD>$loadlog \n";
	print HTMLDTL "</TABLE>";
}

sub output {
	my($msg)=@_;
	print OUTF $msg if defined $OUTFILE;
	print $msg  unless defined $NOSTDOUT;
}

# foreach ( sort @output ) { print $_; }

sub get_db
{
	my($system)=@_;
	if( ! defined $USER or ! defined $PASSWORD) {
		($USER,$PASSWORD)=get_password(-type=>'sqlsvr', -name=>$system);
	}
	error_out( "Cant find user/password for sqlserver $system\n" )
		unless defined $USER and defined $PASSWORD;

	dbi_connect( -srv=>$SYSTEM, -login=>$USER, -password=>$PASSWORD, -type=>"ODBC") or error_out( "Cant connect to $SYSTEM as $USER\n" );
	my(@dblist);
	foreach( dbi_query(-db=>"master",-query=>"select name from master..sysdatabases") ) {
		push @dblist,dbi_decode_row($_);
	}
	return @dblist;
}

sub redotime { my($t)=@_; return substr($t,0,2).":".substr($t,2,2); }

# parse messages about dump stuff
# returns : $operation, $db, $firstlsn, $lastlsn, $device
sub parse_dump_line {
	my($msg,$text)=@_;
	$_=$text;
	s/[\r\n]//g;
	s/^://;

	my($op,$db,$crdt,$firstlsn,$lastlsn,$device,$filetime,$pages,$ignore);

	s/[\r\n]//g;
	s/FILE=\d,//;
	my(@x)=split(/,/,$_);	# split the line

	($op,$ignore,$db)=split(/:/,$x[0],3);
	$db=~s/\s//g;

	my($i)=1;
	while( $i<=$#x ) {
		my($key,$value)= split(/:/,$x[$i],2);
		$key=~s/\s//g;
		$value=~s/^\s+//;
		if( $key eq "Database" ) {
			$db=$value;
		} elsif( $key eq "firstLSN" ) {
			$firstlsn=$value;
			$firstlsn=~s/\s//g;
		} elsif( $key eq "lastLSN" ) {
			$lastlsn=$value;
			$lastlsn=~s/\s//g;
		} elsif( $key eq "numberofdumpdevices" ) {
		} elsif( $key eq "pagesdumped" ) {
			$pages=$value;
			$pages=~s/\s//g;
		} elsif( $key eq "creationdate(time)" ) {
			$value=~s./..g;
			$value=~s/\(/ /;
			$value=~s/\)//;
			$value=~s/://g;
			$crdt=$value;
		} elsif( $key eq "deviceinformation" ) {
			$value =~ m/\{\'([\w:\/\\\.\-]+)\'\}/;
			$device=$1;
			my(@filespec)=split(/\./,basename($device));
			$filetime=$filespec[3]." ".$filespec[4];
		} elsif( $key eq "fullbackupLSN" ) {
			$lastlsn=$value;
			$lastlsn=~s/\s//g;
		} elsif( $key eq "file list" ) {
		} elsif( $key eq "TYPE=DISK" ) {
			$value =~ s/^..//;
			$value =~ s/\'\}//;
			# $value =~ m/\{\'([\w:\/\.\-]+)\'\}/;
			$device=$value;
			$device=~ s.\\.\/.g;
			my(@filespec)=split(/\./,basename($device));
			$filetime=$filespec[3]." ".$filespec[4];
		} else {
			error_out( "ILLEGAL KEY/VALUE => k=$key v=$value" );
		}
		$i++;
	}
	return($op,$db,$crdt,$firstlsn,$lastlsn,$device,$filetime,$pages);
}

sub to_dhm {
	my($t)= @_;
	return "" unless defined $t;
	return "" if $t>1000000;
	my $d= int($t/(24*3600)); $t-=$d*(24*3600);
	my $h= int($t/3600); $t-=$h*3600;
	my $m= int($t/60); $t-=$m*60;
	return "(".$d."D ".$h."H ".$m."M)";
}

# for perldoc - todo - you could break out of log scans when data collected

1;

__END__

=head1 NAME

win32_backupreport.pl - SQL Server Backup Report

=head2 DESCRIPTION

Scans the event log and creates a backup and restore history for a server.
There are two types of reports here.  The first is the Backup Report, which
simply prints backup details for a server.  The second is a Restore report
which is more cmoplicated and shows restore mappings to the primary server

=head2 USAGE

Usage: PC_BA~RP.PL --OUTFILE=file --HTMLFILE=file --SYSTEM=system --HOURS=hours --USER=sa --PASSWORD=password --SRVNAME=ODBCID --NOSTDOUT --REPORT=<backup|restore> --PRIMARY=<primary host if restore> --DEBUG --HTMLDETAIL=file --LISTEVENTS

=head2 EXAMPLE

To see whats going on in a server

   win32_backupreport.pl --SYSTEM=S1 --HOURS=96

To put results in a file

   win32_backupreport.pl --SYSTEM=S1 --HOURS=96 --NOSTDOUT --OUTFILE=my_report.txt

Since the ascii report sucks - an heml file can be generated by

   win32_backupreport.pl --SYSTEM=S1 --HOURS=96 --NOSTDOUT --HTMLFILE=my_report.html

If you are using log shipping, a new report opens up.  Say your server is
BACKUP1 which does log loads from PRIMARY1

   win32_backupreport.pl --SYSTEM=BACKUP1 --PRIMARY=PRIMARY1 --HOURS=96 --NOSTDOUT
     --HTMLFILE=main.html --REPORT=restore --HTMLDETAIL=details.html

The above creates a linked pair of reports - with a summary in HTMLFILE and
details of the shipping and loading in HTMLDETAIL.  Note that some times
your report will be out of sync... the Behind time explains this - its possible
that the lsn's dont match up intentionally.

=end

