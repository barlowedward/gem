#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2004 By Edward Barlow and Lars Karlsson
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

#------------------------------------------------------------
# Copyright: Lars Karlsson, Stockholm, Sweden, 1997-09-13
#                       mail:bergkarl@algonet.se,
#                       url :www.algonet.se/~bergkarl/lasse
#
# License:  You are allowed to use/modify/distribute this script as
#                               long as you do not remove this copyright and license info
#                               and as long as you mark out your changes in the script
#                               and mail a copy of your changes to me.
#
# History:  1.0 by Lars Karlsson, 1997-09-13
#                       1.1 by Edward Barlow
#                       1.2 by Lars Karlsson, 1998-01-01
#                       1.3 by Edward Barlow (complete rewrite)
#                       1.4 by Edward Barlow (major rewrite)		 Fri Dec 17 12:55:09 EST 2004
#------------------------------------------------------------

# ===========================
# Create reserved word list
# ===========================
my(%wordlist) =(
                  'and'         =>'where',
                  'create'      =>'create',
                  'default'     =>'+default',
                  'delete'      =>'delete',
                  'exec'                =>'exec',
                  'execute'     =>'exec',
                  'from'                =>'+from',
                  'go'          =>'exec',
                  'insert'      =>'insert',
                  'into'                =>'+into',
                  'proc'                =>'+proc',
                  'procedure'=>'+proc',
                  'rule'                =>'+rule',
                  'select'      =>'select',
                  'table'       =>'+table',
                  'truncate'=>'truncate',
                  'trigger'     =>'+trigger',
                  'update'      =>'update',
                  'view'                =>'+view',
                  'where'       =>'where',
);

my %obj_desc=(
        "U"=>"table",
        "P"=>"procedure",
        "S"=>"system_table",
        "V"=>"view",
        "TR"=>"trigger",
        "D"=>"default",
        "XP"=>"ext. proc",
        "R"=>"rule"
);

use strict;
use Repository;
use CommonFunc;
use DBIFunc;
use File::Basename;
my($dbproc);
$|=1;

my($progname)=basename($0);
sub usage
{
        die "@_\nSyntax:  $progname -S server -U user -P password [-D database] [-H output directory ] [-I \$SYBASE] [-q]\n\t-q for quiet mode\n\t-Tsybase|sqlsvr\n";
}

use Getopt::Std;
use vars qw($opt_d $opt_D $opt_S $opt_U $opt_P $opt_q $opt_I $opt_H $opt_R $opt_T);

my($args)=join(' ',@ARGV);

usage("Bad Parameter List (only dSUDIHRPA)")
        if( ! getopts('qdS:U:D:I:H:P:RT:') );

my($curdir)=dirname($0);
my($is_system_10)=0;

my($TYPE)=$opt_T || "sybase";
$opt_H= get_gem_root_dir()."/data/depends" if ! $opt_H and -d get_gem_root_dir()."/data/depends";
$opt_H = "." if ! defined $opt_H;
usage("Output Directory Must Be Passed")       if ! defined $opt_H;

print "depends_analyze.pl : run at ".localtime(time)."\n";
print "OUTPUT DIRECTORY=$opt_H\n";

$ENV{'SYBASE'}  = $opt_I if defined $opt_I;

# $debug_object is used to trace one object to see why it does not work
# Used for development/test with the debug flag -d.
#my($debug_object)="dbo.mc_getcmnandmember_sp";
#if( defined $opt_d ) {
#        statusmsg( "DEBUG MODE ACTIVE - analyzing $debug_object in sybsystemprocs");
#        $opt_D="master" unless defined $opt_D;
#}

if( defined $opt_S ) {
	do_analysis($TYPE,$opt_S,$opt_D,$opt_U,$opt_P);
}  else {
	if( $opt_T eq "sybase" or ! $opt_T ) {
		print "Working On Sybase Servers\n";
		my(@servers) = get_password(-name=>$opt_S, -type=>"sybase");
		foreach( @servers) {
			statusmsg( "\n" );
			statusmsg( "\n" );
			statusmsg( "*** Working on server $_ ***" );
			statusmsg( "\n" );
			do_analysis("sybase",$_);
		}
	}
	if( is_nt() ) {
		if( $opt_T eq "sqlsvr" or ! $opt_T ) {
			print "Working On Sql Servers\n";
			my(@servers) = get_password(-name=>$opt_S, -type=>"sqlsvr");
			foreach( @servers) {
				statusmsg( "\n" );
				statusmsg( "\n" );
				statusmsg( "*** Working on server $_ ***" );
				statusmsg( "\n" );
				do_analysis("sqlsvr",$_);
			}
		}
	}
	if( $opt_R ) {
		print "EXEC: $^X $curdir/depends_get.pl -hR\n";
		system("$^X $curdir/depends_get.pl -hR");
	}
}
statusmsg( "Completed Analysis" );
exit();

# ========================================================================
#
# DONE
#
# ========================================================================

#
# CREATE OUTPUT FILE OF FORMAT
#    obj|type|verb:obj|verb:obj
#
# DESCRIPTION OF VARIABLES
#
# - @dblist is a list of non system databases in the system
# - %obj_type is a list of types by object and user.object
# - %obj_text is the text from syscomments for each user.object
# - %del_trigger{$obj} - delete trigger (also ins and upd)
# - %relate{$user.$obj} contains depends information
#   pipe separated blurbs of the form |trig_name:insertTRigger
#   or object:select+from+table...
# - %constraint{owner.table} = $owner.table (TABLE) REFERENCE $reftable | $table
# - %reformatted{owner.object} = formatted output = pipe separated
sub do_analysis{
	my($TYPE, $DSQUERY, $DB, $USR, $PASSWD)=@_;
	my(%del_trigger,%ins_trigger,%upd_trigger,%obj_type,%obj_text,%relate,%constraint,@dblist,%reformatted,$SERVER_TYPE);

	($USR,$PASSWD) = get_password(-name=>$DSQUERY, -type=>$TYPE) if ! $PASSWD or ! $USR;

	usage("Server Must Be Passed")          if ! defined $DSQUERY ;
	usage("User Must Be Passed: ",join(" ",@_))            if ! defined $USR ;
	usage("Password Must Be Passed")        if ! defined $PASSWD;
	usage("Sybase Env Vbl Not Set")         if ! defined $ENV{'SYBASE'};

	my($sql);
	my($dbi_type)="Sybase" if $TYPE eq "sybase";
	$dbi_type="ODBC" if lc($TYPE) eq "sqlsvr" or is_nt();
	$dbi_type="ODBC" if is_nt();

   my($rc)=dbi_connect(-srv=>$DSQUERY,-login=>$USR,-password=>$PASSWD,-type=>ucfirst($TYPE));
	if( ! $rc ) {
        	print "CANT CONNECT TO DATABASE SERVER $DSQUERY\n";
			return;
	}

# ===========================
# run query to get databases if no db passed
# ===========================
if( ! defined $DB ) {
        statusmsg( "Extracting Database List" );
        ($SERVER_TYPE,@dblist)=dbi_parse_opt_D(undef,1,1);
#        $sql="select name from master..sysdatabases  where status & 0x1120 = 0 and status2&0x0030=0";
#
#        foreach ( dbi_query(-db=>"master",-query=>$sql) ) {
#                my($val) = dbi_decode_row($_);
#                $is_system_10=1 if $val eq "sybsystemprocs";
#                push @dblist,$val;
#        }
} else {
		@dblist=($DB);
}

if( $#dblist < 0 ) {
	statusmsg( "No usable, non system, databases found for $DSQUERY, $DB.  Aborting Analysis\n" );
	dbi_disconnect();
	return;
}
statusmsg( "DBLIST is ".join(" ",@dblist)."\n") if defined $opt_d;

# %obj_type key-$object			value=$type
#           key-$user.$object value=$type
# %objtext  key-$user.$object value=text from syscomments
#				foreach (WORD)
#					if in WORDLIST - set $query_type
#						$relate{$key} .= |$word:$query_type
# %XXX_trigger key-$user.$object value=name of trigger
# %XXX_trigger key-$object value=name of trigger


# %reformatted{$DB..$object} = $obj_type |

foreach my $curdb (@dblist) {
        $DB=$curdb;
        statusmsg( "*** Working on $SERVER_TYPE Database $DB ***" );
        next if ! dbi_use_database($DB);

        undef %obj_type;
        undef %relate;
        undef %constraint;
        undef %obj_text;
        undef %del_trigger;
        undef %ins_trigger;
        undef %upd_trigger;

        # ===========================
        # POPULATE %objects hash with types by $name and $user.$name
        # ===========================
        statusmsg( "Getting Object List" );
        $sql="select name, type, user_name(uid) from sysobjects";
        foreach (dbi_query(-db=>$DB,-query=>$sql)) {
        			s/\s//g;
               my($object,$type,$user) = dbi_decode_row($_);
        			next unless defined $user;
        			next unless $object;
        			$obj_type{"$user.$object"}=$type;
        			$obj_type{"$object"}=$type;

               # FIRST OUTPUT ELEMENT IS OBJECT TYPE .  MUST DO IT THIS WAY
               # THOUGH BECAUSE REFORMATTED COULD BE SET BY CROSS DB JOINS
               if( defined $reformatted{$DB."..".$object} ) {
                        $reformatted{$DB."..".$object}=$obj_type{$object}."|".$reformatted{$DB."..".$object};
               } else {
                        $reformatted{$DB."..".$object}=$obj_type{$object}."|";
               }
        }

        # ===========================
        # POPULATE %obj_text{$user.$name} hash with stuff from syscomments
        # ===========================
        statusmsg("Getting Object Text");
        $sql="select o.name,user_name(uid),text from dbo.syscomments c, dbo.sysobjects o where o.id=c.id";
        foreach (dbi_query(-db=>$DB,-query=>$sql)) {
                my( $object, $user, $text) = dbi_decode_row($_);
                $obj_text{$user.".".$object}.=$text;
        }

        # ===========================
        # POPULATE %relate{$user.$object} with pipe separated trigger Information
        # of the form |trig_name:insertTRigger (concatonated together)
        # ===========================
        statusmsg( "Getting Triggers" );
        $sql="select name, user_name(uid), object_name(instrig), object_name(updtrig), object_name(deltrig) from sysobjects where type = 'U'";

        foreach ( dbi_query(-db=>$DB,-query=>$sql) ) {
                s/\s//g;
                my($object,$user,$instrig,$updtrig,$deltrig)=dbi_decode_row($_);
                my($x)="$user.$object";
                        $ins_trigger{$x}=$instrig if $instrig ne "NULL";
                        $upd_trigger{$x}=$updtrig if $updtrig ne "NULL";
                        $del_trigger{$x}=$deltrig if $deltrig ne "NULL";
                        $ins_trigger{$object}=$instrig if $instrig ne "NULL";
                        $upd_trigger{$object}=$updtrig if $updtrig ne "NULL";
                        $del_trigger{$object}=$deltrig if $deltrig ne "NULL";
        }

        # ===========================
        # POPULATE %Constraint{dbo.$table} = "| $table (fk ref)
        # Handle all constraint fk referencies too
        # These relations should be marked out,
        # since it is another type of dependency, eg not a flow.
        # ===========================
        if( $is_system_10 ) {
                statusmsg("Getting Constraints");
                $sql="select object_name(tableid),object_name(reftabid) from sysreferences";

                foreach ( dbi_query(-db=>$DB,-query=>$sql) ) {
                my($table, $reftable) = dbi_decode_row($_);
                next unless $table !~ "^[_a-zA-Z]";
                my($x)="dbo.$table";
                if ($constraint{$x}) {
                                $constraint{$x}.=" | $reftable ";
                } else {
                                $constraint{$x}="$x (TABLE) REFERENCE $reftable ";
                }
                }
        }

        # ===========================
        # Now we analyze text building an associative array per object
        # $relate {$object} will hold info about all relate objects
        # in triggers, views and procedures
        # ===========================
        statusmsg("Analyzing Object Text - $DSQUERY, $DB \n");
        foreach my $key (keys %obj_text) {
					print ".";

					# GET THE TEXT OF THE OBJECT
					#   - remove headings
					# shorten out keywords, numbers, comments, strings, & punctuation etc..
					# essentially we only want sql key words & objects & control to remain
        			$_ = $obj_text{$key};

#        			s/\s\s+/ /g;		# multiple white space
#        			s/^\s+//g;			# leading white space
#        			s/\s+$//g;			# trailing white space

               statusmsg( "Starting SQL Text Analysis of $DSQUERY.$DB..$key","\n") if defined $opt_d;

               # Remove -- comments
        			s/\-\-[\w\W]*?\n//g;

        			# Remove /* comments
        			s~\/\*[\w\W\^\d\D\s]*?\*\/~~g;

               statusmsg( "LINE ",__LINE__," UnParsed SQL Text=".join(" ",split(/\s+/,$_) ),"\n") if defined $opt_d;

               # remove some headings if it is a trigger
               s/create[\w\W\s]+?as\s+begin// if $obj_type{$key} eq "TR";

               # shorten our string a bit by taking out some key words
               #\bend\b|
               s/\bif\b|\bthen\b|\belse\b|\bbegin\b|\bas\b|\bin\b|\bcursor\b|\blike\b|\breturn\b|\bint\b|\bfloat\b|\bdouble\b|\bdatetime\b|\bchar\b|\bdeclare\b|\bset\b|\bnull\b|\bconvert\b|\breturn\b|\btext\b|\bvarchar\b|\bimage\b|\bexit\b|\bgetdate\b|\bNULL\b|\bcount\b|\bvalues\b|\border\s+by\b//g;

               # remove words or != or , or = or ( or )
               s~\@+\w+\b|\!\=|\,|\=|\(|\)~ ~g;

               # remove numbers
               s/\b\d+\b//g;


        			s/\sdbo\.//g;			# Remove hanging dbo.
        			s/\.dbo\./\.\./g;		# Remove hanging .dbo. and replace with ..

        			# Remove strings
        			s/\"[\w\W]*?\"//g;
        			s/\'[\w\W]*?\'//g;

               # Remove Punctuation Marks
               s~\^|\&|\$|\+|\*|\/|\\|\-~~g;

               s/\s\s+/ /g;		# multiple white space
        			s/^\s+//g;			# leading white space
        			s/\s+$//g;			# trailing white space

               statusmsg( "LINE ",__LINE__," Parsed SQL Text=".join(" ",split(/\s+/,$_) ),"\n") if defined $opt_d;

               # REOVE CASE STATEMENTS FROM STUFF
               s/\bCASE\b.+\bEND\b//gi;

                # FOREACH WORD
                my $query_type="";
                foreach my $word ( split(/\s+/,$_) ) {;
                         my $lower;
                         ($lower = $word) =~ tr/[A-Z]/[a-z]/;

                        printf( " %5s word=%10s querytype=%s\n",__LINE__,$lower,$query_type)
                                if defined $opt_d;

                         # SEE IF WORD IS A RESERVED WORD
                         if (defined $wordlist{$lower}) {
                                if ($wordlist{$lower} =~ /^\+/) {
                                   $query_type.=$wordlist{$lower};
                                } else {
                                   $query_type=$wordlist{$lower};
                                }
                                printf( " %5s word=%10s querytype=%s %s\n",__LINE__,$lower,"$lower is reserved word")
                                		if defined $opt_d;

                         # WORD IS AN OBJECT
                         } elsif ($obj_type{$word} or $obj_type{"dbo.".$word}) {

                                # ignore when table exists in where clause
                                # statusmsg( "\tignoring $word as found in where clause")
                                #        if defined $opt_d and $query_type eq "where";
                                printf( " %5s word=%10s querytype=%s %s\n",__LINE__,$lower,"ignoring $word as found in where clause")
                                		if defined $opt_d and $query_type eq "where";

                                next if $query_type eq "where";

                                # in case field with name of table in select clause
                                if($query_type eq "select"){
                                			printf( " %5s word=%10s querytype=%s %s\n",__LINE__,$lower,"ignoring $word as found in select clause")
                                				if defined $opt_d;

                                        #statusmsg( "\tignoring $word as found in select clause")
                                        #        if defined $opt_d;
                                        next ;
                                }

                                if(($key =~ /\.$word$/ && $query_type =~ /^create/)){
                                			printf( " %5s word=%10s querytype=%s %s\n",__LINE__,$lower,"ignoring $word as create stmt")
                                				if defined $opt_d;

                                        #statusmsg( "\tignoring $word as create stmt")
                                        #        if defined $opt_d;
                                        next ;
                                }

                                $relate{$key}.="|$word:$query_type+".$obj_desc{$obj_type{$word}};
                                printf( " %5s word=%10s querytype=%s %s\n",__LINE__,$lower,"SAVING RELATIONSHIP=$word:$query_type+$obj_desc{$obj_type{$word}}")
                                				if defined $opt_d;

                                #statusmsg( "SAVING RELATIONSHIP=$word:$query_type+$obj_desc{$obj_type{$word}} ")
                                 #       if defined $opt_d;

                         # OBJECT WITH FULL PATH REFERENCE
                         #  EITHER xxx.dbo.yyyy or xxx..yyy
                         } elsif ($word =~ /\b\w+\.(dbo)?\.\w+\b/) {
                                # replace .. with .dbo. for consistency
                                $word =~ s/\.\./.dbo./g;
                                if( defined $opt_d and ($query_type eq "where" or $query_type eq 'create' or $query_type eq 'select' )) {
                                		statusmsg( "DBG: IGNORING $word as it appears to be part of a $query_type clause" );
												next;
										  }
                                $relate{$key}.="|$word:$query_type ".$obj_type{$word};
                                #statusmsg( "\tFOUND DEPENDENT OBJECT ($word) relate{$key}=$word:$query_type $obj_type{$word}") if defined $opt_d;
                                printf( " %5s word=%10s querytype=%s %s\n",__LINE__,$lower,"FOUND DEPENDENT OBJECT ($word) relate{$key}=$word:$query_type $obj_type{$word}")
                                				if defined $opt_d;
                         }
                }
        }
        print "\n";
        statusmsg( "Done Analyzing Object Text" );
		  # statusmsg( "Summary Relationship=".$relate{$debug_object}."\n") if defined $opt_d;

		  # foreach (keys %obj_type ) { print "Obj Type $_ => $obj_type{$_}\n"; }
		  # foreach (keys %relate ) { print "Relationship $_\n"; }
#       if( defined $opt_d ) {
#               foreach (keys %reformatted) {
#                       print __LINE__," REFORMATTED ",$_." => ".$reformatted{$_},"\n";
#               }
#       }

        # ===========================
        # Reformat Output()
        # What we want is all relationships for each object
        # so if a calls b calls c then we should end up with
        #   a (type) -> b
        #   b (type) -> c
        #   a (type) -> b -> c
        #  where -> is replaced by calls, triggers, or whatever
        # ===========================
        statusmsg( "Reformatting Output" );
        foreach my $cur_obj (keys %relate) {
           		##format_output($_);
   				#format_output($_,$DB,\%reformatted,\%obj_type,\%ins_trigger,\%upd_trigger,\%del_trigger,\%relate);
					# ===========================
					# Now we have all dependencies, let's create a dependency tree
					#       $reformatted{$full_obj_name}=tree_formatted_for_output
					# ===========================
					#sub format_output {
						 #my ($cur_obj,$DB,$reformat_ptr,$objtype_ptr,$itrg_ptr,$utrg_ptr,$dtrg_ptr,$relate_ptr)=@_;
						 #my(%reformatted)=%$reformat_ptr;
						 #my(%obj_type)=%$objtype_ptr;
						 #my(%ins_trigger)=%$itrg_ptr;
						 #my(%upd_trigger)=%$utrg_ptr;
						 #my(%del_trigger)=%$dtrg_ptr;
						 #my(%relate)=%$relate_ptr;

		  					statusmsg( "Reformatting $cur_obj=".$relate{$cur_obj}."\n") if defined $opt_d;

						 # Remove pipes at start and end
						 $relate{$cur_obj} =~ s/^\s*\|//;
						 $relate{$cur_obj} =~ s/\|\s*$//;

						 return unless defined $relate{$cur_obj};

						 print "reformatting($cur_obj): $relate{$cur_obj}\n" if defined $opt_d;

								# FOR EACH DEPENDENCY YOU HAVE FOUND ON OBJECT
								#        create dependency output report in %reformatted
								#        do it both ways
								#         if $cur_obj (object of interest) is a proc
								#               -> if object is a table then $cur_obj does something to table (SIUD)
								#               -> if object is a proc  then $cur_obj executes $dep_obj
								#         else
								#                       -> so $cur_obj is a proc we put b executed by
								#                       -> if $cur_obj is a table/view and we put b is used by
								#                       -> if table operation then do triggers too
								#
					 #       if( defined $opt_d ) {
					 #               foreach (keys %reformatted) {
					 #                       print __LINE__," REFORMATTED ",$_." => ".$reformatted{$_},"\n";
					 #               }
					 #       }

						 my %found=();
						 foreach (split('\|',$relate{$cur_obj})) {
										  my($dep_obj,$operation)=split(/:/,$_);
										  next unless defined $dep_obj or $dep_obj eq "";

										  # MAKE BOTH OBJECTS xyz_db..abc FORMAT
										  #  - remove dbo if it exists
										  $dep_obj =~ s/dbo\./\./g;
										  $cur_obj =~ s/dbo\./\./g;

										  #  - remove starting period
										  $dep_obj =~ s/^\.//;
										  $cur_obj =~ s/^\.//;

										  # reformat unless there are databases
										  $dep_obj = $DB."..".$dep_obj unless $dep_obj =~ /\./;
										  $cur_obj = $DB."..".$cur_obj unless $cur_obj =~ /\./;

										  $operation =~ s/\+/ /g;

										  $reformatted{$dep_obj} = ""
													 unless defined $reformatted{$dep_obj};
										  $reformatted{$cur_obj} = ""
													 unless defined $reformatted{$cur_obj};

										  if( $obj_type{$dep_obj} eq "P" and $obj_type{$cur_obj} eq "P"
										  or  $operation =~ /^exec/ ) {
																next if defined $found{$dep_obj.$cur_obj."E"};
																$found{$dep_obj.$cur_obj."E"}=1;

													 $reformatted{$dep_obj} .= "executed by:".$cur_obj."|";
													 $reformatted{$cur_obj} .= "executes:".$dep_obj."|";
										  } elsif( $operation =~ /update/ ) {
																next if defined $found{$dep_obj.$cur_obj."U"};
																$found{$dep_obj.$cur_obj."U"}=1;

													 $reformatted{$dep_obj} .= "updated by:".$cur_obj."|";
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj."|"
																unless defined $upd_trigger{$cur_obj};
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj.":triggers:".$upd_trigger{$cur_obj}."|"
																if defined $upd_trigger{$cur_obj};
										  } elsif( $operation =~ /insert/ ) {
																next if defined $found{$dep_obj.$cur_obj."I"};
																$found{$dep_obj.$cur_obj."I"}=1;

													 $reformatted{$dep_obj} .= "inserted by:".$cur_obj."|";
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj."|"
																unless defined $ins_trigger{$cur_obj};
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj.":triggers:".$ins_trigger{$cur_obj}."|"
																if defined $upd_trigger{$cur_obj};
										  } elsif( $operation =~ /delete/ ) {
																next if defined $found{$dep_obj.$cur_obj."D"};
																$found{$dep_obj.$cur_obj."D"}=1;

													 $reformatted{$dep_obj} .= "deleted by:".$cur_obj."|";
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj."|"
																unless defined $del_trigger{$cur_obj};
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj.":triggers:".$del_trigger{$cur_obj}."|"
																if defined $upd_trigger{$cur_obj};

										  } elsif( $operation =~ /select/ ) {
																next if defined $found{$dep_obj.$cur_obj."S"};
																$found{$dep_obj.$cur_obj."S"}=1;

													 $reformatted{$dep_obj} .= "select by:".$cur_obj."|";
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj."|";

										  # MUST HANDLE SUBQUERIES HERE
										  #  this is not exactly correct because it could be an insert
										  #  but if there is a where and a from in the operation
										  #  treat it as a select from
										  #  if type = "where+from" then its a subquery
										  } elsif( $operation =~ /where/ and $operation =~ /from/ ) {
																next if defined $found{$dep_obj.$cur_obj."S"};
																$found{$dep_obj.$cur_obj."S"}=1;

													 $reformatted{$dep_obj} .= "select by:".$cur_obj."|";
													 $reformatted{$cur_obj} .= "select from:".$dep_obj."|";

										  # IGNORE create+table operations... they are not possible
										  } elsif( $operation =~ /truncate\s+table/ ) {
																next if defined $found{$dep_obj.$cur_obj."T"};
																$found{$dep_obj.$cur_obj."T"}=1;

													 $reformatted{$dep_obj} .= "truncated by:".$cur_obj."|";
													 $reformatted{$cur_obj} .= $operation.":".$dep_obj."|";
										  } elsif( $operation =~ /create[\+\s]table/ ) {
													 next;
										  } elsif( $operation =~ /^\s*$/ ) {
													 next;
										  } elsif( $operation =~ /^create/ ) {
													 next;
										  } elsif( $cur_obj eq $dep_obj ) {
													 next;
										  } else {
													 print "Unknown operation $operation for object  $cur_obj\n\tdependant_object=$dep_obj \n\tinput=$_ \n";
													 #print "THis indicates a development error and you should contact SQL Technologies support\n";
													 die "DONE\n" if defined $opt_d;
										  }
						 }
					 #}
        }

        #statusmsg( "Analyzing Constraints" );
        #foreach (keys %constraint) {
           #$reformatted{"$_/_"}=$constraint{$_};
        #}

        statusmsg( "Completed $DB" );
}

#if( defined $opt_d ) {
#       foreach (keys %reformatted) {
#               print __LINE__," REFORMATTED ",$_." => ".$reformatted{$_},"\n";
#       }
#}

  		 die "DSQUERY NOT DEFINED"
			unless defined $DSQUERY;
		   die "opt_H NOT DEFINED" unless defined $opt_H;
       my($outfile)=$opt_H."/depends.".$DSQUERY;
       statusmsg( "Output in $outfile" );

       mkdir("$opt_H",0755) && statusmsg( "Created $opt_H")
                unless -d "$opt_H";

       open( DEPENDENCE,">$outfile" ) or die "Fatal error.  aborting analysis\nCant write output file $outfile $!";
       foreach (sort keys %reformatted) {
        		$reformatted{$_} =~ s/\|$//;
            print DEPENDENCE $_."|".$reformatted{$_},"\n";
            print "SAVING LINE=",$_."|".$reformatted{$_},"\n" if defined $opt_d;
       }
       close DEPENDENCE;
		 chmod(0666, $outfile);

		 dbi_disconnect();
}

sub statusmsg
{
        my($msg)=join("",@_);
        printf( "TIME=%-10d %s\n", time-$^T, $msg);
}

__END__

=head1 NAME

depends_analyze.pl - sybase dependency analyzer

=head2 AUTHOR

        V1.0    1997-09-13 By Lars Karlsson bergkarl@algonet.se

        V1.1    1997-12-1   Ed Barlow did a major modification/cleanup job
                                - build_db No longer Perl4 compatible, but more standard
                                - Using Getopts for arguments
                           - Making it a Package (gzip distribution)

        V1.2    1998-01-01 After getting the code from Edward
                        - I did some rewriting, bug fixes
        V1.3  1999-02-10
                                - Complete Rewrite By EMB

=head2 FILES

=over 4

=item * depends_get.pl

Script to extract and prettyprint dependencies

=item * depends_analyze.pl

Script to build output file

=back

=head2 SYNTAX

perl depends_analyze.pl -S server -U user -P password [-D database] [-H output directory ] [-I $SYBASE] [-q]
        -q for quiet mode
        -Tsybase|sqlsvr

=head2 DESCRIPTION

This script extracts relationships (dependencies) for a server.  This can be used for impact analysis (show me
the impact if i change an object).  It also works to help understand all relationships between triggers, procedures,
tables, views.

=head2 DETAILS

Output File Named is created in directory named depends.\$DSQUERY

=head2 FEATURES

Hides password from most ps sniffers.

Handles references to objects in other databases.

This script is not built on sysdepends, the unreliable table, but on analysing the objects' texts.

Smart enough to follow trigger only if apropriate.

Handles the problem with sp_helptext sometimes truncating spaces.

=head2 BUGS

Constraints Not Implemented.

update #tr
set PX_LAST=isnull(b.PX_LAST,-1)
from #tr a, riskdb..BLOOMBERG_SECMSTR b
where a.ID_BB_UNIQUE = b.ID_BB_UNIQUE

Parses Wrongly

=cut
