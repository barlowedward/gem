README FOR THE DATABASE LAYOUT

*** TRIGGERS
IgnoreList has a trigger that (for the row in question)
	update 	Heartbeat	set 		internal_state='D'
	update 	Event			set 		internal_state='D'

***PROCEDURES
Backup_proc
	maintains BackupState table
	update Heartbeat set 	state='OK' where monitor_program='HourlyBackupReport'

clear_possible_alarms
	update Heartbeat set internal_state='C' where internal_state='P'
	update Event     set internal_state='C' where internal_state='P'
	delete Blackout where enddate<getdate()

Event_Proc
	Insert a new event unless in IgnoreList or Blackout

get_alarm_version
	returns a version string

get_possible_alarms
	can be run with @test_mode which just shows stuff 	TEST MODE MUST BE N,H, or E'
	set internal_state from 'I'nitial to 'P'rocessing
	uses VH_ALARMABLE, Notification, Event_filter join
	only for GEM_servers which are PRODUCTION or DR

heartbeat_proc
	Insert a new heartbeat unless in IgnoreList or Blackout with state=I

