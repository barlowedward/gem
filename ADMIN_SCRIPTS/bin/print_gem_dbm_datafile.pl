#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2006-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com

use lib qw(/apps/sybmon/dev/lib /apps/sybmon/lib G:/dev/lib G:/dev/apps/sybmon/lib G:/dev/Win32_perl_lib_5_8);
use Data::Dumper;
use DBM::Deep;
use Repository;

my($gemfile) = get_gem_root_dir()."/conf/gem.dbm";
print "GEM Data File is $gemfile\n";
die "Woah - That file does not exist\n" unless -e $gemfile;
die "Woah - That file is not readable\n" unless -r $gemfile;
my($gemdata) = DBM::Deep->new( $gemfile );

#delete $gemdata->{sybase};
print Dumper \$gemdata;
exit();

#my(@k) = keys %$gemdata;
#foreach ( @k ) {
#	print "\n*** ",$_," ***\n";
#	print "value=",Dumper $gemdata->{$_},"\n" if $_ eq "sybase";
#}

my(@k) = keys %{$gemdata->{sybase}};
foreach ( @k ) {
	print "\n*** ",$_," ***\n";
	print "value=",Dumper $gemdata->{sybase}->{$_},"\n";
}
