#!/usr/local/bin/perl-5.8.2

BEGIN { $ENV{'LANG'} = 'C'; };

use strict;
use DBI;
use Getopt::Std;
use Carp;

sub usage {
	use File::Basename;
	die "INCORRECT USAGE OF PROGRAM 
	perl ".basename($0)." -U USERNAME -S SERVERNAME -P PASSWORD -D DATABASE -Q QUERY [-OMS]
	-s is required for sybase connections (default)
	-m is required for mysql connections
	-o is required for oracle connections\n";
}
use vars qw ($opt_U $opt_P $opt_S $opt_D $opt_d $opt_q $opt_Q $opt_o $opt_s $opt_m);
usage() unless getopts('U:P:S:D:dq:Q:osm');
usage() unless $opt_U and $opt_P and $opt_S;		# no need if native auth
usage() unless $opt_o or $opt_s or $opt_m or $opt_D;
$opt_q = $opt_Q if $opt_Q and ! $opt_q;

$opt_q = "exec sp__id" unless $opt_m or $opt_q;
$opt_q = "select USER()" unless $opt_q;			# mysql only

my($Driver)="Sybase";
$Driver	=	"mysql" if $opt_m;
$Driver	=	"oracle" if $opt_o;
$Driver	=	"ODBC" if $^O =~ /MSWin32/;

DBI->trace(8) if $opt_d;

# connect to database
#my $dbh = DBI->connect("dbi:$Driver:server=$opt_S", $opt_U, $opt_P) 
#		or die $DBI::errstr;

my $dsn = "dbi:$Driver:$opt_S";
#$dsn = "DBI:mysql:host=$opt_S;port=3306" if $opt_m;
$dsn = "DBI:mysql:host=$opt_S" if $Driver	eq "mysql";

print "Using $dsn\n";
my $dbh = DBI->connect( $dsn, $opt_U, $opt_P) or die $DBI::errstr;
		
#if( $opt_m ) {
#	print "DBGDBG","\n";
#	print "DS=",join(" ", DBI->data_sources("mysql")),"\n";
#	print "DS2=",
#		join(" ", DBI->data_sources("mysql",{"host"=>$opt_S,"user"=>$opt_U,"password"=>$opt_P,"port"=>3306})),
#		"\n";
#}
my $sth;
my $dat;

$dbh->do("use $opt_D") if $opt_D and ! $opt_o;
unless ($sth = $dbh -> prepare ($opt_q)) { 
	print("SQL command $opt_q prepare failed ");
	$dbh->disconnect;
	exit;
}

# excute the query
unless ( $sth -> execute ) { 
	print("SQL command $opt_q execute failed ");
	$dbh->disconnect;
	exit;
}
	 
while ( $dat = $sth -> fetchrow_arrayref ) {
	print "SUCCESS $opt_S ".join(" ",@$dat),"\n";
}

$sth ->finish;
$dbh->disconnect;
exit;

# perl ./GenericDBIQuery.pl -SSCPROD3 -Uoperator -Pdelph1 -O -Qselect USERNAME,MACHINE,PROGRAM,OSUSER from v$session where STATUS='ACTIVE' and PROGRAM!='ORACLE.EXE'
