#!/apps/perl/linux/perl-5.8.2/bin/perl

print "Content-type: text/html\n\n";

print "\nSHOWING ENVIRONMENT VARIABLES FOR THE SCRIPTS\n";
print "<TABLE BORDER=1>\n";
print "<TR><TH>VARIABLE</TH><TH ALIGN=LEFT>VALUE</TH></TR>\n";
foreach ( sort keys %ENV ) {
        if( /PASSWD/i or /PASSWORD/i ) {
                print "<TR><TD>$_</TD><TD>HIDDEN FIELD ON THIS FORM</TD></TR>\n";
        } else {
                print "<TR><TD>$_</TD><TD>$ENV{$_}</TD></TR>\n";
        }
}
print "</TABLE>\n";
