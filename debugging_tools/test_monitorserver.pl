# PROGRAM TO TEST monitorserver.dat
use lib qw(/apps/sybmon/dev/lib);

use Repository;
use Data::Dumper;

my(%x)=get_password(-type=>"monitor_server");
foreach (keys %x) {
	print "KEY=$_\n";
	print join(" ",get_password(-type=>"monitor_server", -name=>$_)),"\n" unless /HISTORICAL/;
}
