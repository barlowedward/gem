#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2004-6 By Edward Barlow

use strict;
use Getopt::Long;
use Repository;
use File::Basename;
use DBIFunc;
use MlpAlarm;
# use GemData;
use Data::Dumper;
use Sys::Hostname;
use CommonFunc;
use CommonHeader;

sub usage
{
	print @_;
	print "OneQueryPerServerReport.pl

Generic procedure runner.  Will run a single query on each of your servers, formatting output correctly.

REQUIRED ARGUMENTS
   -QUERY=QUERY

OPTIONAL ARGUMENTS
   -PROGRAMNAME=PROGRAMNAME   (default to BATCHID)
   -TITLE=TITLE
   -RAWDATA			 (only show raw data output - no glitz)
   -HTML
   -NOLOCK         (ignore lock file to prevent repeat runs)
   -BATCHID=batchid
   -NOHEADER       (ignore header rows from results)
   -PREREQ         (prerequisite query - only continue if this returns something)
   -NODUPS			 (ignore duplicate rows)
   -NOPRINTMSGS    (ignore messages from the server)
   -DEBUG          (debug)
   -NOLOCK
   -SYSTEMS=systems
   -SERVER=Server
   -ALLDATABASES (run in all databases - prepend db name)
   -PROGRAMNAME
   -PRINTSERVERLABEL   (pretty print a nice label for each server)
   -TYPE=(DB Type - sqlsvr / sybase / both)\n";
   return "\n";
}

# cd to the right directory
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

$| = 1;

my(@SQLRESULTS, @SYBASERESULTS, @ORARESULTS);
my($COLOR1, $COLOR2, $CURRENT_COLOR)=("beige","#FFCCCC","beige");
my($HEADER_COLOR)="brown";

my($NUMSYBCOLS,$NUMSQLCOLS,$NUMORACOLS);
use vars qw($opt_S $NOLOCK $NOPRINTMSGS $NODUPS $OUTFILE $QUERYFILE $RAWDATA $ALLDATABASES $ONEROWHEADER $NOTIMEOUT $PRINTSERVERLABEL $NOHEADER $NOSERVERPRINT $PREREQ $HTML $opt_d $opt_T $opt_B $opt_t $QUERY $PROGRAMNAME $TITLE);

GetOptions(
	"systems=s"				=>\$opt_S,
	"server=s"				=>\$opt_S,
	"HTML"					=>\$HTML,
	"NODUPS"					=>\$NODUPS,
	"RAWDATA"				=>\$RAWDATA,
	"ALLDATABASES"			=>\$ALLDATABASES,
	"NOSERVERPRINT"		=>\$NOSERVERPRINT,
	"PRINTSERVERLABEL"	=>\$PRINTSERVERLABEL,
	"NOHEADER"				=>\$NOHEADER,
	"ONEROWHEADER"			=>\$ONEROWHEADER,
	"NOTIMEOUT"				=>\$NOTIMEOUT,
	"NOLOCK"					=>\$NOLOCK,
	"NOPRINTMSGS"			=>\$NOPRINTMSGS,
	"debug" 					=>\$opt_d,
	"type=s"					=>\$opt_T,
	"QUERY=s"				=>\$QUERY,
	"QUERYFILE=s"			=>\$QUERYFILE,
	"OUTFILE=s"				=>\$OUTFILE,
	"PREREQ=s"				=>\$PREREQ,
	"PROGRAMNAME=s"		=>\$PROGRAMNAME,
	"TITLE=s"				=>\$TITLE,
	"batchid=s"				=>\$opt_B
	)
	or die usage( "syntax error\n" );

if( $QUERYFILE ) {
	if( ! -r $QUERYFILE ) {		die "Query File $QUERYFILE is unreadable\n";	}
	open(Q,$QUERYFILE) or die "Cant read Query File $QUERYFILE\n";
	$QUERY="";
	while(<Q>) {
		chomp;
		$QUERY.=$_."\n";
	}
	close(Q);
}

if( $OUTFILE ) {
	open(OUT, ">$OUTFILE") or die "cant write $OUTFILE $!\n";
}

$QUERY="exec sp__configure \@dont_format=\"y\"" unless $QUERY;
$PROGRAMNAME=$opt_B || "OneQueryPerServerReport.pl" unless $PROGRAMNAME;
$TITLE="SERVER CONFIGURATION REPORT" unless $TITLE;
alarm(0) if $NOTIMEOUT;

my(%DUPROWS);

if( ! $RAWDATA ) {
	OutputLine( "<h2>" ) 	if defined $HTML;
	OutputLine( $TITLE );
	OutputLine( "</h2>" ) 	if defined $HTML;

	OutputLine( "\nCreated by $PROGRAMNAME at ".localtime(time)." on host ".hostname()."\n" );
	OutputLine( "<p>" ) if defined $HTML;
	OutputLine( "\n" );
}

my($NL)="\n";
$NL="<br>\n" if $HTML;

die usage("--TYPE must be sybase/sqlsvr/both or oracle\n") if defined  $opt_T
	and $opt_T ne "sybase" and $opt_T ne "sqlsvr" and $opt_T ne "both" and $opt_T ne "oracle";

if( $opt_B ) {
	if( ! $NOLOCK ) {
		print "--NOLOCK IS NOT DEFINED!\n" if $opt_d;
		if( Am_I_Up(-debug=>$opt_d,	-prefix=>$opt_B) ) {
			my($fname,$secs,$thresh)=GetLockfile(-prefix=>$opt_B);
			print "Cant Start $PROGRAMNAME - it is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nLock File=$fname secs=$secs thresh=$thresh secs\n";
			exit(0);
		}
	} else {
		print "NOLOCK IS DEFINED!\n" if $opt_d;
	}
}

dbi_set_option(-print=>\&OnePrint);
dbi_set_web_page(0);
dbi_set_debug($opt_d);

MlpBatchJobStart(-BATCH_ID=>$opt_B) if $opt_B;

if( $opt_T eq "oracle" ) {
	my(@servers)=get_password(-type=>"oracle");
	print "Found ",($#servers+1)," Oracle Servers".$NL unless $opt_S or $RAWDATA;
	print br if $HTML;
	foreach my $server (@servers) {
		if( defined $opt_S ) {	next if $server ne $opt_S; 		}
		process_a_server($server,"oracle");
	}
}

if( ! defined $opt_T or $opt_T eq "sybase" or $opt_T eq "both" ) {
	my(@servers)=get_password(-type=>"sybase");
	print "Found ",($#servers+1)," Sybase Servers".$NL unless $opt_S or $RAWDATA;
	print br if $HTML;
	foreach my $server (@servers) {
		if( defined $opt_S ) {	next if $server ne $opt_S; 		}
		process_a_server($server,"sybase");
	}
}

if( ! defined $opt_T or $opt_T eq "sqlsvr" or $opt_T eq "both" ) {
	if( is_nt() ) {
		my(@servers)=get_password(-type=>"sqlsvr");
		print "Found ",($#servers+1)," Sql Servers".$NL unless defined $opt_S or $RAWDATA;
		foreach my $server (@servers) {
			print "Ignoring $server as it does not match $opt_S".$NL
				if defined $opt_d and defined $opt_S and $server ne $opt_S;
			next if defined $opt_S and $server ne $opt_S;
			process_a_server($server,"sqlsvr");
		}
	} else {
		print "Ignoring sql servers - this is not an NT box".$NL
			if defined $opt_T;
	}
}

sub OutputLine {
	if( $OUTFILE ) {
		print OUT @_;
	} else {
		print @_;
	}
}

if( $#ORARESULTS != -1 ) {
	$NUMORACOLS=10 unless $NUMORACOLS;
	my($l)="ORACLE Results: " if $#ORARESULTS != -1;
	if( $HTML ) {
		OutputLine( "<p>\n<TABLE BORDER=1 WIDTH=100%>\n<TR><TH BGCOLOR=$HEADER_COLOR COLSPAN=$NUMORACOLS ALIGN=CENTER>$l $TITLE</TH></TR>\n" );
		foreach (@ORARESULTS) { OutputLine( $_); }
		OutputLine( "</TABLE>" ) if $HTML;
	} else {
		OutputLine( "\nORACLE SERVER CONFIGURATION RESULTS\n" );
		OutputLine( join("\n",dbi_reformat_results(@ORARESULTS))."\n" );
	}
}

if( $#SQLRESULTS != -1 ) {
	$NUMSQLCOLS=10 unless $NUMSQLCOLS;
	my($l)="Sql Server Results: " if $#SYBASERESULTS != -1;
	if( $HTML ) {
		OutputLine( "<p>\n<TABLE BORDER=1 WIDTH=100%>\n<TR><TH BGCOLOR=$HEADER_COLOR COLSPAN=$NUMSQLCOLS ALIGN=CENTER>$l $TITLE</TH></TR>\n");
		foreach (@SQLRESULTS) { OutputLine($_); }
		OutputLine( "</TABLE>" ) if $HTML;
	} else {
		OutputLine( "\nSQL SERVER CONFIGURATION RESULTS\n" );
		OutputLine( join("\n",dbi_reformat_results(@SQLRESULTS))."\n" );
	}
}
if( $#SYBASERESULTS != -1 ) {
	$NUMSYBCOLS=10 unless $NUMSYBCOLS;
	my($l)="Sybase Results: " if $#SQLRESULTS != -1;
	if( $HTML ) {
		OutputLine( "<p>\n<TABLE BORDER=1 WIDTH=100%>\n<TR><TH BGCOLOR=$HEADER_COLOR COLSPAN=$NUMSYBCOLS ALIGN=CENTER>$l $TITLE</TH></TR>\n");
		foreach (@SYBASERESULTS) { OutputLine( $_ ); }
		OutputLine( "</TABLE>" ) if $HTML;
	} else {
		OutputLine( "\nSYBASE RESULTS\n" ) unless $RAWDATA;
		OutputLine( join("\n",dbi_reformat_results(@SYBASERESULTS))."\n");
	}
}

MlpBatchJobEnd()if $opt_B;
OutputLine( "Successful Completion at ".localtime(time)."".$NL ) unless $RAWDATA;
I_Am_Down(-prefix=>$opt_B) if $opt_B and ! $NOLOCK;
exit(0);

sub process_a_server
{
	my($server,$type)=@_;
	if( $PRINTSERVERLABEL ) {
		print $NL;
		print "=====================================================",$NL;
		printf "| RESULTS FOR SERVER %-30s |%s",$server,$NL;
		print "=====================================================",$NL;
	}

	print "Processing server $server of type $type".$NL if defined $opt_d;
	my($login,$password)=get_password(-type=>$type,-name=>$server);
	I_Am_Up(-prefix=>$opt_B)  if $opt_B and ! $NOLOCK;

	my($conntype);
	if( is_nt() or $type eq "sqlsvr" ) {
   		$conntype="ODBC";
	} elsif( $type eq "oracle" ) {
   		$conntype="Oracle";
	} else {
   		$conntype="Sybase";
	}
	print "\tConnection type is $conntype".$NL if $opt_d;

  	print "\tConnecting To $server...".$NL if $opt_d;
	if( ! dbi_connect(-srv=>$server,-login=>$login,-password=>$password, -type=>$conntype, -debug=>$opt_d, -quiet=>1) ) {
		print "Cant connect to $conntype $server as $login - continuing".$NL;
		return;
	}
	print "\tConnected to $server...".$NL if $opt_d;

	# THE NEXT BLOCK REDEFINES THE SYBASE HANDLER ONLY IF UR TALKING RAWDATA
	# THIS EFFECTIVELY WILL THROW AWAY THE OUTPUT IF RAWDATA IS PASSED AND DO NOTHING
	# IF IT IS NOT PASSED!  BE CAREFUL THATS WHAT U WANT
	sub my_sybase_handler {
		my ($error, $severity, $state, $line, $server, $proc, $error_msg) = @_;
		print "my_sybase_handler() $error, $severity, $state, $line, $server, $proc, $error_msg\n" if $opt_d;
		DBIFunc::sybase_handler(@_) unless $RAWDATA;
	}

	if( $conntype eq "Sybase" ) {
		my($dbp)=dbi_getdbh();
		$dbp->{syb_err_handler}  = \&my_sybase_handler;
	}

	my($ServerType,@DB);
	my($masterdb)="master";
	if( $type eq "oracle" ) {
		$masterdb = "";
		push @DB,"";
	} elsif( $ALLDATABASES ) {
		($ServerType,@DB)=dbi_parse_opt_D("%",1);
	} else {
		push @DB,"master";
	}

	if( $PREREQ ) {
		my($rc);		# if the row returns somethign
		foreach (dbi_query( -db=>$masterdb, -query=>$PREREQ)){
			$rc=1;
		}
		return unless $rc;
	}

	################## CUSTOM CODE HERE ######################
	my($rowid)=0;
	my(%cargs);
	$cargs{-print_hdr}=1 unless $NOHEADER;

	print "\tRunning $QUERY\n" if $opt_d;
	my(@RESULTS);
	foreach my $curdb (@DB) {
	foreach (dbi_query( -db=>$curdb, %cargs, -query=>$QUERY)){
		if( $rowid==0 and ! defined $NOHEADER ) {
			# if( $type eq "sqlsvr" ) {
				my(@x)=dbi_decode_row($_);
				$NUMSQLCOLS = $#x + 2 if $type eq "sqlsvr";
				$NUMSYBCOLS = $#x + 2 if $type eq "sybase";
				$NUMORACOLS = $#x + 2 if $type eq "oracle";
				if( $HTML ) {
					my($s)="<TR BGCOLOR=$HEADER_COLOR>";
					$s.= "<TH>SERVER</TH>\n\t" unless $NOSERVERPRINT;
					$s.= "<TH>DATABASE</TH>"  if $ALLDATABASES and ! $NOSERVERPRINT;
					$s.= "<TH>".	join("</TH>\n\t<TH>",@x)."</TH></TR>\n";
					print __LINE__,"$s<br>" if $opt_d;
					push @RESULTS, $s;
				} else {
					if( $ALLDATABASES and ! $NOSERVERPRINT ) {
						push @RESULTS, dbi_encode_row("SERVER","DATABASE",@x)
					} else {
						push @RESULTS, dbi_encode_row("SERVER",@x)
					}
				}
#			} else {
#				my(@x)=dbi_decode_row($_);
#				$NUMSYBCOLS = $#x + 2;
#				if( $HTML ) {
#					my($s);
#					$s="<TR BGCOLOR=$HEADER_COLOR>";
#					$s.="<TH>SERVER</TH>\n\t" unless $NOSERVERPRINT;
#					$s.= "<TH>DATABASE</TH>"  if $ALLDATABASES;
#					$s.="<TH>".join("</TH>\n\t<TH>",@x)."</TH></TR>\n";
#					print "Line ",__LINE__," $s<br>" if $opt_d;
#					push @RESULTS, $s;
#
#				} else {
##					my($s)="SERVER\t" unless $NOSERVERPRINT;
##					$s.=join("\t",@x)."\n";
##					print "Line ",__LINE__," $s" if $opt_d;
##					push @RESULTS, $s;
#					if( $ALLDATABASES ) {
#						push @RESULTS, dbi_encode_row("SERVER","DATABASE",@x)
#					} else {
#						push @RESULTS, dbi_encode_row("SERVER",@x)
#					}
#				}
#			}
			$rowid++;
		} else {
			if( $NODUPS ) {
				my($rid)=join(":",dbi_decode_row($_));
				next if $DUPROWS{$rid};
				$DUPROWS{$rid} = 1;
			}

			#if( $type eq "sqlsvr" ) {
				if( $HTML ) {
					my($s)="<TR BGCOLOR=$CURRENT_COLOR>\n";
					$s.= "<TD>$server</TD>\n" unless $NOSERVERPRINT;
					$s.= "<TH>DB=$curdb</TH>"  if $ALLDATABASES and ! $NOSERVERPRINT;
					$s.= "\t<TD>".join("</TD>\n\t<TD>",dbi_decode_row($_))."</TD></TR>\n";
					print "Line ",__LINE__," $s<br>" if $opt_d;
					push @RESULTS, $s;
				} else {#
					if( $ALLDATABASES and ! $NOSERVERPRINT ) {
						push @RESULTS, dbi_encode_row($server,$curdb, dbi_decode_row($_))
					} else {
						push @RESULTS, dbi_encode_row($server, dbi_decode_row($_))
					}
				}
			#} else {
#				if( $HTML ) {
#					my($s)="<TR BGCOLOR=$CURRENT_COLOR>\n";
#					$s.= "<TD>$server</TD>\n" unless $NOSERVERPRINT;
#					$s.= "<TH>DB=$curdb</TH>"  if $ALLDATABASES and ! $NOSERVERPRINT;
#					$s.= "\t<TD>".join("</TD>\n\t<TD>",dbi_decode_row($_))."</TD></TR>\n";
#					print "Line ",__LINE__," $s<br>" if $opt_d;
#					push @RESULTS, $s;
#				} else {
#					if( $ALLDATABASES and ! $NOSERVERPRINT ) {
#						push @RESULTS, dbi_encode_row($server,$curdb, dbi_decode_row($_))
#					} else {
#						push @RESULTS, dbi_encode_row($server, dbi_decode_row($_))
#					}
#				}
#			}
		}
	}
	}
	if( $type eq "sqlsvr" ) {
			push @SQLRESULTS, @RESULTS;
	} elsif( $type eq "sybase" ) {
			push @SYBASERESULTS, @RESULTS;
	} else {
			push @ORARESULTS, @RESULTS;
	}

	print "Completed $server...".$NL if $opt_d;
	if($ONEROWHEADER) {
		undef $ONEROWHEADER;
		$NOHEADER=1;
	}

	if( $CURRENT_COLOR eq $COLOR1 ) {
		$CURRENT_COLOR = $COLOR2;
	} else {
		$CURRENT_COLOR = $COLOR1;
	}

	dbi_disconnect();
}

sub OnePrint {
	print @_,$NL unless $NOPRINTMSGS;
}

__END__

=head1 NAME

OneQueryPerServerReport.pl - report on system configurations

=head2 USAGE

OneQueryPerServerReport.pl

Generic procedure runner.  Will run a single query on each of your servers, formatting output correctly.

REQUIRED ARGUMENTS

        -QUERY=QUERY
        -PROGRAMNAME=PROGRAMNAME        (default to BATCHID)
        -TITLE=TITLE

OPTIONAL ARGUMENTS

        -HTML
        -NOLOCK                 (ignore lock file to prevent repeat runs)
        -BATCHID=batchid
        -NOHEADER               (ignore header rows from results)
        -PREREQ                 (prerequisite query - only continue if this returns something)
        -NOPRINTMSGS    (ignore messages from the server)
        -DEBUG                  (debug)
        -NOLOCK
        -SYSTEMS=systems
        -SERVER=Server
        -PROGRAMNAME
        -TITLE
        -PRINTSERVERLABEL       (pretty print a nice label for each server)
        -TYPE=(DB Type - sqlsvr / sybase / both)

=cut


#C:\Perl\bin\perl.exe -I//samba/sybmon/dev/lib -I//samba/sybmon/dev/Win32_perl_lib_5_8 //samba/sybmon/dev/ADMIN_SCRIPTS/console/OneQueryPerServerReport.pl -TYPE=sqlsvr --HTML --TITLE="SqlSvr UserOptions Report" --QUERY="sp__serverproperty" --BATCHID=SqlProperty --NOHEADE
