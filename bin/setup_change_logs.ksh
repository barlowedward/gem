cd ..
dir=`pwd`
echo "Working On $dir/ADMIN_SCRIPTS"
for j in `ls $dir/ADMIN_SCRIPTS`
do
	if [ -d $dir/ADMIN_SCRIPTS/$j -a $j != 'sybmon' ]
	then
		echo "   - $j"
		rm -f $dir/ADMIN_SCRIPTS/$j/change_log.ksh 2>/dev/null 
		ln $dir/bin/change_log.ksh $dir/ADMIN_SCRIPTS/$j/change_log.ksh
	fi
done
echo "Working On $dir"
for j in `ls $dir`
do
	if [ -d $dir/$j -a $j != "bin" ]
	then
		echo "   - $j"
		rm -f $dir/$j/change_log.ksh 2>/dev/null 
		ln $dir/bin/change_log.ksh $dir/$j/change_log.ksh
		#[ -d "$dir/$j" -a $j != "bin" ] && rm -f $dir/$j/change_log.ksh  2>/dev/null  && ln $dir/bin/change_log.ksh $dir/$j/change_log.ksh
	fi
done
