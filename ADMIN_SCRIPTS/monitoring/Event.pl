#!/apps/perl/linux/perl-5.8.2/bin/perl
#!/apps/perl/solaris/perl-5.8.1/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use DBIFunc;
use MlpAlarm;
use CommonFunc;
use Getopt::Long;
use vars qw( $MONITOR_PROGRAM $SEVERITY $STATE $SYSTEM $SUBSYSTEM $MESSAGE_VALUE $EVENT_ID $DEBUG $EVENT_TIME $MESSAGE_TEXT $DOCUMENT_URL $TEST);

$|=1;

sub usage {
	print @_,"\n";
 	return "Usage: MlpEvent.pl --MONITOR_PROGRAM=file --SEVERITY=val --SYSTEM=system --SUBSYSTEM=subsystem --MESSAGE_VALUE=msg --EVENT_ID=id --DEBUG=1 --EVENT_TIME=time --MESSAGE_TEXT=text --DOCUMENT_URL=url --TEST

Save an EVENT using common Event Management

	--TEST			use test data
   --MONITOR_PROGRAM= Unique Name Of the Program Name
   --SEVERITY= Severity Of the Event
        * EMERGENCY = system down / critical failure
        * CRITICAL = serious problem.
        * ALERT = non fatal error needing attention
        * ERROR = non fatal error possibly requiring administrator attention
        * WARNING = non fatal warning.
        * INFORMATION = a simple message.  Synonym for OK.
        * DEBUG = messages only of interest to developers
   --SYSTEM=system
   --SUBSYSTEM=subsystem
   --DEBUG=1 	- print diagnostics
   --EVENT_TIME=time
   --MESSAGE_TEXT=string tehxt
   --DOCUMENT_URL=attachment
\n";
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
    GetOptions( "MONITOR_PROGRAM=s"	=>	\$MONITOR_PROGRAM,
		"SEVERITY=s"		=>	\$SEVERITY,
		"STATE=s"		=>	\$STATE,
		"SYSTEM=s"		=>	\$SYSTEM,
		"SUBSYSTEM=s"		=>	\$SUBSYSTEM,
		"MESSAGE_VALUE=s"	=>	\$MESSAGE_VALUE,
		"EVENT_ID=s"		=>	\$EVENT_ID,
		"DEBUG=s"		=>	\$DEBUG,
		"EVENT_TIME=s"		=>	\$EVENT_TIME,
		"TEST"		=>	\$TEST,
		"MESSAGE_TEXT=s"	=>	\$MESSAGE_TEXT,
		"DOCUMENT_URL=s"	=>	\$DOCUMENT_URL );

if( defined $TEST ) {
		$MONITOR_PROGRAM="test" unless defined $MONITOR_PROGRAM;
	  	$SYSTEM="test" 			unless defined $SYSTEM;
	  	$SUBSYSTEM="test" 		unless defined $SUBSYSTEM;
	  	$MESSAGE_TEXT="test"		unless defined $MESSAGE_TEXT;
}

$SEVERITY=$STATE if defined $STATE and ! defined $SEVERITY;
MlpEvent( 	-monitor_program	=>	$MONITOR_PROGRAM,
	  	-system			=>	$SYSTEM,
	  	-subsystem		=>	$SUBSYSTEM,
	  	-event_time		=>	$EVENT_TIME,
	  	-severity		=>	$SEVERITY,
	  	-message_text		=>	$MESSAGE_TEXT,
		-debug			=>	$DEBUG,
		-event_id		=>	$EVENT_ID,
		-message_value		=>	$MESSAGE_VALUE,
		-document_url 		=>	$DOCUMENT_URL
);


__END__

=head1 NAME

MlpEvent.pl - save an event

=head2 SYNOPSIS

a simple utility that saves a event using the monitoring system

=head2 USAGE

UUsage: MlpEvent.pl --MONITOR_PROGRAM=file --SEVERITY=val --SYSTEM=system --SUBSYSTEM=subsystem --MESSAGE_VALUE=msg --EVENT_ID=id --DEBUG=1 --EVENT_TIME=time --MESSAGE_TEXT=text --DOCUMENT_URL=url --TEST

Save an EVENT using common Event Management

   --TEST                  use test data
   --MONITOR_PROGRAM= Unique Name Of the Program Name
   --SEVERITY= Severity Of the Event
        * EMERGENCY = system down / critical failure
        * CRITICAL = serious problem.
        * ALERT = non fatal error needing attention
        * ERROR = non fatal error possibly requiring administrator attention
        * WARNING = non fatal warning.
        * INFORMATION = a simple message.  Synonym for OK.
        * DEBUG = messages only of interest to developers
   --SYSTEM=system
   --SUBSYSTEM=subsystem
   --DEBUG=1    - print diagnostics
   --EVENT_TIME=time
   --MESSAGE_TEXT=string tehxt
   --DOCUMENT_URL=attachment

=cu
