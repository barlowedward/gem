: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use File::Basename;
use DBIFunc;
use Do_Time;

use vars qw( $USER $SERVER $ACTION $PASSWORD $DATABASE %CONFIG $opt_d @FILE @TITLE );


$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"USER=s"			=> \$USER,
		"PASSWORD=s" 	=> \$PASSWORD,
		"DATABASE=s"	=> \$DATABASE,
		"ACTION=s"	=> \$ACTION,

		"FILE1=s" 		=> \$FILE[0],
		"FILE2=s" 		=> \$FILE[1],
		"FILE3=s" 		=> \$FILE[2],
		"FILE4=s" 		=> \$FILE[3],
		"FILE5=s" 		=> \$FILE[4],
		"FILE6=s" 		=> \$FILE[5],
		"FILE7=s" 		=> \$FILE[6],
		"FILE8=s" 		=> \$FILE[7],
		"FILE9=s" 		=> \$FILE[8],

		"TITLE1=s" 		=> \$TITLE[0],
		"TITLE2=s" 		=> \$TITLE[1],
		"TITLE3=s" 		=> \$TITLE[2],
		"TITLE4=s" 		=> \$TITLE[3],
		"TITLE5=s" 		=> \$TITLE[4],
		"TITLE6=s" 		=> \$TITLE[5],
		"TITLE7=s" 		=> \$TITLE[6],
		"TITLE8=s" 		=> \$TITLE[7],
		"TITLE9=s" 		=> \$TITLE[8],

		"DEBUG"      	=> \$opt_d );
	#process_statistics.pl -USER=USER -SERVER=SERVER -PASSWORD=PASS
			#-DATABASE=DATABASE -ACTION=action -FILE1=file -FILE2=file ...
			#-TITLE1=title -TITLE2=title... --DEBUG

die ("Must pass server\n" ) 				unless defined $SERVER;
die ("Must pass username\n" )	  			unless defined $USER;
die ("Must pass password\n" )	  			unless defined $PASSWORD;
die ("Must pass action\n" )  				unless defined $ACTION;
die ("Must pass database\n" )	  			unless defined $DATABASE;

die "ACTION must be save or compare"
	if $ACTION ne "save" and $ACTION ne "compare";

if( $ACTION eq "save" ) {
	my($query)="optdiag statistics $DATABASE -o$FILE[0] -U$USER -P$PASSWORD -S$SERVER";
	print "Running $query";
	my(@rc)=run_cmd( -cmd=>$query, -printcmd=>$opt_d, -printres=>1 );
	foreach (@rc) { print $_; }
} else {
	#
	# CONNECT TO THE DATABASE
	#
	debug( "Connecting\n" );
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD,-type=>"Sybase");
	die "Cant connect to $SERVER as $USER\n" if ! $rc;

	debug("Connected\n");

	debug( "query  is sp__helptable \@dont_format='Y'\n" );
	my(%real_rows,%realusd);
	foreach( dbi_query(-db=>$DATABASE,-query=>"exec sp__helptable \@dont_format='Y'")) {
		my(@dat)=dbi_decode_row($_);
		print "Query Returned : ",join(" ",@dat),"\n";
		$real_rows{$dat[0]} = $dat[1];
		$realusd{$dat[0]} = $dat[3];
	}

	debug( "program completed successfully.\n" );
	dbi_disconnect();

	my(%ts_pages,%ts_fragmentation,%ts_rows);
	my(%is_pages,%is_fragmentation,%is_rows);
	for my $i (0..8)  {
		next unless defined $FILE[$i];
		$TITLE[$i]=$FILE[$i] unless defined $TITLE[$i];
		print "Reading on $TITLE[$i]\n";
		die "Cant Read $FILE[$i]\n" unless -r $FILE[$i];
		open( FP,$FILE[$i] ) or die "Cant Open $FILE[$i]\n";
		my($owner,$table,$index)=("","","","","","");
		my($key);
		while(<FP>) {
			chomp;
			my($itm,$value)=split(/:/,$_,2);
			$value=~s/^\s+//;
			$value=~s/\s+$//;
			$itm=~s/^\s+//;
			if( $itm eq "Table owner" ) {
				$value=~s/"//g;
				$owner=$value;
			} elsif( $itm eq "Table name" ) {
				$value=~s/"//g;
				$table=$value;
				if( $owner ne "dbo" ) {
					$key=$owner.".".$table;
				} else {
					$key=$table;
				}
				$index="";
			} elsif( $itm eq "Statistics for table"   ) {
				$value=~s/"//g;
				$table=$value;
			} elsif( $itm eq "Statistics for index"  ) {
				#$value=~s/"//g;
				#$index=$value;
				#if( $owner eq "dbo" ) {
				#	$key=$table.".".$index;
				#} else {
				#	$key=$owner.".".$table.".".$index;
				#}
			} elsif( $itm eq "Data page count" ) {
				$ts_pages{$key}=$value;
				print "ts_pages{$key}=$value\n" if defined $opt_d;
			} elsif( $itm eq "Space utilization" ) {
				$ts_fragmentation{$key}=$value;
				#print "ts_fragmentation{$key}=$value\n" if defined $opt_d;
			} elsif( $itm eq "Data row count" ) {
				$ts_rows{$key}=$value;
				#print "ts_rows{$key}=$value\n" if defined $opt_d;
			} else {
				#print "skipping $_\n" if defined $opt_d;
			}
		}
		close(FP);
	}

	#foreach (sort keys %ts_pages) {
		#printf("%30s %10s %10s %10s %10s %10s\n", $_, $ts_pages{$_}, $ts_fragmentation{$_});
	#}

	printf("%30s %10s %10s %10s\n", "Table","Real Rows","Stat Rows","Frag");
	foreach (sort keys %ts_pages) {
		printf("%30s %10s %10s %10s\n", $_, int($real_rows{$_}), int($ts_rows{$_}), int($ts_fragmentation{$_}*1000)/1000) if $real_rows{$_} != $ts_rows{$_};
	}

	#my($query)="optdiag statistics $DATABASE -U$USER -P$PASSWORD -S$SERVER";
	#print "Running $query";
	#my(@rc)=run_cmd( -cmd=>$query, -printcmd=>$opt_d, -printres=>1 );
	#foreach (@rc) { print $_; }
}

exit(0);

sub run_cmd
{
   my(%OPT)=@_;
   my(@rc);
   chomp $OPT{-cmd};

	if( defined $OPT{-printcmd} ) {
   	my $cmd_no_pass=$OPT{-cmd};
   	$cmd_no_pass=~s/-P\w+/-PXXX/;
   	print "(".localtime(time).") ".$cmd_no_pass."\n";
	}

   $OPT{-cmd} .= " 2>&1 |";
   # $OPT{-cmd}=$^X." ".$OPT{-cmd} if $OPT{-cmd} =~ /^[\~\:\\\/\w_\.]+\.pl\s/;

   open( SYS2STD,$OPT{-cmd} ) || logdie("Unable To Run $OPT{-cmd} : $!\n");
   while ( <SYS2STD> ) {
      chop;
      push @rc, $_;
   }
   close(SYS2STD);

	my($cmd_rc)=$?;
   if( $cmd_rc != 0 ) {
		if( defined $OPT{-err} ) {
   		print "completed (failed) return_code=$cmd_rc\n";
			my($str)="Error: ".$OPT{-err}."\n";
			$str.="Return Code: $?\n";
      	$str.="Command: ".$OPT{-cmd}."\n";
			$str.="\n\nResults:\n";
      	logdie( $str."\n-- ".join("\n-- ",@rc)."\n");
		} elsif( defined $OPT{-printres} ) {
			print "Return Code:$?\n";
      	print "Results:\n-- ".join("\n-- ",@rc)."\n";
		}
		unshift @rc,"Error: Return Code $?";
		return @rc;
	} else {
		print "  Command completed at ".localtime(time)."\n"
			if defined $OPT{-printres};
		print "-- ".join("\n-- ",@rc)."\n" if defined $OPT{-printres} and $#rc>=0;
		return @rc;
	}
}

sub debug { print @_ if defined $opt_d; }

__END__

=head1 NAME

process_statistics.pl - compare server to saved statistics file(s)

=head2 USAGE

	process_statistics.pl -USER=USER -SERVER=SERVER -PASSWORD=PASS
			-DATABASE=DATABASE -ACTION=action -FILE1=file -FILE2=file ...
			-TITLE1=title -TITLE2=title...

=head2 DESCRIPTION

	if ACTION=save
		saves statistics for a database in a file
	if ACTION=compare
		compares statistics to files.  Hilighting divergences

=cut


