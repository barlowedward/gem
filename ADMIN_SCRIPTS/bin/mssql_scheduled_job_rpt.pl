#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2003 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Basename;
use File::Copy;
use DBIFunc;
use CommonFunc;
use Repository;
use Do_Time;
use MlpAlarm;
use CommonHeader;
use Getopt::Long;

my($DEBUG,$HTMLFILE,$COPYTODIR,$NUMLOGFILES,$NOSTDOUT);

sub usage {
	return "Usage: mssql_scheduled_job_rpt.pl --NOSTDOUT --DEBUG --HTMLFILE=file\n";
}

GetOptions(
		"NUMLOGFILES=s"	=> \$NUMLOGFILES,
		"DEBUG"		=> \$DEBUG,
		"NOSTDOUT"	=> \$NOSTDOUT,
		"HTMLFILE=s"	=> \$HTMLFILE );

#if( defined $HTMLFILE ) {
	#$COPYTODIR=dirname($HTMLFILE);
	$COPYTODIR=get_gem_root_dir()."/data/CONSOLE_REPORTS";
	die "COPY TO DIRECTORY $COPYTODIR does not exist\n"
		unless -d $COPYTODIR;
	#$COPYTODIR="" unless -d $COPYTODIR;
	mkdir("$COPYTODIR/details", 0755)
		unless -d "$COPYTODIR/details";
	#opendir( DIR , "$COPYTODIR/details" );
	#my(@d)=grep(/^\./,readdir(DIR);
	#closedir(DIR);
	#foreach (@d) { unlink "$COPYTODIR/details/$_"; }
#}

$NUMLOGFILES=3 unless defined $NUMLOGFILES;
# $NUMLOGFILES--;		# perl array bounding math sigh

MlpBatchJobStart(-BATCH_ID => 'MssqlScheduledJobRpt');
my($query)="msdb..sp_help_job";
my($query)="
SELECT  -- dbo.sysjobs.job_id,
	dbo.sysjobs.name,
	-- dbo.sysjobsteps.step_name,
	--dbo.sysjobsteps.subsystem,
	dbo.sysjobsteps.command,
	dbo.sysjobsteps.database_name,
	dbo.sysjobsteps.last_run_duration,
	dbo.sysjobsteps.last_run_date,
	dbo.sysjobsteps.last_run_time,
	dbo.sysjobschedules.next_run_date,
	dbo.sysjobschedules.next_run_time
FROM    dbo.sysjobs
	INNER JOIN dbo.sysjobschedules
		ON dbo.sysjobs.job_id = dbo.sysjobschedules.job_id
	INNER JOIN dbo.sysjobsteps
		ON dbo.sysjobschedules.job_id = dbo.sysjobsteps.job_id
WHERE     dbo.sysjobs.enabled = 1 and last_run_outcome!=1
	and subsystem='TSQL'";


cd_home();

my(@srv)=get_password(-type=>'sqlsvr');

my($cellst,$cellend,$rowst,$rowend,$bst,$bend,$blank);
if( $HTMLFILE )  {
	$blank="&nbsp;";
	$cellst="<TD>";
	$cellend="</TD>";
	$bst="<b>";
	$bend="</b>";
	$rowst="<TR>";
	$rowend="</TR>";
	open(OUTF,">$HTMLFILE") or die "Cant write $HTMLFILE : $!\n";
}

sub myprint {
	print OUTF @_ if defined $HTMLFILE;
	print @_ unless defined $NOSTDOUT;
}

sub myprinthtml {
	print OUTF @_ if defined $HTMLFILE;
}

my($login,$pass,$srv,$cmd);
myprinthtml( "<H1>" );
myprint( "SQL SERVER JOBS WITH ERRORS REPORT" );
myprinthtml( "</H1>\n" );
myprint("\n");
myprint( "created by mssql_scheduled_job_rpt.pl version 1.0");
myprinthtml( "<br>\n" );
myprint("\n");
myprint( "Run at ".localtime(time) );
myprint("\n");
myprinthtml( "<br>\n" );
myprinthtml( "<TABLE Background=white>\n" );
###print "Servers: @srv\n";
###print "Query=>$query\n";
foreach $srv (@srv) {
   my($login,$pass)=get_password(-type=>'sqlsvr',-name=>$srv);

   myprint( "Connecting to $srv\n" ) if defined $DEBUG;
   dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC')
       or next;

	# get list of enabled - failed jobs
   my(@rc)=dbi_query(-db=>"msdb",-query=>$query);
   foreach (@rc) {
	my($name, $command, $database_name, $last_run_duration,
	   $last_run_date, $last_run_time, $next_run_date, $next_run_time)
		=dbi_decode_row($_);

	my(@output);
	my($logdir);
	if( $command =~ /-Rpt/i ) {
		$command=~/-Rpt\s*\"([^"]+)\"/;
		my($logfile)=$1;
		$logfile=~s/^([A-Z]):/\\\\$srv\\$1\$/;
		$logfile=~s/^([a-z]):/\\\\$srv\\$1\$/;

		$logdir=dirname($logfile);
		$logfile =~ /\.(.)+$/;
		my($postfix)="\.".$1;
		my($fileroot)=basename($logfile,"\.txt",$postfix);

		if( ! -d $logdir or ! -r $logdir ) {
			#myprinthtml("<table border=1 bgcolor=white><TR><TD>\n");
			my($str)="Error: Cant Read Directory $logdir\n";
			$str.="<br>" if defined $HTMLFILE;
			$str.= "Server=$srv Name=$name Db=$database_name Last Run=$last_run_date $last_run_time\n";
			$str.="<br>" if defined $HTMLFILE;
			#myprinthtml("</TD></TR></TABLE>");
			print STDERR $str."Directory Permissions Warning\nCant open Log Directory $logdir\n\tFor Job $name \n\ton Server=$srv (skipping)\n--------------------\n";
		} else {
			opendir( DIR, $logdir )
				or die "Cant open $logdir $!\n";
			my(@files)=grep(!/^\./,readdir(DIR));
			closedir(DIR);

# TO DO
# this is what we want
# file://///adsrv081/d$/plan_reports/DB%20Maintenance%20Plan10_200503010100.txt
# this is what we have
# file:///G:/dev/data/CONSOLE_REPORTS/%5C%5CADSDR001%5CC$%5CProgram%20Files%5CMicrosoft%20SQL%20Server%5CMSSQL%5CLOG%5CMerrill%20Optimizations0_200408080600.txt

		   my($count)=0;
		   foreach (reverse sort @files) {
			next unless /^$fileroot/;
			if( $HTMLFILE ) {
				#push @output,"<A HREF=\'file:$logdir\\$_\'>$_</A>";
				push @output,"<A HREF=\'details\\$_\'>$_</A>";
			} else {
				push @output,$_ ;
			}
			#shift @output if $#output>=$NUMLOGFILES;
			$count++;

			#myprint("DBGX: I would copy $logdir/$_ to $COPYTODIR/details\n");
			#myprint("DBGX: note file not readable\n")
			#	unless -r "$logdir/$files[0]";

		   	copy( "$logdir\\$_","$COPYTODIR\\details\\$_" )
				if -r "$logdir/$files[0]";
			last if $count>$NUMLOGFILES;
	  	   }
		}
	}
	chomp $name;
	myprinthtml( $rowst.$cellst,$bst);
	myprint("SERVER:   ");
	myprinthtml($bend.$cellend.$cellst.$bst);
	myprint($srv);
	myprinthtml($bend.$cellend.$rowend);
	myprint("\n");
	myprinthtml( $rowst.$cellst,$bst);
	myprint("NAME:     ");
	myprinthtml($bend,$cellend.$cellst.$bst);
	myprint($name);
	myprinthtml($bend.$cellend.$rowend);
	myprint("\n");
	my($latestlog)=shift @output;
	if( defined $latestlog and $latestlog!~/^\s*$/  ) {
		#die "LOGDIR UNDEFINED\n" unless defined $logdir;
		#die "LOGDIR NOTADIR\n" unless -d $logdir;
		#die "FILE $latestlog NOT READABLE\n" unless -r "$logdir/$latestlog";
		myprinthtml( $rowst.$cellst);
		myprint("LOG DIRECTORY: ");
		myprinthtml($cellend.$cellst);
		myprint($logdir);
		myprint("\n");

		myprinthtml( $rowst.$cellst);
		myprint("LOG FILE: ");
		myprinthtml($cellend.$cellst);
		myprint($latestlog);
		myprint("\n");
	}

	foreach (@output) {
		myprinthtml( $rowst.$cellst);
		myprint("OTHER LOG:");
		myprinthtml($cellend.$cellst);
		myprint($_,"\n");
	}
	myprinthtml( $rowst.$cellst);
	myprint("COMMAND:  ");
	myprinthtml($cellend.$cellst);
	myprint($command);
	myprinthtml($cellend.$rowend);
	myprint( "\n");
	myprinthtml( $rowst.$cellst);
	myprint("DATABASE: ");
	myprinthtml($cellend.$cellst);
	myprint($database_name);
	myprinthtml($cellend.$rowend);
	myprint("\n");
	myprinthtml( $rowst.$cellst);
	myprint("LAST RUN: ");
	myprinthtml($cellend.$cellst);
	myprint("$last_run_date $last_run_time (");
	myprint( " duration=",do_diff( $last_run_duration ),")" );
	myprinthtml($cellend,$rowend);
	myprint( "\n");
	myprinthtml( $rowst.$cellst);
	myprint("NEXT RUN: ");
	myprinthtml($cellend.$cellst);
	myprint("$next_run_date $next_run_time");
	myprinthtml($cellend.$rowend);
	myprint( "\n");
	myprinthtml( $rowst.$cellst,$blank," ".$cellend.$cellst.$blank," ".$cellend.$rowend);
	myprint( "\n");
   }

   dbi_disconnect();
}
myprinthtml( "</TABLE>" );
myprint("\n");
close(OUTF) if defined $HTMLFILE;
MlpBatchJobEnd();
exit(0);

__END__

=head1 NAME

mssql_scheduled_job_rpt.pl - get scheduled jobs report

=head2 DESCRIPTION

Run a program in all your servers.  This is useful with the other utilities provided in this directory.  It requires that the run utility take B<-S>/B<-U>/B<-P> parameters.  The server should be in your password file as per Repository.

In other words... if you wish to run a program to remove user paulr on a server you might type

        adduser -SSYBASE -Usa -PXXX -x -upaulr

but with this utility, you could run that command on ALL your servers in one fell swoop with:

        AllSrv adduser -x -upaulr

As you can see, the B<-U>,B<-P>, and B<-S> parameters are added automatically

=head2 USAGE

        Usage: ./AllSrv Parameters

Generic program  to run on all servers.  If you type

                "./AllSrv MY_PROG OTHER_PARM"

you will end up executing

                "MY_PROG -SSRV -PPASS -UUSR OTHER_PARM"

for all the servers in your password file.

=cut
