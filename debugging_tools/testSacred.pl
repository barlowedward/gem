#!/usr/local/bin/perl-5.6.1
use lib qw(/apps/sybmon/dist/lib /apps/sybmon/dist/ADMIN_SCRIPTS/lib /apps/sybmon/dist/plugins /apps/sybmon/dist/cpan);

use strict;
use Data::Dumper;
use Inventory;

$|=1;

InvSetSACreds(-DSN=>"T1", -monitor_host=>'mysys',
	-state         =>'OK',
  	-system_name   => 'a1',
  	-system_type   => 'a2',
  	-login         => 'a3',
  	-password      => 'a4',
  	-hostname      => 'a5',
  	-port          => 'a7');
  	
InvSetSACreds(-DSN=>"T2", -monitor_host=>'mysys2',
	-state         =>'OK',
  	-system_name   => 'b1',
  	-system_type   => 'b2',
  	-login         => 'b3',
  	-password      => 'b4',
  	-hostname      => 'b5',
  	-port          => 'b7');

my(@x)=InvGetSACreds();
print Dumper \@x;

my(@y)=InvGetSACreds(-DSN=>'T2', -monitor_host=>'mysys2');
print Dumper \@y;

my(@y)=InvGetSACredByDSNandMonhost(-DSN=>'T2', -monitor_host=>'mysys2');
print Dumper \@y;