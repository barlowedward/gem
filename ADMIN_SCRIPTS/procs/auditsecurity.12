/* Procedure copyright(c) 1995 by Edward M Barlow */

/************************************************************************\
|* Procedure Name:      sp__auditsecurity
|*
|* Description:	 server check security
|*
|*	      Users With Paswords like %Id%
|*	      Users With Null Passwords
|*	      Users With Short (<=4 character) Passwords
|*	      Users With Master,Model, or Tempdb Database As Default
|*	      Allow Updates is set
|*	      Checks stupid passwords
|*    Check db with bad statuses
|*		Logins with bad databases for startup
|*
|* Usage:	       sp__auditsecurity @print_only_errors
|*
|*    if @print_only_errors is not null then prints only errors
|*
|* Modification History:
|*
\************************************************************************/

:r database
go
:r dumpdb
go

if exists (select *
	   from   sysobjects
	   where  type = "P"
	   and    name = "sp__auditsecurity")
begin
    drop proc sp__auditsecurity
end
go

create procedure sp__auditsecurity (
	@print_only_errors int = NULL,
	@srvname char(30) = NULL,
	@hostname char(30) = NULL,
	@dont_format char(1) = null )
as
begin
	declare @c int
	declare @numpgsmb int

	--if @srvname is not null and @hostname is null
	--begin
		--print "MUST PASS BOTH SERVER AND HOST IF EITHER SPECIFIED"
		--return 200
	--end

	create table #audsec_errs
	( error_no int not null, msg char(255) not null )

	set nocount on

	INSERT  #audsec_errs
	SELECT  31001,"User "+name+" Is Locked"
	from	    master..syslogins
	where	   status & 2 = 2

	if @@rowcount=0 and @print_only_errors is null
		insert #audsec_errs values (31000, "(No Users With Null Passwords)")

	-- not in 11.5...
	-- INSERT  #audsec_errs
	-- SELECT  31001,"User "+name+" Is Has "+convert(varchar,logincount)+" Failed Logins"
	-- from	  master..syslogins
	-- where	  logincount > 10

	INSERT  #audsec_errs
	SELECT  31002,"Login "+name+" Is Expired"
	from	  master..syslogins
	where	  status & 4 = 4
	and	  status & 2 != 2 -- not locked

	if @@rowcount=0 and @print_only_errors is null
		insert #audsec_errs values (31000, "(No Users With Expired Passwords)")

/*
	if exists ( select 1 where proc_role('sa_role')=1 )
	begin
		INSERT  #audsec_errs
		SELECT  31003,"User "+name+" Has Null/Short Password"
		from	  master..syslogins
		where	  password is null
		or		  status & 1 = 1
	end
*/

	if @@rowcount=0 and @print_only_errors is null
		insert #audsec_errs values (31000, "(No Users With Invalid Passwords)")

	INSERT  #audsec_errs
	SELECT	  31004,"User "+name+" Has "+dbname+" Database As Default"
	from	    master..syslogins
	where	   (( dbname in ("master","model","tempdb")
	and	     name != "sa"
	and	     name != "probe" )
	or		      ( name = "sa" and dbname!="master" )
	or		      ( name = "probe" and dbname!="master" ))
	and	  status & 2 != 2 -- not locked
	if @@rowcount=0 and @print_only_errors is null
		insert #audsec_errs values (31000, "(No Users With Master/Model/Tempdb As Default)")

   -- STUFF THAT SHOULD BE DIFFERENT
	INSERT  #audsec_errs
   SELECT   31026,x.name+" Config Has Been Reset To "+c.value2+" (default="+c.defvalue+")"
   from    master..syscurconfigs c,master..sysconfigures x
   where   c.value2 != c.defvalue
   and     c.config = x.config
   and     c.config not in ( 102,103,104,114,259,158,161,160,153,134,
	       105,138,106,132,131,122,116,126,127,135 )  -- DONT CARE
	and     c.config not between 360 and 372					-- MDA
	and	  c.config not in (
			376,					-- sql batch capture
			302,356,				-- mda stuff
			317,					-- rep
			137,					-- addl net memory

150, --	disk i/o structures
175, --	event buffers per engine
243, --	housekeeper free write percent
139, --	max network packet size
375, --	object lockwait timing
144, --	page lock promotion HWM
323, --	row lock promotion HWM
377, --	statement statistics active
373, --	wait event timing
293, --	xp_cmdshell context

			335,					-- license
			396,					-- max memory
			156,					-- NETWORK LISTENERS
			268,					-- MEMORY PER WORKER PROCESS
			410,					-- HISTOGRAM STEPS
			301,					-- LARGE IO BUFFERS
			263,		-- open indexes
			107,		-- number of open objects
			181,		-- number of sort buffers
			267,		-- worker processes
			387,		-- per object statistics active
			146,		-- procedure cache size
			170,		-- print deadlock information
			113,		-- Deadlock/Recovery Prints
			374,		-- process wait events
			402,		-- total physical memory
			245)		-- user log cache size

 -- STUFF THAT MUST BE DIFFERENT
	INSERT  #audsec_errs
   SELECT   31025,x.name+" Config Is Not Reset From default="+c.defvalue
   from    master..syscurconfigs c,master..sysconfigures x
   where   c.value2 = c.defvalue
   and     c.config = x.config
   and     c.config in (104,		-- total logical memory
   							103, 		-- number of user connections
   							146,		-- procedure cache size
   							106,		-- Number of locks
   							132,		-- total data cache size
   							396,		-- max memory
					   		263,		-- open indexes
								107,		-- number of open objects
								402		-- total physical memory
								 )

   -- and     c.config in ( 139,		-- max network packet size
   --							137,		-- addl net memory
   --							)

	-- Check Devices
	select  @c=count(*) from master..sysdevices
			where status & 2 = 2
	INSERT  #audsec_errs
	SELECT  31006,"ERROR: Num Open Devices Parameter Set Too Low"
	from    master..syscurconfigs c
	where   c.value < @c
	and     c.config=116

	-- Check Databases
	select  @c=count(*) from master..sysdatabases
	INSERT  #audsec_errs
	SELECT  31007,"ERROR: Num Open Databases Parameter Set Too Low"
	from    master..syscurconfigs c
	where   c.value < @c
	and     c.config=105

	INSERT #audsec_errs
	SELECT  32120,'Should Drop '+name+' Database'
	from master..sysdatabases where name in ('Northwind','pubs','pubs2')


	INSERT  #audsec_errs
	SELECT  31008,"Allow Updates is Set"
	from	    master..syscurconfigs
	where	   config=102 and value=1

	if @@rowcount=0
	begin
		INSERT   #audsec_errs
		SELECT	31010,"Allow Updates is Set"
		from	   master..sysconfigures
		where	   config=102 and value=1

		if @@rowcount=0 and @print_only_errors is null
			insert #audsec_errs values (31000, "(Allow Updates is Not Set)")
	end

	INSERT  	#audsec_errs
	SELECT	31012,"User sa is trusted from "+srvname
	from	   master..sysremotelogins r, master..sysservers s
	where	   r.remoteserverid = s.srvid
	and		r.suid=1
	and		r.status=1
	if @@rowcount=0 and @print_only_errors is null
		insert #audsec_errs values (31000, "(No Trusted Remote Logins)")

	INSERT  #audsec_errs
	SELECT	  31013,"Database "+name+" Created For Load"
	FROM     master..sysdatabases
	WHERE	   status & 32 = 32

	INSERT  #audsec_errs
	SELECT	  31014,"Database "+name+" Suspect"
	FROM     master..sysdatabases
	WHERE	   status & 256 = 256

	INSERT  #audsec_errs
	SELECT	  31015,"Database "+name+" Offline"
	FROM     master..sysdatabases
	WHERE	   status2 & 16 = 16

	INSERT  #audsec_errs
	SELECT	  31016,"Database "+name+" Offline until recovery completes"
	FROM     master..sysdatabases
	WHERE	   status2 & 32 = 32

	INSERT  #audsec_errs
	SELECT	  31017,"Database "+name+" Is Being Recovered"
	FROM     master..sysdatabases
	WHERE	   status2 & 64 = 64

	INSERT  #audsec_errs
	SELECT	  31018,"Database "+name+" Has Suspect Pages"
	FROM     master..sysdatabases
	WHERE	   status2 & 128 = 128

	INSERT  #audsec_errs
	SELECT	  31019,"Database "+name+" Is Being Upgraded"
	FROM     master..sysdatabases
	WHERE	   status2 & 512 = 512

	INSERT  #audsec_errs
	SELECT  distinct 31020,"Database "+name+" -> No Log Device and No TL on Chkpt"
	FROM    master..sysdatabases
	WHERE   status2 & 0x8000 = 0x8000
	and     name not in ('master','model','sybsystemprocs','tempdb','sybsystemdb' )
	and     status & 8 != 8

	INSERT   #audsec_errs
	select   31021,"ERROR: MIRROR BROKEN: "+name
	from 		master..sysdevices where cntrltype=0
	and 		( status & 256 = 256 or status & 2048 = 2048 )

	INSERT   #audsec_errs
	select   31022,"ERROR: CHECK MIRRORING: "+name
	from 	   master..sysdevices where cntrltype=0
	and 	   ( status & 4096 = 4096 or status & 8192 = 8192 )

	INSERT  	#audsec_errs
	select  	31030,"ERROR: Login "+name+" Has an invalid default db ("+dbname+")"
	from 		master..syslogins l
	where 	l.dbname not in ( select name from master..sysdatabases )
	and       status & 4 != 4
	and       status & 2 != 2

  -- Check for unused sysusages rows
  	INSERT    #audsec_errs
  	select    31031,"Device "+rtrim(dev.name)+" Is Mapped But Unused (no segments). Size (MB) ="+ltrim(rtrim(str((usg.size/512.),10,0)))
	from master.dbo.sysusages usg,
	master.dbo.sysdevices dev
	where vstart between low and high and usg.segmap=0 and cntrltype = 0
		and dev.name != 'master'

	select @numpgsmb = (1048576. / v.low)
	from master.dbo.spt_values v
	where v.number = 1 and v.type = "E"

	select  @c =  sum(u.size) / @numpgsmb
	from	    master..sysusages u
	where   u.dbid=db_id("sybsystemprocs")
	and	     u.segmap!=4

	if @c < 60
	begin
		INSERT  #audsec_errs
		select  31032,"ERROR: Sybsystemprocs should be > 60 MB ("+convert(varchar,@c)+")"
	end

	select  @c =  sum(u.size) / @numpgsmb
	from	    master..sysusages u
	where   u.dbid=db_id("tempdb")

	if @c < 100
	begin
		INSERT  #audsec_errs
		select  31033,"ERROR: tempdb should be >= 100 MB (currently "+convert(varchar,@c)+"MB)"
	end

	if @@SERVERNAME is null
		INSERT  #audsec_errs select  31039,"ERROR: @@SERVERNAME is null"

		-- Error if segmap=3,4 AND segmap=7 on same database
		-- Error if segmap 3 AND segmap=4 on same device
		SELECT  db=db_name(dbid),u.dbid,
		           name = dv.name,
		        size = u.size,
		        u.lstart,
		           segmap = u.segmap
		into    #devlayout
		FROM    master.dbo.sysusages u, master.dbo.sysdevices dv
		WHERE   dv.low <= size + vstart
		AND     dv.high >= size + vstart - 1
		AND     dv.status & 2 = 2
		AND     db_name(dbid) NOT IN ("master", "model", "tempdb")

		INSERT  #audsec_errs
			select distinct 32135,"Database "+d1.db+" Contains Mixed data/log AND nonmixed devices"
			from   #devlayout d1, #devlayout d2
			where  d1.dbid=d2.dbid
			and    d1.segmap=7 and d2.segmap in (3,4)

		INSERT  #audsec_errs
			select distinct 32136,"Database "+d1.db+" contains data And log on device="+d1.name
			from   #devlayout d1, #devlayout d2
			where  d1.dbid=d2.dbid
			and    d1.segmap=4 and d2.segmap = 3
			and	d1.name=d2.name

-- new 4/2/09
if not exists ( select * from master..sysdatabases
where name not in ('master','model','msdb','sybsystemprocs','pubs','pubs2','tempdb',
	'Northwind','AdventureWorks','AdventureWorksDW','sybsystemdb','sybsystemdb' ))
insert #audsec_errs
select distinct 32142,'No User Databases Exist On Server'

-- new 6/2/09
-- detects log audit login failure not being set - that must be set
insert #audsec_errs
SELECT	32145,"Failed Logins Are NOT detected (sp_configure 'log audit logon failure',1)"
from	   master..sysconfigures
where	   config=297 and value=0

-- new 6/16/09 - audit directives
INSERT #audsec_errs
SELECT  32146,"sp_configure 'check password for digit' is not set"
from	   master..sysconfigures
where	   config=342 and value=0

INSERT #audsec_errs
SELECT  32147,"sp_configure 'select on syscomments.text' is set"
from	   master..sysconfigures
where	   config=258 and value!=0

INSERT #audsec_errs
SELECT  32148,"sp_configure 'minimum password length' is not set"
from	   master..sysconfigures
where	   config=343 and value<5

INSERT #audsec_errs
SELECT  32149,"sp_configure 'maximum failed logins' is not set"
from	   master..sysconfigures
where	   config=344 and value<5

-- June 17th 09 -> add check for server running > 90 days
insert #audsec_errs
select 32151, 'database server has been running for '+convert(varchar,datediff(dd,crdate,getdate()))+' days'
from master..sysdatabases where name='tempdb'
and  datediff(dd,crdate,getdate()) > 90

-- Jun 18th 09
INSERT #audsec_errs
SELECT  32153,"sp_configure 'select on syscomments.text' is set"
from	   master..sysconfigures
where	   config=258 and value!=0

-- July 1 09
INSERT #audsec_errs
SELECT  32155,"sp_configure 'print deadlock info' is not set"
from	   master..sysconfigures
where	   config=170 and value=0

/*
if exists ( select 1 where proc_role('sa_role')=1 )
begin
print "SA"
	INSERT #audsec_errs
	SELECT  32156, 'Table ' +  o.name  + ' next_indentity=' +
	        next_identity(user_name(o.uid)+'.'+o.name) + ' > 80% of max=' +
	        --convert( varchar(10), (case when c.prec > 9 then round((power(2., 31) -1 ) * 0.8, 0) else power(10., c.prec - 1) *  8 end)) + ' and the max value  ' +
	        convert( varchar(10),(case when c.prec > 9 then power(2., 31) -1 else power(10., c.prec) - 1 end) )
	from    sysobjects o,
	        syscolumns c
	where    o.type='U'   and o.id = c.id   and c.status & 128 > 0
	and convert(numeric(12, 0), next_identity (user_name(o.uid)+'.'+o.name)) >= (
	      case when c.prec > 9 then round((power(2., 31) -1 ) * 0.8, 0) else power(10., c.prec - 1) *  8 end)
	--order by o.name
end
else
begin
print 'NOT SA'
end
*/
-----------------------------------------------------
	if @srvname is null
	begin
		if @dont_format is null
			select "Security Violations"=substring(msg,1,79)
			from #audsec_errs
		else
			select "Security Violations"=msg
			from #audsec_errs
	end
	else if @hostname is null
	begin
		if @dont_format is null
			select srvname=@srvname,db="master","Violation"=substring(msg,1,65)
			from #audsec_errs
		else
			select srvname=@srvname,db="master","Violation"=msg
			from #audsec_errs
	end
	else
	begin
		if @dont_format is null
			select hostname=@hostname,srvname=@srvname,error_no,db="master",type="a",day=getdate(),"Violation"=substring(msg,1,65)
			from #audsec_errs
		else
			select hostname=@hostname,srvname=@srvname,error_no,db="master",type="a",day=getdate(),"Violation"=msg
			from #audsec_errs
	end

	drop table #audsec_errs
end
go

grant execute on sp__auditsecurity to public
go
