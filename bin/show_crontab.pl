use strict;
use Getopt::Long;
use CommonFunc;

my($PROGRAM_DESCRIPTION)="Crontab - Show Tasks";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG $descheduleonly $showreal );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" 
unless GetOptions( "DEBUG"	=> \$DEBUG, 
	"showreal"					=> \$showreal,
	"descheduleonly" 			=> \$descheduleonly );

print "$0 - v",$VERSION,"\n";
print "   Reading gem.dat\n";
my($GemConfig)=read_gemconfig(-debug=>$DEBUG);
if( $GemConfig->{ERRORS} ) {
	foreach ( @$GemConfig->{ERRORS} ) {
		_statusmsg("[cfgfiles] $_ \n" );
	}
}
sub _statusmsg { 	print @_; }
# START BODY

die "Must Run on Unix" if is_nt();
	
use JobScheduler;
use Sys::Hostname;
use File::Basename;

my($job_scheduler) = fetch_jobscheduler();			# ALARM SCHEDULE DATA
	
print "CRONTAB VIEWER\n";
print "VERSION V1.0\n";

$GemConfig=read_gemconfig( -debug=>$DEBUG, -nodie=>1, -descheduleonly);

my($rootdir);
$rootdir = $GemConfig->{UNIX_CODE_LOCATION} if $GemConfig->{UNIX_CODE_LOCATION} and -d $GemConfig->{UNIX_CODE_LOCATION} and ! $rootdir;
die "No UNIX_CODE_LOCATION Dir" unless $rootdir;

my(%cron);
print "Running crontab -l\n";
open(CRON,"crontab -l |") or die "Cant run crontab -l\n";
foreach (<CRON>) {
	chop;
	print "Read $_\n" if $DEBUG;
	next unless /unix_batch_scripts/;
	next if /^\s*#/;
	s/[\d,-\[\]]+\s+//;
	s/[\d,-\[\]]+\s+//;
	s/[\d,-\[\]]+\s+//;
	s/[\d,-\[\]]+\s+//;
	s/[\d,-\[\]]+\s+//;
	#print "Fetched $_\n";
	print "WR $_\n" if $DEBUG;
	
	my($job) = basename($_,".ksh" );
#	$cron{ $job } = hostname();	
	$$job_scheduler{$job}->{scheduled_on} = hostname();
}
close(CRON);

#foreach ( @cron ) {
#	print $_,"\n";	
#}

print "Dumping Scheduler Information\n";
report_jobscheduler($job_scheduler);
	
	
#foreach ( keys %$GemConfig ) {
#	print $_," ",$GemConfig->{$_},"\n";
#}

print "Saving Scheduler Information\n";
save_jobscheduler($job_scheduler);

# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "SUCCESSFUL: $PROGRAM_DESCRIPTION\n";
close_gemconfig();

exit(0);

