#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Repository;
use MlpAlarm;
use DBIFunc;
use Getopt::Long;

use vars qw( $DEBUG );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless  GetOptions(  "DEBUG"		=> \$DEBUG);

my($connection)=MlpGetDbh();

my(@types)=get_passtype();
my(@errors);
foreach my $typ (@types) {
	next if $typ eq "documenter";
	my(@svrs)=get_password(-type=>$typ);
	printf( "Found %4s registered servers of type $typ \n",($#svrs+1) );

	foreach my $s ( @svrs ) {
		my($l,$p,$t)=get_password(-type=>$typ,-name=>$s);
		my($query1) = "ST_Update_System_sp '".$s."','".$typ."','"."YES"."','".$l."','".$p."'\n";
		print $query1;

		my(%hsh)=get_password_info(-type=>$typ,-name=>$s);
		foreach ( keys %hsh ) {
			#print " hash $_ => $hsh{$_} \n";
			my($query2) = "ST_Update_Attribute_sp '".$s."','".$typ."','".$_."','".$hsh{$_}."'\n";
			print $query2;
		}
	}
}
if( $#errors<0 ) {
	print "SUCCESSFUL: No Errors\n";
	exit(0);
}


