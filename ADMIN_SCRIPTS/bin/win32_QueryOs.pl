#
# Modified from the below script by dave roth
#
#   QueryOS.pl
#   -----------
#   This will discover and print information about the
#   Windows OS on a given machine.
#   Syntax:
#       perl QueryOS.pl [Machine Name]
#
#   Examples:
#       perl QueryOS.pl
#       perl QueryOS.pl \\server
#
#   2002.01.20 rothd@roth.net
#
#   Permission is granted to redistribute and modify this code as long as
#   the below copyright is included.
#
#   Copyright � 2002 by Dave Roth
#   Courtesty of Roth Consulting
#   http://www.roth.net/

use vars qw( $Log $Message $Name $Value );
use strict;
use Repository;
use Win32::OLE qw( in );

my @OS_TYPE = qw(
  Unknown Other Mac_OS ATTUNIX DGUX DECNT Digital_Unix
  OpenVMS HPUX AIX MVS OS400 OS/2 JavaVM MSDOS
  Windows_3x Windows_95 Windows_98 Windows_NT Windows_CE
  NCR3000 NetWare OSF DC/OS Reliant_UNIX SCO UnixWare
  SCO_OpenServer Sequent IRIX Solaris SunOS U6000 ASERIES
  TandemNSK TandemNT BS2000 LINUX Lynx XENIX VM/ESA
  Interactive_UNIX BSDUNIX FreeBSD NetBSD GNU_Hurd OS9
  MACH_Kernel Inferno QNX EPOC IxWorks VxWorks MiNT
  BeOS HP_MPE NextStep PalmPilot Rhapsody
);

my $Class = "Win32_OperatingSystem";

print "<h1>" ;
print "Win32 OS Report - QueryOs.pl \n";
print "</h1>";
my($COLOR1,$COLOR2, $CURRENT_COLOR)=("beige","#FFCCCC","beige");
my($HEADER_COLOR)="brown";

my(@serverlist)=get_password(-type=>'win32servers');

print "<TABLE>";
foreach my $Machine ( @serverlist ) {

	# (my $Machine = shift @ARGV || "." ) =~ s/^[\\\/]+//;

	my $WMIServices = Win32::OLE->GetObject( "winmgmts:{impersonationLevel=impersonate,(security)}//$Machine" ) || next;

	$~ = "INFO";
	foreach my $OS ( in( $WMIServices->InstancesOf( $Class ) ) )
	{
	  my $Organization = "($OS->{Organization})" if ( "" ne $OS->{Organization} );

	  print "<TR BGCOLOR=$HEADER_COLOR><TH COLSPAN=3>SERVER=$Machine</TH></TR>\n";
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "Name", "</TD><TD>", $OS->{Caption}, "</TD></TR>\n";
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "OS", "</TD><TD>", "$OS_TYPE[$OS->{OSType}] v$OS->{Version}", "</TD></TR>\n";
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "Service pack", "</TD><TD>", $OS->{CSDVersion}, "</TD></TR>\n"  if "" ne $OS->{CSDVersion};
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "Memory (RAM+Virt)", "</TD><TD>", FormatMemory( $OS->{TotalVirtualMemorySize} * 1024 ) . " bytes", "</TD></TR>\n";
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "System path", "</TD><TD>", $OS->{SystemDirectory}, "</TD></TR>\n";
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "OS installed on", "</TD><TD>", $OS->{SystemDevice}, "</TD></TR>\n";
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "Serial number", "</TD><TD>", $OS->{SerialNumber}, "</TD></TR>\n";
	  print "<TR BGCOLOR=$CURRENT_COLOR><TD></TD><TD>", "Registered to", "</TD><TD>", "$OS->{RegisteredUser} $Organization", "</TD></TR>\n";
	}

   if( $CURRENT_COLOR eq $COLOR1 ) {
      $CURRENT_COLOR = $COLOR2;
   } else {
      $CURRENT_COLOR = $COLOR1;
   }
}
print "</TABLE>\n";

sub FormatNumber
{
    my($Number) = @_;
    while( $Number =~ s/^(-?\d+)(\d{3})/$1,$2/ ){};
    return( $Number );
}

sub FormatMemory
{
  my( $Size ) = @_;
  my $Format;
  my $Suffix;
  my $K = 1024;
  my $M = $K * 1024;
  if( $M < $Size )
  {
      $Suffix = "M";
      $Format = $Size / $M;
  }
  elsif( $K < $Size )
  {
      $Suffix = "K";
      $Format = $Size / $K;
  }
  else
  {
      $Format = $Size;
  }
  $Format =~ s/\.(\d){1,2}\d+/.$1/;
  return( FormatNumber( $Format ) . $Suffix );
}


