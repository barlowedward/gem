use strict;
use Sys::Hostname;
use CommonFunc;
my($DEBUG)=0;

use vars qw($BATCHID $DEBUG);

print "Reading Gem Configuration File\n";
my $GemConfig=read_gemconfig( -debug=>$DEBUG, -nodie=>1, -batchid=>\$BATCHID, -batch_id=>\$BATCHID,-READONLY=>1 );

my($subject)= "GEM Is Up!";
my($message)= "Gem Test Mail Message from ".hostname();

print "Sending Mail : To $GemConfig->{ADMIN_EMAIL} \n";
print "Sending Mail : From $GemConfig->{ALARM_MAIL} \n";
print "Sending Mail : Host $GemConfig->{MAIL_HOST} \n";
print "Sending Mail : Subj $subject \n";
print "Sending Mail : Text $message \n";
send_mail_message(
	$GemConfig->{ADMIN_EMAIL}, 
	$GemConfig->{ALARM_MAIL},
	$GemConfig->{MAIL_HOST},
	$subject,
	$message);
