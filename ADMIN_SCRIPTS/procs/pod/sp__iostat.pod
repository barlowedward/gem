
=head1 NAME

sp__iostat  - Equivalent of the unix iostat command

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 SEE ALSO


sp__whoactive, sp__isactive, sp__iostat

=head2 DESCRIPTION


Unix iostat command equivalent. Does a loop on existing processes and prints
only rows in sysprocesses that are doing anything (io or cpu > 0).

=head2 BUGS

Does not monitor logins started after process starts.

=head2 USAGE


sp__iostat { [ @count ] [, @delay ]

        @count is number of iterations (default=3)
        @delay - delay between stats rows in seconds. Valid values
                are 1,3,5,10,30,60

=head2 SAMPLE OUTPUT


1> exec sp__iostat

 -------- ---------
 00:22:32 No Change

 Time     Spid Login      Cmd              Cpu    Io   Mem  Blk
 -------- ---- ---------- ---------------- ------ ---- ---- ---
 00:22:42 6    NULL       SITE HANDLER     DEAD   DEAD DEAD   0

 -------- ---------
 00:22:52 No Change
