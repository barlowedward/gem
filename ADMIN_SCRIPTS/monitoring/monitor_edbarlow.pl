#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Mail::Mailer;
use File::MultiTail;
use CommonFunc;
use Getopt::Long;
use MlpAlarm;
use Logger;
use CommonHeader;

my $VERSION = "1.0";
my $ping_interval=300;	# seconds
my $opt_d;
my $SENDMAIL=0;			# Dont Sent Mail if 0
my $SENDBEEP=0;			# Dont Send Beeps if 0
my $WRITEFILE=0;			# Dont Send Beeps if 0
my $DEBUG;		# = 1;
my $MAILTO = "ebarlow\@mlp.com";

my  $LOG="/monitor/server/log";

# my  $PIDF="$LOG/monitor.pidemb";
my  $VMSTATDB="$LOG/Vmstat/";
my  $MPSTATDB="$LOG/MPstat/";
my  $PDHSTATDB="$LOG/PDHstat/";
my  $DFDB="$LOG/Df/";
my $True=1;
my $False=0;
my $PID = $$;
my %Store=();
my $s=\%Store;
my $tb=20; # time between messages in min

$|=1;

use vars qw( $LOCKFILE $DEBUG $ALARMFILE $LOGFILE);

die usage("Bad Parameter List\n") unless GetOptions(
		"LOCKFILE=s"	=> \$LOCKFILE,
		"ALARMFILE=s"	=> \$ALARMFILE,
		"lFILE"      => \$LOGFILE,
		"DEBUG"      	=> \$DEBUG );

$LOGFILE= "/apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/monitor.log"
	unless defined $LOGFILE;

print "(dbg) $0 started\n" if defined $DEBUG;
cd_home();
my($rc);
if( defined $LOCKFILE ) {
	$rc=Am_I_Up(-filename=>$LOCKFILE, -debug=>$DEBUG);
} else {
	$rc=Am_I_Up(-debug=>$DEBUG);
}
print "(dbg) locks checked rc=$rc\n" if defined $DEBUG;
if( $rc ) {
	exit(0) unless is_interactive();
	my($fname,$secs,$thresh)=GetLockfile(-prefix=>$BATCHID);
	print "Cant Start program is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nFile=$fname secs=$secs thresh=$thresh secs\n";

 	print "(dbg) locks check failed rc=$rc\n" if defined $DEBUG;
	print "(dbg) checking interactivity \n" if defined $DEBUG;
	print "(dbg) interactive=",is_interactive()," \n" if defined $DEBUG;
	print "ERROR: $0 is allready running\n";
	exit(0);
}
I_Am_Up();
print "(dbg) lock check done\n" if defined $DEBUG;
print "monitor.pl version $VERSION\n";

if( defined $ALARMFILE ) {
	open(ALARMFP,">> $ALARMFILE") or die "Cant Write $ALARMFILE : $! \n";
	print "WRITING TO $ALARMFILE\n";
}

logger_init( -debug=> $opt_d, -logfile => $LOGFILE, -command_run => $0, -mail_to =>$MAILTO, -fail_subject=>"Logger Error"  );

my($lastpingtime)=time;
MlpBatchJobStart(-BATCH_ID=>"monitor_edbarlow.pl" ,-AGENT_TYPE=>'Gem Monitor');

# List of beeper ids
# Pagers starting with 800 or 888 are last 7 numbers only
# Pagers with 877 are all 10 numbers
#$bill="7946886";
#$stef="9958746";
my $ED="8690807";
my $fred="9958746";
my $johnb="7629315";
my $miano="4588231";
my $mikek="8773637247";
my $naj="7946865";
my $russell="2085039";
my $stippell="4386184";
my $rcarpana="8773637227";

$ENV{USER} = 'monitor';
$SIG{CHLD} = 'IGNORE';

# if( $WRITEFILE ) {
	# open(PIDF,">$PIDF") || die "Could not open PID file $PIDF\n";
	# print PIDF $PID;
	# close(PIDF);
# }

my(@TestExceptions);
open(EFILE,"ExceptPatternTest.dat") or die "Cant Read ExceptPatternTest.dat";
while(<EFILE>) { chop; push @TestExceptions,$_; }
close(EFILE);

#
# Check dir
#

my($t) = File::MultiTail->new(
#	Files => ["$LOG/app.log","$LOG/daemon.log", "$LOG/messages", "$LOG/ldaemon.log", "$LOG/cisco.log", "$LOG/pixfirewall.log", "$LOG/max666.log", "$LOG/max1800.log"],
	#Files => ["$LOG/app.log","$LOG/daemon.log", "$LOG/messages", "$LOG/ldaemon.log", "$LOG/cisco.log"],
	Files => ["$LOG/daemon.log", "$LOG/messages", "$LOG/ldaemon.log"], #, "$LOG/cisco.log"],
	# steves pattern file
	# Pattern => "/monitor/server/etc/Patterns",
	# eds pattern file
	Pattern => "/apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/Patterns",
	# ExceptPattern => "/monitor/server/etc/pong.override",
	ExceptPattern => "/apps/sybmon/ADMIN_SCRIPTS/monitoring/ExceptPattern.dat",
	Function => \&writedb,
	NumLines => 0,
	RemoveDuplicate => $True,
);

while(1) {
	$t->read;
	# $t->print;
	sleep 30;
	if( defined $LOCKFILE ) {
		I_Am_Up(-filename=>$LOCKFILE);
	} else {
		I_Am_Up();
	}
}
I_Am_Down();
MlpBatchJobEnd();
exit(0);

sub writedb {
	my ($rArray)=shift;

	my ($Mon,$Day,$Time,$Host,$SubSystem,$Text);
	my ($Year,$Mon1,$Day1,$Time1,$TimeZone,$TZoffset);
	my ($From,$DfServer,$Action,$DfText);
	my ($df_1,$DfFS,$df_2,$df_3,$df_4,$df_5,$df_6,$df_7);
	my ($FROM,$VMSTATSERVER,$ACTION,$VMSTATTEXT);
	my ($MPSTATSERVER,$MPSTATTEXT);
	my ($PDHSERVER,$PDHTEXT);
	my ($PIPSSERVER,$PIPSTEXT);
	my ($LIUSERVER,$LIUTEXT);

	if( time - $lastpingtime > $ping_interval ) {
		$lastpingtime=time;
		# MlpBatchJobStep();
		print "(dbg) marking running\n";
	}

	for my $line ( @{$rArray} ) {
		chop $line;
		($Mon,$Day,$Time,$Host,$SubSystem,$Text)=split(' ', $line, 6 );
		print "(processing) ",$line,"\n" if $Host eq "kdb2";

		print ".";
		next if $Text =~ /yp_all/;
		next if $Text =~ /tcp server failing (looping)/;
		next if $SubSystem eq "scsi:" and $Text=~/WARNING: \/pci\@/;

		# The next line ensures that messages are not too frequent
		next unless StoreMessage("$Host$Text",180);

    	if ( $line =~ /NTEVENTLOG/ and $WRITEFILE ) {
			open(NTEV,">>$LOG/nteventlog.log")
				|| die "Can't open file $LOG/nteventlog.log $!\n";
			print NTEV "$line\n";
			close(NTEV);
			next;
		}

		if ( $SubSystem eq "dfstats.pl:" ) {
			# from dfstats.pl - disk space monitor
			my($dummy,$pid,$sev,$fs,$pct,$sz,$usd,$avl);
			print  "==========================\n";
			if( $Text=~/^\[/ ) {
				($dummy,$pid,$sev,$fs,$pct,$sz,$usd,$avl)=split(/\s+/, $Text);
				print  "> regular solaris dfstats.pl line\n"
					if defined $DEBUG;
			} else {
				($fs,$pct,$sz,$usd,$avl)=split(/\s+/, $Text);
				my($p)=$pct;
				$p=~s/Pct=//;
				$sev=".warning]" if $p>=90;
				$sev=".err]" if $p>=97;
				print  "> regular linux dfstats.pl line setting sev to $sev\n"
					if defined $DEBUG;
			}
			my($SEVERITY)="OK";
			$SEVERITY="WARNING" 	if $sev=~/\.warning\]/;
			$SEVERITY="ERROR"	  	if $sev=~/\.err\]/ or $sev=~/\.error\]/;
			$fs=~s/FS=//;
			my($msg)="$fs $pct $sz $usd $avl";
			print  "> dfstats.pl input=",$line,"\n";
			print  "> dfstats.pl system=$Host state=$SEVERITY subsystem=$fs msg=$msg\n";
			print  "==========================\n";
			Heartbeat(
					-monitor_program	=> "dfstats.pl",
					-system				=> $Host,
					-state				=> $SEVERITY,
					-subsystem			=> $fs,
					-message_text		=> $msg );
			next;
		}

  		if ( $Text =~ /ACTION=DF/ ) {
				# modified by ed barlow
				# read from apps.log  - USER=xxx HOST=xxx ACTION=DF TEXT=xxx
    			($From,$DfServer,$Action,$DfText)=split(/ /,$Text,4);
    			$DfServer 	=~ s/HOST=// ;
    			$DfText 		=~ s/TEXT=// ;

				# Text Message Format
				#   time(secs),File System,Kbytes,Used,Avail,File,FilesUsed,FilesAv
    			my($DTime,$DfFS,$df_1,$df_2,$df_3,$fi_1,$fi_2,$fi_3)=split(/,/,$DfText,8);
				return unless defined $df_1 and $df_1=~/\d/ and int($df_1)>0;
				return unless defined $df_3 and $df_3=~/\d/ and int($df_3)>0;
				return unless defined $fi_1 and $fi_1=~/\d/ and int($fi_1)>0;
				return unless defined $fi_3 and $fi_3=~/\d/ and int($fi_3)>0;
				my($bytepct)=int((100*$df_2)/$df_1);
				my($filepct)=int((100*$fi_2)/$fi_1);
				my($state)="OK";
				$state="EMERGENCY" 	if $bytepct>99 or $filepct>99;
				$state="CRITICAL" 	if $bytepct>95 or $filepct>95;
				$state="WARNING" 		if $bytepct>90 or $filepct>90;
				$df_1=int($df_1/1024);
				$df_2=int($df_2/1024);

				my($msg)="Drive $DfFS at $bytepct% UsedMb=$df_2 TotalMb=$df_1";
				$msg.=" Files Pct=$filepct% Files=$fi_1 Used=$fi_2" if $filepct>90;

				info("Saving DF Message Host=$DfServer State=$state $msg\n");
				Heartbeat(
					-monitor_program	=> "MonitorDf",
					-system				=> $DfServer,
					-state				=> $state,
					-subsystem			=> $DfFS,
					-message_text		=> $msg );

    			$DfFS =~ s/\//_/g if defined $DfFS;
    			#system ("touch ${DFDB}hosts/$DfServer") ;
				if( $WRITEFILE ) {
    				open (DfServerFile, ">>${DFDB}$DfServer.$DfFS")
						|| die "Can't open file $DfServer.$DfFS.\n" ;
    				print DfServerFile "$df_1,$df_2,$df_3,$df_4,$df_5,$df_6,$df_7\n" ;
    				close (DfServerFile) ;
				}
				next;
		}

  		if ( $Text =~ /USER=APPSWATCH:pong/ ) {
    		my($USER,$HOST,$ACTION,$TEXT)=split(/ /,$Text,4);
    		$HOST 	=~ s/HOST=// ;
    		$ACTION 	=~ s/ACTION=// ;
    		$TEXT 	=~ s/TEXT=// ;

			my($STATE)="OK";
			$STATE="WARNING" if $ACTION eq "DOWN";

			info("Saving Pong Message Host=$HOST State=$STATE $TEXT\n");
			Heartbeat(
					-monitor_program	=> "MonitorPong",
					-system				=> $HOST,
					-state				=> $STATE,
					-subsystem			=> "Pong",
					-message_text		=> $TEXT );

			next;
		}

    	if ( $Text =~ /APPSLOG/ ) {
			my $mafile = '/dat/brokdata/sybase/accounts/missing_accounts';
			my $msfile = '/dat/brokdata/sybase/securities/missing_securities';
			$Text =~ s/%R/\n/g;
			$Text =~ m|MSG\=(\w+)$|;
			AppMailFile($msfile,$1) if $1 eq "missing_securities";
			AppMailFile($mafile,$1) if $1 eq "missing_accounts";
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			next;
		}

    	if ( $Text =~ /MPSTAT/ ) {
			($FROM,$MPSTATSERVER,$ACTION,$MPSTATTEXT)=split(/ /,$Text,4);
			$MPSTATSERVER =~ s/HOST=//;
			my($OPENMPSERVER) = $MPSTATSERVER;
			$OPENMPSERVER =~ s/\-//;
			$MPSTATTEXT =~ s/TEXT=//;
			#
			# check for bad filehandle
			#
			my $cpu = (split(/\s+/,$MPSTATTEXT))[1];
			next if length($OPENMPSERVER) < 2;
			if( $WRITEFILE )  {
			open( eval $OPENMPSERVER, ">>${MPSTATDB}$MPSTATSERVER.$cpu")
				|| die "Can't open file $MPSTATSERVER. $!\n";
			eval (print $OPENMPSERVER "$MPSTATTEXT\n");
			eval (close $OPENMPSERVER );
			}
			next;
    	}

    	if ( $Text =~ /ACTION=VMSTAT/ ) {
			($FROM,$VMSTATSERVER,$ACTION,$VMSTATTEXT)=split(/ /,$Text,4);
			$VMSTATSERVER =~ s/HOST=//;
			my($OPENVMSERVER) = $VMSTATSERVER;
			$OPENVMSERVER =~ s/\-//;
			$VMSTATTEXT =~ s/TEXT=//;
			#
			# check for bad filehandle
			#
			next if length($OPENVMSERVER) < 2;
			if( $WRITEFILE )  {
			open( eval $OPENVMSERVER, ">>${VMSTATDB}$VMSTATSERVER")
				|| die "Can't open file $VMSTATSERVER. $!\n";
			eval (print $OPENVMSERVER "$VMSTATTEXT\n");
			eval (close $OPENVMSERVER );
			}
			next;
    	}

    	if ( $Text =~ /GROUP=KRTRADE/ ) { 	# emb addition
			my($GROUP,$HOST,$SERVICE,$STAT)=split(/ /,$Text,4);
    		$HOST 	=~ s/HOST=// ;
    		$SERVICE =~ s/SERVICE=// ;
    		$STAT 	=~ s/STAT=// ;

			my($STATE)="OK";
			$STATE="ERROR" if $STAT ne "UP" and $STAT ne "STARTING";

			### DBG ### info("Saving KRtrade Message Host=$HOST State=$STATE Service=$SERVICE\n");
			Heartbeat(
					-monitor_program	=> "MonitorKRTRADE",
					-system				=> $HOST,
					-subsystem			=> $SERVICE,
					-state				=> $STATE,
					-message_text		=> $Text  );

			next;
		}

    	if ( $SubSystem =~ /ASCEND/ ) {
			my $time = scalar(localtime(time));
			my($ASCENDSERVER) = $Host;
			my($OPENASCENDSERVER) = $ASCENDSERVER;
			$OPENASCENDSERVER =~ s/\-//;
			my($ASCENDTEXT) = $Text;

			# check for bad filehandle

			next if length($OPENASCENDSERVER) < 2;
			if( $WRITEFILE )  {
			open( eval $OPENASCENDSERVER, ">>$LOG/$ASCENDSERVER.log")
				|| die "Can't open file $ASCENDSERVER. $!\n";
			eval (print $OPENASCENDSERVER "$time $ASCENDTEXT\n");
			eval (close $OPENASCENDSERVER );
			}
			next;
    	}

    	if ( $line =~ /Memory/ && $line =~ /available/ ) {
			next;
		}
    	if ( $line =~ /memory/ && $line =~ /vmnet/ ) {
			next;
		}
    	if ( $line =~ /memory/ && $line =~ /Freeing/ ) {
			next;
		}
    	if ( $line =~ /memory/ && $line =~ /bttv/ ) {
			next;
		}

    	if ( $line =~ /\sDENY\s/ ) {
			if( $WRITEFILE )  {
			open(FW,">>$LOG/firewall.log")
				|| die "Can't open file $LOG/firewall.log. $!\n";
			print FW "$line\n";
			close(FW);
			}
			next;
		}

    	if ( $Text =~ /USER=APPSWATCH:nt/ ) {
			next if $line =~ /CR Disk/;

			my($USER,$HOST,$ACTION,$TEXT)=split(/ /,$Text,4);
    		$USER 	=~ s/USER=// ;
    		$HOST 	=~ s/HOST=// ;
    		$ACTION  =~ s/ACTION=// ;
    		$TEXT 	=~ s/TEXT=// ;

			my($STATE)="OK";
			$STATE="ERROR" if $ACTION eq "DOWN";

			Heartbeat(
					-monitor_program	=> "MonitorNT",
					-system				=> $HOST,
					-subsystem			=> $USER,
					-state				=> $STATE,
					-message_text		=> $TEXT  );

			next if $STATE eq "OK";
			# info( "Routine at Line ".__LINE__."\n" );
			# LogMessage($line);
			$Text =~ s/USER=daemon HOST=flash ACTION=APPSWATCH:nt//;
			BeepAppsNtMessage("$Text");
			NTSMail($Text);
			next;
		}

		my($TESTFOREXCEPTIONS)="FALSE";
		foreach (@TestExceptions) {
			if($Text=~/$_/) {
				$SubSystem=~s/://;
				if( 	$Text=~/\.warning\]/
				or 	$Text=~/\.err\]/
				or 	$Text=~/\.notice\]/
				or 	$Text=~/\.crit\]/
				or 	$Text=~/\.info\]/
				or 	$Text=~/\.emerg\]/ ) {
					$TESTFOREXCEPTIONS="TRUE - SENT";
					my($SEVERITY)="OK" 	if $Text=~/\.info\]/;
					$SEVERITY="OK" 		if $Text=~/\.notice\]/;
					$SEVERITY="WARNING" 	if $Text=~/\.warning\]/;
					$SEVERITY="WARNING" 	if $Text=~/\.crit\]/;
					$SEVERITY="ERROR"	  	if $Text=~/\.err\]/;
					$SEVERITY="ERROR"	  	if $Text=~/file system full/;
					$SEVERITY="CRITICAL"	if $Text=~/\.emerg\]/;
					info( "################# $SEVERITY ##########################\nOriginal Exception File Value $_ Precludes Line:\n$line\n") unless /bs6 scsi:/;
					die "Cant determine SEVERITY!" unless defined $SEVERITY;
					MlpEvent(
						-monitor_program	=> "monitorEvt",
						-system				=> $Host,
						-severity			=> $SEVERITY,
						-subsystem			=> $SubSystem,
						-message_text		=> $Text );
				} else {
					$TESTFOREXCEPTIONS="TRUE - FALSE";
					info( "###########################################\nOriginal Exception File Value $_ Precludes Line:\n$line\n") unless /bs6 scsi:/;
					info( "Alarm ***NOT*** Sent\n");
				}
				last;
			}
		}
		next if $TESTFOREXCEPTIONS eq "TRUE - SENT";
		###($Mon,$Day,$Time,$Host,$SubSystem,$Text)=split(' ', $line, 6 );

		info( "###########################################\nLINE: ",__LINE__." (debug) Message=[$line]\n");

    	if ( $SubSystem =~ /pptp/ || $SubSystem =~ /ppp/ ) {
			if( $WRITEFILE )  {
			open(PPP,">>$LOG/pptp.log")
				|| die "Can't open file $LOG/pptp.log. $!\n";
			#
			print PPP "$line\n";
			close(PPP);
			}
			next;
		}
    		if ( $line =~ /pptp/ || $line =~ /ppp/ ) {
			next unless $line =~ /APPSWATCH/;
		}
    	if ( $Text =~ /LIUTRADER/ ) {
			($FROM,$LIUSERVER,$ACTION,$LIUTEXT)=split(/ /,$Text,4);
			if( $WRITEFILE )  {
			open(LIU,">>$LOG/LiuTrade.log")
				|| die "Can't open file $LOG/LiuTrade.log. $!\n";
			#
			print LIU "$LIUTEXT\n";
			close(LIU);
			}
			my $pid = fork();
			die "Cannot fork: $!" unless defined($pid);
			if ( $pid == 0 ) {
				#Child process
				# if Jim liu machine is a Solaris box
				system("/usr/bin/rsh top \"exec /bin/csh -cf 'cat /usr/demo/SOUND/sounds/rooster.au > /dev/audio'\"");
				exit;
			}
			next;
    		}

		if ( $Text =~ /NTPERFMON/ ) {
			($FROM,$PDHSERVER,$ACTION,$PDHTEXT)=split(/ /,$Text,4);
			$PDHTEXT =~ s/TEXT=//;
			my($INFO,$PDHSER);
			($PDHSERVER,$INFO)=split(/ /,$PDHTEXT,2);
			$PDHSER=$PDHSERVER;
			$PDHSERVER =~ s/\-//;
			#
			if( $WRITEFILE )  {
			open( eval $PDHSERVER, ">>${PDHSTATDB}$PDHSER")
				|| die "Can't open file $PDHSERVER. $!\n";
			eval (print $PDHSERVER "$INFO\n");
			eval (close $PDHSERVER );
			}
			next;
		}

    	if ( $Text =~ /SuperCaps/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMDMessage("$Host $Text");
    			if ( $Text =~ /elmprod1/ ) {
				SuperCapsMailDR($line);
			} else {
				SuperCapsMail($line);
			}
			next;
		}

    	if ( $Text =~ /rdf/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMDMessage("$Host $Text");
			MDSMail($line);
			next;
		}
    	if ( $Text =~ /siac/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMDMessage("$Host $Text");
			MDSMail($line);
			next;
		}
    	if ( $Text =~ /nasdaq/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMDMessage("$Host $Text");
			MDSMail($line);
			next;
		}
    		if ( $SubSystem =~ /wombat_monitor/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMDMessage("$Host $Text");
			MDSMail($line);
			next;
		}
    		if ( $Text =~ /SYS-/ ) {
			($Mon,$Day,$Time,$Host,$Year,$Mon1,$Day1,$Time1,$TimeZone,$TZoffset,$SubSystem,$Text)=split(' ', $line, 12 );
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepNETMessage("$Host $Text");
			NETMail($line);
			next;
		}
    		if ( $Text =~ /MGMT-5-/ ) {
			($Mon,$Day,$Time,$Host,$Year,$Mon1,$Day1,$Time1,$TimeZone,$TZoffset,$SubSystem,$Text)=split(' ', $line, 12 );
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepNETMessage("$Host $Text");
			NETMail($line);
			next;
		}
    		if ( $Text =~ /LINEPROTO-5-/ ) {
			($Mon,$Day,$Time,$Host,$Year,$Mon1,$Day1,$Time1,$TimeZone,$TZoffset,$SubSystem,$Text)=split(' ', $line, 12 );
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepNETMessage("$Host $Text");
			NETMail($line);
			next;
		}
    		if ( $Text =~ /PIX-1/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepNETMessage("$Host $Text");
			NETMail($line);
			next;
		}
    		if ( $Text =~ /security error/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepNETMessage("$Host $Text");
			NETMail($line);
			next;
		}
    	if ( $Text =~ /ping_ecn/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMDMessage("$Host $Text");
			MDSMail($line);
			next;
		}
    	if ( $Text =~ /SQL/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			SQLMail($line);
			BeepAppsNtMessage("$Host $Text");
			NTSMail($line);
			next;
		}
    	if ( $Text =~ /sql/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			SQLMail($line);
			BeepAppsNtMessage("$Host $Text");
			NTSMail($line);
			next;
		}
    	if ( $Text =~ /SYB/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			SQLMail($line);
			BeepMessage("$Host $Text");
			UNIXMail($line);
			next;
		}
    	if ( $Text =~ /SYB/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			SQLMail($line);
			BeepMessage("$Host $Text");
			UNIXMail($line);
			next;
		}
    	if ( $Text =~ /syb/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepSQLMessage("$Host $Text");
			SQLMail($line);
			next;
		}

    	if ( $Text =~ /IMAG/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepIMAGMessage("$Host $Text");
			IMAGMail($line);
			next;
		}

    	if ( $Text =~ /imag/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepIMAGMessage("$Host $Text");
			IMAGMail($line);
			next;
		}

    	if ( $SubSystem =~ /pong/ ) {
			LogMessage("Line=".__LINE__." Host=$Host SubSystem=$SubSystem Text=$Text");
			next;
		}

    	if ( $line =~ /DOTError/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepDotMessage("$line");
			next;
		}

  		if ( $Text =~ /APPSWATCH:kdb/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMessage("$Host $Text");
			SMail($line);
			next;
		}

  		if ( $Text =~ /APPSWATCH:dns/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMessage("$Host $Text");
			SMail($line);
			next;
		}

  		if ( $Text =~ /ACTION=WATCHPS/ ) {
    		my($USER,$HOST,$ACTION,$TEXT)=split(/ /,$Text,4);
    		$HOST 	=~ s/HOST=// ;
    		$ACTION 	=~ s/HOST=// ;
    		$TEXT 	=~ s/TEXT=// ;

			my($STATE)="OK";
			$STATE="WARNING" unless $TEXT =~ /is UP/;

			info("Saving WATCHPS Message Host=$HOST State=$STATE $TEXT\n");
			Heartbeat(
					-monitor_program	=> "MonitorPong",
					-system				=> $HOST,
					-state				=> $STATE,
					-subsystem			=> "Pong",
					-message_text		=> $TEXT );

			next if $STATE eq "OK";		# CHANGE TO LOGIC BY EMB

			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			$Text =~ s/USER=root//;
			BeepMessage("$Host $Text");
			SMail($line);
			next;
		}

  		if ( $Text =~ /APPSWATCH:reuters/ ) {
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			BeepMessage("$Host $Text");
			SMail($line);
			next;
		}

  		if ( $Text =~ /PIPS/ ) {
			($FROM,$PIPSSERVER,$ACTION,$PIPSTEXT)=split(/ /,$Text,4);
			$PIPSSERVER =~ s/HOST=//;
			$PIPSTEXT =~ s/TEXT=//;
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			LogMDMessage("$line");
			BeepMDMessage("ERROR pips srv=$PIPSSERVER error=$PIPSTEXT");
			MDSMail($line);
			next;
		}

    	if ( $Text =~ /SSL/ ) {
			my($SSLSERVER,$SSLTEXT);
			($FROM,$SSLSERVER,$ACTION,$SSLTEXT)=split(/ /,$Text,4);
			$SSLSERVER =~ s/HOST=//;
			$SSLTEXT =~ s/TEXT=//;
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			LogMDMessage("$line");
			BeepMDMessage("ERROR SSL srv=$SSLSERVER error=$SSLTEXT");
			MDSMail($line);
			next;
		}

		LogMessage("*** Unhandled Message - Rerouting ***\nLine=".__LINE__." Host=($Host) SubSystem=($SubSystem) Text=($Text)");
		# LogMessage($line);
		BeepMessage("$Host $Text");
		UNIXMail($line);
	}
}

sub StoreMessage {
	my ($Text,$tb) = @_;

	#my $Maxsec = $tb * 60; # time in sec
	my $Maxsec = $tb; # time in sec
	my $Time = time;
	if ( defined($s->{$Text})) {
		if (($Time - $s->{$Text}->{Time}) > $Maxsec) {
			$s->{$Text}->{Time} = $Time;
			return $True;
		}
	} else {
		$s->{$Text}->{Time} = $Time;
		return $True;
	}
	### DBG ### print "Unable To Store $Text as Message Occurred At ".localtime($s->{$Text}->{Time})."\n" if defined $DEBUG and $Text=~/ACTION=DF/;
	return $False;
}

sub AppMail {
	my $Text = shift;
	# Mail attr
	my $To = "smiano\@mail1.mlp.com";
	my $From = "APPLOG\@mail1.mlp.com";
	my $Subject = "System APPS monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}
sub AppMailFile {
	my $file = shift;
	my $Subject = shift;
	# Mail attr
	my $To = "misacc\@mail1.mlp.com";
	my $From = "APPLOG\@mail1.mlp.com";
	#
	{
	local($/) = undef;
	open(FILE,"$file") || return;
	my $body = <FILE>;
	close(FILE);
	SendAlert($To,$From,$Subject,$body);
	}

}
sub NTSMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "ntmonitor\@mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "System monitoring message";
	#
	SendAlert($To,$From,$Subject,$Text);
}
#
sub SMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "smiano\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "System monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}
sub LogMessage {
	my $Text = shift;
	chomp $Text;
	info( "LogMessage:".$Text."\n" );
	# return unless $WRITEFILE;
	# my $LogFile="/monitor/server/log/monitor.log";
	# #
	# open(LOG,">>$LogFile") || die "Can't open: $1\n";
	# print LOG $Text . "\n";
	# close(LOG);
}
sub BeepAppsNtMessage {
	info( "sendpage(line=",__LINE__," (".join("",@_)."))\n");
	return unless $SENDBEEP;
	my $Text = shift;
	$Text =~ s/\^//;
	my $BEEP="/apps/bin/beep";
	#
	# fork off the beep program
	#
	my $pid = fork();
	die "Cannot fork: $!" unless defined($pid);
	if ( $pid == 0 ) {
		#Child process
		my $out=`$BEEP $miano \'$Text\'`;
		$out=`$BEEP $stippell \'$Text\'`;
		$out=`$BEEP $rcarpana \'$Text\'`;
		$out=`$BEEP $ED \'$Text\'`;
		exit;
	}
	return;
}

sub BeepMessage {
	info( "BeepMessage(line=",__LINE__,")\n" );
	return unless $SENDBEEP;
	my $Text = shift;
	$Text =~ s/\^//;
	my $BEEP="/apps/bin/beep";
	#
	# fork off the beep program
	#
	my $pid = fork();
	die "Cannot fork: $!" unless defined($pid);
	if ( $pid == 0 ) {
		$Text =~ s/sparesun1/OX server sparesun1/;
		#Child process
		my $out=`$BEEP $miano \'$Text\'`;
		$out=`$BEEP $naj \'$Text\'`;
		$out=`$BEEP $mikek \'$Text\'`;
		exit;
	}
	return;
}
sub BeepDotMessage {
	info( "sendpage(line=",__LINE__," (".join("",@_)."))\n");
	return unless $SENDBEEP;
	my $Text = shift;
	$Text =~ s/\^//;
	my $BEEP="/apps/bin/beep";
	#
	# fork off the beep program
	#
	my $pid = fork();
	die "Cannot fork: $!" unless defined($pid);
	if ( $pid == 0 ) {
		#Child process
		my $out=`$BEEP 4349611 \'$Text\'`;
		exit;
	}
	return;
}
sub LogMDMessage {
	info( "sendpage(line=",__LINE__," (".join("",@_)."))\n");
	return unless $SENDBEEP;
	return unless $WRITEFILE;
	my $Text = shift;
	my $LogFile="/monitor/server/log/pips_ssl.log";
	#
	open(LOG,">>$LogFile") || die "Can't open: $1\n";
	print LOG $Text . "\n";
	close(LOG);
}
sub BeepNETMessage {
	info( "sendpage(line=",__LINE__," (".join("",@_).")\n");
	return unless $SENDBEEP;
	my $Text = shift;
	#
	$Text =~ s/\^//;
	my $BEEP="/apps/bin/beep";
	#
	# fork off the beep program
	#
	my $pid = fork();
	die "Cannot fork: $!" unless defined($pid);
	if ( $pid == 0 ) {
		#Child process
		my $out=`$BEEP $johnb \'$Text\'`;
		$out=`$BEEP $miano \'$Text\'`;
		$out=`$BEEP $naj \'$Text\'`;
		$out=`$BEEP $mikek \'$Text\'`;
		exit;
	}
	return;
}
sub BeepMDMessage {
	info( "I WOULD MD BEEP ",__LINE__," (".join("",@_)."\n" );
	return unless $SENDBEEP;
	my $Text = shift;
	#
	$Text =~ s/\^//;
	my $BEEP="/apps/bin/beep";
	#
	# fork off the beep program
	#
	my $pid = fork();
	die "Cannot fork: $!" unless defined($pid);
	if ( $pid == 0 ) {
		#Child process
		my $out=`$BEEP $johnb \'$Text\'`;
		$out=`$BEEP $miano \'$Text\'`;
		$out=`$BEEP $naj \'$Text\'`;
		$out=`$BEEP $mikek \'$Text\'`;
		exit;
	}
	return;
}
sub BeepIMAGMessage {
	info( "I WOULD IMAG BEEP ",__LINE__," (".join("",@_)."\n");
	return unless $SENDBEEP;
	my $Text = shift;
	#
	$Text =~ s/\^//;
	my $BEEP="/apps/bin/beep";
	#
	# fork off the beep program
	#
	my $pid = fork();
	die "Cannot fork: $!" unless defined($pid);
	if ( $pid == 0 ) {
		#Child process
		my $out=`$BEEP $miano \'$Text\'`;
		$out=`$BEEP $mikek \'$Text\'`;
		$out=`$BEEP $naj \'$Text\'`;
		exit;
	}
	return;
}
sub BeepSQLMessage {
	info( "I WOULD SQL BEEP ",__LINE__," (".join("",@_)."\n");
	return unless $SENDBEEP;
	my $Text = shift;
	#
	$Text =~ s/\^//;
	my $BEEP="/apps/bin/beep";
	#
	# fork off the beep program
	#
	my $pid = fork();
	die "Cannot fork: $!" unless defined($pid);
	if ( $pid == 0 ) {
		#Child process
		my $out=`$BEEP $miano \'$Text\'`;
		$out=`$BEEP $mikek \'$Text\'`;
		$out=`$BEEP $naj \'$Text\'`;
		exit;
	}
	return;
}
sub MDSMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "mdmonitor\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "Market Data System monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}
sub SuperCapsMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "supercapsmonitor\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "Super Caps System monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}
sub SuperCapsMailDR {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "supercapsmonitordr\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "Super Caps System monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}
sub NETMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "netmonitor\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "Network monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}

sub SQLMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "sqlmonitor\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "SQL and Sybase System monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}

sub IMAGMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "imagmonitor\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "Imagine System monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}

sub UNIXMail {
	my $Text = shift;
	# Mail attr
	return 0 if -f "/apps/admin/masterfiles/NOBEEP";
	my $To = "unixmonitor\@mail1.mlp.com";
	my $From = "monitor\@mail1.mlp.com";
	my $Subject = "UNIX System monitoring message";

	SendAlert($To,$From,$Subject,$Text);
}

sub SendAlert {
	my($To,$From,$Subject,$Text)=@_;
	info(  "SendAlert(to=$To Subject=$Subject Text=($Text)\n" );

	if($SENDMAIL) {
		my $mailer = Mail::Mailer->new("sendmail");
		$mailer->open({
				To	=> $To,
				From	=> $From,
				Subject	=> $Subject
		}) or die "Can't open: $!\n";
		print $mailer $Text;
		$mailer->close();
	}
	$Text=~s/\s+$//;

	if( $Subject eq "UNIX System monitoring message" ) {
		# parse syslogs messages
		chomp $Text;
		debug( "TEXT=$Text\n");
		my(@parsemsg)=split(/:/,$Text,4);
		$parsemsg[0] =~ m/^(\w\w\w)\s+(\d+)\s+(\d+)/;
		my($mon,$day,$hr)=($1,$2,$3);
		$parsemsg[1] =~ m/(\d+)/;
		my($mi)=($1);
		my($ss,$system,$progname)=split(/\s+/,$parsemsg[2]);
		$progname=~s/\[\d+\]//;		# parse out pid
		my($msg)=$parsemsg[3];

		# if this parsed report it and return otherwise contiue
		if( defined $mon and defined $day and defined $hr and defined $mi and defined $ss and defined $system and defined $progname and defined $msg and $msg!~/^\s*$/ and $system!~/^\s*$/ and $progname!~/^\s*$/) {
			if( $msg=~/HOST=/ ) {
				$msg=~/HOST=([\w\d\_\-]+)/;
				$system=$1;
			}
			$msg=~s/^\s+//;

			my($SEVERITY)="WARNING";
			$SEVERITY="OK" 		if $Text=~/\.info\]/;
			$SEVERITY="OK" 		if $Text=~/\.notice\]/;
			$SEVERITY="WARNING" 	if $Text=~/\.warning\]/;
			$SEVERITY="WARNING" 	if $Text=~/\.crit\]/;
			$SEVERITY="ERROR"	  	if $Text=~/\.err\]/;
			$SEVERITY="ERROR"	  	if $Text=~/file system full/;
			$SEVERITY="CRITICAL"	if $Text=~/\.emerg\]/;

			alert( "MlpEvent: sys=$system severity=$SEVERITY subsy=$progname msg=$msg\n" );
			MlpEvent(
				-monitor_program	=> "monitorEvt",
				-system				=> $system,
				-severity			=> $SEVERITY,
				-subsystem			=> $progname,
				-message_text		=> $msg );
			return;
		} else {
			info("#####################################\n");
			info( "# COULDNT Parse MSG=$Text\n" );
			info( "# DATE=$mon~$day~$hr~$mi~$ss SYS=$system PROG=$progname MSG=$msg \n" );
			info("#####################################\n");
		}
	} else {
		info("#####################################\n");
		info("# New Section Of The Code Being Run #\n");
		info("#####################################\n");
		chomp $Text;

		my(@parsemsg)=split(/:/,$Text,4);
		$parsemsg[0] =~ m/^(\w\w\w)\s+(\d+)\s+(\d+)/;
		my($mon,$day,$hr)=($1,$2,$3);
		$parsemsg[1] =~ m/(\d+)/;
		my($mi)=($1);
		my($ss,$system,$progname)=split(/\s+/,$parsemsg[2]);
		$progname=~s/\[\d+\]//;		# parse out pid
		my($msg)=$parsemsg[3];

		# if this parsed report it and return otherwise contiue
		if( defined $mon and defined $day and defined $hr and defined $mi and defined $ss and defined $system and defined $progname and defined $msg and $msg!~/^\s*$/ and $system!~/^\s*$/ and $progname!~/^\s*$/) {
			if( $msg=~/HOST=/ ) {
				$msg=~/HOST=([\w\d\_\-]+)/;
				$system=$1;
			}
			$msg=~s/^\s+//;

			my($SEVERITY)="WARNING";
			alert( "MlpEvent: sys=$system severity=$SEVERITY subsy=$progname msg=$msg\n" );
			MlpEvent(
				-monitor_program	=> "monitorEvt",
				-system				=> $system,
				-severity			=> $SEVERITY,
				-subsystem			=> $progname,
				-message_text		=> $msg );
			return;
		} else {
			info("#####################################\n");
			info( "# COULDNT Parse MSG=$Text\n" );
			info( "# DATE=$mon~$day~$hr~$mi~$ss SYS=$system PROG=$progname MSG=$msg \n" );
			info("#####################################\n");
		}
	}
}

sub Heartbeat {
	my(%args)=@_;
	if( defined $ALARMFILE ) {
		my($str);
		foreach (keys %args ) { $str.=$_."=>".$args{$_}." "; }
		print ALARMFP $str,"\n";
	}

	MlpHeartbeat(%args);
}
