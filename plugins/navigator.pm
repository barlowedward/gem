# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package navigator;

@ISA = qw(Plugin);
use strict;
use DBIFunc;
use RosettaStone;

use vars qw($VERSION);
$VERSION = ".01";

my($std_page_name)="schema";
my($current_notebook);	# set on init to the notebook in the schema tab
my(%tab_wholearea);	# set on init the whole area of each tab - dont change this

my($current_entrypath)="";	# the current entrypath
my($current_tab)="";	# name of currently raised tab
my(%tab_frame);		# the frame within the whole area... can be deleted and replaced
my(%latestpath_by_tab);	# last thing drawn in each tab
my($lb,$textObject,$curconnection,$curdb);

my(%current_info);	# a hash of currently selected navigation info, for the ddl generator

my(@menuitems) = (
	"Sybase/DDL Navigator",
	"Sql Server/DDL Navigator",
	"Oracle/DDL Navigator",
	"Oracle/test/Testit2",
	"Oracle/test/test/test3" );

sub getMenuItems { 	return @menuitems; }
sub getPluginPanes { 	return $std_page_name;}

#my( %toi ) = (
#	"Sybase ASE"	=> "sybase",
#	"Unix System"	=> "unix",
#	"Sql Server"	=> "sqlsvr",
#	"NT Server"	=> "win32servers",
#	"Oracle"	=> "oracle"
#);

sub init {
	my($package)=shift;
	my($current_main_frame)=$package->clear_tab($std_page_name);
	$current_notebook   = $current_main_frame->NoteBook()->pack(-side=>'bottom',-fill=>'both',-expand=>1, -anchor=>'n');

	my(@rc) = get_schema_info(-reportname=>"objreports");

	#foreach my $tabname ( 'Tables','Triggers','Views','Procedures','System','Rules', 'Defaults', 'Data' ) {}
	foreach my $tabname ( @rc ) {
		$tabname=ucfirst($tabname);
		$tab_wholearea{$tabname} = $current_notebook->add($tabname, -label=>$tabname,
			-underline=>0,
			-createcmd=>sub {$package->create_a_tab($tabname); },
			-raisecmd =>sub {$package->raise_a_tab($tabname); }
		);
		#$tab_frame{$tabname}= $tab_wholearea{$tabname}->Frame()->pack( -fill=>'x', -side=>'top', -expand=>1);
		$latestpath_by_tab{$tabname}="";
	}
	$current_tab = $current_notebook->raised();
	$current_entrypath = "/";
	dbi_msg_ok(1000);
}

sub create_a_tab {
	my($package,$tab)=@_;
	#$package->statusmsg( "[navigator]   create_a_tab($tab)\n" );
	$current_tab=$tab;

	$package->paint();
	return 1;
}

sub raise_a_tab {
	my($package,$tab)=@_;
	$package->statusmsg( "[navigator]   raise_a_tab($tab) \n" );
	$current_tab=$tab;
	$package->paint();
	return 1;
}

sub event_handler
{
	my($package)=shift;
	my(%args)=@_;

	return unless $args{-curtab} eq $std_page_name
		or $args{-eventtype} eq "menu"
		or $args{-eventtype} eq "treertclick" ;

	# my(@path_dat)=split(/\//,$args{-entrypath});

	if( $args{-eventtype} eq "menu" ) {
		my($found)="FALSE";
		foreach ( @menuitems ) { $found="TRUE" if $args{-menuitem} eq $_; }
		return unless $found eq "TRUE";

		$package->statusmsg( "[navigator] event_handler() called\n" );
		$package->statusmsg( "[navigator] menuclick($args{-menuitem})\n" );

		my($typ)='sqlsvr' if $args{-menuitem} eq "Sql Server/DDL Navigator";
		$typ='oracle' if $args{-menuitem} eq "Oracle/DDL Navigator";
		$typ='sybase' if $args{-menuitem} eq "Sybase/DDL Navigator";

		$package->ignore_events(1);
		if( $args{-context} ne $typ ) {
			$package->refresh_tree($typ);
			$args{-context}=$typ;		# the refresh_tree sets global variable but we are not
							# triggering additional event so we do this.
			$args{-entrypath}="/$typ";
		}

		if( $args{-curtab} ne $std_page_name ) {
        		$package->statusmsg( "[navigator] event_handler() raising tab $std_page_name\n" );
			$main::notebook_f->raise(ucfirst($std_page_name));
		}
		$package->paint($args{-entrypath});
		$package->ignore_events(undef);

	} elsif( $args{-eventtype} eq "tabchange" ) {

		#return if $args{-curtab} ne $std_page_name;

		my($ok);
		foreach( main::get_server_types() ) { next if $args{-context} ne $_; $ok=1; last; }
		if( ! $ok ) {
			$package->statusmsg( "[navigator] menuclick($args{-menuitem})\n" );
			$package->refresh_tree('sqlsvr');
			$args{-context}='sqlsvr';	# the refresh_tree sets global variable but we are not
							# triggering additional event so we do this.
		}

		$package->statusmsg(  "[navigator] schema tab has been raised\n" );
		#$current_entrypath="/".main::get_label_by_type($args{-context}) if $current_entrypath eq "/";
		$args{-entrypath}="/".main::get_label_by_type($args{-context}) if $args{-entrypath} eq "/";
		$package->paint($args{-entrypath});
	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {
		#$package->statusmsg(  "[navigator] treebrowse()\n" );
		$package->statusmsg( "[navigator] event_handler() called\n" );
		$package->paint($args{-entrypath});
	} elsif( $args{-eventtype} eq "treertclick" ) {
		return unless $args{-clicktext} =~ /^DDL Explorer/;
		$package->statusmsg( "[navigator] event_handler() called\n" );
		$package->statusmsg( "[navigator] rtclick(Path=$args{-entrypath} Txt=$args{-clicktext})\n" );

		if( $args{-curtab} ne $std_page_name ) {
        		$package->ignore_events(1);
			$package->statusmsg( "[navigator] event_handler() raising tab $std_page_name\n" );
			$main::notebook_f->raise(ucfirst($std_page_name));
			$package->ignore_events(undef);
		}

		$package->paint($args{-entrypath});
	} elsif( $args{-eventtype} eq "expbutton" ) {
	} elsif( $args{-eventtype} eq "imgbutton" ) {
	}
}

sub getTreeRtclk {
	my($package,$curTreeType,$treeitmptr)=@_;
	$package->debugmsg(  "[navigator] getTreeRtclk(curtype=$curTreeType)\n" );

	return( undef ) unless $curTreeType eq "sybase" or $curTreeType eq "sqlsvr";
	my(@rightclicks);

	foreach my $itm ( @$treeitmptr ) {
		my($dummy,$typ,$server,$db)=split(/\//,$itm);
		if( defined $db ) {
			my( @db_rclk )= ("DDL Explorer $server - $db");
			push @rightclicks, \@db_rclk;
		}elsif( defined $server ) {
			my( @db_rclk )= ("DDL Explorer $server");
			push @rightclicks, \@db_rclk;
		} else {
			push @rightclicks, undef;
		}
	}
	return \@rightclicks;
}

# PAINT A SCREEN IN THE SCHEMA TAB FOR A DATABASE (if selected).
sub paint {
	my($package, $entryPath)=@_;
	#print "DBG: ",__FILE__,__LINE__,"statusmsgbar=$main::statusmsgbar";
        #print __FILE__," ",__LINE__," DBG ERROR: - STATUSMSGBAR is not a tk object\n" unless Tk::Exists($main::statusmsgbar);

	$package->statusmsg( "[navigator] paint($entryPath) path=$current_entrypath, tab=$current_tab)\n" );

	# if the last thing you painted in the tab is this
	$current_entrypath = $entryPath if defined $entryPath;
	if( $latestpath_by_tab{$current_tab} eq $current_entrypath ) {
		$package->statusmsg( "[navigator] returning as no need to redraw - tab=$current_tab path=$current_entrypath\n" );
		foreach (keys %latestpath_by_tab ) {
			$package->statusmsg( "[navigator] tab $_ is drawn for $latestpath_by_tab{$_} \n" );
		}
		return;
	}
	$latestpath_by_tab{$current_tab}=$current_entrypath;

	# hasnt been drawn
	$package->statusmsg( "[navigator] paint(path=$current_entrypath, tab=$current_tab)\n" );

	# so destroy!!!
	$tab_frame{$current_tab}->destroy() if defined $tab_frame{$current_tab} and Tk::Exists($tab_frame{$current_tab});
	#$tab_frame{$current_tab}=$tab_wholearea{$current_tab}->Scrolled(qw/Pane -bg orange -scrollbars osoe/)->pack(qw/-expand 1 -side top -fill both/);
			#-gridded xy -sticky nw
	#$tab_frame{$current_tab}=$tab_wholearea{$current_tab}->Scrolled(qw/Pane -bg orange -scrollbars osoe/)->pack(-fill=>'both', -side=>'top', -expand=>1);
	$tab_frame{$current_tab}= $tab_wholearea{$current_tab}->Frame()->pack( -fill=>'both', -side=>'top', -expand=>1);

	# what do we wish to connect to
	my($dummy,$label,$server,$db)=split(/\//,$current_entrypath);

	my($type)= main::get_type_by_label($label) if defined $label;
	#} elsif( ! defined $toi{$label} ) {

	my($errormsg);
	if( $current_entrypath eq "/oracle" ) {
		$errormsg = "Oracle Database Schema Navigator";
	} elsif( $current_entrypath eq "/sybase" ) {
		$errormsg = "Sybase ASE Database Schema Navigator";
	} elsif( $current_entrypath eq "/sqlsvr") {
		$errormsg = "Microsoft SQL Server Schema Navigator";
	} elsif( ! defined $label ) {
		$errormsg = "Warning: Must Select Server / Database";

	} elsif( ! defined $type ) {
		$errormsg = "Warning: Unknown Type ($label)";
	} elsif( $type ne "sqlsvr" and $type ne "sybase" ) {
		$errormsg = "Warning: Can Only Browse Sybase/Sql Server";
	} elsif( ! defined $server ) {
		$errormsg = "$label Schema Navigator: Please Select A Server";
	} elsif( ! defined $db ) {
		$errormsg = "$label Schema Navigator: Please Select A Database";
	}

	if( defined $errormsg ) {
		$package->statusmsg( "[navigator] $errormsg - path was $current_entrypath\n" );
		$latestpath_by_tab{$current_tab}="N.A.";
		$tab_frame{$current_tab}->Label( -text=>$errormsg,-relief=>'ridge' , -fg=>'red', -bg=>'white'
				)->pack( -side=>'top', -anchor=>'n', -fill=>'x', -expand=>1 );
		return;
	}

	#my($type)=$toi{$label};
	$tab_frame{$current_tab}->Label( -text=>"Object Navigator Manager For $label Server $server - Database $db",
		-relief=>'ridge' , -bg=>'white'
			)->pack( -side=>'top', -anchor=>'n', -fill=>'x' );

	# ok make two things - a rotext for the text and a list box for the list box
	$lb = $tab_frame{$current_tab}->Scrolled("Listbox",
		-scrollbars=>'oe',
		-selectmode=>'single',
		-width=>30,
		-font=>'tiny',
		-bg=>'white')->pack(
			-side   => "left",
                        -anchor => 'w',
                        -fill	=> 'y'  );

        $lb->selectionSet(0,0);
   	$lb->bind('<Double-1>', 	\&select_nav_item );
   	$lb->bind('<1>', 		\&select_nav_item );

	$textObject = $tab_frame{$current_tab}->Scrolled("ROText",
		-relief => "sunken", -wrap=>'none',
                -bg=>'white',-font=>'code',
                -scrollbars => "osoe")->pack(-side   => "right",
                                                -anchor => 'w',
                                                -expand => 1,
                                                 -fill   => 'both');


        $textObject->tagConfigure("t_hdr",   -foreground=>'black', -relief=>'raised', -background=>'beige');
        $textObject->tagConfigure("t_blue",  -foreground=>'blue');
        $textObject->tagConfigure("t_red",   -foreground=>'red');
        $textObject->tagConfigure("t_beige", -foreground=>'beige');

        $package->statusmsg( "[navigator] connecting to server $server\n" );
        my($c,$errmsg)=$main::connectionmanager->connect(-name=>$server, -type=>$type);
	if( ! $c ) {
		$textObject->insert('end',"[ Unbable to connect to server $server ]\n","t_red");
		$textObject->insert('end',"[ $errormsg ]\n","t_red");
		return;
	} else {
		$textObject->insert('end',"[ Text For DDL Object]\n","t_hdr");
	}
	main::colorize_tree($server);
	$curconnection=$c;
	$curdb=$db;

	%current_info = (
		-connection=>$c,
		-schema=>$db,
		-db_type=>$type,
		-reportname=>lc($current_tab)
	);

	$package->statusmsg( "[navigator] querying results ($current_tab)\n" );
	my(@rc)= get_schema_info(%current_info, -returntype=>'keys');
	if( $#rc<0 ) {
		$lb->insert('end',"[ None Found ]");
	} else {
		$lb->insert('end',@rc);
	}
	$package->statusmsg( "[navigator] results completed\n" );
}

# the following relies on $curconnection, $curdb, ...
sub select_nav_item {

	my($curitm)=$lb->get($lb->curselection());
	return if $curitm eq "[ None Found ]";
	main::_statusmsg( "[navigator] select_nav_item($curitm)\n");

	$textObject->delete("1.0",'end');
	$textObject->insert('end',"[ Object Text For $curitm ]\n","t_hdr");

	my(@rc)= get_object_info(%current_info, -returntype=>'ddl', -object=>$curitm );#, -debug=>\&main::_statusmsg);
	if( $#rc<0 ) {
		$textObject->insert('end',"[ No DDL Returned For This Object ]","t_red");
	} else {
		foreach (@rc) {
			s/^01000://;
			s/\s+$//;
			$textObject->insert('end',"$_\n","t_blue");
		}
	}

#	my(@rc);
#	if( $current_tab eq "Tables" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp__revtable $curitm");
#	} elsif( $current_tab eq "Triggers" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp__helptext $curitm");
#	} elsif( $current_tab eq "Views" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp__helptext $curitm");
#	} elsif( $current_tab eq "Procedures" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp__helptext $curitm");
#	} elsif( $current_tab eq "System" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp__revtable $curitm");
#	} elsif( $current_tab eq "Rules" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp__revbindings $curitm");
#	} elsif( $current_tab eq "Defaults" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp_helptext $curitm");
#	} elsif( $current_tab eq "Data" ) {
#		@rc = $main::connectionmanager->query(-connection=>$curconnection, -db=>$curdb,
#				-query=>"exec sp__helptable");
#	} else {
#		die "Unknown Tab $current_tab selected\n";
#	}
#
#	foreach (@rc) {
#		my($obj)=join(" ", $main::connectionmanager->decode_row($_));
#		$obj=~s/^01000://;
#		$obj=~s/\s+$//;
#		chomp $obj;chomp $obj;chomp $obj;
#        	$textObject->insert('end',"$obj\n","t_blue");
#        }

}

1;
