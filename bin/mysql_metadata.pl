select version(), database(), user(), current_user()
show global status
show variables
	basedir
show status
show /*!50002 GLOBAL */ STATUS LIKE 'COnnections'
show engines
see p390 of mysql cookbiook





#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2010 by Edward Barlow. All rights reserved.

use strict;
use lib qw(/apps/sybmon/gem_dev/lib /apps/sybmon/lib /lib /apps/sybmon/lib /Win32_perl_lib_5_8);
use Carp qw/confess/;
use Getopt::Long;
use File::Basename;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw($BATCH_ID $DEBUG $HOST $LOGIN $PASSWORD $SILENT);

sub usage
{
	print @_;

	print "$PROGRAM_NAME Version $VERSION\n\n";
	print @_;
	print "SYNTAX:\n";
	print $PROGRAM_NAME." --BATCH_ID -HOST=h -LOGIN=l -PASSWORD=p
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --SILENT  - only real output
   --DEBUG \n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

# die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"HOST=s"				=> \$HOST ,
		"LOGIN=s"			=> \$LOGIN ,
		"PASSWORD=s"		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

# $BATCH_ID = "InvPromote" unless $BATCH_ID;
MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Gem Monitor') if $BATCH_ID;
print "Running In Diag Mode\n" if $DEBUG;
$DEBUG=0 unless defined $DEBUG;

my($MYSQL)="/usr/bin/mysql" if -x "/usr/bin/mysql";

die "NO MYSQL command line found\n" unless $MYSQL;
die "NO HOST passed" unless $HOST;
die "NO LOGIN passed" unless $LOGIN;
die "NO PASS passed" unless $ROOTPASS;
my($cmd)="$MYSQL -h$HOST -u$LOGIN -p$ROOTPASS -e\"show status;\" -E";

open(C,$cmd) or die "Cant run $cmd\n";
while(<C>) {
	chomp;chomp;
	print "RC=$_\n";
}
close(C);

MlpBatchJobEnd() if $BATCH_ID;
print "Successful Completion\n";
exit(0);