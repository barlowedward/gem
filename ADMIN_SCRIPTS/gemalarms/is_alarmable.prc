create procedure is_alarmable (
@system          		varchar(30)	=NULL
)
as
begin

-- Return 'No' or 'Yes'
if exists ( select * from GEM_servers where system_name=@system and blackedout_until is not null and blackedout_until>getdate() )
begin
	select 'No'
end
else
if exists ( select * from IgnoreList where system=@system)
	select 'No'
else
	select 'Yes'

end
go
