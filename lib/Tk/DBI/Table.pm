package Tk::DBI::Table;
#------------------------------------------------
# automagically updated versioning variables -- CVS modifies these!
#------------------------------------------------
our $Revision           = '$Revision: 1.13 $';
our $CheckinDate        = '$Date: 2003/11/06 17:55:52 $';
our $CheckinUser        = '$Author: xpix $';
# we need to clean these up right here
$Revision               =~ s/^\$\S+:\s*(.*?)\s*\$$/$1/sx;
$CheckinDate            =~ s/^\$\S+:\s*(.*?)\s*\$$/$1/sx;
$CheckinUser            =~ s/^\$\S+:\s*(.*?)\s*\$$/$1/sx;
#-------------------------------------------------
#-- package Tk::DBI::Table -----------------------
#-------------------------------------------------

=head1 NAME

Tk::DBI::Table - Megawidget to display a sql-Statement in HList.

=head1 SYNOPSIS

	use Tk;
	use Tk::DBI::Table;

	my $top = MainWindow->new;
	my $tkdbi = $top->DBITable(
			-sql		=> 'select * from table',
			-dbh   		=> $dbh,
			-display_id	=> 0,
			-headerbackground => $color,
			-headerforeground => $color,
			-selectedcolor => $color,
			-selectedbackground => $color,
			-rowcolorfunc	=> \&func,
			-filterfunc	=> \&filterfunc,
			-browsecmd	=> \&func,
			-command	=> \&func
		    )->pack(expand => 1, -fill => 'both');

	MainLoop;

=head1 DESCRIPTION

This is a megawidget that enables you to display sql statements from a database.
The features are:

=over 4

=item each column has a ResizeButton for flexible width

=item The user can activate any Button to sort the column in the directions 'ASC', 'Desc' or 'None'.

=item Sorted column can display with a extra style

=back

=cut

use Tk::HList;
use Tk::Compound;
use Tk::ResizeButton;
#use Data::Dumper;

use base qw/Tk::Derived Tk::Frame/;

use strict;

Construct Tk::Widget 'DBITable';

my ($BITMAPDOWN, $BITMAPUP);

# ------------------------------------------
sub ClassInit
# ------------------------------------------
{
	my($class,$mw) = @_;

	unless(defined($BITMAPDOWN))
	{
		$BITMAPUP = __PACKAGE__ . "::uparrwow";
		my $bits_up = pack("b10"x10,
				"..........",
				"..........",
				"..........",
				".....#....",
				"....###...",
				"...#####..",
				"..#######.",
				".#########",
				"..........",
				".........."
				);
		$mw->DefineBitmap($BITMAPUP => 10,10, $bits_up);

		$BITMAPDOWN = __PACKAGE__ . "::downarrwow";
		my $bits_down = pack("b10"x10,
				"..........",
				"..........",
				"..........",
				".#########",
				"..#######.",
				"...#####..",
				"....###...",
				".....#....",
				"..........",
				".........."
				);
		$mw->DefineBitmap($BITMAPDOWN => 10,10, $bits_down);

	}
}


# ------------------------------------------
sub Populate {
# ------------------------------------------
	my ($obj, $args) = @_;

	$obj->{next_hlist_itemkey}=0;
=head1 WIDGET-SPECIFIC OPTIONS

=head2 -dbh => $dbh

A database handle, this will return a error if not defined.

=cut

	$obj->{dbh} 		= delete $args->{'-dbh'};

=head2 -sql => 'select * from table'

A sql statement, this will return an error if not defined.

=cut

	$obj->{sql}		= delete $args->{'-sql'};

=head2 -debug [I<0>|1]

This is a switch that turns on debug output to the normal console (STDOUT).

=cut

	$obj->{debug} 		= delete $args->{'-debug'} 	|| 0;

=head2 -display_id [I<Off>|On]

This is a switch for displaying the index column.

=cut

	$obj->{display_id}	= delete $args->{'-display_id'} || 0;

=head2 -columnWidths [colWidth_0, colWidth_1, colWidth_2, ...]

Default field column width.

=cut

	$obj->{maxchars}	= delete $args->{'-maxchars'};

=head2 -maxchars number or {col1 => number}

Maximum displaying chars in the cells. Global or only in named columns.

=cut

	$obj->{maxcols}		= delete $args->{'-maxcols'};

=head2 -maxcols number

Maximum columns in this table, this replace the count of fields in sql-statment.

=cut


	$obj->{columnWidths}	= delete $args->{'-columnWidths'};

=head2 -srtColumnStyle(option => value)

Column sort style.

=cut

	$obj->{srtColumnStyle}	= delete $args->{'-srtColumnStyle'};

	$obj->{filterfunc}	= delete $args->{'-filterfunc'} || undef;
	$obj->{rowcolorfunc}	= delete $args->{'-rowcolorfunc'} || undef;
	$obj->{browsecmd}	= delete $args->{'-browsecmd'} || undef;
	$obj->{command}		= delete $args->{'-command'} || undef;
	$obj->{headerbackground}	= delete $args->{'-headerbackground'} || "white";
	$obj->{headerforeground}	= delete $args->{'-headerforeground'} || "black";
	$obj->{selectedcolor}		= delete $args->{'-selectedcolor'} || "black";
	$obj->{selectedbackground}	= delete $args->{'-selectedbackground'} || "white";

	$obj->SUPER::Populate($args);

=head1 METHODS

These are the methods you can use with this Widget.

=cut

	my %specs;

=head2 $dbitable->sql( new_sql_statement );

Set a new SQL-Statement and will display this.

=cut

	$specs{-filterfunc} = [qw/METHOD  filterfunc      Filterfunc/,   	undef];
	$specs{-rowcolorfunc} = [qw/METHOD  rowcolorfunc      Rowcolorfunc/,   	undef];
	$specs{-headerbackground} = [qw/METHOD  headerbackground      Headerbackground/,   	undef];
	$specs{-headerforeground} = [qw/METHOD  headerforeground      Headerforeground/,   	undef];
	$specs{-selectedcolor} = [qw/METHOD  selectedcolor      selectedcolor/,   	undef];
	$specs{-selectedbackground} = [qw/METHOD  selectedbackground      selectedbackground/,   	undef];
	$specs{-browsecmd} = [qw/METHOD  browsecmd      Browsecmd/,   	undef];
	$specs{-command} = [qw/METHOD  command      Command/,   	undef];

	$specs{-info} 	= [qw/METHOD  info      Info/,   	undef];


=head2 $DBITree->info('anchor, bbox, children, B<data>, dragsite, dropsite ...', $id);

This is a wrapper to the HList Method ->info. The default method is info('data', ...).
Please read the manual from Tk::HList.

=cut

	$specs{-sql} 	= [qw/METHOD  sql      Sql/,   		undef];

=head2 $dbitable->refresh( [to_sort_col_number] );

Refresh the table and sort (optional) the col number.

=cut

	$specs{-refresh} 	= [qw/METHOD  refresh      Refresh/,   		undef];

=head2 $dbitable->sortcol( to_sort_col_number );

Refresh the table and sort the col number or return the actually col sort number.

=cut

	$specs{-sortcol} 	= [qw/METHOD  sortcol      SortCol/,   		undef];

=head2 $dbitable->direction( ['NONE', 'ASC' or 'DESC'] );

Set a new sorting direction. no parameter will return the actual sort direction.

=cut

	$specs{-direction} 	= [qw/METHOD  direction    Direction/,		'NONE'];

        $obj->ConfigSpecs(%specs);

	$obj->refresh();
	$obj->debug("Populate() done\n");

} # end Populate


# Class private methods;
# ------------------
sub sql {
# ------------------
	my $obj = shift;
	$obj->{sql} = shift || $obj->{sql};
	delete $obj->{svhdrptr} ;
	delete $obj->{svdatptr};
	$obj->refresh();
	$obj->debug("sql() done\n");
}

# ------------------------------------------
sub refresh {
# ------------------------------------------
	my $obj = shift or return warn("No object");
	my $sortcolumn = shift;
	$obj->debug("starting refresh()\n");

	$obj->toogle_direction($sortcolumn)
		if(defined $sortcolumn);

	my($old_num_fields)=$#{$obj->{fields}};
	$obj->debug("OLD NUM FIELDS=$old_num_fields\n");

	# get data
	$obj->{data} = my $data = $obj->getSql() or return;
	$obj->debug("Data Retrieved\n");
	return $obj->error("No Data Retrieved") unless defined $data;
	return $obj->error("No Header Retrieved") unless defined $obj->{fields};

        my @fields = @{$obj->{fields}};

        $obj->debug("Flds Returned=",join(" ", @fields),"\n");

	# delete the object if num cols has changed
	# TO DO - should be rebuild probably if we change any of the header items...
	if( $old_num_fields != $#fields ) {
		$obj->debug("Rebuilding Object - New Num Fields=$#fields\n");
		$obj->{table}->destroy() if defined $obj->{table} and Tk::Exists( $obj->{table});
		delete $obj->{table};
		for my $c ( 0..$old_num_fields ) {
			last unless Tk::Exists($obj->{header}->{$c});
			$obj->{header}->{$c}->destroy();
			delete $obj->{header}->{$c};
		}
		$obj->debug("Done Destroy\n");
	}

	# Create HList
	unless(defined $obj->{table}) {
		my $cols = $obj->{maxcols} || scalar @fields;

		$obj->{table} = $obj->Scrolled('HList',
			-scrollbars 	=> 'osoe',
			-columns	=> $cols,
			-header		=> 1,
			-selectbackground => $$obj{selectedbackground},
			-selectforeground => $$obj{selectedcolor},
		)->pack(-expand => 1,
			-fill => 'both');

		$obj->Advertise("table" => $obj->{table});   #TEXT PART.
	}

	my $hl = $obj->{table};

	$hl->configure(	-command => sub {
		return if $#_>0;	# i am not sure why, but browse is called 2x under windows
		$obj->debug("In hlist -command native\n");
		my($key)=@_;

		return $obj->error("Cant Find Row Key $key\n")
			unless defined $obj->{roworder}[$key];
		my $zeile = $obj->{roworder}[$key];
		my(%datahash);
		my($cnt)=0;
		foreach ( @{$obj->{fields}} ) {
			$datahash{lc($_)} = $zeile->[$cnt++];
		}
		$$obj{command}(%datahash);
		$obj->debug("End hlist -command native\n");

	}) if defined $$obj{command};

	$hl->configure(	-browsecmd => sub {
		return if $#_>0;	# i am not sure why, but browse is called 2x under windows
		my($key)=@_;		# the key is the first column...

		return $obj->error("Cant Find Row Key $key\n")
			unless defined $obj->{roworder}[$key];
		my $zeile = $obj->{roworder}[$key];
		#$$obj{browsecmd}(@$zeile);
		my(%datahash);
		my($cnt)=0;
		foreach ( @{$obj->{fields}} ) {
			$datahash{lc($_)} = $zeile->[$cnt++];
		}
		$$obj{browsecmd}(%datahash);

	}) if defined $$obj{browsecmd};

	# create header
	my $c = -1;
	$obj->debug("Start Build\n");
	foreach my $field (@fields) {
		$c++;
		$obj->debug("Building Field $field $c\n");
		$obj->{header}->{$c} = $hl->ResizeButton(
		  -relief 	=> 'flat',
		  -anchor	=> 'nw',
		  -border	=> -2,
		  -pady 	=> -10,
		  -padx 	=> 10,
		  -widget 	=> \$hl,
		  -column 	=> $c,
		  -command	=> [\&refresh, $obj, $c],
		  -background   	=> $$obj{headerbackground},
		  -activebackground   	=> $$obj{headerbackground},
		  -foreground   	=> $$obj{headerforeground},
		);

		$obj->Advertise(sprintf("HB_%d", $c) => $obj->{header}->{$c});   #Buttons PART.

		# create Images (Text)
		my $img = $obj->{header}->{$c}->Compound;
		$obj->{header}->{$c}->configure(-image => $img);
		$img->Line;
		$img->Text(-text => $field);
		if(defined $sortcolumn and $sortcolumn == $c and ($obj->direction eq 'ASC' or $obj->direction eq 'DESC')) {
			$img->Space(-width => 4);
			$img->Bitmap(-bitmap => ($obj->direction eq 'ASC' ? $BITMAPUP : $BITMAPDOWN));
			$img->Space(-width => 10);
		} else {
			$img->Space(-width => 24);
		}

		$hl->headerCreate($c,
			-itemtype => 'window',
			-widget	  => $obj->{header}->{$c},
			-headerbackground=>$$obj{headerbackground}
		);

		$hl->columnWidth($c, $obj->{columnWidths}->[$c])
			if(defined $obj->{columnWidths}->[$c]);
	}

	$hl->columnWidth(0, 0) unless($obj->{display_id});

#printf("SortCol: %s, Type: %s, Direction: %s\n",
#	(defined $sortcolumn ? $sortcolumn : 'undef'),
#	(defined $sortcolumn ? $obj->type($sortcolumn) : 'undef'),
#	(defined $sortcolumn ? $obj->direction : 'undef')
#	);

	$obj->reset_hlist();
	$obj->debug("Start Sort & AddRows\n");

	#use Data::Dumper;
	#$obj->debug("DBG DBG: ",Dumper $data,"\n");

	my $type = $obj->type($sortcolumn);
	if(defined $sortcolumn and $type eq 'TXT' and $obj->direction eq 'ASC') {
		foreach my $zeile (sort { $a->[$sortcolumn] cmp $b->[$sortcolumn] } @$data) {
			$obj->draw_row($hl, $zeile, $sortcolumn);
		}
	} elsif(defined $sortcolumn and $type eq 'TXT' and $obj->direction eq 'DESC') {
		foreach my $zeile (sort { $b->[$sortcolumn] cmp $a->[$sortcolumn] } @$data) {
			$obj->draw_row($hl, $zeile, $sortcolumn);
		}
	} elsif(defined $sortcolumn and $type eq 'INT' and $obj->direction eq 'ASC') {
		foreach my $zeile (sort { $a->[$sortcolumn] <=> $b->[$sortcolumn] } @$data) {
			$obj->draw_row($hl, $zeile, $sortcolumn);
		}
	} elsif(defined $sortcolumn and $type eq 'INT' and $obj->direction eq 'DESC') {
		foreach my $zeile (sort { $b->[$sortcolumn] <=> $a->[$sortcolumn] } @$data) {
			$obj->draw_row($hl, $zeile, $sortcolumn);
		}
	} else {
		foreach my $zeile (@$data) {
			$obj->draw_row($hl, $zeile);
		}
	}
	$obj->debug("Done Refresh\n");
}

# ------------------------------------------
sub draw_row {
# ------------------------------------------
	my ($obj, $hl, $zeile, $sortcolumn) = @_;
	#my($itmkey)=$zeile->[0];

	my($itmkey)=$obj->{next_hlist_itemkey}++;
	push @{$obj->{roworder}}, $zeile;
	$obj->debug("Draw row $itmkey (",join(" ",@$zeile),"\n");
	#$obj->debug("DBG DBG Showing zeile",Dumper $zeile,"\n");

	if( defined  $$obj{filterfunc} ) {
		#$obj->debug("DBG DBG filterfunc called\n");
		my(%datahash);
		my($cnt)=0;
		foreach ( @{$obj->{fields}} ) {
			#$obj->debug("Cnt=$cnt Field=$_ Val=$zeile->[$cnt]\n";
			$datahash{lc($_)} = $zeile->[$cnt++];
		}
		return unless $$obj{filterfunc}( %datahash );
	}

	$hl->add($itmkey);
	my $c = -1;

	my($rowstyle);
	if( defined  $$obj{rowcolorfunc} ) {
		#$obj->debug("DBG DBG rowcolor called\n");
		my(%datahash);
		my($cnt)=0;
		foreach ( @{$obj->{fields}} ) {
			#$obj->debug("Cnt=$cnt Field=$_ Val=$zeile->[$cnt]\n";
			$datahash{lc($_)} = $zeile->[$cnt++];
		}
		$rowstyle= $$obj{rowcolorfunc}( %datahash );	#$obj->{fields},$zeile );
	}

	foreach my $column (@$zeile) {
		$c++;
		my $maxchars =
			(ref $obj->{maxchars} eq 'HASH'
				? $obj->{maxchars}->{$obj->{fields}->[$c]}
				: $obj->{maxchars}
			) || 0;

		$column = ' ' unless(defined $column);
		$column =~ s/(\r|\n)//sig;
		$column = substr($column, 0, $maxchars).'...'
			if($maxchars and length($column)>$maxchars);
		#$obj->debug("DBG DBG 		key = $itmkey text=$column\n");

		$hl->itemCreate( $itmkey, $c, -text => $column	);
		if(defined $sortcolumn and defined $obj->{srtColumnStyle} and $sortcolumn == $c) {
			$hl->itemConfigure($itmkey, $c, -style => $obj->{srtColumnStyle}) ;
		} elsif( defined $rowstyle )  {
			$hl->itemConfigure($itmkey, $c, -style => $rowstyle);
		}
	}
}

# ------------------------------------------
sub sortcol {
# ------------------------------------------
	my $obj = shift or croak("No object");
	$obj->{sortcol} = shift || $obj->{sortcol};
	$obj->refresh($obj->{sortcol});
	$obj->debug("sortcol() done\n");
}

# ------------------------------------------
sub info {
# ------------------------------------------
	my $obj = shift;
	my $typ = shift or return $obj->error("No Type");
	my $entry = shift;

	if($typ =~ /^(selection|anchor|dragsite|dropsite)$/si) {
		my @ids = $obj->{table}->info($typ);
		return \@ids;
	}

	if($entry !~ /\:/) {
		$entry = $obj->{Paths}->{$entry}
			or return $obj->error('Can\'t find <%s> in Paths!', $entry);
	}

	return $obj->error('Can\'t find Id: %s', $entry)
		if($typ ne 'exists' and ! $obj->{table}->info('exists', $entry));
	return $obj->{table}->info(${typ}, $entry);
}


# ------------------------------------------
sub toogle_direction {
# ------------------------------------------
	my $obj = shift or croak("No object");
	my $sortcolumn = shift;
	return $obj->direction('ASC') if(defined $sortcolumn and defined $obj->{sortcol} and $obj->{sortcol} != $sortcolumn);
	return $obj->direction('ASC') if($obj->direction() eq 'NONE');
	return $obj->direction('DESC') if($obj->direction() eq 'ASC');
	return $obj->direction('NONE') if($obj->direction() eq 'DESC');
}


# ------------------------------------------
sub direction {
# ------------------------------------------
	my $obj = shift or croak("No object");
	$obj->{direction} = shift || return $obj->{direction};
}

# ------------------------------------------
sub type {
# ------------------------------------------
	my $obj = shift or croak("No object");
	my $snr = shift or return;
	my $data = $obj->{data} || return;
	my $type = 'INT';
	foreach (@$data){
		$_->[$snr] = ' ' unless(defined $_->[$snr]);
		$type = 'TXT' if(defined $_->[$snr] and $_->[$snr] =~ /[^0-9]+/);
	}
	return $type;
}

sub setConnection {
	my($obj,$dbh)=@_;
	$obj->{dbh}=$dbh;
}

# ------------------------------------------
sub getSql {
# ------------------------------------------
	my $obj = shift or croak("No object");
	my $sql = $obj->{sql};
	my $dbh = $obj->{dbh};

	if( $sql eq "" or ! defined $sql or ! defined $dbh ) {
		# ok return saved data
		$obj->debug("Using Saved Data\n");
		$obj->{fields} = $obj->{svhdrptr};

#		use Data::Dumper
#        	$obj->debug("refresh data is ");
#        	print Dumper $obj->{svdatptr};
#        	print "\n";
#        	print "DBG refresh fields is ";
#        	print Dumper $obj->{fields};
#        	print "\n";

		return $obj->{svdatptr};
	}

	$obj->debug("Running query $sql\n");
	my $sth = $dbh->prepare($sql) or warn("$DBI::errstr - $sql");
	$sth->execute or warn("$DBI::errstr - $sql");
	$obj->{fields} = $sth->{'NAME'};
	my($ref)=	$sth->fetchall_arrayref;

#	use Data::Dumper
#        $obj->debug("refresh data is ");
#        print Dumper $obj->{svdatptr};
#        print "\n";
#        print "DBG refresh fields is ";
#        print Dumper $ref;
#        print "\n";

	return $ref;
}


# ------------------------------------------
sub debug {
# ------------------------------------------
	my $obj = shift;
	my $msg = shift || return;
	return unless $obj->{debug};
	chomp $msg;
	printf("debug: %s\n", $msg);
}

# ------------------------------------------
sub error {
# ------------------------------------------
	my $obj = shift;
	my $msg = shift;
	#$obj->bell;
	unless($msg) {
		my $err = $obj->{error};
		$obj->{error} = '';
		return $err;
	}
	$obj->{error} = sprintf($msg, @_);
	warn $obj->{error};
	return undef;
}

sub reset_hlist {
	my $obj = shift;
	$obj->{table}->delete('all');
	$obj->{next_hlist_itemkey}=0;
	my(@x);
	$obj->{roworder}=\@x;
}

sub setData {
	my($obj,$hdrptr,$datarefref)=@_;
	$obj->debug("setData() starting\n");
	$obj->{sql} = "";
	$obj->{svhdrptr} =$hdrptr;
	$obj->{svdatptr}=$datarefref;
	$obj->refresh();
	$obj->debug("setData() done\n");
}

sub appendData {
	my($obj,$datref)=@_;
	return error("No Saved Data") unless defined $obj->{svdatptr};
	push @{$obj->{svdatptr}}, $datref;
}


sub getRowkey {
	my($obj,$rowindex)=@_;
	my(@data,$key);
	return($key,\@data);
}

1;

=head1 ADVERTISED WIDGETS

=head2 'table' => HList-Widget


This is a normal HList widget. I.e.:

	$dbitable->Subwidget('table')->configure(
		-command = sub{ printf "This is id: %s\n", $_[0] },
	};


=head2 'HB_<column number>' => Button-Widget

This is a (Resize)Button widget. This displays a Compound image with text and image.

=head1 CHANGES

$Log: Table.pm,v $
Revision 1.13  2003/11/06 17:55:52  xpix
! bugfixes in refresh_id
* not hudge load for tree

Revision 1.11  2003/07/17 14:59:53  xpix
! many little bugfixes

Revision 1.8  2003/04/29 16:25:58  xpix
* reformat

Revision 1.6  2003/04/29 16:22:52  xpix
* chnages tag


=head1 AUTHOR

xpix@netzwert.ag

Copyright (C) 2003 , Frank (xpix) Herrmann. All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.


=head1 KEYWORDS

Tk::DBI::*, Tk::ResizeButton, Tk::HList

