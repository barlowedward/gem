-- report_key=DM_OS_PERFORMANCE_COUNTERS
-- report_type=PERFORMANCE
-- run_frequency=15min
-- do_diff=N
-- keep_hours=Y
-- keep_6=Y
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=
-- report_footer=
-- enabled=N
-- performancetype=int
-- performancekey=performance_counters

select *,base=convert(bigint,0) into #DOPC
from sys.dm_os_performance_counters
where
	cntr_value is not null and
(
	instance_name in (
		 'Average wait time (ms)', 'Waits started per second' )
	or  counter_name in ('Page reads/sec',
		'Page writes/sec',
		'Logins/sec',
		'Logouts/sec',
		'User Connections',
		'Processes blocked',
		'Batch Requests/sec',
		-- 'Transactions',
		-- 'Cache Hit Ratio',
		'Cache Hit Ratio Base',
		'Per Database',
		'Data File(s) Size (KB',
		'Log File(s) Size (KB)',
		'Transactions/sec',
		'Repl. Trans. Rate',
		'Log Truncations',
		'Log Growths',
		'Log Shrinks',
		'Log Bytes Flushed/sec',
		'Shrink Data Movement Bytes/sec',
		'Backup/Restore Throughput/sec',
		'Log Cache Hit Ratio Base',
		'Log Cache Hit Ratio',
		'Log Cache Reads/sec',
 		'Target Server Memory (KB)',
		'Total Server Memory (KB)',
		'Total Server Memory (KB)',
		'Average Wait Time (ms)',
		'Errors/sec',
 		'Repl. Trans. Rate' )
	or ( counter_name in (
		'Lock Requests/sec',
		'Lock Timeouts/sec'
	) and instance_name='_Total' )
)

update #DOPC
set base = ( select d2.cntr_value from #DOPC d2
									where d2.cntr_type= 1073939712
									-- and d.counter_name+' Base'=d2.counter_name
									and d.instance_name=d2.instance_name
									and d.object_name=d2.object_name
									and d2.cntr_value is not null
									and d2.cntr_value != 0 )
from #DOPC d
where d.cntr_type = 537003264

update #DOPC set cntr_value = ( 100 * cntr_value ) / base where base != 0

delete #DOPC where cntr_type= 1073939712
/*
select *, ( select d2.cntr_value from #DOPC d2
									where d2.cntr_type= 1073939712
									-- and d.counter_name+' Base'=d2.counter_name
									and d.instance_name=d2.instance_name
									and d.object_name=d2.object_name
									and d2.cntr_value is not null )
from #DOPC d
where d.cntr_type = 537003264

select d2.counter_name, d.counter_name, * from #DOPC d, #DOPC d2
where d.cntr_type = 537003264
and d2.cntr_type= 1073939712
--and rtrim(d2.counter_name) = rtrim(d.counter_name+' Base')
and d.instance_name=d2.instance_name
and d.object_name=d2.object_name
*/

select @@SERVERNAME, rtrim(counter_name), rtrim(instance_name), '' as 'k3', cntr_value
/*
, cntr_type,
case when cntr_type = 537003264 then 'RATIO'
     when cntr_type= 1073939712 then 'BASE FOR RATIO'
     when cntr_type= 272696576 then 'PER SECOND RATE'
     when cntr_type= 65792 then 'VALUE'
end
*/
from #DOPC
order by object_name, counter_name, instance_name

/* WHat is missing
select *
into #X
from sys.dm_os_performance_counters

delete #X
from #X x, #DOPC d
where
    d.counter_name = x.counter_name
and d.instance_name= x.instance_name
and d.object_name= x.object_name

select * from #X
select count(*) from #DOPC
select distinct counter_name, instance_name from #DOPC

--drop table #DOPC

create unique clustered index x on #DOPC ( counter_name, instance_name)
select * from #DOPC where counter_name like 'Transactions %' and instance_name = '_Total'

*/


