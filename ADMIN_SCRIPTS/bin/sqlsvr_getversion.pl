#!/apps/perl/linux/perl-5.8.2/bin/perl

#DOESNT WORK RIGHT - NEEDS TO RUN ON LOCAL SERVER TO GET APPROPARIATE VERSIONING SIGH

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use DBIFunc;
use Repository;
use Getopt::Long;
use MlpAlarm;

use vars qw( $BATCHID $DAYS $DEBUG $LISTEVENTS $SERVER);

sub usage
{
	print "usage: sqlsvr_getversion.pl --BATCHID=id --DEBUG --SERVER=svr --LISTEVENTS --DAYS=days\n";
   	return "\n";
}

$| =1;		# dont buffer standard output

die usage() unless GetOptions(
		"SERVER=s" 		=> \$SERVER,
		"BATCHID=s" 		=> \$BATCHID ,
		"DAYS=s" 		=> \$DAYS ,
		"LISTEVENTS"		=> \$LISTEVENTS,
		"DEBUG"     		=> \$DEBUG );

$DAYS=30 unless $DAYS;
$BATCHID="DbGetVersion" unless $BATCHID;

$|=1;
my($VERSION)="1.0";

#foreach ( get_password(-type=>'sybase') ) { 	$servertype{$_}="sybase"; }
MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor');

print "sqlsvr_getversion.pl v1.0: Run at ".localtime(time)."\n";
print "Fetching version info for database servers\n";
$DAYS*=60*60*24;
$DAYS=time-$DAYS;

my @messages;
sub doprint {
	push @messages,@_;
	#print "XXX: ",@_,"\n";
}

dbi_set_option(-print=>\&doprint);

if( $SERVER ) {
	my(@sqlsvrs)=split(/,/,$SERVER);
	foreach my $SERVER ( @sqlsvrs ) {
		do_one_server($SERVER);
	}
} else {
	my(@sqlsvrs)= get_password(-type=>'sqlsvr');
	foreach my $SERVER ( @sqlsvrs ) {
		do_one_server($SERVER,"sqlsvr");
	}
	@sqlsvrs= get_password(-type=>'sybase');
	foreach my $SERVER ( @sqlsvrs ) {
		do_one_server($SERVER,"sybase");
	}

}

sub do_one_server {
	my($SERVER,$TYPE)=@_;
	print "analyzing... $SERVER\n" if $DEBUG;
	$TYPE="sybase" unless $TYPE;
	my($USER,$PASSWORD)=get_password(-type=>$TYPE,-name=>$SERVER);
	if( $TYPE eq "sybase" and ! $USER ) {
		($USER,$PASSWORD)=get_password(-type=>"sqlsvr",-name=>$SERVER);
	}
	if( ! $USER ) {
		print "*******************************\n";
		print "* Cant fetch password for $SERVER\n";
		print "*******************************\n";
	}

	my($connection)=dbi_connect( -srv=>$SERVER,
		-login=>$USER,
		-password=>$PASSWORD,
		# -type=>$TYPE,
		-debug=>$DEBUG,
		-connection=>1 );

	if ( ! $connection ) {

		print "*******************************\n";
		print "* ".join("* ",@messages) if $DEBUG;
		@messages=();
		print "* Cant connect to $SERVER using dbi\n";
		print "*******************************\n";
		next;
	}

	my($DBTYPE,$DBVERSION)=dbi_db_version( -connection=>$connection );
	print "SERVER=$SERVER TYPE=$DBTYPE VERSION=$DBVERSION\n";
	dbi_disconnect();
};

#foreach my $SERVER ( sort @sqlsvrs ) {
#	print "SERVER: $SERVER \n";
#	$SHORTOSVERSION{$SERVER}=~s/\n/\//g;
#	chomp $SHORTOSVERSION{$SERVER};
#	chomp $OSVERSION{$SERVER};
#	print "  VER: $SHORTOSVERSION{$SERVER}\n";
#	print "   OS: $OSVERSION{$SERVER}\n";
#	foreach ( keys %BOOTRECORDS ) {
#		next unless /^$SERVER:/;
#		s/^$SERVER://;
#		print "   Rebooted at ".localtime($_)."\n";
#	}
#	print "\n";
#}
print "Completed read from eventlogs\n" if defined $DEBUG;
MlpBatchJobEnd();
exit(0);


