create procedure PER_saveintstat (
  @system_name        varchar(30)    =null,
  @system_type        varchar(30)    =null,

  @key_2  			 	 varchar(30) 	=null,
  @key_3  				 varchar(30) 	=null,
  @key_4  				 varchar(30) 	=null,

  @perf_key             varchar(30)    =null,
  @perf_time          datetime          = null,

  @perf_int_1         bigint =null,
  @perf_int_2         bigint =null,
  @perf_int_3         bigint =null,
  @perf_int_4         bigint =null,
  @perf_int_5         bigint =null,
  @perf_int_6         bigint =null
 /* @perf_int_7         bigint =null,
  @perf_int_8         bigint =null,
  @perf_int_9         bigint =null,
  @perf_int_10         bigint =null,
  @perf_int_11         bigint =null,
  @perf_int_12         bigint =null,
  @perf_int_13         bigint =null,
  @perf_int_14         bigint =null,
  @perf_int_15         bigint =null,
  @perf_int_16         bigint =null,
  @perf_int_17         bigint =null,
  @perf_int_18         bigint =null,
  @perf_int_19         bigint =null,
  @perf_int_20         bigint =null */
)
as
begin
	if @perf_time is null
		select @perf_time=getdate()

   if exists ( select * from PER_lastintstatistics
         where system_name= @system_name
           and   system_type= @system_type
           and   perf_key   = @perf_key
	        and		  key_2=@key_2
	        and		  key_3=@key_3
	        and		  key_4=@key_4   )
   begin

      insert PER_intstatistics
         (   system_name,  system_type,  perf_key,  perf_time,  weight,
         		  key_2, key_3, key_4,
                 perf_int_1,   perf_int_2,  perf_int_3,  perf_int_4,  perf_int_5, perf_int_6
            /*     perf_int_6,   perf_int_7,  perf_int_8,  perf_int_9,  perf_int_10,
                 perf_int_11,  perf_int_12,  perf_int_13,  perf_int_14,  perf_int_15,
                 perf_int_16,  perf_int_17,  perf_int_18,  perf_int_19,  perf_int_20 */
                 )
      select     @system_name,  @system_type,  @perf_key,  @perf_time, 1,
      				@key_2, @key_3, @key_4,
                 @perf_int_1 - perf_int_1,  @perf_int_2 - perf_int_2,  @perf_int_3 - perf_int_3,
                    @perf_int_4 - perf_int_4,  @perf_int_5 - perf_int_5,
               @perf_int_6 - perf_int_6

               /* ,  @perf_int_7 - perf_int_7,  @perf_int_8 - perf_int_8,
                  @perf_int_9 - perf_int_9,  @perf_int_10 - perf_int_10,
               @perf_int_11 - perf_int_11,  @perf_int_12 - perf_int_12,  @perf_int_13 - perf_int_13,
                  @perf_int_14 - perf_int_14,  @perf_int_15 - perf_int_15,
               @perf_int_16 - perf_int_16,  @perf_int_17 - perf_int_17,  @perf_int_18 - perf_int_18,
                  @perf_int_19 - perf_int_19,  @perf_int_20 - perf_int_20 */
      from       PER_lastintstatistics
      where    system_name=@system_name
        and      system_type= @system_type
        and      perf_key   = @perf_key
        and		  key_2=@key_2
        and		  key_3=@key_3
        and		  key_4=@key_4

      begin transaction

      DELETE PER_lastintstatistics
         where system_name= @system_name
           and   system_type= @system_type
           and   perf_key   = @perf_key
            and		  key_2=@key_2
        		and		  key_3=@key_3
        		and		  key_4=@key_4

      insert PER_lastintstatistics
         (   system_name,  system_type,  perf_key,  perf_time,
                key_2, key_3, key_4,
                perf_int_1,  perf_int_2,  perf_int_3,  perf_int_4,  perf_int_5,
              perf_int_6
              /* ,  perf_int_7,  perf_int_8,  perf_int_9,  perf_int_10,
              perf_int_11,  perf_int_12,  perf_int_13,  perf_int_14,  perf_int_15,
              perf_int_16,  perf_int_17,  perf_int_18,  perf_int_19,  perf_int_20 */
              )
      values
         (   @system_name,  @system_type,  @perf_key,  @perf_time,
                @key_2, @key_3, @key_4,
                @perf_int_1,  @perf_int_2,  @perf_int_3,  @perf_int_4,  @perf_int_5,
              @perf_int_6
              /* ,  @perf_int_7,  @perf_int_8,  @perf_int_9,  @perf_int_10,
              @perf_int_11,  @perf_int_12,  @perf_int_13,  @perf_int_14,  @perf_int_15,
              @perf_int_16,  @perf_int_17,  @perf_int_18,  @perf_int_19,  @perf_int_20 */
              )

      commit transaction
   end
   else
   begin
      insert PER_intstatistics
         (   system_name,  system_type,  perf_key,  perf_time,  weight,
              key_2, key_3, key_4,
              perf_int_1,  perf_int_2,  perf_int_3,  perf_int_4,  perf_int_5,
              perf_int_6
              /* ,  perf_int_7,  perf_int_8,  perf_int_9,  perf_int_10,
              perf_int_11,  perf_int_12,  perf_int_13,  perf_int_14,  perf_int_15,
              perf_int_16,  perf_int_17,  perf_int_18,  perf_int_19,  perf_int_20 */
              )
      values
         (    @system_name,  @system_type,  @perf_key,  @perf_time,  1,
              @key_2, @key_3, @key_4,
              @perf_int_1,  @perf_int_2,  @perf_int_3,  @perf_int_4,  @perf_int_5,
              @perf_int_6
              /* ,  @perf_int_7,  @perf_int_8,  @perf_int_9,  @perf_int_10,
              @perf_int_11,  @perf_int_12,  @perf_int_13,  @perf_int_14,  @perf_int_15,
              @perf_int_16,  @perf_int_17,  @perf_int_18,  @perf_int_19,  @perf_int_20 */
              )
    end
end
go
grant execute on PER_saveintstat to public
go
