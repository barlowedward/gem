#
# TEMPLATE IS MY STANDARD MODULE FOR GEM TEMPLATE
#
use strict;
use Getopt::Long;
use CommonFunc;

my($PROGRAM_DESCRIPTION)="Procedure Library Installer";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions( "DEBUG"	=> \$DEBUG );

print "$0 - v",$VERSION,"\n";
print "   Reading gem.dat\n";
my($GemConfig)=read_gemconfig(-debug=>$DEBUG);
# START BODY


# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "SUCCESSFUL: $PROGRAM_DESCRIPTION\n";
close_gemconfig();
exit(0);