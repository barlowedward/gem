#  DumpAccounts.pl
#  Example 4.3:
#  ----------------------------------------
#  From "Win32 Perl Scripting: Administrators Handbook" by Dave Roth
#  Published by New Riders Publishing.
#  ISBN # 1-57870-215-1
#
#  This script makes a backup of user accounts.
#  Example_4_4.pl uses this resulting configuration file to recreate
#  the accounts.
#
print "From the book 'Win32 Perl Scripting: The Administrator's Handbook' by Dave Roth\n\n";


use Getopt::Long;
use Win32::Lanman;

%Config = (
    level   =>  1
);
Configure( \%Config );
if( $Config{help} )
{
    Syntax();
    exit;
}
if( defined $Config{domain} )
{
    Win32::Lanman::NetGetDCName( $Config{machine},
                                 $Config{domain},
                                 \$Config{machine} );
}
if( Win32::Lanman::NetGetDisplayInformationIndex( $Config{machine},
                                                  $Config{level},
                                                  $Config{prefix},
                                                  \$Index ) )
{
    do
    {
        Win32::Lanman::NetQueryDisplayInformation( $Config{machine},
                                                   $Config{level},
                                                   $Index,
                                                   10,
                                                   \@Accounts );
        foreach my $Account ( @Accounts )
        {
            print "[$Account->{name}]\n";
            print STDERR "$Account->{name}\n";
            $Index = $Account->{next_index};
            if( Win32::Lanman::NetUserGetInfo( $Config{machine},
                                               $Account->{name},
                                               \%Info ) )
            {
                foreach my $Attribute ( sort( keys( %Info ) ) )
                {
                    my $Value = $Info{$Attribute};
                    $Value =~ s/([^\w_+-])/sprintf( "\\x%02x", unpack( "C", $1 ) )/eg;
                    print "$Attribute=$Value\n";
                }
            }
            print "\n";
        }
    }while( scalar @Accounts );
}

sub Configure
{
    my( $Config ) = @_;
    my $Result = 0;
    Getopt::Long::Configure( "prefix_pattern=(-|\/)" );
    $Result = GetOptions( $Config,
        qw(
          domain|d=s
          machine|m=s
          prefix|p=s
          type|t=s
          help|?|h
        )
    );

    if( lc $Config->{type} eq "user" )
    {
        $Config->{level} = 1;
    }
    elsif( lc $Config->{type} eq "machine" )
    {
        $Config->{level} = 2;
    }
    $Config->{help} = 1 if( ! $Result );
}

sub Syntax
{
    my( $Script ) = ( $0 =~ /([^\\\/]*?)$/ );
    my( $Line ) = "-" x length( $Script );

    print <<EOT;

$Script
$Line
Dumps account information to a file.
Version $VERSION

Syntax:
    perl $Script [-t user | machine][-m Machine][-p Prefix][-d Domain]
        -m...............Machine who's user accounts will be dumped.
                         Defaults to the local machine.
        -t...............Specifies what type of accounts to dump.
                         user: user accounts (Default)
                         machine: machine accounts
        -p Prefix........Specifies an account prefix to use. Only
                         accounts beginning with this string are
                         queried.
        -d Domain........Specifies a domain to use.
EOT
}

