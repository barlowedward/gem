function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",300,21,"Verdana, Arial, Helvetica, sans-serif",14,"#000000","#ffffff","#c6c3c6","#000084");
  fw_menu_0.addMenuItem("0Go To Main Page","location='http://www.edbarlow.com'");
  fw_menu_0.addMenuItem("Who Am I","location='http://www.edbarlow.com/resume.htm'");
  fw_menu_0.addMenuItem("Site Feedback","location='http://www.edbarlow.com/feedback.htm'");
  fw_menu_0.addMenuItem("Website Changes","location='http://www.edbarlow.com/changes.htm'");
  fw_menu_0.addMenuItem("Database News","location='http://www.edbarlow.com/news.htm'");
  fw_menu_0.addMenuItem("Fun","location='http://www.edbarlow.com/fun.htm'");
  fw_menu_0.hideOnMouseOut=true;

  window.fw_menu_1 = new Menu("root",300,21,"Verdana, Arial, Helvetica, sans-serif",14,"#000000","#ffffff","#c6c3c6","#000084");
  fw_menu_1.addMenuItem("2aEd's Links","location='http://www.edbarlow.com/cgi-bin/links.pl'");
  fw_menu_1.addMenuItem("Sybase User Groups","location='http://www.isug.com/'");
  fw_menu_1.addMenuItem("Sybase FAQ","location='http://www.isug.com/Sybase_FAQ/'");
  fw_menu_1.addMenuItem("Sybase Manuals","location='http://www.sybase.com/support/manuals/'");
  fw_menu_1.addMenuItem("Sybperl","location='http://www.mbay.net/~mpeppler'");

  window.fw_menu_2 = new Menu("root",300,21,"Verdana, Arial, Helvetica, sans-serif",14,"#000000","#ffffff","#c6c3c6","#000084");
  fw_menu_2.addMenuItem("2Consulting","location='http://www.edbarlow.com/consulting.htm'");
  fw_menu_2.addMenuItem("Classes","location='http://www.edbarlow.com/classes.htm'");
  fw_menu_2.addMenuItem("Contracting","location='http://www.edbarlow.com/contracting.htm'");
  fw_menu_2.addMenuItem("Systems Admin","location='http://www.edbarlow.com/enhance.htm'");
  fw_menu_2.hideOnMouseOut=true;

  window.fw_menu_3 = new Menu("root",287,21,"Verdana, Arial, Helvetica, sans-serif",14,"#000000","#ffffff","#c6c3c6","#000084");
  fw_menu_3.addMenuItem("3Sybase's Document Library","location='http://techinfo.sybase.com/'");
  fw_menu_3.addMenuItem("Ed Barlow's Document Library","location='http://www.edbarlow.com/documents.htm'");
  fw_menu_3.addMenuItem("Sybase Related List Servers","location='http://www.edbarlow.com/listserv.htm'");
  fw_menu_3.hideOnMouseOut=true;

  window.fw_menu_6 = new Menu("root",287,21,"Verdana, Arial, Helvetica, sans-serif",14,"#000000","#ffffff","#c6c3c6","#000084");
  fw_menu_6.addMenuItem("3Sybase's Document Library","location='http://techinfo.sybase.com/'");
  fw_menu_6.addMenuItem("Ed Barlow's Document Library","location='http://www.edbarlow.com/documents.htm'");
  fw_menu_6.addMenuItem("Sybase Related List Servers","location='http://www.edbarlow.com/listserv.htm'");
  fw_menu_6.hideOnMouseOut=true;

  window.fw_menu_5 = new Menu("root",387,21,"Verdana, Arial, Helvetica, sans-serif",14,"#000000","#ffffff","#c6c3c6","#000084");
  fw_menu_5.addMenuItem("3Sybase's Document Library","location='http://techinfo.sybase.com/'");
  fw_menu_5.addMenuItem("Ed Barlow's Document Library","location='http://www.edbarlow.com/documents.htm'");
  fw_menu_5.addMenuItem("Sybase Related List Servers","location='http://www.edbarlow.com/listserv.htm'");
  fw_menu_5.hideOnMouseOut=true;

  window.fw_menu_4 = new Menu("root",287,21,"Verdana, Arial, Helvetica, sans-serif",14,"#000000","#ffffff","#c6c3c6","#000084");
  fw_menu_4.addMenuItem("3Sybase's Document Library","location='http://techinfo.sybase.com/'");
  fw_menu_4.addMenuItem("Ed Barlow's Document Library","location='http://www.edbarlow.com/documents.htm'");
  fw_menu_4.addMenuItem("Sybase Related List Servers","location='http://www.edbarlow.com/listserv.htm'");
  fw_menu_4.hideOnMouseOut=true;

  fw_menu_4.writeMenus();
} // fwLoadMenus()
