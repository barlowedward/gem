-- report_key=UNUSED_INDEXES
-- report_type=HTML_AGGREGATE
-- run_frequency=daily
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_min_version=9
-- server_max_version=
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- run_query_each_db=Y
-- run_query_in_sys_db=N
-- report_header=This report lists indexes that have not been used since server start.
-- report_footer=
-- enabled=Y
-- outfile=Unused_Indexes

select	@@SERVERNAME as ServerName,
		db_name() as DbName,
		object_schema_name(i.object_id) as SchemaName,
		object_name(i.object_id) as ObjectName,
		i.name as IndexName,
		i.index_id as IndexId
from sys.indexes i
where index_id>0
and objectproperty(i.object_id,'IsUserTable')=1
and not exists ( select index_id from sys.dm_db_index_usage_stats
				where object_id=i.object_id
				and index_id = i.index_id
				and database_id=db_id())
order by ServerName, DbName, SchemaName, ObjectName, IndexName
