#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2011 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com

use File::Basename;
use GemReport;
my($curdir)=dirname($0);
use Getopt::Long;

my($progstart)=time;
sub usage
{
	print @_;
	print "datacompare.pl [-hthl] [-debug] -SOURCESERVER=srv [ -SOURCELOGIN=usr -SOURCEPASS=pass -SOURCEDB=db ]
	-TARGETSERVER=srv [ -TARGETLOGIN=usr -TARGETPASS=pass -TARGETDB=db ]
	-BATCHID=batchid

		Shows tables with different number of rows between two servers
		-g gem report (in html_output directory)
		-h html output
		-x debug mode \n";
	return "\n";
}

use strict;
# use Getopt::Std;

use vars qw($HTML $SOURCEDB $SOURCELOGIN $SOURCESERVER $SOURCEPASS $TARGETDB $TARGETSERVER
	$TARGETLOGIN $GEMREPORT $TARGETPASS $DEBUG $BATCHID);
die usage("") if $#ARGV<0;
# die usage("Bad Parameter List\n") unless getopts('xU:D:S:P:d:u:p:s:chB:');

use vars qw( $DEBUG );

die usage("Bad Parameter List $!")
unless GetOptions(
	"DEBUG"				=> \$DEBUG,
	"HTML"				=> \$HTML,
	"SOURCEDB=s"		=> \$SOURCEDB,
	"SOURCELOGIN=s"	=> \$SOURCELOGIN,
	"SOURCESERVER=s"	=> \$SOURCESERVER,
	"SOURCEPASS=s"		=> \$SOURCEPASS,
	"TARGETDB=s"		=> \$TARGETDB,
	"TARGETSERVER=s"	=> \$TARGETSERVER,
	"TARGETLOGIN=s"	=> \$TARGETLOGIN,
	"GEMREPORT"			=> \$GEMREPORT,
	"TARGETPASS=s"		=> \$TARGETPASS,
	"BATCHID=s"			=> \$BATCHID
);

use Repository;
use DBIFunc;

if( defined $SOURCESERVER and ! defined $TARGETSERVER and defined $SOURCEDB and defined $TARGETDB ){
	$TARGETSERVER=$SOURCESERVER unless $TARGETSERVER;
	$TARGETLOGIN=$SOURCELOGIN unless $TARGETLOGIN;
	$TARGETPASS=$SOURCEPASS unless $TARGETPASS;
}

if( defined $TARGETSERVER and ! defined $SOURCESERVER and defined $SOURCEDB and defined $TARGETDB ){
	$SOURCESERVER=$TARGETSERVER unless $SOURCESERVER;
	$SOURCELOGIN=$TARGETLOGIN unless $SOURCELOGIN;
	$SOURCEPASS=$TARGETPASS unless $SOURCEPASS;
}

# IF ONLY HAVE SOURCESERVER get login & password from password file
if( defined $SOURCESERVER and ! defined $SOURCELOGIN ) {
	print "Fetching $SOURCESERVER password if possible\n" if defined $DEBUG;
   ($SOURCELOGIN,$SOURCEPASS)=get_password(-type=>"sybase",-name=>$SOURCESERVER);
	die usage("Cant Fetch Password For $SOURCESERVER\n")
		if ! defined $SOURCELOGIN or ! defined $SOURCEPASS or ! defined $SOURCESERVER;
}

if( defined $TARGETSERVER and ! defined $TARGETLOGIN ) {
   print "Fetching $TARGETSERVER password if possible\n" if defined $DEBUG;
   ($TARGETLOGIN,$TARGETPASS)=get_password(-type=>"sybase",-name=>$TARGETSERVER);
	die usage("Cant Fetch Password For $TARGETSERVER\n")
		if ! defined $TARGETLOGIN or ! defined $TARGETPASS or ! defined $TARGETSERVER;
}

die usage("Bad Info -U\n") unless defined $SOURCELOGIN;
die usage("Bad Info -S\n") unless defined $SOURCESERVER;
die usage("Bad Info -P\n") unless defined $SOURCEPASS;
die usage("Bad Info -u\n") unless defined $TARGETLOGIN;
die usage("Bad Info -s\n") unless defined $TARGETSERVER;
die usage("Bad Info -p\n") unless defined $TARGETPASS;
#die usage("Bad Info -D\n") unless defined $SOURCEDB;
#die usage("Bad Info -d\n") unless defined $TARGETDB;

$TARGETDB = $SOURCEDB if( defined $SOURCEDB and ! defined $TARGETDB );
$SOURCEDB = $TARGETDB if( defined $TARGETDB and ! defined $SOURCEDB );

my($label_S,$label_s)=($SOURCESERVER,$TARGETSERVER);
($label_S,$label_s)=($SOURCEDB,$TARGETDB) if $TARGETSERVER eq $SOURCESERVER;

use vars qw($DEBUG);
$DEBUG=1 if defined $DEBUG;
my($NL)="\n";
$NL="<BR>\n" if defined $HTML;

if( $GEMREPORT ) {
	report_init(-file_name=>"datacompare_".$SOURCESERVER."_".$TARGETSERVER,-debug=>$DEBUG);
} else {
	report_init(-file_name=>"", -debug=>$DEBUG);
}

report_print( 0,  "<H1>") if $HTML;
report_print( 1,  "Row Count Comparison between server $SOURCESERVER and $TARGETSERVER");
report_print( 0,  "</H1>\n" ) if $HTML;
report_print( 1,  "\n\n" ) unless $HTML;

if( $BATCHID ) {
	report_print( 1,  "Created by Batch $BATCHID running datacompare.pl at ".localtime(time).$NL );
} else {
	report_print( 1,  "Created by datacompare.pl at ".localtime(time).$NL);
}

report_print( 1, sprintf( "Database %21s %23s %23s $NL","Table",$TARGETSERVER,$SOURCESERVER ))
		unless defined $HTML;

if( defined $SOURCEDB ) {
	compare_db($SOURCEDB,$TARGETDB);
} else {
	# work on all db's in server
	report_print( 1, "Getting Databases from $SOURCESERVER",$NL );
	dbi_connect(-srv=>$SOURCESERVER,-login=>$SOURCELOGIN,-password=>$SOURCEPASS)
		or die "Cant connect to $SOURCESERVER as $SOURCELOGIN\n";
	my($server_type_S,@dbl_S)=dbi_parse_opt_D("%", 1, 1);
	dbi_disconnect();
	my($tmp)=$#dbl_S+1;

	report_print( 1,  "Getting Databases from $TARGETSERVER",$NL );
	dbi_connect(-srv=>$TARGETSERVER,-login=>$TARGETLOGIN,-password=>$TARGETPASS)
		or die "Cant connect to $TARGETSERVER as $TARGETLOGIN$NL";
	my($server_type_s,@dbl_s)=dbi_parse_opt_D("%", 1, 1);
	dbi_disconnect();
	my($tmp2)=$#dbl_s+1;
	report_print( 1, "\tFound $tmp Databases on $SOURCESERVER and $tmp2 on $TARGETSERVER $NL" );

	# build db list from dbl_S and dbl_s
	my(%databaselist);
	foreach (@dbl_S) { $databaselist{$_}=0; }
	foreach (@dbl_s) { $databaselist{$_}=1 if defined $databaselist{$_};  }

	foreach (sort keys %databaselist) {
		#Next line modified by B.Muthaiah on 04/05/2006:
		next if $_ eq "model" or $_ eq "master" or $_ eq "sybsystemprocs"
			or $_ eq "sybsecurity" or $_ eq "tempdb"
			or $_ eq "sybsystemdb" or $_ eq "dbccdb";

		if( $databaselist{$_} != 1 ) {
			report_print(1, "Database Named $_ Does Not Exist on Both Servers",$NL );
			next;
		}
	}

	foreach (sort keys %databaselist) {
		#Next line modified by B.Muthaiah on 04/05/2006:
		next if $_ eq "model" or $_ eq "master" or $_ eq "sybsystemprocs"
			or $_ eq "sybsecurity" or $_ =~ /tempdb/
			or $_ eq "sybsystemdb" or $_ eq "dbccdb";
		# print $_,"\n" if defined $DEBUG;
		compare_db($_,$_) if $databaselist{$_} == 1;
	}
	report_close();
	$progstart -= time;
	$progstart *= -1;
	print "Program Completed in $progstart seconds \n";
}

sub compare_db
{
	my($db_S,$db_s)=@_;

	my($realstarttm)=time;

	#
	# GET TABLE INFO FROM SERVER 1
	#
	my($starttime)=time;
	print "Connecting To $SOURCESERVER.$db_S\n";
	dbi_connect(-srv=>$SOURCESERVER,-login=>$SOURCELOGIN,-password=>$SOURCEPASS)
		or die "Cant connect to $SOURCESERVER as $SOURCELOGIN$NL";
	my(@data1)=dbi_query(-db=>$db_S,-query=>"exec sp__helptable \@dont_format='Y'");
	dbi_disconnect();
	my($rows)=$#data1+1;
	$starttime -= time;
	$starttime *= -1;
	print "Found $rows tables in $SOURCESERVER - $db_S in $starttime $NL" if defined $DEBUG;

	$starttime=time;
	print "Connecting To $TARGETSERVER.$db_s\n";
	dbi_connect(-srv=>$TARGETSERVER,-login=>$TARGETLOGIN,-password=>$TARGETPASS)
			or die "Cant connect to $TARGETSERVER as $TARGETLOGIN$NL";
	my(@data2)=dbi_query(-db=>$db_s,-query=>"exec sp__helptable \@dont_format='Y'");
	dbi_disconnect();
	$rows=$#data2+1;
	$starttime -= time;
	$starttime *= -1;
	print "Found $rows tables in $TARGETSERVER - $db_s in $starttime $NL" if defined $DEBUG;

	my(%nrows);
	foreach (@data1) {
		my(@dat)=dbi_decode_row($_);
		$nrows{$dat[0]} = $dat[1];
	}

	my($badtbls)=0;
	my($fmtstr)=("");
	if( defined $HTML ) {
		report_print( 0,  "<H2>" ) if $HTML;
		report_print( 0,  sprintf( "Database %-30.30s",$db_s))		       if $db_s eq $db_S;
		report_print( 0,  sprintf( "Database %-30.30s",$db_s."/".$db_S)) 		if $db_s ne $db_S;
		report_print( 0,  "</H2>" ) if $HTML;
		report_print( 0,  "\n" );
	} else {
		my $curdb    = $db_s if $db_s eq $db_S;
		my $tbllen   = 29 - length($curdb);
		$fmtstr= $curdb." %".$tbllen.".".$tbllen."s %23.23s %23.23s $NL";
	}

	my($hdr)="<TABLE BORDER=1>\n<TR BGCOLOR=BEIGE><TH>TABLE</TH><TH>$label_S Rows</TH><TH>$label_s Rows</TH><TH>Row Difference</TH></TR>\n";
	# my($hdr_printed)=0;
	my($numberequal)=0;

	if( $HTML ) { report_print( 0,  $hdr); }

	foreach (@data2) {
		my(@d)=dbi_decode_row($_);

		# Next line modified by B.Muthaiah on 04/05/2006:
		next if $d[0] eq "syslogs" or $d[0] eq "rs_lastcommit";
		#if( ! $hdr_printed ) { report_print( 0,  $hdr); $hdr_printed=1; }

		if( defined $HTML ) {
			if( ! defined $nrows{$d[0]} ) {
				$badtbls++;
				# if( ! $hdr_printed ) { report_print( 0,  $hdr); $hdr_printed=1; }
				report_print( 0,  "<TR BGCOLOR=PINK><TD COLSPAN=4>Table $d[0] Does Not Exist In $SOURCESERVER</TD>\n</TR>\n" );
			} elsif($nrows{$d[0]} != $d[1] ) {
			   $badtbls++;
			   # if( ! $hdr_printed ) { report_print( 0,  $hdr ); $hdr_printed=1; }

				# Next Row modified by B.Muthaiah on 04/05/2006 :
			   report_print( 0,  "<TR><TD align='center' width='200'>",$d[0],
			   						"</TD>\n<TD align='center'>",$nrows{$d[0]},
			   						"</TD>\n<TD align='center'>", $d[1]," </TD>\n<TD align='center'>");
				report_print( 0,  ($nrows{$d[0]}-$d[1]),"</TD>\n</TR>\n");
			} else {
				$numberequal++;
				print( "Equal Number Of Rows $d[0]$NL") if defined $DEBUG;
			}
		} else {
			if( ! defined $nrows{$d[0]} ) {
				$badtbls++;
				report_print( 0,  sprintf( $fmtstr,$d[0],$d[1],"N.A."))
					if defined $DEBUG;
			} elsif($nrows{$d[0]} != $d[1] ) {
				$badtbls++;
				report_print( 0,  sprintf( $fmtstr,$d[0], $d[1], $nrows{$d[0]} ));
			} else {
				if( defined $DEBUG ) {
					$d[1] = "(".$d[1].")";
					$numberequal++;
					report_print( 0,  sprintf( $fmtstr,$d[0], $d[1], $d[1] ));
				}
			}
		}
	}

	if( $badtbls==0 ) {
		report_print( 0,  "<TR><TD COLSPAN=3>\n" ) if defined $HTML;	#  and $hdr_printed;
		report_print( 0,  "No Tables with Different Num Rows Found" );
		report_print( 0,  "</TD>\n</TR>" ) if defined $HTML;	#  and $hdr_printed;
	} elsif( $numberequal ) {
		if( defined $HTML ) {
			report_print( 0,  "<TR><TD>",'Equal Row Tables',
		   						"</TD>\n<TD align='center' colspan=3>",$numberequal,
		   						"</TD>\n</TR>\n");
		} else {
			report_print( 0,  $numberequal . " Equal Row Tables Found In This Database\n" );
		}
	}
	report_print( 0,  "</TABLE>\n" ) if defined $HTML;	#  and $hdr_printed;
	report_print( 0,  "$NL" );

	$realstarttm -= time;
	$realstarttm *= -1;
	print "Processed $SOURCESERVER.$db_S <=> $TARGETSERVER.$db_s in $realstarttm seconds \n";
}

__END__

=head1 NAME

datacompare.pl - database data comparison

=head2 DESCRIPTION

Compare the number of rows in two db's and print diffrences.  Note that
only the number of rows are compared, not actual data differences.

=head2 USAGE

 datacompare.pl [-xh]  -Ssrv [ -Uusr -Ppass -Ddb ] -ssrv
	   [ -uusr -ppass -ddb ]

 Shows tables with different number of rows between two servers

 B<-h> htmlize output
 B<-c> show row counts instead of just summary info
 B<-x> debug mode
		  B<-x> debug mode

=head2 NOTES

Requires extended stored procedure library

=cut


