#!/bin/sh

echo "RUNNING UNIX BATCH OF INVENTORY SYSTEM"
cd /apps/sybmon/projects/inventory_manager
#/usr/local/bin/perl-5.8.2 INV_load_configfiles.pl
echo "##############################"
echo starting INV_get_etcpasswd.pl
echo "##############################"
/usr/local/bin/perl-5.8.2 INV_get_etcpasswd.pl --BATCH_ID=INV_get_etcpasswd
echo "##############################"
echo starting INV_unix_ypcat_extractor.pl
echo "##############################"
/usr/local/bin/perl-5.8.2 INV_unix_ypcat_extractor.pl --BATCH_ID=INV_get_ypcat_extractor
echo "##############################"
echo starting INV_database_auditor.pl
echo "##############################"
/usr/local/bin/perl-5.8.2 INV_database_auditor.pl --DOALL=sybase --BATCH_ID=INV_database_auditor
echo "##############################"
#/usr/local/bin/perl-5.8.2 INV_dsget.pl
#/usr/local/bin/perl-5.8.2 INV_dsget_computer.pl
#/usr/local/bin/perl-5.8.2 INV_mapper.pl
#/usr/local/bin/perl-5.8.2 INV_win_getsystemusers.pl
