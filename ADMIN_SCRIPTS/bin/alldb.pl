#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use vars qw(@ARGV);
use Getopt::Std;
use File::Basename;
use DBIFunc;

sub usage
{
        print @_;
        print "Usage: alldb.pl -sne -TTYPE -SSRV -UUSR -PPASS -DDB SQL_STMT -p

Log in to SRV and runs SQL in each database you can use.  This program may
also be used to run a proc on each object in a database.

-s - no system databases
-D - Database mask (include sybase wildcards) - Default = %
-T - Object Type - Run \"SQL_STMT ObjectName\" for each object of type -T
   - The type must be U (table), P (proc), TR (trigger)... as per sysobjects
-e - echo cmd only
-n - dont exec command (sets -e option) - just print it
-p - put db name before results
-h - put a header
-r - reformat the rows as per a report

Example:  \"AllDb -SX -Usa -Ppass exec sp_helpdb\" will execute sp_helpdb in
all your databases.

Example: \"AllDb -SX -Usa -Ppass -Dxrm_db -TP sp_helptext\" will execute sp_helptext on each proc in xrm_db.

Output is tab separated and no headers are printed (its not isql you know)\n";

        return "\n";
}

use vars qw($opt_d $opt_s $opt_S $opt_U $opt_P $opt_D $opt_T $opt_e $opt_n $opt_p $opt_h $opt_r);

die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('dS:U:P:sD:T:enpxhr');

$opt_e=1 if defined $opt_n;
$opt_D="%" unless defined $opt_D;

my($cmd)="";
foreach (@ARGV) {    $cmd.=$_." "; }

die usage "Must Pass Server\n" unless defined $opt_S and $opt_S ne "";
die usage "Must Pass User\n" unless defined $opt_U and $opt_U ne "";
die usage "Must Pass Password\n" unless defined $opt_P and $opt_P ne "";

# cd to the right directory
my($curdir)=dirname($0);
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P) or die "Cant connect to $opt_S as $opt_U\n";
print "Connected\n" if defined $opt_d;

my($server_type,@databaselist)=dbi_parse_opt_D($opt_D,1,$opt_s);
print "ServerType=$server_type - Num DB=$#databaselist\n" 	if defined $opt_d;

$opt_h=1 if $opt_h;
my($db);
my(@final_rc);
foreach $db (@databaselist) {
        next if defined $opt_s and ($db eq "master"
                or $db eq "model"
                or $db eq "sybsystemprocs"
                or $db eq "sybsecurity"
                or $db eq "tempdb");
			print "Working on $db\n" if defined $opt_d;

        if( defined $opt_T ) {
					print "-- checking type $opt_T\n" if defined $opt_d;
               my(@obj)=dbi_query(-db=>$db,-query=>"select name,user_name(uid),uid from sysobjects where type=\"$opt_T\"");
               foreach (@obj) {
               	 my($nm,$unm,$uid)=dbi_decode_row($_);
               	 my($obj)=$nm;
               	 $obj=$unm.".".$nm if $uid>1;

               	  my($lcmd)=$cmd;
               	  if( $lcmd =~ /\%s/ ) {
               	   	$lcmd =~ s/\%s/$obj/g;
               	  } else {
               	  		$lcmd .= " $obj";
               	  }
                    print "-- ".$lcmd,"\n" if defined $opt_e;
                    next if defined $opt_n;

                        my(@rc)=dbi_query(-db=>$db,-query=>$lcmd);
                        foreach (@rc) {
                        	push @final_rc,$_;
                                #print join("\t",dbi_decode_row($_)),"\n";
                        }
                }
        } else {
               print "$opt_S $db -- ".$cmd,"\n" if defined $opt_e;
               next if defined $opt_n;
					print $cmd,"\n" if defined $opt_d;

               my(@rc)=dbi_query(-db=>$db,-query=>$cmd , -print_hdr=>$opt_h );
               if( $opt_p and $#rc<0 ) { print "$db: no results\n"; }
               foreach (@rc) {
               		undef $opt_h;
               		#print "$db: " if $opt_p;
               		my(@x)=dbi_decode_row($_);
               		unshift @x,$db if $opt_p;
               		push @final_rc,dbi_encode_row(@x);
               		#print join("\t",dbi_decode_row($_)),"\n";
		}
	}
}

if( $opt_r ) {
	print join("\n",dbi_reformat_results( @final_rc )) ;
} else {
	foreach ( @final_rc ) {
		print join("\t",dbi_decode_row($_)),"\n";
	}
}

dbi_disconnect();

__END__

=head1 NAME

alldb.pl - loop through your databases utility

=head2 DESCRIPTION

Run a program on every db in a server or on all objects in a database.
You can select a subset of databases to run in and can run on objects by
type (tables...).

=head2 USAGE

        Usage: AllDb -sne -TTYPE -SSRV -UUSR -PPASS -DDB SQL_STMT

Log in to SRV and runs SQL_STMT in each database you can use.  This program may also be used to run a proc on each object in a database.

        B<-s> - no system databases
        B<-D> - Database mask (include sybase wildcards) - Default = %
        B<-T> - Object Type - Run "SQL_STMT ObjectName" for each
              object of type -T
        B<-e> - echo cmd to screen
        B<-n> - dont exec command - just print it (sets -e option)

The parameter to B<-T> should be U (table), P (proc), TR (trigger)... as per the definition of the type field of sysobjects.

Example: C<AllDb -SX -Usa -Ppass exec sp_helpdb> will execute sp_helpdb in all your databases.

Example: C<AllDb -SX -Usa -Ppass -Dxrm%db -TP sp_helptext> will execute sp_helptext on each proc in any databases that match the wildcard string xrm%db.

Output is tab separated and no headers are printed (its not isql you know)

=head2 SEE ALSO

AllSrv - AllSrv and AllDb can be used in conjunction easily

=cut
