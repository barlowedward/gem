
use lib qw(/apps/sybmon/dev/lib);
use RosettaStone;
use CommonFunc;
use DBIFunc;

my(%CONFIG)=read_configfile(-nodie=>1);
foreach ( keys %CONFIG ) {
	print $_," => ",$CONFIG{$_},"\n"
		if /^ALARM/;
}

dbi_set_mode("INLINE");
my($c)=dbi_connect(
			-srv=>$CONFIG{ALARM_SERVER},
			-login=>$CONFIG{ALARM_LOGIN},
			-password=>$CONFIG{ALARM_PASSWORD},
			-type=>'Sybase', -connection=>1 );

die "Unable to connect to Alarms Database" unless $c;

my(@reports)=get_schema_info(-reportname=>'allreports');
@reports=get_object_info(-reportname=>'allreports');
@reports=get_system_info(-reportname=>'allreports');
dbi_set_mode("INLINE");
dbi_set_option(-print => \&myprint );

my(%x);
print "*** SURVEY ***\n";
	%x=get_system_info(
			-db_type=>"Sybase", 
			-reportname=>"survey", 
			-connection=>$c,
			-debug=>\&myprint,
			-returntype=>'hash' );
	foreach( keys %x ) { print "K=$_ V=$x{$_} \n";  }
foreach (@reports) {
	print "*** REPORT $_ ***\n";
	print "\t",join("\n\t",get_system_info(
			-db_type=>"Sybase", 
			-reportname=>$_, 
			-connection=>$c,
			-debug=>\&myprint,
			-returntype=>'report' ));
	print "\n*** KEYS $_ ***\n\t";
	print join("\n\t",get_system_info(
			-db_type=>"Sybase", 
			-reportname=>$_, 
			-connection=>$c,
			-debug=>\&myprint,
			-returntype=>'keys' ));
	print "\n";
}

sub myprint {
	print @_;
}
