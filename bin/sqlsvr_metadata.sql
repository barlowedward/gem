set nocount on
declare 		@systemname nvarchar(80)

EXEC master..xp_regread @rootkey=N'HKEY_LOCAL_MACHINE', 
					@key=N'SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName',
					@value_name=N'ComputerName', 
					@value=@systemname OUTPUT
					
create table #SERVERPROPERTIES ( 
					Property varchar(30),	Value nvarchar(80) )
					
-- SETUP STUFF
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'Collation'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'Edition'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'PatchLevel'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'SQLBinRoot'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'SQLDataRoot'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'SQLPath'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'SQLProgramDir'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',	N'Version'

update #SERVERPROPERTIES set Property='Reg_Setup_'+Property
	
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',	N'BackupDirectory'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer\SuperSocketNetLib\Tcp\IPAll',	N'TcpPort'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer\SuperSocketNetLib\Tcp\IPAll',	N'TcpDynamicPorts'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',	N'FullTextDefaultPath'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer\CurrentVersion',	N'CurrentVersion'
			
update #SERVERPROPERTIES set Property='Reg_MSSQL_'+Property where Property not like 'Reg_Setup%'

Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer\Parameters',	N'SqlArg1'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer\Parameters',	N'SqlArg0'
Insert #SERVERPROPERTIES 
exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer\Parameters',	N'SqlArg2'

update #SERVERPROPERTIES set Property='Reg_Parm_ErrorLog' where Property='SqlArg1'
update #SERVERPROPERTIES set Property='Reg_Parm_MasterDataFile' where Property='SqlArg0'
update #SERVERPROPERTIES set Property='Reg_Parm_MasterLogFile' where Property='SqlArg2'

insert #SERVERPROPERTIES select 'Real_MasterDataFile',filename from master..sysfiles where name='master'
insert #SERVERPROPERTIES select 'Real_MasterLogFile',filename from master..sysfiles where name='mastlog'
insert #SERVERPROPERTIES select 'Real_MSDBDataFile',filename from msdb..sysfiles where name='MSDBData'
insert #SERVERPROPERTIES select 'Real_MSDBLogFile',filename from msdb..sysfiles where name='MSDBLog'
					
Insert #SERVERPROPERTIES values ('Collation',convert(varchar(30),ServerProperty('Collation')))
Insert #SERVERPROPERTIES values ('ComputerNamePhysicalNetBIOS',convert(varchar(30),ServerProperty('ComputerNamePhysicalNetBIOS')))
Insert #SERVERPROPERTIES values ('Edition',convert(varchar(30),ServerProperty('Edition')))
Insert #SERVERPROPERTIES values ('EngineEdition',convert(varchar(30),ServerProperty('EngineEdition')))
Insert #SERVERPROPERTIES values ('InstanceName',convert(varchar(30),ServerProperty('InstanceName')))
Insert #SERVERPROPERTIES values ('IsClustered',convert(varchar(30),ServerProperty('IsClustered')))
Insert #SERVERPROPERTIES values ('IsFullTextInstalled',convert(varchar(30),ServerProperty('IsFullTextInstalled')))
Insert #SERVERPROPERTIES values ('IsIntegratedSecurityOnly',convert(varchar(30),ServerProperty('IsIntegratedSecurityOnly')))
Insert #SERVERPROPERTIES values ('IsSingleUser',convert(varchar(30),ServerProperty('IsSingleUser')))
Insert #SERVERPROPERTIES values ('LCID',convert(varchar(30),ServerProperty('LCID')))
Insert #SERVERPROPERTIES values ('LicenseType',convert(varchar(30),ServerProperty('LicenseType')))
Insert #SERVERPROPERTIES values ('MachineName',convert(varchar(30),ServerProperty('MachineName')))
Insert #SERVERPROPERTIES values ('NumLicenses',convert(varchar(30),ServerProperty('NumLicenses')))
Insert #SERVERPROPERTIES values ('ProcessID',convert(varchar(30),ServerProperty('ProcessID')))
Insert #SERVERPROPERTIES values ('ProductVersion',convert(varchar(30),ServerProperty('ProductVersion')))
Insert #SERVERPROPERTIES values ('ProductLevel',convert(varchar(30),ServerProperty('ProductLevel')))
Insert #SERVERPROPERTIES values ('ResourceLastUpdateDateTime',convert(varchar(30),ServerProperty('ResourceLastUpdateDateTime')))
Insert #SERVERPROPERTIES values ('ResourceVersion',convert(varchar(30),ServerProperty('ResourceVersion')))
Insert #SERVERPROPERTIES values ('ServerName',convert(varchar(30),ServerProperty('ServerName')))
Insert #SERVERPROPERTIES values ('SqlCharSetName',convert(varchar(30),ServerProperty('SqlCharSetName')))
Insert #SERVERPROPERTIES values ('SqlSortOrder',convert(varchar(30),ServerProperty('SqlSortOrder')))
Insert #SERVERPROPERTIES values ('BuildClrVersion',convert(varchar(30),ServerProperty('BuildClrVersion')))
Insert #SERVERPROPERTIES values ('SqlSortOrderName',convert(varchar(30),ServerProperty('SqlSortOrderName')))

Insert #SERVERPROPERTIES 
	SELECT 'ClusterNode',convert(nvarchar(80),NodeName) FROM ::fn_virtualservernodes()

Insert #SERVERPROPERTIES 
	select 'SharedDrive',convert(nvarchar(80),DriveName) from ::fn_servershareddrives()

Insert #SERVERPROPERTIES 
	select 'ActiveSystemName',@systemname

select * from #SERVERPROPERTIES

drop table #SERVERPROPERTIES
go