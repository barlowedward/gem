#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2007 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Std;
use Repository;
use Sys::Hostname;
use CommonFunc;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use CommonHeader;

# cd to the right directory
my($curdir)=dirname($0);
my($program_start_time)=time;
chdir $curdir or die "Cant cd to $curdir: $!\n";

$| = 1;

sub usage
{
	print @_;
	print "unix_space_monitor_via_rsh.pl -o outdir  -A
			-A use mlp alarms
			-B BatchId To Use For Alarms
			-t test mode - additional prints
			-S hostname (comma separated)
			-O Outfile (optional)
			-h Html output

Monitor Unix Disk Space on all servers tagged with COMM_METHOD=SSH or RSH
and STORE results using the alarming subsystem. Should be run frequently.
When run from GEM console, the report created is put onto web
pages.\n";
   return "\n";
}

use vars qw($opt_d $opt_A $opt_S $opt_t $opt_B $opt_O $opt_h);
die usage("Bad Parameter List\n") unless getopts('B:dAS:tO:h');
$opt_B="GetUnixDiskSpace" unless $opt_B;

MlpBatchJobStart(-BATCH_ID => $opt_B ) if $opt_A;

if( $opt_O ) {
	open(OOO,">$opt_O") or die "Cant write to $opt_O $!\n";
}

sub output {
	my($msg)=join("",@_);
	$msg=~s/\n/\<br\>\n/g if $opt_h;
	print $msg;
	print OOO $msg if $opt_O;
}

output( "unix_space_monitor_via_rsh.pl started at ".localtime()."\n" );
output("\tBatchId=$opt_B\n");
output("\tOutputFile=$opt_O\n");
output("\tAlarms=$opt_A\n\n");

my($sc)="<FONT COLOR=RED>" if $opt_h;
my($ec)="</FONT>" if $opt_h;

my(@df_data);
$opt_S="-S$opt_S" if defined $opt_S;
my($dbg)="";	#"-d " if $opt_t;
my($cmd)="$^X ../rdist/run_cmd_on_all_hosts.pl $dbg $opt_S -c\"df -kl\" -n -T600 -o -f1,4,5,6 -xDF 2>&1";
output( "Running $cmd\n" );
output( "Command Started At ".localtime()."\n" );
open( OUT, $cmd." |" ) or die "Cant Run Command $cmd : $!\n";
foreach (<OUT>) {
	chomp;
	output( "READ: $_\n" ) if defined $opt_d or $opt_t;
	if( /ERROR: PROGRAM/ ) {
		warn "ERROR: UNIX SPACE MONITOR PROGRAM TIMED OUT\n";
		next;
	}
	push @df_data, $_;
}
close(OUT);
output( "Command Completed At ".localtime()."\n" );

# read override file if one exists
output( "\nReading Override File\n" ) if $opt_d or $opt_t;
my(%overrides);
my($gr_dir)=get_gem_root_dir();
if( -r $gr_dir."/conf/threshold_overrides.dat" ) {
	open (TR, $gr_dir."/conf/threshold_overrides.dat" ) or die "Cant open threshold_overrides.dat\n";
	while (<TR>) {
		chomp;
		s/\s+$//;
		chomp;
		next if /^#/ or /^\s*$/;
		next unless /,\s*UnixDiskspace\s*,/;
		output( "Setting override: $_\n" ) if $opt_t;
		$overrides{$_}=1;
	}
	close(TR);
}

my(@warnings,@messages);
my($lastfs, $current_server)=("","");
output( "\nProcessing Returned Results\n" ) if $opt_d or $opt_t;

foreach (@df_data) {
	chomp;

	s/^\s*//;
	$lastfs="" if $_ eq "";

	if( /connect failure :/ ) {
		s/^[\.\s+]//;
		/^(\S+)\s/;
		my($host)=$1;
		output( "$_ - correct your connection or set COMM_METHOD ins unix_passwords.dat\n" )	if $opt_A;
		MlpHeartbeat(
         		-monitor_program=>$opt_B,
         		-system=>$host,
        			-subsystem=>"DiskMonitor",
         		-state=>"ERROR",
         		-message_text=>$_
      		) if defined $opt_A;
		next;
      	}

	next if $_ eq ""
	     or /^\s*\</
	     or /^run_cmd_on_all_hosts.pl run at/
	     or /^Server List:/
	     or /^do_rsh:/
	     or /^\s*$/
	     or /^Command run in DEBUG mode/
	     or /^Running:/ or /df:/;

	$lastfs="" if /Filesystem/i and /avail/i and /Mounted/i;

	next if /^host / and /method=/;
	next if /Filesystem/i and /avail/i and /Mounted/i;
	next if /Stale NFS/;
	next if /: No such file or directory/;

	output( "DBG DBG Processing: $_\n" ) if defined $opt_d or $opt_t;

	#my($host,$fs,$mountedon,$blocks,$pct) = split(/\s+/,$_);
	#output( "Processing $_\n") ;
	my(@dat) = split;
	if( $#dat == 1 ) {
		#hey we have a lame broken line
		$lastfs   = $dat[1];
		next;
	}
	if( $lastfs ne "" ) {
		my($x)=shift @dat;
		unshift @dat, $lastfs;
		unshift @dat, $x;
	}
	$lastfs="";
	#next if $dat[1] =~ /\:\//;

	# Dat0=hostname Dat1=Filesystem Dat2=Avail Dat3=Capacity Dat4=Mounted
	$dat[3]=~s/\%$//;
	next if $dat[4] =~ /\/cdrom/
		or $dat[4] =~ /^\/mnt/
		or $dat[4] =~ /\/tempdb/ #or $dat[1] eq "swap" or $dat[1] eq "none"
		or $dat[1]=~/^[\d\w]+:\// #or $dat[1] eq "fd" or $dat[1] eq "swap"
		or $dat[1] !~ /[\/\\]/
		or $dat[1] eq "mnttab" or $dat[1] eq "/proc";

	output( "Processing: ",join(",",@dat),"\n" ) if defined $opt_d or $opt_t;
  	my($state)="OK";
	my($msg,$subsystem);

	if( $dat[0] ne $current_server ) {
			output( "MlpHeartbeat( -system=$dat[0] -state=>OK )\n"	) 			if $opt_d or $opt_t;
      	MlpHeartbeat(
         			-monitor_program=>$opt_B,
         			-system=>$dat[0],
         			-subsystem=>"DiskMonitor",
         			-state=>"OK",
         			-message_text=>"Reading Disk Space Information"
      			) if defined $opt_A;
			$current_server = $dat[0];
	}

	if( $dat[3] =~ /^\s*$/ or $dat[4] =~ /^\s*$/ or $dat[1] =~ /permission/ )  {
      		$msg= "Unix Diskspace Monitoring Error ($_). >> Check rsh parameters\njob running on ".hostname()."\ncommand=$cmd\nresults=".join("\n",@df_data)."\n";
      		$msg.= "Dat=".join("\n",@dat);
				$subsystem="DiskMonitor";
      		$state="ERROR";
				output( "BAD PARSE ON DISK DATA (line=$_)\n") ;
	} else {
      $msg= "Drive $dat[4] at ".$dat[3]. "% avail=$dat[2]";
		$subsystem="Disk: $dat[4]";
		output( "DBG: $msg\n")  if $opt_d;

		my(@thresholds)=(90,95,99);
		foreach ( keys %overrides ) {
			next unless /^$dat[0]/;
			my($s,$mp,$lvl,$thresh,$subsys)=split(/,/,$_,5);
			next if defined $subsys and $subsys !~ /^\s*$/ and  $subsys ne $subsystem;
			$s=~s/\s//g;
			next unless $s eq $dat[0];
			$lvl=~s/\s//g;
			$thresh=~s/\s//g;
			if( $lvl eq "WARNING" ) {
				$thresholds[0]=$thresh;
			} elsif( $lvl eq "ERROR") {
				$thresholds[1]=$thresh;
			} elsif( $lvl eq "CRITICAL") {
				$thresholds[2]=$thresh;
			} else {
				die "ERROR>>> UNABLE TO PARSE LEVEL $lvl - must be WARNING or CRITICAL or ERROR:\n line= $_\n";
			}
			output( "DBG: USING OVERRIDE THRESHOLD ",join("/",@thresholds)," - subsys=$subsys subsystem=$subsystem - $dat[3]\n") if $opt_d;
		}
		output( "DBG: THRESHOLD= ",join("/",@thresholds)," - $dat[3]\n" )  if $opt_d;
		$state="WARNING"  if $dat[3]>$thresholds[0];
		$state="ERROR"    if $dat[3]>$thresholds[1];
		$state="CRITICAL" if $dat[3]>$thresholds[2];
		output( "DBG: STATE= $state\n" ) if $opt_d;
	}

	output( "Saving Heartbeat(system=$dat[0] disk=$dat[4] state=$state $msg)\n" ) if $opt_d or $opt_t;
        MlpHeartbeat(
      		#-monitor_program=>"UnixDiskspace",
      		-monitor_program=>$opt_B,
      		-system=>$dat[0],
  				-subsystem=>$subsystem,
     			-state=>$state,
     			-message_text=>$msg
		) if defined $opt_A;

	#next unless $dat[3]>80;
	if( $state ne "OK" ) {
		push @messages,$sc.$state." ".$_.$ec;
		output( "Warning ($state): $_\n" ) if defined $opt_d;
		push @warnings,$sc.$state." ".$ec;
	} else {
		push @messages,"... ".$_;
	}
}

my(@neww);
foreach (@warnings) {
	my(@d)=split(/\s+/,$_);
	push @neww,dbi_encode_row(@d);
}
my(@newm);
foreach (@messages) {
	my(@d)=split(/\s+/,$_);
	push @newm,dbi_encode_row(@d);
}

output( "Unix Disk Space Monitor v1.0\n\n") ;
output( "Run At ".localtime(time)."\n") ;
output( "Overrides in file $gr_dir/conf/threshold_overrides.dat\n")
	if -r $gr_dir."/conf/threshold_overrides.dat";

output( "\n\nUnix Disks That Exceed Threshold (dflt=90%)\n") ;
output( join("\n",dbi_reformat_results(@neww))) ;

output( "\n\nAll Unix Disks\n") ;
output( join("\n",dbi_reformat_results(@newm))) ;

output( "\n") ;
close(OOO) if $opt_O;

MlpBatchJobEnd() if $opt_A;
1;

__END__

=head1 NAME

unix_space_monitor_via_rsh.pl - monitor space on your unix systems

=head2 DESCRIPTION

Monitor Unix Disk Space on all servers tagged COMM_METHOD=RSH or SSH and
then store results using the alarming subsystem. Should be run frequently.
When run from GEM console, the report created is put onto web
pages.

=head2 ARGUMENTS

Use -A to save heartbeats.

=head2 SYNTAX

unix_space_monitor_via_rsh.pl -o outdir  -A
                        -A use mlp alarms
                        -t test mode - additional prints
                        -S hostname (comma separated)

Monitor Unix Disk Space on all servers tagged with COMM_METHOD=SSH or RSH
and STORE results using the alarming subsystem. Should be run frequently.
When run from GEM console, the report created is put onto web
pages.
