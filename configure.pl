cp: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use strict;
use File::Basename;
use Cwd;

# DO NOT EDIT CONFIGURE.PL - EDIT CONFIGURE.PL.ORIG

# *STEP - TEST PERL VERSION, $DISPLAY VARIABLE
BEGIN {
	if( $] < 5.008001 ) {
		if( defined $^V and $^V!~/\s*$/) {
			die "Error : You must have perl 5.8.1 or later installed.  You are running $^V \n";
		}
		my($x)=$];
		$x=~s/0+/\./g;
		$x=~s/\.\.+/\./g;
		die "Error : You must have perl 5.8.1 or later installed.  You are running $x ($])\n";
	}
	die "FATAL ERROR: \$DISPLAY is NOT DEFINED ON UNIX\n" if $^O !~ /MSWin32/  and ! $ENV{DISPLAY};
	umask(0);
}

END {	# We do this next thing to allow running right from the
		# command line with a doubleclick on windows.
      if( $^O =~ /MSWin32/ ) {
          print "<Hit Enter Key To Exit>";
          <STDIN>;
      }
}
			
$|=1;

# DO NOT EDIT CONFIGURE.PL - EDIT CONFIGURE.PL.ORIG
my  $curdir=dirname($0);
die "You Must Map Samba Shares To A Drive Letter.\nCurdir=$curdir\n" 
	if $^O =~ /MSWin32/ and $curdir =~ /\\\\/;
chdir($curdir) or die "Cant cd to $curdir: $!\n";

my($dir)=cwd();
die "ERROR : must run configure.pl from an appropriate directory\n" unless -d "$dir/lib" and -d "$dir/conf";

# *STEP - SET $includes
my($includes)=" -I$dir/lib ";	
$includes=" -I$dir/Win32_perl_lib_5_8".$includes if $^O =~ /MSWin32/ and $]>=5.008;
my(@newargs);
if( defined $ENV{perlargs} ) {
	$includes .= $ENV{perlargs}." ";
	print "Appending environment variable perlargs ($ENV{perlargs}) to includepath\n";
}

# *STEP - PROCESS @ARGV - --PERLINC --DEVELOPERMODE 
my($SKIP)=0;
my($REGISTER_THIS_AS_MONSTATION)='FALSE';

foreach (@ARGV) {
	if( /^\-+PERLINC=/i ) {
		s/\-+PERLINC=//;
		$includes.=" -I$_";
	} elsif( /^\-+SKIP/i ) {
		$SKIP=1;
	} elsif( /^\-+DEVELOPERMODE/i ) {
		$includes=" -w ".$includes;	
	} else {
		push @newargs,$_;
	}
}

# DO NOT EDIT CONFIGURE.PL - EDIT CONFIGURE.PL.ORIG

# *STEP - INTRODUCTION MESSAGE
if( ! $SKIP ) {
system("cls 2>nul ")   if  $^O =~ /MSWin32/;
system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;
print "\n";
print "************************************\n";
print "* Generic Enterprise Manager Setup *\n";
print "************************************\n";
print "
Welcome to GEM.   configure.pl sets up GEM configuration files.

No changes are made to any of your servers from configure.pl
  - exception the web application is copied to a directory you will identify.

*****************************
*** This process is safe  ***
*****************************




Hit Enter To Continue, CTRL-C to Exit\n";

<STDIN>;
system("cls 2>nul")   if  $^O =~ /MSWin32/;
system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;
print "**************************************************************
* Generic Enterprise Manager Setup - PERMISSION REQUIREMENTS *
**************************************************************

GEM is a tool for Database Administrators and requires administrator access.

Windows 
  - Native administrator Access to Windows (BUILTIN/Administrators is best)
  - Administrator level access to Windows Databases
  - This is required to monitor disks/errorlogs and to read system info

Unix 
  - You are responsible for setting up ssh/scp from the monitoring server to 
    the appropriate database (sybase/oracle/mysql) unix/linux accounts.  
  - ssh/scp connectivity must not require passwords
  - Instructions are included in the documentation - its not hard
  - This is required to monitor disks/errorlogs and to read system info
  - Administrator level access (sa_role/admin/super...) to Unix Databases
  
Hit Enter To Continue, CTRL-C to Exit\n";

<STDIN>;
system("cls 2> nul")   if $^O =~ /MSWin32/;
system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;
print "****************************
* GEM REQUIRED COMPONENTS  *
****************************

IIS WEB SERVER
  - The primary user interface is a web application
  - Requires a script enabled directory 
  - Above directory must be writable from Windows monitoring stations 
A MONITORING DATABASE
  - For monitoring data and inventory system
  - The free sybase for linux works fine
PERL ON THE MONITORING SERVER(S)
  - configure.pl will detect if you need to download & install any
    perl modules for this version of perl ($^X)
CLIENT
  - Unix installer uses X access so something like exceed
  - set your \$DISPLAY variable.

Hit Enter To Continue, CTRL-C to Exit\n";

<STDIN>;
# put up a display window only on unix
if(  $^O !~ /MSWin32/ ) {
	system("clear 2> /dev/null");
print "

Unix detected... DISPLAY=$ENV{DISPALY} \n";
print "ERROR: YOU MUST HAVE DISPLAY SET!\n" unless $ENV{DISPALY};


}

system("cls 2>nul")   if $^O =~ /MSWin32/;
system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;

print "\n";
print "************************************\n";
print "* Generic Enterprise Manager Setup *\n";
print "************************************\n\n";

print "GEM is copyright(c) 1996-2010 by SQL Technologies
All rights are reserved.  

Full right to use is detailed elsewhere.  You may use and evaluate GEM so long
as you intend to purchase a license, and may not reverse engineer or copy any
features for inclusion in any other server management products.

Hit Enter To Continue, CTRL-C to Exit\n";

<STDIN>;
}

if( defined $ENV{PROCESSOR_LEVEL} ) {	# WIN32	
	my($rc)=run_discovery_command(
		-title=> "WINDOWS PPM (Perl Package Manager) INSTALL",
		-text => "We will now install win32 perl modules via ppm.\n",
		-command=> "$^X $includes $dir/ppm_installer.pl"
	);
	die "Woah - Your PPM Run Failed - Can Not Proceed" if $rc eq "ERROR";
}

system("cls 2>nul")   if $^O =~ /MSWin32/;
system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;
print "************************************\n";
print "* Generic Enterprise Manager Setup *\n";
print "************************************\n\n";
print "Testing Your Currently Used Perl ($^X)\n\n";

my($rc)=run_discovery_command(
	-title=> "WINDOWS MODULE TEST",
	-text => "We will now test your perl module install.\n",
	-command=> "$^X $includes $dir/bin/test_perl_modules.pl"
);
die "Woah - Your Perl is Missing Modules - Can Not Proceed" if $rc eq "ERROR";

#
# COPY SAMPLE FILES IF NEEDED
#
my($rc,$rowresults)=run_discovery_command(
		-title=> "CONFIG FILE CHECK",
		-text => "Checking Configfiles.\n",
		-command=> "$^X $includes $dir/bin/test_copy_configfiles.pl"
);
die "Woah - Your Config Setup Failed - Can Not Proceed" if $rc eq "ERROR";

#
# CHECK COMMANDS
#
my($rc,$rowresults)=run_discovery_command(
	-title=> "WINDOWS REQUIRED COMMAND CHECKS",
	-text => "Checking Configuration For Required Commands.\n",
	-command=> "$^X $includes $dir/bin/test_required_commands.pl"
);
# die "Woah - Required Command Checker Failed" if $rc eq "ERROR";

# push @newargs, "--REGISTER_AS_MONSRV";
print "\n\n";
print "$^X  $includes $dir/bin/configure.pl ".join(" ",@newargs)."\n\n";
system("$^X $includes $dir/bin/configure.pl ".join(" ",@newargs));
exit(0);

# DO NOT EDIT CONFIGURE.PL - EDIT CONFIGURE.PL.ORIG

sub run_discovery_command {
	my(%args)=@_;
	
	system("cls")   if  $^O =~ /MSWin32/;
	system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;
			
	print "\n";
	print "***********************************\n";
	printf "* %22s *\n", $args{-title};
	print "***********************************\n\n";

		
	print 	$args{-text},"\n";
	
	print   "cmd: ",	$args{-command},"\n\n";
	open( XXY, "$args{-command} |" )	or die "Cant Run $args{-command}\n";
	my($rc)='UNKNOWN';
	my(@results);
	foreach (<XXY>) {
		print "> ",$_;
		push @results,$_;
		$rc='ERROR' 	if /ERROR:/i;
		$rc='SUCCESS' 	if /SUCCESS[A-Z]+:/i;
	} 
	close(XXY);
	
	if( ! $SKIP ) {
		print "\n\nHit Enter To Continue, CTRL-C to Exit\n";		
		$a=<STDIN>;
	}
	print "Continuing...\n";	
	print "\n\n";		
	return( $rc,\@results);
}

__END__


=head1 NAME

configure.pl - GEM Configuration Tool

configure.pl is the <EM><STRONG>GEM</STRONG></EM> configuration utility.  
This utility will build the configuration files (stored in the conf subdirectory) and runs
some basic setup tasks for GEM.  The GEM Configuration Tool is quite intelligent, and will
survey your systems to give you a good baseline that should require minimal tuning to work
within your environment.  

Some of the things configure.pl does include:

=over 4

=item * Create Configuration Files

One primary output of configure.pl is the configuration files stored in the conf directory.  These files can be hand edited once GEM is set up.

=item * Rebuild of all the code

Changing the hashbangs (the first line of the file on unix is known as a hashbang and defines the command line
interpreter to use),  removing Control-M (dos newline) characters from the files, and modifying library paths.

=item * Monitoring Setup

The initial database installation is done via configure.pl

=item * Initial Build

This program also will generate an initial version of the <EM><STRONG>GEM</STRONG></EM> console and prepopulate 
the alarm system with alerts and reports relevant to your environment.  

=head1 IMPORTANT NOTES

=over 4

=item * configure.pl and gem.pl create log files in the logs subdirectory, 
which includes both console messages AND diagnostic messages.  If you
have a problem please look at this file.  It contains all output including
output that should be only produced with configure.pl --DEBUG.

=item * Work through the tabs from left to right.

=item * "Save Configuration Changes"
writes your configuration files.  While some
operations (adding stored procedures to servers)
are permanent once completed, the majority are not. It is important to save before
you quit.

=back

=head1 How to Run Configure.pl

<p><font color=RED>perl configure.pl</FONT></p>

If you wish to see diagnostic messages, you may run <font color=RED>perl configure.pl --DEBUG</FONT></p>

<FONT SIZE=+1 COLOR=RED>FOLLOW THE FOLLOWING STEP BY STEP INSTRUCTIONS TO COMPLETE YOUR INSTALLATION</FONT>

After you run configure.pl, it will bring you through a set of screens designed to validate your perl installation
and to run a basic sanity check.

The first screen:

<PRE>
 ************************************
 * Generic Enterprise Manager Setup *
 ************************************

 Welcome to GEM.   configure.pl sets up GEM configuration files.

 No changes are made to any of your servers from configure.pl
  - exception the web application is copied to a directory you will identify.

 *****************************
 *** This process is safe  ***
 *****************************

 Hit Enter To Continue, CTRL-C to Exit

</PRE>
Hit enter and work through the screens. After the first few screens it will start running programs

For Example: <i>C:\Perl\bin\perl.EXE  -IG:/gem/Win32_perl_lib_5_8 -IG:/gem/lib  G:/gem/bin/test_perl_modules.pl</i> which tests your perl libraries and

For Example: <i>C:\Perl\bin\perl.EXE  -IG:/gem/Win32_perl_lib_5_8 -IG:/gem/lib  G:/gem/bin/test_copy_configfiles.pl</i> which copies the .dat.sample extension configuration files to .dat files (if those .dat files do not exist)

For Example <i>C:\Perl\bin\perl.EXE  -IG:/gem/Win32_perl_lib_5_8 -IG:/gem/lib  G:/gem/bin/test_required_commands.pl</i> which tests that the rquired commands are in your path.  After this, it will start up the configure.pl user interface.
=head1 The Welcome Tab

GENERAL: Navigate the configuration utility tabs from left to right.  Online help is available on our web site.

PURPOSE: Introduction, License Details, License Acceptance

This tab explains GEM and details licensing options.  New users will want to use our Free GEM License to trial the
product - and switch to the Full GEM License after they are comfortable.  

Select "I AGREE WITH THE LICENSE" to continue

The GEM Help pages provide help to guide you through your install.  Each page has a separate help section.
=head1 The Register Tab

PURPOSE: Basic setup plus product registration

=over 4

=item * Enter Your Information:
This info is never shared so you might as well make it correct. Your Email address will be used as a target for some GEM Reports. 

=item * Describe Your Environment & Databases

=item * Select Gem License Type
Details on the differences between the Free and Full License can be found on the welcome page.  

=item * Select Connectivity Options
GEM requires no password ssh access between your UNIX GEM monitoring system and your database hosts.  If this
is set up, use the defaults (Allow SSH on the Unix to Unix line - nothing else selected).  Alternate access (rsh and ftp) methods
are available but some features may be disabled.  Leave the Win32 Monitor connectivity options unchecked.

=item * Click "REGISTER YOUR VERSION"
to register your product.

=back 3

=head1 The Paths Tab

PURPOSE: Define your possible SYBASE and ORAHOME directories

Enter all possible locations for the SYBASE and ORAHOME variables in your preferred order.  This will be used by the server 
discovery process.  

For example, you might put C:/sybase D:/sybase /export/home/sybase /apps/sybase as 4 entries on a samba install for your
sybase directories.

The system actually supports 10 instances of each variable - you will need to manually edit gem.dat if you
have more than 5 though.
=head1 The Mail Tab

PURPOSE: Ensure Mail Connectivity 

This tab is tests mail connectivity from your monitoring server.  You can test mail on windows and on unix.  The windows 
montioring workstation will require that you define your SMTP Mail Server.  

Both "Test Mail Install" buttons will result in a mail being sent to your GEM Administrator Mail Address (entered earlier).  This tab is
important as electronic mail is fundamental way we communicate server problems to you.

=head1 The Database Tab

PURPOSE: Set Up Alarm & Monitoring System

GEM uses a centralized alarm database which must be called gemalarms. This small database must allready exist and be set up
as per the installation instructions.  The Install/Upgrade Database button creates the tables & procedures in that db.

The database must be accessible from all your monitoring servers.  This implies that sql server can only be used for an
alarm db when you are in a win32 only environment.  You are responsible for the unixodbc setup required for sql server
connectivity from unix if you insist on using sql server.  As sql server does not run on unix, this should not be a problem.
Use Sybase for unix only and samba installs.

=over 4

=item * Fill in the Alarm Database Server Info
with information about the preconfigured alarm database.  The Alarm System Mail Address is arbitrary
and alerts are be mailed using that as a reply-to address.  It is ok to use an INVALID mail address 
as you wont ever reply to the alerts.

=item * Install/Upgrade Alarm Database
creates your alarm database tables & procedures 

=item * Validate Alarm Database
checks the database for sanity and version. 
 

=back

=cut
