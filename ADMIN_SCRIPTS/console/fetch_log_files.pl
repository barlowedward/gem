#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# copyright (c) 2005-2011 by SQL Technologies.  All Rights Reserved.
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com

use strict;
use File::Basename;
use Repository;
use Carp qw(confess cluck);
use Getopt::Long;
use CommonFunc;
use MlpAlarm;

my($VERSION)="2.0";

sub usage {
	print @_,"fetch_log_files.pl \n";

#Required: -FILETYPE=FILE TYPE
#      ASE_BACKUP_SERVER 
#      ASE_SERVER_LOG
#      ASE_HISTORICAL_SERVER 
#      ASE_MONITOR_SERVER
#      REP_SERVER 
#      ENVIRONMENT_FILE
#      INTERFACES_FILE
#      
#      Default=ALL
#      \n";

 	exit(1);
}

$|=1;

use vars qw($FILETYPE $DEBUG $BATCHID $FILETYPE );

GetOptions(
#	"FILETYPE=s"				=>\$FILETYPE,		
	"BATCHID=s"					=>\$BATCHID,		
	"BATCH_ID=s"				=>\$BATCHID,		
	"FILETYPE=s"				=>\$FILETYPE,	
	"DEBUG"						=>\$DEBUG
	)	or usage( "syntax error\n" );


sub error_out {
   my($msg)=join("",@_);
   $msg = "BATCH ".$BATCHID.": ".$msg if $BATCHID;
   MlpEvent(-monitor_program=>$BATCHID, -system=>$BATCHID, -message_text=>$msg, -severity =>"ERROR") if $BATCHID;
   MlpBatchJobErr( $msg ) if $BATCHID;
   die $msg;
}

my($root_dir)=get_gem_root_dir()."/data/system_information_data/";
die "Woah... no root directory $root_dir"	unless -d $root_dir;
die "NO FILETYPE arg" unless $FILETYPE;

#if( $FILETYPE ) {
#	usage "Invalid File Type\n" 
#		unless $FILETYPE eq "ASE_BACKUP_SERVER"
#      or $FILETYPE eq "ASE_SERVER_LOG"
#      or $FILETYPE eq "ASE_HISTORICAL_SERVER"
#      or $FILETYPE eq "MONITOR_SERVER"
#      or $FILETYPE eq "REP_SERVER" 
#      or $FILETYPE eq "ENVIRONMENT_FILE"
#      or $FILETYPE eq "INTERFACES_FILE";      
#   }
   
print "fetch_log_files.pl Version $VERSION\n";
print "Called In DEBUG mode\n" if $DEBUG;
print "Batch Id $BATCHID\n" if $BATCHID;
print "No Batch Id \n" unless $BATCHID;
print "Run At ".localtime(time)."\n";
# print "File Type is $FILETYPE\n" if $FILETYPE;
	
MlpBatchJobStart(-BATCH_ID => $BATCHID ) if $BATCHID;

my($dat);
$dat=get_logfiles(-filetype=>$FILETYPE);
if( ! $dat ) {
	print STDERR "no discovered files \n";
	exit(0);
}


#use Data::Dumper;
#print Dumper \$dat;
my %x =%{$dat};
my($number_of_processed_files)=0;
my(%error_messages_by_host);
foreach my $keyname ( sort keys %x ) {
	my $ref=$$dat{$keyname};
	
	if( $ref->{FETCHFILE} ne "YES" ) {
		print "  Skipping:  $keyname\n";
		next;
	}
#	if( $FILETYPE ) {
#		my($skipit)=0;
#		if( $FILETYPE eq "ASE_BACKUP_SERVER" ){
#			$skipit++ if $ref->{File_Type} ne "Errorlog";
#			$skipit++ if $ref->{Server_Type} ne "BackupServer";			
#      } elsif( $FILETYPE eq "ASE_SERVER_LOG" ){
#      	$skipit++ if $ref->{File_Type} ne "Errorlog";
#      	$skipit++ if $ref->{Server_Type} ne "DBServer";
#      } elsif( $FILETYPE eq "ASE_HISTORICAL_SERVER" ){
#      	$skipit++ if $ref->{File_Type} ne "Errorlog";
#      	$skipit++ if $ref->{Server_Type} ne "HistServer";
#      } elsif( $FILETYPE eq "MONITOR_SERVER" ){
#      	$skipit++ if $ref->{File_Type} ne "Errorlog";
#      	$skipit++ if $ref->{Server_Type} ne "MonServer";
#      } elsif( $FILETYPE eq "REP_SERVER"  ){
#      	$skipit++ if $ref->{File_Type} ne "Errorlog";
#      	$skipit++ if $ref->{Server_Type} ne "RepServer";
#      } elsif( $FILETYPE eq "ENVIRONMENT_FILE" ){
#      	$skipit++ if $ref->{File_Type} ne "Environment";
#      } elsif( $FILETYPE eq "INTERFACES_FILE" ){
#      	$skipit++ if $ref->{File_Type} ne "Interfaces";
#      }  
#      if( $skipit ) {
#      	print "Skipping:  $ref->{File_Type} - $keyname\n";
#      	next;
#      }
#   }

	my($hostname,$file)=split(/:/,$keyname);	
	$error_messages_by_host{$hostname}='' unless defined $error_messages_by_host{$hostname};
	my($localdir)=$root_dir."/".$hostname;
	my($localfilename)=$root_dir."/".$hostname."/".basename($file);	
	
	if( ! -d $localdir ) {
		mkdir($localdir,0777);
	} elsif( ! -d dirname($localfilename) ) {
		mkdir(dirname($localfilename),0777);
	}
	
	die "Cant Write to directory $localdir" unless -w "$localdir";
	
	unlink $localfilename if -r $localfilename;
	warn "Cant Remove $localfilename" if -r $localfilename;
	
	my($cmd);	
	if( ! is_nt() ) {		
		$cmd="scp $keyname $localfilename";
		print "   Copying: ",$keyname,"\n";
		print "      $cmd\n";
		
		open( XX, $cmd ." 2>&1 |" ) or die "Cant Run $cmd $!\n";
		my($found)=0;
		while(<XX>) {
			chomp;chomp;
			warn "*** ",$_,"\n";	
			$error_messages_by_host{$hostname} .= $_."\n";
		}
		close(XX);
	} else {
		$cmd = "Copy $file to $localfilename\n";
		use File::Copy;
		if( ! -r $file ) {
			warn "Cant read $file\n";
		} else {
			copy($file,$localfilename);
		}
	}
	if( ! -r $localfilename ) {
		warn "*** $cmd\n";
		warn "*** Copy Fail: $localfilename\n";
		$error_messages_by_host{$hostname} .= "$cmd - Copy Fail: $localfilename\n";
		
	} else {
		# TEST SIZE OF FILE AND ALERT IF > 1 GB
		if( -s $localfilename > 1000000000 ) {
			$error_messages_by_host{$hostname} .= "Huge Log File $keyname\n";
		} 
		print "Successful Copy of $localfilename\n" if $DEBUG;
	}	
}

foreach (keys %error_messages_by_host ) {
	if( $error_messages_by_host{$_} eq "" ) {
		MlpHeartbeat(  -monitor_program => $BATCHID,
			-system=> $_,
			-subsystem=> "Log File Fetch",
			-state=> "OK",
			-message_text=> "Fetch Ok" ) if $BATCHID;
	} else {
		MlpHeartbeat(  -monitor_program => $BATCHID,
			-system=> $_,
			-subsystem=> "Log File Fetch",
			-state=> "ERROR",
			-message_text=> $error_messages_by_host{$_} ) if $BATCHID;
		print "ERROR: $error_messages_by_host{$_} \n";
	}	
}
exit(0);