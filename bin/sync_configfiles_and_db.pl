use strict;
use CommonFunc;
use Repository;
use Getopt::Long;
use MlpAlarm;
use Inventory;

use vars qw( $DEBUG $ANALYZE );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions(  "DEBUG"	=> \$DEBUG  );

my($dir)=get_gem_root_dir();

my $GEMCONFIG=read_gemconfig( -debug=>$DEBUG);
my(@errors);
foreach my $typ ( get_passtype() ) {
	
	next	if $typ eq "sybase" and $GEMCONFIG->{USE_SYBASE_ASE} !~ /Y/;
	next	if $typ eq "sqlsvr" and $GEMCONFIG->{USE_SQLSERVER}  !~ /Y/;
	next	if $typ eq "mysql"  and $GEMCONFIG->{USE_MYSQL}       !~ /Y/;
	next	if $typ eq "oracle" and $GEMCONFIG->{USE_ORACLE}     !~ /Y/;
	
	my(@svrs)=get_password(-type=>$typ);
	printf( "-- Found %4s registered servers of type $typ \n",($#svrs+1) );
	print "-- Loading inventory system with ",$typ,"_discovery\n";
	print "-- Loading GEM_Servers Table\n";
		
	foreach my $s ( @svrs ) {		
		my(%hsh) = get_password_info(-type=>$typ,-name=>$s);	
		my($production_state)="";
		$production_state=$hsh{SERVER_TYPE}	if $hsh{SERVER_TYPE};
		my($mneumonic)=", mneumonic='".$hsh{MNEUMONIC}."'" if $hsh{MNEUMONIC};
		my($mn)="'".$hsh{MNEUMONIC}."'" if  $hsh{MNEUMONIC};
		$mn='null' unless $mn;
		
		my($im)="'N'" if  $hsh{IS_MONITORED} and $hsh{IS_MONITORED} =~ /n/i;
		$im="'Y'" unless $im;
		
		InvUpdSystem(-system_type=>$typ."_discovery", -system_name=>$s, -production_state=>$production_state, -debug=>$DEBUG,
			-is_dba_monitored=>'Y', -attributes=>\%hsh, -attribute_category=>"dba_discovery", -clearattributes=>1 );
		
		my($query)="
if not exists ( select * from GEM_servers where system_name='".$s."' and system_type='".$typ."' )
	insert GEM_servers ( system_name,   system_type,   blackedout_until ,   production_state, mneumonic, is_monitored  )
	values
	('".$s."','".$typ."',null,'".$production_state."',$mn,$im)
else 


	update GEM_servers
		set production_state='".$production_state."', is_monitored=".$im." ". $mneumonic. "
		where system_name='".$s."'
		and 	system_type='".$typ."'	
";
		#print $query;
		_querynoresults($query,1-$DEBUG);
		
#		foreach ( keys %hsh ) {
#			printf "%20s %20s %20s => %s\n",$s,$typ,$_,$hsh{$_};
#			next if $hsh{$_} eq $shsh{$_};
#			if( ! $attribute_map{$typ.":".$_} ) {
#				push @errors,"No Map Data For $typ.$_\n";
#				$attribute_map{$typ.":".$_} = "FAIL";
#				next;
#			} elsif(  $attribute_map{$typ.":".$_} eq "FAIL" ) {
#				next;
#			}
#			print "CHG_ATTRIBUTE SERVER $typ.$s ATTRIBUTE $_ NEWVAL=$hsh{$_}\n" unless $ANALYZE;
#		}		
	}
	
	print "Final Update\n";
	my($query)="update GEM_servers set production_state= ( select p2.production_state
				from GEM_servers p2
				where p1.system_name  = p2.system_name
				and   p1.system_type != p2.system_type )
	from GEM_servers p1
	where    p1.production_state=''";		
	_querynoresults($query,1-$DEBUG);
	
	
	
	my($query)="update GEM_servers 
		set mneumonic = ( select mneumonic from GEM_servers p2
		where p1.system_name=p2.system_name
		and   p1.system_type=p2.system_type
		and   p2.mneumonic is not null )
		from GEM_servers p1
		where p1.mneumonic is null";	
	_querynoresults($query,1-$DEBUG);
		
	print "Final Update 2\n";
	my($query)="update GEM_servers set production_state='PRODUCTION' where production_state=''";
	_querynoresults($query,1-$DEBUG);
}

sub error_out {
	foreach (@_) { chomp;print "ERROR: $_\n"; }
	print @_;
	exit(1);
}

if( $#errors<0 ) {
	print "-- SUCCESSFUL: No Errors\n";
	exit(0);
}

foreach (@errors) {
	s/^\s+//;
	print "ERROR: $_";
}

exit( $#errors+1 );