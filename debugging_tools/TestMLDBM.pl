use strict;
use lib qw(/apps/sybmon/dev/lib /apps/sybmon/lib G:/dev/lib G:/dev/apps/sybmon/lib G:/dev/Win32_perl_lib_5_8);

#use MLDBM qw(BerkleyDB::Hash Storable);
#use MLDBM qw(DB_File Storable);
use DBM::Deep;
#use MLDBM;
use Repository;
use Fcntl;
use Data::Dumper;

my($root)=get_gem_root_dir();
print "ROOT DIR is $root\n";
	
my($dbmfile)=$root."/debugging_tools/mldbm.dat";
print "File=$dbmfile\n";

my(%o);
#my($dbm) = DBM::Deep->new(file=>"$dbmfile", locking=>1, autoflush=>0 );
my($dbm)= tie %o, 'DBM::Deep', $dbmfile;
#my($dbm)= tie %o, 'MLDBM', $dbmfile, O_CREAT|O_RDWR, 0640 or die $!;

$dbm->{hellow} = "hellow";
$dbm->{hello2} = "hello2";
my(%hash);
$hash{x}='1';
$hash{y}='2';
$dbm->{hash}=\%hash;

print Dumper $dbm;
