#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use File::Copy;
use Sys::Hostname;
use MlpAlarm;
use CommonHeader;
use CommonFunc;
use Repository;
use Getopt::Long;
use vars qw( $SYSTEMS $DEBUG $OUTFILE $LOGALARM $NEWLABEL $OUTDIR $INDIR $INFILE $BATCH_ID $BATCH_CAT);


$|=1;

my($curdir)=cd_home();

sub usage {
	return @_, "Usage: ",basename($0)," --OUTDIR=file -INDIR=dir -INSPEC=spec --SYSTEMS=system[,system] [--DEBUG] --NEWLABEL=name \n";
}


die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
   GetOptions(  "OUTFILE=s"	=> \$OUTFILE,
		"SYSTEMS=s"	=> \$SYSTEMS,
		"NEWLABEL=s"	=> \$NEWLABEL,
		"DEBUG"		=> \$DEBUG,
		"LOGALARM"	=> \$LOGALARM,
		"BATCH_ID=s"	=> \$BATCH_ID,
		"BATCH_CAT=s"	=> \$BATCH_CAT,
		"INDIR=s"	=> \$INDIR,
		"OUTDIR=s"	=> \$OUTDIR,
		"INFILE=s"	=> \$INFILE );

my($VERSION)="1.0";
my($program_desc, $program_batchname)=("PC File Fetcher",$BATCH_ID);

$LOGALARM=1 if $BATCH_ID;

sub error_out {
   my($msg)=join("",@_);
   $msg = "BATCH ".$BATCH_ID.": ".$msg if $BATCH_ID;

   MlpBatchJobErr($msg) if $LOGALARM;
   MlpEvent(
      -monitor_program=> $BATCH_ID || "win32_get_file.pl",
      -system=> $BATCH_CAT || "",
      -message_text=> $msg,
      -severity => "ERROR"
   ) if $LOGALARM;
   die $msg;
}


die usage("Must pass indir\n")  unless $INDIR;
die usage("Must pass inspec\n") unless $INFILE;
die usage("Must pass OUTDIR\n") unless $OUTDIR;
error_out("OUTDIR $OUTDIR is not a directory") unless -d $OUTDIR;
error_out("OUTDIR $OUTDIR is not writable") unless -w $OUTDIR;

my(@indirs);	# could do this in a grep but im tired
foreach (split(/,/,$INDIR) ) {
		s.^[\\\/]..;
		s.[\\\/]$..;
		push @indirs,$_;
}

print "$program_desc - ",basename($0)," - version $VERSION\n";
print "Run At: ",(scalar localtime(time))."\n";

MlpBatchJobStart(  -BATCH_ID=>$BATCH_ID, -SUBKEY=>$BATCH_CAT||"" )	if defined $LOGALARM;

#$OUTDIR=get_gem_root_dir()."/data/system_information_data/schdelgu" unless $OUTDIR;
print "Outdir is $OUTDIR\n\n";
mkdir($OUTDIR,0775) unless -d $OUTDIR;

my(@systems);
my(%disks_by_svr);
if( ! defined $SYSTEMS ) {
	@systems=get_password(-type=>'win32servers');
} else {
	@systems=split(/,/,$SYSTEMS);
}

foreach my $sys (@systems) {
	my %info = get_password_info(-name=>$sys, -type=>'win32servers');
	my(@x)=split(/\s+/,$info{DISKS});
	$disks_by_svr{$sys} = \@x;

	# do your thing on each system here
	#MlpBatchRunning( -message_text=>"Working on system $sys\n")
		#if defined $LOGALARM;	# update job monitor

	print "system $sys\n" if defined $DEBUG;
	my $ref = $disks_by_svr{$sys};
	my($foundfile)="FALSE";
	foreach my $disk (@$ref) {
		foreach my $dir ( @indirs ) {
			print "Looking up //$sys/$disk/$dir/\n" if defined $DEBUG;
			#print __LINE__," ff=$foundfile\n";
			if( -d "//$sys/$disk/$dir/" ) {
				if( -r "//$sys/$disk/$dir/$INFILE" ) {
					$foundfile="TRUE";
			#print __LINE__," ff=$foundfile\n";
					print "... found file \\\\$sys\\$disk\\$dir\\$INFILE\n";
					my($newfname)=$sys."_";
					if( defined $NEWLABEL ) {
						$newfname.=$NEWLABEL;
					} else {
						$newfname.=$INFILE;
					}
					print "... copying to $OUTDIR\\",$newfname."\n";
					print "ERROR: no file //$sys/$disk/$dir/$INFILE\n"
							unless -e "//$sys/$disk/$dir/$INFILE";
					print "ERROR: cant read file //$sys/$disk/$dir/$INFILE\n"
							unless -r "//$sys/$disk/$dir/$INFILE";
					print "ERROR: no outdir $OUTDIR\n"
							unless -d $OUTDIR;
					print "ERROR: cant write $OUTDIR\n"
							unless -w $OUTDIR;

					print STDERR "ERROR: no file //$sys/$disk/$dir/$INFILE\n"
							unless -e "//$sys/$disk/$dir/$INFILE";
					print STDERR "ERROR: cant read file //$sys/$disk/$dir/$INFILE\n"
							unless -r "//$sys/$disk/$dir/$INFILE";
					print STDERR "ERROR: no outdir $OUTDIR\n"
							unless -d $OUTDIR;
					print STDERR "ERROR: cant write $OUTDIR\n"
							unless -w $OUTDIR;

					unlink( $OUTDIR."\\".$newfname ) if -e $OUTDIR."\\".$newfname;
					print STDERR "ERROR: cant unlink $OUTDIR\\$newfname\n"
						if -e $OUTDIR."\\".$newfname;
					print "ERROR: cant unlink $OUTDIR\\$newfname\n"
						if -e $OUTDIR."\\".$newfname;
					copy( "//$sys/$disk/$dir/$INFILE", $OUTDIR."\\".$newfname )
						or error_out( "Copy of //$sys/$disk/$dir/$INFILE \nto $OUTDIR\\$newfname \nfailed : $!" );
				}
			} else {
			#print __LINE__," ff=$foundfile\n";
				print "... dir does not exist\n" if defined $DEBUG;
				$foundfile="NA";
			}
		}
	}
	print "($foundfile) ... no file on $sys\n" if $foundfile eq "FALSE";
}

MlpBatchJobEnd() if defined $LOGALARM;
exit(0);

__END__

=head1 NAME

win32_get_file.pl - Get File From Each Win32 System Out there

=head2 DESCRIPTION

Used to copy file to local system - usually into html_output dir.

This is an excellent program and can be used as a template for other
little batches you wish to use.

It probably will mostly be called from other batches - the one i have in
mind right now is to get win32 task scheduler jobs log files from all my
systems.  That would be something like
win32_get_file.pl --INDIR=/winnt,/windows --INFILE=SchedLgu.txt
I will also add one for hosts.

=cut
