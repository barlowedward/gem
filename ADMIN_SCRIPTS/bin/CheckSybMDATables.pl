
sub do_a_server {
	debug( "Connecting\n" );
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD,-type=>"sybase");
	if( ! $rc ) {
		logdie("Cant connect to $SERVER as $USER\n");
	}

	dbi_set_mode("INLINE");
	my(@pids)=split(",",$PID);

	my(%cat,%val,%def);
	foreach( dbi_query(-db=>"master",-query=>"sp__configure \@dont_format='Y'")) {
		my(@dat)=dbi_decode_row($_);	# cat,parm,val,defval
		$cat{$dat[1]} = $dat[0];
		$val{$dat[1]} = $dat[2];
		$def{$dat[1]} = $dat[3];
	}

	# MON DEADLOCKS!

	exit(0);
	my($sql)="select BackupInProgress,LastBackupFailed from master..monTables";
	foreach( dbi_query(-db=>"master",-query=>$sql)) {

	my($sql)="DBName,DBID,BackupInProgress,LastBackupFailed,TransactionLogFull,BackupStartTime,SuspendedProcesses,QuiesceTag
	from monOpenDatabases";
	RunAndAlarm($sql);

	sub RunAndAlarm {
		my($sql)=@_;
		foreach( dbi_query(-db=>"master",-query=>$sql)) {
			my(@dat)=dbi_decode_row($_);
			my($db)=shift @dat;
		}
	}

	#my($count)=0;
	#while ($count<200) {
	#	foreach my $pid (@pids) {
	#		my $sql = "kill $pid";
	#		print $sql,"\n";
	#		foreach( dbi_query(-db=>"master",-query=>$sql)) {
	#			my(@x)=dbi_decode_row($_);
	#			print join(" ",@x),"\n";
	#		}
	#	}
	#	sleep 1;
	#	$count++;
	#}
	#
	dbi_disconnect();
}
exit(0);


#!/apps/sybmon/perl/bin/perl

use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use strict;
use File::Basename;
use MlpAlarm;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Repository;

use vars qw( $SYSTEMS $DOSHRINK $DATABASE $NOLOG $DEBUG $TYPE $DETAILS $HTML $PRODONLY );

my($VERSION)=1.0;

$|=1;
sub debug { print @_ if defined $DEBUG; }

sub usage
{
	return "Usage: crdate_report.pl -TYPE=sqlsvr|sybase [--DATABASE=db] --PRODONLY --DETAILS --HTML --DEBUG --SYSTEMS=system[,system]\n";
}

die usage() if $#ARGV<0;
die "Bad Parameter List $!\n".usage() unless
   GetOptions(  "DATABASE=s"	=> \$DATABASE,
		"SERVER=s"	=> \$SYSTEMS,
		"TYPE=s"	=> \$TYPE,
		"SYSTEMS=s"	=> \$SYSTEMS,
		"HTML"	=> \$HTML,
		"DETAILS"	=> \$DETAILS,
		"PRODONLY"	=> \$PRODONLY,
		"DEBUG"	=> \$DEBUG,
		"NOLOG"	=> \$NOLOG,
		"DOSHRINK"	=> \$DOSHRINK);

$SYSTEMS="ALL" unless $SYSTEMS;
$TYPE="sybase" unless is_nt();
cd_home();

my($nl)="\n";
$nl="<br>\n" if $HTML;
dbi_set_web_page(1) if $HTML;

print "<h1>" if $HTML;
print "Simple Object Changed Report",$nl;
print "</h1>" if $HTML;
print "Created by crdate_report.pl at ".localtime(time).$nl;
print "This report is based only on the crdate field in sysobjects",$nl;
print "This report will run only on production servers",$nl if $PRODONLY;
print "This report excludes GEM Objects",$nl;

MlpBatchJobStart(-BATCH_ID=>"ObjectCrdateReport");
my(@outdat);

foreach my $srv (get_password(-type=>'sybase')) {
			do_a_server($srv,'sybase');
}

print "<TABLE BORDER=1><TR BGCOLOR=BEIGE><TH>SERVER</TH><TH>DB</TH><TH>Ext PRocs</TH><TH>0-7 Days </TH><TH>8-14 Days</TH><TH>15-21 Days</TH><TH>22-28 Days</TH></TR>\n"
	if $HTML;
print "SERVER\tDB\tExt Procs\t0-7 Days \t8-14 Days\t15-21 Days\t22-28 Days\n"
	unless $HTML;
foreach (@outdat) { print $_; }
print "</TABLE>" if $HTML;

MlpBatchJobEnd();
exit(0);

sub do_a_server {
	my($srv,$type)=@_;
	my($login,$pass,$cmd);

	if( $SYSTEMS eq "ALL" and $PRODONLY ) {
		my %info = get_password_info(-name=>$srv, -type=>$type);
		if( $info{SERVER_TYPE} ne "PRODUCTION" and $info{SERVER_TYPE} ne "CRITICAL" and $info{SERVER_TYPE} ne "QA"  ) {
			print "Skipping Server $srv As It is ",$info{SERVER_TYPE},$nl if $DEBUG;
			next;
		}
	}
	print "Processing Server $srv",$nl if $DEBUG;

	if( defined $type ) {
		($login,$pass)=get_password(-type=>$type,-name=>$srv);
	} else {
		($login,$pass)=get_password(-type=>'sqlsvr',-name=>$srv);
		($login,$pass)=get_password(-type=>'sybase',-name=>$srv) if ! $pass;
	}
   	print "Connecting to $srv\n" if $DEBUG;
   	my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass);
	if( ! $rcx ) {
      		print "Cant connect to $srv as $login",$nl;
		next;
	}

	print "Getting Database List",$nl if defined $DEBUG;
	my($server_type,@okdb)=dbi_parse_opt_D('%',1);

	if( $DATABASE ) {
		@okdb = split(/,/,$DATABASE);
	}
	my($allok)="TRUE";
	foreach my $db (@okdb) {
		next if $db eq "tempdb";
		my($num1, $num2, $num3, $num4, $numprocs) = (0,0,0,0,0);
		print "Working on Database $db",$nl if defined $DEBUG;
		dbi_use_database($db);
		my(@d)=dbi_query(-db=>$db,-query=>"select name, crdate, datediff(dd,crdate,getdate())
		 from sysobjects where datediff(dd,crdate,getdate())<=28", -debug=>$DEBUG);

		foreach ( @d ) {
			my($nm, $crdt, $days)=dbi_decode_row($_);
			if( $nm =~ /^sp\_\_/ or $nm eq "record" ) {
				$numprocs++;
			} elsif( $days <= 7 ) {
				$num1++;
			} elsif( $days <= 14 ) {
				$num2++;
			} elsif( $days <= 21 ) {
				$num3++;
			} elsif( $days <= 28 ) {
				$num4++;
			}
			print "$srv\t$db\t$nm\t$crdt\t$days days",$nl if $DETAILS
				and $nm !~ /^sp\_\_/ and $nm ne "record";
		}
		if( $num1+$num2+$num3+$num4 > 0 ) {
			$allok="FALSE";
			if( $HTML ) {
				push @outdat,"<TR><TD>$srv</TD><TD>$db</TD><TD>$numprocs</TD><TD>$num1</TD><TD>$num2</TD><TD>$num3</TD><TD>$num4</TD></TR>\n";
			} else {
				push @outdat,"$srv\t$db\t$numprocs\t$num1\t$num2\t$num3\t$num4\n";
			}
		} elsif( $DEBUG or $DETAILS )  {
			if( $HTML) {
				push @outdat, "<TR BGCOLOR=BEIGE><TD>$srv</TD><TD>$db</TD><TD COLSPAN=5>OK</TD></TR>\n";
			} else {
				push @outdat, "$srv\t$db\tOK\n";
			}
		}
	}
	if( $allok eq "TRUE" ) {
		push @outdat, "<TR BGCOLOR=BEIGE><TD>$srv</TD><TD COLSPAN=6>NO DATABASES HAVE CHANGED OBJECTS</TD></TR>\n";
	}

   	dbi_disconnect();
	print "Completed Work on Server $srv",$nl if defined $DEBUG;
}

__END__

=head1 NAME

monitor_tables.pl - Sybase MDA Table Monitoring Reports

=head2 USAGE

	monitor_tables.pl -USA_USER -SSERVER -PSA_PASS

=cut

