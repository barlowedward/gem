#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Mail::Mailer;
use File::MultiTail;
use CommonFunc;
use Getopt::Long;
use MlpAlarm;
use Logger;
use Repository;
use File::Tail;
use CommonHeader;

my $VERSION = "1.0";
my $ping_interval=15;	# seconds
my $opt_d;					# for internal debugging
my $WRITEFILE=0;			# Dont Send Beeps if 0
my $DEBUG;					# = 1;
my $MAILTO = "ebarlow\@mlp.com";
my $LOGFILE= get_gem_root_dir()."/data/GEM_BATCHJOB_LOGS/monitor_appslog.log";
my $LOG="/monitor/server/log";
my $VMSTATDB="$LOG/Vmstat/";
my $MPSTATDB="$LOG/MPstat/";
my $PDHSTATDB="$LOG/PDHstat/";
my $DFDB="$LOG/Df/";

$|=1;

use vars qw( $DEBUG $ALARMFILE $NOLOG $NOLOCK);
sub usage {
	print @_;
	print "Monitor_appslog.pl v1.0
monitors the file $LOG/app.log
	--ALARMFILE
	--NOLOG
	--NOLOCK
	--DEBUG\n";
	return "";
}

die usage("Bad Parameter List\n") unless GetOptions(
		"ALARMFILE=s"	=> \$ALARMFILE,
		"NOLOG"			=> \$NOLOG,
		"NOLOCK"	=> \$NOLOCK,
		"DEBUG"      	=> \$DEBUG );

print "(dbg) $0 started\n" if defined $DEBUG;
cd_home();
my($rc);
if( ! defined $NOLOCK ) {
	$rc=Am_I_Up(-debug=>$DEBUG);
	print "\n(dbg) checking running status\n";
}

print "(dbg) run lock checked: rc=$rc\n" if defined $DEBUG;
if( $rc ) {
	exit(0) unless is_interactive();
	my($fname,$secs,$thresh)=GetLockfile();
	print "Cant Start program is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nFile=$fname secs=$secs thresh=$thresh secs\n";

	print "(dbg) locks check failed rc=$rc\n" if defined $DEBUG;
	print "(dbg) checking interactivity \n" if defined $DEBUG;
	print "(dbg) interactive=",is_interactive()," \n" if defined $DEBUG;
	print "ERROR: $0 is allready running\n";
	exit(0);
}

# WAIT 60 SECONDS AND CHECK FOR LOCKFILE AGAIN
if( ! defined $NOLOCK ) {
	sleep(60);
	$rc=Am_I_Up(-debug=>$DEBUG);
	print "\n(dbg) checking running status\n";
	if( $rc ) {
		exit(0) unless is_interactive();
		my($fname,$secs,$thresh)=GetLockfile();
		print "Cant Start program is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nFile=$fname secs=$secs thresh=$thresh secs\n";

		print "(dbg) locks check failed rc=$rc\n" if defined $DEBUG;
		print "(dbg) checking interactivity \n" if defined $DEBUG;
		print "(dbg) interactive=",is_interactive()," \n" if defined $DEBUG;
		print "ERROR: $0 is allready running\n";
		exit(0);
	}
}

I_Am_Up_With_Print(-debug=>$DEBUG);
MlpBatchJobStep()	unless defined $NOLOG;
print "\n(dbg) marking running\n";

print "(dbg) lock check done\n" if defined $DEBUG;
print "monitor_appslog.pl version $VERSION\n";

print "THE KEYS ARE:\n	d: dfstats.pl:	D: ACTION=DF\n	P: USER=APPSWATCH:pong\n	A	APPSLOG\n	M	MPSTAT\n	V  ACTION=VMSTAT\n	K  GROUP=KRTRADE\n";

my($filename,$filetimesecs,$threshtime)=GetLockfile();
print "LOCKFILE INFO: File=$filename Ping=$filetimesecs Thres=$threshtime\n";

if( defined $ALARMFILE ) {
	open(ALARMFP,">> $ALARMFILE") or die "Cant Write $ALARMFILE : $! \n";
	print "WRITING TO $ALARMFILE\n";
}

logger_init( -debug=> $opt_d, -logfile => $LOGFILE, -command_run => $0, -mail_to =>$MAILTO, -fail_subject=>"Logger Error"  );
I_Am_Up_With_Print(-debug=>$DEBUG);

my($lastpingtime)=time;
my($jobstarttime)=$lastpingtime;
MlpBatchJobStart(-BATCH_ID=>"monitor_appslog.pl" ,-AGENT_TYPE=>'Gem Monitor') unless defined $NOLOG;

my($fileobj)=File::Tail->new( name=>"$LOG/app.log", maxinterval=>300, adjustafter=>7 );
my($line);
my($lineno)=0;
while( defined( $line=$fileobj->read)) {
	do_a_line( $line );
	if( $lineno++>50 ) {
		I_Am_Up_With_Print(-debug=>$DEBUG);
		MlpBatchJobStep()	unless defined $NOLOG;
		print "\n(dbg) marking running\n";

		print "\n";
		$lineno=0;
		if( time - $jobstarttime > 6*60*60 ) {	# die after 6 hours
			print "Normal Exiting as job has been running 6 hours\n";
			last;
		}
	}
}

I_Am_Down();
exit(0);

sub I_Am_Up_With_Print {
	print "I_Am_Up_With_Print() at ".localtime()."\n" if $DEBUG;
	I_Am_Up(@_);
}

sub do_a_line {
	my($line)=@_;
	my ($Mon,$Day,$Time,$Host,$SubSystem,$Text);
	my ($Year,$Mon1,$Day1,$Time1,$TimeZone,$TZoffset);
	my ($From,$DfServer,$Action,$DfText);
	my ($df_1,$DfFS,$df_2,$df_3,$df_4,$df_5,$df_6,$df_7);
	my ($FROM,$VMSTATSERVER,$ACTION,$VMSTATTEXT);
	my ($MPSTATSERVER,$MPSTATTEXT);
	my ($PDHSERVER,$PDHTEXT);
	my ($PIPSSERVER,$PIPSTEXT);
	my ($LIUSERVER,$LIUTEXT);

	if( time - $lastpingtime > $ping_interval ) {
		$lastpingtime=time;
		I_Am_Up_With_Print(-debug=>$DEBUG);
		MlpBatchJobStep()	unless defined $NOLOG;
		print "\n(dbg) marking running\n";
	}

	chomp $line;
	($Mon,$Day,$Time,$Host,$SubSystem,$Text)=split(' ', $line, 6 );

	if ( $SubSystem eq "dfstats.pl:" ) {
			print "d";
			# note that the next line is an invalid ip address
			return if $Host =~ /\d+\.\d+\.\d+\.\d+\.\d+/;

			# from dfstats.pl - disk space monitor
			my($dummy,$pid,$sev,$fs,$pct,$sz,$usd,$avl);
			print  "==========================\n";
			if( $Text=~/^\[/ ) {
				($dummy,$pid,$sev,$fs,$pct,$sz,$usd,$avl)=split(/\s+/, $Text);
				print  "> regular solaris dfstats.pl line\n"
					if defined $DEBUG;
			} else {
				($fs,$pct,$sz,$usd,$avl)=split(/\s+/, $Text);
				my($p)=$pct;
				$p=~s/Pct=//;
				$sev=".warning]" if $p>=90;
				$sev=".err]" if $p>=97;
				print  "> regular linux dfstats.pl line setting sev to $sev\n"
					if defined $DEBUG;
			}
			my($SEVERITY)="OK";
			if( $pct =~ /Pct/ ) {
				my($x)=$pct;
				$x=~s/\D//g;
				if( $x !~ /^\d+$/ ) {
					die "ERROR - $pct is not numeric\n";
				}
				$SEVERITY="WARNING" 	if $x>92;
				$SEVERITY="ERROR"	  	if $x>98;
			} else {
				$SEVERITY="WARNING" 	if $sev=~/\.warning\]/;
				$SEVERITY="ERROR"	  	if $sev=~/\.err\]/ or $sev=~/\.error\]/;
			}
			$fs=~s/FS=//;
			my($msg)="$fs $pct $sz $usd $avl";
			print  "> dfstats.pl input=",$line,"\n";
			print  "> dfstats.pl system=$Host state=$SEVERITY subsystem=$fs msg=$msg\n";
			print  "==========================\n";
			Heartbeat(
					-monitor_program	=> "MonitorAppslog",
					-system				=> $Host,
					-state				=> $SEVERITY,
					-subsystem			=> $fs,
					-message_text		=> $msg );
			return;
		}

  		if ( $Text =~ /ACTION=DF/ ) {
				print "D";
				# modified by ed barlow
				# read from apps.log  - USER=xxx HOST=xxx ACTION=DF TEXT=xxx
    			($From,$DfServer,$Action,$DfText)=split(/ /,$Text,4);
    			$DfServer 	=~ s/HOST=// ;
    			$DfText 		=~ s/TEXT=// ;

				# Text Message Format
				#   time(secs),File System,Kbytes,Used,Avail,File,FilesUsed,FilesAv
    			my($DTime,$DfFS,$df_1,$df_2,$df_3,$fi_1,$fi_2,$fi_3)=split(/,/,$DfText,8);
				return unless defined $df_1 and $df_1=~/\d/ and int($df_1)>0;
				return unless defined $df_3 and $df_3=~/\d/ and int($df_3)>0;
				return unless defined $fi_1 and $fi_1=~/\d/ and int($fi_1)>0;
				return unless defined $fi_3 and $fi_3=~/\d/ and int($fi_3)>0;
				my($bytepct)=int((100*$df_2)/$df_1);
				my($filepct)=int((100*$fi_2)/$fi_1);
				my($state)="OK";
				$state="EMERGENCY" 	if $bytepct>99 or $filepct>99;
				$state="CRITICAL" 	if $bytepct>95 or $filepct>95;
				$state="WARNING" 		if $bytepct>90 or $filepct>90;
				$df_1=int($df_1/1024);
				$df_2=int($df_2/1024);

				my($msg)="Drive $DfFS at $bytepct% UsedMb=$df_2 TotalMb=$df_1";
				$msg.=" Files Pct=$filepct% Files=$fi_1 Used=$fi_2" if $filepct>90;

				info("Saving DF Message Host=$DfServer State=$state $msg\n");
				Heartbeat(
					-monitor_program	=> "MonitorAppslog",
					-system				=> $DfServer,
					-state				=> $state,
					-subsystem			=> $DfFS,
					-message_text		=> $msg );

    			$DfFS =~ s/\//_/g if defined $DfFS;
    			#system ("touch ${DFDB}hosts/$DfServer") ;
				if( $WRITEFILE ) {
    				open (DfServerFile, ">>${DFDB}$DfServer.$DfFS")
						|| die "Can't open file $DfServer.$DfFS.\n" ;
    				print DfServerFile "$df_1,$df_2,$df_3,$df_4,$df_5,$df_6,$df_7\n" ;
    				close (DfServerFile) ;
				}
				return;
		}

  		if ( $Text =~ /USER=APPSWATCH:pong/ ) {
			print "P";
    		my($USER,$HOST,$ACTION,$TEXT)=split(/ /,$Text,4);
    		$HOST 	=~ s/HOST=// ;
    		$ACTION 	=~ s/ACTION=// ;
    		$TEXT 	=~ s/TEXT=// ;

			my($STATE)="OK";
			$STATE="WARNING" if $ACTION eq "DOWN";

			info("\nPong Detects Host=$HOST State=$STATE $TEXT\n");
			Heartbeat(
					-monitor_program	=> "MonitorAppslog",
					-system				=> $HOST,
					-state				=> $STATE,
					-subsystem			=> "Pong",
					-message_text		=> $TEXT );

			return;
		}

    	if ( $Text =~ /APPSLOG/ ) {
			print "A";
			my $mafile = '/dat/brokdata/sybase/accounts/missing_accounts';
			my $msfile = '/dat/brokdata/sybase/securities/missing_securities';
			$Text =~ s/%R/\n/g;
			$Text =~ m|MSG\=(\w+)$|;
			AppMailFile($msfile,$1) if $1 eq "missing_securities";
			AppMailFile($mafile,$1) if $1 eq "missing_accounts";
			info( "Routine at Line ".__LINE__."\n" );
			LogMessage($line);
			return;
		}

    	if ( $Text =~ /MPSTAT/ ) {
			print "M";
			return unless $WRITEFILE;

			($FROM,$MPSTATSERVER,$ACTION,$MPSTATTEXT)=split(/ /,$Text,4);
			$MPSTATSERVER =~ s/HOST=//;
			my($OPENMPSERVER) = $MPSTATSERVER;
			$OPENMPSERVER =~ s/\-//;
			$MPSTATTEXT =~ s/TEXT=//;
			#
			# check for bad filehandle
			#
			my $cpu = (split(/\s+/,$MPSTATTEXT))[1];
			return if length($OPENMPSERVER) < 2;
			if( $WRITEFILE )  {
			open( eval $OPENMPSERVER, ">>${MPSTATDB}$MPSTATSERVER.$cpu")
				|| die "Can't open file $MPSTATSERVER. $!\n";
			eval (print $OPENMPSERVER "$MPSTATTEXT\n");
			eval (close $OPENMPSERVER );
			}
			return;
    	}

		return if $Text=~/ACTION=NTUSERS/;

    	if ( $Text =~ /ACTION=VMSTAT/ ) {
			print "V";
			return unless $WRITEFILE;
			($FROM,$VMSTATSERVER,$ACTION,$VMSTATTEXT)=split(/ /,$Text,4);
			$VMSTATSERVER =~ s/HOST=//;
			my($OPENVMSERVER) = $VMSTATSERVER;
			$OPENVMSERVER =~ s/\-//;
			$VMSTATTEXT =~ s/TEXT=//;
			#
			# check for bad filehandle
			#
			return if length($OPENVMSERVER) < 2;
			if( $WRITEFILE )  {
			open( eval $OPENVMSERVER, ">>${VMSTATDB}$VMSTATSERVER")
				|| die "Can't open file $VMSTATSERVER. $!\n";
			eval (print $OPENVMSERVER "$VMSTATTEXT\n");
			eval (close $OPENVMSERVER );
			}
			return;
    	}

    	if ( $Text =~ /GROUP=KRTRADE/ ) { 	# emb addition
			print "K";
			my($GROUP,$HOST,$SERVICE,$STAT)=split(/ /,$Text,4);
    		$HOST 	=~ s/HOST=// ;
    		$SERVICE =~ s/SERVICE=// ;
    		$STAT 	=~ s/STAT=// ;

			my($STATE)="OK";
			$STATE="ERROR" if $STAT ne "UP" and $STAT ne "STARTING";

			Heartbeat(
					-monitor_program	=> "MonitorAppslog",
					-system				=> $HOST,
					-subsystem			=> $SERVICE,
					-state				=> $STATE,
					-message_text		=> $Text  );

			return;
		}
		return if $line=~/last message repeated /;

		print "Unhandled Exception\n";
		print " => $line\n";
}

sub AppMail {
	my $Text = shift;
	# Mail attr
	my $To = "smiano\@mail1.mlp.com";
	my $From = "APPLOG\@mail1.mlp.com";
	my $Subject = "System APPS monitoring message";
	SendAlert($To,$From,$Subject,$Text);
}
sub AppMailFile {
	my $file = shift;
	my $Subject = shift;
	# Mail attr
	my $To = "misacc\@mail1.mlp.com";
	my $From = "APPLOG\@mail1.mlp.com";

	{
	local($/) = undef;
	open(FILE,"$file") || return;
	my $body = <FILE>;
	close(FILE);
	SendAlert($To,$From,$Subject,$body);
	}
}
sub LogMessage {
	my $Text = shift;
	chomp $Text;
	info( "LogMessage:".$Text."\n" );
	# return unless $WRITEFILE;
	# my $LogFile="/monitor/server/log/monitor.log";
	# #
	# open(LOG,">>$LogFile") || die "Can't open: $1\n";
	# print LOG $Text . "\n";
	# close(LOG);
}

sub SendAlert {
	my($To,$From,$Subject,$Text)=@_;
	info(  "SendAlert(to=$To Subject=$Subject Text=($Text)\n" );

	$Text=~s/\s+$//;

	if( $Subject eq "UNIX System monitoring message" ) {
		# parse syslogs messages
		chomp $Text;
		debug( "TEXT=$Text\n");
		my(@parsemsg)=split(/:/,$Text,4);
		$parsemsg[0] =~ m/^(\w\w\w)\s+(\d+)\s+(\d+)/;
		my($mon,$day,$hr)=($1,$2,$3);
		$parsemsg[1] =~ m/(\d+)/;
		my($mi)=($1);
		my($ss,$system,$progname)=split(/\s+/,$parsemsg[2]);
		$progname=~s/\[\d+\]//;		# parse out pid
		my($msg)=$parsemsg[3];

		# if this parsed report it and return otherwise contiue
		if( defined $mon and defined $day and defined $hr and defined $mi and defined $ss and defined $system and defined $progname and defined $msg and $msg!~/^\s*$/ and $system!~/^\s*$/ and $progname!~/^\s*$/) {
			if( $msg=~/HOST=/ ) {
				$msg=~/HOST=([\w\d\_\-]+)/;
				$system=$1;
			}
			$msg=~s/^\s+//;

			my($SEVERITY)="WARNING";
			$SEVERITY="OK" 		if $Text=~/\.info\]/;
			$SEVERITY="OK" 		if $Text=~/\.notice\]/;
			$SEVERITY="WARNING" 	if $Text=~/\.warning\]/;
			$SEVERITY="WARNING" 	if $Text=~/\.crit\]/;
			$SEVERITY="ERROR"	  	if $Text=~/\.err\]/;
			$SEVERITY="ERROR"	  	if $Text=~/file system full/;
			$SEVERITY="CRITICAL"	if $Text=~/\.emerg\]/;

			alert( "MlpEvent: sys=$system severity=$SEVERITY subsy=$progname msg=$msg\n" );
			MlpEvent(
				-monitor_program	=> "MonitorAppslog",
				-system				=> $system,
				-severity			=> $SEVERITY,
				-subsystem			=> $progname,
				-message_text		=> $msg )
						unless defined $NOLOG;
			return;
		} else {
			info("#####################################\n");
			info( "# COULDNT Parse MSG=$Text\n" );
			info( "# DATE=$mon~$day~$hr~$mi~$ss SYS=$system PROG=$progname MSG=$msg \n" );
			info("#####################################\n");
		}
	} else {
		info("#####################################\n");
		info("# New Section Of The Code Being Run #\n");
		info("#####################################\n");
		chomp $Text;

		my(@parsemsg)=split(/:/,$Text,4);
		$parsemsg[0] =~ m/^(\w\w\w)\s+(\d+)\s+(\d+)/;
		my($mon,$day,$hr)=($1,$2,$3);
		$parsemsg[1] =~ m/(\d+)/;
		my($mi)=($1);
		my($ss,$system,$progname)=split(/\s+/,$parsemsg[2]);
		$progname=~s/\[\d+\]//;		# parse out pid
		my($msg)=$parsemsg[3];

		# if this parsed report it and return otherwise contiue
		if( defined $mon and defined $day and defined $hr and defined $mi and defined $ss and defined $system and defined $progname and defined $msg and $msg!~/^\s*$/ and $system!~/^\s*$/ and $progname!~/^\s*$/) {
			if( $msg=~/HOST=/ ) {
				$msg=~/HOST=([\w\d\_\-]+)/;
				$system=$1;
			}
			$msg=~s/^\s+//;

			my($SEVERITY)="WARNING";
			alert( "MlpEvent: sys=$system severity=$SEVERITY subsy=$progname msg=$msg\n" );
			MlpEvent(
				-monitor_program	=> "MonitorAppslog",
				-system				=> $system,
				-severity			=> $SEVERITY,
				-subsystem			=> $progname,
				-message_text		=> $msg )
					unless defined $NOLOG;
			return;
		} else {
			info("#####################################\n");
			info( "# COULDNT Parse MSG=$Text\n" );
			info( "# DATE=$mon~$day~$hr~$mi~$ss SYS=$system PROG=$progname MSG=$msg \n" );
			info("#####################################\n");
		}
	}
}

sub Heartbeat {
	my(%args)=@_;
	if( defined $ALARMFILE ) {
		my($str);
		foreach (keys %args ) { $str.=$_."=>".$args{$_}." "; }
		print ALARMFP $str,"\n";
	}

	MlpHeartbeat(%args)
					unless defined $NOLOG;
}

