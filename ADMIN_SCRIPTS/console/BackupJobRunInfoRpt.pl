#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2006 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "Usage: backup_report.pl\n";
}

use strict;
use Getopt::Long;
use Repository;
use MlpAlarm;

use vars qw($DEBUG $FULL $LAST );

GetOptions(
	"debug" 				=> \$DEBUG,
	"FULL"				=>\$FULL,
	"LAST"				=>\$LAST );


#my($BASE_DIR)=get_gem_root_dir();
#die "$BASE_DIR is not a directory\n" unless -d $BASE_DIR;

my($whereclause)="";
my($last)=0;
if( $FULL ) {
	$whereclause=" where JobBatchId!='BackupTran' ";
}
if( $LAST ) {
	$last=1;
}

# GO THROUGH AND GET THE FULL BACKUP FILES
#print "WHERECLAUSE: $whereclause\n" if $DEBUG;
#print "LASTCLAUSE: $last\n" if $DEBUG;

print "<h2>BackupJobRunInfoRpt.pl v1.0</h2>Created at ".scalar(localtime(time))."<p>";
print MlpReportBackupJobRunInfo(undef,$whereclause,$last)
