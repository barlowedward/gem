#!/apps/perl/linux/perl-5.8.2/bin/perl
# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use DBIFunc;
use dsget;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw($BATCH_ID $SHOWSQL $DEBUG $DBGMAX);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID
	--DBGMAX          [ number of rows for diagnostics ]
	--SHOWSQL
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --DEBUG \n";
  return "\n";
}

$| =1;
#my($curdir)=dirname($0);
#chdir $curdir or die "Cant cd to $curdir: $!\n";

# die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"DBGMAX=s"			=> \$DBGMAX ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"			=> \$BATCH_ID ,
		"SHOWSQL"      	=> \$SHOWSQL,
		"DEBUG"      		=> \$DEBUG );

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Credential Mgt' ) if $BATCH_ID;

$DBGMAX=1000000 unless $DBGMAX;		# max number of users & groups this can handle - for diagnostics

#/* , is_deleted       , display_name     */
my($cred_sql) = "(credential_name, credential_type, credential_id, system_name, system_type, collection_date, collected_by )";

my($connection)=MlpGetDbh();
#my(%DISABLED_ACT);		# DISABLED ACCOUNTS
print "INV_dsget.pl v1

Fetch SQL Server Logins From Stored Credentials
   if 	LDAP Login
   			save login info
   elif  LDAP Group
   			save group info
   			save group membership

";

LDAPinit($connection, $SHOWSQL, $PROGRAM_NAME, $DEBUG);

print "get_all_ldap_users()\n";
my($DNMAPPTR)=get_all_ldap_users();
print "get_all_ldap_groups()\n";
get_all_ldap_groups($DNMAPPTR,$DEBUG);
if( $DEBUG ) {
	print "============\n";
	use Data::Dumper;
	print $DNMAPPTR;
	print "============\n";
}
MlpBatchJobEnd() if $BATCH_ID;
exit(0);

#
# LOAD THE LDAP PRINCIPALS
#
my($query)="\tdelete INV_credential where credential_type='ldap user'";
print "SQL: ",$query,"\n" if $SHOWSQL;
MlpAlarm::_querynoresults($query,1);

open(X,"dsquery user -o samid -limit 0 |") or die "Cant run dsquery user -o samid -limit 0 $!\n";
while(<X>) {
	next if /^\s*$/;
	chomp;chomp;
	print $_ unless /\"/;
	next unless /\"/;
	s/\"//g;

	my($base)=	dbi_quote(-connection=>$connection,-text=>$_).",".			# login
					"'ldap user',".
					"null,".																	# sid
					"null,null";															# system, systype
	my($q)= "\tinsert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
	print "SQL: ",$q,"\n" if $SHOWSQL;
	MlpAlarm::_querynoresults($q,1);
	print "\n" if $SHOWSQL;

	#	foreach ( keys %DATA ) {
	#		$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t '".$_."',".
	#			 dbi_quote(-connection=>$connection,-text=>$DATA{$_})	.")";
	#		print "SQL: ",$q,"\n" if $SHOWSQL;
	#		MlpAlarm::_querynoresults($q,1);
	#		print "\n" if $SHOWSQL;
	#	}
}
close(X);

#
# CLEAR OUT USER CREDENTIALS
#
my($query)="\tdelete INV_credential where credential_type='ldap user'";
print "SQL: ",$query,"\n" if $SHOWSQL;
MlpAlarm::_querynoresults($query,1);

my($query)="\tdelete INV_credential_attribute where credential_type='ldap user'";
print "SQL: ",$query,"\n" if $SHOWSQL;
MlpAlarm::_querynoresults($query,1);

my($query)="\tdelete INV_credential where credential_type='ldap group'";
print "SQL: ",$query,"\n" if $SHOWSQL;
MlpAlarm::_querynoresults($query,1);

my($query)="\tdelete INV_credential_attribute where credential_type='ldap group'";
print "SQL: ",$query,"\n" if $SHOWSQL;
MlpAlarm::_querynoresults($query,1);

my($query)="\tdelete INV_group_member where group_type='ldap group'";
print "SQL: ",$query,"\n" if $SHOWSQL;
MlpAlarm::_querynoresults($query,1);


#
# FOR EACH SQL SERVER LOGIN & GROUP WE FETCH LDAP INFO
#

print '
-------------------------------------------------
- GET USERS LIST                                -
-------------------------------------------------
';

my($q)="select distinct credential_name from INV_credential_attribute
where credential_type like 'sqlsvr_syslogin'
and 	attribute_key='isntuser'
and 	attribute_value='1'
and   credential_name not like 'BUILTIN\%'
and 	credential_name not like system_name+'%'";

my(%cache_ldap_user_by_samid,%cache_ldap_user_dn_by_samid,%cache_ldap_user_samid_by_dn);
print "SQL: ",$q,"\n"  if $SHOWSQL;
my($DBGCOUNTER)=$DBGMAX;
foreach( dbi_query(-db=>"", -connection=>$connection, -query=>$q)) {
	my($n)=dbi_decode_row($_);
	$n=basename($n);
	print "==> PROCESSING USER $n\n";
	if( $cache_ldap_user_by_samid{lc($n)} ) { print "skipped - allready loaded\n";	next;	}

	my($DN)=get_user_dn_from_samid($n);
	if( $DN eq "" ) {	print "skipped - no dn returned\n\n";	next;	}

	#print " ==> DN=$DN\n";
	my($DATA)=get_user_info_by_dn_and_save($DN,$n);
	if( $DEBUG and defined $DATA ) {	foreach (keys %$DATA ) { print " -- $_  :: $$DATA{$_} \n"; }}

	if( $DBGCOUNTER-- < 0 ) { $DBGCOUNTER=$DBGMAX;	print "DBGDBG - end\n";	last;	}
}

print '
-------------------------------------------------
- GET GROUPS LIST                               -
-------------------------------------------------
';

my($q)="select distinct credential_name from INV_credential_attribute
where credential_type like 'sqlsvr_syslogin'
and 	attribute_key='isntgroup'
and 	attribute_value='1'
and   credential_name not like 'BUILTIN\%'
and 	credential_name not like system_name+'%'";

my(%allgroups);
print "SQL: ",$q,"\n" if $SHOWSQL;
foreach( dbi_query(-db=>"", -connection=>$connection, -query=>$q)) {
	my($n)=dbi_decode_row($_);
	print "==> PROCESSING GROUP $n\n";
	if( $allgroups{lc($n)} ) { 	print "skipped -group allready saved\n";		next; 	}

	my($DN)=get_group_dn_from_samid($n);
	if( $DN eq "" ) {					print "skipped - no dn returned\n\n";		next;	}

	my(%DATA)=get_group_info_by_dn_and_save($DN);
	if( $DEBUG ) {	foreach (keys %DATA ) { print " -- $_  :: $DATA{$_} \n"; }}
	$allgroups{lc($n)}= \%DATA;

	print "==> PROCESSING GROUP MEMBERS FOR $n\n";
	foreach my $member ( get_group_members_by_dn($DN) ) {
		print "==> Found $member - member of $n\n";
		my($samid)=get_samid_from_dn($member);
		if( $samid ) {
			print "SAVING MEMBER SAMID=",$samid," (group=$n)\n";
			my($base)=	dbi_quote(-connection=>$connection,-text=>$n).",".			# group name
							"'ldap group',".
							dbi_quote(-connection=>$connection,-text=>$DATA{sid}).",".		# sid
							dbi_quote(-connection=>$connection,-text=>$samid ).",".			# credential name
							"'ldap user',". # credential type
							"null,null,null";																	# system, systype

			my($q)= "\tinsert INV_group_member values \n\t( $base )";
			print "SQL: ",$q,"\n" if $SHOWSQL;
			MlpAlarm::_querynoresults($q,1);
			print "\n" if $SHOWSQL;

		} else {
			print "NOT SAVING MEMBER DN =",$member,"\n";
		}
	}
	if( $DBGCOUNTER-- < 0 ) { 	print "DBGDBG - end\n"; 		last; 	}
}

print '
-------------------------------------------------
- GET REMAINING USERS (Group Members Not Saved) -
-------------------------------------------------
';

my($q)="select * into #x from INV_group_member where group_type='ldap group'

delete #x
from #x x, INV_credential c
where	c.credential_name=x.credential_name
and	c.credential_type=x.credential_type

delete #x
from 	#x x, INV_credential c
where	lower(c.credential_name)=lower(x.credential_name)
and	c.credential_type=x.credential_type

select distinct credential_name from #x

drop table #x";

print "SQL: ",$q,"\n"  if $SHOWSQL;
foreach( dbi_query(-db=>"", -connection=>$connection, -query=>$q)) {
	my($n)=dbi_decode_row($_);
	$n=basename($n);

	# if( $DISABLED_ACT{$n} ) {		next;	}

	print "==> PROCESSING GROUP MEMBER - USER $n\n";
	if( $cache_ldap_user_by_samid{lc($n)} ) { print "skipped - allready loaded\n";	next;	}

	my($DN)=get_user_dn_from_samid($n);
	if( $DN eq "" ) {	print "skipped - no dn returned\n\n";	next;	}

	#print " ==> DN=$DN\n";
	my($DATA)=get_user_info_by_dn_and_save($DN,$n);
	if( $DEBUG and defined $DATA ) {	foreach (keys %$DATA ) { print " -- $_  :: $$DATA{$_} \n"; }}

	if( $DBGCOUNTER-- < 0 ) { $DBGCOUNTER=$DBGMAX;	print "DBGDBG - end\n";	last;	}
}

#my(@rows);
#foreach (keys %DISABLED_ACT ) {
#	delete_disabled_ldap_account($_);
#}
#print "THE FOLLOWING ACCOUNTS SHOULD BE DISABLED\n";
#foreach (@rows) {
#	my($n,$t,$sn,$st)=dbi_decode_row($_);
#	print $n," ",$t,' ',$sn,' ',$st,"\n";
#}

MlpBatchJobEnd() if $BATCH_ID;
exit(0);

#sub delete_disabled_ldap_account {
#	my($n)=@_;
#	print "DELETE DISABLED ACCOUNT $n\n";
#	my($q);
#	$q = "delete INV_group_member where credential_name=".dbi_quote(-connection=>$connection,-text=>$n).
#				" and group_type='ldap group'";
#	print "SQL: ",$q,"\n" if $SHOWSQL;
#	MlpAlarm::_querynoresults($q,1);
#	print "\n" if $SHOWSQL;
#
#	$q = "delete INV_credential_attribute where credential_name=".dbi_quote(-connection=>$connection,-text=>$n).
#			"	and credential_type='ldap user'";
#	print "SQL: ",$q,"\n" if $SHOWSQL;
#	MlpAlarm::_querynoresults($q,1);
#	print "\n" if $SHOWSQL;
#
#	$q = "delete INV_credential where credential_name=".dbi_quote(-connection=>$connection,-text=>$n).
#			"	and credential_type='ldap user'";
#	print "SQL: ",$q,"\n" if $SHOWSQL;
#	MlpAlarm::_querynoresults($q,1);
#	print "\n" if $SHOWSQL;
#
#	#$q = "select credential_name,credential_type,system_name,system_type from INV_credential where credential_name=".
#	#	dbi_quote(-connection=>$connection,-text=>$n);
#	#print "SQL: ",$q,"\n" if $SHOWSQL;
#	#push @rows, MlpAlarm::_querywithresults($q,1);
#	#print "\n" if $SHOWSQL;
#}
#
#sub get_user_info_by_dn_and_save {
#	my($DN,$samid)=@_;
#	print "f: get_user_info_by_dn_and_save($DN,$samid)\n" if $DEBUG;
#
#	if( $cache_ldap_user_by_samid{lc($samid)} ) {
#		print "Using Cached get_user_info_by_dn_and_save($DN,$samid)\n";
#		return $cache_ldap_user_by_samid{lc($samid)};
#	}
#
#	my($q2)="dsget.exe user $DN -L -samid -dn -desc -upn -sid -fn -mi -ln -office -display -empid -tel -email -hometel -pager -mobile -fax -iptel  -webpg -title -dept -company -mgr -pwdneverexpires -disabled -acctexpires";
#	open(X,$q2." |") or die "Cant run $q2 $!\n";
#	my(%DATA);
#	while(<X>) {
#		next if /^\s*$/;
#		chomp;chomp;
#		my($k,$v)=split(/:\s*/,$_,2);
#		return undef if /dsget failed:/;
#		next if $k =~ /^\s*$/;
#		$DATA{$k} = $v unless $v =~ /^\s*$/;
#		print ">>> $_\n" if $DEBUG;
#	}
#
#	# save existing data
##	if( $DATA{disabled} ne "yes" ) {
#		my($samid)= $DATA{samid};
#		#print "**************************************\n";
#		print "   => SAMID=$samid\n";
#		#print "**************************************\n";
#
#		my($base)=	dbi_quote(-connection=>$connection,-text=>$samid).",".			# login
#						"'ldap user',".
#						dbi_quote(-connection=>$connection,-text=>$DATA{empid}).",".		# sid
#						"null,null";																	# system, systype
#
#		my($q)= "\tinsert INV_credential values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
#		print "SQL: ",$q,"\n" if $SHOWSQL;
#		MlpAlarm::_querynoresults($q,1);
#		print "\n" if $SHOWSQL;
#
#		foreach ( keys %DATA ) {
#			$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t '".$_."',".
#				 dbi_quote(-connection=>$connection,-text=>$DATA{$_})	.")";
#			print "SQL: ",$q,"\n" if $SHOWSQL;
#			MlpAlarm::_querynoresults($q,1);
#			print "\n" if $SHOWSQL;
#		}
##	} else {
##		print "******************************************\n";
##		print "* Disabled Account $DATA{samid}\n";
##		print "******************************************\n\n";
##		$DISABLED_ACT{$DATA{samid}}=$DN;
##	}
#
#	$cache_ldap_user_by_samid{lc($samid)}= \%DATA;
#	return \%DATA;
#}
#
## GROUP INFO NOT SAVED AS NO SPOT FOR IT RIGHT NOW
#sub get_group_info_by_dn_and_save {
#	my($DN)=@_;
#	print "f: get_group_info_by_dn_and_save($DN)\n" if $DEBUG;
#	my($cmd) = 'dsget.exe group '.$DN.' -L -dn -sid -desc -samid -secgrp -scope';
#	print "exec: $cmd \n";
#	my(%DATA);
#	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
#	while(<Z>) {
#		next if /^\s*$/;
#		chomp;chomp;
#		my($k,$v)=split(/:\s*/,$_,2);
#		return undef if /dsget failed:/;
#		next if $k =~ /^\s*$/;
#		$DATA{$k} = $v unless $v =~ /^\s*$/;
#		print ">>> $_\n" if $DEBUG;
#	}
#	close(Z);
#
#	my($samid)= $DATA{samid};
#	print "**************************************\n";
#	print "* GROUP SAMID=$samid\n";
#	print "**************************************\n";
#	my($base)=	dbi_quote(-connection=>$connection,-text=>$samid).",".			# login
#					"'ldap group',".
#					dbi_quote(-connection=>$connection,-text=>$DATA{sid}).",".		# sid
#					"null,null";																	# system, systype
#
#	my($q)= "\tinsert INV_credential values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
#	print "SQL: ",$q,"\n" if $SHOWSQL;
#	MlpAlarm::_querynoresults($q,1);
#	print "\n" if $SHOWSQL;
#
#	foreach ( keys %DATA ) {
#		$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t '".$_."',".
#			 dbi_quote(-connection=>$connection,-text=>$DATA{$_})	.")";
#		print "SQL: ",$q,"\n" if $SHOWSQL;
#		MlpAlarm::_querynoresults($q,1);
#		print "\n" if $SHOWSQL;
#	}
#}
#
#sub get_group_members_by_dn {
#	my($DN)=@_;
#	print "f: get_group_members_by_dn($DN)\n" if $DEBUG;
#	my(@MEMBERDNS);
#	my($cmd) = 'dsget.exe group '.$DN.' -members -expand';
#	print "exec: $cmd \n";
#	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
#	while(<Z>) {
#		next if /^\s*$/;
#		chomp;
#		return undef if /dsget failed:/;
#		push @MEMBERDNS,$_;
#		#print "MEMBER >> ",$_,"\n";
#	}
#	close(Z);
#
#	return @MEMBERDNS;
#}
#
#sub get_samid_from_dn {
#	my($DN)=@_;
#	print "f: get_samid_from_dn($DN)\n" if $DEBUG;
#	return $cache_ldap_user_samid_by_dn{$DN} if $cache_ldap_user_samid_by_dn{$DN};
#
#	# not  prefetched - get it
#	my($cmd)="dsget.exe user $DN -samid";
#	print "exec: $cmd \n";
#	my($samid);
#	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
#	while(<Z>) {
#		next unless /^\s/;
#		next if /\s*samid\s*$/;
#		chomp;
#		s/\s+$//g;
#		s/^\s+//g;
#		return undef if /dsget failed:/;
#		$samid=$_;
#		#print ">> ",$_,"\n";
#	}
#	close(Z);
#	return undef if ! $samid;
#
#	$cache_ldap_user_samid_by_dn{$DN}=$samid;
#
#	get_user_info_by_dn_and_save($DN,$samid);
#	return $samid;
#}
#
## returns distinguished name from samid
##   get_user_dn_from_samid(ebarlow) returns "CN=Barlow\, Ed,OU=DBA,OU=IT,OU=AD_Users,DC=AD,DC=MLP,DC=com"
#sub get_user_dn_from_samid {
#	my($n)=@_;
#	print "f: get_user_dn_from_samid($n)\n" if $DEBUG;
#
#	return $cache_ldap_user_dn_by_samid{$n} if $cache_ldap_user_dn_by_samid{$n};
#	my($cmd) = 'dsquery.exe user -samid "'. basename($n).'"';
#	print "exec: $cmd \n";
#	my($DN)="";
#	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
#	while(<Z>) {		chomp;		$DN=$_;	}
#	close(Z);
#	$cache_ldap_user_samid_by_dn{$DN}=$n if $DN;
#	$cache_ldap_user_dn_by_samid{$n}=$DN if $n and $DN;
#	return $DN;
#}
#
## returns distinguished name from samid
## dsquery group -name job_it_dba
## "CN=job_IT_DBA,OU=IT,OU=Jobs,OU=Groups,DC=AD,DC=MLP,DC=com"
#sub get_group_dn_from_samid {
#	my($n)=@_;
#
#	my($cmd) = 'dsquery.exe group -name "'. basename($n).'"';
#	print "exec: $cmd \n";
#	my($DN)="";
#	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
#	while(<Z>) {
#		chomp;
#		$DN=$_;
#	}
#	close(Z);
#	return $DN;
#}

__END__

=head1 NAME

INV_dsget - load users and groups into INV_credential and
INV_credential_attribute

=head1 SYNOPSIS

INV_dsget.pl --BATCH_ID=s --DEBUG --DBGMAX=s

=head1 DESCRIPTION

Remove from INV_credential and from INV_credential_attribute all
records with credential_type = 'ldap user'.

Select credential_name from INV_credential_attribute records with

  credential_type  = 'sqlsvr_syslogin'
  attribute_key    = 'isntuser'
  attribute_value  = '1'
  credential_name != 'BUILTIN...'
  credential_name != system_name...

For each record:

  Build up a correspondence between samid and dn
  Get the user ldap data indexed by samid
  Insert a record into INV_credential
  Insert a record into INV_credential_attribute

Select credential_name from INV_credential_attribute records with

  credential_type  = 'sqlsvr_syslogin'
  attribute_key    = 'isntgroup'
  attribute_value  = '1'
  credential_name != 'BUILTIN...'
  credential_name != system_name...

For each record:

  Build up a correspondence between samid and dn
  Get the group ldap data indexed by samid
  Get the dn for each user in the group

Note: 'isntuser' does not mean 'is not user' but rather means
'is nt user'.  Similarly for 'isntgroup'.

=cu
