#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2004 By Edward Barlow
# Modified by B.Muthaiah on 08.05.2006

use strict;
use File::Basename;
use GemData;
use Getopt::Long;

$|=1;

use vars qw($DEBUG $opt_h $SERVERTYPE);

sub usage
{
	print @_;
	print "devices_html_rpt.pl -SERVERTYPE=type --DEBUG --HTML\n";
   return "\n";
}

GetOptions(
	"SERVERTYPE=s"=>\$SERVERTYPE,
	"DEBUG"=>\$DEBUG,
	"HTML"=>\$opt_h 	) or die usage( "syntax error\n" );

die usage() unless $SERVERTYPE;

use Sys::Hostname;
my($h)=hostname();
sub _statusmsg {    print "[msg] ",join("",@_); }
sub _debugmsg {     print "[dbg]  ",join("",@_); }
my($gem_data) = new GemData( -printfunc=>\&_statusmsg, -debugfunc=>\&_debugmsg, -hostname=>$h );

#my($gem_data)=new GemData();

print "<h2>" if defined $opt_h;
print uc($SERVERTYPE)." DEVICES SIZE/SPACE REPORT";
print "</h2>\n" if defined $opt_h;
print "\n" unless $opt_h;
print "<p>" if $opt_h;
print "Created by devices_html_rpt.pl at ".localtime(time)."\n";
print "<\p>" if $opt_h;

my($major_key)="DeviceSize";
my(%DISK_DEVICES);
if( $SERVERTYPE eq "sqlsvr" ) {
	foreach my $host ( @{$gem_data->get_set_data(-class=>'sqlsvr',-keys=>1) } ) {
		print "DBG: Working on $host\n" if defined $DEBUG;
		my($dat)=$gem_data->get_set_data(-class=>'sqlsvr',-name=>$host,-arg=>$major_key);

		foreach ( keys %$dat ) {
			if( ref $$dat{$_} ) {
					$DISK_DEVICES{$host.":".$_} = $$dat{$_};
			} else {
					$DISK_DEVICES{$host.":".$_} = $dat;
					last;
			}
		}
	}
} else {
	foreach my $host ( @{$gem_data->get_set_data(-class=>'sybase',-keys=>1) } ) {
		print "DBG: Working on $host\n" if defined $DEBUG;
		my($dat)=$gem_data->get_set_data(-class=>'sybase',-name=>$host,-arg=>$major_key);

		foreach ( keys %$dat ) {
			if( ref $$dat{$_} ) {
					$DISK_DEVICES{$host.":".$_} = $$dat{$_};
			} else {
					$DISK_DEVICES{$host.":".$_} = $dat;
					last;
			}
		}
	}
}

my($last_server)="";
if( defined $opt_h ) {
	print "<TABLE Border=1>\n" if defined $opt_h;
}

foreach my $k (sort keys %DISK_DEVICES) {
	my($server,$device)=split(":",$k);
	if( $server ne $last_server ) {
		$last_server=$server;

		if( $opt_h ) {
			print "<TABLE Border=3 width=75% cellpadding=2>\n";
			print "<TR HEIGHT=50 BGCOLOR=brown><TD colspan=5>
						<FONT COLOR=white SIZE=+2>DEVICES IN SERVER - $server</FONT></TD></TR>";
			print "<TR BGCOLOR=#FFCCCC><TH>Device</TH>
						<TH>Size</TH>";
			print "	<TH>Allocated</TH>
						<TH>Free Space</TH>" if $SERVERTYPE ne "sqlsvr";
			print "  <TH>Physical File</TH></FONT></TR>\n";
		} elsif( $SERVERTYPE eq "sqlsvr" ) {
			printf "\n%-20.20s %-20.20s %-7.7s %-s\n", "Server", "Device", "Size","Phys";
		} else {
			printf "\n%-20.20s %-20.20s %-7.7s %7s %7s %-s\n", "Server", "Device", "Alloc","Free","Size","Phys";
		}
	}
	my($href)=$DISK_DEVICES{$k};
	$$href{size} =~ s/\.0MB/MB/g;
	$$href{alloc} =~ s/\.0MB/MB/g;
	$$href{free} =~ s/\.0MB/MB/g;
	if( defined $opt_h ) {
		print "<TR><TD>$device</TD>
			<TD>$$href{size}</TD>\n";
		print	"<TD>$$href{alloc}</TD>
			<TD>$$href{free}</TD>\n" if $SERVERTYPE ne "sqlsvr";
		print	"<TD>$$href{phys}</TD></TR>\n";
	} elsif( $SERVERTYPE eq "sqlsvr" ) {
			printf("%-20.20s %-20.20s %-7.7s %-s\n", $server, $device,
				$$href{size},
				$$href{phys});
	} else {
		printf("%-20.20s %-20.20s %-7.7s %7s %7s %-s\n", $server, $device,
			$$href{alloc},
			$$href{free},
			$$href{size},
			$$href{phys});
	}
}

print "</TABLE>\n" if $opt_h;
print "</TABLE>\n" if defined $opt_h;
