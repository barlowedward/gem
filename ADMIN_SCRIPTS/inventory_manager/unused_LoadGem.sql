use gemalarms
go
drop table ST_Systems
go
create table ST_Systems (
	systemname	varchar(30),
	systemtype	varchar(30),
	is_active	varchar(30),	-- YES or NO
	login		varchar(30),
	password	varchar(30),
	record_date datetime
)
go
drop table ST_System_Attributes
go
create table ST_System_Attributes (
	systemname	varchar(30),
	systemtype	varchar(30),
	attribute	varchar(30),
	value			varchar(255),
	record_date datetime
)
go
drop table ST_Attributes
go
create table ST_Attributes (
	systemtype		varchar(30),
	attribute		varchar(30),
)
go
drop table ST_Attribute_Possible_Values
go
create table ST_Attribute_Possible_Values (
	attribute	varchar(30),
	value			varchar(30)
)
go

insert ST_Attribute_Possible_Values values ( 'systemtypes','win32')
insert ST_Attribute_Possible_Values values ( 'systemtypes','unix')
insert ST_Attribute_Possible_Values values ( 'systemtypes','sybase')
insert ST_Attribute_Possible_Values values ( 'systemtypes','oracle')
insert ST_Attribute_Possible_Values values ( 'systemtypes','sqlsvr')

insert ST_Attribute_Possible_Values values ('SERVER_TYPE','DEVELOPMENT')
insert ST_Attribute_Possible_Values values ('SERVER_TYPE','PRODUCTION')
insert ST_Attribute_Possible_Values values ('SERVER_TYPE','DR')
insert ST_Attribute_Possible_Values values ('SERVER_TYPE','QA')
insert ST_Attribute_Possible_Values values ('SERVER_TYPE','DECOMMISSIONED')

insert ST_Attribute_Possible_Values values ('UNIX_COMM_METHOD','SSH')
insert ST_Attribute_Possible_Values values ('UNIX_COMM_METHOD','RSH')
insert ST_Attribute_Possible_Values values ('UNIX_COMM_METHOD','FTP')
insert ST_Attribute_Possible_Values values ('UNIX_COMM_METHOD','NONE')

insert ST_Attribute_Possible_Values values ('WIN32_COMM_METHOD','SSH')
insert ST_Attribute_Possible_Values values ('WIN32_COMM_METHOD','RSH')
insert ST_Attribute_Possible_Values values ('WIN32_COMM_METHOD','FTP')
insert ST_Attribute_Possible_Values values ('WIN32_COMM_METHOD','NONE')

insert ST_Attribute_Possible_Values values ('BUILD_STATE','PROPOSED')
insert ST_Attribute_Possible_Values values ('BUILD_STATE','UNDER CONSTRUCTION')
insert ST_Attribute_Possible_Values values ('BUILD_STATE','BUILD COMPLETED')
go
insert ST_Attributes select  attribute, 'SERVER_TYPE' from ST_Attribute_Possible_Values where attribute='systemtypes'
go
insert ST_Attributes select  attribute, 'BUILD_STATE' from ST_Attribute_Possible_Values where attribute='systemtypes'
go
insert ST_Attributes values ( 'sybase','backupservername')
insert ST_Attributes values ( 'sybase','portnumber')
insert ST_Attributes values ( 'sybase','hostname')
insert ST_Attributes values ( 'win32',	'hostname')
insert ST_Attributes values ( 'unix',	'UNIX_COMM_METHOD')
insert ST_Attributes values ( 'unix',	'WIN32_COMM_METHOD')
insert ST_Attributes values ( 'unix',	'SYBASE')
insert ST_Attributes values ( 'unix',	'IGNORE_RUNFILE')
insert ST_Attributes values ( 'win32',	'DRIVES')
go
drop procedure ST_Update_System_sp
go
create procedure ST_Update_System_sp
	@systemname varchar(30),
	@systemtype varchar(30),
	@is_active	varchar(3),
	@login 	   varchar(30),
	@password   varchar(30)
as
begin
	if not exists ( select * from ST_Attribute_Possible_Values
		where attribute='systemtypes'
		and   value    =@systemtype
	)
	begin
		raiserror 22000 "ERROR - INVALID SYSTEM TYPE"
		return
	end
	if exists ( select * from ST_Systems
					where systemname=@systemname
					and   systemtype=@systemtype )
					-- old system
					UPDATE ST_Systems
					set   login=@login, password=@password, is_active=@is_active, record_date=getdate()
					where systemname=@systemname
					and   systemtype=@systemtype
	else			-- new system
					INSERT ST_Systems values ( @systemname, @systemtype, @is_active, @login, @password, getdate()   )

end
go
drop procedure ST_Update_Attribute_sp
go
create procedure ST_Update_Attribute_sp
	@systemname varchar(30),
	@systemtype varchar(30),
	@attribute  varchar(30),
	@value	   varchar(255)
as
begin
	if not exists ( select * from ST_Systems
		where systemname=@systemname
		and   systemtype=@systemtype
	)
	begin
		raiserror 22001 "ERROR - INVALID SYSTEM"
		return
	end

	if not exists ( select * from ST_Attributes
			where systemtype =@systemtype
			and   attribute = @attribute
	)
	begin
		raiserror 22002 "ERROR - INVALID ATTRIBUTE"
		return
	end

	if exists ( select * from ST_System_Attributes
		where systemname=@systemname
		and   systemtype=@systemtype
	)
		UPDATE ST_System_Attributes
		set value=@value,	record_date=getdate()
		where systemname=@systemname
		and   systemtype=@systemtype
	else
		INSERT ST_System_Attributes values (@systemname, @systemtype, @attribute, @value, getdate())
end
go
