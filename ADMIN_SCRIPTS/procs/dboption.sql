/* Procedure copyright(c) 2008 by Edward M Barlow */

/******************************************************************************
**
** Name        : sp__dboption.sp
**
******************************************************************************/
:r database
go
:r dumpdb
go

if exists (select * from sysobjects
           where  name = "sp__dboption"
           and    type = "P")
   drop proc sp__dboption
go
create proc sp__dboption(
@dbname varchar(30) = NULL,             /* database name to change */
@optname varchar(20) = NULL,            /* option name to turn on/off */
@optvalue varchar(10) = NULL,           /* true or false */
@dockpt tinyint = 1                     /*
                                        ** 0 indicates don't run checkpoint
                                        ** else run checkpoint automatically
                                        */
AS
BEGIN

	if @dbname is not null and
	not exist ( select * from master..sysdatabases where name=@dbname )
	begin
		select @dbname=@optname, @optname=@dbname
	end

	if @dbname is not null
		use master

	exec sp_dboption @dbname,@optname,@optvalue,@dockpt

	if @dbname is not null
		checkpoint @dbname

	return(0)

END
go
grant execute on sp__dboption  TO public
go
