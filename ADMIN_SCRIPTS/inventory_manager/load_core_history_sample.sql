drop table #SystemInfo
drop table #AggregateCoreInfo
drop table #History
xxx

DROP TABLE #History
CREATE TABLE  #History
(
 contact         varchar(255)          NOT NULL,
 location        varchar(255)          NULL,
 cores_12312009        int null,
 cores_03312010        int null,
 cores_06302010        int null,
 cores_09302010        int null,
 cores_12312010        int null
)
select top 100 * from INV_core_history
insert #History (contact,location) select distinct contact,location from INV_core_history
update #History set cores_12312009 = num_cores from #History h,INV_core_history h2 where h2.contact=h.contact and h2.location=h.location and h2.audit_date='12/31/2009'
update #History set cores_03312010 = num_cores from #History h,INV_core_history h2 where h2.contact=h.contact and h2.location=h.location and h2.audit_date='03/31/2010'
update #History set cores_06302010 = num_cores from #History h,INV_core_history h2 where h2.contact=h.contact and h2.location=h.location and h2.audit_date='06/30/2010'
update #History set cores_09302010 = num_cores from #History h,INV_core_history h2 where h2.contact=h.contact and h2.location=h.location and h2.audit_date='09/30/2010'
update #History set cores_12312010 = num_cores from #History h,INV_core_history h2 where h2.contact=h.contact and h2.location=h.location and h2.audit_date='12/31/2010'

select * from #History

select distinct convert(varchar,audit_date,101) from INV_core_history where is_quarter_end='Y' order by audit_date

select  distinct
	c.business_unit ,
 	c.display_name,
 	c.account,
 	sum(h.num_cores),
 	sum(cores_12312009),
 	sum(cores_03312010),
 	sum(cores_06302010),
 	sum(cores_09302010),
 	sum(cores_12312010)
from #AggregateCoreInfo h, INV_contact c, #History hi
where h.contact = c.contact  and isnull(c.business_unit,'')!='' and hi.contact=h.contact
group by c.business_unit, c.display_name,	c.account
having h.contact = c.contact
order by business_unit

