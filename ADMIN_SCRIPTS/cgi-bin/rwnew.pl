#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(. lib);

# set development_mode to 2 for code_section messages!
my($development_mode);
my($production_mode);
my($diagnose_mode);				# diagnose the table layout

my($COMPANYNAME)="Millenium Partner\'s";
my($starttime)=time;

############ MODIFICATION INSTRUCTIONS FOR YOUR WEB SERVER  ###########
# This program needs to be modified by YOU... yes you...
# It should run from the command line so
#
#   a) look at line #1 - the hashbang - make sure it is your perl
#   b) look at line #3 - the use lib line... make sure it points to the
#      correct location for the GEM perl libraries.  If you are on a system
#      where these libs do not exist, copy the whole tree locally.
#   c) the begin block starting on line #9 should correctly set your SYBASE
#      environment variable.  I have multiple lines in an if/then/else
#      because i run this code on multiple machines
#
############# DONT MODIFY BELOW THIS LINE ###################

use strict;
use MlpAlarm;
use CommonFunc;
use Do_Time;
use DBIFunc;
use CGI qw( :standard escapeHTML escape);
use CGI::Carp qw(fatalsToBrowser);
# use File::Basename;

my($header_printed);    # defined if we have printed the cgi header
my(@saved_messages);    # screen lines that should have been printed
								# usually debug messages but couldnt be printed
								# because header() has not yet been called.

# $|=1;

# remove error 9558 - char length 2 long msg that occurs due to limits between
# database varchar(255) (plus null) and perl able only to handle 255 bytes
# per column.
DBIFunc::dbi_msg_exclude(9558);

my($dflt_system)  	= "[ Select System ]";
my($dflt_systemtype) = "[ Select System Type ]";
my($dflt_program) 	= "[ Select Program ]";
my($dflt_user) 		= "[ Select User ]";
my($dflt_group) 		= "[ Select Group ]";

#
# For Developer Prints
#
my($i_am_able_to_understand_sql)='?';
my($dev_diag_str)='';				# populated via developer_diag

#
# varkeys %PARGS will be translated to variables directly...
#    The following variables are named for their keys %PARGS exact name match
#
my($debug);					# From the Debug Checkbox
my($admin) = param("admin") || param("REPORTNAME") || param("sv_admin") ||param("svsv_admin");

my($LOGON_USER)=GetLogonUser();

my(@allsysD,@allsysH,@allsys,@allprg, @containers, @all_ldap_grp, @all_ldap_usr);	# saved values

# CHANGE ALL param() TO PARGS
my(%PARGS);
$PARGS{"DIAGNOSE"}=1 if $diagnose_mode;

if( ! $admin ) {
	my_Header();
	my_Nav_Bar();
}


my($prstring)='';
my $key = localtime(time);

$key .= " ".$LOGON_USER." ";

my($cnt)=0;
foreach ( param() ) {
	$cnt++;
	if( /multiparam_/ ) {
		$PARGS{$_} = join("|",param($_)); 	# checkboxes should be named this
	} else {
		$PARGS{$_} = param($_) if param($_) !~ /^\[/
			or $_ eq 'filter_credentials' 	# the like filters must be set...
			or $_ eq 'filter_likefilter'
			or $_ eq 'filter_num_days';		
	}
	$prstring .= " param($_) => ".param($_).br if /^filter/;
	if( $_ =~ /^filter_/ or $_ eq "REPORTNAME"
		or ( /^add_/ and param($_) !~ /^\[/ and param($_) !~ /^\s*$/ ) ) {
		param(-name=>$_,-value=>$PARGS{$_}) if $_ =~ /^filter/;				# fixed duplication bug
	} else {
		Delete($_) unless param("NOARGFETCH");	# if /^sv_\d+_/ or /^dat_\d+_/; }
	}
}

if( ! $admin ) {		# woah... no page
		my_Header();
		my_Nav_Bar();

		foreach ( keys %PARGS ) { print "PARG: $_ => $PARGS{$_}",br; }
		foreach ( param() ) { print "param: $_ => param($_)",br; }

		print "<center>",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;";
		print "<b><h1>WELCOME TO THE ENTERPRISE DASHBOARD</h1></b>",p;

		my($rc)=MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>1);
		if( $rc ne "Y" ) {
			my($str)=GetLogonUser().": ACCESS DENIED".br."Please Contact Support".br;
			my(@rc)=MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0);
			foreach( @rc ) {
				$str.="Ok: $_".br;
			}
			$str.="You have NO permissions".p if $#rc<0;
			print "<center><FONT face=sans-serif size=+1 color=red>$str</FONT></center>\n";
			errorout('');
		}

		if( $PARGS{'permission'} ) {
				print "simulated permission: ".i($PARGS{'permission'}),br;
		} else {
			foreach( MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0) ) {
				print "You have permission ",i($_),br unless /^Dflt~([a-zA-Z_]+):(.+)/;
			}
		}

		print p,"&nbsp;",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;";
		print "Authenticated as ".$LOGON_USER,p,"</center>";
		my_end_form();
		print end_html;
		exit(0);
}

# This means that if REPORTNAME param is passed, it GRABs & SETs
# THE PARAMETERS FROM THE DATABASE

my_Header();

if( defined $PARGS{"REPORTNAME"} ) {
	debugmsg( "RUNNING REPORT ".$PARGS{"REPORTNAME"});

	my($r)=$PARGS{"REPORTNAME"}||'';
	my(%x)=MlpReportArgs(-reportname=>$r);
	my($prstring)='';
	foreach (sort keys %x) {
		next if defined $PARGS{$_};
		next if $_ eq "NONAVIGATION" and ! $x{$_};

		if( /^HEADER/ or /^TITLE/ )		{
			$prstring .= " => Overriding Argument $_ (not showing value)".br;
		} else {
			$prstring .= " => Overriding Argument $_ With $x{$_}".br;
		}
		$PARGS{$_} = $x{$_};
		$admin = $x{$_} if /^ADMIN$/i;
	}
	code_section("DONE REPLACING ARGUMENTS FROM ReportArgs for ".$PARGS{REPORTNAME}.br.$prstring,__LINE__);
}
debugmsg("REPORT Called In Diagnostic MODE") if defined $PARGS{'debug'} or $PARGS{'DEBUG'};
code_section("DONE REMOVING DEFAULT ARGUMENTS FROM CONSIDERATION",__LINE__);

$debug=1 	if defined $PARGS{'debug'} or $PARGS{'DEBUG'};

code_section("Checking Your Permissions",__LINE__);
errorout("FATAL ERROR - LOGON_USER IS UNDEFINED") unless $LOGON_USER;
my($rc)=MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>1);
if( $rc ne "Y" ) {
	print errorbox("ERR".__LINE__.": ACCESS DENIED - YOU HAVE AUTHENTICATED AS ".GetLogonUser(),
		br,"PLEASE CONTACT SUPPORT");
	errorout("FATAL ERROR");
}

my(%dashboard_permissions);
if( $PARGS{'permission'} ) {
		$dashboard_permissions{$PARGS{'permission'}}=1;
		print "set role: ".i($PARGS{'permission'}),br;
} else {
	foreach( MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>0) ) {
		$dashboard_permissions{$_}=1;
	}
}

if( ! $development_mode or
 ( ! defined $dashboard_permissions{'DASHBOARD ADMINISTRATOR'} and ! $PARGS{'permission'} )) {
	print "<FONT SIZE=+1 COLOR=RED>TURNING OFF DEVELOPMENT MODE AS $LOGON_USER DOES NOT HAVE DASHBOARD ADMINISTRATOR</FONT>",br
		if $development_mode;
	$debug=0;
	$i_am_able_to_understand_sql='N';
} else {
	statusmsg( "<FONT COLOR=RED>Welcome DASHBOARD ADMINISTRATOR !!!</FONT>" );
	if( ! $production_mode ) {
		set_developer(1);
		$i_am_able_to_understand_sql='Y';
	}
	statusmsg( $dev_diag_str ) if $dev_diag_str;
}

if( $PARGS{EXTERNAL_CGI_BTNPRESS} ) {
	code_section("EXTERNAL CGI BTNPRESS",__LINE__);
	debugmsg( "Line=".__LINE__.": CALLING EXTERNAL CGI INLINE- ".$PARGS{EXTERNAL_CGI_BTNPRESS} );
	my($cmd)=$PARGS{EXTERNAL_CGI_BTNPRESS};
	my($codeblock)='';
	open(G,$cmd) or die "CANT READ $cmd";
	while(<G>) { $codeblock.=$_; }
	close(G);
	eval $codeblock;
	print "ERROR".$@ if $@;
}

my($tm)=do_time( -fmt=>"mm/dd/yy hh:mi" );
if( defined $PARGS{"TITLE"} ) {
	print drawbanner( b($PARGS{"TITLE"}),"", $tm );
} else {
	print drawbanner( "","", $tm ),br;
}

my_Nav_Bar();

if( $PARGS{HELPFORM} ) {	# special HELPFORM of this name
	print "HELPFORM Attribute Detected - Fetching Dynamic Help",p;
	use Inventory;
	print INV_gethtmlforms( $PARGS{ADMIN} );
	my_Page_Footer();
}

print "\n",$PARGS{"HEADERTEXT"}."\n".
	$PARGS{"HEADERTEXT1"}."\n".
	$PARGS{"HEADERTEXT2"}."\n".
	$PARGS{"HEADERTEXT3"}."\n".
	$PARGS{"HEADERTEXT4"}."\n".
	$PARGS{"HEADERTEXT5"}."\n".
	$PARGS{"HEADERTEXT6"}."\n".
	$PARGS{"HEADERTEXT7"}."\n".
	$PARGS{"HEADERTEXT8"}."\n".
	$PARGS{"HEADERTEXT9"}."\n".
	$PARGS{"HEADERTEXT10"}."\n".
	$PARGS{"HEADERTEXT11"}."\n".p
	if defined $PARGS{"HEADERTEXT"} or defined $PARGS{"HEADERTEXT1"};
	
if( $PARGS{'EXTERNAL_CGI_EVALFILE'} ) {
	code_section("EXTERNAL CGI INLINE",__LINE__);
	debugmsg( "Line=".__LINE__.": CALLING EXTERNAL CGI INLINE- ".$PARGS{'EXTERNAL_CGI_EVALFILE'} );
	my($cmd)=$PARGS{'EXTERNAL_CGI_EVALFILE'};
	my($codeblock)='';
	open(G,$cmd) or die "CANT READ $cmd";
	while(<G>) { $codeblock.=$_; }
	close(G);
	use DBI;
	my($xx)="THIS IS A TEST PRINT FROM AN EVAL";
	eval $codeblock;
	print "ERROR".$@ if $@;
	
#
# RUN TYPE 2 - system() call using inerpolated parameters
# that means any p(x) get replaced by $PARGS{x}
#
} elsif( $PARGS{'EXTERNAL_CGI_SCRIPT'} ) {
	code_section("EXTERNAL CGI",__LINE__);
	debugmsg( "Line=".__LINE__.": CALLING EXTERNAL CGI - ".$PARGS{'EXTERNAL_CGI_SCRIPT'} );
	my($cmd)=$PARGS{'EXTERNAL_CGI_SCRIPT'};

	foreach ( keys %PARGS ) {
		next unless $cmd =~ /p\($_\)/;
		my($v)=$PARGS{$_};
		$cmd =~ s/p\($_\)/$v/;
	}
	if( $cmd =~ /p\((.+)\)/ ) {
		print errorbox("ERR".__LINE__.": EXTERNAL CGI: THE ".b($PARGS{'TITLE'})." REPORT REQUIRES ARGUMENTS",br,"PLEASE SELECT APPROPRIATE ARGUMENTS AT THE TOP OF THE SCREEN</FONT>\n");
	} else {
		developer_diag($cmd);
		system($cmd);
	}	
}
my_Page_Footer();

###############################################################################
###########                END PAGE - START SUBROUTINES          ##############
###############################################################################
sub query_print {
	print "<FONT COLOR=blue SIZE=-2>",@_,"</FONT><br>\n";
}
sub developer_diag {
	if( $i_am_able_to_understand_sql eq "Y" ) {
		print "<FONT COLOR=GREEN SIZE=-1><PRE>",@_,"</PRE></FONT>";
	} elsif( $i_am_able_to_understand_sql eq "?" ) {
		$dev_diag_str.="<FONT COLOR=BLUE><PRE>".join('',@_)."</PRE></FONT>\n";
	}
}

sub my_Nav_Bar {
	return if defined param("NOHEADER") or $PARGS{NOHEADER};
	if( $PARGS{NAVFILE} ) {
		code_section("NavFile Found",__LINE__);
		foreach ( param() ) { print "DBGDBG my_Nav_Bar param(): $_ => ".param($_).br; }
		my($navbar)='';
		open(F,$PARGS{NAVFILE}) or die "CANT READ NAVFILE=$PARGS{NAVFILE}";
		while(<F>) { $navbar.=$_; }
		close(F);
		print $navbar;
		return;
	}
	code_section("NAVBAR Found - ".$PARGS{"NAVBAR"},__LINE__);
	my($SAVECHANGESFOUND);
	if( $PARGS{"NAVBAR"} eq "SA" ) {	# special case - dont draw a blank table
			$SAVECHANGESFOUND = 1;
	} elsif( $PARGS{"NAVBAR"} and $PARGS{"NAVBAR"} !~ /^\s*$/ and ! $PARGS{"NONAVIGATION"} ) {
		debugmsg( "Line=".__LINE__." NAVBAR defined - adding dashboard NAV bar ".$PARGS{"NAVBAR"});
		my($SAVE_CHANGES_EXISTS)="";

		my(@CHARARY)=split(//,$PARGS{"NAVBAR"});
		print "<TABLE BGCOLOR=#f0e9d9 WIDTH=100% BORDER=1><TR><TD><TABLE WIDTH=95% BORDER=0><TR>\n";
		my($CELLCOUNT)=0;
		while ( $#CHARARY>0 ) {
			my($ctype)=$CHARARY[0].$CHARARY[1];
			shift @CHARARY;
			shift @CHARARY;
			if( $ctype eq "SA" ) {
				#print "DBG: SAVE CHANGES FOUND",br;
				$SAVECHANGESFOUND=1;
				next;
			}
			$CELLCOUNT++;
			print "</TR><TR>" if $CELLCOUNT==5;
			print "<TD WIDTH=25%><center>";

			if( $ctype eq "RR" ) {
				print submit(-name=>"submit",-value=>"Run Report");
			} elsif( $ctype eq "RE" ) {
				print submit(-name=>"submit",-value=>"Reset");
			} elsif( $ctype eq "SV" ) {
				my(@x)=MlpGetSeverity();
				# severity is kind of an oddball case cause you must have one... so i chose errors for default...
				$PARGS{filter_severity}='Errors' unless $PARGS{filter_severity} ;
				$PARGS{filter_severity}='Errors' if $PARGS{filter_severity} eq 'Ok';	# sillycodearound
				print "Severity: ",br.popup_menu( -name=>'filter_severity', -values=>\@x, -default=>$PARGS{filter_severity} );
			} elsif( $ctype eq "OE" ) {
				my(@x)=('ALL','OK','FAILED','RUNNING','NEVER_RUN');
				$PARGS{filter_severity}='FAILED' unless $PARGS{filter_severity};
				print "Severity: ",br.popup_menu( -name=>'filter_severity', -values=>\@x,
					-default=>$PARGS{filter_severity} );
			} elsif( $ctype eq "OX" ) {
				my(@x)=('ALL','ERROR');
				$PARGS{filter_severity}='ERROR' unless $PARGS{filter_severity};
				print "Severity: ",br.popup_menu( -name=>'filter_severity', -values=>\@x,
					-default=>$PARGS{filter_severity} );
			} elsif( $ctype eq "PO" ) {
				if( defined $PARGS{'filter_prodchkbox'} ) {
					print checkbox( -name=>'filter_prodchkbox', -value=>'Production Only', -checked=>1, -label=>"Production Only" );
				} else {
					print checkbox( -name=>'filter_prodchkbox', -value=>'Production Only', -checked=>0, -label=>"Production Only" );
				}
			} elsif( $ctype eq "TI" ) {
				my(@timevalues)=("4 Hours","8 Hours","Since 4PM","1 Day","3 Days", "5 Days","14 Days","30 Days");
				print "Time: ",br.popup_menu( -name=>'filter_time', -values=> \@timevalues, -default=>"1 Day" ),"</TD>";
			} elsif( $ctype eq "BS" ) {
				#my(@containers)=MlpGetContainer( -type=>'c') if $#containers<0;
				my(@containers)=MlpGetBusiness_System(-type=>$PARGS{REPORTKEYWORD});
				print errorbox( "ERR".__LINE__.": NO BUSINESS SYSTEMS FOUND\n" ) if $#containers<0;
				unshift @containers,$dflt_system;
#					# WHAT ARE YOUR PERMISSIONS... IF ONLY FOR 1 SYSTEM THEN DEFAULT THIS
#					# if you pass in a default system, set it, otherwise retrieve it!
				my($d)=$PARGS{filter_container} || $dflt_system;
#					print "\nD is $d FC=",param('filter_container')," Arg=",$PARGS{'filter_container'},"\n";
				Delete('filter_container');
				print "Business System: ",br.
					popup_menu( -name=>'filter_container', -default=>$d, -values=>\@containers );	#
			} elsif( $ctype eq "DE" ) {
				my(@dpts)= MlpGetDepartment();
				unshift @dpts,"All";
				my($dflt)=$PARGS{'filter_department'} || $dpts[0];
				print "Department: ".br.popup_menu( -name=>'filter_department', -values=>\@dpts, -default=>$dflt);
			} elsif( $ctype eq "MA" ) {
				#
				# MATCH STATUS IS SEPARATE - ONLY WORKS IF IT EXISTS
				#
				if( $PARGS{'filter_matchstatus'} ) {
					my(@rc)= ("OK","INVESTIGATE","UNUSEABLE","IT ADMIN","SYSADM","SERVICE","UNMAPPED","ALL");
					my($dflt)=$PARGS{'filter_matchstatus'} || $rc[0];
					print "Match: ".br.popup_menu( -name=>'filter_matchstatus', -values=>\@rc, -default=>$dflt);
				}
			} elsif( $ctype eq "SY" ) {
				@allsys= MlpGetSystem() if $#allsys==-1;
				my($dflt) = $dflt_system;
				unshift @allsys, $dflt_system;
				$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
				print "System: ".br.popup_menu( -name=>'filter_system', -values=>\@allsys, -default=>$dflt);
			} elsif( $ctype eq "SD" ) {
				@allsysD= MlpGetSystem(-type=>'Database') if $#allsysD==-1;
				my($dflt) = $dflt_system;
				unshift @allsysD, $dflt_system;
				$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
				print "Database: ".br.popup_menu( -name=>'filter_system', -values=>\@allsysD, -default=>$dflt);
			} elsif( $ctype eq "Sa" ) {
				@allsysD= MlpGetSystem(-type=>'ApproveRqdDatabase') if $#allsysD==-1;
				my($dflt) = $dflt_system;
				unshift @allsysD, $dflt_system;
				$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
				print "Database: ".br.popup_menu( -name=>'filter_system', -values=>\@allsysD, -default=>$dflt);
			} elsif( $ctype eq "SH" ) {
				if( $#allsysH==-1 ) {
					my($q)="select distinct system_name, system_type from INV_principal_map where system_type not in ('sqlsvr','oracle','sybase') order by system_name";
					foreach ( _querywithresults( $q,1) ) {
						my(@x)=dbi_decode_row($_);
						push @allsysH,$x[0];
					}
				}
				my($dflt) = $dflt_system;
				unshift @allsysH, $dflt_system;
				$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
				print "Sas70 Hostname: ".br.popup_menu( -name=>'filter_system', -values=>\@allsysH, -default=>$dflt);
			} elsif( $ctype eq "DB" ) {
				my(@aa)=("sqlsvr","sybase","mysql","oracle");
				my(%lbls) = (
					"sqlsvr" => "Microsoft Sql Server",
					"sybase" => "Sybase Sql Server",
					"mysql" => "Mysql Database",
					"oracle" => "Oracle Database"
				);
				unshift @aa,$dflt_systemtype;
				my($dflt)=$PARGS{filter_database} || $dflt_systemtype;
				print "Database: ".br.popup_menu( -name=>'filter_database', -labels=>\%lbls, -values=>\@aa, -default=>$dflt);
			} elsif( $ctype eq "HO" ) {
				my(@aa)=("win32servers","unix");
				my(%lbls) = (
				"win32servers" => "Windows Servers",
				"unix" => "Unix Servers"
				);

				unshift @aa,$dflt_systemtype;
				my($dflt)=$PARGS{filter_system_type} || $dflt_systemtype;
				print "System Type: ".br.popup_menu( -name=>'filter_hostname', -labels=>\%lbls, -values=>\@aa, -default=>$dflt);
			} elsif( $ctype eq "ST" ) {
				my(@filter_system_types)=MlpGetLookupInfo('SYSTEM_TYPE');
				unshift @filter_system_types,$dflt_systemtype;
				my($dflt)=$PARGS{filter_system_type} || $dflt_systemtype;
				#print "DEFAULT=$dflt";
				#print "filter_system_types=",join(" ",@filter_system_types),br;
				print "System Type: ".br.popup_menu( -name=>'filter_system_type', -values=>\@filter_system_types, -labels=> -default=>$dflt);
			} elsif( $ctype eq "AS" ) {
				my(@filter_system_types)=("[Choose Permission]","APPROVER","DASHBOARD POWERUSER");
				print "User Type: ".br.popup_menu( -name=>'filter_userpermission', -values=>\@filter_system_types, -default=>$filter_system_types[0]);
			} elsif( $ctype eq "PR" ) {
				@allprg= MlpGetProgram() if $#allprg == -1;
				my($dflt) = $dflt_program;
				unshift @allprg, $dflt_program;
				$dflt=$PARGS{'filter_program_name'} if defined  $PARGS{'filter_program_name'} and $PARGS{'filter_program_name'} ne $dflt_program;
				print "Program: ".br.popup_menu( -name=>'filter_program_name', -values=>\@allprg, -default=>$dflt);
			} elsif( $ctype eq "CE" ) {
				print "&nbsp;";		# cant certity now here - its above
			} elsif( $ctype eq "__" ) {
				print "&nbsp;";
			} elsif( $ctype eq "AL" ) {
				print checkbox( -name=>'ShowAll', -value=>'Show All Groups/Users', -label=>"Show All Groups/Users" );
			} elsif( $ctype eq "LF" ) {
				my($d)=$PARGS{filter_likefilter};
				$d = '' if $d=~/^\s*$/;
				print 'Server Like Filter:',br,textfield(-name=>"filter_likefilter", -default=>$d,-size=>20, -maxlength=>100);
			} elsif( $ctype eq "DL" ) {
				my($d)=$PARGS{filter_showdeleted};
				$d = 'YES' if $d=~/^\s*$/;
				my(@yn)=("YES","NO");
				print "Show Deleted/Locked/Expired: ",br.popup_menu( -name=>'filter_showdeleted', -values=> \@yn, -default=>$d ),"</TD>";
			} elsif( $ctype eq "CF" ) {
				my($d)=$PARGS{filter_credentials};
				$d = '' if $d=~/^\s*$/;
				print 'Credential Like Filter:',br,textfield(-name=>"filter_credentials", -default=>$d,-size=>20, -maxlength=>100);
			} elsif( $ctype eq "LU" ) {
				@all_ldap_usr= MlpGetPrincipal(-type=>'ldap user',-displayable=>'Y') if $#all_ldap_usr == -1;
				my($dflt) = $dflt_user;
				unshift @all_ldap_usr, $dflt_user;
				print "Ldap User: ".br.popup_menu( -name=>'filter_ldap_user', -values=>\@all_ldap_usr, -default=>$dflt_user);
			} elsif( $ctype eq "SU" ) {
				my(@d);
				my($q)="select distinct principal_name  from INV_principal_map order by principal_name";
				foreach ( _querywithresults( $q,1) ) {
					my(@x)=dbi_decode_row($_);
					push @d,$x[0];
				}
				my($dflt) = $dflt_user;
				unshift @d, $dflt_user;
				print "SAS70 Ldap User: ".br.popup_menu( -name=>'filter_ldap_user', -values=>\@d, -default=>$dflt_user);
			} elsif( $ctype eq "LG" ) {
				@all_ldap_grp= MlpGetPrincipal(-type=>'ldap group',-displayable=>'Y') if $#all_ldap_grp == -1;
				my($dflt) = $dflt_user;
				unshift @all_ldap_grp, $dflt_user;
				print "Ldap Group: ".br.popup_menu( -name=>'filter_ldap_group', -values=>\@all_ldap_grp, -default=>$dflt_user);
			} elsif( $ctype eq "SG" ) {
				my(@d);
				my($q)="select distinct department  from INV_cert_department order by department";
				foreach ( _querywithresults( $q,1) ) {
					my(@x)=dbi_decode_row($_);
					push @d,$x[0];
				}
				my($dflt) = $dflt_user;
				unshift @d, $dflt_user;
				print "Sas70 Ldap Group: ".br.popup_menu( -name=>'filter_ldap_group', -values=>\@d, -default=>$dflt_user);

			} elsif( $ctype eq "AP" ) {

				#my(@ap)= ('Approved Accounts','Removed Accounts','Unknown Status',"All Accounts");
				my(@ap)= qw(ALL APPROVED REMOVE UNUSEABLE UNAPPROVED UNMAPPED SYSADMIN SERVICEACCOUNT);
				print "Approval Status:<br>".popup_menu( -name=>'filter_approve', -values=>\@ap,
					-default=>"ALL");
			} elsif( $ctype eq "PL" ) {

				# get the policies
				my($q)= "select distinct policy_name from INV_policy_defn ";
				$q   .= " where policy_name like '".$PARGS{AUDTYP}."_%'";
				$q   .= " order by policy_name";
				my(@pol_list) = ("[ Select Policy ]");
				foreach ( _querywithresults( $q,1) ) {
					my(@x)=dbi_decode_row($_);
					push @pol_list,$x[0];
				}
				my($dflt)= $PARGS{filter_audit_policy} || $pol_list[0];
				print "Audit Policy: ".br.popup_menu( -name=>'filter_audit_policy', -values=>\@pol_list,	-default=>$dflt);

			} elsif( $ctype eq "At" ) {
				my(@ap)= ('All Accounts','Service Accounts','Unmapped Accounts','Mapped Accounts',"Unuseable Accounts","Error Accounts","LDAP User","LDAP Group");
				print "Account Status: ".popup_menu( -name=>'filter_container', -values=>\@ap,
					-default=>"All Accounts");
			} else {
				print "&nbsp;";
			}
			print "</center></TD>";
		}
		if( $PARGS{NAVLABEL} ) {
			die "Must have NAVLISTBOX if NAVLABEL" unless $PARGS{NAVLISTBOX};
			die "Must have NAVVARNAME if NAVLABEL" unless $PARGS{NAVVARNAME};
			my(@datary);
			foreach ( _querywithresults($PARGS{NAVLISTBOX},1) ) {
				push @datary, dbi_decode_row($_);
			}

			$CELLCOUNT++;
			print "</TR><TR>" if $CELLCOUNT==5;
			print "<TD WIDTH=25%><center>";
			print $PARGS{NAVLABEL}.": ".br.popup_menu( -name=>$PARGS{NAVVARNAME}, -values=>\@datary, -default=>$datary[0]);
			print "</center></TD>";
		}
		while ( $CELLCOUNT++ % 4 != 0 ) { print "<TD>&nbsp;</TD>"; }
	
		print "</TABLE></TD></TR></TABLE>",br;

	} elsif( $PARGS{"NAVBAR"} ) {
		debugmsg("Line ".__LINE__.": NONAVIGATION DEFINED...");
	} else {
		debugmsg("Line ".__LINE__.": NO NAVBAR WAS DEFINED...");
	}
	print drawbanner( submit(-name=>"Save Changes"),'','Saves Changed Report Data') if $SAVECHANGESFOUND;
}
sub code_section {
	my($msg,$line)=@_;
	return if $development_mode!=2;

	$msg = "Line=".$line.": Time=".(time-$starttime).' '.$msg;
	if( $i_am_able_to_understand_sql eq "Y" ) {
		print "<FONT COLOR=BLUE>$msg</FONT><br>\n";
	} elsif( $i_am_able_to_understand_sql eq "?" ) {
		$dev_diag_str.="<FONT COLOR=BLUE>$msg</FONT><br>\n";
	}
}
sub my_Header {
	return if defined param("NOHEADER") or $PARGS{NOHEADER} or $header_printed;
	code_section("PAGE HEADER PRINTED",__LINE__);

	if( defined $PARGS{"REPORTNAME"} ) {
		print header,start_html(-title=>$COMPANYNAME.' - '.$PARGS{"REPORTNAME"},
			-style=>'styles/main.css', -style=>'styles/Customer.css'
			);
	} else {
		print header, start_html(-title=>$COMPANYNAME,
			-style=>'styles/main.css', -style=>'styles/Customer.css');
	}
	$header_printed=1;
	foreach ( @saved_messages ) { print $_; }
	print start_multipart_form();  
	print hidden( -name=>"svsv_admin", -default=>$admin ),"\n"	if defined $admin;
}
sub my_Page_Footer {
	if( $PARGS{"NONAVIGATION"} ) {
		Delete("NONAVIGATION");
		print drawbanner(	"&nbsp;",'',$tm );
	} else {
		print drawbanner(	"Authenticated as ".$LOGON_USER, '', $tm );
	}
	my_end_form();
	print end_html;
	exit(0);
}
sub drawbanner {
	my($left,$center,@right)=@_;
	my($outstr);
	$outstr=   "<TABLE WIDTH=100% BGCOLOR=#f0e9d9 BORDER=1><TR class='menuheader'><TD><TABLE WIDTH=100%>
	  <TR class='menuheader'><TD ALIGN=LEFT><FONT face=sans-serif size=+1 color=black>$left</FONT></TD>\n";
	$outstr.=   "<TD ALIGN=CENTER><FONT face=sans-serif size=+1 color=black>$center</FONT></TD>\n" if $center ne "";
	foreach ( @right ) {
		$outstr.=   "<TD ALIGN=RIGHT><FONT face=sans-serif size=+1 color=black>$_</FONT></TD>\n" if $_ ne "";
	}
	$outstr.=   " </TR></TABLE></TD></TR></TABLE>\n";
	return $outstr;
}
sub errorbox {
	my($msg)=join('',@_);
	return "<TABLE BORDER=1 WIDTH=100% BGCOLOR=WHITE><TR ><TD ALIGN=LEFT><FONT face=sans-serif size=+1 color=red>@_</FONT></TD>\n</TR></TABLE>\n";
}
sub errorout {
	my($msg)=join('',@_);
	print errorbox($msg),p if $msg;
	# "<FONT COLOR=RED>***An Error Was Encountered***</FONT>"
	if( $PARGS{"TITLE"} ) {
		print drawbanner( b($PARGS{"TITLE"}),'',$tm, );
	} else {
		print drawbanner( "Alarm Reporting Screen",'',$tm, );
	}
	my_end_form();
	print end_html;
	exit(0);
}
sub diagnose {
	my($msg)=join("",@_);
	return unless defined $PARGS{"DIAGNOSE"};
	if( $header_printed ) {
		print "<FONT COLOR=purple SIZE=-2>$msg</FONT><br>\n";
	} else {
		push @saved_messages, "<FONT COLOR=purple SIZE=-2>$msg</FONT><br>\n";
	}
}

sub statusmsg {
	my($msg)=join("",@_);
	if( $header_printed ) {
		print "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	} else {
		push @saved_messages, "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	}
}

sub debugmsg {
	return unless $debug;
	my($msg)=join("",@_);
	if( $header_printed ) {
		print "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	} else {
		push @saved_messages, "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	}
}

sub diag_show	{	# generic
	my($line,$exclude,$title,$title2,%hash)=@_;
	return unless $development_mode;
	my_Header();
	my($argstr)='<TABLE WIDTH=100% BORDER=1><TR><TD COLSPAN=3 BGCOLOR=PEACH>LINE='.$line.
		" ".$title.$title2;
	$argstr.="</TD></TR><TR>";
	my($argstrlong)='';
	my($cnt)=0;
	foreach ( sort keys %hash ) {
		next if /^HEADER/ or /^TITLE/;
		if( $exclude ) {
			next if /^DEBUG/ or /sv_/ or /dat_/;
		}
		# print __LINE__," $_ => $hash{$_}",br if /^sv/;

		if( length($hash{$_}) > 30 ) {
			$argstrlong.= "<TR><TD COLSPAN=3>$_ => <FONT COLOR=RED>$hash{$_}</FONT> </TD></TR>";
		} else {
			$argstr.= "<TD>Arg $_ => <FONT COLOR=RED>$hash{$_} </FONT></TD>";
			$cnt++;
			$argstr .= "</TR><TR>" if $cnt%3 == 0;
		}
	}
	$argstr .= "</TR>".$argstrlong."</TABLE>";
	print "<FONT SIZE=-1 COLOR=BLUE>",$argstr,"</FONT>";
}
sub diag_showparam {
	return unless $development_mode;
	my($line,$exclude)=@_;
	my($t2)="Also Skips dat_, sv_ " if $exclude;
	return diag_show($line,$exclude,
		"SQL PRINT FOR DASHBOARD ADMINISTRATOR $LOGON_USER<br>Skips HEADER, TITLE, DEBUG",
		$t2,%PARGS);
}

sub my_end_form {
	print hidden( -name=>"sv_admin", -default=>$admin ),"\n"							if defined $admin;
	print hidden( -name=>"REPORTNAME", -default=>$PARGS{'REPORTNAME'} ),"\n"	if defined $PARGS{'REPORTNAME'};
	foreach ( keys %PARGS ) {
		print hidden( -name=>$_, -default=>$PARGS{$_} ),"\n"	if /^filter_/ and $PARGS{$_} and $PARGS{$_} !~ /^\[/;
	}
	print end_form(),p;
}

__END__

=head1 NAME

reportwriter.pl - web browser based generic report framework

=head2 SET UP

The scripts in ADMIN_SCRIPTS/cgi-bin must be copied to a script executable
web server directory as per the CGI installation instructions.
These installation instructions can be found in the post-install tasks section of the installation guide.

=head2 PARAMETERS

These reports use parameters to determine what to do.  These parameters can come from the ReportArgs
table in the database.  This table can be loaded from MimiReportData.dat using the load*.pl script

The following cgi parameters are used by reportwriter.pl

REPORTNAME - used to extract predefined values from the database
           - often the key to everything

NAVBAR			- navbar see later

TITLE				- title of the report

DEBUG		  		- diagnostic mode

HEADERTEXT		- html header for report
HEADERTEXT[1-9]- additional html header for report

NONAVIGATION	- printed report - no buttons or navigation items

ADMIN			   - always saved in sv_admin - default to REPORTNAME if not defined
					- this is the name of the main report for the api

=head2 Header Rows

Header rows repeat based on argument HDR_ROW_REPEATS

Large forms can be avoided using HDR_FORM_REPEATS which repeats the form every 10 rows.  You can also
use HDR_FORM_FIELD1 which repeats every time field1 changes.

These arguments are mutually exclusive

=head2 Form Text

The forms are generated by the following variables

	EXTERNAL_CGI_EVALFILE	= evals a block of code stored in this file
	EXTERNAL_CGI_SCRIPT			= runs this command with command substitution
	MlpRunScreen()			= runs this command if the above two variables are undefined

=head2 BUILTIN ADDS and DELETES

Certain params() will result in predefined actions being performed

The main two are arguments named add_(\d+) and del_(\d+)

These represent ADD or DELETE buttons pressed on the form. The digits represent the rowid
to be processed.

if p(add_(\d+)_XXX) or p(del_(\d+)_XXX) then the button add (or something similar) has been pushed -
the row values were sent with hidden fields
so the syntax is MlpManageData( Admin->p(sv_admin), Operation=add|del,
and XXX=p($_).  del_(\d_+)_XXX similarly works.
That function returns html which is printed in <PRE> block.

The above is automatically created using the built in reports

=head2 ACTION ON IMPLEMENTATION

 if Save Changes {
	dat_ fields only come from Chkbox_*
	if there is a dat_(\d+)_XXX matching a sv_(\d+)_XXX that has changed...
	we must do an insert... using MlpManageData (operation=XXX, Value=>newvalue, With all other sv_items on the row passed.
	This must be individually coded
	see setignore, setproducton, and setblackout
 } else {
	my($rc)=MlpRunScreen( -screen_name=>$admin, -html=>1, -debug=>$debug,
			-severity		=>	param('filter_severity'),
			-production =>	param('filter_prodchkbox'),
			-container	=>	param('filter_container'),
			-system			=>	param('filter_system'),
			-program		=>	param('filter_program_name')
		);
 }

=head2 NAVIGATION BAR

The navigation bar is set based on varibles NAVFILE (a file name read verbatim) or NAVBAR.
If the argument NONAVIGATION is provided, no NAVBAR will be shown.
NAVBAR is composed of a sequence of 2 character KEYCODES AS FOLLOWS

	RR - Run Report Button
	RE	- Reset Button
	SV - SEVERITY
	OE - OK/ERROR
	PO - PRODUCTION ONLY CHECKBOX

	TI - TIME FILTER
	BS - BUSINESS SYSTEM
	SY - SYSTEM
	SD - DATABASES
	Sa - DATABASES THAT REQUIRE APPROVAL
	SH - HOSTNAMES
	ST - SYSTYPE
	AS - APPROVER vs SCRUBBER

	PR - PROGRAM
	CE - CERTIFY NOW BUTTON (special)
	__ - EMPTY
	AL - SHOW ALL GROUPS
	DY - NUMBER OF DAYS - defaults to 30
	LF - LIKE FILTER
	DL - DELETED/LOCKED/EXPIRED
	CF - CREDENTIAL LIKE FILTER
	LU - LDAP USERS
	LG - LDAP GROUPS
	AP - APPROVED vs UNAPPROVED
	At	- Type Of Unmapped Account
	DE - LDAP Department
	MA - Match Status

	HO - Host types (unix, win32)
	DB - Db Types (oracle, sybase, sqlsvr, mysql)

	SA - SAVE CHANGES BUTTON

=head2 The Operation Keyword

The first row returned by the library functions is the header.
The last field of the header row is the operation which controls everyting!
Operations can be
 	Both
	Update
	Delete
	Add
	OkDelete
	Ok

The impact of the above is as follows

Both/Update/Delete/Add all change the word to "Operation" on the header row

YN fields, identified by ^use or YN_LU or ^Is_ then cause the y or n from the field to be expanded to Yes/no

=head2 ROW HEADER values Conversion

if the row header ends in _LU$ then its a special case!
The last field is defined as the Operation and must be Update, Both, Add, Ok, OkDelete, DELETE, ChkBox_
there is a function defining lookups

=head2 NOTES

NOADD_ will not add stuff
FIRSTFIELD=COLOR IN HEADER then its the row color override
FIRSTFIELD=HYPERLINK IN HEADER then its a hyperlink... must come AFTER COLOR


insert INV_credential_notes ( credential_name, credential_type, is_ldap_group, ldap_group )
select group_name, system_type, 'Y', credential_name
from INV_group_member where group_type='GLOBALSHARED'
and credential_type='ldap group' and group_name!='hello'

insert INV_credential_notes ( credential_name, credential_type, is_ldap_user, ldap_user )
select group_name, system_type, 'Y', credential_name
from INV_group_member where group_type='GLOBALSHARED'
and credential_type='ldap user' and group_name!='hello'

=cut

