#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use lib qw(lib);

use strict;
use MlpAlarm;
use File::Basename;
my($curdir)=dirname($0);
my($DEBUG);


MlpAlarm::_querynoresults("delete ReportArgs where reportname like 'GEM_%'");
MlpAlarm::_querynoresults("delete ReportArgs where reportname like 'INV_%'");
MlpAlarm::_querynoresults("delete ReportArgs where reportname like 'DCM_WASP_%'");
open(RD,"$curdir/MimiReportData.dat") or die "Cant open MimiReportData.dat $!\n";
my(@datarows);
while( <RD> ) {
	next if /^\s*#/ or /^\s*$/;
	s/\s+$//;
	#chop;
	if( /^;/ ) {
		s/^;//;
		$datarows[$#datarows].=' '.$_;
		print $datarows[$#datarows],"\n";
	} else {
		push @datarows,$_;
	}
}
close RD;

my($old)='';
foreach (@datarows) {
	my($rptname,$var,$val)=split(/;/);
	print "$rptname -> $var -> $val \n" if $DEBUG;
	print $rptname,"\n" if $rptname ne $old;
	$old=$rptname;
	my($q);
	if( $rptname eq "DEFAULT_ARGS" ) {
		$q="insert ReportArgs ( reportname,keyname,value )
select distinct reportname,'". $var. "','". $val."'
from ReportArgs where reportname like 'GEM%' and reportname not in
 ( select reportname from ReportArgs where keyname='".$var."' )";
		#print $q,"\n";
		MlpAlarm::_querynoresults($q,1);
	} else {
		$q="insert ReportArgs ( reportname,keyname,value )
values('".$rptname."','". $var. "','". $val."')";
		#print $q,"\n";
		MlpAlarm::_querynoresults($q,1);
	}
}
