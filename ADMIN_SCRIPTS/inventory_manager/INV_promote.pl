#!/apps/perl/linux/perl-5.8.2/bin/perl

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use DBIFunc;
use Inventory;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw($BATCH_ID $DEBUG $NOUPDATE );

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID

	--NOUPDATE			[ if set - dont update tables (diagnostics) ]
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --DEBUG \n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s" => \$BATCH_ID,
		"NOUPDATE"   => \$NOUPDATE,
		"DEBUG"      => \$DEBUG );

$BATCH_ID = "InvPromote" unless $BATCH_ID;
MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Gem Monitor') if $BATCH_ID;
print "-- Running In Diag Mode\n" if $DEBUG;
$DEBUG=0 unless defined $DEBUG;
InvPromote(1-$DEBUG,$NOUPDATE);

MlpBatchJobEnd() if $BATCH_ID;
print "-- Successful Completion\n";
exit(0);

