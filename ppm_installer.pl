use strict;

print "***************************************\n";
print "* Perl Add On Package For Win32 Check *\n";
print "***************************************\n\n";
	
my($cmd)='ppm install Tk';
print "Installing Tk - $cmd\n";
open(AAA,$cmd."|") or die "Cant run $cmd $!\n";	while(<AAA>) { print "\t",$_; }	close(AAA); 

$cmd='ppm install DBI';
print "Installing DBI - $cmd\n";
open(AAA,$cmd."|") or die "Cant run $cmd $!\n";	while(<AAA>) { print "\t",$_; }	close(AAA); 

$cmd='ppm install DBD-ODBC';
print "Installing DBD-ODBC - $cmd\n";
open(AAA,$cmd."|") or die "Cant run $cmd $!\n";	while(<AAA>) { print "\t",$_; }	close(AAA); 

$cmd='ppm install Win32-DriveInfo';
print "Installing Win32::DriveInfo - $cmd\n";
open(AAA,$cmd."|") or die "Cant run $cmd $!\n";	while(<AAA>) { print "\t",$_; }	close(AAA); 

#print "Installing Win32::Taskscheduler\n";
#$cmd='ppm install http://taskscheduler.sourceforge.net/perl58/Win32-TaskScheduler.ppd';
#open(AAA,$cmd."|") or die "Cant run $cmd $!\n";	while(<AAA>) { print $_; }	close(AAA); 
print "\n";

# FROM http://taskscheduler.sourceforge.net/
if( $] >= 5.006 and $] < 5.008 ) {
	$cmd = "ppm install Win32-TaskScheduler --location=http://taskscheduler.sourceforge.net/perl/";
} elsif( $] >= 5.008 and $] < 5.01 ) {
	$cmd = "ppm install http://taskscheduler.sourceforge.net/perl58/Win32-TaskScheduler.ppd"; 
} else {
	print "***** ERROR ****** Win32::TaskScheduler has no ppm download for 5.10\n";
	print "If you have the time and skills to compile it, please do and contact us!\n";
	print "Hit Enter To Continue, CTRL-C to Exit\n";
	<STDIN>;
}

print "SUCCESSFUL run of ppm_installer.pl";