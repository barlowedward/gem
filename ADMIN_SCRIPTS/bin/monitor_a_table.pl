#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Do_Time;
use DBIFunc;
use CommonFunc;
use File::Basename;

use vars qw( $TYPE $USER $SERVER $PASSWORD $DEBUG $DATABASE $TABLE $OUTFILE $ADDDATE $ITERATIONS $SLEEPTIME);

# BEGIN { $ENV{SYBASE}="/opt/sybase" unless defined $ENV{SYBASE}; }

sub usage
{
	print @_;
	print "monitor_a_table -TABLE=tbl -DATABASE=database -OUTIFILE=filename USER=SA_USER -SERVER=SERVER --ITERATIONS=xx --SLEEPTIME=xx -PASSWORD=SA_PASS [-debug] -TYPE=Sybase|ODBC --ADDDATE\n";
   return "\n";
}

$| =1; 				# unbuffered standard output

# change directory to the directory that contains THIS file
my($curdir)=cd_home();

die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"DATABASE=s"	=> \$DATABASE,
		"TABLE=s"	=> \$TABLE,
		"OUTFILE=s"	=> \$OUTFILE,
		"SLEEPTIME=s"	=> \$SLEEPTIME,
		"ITERATIONS=s"	=> \$ITERATIONS,
		"USER=s"	=> \$USER ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"ADDDATE" 	=> \$ADDDATE ,
		"TYPE=s" 	=> \$TYPE ,
		"DEBUG"      	=> \$DEBUG );

die usage("Must pass server\n" )	unless defined $SERVER;
die usage("Must pass username\n" )	unless defined $USER;
die usage("Must pass password\n" )	unless defined $PASSWORD;
die usage("Must pass DATABASE\n" )	unless defined $DATABASE;
die usage("Must pass TABLE\n" )	unless defined $TABLE;
#die usage("Must pass OUTFILE\n" )	unless defined $OUTFILE;

$TYPE="Sybase" unless defined $TYPE;

$OUTFILE.=".".do_time(-fmt=>'yyyymmdd') if $OUTFILE and $ADDDATE;

#
# CONNECT TO THE DATABASE
#
debug( "Connecting\n" );
$TYPE="Sybase" unless defined $TYPE;
my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD,-type=>$TYPE);
die "Cant connect to $SERVER as $USER\n" if ! $rc;

my($server_type,@db) = dbi_parse_opt_D();
debug("Connected\n");

my $sql = "select rowcnt(i.doampg) from sysindexes i
			where object_name(id,db_id('".$DATABASE."'))='".$TABLE."'
			and indid=0" if $server_type eq "SYBASE";
$sql=	"select rowcnt from sysindexes where indid<=1 and object_name(id)='".$TABLE."'"
	if $server_type ne "SYBASE";

debug( "query  is $sql\n" );

debug("Getting Last Line\n");
my($lastline)="";
if( $OUTFILE and  -r $OUTFILE ) {
	open(OUT,$OUTFILE)  or die "Cant read $OUTFILE";
	while(<OUT>) {
		chomp;
		$lastline=$_ unless /^\s*$/;
	}
	close(OUT);
}
my($ltime,$rows,$incr,$rate);
if( $lastline eq "" ) {
	$rows=get_rows();
	$ltime=time;
	debug("Prefetched $rows rows from the table at $ltime\n");
	sleep($SLEEPTIME || 5 );
} else {
	($ltime,$rows,$incr,$rate)=split(/\s+/,$lastline);
	debug("Read $rows rows from the data file\n");
}

sub get_rows
{
	my($num_rows);
	foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
		my @dat=dbi_decode_row($_);
		$num_rows=$dat[0];
	}
	die "Table Not Found Or No Size Data Returned???" unless defined $num_rows;
	return $num_rows;
}

if( $OUTFILE ) {
	open(OUTFILE,">>$OUTFILE") or die "Cant write $OUTFILE";
}

$ITERATIONS=1 unless $ITERATIONS;

while ($ITERATIONS-- >= 0 ) {
	my($newrows)=get_rows();
	my($ctime)=time;
	die "ERROR run times are the same!\n" if $ctime == $ltime;
	my($newrate)=(($newrows-$rows)*60)/($ctime-$ltime);
	pr_output(  time," ",$newrows," ",$newrows-$rows," ",$newrate,"\n" );
	$rows = $newrows;
	$ltime= $ctime;
	sleep($SLEEPTIME) if $SLEEPTIME and $ITERATIONS;
}
debug( "program completed successfully.\n" );
dbi_disconnect();
exit(0);

sub debug { print @_ if defined $DEBUG; }

sub pr_output {
	if( defined $OUTFILE ) {
		print OUTFILE @_;
		print @_ if defined $DEBUG;
	} else {
		print @_;
	}
}

__END__

=head1 NAME

monitor_a_table.pl - track a single tables size and incremental

=head2 USAGE

  monitor_a_table.pl -TABLE=tbl -DATABASE=database [-OUTFILE=filename]
   --USER=SA_USER -SERVER=SERVER -PASSWORD=SA_PASS [-debug] -TYPE=Sybase|ODBC

=head2 DESCRIPTION

	Track size of a table.  Has a variety of uses.  Output format

	<time> <rows> <num new rows> <new rate per minute>

=head2 OPTIONS

if --ADDDATE then it will put a yyyymmdd to end of the file

=cut
