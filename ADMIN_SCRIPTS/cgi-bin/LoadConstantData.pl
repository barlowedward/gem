#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use lib qw(lib);

use strict;
use MlpAlarm;
use File::Basename;
my($curdir)=dirname($0);

MlpAlarm::_querynoresults("delete INV_constant");
open(RD,"$curdir/INV_Constant.dat") or die "Cant open INV_Constant.dat $!\n";
my(@datarows);
while( <RD> ) {
	next if /^\s*#/ or /^\s*$/;
	s/\s+$//;
	my($var,$val)=split(/;/);
	my($q)="insert INV_constant values('". $var. "','". $val."')";
	print $q,"\n";
	MlpAlarm::_querynoresults($q,1);
}
close RD;

