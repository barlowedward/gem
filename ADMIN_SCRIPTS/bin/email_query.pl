#!/apps/perl/linux/perl-5.8.2/bin/perl

# see below __END__

BEGIN {

  use constant SYBASE_SERVER	=>"COMPLIANCEDB";
  use constant SYBASE_LOGIN	=>"complianceweb";
  use constant SYBASE_PASS	=>"cweb2000";

}

use lib qw(/apps/sybmon/dev/lib
           /dat/apacheconf/mlpweb/htdocs/complianceadmin/lib);
use strict;
use DBI;
use DBIFunc;
use Carp qw/cluck croak/;
use MLPmail;
use POSIX();	# names are not exported

our $PARFILE;
our $TESTMODE;

our $EMAIL_TO;
our $EMAIL_FROM;
our $EMAIL_RETURN;
our $SUBJECT;
our $SERVER;
our $DATABASE;
our $PRETEXT  = "";
our $POSTTEXT  = "";
our $ZEROTEXT  = "";
our $TITLE = "";
our $QUERY = "";
our $TABLE = "";
our $testdata = "";

$PARFILE  = $ARGV[0];
$TESTMODE = $ARGV[1];   # "test" or "live"

read_par();

if ($TESTMODE eq "test")
{
  $testdata = "
<table border = 1><tr><td>
EMAIL_TO</td><td>$EMAIL_TO
</td></tr><tr><td>
EMAIL_FROM</td><td>$EMAIL_FROM
</td></tr><tr><td>
EMAIL_RETURN</td><td>$EMAIL_RETURN
</td></tr><tr><td>
SUBJECT</td><td>$SUBJECT
</td></tr><tr><td>
SERVER</td><td>$SERVER
</td></tr><tr><td>
DATABASE</td><td>$DATABASE
</td></tr><tr><td>
PRETEXT</td><td>$PRETEXT
</td></tr><tr><td>
POSTTEXT</td><td>$POSTTEXT
</td></tr><tr><td>
POSTTEXT</td><td>$ZEROTEXT
</td></tr><tr><td>
TITLE</td><td>$TITLE
</td></tr><tr><td>
QUERY</td><td>$QUERY
</td></tr></table>
";
}
elsif ($TESTMODE eq "live")
{
}
else
{
  print "Usage: $0 parfile {test|live}\n";
  exit;
}

our $DBCONNECTION = 0;

my @rc;
my @v;
my $letter;

my ($S, $M, $H, $d, $m, $y) = localtime(time());

my $Today_datetime = sprintf("%04d%02d%02d %02d:%02d:%02d",
			    $y+1900, $m+1, $d, $H, $M, $S);

$DBCONNECTION = connect_to_db();

run_query(0, __LINE__, \@rc, $QUERY);

if (@rc > 1)
{
  @v = dbi_decode_row($rc[0]);

  $TABLE .= "<table border = 1\n";

  my $ncol = @v;

  $TABLE .= "<tr><td align = center COLSPAN = $ncol>
<b><i>$TITLE</i></b></td></tr>\n";

  $TABLE .= "<tr style = background:LightSteelBlue><td>";
  $TABLE .=  join("</td><td>", @v);
  $TABLE .=  "</td></tr>\n";

  for (my $r = 1; $r < @rc; ++$r)
  {
    @v = dbi_decode_row($rc[$r]);

    my $color = ($r % 2 == 0) ? "Lavender" : "LightYellow";
    $TABLE .= "<tr style = background:$color>";
    foreach my $f (@v)
    {
      $f = nzstr($f);
      if ($f eq "") {$f = "&nbsp;";}
      $TABLE .= "<td>$f</td>\n";
    }
    $TABLE .=  "</tr>\n";
  }

  $TABLE .= "</table>\n";

  # print $TABLE;


  $letter = '
  <html><body>'."\n"
  .$testdata.$PRETEXT.$TABLE.$POSTTEXT."</body></html>\n";
}
else
{
  $letter = $ZEROTEXT;
}

if ($TESTMODE eq "test")
{
  print $letter;
}
elsif ($TESTMODE eq "live")
{
  my $cmd = "| /usr/sbin/sendmail -f $EMAIL_RETURN $EMAIL_TO";
  open MAIL, $cmd;
  print MAIL "From: $EMAIL_FROM\n";
  print MAIL "To: $EMAIL_TO\n";
  print MAIL "Subject: $SUBJECT\n";
  print MAIL "MIME-Version: 1.0\n";
  print MAIL "Content-Type: text/html\n";
  print MAIL $letter;
}

dbi_disconnect(-connection => $DBCONNECTION);

exit;

sub connect_to_db
{
  $ENV{SYBASE}="/apps/sybase";

  my $dbconnection;

  dbi_set_option(-print=>\&DBIFunc::myprint,-croak=>\&DBIFunc::mycroak);

  $dbconnection = dbi_connect(-srv		=>SYBASE_SERVER,
			      -login		=>SYBASE_LOGIN,
			      -password		=>SYBASE_PASS,
			      -nosybaseenvcheck	=>1,
			      -connection	=>1);

  if (! $dbconnection)
  {
    print "Connection failure\n";

    goto The_End;
  }

  dbi_set_mode("INLINE");

  return $dbconnection;
}

sub run_query
{
  my $print = $_[0];
  my $line  = $_[1];
  my $rc    = $_[2];    # \@
  my $query = $_[3];

  @$rc = dbi_query(-query	=> $query,
                   -db		=> $DATABASE,
                   -connection	=> $DBCONNECTION,
                   -print_hdr   => 1);
  if ($print)
  {
    $query =~ s/\n/\n/g;
    print "$line query\n-------------\n$query\n-------------\n";
    print "=============\n";
    foreach(@$rc)
    {
      print join(" ",  dbi_decode_row($_)), "\n";;
    }
    print "=============\n\n";
  }
}

sub read_par
{
  open PAR, "<$PARFILE";

  my $reading_query = 0;
  my $reading_pretext  = 0;
  my $reading_posttext  = 0;
  my $reading_zerotext  = 0;
  my $end_query;
  my $end_pretext;
  my $end_posttext;
  my $end_zerotext;

  while (<PAR>)
  {
    chomp;
    my @a = split ' ', $_, 2;

    if ($reading_query)
    {
      if ($_ eq $end_query)
      {
        $reading_query = 0;
      }
      else
      {
        $QUERY .= "$_\n";
      }
    }
    elsif ($reading_pretext)
    {
      if ($_ eq $end_pretext)
      {
        $reading_pretext = 0;
      }
      else
      {
        $PRETEXT .= "$_\n";
      }
    }
    elsif ($reading_posttext)
    {
      if ($_ eq $end_posttext)
      {
        $reading_posttext = 0;
      }
      else
      {
        $POSTTEXT .= "$_\n";
      }
    }
    elsif ($reading_zerotext)
    {
      if ($_ eq $end_zerotext)
      {
        $reading_zerotext = 0;
      }
      else
      {
        $ZEROTEXT .= "$_\n";
      }
    }
    else
    {
      if    (@a >= 2 && $a[0] eq "EMAIL_TO")
      {
        $EMAIL_TO = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "EMAIL_FROM")
      {
        $EMAIL_FROM = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "EMAIL_RETURN")
      {
        $EMAIL_RETURN = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "SUBJECT")
      {
        $SUBJECT = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "SERVER")
      {
        $SERVER = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "DATABASE")
      {
        $DATABASE = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "TITLE")
      {
        $TITLE = nzstr($a[1]);
      }
      elsif (@a >= 2 && $a[0] eq "PRETEXT")
      {
        $reading_pretext = 1;
        $end_pretext = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "POSTTEXT")
      {
        $reading_posttext = 1;
        $end_posttext = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "ZEROTEXT")
      {
        $reading_zerotext = 1;
        $end_zerotext = $a[1];
      }
      elsif (@a >= 2 && $a[0] eq "QUERY")
      {
        $reading_query = 1;
        $end_query = $a[1];
      }
    }
  }
}

sub nzstr
{
  my $str = $_[0];

  if (!defined($str)) {$str = "";}

  $str =~ s/^\s*//;
  $str =~ s/\s*$//;

  return $str;
}

# end of module

__END__

=head1 NAME

email_query.pl

=head1 SYNOPSIS

email_query.pl parfile {test|live}

=head1 DESCRIPTION

With "live", sends email as per the parfile.

With "test", writes the contents of the email to stdout,
prefixed by the interpreted contents of parfile.  The result
(written to a file) can be viewed by a browser.

=head1 PARAMETER FILE LAYOUT


  EMAIL_TO     <"to"     email addresses>          [space separated]
  EMAIL_FROM   <"from"   email address>
  EMAIL_RETURN <"return" email address>
  SUBJECT      <subject>

  SERVER   <server>
  DATABASE <database>

  PRETEXT <end-of-text-string>  [optional text preceding the table]
  ...
  ...
  ...
  <end-of-text-string>

  TITLE <table title>           [optional]

  QUERY <end-of-text-string>
  ...
  ...
  ...
  <end-of-text-string>

  POSTTEXT <end-of-text-string> [optional text following the table]
  ...
  ...
  ...
  <end-of-text-string>

  ZEROTEXT <end-of-text-string> [optional text to replace the table, in case the
  ...                            query returns nothing]
  ...
  ...
  <end-of-text-string>

=cut
