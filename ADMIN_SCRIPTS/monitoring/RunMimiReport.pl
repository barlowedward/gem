#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use DBIFunc;
use Repository;
use MlpAlarm;
use CGI qw( :standard escapeHTML escape);
use CommonFunc;
use Getopt::Long;
use CommonHeader;
use HTML::Dashboard;

BEGIN {
	$ENV{LOGON_USER}="BATCHREPORTS" unless $ENV{LOGON_USER};
}

sub usage
{
	return @_,"\n","Usage: $0 --REPORT=rpt --OUTFILE=file --GENREPORT --FILENAME=file --MAILTO=xxx --SUBJECT=xxx --DEBUG [--TIME=time]

	pass --SMTPSERVER if on win32 as the mail server
	if --OUTFILE thats where it ends up
	--GENREPORT will generate a configuration report
	if No --MAILTO then stdoutput is used.  If --FILENAME it will mail the identified file.";
}
use vars qw( $MAILTO $BATCHID $DEBUG $REPORT $GENREPORT $SUBJECT $OUTFILE $FILENAME $MYTIME $SMTPSERVER);

sub error_out {
   use MlpAlarm;
   my($msg)=@_;
   $msg = "BATCH ".$BATCHID.": ".$msg if $BATCHID;

	die $msg unless $REPORT;
   MlpEvent(
      -monitor_program=> "GemAlarmReports",
      -system=> $REPORT,
      -message_text=> $msg,
      -severity => "ERROR"
   );
   MlpBatchJobErr($msg) if $BATCHID;
   die $msg;
}

die usage("Bad Parameter List\n") unless GetOptions(
		"REPORT=s"		=> \$REPORT,
		"TIME=s"			=>	\$MYTIME,
		"GENREPORT"		=>	\$GENREPORT,
		"FILENAME=s"	=> \$FILENAME,
		"SMTPSERVER=s"	=> \$SMTPSERVER,
		"OUTFILE=s"		=> \$OUTFILE,
		"BATCHID=s"		=> \$BATCHID,
		"MAILTO=s"		=> \$MAILTO,
		"SUBJECT=s"		=> \$SUBJECT,
		"DEBUG"     	=> \$DEBUG );

MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor') if $BATCHID;

#my($curdir)=cd_home();
my($curdir)=get_gem_root_dir()."/ADMIN_SCRIPTS/cgi-bin";
my($str)="";
my($srchstr)="OK: No ";
my($srchcnt)=0;

if( defined $FILENAME ) {
	open(FH,$FILENAME) or error_out( "RunMimiReport.pl: Cant open $FILENAME" );
	while(<FH>) {
		$srchcnt++ if /$srchstr/;
		$str.=<FH>;
	}
	close(FH);
} elsif( defined $GENREPORT ) {
	$str.=MlpReportArgs( -reportname => "genreport" );
	$REPORT="genreport";
} elsif( defined $REPORT ) {
	my($cmd)="$^X ";
	my($d)=get_gem_root_dir();
	foreach (@INC) {
		next unless -d $_;
		next unless /$d/;
		$cmd .= " -I$_";
		print "  Adding Perl Include Lib : $_\n" if defined $DEBUG;
	}

	$cmd .= " $curdir/reportwriter.pl REPORTNAME=$REPORT NOHEADER=1";
	$cmd .= " debug=1" if defined $DEBUG;
	$cmd .= " filter_time=".escape($MYTIME) if defined $MYTIME;
	$cmd .= " |";\
	print "$cmd\n" if defined $DEBUG;
	open(FH,"$cmd") or error_out( "RunMimiReport.pl: Cant Run $REPORT: $!" );
	my($line)=1;
	while(<FH>) {
		$line++;
		next if $line<=2;
		$srchcnt++ if /$srchstr/;
		$str.=$_;
	}
	close(FH);
} else {
	error_out( "RunMimiReport.pl: Syntax Error - Must Pass REPORT or FILENAME\n" );
}

print $str if defined $DEBUG;

if( defined $OUTFILE ) {
	error_out( "RunMimiReport.pl: Cant overwrite existing (non-writable) file $OUTFILE $!\n" )
		if -e $OUTFILE and ! -w $OUTFILE;
	use Sys::Hostname;
	open(OUT,">$OUTFILE") or error_out( "RunMimiReport.pl: Cant write to $OUTFILE from ".hostname().": $!\n" );
	print OUT $str;
	close(OUT);
	#MlpBatchJobEnd() if $BATCHID;
	MlpBatchJobEnd() if $BATCHID;
	exit(0) unless defined $MAILTO;
}

if( ! defined $SUBJECT ) {
	my(%res)=MlpReportArgs(-reportname=>$REPORT);
	$SUBJECT=$res{ReportTitle};
	#if( ! defined $SUBJECT ) {
	#	error_out( "RunMimiReport.pl: No ReportTitle Returned for $REPORT" );
		# foreach ( keys %res ) { print "--> key=$_ val=$res{$_} \n"; }
	#}
}
$SUBJECT="Output Of $REPORT " if ! $SUBJECT;
#error_out( "RunMimiReport.pl: Must Pass Subject" ) unless defined $SUBJECT;
$SUBJECT="(No Errors) ".$SUBJECT if $srchcnt==3;

if( defined $MAILTO ) {
	send_mail_message($MAILTO,'noreply@a.com',$SMTPSERVER,$SUBJECT,$str);
#my($msg)=MIME::Lite->new(
#	To=>$MAILTO,
#	Subject=>$SUBJECT,
#	Type=>'multipart/related' );
#$msg->attach(Type=>'text/html', Data=>$str);
#$msg->send() || error_out( "RunMimiReport.pl: You cant send MAIL!!!" );
} else {
	print $str;
}
#MlpBatchJobEnd() if $BATCHID;
MlpBatchJobEnd() if $BATCHID;


__END__

=head1 NAME

RunMimiReport.pl - run a report on alarms database

=head2 SYNTAX

Usage: RunMimiReport.pl --REPORT=rpt --OUTFILE=file --FILENAME=file --MAILTO=xxx --SUBJECT=xxx --DEBUG [--TIME=time]

        if --OUTFILE thats where it ends up
        if No --MAILTO then stdoutput is used.  If --FILENAME it will mail the identified file.

=head2 DESCRIPTION

 Runs a report on the alarms database and distribute the output. The general heirarchy of this is

   exec reportwriter.pl --REPORTNAME=$REPORT
   	set args based on: select * from ReportArgs where reportname=$REPORT

The Report requires data to be inserted in the ReportArgs table within the monitoring DB.  THis has the format
	reportname
	keyname
	value

Where keyname=value are the arguments you might pass into the cgi-bin program reportwriter.pl.  These can include
	admin
	progname
	subsystem
	ProdChkBox
	ReportTitle
	ShowProgram
	filter_time
	radio_screen
	filter_severity
	filter_container

and might, for example, include the following:

	admin	Production
	admin	Ignore List
	admin	Blackout Report
	filter_container	All
	filter_container	FEED
	filter_container	UNIX
	filter_container	Unix
	filter_container	Feeds
	filter_container	WIN32
	filter_container	BACKUP
	filter_container	SYBASE
	filter_container	SQL_SERVER
	filter_container	ExchangeFiles
	filter_container	IMAGINE SYSTEM
	filter_severity	All
	filter_severity	Errors
	filter_severity	Warnings
	filter_time	1 Day
	filter_time	5 Days
	filter_time	2 Hours
	filter_time	Since 4PM
	progname	PcEventlog
	progname	UnixLogmon
	progname	PcDiskspace
	progname	SybaseErrorlog
	progname	Sybase_ASE_Log
	progname	Sybase_Backup_Log
	progname	Sybase_Replication_Log
	radio_screen	Agents
	radio_screen	Events
	radio_screen	Backups
	subsystem	Security
=cut
