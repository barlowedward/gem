#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# Copyright (c) 1997-2006 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Basename;
use Sys::Hostname;
use Getopt::Long;
use Net::myFTP;
use Repository;

sub usage {
	print "Usage $0 -debug -mode=RCP|myFTP";
}

use vars qw( $host $mode $DEBUG );
die usage("Bad Parameter List\n") unless GetOptions(
	"debug"=>\$DEBUG,
	"host=s"=> \$host,
	"mode=s"=> \$mode,
	);

die "Mode must be RCP or FTP\n" unless $mode eq "RCP" or $mode eq "FTP";

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

my($targetdir)=get_gem_root_dir().'/data/system_information_data/rhosts';
die "Cant write $targetdir\n" unless -w $targetdir;
my $curhostname = hostname();
my(@hosts);
if( ! $host ) {
	@hosts=get_password(-type=>"unix");
} else {
	@hosts=($host);
}

my($count)=1;
my(%state);
foreach my $host (@hosts) {
	#print "$host : Retrieving .rhosts File\n";
	if( $mode eq "RCP" ) {
		next if $host eq $curhostname;
		my($login,$password)=get_password(-type=>"unix",-name=>$host);
		my $cmd="rcp $login\@$host:.rhosts $targetdir/${count}_${host}_rhosts";
		#print $cmd,"\n";
		my($rc)=open(CMD,"$cmd 2>&1 |");
		die "ERROR CANT RUN $cmd\n" unless  $rc;
		$state{$host}="${count}_${host}_rhosts";
		while(<CMD>) {
			chomp;
			if( /permission denied/ ) {
				$state{$host}="Permission Denied";
			} elsif( /Connection refused/ ) {
				$state{$host}="Connection refused";
			} elsif( /No such file or directory/ ) {
				$state{$host}="No such file or directory";
			} else {
				$state{$host}=$_;
			}
		}
		close(CMD);
		printf("%20s %s\n",$host,$state{$host});
	} elsif( $mode eq "FTP" ) {
		print "Working on server $host\n";
		my(%FTPCONFIG);
		$FTPCONFIG{Debug}=$DEBUG;
		$FTPCONFIG{Timeout}=90000;
		chdir($targetdir) or die "Cant chdir to $targetdir\n";

		my($unix_login,$unix_password)=get_password(-name=>$host,-type=>"unix");
		if( ! $unix_login ) {
			warn "No Login Found for $host\n";
	   		next;
		}
		if( ! $unix_password ) {
			warn "No Password Found for $host\n";
	   		next;
		}

		debug("ftp_connect()\tnew(host=$host)\n");
		my($ftp)= Net::myFTP->new($host,%FTPCONFIG)
                       or die "Failed To Create FTP to $host Object $@";
        	if( ! $ftp ) {
			warn "Unable to ftp to $host\n";
   			next;
		}

		die( "Failed To Connect To $host" ) unless $ftp;
		if( $unix_login ne "null" and $unix_password ne "null" ) {
			debug("ftp_connect()\tlogin(login=$unix_login)\n");
			my($rc)=$ftp->login($unix_login,$unix_password);
			if( ! $rc ) {
				warn( "Cant FTP To host ($host) As $unix_login : $! \n");
				next;
			}
		}
		$ftp->ascii();

		print "Your $host Directory appears to be ",$ftp->pwd(),"\n";

		my(%FILE_TIMES,%FILE_SIZES);
		my($file_count)=0;
		#debug( "ftp\-\>cwd($dir) \n");
		#if( ! $ftp->cwd($dir) ) {
		#	warn "Cant cwd to $dir\n";
		#	return;
		#}
		#debug("Directory $dir appears to exist\n");

		foreach ( $ftp->fspec(".") ) {
			my($filenm,$filetime,$filesize,$filetype)=@$_;
			next if $filenm =~ /^\./;
			$file_count++;
			$FILE_TIMES{$filenm}=$filetime;
			$FILE_SIZES{$filenm}=$filesize;
			info("$host $file_count) $filenm (tm=$filetime, sz=$filesize, typ=$filetype)\n");
		}
		$ftp->get(".rhosts","${count}_${host}_rhosts") or warn("Cant get .rhosts from $host$!\n");

	} elsif( $mode eq "mySFTP" ) {
		print "Working on server $host\n";
		my(%FTPCONFIG);
		$FTPCONFIG{Debug}=$DEBUG;
		$FTPCONFIG{Timeout}=90000;
		chdir($targetdir) or die "Cant chdir to $targetdir\n";

		my($unix_login,$unix_password)=get_password(-name=>$host,-type=>"unix");
		if( ! $unix_login ) {
			warn "No Login Found for $host\n";
	   		next;
		}
		if( ! $unix_password ) {
			warn "No Password Found for $host\n";
	   		next;
		}

		debug("ftp_connect()\tnew(host=$host)\n");
		$FTPCONFIG{user}=$unix_login;
		$FTPCONFIG{password}=$unix_password;
		my($ftp)= Net::mySFTP->new($host,%FTPCONFIG)
                       or die "Failed To Create FTP to $host Object $@";
        	if( ! $ftp ) {
			warn "Unable to ftp to $host\n";
   			next;
		}

		die( "Failed To Connect To $host" ) unless $ftp;
		#if( $unix_login ne "null" and $unix_password ne "null" ) {
		#	debug("ftp_connect()\tlogin(login=$unix_login)\n");
		#	my($rc)=$ftp->login($unix_login,$unix_password);
		#	if( ! $rc ) {
		#		warn( "Cant FTP To host ($host) As $unix_login : $! \n");
		#		next;
		#	}
		#}
		$ftp->ascii();

		print "Your $host Directory appears to be ",$ftp->pwd(),"\n";

		my(%FILE_TIMES,%FILE_SIZES);
		my($file_count)=0;
		#debug( "ftp\-\>cwd($dir) \n");
		#if( ! $ftp->cwd($dir) ) {
		#	warn "Cant cwd to $dir\n";
		#	return;
		#}
		#debug("Directory $dir appears to exist\n");

		foreach ( $ftp->fspec(".") ) {
			my($filenm,$filetime,$filesize,$filetype)=@$_;
			next if $filenm =~ /^\./;
			$file_count++;
			$FILE_TIMES{$filenm}=$filetime;
			$FILE_SIZES{$filenm}=$filesize;
			info("$host $file_count) $filenm (tm=$filetime, sz=$filesize, typ=$filetype)\n");
		}
		$ftp->get(".rhosts","${count}_${host}_rhosts") or warn("Cant get .rhosts $!\n");
	}


	$count++;
}

#foreach (sort keys %state ) {
#	printf("%20s %s\n",$_,$state{$_});
#}

sub info {
	print @_;
}
sub debug {
	print @_ if $DEBUG;
}
