#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
my($COMPANYNAME)="Millenium Partner\'s";

BEGIN {
	if( ! defined $ENV{SYBASE} or ! -d $ENV{SYBASE} ) {
		if( -d "/export/home/sybase" ) {
			$ENV{SYBASE}="/export/home/sybase";
		} else {
			$ENV{SYBASE}="/apps/sybase";
		}
	}
}

#
# IMPORTANT!  THE PERL YOU SELECT ABOVE MUST BE 5.8 or LATER AND SHOULD INCLUDE
# the DBI and appropriate DBD Modules.  You want to point the use lib line appropriately
# as well.  This page will end up being a navigation page if called in default mode.

use Repository;
use strict;
use CGI qw/:standard/;

# Required for xterm
BEGIN {	$ENV{DISPLAY}=$ENV{"REMOTE_ADDR"}.":0.0" if $ENV{"REMOTE_ADDR"}; }

if( ! defined param("program") ) {
	# pick a program
	dotop();
	#print "<br>\n";
	print "DIRECTORY=".get_gem_root_dir().br;
	print "PERL=$^X".br;
} else {
	# do a call back
	my($command)=param("program");
	if( ! -r $command ) {
		dotop("PROGRAM NOT FOUND $command".br);
		die "PROGRAM NOT FOUND $command\n";
	}
	dotop("SENDING ADISPLAY=$ENV{DISPLAY}"
		.br
		."$^X $command 2>$dir/data/"
		.param("program").".log &"
		.br);

	system("$^X $command >$dir/data/".param("program").".log 2>&1 &" );
	#) {
	#	dotop("COMMAND $^X $command FAILED".br."ERROR=$!");
	#}
}
#	print 	$dir."/".param("program");
	#my($command)="$^X -I$dir/lib -I$dir/Win32_perl_lib -I$dir/ADMIN_SCRIPTS/lib -I$dir/plugins ".join(" ",@ARGV);
	#$command .= " >$dir/data/GEM_BATCHJOB_LOGS/$base_cmd.txt ";
	#$command .= " 2>ki$dir/data/batchjob_errors/$base_cmd.txt ";


sub dotop {
	print 	header,
		start_html('GEM Program Execution'),
		h1("GEM Secure Program Launcher");
	print "This page permits secure remote access to GEM programs. <br>\n";
	print "You will need to be running an x terminal emulator (like exceed) for this program to function. <br>\n";
	print "The program will set the DISPLAY variable to $ENV{DISPLAY} and then send a screen to your workstation\n<p><p>";
	print "It is normal for it to take up to a minute for the screen to appear on your desktop<p>";
	print @_;
	print "<A HREF=mimi.pl>Alarm Reporting CGI Screen</A><br>\n";
	print "<A HREF=webgem.pl?program=monitor.pl>monitor.pl</A><br>\n";
	print "<A HREF=webgem.pl?program=configure.pl>configure.pl</A><br>\n";
	print "<A HREF=webgem.pl?program=eventviewer.pl>eventviewer.pl</A><br>\n";

}

exit(0);

__END__

=head1 NAME

webgem.pl - Secure GEM Interface For Web Servers

=head2 SYNOPSIS

webgem.pl is a program that runs from a web server script directory and provides secure access to the primary
GEM executables.  The GEM programs are X Windows based and the output screen will be sent back to your workstation.
It is required that this program be set up by hand.

=head2 SET UP

The scripts in ADMIN_SCRIPTS/cgi-bin must be copied to a script executable
web server directory as per the CGI installation instructions.  These installation instructions can be found in the post-install tasks section of the installation guide.

=cut

