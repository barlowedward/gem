#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# GENERIC REPOSITORY RUN
# Copyright (c) 2009-2011 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- (native authentication) \n".
$PROGRAM_NAME." -DATABASE=db  -SERVER=SERVER
-or- \n".
basename($0)." -DOALL=sybase|sqlsvr|mysql|win32servers|unix

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL ,
#  NO_CONNECTION_ALERTS => 1,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;

	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	if( $DBTYPE eq "SQL SERVER" ) {
		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";
		return;
	} else {
		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";

		#my %info = get_password_info(-name=>$cursvr, -type=>'sqlsvr');
		#if( $info{SERVER_TYPE} ne "PRODUCTION" and $info{SERVER_TYPE} ne "CRITICAL"   ) {
		#	print "Skipping Server $srv As It is ",$info{SERVER_TYPE},"\n" if $DEBUG;
		#	next;
		#}


		# get current locked/expired status from the repository
		#my($q)="select credential_name, attribute_key, attribute_value
		#	print "(noexec)" if $NOEXEC and $DEBUG;
		#	print $q,"\n" if $DEBUG;
		#	MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
		#  my(@lastresults)=MlpAlarm::_querywithresults($q,1)  unless $NOEXEC
		#  foreach (@lastresults) {
		#		my(@vals) = dbi_decode_row($_);
	}
}

__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

