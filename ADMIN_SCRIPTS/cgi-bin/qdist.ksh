
#
# This is an admin script used to copy the .pl programs to my web server
#
if [ -n "$1" ]
then
cp  $* //adsrv115/c$/cgi-bin-dev
exit
fi

cp G:/gem/ADMIN_SCRIPTS/cgi-bin/*.pl //adsrv115/c$/cgi-bin-dev
cp G:/gem/ADMIN_SCRIPTS/cgi-bin/*.btn //adsrv115/c$/cgi-bin-dev
cp G:/gem/ADMIN_SCRIPTS/cgi-bin/*.evl //adsrv115/c$/cgi-bin-dev
cp G:/gem/lib/MlpAlarm.pm //adsrv115/c$/cgi-bin-dev
cp G:/gem/lib/*.pm //adsrv115/c$/cgi-bin-dev
#perl -IG:/gem/lib LoadConstantData.pl
#perl -IG:/gem/lib LoadMimiReportData.pl
