#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# ===================
# DRIVER TO READ LOG FILES - interface to LogFunc.pm
# ===================
require  5.002;
use             File::Basename;
use             Getopt::Std;
use             strict;
use             LogFunc;

use vars qw($opt_r $opt_d $opt_b $opt_i $opt_e $opt_s $opt_p $opt_o $opt_f $opt_y $opt_P $opt_t $opt_D
	$opt_m $opt_Y $opt_T $opt_S $opt_n $opt_H $opt_h $opt_G $opt_g);

my($curdir)=dirname($0);

sub usage
{
   print "\n",@_,"\n" if $#_>=0;
   die "Usage: log_file_filter.pl -i infile [ -e file -s pat|pat -p file -f fmt -odyt ]
   -d         - run in debug mode
   -b         - ignore blank lines on input
   -i file    - input file name
   -n numcols - number of columns in the file
   -H svr|svr - hosts to ignore
   -h hostcol - column of the hosts to ignore
   -G prg|prg - programs to ignore
   -g progcol - column of the programs
   -e file    - exclude file (see program header for rules)
   -s pat|pat - patern that all output lines must have
   -p file    - file containing patern which will finds first line to print
   -P file    - same as -p but does not write patern file when done
   -o         - print a message if no rows are found
   -f fmt     - format for date strings
   -y         - print yesterdays messages
   -t         - print todays messages
   -D dir     - run on all files in directory (cant also use -i or -p)
   -T         - with -D -> only files modified today
        -r      pat|pat - remove patern (including perl wildcards) from output
   -Y         - with -D -> only files modified yesterday
   -S pat     - with -D -> filter on files with patern in name
   -m cnt     - only print up to m lines

The format string may include spaces, /, mm, yy, dm, dd, mon.\n dd will prepend a 0 to days 1-9, ds will prepend space to 1-9.  It should be used with -t/-y/-T/-Y\n";
}

usage() if $#ARGV == -1;
usage() unless getopts('m:r:s:i:e:p:dof:tyD:TYS:bP:n:g:G:h:H:');
usage("ERROR: INPUT FILE REQUIRED\n")
        unless defined $opt_i or defined $opt_D;

usage("Must pass num columns with -n if you  pass -g\n")
	if defined $opt_g and ! defined $opt_n;
usage("Must pass num columns with -n if you  pass -h\n")
	if defined $opt_h and ! defined $opt_n;

log_set_num_cols($opt_n) if defined $opt_n;
log_filter_programs($opt_g,split(/\|/,$opt_G)) 	if defined $opt_g;
log_filter_hosts($opt_h,split(/\|/,$opt_H)) 	if defined $opt_h;

if( defined $opt_D ) {
        usage("ERROR: CANT PASS -D and -i\n") if defined $opt_i;
        usage("ERROR: CANT PASS -D and -p\n") if defined $opt_p;
} else {
        usage("ERROR: CANT PASS -TSY without -D\n")
                if defined $opt_T or defined $opt_S or defined $opt_Y;
}

usage("Cant pass -p and -P\n") if defined $opt_p and defined $opt_P;

$opt_p = $opt_P if defined $opt_P;

usage( "Must Pass Format\n")
        if defined $opt_t and !defined $opt_f;
usage( "Must Pass Format\n")
        if defined $opt_y and !defined $opt_f;
usage( "Must Pass -t or -y\n")
        if !defined $opt_y and !defined $opt_t and defined $opt_f;

# ====================================
# OK... args validated
# ====================================
log_debug() if defined $opt_d;

log_grep(search_patern=>$opt_s,today=>$opt_t,yesterday=>$opt_y,date_fmt=>$opt_f);

log_rm_pat($opt_r);

# ============================
# get start patern
# ============================
my($byte_count,$start_patern)=(0,"");
if( defined($opt_p) ) {
        if( ! -e $opt_p ) {
                $opt_p=$curdir."/".$opt_p if -e $curdir."/".$opt_p;
        }
        ($byte_count,$start_patern) = log_rd_patern_file(file=>$opt_p);
}

# ============================
# READ EXCLUSION FILES
# ============================
if( defined $opt_e ) {
        if( ! -r $opt_e ) {
                die "Cant Find Patern File $opt_e\n"
                        unless -r $curdir."/".$opt_e;
                $opt_e=$curdir."/".$opt_e;
        }
        log_filter_file(file=>$opt_e);
}

# ============================
# LOOP THROUGH FILES IF -D IS DEFINED
# ============================
if( defined $opt_D ) {
        die "$opt_D not a directory\n" unless -d $opt_D;

        opendir(DIR,$opt_D)
                || &die("Can't open directory $opt_D for reading\n");
        my(@dirlist) = grep -T, map "$opt_D/$_", readdir DIR;
        closedir(DIR);

        my($file);
        foreach $file (@dirlist) {

                print "testing $file - Date=",-M $file,"days\n" if defined $opt_d;
                if( defined $opt_T ) {
                        next if defined $opt_Y and -M $file > 2;
                        next if ! defined $opt_Y and -M $file > 1;
                } elsif( defined $opt_Y ) {
                        next if -M $file > 2;
                        next if -M $file < 1;
                }

                next if defined $opt_S and $file !~ /$opt_S/;
                print "#
#
#                    ERRORLOG PARSER
# $file
#
# ---- THE NEXT LINE STARTS THE ERROR LOG ---- \n";

                my($num_rows_read,$num_rows_printed,$found_pat,$returned_patern,$byte_cnt,@output) = log_process_file(file=>$file,ignore_blanks=>$opt_b,max_lines=>$opt_m);

                foreach (@output) { print $_; }

                print "# ---- ERROR LOG $file DONE ---- \n";
                if( defined $opt_t or defined $opt_y or defined $opt_e ) {
                        print "#\n# [ filter allowed $num_rows_printed of $num_rows_read rows from $file ]\n#\n" if defined $opt_o;
                } else {
                        print "#\n# [ printed $num_rows_printed messages from file $file ]\n#\n" if defined $opt_o;
                }
        }

} else {

        # =============================
        # READ & PARSE A SINGLE INPUT FILE
        # =============================
        my($num_rows_read,$num_rows_printed,$found_pat,$returned_patern,$byte_cnt,@output) = log_process_file(file=>$opt_i,ignore_blanks=>$opt_b,search_patern=>$start_patern,byte_offset=>$byte_count,max_lines=>$opt_m);

        foreach (@output) { print $_; }

        log_wr_patern_file(patern=>$returned_patern,bytes=>$byte_cnt,file=>$opt_p)
                if defined $opt_p and ! defined $opt_P;

        if( defined $opt_t or defined $opt_y or defined $opt_e ) {
                print "[ filter allowed $num_rows_printed of $num_rows_read rows from $opt_i ]\n" if defined $opt_o;
        } else {
                print "[ printed $num_rows_printed messages from file $opt_i ]\n" if defined $opt_o;
        }
}

print "Program Completed\n" if defined $opt_d;
exit 0;

__END__

=head1 NAME

log_file_filter.pl - Generic Error Log Filter

=head2 DESCRIPTION

This program filters log files.  It can filter out garbage messages based on data it reads from an exclude file, which is a list of messages and what to do with them (pretty simple).  It can also filter to only print messages from today or from yesterday & today. Since dates are of many formats, you pass in a format string for the date.

A patern file can be used.  This file is used to only see new messages (after the patern). The patern file is of the format [byte_count\tstring].  If the string is in the file at that byte count (ie it is not a new file), then the filter will only print messages after that line.  If not it will read the whole file.  You can use this to see only new messages (messages since the last run of log_file_filter).

=head2 USAGE

Usage: B<log_file_filter> B<-i> infile [ B<-e> file B<-s> pat|pat B<-p> file B<-f> fmt B<-odyt> ]

B<-d>         - run in debug mode

B<-b>         - ignore blank lines on input

B<-i file>    - input file name

B<-e file>    - exclude file (see program header for rules)

B<-s pat|pat> - patern that all output lines must have

B<-p file>    - patern file containing print print after patern

B<-P file>    - same as B<-p> but does not write patern file

B<-o>         - print a message if no rows are found

B<-f fmt>     - format for date strings

B<-y>         - print yesterdays messages

B<-t>         - print todays messages

B<-D dir>     - run on all files in directory (cant also use B<-i> or B<-p>)

B<-T>         - with B<-D>  only files modified today

B<-Y>         - with B<-D>  only files modified yesterday

B<-S pat>     - with B<-D>  filter on files with patern in name

B<-m cnt>     - only print up to m lines

The format string may include spaces, /, mm, yy, dm, dd, mon.
dd will prepend a 0 to days 1-9, ds will prepend space to 1-9

=head2 EXAMPLES

To read the whole sybase errorlog

C<log_file_filter -i $SYBASE/install/errorlog>

to read errors in that errorlog

C<log_file_filter -i $SYBASE/install/errorlog -e sybase10.excl>

to read errors since the last run

C<log_file_filter -i $SYBASE/install/errorlog -e sybase10.excl -p tmp>

to read todays errors

C<log_file_filter -i $SYBASE/install/errorlog -e sybase10.excl -t -f"yy/mm/dd">

to read todays and yesterdays errors

C<log_file_filter -i $SYBASE/install/errorlog -e sybase10.excl -yt -f"yy/mm/dd">

and finally, to read only today & yesterdays messages that occurred since the last

=head2 FILTER FILES

Currently, the following files can be used for filter purposes:

        aix.excl                                - aix system log
        backupsrvr.excl - sybase backup server messages
        hpux.excl                       - hpux system log
        linux.excl                      - linux system log
        solaris.excl            - solaris system log
        sunos.excl                      - sunos system log
        sybase10.excl           - sybase log

=head2 DETAILS

This probably needs some explanation.  Fundamentally the program takes an input file (from B<-i>) and prints it after filtering.  It uses an excludefile to define what is an error (actually the file defines what is ok).  You can also filter on recent messages using the B<-t> or B<-y> flags in conjunction with the B<-f> date format option (log files have different date formats and
you need to specify what you want to search for).

Another major way to search through this file is to use a patern file (B<-p>).  If a patern file is specified, only lines after the patern (if found) are printed, and the patern in the file is updated with the last line from the errorlog.  This is useful to see errors that have occurred since the last time you ran the filter.

The exclusion file is self documented but basically uses lines of of the format XXX=patern and excludes based on exclusion type as follows:

   EXCLUDE=pat      - excludes lines with pat in them
   ML_START=pat     - excludes lines until patern specified by ML_END
                     (next) is found. Will print ML_END line.
   ML_END=pat       - stop ML_START block
   EXCLNEXT=pat     - excludes line with patern AND next line
   EXCLSTART=pat    - exclude only if patern starts line

The B<-D> option is a new feature, allowing the browsing of error log directories.  This is very different from normal browsing.  The B<-t> and B<-y> flags still work, but they refer to files from today and yesterday.  The B<-s> flag operates on file names (not patern strings in the file).

This general parser should be good enough for most patern parsing of log files.
Output is to standard output.

=head2 BUGS

Converts all * characters to a - to deal with perl wildcarding.
This should only be noticable on the output prints

=cut
