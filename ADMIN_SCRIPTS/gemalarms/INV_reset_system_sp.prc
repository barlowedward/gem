create procedure INV_reset_system_sp
as
begin
/*
update 	INV_system set host_name=h.system_name,host_type=h.system_type
from 	INV_system me, INV_system h, INV_system_attribute a
where 	a.attribute_name='HOSTNAME'
and 	a.attribute_value=h.system_name
and 	h.system_type in ('unix','win32servers')
and 	me.host_name is null
and		me.system_name = a.system_name
and		me.system_type = a.system_type
*/
/*
update 	INV_system set host_name=h.system_name,host_type=h.system_type
from 	INV_system me, INV_system h, INV_system_attribute a
where 	a.attribute_name like '%_HOSTNAME'
and 	a.attribute_value=h.system_name
and 	h.system_type in ('unix','win32servers')
and 	me.host_name is null
and		me.system_name = a.system_name
and		me.system_type = a.system_type
*/

/*
select 	h.business_system_name,system_name=s.host_name,system_type=s.host_type
into #newsys
from 	INV_system_heirarchy h, INV_system s
where 	h.system_name=s.system_name
and   	h.system_type=s.system_type
and		s.host_name is not null
*/
/*
delete #newsys
from #newsys s, INV_system_heirarchy h
where 	h.system_name=s.system_name
and   	h.system_type=s.system_type
and   	h.business_system_name=s.business_system_name

insert INV_system_heirarchy select * from #newsys

drop table #newsys
*/
update INV_system set production_state='DEVELOPMENT'
where system_name like '%DEV%' or system_name like '%dev%' and production_state!='DEVELOPMENT'

update INV_system set production_state='PRODUCTION'
where system_name like '%PROD%' or system_name like '%PROD%' and production_state!='PRODUCTION'

update 	INV_system set description=a.attribute_value
from 		INV_system me,  INV_system_attribute a
where 	a.attribute_name = 'LDAP_desc'
and		me.system_name = a.system_name
and		me.system_type = a.system_type
and     	a.attribute_value is not null
and      me.description is null
end
go
grant execute on INV_reset_system_sp to public
go
