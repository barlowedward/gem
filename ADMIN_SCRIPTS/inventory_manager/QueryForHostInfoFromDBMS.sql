/*select * from INV_system where system_type='sqlsvr'

select distinct system_type from INV_system where system_type='sqlsvr_spreadsheet'
select top 100 * from INV_system_attribute where system_name='uxprocs' order by system_name
select top 100 * from INV_system where system_name like 'BROKERSQL%' order by system_name
select top 100 attribute_name,attribute_value,system_type  from INV_system_attribute where system_name ='IMAGSYB2' order by system_name

select attribute_name,attribute_value,system_type from INV_system_attribute where system_name='uxprocs' order by attribute_name
and system_type!='win32servers'
order by system_type


a.system_type='sqlsvr_spreadsheet'
and a.attribute_name='host'

*/

-- drop table #SYSTEMS

select system_name,system_type,hostname=attribute_value,
C_Drive_Size = convert(int,null), D_Drive_Size =  convert(int,null),  E_Drive_Size =  convert(int,null),  F_Drive_Size =  convert(int,null),  G_Drive_Size =  convert(int,null),  H_Drive_Size =  convert(int,null)
into #SYSTEMS
from INV_system_attribute a
where a.attribute_name='host'
and system_type like '%spreadsheet'
and attribute_value not like 'ln%'

update #SYSTEMS set hostname='ADSRV046' where hostname='ADSDRV046'
update #SYSTEMS set hostname='ADCLUSTER261' where hostname='ADCLUSTER260'
update #SYSTEMS set hostname='ADCLUSTER221' where hostname='ADCLUSTER220'
update #SYSTEMS set hostname='ADCLUSTER041' where hostname='ADCLUSTER040'

update #SYSTEMS set
C_Drive_Size = ( select  distinct perf_int_2/1024 from PER_intsummary  s
		where s.system_name=a.hostname and perf_key='win32_diskspace' and key_2='c$' and category='20110227' ),
D_Drive_Size = ( select  distinct perf_int_2/1024 from PER_intsummary  s
		where s.system_name=a.hostname and perf_key='win32_diskspace' and key_2='d$' and category='20110227' ),
E_Drive_Size = ( select  distinct perf_int_2/1024 from PER_intsummary  s
		where s.system_name=a.hostname and perf_key='win32_diskspace' and key_2='e$' and category='20110227' ),
F_Drive_Size = ( select  distinct perf_int_2/1024 from PER_intsummary  s
		where s.system_name=a.hostname and perf_key='win32_diskspace' and key_2='f$' and category='20110227' ),
G_Drive_Size = ( select  distinct perf_int_2/1024 from PER_intsummary  s
		where s.system_name=a.hostname and perf_key='win32_diskspace' and key_2='g$' and category='20110227' )
from #SYSTEMS a

update #SYSTEMS set system_name=upper(system_name) where system_name like 'ad%'
-- select * from #SYSTEMS where hostname='lnweb1'
-- delete #SYSTEMS where hostname!='lnweb1'

select system_type, Db=system_name, Host=hostname,
DCM_cabinet = ( select attribute_value from INV_system_attribute s where s.system_type='DCM'
					  and attribute_name='cabinet' and s.system_name=a.hostname ),
DCM_build = ( select attribute_value from INV_system_attribute s where s.system_type='DCM'
					  and attribute_name='plbuild' and s.system_name=a.hostname ),
DCM_floor = ( select attribute_value from INV_system_attribute s where s.system_type='DCM'
					  and attribute_name='floor' and s.system_name=a.hostname ),
DCM_city = ( select attribute_value from INV_system_attribute s where s.system_type='DCM'
					  and attribute_name='plcity' and s.system_name=a.hostname ),

WASP_site = ( select attribute_value from INV_system_attribute s where s.system_type='WASP'
					  and attribute_name='site' and s.system_name=a.hostname ),
WASP_location = ( select attribute_value from INV_system_attribute s where s.system_type='WASP'
					  and attribute_name='location' and s.system_name=a.hostname ),
WASP_manuf = ( select attribute_value from INV_system_attribute s where s.system_type='WASP'
					  and attribute_name='manufacturer name' and s.system_name=a.hostname ),
WASP_descr = ( select attribute_value from INV_system_attribute s where s.system_type='WASP'
					  and attribute_name='asset description' and s.system_name=a.hostname ),
WASP_sn = ( select attribute_value from INV_system_attribute s where s.system_type='WASP'
					  and attribute_name='serial no' and s.system_name=a.hostname ),
WASP_tag = ( select attribute_value from INV_system_attribute s where s.system_type='WASP'
					  and attribute_name='asset tag' and s.system_name=a.hostname ),

DB_Version = ( select attribute_value from INV_system_attribute s where s.system_type  in ('sybase_discovery','sqlsvr_discovery')
					  and attribute_name in ( 'Metadata_VERSION_AS_INTEGER','productversion') and s.system_name=a.system_name ),
DB_Level = ( select attribute_value from INV_system_attribute s where s.system_type in ('sybase_discovery','sqlsvr_discovery')
					  and attribute_name='productlevel' and s.system_name=a.system_name ),
DB_Edition = ( select attribute_value from INV_system_attribute s where s.system_type in ('sybase_discovery','sqlsvr_discovery')
					  and attribute_name in ( 'Metadata_VERSION','edition') and s.system_name=a.system_name ),




HW_Os = ( select attribute_value from INV_system_attribute s where s.system_type in( 'unixinv','win32servers_discovery')
					  and attribute_name in ('os','osversion') and s.system_name=a.hostname ),
HW_OsServicePack = ( select attribute_value from INV_system_attribute s where s.system_type  in( 'unixinv','win32servers_discovery')
					  and attribute_name in ('service_pack','ver') and s.system_name=a.hostname ),
HW_Memory = ( select attribute_value from INV_system_attribute s where s.system_type in( 'unixinv','win32servers_discovery')
					  and attribute_name='memory' and s.system_name=a.hostname ),
HW_NumProc = ( select attribute_value from INV_system_attribute s where s.system_type in( 'unixinv','win32servers_discovery')
					  and ( attribute_name='number_of_processors' or attribute_name='cpucount' ) and s.system_name=a.hostname ),
HW_ProcSpeed = ( select attribute_value from INV_system_attribute s where s.system_type in( 'unixinv','win32servers_discovery')
					  and ( attribute_name='processor_speed' or attribute_name='cpuspeed' ) and s.system_name=a.hostname ),
HW_BiosDate = ( select attribute_value from INV_system_attribute s where s.system_type  in ( 'unixinv','win32servers_discovery')
					  and ( attribute_name='bios' or attribute_name='system_bios_date') and s.system_name=a.hostname ),
HW_ProcVendor = ( select attribute_value from INV_system_attribute s where s.system_type in( 'unixinv','win32servers_discovery')
					  and attribute_name in ('processor_vendor','uname') and s.system_name=a.hostname ),
HW_ProcName = ( select attribute_value from INV_system_attribute s where s.system_type in ( 'unixinv','win32servers_discovery')
					  and attribute_name='processor_name' and s.system_name=a.hostname ),
HW_ProcId = ( select attribute_value from INV_system_attribute s where s.system_type in ( 'unixinv','win32servers_discovery')
					  and attribute_name in ('processor_identifier','cputype') and s.system_name=a.hostname ),
C_Drive_Size ,
D_Drive_Size ,
E_Drive_Size ,
F_Drive_Size ,
G_Drive_Size
from #SYSTEMS a
order by system_type,system_name
-- from INV_system_attribute a where a.system_type='sqlsvr_spreadsheet'
-- and a.attribute_name='host'

drop table #SYSTEMS
/*

from INV_system a where system_type='win32servers' and system_name in (
'adsrv045',
'adsrv046',
'ADSDR012',
'ADSRV067',
'ADSRV149',
'ADSRV026',
'ADCLUSTER241',
'ADCLUSTER101',
'ADCLUSTER121',
'ADSDR029',
'ADSDR032',
'ADSRV077',
'ADSRV121',
'ADSRV130',
'ADSRV195',
'ADSRV226',
'ADCLUSTER191',
'ADCLUSTER211',
'ADSRV244',
'ADSDR013',
'ADSDR030',
'ADSRV013',
'ADSRV031',
'ADSRV090',
'ADSRV141',
'ADSRV215',
'ADSRV258',
'ADSRV261',
'ADSDR031',
'ADSDR032',
'ADSRV074',
'ADSRV088',
'ADSRV111',
'ADSRV151',
'ADSRV180',
'ADSRV181',
'ADSRV247',
'ADSRV254',
'LNSRV015',
'LNSRV031',
'LNSRV034',
'LNSRV047',
'PASRV004',
'SISRV005',
'ADCLUSTER081',
'ADCLUSTER171',
'ADCLUSTER261',
'SICLUSTER061',
'LNCLUSTER061',
'ADSRV159',
'LNSRV217',
'ADSRV034',
'ADSRV193',
'ADCLUSTER221',
'ADSRV179',
'LNSQLES1',
'LNSRV032',
'ADSRV170',
'ADSRV222',
'ADCLUSTER041',
'ADSRV118',
'ADSRV197',
'LNSRV230',
'LNSRV231',
'ADSRV246',
'LNSRV211',
'LNSRV212')
order by a.system_name
*/
/*
SELECT *
from INV_system_attribute a where a.system_type='sqlsvr_spreadsheet'
and a.attribute_name='host'
order by a.system_name


select  perf_int_2/1024,* from PER_intsummary where system_name='ADSDR001' and perf_key='win32_diskspace' order by key_2
select top 30  * from PER_intsummary
select top 30  * from PER_lastfloatstatistics
select top 30  * from PER_lastintstatistics

DCM
WASP
unix
unixinv
sybase
sybase_discovery
sybase_spreadsheet
unix_discovery
ldap_computer
win32servers
win32servers_discovery
dnsalias
sqlsvr
sqlsvr_discovery
sqlsvr_spreadsheet
mysql
mysql_spreadsheet
oracle
oracle_spreadsheet
oracle_discovery

sp__helptable
*
