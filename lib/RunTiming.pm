# copyright (c) 2011-2012 by SQL Technologies.  All Rights Reserved.
# All Rights Reserved
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.
#

package 		RunTiming;

use	     	Exporter;

@ISA	    	= qw(Exporter);
@EXPORT	 	= qw(starttiming endtiming printtiming );

use		strict;
use 		Do_Time;

my(%starttime,%endtime,@tasksinorder); # try 2
my($timing_label,$timing_starttime,%timinghash);

sub starttiming {
	($timing_label)=@_;
	$starttime{$timing_label}=time;
	push @tasksinorder,$timing_label;
	print "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
	print "+ Starting: ",$timing_label," at ".do_time(-fmt=>'hh:mi:ss')."\n";
	print "++++++++++++++++++++++++++++++++++++++++++++++++\n";
	$timing_starttime=time;	
}

sub endtiming {
	$endtime{$timing_label}=time;
	my($fmt)=fmt_time(time,$timing_starttime);
	chomp $timing_label;
	$timinghash{$timing_label}=$fmt;
	print "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
	print "+ Completed: ",$timing_label," finished in ",$fmt,"\n";
	print "++++++++++++++++++++++++++++++++++++++++++++++++\n\n";		
}

sub printtiming {
	print "\n========= TIMING INFORMATION ============\n";
	my($mins,$maxe)=(time()+100000,0);
	foreach ( @tasksinorder ) {
		$mins=$starttime{$_} if ! $mins or $mins > $starttime{$_};
		$maxe=$endtime{$_}  if ! $maxe or $maxe < $endtime{$_} ;
		printf("%10s %20s \n", $timinghash{$_},$_);
	}

	print "Total Run Time - ",fmt_time($maxe,$mins),"\n";;
	my $starttime = localtime( $mins );
	$maxe      = localtime( time );
	print "Program Started - $starttime \n";
	print "Program Ended   - $maxe \n";
	print "========= DONE TIMING INFORMATION ============\n\n";
}

sub fmt_time
{
	my($endtime,$starttime)=@_;
	my $minutes=int(($endtime-$starttime)/60);
	return sprintf "%3s min %2s sec.", $minutes, ($endtime-$starttime-60*$minutes);
}

1;