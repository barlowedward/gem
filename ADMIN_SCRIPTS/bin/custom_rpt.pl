#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use Getopt::Std;
use File::Basename;
use strict;
use DBIFunc;

my($curdir)=dirname($0);
chdir $curdir or die "Cant chdir to $curdir: $!\n";

sub usage
{
	print @_;
	print "Usage: custom_rpt.pl  -USA_USER -SSERVER -PSA_PASS -RREPORT_NUM -TREPORT_TYPE

    -h html output, -x or -d debug mode

    Custom reports

    1) Object Protections by Group (SIUDE)
    2) Groups By Database
    3) Users By Database
    4) Num DBO Objects
    5) Num Non DBO Objects
    6) Login Report
    7) Aliases By Database
";

	return "\n";
}

$|=1;

use vars qw($opt_h $opt_d $opt_U $opt_S $opt_P $opt_R $opt_x $opt_T);

die usage("Bad Parameter List") unless getopts('U:S:P:R:xhdT:');
die usage() unless defined $opt_U and defined $opt_S and defined $opt_P and defined $opt_R;

my($flags)="";

$flags.=" -x" if defined $opt_x;
$flags.=" -h" if defined $opt_h;

my $command="";

if( $opt_R eq "1" ) {

	$command="$^X crosstab.pl -t\"OBJECT PROTECTIONS BY GROUP IN SERVER $opt_S\" -S$opt_S -U$opt_U -P$opt_P -Dnosys -Qsp__objprotect -R1,3 -C4 -O5 $flags";

} elsif( $opt_R eq "2" ) {

	$command="$^X crosstab.pl -t\"GROUPS BY DATABASE IN SERVER $opt_S\" -S$opt_S -U$opt_U -P$opt_P -Dnosys -Qsp__helpuser2 -R2 -C1 -O5 $flags";

} elsif( $opt_R eq "7" ) {

	$command="$^X crosstab.pl -t\"ALIASES BY DATABASE IN SERVER $opt_S\" -S$opt_S -U$opt_U -P$opt_P -Dnosys -Qsp__helpuser2 -R2 -C1 -O4 $flags";

} elsif( $opt_R eq "3" ) {

	$command="$^X crosstab.pl -t\"USERS BY DATABASE IN SERVER $opt_S\" -S$opt_S -z14 -U$opt_U -P$opt_P -Dnosys -Qsp__helpuser2 -R2 -C1 -O3 $flags";

} elsif( $opt_R eq "4" ) {

	$command="$^X crosstab.pl -t\"# DBO OBJECTS BY DATABASE IN SERVER $opt_S\" -S$opt_S -U$opt_U -P$opt_P -Q\"select type,count(*) from sysobjects where uid=1 group by type\" -R1 -C2 -O3 $flags";

} elsif( $opt_R eq "5" ) {

	$command="$^X crosstab.pl -t\"# NON DBO OBJECTS BY DATABASE IN SERVER $opt_S\" -S$opt_S -U$opt_U -P$opt_P -Q\"select type,count(*) from sysobjects where uid!=1 group by type\" -R1 -C2 -O3 $flags";

} elsif( $opt_R eq "6" ) {

	# LOGIN REPORT
	print "Connecting to $opt_S\n" if defined $opt_d;
	dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
		or die "Cant connect to $opt_S as $opt_U\n";
	print "Connected to $opt_S\n" if defined $opt_d;

	my($server_type,@databaselist)=dbi_parse_opt_D("%",1,1);

	my(%suid,%defn);
	my($q)="exec sp__helplogin \@dont_format='Y' ";
	print "Running $q \n" if $opt_d;
	foreach (dbi_query( -db=>"master", -query=>$q)) {

		print "Returned:",join(" ",dbi_decode_row($_)),"\n" if $opt_d;
		if( $server_type eq "SQLSERVER" ) {
			my($name,$defdb,$language,$ok, $type, $Admin, $remote) = dbi_decode_row($_);
			$name	=~s/\s//g;
			$defdb	=~s/\s//g;
			$language=~s/\s//g;
			$ok	=~s/\s//g;
			$type	=~s/\s//g;
			$Admin	=~s/\s//g;
			$remote	=~s/\s//g;

			#$suid{$name} = $id;
			$defn{$name} = "";
			$defn{$name} .= " Type=$type";
			$defn{$name} .= " ADMINISTRATOR"      	if $Admin  eq "Yes";
			$defn{$name} .= " REMOTE"      		if $remote  eq "Yes";
			#$defn{$name} .= " SA"	   if $sa   eq "Y";
			#$defn{$name} .= " SSO"	  if $sso  eq "Y";
			#$defn{$name} .= " OPER"	 if $oper eq "Y";
		} else {
			my($id,$name,$defdb,$sht,$lck,$exp,$sa,$sso,$oper,$remote) = dbi_decode_row($_);
			$id	=~s/\s//g;
			$name	=~s/\s//g;
			$defdb	=~s/\s//g;
			$sht	=~s/\s//g;
			$lck	=~s/\s//g;
			$exp	=~s/\s//g;
			$sa	=~s/\s//g;
			$sso	=~s/\s//g;
			$oper	=~s/\s//g;
			$remote	=~s/\s//g;

			$suid{$name} = $id;
			$defn{$name} = "";
			$defn{$name} .= " LOCKED"       if $lck  eq "Y";
			$defn{$name} .= " EXPIRED"      if $exp  eq "Y";
			$defn{$name} .= " SA"	   if $sa   eq "Y";
			$defn{$name} .= " SSO"	  if $sso  eq "Y";
			$defn{$name} .= " OPER"	 if $oper eq "Y";
		}
	}

	my(%dat);
	foreach my $db (@databaselist) {
		next if $db=~/^ReportServer/i;
		print "working on database $db\n" if defined $opt_d;
		my($query)="exec sp__helpuser2 \@dont_format='Y'\n";
		print $query if defined $opt_d;
		foreach (dbi_query(-db=>$db,-query=>$query)) {
			#s/\s//g;
			my(@x)=dbi_decode_row($_);

			$x[0] =~ s/\s//g;
			$x[1] =~ s/\s//g;
			$x[2] =~ s/\s//g;
			$x[3] =~ s/\s//g;
			$x[4] =~ s/\s//g;

			#next if $x[0] =~ /^$/;

			$dat{$db." ".$x[0]} = "Alias" if $x[2] =~ /Y/;
			$dat{$db." ".$x[0]} = $x[3] if $x[3] ne "";
			if( defined $opt_h ) {
				$dat{$db." ".$x[0]} = "<FONT COLOR=BLUE>".$dat{$db." ".$x[0]}."</FONT>"
					if $x[4] eq $db;
			} else {
				$dat{$db." ".$x[0]} .= " (Default Db)"
					if $x[4] eq $db;
			}
		}
	}

	if(defined $opt_h) {
		print "<h2>Login report for server $opt_S</h2>\n";
		print "Dark Blue indicates default database\n";
		print "<TABLE BORDER=1>\n";
		print "<TR><TH>Login</TH><TH>Notes</TH>\n\t";
		foreach my $db (@databaselist) {
			print "<TH>$db</TH>";
		}
		print "</TR>";
	}

	foreach my $login (sort keys %defn) {

		next if $login =~ /\SqlServer20/i or $login =~ /^NT AUTH/i  or $login =~ /^\#\#/i
							or $login =~ /^BUILTIN/i or $login =~ /\$/ or $login eq "sa";

		if( ! defined $opt_h ) {
			printf "%-18s suid=%-4s %s\n",
				$login,$suid{$login},$defn{$login};

			foreach my $db (@databaselist) {
				printf "%15s => %s\n",$db,$dat{$db." ".$login}
					if defined $dat{$db." ".$login};
			}
		} else {
			# HTML....
			print "<TR><TD>$login</TD><TD>$defn{$login}&nbsp;</TD>\n";
			foreach my $db (@databaselist) {
				print "\t<TD>",$dat{$db." ".$login},"&nbsp;</TD>\n";
			}
			print "</TR>\n";
		}
	}
	print "</TABLE>\n" if defined $opt_h;

	dbi_disconnect();
	exit(0);

} else {
	die usage("Error Unknown Report Number");
}

print "Running Command $command\n" if $opt_d;
run_system_cmd($command);
exit(0);

sub run_system_cmd
{
   my($string)=@_;

   $string .= " |";

	print "<PRE>"   if defined $opt_h;
	my($prstring)=$string;
	$prstring =~ s/-P\s*\w+/-PXXX/;
	print "Running: ",$prstring;

	print "</PRE>\n" if defined $opt_h;

   open( SYS2STD,$string )  || die("Unable To Run  $string");
   while ( <SYS2STD> ) { print $_; }
   close SYS2STD;
}
__END__

=head1 NAME

custom_rpt.pl - run precanned sybase report

=head2 USAGE
Usage: custom_rpt.pl  -USA_USER -SSERVER -PSA_PASS -RREPORT_NUM

    -h html output, -x debug mode

    Custom reports

    1) Object Protections by Group (SIUDE)
    2) Groups By Database
    3) Users By Database
    4) Num DBO Objects
    5) Num Non DBO Objects
    6) Login Report
    7) Aliases By Database

=head2 NOTES

Runs predefined crosstab reports.  Requires extended stored procedure library.
=cut
