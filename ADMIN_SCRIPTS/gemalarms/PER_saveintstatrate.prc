create procedure PER_saveintstatrate (

  @system_name        varchar(30)   = null,
  @system_type        varchar(30)   = null,

  @key_2  			 	 varchar(30) 	= null,
  @key_3  				 varchar(30) 	= null,
  @key_4  				 varchar(30) 	= null,

  @perf_key           varchar(30)   = null,
  @perf_time          datetime      = null,

  @weight             bigint =null,
  @perf_int_1         bigint =null,
  @perf_int_2         bigint =null,
  @perf_int_3         bigint =null,
  @perf_int_4         bigint =null,
  @perf_int_5         bigint =null,
  @perf_int_6         bigint =null

)
as
begin
	declare @numsec int

	if @perf_time is null
		select @perf_time=getdate()

  	select @numsec = datediff(ss,perf_time,@perf_time)
  	from PER_lastintstatistics
         where system_name	= 	@system_name
           and system_type	= 	@system_type
           and perf_key   	= 	@perf_key
	        and	key_2			=	@key_2
	        and	key_3			=	@key_3
	        and	key_4			=	@key_4

	 -- only insert if there was sufficient time
	 if @numsec is not null and @numsec>5
	   insert PER_intstatistics
      (   system_name,  system_type,  perf_key,  perf_time,  weight,
          key_2, key_3, key_4, perf_int_1, perf_int_2, perf_int_3, perf_int_4, perf_int_5, perf_int_6  )
      select   @system_name,  @system_type,  @perf_key,  @perf_time, @weight,
      			@key_2, @key_3, @key_4,
               (@perf_int_1 - perf_int_1) / @numsec,
               (@perf_int_2 - perf_int_2) / @numsec,
               (@perf_int_3 - perf_int_3) / @numsec,
               (@perf_int_4 - perf_int_4) / @numsec,
               (@perf_int_5 - perf_int_5) / @numsec,
               (@perf_int_6 - perf_int_6) / @numsec
      from       PER_lastintstatistics
      where    system_name=@system_name
        and      system_type= @system_type
        and      perf_key   = @perf_key
        and		  key_2=@key_2
        and		  key_3=@key_3
        and		  key_4=@key_4

	 -- clobber last stats as we will replace it
	 if @numsec is not null
      DELETE PER_lastintstatistics
      where 	system_name= @system_name
         and   system_type= @system_type
         and   perf_key =	@perf_key
         and	key_2		=	@key_2
     		and	key_3		=	@key_3
    		and	key_4		=	@key_4


    -- ALWAYS insert Last Stats
    insert PER_lastintstatistics
    (  system_name,  system_type,  perf_key,  perf_time,
       key_2, key_3, key_4,
       perf_int_1,  perf_int_2,  perf_int_3,  perf_int_4,  perf_int_5,
       perf_int_6
    ) values (   @system_name,  @system_type,  @perf_key,  @perf_time,
        @key_2, @key_3, @key_4,
        @perf_int_1,  @perf_int_2,  @perf_int_3,  @perf_int_4,  @perf_int_5,
        @perf_int_6
    )
end
go
grant execute on PER_saveintstatrate to public
go
