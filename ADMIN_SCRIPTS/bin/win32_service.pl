#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Win32::Service;
use Getopt::Long;

use MlpAlarm;
use Repository;

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# FIND YOUR SERVICES
my(@service_status)=('',
        'SERVICE STOPPED',
        'SERVICE START PENDING',
        'SERVICE STOP PENDING',
        'SERVICE RUNNING',
        'SERVICE CONTINUE PENDING',
        'SERVICE PAUSE PENDING',
        'SERVICE PAUSED'
);

my( %IGNORABLE_SERVICE_NAMES ) = (
	"WinHttpAutoProxySvc" => 1,
	"RpcLocator" => 1,
	"VSS" => 1,
	"Navssprv" => 1,
	"ComSysApp" => 1,
	"swprv" => 1,
	"AppMgmt" => 1	,
	'DPS' => 1	,
	'WdiSystemHost' => 1	,
	'ProtectedStorage' => 1	,
	'TrustedInstaller' => 1	,
	'AeLookupSvc' => 1
);

my( %ALERTABLE_SERVICE_NAMES ) = (
'MSSQLSERVER' => 1,
'SQLSERVERAGENT' => 1,
'SQLBrowser' => 1,
'msftesql' => 1,
'MsDtsServer' => 1,
'ReportServer' => 1,
'TermService' => 1,
'W32Time' => 1,
'Eventlog' => 1,
'CqMgHost' => 1,
'CpqNicMgmt' => 1,
'CqMgServ' => 1,
'CqMgStor' => 1,
'SQLBackupAgent' => 1
);

$|=1;

use vars qw($SYSTEMS $DEBUG $ACTION $SERVICEREPORT $SERVICE $PATERN $RUNSTATE $CONFIGFILE $EXCLUDE_PAT $OUTFILE $ERRORLOG $BATCH_ID);

die usage("Bad Parameter List $!\n") unless
   GetOptions(
		"SYSTEMS=s"		=> \$SYSTEMS,
		"ACTION=s"		=> \$ACTION,
		"DEBUG"			=> \$DEBUG,
		"BATCHID=s"		=> \$BATCH_ID,
		"BATCH_ID=s"	=> \$BATCH_ID,
		"SERVICE=s"		=> \$SERVICE,
		"PATERN=s"		=> \$PATERN,
		"RUNSTATE=s"	=> \$RUNSTATE,
		"CONFIGFILE=s"	=> \$CONFIGFILE,
		"EXCLUDE_PAT=s"=> \$EXCLUDE_PAT,
		"SERVICEREPORT"=> \$SERVICEREPORT,
		"OUTFILE=s"		=> \$OUTFILE,
		"ERRORLOG=s"	=> \$ERRORLOG,
	);

usage("Must pass --SYSTEMS=ALL|Local|Systemname")
	unless defined $SYSTEMS or $SERVICEREPORT or $ACTION eq "VALIDATE" or $ACTION eq "SNAPSHOT" or $ACTION eq "SNAPSHOTUPDATE";
if( defined $OUTFILE ) {
	open(OUT, ">".$OUTFILE ) or die "Cant Write $OUTFILE : $! \n";
	output("PC Service Manager v1.0");
	output("Run At: ",(scalar localtime(time)),"\n\n");
}

print "Output=$OUTFILE\n"  	if defined $OUTFILE;
output( "Action=$ACTION\n"  )	if defined $ACTION;
output( "Systems=$SYSTEMS\n" ) 	if defined $SYSTEMS;
output( "Output File=$OUTFILE\n" ) 	if defined $OUTFILE;
output( "Error Log=$ERRORLOG\n" ) 	if defined $ERRORLOG;

unlink($ERRORLOG) if defined $ERRORLOG and -w $ERRORLOG;
sub usage {
	print @_;
	print "Usage: win32_service.pl  - show all nt services\n";
	print " --OUTFILE=outfile - save results to outfile \n";
	print " --DEBUG\n";
	print " --SYSTEMS=[system,system]\n";
	print "          if system=ALL will work on all systems in cfg file\n";
	print "          if system=Local will work on local system\n";
	print " --ERRORLOG=errorlog - errorlog\n";
	print " --SERVICE=service - show services matching \n";
	print " --PATERN=patern - show services matching patern\n";
	print " --EXCLUDE_PAT=patern - exclude services matching patern\n";
	print " --RUNSTATE=runstate - show services with state matching patern\n";
	print " --ACTION=action  - START|STOP|SNAPSHOT|SNAPSHOTUPDATE|VALIDATE\n";
	print "          if action is SNAPSHOT then a snapshot is taken\n";
	print "          if action is SNAPSHOTUPDATE then a snapshot new servers\n";
	print "          if action is VALIDATE then a validate vs snapshot\n";

	print "\nmany of the above can take csv list as an arg\n";
	exit();
}

$CONFIGFILE=get_conf_dir()."/win32_service.dat" unless $CONFIGFILE;
my(%servicestatus);
if( $SERVICEREPORT) {
	# parse configfile and go for it
	open(VALID,$CONFIGFILE) or die "Cant Read CONFIGFILE $CONFIGFILE $!\n";
	my(%servicename);
	while (<VALID>) {
		chop;chomp;chomp;
		next if /^\s*#/ or /^\s*$/;
		my($host,$svclong,$svc)=split(":",$_);
		$servicename{$svc}=$svclong;
	}
	foreach ( sort %servicename ) {
		my($x)="YES" if $ALERTABLE_SERVICE_NAMES{$_};
		printf  "%-4s %-25s - %s\n", $x, $_, $servicename{$_} unless $_ =~ /\$/;
	}
	exit();
}

if( $ACTION eq "VALIDATE" ) {
	MlpBatchJobStart(-BATCH_ID => $BATCH_ID)  if $BATCH_ID;

	print "VALIDATING SERVICE VS LIST IN $CONFIGFILE\n" if $DEBUG;
	if( ! -r $CONFIGFILE ) {
		print "win32_service.pl - unable to read your configfile\n";
		print "Configfile=$CONFIGFILE\n";
		print "Perhaps you have not run win32_service.pl --SNAPSHOT?\n";
		print "The above command is run by the gem batch win32_service_checker_init.ksh\n";
		MlpBatchJobErr("Cant find configfile $CONFIGFILE")  if $BATCH_ID;
		exit(0);
	}

	open(VALID,$CONFIGFILE) or die "Cant Read CONFIGFILE $CONFIGFILE $!\n";
	my($lasthost)="";
	while (<VALID>) {
		chop;chomp;chomp;
		next if /^\s*#/ or /^\s*$/;
		my($host,$svclong,$svc)=split(":",$_);
		my(%status);

		next unless $ALERTABLE_SERVICE_NAMES{$svc};

		#
		# get status third arg must be a hashref - its key CurrentState has one of 7 possible
		# values  1=stopped, 2=start pending, 3=stop pending, 4=started,
		#		 5=continue pending, 6=pause pending, and 7=paused
		Win32::Service::GetStatus($host,$svc,\%status);
		if( $DEBUG ) {
			print "HOST $host LONGNM $svclong SVC=$svc\n";
			foreach ( keys %status ) { print $_, " ",$status{$_},"\n"; }
		}
		my($current_state)=$service_status[$status{"CurrentState"}];
		if( ! $current_state ) {
			printf "      Service %s on %s No Longer Available\n",$svclong,$host;
			next;
		};
		print "\n",$host,": " if $DEBUG and $lasthost ne $host;
		$lasthost=$host;
		if( $current_state =~ /RUNNING/ ) {
			print "+" if $DEBUG;
			MlpHeartbeat(
				-state=>"OK",
			  	-monitor_program=>$BATCH_ID,
				-system=>$host,
				-subsystem=>$svc,
				-message_text=>"Service $svclong state=$current_state"
			) if $BATCH_ID;
		} else {
			print "\n\t$host - $svclong is $current_state" if $DEBUG;
			my($severity)="WARNING";
			$severity="ERROR" if $ALERTABLE_SERVICE_NAMES{$svc};
			printf "$severity Service %s on %s %s\n",$svclong,$host,$current_state;
			MlpHeartbeat(
				-state=>$severity,
			  	-monitor_program=>$BATCH_ID,
				-system=>$host,
				-subsystem=>$svc,
				-message_text=>"Service $svclong state=$current_state"
			) if $BATCH_ID;
		}
	}
	close(VALID);
	print "\nVALIDATION COMPLETED\n" if $DEBUG;
	MlpBatchJobEnd() if $BATCH_ID;
	exit();
}

MlpBatchJobStart(-BATCH_ID => $BATCH_ID)  if $BATCH_ID;

my(@saved_snapshot,%existing_snapped_hosts);
if( $ACTION eq "SNAPSHOT" or $ACTION eq "SNAPSHOTUPDATE" ) {
	$SYSTEMS="ALL" unless defined $SYSTEMS;
	print "Saving Snapshot into $CONFIGFILE\n";
	if( -r $CONFIGFILE ) {  # save results for later...
		open(V,$CONFIGFILE) or die "Cant Read CONFIGFILE $CONFIGFILE $!\n";
		while (<V>) {
			push @saved_snapshot, $_;
			my($host,$svclong,$svc)=split(":",$_);
			$existing_snapped_hosts{$host}=1;
		}
		close(V);
	}
	if( $ACTION eq "SNAPSHOTUPDATE" ) {
		open(SNAP,">>$CONFIGFILE") or die "Cant Append CONFIGFILE $CONFIGFILE : $!\n";
	} else {
		open(SNAP,">$CONFIGFILE") or die "Cant Write CONFIGFILE $CONFIGFILE : $!\n";
		if( -r "$CONFIGFILE.sample" ) {
			open(S2,"$CONFIGFILE.sample") or die "Cant read $CONFIGFILE.sample\n";
			while(<S2>) { print SNAP $_; }
			close(S2);
		}
	}
}

my(@systems);
if( $SYSTEMS eq "ALL" ) {
	@systems=get_password(-type=>'win32servers');
	my($xc)=$#systems+1;
	print "$xc systems retrieved from win32 password file\n";
	if( $ACTION eq "SNAPSHOT"  or $ACTION eq "SNAPSHOTUPDATE") {
		# delete old heartbeats
		print "Deleting Old Heartbeat Data \n";
		my(@rc)=MlpManageData( 		Admin=>"Delete By Monitor_Type",
						Monitor=>'PcServiceChecker',
						Operation=>'del',
						Type=>"Heartbeat");
		print join("\n",@rc),"\n";
	}
	@saved_snapshot = ();
} elsif( $SYSTEMS eq "Local" ) {
	push @systems," ";
} else {
	@systems=split(/,/,$SYSTEMS);
	if( $ACTION eq "SNAPSHOT"  or $ACTION eq "SNAPSHOTUPDATE") {
		# delete old heartbeats
		foreach my $system ( @systems ) {
			print "Deleting Data For $system\n";
			my(@rc)=MlpManageData( 		Admin=>"Delete By Row",
							Monitor=>'PcServiceChecker',
							Operation=>'del',
							System=>$system,
							Type=>"Heartbeat");
			print join("\n",@rc),"\n";
		}
		my(%s);
		foreach (@systems) { $s{$_}=1; }
		foreach (@saved_snapshot) {
			my($host,$svclong,$svc)=split(":",$_);
			next if $s{$host};
			print SNAP $_;
		}
	}
}

# read_cluster_configfile() if $ACTION eq "SNAPSHOT" or $ACTION eq "SNAPSHOTUPDATE";

my(%SNAPPED_SERVICES);
foreach my $system ( @systems ) {
	$system='' if $system eq " ";

	my %info = get_password_info(-name=>$system, -type=>'win32servers');
	if( $info{IS_CLUSTERNODE} eq "Y" ) {
		output( "SKIPPING CLUSTERNODE $system - WE DONT COLLECT LOGS ON CLUSTERNODES\n" );
		output( " -- clusternode detected from win32_passwords.dat file\n" );
		next;
	}

	if( $ACTION eq "SNAPSHOTUPDATE" and $existing_snapped_hosts{$system} ) {
		output( "\nSkipping $system\n" );
		next;
	}

	print "\nWorking on system $system\n" if defined $DEBUG;
	output( "\nWorking on system $system\n" );

	my(%services);
	Win32::Service::GetServices($system,\%services);

	if( ! defined $ACTION or $ACTION eq "SNAPSHOT"  or $ACTION eq "SNAPSHOTUPDATE") {

		# show services on $system
		#print "<TABLE>";
		#print "<TR><TH>SERVICE NAME</TH><TH>SERVICE STATE</TH></TR>\n";
		output( "\n" );
		my $x = sprintf( "%20s %38s %s\n","HOST","SERVICE NAME","SERVICE STATE" );
		output($x);
		my(@split_s)=split(/,/,$SERVICE) if defined $SERVICE;
		my(@split_p)=split(/,/,$PATERN) if defined $PATERN;
		my(@split_e)=split(/,/,$EXCLUDE_PAT) if defined $EXCLUDE_PAT;
		my($service_count)=0;

		foreach my $svc (sort keys %services) {
			my(%status);
			print "DBG: $system testing $svc \n" if defined $DEBUG;
			# next if defined $SERVICE and $svc !~ /^$SERVICE$/i;

			if( $IGNORABLE_SERVICE_NAMES{$svc} or $IGNORABLE_SERVICE_NAMES{$services{$svc}}) {
				print "$svc Excluded By Ignorelist\n" if $DEBUG;
				next;
			}

			if( defined $EXCLUDE_PAT ) {
				my($found)=0;
				foreach (@split_e) {
					$found++ if $svc =~ /$_/i;
				}
				print "$svc Excluded By Patern\n" if $found and defined $DEBUG;
				next if $found;
			}

			if( defined $SERVICE ) {
				my($found)=0;
				foreach (@split_s) {
					$found++ if $svc =~ /^$_$/i;
				}
				print "$svc Excluded By Service\n" if ! $found and defined $DEBUG;
				next unless $found;
			}

			# next if defined $PATERN and $svc !~ /$PATERN/i;
			if( defined $PATERN ) {
				my($found)=0;
				foreach (@split_p) {
					$found++ if $svc =~ /$_/i;
				}
#print "DBG: $svc - found=$found\n";
				print "$svc Excluded By PAT\n" if ! $found and defined $DEBUG;
				next unless $found;
			}

			Win32::Service::GetStatus($system,$services{$svc},\%status);
			my($current_state)=$service_status[$status{"CurrentState"}];
			die "ERROR NO CURRENT STATE FOR ",$status{"CurrentState"},"\n"
					unless defined $current_state;
			next if defined $RUNSTATE and $current_state !~ /$RUNSTATE/i;
			if( $ACTION eq "SNAPSHOT"  or $ACTION eq "SNAPSHOTUPDATE" ) {
				if( $current_state =~ /RUNNING/ ) {

					#next if $svc eq "SQLSERVERAGENT";

				#	my($is_clu)=is_clustername($system);
				#	if( $is_clu and $svc!~ /SQL Server/ and $svc!~/MSSQLSERVER/ ) {
				#		print "Skipping non Sql Service on Cluster Alias $system ($svc)\n";
				#		next;
				#	}

					my($clusternm);
					#if( $svc=~/MSSQLSERVER/ or $svc=~/SQL Server/ ) {
					#	$clusternm=get_nodes_clustername($system);
					#	print "Sql Services on $system will be managed on cluster $clusternm\n"
					#		if $clusternm ne $system;
					#} else {
					$clusternm=$system;
					#}

					#if( defined $SNAPPED_SERVICES{$clusternm.":".$services{$svc}} ) {
					#	print "Sql Service $svc on node $system will be managed on cluster $clusternm\n";
					#	next;
					# }

					$SNAPPED_SERVICES{$clusternm.":".$services{$svc}}=1;
					print "Writing: ",$clusternm,":",$svc,":",$services{$svc},"\n" if defined $DEBUG;
					print SNAP $clusternm,":",$svc,":",$services{$svc},"\n";
					if( $svc =~ /^MSSQLSERVER$/i ) {
						print SNAP $clusternm,":SQLSERVERAGENT:SQLSERVERAGENT\n";
					};
					$service_count++;
				}
			} else {

				my($x)=sprintf( "%20s %38s %s\n",$system,$svc,$current_state);
				if( $current_state =~ /RUNNING/i ) {
					errprint($system,$svc,$current_state,"ok");
				} else {
					errprint($system,$svc,$current_state,"ERROR");
				}
				output( $x );
			}
		}
		printf "%-2s services saved for $system\n",$service_count;
		#output( "</TABLE>" );

	} elsif( $ACTION =~ /START/i) {

		# fix case
		if( defined $SERVICE and ! defined $services{$SERVICE} ) {
        		foreach (keys %services) {
                		if( $_ =~ /^$SERVICE$/i ) {
                        		$SERVICE=$_;
                        		last;
                		}
        		}
		}

		die "No Service $SERVICE found to start"
			unless defined $services{$SERVICE};

	      	output( "Starting $SERVICE\n" );
	      	Win32::Service::StartService( $system, $SERVICE)
		 	or die "CANT START SERVICE $SERVICE $!";

	} elsif( $ACTION =~ /STOP/i) {

		# fix case
		if( defined $SERVICE and ! defined $services{$SERVICE} ) {
        		foreach (keys %services) {
                		if( $_ =~ /^$SERVICE$/i ) {
                        		$SERVICE=$_;
                        		last;
                		}
        		}
		}

		die "No Service $SERVICE found to start"
			unless defined $services{$SERVICE};

	      	output( "Stopping $SERVICE\n" );
	      	Win32::Service::StopService( $system, $SERVICE )
		 	or die "CANT STOP SERVICE $SERVICE $!";

	}
}
close(OUT);
MlpBatchJobEnd() if $BATCH_ID;
exit(0);

sub errprint {
	my($system,$svc,$current_state,$err)=@_;
	if( defined $ERRORLOG ) {
		open(ERR, ">>".$ERRORLOG ) or die "Cant Write $ERRORLOG : $! \n";
		print ERR (scalar localtime(time)),",",$system,",$err,",$svc,",",$current_state,"\n";
		close(ERR);
	}
}

sub output {
	return if $ACTION eq "SNAPSHOT"  or $ACTION eq "SNAPSHOTUPDATE";
	return if $ACTION eq "VALIDATE";
	if( defined $OUTFILE ) {
		print OUT @_;
	} else {
		print @_;
	}
}

# my(%GemNames,%ActiveNames,%SharedDrives,%NodeNames);
#sub get_nodes_clustername {
#	my($svr)=@_;
#	foreach my $clust ( keys %NodeNames ) {
#		foreach my $node ( split(/\s/,$NodeNames{$clust}) ) {
#			return $clust if $node eq $svr;
#		}
#	}
#	return $svr;
#}

#sub is_clustername {
#	my($svr)=@_;
#	return 1 if $NodeNames{$svr};
#	return 0;
#}

#sub read_cluster_configfile {
#	my($file)=get_conf_dir()."/win32_clusternodes.dat";
#	die "no $file found" 					unless -e $file;
#	die "cant read $file" 					unless -r $file;
#	die "cant write $file" 					unless -w $file;
#
#	open( C1,$file ) or die "Cant write $file $!\n";
#	my($cursvr);
#
#	while(<C1>) {
#		chop;chomp;
#		next if /^#/;
#		if( ! /^\s/ ) {
#			$cursvr=$_;
#		} else {
#			my($a,$b)=split(/: /,$_,2);
#			if( /^\tServerName: / ) {
#				$GemNames{$cursvr}=$b;
#			} elsif( /^\tActiveSystemName: / ) {
#				$ActiveNames{$cursvr}=$b;
#			} elsif( /^\tSharedDrive: / ) {
#				$SharedDrives{$cursvr}=$b;
#			} elsif( /^\tClusterNode: / ) {
#				$NodeNames{$cursvr}=$b;
#			} else {
#				die "Cant parse $_\n";
#			}
#		}
#	}
#	close(C1);
#}

__END__

=head1 NAME

win32_service.pl - service manager for NT

=head2 USAGE

Usage: win32_service.pl
Usage: win32_service.pl service
Usage: win32_service.pl START|STOP service

SERVICE:                             Alerter   SERVICE RUNNING
SERVICE:                              Apache   SERVICE RUNNING
SERVICE:                   COM+ Event System   SERVICE STOPPED
SERVICE:                     ClipBook Server   SERVICE STOPPED
SERVICE:                    Computer Browser   SERVICE RUNNING
etc...

=head2 DESCRIPTION

Show NT services.  Filters service if service passed (one argument).
Starts & Stops services if second argument is STOP or START.

=head2 USAGE

show all services

 win32_service.pl

start service

 win32_service.pl START service_name

stop service

 win32_service.pl STOP service_name

filter services

 win32_service service_name

show running/stopped services

 win32_service running|stopped

=cut


