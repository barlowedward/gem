#
# usage unix_metadata.pl  HOSTNAME  LOGIN 
# 

use strict;
use Getopt::Long;
use Net::myFTP;
use Repository;

use vars qw( $HOSTNAME $LOGIN $DEBUG );

my($VERSION)=1.0;

$|=1;

sub usage
{
	return "Usage: unix_metadata.pl --HOSTNAME=host --LOGIN=file --DEBUG\n";
}

my($COMMAND_RUN)=$0." ".join(" ",@ARGV);

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
   	"HOSTNAME=s"	=> \$HOSTNAME,
   	"LOGIN=s"			=> \$LOGIN,
		"DEBUG"			=> \$DEBUG
		 );

die "Must Pass --hostname" unless $HOSTNAME;

print "unix_metadata.pl version $VERSION\n";
print " -- working on host $HOSTNAME\n";

my(%HOSTINFO);		# key value pairs for this host

my(%CONNECTION_CONFIG);
$CONNECTION_CONFIG{Debug}=1 if $DEBUG;
$CONNECTION_CONFIG{Timeout}=90000;

# first test ssh connectivity

# test connectivity
my($unix_login,$unix_password)=get_password(-name=>$HOSTNAME,-type=>"unix");
if( ! $unix_login ) {
	print "No Login Found for $HOSTNAME\n";
}

if( ! $unix_password ) {
	print "No Password Found for $HOSTNAME\n";	
}

my(%svrargs)=get_password_info(-type=>"unix",-name=>$HOSTNAME);
my($method)=$svrargs{UNIX_COMM_METHOD};
print "connecting() via $method to $HOSTNAME\n";
my($host_connection) = Net::myFTP->new($HOSTNAME,%CONNECTION_CONFIG,METHOD=>$method)
               or die "Failed To Connect $HOSTNAME Object $@";
if( ! $host_connection ) {
	die  "Unable to connect to $HOSTNAME\n";
}

if( $unix_login ne "null" and $unix_password ne "null" ) {
	print "Connecting as login=$unix_login\n";
	my($rc,$err)=$host_connection->login($unix_login,$unix_password);
	die( "Cant FTP To host ($HOSTNAME) As $unix_login : $err \n") unless $rc;
}

$host_connection->ascii();

my($dir)="/export/home";
my($file_count)=0;
print "ftp\-\>cwd($dir) \n" if $DEBUG;
if( ! $host_connection->cwd($dir) ) {
	die "Cant cwd to $dir\n";
	return;
}
print "Directory $dir appears to exist\n" if $DEBUG;

my(%FILE_TIMES,%FILE_SIZES,@files);
print "ftp\-\>fspec(.) \n" if $DEBUG;
foreach ( $host_connection->fspec(".") ) {
	my($filenm,$filetime,$filesize,$filetype)=@$_;
	next if $filenm =~ /^\./;
	next unless $filetype eq "File";
	# next if $filepat and $filenm!~/$filepat/;
	$file_count++;
	$FILE_TIMES{$filenm}=(time-$filetime)/(60*60*24);  # convert to days
	$FILE_SIZES{$filenm}=$filesize;
	push @files,$filenm;
	print "$file_count) $filenm time=$FILE_TIMES{$filenm} size=$filesize\n" if $DEBUG;
}
print "   ",($#files+1)," Files found in directory $dir\n";

# run a bunch of commands

foreach (sort keys %HOSTINFO) {
	print $_,"\t",$HOSTINFO{$_},"\n";
}
