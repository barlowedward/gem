# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package GenericBatchJobPlugin;

@ISA = qw(Plugin);
use strict;
use BatchJobTab;
use CommonFunc;
use File::Basename;

use vars qw($VERSION);
$VERSION = ".01";

my($RUNFAST)=1;
#
################ FIXED CONSTANTS #######################
#
my($RFRM_Notebook_Tabname)="batchjobs";

# %MENUITEM_HASH should be a list of menu items to add - in the format separated by a slash
# only put the complete paths to the items which you want to see
# or it will generate a Cant locate object method error
my(%MENUITEM_HASH)= ( 
		"Tools/Refresh File List"		=>		"ACTION_REFRESH_FILELIST"
	);

#
# CONTEXT_HASH is a hash of buttons labels (keys) to 'contexts' (values)
#
my(%CONTEXT_HASH) = (
		"GEM JOBS" 		=> "EXP_GEM_JOBS",
		"CRON JOBS" 	=> "EXP_CRON_JOBS",
		"BACKUP PLANS" => "EXP_BACKUP_JOBS",
		"REPORTS" 		=> "EXP_REPORTS",
		"ERROR LOGS" 	=> "EXP_ERROR_LOGS"
	);

my($RFRM_Title)="BATCH JOB VALIDATOR\nRun & Manage GEM Batch Jobs";	

my($current_main_frame);		# the template tab
my($working_frame);				# the dynamic frame that contains results
my(%CONTEXT_HASH_INV);			# reversed CONTEXT_HASH
my($current_UserAction)="ACTION_BATCHJOB_RESULTS";
my($current_Context);			# This will end up being one of the values of CONTEXT_HASH
my($current_TreeItem)="";		# full item path for the selected item of the left side tree
my($save_button);					# the save button
######################################################
#
# LOCAL VARIABLES
#
######################################################
my($BatchJobTab);
my($job_scheduler);
my(%GEM_BatchJob_Names,%GEM_BackupPlan_Names);
my(%GEM_Logfiles,%GEM_Errfiles);							# from data/GEM_BATCHJOB_LOGS
my(%CRON_Logfiles, %CRON_Errfiles);						# from data/cronlogs or data/batchjob_logs
my(%BACKUP_DIRECTORIES, %BACKUP_ERRFILES);			# from data/BACKUP_LOGS + subdirs
######################################################

sub getTreeItems {
	my($package,$curContext)=@_;
	$package->statusmsg( "[$RFRM_Notebook_Tabname] getTreeItems($curContext)\n" );
	
	if( ! $CONTEXT_HASH_INV{$curContext} ) {
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Invalid Tree Type $curContext = Not Drawing Left Side\n" );
		return;
	}
	$current_Context = $curContext;	
	
	if( defined $$package{-modulename} ) {
		my(@rc);
		if( $curContext eq "EXP_GEM_JOBS" ) {			# GET STUFF ON GEM BATCH JOBS
			foreach ( keys %GEM_BatchJob_Names ) { push @rc, "/$_";		}				
		} elsif( $curContext eq "EXP_BACKUP_JOBS" ) {#BACKUPS
			foreach ( keys %BACKUP_DIRECTORIES ) { push @rc, "/$_";		}	
		} elsif( $curContext eq "EXP_ERROR_LOGS" ) {	# ERRORLOGS	
			push @rc,"/Cron";
			foreach ( keys %CRON_Errfiles ) { push @rc, "/Cron/".$_;	}
			push @rc,"/Gem";
			foreach ( keys %GEM_Errfiles ) { push @rc, "/Gem/".$_;	}	
			push @rc,"/Backup";
			foreach ( keys %BACKUP_ERRFILES ) { push @rc, "/Backup/".$_;	}				
		} elsif( $curContext eq "EXP_CRON_JOBS" ) {	# ERRORLOGS	
			foreach ( keys %CRON_Logfiles ) { push @rc, "/".$_;	}
			foreach ( keys %CRON_Errfiles ) { push @rc, "/".$_;	}			
		} elsif( $curContext eq "EXP_REPORTS" ) {	# ERRORLOGS	
			push @rc,"/Report_Execution_History";
			push @rc,"/Report_Batch_Summary";
			push @rc,"/Report_Backup_Summary";			
		}
		my(@ary)=sort @rc;
		return \@ary;
	}
	return undef;
}

sub do_browse {
	my($package,$context,$itm)=@_;
	$package->statusmsg( "[$RFRM_Notebook_Tabname] do_browse($context,$itm)\n" );
	
	# based on the context - figure out what to do.
	if( $context eq "EXP_GEM_JOBS" ) {
		
		$itm=~s/^\///g;							
		$package->handle_plugin_action('ACTION_BATCHJOB_PROPERTIES',$itm);
		
	} elsif( $context eq "EXP_CRON_JOBS" or $itm =~ /^\/cron\// ) {
		
		my($filename);
		$itm=~s/^\/cron//g;
		$itm=~s/^\///g;					
		foreach ( keys %CRON_Logfiles ) { 
			if( $itm eq $_ ) {
				$package->debugmsg( "[$RFRM_Notebook_Tabname] Logfile $_ -> $CRON_Logfiles{$_}\n" );					
				$filename=$CRON_Logfiles{$_}."/".$_;
				last;
			}
		}			
		if( ! $filename ) {
			foreach ( keys %CRON_Errfiles ) { 
				if( $itm eq $_ ) {
					$package->debugmsg( "[$RFRM_Notebook_Tabname] ErrFile $_ -> $CRON_Errfiles{$_}\n" );										
					$filename=$CRON_Errfiles{$_}."/".$_;
					last;
				}
			}
		}						
		$package->statusmsg( "[$RFRM_Notebook_Tabname] filename=$filename\n" );		
		$package->handle_plugin_action('ACTION_SHOW_FILE',$filename);		
		
	} elsif( $context eq "EXP_BACKUP_JOBS" ) {
		
		$itm=~s/^\///;
		$package->handle_plugin_action('ACTION_BACKUPJOB_PROPERTIES',$itm);				
		
	} elsif( $context eq "EXP_REPORTS" ) {
		
		$itm=~s/^\///;
		$package->handle_plugin_action('ACTION_REPORT',$itm);		
		
	} elsif( $context eq "EXP_ERROR_LOGS" ) {
		if( $itm =~ /^\/Gem\// ) {
			$itm =~ s/^\/Gem//;		
			my($d)=get_gem_root_dir()."/data/GEM_BATCHJOB_LOGS".$itm;
			$package->handle_plugin_action('ACTION_SHOW_FILE',$d);
		} elsif( $itm =~ /^\/Backup\// ) {
			$itm =~ s/^\/Backup//;
			$package->handle_plugin_action('ACTION_BACKUPJOB_PROPERTIES',$itm);				
		} elsif( $itm =~ /^\/Cron\// ) {
			$itm =~ s/^\/Cron//;
			
			my($d)=get_gem_root_dir()."/data/cronlogs".$itm;
			$d=get_gem_root_dir()."/data/batchjob_logs".$itm unless -r $d;
			$package->handle_plugin_action('ACTION_SHOW_FILE',$d);
		} else {
		}
	}
}
		
sub event_handler
{
	my($package)=shift;
	my(%args)=@_;

	return unless $args{-curtab} eq $RFRM_Notebook_Tabname
		or $CONTEXT_HASH_INV{$args{-context}}
		or $args{-eventtype} eq "menu"
		or $args{-eventtype} eq "expbutton";

	$package->statusmsg("[$RFRM_Notebook_Tabname] event_handler type=$args{-eventtype}");
	if( $args{-eventtype} eq "menu" ) {
		my($found)="FALSE";
		foreach ( keys %MENUITEM_HASH ) { $found="TRUE" if $args{-menuitem} eq $_; }
		return unless $found eq "TRUE";
		$package->statusmsg( "[$RFRM_Notebook_Tabname] menuclick($args{-menuitem})\n" );
		if( $args{-curtab} ne $RFRM_Notebook_Tabname ) {
    			$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() raising tab $RFRM_Notebook_Tabname\n" );
     			$package->ignore_events(1);
				$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
				$package->ignore_events(undef);
		}
		$package->handle_plugin_action($MENUITEM_HASH{$args{-menuitem}});
	} elsif( $args{-eventtype} eq "tabchange" ) {
		return if $args{-curtab} ne $RFRM_Notebook_Tabname;
		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() called on change tab to $args{-curtab}\n" );
	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {
		return unless $CONTEXT_HASH_INV{$args{-context}};
		$package->statusmsg( "[$RFRM_Notebook_Tabname] ",__LINE__," treebrowse(path=$args{-entrypath})\n" );
		if( $args{-curtab} ne $RFRM_Notebook_Tabname ) {
     		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() raising tab $RFRM_Notebook_Tabname\n" );
			$package->ignore_events(1);
			$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
			$package->ignore_events(undef)
		}
		$current_TreeItem=$args{-entrypath};
		$package->debugmsg( "[$RFRM_Notebook_Tabname] ctxt=$args{-context} item=$args{-entrypath}\n" );					
		$package->do_browse($args{-context},$args{-entrypath});				
	} elsif( $args{-eventtype} eq "treertclick" ) {
		return unless $CONTEXT_HASH_INV{$args{-context}};
		if( $args{-curtab} ne $RFRM_Notebook_Tabname ) {
     		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() raising tab $RFRM_Notebook_Tabname\n" );
			$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
		}
		my($clktext);
		($current_TreeItem, $clktext)=($args{-entrypath},$args{-clicktext});
		$package->statusmsg(  "[$RFRM_Notebook_Tabname] treertclick(Path=$current_TreeItem Txt=$clktext)\n" );
		$package->browse($current_TreeItem);
		if( $args{-curtab} ne $RFRM_Notebook_Tabname ) {
     		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() raising tab\n" );			
			$package->ignore_events(1);
			$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
			$package->ignore_events(undef)
		}	
		if( $args{-clicktext} eq "Show Batchjob Properties" ) {
			my($itm)=$current_TreeItem;
			$current_TreeItem=~ s/\///;
			$package->handle_plugin_action("ACTION_BATCHJOB_PROPERTIES", $itm);		
		} elsif( $args{-clicktext} eq "Show Batchjob Results" ) {
			my($itm)=$current_TreeItem;
			$current_TreeItem=~ s/\///;
			$package->handle_plugin_action("ACTION_BATCHJOB_RESULTS", $itm);		
		} elsif( $args{-clicktext} eq "Show Session Log" ) {
			$package->handle_plugin_action('ACTION_SHOW_FILE');
		} elsif( $args{-clicktext} eq "Show Error Log" ) {
			$package->handle_plugin_action('ACTION_SHOW_FILE');
		} elsif( $args{-clicktext} eq "Refresh List Of Files" ) {
			$package->handle_plugin_action("ACTION_REFRESH_FILELIST");		
		} elsif( $args{-clicktext} eq "Execute Batchjob" ) {	
			$package->handle_plugin_action("ACTION_EXECUTE");		
		} elsif( $args{-clicktext} eq "Show Plan Definition" ) {
		} elsif( $args{-clicktext} eq "Execute Backup Plan" ) {		
		} elsif( $args{-clicktext} eq "Show File Contents" ) {
			my($filename);
			$package->handle_plugin_action("ACTION_SHOW_FILE",$filename);		
		} elsif( $args{-clicktext} eq "Run Report" ) {
		} else {
			$package->unimplemented( "[$RFRM_Notebook_Tabname] non-programmed response to tree right click - $args{-clicktext}\n" );
			$package->handle_plugin_action($args{-clicktext});
		}
	} elsif( $args{-eventtype} eq "expbutton" ) {
		if( $CONTEXT_HASH_INV{$args{-context}} and $args{-curtab} ne $RFRM_Notebook_Tabname ) {
			$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() called\n" );
			$package->statusmsg( "[$RFRM_Notebook_Tabname] raising ".ucfirst($RFRM_Notebook_Tabname)." tab\n" );
			$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
		}
		$package->handle_plugin_action('ACTION_HELP', $current_Context);		
	} elsif( $args{-eventtype} eq "imgbutton" ) {
	}
}

sub getTreeRtclk {
	my($package,$curContext,$treeitmptr)=@_;
	$package->debugmsg(  "[$RFRM_Notebook_Tabname] getTreeRtclk(curtype=$curContext)\n" );
	return(undef) unless $CONTEXT_HASH_INV{$curContext};
	
	my(@rightclicks);
	foreach my $itm ( @$treeitmptr ) {
		my(@rtclick)=();		#	( "Item Help" );
		if ( $itm eq "/" ) {
			# push @rightclicks, \@rtclick;
		} else {
			my(@d)=split(/\//,$itm);
			
			if( $curContext eq "EXP_GEM_JOBS" ) {
				unshift @rtclick, "Show Batchjob Properties";
				unshift @rtclick, "Show Batchjob Results";
				unshift @rtclick, "Show Session Log";
				unshift @rtclick, "Show Error Log";
				unshift @rtclick, "Refresh List Of Files";
				unshift @rtclick, "Execute Batchjob";				
			} elsif( $curContext eq "EXP_BACKUP_JOBS" ) {
				unshift @rtclick, "Show Error Log";
				unshift @rtclick, "Show Session Logs";
				unshift @rtclick, "Show Plan Definition";
				unshift @rtclick, "Execute Backup Plan";				
			} elsif( $curContext eq "EXP_ERROR_LOGS" ) {	
				unshift @rtclick, "Show File Contents";
				unshift @rtclick, "Refresh List Of Files";								
			} elsif( $curContext eq "EXP_CRON_JOBS" ) {	
				unshift @rtclick, "Show File Contents";
				unshift @rtclick, "Refresh List Of Files";				
			} elsif( $curContext eq "EXP_REPORTS" ) {	
				unshift @rtclick, "Run Report";				
			}
				
#			if( $#d==1 ) {
#			} elsif( $#d==2 ) {
#				unshift @rtclick, "$d[2] Properties";				
#				unshift @rtclick, "Right_Click_Num_2 $d[2]";
#				unshift @rtclick, "Right_Click_Num_1 $d[2]";
#			} elsif( $#d==3 ) {
#				unshift @rtclick, "$d[3] Properties";
#				unshift @rtclick, "Right_Click_Num_A2 $d[2]";
#				unshift @rtclick, "Right_Click_Num_A1 $d[3] on $d[2]";
#			}
		}
		push @rightclicks, \@rtclick;
	}
	return \@rightclicks;
}

#####################################################################
# STANDARD FUNCTIONS
#####################################################################
sub getPluginPanes { 		return ($RFRM_Notebook_Tabname,"Options","Help"); }
sub getMenuItems {			return keys %MENUITEM_HASH;}
sub getExplorerButtons {	return %CONTEXT_HASH; }

# PLUGIN INITIALIZATION
sub init {
	my($package)=@_;
	$package->debugmsg("[$RFRM_Notebook_Tabname] Starting init()");
		
	# set up a reverse of the button hash
	foreach ( keys %CONTEXT_HASH ) { $CONTEXT_HASH_INV{$CONTEXT_HASH{$_}} = $_; }
	
	$package->read_all_files();	
	
	$current_main_frame=$package->clear_tab($RFRM_Notebook_Tabname);

	$current_main_frame->Label(
		-text=>$RFRM_Title,-relief=>'ridge' , -bg=>'white'
		)->pack( -side=>'top', -fill=>'x', -anchor=>'n');

	my($butfrm)=$current_main_frame->Frame()->pack( -fill=>'x', -side=>'top', -anchor=>'n');
	
	$BatchJobTab=new BatchJobTab( -toplevel=>$main::CURRENT_CONFIG{mainWindow}, -module_frame=>$current_main_frame );
 	$job_scheduler=main::global_data(-class=>'job_scheduler',-noprint=>1);
	
	foreach my $job_name (	sort keys %$job_scheduler ) {
		next if is_nt() and  $$job_scheduler{$job_name}->{ok_types} eq "unix";
		next if ! is_nt() and  $$job_scheduler{$job_name}->{ok_types} eq "nt";
		if( $$job_scheduler{$job_name}->{job_type} eq "backup" ) {
			$GEM_BackupPlan_Names{$job_name} = 1;
		} else {
			$GEM_BatchJob_Names{$job_name} = 1;
		};
#		_statusmsg( sprintf  "%22s %5s %11s %11s %6s\n",
#			$job_name,
#			$$job_scheduler{$job_name}->{ok_types},
#			$$job_scheduler{$job_name}->{job_type},
#			$$job_scheduler{$job_name}->{frequency},
#			$$job_scheduler{$job_name}->{run_type} );
	}

	$package->refresh_tree( $CONTEXT_HASH{"NORMAL JOBS"} );
	$package->debugmsg("[$RFRM_Notebook_Tabname] Completed init()");	
}

#sub get_file_batchnm {
#	my( $package, $ftype, $basename_treeitem )=@_;
#	if( $ftype eq 's' ) {		# sessionlog
#		if( $basename_treeitem =~ /\.log$/ ) {
#			return $basename_treeitem if $GEM_Logfiles{$basename_treeitem};
#			return undef;
#		} elsif( $basename_treeitem =~ /\.err$/ ) {
#			return $basename_treeitem if $GEM_Errfiles{$basename_treeitem};
#			return undef;
#		} else {
#			foreach ( keys %GEM_Logfiles ) { return $_ if $GEM_Logfiles{$f}; }	
#		}		
#	} elsif( $ftype eq 'e' ) {	# errorlog
#	}else{							# get batchjob
#	}
#	return undef;
#}

sub read_all_files {
	my($package)=@_;
	use Repository;
	my($d)=get_gem_root_dir()."/data/GEM_BATCHJOB_LOGS";
	%GEM_Logfiles=();
	%GEM_Errfiles=();
	$package->statusmsg( "[$RFRM_Notebook_Tabname] Reading Directory $d");	
	opendir(DIR,$d ) or die("Cant Open Directory $d For Listing\n");
	my(@files)=grep( !/^\./, readdir(DIR) );
	closedir(DIR);
	foreach my $file ( @files ) {
		next if -z "$d/$file";
		my($root) = $file;
		$root=~s/^Win_//;
		$root=~s/^Unix_//;
		if( $file =~ /\.log$/ ) {
			$root=~s/\.log//;
			$GEM_Logfiles{$file} = $root;
		} elsif ( $file =~ /\.err$/ ) {
			$root=~s/\.err//;
			$GEM_Errfiles{$file} = $root;
		}
	}	
	$package->statusmsg( "[$RFRM_Notebook_Tabname] Done Reading Directory $d");				

	%CRON_Logfiles=();
	%CRON_Errfiles=();
	$d=get_gem_root_dir()."/data/cronlogs";
	if( -d $d ) {
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Reading Directory $d");	
		opendir(DIR,$d ) or die("Cant Open Directory $d For Listing\n");
		my(@files)=grep( !/^\./, readdir(DIR) );
		closedir(DIR);
		foreach my $file ( @files ) {
			next if -z "$d/$file";
			if ( $file =~ /\.err$/ ) {
				$CRON_Errfiles{$file} = $d;
			} else {
				$CRON_Logfiles{$file} = $d;
			}			
		}		
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Done Reading Directory $d");				
	}
	
	$d=get_gem_root_dir()."/data/batchjob_logs";
	if( -d $d ) {
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Reading Directory $d");	
		opendir(DIR,$d ) or die("Cant Open Directory $d For Listing\n");
		my(@files)=grep( !/^\./, readdir(DIR) );
		closedir(DIR);
		foreach my $file ( @files ) {
			next if -z "$d/$file";
			if ( $file =~ /\.err$/ ) {
				$CRON_Errfiles{$file} = $d;
			} else {
				$CRON_Logfiles{$file} = $d;
			}			
		}		
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Done Reading Directory $d");				
	}
	
	$d=get_gem_root_dir()."/data/BACKUP_LOGS";
	if( -d $d ) {
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Reading Directory $d");	
		opendir(DIR,$d ) or die("Cant Open Directory $d For Listing\n");
		my(@files)=grep( !/^\./, readdir(DIR) );
		closedir(DIR);
		foreach my $file ( @files ) {
			next unless -d "$d/$file/sessionlog" and -r "$d/$file/sessionlog";
			next unless -d "$d/$file/errors" and -r "$d/$file/errors";
			$package->statusmsg( "[$RFRM_Notebook_Tabname] Reading Directory $d/$file");	
			$BACKUP_DIRECTORIES{$file}=1;
			
			if( ! $RUNFAST ) {
				opendir(DIR2,"$d/$file/errors" ) or die("Cant Open Directory $d/$file/errors For Listing\n");
				foreach ( grep( !/^\./, readdir(DIR2) ) ) {
					next if -z "$d/$file/errors/$_" or -M "$d/$file/errors/$_" > 7;
					$package->statusmsg( "[$RFRM_Notebook_Tabname] Error File $_\n" );
					$BACKUP_ERRFILES{$_}=$file;
				}
				closedir(DIR2);	
			}
		}		
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Done Reading Directory $d");				
	}
}

sub handle_plugin_action {
	my($package,$user_action,$action_arg)=@_;
	$package->debugmsg("[$RFRM_Notebook_Tabname] ==========================================");	
	$package->debugmsg("[$RFRM_Notebook_Tabname] handle_plugin_action(action=$user_action)");	
	main::busy();
	
	if( $user_action eq "ACTION_HELP" ) {		
		$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);		
		
		my($title,$text);
		if( $action_arg eq "EXP_GEM_JOBS" ) {			# GET STUFF ON GEM BATCH JOBS
			$title='Help Text For GEM JOBS Items';
		} elsif( $action_arg eq "EXP_BACKUP_JOBS" ) {#BACKUPS
			$title='Help Text For GEM JOBS Items';
		} elsif( $action_arg eq "EXP_ERROR_LOGS" ) {	# ERRORLOGS	
			$title='Help Text For GEM JOBS Items';
		} elsif( $action_arg eq "EXP_CRON_JOBS" ) {	# ERRORLOGS	
			$title='Help Text For GEM JOBS Items';
		} elsif( $action_arg eq "EXP_REPORTS" ) {	# ERRORLOGS	
			$title='Help Text For GEM JOBS Items';
		}
		$text=$title;
		$working_frame  = $current_main_frame->LabFrame(
			-label=>$title,
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
		
		my($rotext)  = $working_frame->Scrolled( 'ROText',
					-relief => "sunken",
					-wrap=>'none',
         		-bg=>'white',
         		-font=>'code',
         		-scrollbars => "osoe" )->pack(-expand=>1,-fill=>'both');         		
		
		$rotext->insert('end',"Properties Of Batch Job $action_arg\n");
		$rotext->insert('end',"=========================================================\n");
		$rotext->insert('end',"$text\n");
		$package->debugmsg("[$RFRM_Notebook_Tabname] File Read Done");				

	} elsif( $user_action eq "ACTION_SHOW_FILE" ) {		
		$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);
		$working_frame  = $current_main_frame->LabFrame(
			-label=>"Current Action = Show Contents of File $action_arg ",
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
		
		my($rotext)  = $working_frame->Scrolled( 'ROText',
					-relief => "sunken",
					-wrap=>'none',
         		-bg=>'white',
         		-font=>'code',
         		-scrollbars => "osoe" )->pack(-expand=>1,-fill=>'both');
         		
		$package->debugmsg("[$RFRM_Notebook_Tabname] handle_plugin_action(filename=$action_arg)");	
		if( ! -e $action_arg ) {
			$package->debugmsg("[$RFRM_Notebook_Tabname] File $action_arg Does Not Exist");	
			$rotext->insert('end',"File $action_arg Does Not Exist");
		} elsif( -r $action_arg ) {
			my(@statdat)=stat $action_arg;			
			$rotext->insert('end',"Contents of file $action_arg\n");
			$rotext->insert('end',"File Date: ".localtime($statdat[9])."\n");
			$rotext->insert('end',"=========================================================\n");
			open(F,$action_arg);
			foreach( <F> ) {	$rotext->insert('end',$_);	}
			close(F);
		} else {
			my(@statdat)=stat $action_arg;			
			$package->debugmsg("[$RFRM_Notebook_Tabname] File Not Readable");	
			$rotext->insert('end',"File $action_arg Not Readable");
			$rotext->insert('end',"File Date: ".localtime($statdat[9])."\n");			
		}
		
		$package->debugmsg("[$RFRM_Notebook_Tabname] Operation Completed");				
		
	} elsif( $user_action eq 'ACTION_REFRESH_FILELIST' ) {				
		$package->read_all_files();		
	} elsif( $user_action eq 'ACTION_EXECUTE_BATCH' ) {	

		$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);
		$working_frame  = $current_main_frame->LabFrame(
			-label=>"Execute  BatchJob = $action_arg",
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
		
		my($line)=$$job_scheduler{$action_arg}->{command};
		chomp $line;
		$line=~s/\s+$//;
		$line=~s./.\\.g;
		$line=~s.\\\\.\\.g;
		chomp $line;
		if( is_nt() ) {
			$line=~s/__PERL__/$main::CONFIG{NT_PERL_LOCATION}/g;
			$line=~s/__CODELOCATION__/$main::CONFIG{NT_CODE_LOCATION}/g;
			$line=~s.\/.\\.g;
		} else {
			$line=~s/__PERL__/$main::CONFIG{UNIX_PERL_LOCATION}/g;
			$line=~s/__CODELOCATION__/$main::CONFIG{UNIX_CODE_LOCATION}/g;
			$line=~s.\\.\/.g;
		}
		
   	my(%subargs);
   	$subargs{-showinwin}="BATCH RESULTS FOR $action_arg";
   	my($rc,$datptr)=RunCommand(
   		-cmd=>$line,
   		-mainwindow=>$main::CURRENT_CONFIG{mainWindow},
   		-statusfunc=>\&_statusmsg,
   		-printfunc =>\&_statusmsg,
   		#-ignore_string=>"^Installing",
   		-printhdr  => "[$action_arg] : ",
   		%subargs
		);
		
		$BatchJobTab->paint($action_arg,$working_frame); 
		
	} elsif( $user_action eq 'ACTION_BATCHJOB_PROPERTIES'  ) {	
		$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);		
		$working_frame  = $current_main_frame->LabFrame(
			-label=>"Current BatchJob = ".$action_arg." - Report = $user_action ",
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
		
		my($rotext)  = $working_frame->Scrolled( 'ROText',
					-relief => "sunken",
					-wrap=>'none',
         		-bg=>'white',
         		-font=>'code',
         		-scrollbars => "osoe" )->pack(-expand=>1,-fill=>'both');         		
		
		$rotext->insert('end',"Properties Of Batch Job $action_arg\n");
		$rotext->insert('end',"=========================================================\n");
		$package->debugmsg("[$RFRM_Notebook_Tabname] File Read Done");				

	} elsif( $user_action eq 'ACTION_BACKUPJOB_PROPERTIES'  ) {	
		$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);		
		$working_frame  = $current_main_frame->LabFrame(
			-label=>"Current Backup Job = ".$action_arg." - Report = $user_action ",
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
		
		my($rotext)  = $working_frame->Scrolled( 'ROText',
					-relief => "sunken",
					-wrap=>'none',
         		-bg=>'white',
         		-font=>'code',
         		-scrollbars => "osoe" )->pack(-expand=>1,-fill=>'both');         		
		
		$rotext->insert('end',"Properties Of Backup Job $action_arg\n");
		$rotext->insert('end',"=========================================================\n");
		$package->debugmsg("[$RFRM_Notebook_Tabname] File Read Done");				
		
	} elsif( $user_action eq 'ACTION_BATCHJOB_RESULTS'  ) {	
		$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);		
		$working_frame  = $current_main_frame->LabFrame(
			-label=>"Current BatchJob = ".$action_arg." - Report = $user_action ",
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
		
		my($rotext)  = $working_frame->Scrolled( 'ROText',
					-relief => "sunken",
					-wrap=>'none',
         		-bg=>'white',
         		-font=>'code',
         		-scrollbars => "osoe" )->pack(-expand=>1,-fill=>'both');         		
		
		$rotext->insert('end',"Results Of Batch Job $action_arg\n");
		$rotext->insert('end',"=========================================================\n");
		$package->debugmsg("[$RFRM_Notebook_Tabname] File Read Done");				
	
	} elsif( $user_action eq 'ACTION_REPORT'  ) {		
		$package->debugmsg("[$RFRM_Notebook_Tabname] Run Report $action_arg");				
		$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);		
		$working_frame  = $current_main_frame->LabFrame(
			-label=>"Report For $user_action ",
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
		
		my($rotext)  = $working_frame->Scrolled( 'ROText',
					-relief => "sunken",
					-wrap=>'none',
         		-bg=>'white',
         		-font=>'code',
         		-scrollbars => "osoe" )->pack(-expand=>1,-fill=>'both');         		
		
		$rotext->insert('end',"REPORT NAME: $action_arg\n");
		$rotext->insert('end',"=========================================================\n");
		$package->debugmsg("[$RFRM_Notebook_Tabname] Report Run Done");				
		
	} else {
		$package->statusmsg("[$RFRM_Notebook_Tabname] Uncoded Action $user_action");	
	}
	main::unbusy();					
	$current_UserAction = $user_action;
	$package->debugmsg("[$RFRM_Notebook_Tabname] Completed handle_plugin_action($user_action)");	
	$package->debugmsg("[$RFRM_Notebook_Tabname] ==========================================");		
}

1;

__END__

=head1 NAME

GenericBatchJobPlugin.pm - Batch Job Manager

=head2 DESCRIPTION

Batch Job Manager allows you to see the results of your batch jobs

=head2 CONTEXTS

	"GEM JOBS" 		=> "EXP_GEM_JOBS"			=> Lists /%GEM_BatchJob_Names
	"CRON JOBS" 	=> "EXP_CRON_JOBS",		=> Lists /%CRON_Logfiles /%CRON_Errfiles
	"BACKUPS" 		=> "EXP_BACKUP_JOBS",	=> Lists /%BACKUP_DIRECTORIES
	"REPORTS" 		=> "EXP_REPORTS",			=> Lists the reports
	"ERROR LOGS" 	=> "EXP_ERROR_LOGS"		=> Lists /Cron/%CRON_Errfiles /Gem/%GEM_Errfiles /Backup/%BACKUP_ERRFILES
	
=head2 ACTIONS
	
		ACTION_EXECUTE, BatchJob
		ACTION_REFRESH_FILELIST
		ACTION_BATCHJOB_PROPERTIES, Batchjob
		ACTION_BACKUPJOB_PROPERTIES, Backupjob
		ACTION_BATCHJOB_RESULTS, Batchjob
		ACTION_SHOW_FILE, Filename
		ACTION_REPORT, Report_Execution_History
		ACTION_REPORT, Report_Backup_Summary
		ACTION_REPORT,	Report_Batch_Summary				
	