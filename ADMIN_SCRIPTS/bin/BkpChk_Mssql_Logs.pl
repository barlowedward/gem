#!/apps/perl/linux/perl-5.8.2/bin/perl

# copyright (c) 2005-2007 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use MlpAlarm;
use Do_Time;
use Data::Dumper;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Win32::EventLog;
use Repository;

use vars qw( $NOALARM $NOCOLLECT $DIAGNOSTICS $BATCHID $HTML $DAYS $HOURS $DEBUG $DATABASE $NOSTDOUT $LISTEVENTS $SYSTEM $NOREPORT $OUTFILE);

$|=1;
my($VERSION)="1.0";

sub usage {
	print @_,"\n";
	return "Usage: BkpChk_Mssql_Logs.pl --HTML --DIAGNOSTICS --SYSTEM=system [--NOREPORT|--NOCOLLECT] --NOALARM --OUTFILE --DAYS=days --HOURS=hours --NOSTDOUT --LISTEVENTS --BATCHID=name --DEBUG";
}

sub error_out {
   my($msg)=join("",@_);
   MlpBatchJobErr($msg) unless $NOALARM;
   MlpEvent(
      -monitor_program=> $BATCHID,
      -system=> $BATCHID,
      -message_text=> $msg,
      -severity => "ERROR"
   ) if $BATCHID and ! $NOALARM;
   die $msg;
}

die usage("Bad Parameter List $!\n") unless
	GetOptions(
			"HOURS=s"		=> \$HOURS,
			"DAYS=s"			=> \$DAYS,
			"SYSTEM=s"		=> \$SYSTEM,
			"SERVER=s"		=> \$SYSTEM,
			"DATABASE=s"	=> \$DATABASE,
			"LISTEVENTS"	=> \$LISTEVENTS,
			"NOSTDOUT"		=> \$NOSTDOUT,
			"NOREPORT"		=> \$NOREPORT,
			"NOCOLLECT"		=> \$NOCOLLECT,
			"NOALARM"		=> \$NOALARM,
			"BATCHID=s"		=> \$BATCHID,
			"BATCH_ID=s"		=> \$BATCHID,
			"OUTFILE=s"		=> \$OUTFILE,
			"HTML"			=> \$HTML,
			"DEBUG"			=> \$DEBUG,
			"DIAGNOSTICS"	=> \$DIAGNOSTICS,
	 );

$BATCHID="MssqlBackupCrosscheck" unless $BATCHID;
my($NL)="\n";
$NL="<br>\n" if $HTML;
if( defined $DAYS ) {
	$HOURS=0 unless defined $HOURS;
	$HOURS+=24*$DAYS;
}

$NOREPORT=1 if $SYSTEM;

MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor') if $BATCHID and ! $NOALARM;

my($tmpfile);
if( defined $OUTFILE ) {
	$tmpfile=get_tempfile();
	open(OUTF,">".$tmpfile) or die "Cant write to $tmpfile\n";
}

if( $BATCHID ) {
	output( "BATCH $BATCHID (BkpChk_Mssql_Logs.pl)" );
} else {
	output( "BkpChk_Mssql_Logs.pl" );
}
output( " : started at ".localtime(time).$NL );
output( "Batch will collect sql server event log data for $HOURS hours".$NL ) if $HOURS;
output( "Output stored in $OUTFILE".$NL) if $OUTFILE;

my(@sqlsvrs);
if( defined $SYSTEM ) {
	push @sqlsvrs,$SYSTEM;
} else {
	@sqlsvrs = get_password(-type=>'sqlsvr');
}
$SYSTEM='' unless defined $SYSTEM;

my(%EVENTS_COLLECTED_BYSYS);
if( ! $NOCOLLECT ) {
foreach my $CURSYSTEM ( @sqlsvrs ) {

	$EVENTS_COLLECTED_BYSYS{$CURSYSTEM}=0;

	my( %V_BACKUP_TIME, %V_BACKUP_FILE, %V_BACKUP_LSN, %V_TRANDUMP_TIME, %V_TRANDUMP_FILE,
		%V_TRANDUMP_LSN, %V_RESTORE_TIME, %V_RESTORE_FILE, %V_RESTORE_LSN, %V_TRANLOAD_TIME,
		%V_TRANLOAD_FILE, %V_TRANLOAD_FILETIME, %V_TRANLOAD_LSN, %V_LOGTRUNCATED );

	my($server_type,@databaselist);
	if( defined $HOURS and $HOURS>24 ) {
		my($login,$pass)=get_password(-type=>'sqlsvr',-name=>$CURSYSTEM);
	   print "Connecting to $CURSYSTEM".$NL if defined $DEBUG;

		# connect and fetch db list then disconnect
		my($rc)=dbi_connect(-srv=>$CURSYSTEM,-login=>$login,-password=>$pass);
		if( ! $rc ) {
			print "ERROR: Cant connect to Database $CURSYSTEM as $login\n";
			next;
	   }
		print "Connected\n" if defined $DEBUG;

		($server_type,@databaselist)=dbi_parse_opt_D();
		dbi_disconnect();
	}

	#my(@databases)=get_db($CURSYSTEM);
   output( $NL );
	output( "Working on system $CURSYSTEM".$NL );
	output( $NL );
	my($alarmlvl)="ok";
	my($alarmmsg)="";
	my $handle = Win32::EventLog->new("Application",$CURSYSTEM )
			or error_out( "Cant open Application Eventlog on $CURSYSTEM" );

	my($base,$recs);
	$handle->GetOldest($base);
	$handle->GetNumber($recs);
	$Win32::EventLog::GetMessageText=1;		# formatted messages

	my($recno)=1;
	my($hashRef);
	my($numrecread)=0;
	my(%dblist);

	#print "DBGDBG GetOldest=$base GetNumber=$recs recno=$recno\n";
	output( "DBG DBG: GetOldest=$base GetNumber=$recs recno=$recno".$NL ) if $DIAGNOSTICS;

	my($lasttime) = time + 10000;	# cant go forward in time!
	# print "DBGDBG: Setting lasttime to $lasttime\n";

	# want to force at least one record if we can...
	while( $recno++<$recs or $EVENTS_COLLECTED_BYSYS{$CURSYSTEM}!=0 ) {
		$EVENTS_COLLECTED_BYSYS{$CURSYSTEM}++;

		# read sequentially from most recent
		#output( "DBG DBG: Calling Read() On Event Log".$NL ) if $DIAGNOSTICS;
		my($rc) = $handle->Read(EVENTLOG_BACKWARDS_READ|EVENTLOG_SEQUENTIAL_READ,undef,$hashRef);
		if( ! $rc ) {
			output("DBGDBG: Unable to retrieve record",$NL) if $DIAGNOSTICS;
			next;
		}

		# skip login succeeded / failed audits here for performance reasons
		my($eid) = $$hashRef{'EventID'} & 0xffff;		# Make Event Id Readable
		if( $eid==17055 ) {
			next if  $$hashRef{Strings} =~ /^18453/;
			next if  $$hashRef{Strings} =~ /^18454/;
		}

		output( "DBG DBG: Non Login Event $eid".$NL ) if $DIAGNOSTICS;

		if( $recs < $recno ) {	# shouldnt happen... this is an end condition
			output("LAST RECORD FOUND BASED ON LOG FILE OVERFLOW\n");
			last;
		}

		print "DBGDBG: Comparing ",$$hashRef{TimeGenerated}," to $lasttime\n" if $DIAGNOSTICS;
		if( $lasttime < $$hashRef{TimeGenerated} ) {
			# how odd??? time increased!!! means the events are not sequential
			print "HOW ODD: THE TIME INCREASED $lasttime $$hashRef{TimeGenerated} \n";
			print "\tLast=",scalar localtime($lasttime),"\n";
			print "\tNext=",scalar localtime($$hashRef{TimeGenerated}),"\n";
			$lasttime = $$hashRef{TimeGenerated};
			# next;
		} else {
			$lasttime = $$hashRef{TimeGenerated};
		}

		output( "DBG DBG: base=$base recs=$recs recno=$recno time=".localtime($$hashRef{TimeGenerated}).$NL ) if $DIAGNOSTICS;
		output( "LINE".__LINE__." => INPUT MSG=".$$hashRef{Message}.$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;

		# when the time limit has been reached... quit

		my($rowage_hours)= time - $$hashRef{TimeGenerated};
		$rowage_hours /= 60;
		$rowage_hours /= 60;
		$rowage_hours = int $rowage_hours;
		#if( defined $HOURS and time - $$hashRef{TimeGenerated} > $HOURS*60*60 ) {
		if( defined $HOURS and $rowage_hours > $HOURS ) {
			output( "TIME LIMIT EXCEEDED - HOURS=$HOURS TIME=".((time-$$hashRef{TimeGenerated})/3600).$NL )
			 	if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
      	last;
      }

		# put message into standard format
		output( "LINE".__LINE__." => Calling process_win32_event()".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
      my(%OUTPUT)=process_win32_event($hashRef,$DEBUG,\&wanted);
      output( "LINE".__LINE__." => Done process_win32_event()".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
      if( $OUTPUT{IsError} eq "TRUE" ) {
      	output( "LINE".__LINE__." => IsError=TRUE".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
   		next if $OUTPUT{ErrorMsg} =~ /\(ignored\)/;

   		output( "LINE".__LINE__." => INPUT HASH=".Dumper($hashRef).$NL ) if $DIAGNOSTICS;
      	output( "LINE".__LINE__." => OUTPUT HASH=".Dumper(\%OUTPUT).$NL ) if $DIAGNOSTICS;

      	output( sprintf("%-55s : %s%s",
      					"[".$CURSYSTEM."] Skipping Event $OUTPUT{EventID} ".$OUTPUT{ErrorMsg},
      					do_time(-fmt=>"mm/dd hh:mi:ss",-time=>$OUTPUT{TimeGenerated}),
      					$NL ))	if defined $DEBUG or defined $LISTEVENTS;

			output( sprintf("%-70.70s %s","========> ". $OUTPUT{MessageText}, $NL ))
				if defined $DEBUG or defined $LISTEVENTS;
      	#output( Dumper $hashRef );
      	#output( Dumper \%OUTPUT );
      	next;
      }

	   output( "LINE".__LINE__." => READ MESSAGE NUMBER $recno OF $recs FROM EVENTLOG $NL" ) if $DIAGNOSTICS;

		$numrecread++;
      output( "[$numrecread event records read from $CURSYSTEM - ".localtime($$hashRef{TimeGenerated})." age in hours = $rowage_hours vs $HOURS ]".$NL ) if $numrecread%1000==0;

		output( "LINE".__LINE__." => INPUT HASH=".Dumper($hashRef).$NL ) 	if $DIAGNOSTICS;
      output( "LINE".__LINE__." => OUTPUT HASH=".Dumper(\%OUTPUT).$NL ) if $DIAGNOSTICS;

		my($m) = $OUTPUT{EventID};
		my($s) = $OUTPUT{MessageText};

		output( "LINE".__LINE__." => Evid=$m Msg=$s".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;

		if( ! defined $OUTPUT{Database} ) {
			output( "Skipping Message As Database Not Defined\n" );
			next;
		}
		if( ! defined $OUTPUT{op} ) {
			output( "Skipping Message As Operation Not Defined\n" );
			next;
		}
		if( defined $DATABASE and $OUTPUT{Database} ne $DATABASE ) {
			output( "Skipping Message As Database $OUTPUT{Database} != $DATABASE]\n" );
			next;
		}

		if( $OUTPUT{severity} eq "ERROR" and $m != 18278  ) {
			output("Skipping Error Message\n") if $DEBUG;
      	next;
		} elsif( $m >= 18264 and $m<=18271 ) {

			$dblist{$OUTPUT{Database}}=1;

			output( "LINE".__LINE__." => Found Backup Event".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
			output( sprintf("%-55s : %s%s","[".$CURSYSTEM."] ".$m.": ".$OUTPUT{Database}." ".$OUTPUT{op},
					do_time(-fmt=>"mm/dd hh:mi:ss",-time=>$OUTPUT{TimeGenerated}),
					$NL ))
				if defined $DEBUG or defined $LISTEVENTS;

			if( $m == 18264 ) {			# db backup
				if( ! defined $V_BACKUP_TIME{$OUTPUT{Database}} ) {
					output( "LINE".__LINE__." => Found Backup Event $m".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
					$V_BACKUP_TIME{$OUTPUT{Database}} = $OUTPUT{TimeGenerated};
					$V_BACKUP_FILE{$OUTPUT{Database}} = $OUTPUT{directory}."/".$OUTPUT{device};
					$V_BACKUP_LSN{$OUTPUT{Database}}  = $OUTPUT{lastLSN};
				}
			} elsif( $m == 18265 ) {	# log backup
				if( ! defined $V_TRANDUMP_TIME{$OUTPUT{Database}} ) {
					output( "LINE".__LINE__." => Found Backup Event $m".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
					$V_TRANDUMP_TIME{$OUTPUT{Database}} = $OUTPUT{TimeGenerated};
					$V_TRANDUMP_FILE{$OUTPUT{Database}} = $OUTPUT{directory}."/".$OUTPUT{device};
					$V_TRANDUMP_LSN{$OUTPUT{Database}}  = $OUTPUT{lastLSN};
				}
			} elsif( $m == 18266 ) {	# db file backup
			} elsif( $m == 18269 ) {	# file restore
			} elsif( $m == 18267 ) {	# db restored
				if( ! defined $V_RESTORE_TIME{$OUTPUT{Database}} ) {
					output( "LINE".__LINE__." => Found Backup Event $m".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
					$V_RESTORE_TIME{$OUTPUT{Database}} = $OUTPUT{TimeGenerated};
					$V_RESTORE_FILE{$OUTPUT{Database}} = $OUTPUT{directory}."/".$OUTPUT{device};
					$V_RESTORE_LSN{$OUTPUT{Database}}  = $OUTPUT{lastLSN};
				}
			} elsif( $m == 18268 ) {	# log restored
				if( ! defined $V_TRANLOAD_TIME{$OUTPUT{Database}} ) {
					if( $DEBUG ) {
						foreach ( keys %OUTPUT ) {
							print "DBGDBG ",__LINE__," $_ => ",$OUTPUT{$_},$NL;
						}
						output("New Restore of $OUTPUT{Database}: $NL\tTime=$OUTPUT{TimeGenerated}$NL\tFile=$OUTPUT{device}$NL\tAt Time=$OUTPUT{filetime} (".localtime($OUTPUT{filetime}).")".$NL);
					}
					output( "LINE".__LINE__." => Found Backup Event $m".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;
					$V_TRANLOAD_TIME{$OUTPUT{Database}} = $OUTPUT{TimeGenerated};
					$V_TRANLOAD_FILE{$OUTPUT{Database}} = $OUTPUT{directory}."/".$OUTPUT{device};
					$V_TRANLOAD_FILETIME{$OUTPUT{Database}} = $OUTPUT{filetime};
					$V_TRANLOAD_LSN{$OUTPUT{Database}}  = $OUTPUT{lastLSN};

				}
			} elsif( $m == 18270 ) {	# diff backup
			} elsif( $m == 18271 ) {	# db diff restore
			}
		} elsif( $m == 18278 ) {	# log truncated
			$s=~s/[\r\n]//g;
			$m=~s/[\r\n]//g;
			output( "($m) $OUTPUT{severity} $s".$NL ) if defined $DEBUG;
			$s=~ s/:Database log truncated: Database: //;
			$s=~ s/\s//g;
			$s=~ s/\.//g;

			$V_LOGTRUNCATED{$s} = $OUTPUT{TimeGenerated};
			output( "($m) $s".$NL ) if defined $LISTEVENTS;
		}
#		} elsif( $m == 18204 ) {
#			output( "($m) $s".$NL ) if defined $LISTEVENTS;
#		} elsif( $m == 17550 or  $m==17551 ) {			# dbcc traceon
#		} elsif( $m >= 18450 and $m<=18470 ) {			# Login Succeeded/fail
#		} elsif( $m == 18100 ) {							# user killed
#		} elsif( $s =~ /^\s*$/ ) {
#			output( "(no msgno) $m".$NL ) if defined $LISTEVENTS;
#		} elsif( $m >= 18000 and $m<19000 ) {
#			output( "($m) $s".$NL );
#		} elsif( $m >= 8900 and $m<8999 ) {	# dbcc error
#		} elsif( $m == 17052 ) {		# device activation error
#		} elsif( $m == 5145 ) {			# autogrow delayed
#		} elsif( $m == 3454 ) {			# recovery checkpoint
#		} elsif( $m == 380 ) {			# process id
#		} else {
#			next unless $DEBUG;
#			my($msg)="\n\tTime:".(scalar localtime($OUTPUT{TimeGenerated}))."\n\tComputer:".
#					$$hashRef{Computer}."\n\tTag:".
#					$OUTPUT{severity}.",".
#					$$hashRef{Source}."\n\tSource:".
#					$s;
#			output( "=================================",$NL,"LINE: ",__LINE__," Error: * unknown message $m ",$NL,"Dumper Results=".Dumper($hashRef).$NL."TimeGenerated=".localtime($OUTPUT{TimeGenerated}).$NL,"=================================",$NL, ) if $DEBUG;
#		}
	}
	output( "DBG DBG: NumRecsTotal=$EVENTS_COLLECTED_BYSYS{$CURSYSTEM} \n" ) if $DIAGNOSTICS;
	output( "[completed server read: $numrecread event records]".$NL );

	$handle->Close();
	foreach (@databaselist) {
		next if $_ eq "model" or $_ eq "tempdb" or $dblist{$_};
		$dblist{$_}=1;
		output( "Found Database $_ on system $CURSYSTEM".$NL );
	}

	# save the info
	foreach my $db ( sort keys %dblist ) {
		next if $db eq "model" or $db eq "tempdb";
		output( "LINE".__LINE__." => Saving Db Info for $db ".$NL ) if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;

		$V_BACKUP_TIME{$db} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_BACKUP_TIME{$db})
			if $V_BACKUP_TIME{$db};
		$V_TRANDUMP_TIME{$db} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_TRANDUMP_TIME{$db})
			if $V_TRANDUMP_TIME{$db};
		$V_RESTORE_TIME{$db} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_RESTORE_TIME{$db})
			if $V_RESTORE_TIME{$db};
		$V_TRANLOAD_TIME{$db} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_TRANLOAD_TIME{$db})
			if $V_TRANLOAD_TIME{$db};
		$V_LOGTRUNCATED{$db} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_LOGTRUNCATED{$db})
			if $V_LOGTRUNCATED{$db};

		my($d)=$DEBUG;
		$d=1  if $DIAGNOSTICS and $CURSYSTEM eq $SYSTEM;

		print "Saving BackupState for $CURSYSTEM / $db \n";
		print "   -last_fulldump_time     => $V_BACKUP_TIME{$db} \n"    if $V_BACKUP_TIME{$db};
      print "   -last_fulldump_file     => $V_BACKUP_FILE{$db} \n"    if $V_BACKUP_FILE{$db};
      print "   -last_fulldump_lsn      => $V_BACKUP_LSN{$db} \n"       if $V_BACKUP_LSN{$db};
      print "   -last_trandump_time     => $V_TRANDUMP_TIME{$db} \n"    if $V_TRANDUMP_TIME{$db};
      print "   -last_trandump_file     => $V_TRANDUMP_FILE{$db} \n"    if $V_TRANDUMP_FILE{$db};
      print "   -last_trandump_lsn      => $V_TRANDUMP_LSN{$db} \n"    if $V_TRANDUMP_LSN{$db};
      print "   -last_fullload_time     => $V_RESTORE_TIME{$db} \n"    if $V_RESTORE_TIME{$db};
      print "   -last_fullload_file     => $V_RESTORE_FILE{$db} \n"    if $V_RESTORE_FILE{$db};
      print "   -last_fullload_lsn      => $V_RESTORE_LSN{$db} \n"    if $V_RESTORE_LSN{$db};
      print "   -last_tranload_time     => $V_TRANLOAD_TIME{$db} \n"    if $V_TRANLOAD_TIME{$db};
      print "   -last_tranload_file     => $V_TRANLOAD_FILE{$db} \n"    if $V_TRANLOAD_FILE{$db};
      print "   -last_tranload_lsn      => $V_TRANLOAD_LSN{$db} \n"    if $V_TRANLOAD_LSN{$db};
      print "   -last_tranload_filetime => $V_TRANLOAD_FILETIME{$db} \n" if $V_TRANLOAD_FILETIME{$db};
      print "   -last_truncation_time   => $V_LOGTRUNCATED{$db} \n"    if $V_LOGTRUNCATED{$db};

		output( MlpSetBackupState(
			-system					=>	$CURSYSTEM,
			-dbname					=>	$db,
			-debug					=> $d,
			-last_fulldump_time 	=> $V_BACKUP_TIME{$db},
			-last_fulldump_file 	=> $V_BACKUP_FILE{$db},
			-last_fulldump_lsn 	=> $V_BACKUP_LSN{$db},

			-last_trandump_time 	=> $V_TRANDUMP_TIME{$db},
			-last_trandump_file 	=> $V_TRANDUMP_FILE{$db},
			-last_trandump_lsn 	=> $V_TRANDUMP_LSN{$db},

			-last_fullload_time 	=> $V_RESTORE_TIME{$db},
			-last_fullload_file 	=> $V_RESTORE_FILE{$db},
			-last_fullload_lsn 	=> $V_RESTORE_LSN{$db},

			-last_tranload_time 	=> $V_TRANLOAD_TIME{$db},
			-last_tranload_file 	=> $V_TRANLOAD_FILE{$db},
			#-xxx => $V_TRANLOAD_FILETIME{$db},
			-last_tranload_lsn 	=> $V_TRANLOAD_LSN{$db},
			-last_tranload_filetime => $V_TRANLOAD_FILETIME{$db},

			-last_truncation_time => $V_LOGTRUNCATED{$db},
			-file_time => do_time( -fmt => "yyyy/mm/dd hh:mi:ss")
			));
	}
}
output( "Completed read from windows eventlogs at ".localtime(time).$NL );
} else {
	output( "Skipped read from windows eventlogs at ".localtime(time).$NL );
};

#if( ! $NOREPORT ) {
#	# PART 1 UPDATE BACKUP STATE
#	output( $NL.$NL."SQL SERVER ALARMS FETCH COMPLETED - UPDATING BACKUP STATE STATIC INFO".$NL.$NL );
#	my($rootdir)=get_gem_root_dir();
#	my($cmd)="$^X  ".$rootdir."/ADMIN_SCRIPTS/bin/BkpChk_UpdateStaticInfo.pl  --BATCH_ID=$BATCHID --TYPE=sqlsvr";
#	$cmd .= " --DEBUG" if $DEBUG;
#	output( "RUNNING $cmd".$NL.$NL );
#	open(OUTA, "$cmd |" ) or die "Cant Run Command\n";
#	while(<OUTA>) { output("> ".$_); }
#	close(OUTA);
#	output($NL);
#
#	# PART 2 REPORT ON IT
#	output( $NL.$NL."SQL SERVER STATIC INFO FETCH COMPLETED - CALLING CROSSCHECK REPORT".$NL.$NL );
#	my($rootdir)=get_gem_root_dir();
#	my($cmd)="$^X  ".$rootdir."/ADMIN_SCRIPTS/bin/BkpChk_Report.pl --SQLSVRONLY -NOSTDOUT --ERROR_REPORT=$rootdir/data/html_output/MssqlBackupCrosscheckErrors.html 	--ALL_REPORT=$rootdir/data/html_output/MssqlBackupCrosscheckAll.html --BATCHID=$BATCHID --TRANHOURS=2";
#	$cmd .= " --DEBUG" if $DEBUG;
#	$cmd .= " --NOALARM" if $NOALARM;
#	output( "RUNNING $cmd".$NL.$NL );
#	open(OUTX, "$cmd |" ) or die "Cant Run Command\n";
#	while(<OUTX>) { output("> ".$_); }
#	close(OUTX);
#	output($NL);
#
#	if( $OUTFILE ) {
#		output("Renaming $tmpfile to $OUTFILE".$NL);
#		output( "Program Completed at ".localtime(time).$NL );
#		close(OUTF);
#		unlink $OUTFILE if -r $OUTFILE;
#		rename($tmpfile,$OUTFILE) or warn("ERROR renaming $tmpfile to $OUTFILE: $!\n");
#		unlink $tmpfile;
#	}
#} else {
#	output( $NL,"NOT RUNNING REPORTS PER COMMAND LINE DIRECTIVE\n");
#	output( "Program Completed at ".localtime(time).$NL );
#}
MlpBatchJobEnd() unless $NOALARM;

foreach ( keys %EVENTS_COLLECTED_BYSYS ) {
	output( "WARNING - NO ROWS COLLECTED FOR $_", $NL ) if $EVENTS_COLLECTED_BYSYS{$_}==0;
	if( $BATCHID and ! $NOALARM ) {
		MlpHeartbeat(
				-monitor_program	=>	'MssqlBackupCrosscheck',		# hardcoded so it works for both weekly and daily jobs
			  	-system			=>	$_,
				-state			=>	"ERROR",
			  	-message_text	=>	"LAST Collected $EVENTS_COLLECTED_BYSYS{$_} Crosscheck Events At ".localtime(),
			  	-heartbeat=>(24*60),			# THIS WOULD BE 24 HOURS!
			  #-debug=>1
		) unless $EVENTS_COLLECTED_BYSYS{$_}==0;		# no rows read - must be something wrong... dont refresh heartbeat
																	# and count it as a miss...
	}
}
exit(0);

sub to_dhm {
	my($t)= @_;
	return "" unless defined $t;
	return "" if $t>1000000;
	my $d= int($t/(24*3600)); $t-=$d*(24*3600);
	my $h= int($t/3600); $t-=$h*3600;
	my $m= int($t/60); $t-=$m*60;
	return "(".$d."D ".$h."H ".$m."M)";
}

sub output {
	my(@msg)=@_;
	print OUTF @msg if defined $OUTFILE;
	print @msg  unless defined $NOSTDOUT;
}

#my(%IS_SQL_SERVER_2000);

# PROCESS EVENTLOG ROW - RETURNING A CLEAN HASH OF THE VALUES
sub process_win32_event {
	my($hashRef,$DEBUG,$FUNCREF)=@_;
	my(%OUTPUT);
	output( "LINE ",__LINE__," process_win32_event() ",Dumper($hashRef),"\n" ) if $DIAGNOSTICS;

	$$hashRef{'EventID'} &= 0xffff;		# Make Event Id Readable
	output( "LINE ",__LINE__," NEW EVENT ID=",$$hashRef{'EventID'},"\n" ) if $DIAGNOSTICS;

	$OUTPUT{IsError}="FALSE";
	$OUTPUT{TimeGenerated} = 	$$hashRef{TimeGenerated};
	$OUTPUT{TimeGeneratedTxt} = localtime($$hashRef{TimeGenerated});
	$OUTPUT{EventID} = $$hashRef{'EventID'};

   if( $$hashRef{EventType} == EVENTLOG_INFORMATION_TYPE ) {
     	$OUTPUT{severity}="INFORMATION";
   } elsif( $$hashRef{EventType} == EVENTLOG_ERROR_TYPE ) {
		$OUTPUT{severity}="ERROR";
	} elsif( $$hashRef{EventType} == EVENTLOG_WARNING_TYPE ) {
		$OUTPUT{severity}="WARNING";
	} elsif( $$hashRef{EventType} == EVENTLOG_AUDIT_SUCCESS ) {
		$OUTPUT{IsError}="TRUE";
		$OUTPUT{ErrorMsg}="IGNORING AUDIT SUCCESS";
		output( "LINE ",__LINE__," *** IGNORING THE ABOVE MESSGE\n" ) if $DIAGNOSTICS;
		return %OUTPUT;
	} elsif( $$hashRef{EventType} == EVENTLOG_AUDIT_FAILURE ) {
		$OUTPUT{IsError}="TRUE";
		$OUTPUT{ErrorMsg}="IGNORING AUDIT FAILURE";
		output( "LINE ",__LINE__," *** IGNORING THE ABOVE MESSGE\n" ) if $DIAGNOSTICS;
		return %OUTPUT;
	}

	my($msgstring)=$$hashRef{Message};
	$msgstring = $$hashRef{Strings}	if ! $msgstring;

	my($p)=chr(0);
	my(@MESSAGE_ARRAY)=split(/$p/,$msgstring);
	$msgstring=~s/$p/ /g;
	$OUTPUT{MESSAGE_ARRAY} = \@MESSAGE_ARRAY;

	my($IS_SQL_2000)="FALSE";
	if( $$hashRef{'EventID'} == 17055 ) {
		$IS_SQL_2000="TRUE";

		# OK I FIND SOME SQL 2000 MESSAGES THAT HAVE 1 PART
		if( $#MESSAGE_ARRAY == 0 ) { @MESSAGE_ARRAY=split(/\s+:\s+/,$msgstring,2);	}

		$$hashRef{'EventID'} = $OUTPUT{EventID} = $MESSAGE_ARRAY[0];

		if( $#MESSAGE_ARRAY != 1 ) {
			warn "HOW ODD: IS THIS A SQL SERVER 2000 MESSAGE? \nINPUT: ",Dumper($hashRef),"\nOUTPUT: ",Dumper(\%OUTPUT),"\nMESSAGE HAS $#MESSAGE_ARRAY PARTS: \n=>",join("\n=>",@MESSAGE_ARRAY),"\n"
				unless $OUTPUT{severity} eq "ERROR";
			$OUTPUT{IsError}="TRUE";
			$OUTPUT{ErrorMsg}="IGNORING ODD SQL2000 MESSAGE";
			output( "LINE ",__LINE__," *** IGNORING THE ABOVE MESSGE\n" ) if $DIAGNOSTICS;
			return %OUTPUT;
		}
	}

	output( "LINE ",__LINE__," DIAGNOSTICS: ",Dumper($hashRef),"\n" ) if $DIAGNOSTICS;
	my $rc = &$FUNCREF($hashRef);
	if( $rc ne '' ) {
		$OUTPUT{IsError}="TRUE";
		$OUTPUT{ErrorMsg}=$rc;
		$OUTPUT{MessageText} = $msgstring;
		$OUTPUT{MessageText} =~ s/[\r\n]//g;

		output( "LINE ",__LINE__," *** IGNORING THE ABOVE MESSGE (FAILED wanted())\n" ) if $DIAGNOSTICS;
		return %OUTPUT;
	}

	my($m) = $OUTPUT{EventID};
	my($s);

	output( "LINE ",__LINE__," IS_SQL_2000=$IS_SQL_2000\n" ) if $DIAGNOSTICS;

	if( $IS_SQL_2000 eq "FALSE" and $#MESSAGE_ARRAY == 0 ) {
		output( "STRINGS: ",join("\n=>",split(/$p/,$$hashRef{Strings})),"\n" ) if $DIAGNOSTICS;
		@MESSAGE_ARRAY=split(/$p/,$$hashRef{Strings});
	}

	if( $IS_SQL_2000 eq "TRUE" ) {

		if( $#MESSAGE_ARRAY != 1 ) {
			die "WOAH IS THIS A SQL SERVER 2000 MESSAGE? \nINPUT: ",Dumper($hashRef),"\nOUTPUT: ",Dumper(\%OUTPUT),"\nMESSAGE HAS $#MESSAGE_ARRAY PARTS: \n=>",join("\n=>",@MESSAGE_ARRAY),"\n";
		}
		$s=$MESSAGE_ARRAY[1];

		if( $m >= 18264 and $m<=18271 ) {
			($OUTPUT{op},
			$OUTPUT{Database},
			$OUTPUT{crdt},
			$OUTPUT{firstLSN},
			$OUTPUT{lastLSN},
			$OUTPUT{device},
			$OUTPUT{filetime},
			$OUTPUT{pages},
			$OUTPUT{directory})=parse_dump_line(@MESSAGE_ARRAY);
		}
	} elsif(	$#MESSAGE_ARRAY == 2 and $$hashRef{'EventID'}==17177 )		{
		$OUTPUT{op}="Server Uptime";
		$OUTPUT{SERVERSTARTTIME} = $MESSAGE_ARRAY[1];
	} elsif(	$#MESSAGE_ARRAY == 2 and $$hashRef{'EventID'}==15457 )		{
		$OUTPUT{op}="Server Option Change";
		$OUTPUT{OPTIONNAME} = $MESSAGE_ARRAY[0];
		$OUTPUT{OLDVALUE} = $MESSAGE_ARRAY[1];
		$OUTPUT{NEWVALUE} = $MESSAGE_ARRAY[2];
	} elsif(	$#MESSAGE_ARRAY == 11 and $$hashRef{'EventID'}==8957 )		{
		$OUTPUT{op}="DBCC";
		$OUTPUT{DBCC}="DBCC ".$MESSAGE_ARRAY[1]."(".$MESSAGE_ARRAY[2].")".$MESSAGE_ARRAY[5];
		$OUTPUT{DBCCERRORS} = $MESSAGE_ARRAY[7];
		$OUTPUT{DBCCREPAIRS} = $MESSAGE_ARRAY[8];
		$OUTPUT{DBCCDURATION} = $MESSAGE_ARRAY[9]."H ".$MESSAGE_ARRAY[10]."M "..$MESSAGE_ARRAY[11]."S";
	} elsif(	( $#MESSAGE_ARRAY == 7 and $$hashRef{'EventID'}==18264 )
			or ( $#MESSAGE_ARRAY == 6 )) { # SQL SERVER 2005
		$m=$$hashRef{'EventID'};
		$OUTPUT{EventID} 		= $m;
		if( $m >= 18264 and $m<=18271 ) {
			$OUTPUT{Database}			=$MESSAGE_ARRAY[0];
			if( $m ==18264 ) {
				$OUTPUT{firstLSN}			=$MESSAGE_ARRAY[4];
				$OUTPUT{lastLSN}			=$MESSAGE_ARRAY[5];
				$OUTPUT{numDumpDevices}	=$MESSAGE_ARRAY[6];
				$MESSAGE_ARRAY[7] =~ /\'(.+)\'/;
				$OUTPUT{File}				=$1;
			} else {
				$OUTPUT{firstLSN}			=$MESSAGE_ARRAY[3];
				$OUTPUT{lastLSN}			=$MESSAGE_ARRAY[4];
				$OUTPUT{numDumpDevices}	=$MESSAGE_ARRAY[5];
				$MESSAGE_ARRAY[6] =~ /\'(.+)\'/;
				$OUTPUT{File}				=$1;
			}
			$OUTPUT{File} =~ s/\\/\//g;

			if( $m == 18264 ) {
				$OUTPUT{op} = 'Database backed up';
			} elsif( $m == 18265 ) {
				$OUTPUT{op} = 'Log backed up';
			} elsif( $m == 18266 ) {
				$OUTPUT{op} = 'DB File Backup';
			} elsif( $m == 18267 ) {
				$OUTPUT{op} = 'DB File Restore';
			} elsif( $m == 18268 ) {
				$OUTPUT{op} = 'Log restored';
			} elsif( $m == 18269 ) {
				$OUTPUT{op} = 'Unknown';
			} elsif( $m == 18270 ) {
				$OUTPUT{op} = 'Diff backup';
			} elsif( $m == 18271 ) {
				$OUTPUT{op} = 'Diff restore';
			}

			$OUTPUT{directory} =	dirname($OUTPUT{File});
			$OUTPUT{device} = basename($OUTPUT{File});

			my(@filespec)=split(/\./,$OUTPUT{device});
			if( $#filespec>3 ) {	# it has periods
				$OUTPUT{filetime} =$filespec[3]." ".$filespec[4];
			} else {
				$OUTPUT{device}=~s/\.\w\w\w$//;
				@filespec=split(/\_/,$OUTPUT{device});
				$OUTPUT{filetime}="";
				foreach ( @filespec ) {
					if( /^\d\d\d\d\d\d\d\d+$/ ) {
						$OUTPUT{filetime}=$_;
						substr($OUTPUT{filetime},8,0)=" ";
						last;
					}
				}
			}
		}
	} else {
		if( $#MESSAGE_ARRAY == 0 and $m >= 18264 and $m<=18271 ) {
			($OUTPUT{op},
			$OUTPUT{Database},
			$OUTPUT{crdt},
			$OUTPUT{firstLSN},
			$OUTPUT{lastLSN},
			$OUTPUT{device},
			$OUTPUT{filetime},
			$OUTPUT{pages},
			$OUTPUT{directory})=parse_dump_line(@MESSAGE_ARRAY);
		}
		die "UNABLE TO PARSE EVENT ID ",$$hashRef{'EventID'},"\nINPUT: ",Dumper($hashRef),"\nOUTPUT: ",Dumper(\%OUTPUT),"\nERROR: CANT PARSE MESSAGE AS IT HAS $#MESSAGE_ARRAY PARTS: \n=>",join("\n=>",@MESSAGE_ARRAY),"\n";
	}

	#($m,$s)= split(/\s/,$msgstring,2);
	#if( ! defined $m or $m !~ /^\d+$/ ) {
	#	$m=$$hashRef{'EventID'};
	#	$s=$msgstring;

#		if( ! defined $m or $m !~ /^\d+$/ ) {
#			$OUTPUT{IsError}="TRUE";
#			$OUTPUT{ErrorMsg}="CANT DECIPHER ERROR NUMBER";
#			return %OUTPUT;
#		}
#	}
#	$s=~ s/\r//g;				# remove ctrl m
#	$s=~ s/\)\.\s*$//;		# remove ). from end of line

	$OUTPUT{MessageText} = $msgstring;

	return %OUTPUT;
}

# parse messages about dump stuff
# returns : $operation, $db, $OUTPUT{firstLSN}, $lastLSN, $device
sub parse_dump_line {
	my($msg,$text)=@_;
	$_=$text;
	s/[\r\n]//g;
	s/^://;
	s/\)\.\s*$//;		# remove ). from end of line

	my($op,$db,$crdt,$firstLSN,$lastLSN,$device,$directory,$filetime,$pages,$ignore);

	s/[\r\n]//g;
	s/FILE=\d,//;
	my(@x)=split(/,/,$_);	# split the line

	($op,$ignore,$db)=split(/:/,$x[0],3);
	$db=~s/\s//g;

	my($i)=1;
	while( $i<=$#x ) {
		my($key,$value)= split(/:/,$x[$i],2);
		$key=~s/\s//g;
		$value=~s/^\s+//;
		if( $key eq "Database" ) {
			$db=$value;
		} elsif( $key eq "firstLSN" ) {
			$firstLSN=$value;
			$firstLSN=~s/\s//g;
		} elsif( $key eq "lastLSN" ) {
			$lastLSN=$value;
			$lastLSN=~s/\s//g;
		} elsif( $key eq "numberofdumpdevices" ) {
		} elsif( $key eq "pagesdumped" ) {
			$pages=$value;
			$pages=~s/\s//g;
		} elsif( $key eq "creationdate(time)" ) {
			$value=~s./..g;
			$value=~s/\(/ /;
			$value=~s/\)//;
			$value=~s/://g;
			$crdt=$value;
		} elsif( $key eq "deviceinformation" ) {
			#print "DBG: Line=$value\n";
			$value =~ m/\{\'([ \w:\/\\\.\-\$]+)\'\}/;
			if( ! defined $1 ) {
				# ok this can happen with odly formed restore lines, specifically ones
				# from multiple stripes.  Lets just ignore them all
				# as an exception case.  We need to rejoin the line up
				my($key,$value)= split(/:/,$x[$i].$x[$i+1],2);
				$key=~s/\s//g;
				$value=~s/^\s+//;
				$value =~ m/\{\'([ \w:\/\\\.\-\$]+)\'\s*\'([ \w:\/\\\.\-\$]+)\'\}/;
				if( ! defined $1 ) {
					output( "DBG: Orig Msg=$msg".$NL);
					output( "DBG: Orig Text=$text".$NL);
					output( "DBG: line was parsed into $NL\t",join("$NL\t",@x),$NL );
					output( "DBG: op=$op ignore=$ignore db=$db".$NL );
					output( "DBG: key=$key value=$value".$NL ) ;
					output( "DBG: Disk Info Line=$value".$NL );
					output( "DBG: Parsed to $1".$NL );
					#die "UNABLE TO PARSE!!!$NL";
					return("undef","undef");
				}
				output( "WHEE: We fixed the bug!".$NL );
				output( "1 = $1".$NL );
				$i++;
			}
			#print "DBG: Parsing $1\n";
			$device=basename($1);
			$directory = dirname($1);
			my(@filespec)=split(/\./,$device);
			if( $#filespec>3 ) {	# it has periods
				$filetime=$filespec[3]." ".$filespec[4];
			} else {
				$device=~s/\.\w\w\w$//;
				@filespec=split(/\_/,$device);
				$filetime="";
				foreach ( @filespec ) {
					if( /^\d\d\d\d\d\d\d\d+$/ ) {
						$filetime=$_;
						substr($filetime,8,0)=" ";
						#output( "NEW FILETIME=$filetime from $device\n" ) if $DEBUG;
						last;
					}
				}
#				if( $filespec[2]=~/^\d\d\d\d\d\d\d\d+$/ ) {
#					$filetime=$filespec[2];
#					if( length $filetime<8 ) {
#						print "WOAH>>> POSSIBLE BAD LINE\n";
#						print "IN=$_\n";
#						print "DEVICE=$device\n";
#						print "FILETIME=$filetime\n";
#						die "DBG - NOT DONE";
#					}
#					substr($filetime,8,0)=" ";
#					output( "NEW FILETIME=$filetime from $device$NL" ) if $DEBUG;
#				} else {
				if( $filetime eq "" ) {
					output( "UNABLE TO PARSE FILE TIME FROM device=$device ".$NL ) if $DEBUG;
				}
			}
		} elsif( $key eq "fullbackupLSN" ) {
			$lastLSN=$value;
			$lastLSN=~s/\s//g;
		} elsif( $key eq "file list" ) {
		} elsif( $key eq "TYPE=DISK" ) {
			$value =~ s/^..//;
			$value =~ s/\'\}//;
			# $value =~ m/\{\'([\w:\/\.\-]+)\'\}/;
			$device=$value;
			$device=~ s.\\.\/.g;
			$directory=dirname($device);
			my(@filespec)=split(/\./,basename($device));
			$filetime=$filespec[3]." ".$filespec[4];
		} else {
			error_out( "ILLEGAL KEY/VALUE => k=$key v=$value" );
		}
		$i++;
	}
	return($op,$db,$crdt,$firstLSN,$lastLSN,$device,$filetime,$pages,$directory);
}

sub wanted {
	my($hashref)=@_;
	return $$hashref{'Source'}.' (ignored)' unless $$hashref{'Source'} eq "MSSQLSERVER";
	return 'IGNORING NON BACKUP MSG'
				unless 	$$hashref{'EventID'}==18264
						or $$hashref{'EventID'}==18265
						or $$hashref{'EventID'}==18267
						or $$hashref{'EventID'}==18268;
	return "IGNORING WARNING MSG" if $$hashref{EventType} == EVENTLOG_WARNING_TYPE
	return '';
}
__END__

=head1 NAME

BkpChk_Mssql_Logs.pl - get and save info about backups of your sql servers

=head2 DESCRIPTION

Finds the state of your windows MSSQL backups by reading the eventlogs and then saving
the results into the Alarms database.  It then runs BkpChk_Report.pl to create reports.

This command is called from two batches:

MssqlBackupCrosscheck
	BkpChk_Mssql_Logs.pl -HOURS=3 --OUTFILE=...html_output/MssqlBackupCrosscheck.txt --BATCHID=MssqlBackupCrosscheck\n",

MssqlBackupCrosscheckWeekly
	BkpChk_Mssql_Logs.pl -DAYS=7 --OUTFILE=.../html_output/MssqlBackupCrosscheckWeekly.txt --BATCHID=MssqlBackupCrosscheckWeekly\n",

The output from this command will therefore be

	MssqlBackupCrosscheck.txt or MssqlBackupCrosscheckWeekly.txt

This program will also call BkpChk_Report.pl To Create

	BkpChk_Report.pl --SQLSVRONLY -NOSTDOUT --ERROR_REPORT=.../html_output/MssqlBackupCrosscheckErrors.html 	--ALL_REPORT=.../html_output/MssqlBackupCrosscheck.html --BATCHID=$BATCHID --TRANHOURS=2

Creating
	MssqlBackupCrosscheckErrors.html
	MssqlBackupCrosscheck.html

=cut

