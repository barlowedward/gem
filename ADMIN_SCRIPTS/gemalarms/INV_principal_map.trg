create trigger INV_principal_map_trg on INV_principal_map for insert,update,delete
as
begin
	insert INV_principal_map_audit
	select 'INS',suser_name(),getdate(),* from inserted
	where suser_name() != 'gemalarms'

	insert INV_principal_map_audit
	select 'DEL',suser_name(),getdate(),* from deleted
	where suser_name() != 'gemalarms'
end
go
