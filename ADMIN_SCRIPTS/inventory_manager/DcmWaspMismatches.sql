use racks
go
select
case when wasp_floor!=floor then 'FLOOR MISSMATCH'
 when wasp_row!=row 		then 'ROW MISSMATCH'
 when wasp_cabinet!=cabinet then 'CABINET MISSMATCH'
else 'OK'
end,
Site=wasp_site,WaspName=system_name,
DcmName=case when machinename!=system_name then machinename else '' end,
p.asset_tag,wasp_floor,wasp_row,wasp_cabinet,floor,row,cabinet
from barlow_precompute p, machines m
where m.machnumber = convert(int,p.asset_tag)
and ( wasp_floor!=floor  or wasp_row!=row  or wasp_cabinet!=cabinet  )
order by wasp_site,wasp_floor,wasp_row,wasp_cabinet
go
-- select top 10 * from machines where machnumber=5225
