/*
create trigger INV_system_upd_trg on INV_system for update
as
begin

 -- update hostname fields if the database is what was updated...
 if update(blackedout_until)
    update 	INV_system
    set    	blackedout_until = s1.blackedout_until
    from   	INV_system s2, inserted s1
    where 	s1.system_type in ('sybase','oracle','sqlsvr')
	and     	s2.system_type in ('unix','win32servers')
    and 		s1.host_name is not null
    and  	s1.host_name = s2.system_name
    and 		s1.host_type=s2.system_type

 if update(is_monitored)
    update INV_system
    set    is_monitored = s1.is_monitored
    from   INV_system s2, inserted s1
    where 	s1.system_type in ('sybase','oracle','sqlsvr')
	 and     s2.system_type in ('unix','win32servers')
    and 	s1.host_name is not null
    and  	s1.host_name = s2.system_name
    and 	s1.host_type=s2.system_type

 if update(production_state)
    update INV_system
    set    production_state = s1.production_state
    from   INV_system s2, inserted s1
    where 	s1.system_type in ('sybase','oracle','sqlsvr')
	and     s2.system_type in ('unix','win32servers')
    and 	s1.host_name is not null
    and  	s1.host_name = s2.system_name
    and 	s1.host_type=s2.system_type
end
*/
go
