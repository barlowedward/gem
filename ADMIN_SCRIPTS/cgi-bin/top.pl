use lib qw(.);
use lib qw(lib);
use strict;
use MlpAlarm;

my($DIAG);

print "
<html>
<head>
	<link rel='stylesheet' type='text/css' href='styles/main.css' />
	<link rel='stylesheet' type='text/css' href='styles/Customer.css' />
</head>
<BODY>
<! Copyright &#169 1998-2010>
<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0>
	<TR HEIGHT=23 class='menuheader'><TD ALIGN=LEFT>\n";

my($str)='';
my(%perms);

sub get_dashboard_perms {
	my($show_credentials,$show_amreview,$show_systems,$show_alerts,$show_administration,$show_audit,$show_report,$show_help,$show_development,$show_datacenter,$show_feeds)=(0,0,0,0,0,0,0,0,0,0);
	foreach( MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0) ) {
		next if /^Dflt/;
		print $_,"<br>" if $DIAG;
		$perms{$_}=1;
		$show_report=1 if /REPORTS PG/;

		if( /DASHBOARD ADMINISTRATOR/ or /IT MANAGER/ ) {
			($show_credentials,$show_amreview,$show_systems,$show_alerts,$show_administration,$show_audit,$show_help,$show_development)=(1,1,1,1,1,1,1,1);
		}
		if( /APPROVER/ or /AUDITOR/ or /DASHBOARD POWERUSER/ or /DASHBOARD POWERUSER/ or /CREDENTIALS PG/ ) {
			$show_credentials++;
		}
		$show_systems++ 			if /INVENTORY PG/ or /AUDITOR/;
		$show_administration++ 	if /DASHBOARDADMIN PG/;
		$show_audit++ 				if /AUDIT PG/ or /AUDITOR/;
		$show_feeds++ 				if /FEEDS PG/;
		$show_datacenter++ 		if /DATACENTER PG/;
		$show_amreview++ 			if /AMREVIEW PG/;
		$show_alerts++ 			if /ALERTS PG/ or /DASHBOARD POWERUSER/;
	}
	return($show_credentials,$show_amreview,$show_systems,$show_alerts,$show_administration,$show_audit,$show_report,$show_help,$show_development,$show_datacenter,$show_feeds);
}

#	INVENTORY PG
#	ALERTS PG
#	AUDIT PG
#	DASHBOARDADMIN PG
#	CREDENTIALS PG
#	FEEDS PG
#	REPORTS PG

my($show_credentials,$show_amreview,$show_systems,$show_alerts,$show_administration,$show_audit,$show_report,$show_help,$show_development,$show_datacenter,$show_feeds)=get_dashboard_perms();
if( $show_credentials+$show_amreview+$show_systems+$show_alerts+$show_administration+$show_audit+$show_report+$show_datacenter+$show_feeds == 0 ) {
	print "<FONT COLOR=RED SIZE=+1>ERROR - YOU HAVE NO PERMISSIONS AS ".GetLogonUser()."</FONT><br>";
	foreach ( keys %perms ) { print $_," ",$perms{$_},"<br>"; }
	exit(0);
}

my($numitms)=  $show_credentials+$show_systems+$show_amreview+$show_alerts+$show_administration+$show_audit+$show_report+$show_datacenter+$show_feeds;
print "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=100%>
	<TR><TD ALIGN=LEFT WIDTH=70%>ENTERPRISE DASHBOARD <br>\n";

print "<A TARGET=_top HREF=index_credentials.htm>Credentials</A>" 		if $show_credentials;
print " | " if $show_credentials and $numitms--;
print "<A TARGET=_top HREF=index_inventory.htm>Systems</A>" 				if $show_systems;
print " | " if $show_systems and $numitms--;
print "<A TARGET=_top HREF=index_inventory_feeds.htm>Feeds</A>" 			if $show_feeds;
print " | " if $show_feeds and $numitms--;
print "<A TARGET=_top HREF=index_inventory_dcm.htm>Data Center</A>" 		if $show_datacenter;
print " | " if $show_datacenter and $numitms--;
print "<A TARGET=_top HREF=index_alerts.htm>Alerts</A>" 						if $show_alerts;
print " | " if $show_alerts and $numitms--;
print "<A TARGET=_top HREF=index_audit.htm>Audits</A>" 						if $show_audit;
print " | " if $show_audit and $numitms--;

print "<A TARGET=_top HREF=index_amreview.htm>AM Review</A>" 				if $show_amreview;
print " | " if $show_amreview and $numitms--;

print "<A TARGET=_top HREF=index_reports.htm>Reports</A>" 					if $show_report;
print " | " if $show_report and $numitms--;
print "<A TARGET=_top HREF=index_development.htm>Development</A>" 		if $show_development;
print " | " if $show_administration;
print "<A TARGET=_top HREF=index_admin.htm>Administration</A>" 			if $show_administration;
print "</TD>";
print "<TD WIDTH=30% ALIGN=RIGHT VALIGN=TOP><A TARGET=_top HREF=index_help.htm>HELP</A></TD>" if $show_help;
print "</TR></TABLE>\n";

print "</TD></TR>
<TR HEIGHT=23 class='menuheader'><TD>&nbsp;</TD>
</TR>
</TABLE>
</body>
</html>
"
