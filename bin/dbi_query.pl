#!/usr/local/bin/perl-5.6.1

use strict;
use DBI;
use Socket;			 # needed for gethostbyaddr
use Getopt::Long;

$|=1;

sub usage {
	warn @_;
	die "usage : dbi_query.pl --SERVER=xxx --LOGIN=xxx --PASSWORD=xxx --DEBUG
   -or-
usage : dbi_query.pl --HEADER --SERVER=xxx --LOGIN=xxx --PASSWORD=xxx --DEBUG --QUERY
";
}
use vars qw( $SERVER $LOGIN $PASSWORD $HEADER $DEBUG $QUERY);
usage()  unless    GetOptions(  
				"SERVER=s"	=> \$SERVER,
   			"LOGIN=s"	=> \$LOGIN,
   			"HEADER"	=> \$HEADER,		# sadly it doesnt work
   			"QUERY=s"	=> \$QUERY,
   			"DEBUG"		=> \$DEBUG,
				"USER=s"		=> \$LOGIN,
				"PASSWORD=s"	=> \$PASSWORD	);

my($CType);
if( is_nt() ) {
	  $CType="ODBC";
	  print "Running on Windows - Checking ODBC Settings\n";
	  my(@av_drivers)=DBI->available_drivers();
	  my($found)=0;
	  foreach (@av_drivers) { $found=1 if $_ eq "ODBC"; }
	  error_out( "DBI ODBC Driver not found - found ",join(" ",@av_drivers),"\n"  ) unless $found;
	  my($dsn_list);
	  eval {  DBI->data_sources("ODBC");	};
	  if( $@ ) {
		  error_out( "Errors loading ODBC : $@\n" );
	  } else {
		  $found=0;
		  my(@dat)= grep(s/DBI:ODBC://i,DBI->data_sources("ODBC"));
		  foreach (@dat) {
			  $found++ if $_ eq "$SERVER";
		  }
		  error_out( "$SERVER is not set up as an odbc server\nAllowed Servers=",join(" ",sort @dat),"\n") unless $found;
	  }

} else {
	  $CType="Sybase";
	  print "Testing Sybase server $SERVER\n" unless $QUERY;
	  error_out( "ERROR: SYBASE Environment variable is not set" )
			  unless defined $ENV{SYBASE};
	  error_out( "ERROR: SYBASE Environment variable is not valid" )
			  unless -d $ENV{SYBASE};

	  my($IF_FILE)	= "$ENV{SYBASE}/interfaces"	if -r "$ENV{SYBASE}/interfaces";
	  $IF_FILE	= "$ENV{SYBASE}/ini/sql.ini"	if -r "$ENV{SYBASE}/ini/sql.ini";

	  error_out( "ERROR: SYBASE variable ($ENV{SYBASE}) appears incorrect.\nInterfaces file does not exist" )
			  unless -r $IF_FILE;
	 if( $DEBUG ) {
	 	 my(@rc)=dbi_get_interfaces($SERVER,$IF_FILE);
	  	error_out( "Apparently your sybase server ($SERVER) is not in your interfaces file\n" ) if $#rc<0;
	 }
}

DBI->trace(2) if defined $DEBUG;

print "Testing connectivity to $SERVER\n" unless $QUERY;
my($dbh)= DBI->connect("dbi:$CType:$SERVER",$LOGIN,$PASSWORD, 
					{RaiseError=>1,PrintError=>0,AutoCommit=>1,syb_err_handler=>\&sybase_handler} );
					
$dbh->{syb_err_handler}  = \&sybase_handler ;
if( defined $dbh ) {
	if( defined $QUERY ) {
	  	my($ary_ref)=$dbh->selectall_arrayref($QUERY);			 			  
	 			 
#		if( defined $HEADER ) {
#				# Populate Header
#				my($ncol) = $sth->{NUM_OF_FIELDS};
#				my($ptr_colnames)=$sth->{NAME};
#				my(@header)= @$ptr_colnames;
#				unshift @header,"BATCH ".$batchid if defined $add_batchid;
#				debugmsg("adding header ".join("~",@header));
#				push(@output_results,\@header);
#				undef $header_count;
#		}
		
		my(@output);
	
		# BUILD FORMAT STRING
		my(@widths)=(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);
		foreach my $dat ( @$ary_ref ) {
			my $count=0;
			foreach ( @$dat ) {
				s/\s+$//;
				$widths[$count] = length($_) if $widths[$count] < length($_);
				$count++;
			}
		}
	
		my($fmt)="";
		foreach (@widths) {
			last if $_ == -1;
			$fmt.="%-".$_."s ";
		}
		
		foreach( @$ary_ref ) {
			my(@rcdat) = @$_;
			if( $#rcdat==0 ){
				push @output,$rcdat[0];
				next;
			}
			printf($fmt,grep(s/\s*$//,@rcdat));					
			print "\n";
		}
	  $dbh->disconnect();
	  exit(0);
	} else {
	  print "Connected to Database Server $SERVER\n" unless $QUERY;
	  my($ary_ref)=$dbh->selectall_arrayref("select count(*) from master..sysobjects where name='sysprocesses'");
	  my($ok)=0;
	  foreach my $row (@$ary_ref) {
		  $ok++ if $$row[0] == 1;
	  }
	  if( $ok == 0 ) {
		  warn "-- Testing Failed\n";
		  foreach my $row (@$ary_ref) {
			  warn "--->",join(" ",@$row)," \n";
		  }
	  } else {
		  print "Testing Completed Successfully\n" unless $QUERY;
	  }
	  $dbh->disconnect();
	  exit(0);
	}
} else {
  error_out( "FALIED TO CONNECT\n" );
}
exit(0);

sub error_out {
	my($str)=join("",@_);
	$str=~s/\n/\n* /;
	die "********************************************\n* ",$str,"********************************************\n";
}

sub debugmsg {	print @_ if $DEBUG; }
sub mycroak { 	die(@_); }
sub myprint { 	print STDERR @_; }

sub sybase_handler {
	my ($error, $severity, $state, $line, $server, $proc, $error_msg) = @_;
		
		die "SYBASE HANDLER CALLED!\n @_";
	if( $error==3 and $severity==5 and $error_msg=~/Requested server name not found.$/ ) {
		mycroak("Server Name Not Found\n");
		return;
	}
	if( $error==4 and $severity==5 and $error_msg=~/call to connect two endpoints failed$/ ) {
		mycroak("Server Not Available\n");
		return;
	}

	debugmsg( "SYBASE ERROR HANDLER (error=$error severity=$severity)" );

	if( $error == 0 ) {
			debugmsg("DBG: printing $error_msg ");
			myprint($error_msg);		
	} else {
			my($str) = "sybase_handler() ERROR # $error: $error_msg";
			debugmsg("DBG: Print/Die $str");
			mycroak($str."\n FATAL ERROR\n");
	}	
	return 0;
}

################################## SUBROUTINES ###########################
sub is_nt {
	my($is_nt)=0;
	$is_nt=1 if defined $ENV{PROCESSOR_LEVEL};
	return $is_nt;
}
my(%name_in_if_file);
sub dbi_get_interfaces
{
	my($search_string,$filename)=@_;

	die "ERROR: SYBASE ENV not defined" unless defined $ENV{"SYBASE"};

	my(@srvlist,%port,%host,%found_svr);

	if( defined $filename and ! -e $filename ) {
		print "Warning: File $filename Not Found\n";
	}

	if( -e $ENV{"SYBASE"}."/interfaces" or -e $filename ) {
		$filename=$ENV{"SYBASE"}."/interfaces" unless defined $filename;

		# UNIX
		open FILE,$filename or die "ERROR: Cant open $filename : $!";
		my($srv,$dummy)=("","");
		while( <FILE> ){
			next if /^#/;
			chomp;

			if( /^\s/ ) {
				next unless /query/;
				next if $srv eq "";
				chomp;

				my(@vals)=split;

				# Sun Binary Format
				if( $vals[4] =~ /^\\x/ ) {
					#format \x
					$vals[4] =~ s/^\\x0002//;

					my($p) = hex(substr($vals[4],0,4));
					my($pk_ip) = pack('C4',
						hex(substr($vals[4],4,2)),
						hex(substr($vals[4],6,2)),
						hex(substr($vals[4],8,2)),
						hex(substr($vals[4],10,2)));

					my($name,$aliases,$addrtype,$len,@addrs);
					if( defined $name_in_if_file{$pk_ip} ) {
						$name=$name_in_if_file{$pk_ip};
					} else {
						($name,$aliases,$addrtype,$len,@addrs) = gethostbyaddr($pk_ip,AF_INET);
						$name_in_if_file{$pk_ip}=$name if defined $name and $name !~ /^\s*$/;
					}

					my($h) =$name ||
						hex(substr($vals[4],4,2)).".".
						hex(substr($vals[4],6,2)).".".
						hex(substr($vals[4],8,2)).".".
						hex(substr($vals[4],10,2));

					if( defined $host{$srv}  ) {
						$host{$srv}.="+".$h;
						$port{$srv}.="+".$p;
					} else {
						$host{$srv}=$h;
						$port{$srv}=$p;
					}
				} else {
					$port{$srv} = $vals[4];
					$host{$srv} = $vals[3];
				}

				push @srvlist,$srv unless defined $found_svr{$srv};
				$found_svr{$srv} = 1;
				# $srv="";
			} else {
				($srv,$dummy)=split;
			}
		}
		close FILE;

	} elsif( -e $ENV{"SYBASE"}."/ini/sql.ini"  ) {

		# DOS
		# fix up sybase env vbl
		$ENV{"SYBASE"} =~ s#\\#/#g;
		open FILE,$ENV{"SYBASE"}."/ini/sql.ini"
			or die "ERROR: Cant open file ".$ENV{"SYBASE"}."/ini/sql.ini: $!\n";
		my($cursvr)="";
		while( <FILE> ){
			next if /^#/;
			next if /^;/;
			next if /^\s*$/;
			chop;

	#
	# MUST ALSO HANDLE BIZARROS LIKE THE FOLLOWING
	#
	#[NYSISW0035]
	#$BASE$00=NLMSNMP,\pipe\sybase\query
	#$BASE$01=NLWNSCK,nysisw0035,5000
	#MASTER=$BASE$00;$BASE$01;
	#$BASE$02=NLMSNMP,\pipe\sybase\query
	#$BASE$03=NLWNSCK,nysisw0035,5000
	#QUERY=$BASE$02;$BASE$03;

			if( /^\[/ ) {
				# IT A SERVER LINE
				s/^\[//;
				s/\]\s*$//;
				s/\s//g;
				$cursvr=$_;
				push @srvlist,$_ unless defined $found_svr{$_};
				$found_svr{$_} = 1;
			} else {
				# IT A DATA LINE
				next if /^master=/i;
				next if /QUERY=\$BASE/i;
				next if /\,.pipe/i;
				next if /NLMSNMP/i;
				(undef,$host{$cursvr},$port{$cursvr})=split /,/;
			}
		}
		close FILE;
	} elsif( defined $filename ) {
		die "ERROR: $filename error";
	} else {
		die "ERROR: UNABLE TO DETERMINE INTERACE FILE";
	}

	my(@rc);
	foreach (sort @srvlist ) {
		if(defined $search_string) {
			next unless /$search_string/i
				or $host{$_} =~ /$search_string/i
				or $port{$_} =~ /$search_string/i;
		}
		push @rc,[ $_,$host{$_},$port{$_} ];
	}
	return @rc;
}

__END__

=head1 NAME

dbi_query.pl - Generic DBI Query Runner (template)

=head2 USAGE

Uses Information from surveys
__END__

=head1 NAME

OneQueryPerServerReport.pl - report on system configurations

=head2 USAGE

Template for pure dbi query.  

=cut


=cut
