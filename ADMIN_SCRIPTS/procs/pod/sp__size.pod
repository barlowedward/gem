=head1 NAME

sp__size - rewrite

=head2 AUTHOR

Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION

=head2 SEE ALSO

=head2 USAGE

 Proc_name                      Order Parameter
 ------------------------------ ----- ------------------------------
 sp__size                           1 @objname varchar(40) NOT NULL
(return status = 0)

=head2 SAMPLE OUTPUT

 Proc_name                Size  Avail_size  Lines       Avail_lines
 ------------------------ ----- ----------- ----------- -----------
 mon_authorize_non_sa     6  KB         122           2         253
 mon_rpc_attach           3  KB         125           1         254
 mon_rpc_connect          2  KB         126           1         254
 sp_configure             180KB         -52          73         182
 sp_dboption              116KB          12          49         206
 sp_dbupgrade             73 KB          55          19         236
 sp_getmessage            33 KB          95          14         241
 sp_loaddbupgrade         6  KB         122           2         253
 sp_procxmode             28 KB         100          12         243
 sp_prtsybsysmsgs         4  KB         124           1         254
 sp_validlang             6  KB         122           2         253

(11 rows affected)
(return status = 0)
