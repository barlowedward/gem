#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# cd to the right directory
use File::Basename;
my($curdir)=dirname($0);

#
# analyze a stored procedure by Edward Barlow
#

use Sybase::DBlib;
require "sybutil.pl";
use Getopt::Std;
use strict;
use vars qw($DO_PLAN_STUFF);
use File::Basename;

use vars qw($opt_h $opt_i $opt_l $opt_o $opt_d $opt_S $opt_D $opt_U $opt_P $opt_C $opt_F $opt_L $opt_n $opt_r $opt_t $opt_e);

my($io_printed);
my($time_1,$time_2,$time_3,$DBPROC,$ERROR,$ENDFONT,$NEWL);
my(@real_out,@io_out,@res_out,@lock_out,%tblrows,@original_showplan,@exec_out);
my( $statement_id, $line_id ) = (0,0);

my(@errors);			    # List of Errors to print at top of the page
my($from_tbl_row_num);  # number of row after FROM/TO TABLE LINE
my($is_step_one);		       # help format step_number
my($step_number);		       # STEP X
my($query_type);			# type of query line (parsed)
my($prnted_something);  # handle trivial plans (declare stmts...)
my($next_row_is_to_table)=0;    # next row is to table

# AFTER A FROM TABLE THE FOLLOWING ARE SET
my($cur_table);		 # table mentioned on line after FROM/TO
my($is_tbl_scan);		       # Table Scan...
my($index_info);

# ===================================
# BASICALLY YOU READ MESSAGES IN ORDER AND KEEP A STATE MACHINE AS FOLLOWS
#  - if $from_tbl_row_num>0 then you have passed a FROM TABLE LINE and print a
#    set of things based on this format
#		       6215		    FROM TABLE	      - from_tbl_row_num=1  cur_table=""
#		       6217		    tablename
#		       6220/6219       Nested Iteration
#		       6223		    table scan
#  -  6203 or 6202      store type of query in $query_type
#  -  6201	      store "STEP #" in $step_number
# ===================================

# SECTION TITLE INFORMATION
my(%titles) = (
"showplan",	     "SHOWPLAN ANALYSIS OUTPUT",
"history",	      "QUERIES THAT WERE EXECUTED BY ANALYSIS PROGRAM",
"helptext",	     "QUERY TEXT",
"summary",	      "SUMMARY RESULTS",
"showplan",	     "ANALYSIS OUTPUT",
"locks",			"LOCK OUTPUT",
"results",	      "PROCEDURE OUTPUT",
"statsio",	      "STATISTICS IO RESULTS",
"raw",		  "RAW SHOWPLAN OUTPUT"
);

# ==================
# USAGE()
# ==================
sub usage
{
  return "@_
USAGE: showplan_analyze.pl [-dLhnitre] -o OUTFILE -U USER -P PASSWORD -S SERVER -D DATABASE [ -F FILE | -C CMD ]

Showplan Analyzer Copyright (c) 1996-9 by Edward Barlow

  -d : debug mode
  -L : do lock analysis.  The session is trapped inside a 'BEGIN TRAN' and
       locks are measured.  The tran is rolled back after.  This aborts
       on operations that cant handle a tran (select into...), but should be
		 set by default.
  -h : html'ize output
  -n : set noexec on - will work poorly if running a stored proc

  -i : show statistics io
  -l : show lock stuff
  -o : output to this file as well as to screen
  -t : show procedure text (proc name is first word after exec)
  -r : show raw showplan output
  -e : show sql the analyze program executes

";
}

# ==================
# MESSAGE HANDLER
#   Call parse_showplan and parse_stats_io
# ==================
sub message_handler_mine
{
   my ($db, $message, $state, $severity, $text, $server, $procedure, $line)=@_;
   my($dbrc);

	push @original_showplan,"(".$message.") ".$text."\n" if defined $opt_r;

   if ($message==226) {
      print "$ERROR Server Msg $message: Select Into Found!$ENDFONT\n";
      print $ERROR,"\nTHIS QUERY CONTAINS A SELECT INTO!\n",$ENDFONT;
      print $ERROR,"PLEASE RERUN THIS QUERY WITHOUT THE -L Option (no transaction)",$ENDFONT,"\n\n";
      $dbrc=1;

   } elsif ($message==916) {
      print "$ERROR Server Msg $message: Unable To Use Db$ENDFONT\n";
      $dbrc=0;

   } elsif ($severity > 0) {
      print $ERROR, "Server Msg $message: $text\n",$ENDFONT;
      $dbrc=0;
    } elsif ($message == 1562) {
		$dbrc=0;
    } elsif ($message == 0) {

		# print messages should go to the results file
      normal_print( "$text\n");
      $dbrc=0;

    } else {

      # SHOWPLAN OUTPUT
      if( $DO_PLAN_STUFF==1 and $message >= 6200 and $message <=6300 ) {
	 parse_showplan_output($message,$text);

      } elsif( $DO_PLAN_STUFF==1 and $message >= 10200 and $message <=10300 ) {
	 parse_showplan_output($message,$text);

      # STATISTICS IO OUTPUT
      } elsif( $message >= 3600 and $message <=3700 ) {
	 parse_stats_io_output($message,$text);
      } else  {
	 print $ERROR, "Server Msg $message: $text\n",$ENDFONT;
      }
      $dbrc=1;
   }
}

# ================
# Parse_showplan_output()
#	       calls output_print()
# ================
sub parse_showplan_output
{
   my($message,$text)=@_;
   $text =~ s/^\s+//;	   # remove leading spaces

   output_print($message,$text, "\n") if defined $opt_d;

	# 6219/6220 Nested Iteration lines should be ignored
   if( $message == 6219 or $message == 6220 ) {
		return;

   # The from_tbl_row_num variable is the row after FROM TABLE line
	# indremented each time until to either to table found or 6248
	} elsif ( $from_tbl_row_num > 0 ) {

		# ERROR CONDITIONS - should never happen
		if( $message==6201 ) {
	output_print(format_output_row());
			reset_tbl_row_vals();
			parse_showplan_output($message,$text);

			# ok make recursive call - system 10 server your query is done
			return;
		}

		# if find TO TABLE then print next row
      if ( $message == 6214 ) {
	output_print(format_output_row());
			reset_tbl_row_vals();
			$next_row_is_to_table=1;
			return;
		}

		# done if spaced
		if( $message == 6248 ) {
	output_print(format_output_row());
			reset_tbl_row_vals();
			return;
		}

		# another from table line - hmmm.  - print what we have and restart
		if( $message == 6215 ) {
	output_print(format_output_row());
	reset_tbl_row_vals();
	$from_tbl_row_num=1;
			return;
		}

		# TABLENAME ROW IS ROW NUMBER 1 AFTER 6215
		if( $from_tbl_row_num==1 ) {
			$cur_table = $text;
		}

		$is_tbl_scan=1 if $message==6223;
		push @errors, "Table Scan of $cur_table - $tblrows{$cur_table} rows\n"
			if $message==6223
			and substr($cur_table,0,1) ne "#"
			and substr($cur_table,0,9) ne "Worktable";

		return if $message==6276;				       # ascending scan
		return if $message==6278;				       # positioning
		return if $message==6272;				       # IO Size
		return if $message==6273;				       # LRU Buffer Replacement

		$index_info .= $text if $message==6225;
		$index_info .= " (covered)" if $message==6286;

	$from_tbl_row_num++;

   } elsif( $next_row_is_to_table==1 ) {
		output_print(row_output($step_number,$query_type,"TO TABLE ".$text));
      $next_row_is_to_table = 0;

   } elsif ( $message == 6214 ) {
		$next_row_is_to_table=1;

   } elsif ( $message == 6205
		or $message==6209
		or $message==6210
		or $message==6211
		or $message==6212
		or $message==6213
		or $message==6217
		or $message==6223 ) {

		output_print(row_output($step_number,$query_type,$text));

   } elsif ( $message == 6226 or $message == 6227 ) {
		# 6227 = log scan
		# 6226 = index name not found

		push @errors,$text;
		output_print(row_output($step_number,$query_type,$text));

   } elsif ( $message == 6203 or $message==6202 ) {
		# The Type of Query is...

      my($tmp) = substr($text,21);
      chop($tmp);
      if( $is_step_one==1 ) {

			output_print( "$statement_id) " ) if $statement_id != 0;
	 output_print($tmp);
			output_print( " (Line $line_id)" ) if $line_id != 0;
			$line_id  = 0;
			$statement_id = 0;
	 output_print(" -----------\n");
	 $is_step_one=0;
      }
      $query_type = '('.$tmp.'): ';

	# set the step_number for future prints
   } elsif ( $message == 6201 ) {
		# STEP N...

	# step_number is something like "2:" for STEP 2
		$text =~ s/\s//g;
		$text =~ s/STEP//;
	$step_number="STEP ".$text." ";

	if( $text eq "1" ) {
	$prnted_something=0;
	output_print( "----------- " );
	$is_step_one=1;
	} else {
			output_print("\n") if $prnted_something==1 and ! defined $opt_h;
	}

    } elsif ( $message == 6289 ) {
		# QUERY PLAN FOR LINE NUM

	   $text =~ s/[a-z]//g;
	   $text =~ s/[A-Z]//g;
	   $text =~ s/^\s+//;
	   ( $statement_id, $line_id ) = split /\s/,$text,2;
	   $line_id =~ s/\D//g;

	# initialize from_tbl_row_numers
   } elsif ( $message == 6215 ) {
		# FROM TABLE

      reset_tbl_row_vals();
      $from_tbl_row_num=1;

	# ignore these
   } elsif ( $message == 6204 ) {
		# Update Mode Is Direct
   } elsif ( $message == 6208 ) {
		# Scalar Aggregate
   } elsif ( $message == 6248 and $text eq "" ) {
		# blank line

   } elsif ( $message >= 10200 ) {

		if( $message >= 10203 and $message <= 10207 ) {
			output_print( $text,"\n" );
		}

	# not handled
   }
}

sub fix_getopts
{
	my(@tmpargv);
	my($cnt)=-1;
	foreach (@ARGV) {
		if( /^-/ ) {
			$cnt++;
			$tmpargv[$cnt] = $_;
		} else {
			$tmpargv[$cnt] .= " ".$_;
		}
	}
	@ARGV=@tmpargv;
}

# =======================
# parse_stats_io()
#    messages between 3600 and 3700
# =======================
sub parse_stats_io_output
{
   my($message,$text)=@_;

   # FOR STATS IO - Print physical reads > 0
   #	      Print info on non tmp tables / worktables
   my($idx);

   $idx = index( $text, "Table:");
   if( $idx>=0 ) {
      $text =~ s/Table://;
      $text =~ s/scan from_tbl_row_num//;
      $text =~ s/physical reads://;
      $text =~ s/logical reads://;
      $text =~ s/,//g;
      $text =~ s/^[ \t]+//;
      my($tblname,$scan_from_tbl_row_num,$lreads,$preads) = split(/[ \t\n]+/,$text);
      $io_printed=1;
		if( $DO_PLAN_STUFF == 1 ) {
			my($str)= sprintf("%-30s %-7d   %-7d   %-6d\n",$tblname,$scan_from_tbl_row_num,$lreads,$preads);
	io_print($str);
		}
   } else {
      # ignore 0 writes message
      if( $message == 3614 and index( $text,": 0") < 0 ) {
	 $io_printed=1;
	 io_print ($text,"\n") if $DO_PLAN_STUFF==1;
      }

      if( $message == 3614 and $io_printed==1 )  {
	 io_print("\n-------------------------------------\n");
	 $io_printed=0;
      }
   }
}

# =============================
# PRINT/SAVE OUTPUT TO APPROPRIATE ARRAYS
#   lock_print()
#   normal_print()
#   results_print()
#   io_print()
#   output_print()
# =============================
sub lock_print
{
	return if ! defined $opt_L;
	push @lock_out, @_ if defined $opt_l;
}

sub normal_print
{
	push @res_out, @_ if ! defined $opt_h;
	push @res_out, @_."<BR>" if defined $opt_h;
}

sub results_print
{
	push @res_out, @_;
}

sub io_print
{
	my($line)=@_;
	return if $line =~ /^\s*-*\s*$/;
	chomp $line;
	$line .= "\n";
	push @io_out, $line if defined $opt_i;
}

# if you have line starting <TR> then print <TABLE> and up the in_table
# the next time a line does NOT have <TR> at start, then do reverse
# only if $opt_h;
my($in_table)=0;
sub output_print
{
	my($str)=join("",@_);

	return if $str =~ /^\s*$/;

	if( defined $opt_h ) {
		if( substr($str,0,4) eq "<TR>" ) {
			if( $in_table == 0 ) {
				$in_table=1;
				push @real_out, "</PRE><TABLE BORDER=1><th>Step</th><th>Type</th><th>Table</th><th>Rows</th><th>Scan</th><th>Index Info</th>\n";
			}
		} else {
			if( $in_table == 1 ) {
				$in_table=0;
				push @real_out, "</TABLE>\n<PRE>";
			}
		}
	}
	push @real_out, $str;
}

###############################################################
#
#		       MAIN ROUTINE
#
###############################################################

die usage("") if $#ARGV == -1;

my($c)="Running: showplan_analyze.pl ".join(" ",@ARGV)."\n";
$c =~ s/\-P\s*\w+/-PXXXXX/;
print $c;

fix_getopts();
$|=1;
getopts('uendC:S:D:U:P:F:Lihlo:rt') or die usage("Bad Parameter List");

if( !defined($opt_S)) { die usage("Error: Must Pass Server\n"); }
if( !defined($opt_D)) { die usage("Error: Must Pass Database\n"); }
if( !defined($opt_U)) { die usage("Error: Must Pass User\n"); }
if( !defined($opt_P)) { die usage("Error: Must Pass Password\n"); }
if( !defined($opt_F) and !defined $opt_C ) { die usage("Error: Must Pass Filename or Command\n"); }

###############################################################
print "Login To Sybase\n" if defined($opt_d);
###############################################################
($DBPROC = new Sybase::DBlib $opt_U, $opt_P, $opt_S)
	|| die "Can't connect to the $opt_S Sybase server.\n";
$DBPROC->dbuse($opt_D);
&dbmsghandle ("message_handler_mine");
&dberrhandle ("error_handler_mine");

# =================
print "Defining Variables\n" if defined $opt_d;
# =================
if( defined $opt_h ) {
	$ERROR="<FONT COLOR=RED>";
	$ENDFONT  ="</FONT>";
	$NEWL="<br>\n";
} else {
	$ERROR="";
	$ENDFONT="";
	$NEWL="\n";
}

# ===========================
print "READ INPUT FILE OR COMMAND\n" if defined $opt_d;
# ===========================
my($sql_cmd_to_analyze) = "";
my($qfrom_tbl_row_num)=0;
if( defined $opt_C ) {
	print "query from -C option: $opt_C\n" if defined $opt_d;
	if( $opt_C=~ /^\'/ ) {
		$opt_C=~ s/^\'//;
		$opt_C=~ s/\'$//;
	}
	$sql_cmd_to_analyze = $opt_C;
	$qfrom_tbl_row_num=1;
	chomp $sql_cmd_to_analyze;
} else {
	print "query from -F option: $opt_F\n" if defined $opt_d;
	open(QUERY,$opt_F)
		|| die "Couldnt Open $opt_F\n";
	while ( <QUERY> ) {
	#chop;
	$sql_cmd_to_analyze .= $_." ";
		$qfrom_tbl_row_num++;
	}
	close QUERY;
	chmod 0666,$opt_F or die "Cant chmod $opt_F";
}

print "Analyzing Query: $sql_cmd_to_analyze\n" if defined $opt_d;

# ===========================
# FIGURE OUT IF IT IS A PROC
#  PROCEDURE NAME IS FIRST WORD AFTER ANY EXEC or EXECUTE STMT
# ===========================
my($procname) = $sql_cmd_to_analyze;
$procname =~ s/^\s+//;			  # remove leading spaces
if( $procname =~ /^exec/i ) {
	$procname =~ m/\s\b(\w+)\b/;    # get second word
	$procname = $1;
} else {
	$procname =~ /^(\w+)/;		  # get first word
	$procname = $1;
}
$procname="" if( $procname =~ /\b(as|begin|between|declare|delete|drop|else|end|exec|exists|go|if|insert|procedure|return|set|update|values|from|select|where|and|or|create|order by)\b/i);

# ===========================
# OPEN OUTPUT FILES
# ===========================
$opt_F = "output" if ! defined $opt_F;
my($dir)=dirname($0);
my($out_commands)       = "$dir/$opt_F.history";
my($out_showplan)       = "$dir/$opt_F.raw";
my($out_results)	= "$dir/$opt_F.results";
my($out_locks)	  = "$dir/$opt_F.locks";
my($out_output) = "$dir/$opt_F.analysis";
my($out_statsio)	= "$dir/$opt_F.statsio";
my($out_helptext)       = "$dir/$opt_F.helptext";

io_print("Table Name		     Scan:     Log Io:   Phys Io:\n");
io_print("------------------------       ------    -------   -------\n");

$DO_PLAN_STUFF=0;
$io_printed=0;

# ===========================
# GET ROWCOUNTS FOR TABLES
# ===========================
my(@dat);
print "<PRE>" if defined $opt_h;
my($sql_tbl_rowcnt) = "/* Getting Row Counts For Tables */
select o.name, rowcnt(i.doampg)
from   sysobjects o, sysindexes i
where  i.id = o.id
and    indid<=1
and    uid=1";

# ===========================
# EXECUTE SQL
# ===========================
my($tbl,$rows);
sql_exec($sql_tbl_rowcnt,1);
while (($tbl,$rows) = $DBPROC->dbnextrow()) { $tblrows{$tbl} = $rows; }

# ===========================
# SET NOCOUNT ON
# ===========================
my($sql_nocount) = "set nocount on";
sql_exec($sql_nocount,1);
while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}

# ===========================
# GET SQL TEXT
# ===========================
my(@sql_cmd_text_out)=();
if( $procname ne "" and defined $opt_t ) {
	 my($sql_comments)="select text from dbo.syscomments where object_name(id) = \"$procname\"";
print "Executing $sql_comments\n" if defined $opt_d;
	 sql_exec($sql_comments,1);

    my($text, $joined_text)=("","");
    while(($text) = $DBPROC->dbnextrow) { $joined_text .= $text; }
    $joined_text = db_colorize_sql($joined_text) if defined $opt_h;
	 @sql_cmd_text_out = split( /\n/,$joined_text );
} elsif( defined $opt_t ) {
	print "ERROR - -t passed but \$procname is $procname \n";
	print "<br>" if defined $opt_h;
}

# ===========================
# BEGIN TRAN
# ===========================
sql_exec("BEGIN TRAN",1) if defined($opt_L);
while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}

# ===========================
# SET STATS IO
# ===========================
my($sql_statsio_on) = "set statistics io on";
sql_exec($sql_statsio_on,1);
while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}

# ===========================
# SET SHOWPLAN
# ===========================
my($sql_showplan_on) = "set showplan on";
sql_exec($sql_showplan_on,1);
while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}

# ===========================
# SET NOEXEC
# ===========================
if( defined $opt_n ) {
	my($sql_noexec_on)="set noexec on";
	sql_exec($sql_noexec_on,1);
	while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}
}

$DO_PLAN_STUFF=1;
$is_step_one=0;
$from_tbl_row_num=0;
reset_tbl_row_vals();
$step_number="1: ";
$prnted_something=0;

# ===========================
# RUN COMMAND
# ===========================
$time_1 = time;
sql_exec($sql_cmd_to_analyze,0);
$time_2=time;
print "SQL Has Completed\n" if defined $opt_d;

# ===========================
# PROCESS RESULTS
# ===========================
while($DBPROC->dbresults() != &NO_MORE_RESULTS) {  # copy all results
	my($statement_has_results) = 0;
	my($fmtstring);
	print "DbResults()\n" if defined $opt_d;
   while (@dat = $DBPROC->dbnextrow() ) {
		results_print "<TABLE>\n"
			if $statement_has_results==0 and defined $opt_h;
		$statement_has_results++;
		if( ! defined $opt_h ) {

			# get column formatting if needed
			if( $statement_has_results == 1 ) {
					my($stmt_num_cols) = $DBPROC->dbnumcols();
					my($i)=1;
					$fmtstring="";
					while( $i<=$stmt_num_cols ) {
						$fmtstring .= "%-".$DBPROC->dbcollen($i).".".$DBPROC->dbcollen($i)."s ";
						$i++;
					}
					$fmtstring .= "\n";
			}

			my($string)=sprintf $fmtstring,@dat;
	results_print( $string );

		} else {
	results_print( "<TR><TD>",join('</TD><TD>',@dat),"</TD></TR>\n");
		}
   }
	results_print "</TABLE>\n"
			if $statement_has_results>0 and defined $opt_h;
}
print "Main Query Results Have Completed\n" if defined $opt_d;

$time_3=time;

# ===========================
# CLEANUP
# ===========================
	sql_exec("set showplan off",1);
	while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}

	sql_exec("set statistics io off",1);
	while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}

# ===========================
# READ LOCKS AND END TRANSACTION
# ===========================
if( defined($opt_L)) {

my($sql_get_locks)="/* Getting Locks From Syslocks */
select
'Type'=v.name,
'User'=suser_name(p.suid)+' (pid='+rtrim(convert(char(3),l.spid))+')',
'Table'=db_name(l.dbid)+'..'+convert(char(20),object_name(l.id)),
'Page'=l.page
from     master..syslocks l,
    master..sysprocesses p,
    master..spt_values v
where  p.spid=l.spid
and    l.type = v.number
and    v.type = 'L'
and    p.suid = suser_id()
and    p.dbid = db_id()
and    l.dbid = db_id()
--and    cmd = 'SELECT'";

	sql_exec($sql_get_locks,0);
print "Executing $sql_get_locks\n" if defined $opt_d;

   while($DBPROC->dbresults() != &NO_MORE_RESULTS) {  # copy all results
       while (@dat = $DBPROC->dbnextrow() ) {
	 lock_print( join('    ',@dat),"\n");
       }
   }

	sql_exec("ROLLBACK TRAN",1);
	while (@dat = $DBPROC->dbnextrow()) { normal_print( join('    ',@dat),"\n");}
}

$DBPROC->dbclose();
print "</PRE>" if defined $opt_h;

# ===========================
# PRINT TOC
# ===========================
if( defined $opt_h ) {
	print_toc("showplan",$out_output)       if $#real_out>=0;
	print_toc("statsio",$out_statsio)       if $#io_out>=0;
	print_toc("helptext",$out_helptext) if $#sql_cmd_text_out >= 0;
	print_toc("locks",$out_locks)	   if $#lock_out>=0;
	print_toc("results",$out_results)       if $#res_out>=0;
	print_toc("raw",$out_showplan)	  if $#original_showplan>=0;
	print_toc("history",$out_commands)      if $#exec_out>=0;
}

# ===========================
# GET PAGE OF OUTPUT
# ===========================
my($str)= format_page();

print $str;
if( defined $opt_o ) {
	open(OUTFILE,"> $opt_o")
		|| die "Couldnt Open $opt_o for writing\n";
	print OUTFILE $str;
	close OUTFILE;
	chmod 0666,$opt_o;
}

# ===========================
# DONE
# ===========================
exit();

# ========================================================================

# ===========================
# PRINT THE PAGE
# ===========================
sub format_page()
{
my($str)="";

$str.=title("summary");
$str.= "PROCNAME is $procname$NEWL" if $procname ne "";
$str.= "Command To Run is $opt_C $NEWL" if defined $opt_C;
$str.= "File To Run From is $opt_F $NEWL" if defined $opt_F;
$str.=sprintf("EXECUTE TOOK %d SECONDS, RESULTS TOOK %d SECONDS $NEWL", $time_2-$time_1, $time_3-$time_2);
foreach (@errors) {
	$str.= "<FONT COLOR=RED>" if defined $opt_h;
	$str.=$_;
	$str.= "</FONT><br>" if defined $opt_h;
}

if( $#real_out > -1 ) {
	$str.=title( "showplan" );
	$str.= "<PRE>\n" if defined $opt_h;
	foreach (@real_out) { $str.=$_; }
	$str.= "</PRE>\n" if defined $opt_h;
}

if( $#io_out > -1 ) {
	$str.=title( "statsio" );
	$str.= "<PRE>\n" if defined $opt_h;
	foreach (@io_out) { $str.=$_; }
	$str.= "</PRE>\n" if defined $opt_h;
}

if( $#sql_cmd_text_out >= 0 ) {
	$str.=title( "helptext" );
	my($count)=1;

	foreach (@sql_cmd_text_out) {
		$str .= substr($count."     ",0,6).$_;
		$count++;
	}
}

if( $#lock_out > -1 ) {
	$str.=title( "locks" );
	$str.= "<PRE>\n" if defined $opt_h;
	foreach (@lock_out) { $str.=$_; }
	$str.= "</PRE>\n" if defined $opt_h;
}

if( $#res_out > -1 ) {
	$str.=title( "results" );
	foreach (@res_out) { $str.=$_; }
}

if( $#original_showplan >= 0 ) {
	$str.=title( "raw" );
	$str.= "<PRE>" if defined $opt_h;
	foreach (@original_showplan) { $str.=$_; }
	$str.= "</PRE>" if defined $opt_h;
}

if( $#exec_out >= 0 ) {
	$str.=title("history");
	$str.= "<PRE>\n" if defined $opt_h;
	foreach (@exec_out) { $str.=$_; }
	$str.= "</PRE>\n" if defined $opt_h;
}

if( defined $opt_h ) {
$str.= "<hr>End Of Output<hr>
This output was created by the free program analyze.
<ADDRESS>copyright &#169 1997-2000 By
<A HREF=\"http://www.edbarlow.com\">SQL Technologies</A>
<BR>
We appreciate
<A HREF=\"mailto:barlowedward\@hotmail.com\">feedback</A>.
on this product!
</ADDRESS>" if defined $opt_h;
} else {

	$str.= "

End Of Output

This Output produced by the Showplan Analyzer.
Copyright (c) 1998-2000 by Edward Barlow
URL: http://www.edbarlow.com
All Rights Reserved\n";
}
return $str;

}

# ===========================
# PAGE TITLE
# ===========================
sub title
{
	my($line)=@_;
	my($str);
	$line = $titles{$line};

	my($ltitle)=$line;
	$ltitle=~s/\s//g;
	$str= "<A NAME=\"$ltitle\">" if defined $opt_h;
	$str.= "\n<hr><center>\n<TABLE CELLPADDING=5 BGCOLOR=GAINSBORO BORDER=1><TR><TD BGCOLOR=GAINSBORO ALIGN=CENTER>" if defined $opt_h;
	$str.= "\n\n" if ! defined $opt_h;
	$str.= uc $line;
	$str.= "\n\n" if ! defined $opt_h;
	$str.= "\n</TABLE>\n</center></A>\n" if defined $opt_h;
	return $str;
}

# ===========================
# print table of contents entry and location of output file
# ===========================
sub print_toc
{
	my($line,$filename)=@_;
	$line = $titles{$line};
	return unless defined $line;
	my($ltitle)=$line;
	$ltitle=~s/\s//g;
	return if $ltitle eq "";
	print "<A HREF=\"#$ltitle\">$line</A>";
	print $NEWL;
}

# ===========================
# COLORIZE SQL
# ===========================
sub db_colorize_sql {
    my $sql_cmd_text_out = shift;
    $sql_cmd_text_out =~ s/\n/<br>\n/g;

	 # comments
	 $sql_cmd_text_out =~ s/(\/\*.*?\*\/)/<FONTCOLOR="BLUE">$1<\/FONT>/g;

	 # use FONTCOLOR and then respace out later to FONT COLOR
	 $sql_cmd_text_out =~ s/(\@+\w+)/<FONTCOLOR="GREEN">$1<\/FONT>/ig;
    $sql_cmd_text_out =~ s/\b(not null|null)\b/<FONTCOLOR="ORANGE">$1<\/FONT>/ig;
    $sql_cmd_text_out =~ s/\b(or|between|and|distinct|order by|avg|max|min|sum)\b/<FONTCOLOR="DARKRED">$1<\/FONT>/ig;
    $sql_cmd_text_out =~ s/\b(as|begin|declare|delete|drop|else|end|exec|exists|go|if|insert|procedure|return|set|update|values|from|select|where|create)\b/<b>$1<\/b>/ig;
    $sql_cmd_text_out =~ s/\b(tinyint|smallint|int|char|varchar|float|double|datetime|smalldatetime|money|smallmoney|numeric|decimal|text|binary|varbinary|image)\b/<i>$1<\/i>/gi;

    $sql_cmd_text_out =~ s/\t/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g;
    $sql_cmd_text_out =~ s/ /\&nbsp;/sg;
	 $sql_cmd_text_out =~ s/FONTCOLOR/FONT COLOR/g;

    $sql_cmd_text_out;
}

# ===========================
# Format Output row
# ===========================
sub row_output
{
	my($a,$b,$c);
	if( defined $opt_h ) {
	return("<TR><TD>$a</TD><TD>$b</TD><TD COLSPAN=4>$c</TD></TR>\n");
	} else {
	return("$a $b $c\n");
	}
}
sub format_output_row
{
	my($str)="";

	if( defined $opt_h )  {
		# TABLIFY THE HTML
		my($hilite,$normal)="";
		if( defined $opt_h and $is_tbl_scan and $tblrows{$cur_table}>1000 ) {
			$hilite="<FONT COLOR=RED>";
			$normal="</FONT>";
		} else {
			$hilite="";
			$normal="";
		}
		$str.="<TR><TD>".$step_number."</TD>";
		$str.="<TD>".$query_type."</TD>";
		$str.="<TD>$hilite".$cur_table."$normal</TD>";

		if(defined $tblrows{$cur_table} ) {
			$str .= "<TD>".$hilite."Rows=".$tblrows{$cur_table}."$normal</TD>"
		} else {
			$str .= "<TD>UnAvailable</TD>";
				#if( substr($cur_table,0,1) eq "#"
				#or substr($cur_table,0,9) eq "Worktable" );
		}

		$str.="<TD>Table Scan</TD>"     if $is_tbl_scan;
		$str.="<TD></TD>"       if ! $is_tbl_scan;
		$str.="<TD>$hilite$index_info$normal</TD>" if $index_info ne "";
		$str.="<TD></TD>" if $index_info eq "";
		$str.="</TR>\n";

		return $str;
	} else {
		$str.=$step_number." ";
		$str.=$query_type." ";
		$str.=$cur_table." ";

		if(defined $tblrows{$cur_table} ) {
			$str .= " Rows=".$tblrows{$cur_table}." "
		} else {
			$str .= " Rows=UnAvailable "
				if( substr($cur_table,0,1) eq "#"
				or substr($cur_table,0,9) eq "Worktable" );
		}

		$str.=" Table Scan"     if $is_tbl_scan;
		$str.=" $index_info" if $index_info ne "";

		$str.= "\n";
		return $str;
	}


}

sub reset_tbl_row_vals
{
	$cur_table="";
	$index_info="";
	$is_tbl_scan=0;
	$next_row_is_to_table=0;
	$from_tbl_row_num=0;
   $prnted_something=1;
}

sub sql_exec
{
	my($cmd,$do_results)=@_;
	print "Executing $cmd\n" if defined $opt_d;
	my(@parsed_query)=split /\n/,$cmd;
	if( defined $opt_e ) {
		push @exec_out,"EXECUTING    ".shift(@parsed_query)."\n";
		foreach (@parsed_query) {
			next if /^\s*$/;
			push @exec_out,"	     ".$_."\n";
		}
	}
   $DBPROC->dbcmd("$cmd");
   $DBPROC->dbsqlexec();
	$DBPROC->dbresults() if $do_results;
}

sub error_handler_mine {
	my ($db, $severity, $error, $os_error, $error_msg, $os_error_msg) = @_;

	my(@exclude_msgs)=(5701,5703,5704,4002,916,20012,20014,20018,20009,SYBESMSG);
	foreach (@exclude_msgs) {
		return INT_CANCEL if $_ == $error;
	}

	my($Sybase_Error) = "ERROR # $error: $error_msg";
	$Sybase_Error .= "\nSEVERITY $severity";
	$Sybase_Error .= "\nOS ERROR $os_error: ".$os_error_msg if defined $os_error_msg;

	print "Query Generated An Error: $Sybase_Error";
	exit;

	INT_CANCEL;
}

__END__

=head1 NAME

showplan_analyze.pl - showplan analyzer

=head2 DESCRIPTION

Analyze will analyze ddl by using showplan.  It can runs the program in an optional transaction and rolls back the query when it is done.  Its primary purpose is to reformat the showplan output, It produces color coded output files that contain query results, statistics io, showplan information, and readable summary output.

Analyze can output html directly, which is the best way to view results.

The best way to tell you what this tool does is to demonstrate.  If you are a unix user, the files example1, example2, and example3 contain code/stored procedures in them that can be run by the run_examples script (requires pubs). Please look at the example* files after you run the script (change the script so it has appropriate passwords.  Thats all there is to it.

Note this package requires perl 5.002 or later.  The software will work under all versions of sybase. The software requires sybperl.

=head2 USAGE

USAGE: analyze [-dL] B<-o> FILE B<-U> USER B<-P> PASSWORD B<-S> SERVER B<-D> DATABASE [ B<-F> FILE or B<-C> CMD ]

Showplan Analyzer Copyright (c) 1996-8 by Edward Barlow

B<-d> : debug mode

B<-L> : do lock analysis.  The session is trapped inside a 'BEGIN TRAN' and locks are measured.  The tran is rolled back after.  This aborts on operations that cant handle a tran (select into...), but should be set by default.

B<-h> : html'ize output

B<-n> : set noexec on - will work poorly if running a stored proc

B<-i> : show statistics io

B<-l> : show lock stuff

B<-o> : output to this file as well as to screen

B<-t> : show procedure text (proc name is first word after exec)
B<-r> : show raw showplan output

B<-e> : show sql the analyze program executes

=head2 FUNCTIONS
	THE FOLLOWING SUBROUTINES HAVE BEEN DEFINED AT THIS POINT:

	    sub usage()
	    sub message_handler_mine()
	    sub parse_showplan_output()
	    sub parse_stats_io_output()
	    sub lock_print()
	    sub normal_print()
	    sub results_print()
	    sub io_print()
	    sub output_print()
	    sub format_page()
	    sub title()
	    sub print_toc()
	    sub db_colorize_sql()
	    sub row_output()
	    sub format_output_row()
	    sub reset_tbl_row_vals()
	    sub sql_exec()
	    sub error_handler_mine()

=cut
