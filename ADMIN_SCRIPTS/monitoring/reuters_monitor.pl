#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use MlpAlarm;

#
# Script to monitor feedhandlers
#
use Mlpdata;

#
# set some var
my $conf=\%conf;
my $firststatus = 1;
my @d=();
my $CLOSED = 'C';
#
my $Control="/home/wombat/bin/reuterscontrol.cfg";

#
# open config file for feed handlers
#
open(CONF,"$Control") || die "Could not open $Control file\n";
while(<CONF>) {
   chomp;
	my($feed,$symbols)=split(/:/,$_);
	$conf->{$feed}->{symbol}=$symbols;
}
close(CONF);
#
my $msg;
#
# First, subscribe all the symbols

my @syms=();
my $ksyms = 0; 		# count of symbols requested
foreach my $FEED (  keys %{$conf} ) {
    my @Symbol = split(/\,/,$conf->{$FEED}->{symbol});
    foreach my $S ( @Symbol ) {
        push @syms, $S;
        $ksyms++;
    }
}
mlpGetSymbolList(@syms);
mlpAddFID($FI_SYM, 0);
mlpAddFID($FI_LPRICE, 0);
mlpAddFID($FI_STATUS, 0);

MlpBatchJobStart(-BATCH_ID=>"reuters_monitor.pl");

# Unfortunately, the perl version of the mlpdata library does not keep the FI_STATUS
# field in the in-memory data structure. The status has to be picked out of the
# data stream. So, subscribe to receive the data stream.
mlpSubscribe(1);

# OK -- Each symbol when subscribed first produces a snapshot. This occurs even if the
# symbol is illegal or the datafeed is down. Grab the status out of that snapshot.
# Ignore all other streaming data. And leave the loop after we get status on everything.
undef %sym;
$kstatus = 0;
while ($kstatus < $ksyms) {
    Mlpdata::readdata(\@d);
    $sym = 1;
    undef @o;
    for ($i = 0; $i <= $#d; $i++) {
        if ($sym == 1) {
            $sym = 0;
            $o[0] = $d[$i];
        } else {
            if ($d[$i] == $FI_STATUS) {
                $o[6] = $d[$i+1];
                $i++;
            } else {
                $i++;       # skip the unknown fid
            }
        }
    }
    if (!defined($status{$o[0]})) {
        $status{$o[0]} = $o[6];
        $kstatus++;
    }
}

foreach my $FEED (  keys %{$conf} ) {
	my @Symbol = split(/\,/,$conf->{$FEED}->{symbol});
	foreach my $S ( @Symbol ) {
                # We have the status, so use the information directly to feed the
                # building of the message to go out over tibco.
                if ($status{$S} eq $CLOSED) {
                    $ft = "primary";
                    $conf->{$FEED}->{$ft}->{'mdrvcount'} = undef;
                    $conf->{$FEED}->{$ft}->{'linecount'} = undef;
                    $conf->{$FEED}->{$ft}->{'status'} = 'Down';
                    $conf->{$FEED}->{$ft}->{'linestatus'} = 'down';
                    $conf->{$FEED}->{ft} = $ft;
                }
                else {
                    $ft = "primary";
                    $conf->{$FEED}->{$ft}->{'mdrvcount'} = undef;
                    $conf->{$FEED}->{$ft}->{'linecount'} = undef;
            	    $conf->{$FEED}->{$ft}->{'status'} = 'Normal';
            	    $conf->{$FEED}->{$ft}->{'linestatus'} = 'up';
                    $conf->{$FEED}->{ft} = $ft;
                }

		my $ft= $conf->{$FEED}->{ft};
		#
		# if no reply from feedhandler
		#
		$ft = 'primary' if length($ft) < 2;
		$conf->{$FEED}->{$ft}->{'linestatus'} = 'down' if length($conf->{$FEED}->{$ft}->{'linestatus'}) < 2;
		$conf->{$FEED}->{$ft}->{'status'} = 'Down' if length($conf->{$FEED}->{$ft}->{'status'}) < 2;
		#
		my($state)="OK";
		$state="ERROR" unless $conf->{$FEED}->{$ft}->{'status'} eq "Normal";
		$state="ERROR" unless $conf->{$FEED}->{$ft}->{'linestatus'} eq "up";
		my $message = "$FEED $ft $S $conf->{$FEED}->{$ft}->{'status'} $conf->{$FEED}->{$ft}->{'linestatus'}";
		my $UFEED= uc $FEED;
		my $US= uc $S;
		print "SYSTEM=REUTERS SUBSYSTEM=$UFEED STATE=$state MESSAGE: $message\n";
		MlpHeartbeat( -monitor_program=>"ReutersMonitor", -state=>$state, -system=>"REUTERS", -subsystem=>$UFEED, -message_text=>$message );
	}
}

MlpBatchJobEnd();

exit;
