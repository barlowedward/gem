use racks
go
drop table barlow_place_map
go
create table barlow_place_map (
	  wasp_site 	varchar(30),
	  wasp_location varchar(30),
	  wasp_floor 	varchar(30) null,
	  wasp_row 		varchar(30) null,
	  wasp_cabinet 	varchar(30) null,
	  dcm_placeid 	int 		null,
	  dcm_cabfloor	varchar(30) null,
	  dcm_cabrow	varchar(30) null,
	  dcm_cabcab	varchar(30) null
)
go
create index xxx on barlow_place_map (wasp_site,wasp_location,dcm_cabfloor,dcm_cabrow,dcm_cabcab)
go
insert 	  barlow_place_map
select distinct
'Site' = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Site'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
Location = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
'Floor' = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Floor'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
Row = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Row'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
Cabinet = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Cabinet'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
-1,
null,
null,
null
from 	gemalarms..INV_system s
where 	s.system_type ='WASP'
-- order by 1,2,3
go
-- select "update barlow_place_map set placid=X where wasp_site='"+wasp_site+"' and wasp_location='"+wasp_location+"'" from barlow_place_map

update barlow_place_map set dcm_placeid=2 where wasp_site='Elmsford' and wasp_location='CoLo Verizon - Elmsford'
update barlow_place_map set dcm_placeid=3 where wasp_site='Greenwich' and wasp_location='2nd Floor Data Center'
update barlow_place_map set dcm_placeid=3 where wasp_site='Greenwich' and wasp_location='3rd Floor Office'
update barlow_place_map set dcm_placeid=4 where wasp_site='1155 6th Ave' and wasp_location='CoLo Verizon - 1155'
update barlow_place_map set dcm_placeid=5,dcm_cabfloor='10' where wasp_site='666 5th Ave' and wasp_location='10th Floor Data Center'
update barlow_place_map set dcm_placeid=5,dcm_cabfloor='07' where wasp_site='666 5th Ave' and wasp_location='7th Floor Data Center'
update barlow_place_map set dcm_placeid=5,dcm_cabfloor='08' where wasp_site='666 5th Ave' and wasp_location='8th Floor Data Center'
update barlow_place_map set dcm_placeid=5,dcm_cabfloor='09' where wasp_site='666 5th Ave' and wasp_location='9th Floor New Data Center'
update barlow_place_map set dcm_placeid=5,dcm_cabfloor='09' where wasp_site='666 5th Ave' and wasp_location='9th Floor Old Data Center'
update barlow_place_map set dcm_placeid=8,dcm_cabfloor='04' where wasp_site='White Plains' and wasp_location='4th Floor Data Center'
update barlow_place_map set dcm_placeid=8,dcm_cabfloor='05' where wasp_site='White Plains' and wasp_location='5th Floor Data Center'
update barlow_place_map set dcm_placeid=13 where wasp_site='650 5th Ave' and wasp_location='32nd Floor Data Center'
update barlow_place_map set dcm_placeid=30 where wasp_site='Carteret' and wasp_location='CoLo Nasdaq - Carteret'
update barlow_place_map set dcm_placeid=30 where wasp_site='Carteret' and wasp_location='CoLo Verizon - Carteret'

update barlow_place_map set dcm_placeid=20,dcm_cabfloor='06',dcm_cabrow='100' where wasp_site='Chicago' and wasp_location='CoLo Equinix - Chicago'
update barlow_place_map set dcm_placeid=27,dcm_cabfloor='04',dcm_cabrow='200' where wasp_site='Chicago' and wasp_location='CoLo TELX - Chicago'

update barlow_place_map set dcm_placeid=25 where wasp_site='Ridgefield Park'
update barlow_place_map set dcm_placeid=29 where wasp_site='Weehawken'
update barlow_place_map set dcm_placeid=24 where wasp_site='Secaucus'
update barlow_place_map set dcm_placeid=7,dcm_cabfloor='I1',dcm_cabrow='02' ,dcm_cabcab='13'
	where wasp_site='White Plains' and wasp_location='LL1' and wasp_cabinet='13'

-- MERGE THE TWO WHITE PLAINS LOCATIONS
select distinct wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet , cntrycitybldgnum,floor,row,cabinet
into #WHP
from barlow_place_map b, machines m
where dcm_placeid=-1 and wasp_site='White Plains' and cntrycitybldgnum in (7,8)
and m.floor = wasp_floor
and m.row = wasp_row
and m.cabinet = wasp_cabinet

begin tran
delete barlow_place_map
from barlow_place_map b, #WHP p
where p.wasp_floor=b.wasp_floor
and	  p.wasp_row=b.wasp_row
and	  p.wasp_site=b.wasp_site
and	  p.wasp_location=b.wasp_location
and	  p.wasp_cabinet=b.wasp_cabinet

insert barlow_place_map select * from #WHP
commit tran
drop table #WHP
-- DONE WHP MERGE

-- MERGE THE TO RFP LOCATIONS
-- select * from barlow_place_map where wasp_site='Ridgefield Park' order by wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet
-- select distinct cntrycitybldgnum,floor,row,cabinet from machines where cntrycitybldgnum in (19,25) order by cntrycitybldgnum,floor,row,cabinet
select distinct wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet , cntrycitybldgnum,floor,row,cabinet
into #RFP
from barlow_place_map b, machines m
where wasp_site='Ridgefield Park' and cntrycitybldgnum in (19,25)
and upper(m.floor) = upper(wasp_floor)
and upper(m.row) = upper(wasp_row)
and upper(m.cabinet) = upper(wasp_cabinet)


begin tran
delete barlow_place_map
from barlow_place_map b, #RFP p
where p.wasp_floor=b.wasp_floor
and	  p.wasp_row=b.wasp_row
and	  p.wasp_site=b.wasp_site
and	  p.wasp_location=b.wasp_location
and	  p.wasp_cabinet=b.wasp_cabinet

insert barlow_place_map select * from #RFP
commit tran
drop table #RFP
-- DONE RFP MERGE

-- MERGE TWO LOCS GENERIC
declare @plid int, @plid2 int, @wasp_site varchar(30)
select  @plid=14,  @plid2=3, @wasp_site='Greenwich'

-- select * from barlow_place_map where wasp_site='Ridgefield Park' order by wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet
-- select distinct cntrycitybldgnum,floor,row,cabinet from machines where cntrycitybldgnum in (19,25) order by cntrycitybldgnum,floor,row,cabinet
select distinct wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet , cntrycitybldgnum,floor,row,cabinet
into #RFPX
from barlow_place_map b, machines m
where wasp_site=@wasp_site and cntrycitybldgnum in (@plid,@plid2)
and upper(m.floor) = upper(wasp_floor)
and upper(m.row) = upper(wasp_row)
and upper(m.cabinet) = upper(wasp_cabinet)

begin tran
delete barlow_place_map
from barlow_place_map b, #RFPX p
where p.wasp_floor=b.wasp_floor
and	  p.wasp_row=b.wasp_row
and	  p.wasp_site=b.wasp_site
and	  p.wasp_location=b.wasp_location
and	  p.wasp_cabinet=b.wasp_cabinet

insert barlow_place_map select * from #RFPX
commit tran
drop table #RFPX
-- DONE RFP MERGE


-- MASTER MERGE FOR SINGLE SITE LOCATIONS
/*
select distinct b.* , cntrycitybldgnum,floor,row,cabinet
from barlow_place_map b, machines m
where dcm_placeid=cntrycitybldgnum
and upper(m.floor) = upper(wasp_floor)
and upper(m.row) = upper(wasp_row)
and upper(m.cabinet) = upper(wasp_cabinet)
and isnull(dcm_cabfloor,m.floor)=m.floor
and isnull(dcm_cabrow,m.row)=m.row
and isnull(dcm_cabcab,m.cabinet)=m.cabinet
order by wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet
*/

select distinct wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet , cntrycitybldgnum,floor,row,cabinet
into #WHP
from barlow_place_map b, machines m
where dcm_placeid=cntrycitybldgnum
and upper(m.floor) = upper(wasp_floor)
and upper(m.row) = upper(wasp_row)
and upper(m.cabinet) = upper(wasp_cabinet)
and isnull(dcm_cabfloor,m.floor)=m.floor
and isnull(dcm_cabrow,m.row)=m.row
and isnull(dcm_cabcab,m.cabinet)=m.cabinet

-- select * from #WHP where cntrycitybldgnum=29

begin tran
delete barlow_place_map
from barlow_place_map b, #WHP p
where p.wasp_floor=b.wasp_floor
and	  p.wasp_row=b.wasp_row
and	  p.wasp_site=b.wasp_site
and	  p.wasp_location=b.wasp_location
and	  p.wasp_cabinet=b.wasp_cabinet

insert barlow_place_map select * from #WHP
commit tran

-- select * from barlow_place_map where dcm_placeid=29

-- FIX UP NULL FLOORS WITH QUICK UPDATE
--select distinct wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet , cntrycitybldgnum,floor,row,cabinet
update barlow_place_map
set dcm_cabrow=row
,dcm_cabfloor=floor
,dcm_cabcab=cabinet
from barlow_place_map b, machines m
where dcm_placeid=cntrycitybldgnum
and wasp_floor is null
and upper(m.row) = upper(wasp_row)
and upper(m.cabinet) = upper(wasp_cabinet)
and isnull(dcm_cabfloor,m.floor)=m.floor
and isnull(dcm_cabrow,m.row)=m.row
and isnull(dcm_cabcab,m.cabinet)=m.cabinet

select distinct wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet , cntrycitybldgnum,floor,row,cabinet
--	update barlow_place_map
--	set dcm_cabrow=row,dcm_cabfloor=floor,dcm_cabcab=cabinet
from barlow_place_map b, machines m
where dcm_placeid=cntrycitybldgnum
and  wasp_floor like '[0-9]'
and  wasp_row like '[0-9]'
and  wasp_cabinet like '[0-9]'
and convert(integer,m.floor) = convert(integer,wasp_floor)
and convert(integer,m.row = convert(integer,wasp_row)
and convert(integer,m.cabinet) = convert(integer,wasp_cabinet)
and upper(m.row) = upper(wasp_row)
and upper(m.cabinet) = upper(wasp_cabinet)
and isnull(dcm_cabfloor,m.floor)=m.floor
and isnull(dcm_cabrow,m.row)=m.row
and isnull(dcm_cabcab,m.cabinet)=m.cabinet


delete 	barlow_place_map
from  	barlow_place_map m
where   not exists ( select 1 from racks..machines b
						where 	b.cntrycitybldgnum 	= m.dcm_placeid
						and 	b.floor 			= m.dcm_cabfloor
						and 	b.row				= m.dcm_cabrow
						and 	b.cabinet 			= m.dcm_cabcab
