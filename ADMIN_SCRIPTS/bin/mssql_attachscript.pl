#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use MlpAlarm;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Repository;

use vars qw( $SYSTEMS $DATABASE $NOLOG $GO $DETACH $DEBUG $ATTACH $BOTH);

my($VERSION)=1.0;

$|=1;

sub usage
{
	return "Usage: mssql_attachscript.pl [--DATABASE=db] --GO --DEBUG --SYSTEMS=system[,system] [-ATTACH|-DETACH|-BOTH]\n";
}

die usage() if $#ARGV<0;
die "Bad Parameter List $!\n".usage() unless
   GetOptions(  "DATABASE=s"	=> \$DATABASE,
		"SERVER=s"	=> \$SYSTEMS,
		"DETACH"		=> \$DETACH,
		"ATTACH"		=> \$ATTACH,
		"BOTH"		=> \$BOTH,
		"GO"			=> \$GO,
		"SYSTEMS=s"	=> \$SYSTEMS,
		"DEBUG"		=> \$DEBUG,
		"NOLOG"		=> \$NOLOG
		);

cd_home();
my(@srv);
if( $SYSTEMS eq "ALL" ) {
	@srv=get_password(-type=>'sqlsvr');
} else {
	@srv=split(/,/,$SYSTEMS);
}
die "No Systems Defined" if $#srv<0;

if( $BOTH ) { $ATTACH=1; $DETACH=1; }
die "Must pass --ATTACH or --DETACH " unless $ATTACH or $DETACH;

MlpBatchJobStart(-BATCH_ID => "MssqlAttachScript");

my($login,$pass,$ctype,$srv,$cmd);
foreach $srv (@srv) {
	($login,$pass,$ctype)=get_password(-type=>'sqlsvr',-name=>$srv);
   	print "--Connecting to $srv\n";
   	my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
	if( ! $rcx ) {
      		print "--Cant connect to $srv as $login\n";
		next;
	}

	print "Getting Database List\n" if defined $DEBUG;
	my($server_type,@okdb)=dbi_parse_opt_D('%',1);

	if( $DATABASE ) {
		@okdb = split(/,/,$DATABASE);
	}

#   my(@rc)=dbi_query(-db=>"master",-query=>"select name from master..sysdatabases", -debug=>$DEBUG);
#	foreach ( @rc ) {}
#		my($db)=dbi_decode_row($_);

foreach my $db (@okdb) {
		next if $db eq "master"
			or $db eq "model"
			or $db eq "tempdb"
			or $db eq "msdb"
			or $db eq "Northwind"
			or $db eq "pubs";

		print "Working on Database $db\n" if defined $DEBUG;
		if( $DETACH ) {
			print "exec sp_detach_db '".$db."','true'\n";
			print "go\n" if $GO;
	#		next;
		}

		if( $ATTACH ) {
			dbi_use_database($db);
			my(@fname)=dbi_query(-db=>$db,-query=>"select fileid,filename from sysfiles order by fileid", -debug=>$DEBUG);
			print "exec sp_attach_db \"$db\"";
			foreach ( @fname ) {
				my($id,$file)=dbi_decode_row($_);
				$file=~s/\s+$//;
				$file=~s/^\s+//;
				print ",\@filename".$id."=\"".$file."\"";
			}
			print "\n";
			print "go\n" if $GO;
		}
	}

   	dbi_disconnect();
	print "Completed Work on Server $srv\n" if defined $DEBUG;
}

MlpBatchJobEnd();
exit(0);
__END__

=head1 NAME

mssql_attachscript.pl - create a set of sql statements to attach or detach your databases.

=head2 DESCRIPTION

When moving your sql server files around, it is a pain to create the scripts to attach
and detach the files.  This program will create a set of scripts capable of attaching
files once you detach the database and the sql statements needed to detach them as well.

The --GO option puts go's between lines

=head2 USAGE

Usage: mssql_attachscript.pl [--DATABASE=db] --DEBUG --SYSTEMS=system[,system] [-DETATCH] [-GO]

--GO will print go's

=cut

