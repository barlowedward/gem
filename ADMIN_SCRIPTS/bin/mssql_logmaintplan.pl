#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use MlpAlarm;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Repository;

use vars qw( $SYSTEMS $CONFIGFILE $NOCONFIGFILE $DEBUG $NOLOG $NOWRITECONFIGFILE);

my($VERSION)=1.0;

$|=1;

sub usage
{
	return "Usage: mssql_logmaintplan.pl --NOLOG --CONFIGFILE=file --NOCONFIGFILE --NOWRITECONFIGFILE --SYSTEMS=system[,system] --DEBUG\n";
}

my($COMMAND_RUN)=$0." ".join(" ",@ARGV);

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
   	"CONFIGFILE=s"	=> \$CONFIGFILE,
   	"NOCONFIGFILE"	=> \$NOCONFIGFILE,
		"NOLOG"		=> \$NOLOG,
		"DEBUG"		=> \$DEBUG,
		"NOWRITECONFIGFILE"		=> \$NOWRITECONFIGFILE,
		"SYSTEM=s"	=> \$SYSTEMS,
		"SYSTEMS=s"	=> \$SYSTEMS );


cd_home();
sub error_out {
   my($msg)=@_;
   $msg = "BATCH MssqlLogMaintPlan: ".$msg;

   MlpBatchJobEnd(-EXIT_CODE=>"ERROR", -EXIT_NOTES=>$msg);

   MlpEvent(
      -monitor_program=> "MssqlLogMaintPlan",
      -system=>  "",
      -message_text=> $msg,
      -severity => "ERROR"
   );
   die $msg;
}

print "mssql_logmaintplan.pl (batch MssqlLogMaintPlan) run at ".localtime(time)."\n";
print "  configfile=$CONFIGFILE\n";
print "  ignoring configfile per directive\n" if $NOCONFIGFILE;
print "  systems=$SYSTEMS\n" if $SYSTEMS;
print "\nTHIS PROGRAM GRABS SQL SERVER JOB INFORMATION & SAVES IT AS HEARTBEATS\n";

my(%sqid_by_svr);
if( defined $CONFIGFILE and -r $CONFIGFILE and ! $NOCONFIGFILE ) {
	open(CONF,$CONFIGFILE) or error_out( "Cant Read $CONFIGFILE $!" );
	foreach (<CONF>) {
		chomp;
		my($svr,$sqid)=split;
		my($a)=int($sqid);
		$sqid=0 if $a ne $sqid;
		#die "Squid $sqid from configfile is wrong (not integer $a)"
			#unless $a eq $sqid;
		$sqid_by_svr{$svr}=$sqid;
	}
	close(CONF);
}

MlpBatchJobStart(-BATCH_ID => "MssqlLogMaintPlan");

my(%syshash);
foreach ( split(/,/,$SYSTEMS) ) { $syshash{lc($_)}=1; }

my(@svr)=get_password(-type=>"sqlsvr");
my($login,$pass,$srv,$cmd);
foreach $srv (@svr) {
	next if defined $SYSTEMS and ! defined $syshash{lc($srv)};
	my($login,$pass)=get_password(-type=>"sqlsvr", -name=>$srv);
	print "Connecting to $srv as $login\n";
	my($rc)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
	if( ! $rc ) {
    	print "Cant connect to $srv as $login\n";
		next;
	}

#	my($query)=
#'select 	sequence_id, succeeded, plan_name, database_name,
#		server_name, activity, start_time, end_time, message
#		from sysdbmaintplan_history where server_name=@@servername';

my($query)='select distinct sequence_id,succeeded,plan_name,database_name,server_name,activity,start_time,end_time,message
	from sysjobs j, sysdbmaintplan_jobs pj, sysdbmaintplan_history h
	where j.enabled=1
	and pj.plan_id=h.plan_id
	and j.job_id = pj.job_id
	and server_name=@@servername';

	$query.=" and sequence_id>$sqid_by_svr{$srv}"
			if defined $sqid_by_svr{$srv};
 	$query.=" order by sequence_id";

	print $query,"\n" if defined $DEBUG;
   my(@rc)=dbi_query(-db=>"msdb",-query=>$query);
	my(%state,%endtime,%msg);	# key=subsystem
	foreach ( @rc ) {
		my(	$sequence_id, $succeeded, $plan_name, $database_name, $server_name,
				$activity, $start_time, $end_time, $message	) = dbi_decode_row($_);

		if( $plan_name=~/^\s*$/ ) {
			print "DATA\tid=$sequence_id\n\tsucc=$succeeded\n\tplam=$plan_name\n\tdb=$database_name\n\tsvr=$server_name\n\tact=$activity\n\tsttm=$start_time\n\tendtm=$end_time\n\tmsg=$message)\n";
			$plan_name="MssqlLogMaintPlan";
			$message="ERROR FETCHING DATA - $sequence_id - $query - $COMMAND_RUN";
			print 'Failed Query='.$query."\n";
		}

		# It is NOT an error to have this
		if( $activity =~ /backup transaction log/i and $message=~/^Backup can not be performed on this database/ ) {
			print "Plan=$plan_name Db=$database_name Setting state=ok for tran log settings missmatch\n";
			$succeeded=1;
		}

		my($key)=$database_name." ".$activity;
		$state{$key} = $succeeded;
		if( $message =~ /^\s*$/ ) {
			$msg{$key}   = "PLAN=$plan_name Activity=$activity";
		} else {
			$msg{$key}   = "PLAN=$plan_name MSG=$message";
		}
		$endtime{$key} = $end_time;
		$sqid_by_svr{$srv}=$sequence_id;
		#		if( $dat[1] == 0 or ! defined $state{$dat[2]} ) {
		#			$state{$dat[2]} = $dat[1];
		#			$msg{$dat[2]}   .= "DB=$dat[3] ";
		#			$msg{$dat[2]}   .= $dat[5]." ".$dat[8];
		#			$msg{$dat[2]}   .= "\n";
		#		}
		#		$endtime{$dat[2]} = $dat[7];
		#		$sqid_by_svr{$srv}=$dat[0];
	}

	my($rows)=$#rc+1;
   print $rows," Rows Processed\n";
	foreach ( sort keys %state ) {
		if( $state{$_} == 1 ) {
			print "Succeeded: system=$srv subsy=$_ time=$endtime{$_} Txt=$msg{$_}\n";
			MlpHeartbeat(
				-monitor_program 	=> "MssqlLogMaintPlan",
				-system 				=>	$srv,
				-state => "COMPLETED",
				-debug=>$DEBUG,
				-subsystem => $_,
				-event_time => $endtime{$_},
				-message_text=> "Succeeded: ".$msg{$_},
				-batchjob=>1 ) unless defined $NOLOG;
		} else {
			print "####################################\n";
			print "System=$srv Subsys=$_	EndTime=$endtime{$_}\n";
			print "Alert: $msg{$_}\n";
			print "####################################\n";
			MlpHeartbeat(
				-monitor_program => "MssqlLogMaintPlan",
				-system =>	$srv,
				-state => "ERROR",
				-debug=>$DEBUG,
				-subsystem => $_,
				-event_time => $endtime{$_},
				-message_text=> $msg{$_},
				-batchjob=>1 ) unless defined $NOLOG;
		}
	}
   dbi_disconnect();
}

if( defined $CONFIGFILE and ! $NOWRITECONFIGFILE and ! $NOCONFIGFILE ) {
	open(CONFOUT,">".$CONFIGFILE) or error_out( "Cant Write $CONFIGFILE $!" );
	foreach( keys %sqid_by_svr ) {
		print CONFOUT $_," ",$sqid_by_svr{$_},"\n";
	}
	close(CONFOUT);
}

MlpBatchJobEnd();
exit(0);
__END__

=head1 NAME

mssql_logmaintplan.pl - read logmaintplan table in mssql and save as heartbeats

=head2 DESCRIPTION

Reads the logmaintplan table in mssql - which stores sql server job states
and stores the results as heartbeats.

=cut

