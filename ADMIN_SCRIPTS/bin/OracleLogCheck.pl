#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use Getopt::Long;
#use Repository;
#use DBI;
#use MlpAlarm;
use Time::Local;
#use CommonFunc;
#use DBIFunc;
#use Do_Time;

use vars qw( $BATCHID $OUTFILE $LOG_DIRECTORY $DEBUG);

$|=1;
my($VERSION)="1.0";

# changedir to log_directory in which the executable exists
my($curdir)=dirname($0);
chdir $curdir or die "Cant chdir to $curdir : $! \n";

sub usage {
	print @_,"\n";
	return "Usage: OracleLogCheck.pl --LOG_DIRECTORY=dir --OUTFILE=file --BATCHID=name --DEBUG";
}

$LOG_DIRECTORY="//adsrv045/d\$/Oracle/ArchiveLogsFromProd";
my($DUMPDIR) = "//adsrv045/d\$/Oracle/Robocopyfromprod/SCPLAY/hot/data";
my($DUMPDIR2)= "//adsrv046/d\$/backup/SCPLAY/hot/data";

die usage("Bad Parameter List $!\n") unless
	GetOptions(
			"OUTFILE=s"				=>\$OUTFILE,
			"BATCHID=s"				=>\$BATCHID,
			"LOG_DIRECTORY=s"				=>\$LOG_DIRECTORY,
			"DEBUG"					=>\$DEBUG
			 );


print "AUDITING DUMP DIRECTORY $DUMPDIR\n";
opendir(DIR,$DUMPDIR) || die("Cant read DUMPDIR $DUMPDIR\n");
my(@dd) = grep(!/^\./,readdir(DIR));
closedir(DIR);

print "AUDITING DUMP DIRECTORY $DUMPDIR2\n";
opendir(DIR,$DUMPDIR2) || die("Cant read DUMPDIR2 $DUMPDIR2\n");
my(@dd2) = grep(!/^\./,readdir(DIR));
closedir(DIR);

my(%filestate);
foreach ( @dd )  { $filestate{$_}="ON 1"; }
foreach ( @dd2 ) {
	if( $filestate{$_} eq "ON 1" ) {
		$filestate{$_}="ON BOTH";
	} else {
		$filestate{$_}="ON 2";
	}
}

my(@err);

my($fcount, $badcount)=(0,0);
foreach ( sort keys %filestate ) {
	$fcount++;
	#if( $filestate{$_} eq "O
	next if $filestate{$_} eq "ON BOTH";
	$badcount++;
	print $_, " " , $filestate{$_}, "\n";
}

if( $badcount == 0 ) {
	print "AUDITED $fcount FILES - ALL IS WELL\n";
} else {
	print "AUDITED $fcount FILES - $badcount FILES ARE BAD\n";
	push @err, "AUDITED $fcount FILES - $badcount FILES ARE BAD\n";
}

print "\nAUDITING LOG DIRECTORY $LOG_DIRECTORY\n";
die "NO LOG_DIRECTORY $LOG_DIRECTORY\n" unless -d $LOG_DIRECTORY;
die "NO LOG_DIRECTORY $LOG_DIRECTORY unreadable\n" unless -r $LOG_DIRECTORY;
opendir(DIR,$LOG_DIRECTORY) || die("Cant read $LOG_DIRECTORY LOG_DIRECTORY\n");
my(@dirlist) = grep(!/^\./,readdir(DIR));
closedir(DIR);

my($lastnumber)=0;
my($mintime,$maxtime);
foreach (sort @dirlist) {
	my(@statinfo)=stat("$LOG_DIRECTORY/$_");
	s/^......//;
	s/..........ARC$//;

	# print "size=$statinfo[7] mtime=$statinfo[8] --\n";
	# print "ltime=".localtime($statinfo[8])."\n";
	my($TT)=localtime($statinfo[8]);
	$mintime=$statinfo[8] unless $mintime;
	$maxtime=$statinfo[8];
	if( $lastnumber!= 0 ) {
		if( $lastnumber +1 != $_  ) {
			print "\n$_ WARNING - NUMBERS NOT INCREMENTAL $_/$lastnumber\n" ;
			push @err, "$_ WARNING - NUMBERS NOT INCREMENTAL $_/$lastnumber\n" ;
		} else {
			print ".";
			# print "$_ OK\n";
		}
	} else {
		print ".";
		# print $_,"\n";
	}
	$lastnumber=$_;
	#print "$_\n";
}

print "\n";
print "Min Time = ".localtime($mintime),"\n";
print "Max Time = ".localtime($maxtime),"\n";
if( time-$maxtime > 6*60*60 ) { # 6 horus
	push @err,"MAXTIME not within 6 hours\n";
}

if( time-$mintime < 10*24*60*60 ) { # 6 horus
	push @err,"MINTIME < 10 days\n";
}

foreach (@err) { print "$_\n"; }
print "THERE ARE NO ERRORS\n" if $#err<0;
