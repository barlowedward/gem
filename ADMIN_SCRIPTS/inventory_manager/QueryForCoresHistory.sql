use tempdb
go
DROP TABLE INV_cores_by_location2
go
CREATE TABLE INV_cores_by_location2 (
  cores_201101	int  null,
  cores_201011	int  null,
  cores_201007	int  null,
  cores_201004	int  null,
  cores_200912	int  null,

  bu_name  varchar(255)  NULL,
  location varchar(255)  NULL,
  strategy varchar(255)  NULL,
  contact  varchar(255)  NULL,
  account varchar(255) default NULL
)
sp__whodo
go
delete INV_cores_by_location2
select top 100 * from racks..BarlowMachineView

select * from INV_cores_by_location2 l, gemalarms..INV_contact c
where l.contact=c.contact

use gemalarms
select * into INV_contact_20110204 from INV_contact
use tempdb
select * from INV_cores_by_location2 where contact not in (select contact from gemalarms..INV_contact)

update gemalarms..INV_contact
set business_unit = ss.account,
	display_name = bu_name,
	location = ss.location,
	account	 = strategy
from INV_cores_by_location2 ss, gemalarms..INV_contact c
where ss.contact = c.contact

update gemalarms..INV_contact
set location = ''
where location is null

update gemalarms..INV_contact
set display_name = ''
where display_name is null
s
update gemalarms..INV_contact
set account = ''
where account is null
update tempdb..INV_cores_by_location2 set contact = 'aanche@mlp.com' where  bu_name='ANCHE'
select cores_200912,bu_name,location,strategy,contact,account  from tempdb..INV_cores_by_location2
select * from INV_contact where lower(contact) like '%anche%'
select * from tempdb..INV_cores_by_location2
update INV_core_history set is_year_end='Y' where  datepart(mm,audit_date)!=2
select * from INV_core_history order by audit_date asc

update INV_core_history set is_quarter_end = 'Y' where
	( datepart(mm,audit_date)=3 and datepart(dd,audit_date)=31 ) or
	( datepart(mm,audit_date)=6 and datepart(dd,audit_date)=30 ) or
	( datepart(mm,audit_date)=9 and datepart(dd,audit_date)=30 ) or
	( datepart(mm,audit_date)=12 and datepart(dd,audit_date)=31 )
update INV_core_history set is_week_end = 'Y' where datepart(dw,audit_date)=1
update INV_core_history set is_year_end = 'Y' where datepart(mm,audit_date)=12 and datepart(dd,audit_date)=31


select datepart(dw,audit_date),datepart(mm,audit_date),* from INV_core_history
update INV_core_history set is_quarter_end='Y' where  datepart(mm,audit_date)!=2
update INV_core_history set is_week_end='Y'    where  datepart(mm,audit_date)!=2
update INV_core_history set is_year_end='Y' where  datepart(mm,audit_date)!=2

select top 10 datepart(mm,audit_date),* from INV_core_history where datepart(mm,audit_date)=12

insert INV_Core_history select distinct contact,isnull(location,''),"12/31/2009", cores_200912, 0,'unixinv','N','N','N' from tempdb..INV_cores_by_location2 order by contact,location
insert INV_Core_history select distinct contact,isnull(location,''),"3/31/2010", cores_201004, 0,'unixinv','N','N','N' from tempdb..INV_cores_by_location2 order by contact,location
insert INV_Core_history select distinct contact,isnull(location,''),"6/30/2010", cores_201007, 0,'unixinv','N','N','N' from tempdb..INV_cores_by_location2 order by contact,location
insert INV_Core_history select distinct contact,isnull(location,''),"9/30/2010", cores_201011, 0,'unixinv','N','N','N' from tempdb..INV_cores_by_location2 order by contact,location
insert INV_Core_history select distinct contact,isnull(location,''),"12/31/2010", cores_201101, 0,'unixinv','N','N','N' from tempdb..INV_cores_by_location2 order by contact,location
delete INV_Core_history
select top 500 datepart(dw,audit_date),* from INV_Core_history order by contact
select sum(num_cores) from INV_core_history where audit_date='Feb  7 2011'
select

select distinct top 5 convert(varchar,audit_date,101) from INV_core_history where datepart(dw,audit_date)=1 order by audit_date desc

select distinct audit_date from INV_core_history



select distinct attribute_value from INV_system_attribute where attribute_name='location' and system_type='unixinv'

select distinct c.contact from INV_contact c, tempdb..INV_cores_by_location2  t
where c.contact=t.contact and t.location=c.location
