#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Std;
use File::Basename;
use CommonFunc;

use vars qw( %CONFIG $opt_C $opt_D $opt_d);

#Copyright (c) 2005-2008 by SQL Technologies.
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
  	print $0.' -Ccmd_to_get_files -Ddbname [-d]';
	return "\n";
}

$| =1;


die usage("Bad Parameter List\n") unless getopts('C:D:d');

open(CMD,$opt_C ." |" ) or die "Cant run command $opt_C\n";
#my(@rc);
my($max_date)=0;
while (<CMD>) {
	next unless /\.$opt_D\./;
	chop;chomp;
#	push @rc, $_;
	/\.(\d+\.\d+)\./;
	$max_date=$1 if $1>$max_date;
#	print "DATE=$1\n";

}
close(CMD);

print $max_date,"\n";


__END__

=head1 NAME

get_latest_filedate.pl - get the latest file date from a directory

=head2 EXAMPLE

IMDBDATE=`/usr/local/bin/perl /apps/sybmon/dev/ADMIN_SCRIPTS/dbi_backup_scripts/get_latest_filedate.pl -C"ssh sybase@sybhost ls /export/home/sybase-dump" -Dmydb`
echo loading db for date=$IMDBDATE
echo ---- DB=mydb DATE=$IMDBDATE >> $FILE
echo set nocount on >> $FILE
echo 'select getdate()' >> $FILE
echo go >> $FILE
echo load database mydb  >> $FILE
echo from      \"compress::/export/home/sybase-dump/sybhost.mydb.dbdump.$IMDBDATE.S1\" >> $FILE
echo stripe on \"compress::/export/home/sybase-dump/sybhost.mydb.dbdump.$IMDBDATE.S2\" >> $FILE
echo stripe on \"compress::/export/home/sybase-dump/sybhost.mydb.dbdump.$IMDBDATE.S3\" >> $FILE
echo stripe on \"compress::/export/home/sybase-dump/sybhost.mydb.dbdump.$IMDBDATE.S4\" >> $FILE
echo go >> $FILE
echo online database mydb >> $FILE
echo go >> $FILE
echo 'select getdate()' >> $FILE
echo go >> $FILE

