#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#GENERIC REPOSITORY RUN

# Copyright (c) 2009-2011 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use Repository;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use CommonFunc;
use CGI qw( :standard );
use CGI::Carp qw(fatalsToBrowser);


my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $BACKUPDIR1 $BACKUPDIR2 $BACKUPDIR3 $NOPRODUCTION $PASSWORD $OUTDIR $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- (native authentication) \n".
$PROGRAM_NAME." -DATABASE=db  -SERVER=SERVER
-or- \n".
basename($0)." -DOALL=sybase
-or- \n".
basename($0)." -DOALL=sqlsvr

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTDIR=Outfile
   --NOEXEC\n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTDIR=s"			=> \$OUTDIR ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		"DOALL=s"			=> \$DOALL ,

		"BACKUPDIR1=s"			=> \$BACKUPDIR1,
		"BACKUPDIR2=s"			=> \$BACKUPDIR2,
		"BACKUPDIR3=s"			=> \$BACKUPDIR3,

		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTDIR			=> $OUTDIR ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL ,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

exit(0);

my($CONSOLE_DIRECTORY,$gem_root_dir);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";

	$gem_root_dir=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gem_root_dir;
	print "  GEM ROOT=$gem_root_dir",br;

	my $GemConfig=read_gemconfig( -debug=>$DEBUG);
	$CONSOLE_DIRECTORY = $gem_root_dir."/data/CONSOLE_REPORTS";
}

my($filename);

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;
	my($server_type,@databaselist)=dbi_parse_opt_D("%",1);
	print "DB LIST=",join(" ",@databaselist),"\n" if $DEBUG;

	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();

	my($dtype);
	if( $DBTYPE eq "SQL SERVER" ) {
		$dtype='sqlsvr';
		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";
		$filename = $CONSOLE_DIRECTORY."/console_sqlsvr/${cursvr}_report.htm";
	} else {
		$dtype='sybase';
		$filename = $CONSOLE_DIRECTORY."/console_sybase/${cursvr}_report.htm";
		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";

		# get current locked/expired status from the repository
		#my($q)="select credential_name, attribute_key, attribute_value
		#	print "(noexec)" if $NOEXEC and $DEBUG;
		#	print $q,"\n" if $DEBUG;
		#	MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
		#  my(@lastresults)=MlpAlarm::_querywithresults($q,1)  unless $NOEXEC
		#  foreach (@lastresults) {
		#		my(@vals) = dbi_decode_row($_);
	}

	print "Writing $filename\n";
	my($rcx)=open( FF, "> $filename" );
	if(! $rcx) { print "Cant Open $filename $!\n";warn "Cant Open $filename $!\n"; return; }
	print FF "$DBTYPE CONFIGURATION REPORT\n",br;
	print FF "Report Run At: ".localtime().p;
	print FF h1( "SERVER REPORT FOR $cursvr ($DBTYPE)" );

	print FF "Server Version = $DBVERSION",br;
	print FF "Server Patch Level = $PATCHLEVEL",br;

	print FF h2("Configuration");
	print FF "The following attributes are collected from various internal databases.  It is possible that some of these
	attributes have not been updated if the server moves or is upgraded<br>";

	my(@list)= ( 'asset description',
		'asset tag',
		'totalphysmemory',
		'business_unit',
		'contact',
		'contact1',
		'contact2',
		'contact3',
		'description',
		'host',
		'hw_type',
		'location',
		'state',
		'alertgr1',
		'alertgr2',
		'alertgr3',
		'processor_identifier',
		'processor_name',
		'processor_speed',
		'processor_vendor',
		'ProductVersion',
		'plbuild' );
	my($q)= "Select distinct attribute_name, attribute_value, mod_date from INV_system_attribute where system_name='".
				$cursvr."' and attribute_name in (";
	foreach (@list) { $q .= "'".$_."',"; }
	$q =~ s/,$/\) order by attribute_name, mod_date/;

	print FF "<PRE>",$q,"</PRE>" if $DEBUG;

	my(@rc) = _querywithresults( $q , 1);
	print FF "<TABLE><TR><TH>Attribute</TH><TH>Value</TH><TH>Data Date</TH></TR>";
	my($lastattnm,$lastval)= ('xx','xx');
	foreach (@rc) {
		my @rcdat=dbi_decode_row($_);
		$rcdat[0] =~ s/\d+$//;
		$rcdat[0] =~ s/\_/ /;

		if( $lastattnm ne $rcdat[0] or $lastval ne $rcdat[1] ) {				# skip dups
			print FF "<TR>";
			print FF "<TD>".b($rcdat[0])."</TD>";
			print FF "<TD>".$rcdat[1]."</TD>";
			print FF "<TD>".$rcdat[2]."</TD>";
			print FF "</TR>";
		}
		$lastattnm = $rcdat[0];
		$lastval   = $rcdat[1];

	}
	print FF "</TABLE>";

	print FF h2("Server Metadata");
	print FF "The following attributes are collected from the dataserver<br>";

	my(@rc)= dbi_query(-db=>'master',-query=>"exec sp__metadata",-debug=>$DEBUG);
	if( $#rc<0 ) {
		output("No results for query $q\n");
	}

	print FF "<TABLE><TR><TH>Property</TH><TH>Value</TH></TR>";
	foreach (@rc) {
		my @rcdat=dbi_decode_row($_);

		next unless $rcdat[0] =~ /^Cluster/i or $rcdat[0] =~ /^SharedDrive/i or $rcdat[0] =~ /^ServerName/i
			or $rcdat[0] =~ /ProductVersion/i or $rcdat[0] =~ /ProductLevel/i or $rcdat[0] =~ /MachineName/i
			or $rcdat[0] =~ /^Edition/i;

		print FF "<TR>";
		print FF "<TD>".b($rcdat[0])."</TD>";
		print FF "<TD>".$rcdat[1]."</TD>";
		print FF "</TR>";

	}
	print FF "</TABLE>\n";

	print FF h2("Database Sizes");
	pr_query("master","exec sp__helpdb \@dont_format=\'Y\'","1,2,3","2,3");

	# print FF h2("Space Growth");
	# print FF h2("Transaction Log Growth");
	# print FF h2("Summary Benchmarks");
	print FF h2("Backup Status");

#	my($q)="SELECT
#		T1.Name as DatabaseName,
#		COALESCE(Convert(varchar(12), MAX(T2.backup_finish_date), 101),'Not Yet Taken') as LastBackUpTaken,
#		COALESCE(Convert(varchar(12), MAX(T2.user_name), 101),'NA') as UserName
#		FROM sys.sysdatabases T1
#		LEFT OUTER JOIN msdb.dbo.backupset T2 ON T2.database_name = T1.name
#		where T1.Name not in ('master','model','tempdb','msdb','distribution','SQLTrace')
#		GROUP BY T1.Name
#		ORDER BY T1.Name";

	my($q)="SELECT
		T1.Name as DatabaseName,
		COALESCE(Convert(varchar, MAX(T2.backup_finish_date), 101),'Not Yet Taken') as LastBackUpTaken,
		COALESCE(Convert(varchar, MAX(T2.user_name), 101),'NA') as UserName,
		case when  status & 1024 = 1024 then 'Yes' else 'No' end as Is_readonly,
		case when  status & 32 != 0 or status&64!=0 or status&128!=0 or status&512!=0
			then 'Yes' else 'No' end as Is_Offline
		FROM sysdatabases T1
		LEFT OUTER JOIN msdb.dbo.backupset T2 ON T2.database_name = T1.name
		where T1.Name not in ('master','model','tempdb','msdb','distribution','SQLTrace')
		GROUP BY T1.Name,T1.status
		ORDER BY T1.Name";

	$q =~ s/sys.sysdatabases/sysdatabases/ if $PATCHLEVEL < 9;
	print FF "<PRE>",$q,"</PRE>" if $DEBUG;

	my(@rc)= dbi_query(-db=>'master',-query=>$q,-debug=>$DEBUG);
	if( $#rc<0 ) {
		output("No results for query $q\n");
	}

	print FF "<TABLE><TR><TH>Database Name</TH><TH>Last Backup Taken</TH><TH>User name</TH><TH>Is Readonly</TH><TH>Is Offline</TH></TR>";
	foreach (@rc) {
		my($rowcolor)='';
		my @rcdat=dbi_decode_row($_);
		$rowcolor='BGCOLOR=yellow' if $rcdat[3] eq "No" and $rcdat[4] eq "No";
		$rowcolor='BGCOLOR=red' if $rcdat[3] eq "No" and $rcdat[4] eq "No" and $rcdat[2] eq "Not Yet Taken";
		print FF "<TR $rowcolor>";
		print FF "<TD>".$rcdat[0]."</TD>";
		print FF "<TD>".$rcdat[1]."</TD>";
		print FF "<TD>".$rcdat[2]."</TD>";
		print FF "<TD>".$rcdat[3]."</TD>";
		print FF "<TD>".$rcdat[4]."</TD>";
		print FF "</TR>";
	}
	print FF "</TABLE>";

	if( ! $BACKUPDIR1 and ! $BACKUPDIR2 and ! $BACKUPDIR3 ) {
		print "No backup directories passed to the configuration",br;
	} else {
		if( $BACKUPDIR1 ) {
			print "Files Found in $BACKUPDIR1\n",br;
			opendir( DD, $BACKUPDIR1 ) or die "Cant read BACKUPDIR1\n";
			foreach ( grep( !/^\./, readdir(DIR) ) ) {
				print $_,br;
			}
			closedir(DIR);
		}
		if( $BACKUPDIR2 ) {
			print "Files Found in $BACKUPDIR2\n",br;
			opendir( DD, $BACKUPDIR2 ) or die "Cant read BACKUPDIR2\n";
			foreach ( grep( !/^\./, readdir(DIR) ) ) {
				print $_,br;
			}
			closedir(DIR);
		}
		if( $BACKUPDIR3 ) {
			print "Files Found in $BACKUPDIR3\n",br;
			opendir( DD, $BACKUPDIR3 ) or die "Cant read BACKUPDIR3\n";
			foreach ( grep( !/^\./, readdir(DIR) ) ) {
				print $_,br;
			}
			closedir(DIR);
		}
	}
	# print FF h2("Log Shipping Status");
	# print FF h2("Heartbeats");
	# print FF h2("Events");

	close(FF);

	output( br );
}

sub pr_query
{
	my($db,$query,$col_print,$col_sum)=@_;
	my(@results)= dbi_query(-db=>$db,-query=>$query,-print_hdr=>1,-add_batchid=>1,-debug=>$DEBUG);
	if( $#results<0 ) {
		output("No results for query $query\n");
	}
	my(@columns_to_print)=split(/,/,$col_print);
	my(@columns_to_sum)=split(/,/,$col_sum);

	my(@summary_results);
	print "Query returned $#results Rows\n" if defined $DEBUG;

	output( "<TABLE>" );
	my($rowcnt)=0;
	foreach( @results ) {
		$rowcnt++;
		my @rcdat=dbi_decode_row($_);
		if( $rowcnt==1 ) {
			print FF "<TR>";
			foreach (@columns_to_print ) {
				print FF "<TH>".uc($rcdat[$_])."</TH>";
			}
			print FF "</TR>";
			next;
		}

		print FF "<TR>";
		foreach (@columns_to_print ) {
			print FF "<TD>".$rcdat[$_]."</TD>";
		}
		print FF "</TR>";

		foreach (@columns_to_sum ) {
			$summary_results[$_] += $rcdat[$_];
		}

	}

	print FF "<TR>";
	foreach (@columns_to_print ) {

		if( $summary_results[$_] ) {
			print FF "<TD>".b($summary_results[$_])."</TD>";
		} else {
			print FF "<TD>Total</TD>";
		}
	}
	print FF "</TR>";

	output( "</TABLE>" );
	output("\n");
}

sub output
{
	if( defined $filename ) {
		print FF @_ ;
	} else {
		print @_;
	}
}

__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

