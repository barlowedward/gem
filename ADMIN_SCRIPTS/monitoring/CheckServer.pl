#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Sys::Hostname;
use DBIFunc;
use Repository;
use MlpAlarm;
use CommonFunc;
use File::Basename;
use Data::Dumper;

use vars qw( $BATCH_ID $BLOCKTIME $DEBUG $DOALL $FIRSTSERVER $LASTSERVER $HIGHCPU $HIGHIO $LONGRUNNING $FATAL_MAILTO
             $NOLOCK $NOMONITOR $NOPRODUCTION $PRODUCTION $NOSA $OUTFILE $PASSWORD $PERSIST $PERSIST_MAIL_DELAY
             $RUNNABLE $SERVER $SLEEPTIME $USER $WARN_MAILTO $ZOMBIE );

use vars qw( $MAXNUMSERVERS );

my($NAME)='CheckServer.pl';
my($VERSION)="1.02";
my($MAX_NUM_CONNECTIONS)=99;  # maximum number of connections in --PERSIST

# Copyright (c) 1997-2011 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

my(%current_connection);
our($current_server);
my(%persist_initialized);
my(%LAST_EMAIL_TIME);         # key=servername, value=time
my(%LAST_EMAIL_OFFSET);
my($PERSIST_ERROR_FOUND);
my($num_errors_found);
my(%DBTYPE, %DBVERSION);      # key=servername

# NEW MAIL FUNCTIONS
my($MAIL_SUBJECT,$MAIL_TEXT,$MAIL_TO,$MAIL_FROM,$MAIL_HOST);

sub usage
{
  print @_;
  print "
$NAME --BATCH_ID=id --PERSIST [--SLEEPTIME=120] [--PERSIST_MAIL_DELAY=300] [--[NON]PRODUCTION] [--DOALL={sybase|sqlsvr}] [--FIRSTSERVER=n] [--LASTSERVER=m] [--OUTFILE=log_file] [--MAILTO=x,y,z] [--WARN_MAILTO=a,b,c] ...
  - or -
$NAME --BATCH_ID=id --SERVER=server_name [--USER=sa] --PASSWORD=sa_password [--OUTFILE=log_file] [--MAILTO=x,y,z] [--WARN_MAILTO=a,b,c] ...

For the complete list of command line arguments and their default values
refer to the POD docummentation at the end of the program.\n\n";

  return "\n";
}

# $| =1;

die usage("") if $#ARGV < 0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
   "BATCH_ID=s"           => \$BATCH_ID,
   "BATCHID=s"           => \$BATCH_ID,
   "BLOCKTIME=s"          => \$BLOCKTIME,
   "TIME=s"               => \$BLOCKTIME,
   "DEBUG"     	        => \$DEBUG,
   "DOALL=s"              => \$DOALL,
   "FIRSTSERVER=i"        => \$FIRSTSERVER,
   "LASTSERVER=i"         => \$LASTSERVER,
   "HIGHCPU=s"            => \$HIGHCPU,
   "HIGHIO=s"             => \$HIGHIO,
   "LONGRUNNING=s"        => \$LONGRUNNING,
   "MAILTO=s"             => \$FATAL_MAILTO,
   "NOLOCK"               => \$NOLOCK,
   "NOMONITOR"            => \$NOMONITOR,
   "NOPRODUCTION"         => \$NOPRODUCTION,
   "PRODUCTION"           => \$PRODUCTION,
   "NOSA"                 => \$NOSA,
   "OUTFILE=s"            => \$OUTFILE,
   "PASSWORD=s"           => \$PASSWORD,
   "PERSIST"              => \$PERSIST,
   "PERSIST_MAIL_DELAY=s" => \$PERSIST_MAIL_DELAY,
   "RUNNABLE=s"           => \$RUNNABLE,
   "SERVER=s"             => \$SERVER,
   "SYSTEM=s"             => \$SERVER,
   "SLEEPTIME=s"          => \$SLEEPTIME,
   "USER=s"               => \$USER,
   "WARN_MAILTO=s"        => \$WARN_MAILTO,
   "ZOMBIE=s"             => \$ZOMBIE );

$NOMONITOR = 0            unless $NOMONITOR;
$PERSIST_MAIL_DELAY = 300 unless $PERSIST_MAIL_DELAY;
$BLOCKTIME = 30           unless $BLOCKTIME;
$LONGRUNNING = 0          unless $LONGRUNNING;
$HIGHCPU = 0              unless $HIGHCPU;
$HIGHIO = 0               unless $HIGHIO;
$RUNNABLE = 0             unless $RUNNABLE;
$SLEEPTIME = 120          if $PERSIST and ! $SLEEPTIME;
$USER = 'sa'              unless $USER;
$ZOMBIE = 60              unless $ZOMBIE;

print "CheckServer.pl: run at ". localtime(time)." on host ".hostname()."\n";
if( $BATCH_ID) {
	my($f)=get_gem_root_dir().'/conf/CheckServer.dat';
	if( -r $f ) {
	#	my($found)=0;
		open( F , $f ) or die "Cant open $f\n";
		while(<F>) {
			next if /^\s*#/;
			next if /^\s*$/;
			chomp;
			chomp;
			chomp;
			my($k,$v);
			if( ! /\:/ ) {	# default argument
				$v=$_;
				print "CheckServer.dat Overrides Found For All Jobs\n";
			} else {
				($k,$v)=split(/:/,$_,2);
				# print "CHECKING |$k|$BATCH_ID|\n";
				next if $k ne $BATCH_ID;
				print "CheckServer.dat Overrides Found For Job $BATCH_ID\n";
			}
			# $found++;
			$v=~s/^\s//;
			$v=~s/\s$//;
			$v=~s/\s\s+/ /;

			# print "[diag] $v\n";

			my(@pairs)= split(/\s/,$v);
			foreach ( @pairs ) {
				next if /^\s*$/;
				if( /\=/ ) {
					my($ky,$val) = split( /\=/, $_, 2);
					$ky =~ s/\s*\-+//;
					print "  Override $ky to $val\n";

					if ( $ky eq "BATCH_ID" ) 			{ $BATCH_ID = $val;
	   			} elsif( $ky eq "BATCHID" ) 		{ $BATCH_ID = $val ;
      			} elsif( $ky eq "BLOCKTIME") 		{ $BLOCKTIME = $val ;
      			} elsif( $ky eq "TIME"     ) 		{ $BLOCKTIME = $val ;
      			} elsif( $ky eq "DOALL"    ) 		{ $DOALL = $val ;
      			} elsif( $ky eq "FIRSTSERVER" )	{ $FIRSTSERVER = $val ;
      			} elsif( $ky eq "LASTSERVER" ) 	{ $LASTSERVER = $val ;
      			} elsif( $ky eq "HIGHCPU"  )	 	{ $HIGHCPU = $val ;
      			} elsif( $ky eq "HIGHIO"   ) 		{ $HIGHIO = $val ;
      			} elsif( $ky eq "LONGRUNNING" ) 	{ $LONGRUNNING = $val ;
      			} elsif( $ky eq "MAILTO"   ) 		{ $FATAL_MAILTO = $val ;
      			} elsif( $ky eq "OUTFILE"  ) 		{ $OUTFILE = $val ;
      			} elsif( $ky eq "PASSWORD" ) 		{ $PASSWORD = $val ;
      			} elsif( $ky eq "PERSIST_MAIL_DELAY"  ) { $PERSIST_MAIL_DELAY = $val ;
      			} elsif( $ky eq "RUNNABLE" ) 		{ $RUNNABLE = $val ;
      			} elsif( $ky eq "SERVER"   ) 		{ $SERVER = $val ;
      			} elsif( $ky eq "SYSTEM"   ) 		{ $SERVER = $val ;
      			} elsif( $ky eq "SLEEPTIME")	 	{ $SLEEPTIME = $val ;
      			} elsif( $ky eq "USER"     ) 		{ $USER = $val ;
      			} elsif( $ky eq "WARN_MAILTO" ) 	{ $WARN_MAILTO = $val ;
      			} elsif( $ky eq "ZOMBIE"   ) 		{ $ZOMBIE = $val ;
      			} else {
      				die "INVALID OPTION SPEC IN CONFIG FILE key=$ky val=$val\n";
      			}
				} else {
					print "  Override KEY $_\n";
					s/\s//g;
					s/\s*\-+//;
					$PERSIST = 1 	if $_ eq "PERSIST";
   				$NOLOCK=1 		if $_ eq "NOLOCK";
				   $NOMONITOR=1 	if $_ eq "NOMONITOR";
				   $NOPRODUCTION=1 if $_ eq "NOPRODUCTION";
				   $DEBUG=1 		if $_ eq "DEBUG";
				   $PRODUCTION=1 	if $_ eq "PRODUCTION";
				   $NOSA=1 			if $_ eq "NOSA";
   			}
			}
			# last;
		}
	#	print "FOUND=$found\n";
		close(F);
	} else {
		print "No File $f\n";
	}

} else {
#	$BATCH_ID = "CheckServer" unless $BATCH_ID;
}

if ( $NOMONITOR ) {
  $BLOCKTIME = 2147483647;
  $HIGHCPU = $HIGHIO = $LONGRUNNING = $RUNNABLE = 0;
}

die "FIRSTSERVER starts at 1 not 0"   if defined $FIRSTSERVER and $FIRSTSERVER==0;
print "$NAME Called at ".localtime(time)." in Debug Mode:\n"  if defined $DEBUG;
die( usage("Must pass --SERVER or --DOALL=\{sybase\|sqlsvr\}\n" )) 	  unless $SERVER or $DOALL;
die( usage("DOALL must be sqlsvr or sybase\n" ))	  if defined $DOALL and $DOALL ne "sqlsvr" and $DOALL ne "sybase";

# Check if the program can run.
my(%am_i_up_args)= (-threshtime => 120, -debug => $DEBUG, -prefix => $BATCH_ID );
if( ! $NOLOCK ) {
  print "NOLOCK IS NOT DEFINED!\n" if $DEBUG;
  if( Am_I_Up(%am_i_up_args) ) {
    my($fname,$secs,$thresh)=GetLockfile(%am_i_up_args);
    print "
Cant Start $NAME - it is apparently allready running
Use --NOLOCK to ignore this, --DEBUG to see details
File=$fname secs=$secs thresh=$thresh secs.
";
    exit(0);
  }
} else {
  debug( "Running with --NOLOCK switch!\n" );
}

unlink($OUTFILE) if defined $OUTFILE;

I_Am_Up(%am_i_up_args) unless $NOLOCK;
my $GemConfig	=	read_gemconfig( -debug=>$DEBUG, -nodie=>1, -READONLY=>1 );
$MAIL_TO	 		= $GemConfig->{ADMIN_EMAIL} 	unless $MAIL_TO;
$FATAL_MAILTO	= $GemConfig->{ADMIN_EMAIL} 	unless $FATAL_MAILTO;
$MAIL_FROM		= $GemConfig->{ALARM_MAIL} 	unless $MAIL_FROM;
$MAIL_HOST		= $GemConfig->{MAIL_HOST} 		unless $MAIL_HOST;

if ( defined $DEBUG ) {
  print "\nCheckServer.pl is Running with the following parameters
   BATCH_ID           <$BATCH_ID>
   BLOCKTIME          <$BLOCKTIME>
   DOALL              <$DOALL>
   FIRSTSERVER        <$FIRSTSERVER>
   LASTSERVER         <$LASTSERVER>
   HIGHCPU            <$HIGHCPU>
   HIGHIO             <$HIGHIO>
   LONGRUNNING        <$LONGRUNNING>
   MAILTO             <$FATAL_MAILTO>
   NOLOCK             <$NOLOCK>
   NOMONITOR          <$NOMONITOR>
   NOPRODUCTION       <$NOPRODUCTION>
   PRODUCTION         <$PRODUCTION>
   NOSA               <$NOSA>
   OUTFILE            <$OUTFILE>
   PASSWORD           <$PASSWORD>
   PERSIST            <$PERSIST>
   PERSIST_MAIL_DELAY <$PERSIST_MAIL_DELAY>
   RUNNABLE           <$RUNNABLE>
   SERVER             <$SERVER>
   SYSTEM             <$SERVER>
   SLEEPTIME          <$SLEEPTIME>
   USER               <$USER>
   WARN_MAILTO        <$WARN_MAILTO>
   ZOMBIE             <$ZOMBIE>\n";
}

# Create the list of servers to work on.
my(@server_list);
if( defined $DOALL or $PRODUCTION or $NOPRODUCTION ) {
  if( $DOALL ) {
    MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -AGENT_TYPE=>'Gem Monitor',
                     -SUBKEY=>$DOALL) if $BATCH_ID;
  } elsif( $PRODUCTION ) {
    MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -AGENT_TYPE=>'Gem Monitor',
                     -SUBKEY=>"PRODUCTION") if $BATCH_ID;
  } elsif( $NOPRODUCTION ) {
    MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -AGENT_TYPE=>'Gem Monitor',
                     -SUBKEY=>"NOPRODUCTION") if $BATCH_ID;
  }

  if( $PRODUCTION ) {
    push @server_list, get_password(-type=>$DOALL, PRODUCTION=>1, MONITORED=>1);
  } elsif( $NOPRODUCTION ) {
    push @server_list, get_password(-type=>$DOALL, NOPRODUCTION=>1, MONITORED=>1);
  } else {
    push @server_list, get_password(-type=>$DOALL, MONITORED=>1);
  }
} elsif( defined $SERVER ) {
 #  print "FIXME - MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, $SERVER )\n";
  MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -AGENT_TYPE=>'Gem Monitor',   -SUBKEY=>$SERVER) if $BATCH_ID;
  push @server_list, $SERVER;
}
@server_list = sort @server_list;
print "$NAME servers: " . join("\n",@server_list), "\n" if $DEBUG;

if( $FIRSTSERVER or $LASTSERVER ) {
  print "Filtering Server List Based On --FIRSTSERVER=$FIRSTSERVER / --LASTSERVER=$LASTSERVER\n";
  $FIRSTSERVER=1    unless $FIRSTSERVER;
  $LASTSERVER =1000 unless $LASTSERVER;
  @server_list =
    splice(@server_list, $FIRSTSERVER-1, $LASTSERVER-$FIRSTSERVER+1 );

  if ( @server_list < 0 ) {
    print "NO SERVERS TO WORK ON...\n";
    exit(0);
  }

  print "Working on servers: ", join(",",@server_list), "\n";
}

# Common code for Sybase and MSSQL error handlers.

sub l_handler {
  my ($message) = @_;
  my($msg, $title, $severity);
  if( $PERSIST and $persist_initialized{$current_server} ) {
	 # Secondary connection in Persist mode failed.
    $severity="CRITICAL";
    $title="$current_server: CONNECTION ERROR\n";
    $msg="The Persistent Monitor has found an error with $current_server
$message\nThe text of this message may not matter.";
    $PERSIST_ERROR_FOUND = 1;
  } elsif( $PERSIST ) {
    # Initial connection in Persist mode failed.
    statusmsg( "SKIPPED - unable to make initial connection\n($message)\n" );
    return;
  } else {
    # Initial connection in non -Persist mode.
    $severity="ERROR";
    $title="$current_server: ERROR $message\n";
    $msg="${NAME}[$current_server] Error: $message\n";
  }

	$MAIL_TO = ( $severity eq "WARNING" or $severity eq "ERROR" ) ?  $WARN_MAILTO : $FATAL_MAILTO;
	$MAIL_SUBJECT= "$severity $title";

  	$LAST_EMAIL_OFFSET{$current_server} = $PERSIST_MAIL_DELAY
   	 unless $LAST_EMAIL_OFFSET{$current_server};

  if( $PERSIST and
     (! $LAST_EMAIL_TIME{$current_server} or time > $LAST_EMAIL_TIME{$current_server} + $LAST_EMAIL_OFFSET{$current_server} )) {

    $LAST_EMAIL_OFFSET{$current_server} += 60;

	 print "[diag] MlpHeartbeat() at Line=",__LINE__,"\n";
    MlpHeartbeat(
      -state           => $severity,
      -subsystem       => $NAME,
      -system          => $current_server,
      -monitor_program => $BATCH_ID,
      -message_text    => "GEM_009: ".$msg);


    #if( MlpIsPageable($current_server, 0) == 0 ) {
    #  statusmsg("**** IGNORING AS SERVER IS ON THE IGNORE LIST ****\n");
    #} else {
    	if( ! $MAIL_TO ) {
    		statusmsg("**** $NAME Mail ****\n* TITLE=",$title,"* MESSAGE=$msg
********************\n");
    		print "[diag] send_mail_message($MAIL_TO) at Line=",__LINE__,"\n";
      	send_mail_message($MAIL_TO, $MAIL_FROM, $MAIL_HOST, $MAIL_SUBJECT, "<PRE>".$msg."</PRE>"." at line=".__LINE__);
      } else {
		    statusmsg("**** NOT SENDING MAIL - NO MAIL_TO - $NAME Mail ****\n* TITLE=",$title,"* MESSAGE=$msg
********************\n");
      }
    #}
    $LAST_EMAIL_TIME{$current_server}=time;
  }

  if( $PERSIST ) {
    statusmsg( "Connection Error $message\n" );
    statusmsg( "All connection errors are considered Fatal when --PERSIST is set\n" );
    $persist_initialized{$current_server} = undef;
  } else {
    statusmsg("Program Completed - Fatal Error $message\n");
    exit();
  }
}

# THE NEXT BLOCK REDEFINES THE HANDLERS!

sub l_sybase_handler {
  my ($error, $severity, $state, $line, $server, $proc, $message) = @_;

  print "l_sybase_handler(): #E=$error\nS=$severity\nState=$state
Line=$line\nSvr=$server\nProc=$proc\nMsg=$message\n" if $DEBUG;

  warn "REASON: LOGIN INCORRECT\n" if $error==4002;
  warn "REASON: SERVER NOT FOUND IN INTERFACES ($message)\n" if $error==20012;
  warn "REASON: SERVER DOWN\n" if $error==20009;

  $num_errors_found++;
  return &l_handler($message);
}

sub l_ms_error_handler {
  my ($state, $message, $num) = @_;

  $message =~ s/^(\[[\w\s]*\])+//;
  statusmsg( "l_ms_error_handler():\n\tError Num: $state Arg=$num\n\tError Txt: $message\n\tTarget Server=$current_server\n");

  $num_errors_found++;
  return &l_handler($message);
}

sub is_odbc_dsn {
	my($SERVER)=@_;
	return "FALSE" unless is_nt();
	foreach ( DBI->data_sources("ODBC")) {
		return "TRUE" if $_ =~ /:$SERVER$/;
	}
	return "FALSE";
}

testit(__LINE__);
sub testit {
	my($l)=@_;
	if( $SERVER eq "BOB" ) {
		# print "DBGDBG: SPECIAL DIAGNOSTICS\n";

		my $does_odbc_dsn_exist = is_odbc_dsn($SERVER);
		print "FIXME - does odbc exist == $does_odbc_dsn_exist \n";

		my($dbprocess) = DBI->connect("dbi:ODBC:$SERVER","native","native");
		if( $dbprocess ) {
			print "CONNECTED OK\n";
		} else {
			# print "DBGDBG THE ERR IS $DBI::errstr\n";
			print "*****\n* FAIL\n*****\n";
		}
			#print "DBGDBG THE RC IS $dbprocess\n";
			#{RaiseError=>0,PrintError=>$args{-PrintError},AutoCommit=>1,odbc_err_handler=>$error_handler});
		die "DONE\n";
	}
}

# Main monitoring loop starts here.
while(1) {
  print "MAIN LOOP CALLED AT ".localtime(time)."\n" if $PERSIST;
  $num_errors_found=0;

  my($server_id)=1;

  foreach $current_server (@server_list ) {

# This program must run at least 1ce per hour in Persistent mode.
# Pre -initialize hearbeat error message if by chance the program does
# not finish (hang).

# 300 minutes == 5 hours
#    MlpHeartbeat(
#      -state           => "ERROR",
#      -heartbeat       => 300,
#      -monitor_program => "HEARTBEAT",
#      -system          => $BATCH_ID,
#      -subsystem       => $current_server,
#      -message_text    => "Batch Job last run on ".hostname()." as $c")
#      if $BATCH_ID;


   $MAIL_TO=$WARN_MAILTO;
   $MAIL_SUBJECT="Checking $DBTYPE{$current_server} Server $current_server From ".hostname();

    statusmsg("*******************************************************************************\n");
    statusmsg("* $BATCH_ID Checking $DBTYPE{$current_server} $current_server \n");
    statusmsg("* Run Time ".localtime(time)."\n");
    statusmsg("* Monitoring From ".hostname()."\n");
    statusmsg("*******************************************************************************\n");

    $PERSIST_ERROR_FOUND=0;

# Initialize connection to current server.
    if(   ! $persist_initialized{$current_server}
       or ! $current_connection{$current_server} ) {
      print "[$current_server] Initializing Connection \n";

      if( defined $DOALL ) {
        ($USER, $PASSWORD) = get_password(-type=>$DOALL, -name=>$current_server);
      } else {
        ($USER, $PASSWORD) =  get_password(-type=>'sybase', -name=>$current_server)
            if ! defined $USER or ! defined $PASSWORD;
        ($USER, $PASSWORD) =  get_password(-type=>'sqlsvr', -name=>$current_server)
            if ! defined $USER or ! defined $PASSWORD;
      }

      if( ! $USER and ! $PASSWORD ) {
        my ($msg) = "Cant retrieve login/password for server $current_server. Check your configuration files. Has this server been disabled?";
        warn "ERROR: Cant retrieve login/password for server $current_server. Check your configuration files. Has this server been disabled?\n";
        print "[diag] MlpHeartbeat() at Line=",__LINE__,"\n";

        MlpHeartbeat(
          -state           => "CRITICAL",
          -debug           => $DEBUG,
          -subsystem       => $NAME,
          -system          => $current_server,
          -monitor_program => $BATCH_ID,
          -message_text    => "GEM_009: ERROR: $msg");

     	  $MAIL_SUBJECT=  "CRITICAL Server $SERVER: Monitor $NAME";

     	  my($ispageable)=MlpIsPageable($current_server,0);
     	  print "[diag] send_mail_message($FATAL_MAILTO) at Line=",__LINE__,"\n";
        send_mail_message($FATAL_MAILTO, $MAIL_FROM, $MAIL_HOST, $MAIL_SUBJECT, "<PRE>"."FATAL ERROR - $msg."."MlpIsPageable($current_server) = $ispageable</PRE>"." at line=".__LINE__);
        next;                    # Proceed with the next server in the list.
      }

		# Define correct error handler in Persistent mode. For non -Persistent
		# mode we do not know what server Sybase or MSSQL we are connecting to.
      my($error_handler);
      if( $PERSIST ) {
        if( $DOALL eq "sybase" ) {
          $error_handler = \&l_sybase_handler;
        } elsif( $DOALL eq "sqlsvr" ) {
          $error_handler = \&l_ms_error_handler;
        }
      }

		# Connect to the server. This will be done for both modes Persistent and
		# non -Persistent.
      $current_connection{$current_server} =
        dbi_connect(-srv=>$current_server, -login=>$USER,
                    -password=>$PASSWORD, -connection=>1,
                    -maxConnect=>$MAX_NUM_CONNECTIONS,
                    -errorhandler=>$error_handler);

      if( ! $current_connection{$current_server} ) {

      	$MAIL_TO=$FATAL_MAILTO;
   		$MAIL_SUBJECT= "CRITICAL $current_server: Connection Failed";

        statusmsg("[$current_server] Unable To Connect To Server... Trying Again in 20 Seconds\n");
        sleep(20);
        statusmsg("[$current_server] RE- Connecting to $current_server as $USER\n");

        $current_connection{$current_server} =
          dbi_connect(-srv=>$current_server, -login=>$USER, -password=>$PASSWORD, -connection=>1,
                      -maxConnect=>$MAX_NUM_CONNECTIONS);

        if( ! $current_connection{$current_server} ) {
          my($msg)="Cant connect to $current_server as $USER from ".hostname()." ".dbi_has_login_failed()."\n";
          print "[diag] MlpHeartbeat() at Line=",__LINE__,"\n";

          MlpHeartbeat(
            -state           => "CRITICAL",
            -debug           => $DEBUG,
            -subsystem       => $NAME,
            -system          => $current_server,
            -monitor_program => $BATCH_ID,
            -message_text    => "GEM_009: ".$msg);

          statusmsg("**** $NAME Mail ****
* TITLE=E.Can Not Connect To $current_server
* MESSAGE=FATAL ERROR - Can Not Connect To $current_server
********************\n");

          #if( MlpIsPageable($current_server, 0) == 0 ) {
          #  statusmsg("**** IGNORING AS SERVER IS ON THE IGNORE LIST ****\n");
          #} else {
        	   print "[diag] send_mail_message($MAIL_TO) at Line=",__LINE__,"\n";
            send_mail_message($MAIL_TO, $MAIL_FROM, $MAIL_HOST, $MAIL_SUBJECT, "<PRE>"."FATAL ERROR - Can Not Connect To $current_server"."</PRE>"." at line=".__LINE__);
          #}
          next;                 # Proceed with the next server on the list.
        }
      } else {
        debug("[$current_server] Connected\n");
      }

      debug("[$current_server] Checking Database Version...\n");
      ($DBTYPE{$current_server}, $DBVERSION{$current_server}) =
        dbi_db_version(-connection=>$current_connection{$current_server});
      statusmsg("[$current_server] DBTYPE=$DBTYPE{$current_server}
[$current_server] DBVERSION=$DBVERSION{$current_server}\n");

      if( $PERSIST ) {
        if( ! is_nt() and $DBTYPE{$current_server} eq "SYBASE" ) {
          statusmsg("[$current_server] Resetting Sybase Error Handler\n");
          $current_connection{$current_server}->{syb_err_handler}=
            \&l_sybase_handler;
        } else {
          statusmsg("[$current_server] Resetting ODBC Handler\n");
          $current_connection{$current_server}->{odbc_err_handler} =
            \&l_ms_error_handler;
        }
      }

      statusmsg("[$current_server] Connected \n");
      $persist_initialized{$current_server}=1;
    } else {
      statusmsg("[$current_server] Using Persistent Connection To $current_server\n");
    }

    print "[$current_server] Connection Ok\n" if $DEBUG;

	 # Connection to the server is Ok. Run the requested checks depending on the
	 # type of the server and its version.
    my(@zombie, @loghold, @processes, $query);

#
# HERE IS THE QUERY FOR SYBASE
#
    if( $DBTYPE{$current_server} eq "SYBASE") {

#
# SYBASE ZOMBIE CHECK
#
      $query = " -- Check for zombie processes.
 select l.spid,db = db_name(l.dbid),l.starttime,l.name
 from master..syslogshold l
 where l.spid != 0
   and not exists (select * from master..sysprocesses p where p.spid = l.spid)
   and     exists (select * from master..syslocks x where x.spid = l.spid)
   and datediff(minute,l.starttime,getdate()) >= $ZOMBIE
";

      $query .= "
union
select t.spid,db = db_name(t.masterdbid),t.starttime,t.xactname
from master..systransactions t
where not exists (select * from master..sysprocesses p where p.spid = t.spid)
   and     exists (select * from master..syslocks l where l.spid = t.spid)
   and datediff(minute,t.starttime,getdate()) >= $ZOMBIE
" unless $DBVERSION{$current_server} =~ /11.9/;

      statusmsg( "[$current_server] Checking Zombies.\n" );
      debug( "Running query:\n$query\n" );
      @zombie = dbi_query(-db=>"master", -query=>$query,
                          -connection=>$current_connection{$current_server});
      if ( $PERSIST_ERROR_FOUND ) {
        debug( " PERSIST error found -> restarting the loop\n" );
        next;
      }

#
# Check for longrunning processes.
#
      if ( $LONGRUNNING ) {
        $query = "
select p.spid,login_nm = suser_name(p.suid),db_nm = db_name(p.dbid)
      ,p.status,command = p.cmd,p.blocked,p.time_blocked,p.cpu,p.physical_io
      ,host = case when p.hostname is NULL or p.hostname = '' then NULL
                   else convert(varchar, p.hostname)
              end ";
        $query .= $DBVERSION{$current_server} =~ /11.9/ ? " " : "+': '+convert (varchar, p.ipaddr)";
        $query .= "
      ,host_pid = p.hostprocess
      ,program_name
      ,";
        $query .= $DBVERSION{$current_server} =~ /11.9/ ? "'(N.A.)'" : "loggedindatetime";
        $query .= "
      ,minutes = datediff(minute,l.starttime,getdate())
      ,db = db_name(l.dbid),l.starttime,l.name
  from master..syslogshold l, master..sysprocesses p
 where l.spid != 0
   and datediff(minute,l.starttime,getdate()) > $LONGRUNNING
   and datediff(dd,l.starttime,getdate()) < 10
   and l.spid not in (select spid from master..sysprocesses
                       where (cmd like 'UPDATE STAT%' or program_name like 'RepServer%'))
   and l.spid = p.spid
";

        statusmsg( "[$current_server] Checking Long Running Transactions.\n" );
        debug( "Running query:\n$query\n" );
        @loghold = dbi_query(-db=>"master", -query=>$query,
                             -connection=>$current_connection{$current_server});
        if( $PERSIST_ERROR_FOUND ) {
          debug( " PERSIST error found -> restarting the loop\n" );
          next;
        }
      }

#
# Now check bad processes.
#
      $query = "
select p.spid,login_nm = suser_name(p.suid)
      ,db_nm = db_name(p.dbid)
      ,p.status,command = p.cmd,p.blocked
      ,p.time_blocked,p.cpu,p.physical_io
      ,host = case when p.hostname is NULL or p.hostname = ''
                   then NULL
                   else convert(varchar, p.hostname)+': '
              end ";
      $query .= $DBVERSION{$current_server} =~ /11.9/ ? " " : "+': '+convert (varchar, p.ipaddr)";
      $query .= "
      ,host_pid = p.hostprocess
      ,program_name
      ,";
      $query .= $DBVERSION{$current_server} =~ /11.9/ ? "'(N.A.)'" : "loggedindatetime";
      $query .= "
      ,minutes = -1
      ,db = db_name(p.dbid)
      ,'12/31/2000'
      ,name = ''
  from master..sysprocesses p
 where lower(p.cmd) in ('infected','bad status','log suspend','stopped')
    or lower(p.status) in ('infected','bad status','log suspend','stopped')";

      $query .= "
    or time_blocked > $BLOCKTIME
    or exists (select * from master..sysprocesses p2
                where p2.time_blocked >= $BLOCKTIME
                  and p2.blocked = p.spid)"
      unless $NOMONITOR;

      $query .= "
    or (    p.cpu >= $HIGHCPU
        and p.program_name != 'RepServer'
        and lower(p.cmd) not in
              ('rep agent', 'checkpoint sleep', 'hk wash', 'hk gc', 'hk chores', 'audit process')
        and exists (select * from master..syslocks l1 where l1.spid = p.spid))"
      if $HIGHCPU;

      $query .= "
    or (    p.physical_io >= $HIGHIO
        and p.program_name != 'RepServer'
        and lower(p.cmd) not in
              ('rep agent', 'checkpoint sleep', 'hk wash', 'hk gc', 'hk chores', 'audit process')
        and exists (select * from master..syslocks l2 where l2.spid = p.spid))"
      if $HIGHIO;

      if ($RUNNABLE) {
        $query .= "
 union
select p.spid
      ,login_nm = suser_name(p.suid)
      ,db_nm = db_name(p.dbid)
      ,p.status,command = p.cmd,p.blocked
      ,p.time_blocked,p.cpu,p.physical_io
      ,host = case when p.hostname is NULL or p.hostname = ''
                   then NULL
                   else convert(varchar, p.hostname)+': '
              end
             +";
      $query .= $DBVERSION{$current_server} =~ /11.9/ ?
              "'(N.A.)'" : "convert (varchar, p.ipaddr)";
      $query .= "
      ,host_pid = p.hostprocess
      ,program_name
      ,";
      $query .= $DBVERSION{$current_server} =~ /11.9/ ?
              "'(N.A.)'" : "loggedindatetime";
      $query .= "
      ,minutes = -1
      ,db = db_name(p.dbid)
      ,'12/31/2000'
      ,name = ''
  from master..sysprocesses p
 where (select count(*) from master..sysprocesses where status = 'runnable') >=
       (select ceiling(count(*)*".$RUNNABLE .") from master..sysengines)";
      }
    } else {
#
# MSSQL server.
#
      $query = "
select p.spid
      ,login_nm = isnull(suser_sname(p.sid), isnull(p.nt_username, '('+'N.A.'+')'))
      ,db_nm = db_name(p.dbid)
      ,p.status
      ,p.cmd
      ,p.blocked
      ,time_blocked = p.waittime/1000
      ,cpu
      ,physical_io
      ,hostname
      ,hostprocess
      ,program_name
      ,login_time
      ,minutes = -1
      ,db = db_name(p.dbid)
      ,'12/31/2000'
      ,name = ''
  from master..sysprocesses p
 where p.cmd != 'BACKUP LOG'
   and (   ((p.waittime/1000) >= $BLOCKTIME and p.blocked != p.spid and p.blocked != 0)
        or exists (select * from master..sysprocesses p2
                    where (p2.waittime/1000) >= $BLOCKTIME
                      and p2.blocked = p.spid and p2.blocked != p2.spid)
        or lower(p.status) not in
          ('background','suspend','suspended','runnable','running',
           'dormant', 'sleeping','stopped','defwakeup'))";

      $query .= "
 union
select p.spid
      ,login_nm = isnull(suser_sname(p.sid), isnull(p.nt_username, '('+'N.A.'+')'))
      ,db_nm = db_name(p.dbid)
      ,p.status
      ,p.cmd
      ,p.blocked
      ,time_blocked = p.waittime/1000
      ,cpu
      ,physical_io
      ,hostname
      ,hostprocess
      ,program_name
      ,login_time
      ,minutes = -1
      ,db = db_name(p.dbid)
      ,'12/31/2000'
      ,name = ''
  from master..sysprocesses p
 where p.cmd!='BACKUP LOG'
   and p.status = 'runnable'
   and (select count(*) from master..sysprocesses where status = 'runnable') >=
       (select ceiling(cpu_count*".$RUNNABLE.") from sys.dm_os_sys_info)"
      if $RUNNABLE;
    }

    $query .= "\n order by p.blocked, p.spid ";

    statusmsg( "[$current_server] Checking Server Processes.\n" );
    debug( "Running query:\n$query\n" );

    @processes = dbi_query(-db=>"master", -query=>$query,
                           -connection=>$current_connection{$current_server});
    if( $PERSIST_ERROR_FOUND ) {
      debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
      next;
    }

	 # Now all queries were executed. Process results.
    if($#zombie < 0 and $#loghold < 0 and $#processes < 0) {
		# There are no processes that require attention. Return with the good news.
		# For Future --> Check if there in unread information in
		# sybsytemprocsa..gem_threshold_hist4.
      $LAST_EMAIL_OFFSET{$current_server} = $PERSIST_MAIL_DELAY;
      statusmsg( "[$current_server] Server Looks Ok.\n" );
		print "[diag] MlpHeartbeat() at Line=",__LINE__,"\n";

      MlpHeartbeat(
         -state           => "OK",
         -system          => $current_server,
         -subsystem       => $NAME,
         -monitor_program => $BATCH_ID,
         -message_text    => "GEM_009: Server Checked Ok...");

      unless ( $PERSIST ) {
        MlpBatchJobEnd();
        dbi_disconnect(-connection=>$current_connection{$current_server});
      }
      next;
    }

	 # There are some processes that require attention.
    my(@str);
    my(%blocked_by,%blocked_count,%process_info,%process_detail,%process_data);
    my($num_zombies)=0;
    my($num_longrunning)=0;
    my($num_blocked)=0;
    my($num_suspend)=0;
    my($num_infected)=0;
    my($num_stopped)=0;
    my($num_badstatus)=0;
    my($num_runnable)=0;
    my($num_highcpu)=0;
    my($num_highio)=0;
    my($longest_block)=0;
    my($subject)="";
    my($header_str)="";
    my($severity)="WARNING";
    my($email_flag)=0;
    my($num_cdeadlock)=0;
    my(@blocked_spid, @blocker_spid, @c_info);

	 # Calculate number of ZOMBIES.
    foreach( @zombie ) {
      $severity="CRITICAL";
      $num_zombies++;
      $email_flag=1;
    }

# Populate @process_info and @process_data information.
# Initialize @process_detail.
    foreach( @loghold, @processes ) {
      #my @dat=dbi_decode_row($_);
      my ($vspid,$vloginnm,$vdbnm,$vstatus,$vcommand,$vblocked,$vtblocked,$vcpu,$vpio,
      	$vhost,$vhpid,$vprognm,$vlogdt,$vmin,$vdb,$vdt,$vnm)=dbi_decode_row($_);
      $vstatus =~ s/\s*$//g;
      $vhpid =~ s/\s+$//g;
      $vhpid = "(n.a.)" if $vdb eq "";
      $vprognm =~ s/\s+$//g;
      $vprognm = "(n.a.)" if $vdt eq "";

      my($cmd)=lc($vcommand);

# Populate @process_info for all processes.
      $process_info{$vspid}="Process: SPID $vspid($vloginnm) [$vstatus]; Database $vdb; Host: $vhost; PID: $vhpid; Program: $vprognm:";

# Populate @process_data for all processes.
      $process_data{$vspid}={};
      $process_data{$vspid}->{spid}=$vspid;
      $process_data{$vspid}->{login}=$vloginnm;
      $process_data{$vspid}->{command}=$vcommand;
      $process_data{$vspid}->{cpu_used}=$vcpu;
      $process_data{$vspid}->{io_used}=$vpio;
      $process_data{$vspid}->{application_name}=$vprognm;
      $process_data{$vspid}->{database_name}=$vdb;
      $process_data{$vspid}->{query_start_time}=$vdt;
      $process_data{$vspid}->{query}="";
      $process_detail{$vspid} = "";

# Mandatory checks (CRITICAL or ERROR).
      if ( $vstatus =~ /infected/i or $vcommand =~ /infected/i ) {
        $severity="CRITICAL";
        $num_infected++;
        $email_flag=1;
        $process_detail{$vspid} .= "   is INFECTED: $vstatus, $vcommand\n";
        debug ("infected: $vspid --> $vstatus, $vcommand\n");
      } elsif ( $vstatus =~ /bad status/i or $vcommand =~ /bad status/i ) {
        $severity="CRITICAL";
        $num_badstatus++;
        $email_flag=1;
        $process_detail{$vspid} .= "   has BAD STATUS: $vstatus, $vcommand\n";
        debug ("bad status: $vspid --> $vstatus, $vcommand\n");
      } elsif ( $vstatus =~ /suspend/i or $vcommand =~ /suspend/i ) {
        if ( $DBTYPE{$current_server} eq "SYBASE" ) {
          $severity="CRITICAL";
          $num_suspend++;
          $email_flag=1;
          $process_detail{$vspid} .=
            "   is in LOG SUSPEND state: $vstatus, $vcommand\n";
          debug ("suspended: $vspid --> $vstatus, $vcommand\n");
        }
      } elsif ( $vstatus =~ /stopped/i or $vcommand =~ /stopped/i ) {
        $severity="ERROR" unless $severity eq "CRITICAL";
        $num_stopped++;
        $email_flag=1;
        $process_detail{$vspid} .= "   is STOPPED: $vstatus, $vcommand\n";
        debug ("stopped: $vspid --> $vstatus, $vcommand\n");
      }

      $severity="WARNING" unless $severity eq "CRITICAL" or $severity eq "ERROR";

# Process blocks (WARNING).
      if ($vblocked > 0 or $vtblocked > 0) {
        $num_blocked++ if $vtblocked > 0;
        $longest_block = $vtblocked if $vtblocked > $longest_block;
        $blocked_by{$vspid} = $vblocked if $vtblocked > 0;
        if (defined $blocked_count{$vblocked}) {
          $blocked_count{$vblocked}++;
        } else {
          $blocked_count{$vblocked}=1;
        }

# If the following appears on the output --> internal error.
        $process_info{$vblocked} = "Process: SPID $vblocked [undefined]"
          unless defined $process_info{$vblocked};

        my ($tstr)=to_timestr($vtblocked);
        $process_detail{$vblocked} .= "   BLOCKING Process SPID $vspid for $tstr\n";
        $process_detail{$vspid} .= "   IS BLOCKED BY Process SPID $vblocked for $tstr\n";
        push @blocked_spid, $vspid;
        push @blocker_spid, $vblocked;

        $severity="ERROR" if $severity eq "WARNING" and $longest_block > $BLOCKTIME;
      }

# Optional checks (WARNING).
      if ($RUNNABLE) {
        $num_runnable++ if $vstatus =~ /runnable/i;
      }

      if ($LONGRUNNING and $vmin >= $LONGRUNNING) {
        $num_longrunning++;
        $email_flag=1 unless $vloginnm eq 'sa' and
          ($cmd =~ "dump" or $cmd =~ "load" or $cmd =~ "dbcc" or
           $cmd =~ "update.+st");
        $process_detail{$vspid}.= "   RUNNING for $vmin min.: Started $vdt; Transaction $vnm\n";
        debug("long running: $vspid --> $vmin\n" );
      }

      if ( $HIGHCPU and $vcpu >= $HIGHCPU ) {
        $num_highcpu++;
        $email_flag=1 unless $vloginnm eq 'sa' and
          ($cmd =~ "dump" or $cmd =~ "load" or $cmd =~ "dbcc" or
           $cmd =~ "update.+st");
        $process_detail{$vspid}.= "   has HIGH CPU: $vcpu\n";
        debug("high CPU: $vspid --> $vcpu\n");
      }

      if ( $HIGHIO and $vpio >= $HIGHIO ) {
        $num_highio++;
        $email_flag=1 unless $vloginnm eq 'sa' and
          ($cmd =~ "dump" or $cmd =~ "load" or $cmd =~ "dbcc" or
           $cmd =~ "update.+st");
        $process_detail{$vspid}.= "   has HIGH IO: $vpio\n";
        debug("high IO: $vspid --> $vpio\n");
      }
    }

# Determine if there are circular deadlocks.
    my($blocked);
    my($blocker);
    my($c_string);
    for (my $i=0; $i<=$#blocked_spid; $i++) {
      $blocked=$blocked_spid[$i];
      $blocker=$blocker_spid[$i];
      $c_string="SPID $blocker blocks SPID $blocked";
      for (my $j=$i+1; $j<=$#blocked_spid; $j++) {
        if ( $blocked_spid[$j] == $blocker ) {
          $blocker=$blocker_spid[$j];
          $c_string="SPID $blocker BLOCKS ".$c_string;
          if ( $blocker == $blocked ) {
            $severity="ERROR" unless $severity eq "CRITICAL";
            $num_cdeadlock++;
            push @c_info, $c_string."   ";
            last;
          }
        }
      }
    }

    if( $NOMONITOR ) {
      if( $num_suspend > 0 or $num_infected > 0 or $num_stopped > 0 or
          $num_badstatus >0 or $num_zombies > 0 or $num_longrunning > 0 ) {

        $header_str = "GEM_009: $current_server:\n";
        $header_str.="     $num_zombies Zombie Processes\n"          	if $num_zombies>0;
        $header_str.="     $num_cdeadlock Circular Deadlocs\n"          if $num_cdeadlock>0;
        $header_str.="     $num_suspend Processes Suspended\n"          if $num_suspend>0;
        $header_str.="     $num_infected Processes Infected\n"          if $num_infected>0;
        $header_str.="     $num_stopped Processes Stopped\n"          	if $num_stopped>0;
        $header_str.="     $num_badstatus Processes with Bad Status\n"  if $num_badstatus>0;
        $header_str.="     $num_longrunning Long Running Processes\n"   if $num_longrunning>0;
        $header_str.="     $num_blocked Processes Blocked\n"          	if $num_blocked>0;
        $header_str.="     $num_highcpu Processes with High CPU\n"      if $num_highcpu>0;
        $header_str.="     $num_highio Processes with High IO\n"        if $num_highio>0;

        statusmsg($header_str);

        MlpEvent( -severity        => $severity,
                  -system          => $current_server,
                  -monitor_program => $BATCH_ID,
                  -subsystem       => $NAME,
                  -message_text    => $header_str);

        if( $PERSIST ) {
          if( ! $LAST_EMAIL_TIME{$current_server}
          or  time > $LAST_EMAIL_TIME{$current_server} + $PERSIST_MAIL_DELAY ) {

            #if( MlpIsPageable($current_server,0) == 0 ) {
            #  statusmsg("**** IGNORING AS SERVER IS ON THE IGNORE LIST ****\n");
            #} else {
            #  statusmsg( "SENDING MAIL: $header_str\n" );
              print "[diag] send_mail_message($FATAL_MAILTO) at Line=",__LINE__,"\n";
              send_mail_message($FATAL_MAILTO, $MAIL_FROM, $MAIL_HOST, $header_str, "<PRE>".$header_str." ".__LINE__."</PRE>"." at line=".__LINE__);
            #}

            $LAST_EMAIL_TIME{$current_server}=time;
          }
        }
      }
      next;
    }

    $subject = "$severity Server $current_server:";
    push @str, "========== $current_server Summary Information ==========";
    push @str, "     ";
    if ( $num_zombies > 0 ) {
      push @str, "Zombie Process(es) found: $num_zombies";
      $subject.= " Zombie,";
      $header_str="$num_zombies ZOMBIE process(es)\n";
    }
    if ( $num_cdeadlock > 0 ) {
      push @str, @c_info;
      $subject.= "Circular Deadlocks";
      $header_str.="$num_cdeadlock Circular Deadlock(s)\n";
    }
    if ( $num_infected > 0 ) {
      push @str, "Infected Process(es) found: $num_infected";
      $subject.= " Infected,";
      $header_str.="$num_infected Infected process(es)\n";
    }
    if ( $num_suspend > 0 ) {
      push @str, "Suspended Process(es) found: $num_suspend";
      $subject.= " Suspended,";
      $header_str.="$num_suspend Suspended process(es)\n";
    }
    if ( $num_badstatus > 0 ) {
      push @str, "Process(es) with Bad Status found: $num_badstatus";
      $subject.= " Bad Status,";
      $header_str.="$num_badstatus Bad Status process(es)\n";
    }
    if ( $num_stopped > 0 ) {
      push @str, "Stopped Process(es) found: $num_stopped";
      $subject.= " Stopped,";
      $header_str.="$num_stopped Stopped process(es)\n";
    }
    if ( $RUNNABLE and $num_runnable >= $RUNNABLE ) {
      push @str, "Runnable Process(es) found: $num_runnable";
      $subject.= " Runnable,";
      $header_str.="$num_runnable Runnable process(es)\n";
    }

    if ( $num_blocked > 0 ) {
      push @str, "Blocked Process(es) found: $num_blocked";
      push @str, "Longest Block Time: ".to_timestr($longest_block)."   ";
      $subject.= " Blocked,";
      $header_str.="$num_blocked Blocked process(es)\n";
      $header_str.="Block Time: ".to_timestr($longest_block)."\n";
      foreach ( keys %blocked_count ) {
        $header_str.="Spid $_ Blocking $blocked_count{$_} jobs\n";
      }
    }
    if ( $num_longrunning > 0 ) {
      push @str, "Long Running Process(es) found: $num_longrunning";
      $subject.= " Long Running,";
      $header_str.="$num_longrunning Long Running process(es)\n";
    }
    if ( $num_highcpu > 0 ) {
      push @str, "Process(es) with high CPU found: $num_highcpu";
      $subject.= " High CPU,";
      $header_str.="$num_highcpu High CPU process(es)\n";
    }
    if ( $num_highio > 0 ) {
      push @str, "Process(es) with high I/O found: $num_highio";
      $subject.= " High I/O,";
      $header_str.="$num_highio High I/O process(es)\n";
    }

    push @str, "     ";
    push @str, "========== Process Summary Information ==========";
    push @str, "     ";
    $subject =~ s/\,$//;
    $subject.= " Processes";

# Process zombies one more time because zombies are NOT in %process_info.
    foreach( @zombie ) {
      my @dat=dbi_decode_row($_);
      debug( "FOUND ZOMBIE $dat[3]\n" );
      $dat[0] = "(n.a.)" if $dat[0] =~ /^\s*$/;
      push @str, "ZOMBIE Process: SPID $dat[0]; Database $dat[1]:   ";
      push @str, "   Started $dat[2]; Transaction $dat[3]   ";
    }

    push @str, "CIRCULAR DEADLOCK chain(s):   ", @c_info
      if $num_cdeadlock>0;

# Process process information.
    foreach my $key (keys %process_info) {
      push @str, $process_info{$key};
      push @str, split("\n", $process_detail{$key});
    }

    push @str, "     ";
    push @str, "========== Process Detailed Information ==========";
    push @str, "     ";

    if( $DBTYPE{$current_server} ne "SQL SERVER" ) {

# Set 3604
      $query = "dbcc traceon(3604)";
      debug( "running: $query\n" );
      dbi_query(-db=>"master",-query=>$query, -no_results=>1,
                -connection=>$current_connection{$current_server});
      if( $PERSIST_ERROR_FOUND ) {
        debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
        next;
      }

# Get information for every process.
      debug("Getting sqltext for each process\n");
      foreach my $key ( keys %process_info ) {
        push @str, "---".$process_info{$key}."---   ";

# Getting SQL text for each process.
        my $query = "dbcc sqltext($key)";
        debug( "running: $query\n" );
        my($dbcc)="";
        foreach( dbi_query(-db=>"master",-query=>$query,-no_results=>1,
                           -connection=>$current_connection{$current_server})) {
          if( $PERSIST_ERROR_FOUND ) {
            debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
            last;
          }
          my($dat)=join(" ",dbi_decode_row($_));
          $dat =~ s/^\s+//;
          $dat =~ s/\s+$//;
          next if $dat=~/DBCC execution completed/;
          $dat =~ s/^SQL Text: //g;
          $dbcc.="$dat\n" unless $dat =~ /^\s*$/;
          $process_data{$key}->{query}.=$dat if defined $process_data{$key};
        }

# Reformat SQL text into 70 bytes chunks.
        $dbcc =~ s/Executing procedure:/\nExecuting procedure:\n/;
        $dbcc =~ s/Subordinate SQL Text:/\nSubordinate SQL Text:\n/;

        push @str, "SQL Text:";
        foreach ( split("\n", $dbcc) ) {
          my($len)=length($_);
          my($pos)=0;
          while ($pos <= $len) {
            my($prefix)="     ";
            $prefix = "  " if $_ =~ /^Subordinate SQL Text/ or
                              $_ =~ /^Executing procedure/;
            push @str, $prefix.substr($_, $pos, 70)."   ";
            $pos+=70;
          }
        }

# Get blocking information for every process.
        $query = "exec sp__lock \@spid=$key, \@dont_format='Y'";
        debug( "running: $query\n" );
        my(@x)=dbi_query(-db=>"master",-query=>$query,-print_hdr=>1,
                         -connection=>$current_connection{$current_server});

        if( $PERSIST_ERROR_FOUND ) {
          debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
          last;
        }

        push @str, "   ";
        push @str, "Lock information:";
        push @str, dbi_reformat_results(@x);
        push @str, "   ";
      }

# Save process detailed information in the database.
      foreach my $key ( keys %process_data ) {
        print "[diag] MlpLongRunningQuery() at Line=",__LINE__,"\n";
        MlpLongRunningQuery(
          -server_name      => $current_server,
          -server_type      => $DBTYPE{$current_server},
          -query_start_time => $process_data{$key}->{query_start_time},
          -spid             => $process_data{$key}->{spid},
          -database_name    => $process_data{$key}->{database_name},
          -login            => $process_data{$key}->{login},
          -query            => $process_data{$key}->{query},
          -io_used          => $process_data{$key}->{io_used},
          -cpu_used         => $process_data{$key}->{cpu_used},
          -client_hostname  => $process_data{$key}->{client_hostname},
          -application_name => $process_data{$key}->{application_name},
        );
      }
    }
    else {

# Set 3604
      $query = "dbcc traceon(3604)";
      debug( "running: $query\n" );
      dbi_query(-db=>"master",-query=>$query, -no_results=>1,
                -connection=>$current_connection{$current_server});
      if( $PERSIST_ERROR_FOUND ) {
        debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
        next;
      }

# Get information for every process.
      debug("Getting sqltext for each process\n");
      foreach my $key ( keys %process_info ) {
        push @str, "---".$process_info{$key}."---   ";

# Getting SQL text for each process.
        my $query = "dbcc inputbuffer($key)";
        debug( "running: $query\n" );
        my($dbcc)="";
        foreach( dbi_query(-db=>"master",-query=>$query,-no_results=>1,
                           -connection=>$current_connection{$current_server})) {
          if( $PERSIST_ERROR_FOUND ) {
            debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
            last;
          }
          my($dat)=join(" ",dbi_decode_row($_));
          $dat =~ s/^\s+//;
          $dat =~ s/\s+$//;
          next if $dat=~/DBCC execution completed/;
          $dat =~ s/^SQL Text: //g;
          $dbcc.="$dat\n" unless $dat =~ /^\s*$/;

# Save process detailed information.
          $process_data{$key}->{query}.=$dat if defined $process_data{$key};
        }

# Reformat SQL text into 70 bytes chunks.
        $dbcc =~ s/Executing procedure:/\nExecuting procedure:\n/;
        $dbcc =~ s/Subordinate SQL Text:/\nSubordinate SQL Text:\n/;

        push @str, "SQL Text:";
        foreach ( split("\n", $dbcc) ) {
          my($len)=length($_);
          my($pos)=0;
          while ($pos <= $len) {
            my($prefix)="     ";
            $prefix = "  " if $_ =~ /^Subordinate SQL Text/ or
                              $_ =~ /^Executing procedure/;
            push @str, $prefix.substr($_, $pos, 70)."   ";
            $pos+=70;
          }
        }

			# Get blocking information for every process.
			$query = "exec sp__lock \@spid=$key, \@dont_format='Y'";
			debug( "running: $query\n" );
			my(@x)=dbi_query(-db=>"master",-query=>$query,-print_hdr=>1,
			                -connection=>$current_connection{$current_server});

			if( $PERSIST_ERROR_FOUND ) {
				debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
				last;
			}

			push @str, "   ";
			push @str, "Lock information:";
			push @str, dbi_reformat_results(@x);
			push @str, "   ";
      }

		# Save process detailed information in the database.
      foreach my $key ( keys %process_data ) {
        print "[diag] MlpLongRunningQuery() at Line=",__LINE__,"\n";
        MlpLongRunningQuery(
          -server_name      => $current_server,
          -server_type      => $DBTYPE{$current_server},
          -query_start_time => $process_data{$key}->{query_start_time},
          -spid             => $process_data{$key}->{spid},
          -database_name    => $process_data{$key}->{database_name},
          -login            => $process_data{$key}->{login},
          -query            => $process_data{$key}->{query},
          -io_used          => $process_data{$key}->{io_used},
          -cpu_used         => $process_data{$key}->{cpu_used},
          -client_hostname  => $process_data{$key}->{client_hostname},
          -application_name => $process_data{$key}->{application_name},
        );
      }
    }

    if( $PERSIST_ERROR_FOUND ) {
      debug("PERSIST ERROR FOUND - RESTARTING LOOP\n");
      next;
    }

    push @str, "==================================================";
    push @str, "     ";

    my($body_str)="";
    my($found)="FALSE";
    foreach ( @str ) {
      chomp; chomp;
      if( /^Blocker spid=/ ) {
        s/^Blocker spid=/Spid=/;
        $header_str.=$_."\n";
        next;
      }
      $body_str.=$_."\n";
    }

	 print "[diag] MlpHeartbeat() at Line=",__LINE__,"\n";
    statusmsg( "[$current_server] Writing $severity to Alarm Database: text=$header_str\n" );
    statusmsg( "[$current_server] MlpHeartbeat( -state=>$severity, -debug=>$DEBUG, -system=>$current_server, -subsystem=>Blocks, -monitor_program=>$BATCH_ID, -message_text=>GEM_009: $header_str) \n");
    MlpHeartbeat( -state           => $severity,
                  -debug           => $DEBUG,
                  -system          => $current_server,
                  -subsystem       => $NAME,
                  -monitor_program => $BATCH_ID,
                  -message_text    => "GEM-009: ".$header_str);
    if( defined $OUTFILE ) {
      open(OUT,">> $OUTFILE") or logdie("Cant Write to $OUTFILE: $!");
      print OUT $header_str."\n".$body_str;
      close OUT;
    }

# Check if email is necessary.
    my($email_flag)=1;
    if ( $num_zombies == 0 	and $num_cdeadlock == 0 and $num_suspend == 0 and
         $num_infected == 0 	and $num_stopped == 0 	and $num_badstatus == 0 and $num_blocked == 0 ) {

		# There are only questionable processes.
      $email_flag=0;
      foreach my $key ( keys %process_data ) {
        my($cmd)=lc($process_data{$key}->{command});
        next if $process_data{$key}->{login} eq 'sa' and
               ( $cmd =~ "dump" or $cmd =~ "load" or $cmd =~ "dbcc" or
                 $cmd =~ "update.+st" );

# The email is necessary.
        $email_flag=1;
        last;
      }
    }

    if ( $email_flag == 1 ) {
    	my($send_to);
    	if( $severity eq "WARNING" or $severity eq "ERROR" ) {
    		$send_to = $WARN_MAILTO;
    		print "[diag] NOT sending mail as no WARN_MAILTO\n" unless $WARN_MAILTO;
      } else {
    		$send_to = $FATAL_MAILTO;
    		warn "[diag] NOT sending mail as no FATAL_MAILTO\n" unless $FATAL_MAILTO;
    	}
    	# ($send_to) = ( $severity eq "WARNING" or $severity eq "ERROR" ) ?  $WARN_MAILTO : $FATAL_MAILTO;
      #my ($send_to) = $severity eq "WARNING" ?  $WARN_MAILTO :
      #  ($FATAL_MAILTO eq "" ? $WARN_MAILTO : $FATAL_MAILTO.",".$WARN_MAILTO);

		if( $send_to ) {
			my($ispageable)=MlpIsPageable($current_server,0);
			print "[diag] send_mail_message($severity) to $send_to at Line=",__LINE__,"\n";
			send_mail_message($send_to, $MAIL_FROM, $MAIL_HOST, $subject, "<PRE>".$body_str." MlpIsPageable($current_server) = $ispageable</PRE>"." at line=".__LINE__);
		}
    }
    dbi_disconnect(-connection=>$current_connection{$current_server})  unless $PERSIST;
  }

  last unless $PERSIST;

  statusmsg( "*******************************************************************************\n");
  statusmsg( "* SLEEPING $SLEEPTIME SECONDS as --PERSIST FLAG WAS SET\n" );
  statusmsg( "*******************************************************************************\n");
  sleep($SLEEPTIME);
}

MlpBatchJobEnd();

statusmsg( "Program completed successfully.\n" );
dbi_disconnect(-connection=>$current_connection{$current_server});
exit(0);

sub debug {  print @_ if defined $DEBUG; }
sub statusmsg {  print @_; }

sub to_timestr {
  my($secs)=@_;
  my $hrs= int($secs/3600);
  $secs -= 3600*$hrs;
  $hrs .= " hour " if $hrs==1;
  $hrs .= " hours " if $hrs>1;
  $hrs = "" if $hrs < 1;

  my $min= int($secs/60);
  $secs -= 60*$min;

  my($outstr)="$hrs $min Minutes $secs Secs";
  $outstr=~s/\s+/ /g;
  return $outstr;
}

__END__

=head1 NAME

CheckServer.pl - Utility to check database servers

=head2 SYNOPSIS

=over 2

This flexible program checks Sybase and MSSQL servers.

Things that are always monitored:

  Server Availablility
  "Zombie" processes (Sybase only)
  Blocked processes
  Processes that are in bad state ("infected", "log suspend", "bad", etc.)

These conditions are considered CRITICAL or ERROR. email alerts will be sent to both --MAILTO and --WARN_MAILTO recepients.

Things that are monitored based on command line arguments:

  Long running transactions
  Processes with high CPU or I/O
  High number of runnable processes
  Blocked processes (and circular deadlocks)

These conditions are considered WARNINGs. eMails will be sent to --WARN_MAILTO recepients only.

=back

=head2 USAGE

=over 2

The program can operate in two modes:

=item Persistent mode

Persistent mode simply connects to all your servers and loops through them running a small, quick, query.
The program then sleeps --SLEEPTIME seconds and continues.  This will test whether your connection is
broken, and if it is, will alert.  To allow configuration via older perl modules that have number of
connection limits, you can use command line arguments to cycle through your registered servers.  For example,
--FIRSTSERVER=21 --LASTSERVER=40 will loop through servers 21-40 within your password file.

CheckServer.pl -BATCH_ID=id --PERSIST [--SLEEPTIME=120] [--FIRSTSERVER=n --LASTSERVER=m] [--[NON]PRODUCTION] [--PERSIST_MAIL_DELAY=300] [--DOALL] -OUTFILE=Outfile --MAILTO=a,b,c --WARN_MAILTOTO=x,y,z ...

=item Regular mode

Regular mode (No --PERSIST) will attempt to connect to your server, check it, and alert if it detects a problem.

CheckServer.pl -BATCH_ID=id -USER=SA_USER -SERVER=SERVER_NAME -PASSWORD=SA_PASS -OUTFILE=Outfile --MAILTO=a,b,c --WARN_MAILTO=x,y,z ...

=back

=head2 COMMAND LINE ARGUMENTS - STANDARD ARGUMENTS

=over 2

=item --DEBUG

Instructs program to run with informational printout

=item --BATCH_ID=batch_ID

Unique identifie of this particular program - used to track alerts and runtimes

=item --NOLOCK

To avoid "spam", the program is not allowed to run against the same
server until "threshold timeout" (120 sec) between the runs expires.
--NOLOCK switch overrides this restriction

=item --OUTFILE=log_file_name

Specifies log file name

=back

=head2 COMMAND LINE ARGUMENTS - SERVER SELECTION

The following arguments define which servers the program will run on:

=over 2

=item --DOALL={sybase|sqlsvr}

Request to monitor either:
--DOALL=sybase - Sybase database servers only
--DOALL=sqlsvr - MSSQL database servers only

=item --FIRSTSERVER=number --LASTSERVER=number

Request to monitor only servers in range (x - y)

=item --[NON]PRODUCTION

Request to monitor only production (--PRODUCTION), or non -production (--NOPRODUCTION) servers

=back

-or-

=over 2

=item -PASSWORD=......

Password for Server login ID

=item -SERVER=server_name

Name of the server to monitor

=item -USER=login_ID

Login ID (with sa and sso roles) to be used to
login to server

=back

=head2 COMMAND LINE ARGUMENTS - BEHAVIOR

=over 2

The following command line arguments can be specified:

=item --BLOCKTIME=30

Specifies blocked process time threshold (sec). Default is 30 sec.

=item --HIGHCPU=5000

Specifies high CPU utilization threshold.
By default high CPU utilization is not monitored (--HIGHCPU=0)

=item --HIGHIO=50000

Specifies high I/O threshold
By default high I/O is not monitored (--HIGHIO=0)

=item --LONGRUNNING=120

Specifies long running transaction threshold
Default is 120 min (2 hours)

=item --MAILTO=a,b,c

Comma separated list of eMail recepients that will be notified in case of
Critical or Error conditions only

=item --NOMONITOR

If specified, only server availability and "bad" statuses are being monitored.

=item --NOSA

Supresses checks for processes initiated by user with privileged roles:
sa, sso, oper

=item --PERSIST

Specifies the persistent mode of operation

=item --PERSIST_MAIL_DELAY=300

(Persistent mode). Specifies mail delays in sec. (to avoid spam).
The default is 300 sec

=item --RUNNABLE=xxx

Specifies runnable process count threshold as multiplier of the number of
engines on the monitored server.
By default runnable process count is not monitored (--RUNNABLE=0)

=item --SLEEPTIME=120

(Persistent mode). Specifies inactivity time between the consecutive runs.
The default is 120 sec (--SLEEPTIME=120)

=item --WARN_MAILTO=x,y,z

Comma separated list of eMail recepients that will be notified in case of
Critical, Error or Warning condition

=item --ZOMBIE=60

"zombie" process time thresholdi (in minutes).
Default is 60 min.

=back

=head2 NOTES

To supress server monitoring, add NOT_MONITORED=1 clause to the server password file.

=cut

