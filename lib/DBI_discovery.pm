package  DBI_discovery;

#
# This file best viewed at tabstop=3
#
use   vars qw($VERSION @ISA @EXPORT @EXPORT_OK); 
use   strict;

use   Carp qw(confess croak);
use  	Do_Time;
use	CommonFunc;
use 	DBIFunc;
use 	File::Basename;
use 	Sys::Hostname;
use 	Net::Ping::External qw(ping);
use	IO::Socket::INET;

# our($main::DEBUG);
my(%saved_portping_results, %pinged_hosts);

my  $curdir=dirname($0);
# die "You Must Map Samba Shares To A Drive Letter.\nCurdir=$curdir\n" 
#	if $^O =~ /MSWin32/ and $curdir =~ /\\\\/;
# chdir($curdir) or die "Cant cd to $curdir: $!\n";

require  Exporter;
@ISA     = qw(Exporter);
@EXPORT  = qw( LoginAndFetch_SqlServer LoginAndFetch_Sybase LoginAndFetch_Win32 LoginAndFetch_Unix LoginAndFetch_Mysql LoginAndFetch_Oracle
port_detect port_detect_is_win32 port_detect_is_sqlsvr fetch_win32_driveinfo_sub
port_detect_is_httpd port_detect_is_mysql port_up port_report port_report2 qs_do_ping_hostname 
tcp_portping TnsPing fetch_dbi_data_sources FetchMetadataOracle FetchMetadataSybase	
FetchMetadataSqlsvr FetchMetadataMysql	FetchMetadataUnix	FetchMetadataWin32 );

$VERSION = '1.0';

sub _statusmsg { print "        ",@_; }
sub _debugmsg { print( "[debug] ",@_) if $main::DEBUG; }
sub _diagmsg  { print( "[diag]  ",@_) if $main::DIAGNOSE; }

my(%qs_pingstate);		# saved ping state so you dont need to reping
sub qs_do_ping_hostname {
	my($host)=@_;
	return $qs_pingstate{$host} if defined $qs_pingstate{$host};
	$! = undef;

	# print "[func] qs_do_ping_hostname(name=$host)\n";
	#
	# WARNING: YOU CAN *** NOT *** USE AN EVAL BLOCK HERE - Net::Ping::External
	#    aborts ungracefully (no messages or whever) if you do
	#
	#local $SIG{ALRM} = sub { die "CommandTimedout"; };
	#eval {
	#	alarm(10);

	$qs_pingstate{$host} = ping( host=>$host, timeout=>1 );
	
	#	alarm(0);
	#};
	#if( $@ ) {
	#	print __FILE__," ",__LINE__," DBG DBG\n";
	#
	#	if( $@=~/CommandTimedout/ ) {
	#		_statusmsg( "[func] PING COMMAND TO $host TIMED OUT.\n" );
	#		print __FILE__," ",__LINE__," DBG DBG\n";
	#
	#	} else {
	#		_statusmsg( "[func] PING Command To $host Returned $@\n" );
	#		alarm(0);
	#	}
	#}

	$! = undef if $! =~ /Bad file descriptor/ and $^O eq 'MSWin32';

	die("ping returned \"$!\" - could ping not be in your path\n")
		if $! eq "No such file or directory" or $! =~ /ping: not found/;

	return $qs_pingstate{$host};
}

sub RunCommand {
 	my($cmd)=@_;
 	if( ! open( WINCMD,"$cmd |")) {
		print "Cant Run $cmd\n";		
		return undef;
	}		
	my(@output);	
   while ( <WINCMD> ) {
   	chomp; chomp; 			
		push @output, $_;		
   }   
   close(WINCMD);	
   return @output;
}

#sub fetch_sybase_interfaces
#{
#	my($search_string,$filename)=@_;
#	error_out( "SYBASE ENV not defined") unless defined $ENV{"SYBASE"};
#
#	my(@srvlist,%port,%host,%found_svr);
#
#	if( defined $filename and ! -e $filename ) {
#		print "Warning: File $filename Not Found\n";
#	}
#
#	if( -e $ENV{"SYBASE"}."/interfaces" or ( $^O ne 'MSWin32' and -e $filename )) {
#		$filename=$ENV{"SYBASE"}."/interfaces" unless defined $filename;
#
#		# UNIX
#		open FILE,$filename or error_out( "Cant open $filename : $!" );
#		my($srv,$dummy);
#		while( <FILE> ){
#			next if /^#/;
#			chomp;
#
#			if( /^\s/ ) {
#				next unless /query/;
#				next if $srv eq "";
#				chomp;
#
#				my(@vals)=split;
#
#				# Sun Binary Format
#				if( $vals[4] =~ /^\\x/ ) {
#					#format \x
#					$vals[4] =~ s/^\\x0002//;
#
#					my($p) = hex(substr($vals[4],0,4));
#					my($pk_ip) = pack('C4',
#						hex(substr($vals[4],4,2)),
#						hex(substr($vals[4],6,2)),
#						hex(substr($vals[4],8,2)),
#						hex(substr($vals[4],10,2)));
#
#					my($name,$aliases,$addrtype,$len,@addrs);
#					if( defined $found_name{$pk_ip} ) {
#						$name=$found_name{$pk_ip};
#					} else {
#						($name,$aliases,$addrtype,$len,@addrs) = gethostbyaddr($pk_ip,AF_INET);
#						$found_name{$pk_ip}=$name if defined $name and $name !~ /^\s*$/;
#					}
#
#					my($h) =$name ||
#						hex(substr($vals[4],4,2)).".".
#						hex(substr($vals[4],6,2)).".".
#						hex(substr($vals[4],8,2)).".".
#						hex(substr($vals[4],10,2));
#
#					if( defined $host{$srv}  ) {
#						$host{$srv}.="+".$h;
#						$port{$srv}.="+".$p;
#					} else {
#						$host{$srv}=$h;
#						$port{$srv}=$p;
#					}
#				} else {
#					$port{$srv} = $vals[4];
#					$host{$srv} = $vals[3];
#				}
#
#				push @srvlist,$srv unless defined $found_svr{$srv};
#				$found_svr{$srv} = 1;
#				# $srv="";
#			} else {
#				($srv,$dummy)=split;
#			}
#		}
#		close FILE;
#
#	} elsif( ( $^O eq 'MSWin32' and -e $filename ) or -e $ENV{"SYBASE"}."/ini/sql.ini"  ) {
#
#		# DOS
#		# fix up sybase env vbl
#		$ENV{"SYBASE"} =~ s#\\#/#g;
#		open FILE,$ENV{"SYBASE"}."/ini/sql.ini"
#			or error_out( "Cant open file ".$ENV{"SYBASE"}."/ini/sql.ini: $!\n" );
#		my($cursvr)="";
#		while( <FILE> ){
#			next if /^#/;
#			next if /^;/;
#			next if /^\s*$/;
#			chop;
#
#	#
#	# MUST ALSO HANDLE BIZARROS LIKE THE FOLLOWING
#	#
#	#[NYSISW0035]
#	#$BASE$00=NLMSNMP,\pipe\sybase\query
#	#$BASE$01=NLWNSCK,nysisw0035,5000
#	#MASTER=$BASE$00;$BASE$01;
#	#$BASE$02=NLMSNMP,\pipe\sybase\query
#	#$BASE$03=NLWNSCK,nysisw0035,5000
#	#QUERY=$BASE$02;$BASE$03;
#
#			if( /^\[/ ) {
#				# IT A SERVER LINE
#				s/^\[//;
#				s/\]\s*$//;
#				s/\s//g;
#				$cursvr=$_;
#				push @srvlist,$_ unless defined $found_svr{$_};
#				$found_svr{$_} = 1;
#			} else {
#				# IT A DATA LINE
#				next if /^master=/i;
#				next if /QUERY=\$BASE/i;
#				next if /\,.pipe/i;
#				next if /NLMSNMP/i;
#				(undef,$host{$cursvr},$port{$cursvr})=split /,/;
#			}
#		}
#		close FILE;
#	} elsif( defined $filename ) {
#		error_out( "$filename error" );
#	} else {
#		error_out( "UNABLE TO DETERMINE INTERFACE FILE" );
#	}
#
#	my(@rc);
#	foreach (sort @srvlist ) {
#		if(defined $search_string) {
#			next unless /$search_string/i
#				or $host{$_} =~ /$search_string/i
#				or $port{$_} =~ /$search_string/i;
#		}
#		push @rc,[ $_,$host{$_},$port{$_} ];
#	}
#	return @rc;
#}

#
# INTERNAL DBI STUFF
#
sub fetch_dbi_data_sources {
	my(%DBI_RC);
	
	my(@av_drivers)=DBI->available_drivers();
	_debugmsg("Available Drivers: ",join(" ",@av_drivers),"\n" );
	
	# header( "Fetching Available DBI DSN's By Type" );
	foreach my $dt (@av_drivers) {
		if( $^O =~ /MSWin32/ ) {
			next unless $dt=~/ODBC/;
		} else {
			next unless $dt=~/Oracle/i or $dt=~/Sybase/i or $dt=~/Adaptive/ or $dt=~/Mysql/i;
		}
		eval {  DBI->data_sources($dt);	};
		if( $@ ) {
			_statusmsg( "# The following error can be ignored\n" );
			_statusmsg( "# ERRORS FOUND for $dt \n" );
			_statusmsg( "# $@ \n" );
			_statusmsg( "# ^^^ The above error can be ignored\n" );
		} else {
			my($cnt)=0;
			foreach ( sort DBI->data_sources($dt)) {
				s/DBI:$dt://i;
				s/server=//i;
				next if /Visio Database Samples/i or /MS Access/i or /localserver/i or /excel Files/i
						or /LocalServer/;
				_debugmsg( "\tFOUND $_ - $dt\n" );
				s/\..+$//;
				$DBI_RC{"$dt:$_"} = "DBIONLY";	
				$cnt++;
			}
			_statusmsg( "Processed DSN Type $dt - $cnt Entries\n" );			
		}
	}	
	return %DBI_RC;
}

sub tcp_portping {
   #my($name,$port)=@_;
   #local $SIG{__WARN__} = sub { };
   #my $connection = IO::Socket::INET->new(Proto=>'tcp', PeerAddr=>$name, PeerPort=>$port, Timeout=>1);
   #return 0 unless $connection;
   #return 1;

   my($name,$port,$timeout)=@_;
	$timeout=1 unless $timeout;
   #local $SIG{WARN} = sub { };
   local $SIG{ALRM} = sub { die "CommandTimedout"; };

	my($connection);
	#_debugmsg("[func] tcp_portping(name=$name,port=$port)\n");

	eval {
		alarm(2*$timeout);
   	$connection = IO::Socket::INET->new(Proto=>'tcp', PeerAddr=>$name, PeerPort=>$port, Timeout=>$timeout);
   	alarm(0);
	};
	if( $@ ) {
		_statusmsg( "[func] PORTPING RETURNED $@\n" );
		if( $@=~/CommandTimedout/ ) {
			return 0;
		} else {
			return 0;
			alarm(0);
		}
	}
   return 0 unless $connection;
	return 1;
}
sub TnsPing {
	my($svr,$DIAG)=@_;
	_statusmsg( "executing tnsping $svr\n" );
	my(@rc)=RunCommand("tnsping $svr\n");
	if( $DIAG ) {	foreach (@rc) { print "\t=> $_\n"; } }
	my($ll)=$rc[$#rc];
	if( $ll =~ /Failed/ ) {
		_debugmsg( "*** FAILED ***\n" );		
		return 0;
	} elsif( $ll =~ /^OK\s/ ) {
		_debugmsg( "OK\n" );		
		return 1;
	} else {
		_debugmsg( "UNKNOWN $ll\n" );
		return 0;
	}	
}

#require DBD::Driver::GetInfo;
#require DBD::Driver::TypeInfo;
#print Dumper DBD::Sybase::TypeInfo::type_info_all;
# print Dumper DBD::Driver::TypeInfo::type_info_all;
#my($dbh, $info_type) = @_;
#require DBD::Driver::GetInfo;
#my $v = $DBD::Driver::GetInfo::info{int($info_type)};
#print Dumper $v;


########## port ping code
sub port_detect {
	my($host)=@_;
	return if $pinged_hosts{$host};
	$pinged_hosts{$host} = 1;
	foreach my $port ( 7, 22, 37, 80, 8080, 1433, 1434, 3306 ) {
		$saved_portping_results{$host.":".$port} = tcp_portping($host,$port);
	}
}		
sub port_detect_is_win32 {	
	my($host)=@_;	
	return 0 unless port_up($host,22); 
	#return 0 if port_up($host,7); 
	return 1; 
}

sub port_detect_is_sqlsvr{	
	my($host)=@_;	return 0 unless port_detect_is_win32(); 
	return 1 if port_up($host,1433) or port_up($host,1434); 
	return 0; 
}
sub port_detect_is_httpd {	
	my($host)=@_;	
	return 1 if port_up($host,80) or port_up($host,8080); 
	return 0; 
}
 sub port_detect_is_mysql {	
	my($host)=@_;	
	return port_up($host,3306); 
}
sub port_up {
	my($host,$port)=@_;
	port_detect($host);
	return $saved_portping_results{$host.":".$port};
}

sub port_report {
	printf "%20s", " ";	
	foreach my $port ( 7, 80, 8080, 1433, 1434, 3306 ) {	printf "%6s", $port; }
	print "\n";
	
	foreach my $host ( sort ( keys %pinged_hosts )) {
		printf "%20s", $host;
		foreach my $port ( 7, 80, 8080, 1433, 1434, 3306 ) {
			printf "%6s", $saved_portping_results{$host.":".$port};
		}
		print "\n";
	}
	print "\n";	
}

sub port_report2
{
	printf "%20s", "Host";
	printf "%7s", "SqlSvr";
	printf "%7s", "Win32";
	printf "%7s", "Http";
	printf "%7s\n", "Mysql";
		
	foreach my $host ( sort ( keys %pinged_hosts )) {
	# foreach my $host ( "sybmon", "adsdr001", "imagsyb2","adsrv059","adsrv004","imagrep5","ctsybmon","omsprod1","adsrv115" ) {
		printf "%20s", $host;
		printf "%7s", port_detect_is_sqlsvr($host); 
		printf "%7s", port_detect_is_win32($host); 
		printf "%7s", port_detect_is_httpd($host); 
		printf "%7s", port_detect_is_mysql($host);
		print "\n";
	}
	print "\n";
}


sub fetch_win32_driveinfo_sub {
	my($system)=@_;

	local $SIG{ALRM} = sub { die "CommandTimedout"; };
	my(@rc);
	eval {
		alarm(2);
		my($url)="\\\\".$system.'\c$';
		my(@diskdat)=Win32::DriveInfo::DriveSpace($url);
		if( defined $diskdat[5] ) {
			push @rc,'c$';
		}
	   alarm(0);
	};

	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			_statusmsg( "[func] DriveSpace \\$system\c$ Command Timed Out\n" );
			return ();
		} else {
			_statusmsg( "[func] DriveSpace \\$system\c$ Command Returned $@\n" );
			alarm(0);
		}
	}

	foreach my $drive ( 'd$','e$','f$','g$','h$','i$' ) {
		my($url)="\\\\".$system."\\".$drive;
		my(@diskdat)=Win32::DriveInfo::DriveSpace($url);
		push @rc,$drive if defined $diskdat[5];
	}
	return @rc;
}

# Example Port Detection Code	
#foreach my $host ( "sybmon", "adsdr001", "imagsyb2","adsrv059","adsrv004","imagrep5","ctsybmon","omsprod1","adsrv115" ) {
#	port_detect($host);
#}
#port_report();
#port_report2();
########## port ping code end

#
# GetMetaData Procedures
#     takes connectivity parameters etc
#     returns hashref containing
#			BestConnectionMethod
#				UNKNOWN
#				OSQL_NATIVE 
#				OSQL_LOGINPASS
#				ISQL_LOGINPASS
#				DBI_LOGINPASS
#
#		$SERVER_PROPERTIES{BestConnectionMethod}	= "UNKNOWN";	# UNKNOWN|DBI_LOGINPASS, ISQL_LOGINPASS, OSQL_NATIVE, OSQL_LOGINPASS
#		$SERVER_PROPERTIES{XsqlNativeAuth}			= "UNKNOWN";	# UNKNOWN|OK|FAILED
#		$SERVER_PROPERTIES{XsqlLoginPassAuth}		= "UNKNOWN";	# UNKNOWN|OK|FAILED
#		$SERVER_PROPERTIES{DBINativeAuth}			= "UNKNOWN";	# UNKNOWN|OK|FAILED
#		$SERVER_PROPERTIES{DBILoginPassAuth}		= "UNKNOWN";	# UNKNOWN|OK|FAILED
#
# if ok, then set these
# 		$SERVER_PROPERTIES{LoginToUse}= "";
# 		$SERVER_PROPERTIES{PasswordToUse}= "";		

sub LoginAndFetch_Sybase {
	my($svr,$dummy, $skip_metadata,$login,$pass)=@_;	
	
	return unless $svr=~/\w/;	# be nice if the server wasnt a blank wouldnt it... 	
	
	my(%SERVER_PROPERTIES);	# master hash of server properties - returned...
	 
	$SERVER_PROPERTIES{BestConnectionMethod}	= "UNKNOWN";	
	$SERVER_PROPERTIES{XsqlNativeAuth}			= "UNKNOWN";	
	$SERVER_PROPERTIES{XsqlLoginPassAuth}		= "UNKNOWN";	
	$SERVER_PROPERTIES{DBINativeAuth}			= "UNKNOWN";	
	$SERVER_PROPERTIES{DBILoginPassAuth}		= "UNKNOWN";	# OK|FAILED
	
	print " ==> Trying isql login/pass authentication\n" if $main::DEBUG;				
	my($query) = "echo \"print 'LOGIN OK'\ngo\" | isql -U$login -P$pass -S$svr";
	print "DBGDBG $query\n" if $main::DEBUG;
	$SERVER_PROPERTIES{XsqlLoginPassAuth}	= "FAILED";			
	open(Q,"$query |") or die "Cant run isql query\n";		
	while(<Q>) {
		chomp;
		print "RC=$_\n" if $main::DEBUG;
		die $_ if /command not found/i;
			
		if( /LOGIN OK/ ) {
			$SERVER_PROPERTIES{XsqlLoginPassAuth}= "OK";  	
			$SERVER_PROPERTIES{BestConnectionMethod} = "ISQL_LOGINPASS";
		}
	}
	close(Q);
	
	if( $SERVER_PROPERTIES{XsqlLoginPassAuth} ne "OK"  ) {
			return \%SERVER_PROPERTIES;
	}
	
	my($rc) = dbi_connect(-srv=>$svr,-login=>$login,-password=>$pass,-quiet=>1);
	if( $rc ) {	
		_debugmsg("DBI Connection to $svr OK\n");
		$SERVER_PROPERTIES{BestConnectionMethod} = "DBI_LOGINPASS";
		$SERVER_PROPERTIES{DBILoginPassAuth}= "OK";
		$SERVER_PROPERTIES{LoginToUse}= $login;
		$SERVER_PROPERTIES{PasswordToUse}= $pass;
		foreach 	(  dbi_query(-db=>'master',-query=>'select @@servername') ) {
			my($x)=dbi_decode_row($_);
			$SERVER_PROPERTIES{SERVERNAME}= $x;	
			print "DBGDBG: setting servername to $x\n";	
		}
		dbi_disconnect();
	} else {
		$SERVER_PROPERTIES{DBILoginPassAuth}= "FAILED";
		_debugmsg("DBI Connection to $svr FAILED\n");
	}

	
	if( $SERVER_PROPERTIES{BestConnectionMethod} =~ /UNKNOWN/ ) {
		_debugmsg( "     ==> Cant connect to $svr via DBI - ignoring\n" );
		return \%SERVER_PROPERTIES;
	}

	if( $skip_metadata ) {
		_debugmsg( "Returning as skip_metadata specified\n" );
		return \%SERVER_PROPERTIES;
	}
	return FetchMetadataSybase($svr,$login,$pass,%SERVER_PROPERTIES);
}

sub FetchMetadataSybase {
	my($svr,$login,$pass,%SERVER_PROPERTIES)=@_;
	
	#
	# FETCH METADATA
	#
	print " ==> Trying isql login/pass authentication\n" if $main::DEBUG;				
	my($mquery) = "isql -U$login -P$pass -n -S$svr -i$curdir/sybase_metadata.sql -s~ -w2000";
	_statusmsg( " => running $curdir/sybase_metadata.sql\n" );
	_statusmsg( " => running $mquery\n" );
	open(Q,"$mquery |") or die "Cant run: $mquery\n";		
	my($cnt)=0;
	while(<Q>) {
		chomp;
		s/\s+~/~/g;
		s/~\s*$//g;
		s/^~//g;
		next if /-----~----/;
		next if /Property~Value/i;
		$cnt++;
		print "RC=$_\n" if $main::DEBUG 
					and ! /has occurred while establishing a connection/
					and ! /When connecting to SQL Server 2005/
					and ! /the fact that under the default settings SQL/
					and ! /^Server /
					and ! /Provider: Could not open a connection to SQL/
					and ! /^connections\./
					and ! /------/;
		my($a,$b)=split(/~/,$_,2);	
		$a=~s/\s+$//;		
		$b=~s/\s+$//;
		if( $SERVER_PROPERTIES{$a} ) {
			$SERVER_PROPERTIES{$a} .= ";".$b unless $b eq "NULL" or $b =~ /^\s*$/;
		} else {
			$SERVER_PROPERTIES{$a} = $b unless $b eq "NULL" or $b =~ /^\s*$/;
		}
	}
	close(Q);
	
	print "DBGDBG ",__LINE__," Metadata Query REturned $cnt Rows\n";

	use Data::Dumper;
	die "Woah - Meta Data Failed Dramatically - $cnt rows", Dumper(\%SERVER_PROPERTIES) if $cnt<10;		
	$SERVER_PROPERTIES{DBTYPE}="sybase";
	
	return \%SERVER_PROPERTIES;	
}

sub LoginAndFetch_Unix {
	my($svr,$skip_metadata)=@_;	
	my(%SERVER_PROPERTIES);	# master hash of server properties - returned...	 
	$SERVER_PROPERTIES{BestConnectionMethod}	= "UNKNOWN";	
	
	if( $skip_metadata ) {
		return \%SERVER_PROPERTIES;
	}
	
	return FetchMetadataUnix($svr,%SERVER_PROPERTIES);
}

sub FetchMetadataUnix {
	my($cursvr,%SERVER_PROPERTIES)=@_;
	return \%SERVER_PROPERTIES;
}
	
sub LoginAndFetch_Win32 {
	my($svr,$skip_metadata)=@_;	
	my(%SERVER_PROPERTIES);	# master hash of server properties - returned...	 
	load_pkg("Win32::DriveInfo");
	my(@disks)=fetch_win32_driveinfo_sub($svr);
	$SERVER_PROPERTIES{BestConnectionMethod}	= "UNKNOWN";	
	$SERVER_PROPERTIES{BestConnectionMethod}	= "OK" if $#disks>=0;	
	$SERVER_PROPERTIES{DRIVES}=join(",",@disks);
	if( $skip_metadata ) {
		return \%SERVER_PROPERTIES;
	}
	
	return FetchMetadataWin32($svr,%SERVER_PROPERTIES);
}

sub FetchMetadataWin32 {
	my($srv,%SERVER_PROPERTIES)=@_;
	load_pkg("Win32::MachineInfo");	
	load_pkg("Win32::DriveInfo");
	load_pkg("Win32::SystemInfo");
	my(@disks)=fetch_win32_driveinfo_sub($srv);
	
	if (Win32::MachineInfo::GetMachineInfo($srv, \%SERVER_PROPERTIES)) {
		print "Collected Machine Info for $srv\n";
	} else {
		print STDERR "\tError Collecting Data From $srv : $^E\n"
			unless $^E =~ /Overlapped I\/O operation/;
 	}
 	$SERVER_PROPERTIES{DriveInfo}=join(" ",@disks);
	
	my( %mHash );
   if ( Win32::SystemInfo::MemoryStatus( \%mHash,"MB" ) )   {
   	foreach (keys %mHash) {
   		my($val)=int($mHash{$_}+.5);
   		$SERVER_PROPERTIES{$_."Memory"} = $val."MB"
   	}
   } else {
   	print STDERR "\tCant Collect Memory Status of $srv\n";
   }
   
   my( %pHash);
   if (Win32::SystemInfo::ProcessorInfo(%pHash))   {
   	$SERVER_PROPERTIES{"NumProcessors"} = $pHash{NumProcessors};
      foreach (keys %{$pHash{Processor1}}) {
   		$SERVER_PROPERTIES{$_} = $pHash{Processor1}->{$_};
   	}
   } else {
   	print STDERR "\tCant Collect Memory Status of $srv\n";
   }
   
   #use Data::Dumper;
   #print Dumper \%SYSTEM_INFO;
   
	return \%SERVER_PROPERTIES;
}

sub LoginAndFetch_Mysql {
	my($svr,$hostname,$skip_metadata,$login,$pass)=@_;	
	my(%SERVER_PROPERTIES);	# master hash of server properties - returned...	 
	$SERVER_PROPERTIES{BestConnectionMethod}	= "UNKNOWN";	# UNKNOWN|DBI_LOGINPASS, ISQL_LOGINPASS, OSQL_NATIVE, OSQL_LOGINPASS
	$SERVER_PROPERTIES{XsqlNativeAuth}			= "UNKNOWN";	# UNKNOWN|OK|FAILED
	$SERVER_PROPERTIES{XsqlLoginPassAuth}		= "UNKNOWN";	# UNKNOWN|OK|FAILED
	$SERVER_PROPERTIES{DBINativeAuth}			= "UNKNOWN";	# UNKNOWN|OK|FAILED
	$SERVER_PROPERTIES{DBILoginPassAuth}		= "UNKNOWN";	# UNKNOWN|OK|FAILED	
	if( $skip_metadata ) {
		return \%SERVER_PROPERTIES;
	}
	
	return FetchMetadataMysql($svr,"mysql",$login,$pass,%SERVER_PROPERTIES);
}

sub FetchMetadataMysql {
	my($cursvr,$curtyp,$login,$pass,%SERVER_PROPERTIES)=@_;
	return \%SERVER_PROPERTIES;
}

sub LoginAndFetch_Oracle {
	my($svr,$dummy,$skip_metadata,$login,$pass)=@_;	
	return undef unless $svr=~/\w/;	# be nice if the server wasnt a blank wouldnt it... 	
	
	my(%SERVER_PROPERTIES);	# master hash of server properties - returned...	 
	$SERVER_PROPERTIES{BestConnectionMethod}	= "UNKNOWN";	# UNKNOWN|DBI_LOGINPASS, ISQL_LOGINPASS, OSQL_NATIVE, OSQL_LOGINPASS
	$SERVER_PROPERTIES{XsqlNativeAuth}			= "UNKNOWN";	# UNKNOWN|OK|FAILED
	$SERVER_PROPERTIES{XsqlLoginPassAuth}		= "UNKNOWN";	# UNKNOWN|OK|FAILED
	$SERVER_PROPERTIES{DBINativeAuth}			= "UNKNOWN";	# UNKNOWN|OK|FAILED
	$SERVER_PROPERTIES{DBILoginPassAuth}		= "UNKNOWN";	# UNKNOWN|OK|FAILED	
	
	if( $skip_metadata ) {
		return \%SERVER_PROPERTIES;
	}
	
	return FetchMetadataOracle($svr,$login,$pass,%SERVER_PROPERTIES);
}

sub FetchMetadataOracle {
	my($cursvr,$login,$pass,%SERVER_PROPERTIES)=@_;
	return \%SERVER_PROPERTIES;
}

sub LoginAndFetch_SqlServer {

#	if( ! defined 	$sqlsvr_metadata_query ) {		# init
#		$sqlsvr_metadata_query = "select \n";
#		foreach ( @sql_svr_properties ) {	$sqlsvr_metadata_query.= "\tconvert(varchar(30),ServerProperty('".$_."')),\n";	}
#		$sqlsvr_metadata_query =~ s/,$//;
#		print "QUERY: $sqlsvr_metadata_query\n" if $main::DEBUG;
#	}
	
	my($svr,$nativesvr,$skip_metadata,$login,$pass)=@_;		
	return undef unless $svr=~/\w/;	# be nice if the server wasnt a blank wouldnt it... 	
	
	my(%SERVER_PROPERTIES);	# master hash of server properties - returned...	 
	$SERVER_PROPERTIES{BestConnectionMethod}	= "UNKNOWN";	
	$SERVER_PROPERTIES{XsqlNativeAuth}			= "UNKNOWN";	
	$SERVER_PROPERTIES{XsqlLoginPassAuth}		= "UNKNOWN";	# OK|FAILED
	$SERVER_PROPERTIES{DBINativeAuth}			= "UNKNOWN";	
	$SERVER_PROPERTIES{DBILoginPassAuth}		= "UNKNOWN";	# OK|FAILED
	
	if( $login=~/^\s*$/ and $pass=~/^\s*$/ ) {
		#
		# OSQL Native Authentication
		#
		_debugmsg( " ==> Trying osql native authentication" );
		my($query) = "osql -E -n -S$nativesvr -Q\"print \'LOGIN OK\'\"";
		print "\n => $query\n" if $main::DEBUG;
		$SERVER_PROPERTIES{XsqlNativeAuth}	= "FAILED";			
		open(Q,"$query |") or die "Cant run osql query\n";		
		while(<Q>) {
			chomp;
			print "RC=$_\n" if $main::DEBUG 
						and ! /has occurred while establishing a connection/
						and ! /When connecting to SQL Server 2005/
						and ! /the fact that under the default settings SQL/
						and ! /^Server /
						and ! /Provider: Could not open a connection to SQL/
						and ! /^connections\./;
			if( /LOGIN OK/ ) {
				$SERVER_PROPERTIES{XsqlNativeAuth}	= "OK";
				$SERVER_PROPERTIES{BestConnectionMethod} = "OSQL_NATIVE";
				$SERVER_PROPERTIES{LoginToUse}= "";
				$SERVER_PROPERTIES{PasswordToUse}= "";											
			}
		}
		close(Q);
		
		if( $SERVER_PROPERTIES{XsqlNativeAuth} eq "OK"  ) {
			my($rc) = dbi_connect(-srv=>$svr,-login=>'',-password=>'',-quiet=>1);
			if( $rc ) {	
				_debugmsg("DBI Connection to $svr OK\n");
				$SERVER_PROPERTIES{BestConnectionMethod} = "DBI_LOGINPASS";
				$SERVER_PROPERTIES{DBINativeAuth}= "OK";
				$SERVER_PROPERTIES{LoginToUse}= "";
				$SERVER_PROPERTIES{PasswordToUse}= "";							
				dbi_disconnect();
			} else {
				$SERVER_PROPERTIES{DBINativeAuth}= "FAILED";
				_debugmsg("DBI Connection to $svr FAILED\n");
			}
		}
	} elsif( $login=~/\w/ and $pass=~/\w/ ) {
		# 
		# LOGIN/PASS Authentication If Available
		#		
		_debugmsg( " ==> Trying osql login/pass authentication" );				
		my($query) = "osql -U$login -P$pass -n -S$nativesvr -Q\"print \'LOGIN OK\'\"";
		_debugmsg( " ==> $query\n" );
		$SERVER_PROPERTIES{XsqlLoginPassAuth}	= "FAILED";			
		open(Q,"$query |") or die "Cant run osql query\n";		
		while(<Q>) {
			chomp;
			print "RC=$_\n" if $main::DEBUG 
						and ! /has occurred while establishing a connection/
						and ! /When connecting to SQL Server 2005/
						and ! /the fact that under the default settings SQL/
						and ! /^Server /
						and ! /Provider: Could not open a connection to SQL/
						and ! /^connections\./;
			if( /LOGIN OK/ ) {
				$SERVER_PROPERTIES{XsqlLoginPassAuth}= "OK";			
				$SERVER_PROPERTIES{BestConnectionMethod} = "OSQL_LOGINPASS";
				$SERVER_PROPERTIES{LoginToUse}= $login;
				$SERVER_PROPERTIES{PasswordToUse}= $pass;										
			}
		}
		close(Q);
		
		if( $SERVER_PROPERTIES{XsqlLoginPassAuth} eq "OK"  ) {
			my($rc) = dbi_connect(-srv=>$svr,-login=>$login,-password=>$pass,-quiet=>1);
			if( $rc ) {	
				_debugmsg("DBI Connection to $svr OK\n");
				$SERVER_PROPERTIES{BestConnectionMethod} = "DBI_LOGINPASS";
				$SERVER_PROPERTIES{DBILoginPassAuth}= "OK";	
				$SERVER_PROPERTIES{LoginToUse}= $login;
				$SERVER_PROPERTIES{PasswordToUse}= $pass;														
				dbi_disconnect();
			} else {
				$SERVER_PROPERTIES{DBILoginPassAuth}= "FAILED";
				_debugmsg("DBI Connection to $svr FAILED\n");
			}
		}
	} else {
		die "Error - invalid login/password pair ($login/$pass) Sent \n";
	}
		
	if( $SERVER_PROPERTIES{BestConnectionMethod} =~ /UNKNOWN/ ) {
		_debugmsg( "     ==> Cant connect to $svr via osql - ignoring\n" );
		return \%SERVER_PROPERTIES;
	}
	
	$SERVER_PROPERTIES{DBTYPE}="sqlsvr";

	if( $skip_metadata ) {
		return \%SERVER_PROPERTIES;
	}
	return FetchMetadataSqlsvr($svr,$login,$pass,%SERVER_PROPERTIES);
}

sub FetchMetadataSqlsvr {
	my($svr,$login,$pass,%SERVER_PROPERTIES)=@_;
	
	my($mquery);
	#if( $SERVER_PROPERTIES{XsqlNativeAuth}	eq "OK" ) {
	if( ! $login or $login eq "native" ) {
		_debugmsg( " ==> Trying osql native authentication" );
		$mquery = "sqlcmd -E -W -S$svr -i$curdir/sqlsvr_metadata.sql -s~ -w2000\n";
	} else {
		_debugmsg( " ==> Trying osql login $login authentication" );
		$mquery = "sqlcmd -U$login -P$pass -W -S$svr -i$curdir/sqlsvr_metadata.sql -s~ -w2000\n";
	}

	_debugmsg( " => $mquery\n" );
	open(Q,"$mquery |") or die "Cant run: $mquery\n";		
	while(<Q>) {
		chomp;
		next if /Property~Value/i;
		next if /\-\-\-\-~\-\-\-\-/i;
		print "RC=$_\n" if $main::DEBUG 
					and ! /has occurred while establishing a connection/
					and ! /When connecting to SQL Server 2005/
					and ! /the fact that under the default settings SQL/
					and ! /^Server /
					and ! /Provider: Could not open a connection to SQL/
					and ! /^connections\./
					and ! /------/;
		my($a,$b)=split(/~/,$_,2);	
		if( $SERVER_PROPERTIES{$a} ) {
			$SERVER_PROPERTIES{$a} .= ";".$b unless $b eq "NULL" or $b =~ /^\s*$/;
		} else {
			$SERVER_PROPERTIES{$a} = $b unless $b eq "NULL" or $b =~ /^\s*$/;
		}
	}
	close(Q);			
	return \%SERVER_PROPERTIES;
}	
	
	# my($q2)="select convert(varchar(30),ServerProperty('MachineName')),convert(varchar(30),ServerProperty('IsClustered')),convert(varchar(30),ServerProperty('ProductLevel')),convert(varchar(30),ServerProperty('ServerName')),	convert(varchar(30),ServerProperty('ProductVersion')),convert(varchar(30),ServerProperty('ComputerNamePhysicalNetBIOS'))";
	#
	#my($q3)='DECLARE @ValueOut varchar(20)
	#EXEC master..xp_regread @rootkey=\'HKEY_LOCAL_MACHINE\', @key=\'SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName\',@value_name=\'ComputerName\', @value=@ValueOut OUTPUT
# SELECT @ValueOut';

	# CLUSTER NODES
	# my($q5)='select * from ::fn_servershareddrives()';
	# my($login,$pass) = get_password(-name=>$svr, -type=>'sqlsvr');
	
#		if( $SERVER_PROPERTIES{BestConnectionMethod} =~ /^OK/ ) {	# we got in via native auth!
#			print " => osql Ok! Digging in!\n";
#			# $query = "osql -w2000 -s~ -E -n -S$svr -Q\"$data_qry\"";
#			$query = "osql -w2000 -s~ -E -n -S$svr -i\"$curdir/sqlsvr_metadata.sql\"";
#			print $query,"\n" if $main::DEBUG;
#			open(Q,"$query |") or die "Cant run osql query\n";
#			while(<Q>) {
#				print __LINE__," Returned $_\n";
#			}
#			close(Q);
#		} else {
#			print "\tosql failed\n";
#		}
#		return \%SERVER_PROPERTIES;
#	}
			
#			$query = "osql -w2000 -s~ -E -n -S$svr -Q\"$q2\"";
#			print $query,"\n" if $main::DEBUG;
#			open(Q,"$query |") or die "Cant run osql query\n";
#			my($row)=0;
#			while(<Q>) {
#				$row++;
#				next if $row<=2;
#				s/\s//g;
#				chomp;
#				
#				my($m,$c,$pr,$se,$pv,$nb)=split(/~/);		
#				# print __LINE__," DBG DBG - SETTING MCHN $svr = $m \n";
#				
#				$SERVER_PROPERTIES{'MachineName'} = uc($m);
#				$SERVER_PROPERTIES{'IsClustered'} = $c;
#				$SERVER_PROPERTIES{'ProductLevel'} = $pr;
#				$SERVER_PROPERTIES{'ServerName'} 	= $se;
#				$SERVER_PROPERTIES{'ProductVersion'} = $pv;
#				
#				$nb=~s/\s//g;
#				$SERVER_PROPERTIES{'ComputerNamePhysicalNetBIOS'} = uc($nb);
#				$SERVER_PROPERTIES{'ComputerNamePhysicalNetBIOS'} = uc($m) unless uc($nb) or $c;
#				
#				# CREATE A WIN32 SERVER IF THIS IS IT!
#				#	set_aliases($svr, $q_netbiosname{$svr}, $q_mchn{$svr}, $q_servername{$svr});
#				
#				my($packedip) = inet_aton($m);		#'adsdr001.ad.mlp.com');
#				my($unpackedip)= join(".",unpack('C4', $packedip));
#				$q_ip{$svr} = $unpackedip;
#				
#				if( $c ) { # CLUSTERED!
#					$query = "osql -w2000 -s~ -E -n -S$svr -Q\"exec master..xp_cmdshell 'ping -a 127.0.0.1'\"";
#					print $query,"\n" if $main::DEBUG;
#					open(I,"$query |") or die "Cant run osql $query\n";
#					my($r1)=0;
#					while(<I>)	 { 
#						$r1++;
#						last if /Msg 15281/;
#						next if $r1<=2;
#						chomp; 
#						s/\s*~//g;
#						s/\s*$//;
#						print "PING: $_\n" if $main::DEBUG; 
#					}
#					close(I);
#					
#					$query = "osql -w20000 -s~ -E -n -S$svr -Q\"$q3\"";
#					print $query,"\n" if $main::DEBUG;
#					my($r2)=0;
#					open(J,"$query |") or die "Cant run osql $query\n";
#					while(<J>) { 
#						$r2++;
#						next if $r2<=2;
#						chomp; 
#						#s/\s*~//g;
#						print "Q3: $_\n" if $main::DEBUG;
#						s/\s//g; 
#						$q_netbiosname{$svr}=uc($_);
#						set_win32($q_netbiosname{$svr});				
#						last;
#					}
#					close(J);				
#				}
#				last;				
#			}
#			close(Q);
#			if( $q_clustered{$svr} ) {			# CLUSTERED NODE
#				get_cluster_info_via_osql($svr);
#			}
#		} else {
#			print " => \'osql -E $svr\' Failed!\n";
#		}
#		return \%SERVER_PROPERTIES;;
#	}					
#	my(@rc)=dbi_query(-db=>"master",-query=>$q);			
#	foreach (@rc) {
#		my($m,$c,$pr,$se,$pv,$nb)=dbi_decode_row($_);
#		
#		if( ! $se ) {
#			print " => $svr IS NOT A SQL SERVER!\n";
#			$SERVER_PROPERTIES{BestConnectionMethod} = "FAILED_NOT_A_SQL_SERVER";	
#			return \%SERVER_PROPERTIES;;
#		}		
#		
#		# print __LINE__," DBG DBG - SETTING MCHN $svr = $m \n";
#		$q_mchn{$svr} = uc($m);
#		set_win32($q_mchn{$svr});
#		$q_clustered{$svr} = $c;
#		$q_product{$svr} = $pr;
#		$q_servername{$svr} = $se;
#		$q_version{$svr} = $pv;
#		$nb=~s/\s//g;
#		$q_netbiosname{$svr}=uc($nb);
#		$q_netbiosname{$svr}=$q_mchn{$svr} unless $q_netbiosname{$svr} or $q_clustered{$svr};
#		set_win32($q_netbiosname{$svr});				
#		
#		set_aliases($svr, $q_netbiosname{$svr}, $q_mchn{$svr}, $q_servername{$svr});
#			
#		my($packedip) = inet_aton($m);		#'adsdr001.ad.mlp.com');
#		my($unpackedip)= join(".",unpack('C4', $packedip));
#		$q_ip{$svr} = $unpackedip;
#	}
#	
#	if( ! $q_netbiosname{$svr} ) {		
#		my(@rc)=dbi_query(-db=>"master",-query=>$q3);			
#		print $q3,"\n" if $main::DEBUG;
#		foreach (@rc) {
#			my($k,$v)=dbi_decode_row($_);
#			$v=~s/\s*//g;
#			next unless $v;
#			print "Netbiosname= $v\n" if $main::DEBUG;
#			$q_netbiosname{$svr}=uc($v);
#			set_win32($q_netbiosname{$svr});				
#		}
#	}	
#	
#	if( ! $q_netbiosname{$svr} ) {
#		my(@rc)=dbi_query(-db=>"master",-query=>'exec master..xp_cmdshell \'ping -a 127.0.0.1\'');			
#		my($lineno)=0;
#		foreach (@rc) {
#			$lineno++;
#			if( $lineno==2 ) {
#				my($rc)=dbi_decode_row($_);
#				print "PINGER: ",$rc,"\n" if $main::DEBUG; 
#				next unless $rc=~/\s*Pinging/i;
#				$rc=~s/^\s*Pinging\s*//i;
#				my($nh,$junk)=split(/\s/,$rc,2);				
#				$nh=~s/\..+//;
#				next unless $nh;
#				$nh=~s/\s//g;
#				print "Netbiosname=$nh\n" if $main::DEBUG;
#				$q_netbiosname{$svr}=uc($nh);
#				set_win32($q_netbiosname{$svr});
#				
#				last;				
#			}			
#		}
#	}
#	my($q4)='select * from ::fn_virtualservernodes()';
#	my($q5)='select * from ::fn_servershareddrives()';
	
sub load_pkg {
	my($str)=@_;
	if ( eval "require $str" ) {
		$str->import();
	} else {
		my $err = $@;
		die "warning: unable to load package $str: $err\n";
	}
}

1;