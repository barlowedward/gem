#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
sub usage
{
	print @_;
	print "extract_ddl.pl [-dx] -UUSER -SSERVER -PSA_PASS -DDB -Oobject

	Extracts object ddl.  Fully qualify object to get a users stuff.\n";
	return "\n";
}

use strict;
use Getopt::Std;
use Sybase::SybFunc;

use vars qw($DEBUG $opt_U $opt_S $opt_P $opt_D $opt_O $opt_d);
die usage("Bad Parameter List") unless getopts('U:D:S:P:O:d');

my($DEBUG)=1 if defined $opt_d;
die usage("Must pass db name\n") 	unless defined $opt_D;
die usage("Must pass password\n" ) 	unless defined $opt_P;
die usage("Must pass server\n") 		unless defined $opt_S;
die usage("Must pass username\n" ) 	unless defined $opt_U;

#
# CONNECT TO DB
#
db_connect($opt_S,$opt_U,$opt_P)
	 or die "Cant connect to $opt_S as $opt_U ($opt_P)\n";

#
# is it a user object
#
my($typ);
my(@rc)=db_query($opt_D,"select type from sysobjects where id=object_id(\"$opt_O\")");
die "Cant Find Object ($opt_O) in Database $opt_D\n" if $#rc<0;
foreach (@rc) { $typ=$_; }

$typ=~s/\s//g;
if( $typ eq "U" ) {
	print "if exists ( select * from sysobjects where id=object_id(\"$opt_O\") )\ndrop table $opt_O\ngo\n";
} elsif( $typ eq "P" ) {
	print "if exists ( select * from sysobjects where id=object_id(\"$opt_O\") )\ndrop proc $opt_O\ngo\n";
} else  {
	print "-- object type = |$typ|\n";
}

print db_get_sql_text($opt_O,1);
print "\ngo\n";

# truncate off user name -perms are probably not right
$opt_O =~ s/\w+\.//;
@rc= db_query("$opt_D","exec sp__helprotect $opt_O");
foreach (@rc) { print $_,"\n"; }
print "go\n";

db_close_sybase();

__END__

=head1 NAME

extract_ddl.pl - extract_ddl utility

=head2 DESCRIPTION

Extract DDL for object and print it.

=head2 USAGE

 extract_ddl.pl [-dx] -UUSER -SSERVER -PSA_PASS -DDB -Oobject

 Extracts object ddl.  Fully qualify object to get a users stuff.
 You may ignore -U and -P options if you have set up password file

=cut

