use strict;
use CGI qw( :standard escapeHTML escape);
use CGI::Carp qw(fatalsToBrowser);
use File::Basename;
use lib qw(..);
my($DEBUG);

my($curdir)=dirname($0);
print "Content-Type: text/html; charset=ISO-8859-1

<HTML><HEAD><TITLE>Server Reports</TITLE></HEAD>
<BODY TEXT=BLACK BGCOLOR=WHITE>";

my($servername)= param('servername');
my($metric)    = param('metric');
$servername=~s/\"//g;
$metric=~s/\"//g;

my($subkey_to_use)=param('SubKey');
$subkey_to_use = '' if ! $subkey_to_use or param('Show Totals') eq "Show Totals";

#print hr;
#foreach( param()) {
#	print $_," ",param($_),br;
#}
# print hr;

print h1(reword($metric)." On $servername");
blueprint("SUB KEY ",$subkey_to_use,br) if $subkey_to_use and $DEBUG;
blueprint('This report is run with $DEBUG set',br) if $DEBUG;

my($q)="select distinct key_2
from PER_intsummary
where system_name='".$servername."'
and   perf_key = '".$metric."'
order by perf_key,key_2";
blueprint($q,p) if $DEBUG;
my(@rc) = _querywithresults($q,1);

my($does_total_exist,@vals);
foreach (@rc) {
	my(@kys)=dbi_decode_row($_);
	if($kys[0] =~ /^total$/i ) {
		$does_total_exist=1;
	} else {
		push @vals,$kys[0];
	}
}
if($DEBUG) {
	print "<FONT COLOR=BLUE>does_total_exist: ",$does_total_exist,"</FONT>",br;
	print "<FONT COLOR=BLUE>vals: ",join("|",@vals),"</FONT>",br;
}

# print Totals and Subtotal By Db box if there are subkeys
if( $#vals>=0 and  $metric ne "space_monitor"  and  $metric ne "win32_diskspace"  ) {
	print start_form();
	print hidden( -name=>"servername",  -default=>$servername);
	print hidden( -name=>"metric", 		-default=>$metric);
	print submit('Show Totals'),"&nbsp;&nbsp;";
	print submit('Subtotal By Database'),"&nbsp;&nbsp;";
	print popup_menu(-name=>'SubKey', -values=>\@vals),"&nbsp;&nbsp;";

	if( $metric =~ /db_file_io/i ) {
		my( @vvv ) =('num_of_bytes_read','num_of_bytes_written','io_stall_read_ms');
		param(-name=>'specialk',-value=>'num_of_bytes_read') unless param('specialk');
		print popup_menu(-name=>'specialk', -values=>\@vvv),"<p>";
	}
	print p;
	print end_form();
}

print "<hr>";


#foreach( param()) {
#	print $_," ",param($_),br;
#}
#print "Psubmit=",param('submit');

if( $metric eq "connections" ) {
	print h2("Connections Report");
	one_variable_15min_report( $servername,$subkey_to_use, "connections",undef);
} elsif( $metric eq "running_threads" ) {
	print h2("Threads Report");
	one_variable_15min_report( $servername,$subkey_to_use, "running_threads",undef);
} elsif( $metric eq "running_processes" ) {
	print h2("Processes Report");
	one_variable_15min_report( $servername,$subkey_to_use, "running_processes",undef);
} elsif( $metric eq "io_usage" ) {
	if( ! param('specialk')) {
		print h2("Io Usage Report");
		one_variable_15min_report( $servername,$subkey_to_use, "io_usage",'add2','M');
	} elsif( param('specialk') eq "writes" ) {
		print h2("Io Writes Report");
		one_variable_15min_report( $servername,$subkey_to_use, "io_usage",'col2','M');
	} elsif( param('specialk') eq "reads" ) {
		print h2("Io Reads Report");
		one_variable_15min_report( $servername,$subkey_to_use, "io_usage",undef,'M');
	}
} elsif( $metric eq "space_monitor" ) {
	print h2("Database Space Monitor Report");
	one_point_per_day_report( $servername, "space_monitor");
} elsif( $metric eq "win32_diskspace" ) {
	print h2("Windows Server Diskspace Report");
	one_point_per_day_report( $servername, "win32_diskspace");
} elsif( $metric =~ /db_file_io/i ) {
	if( ! param('specialk') or param('specialk') eq 'num_of_bytes_read' ) {
		print h2("Database Io By File Id - Reads");
		rate_per_hour_report( $servername, "db_file_io", undef , "M");
	} elsif( param('specialk') eq "num_of_bytes_written" ) {
		print h2("Database Io By File Id - Writes");
		rate_per_hour_report( $servername, "db_file_io", "col2" , "M");
	} elsif( param('specialk') eq "io_stall_read_ms" ) {
		print h2("Database Io By File Id - Stalls");
		rate_per_hour_report( $servername, "db_file_io", "col3", "M");
	}
} elsif( $metric =~ /index_usage_stats/i ) {
	print h2("Windows Server Diskspace Report");
	rate_per_hour_report( $servername, "index_usage_stats");
} else {
	my($numberofdays)=0;
	$numberofdays=14 if $metric eq 'space_monitor';
	print "<FONT COLOR=BLUE>numberofdays: ",$numberofdays,"</FONT>",br if $DEBUG;
	print "Number of Days - $numberofdays",br;
	print "Metric - $metric",br;
	print "Subkey To Use=$subkey_to_use",br if $subkey_to_use;
	print "Normal Report",br;
	# Show The Report
	get_stats_data( $servername,$metric,$subkey_to_use,$numberofdays );
	get_rollup_data( '30DayRollup', $servername,$metric,$subkey_to_use );
	get_rollup_data( '90DayRollup', $servername,$metric,$subkey_to_use );
}
print "</body></html>\n";
exit(0);

use MlpAlarm;
use DBIFunc;

sub rate_per_hour_report {
	# $DEBUG=1;
	print b('rate_per_hour_report') if $DEBUG;
	my( $servername,$perf_key,$instructions,$units ) = @_;

	my($ctrs)="perf_int_1";
	$ctrs="perf_int_1+perf_int_2" if $instructions eq "add2";
	$ctrs="perf_int_2" if $instructions eq "col2";
	$ctrs="perf_int_3" if $instructions eq "col3";

	my($num_counters)=1;
	my($number_of_days)=0;

	my($query);
	#if( $subkey and $subkey ne /total/i ) {
	$query = "select
	datepart(hh,perf_time)*10+datepart(mi,perf_time)/10 as 'time_bucket',
	key_2, key_3, $ctrs
from PER_intstatistics
where system_name = '".$servername."' and perf_key='".$perf_key."' ";

	$number_of_days=0 unless defined $number_of_days;
	$query.= " and datediff(dd,getdate(),perf_time)<=$number_of_days ";
	$query.= " order by perf_time";

	blueprint($query,p) if $DEBUG;
	my(@rc) = _querywithresults($query,1);
	if( $#rc < 0 ) {
		print h2("No Performance Counters Available For Server $servername\n");
		return
	}

	my(%xsubkeys,%xdates, %xdata, %xdata2, %xdata3);
	foreach ( @rc ) {
		my( $date,$key2, $key3, $val1, $val2, $val3 ) = dbi_decode_row($_);
		$xdates{$date}					= 1;
		$xsubkeys{$key2.":".$key3}	= 1;
		if( $units eq "M" ) {
			$xdata{ $key2.":".$key3.":".$date } = int( $val1 / 1000000 ) ;
			$xdata{ $key2.":".$key3.":".$date } .= "M";
		} else {
			$xdata{ $key2.":".$key3.":".$date } = $val1;
		}
		#$xdata2{$key2.":".$key3.":".$date} = $val2;
		#$xdata3{$key2.":".$key3.":".$date} = $val3;
	}

	my(@dts)= sort  { $a <=> $b } keys %xdates;
	my(@display_dts);
	foreach (@dts) { push @display_dts, ($_/10) . ":00"; }

	print "<TABLE BORDER=1>";
	print "<TR><TH>Date</TH><TH>",join("</TH><TH>",@display_dts),"</TH></TR>";
		foreach my $sk ( sort keys %xsubkeys ) {
			print "<TR><TD>",
					$sk,
					"</TD>";
			foreach my $ydate (@dts) {
				my($d)=$xdata{$sk.":".$ydate} || "-";
				print "<TD>",$d,"</TD>";
			}
			print "</TR>";
		}
	print "</TABLE>";
}
sub blueprint {
	print "<FONT COLOR=BLUE>",@_,"</FONT>";
}

sub one_point_per_day_report {
	print b('one_point_per_day_report'),br if $DEBUG;

	my( $servername,$perf_key ) = @_;
	blueprint("servername = $servername",br,"perf_key=",$perf_key,br) if $DEBUG;
	my($num_counters)=1;
	my($query) = "
	select category,	key_2, perf_int_1
	from PER_intsummary
	where system_name = '".$servername."' and perf_key='".$perf_key."' and category not like '%Rollup%'";

	blueprint($query,p) if $DEBUG;
	my(@rc) = _querywithresults($query,1);
	if( $#rc < 0 ) {
		print h2("No Performance Counters Available For Server $servername\n");
		return;
	}

	my(%xsubkeys,%xdates, %xbuckets, %xdata);
	foreach ( @rc ) {
		my( $date,$key,$val ) = dbi_decode_row($_);
		$xdates{$date}		=1;
		$xsubkeys{$key}	=1;
		$xdata{$key.":".$date} = $val;
	}

	my(@dts)=reverse sort keys %xdates;

	#ok we have a lot of data - it wont show so condense
	if( $#dts > 70 ) {		# do once every 4 weeks
		blueprint( "$#dts data points found - printing 1ce/4 weeks ",br ) if $DEBUG;
		my($skips)= 28;
		my(@x);
		my($i)=0;
		foreach (@dts) {
			push @x,$_ if $i==0;
			$i++;
			$i=0 if $i>$skips;
		}
		@dts=@x;
	} elsif( $#dts > 22 ) { # do once a week
		blueprint( "$#dts data points found - printing 1ce/week ",br ) if $DEBUG;

	#	my($skips)= int( ($#dts+9) / 10);
		my($skips)= 7;
		# print "<FONT COLOR=BLUE>Skipping $skips Datapoints to arrive at ~10 columsn</FONT>",p if $DEBUG;
		my(@x);
		my($i)=0;
		foreach (@dts) {
			push @x,$_ if $i==0;
			$i++;
			$i=0 if $i>$skips;
		}
		@dts=@x;
	}
	blueprint( "$#dts data points remain",br ) if $DEBUG;


	print "<TABLE BORDER=1>";
	print "<TR><TH>Date</TH><TH>",join("</TH><TH>",@dts),"</TH></TR>";
	my($total_line);
	foreach my $sk ( sort keys %xsubkeys ) {
		my($lline)= "<TR><TD>".	$sk. "</TD>";
		foreach my $ydate (@dts) {
			my($d)=$xdata{$sk.":".$ydate} || "-";
			$lline .= "<TD>".$d."</TD>";
		}
		$lline .= "</TR>";
		if( $sk =~ /Total/ ) {
			$total_line = $lline;
		} else {
			print $lline;
		}
	}
	print $total_line;
	print "</TABLE>";
}

sub one_variable_15min_report {
	# $DEBUG=1;
	my( $servername,$subkey,$perf_key,$instructions,$units ) = @_;
	my($num_counters)=1;

	#if( $cntrptr ) {
	#	$num_counters = $#cntrptr + 1;
	#}
	my($number_of_days)=14;

	#key_2, key_3, key_4, perf_int_1, perf_int_2, perf_int_3,
	#perf_int_4, perf_int_5
	my($query);
	my($ctrs)="perf_int_1";
	$ctrs="perf_int_1+perf_int_2" if $instructions eq "add2";
	$ctrs="perf_int_2" if $instructions eq "col2";

	if( $subkey and $subkey ne /total/i ) {
		$query = "
	select
		convert(varchar,perf_time,112),
		datepart(hh,perf_time)*10+datepart(mi,perf_time)/10 as 'time_bucket',
		key_2, $ctrs
	from PER_intstatistics
	where system_name = '".$servername."' and perf_key='".$perf_key."' ";

		$number_of_days=0 unless defined $number_of_days;
		$query.= " and datediff(dd,getdate(),perf_time)<=$number_of_days ";
		$query .= "	and key_2='".$subkey."'";

	}	else {

		$query = "	select distinct
		convert(varchar,perf_time,112) as 'perf date',
		datepart(hh,perf_time)*10+datepart(mi,perf_time)/10 as 'time_bucket',
		'Total',
		sum($ctrs)
	from PER_intstatistics
	where system_name = '".$servername."' and perf_key='".$perf_key."' ";

		$number_of_days=0 unless defined $number_of_days;
		$query.= " and datediff(dd,getdate(),perf_time)<=$number_of_days ";
		$query .= " group by 	convert(varchar,perf_time,112),	datepart(hh,perf_time)*10+datepart(mi,perf_time)/10 ";
	}

	blueprint($query,p) if $DEBUG;
	my(@rc) = _querywithresults($query,1);
	if( $#rc < 0 ) {
		print h2("No Performance Counters Available For Server $servername\n");
		return
	}

	my(%xsubkeys,%xdates, %xbuckets, %xdata);
	foreach ( @rc ) {
		my( $date,$bucket,$key,$val ) = dbi_decode_row($_);
		$xdates{$date}		=1;
		$xsubkeys{$key}	=1;
		$xbuckets{$bucket}=1;
		if( $units eq "M" ) {
			$xdata{$key.":".$date.":".$bucket} = int($val / 1000000) ;
			$xdata{$key.":".$date.":".$bucket} .= "M";
		} else {
			$xdata{$key.":".$date.":".$bucket} = $val;
		}
	}

	my(@dts)=reverse sort keys %xdates;

	print "<TABLE BORDER=1>";
	print "<TR><TH>Bucket</TH><TH>Time</TH><TH>",join("</TH><TH>",@dts),"</TH></TR>";
	foreach my $bkt ( sort { $a <=> $b } keys %xbuckets ) {
		my($hour)  = int($bkt / 10);
		$hour="0".$hour if $hour <= 9;
		my($minute)= ($bkt-10*$hour) * 6;
		$minute = "0".$minute if $minute==6 or $minute==0;

		foreach my $sk ( sort keys %xsubkeys ) {
			print "<TR><TD>",
					$hour.":".$minute,
					"</TD><TD>",
					$sk,
					"</TD>";
			foreach my $ydate (@dts) {
				my($d)=$xdata{$sk.":".$ydate.":".$bkt} || "-";
				print "<TD>",$d,"</TD>";
			}
			print "</TR>";
		}
	}
	print "</TABLE>";

	return;

	#print h2("$perf_key Performance Counters for $servername $subkey \n");
	#foreach my $req_key ( sort keys %xsubkeys ) {
	#	print h3("SUB KEY $req_key \n") if $req_key;
		print "<TABLE BORDER=1>";
		print "<TR><TD>".join('</TD><TD>',qw(Day Hour Key1 Key2 Key3 Counter1 Counter2 Counter3 Counter4 Counter5 )),"</TD></TR>";
		foreach ( @rc ) {
			my($dayofweek, $time_bucket,
				$key_2, $key_3, $key_4,
				$perf_int_1, $perf_int_2,$perf_int_3,$perf_int_4,$perf_int_5 ) = dbi_decode_row($_);
	#		next unless $x[2] == $req_key;
			my($hour)  = int($time_bucket / 10);
			my($minute)= ($time_bucket-10*$hour) * 6;
			$minute = "0".$minute if $minute==6 or $minute==0;
			print "<TR><TD>".join('</TD><TD>',($hour.":".$minute,$key_2, $key_3, $key_4,
				$perf_int_1, $perf_int_2,$perf_int_3,$perf_int_4,$perf_int_5)),"</TD></TR>";
		}
		print "</TABLE>";
	#}

}

#if( param('key1') or param('key2')) {	# drilldown
#	my($query1,$query2);
#	if( param('key2') ) {
#		print "Totals For ",param('key1'),' subkey ',param('key2'),br;
#		$query1="select dayofweek,time_bucket,key_3, perf_int_1,perf_int_2,perf_int_3,perf_int_4,perf_int_5
#from PER_intstatistics where perf_key='".param('key1')."' and key_2='".param('key2')."' and ";
#		$query2="
#select category,dayofweek,time_bucket,key_3, perf_int_1,perf_int_2,perf_int_3,perf_int_4,perf_int_5
#from PER_intsummary    where perf_key='".param('key1')."' and key_2='".param('key2')."'
#and key_2 != 'Total' and key_3!='Total' and key_4='Total'
#and category like '%Rollup'";
#	}else {
#		print "Totals For ",param('key1'),br;
#		$query1="select dayofweek,time_bucket,perf_int_1,perf_int_2,perf_int_3,perf_int_4,perf_int_5 from PER_intstatistics where perf_key='".param('key1')."'";
#		$query2="select * from PER_intsummary    where perf_key='".param('key1')."' and key_2 = 'Total'";
#	}
#	print $query1,br;
#	print $query2,br;
#

#	print "</body></html>\n";
#	exit(0);
#}

sub get_stats_data {
	my($servername,$perf_key,$subkey,$number_of_days ) = @_;
	my($query) = "
select top 100
	datepart(dw,perf_time) as 'dayofweek',
	datepart(hh,perf_time)*10+datepart(mi,perf_time)/10 as 'time_bucket',
	key_2, key_3, key_4, perf_int_1, perf_int_2, perf_int_3,
	perf_int_4, perf_int_5
from PER_intstatistics
where system_name = '".$servername."' and perf_key='".$perf_key."' ";

	$number_of_days=0 unless defined $number_of_days;
	$query.= " and datediff(dd,getdate(),perf_time)<=$number_of_days ";
	if( $subkey and $subkey ne /total/i ) {
		$query .= "	and key_2='".$subkey."'";
	}

	blueprint($query,p) if $DEBUG;
	my(@rc) = _querywithresults($query,1);
	if( $#rc < 0 ) {
		print h2("No Performance Counters Available For Server $servername\n");
	} else {
		#my(%xsubkeys);			# get the subkeys
		#foreach ( @rc ) {	my( @x )   = dbi_decode_row($_); $xsubkeys{$x[2]}=1; }
		#print h2("$perf_key Performance Counters for $servername $subkey \n");
		#foreach my $req_key ( sort keys %xsubkeys ) {
		#	print h3("SUB KEY $req_key \n") if $req_key;
			print "<TABLE BORDER=1>";
			print "<TR><TD>".join('</TD><TD>',qw(Hour Key1 Key2 Key3 Counter1 Counter2 Counter3 Counter4 Counter5 )),"</TD></TR>";
			foreach ( @rc ) {
				my($dayofweek, $time_bucket,
					$key_2, $key_3, $key_4,
					$perf_int_1, $perf_int_2,$perf_int_3,$perf_int_4,$perf_int_5 ) = dbi_decode_row($_);
		#		next unless $x[2] == $req_key;
				my($hour)  = int($time_bucket / 10);
				my($minute)= ($time_bucket-10*$hour) * 6;
				$minute = "0".$minute if $minute==6 or $minute==0;
				print "<TR><TD>".join('</TD><TD>',($hour.":".$minute,$key_2, $key_3, $key_4,
					$perf_int_1, $perf_int_2,$perf_int_3,$perf_int_4,$perf_int_5)),"</TD></TR>";
			}
			print "</TABLE>";
		#}
	}
}

sub get_rollup_data {

	my($category,$servername,$perf_key,$subkey ) = @_;
	my($query) = "
select
	category, dayofweek, time_bucket, key_2, key_3, key_4, perf_int_1, perf_int_2, perf_int_3,
	perf_int_4, perf_int_5
from PER_intsummary
where system_name = '".$servername."' and perf_key='".$perf_key."'
and category = '".$category."'";

#	$query.= " and datediff(dd,getdate(),perf_time)=0 ";
	if( $subkey and $subkey ne /total/i ) {
		$query .= "	and key_2='".$subkey."'";		# and key_3!='Total' and key_4='Total'";
	}	else {
		$query .= " and key_2='Total'";
	}

	print $query,br if $DEBUG;
	my(@rc) = _querywithresults($query,1);
	if( $#rc < 0 ) {
		print h2("No Rollup Counters Available For Server $servername\n");
		print "Performance counters are only collected for production servers\n",br;
		print "Most counters are available for only Sql Server 2005 and later systems\n",br;
	} else {
		#my(%xsubkeys);

		my($rowcnt)=$#rc;
		$rowcnt++;
		print "Retrieved $rowcnt Rows",br if $DEBUG;

		# get the subkeys
		#foreach ( @rc ) {	my( @x )   = dbi_decode_row($_); $xsubkeys{$x[2]}=1; }
		foreach ( @rc ) {	my( @x )   = dbi_decode_row($_); print "ROW=",join(":",@x),br if $DEBUG; }
		#print h2("$category $perf_key Performance Counters for $servername $subkey \n");


		#foreach my $req_key ( sort keys %xsubkeys ) {
		#	print h3("SUB KEY $req_key \n");
			print "<TABLE BORDER=1>";
			print "<TR><TD>".join('</TD><TD>',qw(Category Hour Key1 Key2 Key3 Counter1 Counter2 Counter3 Counter4 Counter5 )),"</TD></TR>";
			my($rcnt)=0;
			foreach ( @rc ) {
				my($category, $dayofweek, $time_bucket, $key_2, $key_3, $key_4,
					$perf_int_1, $perf_int_2, $perf_int_3,
					$perf_int_4, $perf_int_5 ) = dbi_decode_row($_);
				if($rcnt==0 and $category=~/^ZZZ/ ) {
					print b("FATAL ERROR: ".$category);
					last;
				}
				$rcnt++;

		#		next unless $x[2] == $req_key;
				my($hour)  = int($time_bucket / 10);
				my($minute)= ($time_bucket-10*$hour) * 6;
				$minute = "0".$minute if $minute==6 or $minute==0;
				print "<TR><TD>".join('</TD><TD>',($category,$hour.":".$minute,
					$key_2, $key_3, $key_4,
					$perf_int_1, $perf_int_2, $perf_int_3, 	$perf_int_4, $perf_int_5)),"</TD></TR>";
			}
			print "</TABLE>";
		#}
	}
}

#my($q)="select distinct perf_key,key_2,key_3,key_4 from PER_intsummary where system_name='".$servername."' order by perf_key,system_name,key_2,key_3,key_4";
#print $q,p;
#my(@rc) = _querywithresults($q,1);
#
#if( $#rc < 0 ) {
#	print h2("No Performance Counters Available For Server $servername\n");
#	print "Performance counters are only collected for production servers\n",br;
#	print "Most counters are available for only Sql Server 2005 and later systems\n";
#} else {
#
#	#use Data::Dumper;
#	#print Dumper \@rc;
#
#	my($substring,$totalstring,$curcategory)=('','','');
#	my(%found_k2);
#	foreach ( @rc ) {
#		my( @x ) = dbi_decode_row($_);
#		#print join("|",@x),br;
#		if( $curcategory ne $x[0] and $curcategory ne '' ) {
#			# print current header
#			print $totalstring,$substring,p;
#			$totalstring = $substring ='';
#			%found_k2=();
#		}
#
#		$curcategory = $x[0];
#		if( $x[1] eq "Total" or $x[2] eq "Total" ) {
#			if( $x[1] eq "Total" ) {
#				$totalstring= mkurl($x[0]).br;
#			} else {
#				$substring .= ' ---> '.mkurl($x[0],$x[1]).br unless $found_k2{$x[1]};
#				$found_k2{$x[1]} = 1;
#			}
#		} else {
#			#print "NO",br;
#		}
#		# print headers that have Total as second column
#
#	}
#	#if( $curcategory ne $x[0] and $curcategory ne '' ) {
#	#	print $totalstring,br,$substring,p;
#	#}
#	print $totalstring,$substring,p;
#
#
#}

#sub mkurl {
#	my($k1,$k2)=@_;
#	if( $k2 ) {
#		return "<A HREF=performance.pl?servername=$servername&key1=$k1&key2=$k2>$k2</A>"
#	} else {
#		return "<A HREF=performance.pl?servername=$servername&key1=$k1>$k1</A>"
#	}
#}
sub reword {
	my($word)=@_;
	my($rc);
	foreach( split(/[\.\_]/,$word) ) {
		$rc .= ucfirst($_)." ";
	}
	$rc =~ s/^\s+//;
	$rc=~s/\s$//;
	return $rc;
}
