#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2005 By Edward Barlow

use strict;
use File::Basename;
use MlpAlarm;
use DBIFunc;
use Getopt::Std;

sub usage
{
        print @_;
        die "data_mover.pl -Ssrv -Uusr -Ppass -Ddb -Qquery -ssrv -uusr -ppass -ddb -ttbl -Kkey1,key2\n Source may be -FFile -Ilines_to_ignore -TStartString -Cvals -Nnum_fields_min -Rdelimeter [-Ooutfile]\n-C -copy down first vals values if no value -istr1,str2 -> to ignore\n-x for debug mode\n";
}

use vars qw($opt_q $opt_O $opt_x $opt_K $opt_D $opt_U $opt_S $opt_P $opt_Q $opt_d $opt_s $opt_u $opt_p $opt_t $opt_F $opt_I $opt_T $opt_C $opt_N $opt_R $opt_i $opt_q);
die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless getopts('K:U:D:S:P:Q:d:u:p:s:t:F:I:S:T:C:N:R:i:qO:x');

die usage("Bad User Info -U\n") unless defined $opt_U or $opt_F;
die usage("Bad User Info -S\n") unless defined $opt_S or $opt_F;
die usage("Bad User Info -P\n") unless defined $opt_P or $opt_F;

die usage("Bad User Info -u\n") unless defined $opt_u;
die usage("Bad User Info -s\n") unless defined $opt_s;
die usage("Bad User Info -p\n") unless defined $opt_p;

my($DELIMETER)="\'";
my($c1);
if( ! defined $opt_F ) {
	$c1 = dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -connection=>1);
	die "Cant connect to $opt_S as $opt_U \n" unless $c1;
	print "Connected to $opt_S\n";
}

my($c2) = dbi_connect(-srv=>$opt_s,-login=>$opt_u,-password=>$opt_p, -connection=>1);
die "Cant connect to $opt_s as $opt_u \n" unless $c2;
print "Connected to $opt_s\n";

my($insfmt,$delfmt)=GetFormatString($opt_d,$opt_t,$c2);
print "INS: $insfmt\n";
print "DEL: $delfmt\n";

$opt_N-- if defined $opt_N;	# handle perl array refs
my($numblanks)=0;
my($SP_DEBUG)=$opt_x;   #=1;

if( defined $opt_F ) {
	die "Cant read file $opt_F" unless -r $opt_F;
	open(FIL,$opt_F) or die "Cant read $opt_F\n";
	my($rowcount)=0;
	my(@svvals);
	$opt_R="," unless $opt_R;
	while(<FIL>) {
		chomp;
		$rowcount++;
		next if defined $opt_I and $rowcount<=$opt_I;
		my($line)=$_;
		print "INITIAL:",$line,"\n" if defined $SP_DEBUG;
		if( defined $opt_i )  {
			my($found)=0;
			foreach my $pat (split(/,/,$opt_i)) {
				$found++ if $line=~/$pat/;
			}
			next if $found;
		}

		s/"//g;
		if( /^[\s,]+$/ ) { $numblanks++; next; }
		my($parse_string)=$_;
		print "INTERMEDIATE:",$parse_string,"\n" if defined $SP_DEBUG;
		$parse_string = $opt_T.$opt_R.$_ if defined $opt_T;
		print "FINAL:",$parse_string,"\n" if defined $SP_DEBUG;
		my(@x)=split(/$opt_R/,$parse_string);
		print "DBG: FOUND $#x VALUES\n" if defined $SP_DEBUG;
		if ( defined $opt_N and $#x<$opt_N ) {
			print "IGNORING LINE $_ as it only has $#x elements (+1)\n";
			$#svvals=-1;
			next;
		}
		my($j);
		if( defined $opt_C ) {
			my($j)=-1;
			while( ++$j < $opt_C ) {
				$x[$j]=$svvals[$j] if $x[$j] eq "";
			}
			$j=-1;
			while( ++$j < $opt_C ) {
				$svvals[$j]=$x[$j] if $x[$j] ne "";
			}
		}
		grep(s/^\s+//,@x);
		grep(s/\s+$//,@x);
		print "DBG: ",join(",",@x),"\n" if defined $SP_DEBUG;
		my($query)=sprintf($delfmt, @x);
		print "------------\nQUERY: $query\n" unless $opt_q;
		foreach (dbi_query(-db=>$opt_d, -query=>$query, -connection=>$c2)) {
			my(@d)=dbi_decode_row($_);
			die("QUERY $query ERROR: ".join(" ",@d));
		}

		$query=sprintf($insfmt, @x);
		print "------------\n$query\n" unless $opt_q;
		foreach (dbi_query(-db=>$opt_d, -query=>$query, -connection=>$c2)) {
			my(@d)=dbi_decode_row($_);
			die("QUERY $query ERROR: ".join(" ",@d));
		}
	}
	close(FIL);
} else {
	if( $opt_O ) {
		open(OUT, ">$opt_O" ) or die "Cant write to $opt_O $!\n";
	}

	print "------------\nDB=$opt_D QUERY: $opt_Q\n" unless $opt_q;
	foreach (dbi_query(-db=>$opt_D, -query=>$opt_Q, -connection=>$c1)) {
		my(@x);
		foreach ( dbi_decode_row($_) ) {		# need to replace all ' with '' - tried map() but couldnt get it work...
			s/\'/\'\'/g;
			push @x,$_;
		}
		#my(@x)=dbi_decode_row($_);
		print "-------------- DATA --------------\n".join("|",@x)."\n" if $SP_DEBUG;
		if( $opt_O ) {
			my($line)=join(" ",@x);
			die "WOAH: $line contains a tab\n" if $line=~/\t/;
			print OUT join("\t",@x),"\n";
		}
		my($query)=sprintf($delfmt, @x);
		print "------------\n$query\n" unless $opt_q;
		foreach (dbi_query(-db=>$opt_d, -query=>$query, -connection=>$c2)) {
			my(@d)=dbi_decode_row($_);
			die("QUERY $query ERROR: ".join(" ",@d));
		}

		$query=sprintf($insfmt, @x);
		$query=~s/,\s+,/,null,/g;			# this in case a null comes back where an int should be
		$query=~s/,\s+\)/,null\)/g;		# this in case a null comes back in last position where an int should be
		print "------------\n$query\n" unless $opt_q;
		foreach (dbi_query(-db=>$opt_d, -query=>$query, -connection=>$c2)) {
			my(@d)=dbi_decode_row($_);
			die("QUERY $query ERROR: ".join(" ",@d));
		}
	}
	close(OUT) if $opt_O;
}
print "Ignored $numblanks blank lines" if $numblanks>0;
dbi_disconnect(-connection=>$c1) unless defined $opt_F;
dbi_disconnect(-connection=>$c2);

# BUILD STRING FOR INSERT
#     helpcolumn returns ordered list of column names & types
#     we want a string that looks like
#     INSERT $tbl ( $field_name_list ) VALUES ( $format_str )
sub GetFormatString {
   my($db,$tbl,$connection)=@_;

	my(%keylist);
	foreach ( split(/,/,$opt_K) ) {
		$keylist{$_}=1;
	};

   my($ins_header) =  "INSERT $tbl (\n\t";
   my($del_string) =  "DELETE $tbl WHERE \n";
   # $ins_header =~ s/r_//;
   # $del_string =~ s/r_//;
   my($hdr_cnt)=0;      # used for indenting
   my($format_str)="";
	print "------------\nexec sp__helpcolumn $tbl\n";
   foreach ( dbi_query( -db=>$db,
            -query=>"exec sp__helpcolumn \@objname=$tbl, \@dont_format='Y'",
				-connection=>$connection) ) {
      $hdr_cnt++;
      #next unless $hdr_cnt>3;
      #s/^\s//g;
      my(@rc)=dbi_decode_row($_);
		print "DIAG COL: ",join("|",@rc),"\n";
      $rc[0]=~s/\s//g;
      $ins_header.=$rc[0].",\t";
      $ins_header.="\n\t" if $hdr_cnt%3==0;
      $del_string .= " -- " if defined $opt_K and ! defined $keylist{$rc[0]};
      $del_string .= " $rc[0] = ";
      if(     $rc[1] !~ /int/
           and $rc[1] !~ /float/
           and $rc[1] !~ /decimal/
           and $rc[1] !~ /numeric/
           and $rc[1] !~ /money/
           and $rc[1] !~ /double/ ) {
         $format_str .= $DELIMETER."%s".$DELIMETER;
         $del_string .= $DELIMETER."%s".$DELIMETER;
      } else {
         $format_str .= "%s";
         $del_string .= "%s";
      }
      if( $rc[2] eq "1" ) {
      	$ins_header="SET IDENTITY_INSERT $tbl ON\n".$ins_header;
      }
      $del_string   .= " and \n";
      $format_str   .= ",\t" unless $hdr_cnt == 0;
      $format_str   .= "\n\t" if $hdr_cnt%3==0;
   };

   $ins_header    =~ s/\n\t$//;
   $ins_header    =~ s/,\t$//;
   $format_str    =~ s/\n\t$//;
   $format_str    =~ s/,\t$//;
   $del_string    .= " 1=1";
   #$del_string    =~ s/ and \n$//;
   return ( $ins_header."\n\t) VALUES (\n\t$format_str )\n",
            $del_string );
}
__END__

=head1 NAME

data_mover.pl - move data around!

=head2 USAGE

data_mover.pl -Ssrv -Uusr -Ppass -Ddb -Qquery -ssrv -uusr -ppass -ddb -ttbl -Kkey1,key2

-O Outfile

 Source may be -FFile 	or -U/-S/-P/-D and -Q
 Target is -u/-s/-p/-d
 Target Table is -t

-K is the keys for the rows

-Ilines_to_ignore

-I1 would ignore the first line

-I4 would ignore lines 1 to 4

-TStartString

-Cvals

-Nnum_fields_min

-Rdelimeter	This defaults to a comma but you can pass whatever you want

-C -copy down first vals values if no value -istr1,str2 -> to ignore

=head2 SYNOPSIS

 Either pass -U/-P/-S or a -Ffile
 Must pass -u/-p/-s of the target

-t TABLE is the table you want to copy into or to copy from one server to another

=cut

