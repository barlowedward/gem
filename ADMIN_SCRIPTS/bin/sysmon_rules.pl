#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# perl script that analyzes output for errors
#
# GET SOME SUMMARY INFO
#
print "sp_sysmon analyzer\n\nThis analysis report was prepared by comparing sp_sysmon output to a sample rules base.  It in no way makes it unnecessary to understand the actual sp_sysmon output report.  It is intended as a tool to make sp_sysmon analysis easier.  \n\n";

section_hdr(1,"sp_sysmon Summary");
msgln("  Sybase Version: $srvversion") if defined $srvversion;

my($minbusy,$maxbusy,$sumbusy,$numengines)=(100,0,0,0);
my(@data)=get_lines("Kernel Utilization","Engine Busy Utilization");
foreach (@data) {
        next if /Utilization/;
        my(@d)=split /\s+/;
        my $busy = $d[3] | $d[2];
        $minbusy = $busy if $minbusy > $busy;
        $maxbusy = $busy if $maxbusy < $busy;
        $sumbusy += $busy;
        $numengines++;
}
$numengines=1 if $numengines==0;
my($avgbusy)= $sumbusy / $numengines;
my($dp,$i);

table();
row( bold("Run at"), "$rundate $runtime");
row( bold("sp_sysmon Interval"  ) ,$interval." min" );
row( bold( "Number of CPUs Found"), $numengines);
row( bold( "Average CPU Busy" ), $avgbusy);

$dp= Data("Procedure Cache Management","Procedure Requests",undef,3,1);
row( bold("# Of Stored Procedures Run"), $dp);

row( bold("Num Committed Xactions"), Data("Transaction Profile","Transaction Summary","Committed Xacts",3,1));
row( bold("Num Committed Xactions Per Sec"), Data("Transaction Profile","Transaction Summary","Committed Xacts",1,1));

$dp= Data("Lock Management","Total Lock Requests",undef,3,0);
row( bold("Number Of Locks"), $dp);

$dp= Data("Lock Management","Avg Lock Contention",undef,3,0);
row( bold("Lock Contention"), $dp);

$dp= Data("Lock Management","Deadlock Percentage",undef,3,0);
row( bold("Number Of Deadlocks"), $dp);

endtable();

$s="Kernel Utilization";
section_hdr(2,$s);

# Load average of engines
warning("CPU is very lightly loaded (Avg Busy=$avgbusy) - Numbers from this run may be misleading") if $avgbusy < 5;
warning("CPU is lightly loaded (Avg Busy=$avgbusy)") if defined $avgbusy and $avgbusy >= 5 and $avgbusy < 10;
warning("CPU is heavily loaded (Avg Busy=$avgbusy)") if defined $avgbusy and $avgbusy > 60 and $avgbusy <= 95;
warning("CPU is over loaded (Avg Busy=$avgbusy) - You might benefit from another engine") if defined $avgbusy and $avgbusy > 95 and $avgbusy <= 99;
warning("CPU running flat out (Avg Busy=$avgbusy) - You might benefit from another cpu")  if defined $avgbusy and $avgbusy > 99;

# See if Engines are balanced
$dp=$maxbusy-$minbusy;
error("Uneven Load Balancing Between Engines (spread btwn hi and lo was $dp %)")  if $dp > 40;

$dp=Data($s,"Network Checks","Non-Blocking",4,1);
advice("High Blocking Network Checks (".(100-$dp)." %)") if defined $dp and $dp<5;

$dp=Data($s,"Avg Net I/Os per Check",undef,3,0);
if( defined $dp ) {
        advice("Avg Net I/Os per Check shows almost no activity - ignore i/o numbers") if $dp == 0;
        advice("Avg Net I/Os per Check shows little activity when checked ($dp % of the time) ") if $dp < .01 and $dp > 0;
        advice("Avg Net I/Os per Check shows significant network activity ($dp % of the time).  You might gain by increasing packet size.") if $dp > 1;
}

$dp=Data($s,"Disk I/O Checks","Checks Returning I/O",4,1);
advice("Disk I/O Checks show very few hits ($dp %) ") if defined $dp and $dp < .01;
advice("Disk I/O Checks show many hits ($dp %). IO intensive work done or disk subsystem is too slow.") if defined $dp and $dp > 1;

$s="Worker Process Management";
section_hdr(2,$s);

$s="Parallel Query Management";
section_hdr(2,$s);

table();
data_row($s,"Total Parallel Queries");
#$dp= Data($s,"Total Parallel Queries",undef,3,0);
#row( bold("Number of Parallel Queries"), $dp);
endtable();

$s="Task Management";
section_hdr(2,$s);

$dp=Data($s,"Connections Opened",undef,1,1);
row( bold( "Connections Opened / Second"), $dp );

warning( "Connections Opened/Sec=$dp (excessive?)" )  if defined $dp and $dp>2;
error( "Connections Opened/Sec=$dp" ) if defined $dp and $dp > 10;
warning( "Connections Opened=$dp Nobody Connected During Period" ) if defined $dp and $dp == 0;

$i="Task Context Switches Due To";
$dp=Data($s,$i,"Voluntary Yields",4,1);
advice( "Excessive Context Switching Due To Voluntary Yields ($dp %) - increase time slice via sp_configure" ) if defined $dp and $dp > 90;

$dp=Data($s,$i,"Cache Search Misses",4,1);
advice( "Excessive Context Switching Due To Cache Search Misses ($dp %) - see cache details - add memory or change cache information." ) if defined $dp and $dp > 15;

$dp=Data($s,$i,"System Disk Writes",4,1);
advice( "Excessive Context Switching Due To System Disk Writing ($dp %) - check page splits" ) if defined $dp and $dp > 15;

$dp=Data($s,$i,"I/O Pacing",4,1);
advice( "Excessive Context Switching Due To I/O Pacing ($dp %) - See Documentation if you have high throughput environment with a large data cache" ) if defined $dp and $dp > 15;

$dp=Data($s,$i,"Logical Lock Contention",4,1);
advice( "Excessive Context Switching Due To Logical (Regular) Locking ($dp %) - Check Lock Section" ) if defined $dp and $dp > 15;

$dp=Data($s,$i,"Address Lock Contention",4,1);
advice( "Excessive Context Switching Due To Address Locking ($dp %) - No suggestion" ) if defined $dp and $dp > 15;

$dp=Data($s,$i,"Log Semaphore Contention",4,1);
advice( "Excessive Context Switching Due To Log Semaphore Contention ($dp %) - Increase user log cache (ULC) size and check disk queuing on the disk used by the transaction log" ) if defined $dp and $dp > 10;

$dp=Data($s,$i,"Group Commit Sleeps",4,1);
advice( "Excessive Context Switching Due To Group Commit Sleeps ($dp %) - If Log io size (via sp_configure) is > 2K, you may reduce it unless you have a high throughput environment. This is otherwise not a problem " ) if defined $dp and $dp > 10;

$dp=Data($s,$i,"Last Log Page Writes",4,1);
advice( "Excessive Context Switching Due To Last Log Page Writes ($dp %) - Check Avg # writes per log page (in Xact Mgmt Section) to see if server is repeatedly rewriting same page or  reduce the log io size via sp_configure if > 2K" ) if defined $dp and $dp > 10;

$dp=Data($s,$i,"Modify Conflicts",4,1);
advice( "Excessive Context Switching Due To Modify Conflicts($dp %) - This can occur due to excessive dirty reads or access to system tables" ) if defined $dp and $dp > 10;

$dp=Data($s,$i,"I/O Device Contention",4,1);
advice( "Excessive Context Switching Due To I/O Device Contention ($dp %) - Spread your data across disks better or reduce physical I/O by adding memory." ) if defined $dp and $dp > 10;

my($abspp)=Data("Network I/O Management","Avg Bytes Sent per Packet",undef,3,0);
my($abrpp)=Data("Network I/O Management","Total Bytes Rec'd","Avg Bytes Rec'd per Packet",3,0);

$dp=Data($s,$i,"Network Packet Received",4,1);
if( ! defined $abrpp  or $abrpp>150 )  {
        advice( "Excessive Context Switching Due To Network Packets Recieved ($dp %) - Increase the network packet size (size=$abrpp)" ) if defined $dp and $dp > 10;
} else {
        advice( "Excessive Context Switching Due To Network Packets Recieved ($dp %) - Small Avg Sized Packets Found so place more statements in your batch queries" ) if defined $dp and $dp > 10;
}

$dp=Data($s,$i,"Network Packet Sent",4,1);
if( ! defined $abspp or $abspp>150 )  {
        advice( "Excessive Context Switching Due To Network Packets Sent ($dp %) - Increase the network packet size (size=$abspp)" ) if defined $dp and $dp > 10;
} else {
        advice( "Excessive Context Switching Due To Network Packets Sent ($dp %) - Small Avg Sized Packets Found" ) if defined $dp and $dp > 10;
}

$dp=Data($s,$i,"SYSINDEXES Lookup",4,1);
advice( "Excessive Context Switching Due To Sysindexes Lookup ($dp %)" ) if defined $dp and $dp > 10;

$dp=Data($s,$i,"Other Causes",4,1);
advice( "Other Causes: Inadequate Context Switching Due To Other Causes ($dp %) - This number should be high in a well-tuned system." ) if defined $dp and $dp < 20;

$s="Application Management";
section_hdr(2,$s);

$s="ESP Management";
section_hdr(2,$s);

$s="Monitor Access to Executing SQL";
section_hdr(2,$s);

$s="Transaction Profile";
section_hdr(2,$s);

table();
row( bold("Transaction Type"), bold("Number"), bold("Notes"));

$dp=Data("Transaction Profile","Transaction Detail","Heap Table",3,0);
row( bold("Inserts into Heap Tables"), $dp, "Can contend w/each other");

$dp=Data("Transaction Profile","Transaction Detail","Heap Table",4,0);
advice("Heap Insert percentage is high...  If not into temp tables, a clustered index would help if the inserts are multi-threaded\n") if $dp>65;

$dp=Data("Transaction Profile","Transaction Detail","Clustered Table",3,0);
row( bold( "Inserts into Tables W/Clustered Index "), $dp );

$dp=Data("Transaction Profile","Transaction Detail","Deferred",3,0);
row( bold("Updates (Deferred) "), $dp , "Slowest update type" );

$dp=Data("Transaction Profile","Transaction Detail","Direct In-place",3,0);
row( bold("Updates (In Place) "), $dp , "Best update type" );

$dp=Data("Transaction Profile","Transaction Detail","Direct Cheap",3,0);
row( bold("Updates (Direct Cheap) "), $dp , "Good update type" );

$dp=Data("Transaction Profile","Transaction Detail","Direct Expensive",3,0);
row( bold("  Updates (Direct Expensive)") , $dp );

$dp=Data("Transaction Profile","Transaction Detail","Deferred",3,0);
row( bold("  Deletes (Deferred) "), $dp ) ;

$dp=Data("Transaction Profile","Transaction Detail","Direct",3,0);
row( bold("  Deletes (Direct) "), $dp ) ;
endtable();

$s="Transaction Management";
section_hdr(2,$s);
#s_print_section($s);

my($found_ulc_err)=0;
$dp= Data($s,"ULC Flushes to Xact Log","by Full ULC",4,1);
if( $dp > 20 ) {
        $found_ulc_err=1;
        $dp= Data($s,"Max ULC Size",undef,3,0);
        if( defined $dp ) {
                if( $dp > 2048 ) {
                        advice("High Flushes by Full ULC: Increase the user log cache size via sp_configure to at least $dp to reduce this.");
                } else {
                        advice("High Flushes by Full ULC: max used ULC=$dp - this is a very confusing result.  Check size of ULC (sp_configure).  It should be > 2K.");
                }
        }
} else {
        $dp= Data($s,"Max ULC Size",undef,3,0);
        if( defined $dp ) {
                advice("Max ULC Size: Check user log cache size via sp_configure => it should be 2048.") if $dp <= 2048;
                advice("Max ULC Size: Check user log cache size via sp_configure => it should be > $dp.") if $dp > 2048;
        }
}

$dp= Data($s,"ULC Flushes to Xact Log","by Change of Database",4,1);
advice("Flushes by Change of Database: Many multi database joins. If user log cache (ULC) > 2K consider reducing it via sp_configure") if defined $dp and $dp > 20;
$found_ulc_err=1 if defined $dp and $dp > 20;

$dp= Data($s,"ULC Flushes to Xact Log","by System Log Record",4,1);
advice("Many Flushes by System Log Record: If user log cache (ULC) > 2K consider reducing it via sp_configure") if defined $dp and $dp > 20;
$found_ulc_err=1 if defined $dp and $dp > 20;

$dp= Data($s,"ULC Flushes to Xact Log","by Other",4,1);
advice("Flushes by Other: If user log cache (ULC) > 2K consider reducing it via sp_configure") if defined $dp and $dp > 20;
$found_ulc_err=1 if defined $dp and $dp > 20;

$dp= Data($s,"ULC Flushes to Xact Log","by End Transaction",4,1);
advice("Flushes by End Transaction: You should have a high rate if you have short simple transactions") if $found_ulc_err==0 and defined $dp and $dp < 90;

$dp= Data($s,"ULC Semaphore Requests","Waited",4,1);
advice("ULC Semaphore Requests - Waited = $dp %: please investigate") if defined $dp and $dp > 0;
$dp= Data($s,"Log Semaphore Requests","Waited",4,1);
advice("Log Semaphore Requests - Waited = $dp %: please investigate") if defined $dp and $dp > 0;
$dp= Data($s,"Avg # Writes per Log Page",undef,3,0);
advice("Avg # Writes per log page ($dp) should be about 1 in high throughput environments - otherwise it does not matter.") if defined $dp;

$s="Index Management";
section_hdr(2,$s);

# if page splits offer to rebuild indexes
$dp= Data($s,"Page Splits",undef,1,0);
advice("Page Splits/Sec: Significant ($dp) Page Splits found - consider using fillfactor or decreasing max_rows_per_page") if defined $dp and $dp > 1;

$dp= Data($s,"Page Splits","Retries",3,0);
advice("$dp Page Split Retries Found - investigate") if defined $dp and $dp > 0;

$dp= Data($s,"Page Splits","Deadlocks",3,0);
advice("$dp Page Split Deadlocks Found - investigate") if defined $dp and $dp > 0;

# if page shrinks > 2 offer to rebuild indexes
$dp= Data($s,"Page Shrinks",undef,1,0);
advice("Significant page shrinks/second found ($dp) - consider rebuilding indexes") if defined $dp and $dp > 3;

table();

$dp= Data($s,"Index Scans","Ascending Scans",3,0);
row("Ascending Index Scans",$dp) if defined $dp and $dp > 0;

$dp= Data($s,"Index Scans","Descending Scans",3,0);
row("Descending Index Scans",$dp) if defined $dp and $dp > 0;

$dp=Data($s,"Page Splits","Page Splits",3,0);
row("Page Splits", $dp) if defined $dp and $dp>0;

endtable();

$s="Metadata Cache Management";
section_hdr(2,$s);

$dp= Data($s,"Open Object Usage","Failed",3,0);
advice("$dp Open Object Usage - Failed") if defined $dp and $dp > 0;

$dp= Data($s,"Open Index Usage","Failed",3,0);
advice("$dp Open Index Usage - Failed") if defined $dp and $dp > 0;

$dp= Data($s,"Open Database Usage","Failed",1,0);
advice("Open Database Usage - Failed Reuse Requests ($dp) is > 0 - increase num open objects/indexes/databases") if defined $dp and $dp > 0;

$s="Lock Management";
section_hdr(2,$s);

$dp= Data($s,"Avg Lock Contention",undef,4,0);
error("Avg Lock Contention: $dp % of locks") if defined $dp and $dp>=3;
warning("Avg Lock Contention: $dp % of locks") if defined $dp and $dp > 0 and $dp<3;

$dp= Data($s,"Deadlock Percentage",undef,3,0);
error("Deadlock Percentage: $dp Pct") if defined $dp and $dp > 0;

$dp= Data($s,"Total EX-Table Requests",undef,4,0);
advice("High EX-Table Requests - $dp % of Locks") if defined $dp and $dp > 4;

# DEADLOCK STUFF
# some searches but some are finding locks
if( Data($s,"Deadlock Detection","Deadlock Searches",3,0) > 10
and Data($s,"Deadlock Detection","Avg Deadlocks per Search",4,0) > .05) {
        advice("Deadlock Searches: Checking deadlocks too frequently: decrease deadlock checking period slightly");
}

# lot skipped but few found
if( Data($s,"Deadlock Detection","Searches Skipped",3,0) > 20
and Data($s,"Deadlock Detection","Avg Deadlocks per Search",4,0) < .02 ){
        advice("Avg deadlocks per search: low value, increase deadlock checking period");
}

table();
$dp= Data($s,"Lock Promotions","Total Lock Promotions",3,0);
row( bold("Total Lock Promotions"), $dp);

$dp= Data($s,"Total Lock Requests",undef,3,0);
row( bold("Number Of Locks"), $dp);

$dp= Data($s,"Avg Lock Contention",undef,3,0);
row( bold("Lock Contention"), $dp) ;

$dp= Data($s,"Deadlock Percentage",undef,3,0);
row( bold("Number Of Deadlocks"), $dp);

endtable();

# TYPEs of locks
table();
row( bold("Lock Type"), bold("Number of Locks"), bold("# Waited"));
row( bold("Exclusive Table"), Data($s,"Total EX-Table Requests",undef,3,0), Data($s,"Exclusive Table","Waited",3,0));
row( bold("Shared Table"), Data($s,"Total SH-Table Requests",undef,3,0), Data($s,"Shared Table","Waited",3,0));
row( bold("Exclusive Intent"), Data($s,"Total EX-Intent Requests",undef,3,0), Data($s,"Exclusive Intent","Waited",3,0));
row( bold("Shared Intent"), Data($s,"Total SH-Intent Requests",undef,3,0), Data($s,"Shared Intent","Waited",3,0));
row( bold("Exclusive Page"), Data($s,"Total EX-Page Requests",undef,3,0), Data($s,"Exclusive Page","Waited",3,0));
row( bold("Update Page"), Data($s,"Total UP-Page Requests",undef,3,0), Data($s,"Update Page","Waited",3,0));
row( bold("Shared Page"), Data($s,"Total SH-Page Requests",undef,3,0), Data($s,"Shared Page","Waited",3,0));
row( bold("Exclusive Address"), Data($s,"Total EX-Address Requests",undef,3,0), Data($s,"Exclusive Address","Waited",3,0));
row( bold("Shared Address"), Data($s,"Total SH-Address Requests",undef,3,0), Data($s,"Shared Address","Waited",3,0));
row( bold("Last Page Locks on Heaps"), Data($s,"Total Last Pg Locks",undef,3,0), Data($s,"Last Page Locks on Heaps","Waited",3,0));
endtable();

$s="Data Cache Management";
section_hdr(2,$s);

table();
row( bold("Total Cache Misses"), Data($s,"Cache Statistics Summary All Caches","Total Cache Misses",3,0));
row( bold("Cache Misses Percent"), Data($s,"Cache Statistics Summary All Caches","Total Cache Misses",4,0));
endtable();
# tests for data cache mgt
#  - total misses > 20% and total searches > 10000 - need more cache
#  -  "    "      " 10                 "   >  5000 - missing frequently
#  - buffers grabbed dirty > 0 is serious problem
#  - large io denied is hi (> 20) & doing large then investigate

#  - large io effectiventess should be > 50 if it exists at all
#  - report any spinlock contention  in smp
#  - report cache miss percentage
#  - report grabbed dirty results for any buffer pools

$i="Cache Statistics Summary (All Caches)";
msgln("\nFor cache problems use sp_cacheconfig and sp_helpcache");

advice("Very High % Cache Misses: You Need More Data Cache")
        if   Data($s,$i,"Total Cache Misses",4,1)>20
        and  Data($s,$i,"Total Cache Hits",3,1)>5000;
advice("High % Cache Misses. You Might Want More Data Cache")
        if   Data($s,$i,"Total Cache Misses",4,1)>10
        and  Data($s,$i,"Total Cache Hits",3,1)>5000;
error("Found Buffers Grabbed Dirty > 0")
        if Data($s,$i,"Buffers Grabbed Dirty",4,0) > 0 ;
warning("Investigate Large I/O Denied Ratios")
        if Data($s,$i,"Large I/O Denied",4,0) > 20
        and Data($s,$i,"Large I/O Performed",3,0) > 100;
$dp = Data($s,$i,"Pages by Lrg I/O Cached",4,0);
warning("Pages by Lrg I/O Cached Should Be >50 %: Should Investigate")
        if  defined $dp
        and Data($s,$i,"Pages by Lrg I/O Cached",3,0) > 0
        and $dp < 50 ;

$i="default data cache";
warning( "Spinlock Contention Found" )
        if Data($s,$i,"Spinlock Contention",4,0) > 0;

$s="Procedure Cache Management";
section_hdr(2,$s);
$dp= Data($s,"Procedure Requests",undef,3,1);
msgln("$dp Stored Procedures Were Executed") if defined $dp;
if( $dp > 0 ) {
        $dp= Data($s,"Procedure Reads from Disk",undef,4,0);
        error("Read Procedures From Disks $dp pct of time - procedure cache is too small or system not at steady state.") if $dp > 20;

        $dp= Data($s,"Procedure Writes to Disk",undef,4,0);
        msgln("Procedure Writes to Disk - $dp Procedures Created In Time Interval") if $dp > 0;

}

$dp= Data($s,"Procedure Reads from Disk",undef,3,1);
msgln("$dp Stored Procedures Were Read From Disk (not in procedure cache)") if defined $dp;

$s="Memory Management";
section_hdr(2,$s);
table();
$dp= Data($s,"Pages Allocated",undef,3,1);
row("Allocated Pages of Memory",$dp);
$dp= Data($s,"Pages Released",undef,3,1);
row("Released Pages of Memory",$dp);
endtable();

$s="Recovery Management";
section_hdr(2,$s);
$dp= Data($s,"Checkpoints","# of Free Checkpoints",3,1);
advice("# of Free Checkpoints: You may increase recovery interval to improve performance, and may reduce housekeeper free write percent") if $dp > 8*$sysmon_interval;
if( $dp < 10 ) {
        $dp= Data($s,"Avg Time per Normal Chkpt",undef,1,0);
        advice("Avg Time per Normal Chkpt: Try increasing batch size see sp_sysmon manual for details.") if $dp > .1;
}

$dp= Data($s,"Checkpoints","# of Normal Checkpoints",3,1);
advice("# of Normal Checkpoints: Increase # checkpoints performed by housekeeper task") if $dp > 8*$sysmon_interval;
$dp= Data($s,"Avg Time per Free Chkpt",undef,1,0);

$s="Disk I/O Management";
#print format_section($s);

section_hdr(2,$s);

$dp= Data($s,"I/Os Delayed by","Disk I/O Structures",3,1);
warning("I/Os Delayed by Disk I/O Structures - increase sp_configure \"disk i/o structures\"") if defined $dp and $dp > 0;

$dp= Data($s,"I/Os Delayed by","Server Config Limit",3,1);
warning("I/Os Delayed by Server Config Limit - increase sp_configure \"max async i/os per server\"") if defined $dp and $dp > 0;

$dp= Data($s,"I/Os Delayed by","Engine Config Limit",3,1);
warning("I/Os Delayed by Engine Config Limit - increase sp_configure \"max async i/os per engine\"") if defined $dp and $dp > 0;

$dp= Data($s,"I/Os Delayed by","Operating System Limit",3,1);
warning("I/Os Delayed by Operating System Limit - check with your administrator") if defined $dp and $dp > 0;

my($tr,$tc);
$tr= Data($s,"Total Requested Disk I/Os",undef,3,1);
$tc= Data($s,"Total Completed I/Os",undef,3,1);
error("There seems to be an I/O Bottleneck and significant I/O is incomplete")
        if( $tr+$tc>0 and (($tr-$tc) / ($tr+$tc)>5.0));

# print join("\n",get_items_in_section($s));

# get & print disk activity disk reads writes
table();
row( "Disk", "Reads/Sec", "Writes/Sec" );
my(@disk_items)=get_items_in_section($s);
my($total_rd,$total_wr)=(0,0);
my(%busy_rd,%busy_wr);
foreach (sort @disk_items) {
        next unless /^Device:/;
        #next if /Delayed by/ or /Total/ or /Completed/ or /Max Outstanding/ or /Device/i;
        #row( bold("$_ "));

        my(@keys)=get_item_keys($s,$_);

        if( $#keys<0 ) {
                row( bold($_), "--", "--" );
                next;
        }

        my($rd)= Data($s,$_,"APF",1,0) + Data($s,$_,"Non-APF",1,0);
        $busy_rd{$_}=$rd if defined $rd;
        $total_rd+=$rd if defined $rd;
        $rd= "--" unless defined $rd;
        my($wr)= Data($s,$_,"Writes",1,0);
        $total_wr+=$wr if defined $wr;
        $busy_wr{$_}=$wr if defined $wr;
        $wr= "--" unless defined $wr;

        row( bold($_), $rd, $wr );
}
row( bold("TOTAL"), $total_rd, $total_wr );
endtable();

if( $total_rd > 30 ) {
        foreach (keys %busy_rd) {
                advice(int(($busy_rd{$_}*100)/$total_rd)."% of Total Reads on $_")
                        if $busy_rd{$_} > .15 * $total_rd;
        }
}

if( $total_wr > 20 ) {
        foreach (keys %busy_wr) {
                advice(int(($busy_wr{$_}*100)/$total_wr)."% of Total Writes on $_")
                        if $busy_wr{$_} > .15 * $total_wr;
        }
}

$s="Network I/O Management";
section_hdr(2,$s);

table();
$dp= Data($s,"Total Network I/O Requests","Total Network I/O Requests",3,0);
row( "Num Network Requests ",  $dp );

$dp= Data($s,"Total Network I/O Requests","Network I/Os Delayed",4,0);
row( "% Network Delayed Packets", $dp );

$dp= Data($s,"Total TDS Packets Rec'd",undef,3,0);
row( "Number of TDS Packets Recieved ", $dp );

$dp= Data($s,"Total Bytes Rec'd","Avg Bytes Rec'd per Packet",3,0);
row( "Avg Bytes Rec'd Per Packet", $dp );

$dp= Data($s,"Total TDS Packets Sent",undef,3,0);
row( "Number of TDS Packets Sent ", $dp );

$dp= Data($s,"Total Bytes Sent",undef,3,0);
row( "Total Bytes Sent ", $dp );

$dp= Data($s,"Avg Bytes Sent per Packet",undef,3,0);
row( "Avg Bytes Sent Per Packet", $dp  );
endtable();
1;
