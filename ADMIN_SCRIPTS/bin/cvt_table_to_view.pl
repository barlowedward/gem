#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "cvt_table_to_view.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST
						object object ...";

        return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_U $opt_S $opt_P $opt_D);
die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('U:D:S:P:');

# cd to the right directory
use File::Basename;
use Sybase::SybFunc;

db_connect($opt_S,$opt_U,$opt_P) or die "Cant connect to $opt_S as $opt_U\n";

foreach (@ARGV) {
	my(@cols)= db_query($opt_D,"select name from syscolumns where id=object_id(\"$_\") order by colid");
	print "if exists (
  select * from sysobjects where type='V' and name='".$_."'
)
begin
  drop view $_
end
go
create view $_ as select\n\t",join(",\n\t",@cols),"\nfrom $opt_D..$_\ngo\n";
	my(@prots)= db_query($opt_D,"exec sp__helprotect \"$_\"");
	foreach (@prots) { print $_; }
}

db_close_sybase();

__END__

=head1 NAME

cvt_table_to_view.pl - make a view on a table

=head2 DESCRIPTION

Simple utility to create byname views on tables

=cut
