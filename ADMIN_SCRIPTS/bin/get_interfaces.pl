#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# generic cd to the right directory
use File::Basename;
my($curdir)=dirname($0);
chdir $curdir or die "ERROR: Cant cd to $curdir: $!\n";

use strict;
# use Sybase::SybFunc;
use DBIFunc;
use Getopt::Std;
use vars qw($opt_i $opt_c);

getopts("i:c:") or die "usage: $0 -i filename -c:column order (eg. -c321)";
$opt_c="123" unless defined $opt_c;
my(@colorder)=split //,$opt_c;

die "ERROR: SYBASE ENV not defined" unless defined $ENV{"SYBASE"};

foreach ( dbi_get_interfaces($ARGV[0],$opt_i) ) {
        printf("%-30s %-30s %-10s\n", $_->[$colorder[0]-1],$_->[$colorder[1]-1],$_->[$colorder[2]-1])
}

__END__

=head1 NAME

get_interfaces.pl - print interfaces file utility

=head2 DESCRIPTION

Pretty Interfaces file entries.  Works on both unix and nt.  On NT it reads $SYBASE/sql/ini.  On unix it reads $SYBASE/interfaces.  If you pass an argument it only shows server with that string in it.

=head2 USAGE

get_interfaces.pl [search_argument] [-i interfacesfile ]

-c - reformat column order or it will go 1 2 3

=head2 SAMPLE OUTPUT

 BARLONTSERVER                  barlontserver                  5000
 BARLONTSERVER_BS               barlontserver                  5001
 BARLONTSERVER_HS               barlontserver                  5003
 BARLONTSERVER_MS               barlontserver                  5002
 BARLONTSERVER_XP               barlontserver                  5004

=cut

