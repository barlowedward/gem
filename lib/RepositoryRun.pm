# copyright (c) 2005-2008 by SQL Technologies.  All Rights Reserved.
# All Rights Reserved
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.
#

package 		RepositoryRun;

use	     	Exporter;
use	     	Carp qw/cluck confess croak/;
use 			DBIFunc;
use			Repository;
use 			MlpAlarm;
use 			Sys::Hostname;

@ISA	    	= qw(Exporter);
@EXPORT	 	= qw(wr_debug wr_console wr_report repository_parse_and_run );

use		strict;
my($DEBUG,$NOSTDOUT,$OUTFILE);

# debug, console=formatstuff, outfile=report
sub wr_debug 	{ 	print "[RepositoryRun] ",@_ if defined $DEBUG;   }
sub wr_console {	print @_ unless $NOSTDOUT;}
sub wr_report 	{ 	print OUT @_ if $OUTFILE; print @_ unless $OUTFILE;	}
	
sub repository_parse_and_run {
	my(%args)=@_;
	$DEBUG	=	$args{DEBUG};
	$NOSTDOUT=	$args{NOSTDOUT};
	$OUTFILE	=	$args{OUTFILE};
	$args{AGENT_TYPE} = 'Gem Monitor' unless $args{AGENT_TYPE};
	
	die "Must pass --SERVER or --DOALL\n"	unless defined $args{SERVER} or $args{DOALL};
	$args{DOALL}=lc($args{DOALL})	if defined $args{DOALL} and $args{DOALL} ne "sqlsvr" and $args{DOALL} ne "sybase";
	die( "--DOALL must be sqlsvr or sybase or oracle or mysql or unix or win32servers\n" )
		if defined $args{DOALL} and $args{DOALL} ne "sqlsvr" and $args{DOALL} ne "sybase" and $args{DOALL} ne "oracle"
				and  $args{DOALL} ne "mysql" and $args{DOALL} ne "unix" and $args{DOALL} ne "win32servers";
	die( "Must Pass --DOALL or --SERVER\n" )
		if defined $args{SERVER} and $args{SERVER} =~ /^\s*$/;
	
	if( defined $args{OUTFILE} ) {
		unlink($args{OUTFILE});
		open(OUT,">> $args{OUTFILE}") or die("Cant Write to $args{OUTFILE}: $!");
	}
		
	# wr_console( $args{PROGRAM_NAME}.".pl v1.0\n" );
	# wr_console( "Run at ".localtime(time)."<br>\n" );
	&{$args{init}}() if $args{init};
	
	if( defined $DEBUG ) {
		wr_debug( "Called in Debug Mode\n");
		foreach (keys %args) { 
			wr_debug( "$_ \t = $args{$_} \n") if defined $args{$_}; 
		}
	}
	
	die "Cant pass --DOALL and --SERVER - that makes no sense\n" if defined $args{DOALL} and defined $args{SERVER};
	
	my(@server_list);
	if( defined $args{DOALL} ) {		
	  MlpBatchJobStart(-BATCH_ID=>$args{BATCH_ID},-AGENT_TYPE=>$args{AGENT_TYPE},-SUBKEY=>$args{DOALL} ) if $args{BATCH_ID};
	  if( $args{PRODUCTION} ) {
	 		push @server_list, get_password(-type=>$args{DOALL},-PRODUCTION=>1);
	 	} elsif( $args{NOPRODUCTION} ) {
	 		push @server_list, get_password(-type=>$args{DOALL},-NOPRODUCTION=>1);
	 	} else {
			push @server_list, get_password(-type=>$args{DOALL});
		}
	} elsif( defined $args{SERVER} ) {
		MlpBatchJobStart(-BATCH_ID=>$args{BATCH_ID},-AGENT_TYPE=>$args{AGENT_TYPE},-SUBKEY=>$args{SERVER} ) if $args{BATCH_ID};
	  push @server_list, split(/[,\|]/,$args{SERVER});
	}
	
	foreach my $cursvr ( sort @server_list ) {
		wr_console("+++++++++++++++++++++++++++++++++++++++++\n");
		wr_console("+ repository_parse_and_run on $cursvr at ".localtime(time)."\n");
		wr_console("+++++++++++++++++++++++++++++++++++++++++\n");
		
	   wr_debug("Fetching Password Info","\n");
	   my($svrtype);
	   if( defined $args{DOALL} ) {
	   	$svrtype=$args{DOALL};
	      ($args{USER},$args{PASSWORD})=get_password(-type=>$args{DOALL}, -name=>$cursvr);
	   } else {
	   	
	      ($args{USER},$args{PASSWORD})=get_password(-name=>$cursvr, -type=>'sybase')
	      	if ! defined $args{USER} or ! defined $args{PASSWORD};
	      $svrtype='sybase' if $args{USER};
	      ($args{USER},$args{PASSWORD})=get_password(-name=>$cursvr, -type=>'sqlsvr')
	      	if ! defined $args{USER} or ! defined $args{PASSWORD};
	      $svrtype='sqlsvr' if $args{USER};
	   }
	   
	   if( $svrtype ne "sqlsvr" or $svrtype ne "win32servers" ) {
	   	$args{USER}="native" 		if ! $args{USER};
	   	$args{PASSWORD}="native"	if ! $args{PASSWORD};
	   }
	
	   if( ! $args{USER} and ! $args{PASSWORD} ) {
	   	warn "ERROR: Cant retrieve login/password for server $cursvr\n";
			MlpHeartbeat(
				-state=>"CRITICAL",
				#-debug=>$DEBUG,
				-subsystem=>$args{PROGRAM_NAME},
				-system=>$cursvr,
				-monitor_program=>$args{BATCH_ID},
				-message_text=>"GEM_010: ERROR: Cant retrieve login/password for server $cursvr.  Check Config Files.") 
					if $args{BATCH_ID} and ! $args{NO_CONNECTION_ALERTS};
			next;
	   }
	   
	   # wr_debug("\tLogin = $args{USER}\n");
	   
	   if( $args{returnonlycmdline} or $args{DOALL} eq "unix" or $args{DOALL} eq "win32servers" ) {
		   wr_debug("\tPassing Arguments For $cursvr\n");
			&{$args{server_command}}($cursvr,$svrtype,$args{USER},$args{PASSWORD});	
			wr_console("==> Completed $cursvr at ".localtime(time)."\n");   
			wr_console("+++++++++++++++++++++++++++++++++++++++++\n");
			next;
	   }
	   
	   my($rc)=dbi_connect(-srv=>$cursvr,-login=>$args{USER},-password=>$args{PASSWORD});
		if( ! $rc ) {
			wr_console("Unable To Connect To Server... Skipping\n");
			my($msg)="Cant connect to $cursvr as $args{USER} from ".hostname()." ".dbi_has_login_failed()."\n";
			MlpHeartbeat(
					-state=>"ERROR",
					# -debug=>$DEBUG,
					-subsystem=>$args{PROGRAM_NAME}." (connection)",
					-system=>$cursvr,
					-monitor_program=>$args{BATCH_ID},
					-message_text=>"GEM_010: ".$msg) 
						if $args{BATCH_ID} and $args{PROGRAM_NAME}  and ! $args{NO_CONNECTION_ALERTS};
			print "**************************************\n";	
			print "* ",$msg;	
			print "**************************************\n";	
			wr_console($msg);
			next;
		}
		
		MlpHeartbeat(
					-state=>"OK",
					# -debug=>$DEBUG,
					-subsystem=>$args{PROGRAM_NAME}." (connection)",
					-system=>$cursvr,
					-monitor_program=>$args{BATCH_ID},
					-message_text=>"GEM_010: Connected Successfully from ".hostname()) 
						if $args{BATCH_ID} and $args{PROGRAM_NAME}  and ! $args{NO_CONNECTION_ALERTS};
				
		wr_debug("\tConnected To $cursvr\n");
		dbi_set_mode("INLINE");
		&{$args{server_command}}($cursvr);		#,$connection);
		
		wr_console("+++++++++++++++++++++++++++++++++++++++++\n");
		wr_console("+ repository_parse_and_run completed on $cursvr at ".localtime(time)."\n");
		wr_console("+++++++++++++++++++++++++++++++++++++++++\n");
		
		dbi_disconnect();
	}
	
	MlpBatchJobEnd() if $args{BATCH_ID};
	# MlpAgentDone()  if $args{BATCH_ID};
	wr_console( "Program completed successfully.\n" );
	dbi_disconnect();
}

1;