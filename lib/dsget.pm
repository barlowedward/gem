# copyright (c) 2003-2008 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

package dsget;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use MlpAlarm;
use DBIFunc;
use File::Basename;

my($connection, $SHOWSQL, $PROGRAM_NAME, $DEBUG, $LDAP_KEY);
my(%cache_ldap_user_by_samid,%cache_ldap_user_dn_by_samid,%cache_ldap_user_samid_by_dn);

#/* , is_deleted       , display_name     */
my($cred_sql) = "(credential_name, credential_type, credential_id, system_name, system_type, collection_date, collected_by )";

$VERSION= 1.0;

@ISA      = (	'Exporter'	);
@EXPORT   = qw( delete_disabled_ldap_account get_user_info_by_dn_and_save get_group_members_by_dn
get_group_info_by_dn_and_save get_samid_from_dn get_user_dn_from_samid get_group_dn_from_samid
is_cached_user_samid LDAPinit get_all_ldap_users get_all_ldap_groups);

my($IS_INITIALIZED)=0;
my($DSGET,$DSQUERY);
sub LDAPinit {
	($connection, $SHOWSQL, $PROGRAM_NAME, $DEBUG, $LDAP_KEY)=@_;
	$LDAP_KEY="ldap user" unless $LDAP_KEY;
	$IS_INITIALIZED=1;

	$DSGET="C:/WINDOWS/system32/dsget.exe" if -e "C:\\WINDOWS\\system32\\dsget.exe";
	$DSGET="C:/WINDOWS/system32/dsget.exe" if -e "C:/WINDOWS/system32/dsget.exe";
	$DSGET="C:/WINDOWS/system32/dsget.exe" if ! $DSGET and -x "C:\\WINDOWS\\system32\\dsget.exe";
	$DSGET="C:/WINDOWS/system32/dsget.exe" if ! $DSGET and -x "C:/WINDOWS/system32/dsget.exe";
	$DSGET="C:/dsget.exe" if -e "C:/dsget.exe";

	opendir(DIR,'C:\windows\system32') || die("Can't open directory for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);
	foreach (sort @dirlist) { print $_,"\n" if /^ds/i and /\.exe/i; }

	if( ! $DSGET ) {
	   foreach my $cmd ( "dsget.exe -?  2>&1 " ) {
			print "exec: $cmd \n";
			open(Z,$cmd." |") or die "ERROR: Cant run $cmd $!\n";
			my($ok)=1;
			while(<Z>){
				if( /is not recognized as an internal/ or /not found/ ) { $ok=0; last;	}
				print __LINE__," ",$_;
			}
			 #		chomp;		print $_,"\n";	}
			$DSGET="dsget.exe" if $ok;
			close(Z);
		}
	}

	if( ! $DSGET ) {
		my($path)=$ENV{"PATH"};
		foreach( split /;/,$path ) {
			if( -e $_.'\dsget.exe' ) {
				print "\tSearching $_ OK\n";
				s.\\./.g;
				$DSGET=$_.'\dsget.exe';
				last;
			} else {
				print "\tSearching $_ NOT FOUND\n" if /win/ and /system/;
				next;
			}
		}
	}
	
	die "ERROR: CANT FIND DSGET in your path ! If in doubt, you can place dsget.exe/dsquery.exe in C:/" unless $DSGET;
	$DSQUERY=$DSGET;
	$DSQUERY=~s/dsget.exe/dsquery.exe/;

	print "DSQUERY=$DSQUERY\n";
	print "DSGET=$DSGET\n";
}

sub is_cached_user_samid {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($n)=@_;
	return 1 if $cache_ldap_user_by_samid{lc($n)};
	return 0;
}

sub delete_disabled_ldap_account {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($n)=@_;
	print "DELETE DISABLED ACCOUNT $n\n";
	my($q);
	$q = "delete INV_group_member where credential_name=".dbi_quote(-connection=>$connection,-text=>$n).
				" and group_type='dsget group'";
	print "SQL: ",$q,"\n" if $SHOWSQL;
	MlpAlarm::_querynoresults($q,1);
	print "\n" if $SHOWSQL;

	$q = "delete INV_credential_attribute where credential_name=".dbi_quote(-connection=>$connection,-text=>$n).
			"	and credential_type='".$LDAP_KEY."'";
	print "SQL: ",$q,"\n" if $SHOWSQL;
	MlpAlarm::_querynoresults($q,1);
	print "\n" if $SHOWSQL;

	$q = "delete INV_credential where credential_name=".dbi_quote(-connection=>$connection,-text=>$n).
			"	and credential_type='".$LDAP_KEY."'";
	print "SQL: ",$q,"\n" if $SHOWSQL;
	MlpAlarm::_querynoresults($q,1);
	print "\n" if $SHOWSQL;

	#$q = "select credential_name,credential_type,system_name,system_type from INV_credential where credential_name=".
	#	dbi_quote(-connection=>$connection,-text=>$n);
	#print "SQL: ",$q,"\n" if $SHOWSQL;
	#push @rows, MlpAlarm::_querywithresults($q,1);
	#print "\n" if $SHOWSQL;
}

sub get_user_info_by_dn_and_save {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($DN,$samid)=@_;
	print "f: get_user_info_by_dn_and_save($DN,$samid)\n" if $DEBUG;

	if( $cache_ldap_user_by_samid{lc($samid)} ) {
		print "Using Cached get_user_info_by_dn_and_save($DN,$samid)\n";
		return $cache_ldap_user_by_samid{lc($samid)};
	}

	my($q2)="$DSGET user $DN -L -samid -dn -desc -upn -sid -fn -mi -ln -office -display -empid -tel -email -hometel -pager -mobile -fax -iptel  -webpg -title -dept -company -mgr -pwdneverexpires -disabled -acctexpires";
	open(X,$q2." |") or die "Cant run $q2 $!\n";
	my(%DATA);
	while(<X>) {
		next if /^\s*$/;
		chomp;chomp;
		my($k,$v)=split(/:\s*/,$_,2);
		return undef if /dsget failed:/;
		next if $k =~ /^\s*$/;
		$DATA{$k} = $v unless $v =~ /^\s*$/;
		print ">>> $_\n" if $DEBUG;
	}

	# save existing data
#	if( $DATA{disabled} ne "yes" ) {
		my($samid)= $DATA{samid};
		#print "**************************************\n";
		print "   => SAMID=$samid\n";
		#print "**************************************\n";

		my($base)=	dbi_quote(-connection=>$connection,-text=>$samid).",".			# login
						"'".$LDAP_KEY."',".
						dbi_quote(-connection=>$connection,-text=>$DATA{empid}).",".		# sid
						"null,null";																	# system, systype

		my($q)= "\tinsert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
		print "SQL: ",$q,"\n" if $SHOWSQL;
		MlpAlarm::_querynoresults($q,1);
		print "\n" if $SHOWSQL;

		foreach ( keys %DATA ) {
			$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t '".$_."',".
				 dbi_quote(-connection=>$connection,-text=>$DATA{$_})	.")";
			print "SQL: ",$q,"\n" if $SHOWSQL;
			MlpAlarm::_querynoresults($q,1);
			print "\n" if $SHOWSQL;
		}
#	} else {
#		print "******************************************\n";
#		print "* Disabled Account $DATA{samid}\n";
#		print "******************************************\n\n";
#		$DISABLED_ACT{$DATA{samid}}=$DN;
#	}

	$cache_ldap_user_by_samid{lc($samid)}= \%DATA;
	return \%DATA;
}

# GROUP INFO NOT SAVED AS NO SPOT FOR IT RIGHT NOW
sub get_group_info_by_dn_and_save {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($DN)=@_;
	print "f: get_group_info_by_dn_and_save($DN)\n" if $DEBUG;
	my($cmd) = $DSGET.' group '.$DN.' -L -dn -sid -desc -samid -secgrp -scope';
	print "exec: $cmd \n";
	my(%DATA);
	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
	while(<Z>) {
		next if /^\s*$/;
		chomp;chomp;
		my($k,$v)=split(/:\s*/,$_,2);
		return undef if /dsget failed:/;
		next if $k =~ /^\s*$/;
		$DATA{$k} = $v unless $v =~ /^\s*$/;
		print ">>> $_\n" if $DEBUG;
	}
	close(Z);

	my($samid)= $DATA{samid};
	print "**************************************\n";
	print "* GROUP SAMID=$samid\n";
	print "**************************************\n";
	my($base)=	dbi_quote(-connection=>$connection,-text=>$samid).",".			# login
					"'dsget group',".
					dbi_quote(-connection=>$connection,-text=>$DATA{sid}).",".		# sid
					"null,null";																	# system, systype

	my($q)= "\tinsert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
	print "SQL: ",$q,"\n" if $SHOWSQL;
	MlpAlarm::_querynoresults($q,1);
	print "\n" if $SHOWSQL;

	foreach ( keys %DATA ) {
		$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t '".$_."',".
			 dbi_quote(-connection=>$connection,-text=>$DATA{$_})	.")";
		print "SQL: ",$q,"\n" if $SHOWSQL;
		MlpAlarm::_querynoresults($q,1);
		print "\n" if $SHOWSQL;
	}
}

sub get_group_members_by_dn {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($DN)=@_;
	print "f: get_group_members_by_dn($DN)\n" if $DEBUG;
	my(@MEMBERDNS);
	my($cmd) = $DSGET.' group '.$DN.' -members -expand';
	print "exec: $cmd \n";
	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
	while(<Z>) {
		next if /^\s*$/;
		chomp;
		return undef if /dsget failed:/;
		push @MEMBERDNS,$_;
		#print "MEMBER >> ",$_,"\n";
	}
	close(Z);

	return @MEMBERDNS;
}

sub get_samid_from_dn {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($DN)=@_;
	print "f: get_samid_from_dn($DN)\n" if $DEBUG;
	return $cache_ldap_user_samid_by_dn{$DN} if $cache_ldap_user_samid_by_dn{$DN};

	# not  prefetched - get it
	my($cmd)="$DSGET user $DN -samid";
	print "exec: $cmd \n";
	my($samid);
	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
	while(<Z>) {
		next unless /^\s/;
		next if /\s*samid\s*$/;
		chomp;
		s/\s+$//g;
		s/^\s+//g;
		return undef if /dsget failed:/;
		$samid=$_;
		#print ">> ",$_,"\n";
	}
	close(Z);
	return undef if ! $samid;

	$cache_ldap_user_samid_by_dn{$DN}=$samid;

	get_user_info_by_dn_and_save($DN,$samid);
	return $samid;
}

# returns distinguished name from samid
#   get_user_dn_from_samid(ebarlow) returns "CN=Barlow\, Ed,OU=DBA,OU=IT,OU=AD_Users,DC=AD,DC=MLP,DC=com"
sub get_user_dn_from_samid {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($n)=@_;
	print "f: get_user_dn_from_samid($n)\n" if $DEBUG;

	return $cache_ldap_user_dn_by_samid{$n} if $cache_ldap_user_dn_by_samid{$n};
	my($cmd) = $DSQUERY.' user -samid "'. basename($n).'"';
	print "exec: $cmd \n";
	my($DN)="";
	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
	while(<Z>) {		chomp;		$DN=$_;	}
	close(Z);
	$cache_ldap_user_samid_by_dn{$DN}=$n if $DN;
	$cache_ldap_user_dn_by_samid{$n}=$DN if $n and $DN;
	return $DN;
}

# returns distinguished name from samid
# dsquery group -name job_it_dba
# "CN=job_IT_DBA,OU=IT,OU=Jobs,OU=Groups,DC=AD,DC=MLP,DC=com"
sub get_group_dn_from_samid {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	my($n)=@_;

	my($cmd) = $DSQUERY.' group -name "'. basename($n).'"';
	print "exec: $cmd \n";
	my($DN)="";
	open(Z,$cmd." |") or die "Cant run $cmd $!\n";
	while(<Z>) {
		chomp;
		$DN=$_;
	}
	close(Z);
	return $DN;
}

sub get_all_ldap_groups {
	my($dnmap,$dbg)=@_;
	$DEBUG=$dbg if $dbg;
	die "NO DN MAP\n" unless $dnmap;
	my(%DNMAP)=%$dnmap;

	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	print "f: get_all_ldap_groups\n" if $DEBUG;
	MlpAlarm::_querynoresults('DELETE INV_credential where credential_type=\'ldap group\'',1);
	MlpAlarm::_querynoresults('DELETE INV_credential_attribute where credential_type=\'ldap group\'',1);
	MlpAlarm::_querynoresults('DELETE INV_group_member where group_type=\'ldap group\'',1);

	# we broke the following command up because apparently 1 group abends the $DSGET
	#my($cmd) = 'dsquery group -limit 0 | $DSGET group -L -dn -sid -desc -samid -secgrp -scope';

	open(X,"$DSQUERY group -limit 0 |") or die "Cant run dsquery group -limit 0 $!\n";
	my(@grps);
	while(<X>) { chop; chomp; push @grps,$_;	}
	close(X);

	my($dn,$desc);
	foreach my $grp ( @grps ) {			# $grp is a DN not a SAMID
		#
		# GET GROUP ARGS
		#
		my($cmd) = $DSGET.' group '.$grp.' -L -dn -sid -desc -samid -secgrp -scope';
		print "exec: $cmd \n";
		my($dn,$desc);
		my($cursamid)='';
		my($base,$q);

		open(Z,$cmd." |") or die "Cant run $cmd $!\n";
		while(<Z>) {
			#print $_;
			next if /dsget failed:/ or /dsget succeeded/;
			next if /^\s*$/ or /^Dsquery has reached the spec/;
			chomp;chomp;

			my($k,$v)=split(/:\s*/,$_,2);
			next if $k =~ /^\s*$/ or ! defined $v;

			if( $k eq "dn" ) {
				$dn = $v;
				next;
			} elsif( $k eq "desc" ) {
				$desc = $v;
				next;
			} elsif( $k eq "samid" ) {
				$cursamid=$v;
				$base=	dbi_quote(-connection=>$connection,-text=>$cursamid).",".	# login
							"'ldap group',".
							"null,".																	# sid
							"null,null";															# system, systype
				$q= "\tinsert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;

				$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t".
					"'dn'".
					",".
					dbi_quote(-connection=>$connection,-text=>$dn)	.")";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;

				$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t".
					"'desc'".
					",".
					dbi_quote(-connection=>$connection,-text=>$desc)	.")";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;
			} else {
				die "BASE is undefined??? k=$k v=$v" unless $base;
				$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t".
						dbi_quote(-connection=>$connection,-text=>$k).
						",".
					 	dbi_quote(-connection=>$connection,-text=>$v)	.")";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;
			}
		}
		close(Z);

		my($cmd) = $DSGET.' group '.$grp.' -members -expand';
		print "exec: $cmd \n";
		open(R,$cmd." |") or die "Cant run $cmd $!\n";
		while(<R>) {
			next if /^\s*$/;
			chomp;
			next if /dsget failed:/ or /dsget succeeded/;
			s/"//g;
			# print "MEMBERDN    >> ",$_,"\n";

			if( $DNMAP{$_} ) {
				print "\tMEMBERSAMID >> ",$DNMAP{$_},"\n" if $DEBUG;
				my($q)=	"\tinsert INV_group_member values \n\t( ".
							dbi_quote(-connection=>$connection,-text=>$cursamid).",".		# group name
					  		"'ldap group',".
					  		dbi_quote(-connection=>$connection,-text=>$grp).",".		# group dn
							dbi_quote(-connection=>$connection,-text=>$DNMAP{$_}).",".
							"'ldap user',null,null,null)";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;
			} else {
				print "*** UNKNOWN MEMBER $_ [ GRP $cursamid  ]\n";
			}
#
#	this actually works
#			$q="select credential_name from INV_credential_attribute where attribute_key='dn'
#					and attribute_value=".dbi_quote(-connection=>$connection,-text=>$_);
#			foreach ( MlpAlarm::_querywithresults($q,1) ) {
#				my($n)=dbi_decode_row($_);
#				print "DBMATCHNAME=$n\n";
#			}
		}
		close(R);
	}
#	use Data::Dumper;
	#print Dumper $dnmap;
}

#	print "==> PROCESSING GROUP MEMBERS FOR $n\n";
#	foreach my $member ( get_group_members_by_dn($DN) ) {
#		print "==> Found $member - member of $n\n";
#		my($samid)=get_samid_from_dn($member);
#		if( $samid ) {
#			print "SAVING MEMBER SAMID=",$samid," (group=$n)\n";
#			my($base)=	dbi_quote(-connection=>$connection,-text=>$n).",".			# group name
#							"'ldap group',".
#							dbi_quote(-connection=>$connection,-text=>$DATA{sid}).",".		# sid
#							dbi_quote(-connection=>$connection,-text=>$samid ).",".			# credential name
#							"'ldap user',". # credential type
#							"null,null,null";																	# system, systype
#
#			my($q)= "\tinsert INV_group_member values \n\t( $base )";
#			print "SQL: ",$q,"\n" if $SHOWSQL;
#			MlpAlarm::_querynoresults($q,1);
#			print "\n" if $SHOWSQL;
#
#		} else {
#			print "NOT SAVING MEMBER DN =",$member,"\n";
#		}
#	}
#
sub get_all_ldap_users {
	die "LDAP_init has not been called" unless $IS_INITIALIZED;
	print "f: get_all_ldap_users\n" if $DEBUG;

	MlpAlarm::_querynoresults('DELETE INV_credential where credential_type=\'ldap user\'',1);
	MlpAlarm::_querynoresults('DELETE INV_credential_attribute where credential_type=\'ldap user\'',1);

	open(Z,"$DSQUERY user  -limit 0 |") or die "Cant run dsquery user  -limit 0 $!\n";
	my(@usrs);
	while(<Z>) { chop; chomp; push @usrs,$_;	}
	close(Z);

	my($dn,$desc);
	my(%DN_MAP);

	foreach my $usr ( @usrs ) {			# $grp is a DN not a SAMID
		# -mgr seems to have a problem
		my($cmd)=$DSGET.' user '.$usr.' -L -dn -samid -desc -upn -sid -fn -mi -ln -office -display -empid -tel -email -hometel -pager -title -dept -company -pwdneverexpires -disabled -acctexpires';
		print "exec: $cmd \n";
		my($dn,$desc);
		my($cursamid)='';
		my($base,$q);

		open(X," $cmd |") or die "Cant run $cmd $!\n";
		while(<X>) {

			print "FETCHED $_" if $DEBUG;
			die $_ if /dsget failed:/;
			next if /^\s*$/ or /^Dsquery has reached the spec/ or /dsget succeeded/;
			chomp;chomp;
			my($k,$v)=split(/:\s*/,$_,2);
			next if $k =~ /^\s*$/ or ! defined $v;

			if( $k eq "dn" ) {
				$dn = $v;
				next;
			} elsif( $k eq "desc" ) {
				$desc = $v;
				next;
			} elsif( $k eq "samid" ) {
				$cursamid=$v;
				$base=	dbi_quote(-connection=>$connection,-text=>$cursamid).",".	# login
							"'ldap user',".
							"null,".																	# sid
							"null,null";															# system, systype
				$q= "\tinsert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;

				$DN_MAP{$dn}=$cursamid;

				$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t".
					"'dn'".
					",".
					dbi_quote(-connection=>$connection,-text=>$dn)	.")";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;

				$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t".
					"'desc'".
					",".
					dbi_quote(-connection=>$connection,-text=>$desc)	.")";
				print "SQL: ",$q,"\n" if $SHOWSQL;
				MlpAlarm::_querynoresults($q,1);
				print "\n" if $SHOWSQL;
			}

			$q="\tinsert INV_credential_attribute values \n\t( $base ,\n\t".
						dbi_quote(-connection=>$connection,-text=>$k).
						",".
					 	dbi_quote(-connection=>$connection,-text=>$v)	.")";
			print "SQL: ",$q,"\n" if $SHOWSQL;
			MlpAlarm::_querynoresults($q,1);
			print "\n" if $SHOWSQL;
		}
		close(X);
	}
	return \%DN_MAP;
}

1;

__END__

=head1 NAME

DSget.pm - Common Function Library For LDAP programs

=head2 DESCRIPTION

Standard/Miscellaneous functions for ldap

=head2 SUMMARY
