#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "
document_db.pl -dcodedir -SSRVR -UUSR -PPASS -DDATABASE [-x]

        -x debug mode

Document a database based on source code and ddl

Retrieves from server:
        procedures  - uses sp__syntax
        tables          - uses sp__helpcolumn & sp__helpindex

Retrieves from code directory and its subdirectories:
        - object & type based on file endings (must be in in \%okext hash)
        - object comments excluding predefined strings in \@ignorestrings
        - code directories can be ; separated
\n";
        return "\n";
}

my( @headings ) = ( "Name:", "Function:", "Date:", "Author:");

# COMMENT STRINGS TO IGNORE
my( @ignorestrings ) =  (
"^select database",
"^if table exists drop it",
"^if procedure exists drop it",
"^Creating",
"^grant security",
"^Primary Key",
"^create",
"rollback tran",
"^if modification and in xrm_sod_db, error and exit",
"^Create primary key",
"^Create indexes"
);

# OK EXTENSIONS TO PROCESS
my( %okext ) = (
        "Table",".tbl\$",
        "Procedure",".prc\$",
        "Trigger",".trg\$",
        "Rule",".rul\$",
        "Default",".def\$",
        "View",".vie\$",
        "Other File",".sql\$",
        "Insert Script",".ins\$"
);

use strict;
use Getopt::Std;
use File::Basename;

use vars qw($opt_d $opt_S $opt_U $opt_P $opt_D $opt_x);
die usage() if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('d:hS:U:P:D:x');
die usage("must pass db with $opt_D") unless defined $opt_D;

my($curdir)=dirname($0);
use Sybase::SybFunc;
#require("$curdir/syb_func.pl");

die usage("Must pass database\n" )
        unless defined $opt_D;
die usage("Must pass sa password\n" )
        unless defined $opt_P;
die usage("Must pass server\n")
        unless defined $opt_S;
die usage("Must pass sa username\n" )
        unless defined $opt_U;

print "connecting to sybase...\n";
db_connect($opt_S,$opt_U,$opt_P);

print "Retrieving stored procedure syntax info\n";
my(%proc_syntax);
my($proc)="exec sp__syntax NULL,\"Y\"";
print "executing $proc\n" if defined $opt_x;
my(@syntax) = db_query($opt_D,$proc);
foreach (@syntax) {
        my($name,$order,$parm)=db_decode_row($_);
        $proc_syntax{$name.$order}=$parm;
}

print "Retrieving table column info\n";
$proc="exec sp__helpcolumn NULL,NULL,\"Y\"";
print "executing $proc...\n" if defined $opt_x;
my(%table_columns);
my(@columns) = db_query( $opt_D,$proc );
foreach (@columns) {
        s/\s//g;
        my($col,$type,$ident,$null,$dflt,$rule,$tbl,$colnum)=db_decode_row($_);
        $table_columns{$tbl}= $colnum
                if ! defined $table_columns{$tbl}
                or $table_columns{$tbl}<$colnum;
        $table_columns{$tbl." ".$colnum}="";
        $table_columns{$tbl." ".$colnum}.="$col $type ";
        $table_columns{$tbl." ".$colnum}.="Not Null "
                if $null eq "No";
        $table_columns{$tbl." ".$colnum}.="Null "
                if $null eq "Yes";
        $table_columns{$tbl." ".$colnum}.="Identity "
                if $ident ne "0";
        $table_columns{$tbl." ".$colnum}.="(has Default) "
                if defined $dflt and $dflt ne "";
        $table_columns{$tbl." ".$colnum}.="(has Rule) "
                if defined $rule and $rule ne "";
}

print "Retrieving table index info\n";
$proc="exec sp__helpindex \@dont_format=\"Y\"";
print "executing $proc\n" if defined $opt_x;
db_msg_exclude(0);
my(%index_data);
my(@indexdata) = db_query($opt_D,$proc);
foreach (@indexdata) {
        s/\s//g;
        my($name,$c,$u,$i,$a,$s,$keys,$idxnm)=db_decode_row($_);
        $index_data{$name}="" if ! defined $index_data{$name};
        $index_data{$name}.="UNIQUE " if $u ne "";
        $index_data{$name}.="CLUSTERED " if $c ne "";
        $index_data{$name}.= $keys;
        $index_data{$name}.="\n";
}
db_msg_ok(0);

db_close_sybase();
print "exiting sybase...\n" if defined $opt_x;

if( defined $opt_d ) {

        my(@files);
        foreach ( split(";",$opt_d) ) {
                print "Reading files in $opt_d\n" if defined $opt_x;
                push @files, get_files($_);
        }

        foreach my $types ( keys %okext ) {
                foreach my $file (sort @files) {

                        next unless $file=~/$okext{$types}/;

                        # We now have a good file
                        my($nm)=basename($file);
                        $nm=~s/$okext{$types}//;

                        # nm is now name of proc/tbl/rule...
                        print "   $types $nm\n\n";
                        print_file($file);

                        if( $types eq "Procedure" ) {
                                if( defined     $proc_syntax{$nm."1"} ) {
                                        for (1..32) {
                                                last unless defined $proc_syntax{$nm.$_};
                                                printf( "       Parameter:    %s  %s\n",$_,$proc_syntax{$nm.$_});
                                        }
                                        print "\n";
                                }
                        } elsif( $types eq "Table" ) {
                                if( defined     $table_columns{$nm} ) {
                                        for (1..$table_columns{$nm}) {
                                                next unless defined $table_columns{$nm." ".$_};
                                                if( $_<10 ) {
                                                        printf( "       Column  $_:      %s\n",
                                                                $table_columns{$nm." ".$_});
                                                } else {
                                                        printf( "       Column $_:      %s\n",
                                                                $table_columns{$nm." ".$_});
                                                }
                                        }
                                } else {
                                                printf( "       Column:       no columns found for $nm\n");
                                }
                                if( defined     $index_data{$nm} ) {
                                        foreach( split /\n/,$index_data{$nm} ) {
                                                printf( "       Index:          %s\n",$_);
                                        }
                                } else {
                                                printf( "       Index:          no index found for $nm\n");
                                }
                                print "\n";
                        }
                }
        }
}

# read files in basedir.subdir and return list of files
sub get_files
{
        my($basedir)=@_;
        my(@out)=();
        if( -d $basedir ) {
                opendir(DIR,$basedir)
                        || die("Can't open dir $basedir for reading\n");
                my(@files) = grep(!/^\./,readdir(DIR));
                closedir(DIR);
                foreach (@files) {
                        if( -d $basedir."/".$_ ) {
                                push @out,get_files($basedir."/".$_);
                        } else {
                                push @out,"$basedir/$_";
                        }
                }
        }
        return @out;
}

# print only comments on new line from the file
sub print_file
{
        my($filename)=@_;
        open(FILE,$filename) or die "Cant Read $filename : $!\n";

        undef $/;
        my $file=<FILE>;  # slurp...
        $/="\n";                                # probably good practice :)
        my(@comments)=();
        while ( $file =~ m/\/\*([\w\W]+?)\*\/|--(.*?)\n/ig ) {
                my($comment)=$1|$2;
                $comment=~s/
//;                             # remove any control m's
                push @comments,split(/\n/,$comment);
        }

        COMMENT: foreach (@comments) {
                s/\*\*+//;                              # remove multiple stars  (comment format)
                s/^\s*\*+//;                    # remove stars at start of line
                s/\s+/ /;                               # all tabs and multi spaces to 1 space
                s/^\s//;                                        # remove leading spaces
                s/\s$//;                                        # remove space at line end
                next unless /\w/;       # must have alpha or digit on line
                foreach my $str (@ignorestrings) { next COMMENT if $_ =~ /$str/i; }
                print "   => $_\n";
        }
        print "\n" if $#comments>=0;

        close FILE;

                #foreach $str (@headings) {
                        #if(/$str/i) {
                                #s/$str//i;
                                #s/^\s+//;
                                #printf( "      %8.8s       $_",$str,$_) if /\w/;
                                #next LINE;
                        #}
                #}
}

__END__

=head1 NAME

document_db.pl - database documentation utility

=head2 DESCRIPTION

Self document your database!  Uses source code and the stuff in
the system tables.

=head2 USAGE

 document_db.pl -dcodedir -SSRVR -UUSR -PPASS -DDATABASE [-x]

        -x debug mode

 Document a database based on source code and ddl

 Retrieves from server:
        procedures  - uses sp__syntax
        tables          - uses sp__helpcolumn & sp__helpindex
 Retrieves from code directory and its subdirectories:
        - object & type based on file endings (must be in in %okext hash)
        - object comments excluding predefined strings in @ignorestrings
        - code directories can be ; separated

=head2 NOTES

Pretty good output.  Kind of loses shape if the comments in your
ddl are ugly.   They probably are though.  One concept here is that
you can document stuff inline FOR PURPOSES of this type of utility.
Reformat your stuff so you can see what your tables do etc...  Simple
and Somewhat useful.

=cut

