#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Repository;
use Getopt::Long;
use DBIFunc;
use MlpAlarm;
use CommonFunc;
use Logger;
use File::Basename;
use CommonHeader;

use vars qw( $TYPE $OUTFILE $USER $SERVER $BATCH_ID $PASSWORD $MAXRUNNING $MAXUSERS $opt_d $MAILTO $DOALL);

my($heartbeat_debug);		# set to 1 to just to debug mlpheartbeats

sub usage
{
   print @_;
   print "check_num_users.pl --USER=USER --SERVER=SERVER --PASSWORD=PASS --BATCH_ID=ID -TYPE=Sybase|ODBC [--DEBUG] --MAXRUNNING=num --MAXUSERS=num --OUTFILE=OUTPUTFILE--MAILTO=a,b --DOALL=sybase|sqlsvr

   if --MAXUSERS not passed - defaults to 80\% of the maximum value\n";

   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"OUTFILE=s"	=> \$OUTFILE ,
		"MAILTO=s"	=> \$MAILTO ,
		"USER=s"	=> \$USER ,
		"BATCH_ID=s"	=> \$BATCH_ID ,
		"DOALL=s"	=> \$DOALL ,
		"PASSWORD=s"	=> \$PASSWORD ,
		#"TYPE=s"	=> \$TYPE ,
		"MAXRUNNING=s"	=> \$MAXRUNNING ,
		"MAXUSERS=s"	=> \$MAXUSERS ,
		"DEBUG"		=> \$opt_d
	);

unlink(	$OUTFILE ) if defined $OUTFILE and -w $OUTFILE;

my($subject)="Error Users";
$subject.=" on server $SERVER" if defined $SERVER;
$subject.=" checking $DOALL"   if defined $DOALL;

logger_init( -debug=> $opt_d, -logfile => $OUTFILE, -command_run => $0, -mail_to =>$MAILTO, -fail_subject=>"Error Users on Server $SERVER"  );

logdie( usage("Must pass server or --DOALL=sybase|sqlsvr\n" ))   unless defined $SERVER or defined $DOALL;
logdie( usage("Must pass username\n" )) unless defined $USER or defined $DOALL;	# or defined $TYPE;
logdie( usage("Must pass password\n" )) unless defined $PASSWORD or defined $DOALL;	# or defined $TYPE;

my(@server_list);
if( defined $DOALL ) {
	logdie( usage("DOALL must be sqlsvr or sybase\n" ))
		if $DOALL ne "sqlsvr" and $DOALL ne "sybase";
	MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Gem Monitor',-SUBKEY=>$DOALL) if $BATCH_ID;

	push @server_list, get_password(-type=>$DOALL);
} elsif( defined $SERVER ) {
	MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Gem Monitor',-SUBKEY=>$SERVER) if $BATCH_ID;
	push @server_list, $SERVER;
} else {
	die "Must pass --SERVER or --DOALL";
}

info("MAXRUNNING parameter not passed\n")
	unless defined $MAXRUNNING;
info("Alarming is Off (--BATCH_ID not passed)\n") unless $BATCH_ID;
info("Alarming is On (--BATCH_ID = $BATCH_ID)\n") if $BATCH_ID;

my($maxusers);
my($die_string)="";
foreach my $cursvr (@server_list ) {
	info("Check Users Called on server $cursvr at ".localtime(time)."\n");
	debug( "Connecting to $cursvr\n" );
	if( defined $DOALL ) {
		($USER,$PASSWORD)=get_password(-type=>$DOALL, -name=>$cursvr);

	} else {
		($USER,$PASSWORD)=get_password(-type=>"sybase", -name=>$cursvr);
		($USER,$PASSWORD)=get_password(-type=>"sqlsvr", -name=>$cursvr) unless $USER;
	}

	my($rc)=dbi_connect(-srv=>$cursvr,-login=>$USER,-password=>$PASSWORD);
	#if( ! $rc ) {
		#my(@av_drivers)=DBI->available_drivers();
		#info("Failed to connect - trying new type\n");
		#if( $TYPE eq "Sybase" ) {
		#	$TYPE="ODBC";
		#} else {
		#	$TYPE="Sybase";
		#}
		#my($found)=0;
		#foreach (@av_drivers) { $found=1 if $_ eq $TYPE; }
		#if($found==0) {
		#	info("No alternative drivrers\n");
		#} else {
		#	info("Using alternative driver $TYPE\n");
		#	$rc=dbi_connect(-srv=>$cursvr,-login=>$USER,-password=>$PASSWORD, -type=>$TYPE);
		#}
	#}

	if( ! $rc ) {
		MlpHeartbeat(	-state=>"CRITICAL",
			-debug=>$heartbeat_debug,
			-system=>$cursvr,
			-subsystem=>"Total Users",
			-monitor_program=>$BATCH_ID,
			-message_text=>"Cant connect to $cursvr as $USER\n")  if $BATCH_ID;
		logger_init( -debug=> $opt_d, -logfile => $OUTFILE, -command_run => $0, -mail_to =>$MAILTO, -fail_subject=>"YCan Not Connect To $cursvr"  );

		#logdie("Cant connect to $cursvr as $USER\n");
		$die_string.="Cant connect to $cursvr as $USER\n";
		info("Cant connect to $cursvr as $USER\n");
		next;
	}

	debug("Connected To $cursvr\n");
	my($DBTYPE,$DBVERSION)=dbi_db_version();
	info("SERVER TYPE=$DBTYPE VER=$DBVERSION\n") if $opt_d;
	dbi_set_mode("INLINE");

	if( ! defined $MAXUSERS	) {
		my $sql2 = "select value from syscurconfigs where config=103\n";
		print $sql2 if $opt_d;
		foreach( dbi_query(-db=>"master",-query=>$sql2)) {
			my @dat=dbi_decode_row($_);
			$maxusers=$dat[0];
			debug( "Max Number of database connections is $maxusers\n");
			$maxusers *= .8;
		}
	} else {
		$maxusers=$MAXUSERS;
	}
	info( "Max Connections Allowed is $maxusers\n"	)	unless $maxusers==0;
	logdie( usage("Must pass number_of_users\n" )) unless defined $maxusers;

	my($num_users);
	my($sql)= "select count(*) from	sysprocesses ";
 	if( $DBTYPE eq "SQL SERVER" ) {
		$sql.=" where status!='background'\n";
	} else {
		$sql.=" where suser_name(suid) is not null\n";
	}
	print $sql if $opt_d;
	foreach( dbi_query(-db=>"master",-query=>$sql))	{
		my @dat=dbi_decode_row($_);
		$num_users=$dat[0];
	}

	my($num_sim_users);
	if( $DBTYPE eq "SQL SERVER" ) {
		$sql="select count(*) from sysprocesses	where status not in ('sleeping','background')\n";
	} else {
		$sql="select count(*) from sysprocesses	where suid!=0 and cmd!='AWAITING COMMAND'\n";
	}
	print $sql if $opt_d;
	foreach( dbi_query(-db=>"master",-query=>$sql))	{
		my @dat=dbi_decode_row($_);
		$num_sim_users=$dat[0];
	}

	debug( "Server $cursvr has $num_users Users\n");
	debug( "Server $cursvr has $num_sim_users Running Users\n");

	if( $num_users > $maxusers and $maxusers>0 ) {
		my($str)= "Server $cursvr has $num_users connections. This exceeds the max number allowed ($maxusers).\n";
		alert(  localtime(time)." ".$str ) ;
		MlpHeartbeat(	-state=>"ERROR",
			-debug=>$heartbeat_debug,
			-system=>$cursvr,
			-subsystem=>"Total Users",
			-monitor_program=>$BATCH_ID,
			-message_text=>$str) if $BATCH_ID;
	} else {
		my($str)= "Server $cursvr has $num_users connections (OK) \n";
		$str.="(Max=$maxusers)" if $maxusers>0;
		info(  localtime(time)." ".$str ) ;
		MlpHeartbeat(	-state=>"OK",
			-debug=>$heartbeat_debug,
			-system=>$cursvr,
			-subsystem=>"Total Users",
			-monitor_program=>$BATCH_ID,
			-message_text=>$str) if $BATCH_ID;
	}

	if( defined $MAXRUNNING ) {
		if( $num_sim_users > $MAXRUNNING ) {
			my($str)= "Server $cursvr has $num_sim_users simultaneous running connections. This exceeds the max number allowed ($MAXRUNNING).\n";
			info(localtime(time)." ".$str);
			MlpHeartbeat(	-state=>"ERROR",
				-debug=>$heartbeat_debug,
				-system=>$cursvr,
				-subsystem=>"Running Users",
				-monitor_program=>$BATCH_ID,
				-message_text=>$str) if $BATCH_ID;
			dbi_disconnect();
			#logdie( $str );
			$die_string.=$str;
		} else {
			my($str)="Server $cursvr has $num_sim_users simultaneous running connections (OK) (Max=$MAXRUNNING)";
			info(localtime(time)." ".$str."\n");
			MlpHeartbeat(	-state=>"OK",
				-debug=>$heartbeat_debug,
				-system=>$cursvr,
				-subsystem=>"Running Users",
				-monitor_program=>$BATCH_ID,
				-message_text=> $str ) if $BATCH_ID;
			dbi_disconnect();
		}
	}
	info("\n");

	dbi_disconnect();
}

MlpBatchJobEnd() if $BATCH_ID;

if( $die_string ne "" ) {
	print $die_string;
	bad_email( $die_string );
}

exit(0);

__END__

=head1 NAME

check_sybase_num_users.pl - Count Number Of Users on sybase or sql server and alarm if error

=head2 USAGE

	check_num_users.pl --USER=USER --SERVER=SERVER --PASSWORD=PASS  --MAXRUNNING=num

	or

	check_num_users.pl --USER=USER --SERVER=SERVER --PASSWORD=PASS --BATCH_ID=ID -TYPE=Sybase|ODBC [--DEBUG] --MAXRUNNING=num --MAXUSERS=num --OUTFILE=OUTPUTFILE --MAILTO=a,b --DOALL=sybase|sqlsvr

   if --MAXUSERS not passed - defaults to 80% of the maximum value

=head2 DESCRIPTION

Checks for number of user connections >	max_num_users

=cut

