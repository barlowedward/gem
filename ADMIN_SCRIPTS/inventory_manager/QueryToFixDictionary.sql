
select d1.attribute_name,d1.system_type,d2.system_type,d1.detail_display_key,d2.detail_display_key
, d1.compute_into_type,d2.compute_into_type
, d1.compute_into_name,d2.compute_into_name
from INV_attribute_dictionary d1, INV_attribute_dictionary d2
where d1.system_type!=d2.system_type
and   (d1.attribute_name=d2.attribute_name or d1.attribute_name=d2.compute_into_name)
and   d1.detail_display_key != d2.detail_display_key
and   d1.system_type in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
and   d2.system_type not in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')

update INV_attribute_dictionary set d1.detail_display_key = d2.detail_display_key
from INV_attribute_dictionary d1, INV_attribute_dictionary d2
where d1.system_type!=d2.system_type
and   (d1.attribute_name=d2.attribute_name or d1.attribute_name=d2.compute_into_name)
and   d1.detail_display_key != d2.detail_display_key
and   d1.system_type in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
and   d2.system_type not in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
/*
update INV_attribute_dictionary set d1.detail_display_key = 0
from INV_attribute_dictionary d1, INV_attribute_dictionary d2
where d1.system_type!=d2.system_type
and   (d1.attribute_name=d2.attribute_name or d1.attribute_name=d2.compute_into_name)
and   d1.detail_display_key != d2.detail_display_key
and   d1.system_type in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
and   d2.system_type not in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
*/
select d1.attribute_name,d1.system_type,d2.system_type,d1.master_display_key,d2.master_display_key
, d1.compute_into_type,d2.compute_into_type
, d1.compute_into_name,d2.compute_into_name
from INV_attribute_dictionary d1, INV_attribute_dictionary d2
where d1.system_type!=d2.system_type
and   (d1.attribute_name=d2.attribute_name or d1.attribute_name=d2.compute_into_name)
and   d1.master_display_key != d2.master_display_key
and   d1.system_type in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
and   d2.system_type not in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')

update INV_attribute_dictionary set d1.master_display_key = d2.master_display_key
from INV_attribute_dictionary d1, INV_attribute_dictionary d2
where d1.system_type!=d2.system_type
and   (d1.attribute_name=d2.attribute_name or d1.attribute_name=d2.compute_into_name)
and   d1.master_display_key != d2.master_display_key
and   d1.system_type in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
and   d2.system_type not in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')

update INV_attribute_dictionary set d1.master_display_key = 0
from INV_attribute_dictionary d1, INV_attribute_dictionary d2
where d1.system_type!=d2.system_type
and   (d1.attribute_name=d2.attribute_name or d1.attribute_name=d2.compute_into_name)
and   d1.master_display_key != d2.master_display_key
and   d1.system_type in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
and   d2.system_type not in  ('sybase','sqlsvr','oracle','mysql','win32servers','unix')
