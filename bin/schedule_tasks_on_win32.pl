
#
# TEMPLATE IS MY STANDARD MODULE FOR GEM TEMPLATE
#
use strict;
use Getopt::Long;
use CommonFunc;

my($PROGRAM_DESCRIPTION)="Win32 Task Installer";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG $descheduleonly $SCHLOGIN $SCHPASS );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG --SCHLOGIN=login --SCHPASS=pass" 
	unless GetOptions( "DEBUG"	=> \$DEBUG, "descheduleonly" => \$descheduleonly, "SCHLOGIN=s"=>\$SCHLOGIN, "SCHPASS=s"=>\$SCHPASS );

print "$0 - v",$VERSION,"\n";
print "   Reading gem.dat\n";

die "ERROR - Must pass Scheduler Login\n" unless $SCHLOGIN;
die "ERROR - Must pass Scheduler Password\n" unless $SCHPASS;

my($GemConfig)=read_gemconfig(-debug=>$DEBUG);
if( $GemConfig->{ERRORS} ) {
	foreach ( @$GemConfig->{ERRORS} ) {
		_statusmsg("[cfgfiles] $_ \n" );
	}
}
sub _statusmsg { 	print @_; }
# START BODY
	
use Win32Scheduler;
use JobScheduler;
use Sys::Hostname;
use File::Basename;

my $scheduler=new Win32Scheduler(-name_prefix=>'gem_');
my $real_scheduled_jobs=$scheduler->get_all_tasks();
my($job_scheduler) = fetch_jobscheduler();			# ALARM SCHEDULE DATA
die "Must Run on Win32" unless is_nt();
	
print "MASTER INTERFACE TO WINDOWS SCHEDULER\n";
print "VERSION V1.0";

# $GemConfig=read_gemconfig( -debug=>$DEBUG, -nodie=>1, -descheduleonly);

# die usage("Must pass Windows Account\n")	unless defined $act and $act !~ /^\s*$/;
# $GemConfig->{scheduler_act}   = $act;
# $GemConfig->{scheduler_pass}  = 'dev2000';

# my($scheduler_act);	
#if( ! $GemConfig->{scheduler_act} ) {
#	print "\nPlease Enter NT Account LoginName For The Scheduled Task: ";
#	$scheduler_act=<STDIN>;
#	chomp $scheduler_act;
#	chomp $scheduler_act;
#	die "\nScheduled Account May not be  blank\n" unless $scheduler_act;
#} else {
#	chomp $GemConfig->{scheduler_act};
#	$scheduler_act = $GemConfig->{scheduler_act};
#}
#
# my($scheduler_pass);	
#if( ! $GemConfig->{scheduler_act} ) {
#	$scheduler_act = $GemConfig->{scheduler_act};
#	print "\nPlease Enter NT Account Password For The Scheduled Task: ";
#	$scheduler_pass=<STDIN>;
#	chomp $scheduler_pass;
#	chomp $scheduler_pass;
#	die "Scheduled Pass May not be  blank\n" unless $scheduler_pass;
#} else {
#	chomp $GemConfig->{scheduler_pass};
#	$scheduler_pass = $GemConfig->{scheduler_pass};
#}

my($rc)=$scheduler->deschedule_a_job();
my($cnt)=0;
foreach my $job_name (	sort keys %$job_scheduler ) {
	print "Removing Job $job_name\n" if defined $$job_scheduler{$job_name}->{scheduled_on} and $DEBUG;
	$$job_scheduler{$job_name}->{scheduled_on}="";
	$cnt++;
}
_statusmsg("[configure] Removal of $cnt Existing Jobs Completed\n");

exit if $descheduleonly;

# convert home dir to full path
my($NT_CODE_ROOT)=$GemConfig->{NT_CODE_LOCATION};
print "CODE=$NT_CODE_ROOT\n" if $DEBUG;
if( $GemConfig->{INSTALLTYPE} eq "SAMBA" ) {
	open(CMD,"net use |") or die "Cant Run net use";
	my($ok)="FALSE";
	my($a)=substr($NT_CODE_ROOT,0,2);
	while(<CMD>) {
		next if /^\s*$/ or /^New connections/;
		if( /^Status\s*Local\s*Remote\s*Network/ ) {
			$ok="TRUE";		
			next;
		}	
		next if /-----------------------/ or /The command compl/;
		next unless /^OK/;
		chomp;
	
		my($status,$localdr,$remote,$network)=split;
		
		$NT_CODE_ROOT = $remote.substr($NT_CODE_ROOT,2) if $localdr eq uc($a);
	}
	close(CMD);
	die "Unable to parse 'net use' for base directory. pls seek help" unless $ok eq "TRUE";
	$NT_CODE_ROOT =~ s.\/.\\.g;
} 
_statusmsg("[configure] NT ROOT=$NT_CODE_ROOT\n" );

foreach my $job_name (	sort keys %$job_scheduler ) {
	if( $$job_scheduler{$job_name}->{run_type} ne "Win32"
	    or $$job_scheduler{$job_name}->{enabled} eq "NO" ) {
		  	if( $$job_scheduler{$job_name}->{run_type} ne "Win32" ) {
		     	print "Ignoring $job_name runtype=$$job_scheduler{$job_name}->{run_type}\n" if $DEBUG;
		   } else {
		     	print "Ignoring $job_name enabled=$$job_scheduler{$job_name}->{enabled}\n" if $DEBUG;
		   }
		  	next;
	}

	# SKIP AS NEEDED
	next if $GemConfig->{INSTALLTYPE} 	 eq "WINDOWS" and $job_name=~/Unix/;
	next if $GemConfig->{USE_SYBASE_ASE} eq "N" 	     and ($job_name=~/Sybase/ or $job_name=~/^GemRpt_Syb/ or $job_name=~/^Syb/);
	next if $GemConfig->{USE_SQLSERVER}  eq "N"	 	  and $job_name=~/Mssql/;
	next if $GemConfig->{USE_ORACLE}     eq "N"	 	  and $job_name=~/Oracle/;

	$$job_scheduler{$job_name}->{scheduled_on}=hostname();
	do_schedule($job_name,$scheduler);
}
		
_statusmsg(  "[win32 scheduler] Scheduling Completed\n" );

# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "SUCCESSFUL: $PROGRAM_DESCRIPTION\n";
close_gemconfig();

if( $DEBUG ) {
	print "Dumping Scheduler Information\n";
	dump_jobscheduler($job_scheduler);
}

print "Saving Scheduler Information\n";
save_jobscheduler($job_scheduler);

show_min() if $DEBUG;
exit(0);

my(%schedule_minutes);		# used to even out the jobs... 
sub show_min {
	foreach (sort { $a <=> $b } keys %schedule_minutes ) {
		print $_," ",$schedule_minutes{$_},"\n";
	}
}

# returns lowest used in 1..$sch
sub get_start_min {
	my($sch)=@_;	# 10 or 15
	my($min_i, $min_cnt)=(0,1000);
	$sch--;
	
	my $i=$sch;
	while(1) {
	#for my $i ($sch..0) {
		if( $schedule_minutes{$i} ) {
			if( $schedule_minutes{$i} < $min_cnt ) {
				$min_cnt = $schedule_minutes{$i};
				$min_i=$i;
			}				  
		} else {
			return $i;
		}		
		$i--;
		return $min_i if $i<0;
	}
	return $min_i;
}	

sub do_schedule {
	my($job_name,$scheduler)=@_;
	if( ! defined $$job_scheduler{$job_name} ) {
		_statusmsg("[win32 scheduler] No Job Defined named $job_name - this shouldnt happen\n");
		return;
	}

	my( %job_hash ) = %{$$job_scheduler{$job_name}};
	my( $real_job_hashptr ) = $$real_scheduled_jobs{"gem_".$job_name.".job"};
	if( defined $$real_scheduled_jobs{"gem_".$job_name.".job"} ) {
		_statusmsg(  "[win32 scheduler] gem_$job_name - replacing task\n" );
	} else {
		_statusmsg(  "[win32 scheduler] gem_$job_name - scheduling new task\n" );
	}

	if( $job_hash{run_type} ne "Win32" ) {
		_statusmsg("[win32 scheduler] This Program can only run on win32\n");
		return;
	}

	$$job_scheduler{$job_name}->{scheduled_on}=hostname();

	my($program,$wdir);
	if( $job_hash{job_type} eq "backup" ) {
		$program=$NT_CODE_ROOT."\\win32_batch_scripts\\plan_batch\\".$job_name.".bat";
		#$wdir=$NT_CODE_ROOT."\\win32_batch_scripts\\plan_batch\\";
	} else {
		$program=$NT_CODE_ROOT."\\win32_batch_scripts\\batch\\".$job_name.".bat";
		#$wdir=$NT_CODE_ROOT."\\win32_batch_scripts\\batch\\";
	}
	
	$program =~ s/\//\\/g;
	#$wdir    =~ s/\//\\/g;
	
	$wdir = "C:\\";   # this is what works.
	

	my($freq)=$job_hash{frequency};
	my($start_hour,$start_min);
	if( $freq eq "sunday-am" or $freq eq "saturday-am" ) {		
		$start_hour=int(rand(10));		
		$start_min =int(rand(60));	
	} elsif( $freq eq "sunday-pm" or $freq eq "saturday-pm" ) {		
		$start_hour=int(rand(10))+12;		
		$start_min =int(rand(60));	
	} elsif( $freq =~ /^daily\-/ ) {
#     	if( $freq eq 'daily-8pm' ) {
#     		$freq='daily';$start_hour=20;
#     	} elsif( $freq eq 'daily-10pm' ) {
#     		$freq='daily';$start_hour=22;
#     	} elsif( $freq eq 'daily-Midnight' ) {
#     		$freq='daily';$start_hour=0;
#     	} elsif( $freq eq 'daily-2am' ) {
#     		$freq='daily';$start_hour=2;
#     	} elsif( $freq eq 'daily-4am' ) {
#     		$freq='daily';$start_hour=4;
#     	} elsif( $freq eq 'daily-7am' ) {
#     		$freq='daily';$start_hour=7;
#     	} elsif( $freq eq 'daily-6am' ) {
#     		$freq='daily';$start_hour=6;
#     	} elsif( $freq eq 'daily-8am' ) {
#     		$freq='daily';$start_hour=8;
#     	} els
     	if( $freq =~ /daily\-(\d+)am/ ) {
     		$freq='daily';$start_hour=$1;
     	} elsif( $freq =~ /daily\-(\d+)pm/ ) {
     		$freq='daily';$start_hour=$1;$start_hour+=12;
     	} elsif( $freq eq 'daily-Midnight' ) {
     		$freq='daily';$start_hour=0;
     	} else {
     		die("ERROR: Unknown schedule $freq\n");
     	}
     	$start_min  =  int(rand( 60 ));     	
   } elsif( $freq eq "hourly" or $freq eq "Every15Min" or $freq eq "Every10Min" ) {
		$start_hour = 0;
		if( $freq eq "hourly" ) {
			$start_min  = get_start_min(60);
			#$start_min  = int(rand( 60 ));	
		} elsif( $freq eq "Every15Min" ) {
			# $start_min  = int(rand( 15 ));	
			$start_min  = get_start_min(15);
			$schedule_minutes{$start_min+15}++;
			$schedule_minutes{$start_min+30}++;
			$schedule_minutes{$start_min+45}++;
		} elsif( $freq eq "Every10Min" ) {
			# $start_min  = int(rand( 10 ));	
			$start_min  = get_start_min(10);
			$schedule_minutes{$start_min+10}++;
			$schedule_minutes{$start_min+20}++;
			$schedule_minutes{$start_min+30}++;
			$schedule_minutes{$start_min+40}++;
			$schedule_minutes{$start_min+50}++;
		}		
	} else {			# weekly or never are the only ones left
		$start_hour = 20+int(rand( 8 ));
		$start_min  =  int(rand( 60 ));
	}	

	$start_hour -= 24 if $start_hour >= 24;
		
	my($enabled)=1;
	if( $job_hash{enabled} eq "NO" ) {		$enabled=0;	}
	_statusmsg("[ $program ]\n"); 
	_statusmsg("[win32 scheduler]   enabled      =$enabled\n") unless $enabled=~/1/;
	_statusmsg("[win32 scheduler]   schedule     =$freq\n");
	_statusmsg("[win32 scheduler]   start_hour   =$start_hour       start_minutes=$start_min\n");
	
	$schedule_minutes{$start_min}++ if $start_hour==0;
	my($rc)=$scheduler->schedule_a_job(
		 #-debug			=> 	1,
		 -enabled		=>	$enabled,
		 -name			=>	$job_name,
		 -schedule		=>	$freq,
		 -account		=>	$SCHLOGIN,
		 -password		=>	$SCHPASS,
		 -program 		=>	$program,
		 -directory		=>	$wdir,
		 -start_hour	=>	$start_hour,
		 -start_minutes=>	$start_min );
	return $rc;
}