# Copyright (c) 1999 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package LogFunc;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use Do_Time;

my @must_have_patern_list;			# ignore lines that dont have grep patern in them
my @normal_filters;	     # ignore lines containing (includes start and mult ln)
my @pat_filters;
my @filter_start;	       # ignore lines starting with
my @multi_line_patern;  # multi line ignore
my @paterns_to_rm_via_sed;			 # remove patern
my @multi_line_end;
my @filter_next;
my( $host_col,$program_col,$max_cols);
my( @hosts_to_ignore, @programs_to_ignore );
my( $today_flag, $yesterday_flag );

$VERSION= 1.0;

@ISA      = ('Exporter');
@EXPORT = ( 'log_filter_file', 'log_process_file', 'log_rd_patern_file', 'log_wr_patern_file',
	'log_clear', 'log_debug', 'log_grep', 'log_rm_pat',
	'log_filter_hosts', 'log_filter_programs','log_set_num_cols');


my $DEBUG;

sub log_filter_hosts {
	($host_col,@hosts_to_ignore)=@_;
	die "Error must call log_set_num_cols() if you use log_filter_hosts()\n" unless defined $max_cols;
}
sub log_filter_programs {
	($program_col,@programs_to_ignore)=@_;
	die "Error must call log_set_num_cols() if you use log_filter_programs()\n" unless defined $max_cols;
}
sub log_set_num_cols {
	($max_cols)=@_;
}

sub log_clear
{
	undef @must_have_patern_list;
	undef @normal_filters;
	undef @pat_filters;
	undef @hosts_to_ignore;
	undef @programs_to_ignore;
	undef @filter_start;
	undef @multi_line_patern;
	undef @paterns_to_rm_via_sed;
	undef @multi_line_end;
	undef @filter_next;
	undef $today_flag;
	undef $yesterday_flag;
}

sub log_rm_pat
{
	my($pat)=@_;
	@paterns_to_rm_via_sed = split(/\|/,$pat);
}

sub log_grep
{
	my($srch_patern,$fmt_str);
	my(%PAR)=@_;
	my($filter_file);
	undef @must_have_patern_list;
	foreach (keys %PAR) {
		if( $_ eq "search_patern" ) {
			$srch_patern = $PAR{$_};
		}elsif( $_ eq "today" ) {
			$today_flag = $PAR{$_};
		}elsif( $_ eq "yesterday" ) {
			$yesterday_flag = $PAR{$_};
		}elsif( $_ eq "date_fmt" ) {
			$fmt_str = $PAR{$_};
		} else {
			die "Invalid Parameter Passed : $_\nMust be search_patern, today, yesterday, or date_fmt\n";
		}
	}
	@must_have_patern_list = split(/\|/,$srch_patern)	if defined($srch_patern);
	push(@must_have_patern_list,get_date( 0,$fmt_str))	if defined $today_flag;
	push(@must_have_patern_list,get_date(-1,$fmt_str))	if defined $yesterday_flag;
	print "FORMAT=$fmt_str GREP PATERN=<",join("><",@must_have_patern_list),">\n"
		if $#must_have_patern_list>=0 and  defined $DEBUG;
}

sub log_debug { $DEBUG=1; }

sub log_filter_file
{
	my(%PAR)=@_;
	my($filter_file);
	foreach (keys %PAR) {
		if( $_ eq "file" ) {
			$filter_file = $PAR{$_};
		} else {
			die "Invalid Parameter Passed : $_";
		}
	}

	if( ! defined $filter_file ) {
		print "No Exclusion File\n" if defined $DEBUG;
		undef @normal_filters;
		undef @pat_filters;
		undef @filter_start;
		undef @multi_line_patern;
		undef @multi_line_end;
		undef @filter_next;
		return;
	}

	# get filter data file
	print "log_filter_file() Reading Exclusion File $filter_file\n" if defined $DEBUG;
	open(CONFIGFILE,$filter_file) || die  "Can't open filter file $filter_file: $!\n";
	while ( <CONFIGFILE> ) {
		next if /^#/ or /^\s*$/;
		chomp;
		chomp;
		chomp;
		tr/*/-/;			# needed for wild cards
		if ( /^EXCLUDE/ ) {
					chop;
					s/^EXCLUDE=//;
					push(@normal_filters,$_);
		} elsif ( /^PATERN=/ ) {
					chop;
					s/^PATERN=//;
					push(@pat_filters,$_);
		} elsif ( /^ML_START/ ) {
					chop;
					s/^ML_START=//;
					push(@normal_filters,$_);
					push(@multi_line_patern,$_);
		} elsif ( /^ML_END/ ) {
					chop;
					s/^ML_END=//;
					push(@multi_line_end,$_);
		} elsif ( /^EXCLNEXT/ ) {
					chop;
					s/^EXCLNEXT=//;
					push(@normal_filters,$_);
					push(@filter_next,$_);
		} elsif ( /^EXCLSTART/ ) {
					chop;
					s/^EXCLSTART=//;
					push(@filter_start,$_);
		}
	}
	close(CONFIGFILE);

	if( defined $DEBUG ) {
		print " log_filter_file(): $#normal_filters normal paterns found\n";
		print " log_filter_file(): $#pat_filters pat filters found\n";
		print " log_filter_file(): $#multi_line_patern multi line patern found\n";
		print " log_filter_file(): $#filter_next next line normal_filters found\n";
		print " log_filter_file(): $#filter_start start line normal_filters found\n";
	}
}

# process an input file
#  returns rows read,found_input_patern,last_line,
sub log_process_file
{
	my(%PAR)=@_;
	my($input_file,$ignore_blanks,$patern,$byte_offset,$max_lines,$cat_nogrep,$i_am_up_func,$i_am_up_rows);
	print "Called log_process_file() in debug mode\n" if $DEBUG;
	my($textcol);
	foreach (keys %PAR) {
		if( $_ eq "search_patern" ) {
			$patern = $PAR{$_};
		} elsif( $_ eq "textcol" ) {
			$textcol = $PAR{$_};
		} elsif( $_ eq "max_lines" ) {
			$max_lines = $PAR{$_};
		} elsif( $_ eq "file" ) {
			$input_file = $PAR{$_};
		} elsif( $_ eq "ignore_blanks" ) {
			$ignore_blanks = $PAR{$_};
		} elsif( $_ eq "byte_offset" ) {
			$byte_offset = $PAR{$_};
		} elsif( $_ eq "i_am_up_func" ) {
			$i_am_up_func = $PAR{$_};
		} elsif( $_ eq "i_am_up_rows" ) {
			$i_am_up_rows = $PAR{$_};
		} elsif( $_ eq "cat_nogrep" ) {
			$cat_nogrep = $PAR{$_};
		} elsif( $_ ne "is_sybase_ase_file" ) {
			die "Invalid Parameter Passed : $_\nMust be search_patern, max_lines, file, ignore_blanks, byte_offset, or cat_nogrep\n";
		}
	}
	
	my(@output);
	unshift @output, "(dbg) log_process_file($input_file,$ignore_blanks,$patern,$byte_offset,$max_lines,$cat_nogrep)\n"
		if $DEBUG;

	my($num_rows_read,$num_rows_returned)=(0,0);
	$max_lines=100000 unless defined $max_lines;
	my($found_input_patern)=0;
	my($lastline,$lastlinelen);

	unshift @output, "(dbg) RUNNING FILTER IN DEBUG MODE\n" if defined $DEBUG;
	unshift @output, "(dbg) Reading input file $input_file\n" if defined $DEBUG;
	open(MESSAGESFILE,$input_file)
		|| die  "Can't open input file $input_file: $!\n";

	&$i_am_up_func if $i_am_up_func;
	
	# move forward to the patern
	if( defined $patern and defined $byte_offset and $patern ne "" and $byte_offset != 0) {

		unshift @output, "(dbg) byte_offset=$byte_offset patern=$patern\n"	if $DEBUG;

		seek(MESSAGESFILE,$byte_offset,0);
			# or print "DEBUG - SEEK INTO $input_file By $byte_offset FAILED\n";
		my($tline);
		while( <MESSAGESFILE> ) {
			$tline=$_;
			last;
		}
		$tline =~ tr/*/-/;
		# print "DEBUG tline=$tline\n";
		# print "DEBUG patern=$patern\n";
		if( index( $tline , $patern )==0 ) {
			unshift @output, "(dbg) FOUND PATERN IN INPUT FILE AT THE CORRECT SPOT\n" if defined $DEBUG;
		} else {
			if( defined $DEBUG ) {
				unshift @output, "(dbg) File At Byte Offset = $tline\n";
				unshift @output, "(dbg) PATERN=$patern\n";
				unshift @output, "(dbg) DID NOT FIND PATERN IN INPUT FILE - RESETTING FILE\n";
			}
			seek MESSAGESFILE,0,0 ;
		}
	} else {
		unshift @output, "(dbg) skipping patern/byte offset search\n"			if $DEBUG;
	}
	&$i_am_up_func if $i_am_up_func;
	
	# ok if you have patern and no byte offset then repeatedly bisect
	# file until you find it patern
	if( $byte_offset==0 and ( defined $today_flag or defined $yesterday_flag )) {
		unshift @output,"Did not find Patern - but did find today/yesterday flag\n";
		#print "xxxDBG: BYTE OFFSET=$byte_offset = bisecting file\n";
		seek MESSAGESFILE,0,2 ;
		my($file_size)= tell MESSAGESFILE;
		if( $file_size > 1000000000 ) {
			die "FILE Has a size problem - size=$file_size - can not handle files over a gigabyte\n";
		} elsif( $file_size > 100000 ) {
			my($incr)=int $file_size;
			my($offset)=0;
			print "(dbg) Bisecting File of size $file_size\nLooking For \'".join("\' or \'",@must_have_patern_list)."\'\n" if defined $DEBUG;
			while(1) {
				$incr = int $incr / 2;
				$offset+=$incr;
				last if $incr < 100;
				last if search_file( $offset );
			}
			$offset -= $incr;
			unshift @output, "(dbg) DONE - $offset out of $file_size\n" if defined $DEBUG;
			seek MESSAGESFILE,$offset,0 ;
			<MESSAGESFILE>;
		} else {
			seek MESSAGESFILE,0,0 ;
		}
	}else {
		unshift @output, "(dbg) skipping today/yesterday search\n"			if $DEBUG;
	}

	&$i_am_up_func if $i_am_up_func;
	
	##########################
	# now read the file
	##########################
   my($passed_grep_test)="FALSE";
	while ( <MESSAGESFILE> ) {
		tr/*/-/;
		my($line)=$_;
		$num_rows_read++;
	
		if( $i_am_up_rows and $num_rows_read%$i_am_up_rows==0 ) {
				print "REMOVE ME DBG DBG : ROWCOUNT=$num_rows_read\n";
				&$i_am_up_func if $i_am_up_func;			
		}
		
		# ignore blank lines if you so wish
		next if defined $ignore_blanks and /^\s*$/;

		# for sybase ... join lines together
		if( $PAR{is_sybase_ase_file} and $line !~ /^\d\d:\d\d\d\d\d:/ ) {
			next if /^\s*$/;
			$output[0]='' if $#output<0;
			$output[$#output].=$line;
			$lastlinelen += length $line;
			# unshift @output, "(dbg) appended $line to output\n" if defined $DEBUG;
			next;
		}
		
		# search for greps
		if ( $#must_have_patern_list>=0 ) {
			my $found_grep=0;
			foreach (@must_have_patern_list) {
				if( $line=~/$_/) {
					$found_grep++;
					last;
				}
			}

			if( ! $found_grep and $#must_have_patern_list != -1 ) {
				if( $passed_grep_test eq "TRUE" and defined $cat_nogrep )  {
					$output[0]='' if $#output<0;
					$output[$#output].=$line;
					$lastlinelen += length $line;
					unshift @output, "(dbg) appended $line to output\n" if defined $DEBUG;
				}
				next;
			}
		}
		
		my(@splitline)=split(/\s+/,$line,$max_cols) if defined $max_cols;
		if( defined $program_col ) {
			my($foundexclude)=0;
			foreach( @programs_to_ignore ) {
				next unless $splitline[$program_col] eq $_;
				print "(dbg) Ignoring Line with Program $_\n" if $DEBUG;
				$foundexclude=1;
				last;
			}
			next if $foundexclude;
		}

		if( defined $host_col ) {
			my($foundexclude)=0;
			foreach( @hosts_to_ignore ) {
				next unless $splitline[$host_col] eq $_;
				print "(dbg) Ignoring Line with Host $_\n" if $DEBUG;
				$foundexclude=1;
				last;
			}
			next if $foundexclude;
		}

   	$passed_grep_test="FALSE";

		#ignore date lines
		$lastline=$line;
		$lastlinelen=length $line;

		# PATCH - ALLOW PATERN FILTERS
		my($found_excl)=0;
		#print "DBG DBG: line is $line\n";
		foreach my $excl (@pat_filters) {
			#print "DBG DBG: excl is $excl\n";
			if( $line =~ /$excl/ ) {
					#print "DBG DBG IGNORING LINE!\n";				
					unshift @output, "(dbg) PatFilt:".$excl."\n" if defined $DEBUG;
					$found_excl=1;
					last;
			}
		}
		next if $found_excl;
		
		# SEARCH for normal_filters
		EXCLUDES: foreach my $excl (@normal_filters) {
				my $pat_idx=index($line,$excl);
				if( $pat_idx >= 0 ) {
				
					# FOUND SOMETHING - NOW WHAT KIND
					unshift @output, "(dbg) Exclusion:".$excl."\n" if defined $DEBUG;

					# IS IT AN EXCLUDE START? IF SO ONLY IGNORE IF $pat_idx = 0
					if( $pat_idx == 0 ) {
						my $ex_start=0;
						EXSTART: foreach (@filter_start) {
							if( $_ eq $excl ) {
								$ex_start=1;
								last EXSTART;
							}
						}

						if ( $ex_start == 1 ) {
							unshift @output, "(dbg) Ignoring Exclude Start ($excl) in line \n".$line."\n" if defined $DEBUG;
							# If filter start and not at start of line then next
							next if( $pat_idx > 0 );
						}
					}

					$found_excl=1;

					# Now You Must Decide if it is a special type of filter
					my $cnter = 0;
					MLPAT: foreach my $ml (@multi_line_patern) {
						if( index($line,$ml) >= 0 ) {
							# you have found an multiline reject
							unshift @output, "(dbg) Found MULTILINE:".$ml."\n" if defined $DEBUG;
							my($found)=0;
							READ: while ( <MESSAGESFILE> ) {
								$lastlinelen+=length $_;
								$num_rows_read++;
								if( index($_,$multi_line_end[$cnter])>=0 ) {
									$found=1;
									last READ;
								}
							}
							push @output, "Error No End Patern Found ($multi_line_end[$cnter])\n" if $found==0;
							$found_excl=0;
							unshift @output, "(dbg) DONE MULTI:".$line if defined $DEBUG;
							last MLPAT;
						}
						$cnter = $cnter + 1;
					}

					ENEXT: foreach my $en (@filter_next) {
						if( index($line,$en) >= 0 ) {
							unshift @output, "(dbg) Found Exclude Next:".$en."\n" if defined $DEBUG;
							# you have found an filter next row
							<MESSAGESFILE>;
							$lastlinelen+=length $_;
							$num_rows_read++;
							unshift @output, "(dbg) Next Row Was:".$_ if defined $DEBUG;
							last ENEXT;
						}
					}
					last EXCLUDES;
				} else {
					$found_excl=0;
				}
			}

			if( $found_excl==0 ) {
							
				chop $line;
				if( $num_rows_returned > $max_lines ) {
					push @output, "(dbg) Exiting as over $max_lines rows printed\n";
					last;
				}
				foreach ( @paterns_to_rm_via_sed ) {
					$line =~ s/$_//g;
				}
   			$passed_grep_test="TRUE";
				unshift @output, "(dbg) Including $line\n" if defined $DEBUG;
				push @output, $line."\n";
				$num_rows_returned++;
		  }
	}

	# maybe no new lines read
	my($byte_count);

	if( ! defined $lastline ) {
		$lastline = $patern ;
		$byte_count=$byte_offset;
	} else {
		$byte_count=tell MESSAGESFILE;
		#$byte_count -= length $lastline;
		$byte_count -= $lastlinelen;
	}

	close(MESSAGESFILE);
	return($num_rows_read,$num_rows_returned,$found_input_patern,$lastline,$byte_count,@output);

}

# get patern
sub log_rd_patern_file
{
	my(%PAR)=@_;
	my($patern_file);
	foreach (keys %PAR) {
		if( $_ eq "file" ) {
			$patern_file = $PAR{$_};
		} else {
			die "Invalid Parameter Passed : $_";
		}
	}

	my($byte_count);
	return(0,"") if ! -e $patern_file;
	die "Cant write to $patern_file" if ! -w $patern_file;
	open(PATFILE,$patern_file) || die  "Can't open patern file $patern_file: $!\n";
	my($patern)="";
	while( <PATFILE> ) {
		chop;
		tr/*/-/;				# needed for wild cards
		($byte_count,$patern)=split /\t/,$_;
	}
	close(PATFILE);
	return ($byte_count,$patern);
}

sub log_wr_patern_file
{
	my(%PAR)=@_;
	my($patern,$byte_count,$patern_file)=@_;
	foreach (keys %PAR) {
		if( $_ eq "patern" ) {
			$patern = $PAR{$_};
		} elsif( $_ eq "bytes" ) {
			$byte_count = $PAR{$_};
		} elsif( $_ eq "file" ) {
			$patern_file = $PAR{$_};
		} else {
			die "Invalid Parameter Passed : $_";
		}
	}

	return  if ! defined $patern_file;
	return  if ! defined $patern;

	# note that you need patern ne "" in case of empty log file!
	if ( defined $DEBUG ) {
		print "log_wr_patern_file() Debug Mode: Not Writing Back Patern With Last Patern\n";
		print "log_wr_patern_file() Final Patern Is:",$patern," File=",$patern_file,"\n";
	} else {
		if( -e $patern_file ) {
			truncate($patern_file,0) || die  "Can't trunc $patern_file: $!\n";
		}
		open(PATFILE2, "> $patern_file")
			|| die  "Can't open $patern_file: $!\n";
		print PATFILE2 $byte_count,"\t",$patern;
		close PATFILE2;
	}
}

# pass in an offset (days), format string, and return formated string
sub get_date
{
	my($offset,$format)=@_;

	# (sec,min,hour,mday,mon,year,wday,yday,isdst)
	my @tarray = localtime(time + $offset*60*60*24);

	# correct the month subscript
	$tarray[4]++;
	$tarray[5]-=100 if $tarray[5]>=100;

	# make sure you have appropriate length
	if ( length($tarray[4])==1 ) { substr($tarray[4],0,0)='0'; }
	if ( length($tarray[5])==1 ) { substr($tarray[5],0,0)='0'; }

	my(@month)=("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	my($ldate)="";
	my(@f)=split(//,$format);
	while($#f>0) {
		if( $f[0] eq "/" ) {
			$ldate.="/";
		} elsif( $f[0] eq " " ) {
			$ldate.=" ";
		} elsif( $f[0] eq "m" and $f[1] eq "m" ) {
			$ldate.=$tarray[4];
			shift @f;
		} elsif( $f[0] eq "y" and $f[1] eq "y" and $f[2] eq "y" and $f[3] eq "y") {
			$ldate.="20".$tarray[5];
			shift @f;
			shift @f;
			shift @f;
		} elsif( $f[0] eq "y" and $f[1] eq "y" ) {
			$ldate.=$tarray[5];
			shift @f;
		} elsif( $f[0] eq "d" and $f[1] eq "m" ) {
			$ldate.= $tarray[3];
			shift @f;
		} elsif( $f[0] eq "d" and $f[1] eq "s" ) {
			if ( length($tarray[3])==1 ) { substr($tarray[3],0,0)=' '; }
			$ldate.=$tarray[3];
			shift @f;
		} elsif( $f[0] eq "d" and $f[1] eq "d" ) {
			if ( length($tarray[3])==1 ) { substr($tarray[3],0,0)='0'; }
			$ldate.=$tarray[3];
			shift @f;
		} elsif( $f[0] eq "m" and $f[1] eq "o" and $f[2] eq "n" ) {
			$ldate.= @month[$tarray[4]];
			shift @f;
			shift @f;
		} else {
			die "Illegal Character ($f[0]) in Format String $format\n";
		}
		shift @f;
	}
	print "GetDate($offset,$format) = $ldate\n" if defined $DEBUG;
	$ldate;
}

sub search_file
{
	my($offset)=@_;
	seek MESSAGESFILE,$offset,0 ;
	<MESSAGESFILE>;
	my($line)=<MESSAGESFILE>;
	foreach (@must_have_patern_list) {
		if( $line=~/$_/) {
			print "Pat found at offset $offset - $line\n" if defined $DEBUG;
			return(1);
		}
	}
	print "(dbg) Patern Not Found at of=$offset - line=$line" if defined $DEBUG;
	return 0;
}

1;

__END__

=head1 NAME

LogFunc.pm - Parse Log Files For Errors

=head2 DESCRIPTION

Generic log file filtering library.  Can do lots of neat stuff.  It is probably
not the best way to do it, but it works.  Features an optional patern file
which can be used to keep track of "recent" changes to the file.

The patern file feature allows you to identify recent changes in your
error log.  This is optional.
The patern file works this way.  The first time through (no file will exist),
the file will be writen with a byte offset and the text from
the last line successfully
processed.  The next time through, this
file can be read and checked to see that the appropriate line
exists at the byte offset.  This will tell us that the
log file has not been overwritten, removed, or whatever. If the line
exists at the byte offset, it is assumed that the file has simply been
appended to.   The search then starts at
the byte offset from the patern file.  If the string is not at that byte
count, the patern file is ignored and the search starts at the beginning
of the file.

You may specify an filter file which contains directives on
what lines to ignore.   Additionally you may ignore lines by using the
log_rm_pat function.

=head2 SUMMARY

 log_filter_file    - read a filter directives file
 log_process_file   - handle an input file and return filtered results
 log_rd_patern_file - read patern file
 log_wr_patern_file - write patern file
 log_debug	  - set debug mode
 log_grep	   - set paterns that must be in output string
 log_rm_pat	 - this patern should not appear in output

=head2 FUNCTIONS

=over 4

=item * log_rm_pat

Set paterns to remove from output lines.  The lines are not removed, just
the patern.  The remove paterns should be a pipe (|) separated list.  They
may contain perl wild cards.

=item * log_grep(search_patern=>pat,today=>1|0,yesterday=>1|0,date_fmt=>fmtstr)

ARGUMENTS: "search_patern" "today" "yesterday" "date_fmt"

Set information that the output must contain.  This can be either based on
dates (using today/yesterday with date_fmt) or explicitly.
The Search Patern Instructions are a pipe separated list of
strings (format a|b|c).  Date Format is parsed as follows:

	mm 1-12	month
	yy 99,00,01	     year
	dm 1,9,10,13    day of month
	dd 01,09,10,13  day of month
	mon Jan Feb... month

Input Arguments

 search_patern	  - grep paterns in addition to date filters
 today			  - if defined do today
 yesterday		  - if defined do yesterday
 date_fmt		  - search string format for dates

=item * log_debug

Sets debug flag.  Only run if you want debug messages shown.

=item * log_filter_file(file=>filename)

Input Arguments
	file   - patern file to write

Read filter instructions from filename

=item * log_process_file(file=>input_file,ignore_blanks=>1, search_patern=>patern, byte_offset=>byte_offset_for_pat, max_lines=>max_output_lines, cat_nogrep=>1)

Input Arguments

file    - input file to read

ignore_blanks - ignore blanks if defined

search_patern - search patern of last line read

byte_offset  - byte offset to start search of search_patern from

cat_nogrep   - lines that dont pass grep pass are appended to prior line if the prior line passed all tests.  This can join things like deadlock info that spans multiple lines

max_lines  - max output lines

Output Parameters
 $num_rows_read   - rows read
 $num_rows_returned - rows filtered
 $found_input_patern - did you find the input patern
 $lastline_read   - can be used as patern for next search
 $byte_count    - byte count of this last line
 @output     - filtered output

Filter a File.  Returns (num_rows_printed,num_rows_read,found_patern,returned_patern,byte_cnt,@output)

=item * log_rd_patern_file(file=>filename)

Input Arguments
 file   - patern file to write

Read a patern file.  Returns byte count and start patern.

=item * log_wr_patern_file(patern=>pat,bytes=>bytes,file=>filename)

Input Arguments
 patern  - line on the file
 bytes   - byte count of last read line
 file   - patern file to write

Write out a patern file.

=head2 Notes

The exclude files use a simple syntax...

PATERN=[perlpat]
EXCLUDE=[string to exclude]


=back
