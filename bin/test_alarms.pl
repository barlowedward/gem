#
# standard plugin header
#
use strict;
use Getopt::Long;
use MlpAlarm;

use vars qw( $DEBUG $ALARM_SERVER $ALARM_DATABASE $ALARM_LOGIN $ALARM_PASSWORD );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" 
	unless GetOptions( "DEBUG"	=> \$DEBUG,
		"ALARM_SERVER=s"		=> \$ALARM_SERVER,
		"ALARM_DATABASE=s"	=> \$ALARM_DATABASE,
		"ALARM_LOGIN=s"		=> \$ALARM_LOGIN,
		"ALARM_PASSWORD=s"	=> \$ALARM_PASSWORD  );

my(@errors);


print "test_alarms.pl - v1.0\n";
print "Reading configuration variables\n";
my( $server, $login, $password , $db ) = MlpGetVars();

print "Read The Following Server Info From MlpAlarm.pm Server=$server Login=$login DB=$db\n";

use CommonFunc;
print "Reading gem.dat\n";
my($GemConfig)=read_gemconfig(	-debug=>$DEBUG );

$ALARM_SERVER 		= $GemConfig->{ALARM_SERVER} 		unless $ALARM_SERVER;
$ALARM_DATABASE 	= $GemConfig->{ALARM_DATABASE} 	unless $ALARM_DATABASE;
$ALARM_LOGIN 		= $GemConfig->{ALARM_LOGIN} 		unless $ALARM_LOGIN;
$ALARM_PASSWORD 	= $GemConfig->{ALARM_PASSWORD} 	unless $ALARM_PASSWORD;

push @errors, "ALARM_SERVER is not set" 				unless $ALARM_SERVER;
push @errors, "ALARM_DATABASE is not set" 			unless $ALARM_DATABASE;
push @errors, "ALARM_LOGIN is not set" 				unless $ALARM_LOGIN;
push @errors, "ALARM_PASSWORD is not set" 			unless $ALARM_PASSWORD;

if( $#errors == 3 ){
	print "ERROR: Alarm System Not Installed\n";
	exit(2);
}

push @errors, "$ALARM_SERVER does not match $server" 				unless $ALARM_SERVER eq $server;
push @errors, "$ALARM_DATABASE does not match $db" 				unless $ALARM_DATABASE eq $db;
push @errors, "$ALARM_LOGIN does not match $login" 				unless $ALARM_LOGIN eq $login;
push @errors, "$ALARM_PASSWORD does not match $password" 		unless $ALARM_PASSWORD eq $password;

if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "Initial Setup OK\n";

my($rc)=MlpTestInstall(-debug=>$DEBUG);
if( $rc eq "" ) {
	print "SUCCESSFUL: Alarm System Ok\n";
	exit(0);
}
my($errcnt)=0;
foreach ( split("/n",$rc) ){
	$errcnt++;
	print "ERROR: ",$rc;
}
exit($errcnt);