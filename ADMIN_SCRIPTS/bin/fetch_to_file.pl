#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# Perl Script To Fetch Results To File
#
# Author E. Barlow

use    Sybase::DBlib;

use strict;
use vars qw($opt_D $opt_U $opt_S $opt_P $opt_X $opt_F $opt_T);

sub usage
{
        print @_,"\n";
        print "Usage: $0 -X -Uusr -Ssrv -Ppass -Tdelimiter -Ddb -FFile QUERY\n";
        print "\n";
        print " Tab is default delimeter.  -X option to set debug mode\n";
        print "Fetch Data To File.\n";
        exit 0;
}
use    Getopt::Std;
use File::Basename;
my($curdir)=dirname($0);

use Sybase::SybFunc;

usage("Bad Parameter List")                     unless getopts('XT:U:S:P:D:F:');
usage("Must Pass Srv with -S")          unless defined $opt_S;
usage("Must Pass User with -U")                 unless defined $opt_U;
usage("Must Pass Pass with -P")                 unless defined $opt_P;
usage("Must Pass Database")                     unless defined defined $opt_D;

$opt_T="\t" unless defined $opt_T;

my($QUERY)=join(" ",@ARGV);
chomp $QUERY;
$QUERY.="\n";

my($dbproc) = new Sybase::DBlib $opt_U, $opt_P, $opt_S;
if( ! defined $dbproc ) {
        die("Can't connect to the $opt_S Sybase server as login $opt_U.\n")
}

print "Connected\n" if defined $opt_X;

$dbproc->dbuse($opt_D);

if( defined $opt_F ) {
        unlink $opt_F if -e "$opt_F";
        open(OUT1,"> $opt_F") or die "cant open $opt_F";
}

print "QUERY: ",$QUERY if defined $opt_X;

$dbproc->dbcmd($QUERY);
$dbproc->dbsucceed(1);
my($cnt)=0;
my(@dat);
print "Query Completed\n";
my(@float_col_ids)=();
for( my($i)=0; $i<$dbproc->dbnumcols(); $i++ ) {
        next unless $dbproc->dbcoltype($i)==62;
        print "Column $i is a floating point column\n" if defined $opt_X;
        push @float_col_ids,$i;
}

while(@dat = $dbproc->dbnextrow()) {

        # round it off if float...
        foreach (@float_col_ids) { $dat[$_]=0 if $dat[$_]<.0000000000000000000001; }

        if( defined $opt_F ) {
                print OUT1 join($opt_T,@dat),"\n";
        } else {
                print join($opt_T,@dat),"\n";
        }
        $cnt++;
}

$dbproc->dbclose();

close(OUT1) if defined $opt_F;
