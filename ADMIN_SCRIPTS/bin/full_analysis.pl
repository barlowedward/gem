#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Path;
use File::Basename;
use Getopt::Std;
use vars qw($opt_c $opt_h $opt_o $opt_d $opt_s $opt_U $opt_S $opt_P $opt_D $opt_r);
use vars qw($DEBUG);
use Cwd;

# SET THIS ONLY FOR DEBUGGING - makes program work on this proc only
#my($debug_one_proc)="sp_who";
my($debug_one_proc);
my(%search_info);

$|=1;

#
# GRAB STUFF FOR DIRECTORY AND REPEATEDLY RUN ANALYZE ON IT
# OUTPUT IN DATA/SERVER/DB/analyze
#

my(%type_override,%type_values,%vbl_override);
my(%tbl_scan_count);            # number of table scans by table
my(%tbl_scan_proc_data);# data for tbl_scan_procs
my(%tbl_scan_procs);            # procs that scanned the table
my(%proc_scan_count);   # number of table scans by proc
my(%proc_scan_rows);            # number of rows table scanned by proc
my(%proc_scan_proc_data);
my(%proc_scan_procs);
my(%table_rows);                        # rows for each table
my($count)=1;                           # counter of procs run

# File Extension of output data (either .out or .htm)
my($ext)="out";

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd $curdir :$!\n";
$curdir=getcwd();

sub usage
{
        print @_;
        print "\nfull_analysis.pl [-ds -h -odir] -UUSER -SSERVER -PPASS -Ddb

                -c use specified config file (default full_analysis.cfg)
                -d debug mode
                -h output in html format
                -o output in this directory
                -s build summary info only (dont run any procs)
                -v verbose ( more verbose than normal less than -d )
                -r redo procs that have errors

        This procedure loops through all stored procs in a db and runs the
        analyze package on each of them.  Output is placed in files. It can
        be html format (with -h).

        Output is in SERVER/DB subdirectory of the current directory unless
        -o passed.

        You may ignore -U and -P options if you have set up your password file\n";

        return "\n";
}

print "full_analysis.pl - Full Database Showplan Analyzer\n";
print "copyright (c) 1999 by SQL Technologies\n";

die usage() unless getopts('c:dU:D:sS:P:ho:r');

use Sybase::SybFunc;

die usage("Must pass db name list with -D")     unless defined $opt_D;
die usage("Must pass sa password")                              unless defined $opt_P;
die usage("Must pass server")                                   unless defined $opt_S;
die usage("Must pass sa username")                              unless defined $opt_U;

$DEBUG=1 if defined $opt_d;
$ext="htm" if defined $opt_h;

#
# READ DATA FILE
#
# - THIS SETS UP
#                       %type_override
#                       %type_values
#                       %vbl_override

$opt_c="full_analysis.cfg" unless defined $opt_c;
read_config_file($opt_c) unless defined $opt_s;

#
# CONNECT TO DB
#
print "Connecting to Database\n";
db_connect($opt_S,$opt_U,$opt_P)
        or die "Cant connect to $opt_S as $opt_U\n";

#
# GET PROCEDURES IN DB
#
print "Getting Procedures\n";
my(@proclist);
if( defined $debug_one_proc ) {
        push @proclist,$debug_one_proc;
} else {
        @proclist=db_query($opt_D,"select name from sysobjects where uid=1 and type=\"P\"");
}

if( $#proclist < 0 ) {
        die "Aborting: No Stored Procedures Found In Database\n";
}

#
# GET TABLE ROWS
#
foreach (db_query($opt_D,"exec sp__helptable")) {
        my($tbl,$rows,$junk)=db_decode_row($_);
        $table_rows{$tbl} = $rows;
}

#
# GET PROCEDURE SYNTAX
#
my(@syntax);
if( defined $debug_one_proc ) {
        @syntax = db_query($opt_D,"exec sp__syntax \"$debug_one_proc\",\"Y\"");
} else {
        @syntax = db_query($opt_D,"exec sp__syntax NULL,\"Y\"");
}

#
# BUILD COMMAND LINES
#
my(%proc_cmd_line);
foreach (@proclist) {
        next if defined $debug_one_proc and $_ ne $debug_one_proc;
        $proc_cmd_line{$_}="exec $_";
}

# WORK AROUND 11.5.1 BUG FOR NULLITY OF PROC PARAMETERS
my($found_null_param)=0;
foreach( db_query($opt_D, "if exists ( select 1 from sysobjects o, syscolumns c where o.id=c.id and o.type='P' and c.status&8 = 1 ) select 1" )) {
        $found_null_param=1;
}

my(%proctext);
if($found_null_param==0) {

        # PARSE FOR NULL COLUMNS
        my($query) = "select o.name,c.text from sysobjects o, syscomments c where o.type='P' and o.id in ( select id from syscolumns ) and o.id = c.id and c.colid=1";
        my(@rc)= db_query($opt_D,$query);
        foreach (@rc) {
                my($procname);
                ($procname,$_)=db_decode_row($_);
                s/^\s*create\s*proc\w*//i;
                s/$procname//;
                my($offset)=index($_," as ");
                $offset=index($_,"\)as ") if $offset == -1;
                $offset=index($_," as\n") if $offset == -1;
                $offset=index($_,"\nas\n") if $offset == -1;
                $offset=index($_,"\)as\n") if $offset == -1;
                #$offset=index($_,"as") if $offset == -1;
                substr($_,$offset,255)="" if $offset > -1;
                s/\s+/ /;
                $proctext{$procname}=$_;
                #print "<b>",$procname,"</b> ",$_,"\n";
        }
}

#
# CONTINUE BUILDING %proc_cmd_line (ADD PARAMETERS)
#
# build proc_cmd_string{$proc}
#       ignore null parameters from sp__syntax
#       ignore null parameters from proc text (if 11.5.1
#       set parameter override if exist then type override
#
foreach (@syntax) {
        my($procname,$order,$parm)=db_decode_row($_);
        my($parm_is_null)=0;

        # parm is of type name type [NOT] NULL
        chomp $parm;
        $parm_is_null=1 if $parm!~/NOT NULL/;
        $parm =~ s/NOT NULL//;
        $parm =~ s/NULL//;

        my($parm_name,$parm_type,$junk)=split /\s+/,$parm;

        if( defined $proctext{$procname} ) {
                if($proctext{$procname} =~ /$parm_name\s*$parm_type\s*=\s*null/i ) {
                        $parm_is_null=1;
                }
        }

        # DO VARIABLE OVERRIDE THEN TYPE OVERRIDE THEN FIGURE IT OUT
        if( defined $vbl_override{$parm_name} ) {
                print ">>>procedure $procname parameter=>$parm_name override=>$vbl_override{$parm_name}\n" if defined $opt_d;
                $proc_cmd_line{$procname}.=" $parm_name=".$vbl_override{$parm_name};
        } elsif( $parm_is_null==1 ) {
                print "Ignoring null parameter $parm_name in $procname\n"
                        if defined $opt_d;
                next;
        } elsif( defined $type_values{$parm_type}
                                and $type_values{$parm_type} ne "" ) {
                $proc_cmd_line{$procname}.=" $parm_name=".$type_values{$parm_type};
        } else {
                my($parm_vartype)="";
                $parm_type = $type_override{$parm_type}
                                        if defined $type_override{$parm_type};
                $parm_vartype="INT"
                                        if  $parm_type =~ /int/i
                                or $parm_type =~ /float/i
                                or $parm_type =~ /double/i
                                or $parm_type =~ /decimal/i
                                or $parm_type =~ /numeric/i
                                or $parm_type =~ /money/i
                                or $parm_type =~ /real/i;
                $parm_vartype="DATE"
                                        if  $parm_type =~ /date/i;
                $parm_vartype="CHAR"
                                        if $parm_type =~ /char/i
                                        or $parm_type =~ /binary/i;

                die "UNKNOWN DATATYPE $parm_type - please add to config file\n"
                                        unless defined $parm_vartype;

                $proc_cmd_line{$procname} .= " $parm_name=";
                $proc_cmd_line{$procname} .= "\"Jan 1 2000\"" if $parm_vartype eq "DATE";

                # IF DATE THEN SET TO str unless length <3
                if( $parm_vartype eq "CHAR" ) {
                        if( $parm_type =~ /char(1)/ or  $parm_type =~ /char(2)/  ) {
                                $proc_cmd_line{$procname} .= "\"X\"";
                        } else {
                                $proc_cmd_line{$procname} .= "\"str\"" ;
                        }
                }
                $proc_cmd_line{$procname} .= "1" if $parm_vartype eq "INT";
        }
        $proc_cmd_line{$procname} .= ","
}

#
# BUILD ANALYZE STRING
#
# OUTPUT IN DATA/SERVER/DB/analyze
my($outdir);
$outdir = "./".$opt_S."/".$opt_D unless defined $opt_o;
$outdir = $opt_o if defined $opt_o;

if( ! -d $outdir ) {
    mkpath( $outdir, 0, 0777 )
                or die "Cant Create Directory $outdir: $!";
}

my %redo_procs;
if( defined $opt_r ) {
        print "Finding bad files\n";
        search_out_htm("Server Msg");
        %redo_procs=%search_info;
}

if( ! defined $opt_s )  {
foreach (sort keys %proc_cmd_line) {

        # Remove trailing comma
        $proc_cmd_line{$_}=~ s/,$//;

        # format your command
        my($cmd)="$curdir/showplan_analyze.pl -Lilort -U$opt_U -P$opt_P -S$opt_S -D$opt_D -C'$proc_cmd_line{$_}'";
        $cmd .= " -h" if defined $opt_h;

        # print some stuff
        print "$count) $_\n" if ! defined $opt_s and ! defined $opt_r;
        print "$count) (norun) $proc_cmd_line{$_}\n"
                if defined $opt_s and ! defined $opt_r;

        next if defined $opt_r and ! defined $redo_procs{$_};
        print "$count) $proc_cmd_line{$_}\n" if defined $opt_r;

        system_to_file($outdir."/".$_.".$ext","Results of $_",$cmd)
                unless defined $opt_s;

        $count++;
}
}

#
# OK NOW SEARCH OUT SOME SUMMARIES
#
print "Summarizing showplan output\n";

# COUNT FAILURES
print "Searching for files with select/into\n";

my $num_bad_procs = search_out_htm("Server Msg");
my %bad_proc_list =%search_info;

my %select_into_procs;
foreach my $proc (sort keys %bad_proc_list) {
        print "Searching $proc";
        foreach ( split /\n/,$bad_proc_list{$proc} ) {
                if( /Server\s*Msg\s*226/ or /Server\s*Msg\s*2762/ or /Server\s*Msg\s*208/ ) {
                        $select_into_procs{$proc} = 1;
                        print " Select Into Found";
                        last;
                } else {
                        print "\n... ",$_;
                }
        }
        print "\n";
}

undef %search_info;
my $num_scans = search_out_htm("Table Scan");
$num_scans=0;
foreach my $file ( keys %search_info ) {
        foreach ( split /\n/,$search_info{$file} ) {
                next unless /STEP/;
                s/STEP\s\d\s*//;
                my $procedure = $file;
                $procedure =~ s/\.\w+$//;

                # if rows unavailable, add to list of scanned tables for the proc
                my($tbl,$rows);
                if( /UnAvailable/ ) {
                        m/:\s+([\#\w\.]+)\s+UnAvailable/;
                        $tbl=$1;
                        next if $tbl=~/Worktable/ or $tbl =~ /^#/;
                        $rows="NA";
                }  else {
                        # the data looks like => (INSERT): tbl Rows=xx Table Scan
                        # with multiple spaces between
                        # we would like table name and number of rows so this is our parse
                        m/:\s+(\w+)\s+Rows=(\d+)/;
                        ($tbl,$rows)=($1,$2);
                }

                if( ! defined $tbl or ! defined $rows ) {
                        #next if defined $bad_proc_list{$file};
                        print "------------------\n";
                        print "Possibe bad parse of $procedure... \n";
                        print "The parsed string was: (FORMAT->...: table Rows=xxx Table Scan\n";
                        print $_,"\n";
                        print "Tbl: $tbl Rows: $rows\n";
                        print "------------------\n";
                        next;
                }
                print "\tPROCEDURE $procedure TABLE $tbl ROWS $rows\n" if defined $opt_d;

                if( defined $tbl_scan_count{$tbl} ) {
                                $tbl_scan_count{$tbl} ++;
                                $tbl_scan_proc_data{$tbl.":".$procedure} ++;
                } else {
                                $tbl_scan_count{$tbl} =1;
                                $tbl_scan_proc_data{$tbl.":".$procedure} = 1;
                }

                if( defined $proc_scan_count{$procedure} ) {
                                $proc_scan_rows{$procedure} += $rows if $rows ne "NA";
                                $proc_scan_count{$procedure} ++;
                                $proc_scan_proc_data{$tbl.":".$procedure} ++;
                } else {
                                $proc_scan_rows{$procedure} = $rows if $rows ne "NA";
                                $proc_scan_count{$procedure} =1;
                                $proc_scan_proc_data{$tbl.":".$procedure} = 1;
                }
                $num_scans++;
        }
}

print "WARNING - The Stored Procedures Returned  NO Sybase Errors! This might be odd.  It either means you have a simple design or have tuned your parameters - normally you would expect at least one of your procedures to die when passed garbage parameters (as we are doing here).\n" if $num_bad_procs==0;
print "Number of Sybase Errors $num_bad_procs\n" if $num_bad_procs>0;
print "Number of Sybase Table Scans: $num_scans\n";

if( defined $opt_h ) {
        open( LOG,"> $outdir/index.html" ) or die("Unable To Open full_analysis.log");
        print LOG "<html><head><title>SHOWPLAN ANALYSIS</title></head>";
        print LOG "<BODY>";
        print LOG "<H1>Showplan Analysis of Server $opt_S Database $opt_D</H1>";
        print LOG "The showplan analyzer loops through all procedures in
your database and tries to analyze them.  Each procedure is run in a
transaction (so it can be rolled back) and using made up parameters
(unless these parameters are specified in the override file).  If a parameter
allows null, null is used.  The only way it could fail is if the procedures
contain select into statements, use temp tables, or use create table
statements, none of which are allowed in transactions.  There is really no
work around for this, because the tool should be NON DESTRUCTIVE, which means
the tool must roll back all commands when done.  In other words, this process
will result in no modification to your database (besides adding some records
to the log which will be rolled back). Some procedures will generate errors
(because they contain select into's) and some procedures might not run the
way they do in production (because parameters are bad).<br>";

        $count=$#proclist+1;
        print LOG "<br>$count procedures analyzed<br>\n";
        print LOG "$num_bad_procs procedures generated sybase errors or contained select intos<br>\n";
} else {
        open( LOG,"> full_analysis.log" ) or die("Unable To Open full_analysis.log");
}

foreach (keys %tbl_scan_count) { $tbl_scan_procs{$_}=""; }
foreach (keys %proc_scan_count) { $proc_scan_procs{$_}=""; }

foreach (keys %tbl_scan_proc_data) {
        my($tbl,$prc)=split ":",$_;
        $tbl_scan_procs{$tbl}.="<A HREF=$prc.$ext>$prc=>($tbl_scan_proc_data{$_})</A> ";
}

foreach (sort keys %proc_scan_proc_data) {
        my($tbl,$prc)=split ":",$_;
        $proc_scan_procs{$prc}.="$tbl=>($proc_scan_proc_data{$_}) ";
}

print LOG "<H2>Summary By Stored Procedure</H2>\n";
print LOG "<TABLE>\n";
print LOG "<TR><TH>Procedure</TH><TH>Num Scans</TH><TH>Rows Scanned</TH><TH>Tables Scaned and Num Times</TH></TR>\n";

foreach my $proc (sort @proclist) {
        if( defined $proc_scan_count{$proc} ) {
                print LOG "<TR><TD VALIGN=TOP><A HREF=\"$proc.$ext\">$proc</A></TD><TD VALIGN=TOP>$proc_scan_count{$proc}</TD><TD VALIGN=TOP>$proc_scan_rows{$proc}</TD><TD VALIGN=TOP>$proc_scan_procs{$proc}";

                if( defined $select_into_procs{$proc.".htm"} ) {
                        print LOG "<FONT COLOR=RED>" if defined $opt_h;
                        print LOG "SELECT INTO! ";
                        print LOG "</FONT>" if defined $opt_h;
                } elsif ( defined $bad_proc_list{$proc.".htm"} ) {
                        print LOG "<FONT COLOR=RED>" if defined $opt_h;
                        print LOG $bad_proc_list{$proc.".htm"};
                        print LOG "</FONT>" if defined $opt_h;
                }

                print LOG "</TD></TR>\n";
        } elsif( -e $outdir."/$proc.$ext" ) {
                # File Exists - Now print diff if it is a bad file or a good one
                if( defined $bad_proc_list{$proc.".htm"} ) {
                        print LOG "<TR><TD><A HREF=\"$proc.$ext\">$proc</A></TD><TD><TD><TD>",$bad_proc_list{$proc.".htm"},"</TD></TR>\n";
                } else {
                        print LOG "<TR><TD><A HREF=\"$proc.$ext\">$proc</A></TD></TR>\n";
                }
        } else {
                print LOG "<TR><TD>$proc</TD><TD><TD><TD>No File $outdir/$proc.$ext</TR>\n";
        }
}
print LOG "</TABLE>\n";

print LOG "<H2>Summary By Table</H2>\n";
print LOG "<TABLE>\n";
print LOG "<TR><TH>Table</TH><TH>Num Scans</TH><TH>Rows</TH><TH>Procedures and Number of Times</TH></TR>\n";
foreach (sort keys %tbl_scan_count) {
        print LOG "<TR><TD>$_</TD>
     <TD>$tbl_scan_count{$_}</TD>
     <TD>$table_rows{$_}</TD>
     <TD>$tbl_scan_procs{$_}</TD></TR>\n";
}
print LOG "</TABLE>\n";

print LOG "<hr>End Of Output<hr>
This output was created by the free program analyze.
<ADDRESS>copyright &#169 1997-1998 By
<A HREF=\"http://www.edbarlow.com\">SQL Technologies</A>
<BR>
We appreciate
<A HREF=\"mailto:barlowedward\@hotmail.com\">feedback</A>.
on this product!
</ADDRESS>" if defined $opt_h;

print LOG "</BODY>\n" if defined $opt_h;
close LOG;
print "Output in the directory: $outdir\n";
db_close_sybase();
exit 0;

sub read_config_file
{
        my($fname)=@_;
        print "Reading Configuration File $fname";
        open(DFILE,$fname) or die "Cant open $fname: $!";
        while ( <DFILE> ) {
                next if /^#/ or /^\s+$/;
                chop;
                if( substr($_,0,4) eq "TYPE" ){
                        s/^TYPE\s+//;
                        # ok we have a type
                        my($key,$val,$ovr)=split(/\s+/,$_,4);
                        $type_override{$key} = $val;
                        $type_values{$key} = $ovr;
                        print " Override for $key => $ovr\n" if defined $opt_d;
                } elsif( substr($_,0,8) eq "VARIABLE" ) {
                        s/^VARIABLE\s+//;
                        # ok we have a variable override
                        my($key,$val)=split(/\s+/,$_,3);
                        $vbl_override{$key} = $val;
                }
        }
        close DFILE;
        print " ... Completed\n";
}

sub system_to_file
{
   my($outfile,$heading,$string)=@_;

        $string =~ s/"/\\"/g;
        my($print_string)=$string;
        $print_string =~ s/-P\s*\w+/-PXXX/g;

   open( OUT,"> $outfile" ) || die("Unable To Open $outfile");
        print OUT "<H1><CENTER>$heading</CENTER></H1>\n\n"
                if defined $opt_h;
        print OUT "$heading\n\n" if ! defined $opt_h;
        #print OUT "Actual Command:<em>$print_string</em><br>\n";

   $string .= " 2>&1 |";

        print "Running $print_string\n";
        my($count)=0;
   open( SYS2STD,$^X." ".$string )  || die("Unable To Run  $string");
   while ( <SYS2STD> ) { $count++; print OUT $_; }
   close SYS2STD;
        print OUT "<hr>" if defined $opt_h;
        print OUT $count," Lines Returned\n";
        close OUT;
        print $count," Lines Returned\n";
}

# get files where analysis failed
# Looks for Server Msg String
sub search_out_htm
{
        my($srchstr)=@_;
        undef %search_info;

        my $files_found=0;
        die "No Outdir" unless defined $outdir;

   opendir(DIR,$outdir)
                        || die("Can't open directory $outdir for reading\n");
   my(@dirlist) = grep( /\.htm$/ ,readdir(DIR));
   closedir(DIR);

        foreach my $file (@dirlist) {
                print " Searching $file\n" if defined $opt_d;
                open( HFILE,$outdir."/".$file ) or die "Cant open $file: $!\n";
                while ( <HFILE> ) {
                        next unless /$srchstr/i;
                        $file =~ /\.htm/;
                        # remove html tags and clean up line
                        chomp;
                        s/<[\/\w\s=]*>/ /g;
                        $files_found++ unless defined $search_info{$file};
                        $search_info{$file}.=$_."\n";
                }
                close HFILE;
        }
        return $files_found;
}

__END__

=head1 NAME

full_analysis.pl - Analyze a whole database using showplans

=head2 DESCRIPTION

This program acts as driver to the analyze.pl program, which performs a
showplan analysis on a single stored procedure.  Runing this program will
analyzes a whole database.  Output can be ascii or html, but the program
runs best if used with html output (that is how it is tested and html lets
me do pretty things).

Information is summarized into a nice web page which gives a synopsis of
your database.  This includes analysis that helps you identify table scans.
You can drill down into your procedures and see detailed analysis of these
procedures.

This procedure loops through all stored procs in a db and runs the analyze
package on each of them.  Output is placed in files in a subdirectory
SERVER/DB in either the document area (if B<-h> is passed) or the data
subdirectory of the current directory.

An index.htm file is placed in .../analysis/SERVER/DB with appropriate
pointers to the other files created.  If analysis is not in your tree
it is created (must have write permissions) as are the subdirectories.

=head2 INSTALLATION

The extended stored procedure library must be installed

User defined data types should be set up in the full_analysis.dat file. What this means is that the program will substitute values for parameters to each of the stored procedures it runs.  It does this by making something up.  For strings, it usually uses "str", unless it is less than a varchar(3), in which case it will use "X".  One problem here is user defined data types.  Obviously the program needs to know whether the type is a string... Sometimes your stored procs expect certain values for particular parameters.  You can set these values also.  After you do a run, you will see errors, which can be fixed by editing the full_analysis.cfg file.  As you work with this tool, you will build a database of default values for your procs.

=head2 USAGE

        ./full_analysis [-ds -hdir] -UUSER -SSERVER -PPASS -Ddb

        -d debug mode
        -h document root of optional web server
        -s summarize only (dont run procs)
        -v verbose ( more verbose than normal less than -d )

This procedure loops through all stored procs in a db and runs the analyze package on each of them.  Output is placed in files or can be html.

Output is in SERVER/DB in either the document area (if B<-h>) or data subdirectory.

You may ignore B<-U> and B<-P> options if you have set up password file

=cut

