# ==================================================================
# SYSMON FUNCTIONS
# ==================================================================
use strict;
use vars qw(%section_count %item_count %item_data %item_keys $opt_d $rundate $runtime $item_name $srvversion @rawsysmon $section_name $section_line $section_num $last_subitem $srvname $interval  %section_count  );

sub s_read_sysmon_inline
{
   my($msg) = @_;
	$_ = $msg;
   chomp;

   push @rawsysmon, $_;				  # save raw input
   return if /^\s*$/;
   return if /\-\-\-\-\-\-\-\-\-/;

   s/\s+$//g;
	s/%//g;
   s_debugmsg( "\tread: $_" );

   $section_line++;

   if( /\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=/ ) {

		#
		# FOUND NEW SECTION
		#
      s_debugmsg("\tsection divider found" ) if defined $opt_d;
      $section_num++;
      $section_name   = "";
      $section_line   = 0;
      $item_name   = "";
      $last_subitem= "";

   } elsif( $section_num <=2 ) {

      s_debugmsg("\tstartup section" ) if defined $opt_d;
		s/://;

      if( /Run Date/ ) {
	 		$rundate = $_ ;
	 		$rundate =~ s/Run Date:*\s*//;
      }

      if( /Server Version:/ ) {
	 		$srvversion = $_ ;
	 		$srvversion =~ s/Server Version:*\s*//;
      }

      if( /Server Name:/ ) {
	 		$srvname  = $_ ;
	 		$srvname =~ s/Server Name:*\s*//;
      }

      if( /Statistics Sampled at/ ) {
	 		$runtime  = $_ ;
	 		$runtime =~ s/Statistics Sampled at:*\s*//;
      }

      if( /Sample Interval/ ) {
	 		$interval = $_ ;
	 		$interval =~ s/Sample Interval:*\s*//;
	 		$interval =~ s/min.//;
	 		$interval =~ s/\s//;
      }

   } elsif( $section_line == 1 ) {

		#
		# YOU HAVE THE SECTION NAME
		#
      # some of the sections have garbage at the end
      s/ment[\s\w\%]+/ment/;
		s/^\s+|\(|\)|\s+$//g;
      $section_name  = $_;
      s_debugmsg("\tFound section heading => $section_name" )
			if defined $opt_d;
      $section_line++;
      $item_name   = "";
		$last_subitem= "";

   } elsif( $section_name eq "Housekeeper Task Activity" ) {
		return;

   } elsif( /^  \w/ or /^ \w/ ) {

		s/\s\s*/ /g;

		#
		# 1 or 2 spaces - ITEM
		#
		my($ItemLineText)=$_;

		# extract first field (words?) which is item name

		s/^\s+|\(|\)|\s+$//g;
      s/\s*per sec[\W\w]*//;	    # remove stuff from per sec to right
      s.n/a..g;
      s/ [\%\.\s\d]+//g;
      s/:[\s\w]*//g;

      s_quit( "Cant Parse Down $ItemLineText" ) if $_ eq "";

      my($first_field)=$_;
      $item_name = $first_field;

		# we need to handle special items here...
		if( $section_name eq "Disk I/O Management" ){
			return if /Total I\/Os/;
			if( $first_field  =~ /Device/ ) {
				$item_name=$first_field;
				return;
			}
		}

		# Ok we have an item name (probably) and maybe some data
		# WARN OF DUPS HERE
		if( defined $item_data{$section_name.":".$item_name}
		and $item_data{$section_name.":".$item_name} ne "") {
			statusmsg("WARNING DUPLICATE SECTION" );
			statusmsg("\tName=>$section_name      Item =>$item_name" );
			#statusmsg("\t\tDATA ".$item_data{$section_name.":".$item_name}."\n" );
			#statusmsg("\t\tADDING  $ItemLineText\n" );

			$section_count{$section_name}++;
			$item_count{$section_name.":".$item_name}++;
			$item_data{$section_name.":".$item_name} .= $ItemLineText."\n";
			$item_keys{$section_name.":".$item_name} .= $_."\n";
		} else {
			$section_count{$section_name}=0
	    		unless defined $section_count{$section_name};
			$item_count{$section_name.":".$item_name}=0
	    		unless defined $item_count{$section_name.":".$item_name};
			$item_data{$section_name.":".$item_name}=""
				unless defined $item_data{$section_name.":".$item_name}
				and $ItemLineText !~ /per sec/;

			$section_count{$section_name}++;
			$item_count{$section_name.":".$item_name}++;
			$item_data{$section_name.":".$item_name} .= $ItemLineText."\n"
				unless $ItemLineText=~/per sec/;
			$item_keys{$section_name.":".$item_name}="";
			s_debugmsg("\tnew item |$section_name->$item_name|" );
			s_debugmsg("\titem data=>".join("|",split(/\n/,$item_data{$section_name.":".$item_name} )));
		}
   } else {

		print "DBG: No spaces at start of $_\n" if /^\S/;

		#
		# NO SPACES AT START OF LINE OR > 2 SPACES
		#
		# Note - if no data then dont save item - store it for next time
		#
		return if /per sec/;
		s/\s\s*/ /g;

		if( $item_name eq "Device" ) {
			$item_name.= ": ".$_;
			$section_count{$section_name}=0
	    		unless defined $section_count{$section_name};
			$item_count{$section_name.":".$item_name}=0
	    		unless defined $item_count{$section_name.":".$item_name};
			$item_data{$section_name.":".$item_name}=""
				unless defined $item_data{$section_name.":".$item_name};
			return;
		}

		s/^\s+|\(|\)|\s+$//g;
		my $data = $_;
		s/\s*\d*\d\.\d[\w\W]*$//g;
		s/n\/a[\w\W]*$//g;

		if(/^\s*$/) {
			# use last subitem if you dont have any text
			print "DBG: found special case in $last_subitem => $data\n"
				if defined $opt_d;
			$data = $last_subitem;
			$_ = $last_subitem . " ". $_;
		};

		$last_subitem=$data;

      s_quit( "Cant Save $data - current item name is null\n" )
			if $item_name eq "";
      $section_count{$section_name}++;
      $item_count{$section_name.":".$item_name}++;
      $item_data{$section_name.":".$item_name} .= $data."\n";
		$item_keys{$section_name.":".$item_name} .= $_."\n";
      s_debugmsg("\tappend data for |$section_name->$item_name|" );
      s_debugmsg("\titem data=>".join("|",split(/\n/,$item_data{$section_name.":".$item_name} )));
   }
}

1;
