package  Win32Scheduler;

#ppm install Win32-TaskScheduler --location=http://taskscheduler.sourceforge.net/perl/
#ppm install http://taskscheduler.sourceforge.net/perl58/Win32-TaskScheduler.ppd

use   vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

use 	strict;
use 	Carp;
use 	Win32::TaskScheduler;
use	File::Basename;
use	CommonFunc;
#use	Data::Dumper;

require  Exporter;
@ISA     = qw(Exporter);
@EXPORT  = qw(get_a_task get_all_task schedule_a_job change_host deschedule_a_job);

$VERSION = '1.0';

#############################
# GLOBAL VARIABLES          #
#############################
my($i_am_initialized)="FALSE";

##############################################################################
# METHOD ARGUMENTS                                                           #
# Set to 1 if Required, optional if 0.  key is func:value	                 #
# all method arguments must be in this array which is used for validation    #
##############################################################################

my($scheduler) = Win32::TaskScheduler->New();

my %hourTrigger = (
 'MinutesInterval'=>60,
 'MinutesDuration'=>1440,
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'StartHour' => 1,
 'StartMinute' => 1,
 'TriggerType' =>  $scheduler->TASK_TIME_TRIGGER_DAILY,
 'Type' => {
		'DaysInterval'=>1,
	}
);

my %dailyTrigger = (
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'StartHour' => 1,
 'StartMinute' => 1,
 'TriggerType' =>  $scheduler->TASK_TIME_TRIGGER_DAILY,
 'Type' => {
		'DaysInterval'=>1,
	}
);

my %weeklyTrigger = (
 'MinutesInterval'=>0,
 'MinutesDuration'=>0,
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'StartHour' => 14,
 'StartMinute' => 10,
 'TriggerType' =>  $scheduler->TASK_TIME_TRIGGER_WEEKLY,
 'Type' => {
		'DaysOfTheWeek'=>1,
		'WeeksInterval'=>1,
	}
);

my %bootTrigger = (
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'TriggerType' =>  $scheduler->TASK_EVENT_TRIGGER_AT_SYSTEMSTART,
);

my(%OK_OPTS)= (
	"schedule_a_job:-debug"		=> 0,
	"schedule_a_job:-name"		=> 1,
	"schedule_a_job:-schedule"	=> 1,
	"schedule_a_job:-account"	=> 1,
	"schedule_a_job:-password"	=> 1,
	"schedule_a_job:-enabled"	=> 0,
	"schedule_a_job:-program"	=> 1,
	"schedule_a_job:-directory"	=> 0,
	"schedule_a_job:-name_prefix"	=> 0,
	"deschedule_a_job:-name_prefix"	=> 0,
	"deschedule_a_job:-name"	=> 0,
	"schedule_a_job:-start_hour"	=> 0,
	"schedule_a_job:-start_minutes"	=> 0,
	"schedule_a_job:-end_hour"	=> 0,
	"schedule_a_job:-end_minutes"	=> 0,
	"schedule_a_job:-frequency"	=> 0,
	"get_a_task:-name"		=> 1,
);

sub new {
	my($class)=shift;
	my(%args)=@_;
	my $self = bless { %args }, $class;
	return $self;
}

sub change_host {
	my($package,$host)=@_;
	my($rc)=$scheduler->SetTargetComputer($host);
	print "Cant set target computer to $host: $!" unless $rc==1;
	die "Cant set target computer to $host: $!" unless $rc==1;
}

sub deschedule_a_job {
	my($package)=shift;
	my(%args)=@_;
	validate_params("deschedule_a_job",%args);
	if( ! defined $args{-name} ) {
		my($tasks)=$package->get_all_tasks();
		my($err)="";
		foreach( keys %$tasks ) {
			print "Descheduling Task $_\n";
			$scheduler->Delete($_)
				or $err .= "Can Not Delete Job $args{-name}\n";
		}
		return $err;
	} elsif( defined $args{-name} ) {
		my($tasks)=$package->get_all_tasks();
		my($found)="FALSE";
		foreach( keys %$tasks ) {
			next unless $_ eq $args{-name}.".job";
			$found="TRUE";last;
		}
		if( $found eq "TRUE" ) {
			print "Descheduling Task $args{-name}\n";
			$scheduler->Delete($args{-name}.".job") or return "Can Not Delete Job $args{-name}";
			return "";
		} else {
			print "Can Not Deschedule Task $args{-name} - Not Found\n";
			my($str);
			foreach( keys %$tasks ) { $str.="\t$_\n"; }
			return "Task $args{-name} Not Found - Available tasks:\n$str";
		}
	}
}

sub schedule_a_job {
	my($package)=shift;
	my(%args)=@_;
	validate_params("schedule_a_job",%args);
	return "Cant Schedule on Unix/Linux\n" unless is_nt();
	if( defined $args{-debug} ) {
		print "-debug passed to schedule_a_job: Printing Arguments\n";
		foreach ( keys %args) { print " $_ => ",$args{$_},"\n" unless /^-password$/; }
	}

	#$package->change_host($args{-host}) if defined $args{-host};
	$args{-name} = $$package{-name_prefix}.$args{-name}
		if 	defined $$package{-name_prefix}
		and 	$args{-name}!~/^$$package{-name_prefix}/;
	$args{-directory}= basename($args{-program}) unless $args{-directory};

	my @tasks = $scheduler->Enum();
	my($task_exists)="FALSE";
	foreach (@tasks) {
		next unless $_ eq $args{-name}.".job";
		$task_exists="TRUE";
	}

 	return "Error Task Exists\n" if $task_exists eq "TRUE";

	my($trigger);
	if( $args{-schedule} eq "hourly" 
			or $args{-schedule} eq "frequently" 
			or $args{-schedule} eq "Every15Min" 
			or $args{-schedule} eq "Every10Min"  ) {
		$trigger = \%hourTrigger;
		if( defined $args{-end_hour} 	and 	defined $args{-end_minutes}
		and defined $args{-start_hour} 	and 	defined $args{-start_minutes} ) {
			$$trigger{MinutesDuration} =
					60 * $args{-end_hour}
					+ $args{-end_minutes}
					- 60 * $args{-start_hour}
					- $args{-start_minutes};
	 	}
	 	$$trigger{MinutesInterval} = $args{-frequency} if defined $args{-frequency};
		$$trigger{MinutesInterval} = 15 if $args{-schedule} eq "frequently";
		$$trigger{MinutesInterval} = 15 if $args{-schedule} eq "Every15Min";
		$$trigger{MinutesInterval} = 10 if $args{-schedule} eq "Every10Min";
		$$trigger{StartMinute}=int(rand(10)) if $$trigger{StartMinute}==1;
	} elsif( $args{-schedule} eq "saturday-am" or $args{-schedule} eq "saturday-pm" ) {
		$trigger = \%weeklyTrigger;
		$$trigger{Type}->{DaysOfTheWeek}= $scheduler->TASK_SATURDAY;		
	
	#	$$trigger{StartMinute}=int(rand(10));		
	#	$$trigger{StartHour}=int(rand(15));
	
		#foreach ( keys %args) { print " $_ => ",$args{$_},"\n" unless /^-password$/; }	
		#use Data::Dumper;
		#print Dumper $trigger,"\n";
	
	} elsif( $args{-schedule} eq "sunday-am" or $args{-schedule} eq "sunday-pm" ) {
		$trigger = \%weeklyTrigger;
		$$trigger{Type}->{DaysOfTheWeek}= $scheduler->TASK_SUNDAY;		
	#	$$trigger{StartMinute}=int(rand(10));
	#	$$trigger{StartHour}=int(rand(15));
	} elsif( $args{-schedule} eq "weekly" ) {
		$trigger = \%weeklyTrigger;
		$$trigger{StartMinute}=int(rand(10));
	} elsif( $args{-schedule} eq "daily" or $args{-schedule} eq "nightly" ) {
		$trigger = \%dailyTrigger;
		$$trigger{StartMinute}=int(rand(10));
	} elsif( $args{-schedule} eq "boot" ) {
		$trigger = \%bootTrigger;
	} elsif( $args{-schedule} eq "never" ) {
		$trigger = \%weeklyTrigger;
		#$$trigger{Flags}='DISABLED';
		#$$trigger{Next_Run_Time}='Disabled'
	} else {
		print "ERROR schedule must be hourly,weekly,daily,nightly, or boot not $args{-schedule}\n";
		die "schedule must be hourly,weekly,daily,nightly, or boot not $args{-schedule}\n";
	}

	$$trigger{'StartHour'} =  $args{-start_hour} if defined $args{-start_hour};
 	$$trigger{'StartMinute'}= $args{-start_minutes} if defined $args{-start_minutes};

# 		$scheduler->Activate($args{-name});
# 		my($trigger_count)=$scheduler->GetTriggerCount();
# 		my($rc)=$scheduler->SetTrigger(0,$trigger);
# 		if( $rc == 0 ) {
#			die "Cant allocate new trigger for $args{-name}\n";
#		} elsif( $rc == -1 ) {
#			print "trigger invalid\n";
#			foreach ( keys %$trigger ) {	print ".. $_ => $$trigger{$_} \n"; }
#			die "The Trigger supplied to $args{-name} is not valid\n";
#		}

 	my($rc)=$scheduler->NewWorkItem($args{-name},$trigger);
	if( $rc == 0 ) {
		print "Cant allocate new trigger for $args{-name}\n";
		die "Cant allocate new trigger for $args{-name}\n";
	} elsif( $rc == -1 ) {
		print "trigger invalid\n";
		foreach ( keys %$trigger ) {	print ".. $_ => $$trigger{$_} \n"; }
		print "The Trigger supplied to $args{-name} is not valid\n";
		die "The Trigger supplied to $args{-name} is not valid\n";
	}

	$rc=$scheduler->SetApplicationName($args{-program});
	warn "Cant Set Application Name" if $rc==0;
   	$rc=$scheduler->SetWorkingDirectory($args{-directory});
   	warn "Cant Set Working Directory" if $rc==0;
   	$rc=$scheduler->SetAccountInformation($args{-account},$args{-password});   #the SYSTEM account will be used
 	warn "Cant Set account information (rc=$rc)\n" unless $rc==1;								# if "" , "

	if( $args{-schedule} eq "never" or $args{-enabled}==0 ) {
		#print "disabling task\n";
		$rc=$scheduler->SetFlags($scheduler->TASK_FLAG_DISABLED);
		print "Error - faied to disable job" if $rc==0;
	}

	$rc=$scheduler->Save();
	warn "Cant Save Job ($!)\n" if $rc==0;

   	# reset triggers that we have changed (or may have)
   	$hourTrigger{MinutesDuration} = 1440;
   	$hourTrigger{MinutesInterval} = 60;

   	$weeklyTrigger{Flags}=0;
	delete $weeklyTrigger{Next_Run_Time};

   	return "";
}

sub get_all_tasks {
	my($package)=shift;
	my(%task_list);
	return \%task_list unless is_nt();
	my(%args)=@_;
	validate_params("get_all_tasks",%args);
	my @tasks = $scheduler->Enum();

	foreach my $task (@tasks) {
		next if defined $$package{-name_prefix} and $task!~/^$$package{-name_prefix}/;
		$task_list{$task}=$package->get_a_task(-name=>$task);
	}
	return \%task_list;
}

sub get_a_task {
	my($package)=shift;
	my(%args)=@_;
	validate_params("get_a_task",%args);

	my(%results);
   $scheduler->Activate($args{-name});

	$results{Creator}=$scheduler->GetCreator();
	$results{Comment}=$scheduler->GetComment();
	$results{UserName} = $scheduler->GetAccountInformation();
	$results{Application} = $scheduler->GetApplicationName();
	$results{Parameters} = $scheduler->GetParameters();
	$results{Working_Directory} = $scheduler->GetWorkingDirectory();

	my($p);
	$scheduler->GetPriority($p);
	$results{Priority}=$p;

	$p=$scheduler->GetFlags();
	$results{Flags} ="";
	$results{Flags}.="INTERACTIVE " if $p=~$scheduler->TASK_FLAG_INTERACTIVE;
	$results{Flags}.="DISABLED " if $p=~$scheduler->TASK_FLAG_DISABLED;
	$results{Flags}.="DELETE_WHEN_DONE " if $p=~$scheduler->TASK_FLAG_DELETE_WHEN_DONE;
	$results{Flags}.="HIDDEN " if $p=~$scheduler->TASK_FLAG_HIDDEN;

      	$scheduler->GetStatus($p);
	$results{Status}=$p;
      	$scheduler->GetExitCode($p);
	$results{ExitCode}=$p;

      	my(@time)=$scheduler->GetNextRunTime();
      	$results{Next_Run_Time}= $time[7].$time[6].$time[4]." $time[3]:$time[2]:$time[1]";
      	$results{Next_Run_Time}= "Disabled"
			if $results{Next_Run_Time} eq "000 0:0:0";
	@time=$scheduler->GetMostRecentRunTime();
	$results{Last_Run_Time}= $time[7].$time[6].$time[4]." $time[3]:$time[2]:$time[1]";
      	$results{Last_Run_Time}= "Never"
		if $results{Last_Run_Time} eq "000 0:0:0";

	$results{MaxRunTime} = $scheduler->GetMaxRunTime();

	my($trigger_count)=$scheduler->GetTriggerCount()-1;
	$results{trigger_count}=$trigger_count;
	for my $i (0..$trigger_count) {
		my(%trigger);
		next if $scheduler->GetTrigger($i,\%trigger)==0;
		$trigger{in_english}=$scheduler->GetTriggerString($i);
		$results{"trigger $i"}=\%trigger;

	}

	return \%results;
}

sub validate_params
{
	my($func,%OPT)=@_;
	# check for bad keys
	foreach (keys %OPT) {
		next if defined $OK_OPTS{$func.":".$_};

		# did you mix up name=>x with -name=>x
		croak "ERROR: Incorrect $func Arguments - Change $_ argument to -$_\n"
			if defined $OK_OPTS{$func.":-".$_};

		croak "ERROR: Function $func Does Not Accept Parameter Named $_\n";
	}

	# check for missing required keys
	foreach (keys %OK_OPTS) {
		next unless /^$func:/;
		next unless $OK_OPTS{$_} == 1;
		$_=~s/$func://;
		next if defined $OPT{$_};
		croak "ERROR: Function $func Requires Parameter Named $_\n";
	}

	return %OPT;
}

1;

__END__

=head1 NAME

Win32Scheduler.pm - Scheduling Module For Windows and Unix

=head2 DESCRIPTION

This module can set schedules for batchs on unix and windows.
The information as to what you wish to schedule is saved in the
gem.xml configuration file.

This module is acutally very simple.  It only allows one time schedule
(trigger) per job and only permits jobs to run under some very basic
criterion.

=head2 FUNCTIONS

get_a_task -
	-name	=> task name

get_all_tasks -
	returns a hash of hashes from the job scheduler.  Keys of primary hash are job names.  Keys of subhash are:

	Creator
	Comment
	UserName
	Application
	Parameters
	Working_Directory
	Priority

	Flags ="";
	Flags.="INTERACTIVE " if $p=~$scheduler->TASK_FLAG_INTERACTIVE;
	Flags.="DISABLED " if $p=~$scheduler->TASK_FLAG_DISABLED;
	Flags.="DELETE_WHEN_DONE " if $p=~$scheduler->TASK_FLAG_DELETE_WHEN_DONE;
	Flags.="HIDDEN " if $p=~$scheduler->TASK_FLAG_HIDDEN;

   Status
   ExitCode
   Next_Run_Time
   Last_Run_Time= $time[7].$time[6].$time[4]." $time[3]:$time[2]:$time[1]";
	MaxRunTime
	trigger $i=\%trigger;

schedule_a_job
	 -name
	 -schedule	hourly, weekly, daily, boot
	 -account
	 -password
	 -program	the program you wish to run
	 -directory	working directory
	 -start_hour
	 -start_minutes

	 the following only apply to the hourly schedule and are optional
	 -end_hour
	 -end_minutes
	 -frequency

new
	argument : -name_prefix=>prefix : this prefix applies to all jobs you care about.  This can be
	           used to ignore other scheduled jobs that are on the system.  If -name_prefix=>xx_,
	           then only jobs starting with xx_ will be considered
