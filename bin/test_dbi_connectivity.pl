#!/usr/local/bin/perl-5.6.1

use strict;
use DBI;
use Socket;			 # needed for gethostbyaddr
use Getopt::Long;

$|=1;

my($helpstring)="This simple utility will diagnose DBI connectivity issues.
Pass In --SERVER/-LOGIN/-PASSWORD or --ALL for all registered servers.
The utility will connect and report on common errors.  If you prefer, add
--DEBUG to see internal DBI trace information. \n";

sub usage {
	warn @_;
	die "usage : test_dbi_connectivity.pl --ALL
	or
usage : test_dbi_connectivity.pl --SERVER=xxx --LOGIN=xxx --PASSWORD=xxx --DEBUG
   or
usage : test_dbi_connectivity.pl --SERVER=xxx --LOGIN=xxx --PASSWORD=xxx --DEBUG --QUERY
$helpstring";
}
use vars qw( $SERVER $LOGIN $PASSWORD $DEBUG $ALL $QUERY);
usage()  unless    GetOptions(  
				"SERVER=s"	=> \$SERVER,
   			"LOGIN=s"	=> \$LOGIN,
   			"QUERY=s"	=> \$QUERY,
   			"ALL=s"		=> \$ALL,
   			"DEBUG"		=> \$DEBUG,
				"USER=s"		=> \$LOGIN,
				"PASSWORD=s"	=> \$PASSWORD	);

if( ! defined $ALL ) {
	usage( "Must Pass SERVER\n" )			unless defined $SERVER;
	usage( "Must Pass LOGIN\n"  )			unless defined $LOGIN;
	usage( "Must Pass PASSWORD\n" ) 		unless defined $PASSWORD;
}

if( $ALL ) {
	use Repository;
	foreach ( get_password( -type=>$ALL )) {
		my($LOGIN,$PASSWORD)=get_password( -type=>$ALL, -name=>$_ ); 
		my($rc)=do_connection($_,$LOGIN,$PASSWORD);
	}
	print "Exit Code 0\n";
	exit(0);
} 

my($rc)=do_connection($SERVER,$LOGIN,$PASSWORD);
exit($rc);

sub do_connection {
		  my($SERVER,$LOGIN,$PASSWORD)=@_;
		  my($CType);
		  if( is_nt() ) {
			  $CType="ODBC";
			  print "Running on Windows - Checking ODBC Settings\n";

			  my(@av_drivers)=DBI->available_drivers();
			  my($found)=0;
			  foreach (@av_drivers) { $found=1 if $_ eq "ODBC"; }
			  error_out( "DBI ODBC Driver not found - found ",join(" ",@av_drivers),"\n"  ) unless $found;
			  my($dsn_list);
			  eval {  DBI->data_sources("ODBC");	};
			  if( $@ ) {
				  error_out( "Errors loading ODBC : $@\n" );
			  } else {
				  $found=0;
				  my(@dat)= grep(s/DBI:ODBC://i,DBI->data_sources("ODBC"));
				  foreach (@dat) {
					  $found++ if $_ eq "$SERVER";
				  }
				  error_out( "$SERVER is not set up as an odbc server\nAllowed Servers=",join(" ",sort @dat),"\n") unless $found;
			  }

		  } else {
			  $CType="Sybase";
			  print "Testing Sybase server $SERVER\n";
			  error_out( "SYBASE Environment variable is not set" )
					  unless defined $ENV{SYBASE};
			  error_out( "SYBASE Environment variable is not valid" )
					  unless -d $ENV{SYBASE};

			  my($IF_FILE)	= "$ENV{SYBASE}/interfaces"	if -r "$ENV{SYBASE}/interfaces";
			  $IF_FILE	= "$ENV{SYBASE}/ini/sql.ini"	if -r "$ENV{SYBASE}/ini/sql.ini";

			  error_out( "SYBASE variable ($ENV{SYBASE}) appears incorrect.\nInterfaces file does not exist" )
					  unless -r $IF_FILE;
			  my(@rc)=dbi_get_interfaces($SERVER,$IF_FILE);
			  error_out( "Apparently your sybase server ($SERVER) is not in your interfaces file\n" ) if $#rc<0;
		}

		DBI->trace(2) if defined $DEBUG;

		print "Testing  connectivity to $SERVER\n";
		my($dbh)= DBI->connect("dbi:$CType:$SERVER",$LOGIN,$PASSWORD);
		if( defined $dbh ) {
			  print "Connected to $SERVER\n";
			  my($ary_ref)=$dbh->selectall_arrayref("select count(*) from master..sysobjects where name='sysprocesses'");
			  my($ok)=0;
			  foreach my $row (@$ary_ref) {
				  $ok++ if $$row[0] == 1;
			  }
			  if( $ok == 0 ) {
				  warn "-- Testing Failed\n";
				  foreach my $row (@$ary_ref) {
					  warn "--->",join(" ",@$row)," \n";
				  }
			  } else {
				  print "SUCCESSFUL: Testing Completed Successfully\n";
			  }
			  $dbh->disconnect();
			  return 0;
		} else {
			  error_out( "FALIED TO CONNECT\n" );
			  return 1;
		}
}

sub error_out {
	my($str)=join("",@_);
	$str=~s/\n/\n* /;
	die "********************************************\n* ERROR: ",$str,"********************************************\n";
}

################################## SUBROUTINES ###########################
sub is_nt {
	my($is_nt)=0;
	$is_nt=1 if defined $ENV{PROCESSOR_LEVEL};
	return $is_nt;
}
my(%found_name);
sub dbi_get_interfaces
{
	my($search_string,$filename)=@_;
	error_out( "SYBASE ENV not defined") unless defined $ENV{"SYBASE"};

	my(@srvlist,%port,%host,%found_svr);

	if( defined $filename and ! -e $filename ) {
		print "Warning: File $filename Not Found\n";
	}

	if( -e $ENV{"SYBASE"}."/interfaces" or -e $filename ) {
		$filename=$ENV{"SYBASE"}."/interfaces" unless defined $filename;

		# UNIX
		open FILE,$filename or error_out( "Cant open $filename : $!" );
		my($srv,$dummy);
		while( <FILE> ){
			next if /^#/;
			chomp;

			if( /^\s/ ) {
				next unless /query/;
				next if $srv eq "";
				chomp;

				my(@vals)=split;

				# Sun Binary Format
				if( $vals[4] =~ /^\\x/ ) {
					#format \x
					$vals[4] =~ s/^\\x0002//;

					my($p) = hex(substr($vals[4],0,4));
					my($pk_ip) = pack('C4',
						hex(substr($vals[4],4,2)),
						hex(substr($vals[4],6,2)),
						hex(substr($vals[4],8,2)),
						hex(substr($vals[4],10,2)));

					my($name,$aliases,$addrtype,$len,@addrs);
					if( defined $found_name{$pk_ip} ) {
						$name=$found_name{$pk_ip};
					} else {
						($name,$aliases,$addrtype,$len,@addrs) = gethostbyaddr($pk_ip,AF_INET);
						$found_name{$pk_ip}=$name if defined $name and $name !~ /^\s*$/;
					}

					my($h) =$name ||
						hex(substr($vals[4],4,2)).".".
						hex(substr($vals[4],6,2)).".".
						hex(substr($vals[4],8,2)).".".
						hex(substr($vals[4],10,2));

					if( defined $host{$srv}  ) {
						$host{$srv}.="+".$h;
						$port{$srv}.="+".$p;
					} else {
						$host{$srv}=$h;
						$port{$srv}=$p;
					}
				} else {
					$port{$srv} = $vals[4];
					$host{$srv} = $vals[3];
				}

				push @srvlist,$srv unless defined $found_svr{$srv};
				$found_svr{$srv} = 1;
				# $srv="";
			} else {
				($srv,$dummy)=split;
			}
		}
		close FILE;

	} elsif( -e $ENV{"SYBASE"}."/ini/sql.ini"  ) {

		# DOS
		# fix up sybase env vbl
		$ENV{"SYBASE"} =~ s#\\#/#g;
		open FILE,$ENV{"SYBASE"}."/ini/sql.ini"
			or error_out( "Cant open file ".$ENV{"SYBASE"}."/ini/sql.ini: $!\n" );
		my($cursvr)="";
		while( <FILE> ){
			next if /^#/;
			next if /^;/;
			next if /^\s*$/;
			chop;

	#
	# MUST ALSO HANDLE BIZARROS LIKE THE FOLLOWING
	#
	#[NYSISW0035]
	#$BASE$00=NLMSNMP,\pipe\sybase\query
	#$BASE$01=NLWNSCK,nysisw0035,5000
	#MASTER=$BASE$00;$BASE$01;
	#$BASE$02=NLMSNMP,\pipe\sybase\query
	#$BASE$03=NLWNSCK,nysisw0035,5000
	#QUERY=$BASE$02;$BASE$03;

			if( /^\[/ ) {
				# IT A SERVER LINE
				s/^\[//;
				s/\]\s*$//;
				s/\s//g;
				$cursvr=$_;
				push @srvlist,$_ unless defined $found_svr{$_};
				$found_svr{$_} = 1;
			} else {
				# IT A DATA LINE
				next if /^master=/i;
				next if /QUERY=\$BASE/i;
				next if /\,.pipe/i;
				next if /NLMSNMP/i;
				(undef,$host{$cursvr},$port{$cursvr})=split /,/;
			}
		}
		close FILE;
	} elsif( defined $filename ) {
		error_out( "$filename error" );
	} else {
		error_out( "UNABLE TO DETERMINE INTERACE FILE" );
	}

	my(@rc);
	foreach (sort @srvlist ) {
		if(defined $search_string) {
			next unless /$search_string/i
				or $host{$_} =~ /$search_string/i
				or $port{$_} =~ /$search_string/i;
		}
		push @rc,[ $_,$host{$_},$port{$_} ];
	}
	return @rc;
}
