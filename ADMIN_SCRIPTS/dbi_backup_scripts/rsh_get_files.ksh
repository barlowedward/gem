#!/bin/sh
#

# Find the perl on the remote system
PERL=`rsh $1 "ls /usr/local/bin/perl 2>/dev/null"`
[ -z "$PERL" ] && PERL=`rsh $1 "ls /bin/perl 2>/dev/null"`
[ -z "$PERL" ] && PERL=`rsh $1 "ls /usr/bin/perl 2>/dev/null"`

rsh $1 $PERL \
-e \'my\(\$dir\)=\"$2\"\;\' \
-e \'die \"ERROR No Directory On Remote System \$dir\\n\" unless -d \$dir\;\' \
-e \'opendir\(D,\$dir\)\;\' \
-e \'foreach \(readdir\(D\)\) { \' \
-e		\'next if \$_ eq \"\.\" or \$_ eq \"\.\.\"\;\' \
-e		\'print \$_,\" \",\(-M \$dir.\"/\$_\"\),\" \",\(-s \$dir.\"/\$_\"\),\"\\n\"\;\' \
-e \'}\' \
-e \'closedir\(D\)\;\'

exit $?
