: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
use Do_Time;
use CommonFunc;

use vars qw( $USER $SERVER $PASSWORD $OUTDIR $DEBUG);

$| =1;

die ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"USER=s"	=> \$USER,
		"PASSWORD=s" 	=> \$PASSWORD,
		"OUTDIR=s" 	=> \$OUTDIR,
		"DEBUG"      	=> \$DEBUG );

die ("Must pass server\n" ) 		unless defined $SERVER;
die ("Must pass username\n" )	  	unless defined $USER;
die ("Must pass password\n" )	  	unless defined $PASSWORD;
die ("Must pass out directory\n" ) 	unless defined $OUTDIR;
die ("Must $OUTDIR not a directory\n" ) unless -d $OUTDIR;

my(%queries);

$queries{ADMIN_WHO}	= 'admin who';
$queries{ADMIN_WHO_SQM} = 'admin who,sqm';
$queries{ADMIN_WHO_SQT} = 'admin who,sqt';
$queries{ADMIN_STAT_SYSMON} = 'admin statistics, sysmon';
$queries{ADMIN_STAT_MEM} = 'admin statistics, mem_in_use';
$queries{ADMIN_STAT_AGENT} = 'admin statistics, repagent';

my($TYPE)="Sybase";
$TYPE="ODBC" if is_nt();

my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD, -type=>$TYPE);
if( ! $rc ) {
	die "OOPS - CANT CONNECT TO SERVER $SERVER as use $USER\n";
}

my($tstamp)=do_time(-fmt=>'yyyymmdd.hhmi');

foreach my $q (keys %queries) {
	print "Creating $OUTDIR/$q.$tstamp.csv\n";
	open(OUT,"> $OUTDIR/$q.$tstamp.csv") or die "Cant write $OUTDIR/$q.$tstamp.csv\n";
	print OUT $q,"\n";
	my(@rc)= dbi_query(-db=>"", -query=>$queries{$q}, -print_hdr=>1, -debug=>$DEBUG);
	@rc=dbi_reformat_results(@rc);
	foreach (@rc) {
		s/\n/ /g;
		print OUT $_,"\n";
	}
	close(OUT);
}
print "Process Completed\n";

__END__

=head1 NAME

dump_sybase_rep_server.pl - dump sybase rep server state for investigation

=head2 USAGE

	dump_sybase_rep_server.pl -USER=USER -SERVER=SERVER -PASSWORD=PASS -OUTDIR=dir

=head2 DESCRIPTION

Creates dump of rep server info in .csv format that can be used to interogate server state in the event of
an emergency.  Basically it creates a set of .csv files that you can browse (with excel) to see what
was going on at the time of the “incident”.  This includes very detailed process information – like admin who,sqt and
other basic rep server queries.  It creates output that needs to be pieced together – but when
something weird happens and you have no time for diagnostics because you need to fix production, this is the
program to run (quickly) – before you fix the problem.  Takes about 1 seconds to run.

=cut
