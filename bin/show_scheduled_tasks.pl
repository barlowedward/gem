use strict;
use Getopt::Long;
use CommonFunc;

my($PROGRAM_DESCRIPTION)="Show Task Schedule";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG $html $showreal );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" 
unless GetOptions( "DEBUG"	=> \$DEBUG, 
	"showreal"	=> \$showreal,
	"html" => \$html );

print "$0 - v",$VERSION,"\n" unless $html;

sub _statusmsg { 	print @_; }

use Win32Scheduler;
use JobScheduler;
use Sys::Hostname;

my $scheduler=new Win32Scheduler(-name_prefix=>'gem_');
my $real_scheduled_jobs=$scheduler->get_all_tasks();

my($job_scheduler) = fetch_jobscheduler();			# ALARM SCHEDULE DATA
die "Must Run on Win32" unless is_nt();
	
print "MASTER INTERFACE TO WINDOWS SCHEDULER\n" unless $html;

if( $html ) {
	
	while( <DATA> ) {
		next if /^\s*$/;
		my($job,$txt)=split(/\s+/,$_,2);		
		warn "$job\n" unless defined $$job_scheduler{$job};		
		$$job_scheduler{$job}->{Description}=$txt;
	}	
		
	$$job_scheduler{"Sybase_CheckServer_*"}->{Description} = 'Is your server ok?';
	$$job_scheduler{"Sybase_CheckServer_*"}->{frequency} = 'Every15Min';
	$$job_scheduler{"Sybase_CheckServer_*"}->{ok_types} = 'Every15Min';
	$$job_scheduler{"Sybase_CheckServer_*"}->{command} = 	"\\monitoring\\CheckServer.pl --BATCH_ID=CheckServer_<\$svr> -TIME=<TIME> --SERVER=<\$svr> \n",
			
	
	$$job_scheduler{"Mssql_CheckServer_*"}->{Description} = 'Is your server ok?';
	$$job_scheduler{"Mssql_CheckServer_*"}->{frequency} = 'Every15Min';
	$$job_scheduler{"Mssql_CheckServer_*"}->{ok_types} = 'Every15Min';
	$$job_scheduler{"Mssql_CheckServer_*"}->{command} = 	"\\monitoring\\CheckServer.pl --BATCH_ID=CheckServer_<\$svr> -TIME=<TIME> --SERVER=<\$svr> \n",
	
	report_htmlscheduler($job_scheduler);
	
	
} elsif( $showreal ) {
	print "Dumping Schedule Information\n";
	report_realjobscheduler($real_scheduled_jobs);
} else {
	print "Dumping Scheduler Information\n";
	report_jobscheduler($job_scheduler);
}

# print "Saving Scheduler Information\n";
# save_jobscheduler($job_scheduler);

# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "SUCCESSFUL: $PROGRAM_DESCRIPTION\n"  unless $html;
close_gemconfig();

exit(0);

__DATA__

INV_GetEtcPasswd				Get /etc/password files
INV_LoadYpcatHosts			Ypcat Hosts
INV_MysqlDatabaseAuditor			Change Auditor for Mysql
INV_OracleDatabaseAuditor			Change Auditor for Oracle
INV_SqlSvrDatabaseAuditor			Change Auditor for Sql Server
INV_SybaseDatabaseAuditor			Change Auditor for Sybase
INV_WinDsget						Dsget
INV_WinDsgetComputer				Dsget Computer
INV_WinGetSystemUsers			Get Windows Users
INV_YpcatExtractor				Ypcat Hosts
Mssql_Auditor						Audit Mysql
Mssql_BkpChk_ByProc			Check Backups - By Stored Procedure
Mssql_BkpChk_Evening				Check Backups - Evening Procedure
Mssql_BkpChk_Hourly			Check Backups - Hourly Procedure
Mssql_BkpChk_Morning			Check Backups - Morning Procedure
Mssql_BkpChk_Report			Check Backups - Create Report 
Mssql_BkpChk_Weekly			Check Backups - Weekly Procedure
Mssql_CheckPersistent			Persistent Monitor for Sql Server
Mssql_CheckServerDaily		Config Audit - Daily

Mssql_ClusterManager			Check its still running on the same cluster
Mssql_ClusterManagerInit	Detect New clusters
Mssql_CycleEventLogs			Cycle Event Logs
Mssql_DrChecker				Check DR Configuration
Mssql_JobStatusRpt			Sql Server Job Scheduler Check
Mssql_LogMaintPlan			Look at log maint plans
Mssql_Ping						Ping MSSQL servers
Mssql_ScheduledJobRpt		SCheduled Jobs
Mssql_Shrinklogs				Shrink Logs
Mssql_SpaceMonitor			Space Monitor	
Mssql_StoredProcUpdater			Ensure that all stored procedure libraries are at the latest revision	
Mssql_TruncateMaster			Truncate Master Logs
Mssql_UpdateStatsMsdb			Update Stats on MSDB	
Oracle_CheckPersistent			Persistent Monitor For Oracle
Sybase_Auditor					Audit Sybase
Sybase_BackupServer				Check Sybase Backup Servers
Sybase_BkpCheck_Report			Sybase Backup Server Output Report
Sybase_CheckPersistent			Check Persistent Sybase
Sybase_CheckServerDaily			Daily Check of Sybase - Configuration Audit

Sybase_FetchLogFiles				Fetch Lob FIles
Sybase_LockedLoginDetector		Checks for locked logins and alerts on new ones		
Sybase_Ping			Monitor Your Sybase Servers via ping	
Sybase_RepCompare			Compare Production Replication To Target
Sybase_RepMonAgent			Check Rep Agents
Sybase_RepMonCheck			Check Replication
Sybase_RepMonReport			Report On Replication
Sybase_RepServer			Check Sybase REplication Server State
Sybase_RepServerChecker			Check Latency Of Replication
Sybase_SpaceMonitor1			Sybase Space Monitor	
Sybase_SpaceMonitor2			Sybase Space Monitor	
Sybase_SpaceMonitor3			Sybase Space Monitor	
Sybase_SpaceMonitor4			Sybase Space Monitor	
Sybase_SpaceMonitor5			Sybase Space Monitor	
Sybase_SpaceMonitorRW			Sybase Space Monitor	- Write historical info
Sybase_StoredProcUpdater			Ensure that all stored procedure libraries are at the latest revision	
Sybase_ThresholdManager			Run Threshold Manager	
Sybase_ThresholdManagerReinstall			Reinstall Threshold Manager for databases that have changed in size	

Sybase_TruncateMaster				Truncate logs of the master db
Sybase_TruncateSybsystemdb			Truncate logs of the sysbsystemdb db
Sybase_TruncateSybsystemprocs			Truncate logs of the sybsystemprocs db
Sys_Agent							Agent Report
Sys_AlarmDbCleanup			       				Internal cleanup of the gemalarms database	       
Sys_AlarmRoutingAgent					Read Alerts & Route Them	       
Sys_BackupReport						General Backup Report
Sys_Backups								Collect Backup Infor
Sys_BlackoutReport					Report On blacked out servers
Sys_ConsoleArchiver					Archive the console 
Sys_DirectoryChecker					Check Directories
Sys_DnsMonitor							Monitor Dns Servers
Sys_MailGemErrors				Mail Gem Error Report to the Admin
Sys_PortMonitor			Test Services (sybase servers, win32 services...)	
Sys_ProductionErrors				Report on Production Server Errors
Sys_ProductionList				Report on prodcution Servers
Sys_ProductionWarnings			Report on Production Server Warnings
Sys_TestDfltConnectivity			Test SSH Connectivity
Sys_TraceRoute						TraceRoute
Unix_BackupStateStaticInfo			Fill BackupState table for Unix Databases
Unix_CleanupFiles			Cleanup Files
Unix_Console_15min				Console Jobs - every 15 minutes
Unix_Console_Daily			Console Jobs - Daily
Unix_Console_Hourly			Console Jobs - Hourly
Unix_Console_Promote			Console Jobs - Promote
Unix_Console_Weekly			Console Jobs - Weekly
Unix_Errors						Report on Unix Errors
Unix_FileCheck					Check File Existence
Unix_FileCleanup				Cleanup FIles
Unix_GetDiskSpace				Get Unix Disk Space via df -k
Unix_Ping						Ping Unix Systems
Unix_Warnings					Unix Warnings Report
Win32_BackupStateStaticInfo			Fill BackupState table for Windows Databases
Win32_CheckBackupDirs			Check Backup Directories
Win32_CleanupFiles			Cleanup windows files
Win32_Console_15min				Console Jobs - every 15 minutes
Win32_Console_Daily			Console Jobs - Daily
Win32_Console_Hourly			Console Jobs - Hourly
Win32_Console_Promote			Console Jobs - Promote
Win32_Console_Weekly			Console Jobs - Weekly
Win32_CreateBackupDirs			Create Backup Directories as needed
Win32_Diskspace				Monitor Windows Diskspace
Win32_Diskspace_Rpt			Report on Windows Diskspace
Win32_Eventlog				Read Event logs
Win32_Eventlog_Rpt		Report On Eventlogs
Win32_FileCheck			Check FIle existence Based On Configfile
Win32_FileCleanup			Cleanup Files Based On ConfigFIle
Win32_GetHosts			Get Hosts on windows
Win32_HostsRpt				Fetch Win32 Host Information	
Win32_Ping					Pings your Win32 Hosts	
Win32_Security_Rpt			Static Windows Server Security Report
Win32_ServiceChecker			Monitors Windows Services	
Win32_ServiceCheckerUpdate			Append to Services List for any new servers	
Win32_TaskScheduler          				Monitors Windows Task Scheduler	

