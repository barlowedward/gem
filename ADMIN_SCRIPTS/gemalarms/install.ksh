#!/bin/sh
SERVER=CTSYBMON

PERL=/usr/local/bin/perl-5.8.2
if [ -z "$1" ]
then
	echo "USAGE $0 PASSWORD OBJECT"
	exit
fi

if [ -z "$2" ]
then
$PERL ../bin/do_release.pl -Usa -S$SERVER -P$1 -Dgemalarms *.tbl *.trg *.vie *.prc *.ins *.sql
else
PASS=$1
shift
$PERL ../bin/do_release.pl -Usa -S$SERVER -P$PASS -Dgemalarms $* permissions.sql
fi
