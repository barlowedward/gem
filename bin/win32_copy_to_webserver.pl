use File::Copy;
use Getopts;
use Sys::Hostname;
use CommonFunc;

sub is_localhost {	# return 1 if host is localhost
	my($h)=@_;
	
	return 1 if ! defined $h or ! $h or $h eq "localhost" or $h eq "127.0.0.1";
	
	my $host = hostname();
	$host	  =~ s/\.[\w\.]+//;
	
	my $thost= $h;
	$thost  =~ s/\.[\w\.]+//;
	
	return 1 if $host eq $thost();
	return 0;
}

sub copy_file_from {
	my($fromfile,$fromhost,$tofile)=@_;
	if( is_localhost($fromhost) ) {
		copy( $fromfile, $tofile );		
	} else {
	}
}
sub copy_file_to {
	my($fromfile,$tohost,$tofile)=@_;
	if( is_localhost($tohost) ) {
		copy( $fromfile, $tofile );
	} else {
	}
}
sub copy_directory_from {
	my($fromdir,$fromhost,$todir)=@_;
}
sub copy_directory_to {
	my($fromdir,$tohost,$todir)=@_;
}

sub rebuild_alarm_file {
	my($file)=@_;	
	_statusmsg("[configure] Rebuilding Alarm File $file\n");	
	open(A77,"$file")	  or return l_warning("Cant Read $file : $!");
	open(B76,">$file.tmp") or return l_warning("Cant Write $file.tmp : $!");
	my($foundstr)='FALSE';
	foreach (<A77>) {
		if( /^my\(\$server,\$login,\$pass/ ) {
			$foundstr='TRUE';
			print B76 'my($server,$login,$password,$db)=("'.$GemConfig->{ALARM_SERVER}.'","'.
					$GemConfig->{ALARM_LOGIN}.'","'.
					$GemConfig->{ALARM_PASSWORD}.'","'.
					$GemConfig->{ALARM_DATABASE}.'");'."\n";
		} else {
			print B76 $_;
		}
	}
	close(A77);
	close(B76);
	
	l_die( "ERROR - COULD NOT FIND KEY STRING IN $file\n" ) if $foundstr eq "FALSE";
	#if( compare($file.".tmp",$file) != 0 ) {
	#	_statusmsg("[configure] File Modified Successfully\n");
		archive_file($file,5);
		rename($file.".tmp",$file) or return l_warning("Cant rename $file.tmp to $file : $!");
	#} else {
	#	_statusmsg("[configure] File Unchanged\n");
	#	unlink $file.".tmp";
	#}
	_statusmsg("[configure] Rebuild completed\n");		
}
