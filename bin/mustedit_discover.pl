# Database Surveyer
#
# copyright (c) 2010 by Edward Barlow
#
# Surveys & Autodiscovers Stuff a Dba Needs
#
# It all requires appropriate db variables
#   like SYBASE and ORAHOME and whatever
#
# Sadly no autodiscovery of mysql is possible.
# does NOT randomly port ping but does use all
# available information from your system.
#

use 	strict;
use 	DBI;
use   DBIFunc;
use   File::Copy;
use   Sys::Hostname;
use 	Carp qw/confess/;
use 	Getopt::Long;
use   IO::Socket;
use   File::Basename;
use	Repository;
use	CommonFunc;
use   DBI_discovery;
use   Inventory;

my(%TYPEMAP)=(
	'sybase' => 'unix',
	'oracle' => 'win32',
	'mysql'  => 'unix',
	'sqlsvr' => 'win32',
);
my @configuration_errors;
my  $curdir=dirname($0);
die "You Must Map Samba Shares To A Drive Letter.\nCurdir=$curdir\n"
	if $^O =~ /MSWin32/ and $curdir =~ /\\\\/;
chdir($curdir) or die "Cant cd to $curdir: $!\n";

my($PROGRAM_NAME)	=	'discover.pl';
my($HOSTNAME)		=	hostname();
my(%found_name);

use vars qw( $IS_GEM $DEBUG $FAST $UPDATECACHE $USEVAULT $VAULT $DIAGNOSE $CREATECFGFILES );

sub usage	{
	print "Usage Error : $PROGRAM_NAME\n
Current Argument List:
  GEM         reads the gem configuration files as a source of passwords
  DEBUG       diagnostic mode
  DIAGNOSE    super diagnostic mode
  UPDATECACHE force rediscovery of underlying data
  CREATECFGFILES
  USEVAULT
   ";
   return "\n";
}

$| =1;    			# unbuffered standard output
die usage("Bad Parameter List\n") unless GetOptions(
		"GEM"					=> \$IS_GEM,
		"UPDATECACHE"		=> \$UPDATECACHE,

		"CREATECFGFILES"		=> \$CREATECFGFILES,
		"DEBUG"				=> \$DEBUG 	,
		"FAST"				=> \$FAST 	,
		"DIAGNOSE"			=> \$DIAGNOSE,
		"USEVAULT"			=> \$USEVAULT
		);

#####################################################
#
# FINAL OUTPUT VARIABLE DEFINITIONS
#
#####################################################

# THE FOLLOWING REPRESENT A DATABASE
#   THE DB can either come from ODBC settings or from Native (sybase,osql -l...)
#   We will attempt a recon later

my(%ServerType_List_By_DbName);			# ; separated list of servers on this host
											# populated via add_server_to_host
											# host name is in uppercase?

my(%HASH_OF_DSN_AND_NATIVE);		# for looping
my(%Is_Dsn_By_DbName);
my(%Is_Native_By_DbName);
my(%Realtype_By_DbName);
my(%Ip_By_DbName);
my(%InternalName_By_DbName);
my(%Port_By_DbName);
my(%Hostname_By_DbName);
my(%Description_By_DbName);

# THE FOLLOWING REPRESENT HOSTS
my(%HostSave_By_UcName);	 # if defined, then its a host you want to save in the config files
my(%HostType_By_Ip);			 # UNIX|WIN32 By Ip
my(%HostType_By_UcName);	 # type=WIN32 | UNIX
                       		 # source net view & ssh/known_hosts
                       		 # used as key for our list of interesting HOSTs
my(%DriveInfo_By_Ip);
my(%Host_Comment_By_Ip);
my(%Registered_Hostname_By_Ip); # Registered_Hostname_By_Ip is the perferred name to use for the host

# The following info is keyed on Hostname as defined by ypcat
# data may be rotated (ie besthostname changed) if needed
my(%IPFAIL,%ipcache);	# hashes for ip caches
								# ipcache is set by ypcat hosts
								# keyed on uc($HOST)



my(%ServerMetadata);					# MetaData by DSN fetched from DB Internals
my(%IS_SQLSERVER);					# list of all sql servers
# my(%IS_VHOST);						# a list of all vhostnames
my(%CLUSTERNODE_OF_By_Ip);		# a list of all clusternodes
my(%DBI_HASHREF);						# a hash of available DSN's

#
# GEM INFORMATION
#
my($password_vault);							# password information from the vault
my($GEM_ROOT_DIR);							# gem root directory if gem
my($GEMCONFIG);								# data from gem.dat

my(@sybase_svrs_inpassfile);
my(@sqlsvr_svrs_inpassfile);
my(@mysql_svrs_inpassfile);
my(@oracle_svrs_inpassfile);
my(@unix_svrs_inpassfile);
my(@win32servers_svrs_inpassfile);
my(%saved_passfile_info);			# hash of anything  saved in the password file

############################### END VARIABLES ######################

header( "Starting $PROGRAM_NAME at ".localtime()."\n" );
$GEM_ROOT_DIR=get_gem_root_dir();
_statusmsg("  GEM             => $IS_GEM\n");
_statusmsg("  GEM_ROOT_DIR    => $GEM_ROOT_DIR\n");
_statusmsg("  UPDATECACHE     => $UPDATECACHE\n") if $UPDATECACHE;
_statusmsg("  CREATECFGFILES  => $CREATECFGFILES\n") if $CREATECFGFILES;
_statusmsg("  DEBUG           => $DEBUG    \n") if $DEBUG;
_statusmsg("  DIAGNOSE        => $DIAGNOSE\n") if $DIAGNOSE;
_statusmsg("  FAST            => $FAST\n") if $FAST;

if( $IS_GEM ) {
	header("Reading GEM Configuration (the --IS_GEM command line option is SET)");
	$GEMCONFIG=read_gemconfig(-debug=>$DEBUG);
	if( ! defined $GEMCONFIG ) {
		print "The GENERIC ENTERPRISE MANAGER IS NOT CONFIGURED\n";
		print "Please Run configure.pl prior to running $PROGRAM_NAME\n";
		exit(1);
	}

	if( $GEMCONFIG->{INSTALLSTATE} ne "INSTALLED" ) {
		die "THIS GEM CONFIGURATION's STATE IS ",$GEMCONFIG->{INSTALLSTATE},"\nPlease run configure.pl to install an et it to INSTALLED\n";
	}

	_statusmsg( "Using Gem Configuration Value - USE_ORACLE = ",		$GEMCONFIG->{USE_ORACLE},"\n" );
	_statusmsg( "Using Gem Configuration Value - USE_SYBASE_ASE = ",	$GEMCONFIG->{USE_SYBASE_ASE},"\n" );
	_statusmsg( "Using Gem Configuration Value - USE_MYSQL = ",			$GEMCONFIG->{USE_MYSQL},"\n" );
	_statusmsg( "Using Gem Configuration Value - USE_SQLSERVER = ",	$GEMCONFIG->{USE_SQLSERVER},"\n" );

	_statusmsg( "Using Gem Configuration Value - NT_MONITOR_SERVER   = ",
			$GEMCONFIG->{NT_MONITOR_SERVER},"\n" );
	_statusmsg( "Using Gem Configuration Value - UNIX_MONITOR_SERVER = ",
			$GEMCONFIG->{UNIX_MONITOR_SERVER},"\n" );
	_statusmsg( "Using Gem Configuration Value - ALT1_MONITOR_SERVER = ",
			$GEMCONFIG->{ALT1_MONITOR_SERVER},"\n" );
	_statusmsg( "Using Gem Configuration Value - ALT2_MONITOR_SERVER = ",
			$GEMCONFIG->{ALT2_MONITOR_SERVER},"\n" );

	if( $HOSTNAME eq $GEMCONFIG->{NT_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Win32 Monitor Station\n") ;
	} elsif( $HOSTNAME eq $GEMCONFIG->{UNIX_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Unix Monitor Station\n") ;
	} elsif( $HOSTNAME eq $GEMCONFIG->{ALT1_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Win32 Monitor Station\n") ;
	} elsif( $HOSTNAME eq $GEMCONFIG->{ALT2_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Unix Monitor Station\n") ;
	} else {
		_statusmsg( "You are not running on a Monitor Station\n") ;
	}
	
	$password_vault = vault_read();		# password information from the vault

	_statusmsg("Reading GEM Password Files\n");
	@sybase_svrs_inpassfile			= get_password(-type=>'sybase')			if $GEMCONFIG->{USE_SYBASE_ASE} =~ /Y/;
	@sqlsvr_svrs_inpassfile			= get_password(-type=>'sqlsvr')			if $GEMCONFIG->{USE_SQLSERVER} =~ /Y/;
	@mysql_svrs_inpassfile			= get_password(-type=>'mysql')			if $GEMCONFIG->{USE_MYSQL} =~ /Y/;
	@oracle_svrs_inpassfile			= get_password(-type=>'oracle')			if $GEMCONFIG->{USE_ORACLE} =~ /Y/;
	@unix_svrs_inpassfile			= get_password(-type=>'unix');
	@win32servers_svrs_inpassfile	= get_password(-type=>'win32servers');
	
	my($x);
	$x=$#sybase_svrs_inpassfile+1; _statusmsg( "Read $x sybase servers\n" ) 	if $x>0;
	$x=$#sqlsvr_svrs_inpassfile+1; _statusmsg( "Read $x sqlsvr servers\n" ) 	if $x>0;
	$x=$#mysql_svrs_inpassfile+1; _statusmsg( "Read $x mysql servers\n" ) 		if $x>0;
	$x=$#oracle_svrs_inpassfile+1; _statusmsg( "Read $x oracle servers\n" ) 	if $x>0;
	$x=$#unix_svrs_inpassfile+1; _statusmsg( "Read $x unix hosts\n" ) 			if $x>0;
	$x=$#win32servers_svrs_inpassfile+1; _statusmsg( "Read $x win32 hosts\n" ) if $x>0;
	
	#print "\tSYBASE:\t",	join("\nSYBASE:\t",@sybase_svrs_inpassfile),			"\n" ;
	#rint "\nSQLSVR:\t",	join("\nSQLSVR:\t",@sqlsvr_svrs_inpassfile),			"\n";
	#print "\nMYSQL:\t",	join("\nMYSQL:\t",@mysql_svrs_inpassfile),			"\n";
	#print "\nORACLE:\t",	join("\nORACLE:\t",@oracle_svrs_inpassfile),			"\n";
	#print "\nUNIX:\t",	join("\nUNIX:\t",@unix_svrs_inpassfile),				"\n";
	#print "\nWIN32:\t",	join("\nWIN32:\t",@win32servers_svrs_inpassfile),	"\n";
	#die "DBGDBG";

} else {
	my(%values);
	$GEMCONFIG = \%values;
	$GEMCONFIG->{USE_ORACLE} 		= "Y";
	$GEMCONFIG->{USE_SYBASE_ASE}  = "Y";
	$GEMCONFIG->{USE_MYSQL} 		= "Y";
	$GEMCONFIG->{USE_SQLSERVER} 	= "Y";
}
die "NO GEM_ROOT_DIR" unless -d $GEM_ROOT_DIR;

fetch_dnsaliases();		# fetch ypcat hosts into %ipcache & %Registered_Hostname_By_Ip

header( "Fetching DBI DSN Information");
%DBI_HASHREF=fetch_dbi_data_sources();	# key=$DRIVER:$DSN
foreach ( keys %DBI_HASHREF ) {
	my($typ,$nm)=split(":",$_,2);
	$HASH_OF_DSN_AND_NATIVE{$nm}=1;
}

#
# USE WIN32::ODBC
#		use the powerful win32::odbc native module to pull in lots of stuff
# 		DBI_HASHREF: key=DRIVER:$DSN  updates value = STATE = DBIONLY|ODBCONLY|OK
#
if( $^O eq 'MSWin32' ) {
	header( "Fetching Win32 Native ODBC DSN Information" );
	fetch_win32_odbc();

	my($cfile) = get_writeable_discovery_file("DBI_hashref");
	open(CACHE2,">".$cfile) or die "Cant Write File $cfile";
	foreach ( sort ( keys %DBI_HASHREF )) {
		print CACHE2 $_,";",$DBI_HASHREF{$_},";",
			$Realtype_By_DbName{$_},";",
			$Hostname_By_DbName{$_},";",
			$Port_By_DbName{$_},";",
			$InternalName_By_DbName{$_},";",
			$Description_By_DbName{$_},";",
			"\n";
	}
	close(CACHE2);
}

# use Data::Dumper;
# print Dumper \%DBI_HASHREF;
# print join("\n",get_dsn_list());
# exit();

#
# NET VIEW - run it before osql tests
#
header('Net View Information');
my(%x) = fetch_cmd_net_view();
my($cnt)=0;
foreach ( keys %x ) {
	$HostType_By_UcName{uc($_)} = "WIN32";
	$HostSave_By_UcName{uc($_)} = 1;
	my($ip)=get_ip_by_hostname($_);
	_debugmsg( "NO IP For $_ = Skipping \n") unless $ip;
	next unless $ip;
	$HostType_By_Ip{$ip}="WIN32";
	$Host_Comment_By_Ip{$ip} = $x{$_};
	$cnt++;
}
_statusmsg("Found $cnt Windows Systems From Net View\n");

#
# GET HOSTNAMES IN YOUR .SSH CONFIGURATION
#
header('SSH Info Information');
my(%ssh_rc) = fetch_ssh_profile();
$cnt=0;
foreach my $host ( keys %ssh_rc ) {

	if( $host=~/^\d+\.\d+\.\d+\.\d+$/ ) {
		my($host2) = get_hostname_by_ip($host);
		if( ! $host2 ) {
			_debugmsg( "Ignoring Ip Hostname $host\n");
			next;
		}
		$host=$host2;
	}
	if( $host=~/^\d+/ ) {
		_debugmsg( "Ignoring Host That starts with a number: $host\n");
		next;
	}
	$host =~ s/\..+$//;
	my($ps)=qs_do_ping_hostname($host);
	_debugmsg( "Ignoring Unpingable SSH Host $host\n") unless $ps;
	next unless $ps;

	my($ip)=get_ip_by_hostname($host);
	_debugmsg( "NO IP For $host = Skipping \n") unless $ip;
	next unless $ip;

	$cnt++;
	_diagmsg("SETTING $host TO UNIX\n");
	$HostType_By_UcName{uc($host)}='UNIX';
	$HostSave_By_UcName{uc($host)} = 1;
	$HostType_By_Ip{$ip}="UNIX";
	set_registered_hostname_for_host($host,1);

	# test some ssh if you are on unix
	#if( $^O ne 'MSWin32' ) {
	#	foreach( vault_get_credentials( "unix" ) ) {
	#		my($l,$p)=split(/;/,$_,2);
	#		_debugmsg( "DBGDBG NOT: testing ssh ",$l,'@',"$host \n" );
	#	}
	#}
}
_statusmsg("Completed Loading $cnt Unix hosts\n");

#
# NOW RUN OSQL -L
#
header( "Fetching Sql Servers" );
my($count)=0;
foreach my $ss ( fetch_cmd_osql_l() ) {
	next if $ss =~ /SQLEXPRESS/i;
	my($ip)=get_ip_by_hostname($ss);

	if( ! $ip ) {
		_statusmsg( "   NO IP For $ss = Skipping \n");
		next unless $ip;
	}

	$Port_By_DbName{$ss}=1433 unless $Port_By_DbName{$ss};
	$HASH_OF_DSN_AND_NATIVE{$ss}=1;
	
	_debugmsg( "Setting $ss As Native (from osql)\n") if $^O eq 'MSWin32';
	$Is_Native_By_DbName{$ss}=1 if $^O eq 'MSWin32';
	$Realtype_By_DbName{$ss}="sqlsvr" unless $Realtype_By_DbName{$ss};
	add_server_to_host( $ss, 'SQL_SERVER_OSQL' );

	$Ip_By_DbName{$ss}=$ip;
	$HostType_By_Ip{$ip}="WIN32";
	$Hostname_By_DbName{$ss} = get_registered_hostname_by_ip($ip) unless $Hostname_By_DbName{$ss};
	$IS_SQLSERVER{$ss} = "STANDALONE DATASERVER";
	$count++;

	# my($pp) = tcp_portping($ss,1433);		# NO IDEA WHY THIS DOES NOT WORK..
	#my($pp) = port_detect_is_sqlsvr($ss,1433);
	#if( $pp == 0 ) {
	#	warn "Oddly - Cant Ping 1433 on system $ss\n";
	#	_statusmsg( "Oddly - Cant Ping 1433 on system $ss\n" );
	#}

	# if this server name exists as a Host - that host is Win32 and Map It
	#if( defined $HostType_By_UcName{uc($ss)} ) {
	#	$HostType_By_UcName{uc($ss)} = "WIN32";
	#	$Hostname_By_DbName{$ss} = $ss;
	#}
}
_statusmsg("Found $count Sql Systems\n");

if( $GEMCONFIG->{USE_ORACLE} eq "Y" ) {
	header( "Fetching Oracle Native Information" );
	fetch_oracle_native_info();
	# DSN_save_database_information("ORACLE_FROM_TNSNAMES") if $DEBUG;
} else {
	_statusmsg("Skipping Oracle Config Per Setup Instructions\n");
}

if( $GEMCONFIG->{USE_SYBASE_ASE} eq "Y" ) {
	if( $ENV{SYBASE} and -d $ENV{SYBASE} ) {
		header( "Fetching Sybase Native Servers" );
		fetch_sybase_native_info();
		# DSN_save_database_information("Possible_Sybase_Server") if $DEBUG;
	} else {
		_statusmsg("Skipping Sybase Config SYBASE ENV NOT SET (\$SYBASE)\n");
	}
} else {
	_statusmsg("Skipping Sybase Config Per Setup Instructions\n");
}

dump_it(__LINE__);

#header("Set Ip For All Remaining Hosts");
#set_ip_addresses_for_all_hosts();

discover_win32();
discover_unix();

dump_it(__LINE__);

header( "Mapping Hosts To Registered Hosts\n" );
foreach my $hh ( @unix_svrs_inpassfile ) {
	set_registered_hostname_for_host($hh);
	$HostType_By_UcName{uc($hh)} = "UNIX";
	$HostSave_By_UcName{uc($hh)} = 1;
	my($ip)=get_ip_by_hostname($hh);
	_debugmsg( "NO IP For $hh = Skipping \n") unless $ip;
	next unless $ip;
	$HostType_By_Ip{$ip}="UNIX";
	my(@allhosts)=get_all_hostnames_by_ip($ip);
	foreach ( @allhosts ) {
		next if uc($_) eq uc($hh);
		_debugmsg("Dropping Unix Alias $_ in favor of registered host $hh\n");
		delete $HostType_By_UcName{uc($_)};
		delete $HostSave_By_UcName{uc($_)};
	}
}

dump_it(__LINE__);

foreach my $hh( @win32servers_svrs_inpassfile ) {
	set_registered_hostname_for_host($hh);
	$HostType_By_UcName{uc($hh)} = "WIN32";
	$HostSave_By_UcName{uc($hh)} = 1;
	my($ip)=get_ip_by_hostname($hh);
	_debugmsg( "NO IP For $hh = Skipping \n") unless $ip;
	next unless $ip;
	$HostType_By_Ip{$ip}="WIN32";
	my(@allhosts)=get_all_hostnames_by_ip($ip);
	foreach ( @allhosts ) {
		next if uc($_) eq uc($hh);
		_debugmsg("Dropping Win32 Alias $_ in favor of registered host $hh\n");
		delete $HostType_By_UcName{uc($_)};
		delete $HostSave_By_UcName{uc($_)};
	}
}

# each of these are GEM registered so journal any variables to this name that occurred in the old names.
header( "Mapping Databases To Registered Databases \n" );
_debugmsg("Sybase\n");
foreach my $nm ( @sybase_svrs_inpassfile ) {
	my($hostname) 	= $Hostname_By_DbName{$nm};
	my($ip) 			= $Ip_By_DbName{$nm};
	my(%info) 		= get_password_info(-name=>$nm, -type=>'sybase');
	$hostname		= $info{HOSTNAME} unless $hostname;
	$ip				= $info{IP} unless $ip;
	$ip 				= get_ip_by_hostname($hostname) unless $ip;
	$hostname      = get_registered_hostname_by_ip($ip) unless $hostname;
	$hostname      = get_hostname_by_ip($ip) unless $hostname;

	$saved_passfile_info{$nm}=\%info;
	$Ip_By_DbName{$nm} 				=	$ip;
	$Hostname_By_DbName{$nm} 		=	$hostname;
	$HostSave_By_UcName{uc($nm)} = 1;
	$HASH_OF_DSN_AND_NATIVE{$nm}	=	1;

	printf "SSYB Metadata for %20s: ip=%s host=%s isdsn=%2s nat=%2s typ=%s\n",$nm,$ip,$hostname,
				$Is_Dsn_By_DbName{$nm},$Is_Native_By_DbName{$nm},$Realtype_By_DbName{$nm};
	foreach my $ipnm ( keys %Ip_By_DbName ) {		
		next if $ipnm eq $nm;
		next if $ServerType_List_By_DbName{$ipnm} ne "sybase";
		next unless $ip eq $Ip_By_DbName{$ipnm} or $hostname eq $Hostname_By_DbName{$ipnm};
		_debugmsg("  combining $ipnm & $nm / $ServerType_List_By_DbName{$ipnm}==sybase / $ip==$Ip_By_DbName{$ipnm}\n");
		if( $HASH_OF_DSN_AND_NATIVE{$ipnm} ) {
			combine_two_dsns($ipnm,$nm);
		}
#		if( $HASH_OF_DSN_AND_NATIVE{$ipnm} and ! $HASH_OF_DSN_AND_NATIVE{$nm} ) {
#			_debugmsg("Depricating $ipnm and Replacing it with $nm based on GEM password file\n");
#	#		$HASH_OF_DSN_AND_NATIVE{$nm}	=	1;
#			$ServerType_List_By_DbName{$nm} 	=	$ServerType_List_By_DbName{$ipnm};
#			$Is_Dsn_By_DbName{$nm} 			=	$Is_Dsn_By_DbName{$ipnm};
#			$Is_Native_By_DbName{$nm} 		=	$Is_Native_By_DbName{$ipnm};
#			$Realtype_By_DbName{$nm} 		=	$Realtype_By_DbName{$ipnm};
#	#		$Ip_By_DbName{$nm} 				=	$ip;
#			$InternalName_By_DbName{$nm} 	=	$InternalName_By_DbName{$ipnm};
#			$Port_By_DbName{$nm} 			=	$Port_By_DbName{$ipnm};
#	#		$Hostname_By_DbName{$nm} 		=	$hostname;
#			$Description_By_DbName{$nm}	= 	$Description_By_DbName{$ipnm};
#			delete 	$HASH_OF_DSN_AND_NATIVE{$ipnm};
#			delete   $HostSave_By_UcName{$ipnm};
#		}
	}
	printf "ESYB Metadata for %20s: isdsn=%2s nat=%2s typ=%s\n",$nm,$Is_Dsn_By_DbName{$nm},$Is_Native_By_DbName{$nm},$Realtype_By_DbName{$nm};
}

_debugmsg("Mysql\n");
foreach my $nm ( @mysql_svrs_inpassfile ) {
	my($hostname) 	= $Hostname_By_DbName{$nm};
	my($ip) 			= $Ip_By_DbName{$nm};
	printf "SMYS Metadata for %20s: isdsn=%2s nat=%2s typ=%s\n",$nm,$Is_Dsn_By_DbName{$nm},$Is_Native_By_DbName{$nm},$Realtype_By_DbName{$nm};

	my(%info) 		= get_password_info(-name=>$nm, -type=>'mysql');
	$hostname		= $info{HOSTNAME} unless $hostname;
	$ip				= $info{IP} unless $ip;
	$ip 				= get_ip_by_hostname($hostname) unless $ip;
	$hostname      = get_registered_hostname_by_ip($ip) unless $hostname;
	$hostname      = get_hostname_by_ip($ip) unless $hostname;

	$saved_passfile_info{$nm}=\%info;
	$Ip_By_DbName{$nm} 				=	$ip;
	$Hostname_By_DbName{$nm} 		=	$hostname;
	$HASH_OF_DSN_AND_NATIVE{$nm}	=	1;
	$HostSave_By_UcName{uc($nm)} = 1;

	foreach my $ipnm ( keys %Ip_By_DbName ) {
		next if $ipnm eq $nm;
		next if $ServerType_List_By_DbName{$ipnm} ne "mysql";
		next unless $ip eq $Ip_By_DbName{$ipnm} or $hostname eq $Hostname_By_DbName{$ipnm};
		if( $HASH_OF_DSN_AND_NATIVE{$ipnm} ) {
			combine_two_dsns($ipnm,$nm);
		}
#		if( $HASH_OF_DSN_AND_NATIVE{$ipnm} and ! $HASH_OF_DSN_AND_NATIVE{$nm} ) {
#			_debugmsg("Depricating $ipnm and Replacing it with $nm based on GEM password file\n");
#			$Is_Dsn_By_DbName{$nm} 			=	$Is_Dsn_By_DbName{$ipnm};
#			$Is_Native_By_DbName{$nm} 		=	$Is_Native_By_DbName{$ipnm};
#			$ServerType_List_By_DbName{$nm} 	=	$ServerType_List_By_DbName{$ipnm};
#			$Realtype_By_DbName{$nm} 		=	$Realtype_By_DbName{$ipnm};
#			$InternalName_By_DbName{$nm} 	=	$InternalName_By_DbName{$ipnm};
#			$Port_By_DbName{$nm} 			=	$Port_By_DbName{$ipnm};
#			$Description_By_DbName{$nm}	= 	$Description_By_DbName{$ipnm};
#			delete 	$HASH_OF_DSN_AND_NATIVE{$ipnm};
#			delete   $HostSave_By_UcName{$ipnm};
#		}
	}
	printf "EMYS Metadata for %20s: isdsn=%2s nat=%2s typ=%s\n",$nm,$Is_Dsn_By_DbName{$nm},$Is_Native_By_DbName{$nm},$Realtype_By_DbName{$nm};

}

_debugmsg("Oracle\n");
foreach my $nm ( @oracle_svrs_inpassfile ) {
	my($hostname) 	= $Hostname_By_DbName{$nm};
	my($ip) 			= $Ip_By_DbName{$nm};
	
	my(%info) 		= get_password_info(-name=>$nm, -type=>'oracle');
	$hostname		= $info{HOSTNAME} unless $hostname;
	$ip				= $info{IP} unless $ip;
	$ip 				= get_ip_by_hostname($hostname) unless $ip;
	$hostname      = get_registered_hostname_by_ip($ip) unless $hostname;
	$hostname      = get_hostname_by_ip($ip) unless $hostname;

	$saved_passfile_info{$nm}=\%info;
	$Ip_By_DbName{$nm} 				=	$ip;
	$Hostname_By_DbName{$nm} 		=	$hostname;
	$HASH_OF_DSN_AND_NATIVE{$nm}	=	1;
	$HostSave_By_UcName{uc($nm)} = 1;

	foreach my $ipnm ( keys %Ip_By_DbName ) {
		next if $ipnm eq $nm;
		next if $ServerType_List_By_DbName{$ipnm} ne "oracle";
		next unless $ip eq $Ip_By_DbName{$ipnm} or $hostname eq $Hostname_By_DbName{$ipnm};
		if( $HASH_OF_DSN_AND_NATIVE{$ipnm} ) {
			combine_two_dsns($ipnm,$nm);
		}
#		if( $HASH_OF_DSN_AND_NATIVE{$ipnm} and ! $HASH_OF_DSN_AND_NATIVE{$nm} ) {
#			_debugmsg("Depricating $ipnm and Replacing it with $nm based on GEM password file\n");
#	#		$HASH_OF_DSN_AND_NATIVE{$nm}	=	1;
#			$ServerType_List_By_DbName{$nm} 	=	$ServerType_List_By_DbName{$ipnm};
#			$Is_Dsn_By_DbName{$nm} 			=	$Is_Dsn_By_DbName{$ipnm};
#			$Is_Native_By_DbName{$nm} 		=	$Is_Native_By_DbName{$ipnm};
#			$Realtype_By_DbName{$nm} 		=	$Realtype_By_DbName{$ipnm};
#	#		$Ip_By_DbName{$nm} 				=	$ip;
#			$InternalName_By_DbName{$nm} 	=	$InternalName_By_DbName{$ipnm};
#			$Port_By_DbName{$nm} 			=	$Port_By_DbName{$ipnm};
#	#		$Hostname_By_DbName{$nm} 		=	$hostname;
#			$Description_By_DbName{$nm}	= 	$Description_By_DbName{$ipnm};
#			delete 	$HASH_OF_DSN_AND_NATIVE{$ipnm};
#			delete   $HostSave_By_UcName{$ipnm};
#		}
	}
	printf "EORA Metadata for %20s: isdsn=%2s nat=%2s typ=%s\n",$nm,$Is_Dsn_By_DbName{$nm},$Is_Native_By_DbName{$nm},$Realtype_By_DbName{$nm};

}

_debugmsg("SqlSvr\n");
foreach my $nm ( @sqlsvr_svrs_inpassfile ) {
	my($hostname) 	= $Hostname_By_DbName{$nm};
	my($ip) 			= $Ip_By_DbName{$nm};
	
	my(%info) 		= get_password_info(-name=>$nm, -type=>'sqlsvr');
	$hostname		= $info{HOSTNAME} unless $hostname;
	$ip				= $info{IP} unless $ip;
	$ip 				= get_ip_by_hostname($hostname) unless $ip;
	$hostname      = get_registered_hostname_by_ip($ip) unless $hostname;
	$hostname      = get_hostname_by_ip($ip) unless $hostname;

	$saved_passfile_info{$nm}=\%info;
	$Ip_By_DbName{$nm} 				=	$ip;
	$Hostname_By_DbName{$nm} 		=	$hostname;
	$HASH_OF_DSN_AND_NATIVE{$nm}	=	1;
	$HostSave_By_UcName{uc($nm)} = 1;

	foreach my $ipnm ( keys %Ip_By_DbName ) {
		next if $ipnm eq $nm;
		next if $ServerType_List_By_DbName{$ipnm} ne "sqlsvr";
		next unless $ip eq $Ip_By_DbName{$ipnm} or $hostname eq $Hostname_By_DbName{$ipnm};
		if( $HASH_OF_DSN_AND_NATIVE{$ipnm} ) {
			combine_two_dsns($ipnm,$nm);
		}
	}
	printf "SQL Metadata for %20s: isdsn=%2s nat=%2s typ=%s\n",
			$nm,$Is_Dsn_By_DbName{$nm},$Is_Native_By_DbName{$nm},$Realtype_By_DbName{$nm};
	printf "SQL Metadata2: nm=%s hr=%20s type=%20s host=%20s port=%4s internal=%20s descr=%20s\n",
			$nm,
			$DBI_HASHREF{$nm},
			$Realtype_By_DbName{$nm},
			$Hostname_By_DbName{$nm},
			$Port_By_DbName{$nm},
			$InternalName_By_DbName{$nm},
			$Description_By_DbName{$nm};
}

dump_it(__LINE__);
header( "Analyzing Database/Service Connections" );
dbi_get_metadata();

dump_it(__LINE__);

# put together a list of cluster info
foreach my $s ( keys %ServerMetadata ) {
	next unless $ServerMetadata{ $s }->{IsClustered};
	next unless $ServerMetadata{ $s }->{ClusterNode};
	print "CLUSTER CHECK FOUND $s and $ServerMetadata{ $s }->{ClusterNode}\n";

	$IS_SQLSERVER{$s}="CLUSTERED DATASERVER";
	my($clusterip)=get_ip_by_hostname($s);
	die "WOAH - no clusterip " unless $clusterip;
	set_registered_hostname_for_host($s,1);
	
	foreach my $node ( split(/;/,$ServerMetadata{ $s }->{ClusterNode})) {
		_debugmsg( "Node $node is a cluster Node of $s\n" );

		my($ip)=get_ip_by_hostname($node);
		print __LINE__," DBGDBG Setting CLUSTERNODE_OF_By_Ip $ip=$s \n";
		$CLUSTERNODE_OF_By_Ip{$ip}=$s;
		delete $IS_SQLSERVER{$node};
		next unless $ip;
		combine_two_dsns($node,$s);

		if( $DriveInfo_By_Ip{$ip} ) {		# clusters only have C: drives
			_debugmsg("Journaling Drives of $s node $node/$ip to cluster\n");
			#die "ODDLY - Server $s has drive info of $DriveInfo_By_Ip{$clusterip} Which doesnt include C\$\n"
			#	if  $DriveInfo_By_Ip{$ip} !~ /c\$/i;

			my(@node_drives)    = split(/,/,$DriveInfo_By_Ip{$ip});
			my(@cluster_drives) = split(/,/,$DriveInfo_By_Ip{$clusterip});
			_debugmsg("Node Drives=",join(" ",@node_drives),"\n");
			_debugmsg("Clus Drives=",join(" ",@cluster_drives),"\n");

			# merge the drives
			my(%c);
			foreach( @node_drives )    { $c{$_}=1; }
			$DriveInfo_By_Ip{$clusterip} = "c\$" if $c{"c\$"};
			foreach( @cluster_drives ) { $c{$_}=1; }
			delete $c{"c\$"};
			$DriveInfo_By_Ip{$clusterip} = join(",", keys(%c));
		}
	}
}

dump_it(__LINE__);

# PRINT AND FIX UP STUFF
foreach my $DSN ( get_dsn_list() ) {
	_debugmsg("Fixing Up Dsn $DSN\n");
	my($ip)=get_ip_by_hostname($DSN);
	my($intname) = $CLUSTERNODE_OF_By_Ip{$ip} || $InternalName_By_DbName{$DSN};
	
	warn "WARN   No MetaData for DSN $DSN?" unless $ServerMetadata{$DSN};
	next  unless $ServerMetadata{$DSN};
	
	my( $bestconmethod ) = "nil";
	if( $ServerMetadata{$DSN} ) {
		$bestconmethod =  $ServerMetadata{$DSN}->{BestConnectionMethod};
	} elsif( $DSN ne $intname ) {
		$bestconmethod =  $ServerMetadata{$intname}->{BestConnectionMethod}
			if $ServerMetadata{$intname};
	}

	if( $intname ) {
		$intname .= "/".get_nativename_from_DSN($DSN) if $intname ne get_nativename_from_DSN($DSN);
	} else {
		$intname = get_nativename_from_DSN($DSN);
	}

	if( $^O eq 'MSWin32' ) {
		push @configuration_errors,"No ODBC Dsn For $DSN"
			if  $bestconmethod eq "nil"
			and $Realtype_By_DbName{$DSN} eq "sqlsvr";
	} else {
		push @configuration_errors,"No Driver For $DSN - INTNAME="
			if  $bestconmethod eq "nil"
			and $Realtype_By_DbName{$DSN} ne "sqlsvr";
	}

	if( ! $Realtype_By_DbName{$DSN} ) {
		$Realtype_By_DbName{$DSN} = get_realtype_from_dsn( $DSN );
	}
	_debugmsg("  Setting Realtype_By_DbName = ",$Realtype_By_DbName{$DSN},"\n");
	_debugmsg("  Setting InternalName_By_DbName = $intname\n");
	$InternalName_By_DbName{$DSN} = $intname;

	if( ! $Hostname_By_DbName{$DSN} ) {
		$Hostname_By_DbName{$DSN} = get_hostname_by_ip($ip);
	}
	if( $Hostname_By_DbName{$DSN} ) {
		$HostSave_By_UcName{ uc($Hostname_By_DbName{$DSN}) }=1;
	}

	foreach ( qw(ComputerNamePhysicalNetBIOS MachineName ActiveSystemName) ) {
		my($hx)= $ServerMetadata{$DSN}->{$_};
		$HostSave_By_UcName{ uc($hx) }=1 if $hx and $HostType_By_UcName{uc($hx)};
	}
}
# END PRINT AND FIX UP STUFF

dump_it(__LINE__);

# clobber any hostnames that are ip addresses
foreach my $hnm ( sort ( keys %HostType_By_UcName )) {
		next unless $hnm =~ /^\d+\.\d+\.\d+\.\d+$/;
		my($hostname)=get_hostname_by_ip($hnm);
		if( $HostSave_By_UcName{uc($hostname)} ) {
			delete $HostSave_By_UcName{uc($hnm)};
		}
		if( $HostType_By_UcName{uc($hostname)} ) {
			delete $HostType_By_UcName{$hnm};
			next;
		}
		$HostType_By_UcName{$hostname} = $HostType_By_Ip{$hnm} unless $HostType_By_UcName{$hostname} ne '';
		_diagmsg("SETTING hostname TO ",$HostType_By_UcName{$hostname},"\n" );
}

save_database_information();
save_host_information();

header("PROGRAM COMPLETED SUCCESSFULLY");
foreach (@configuration_errors) { print "WARNING:",$_,"\n"; }

exit(0);

my(%depricated_dsn);		# key = depricated dsn, value = new dsn
sub is_dsn_combined {
	my($dsn)=@_;
	#use Data::Dumper;
	#print Dumper \%depricated_dsn;
	return $depricated_dsn{$dsn};
}

sub combine_two_dsns {
	my($fromdsn,$todsn)=@_;
	if( ! $HASH_OF_DSN_AND_NATIVE{$fromdsn} ) {
		print __LINE__," DBGDBG: cant combine - no dsn $fromdsn\n";
		return;
	}
	_debugmsg("\tDepricating $fromdsn and Replacing it with $todsn based on GEM password file\n");
	$ServerType_List_By_DbName{$todsn} 	=	$ServerType_List_By_DbName{$fromdsn} 	 unless $ServerType_List_By_DbName{$todsn};
	$Is_Dsn_By_DbName{$todsn} 			=	$Is_Dsn_By_DbName{$fromdsn}		 unless $Is_Dsn_By_DbName{$todsn};
	$Is_Native_By_DbName{$todsn} 		=	$Is_Native_By_DbName{$fromdsn}	 unless $Is_Native_By_DbName{$todsn};
	$Realtype_By_DbName{$todsn} 		=	$Realtype_By_DbName{$fromdsn}		 unless $Realtype_By_DbName{$todsn} ;
	$Ip_By_DbName{$todsn} 				=	$Ip_By_DbName{$fromdsn}		 		 unless $Ip_By_DbName{$todsn} ;
	$InternalName_By_DbName{$todsn} 	=	$InternalName_By_DbName{$fromdsn} unless $InternalName_By_DbName{$todsn};
	$Port_By_DbName{$todsn} 			=	$Port_By_DbName{$fromdsn}			 unless $Port_By_DbName{$todsn};
	$Hostname_By_DbName{$todsn} 		=	$Hostname_By_DbName{$fromdsn}		 unless $Hostname_By_DbName{$todsn} ;
	$Description_By_DbName{$todsn}	= 	$Description_By_DbName{$fromdsn}  unless $Description_By_DbName{$todsn};
	$depricated_dsn{$fromdsn}= $todsn;
	warn "Hmm no Hash $fromdsn\n" unless $HASH_OF_DSN_AND_NATIVE{$fromdsn};
	warn "Hmm no HostSave $fromdsn\n" unless $HostSave_By_UcName{uc($fromdsn)};
	delete 	$HASH_OF_DSN_AND_NATIVE{$fromdsn};
	delete   $HostSave_By_UcName{uc($fromdsn)};
}

sub get_nativename_from_DSN {
	my($DSN)=@_;

	# return something that is true for Hostname_By_DbName from $DSN
	return $DSN if $ServerType_List_By_DbName{$DSN};
	my($ip)=get_ip_by_hostname($DSN);

	return $CLUSTERNODE_OF_By_Ip{$ip}
		if $ip and $CLUSTERNODE_OF_By_Ip{$ip} and $ServerType_List_By_DbName{$CLUSTERNODE_OF_By_Ip{$ip}};
	foreach ( get_all_hosts_by_name($DSN) ) {
		return $_ if $ServerType_List_By_DbName{$_};
	}

	return is_dsn_combined{$DSN} if is_dsn_combined($DSN);
		
	if( $ServerMetadata{$DSN} ) {
		if( $ServerMetadata{$DSN}->{BestConnectionMethod} eq "UNKNOWN" ) {
			return undef;
		}

		if( defined $ServerMetadata{ $DSN }->{ClusterNode} ) {
			foreach ( split(/;/,$ServerMetadata{ $DSN }->{ClusterNode} )){
				return $_ if $ServerType_List_By_DbName{$_};
			}
		}
		foreach (qw( ComputerNamePhysicalNetBIOS InstanceName MachineName ServerName ActiveSystemName )) {
			next unless $ServerType_List_By_DbName{$_};
			return $ServerMetadata{$DSN}->{$_};
		}
		print "DIAG: Cant find native name for $DSN but metadata found\n";
	} else {
		print "DIAG: Cant find native name for $DSN\n";
	}
	return undef;
}

sub save_database_information {
	
	my($cfile,$dfile);
	if( $^O eq 'MSWin32' ) {
		$cfile=get_writeable_discovery_file("sqlsvr_password");
		$dfile=get_writeable_discovery_file("oracle_password");
	} else {
		$cfile=get_writeable_discovery_file("sybase_password");
		$dfile=get_writeable_discovery_file("mysql_password");
	}

	header("Create Database Password Files");
	_statusmsg("Opening $cfile\n");
	_statusmsg("Opening $dfile\n");
	
	open(CFILE,">".$cfile) or die "Cant Write File $cfile";
	open(DFILE,">".$dfile) or die "Cant Write File $dfile";

	# printf "%-21s %21s %21s %10s\n","DSN","IntName","Host","Conn?";
	foreach my $DSN ( sort ( get_dsn_list() )  )  {
		my($ip)= $Ip_By_DbName{$DSN};
		$ip = get_ip_by_hostname($DSN) unless $ip;

		print __LINE__," DBGDBG : DSN=$DSN  ip=$ip clusternode_of=$CLUSTERNODE_OF_By_Ip{$ip}\n";
		if( $CLUSTERNODE_OF_By_Ip{$ip} ) {
			_statusmsg("Ignoring $DSN as it is a cluster node of $CLUSTERNODE_OF_By_Ip{$ip} \n" );
			next;
		}

		my($intname) = $InternalName_By_DbName{$DSN};
		my( $bestconmethod ) = "nil";
		if( $ServerMetadata{$DSN} ) {
			$bestconmethod =  $ServerMetadata{$DSN}->{BestConnectionMethod};
		} elsif( $DSN ne $intname ) {
			$bestconmethod =  $ServerMetadata{$intname}->{BestConnectionMethod}
				if $ServerMetadata{$intname};
		}
		if( $intname ) {
			$intname .= "/".get_nativename_from_DSN($DSN) if $intname ne get_nativename_from_DSN($DSN);
		} else {
			$intname = get_nativename_from_DSN($DSN);
		}

		if( $^O eq 'MSWin32' ) {
			push @configuration_errors,"No ODBC Dsn For $DSN" if  $bestconmethod eq "nil";
		} else {
			push @configuration_errors,"No Driver For $DSN - INTNAME="   if  $bestconmethod eq "nil";
		}

		my($prefix)="[xxxx]";

		if( $^O eq 'MSWin32' ) {
			$prefix='[save]' if $Realtype_By_DbName{$DSN} eq "sqlsvr" or  $Realtype_By_DbName{$DSN} eq "oracle";
		} else {
			$prefix='[save]' if $Realtype_By_DbName{$DSN} eq "sybase" or  $Realtype_By_DbName{$DSN} eq "mysql";
		}
		next if $prefix eq "[xxxx]";

#		printf "%s %-21s %21s %21s %10s\n",
#						$prefix,
#						$DSN,
#			      	$intname,
#			      	$h,
#			      	$bestconmethod;

#		printf "%s             %22s => %s\n",$prefix,"Is_Dsn(Func)"	, is_dsn($DSN)	;
#		printf "%s             %22s => %s\n",$prefix,"ServerType_List_By_DbName"	,$ServerType_List_By_DbName{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"HASH_OF_DSN_AND_NATIVE",$HASH_OF_DSN_AND_NATIVE{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"Is_Native_By_DbName",$Is_Native_By_DbName{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"Realtype_By_DbName",$Realtype_By_DbName{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"Ip_By_DbName",$Ip_By_DbName{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"InternalName_By_DbName",$InternalName_By_DbName{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"Port_By_DbName",$Port_By_DbName{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"Hostname_By_DbName",$Hostname_By_DbName{$DSN};
#		printf "%s             %22s => %s\n",$prefix,"Description_By_DbName",$Description_By_DbName{$DSN};
#print "DBGDBG Line ",__LINE__,"\n";
		my($hashref)=$ServerMetadata{$DSN};
		if( ! $hashref ) {
				warn "ODDLY - WE HAVE NO METADATA FOR $DSN\n";
				next;
		}
#print "DBGDBG Line ",__LINE__,"\n";
#		foreach ( sort ( keys %$hashref )) {
#			next if /^CONFIG:/;
#			printf "%s             %22s => %s\n",$prefix,$_,$hashref->{$_};
#		}

		my(%InvAtts);
		my($stype) = $InvAtts{Driver_Type}		=	$Realtype_By_DbName{$DSN};
		$InvAtts{Driver_Hostname}					=	$Hostname_By_DbName{$DSN};
		$InvAtts{Driver_Port}						=	$Port_By_DbName{$DSN};
		$InvAtts{Driver_InternalName}				=	$InternalName_By_DbName{$DSN};
		$InvAtts{Driver_Description}				=	$Description_By_DbName{$DSN};
   	$InvAtts{Driver_IS_SQLSERVER}				=	$IS_SQLSERVER{$DSN} if $IS_SQLSERVER{$DSN};
   	$InvAtts{Driver_ServerType_List_By_DbName}	=	$ServerType_List_By_DbName{$DSN};
   	$InvAtts{Driver_Is_Native_By_DbName}	=	$Is_Native_By_DbName{$DSN};
   	$InvAtts{Driver_Ip_By_DbName}				=	$Ip_By_DbName{$DSN};
   	$InvAtts{Driver_is_dsn}						=	is_dsn($DSN);
#print "DBGDBG Line ",__LINE__,"\n";
	   foreach ( keys %{$ServerMetadata{ $DSN }} ) {
	   	if( defined $ServerMetadata{ $DSN }->{$_}
				and $ServerMetadata{ $DSN }->{$_} !~ /^\s*$/
				and $_ !~ /^---+$/ ) {
				$InvAtts{"Metadata_".$_} = $ServerMetadata{ $DSN }->{$_};
			}
		}
		
		#
		# some servers dont need saving
		#
		if( $ServerMetadata{ $DSN }->{BestConnectionMethod} eq "UNKNOWN" ) {
			print "Skipping DSN $DSN as connection => ",$ServerMetadata{ $DSN }->{BestConnectionMethod},"\n";
			next;
		}

# $saved_passfile_info{$nm}=\%info;
# $Ip_By_DbName{$nm} 				=	$ip;
# $Hostname_By_DbName{$nm} 		=	$hostname;
# $HASH_OF_DSN_AND_NATIVE{$nm}	=	1;
# print "DBGDBG Line ",__LINE__,"\n";
		if( $HostSave_By_UcName{uc($DSN)} ) {
			# SAVE A PASSWORD FILE ENTRY
			my(%DSNATTRIBUTES);
			if( $saved_passfile_info{$DSN} ) {
				foreach ( keys %{$saved_passfile_info{$DSN}} ) {
					$DSNATTRIBUTES{$_} = $saved_passfile_info{$DSN}->{$_};
				}
			}

			$DSNATTRIBUTES{PORTNUMBER}=$Port_By_DbName{$DSN};
			if( ! $DSNATTRIBUTES{SERVER_TYPE} ) {
				if( $DSN =~ /PROD/ ) {
					$DSNATTRIBUTES{SERVER_TYPE}="PRODUCTION";
				} elsif( $DSN =~ /DEV/ ) {
					$DSNATTRIBUTES{SERVER_TYPE}="DEVELOPMENT";
				} elsif( $DSN =~ /DR/ ) {
					$DSNATTRIBUTES{SERVER_TYPE}="DR";
				} else {
					$DSNATTRIBUTES{SERVER_TYPE}="PRODUCTION";
				}
			}
			$DSNATTRIBUTES{IP}=$ip;
			$DSNATTRIBUTES{HOSTNAME}=$Hostname_By_DbName{$DSN};
			$DSNATTRIBUTES{HOSTTYPE}= $HostType_By_UcName{uc($DSNATTRIBUTES{hostname})}
					if $Hostname_By_DbName{$DSN} and $HostType_By_UcName{uc($DSNATTRIBUTES{hostname})};

			if( $bestconmethod eq "DBI_LOGINPASS" ) {
				if( $^O eq 'MSWin32' ) {
					if( $Realtype_By_DbName{$DSN} eq "sqlsvr" ) {
						print CFILE $DSN,"\n";
						foreach ( sort keys %DSNATTRIBUTES ) {
							printf CFILE "\t%s=%s\n",$_,$DSNATTRIBUTES{$_};
						}
						foreach ( sort keys %InvAtts ) {
							printf CFILE "\trawdata %s=%s\n",$_,$InvAtts{$_};
						}
					} elsif(  $Realtype_By_DbName{$DSN} eq "oracle" ) {
						print DFILE $DSN,"\n";
						foreach ( sort keys %DSNATTRIBUTES ) {
							printf CFILE "\t%s=%s\n",$_,$DSNATTRIBUTES{$_};
						}
						foreach ( sort keys %InvAtts ) {
							printf DFILE "\trawdata %s=%s\n",$_,$InvAtts{$_};
						}
					};
				} else {
					if( $Realtype_By_DbName{$DSN} eq "sybase"  ) {
						print CFILE $DSN,"\t",$InvAtts{Metadata_LoginToUse},"\t",$InvAtts{Metadata_PasswordToUse},"\n";
#						my($port)=  $InvAtts{Metadata_PORT} || $InvAtts{Driver_Port};
#						print CFILE "\tportnumber=",$port,"\n" if $port;
#
#						my($ip)=  $InvAtts{Driver_Ip_By_DbName};
#						print CFILE "\ip=",$ip,"\n" if $ip;
#
#						my($SERVERNAME)= $InvAtts{Driver_InternalName} || $InvAtts{Metadata_SERVERNAME};
#						print CFILE "\SERVERNAME=",$SERVERNAME,"\n" if $SERVERNAME;
#
#						my($SERVER_TYPE)= "sybase";
#						print CFILE "\SERVER_TYPE=",$SERVER_TYPE,"\n" if $SERVER_TYPE;
#
#						my($hostname)= $InvAtts{Driver_Hostname} || $InvAtts{Metadata_HOSTNAME};
#						if( $hostname=~/;/ ) { my($h,@junk)=split(";",$hostname); $hostname=$h; }
#						print CFILE "\hostname=",$hostname,"\n" if $hostname;
#
#						my($backupservername)=$InvAtts{Metadata_BACKUPSERVER};
#						print CFILE "\backupservername=",$backupservername,"\n" if $backupservername;
#

						foreach ( sort keys %DSNATTRIBUTES ) {
							printf CFILE "\t%s=%s\n",$_,$DSNATTRIBUTES{$_};
						}
						foreach ( sort keys %InvAtts ) {
							printf CFILE "\trawdata %s=%s\n",$_,$InvAtts{$_};
						}
					} elsif(  $Realtype_By_DbName{$DSN} eq "mysql"  ) {
						print DFILE $DSN,"\n";
						foreach ( sort keys %DSNATTRIBUTES ) {
							printf CFILE "\t%s=%s\n",$_,$DSNATTRIBUTES{$_};
						}
						foreach ( sort keys %InvAtts ) {
							printf DFILE "\trawdata %s=%s\n",$_,$InvAtts{$_};
						}
					};
				}
			}
		}

		print $prefix,	" InvUpdSystem(",$DSN,", ", $stype,")\n";
		#printf "%s intname=%21s h=%21s conmethod=%10s\n",	'note',	$intname, $Hostname_By_DbName{$DSN}, $bestconmethod;
		#foreach ( sort keys %InvAtts ) {
		#	printf "%s             %22s => %s\n",$prefix,$_,$InvAtts{$_};
		#}
		InvUpdSystem (	-system_name			=> $DSN,
							-system_type			=> $stype."_discovery",
							-attribute_category	=> 'dba_discovery',
							-attributes 			=> \%InvAtts,
							-clearattributes		=> 1);

	}
	close(CFILE);
	close(DFILE);
	
}


#sub DSN_save_database_information {
#	my($typ)=@_;
#	header( "$typ Summary Report\n" );
#	_diagmsg( "SERVERS: ",join(" ",get_servers_by_type($typ)),"\n" );
#	printf "%-21s %21s %21s %10s\n","DSN","IntName","Host","Conn?";
#	foreach my $DSN ( sort ( get_servers_by_type($typ))  )  {
#		my($ip)=get_ip_by_hostname($DSN);
#		my($intname) = $CLUSTERNODE_OF_By_Ip{$ip} || $InternalName_By_DbName{$DSN};
#
#		my( $bestconmethod ) = "nil";
#		if( $ServerMetadata{$DSN} ) {
#			$bestconmethod =  $ServerMetadata{$DSN}->{BestConnectionMethod};
#		} elsif( $DSN ne $intname ) {
#			$bestconmethod =  $ServerMetadata{$intname}->{BestConnectionMethod}
#				if $ServerMetadata{$intname};
#		}
#		if( $intname ) {
#			$intname .= "/".get_nativename_from_DSN($DSN) if $intname ne get_nativename_from_DSN($DSN);
#		} else {
#			$intname = get_nativename_from_DSN($DSN);
#		}
#
#		if( $^O eq 'MSWin32' ) {
#			push @configuration_errors,"No ODBC Dsn For $DSN" if  $bestconmethod eq "nil";
#		} else {
#			push @configuration_errors,"No Driver For $DSN - INTNAME="   if  $bestconmethod eq "nil";
#		}
#		my($h)=$Hostname_By_DbName{$DSN};
#		my($port)=$Port_By_DbName{$DSN};
#		$h.=":".$port if $h and $port;
#		$h.=":".$HostType_By_UcName{uc($h)} if $h and $HostType_By_UcName{uc($h)};
#		printf "%-21s %21s %21s %10s\n",
#						$DSN,
#			      	$intname,
#			      	$h,
#			      	$bestconmethod;
#	}
#}

sub save_host_information {

	#
	# WINDOWS OR UNIX HOSTS
	#
	my($req_type,$pass_type);
	if( $^O eq 'MSWin32' ) {
		$pass_type='win32servers';
		$req_type='WIN32';
	} else {
		$pass_type='unix';
		$req_type='UNIX';
	}
	my($cfile)=get_writeable_discovery_file(lc($req_type)."_password");
	header("Create Password File $cfile");

	open(WCACHE,">".$cfile) or die "Cant Write File $cfile";

	my(%allready_found);
	foreach my $hnm ( sort ( keys %HostSave_By_UcName )) {

		# skip ip
		if( $hnm =~ /^\d+\.\d+\.\d+\.\d+$/ ) {
			_diagmsg( "Skipping $hnm as an ip address\n" );
			next;
		}

		# skip non appropriate servers
		if( $HostType_By_UcName{$hnm} ne $req_type and $HostType_By_UcName{$hnm} ne "?") {
			_diagmsg( "Skipping $hnm as its typ= $HostType_By_UcName{$hnm}\n" );
			next;
		}

		# if type=? then figure it out
		my($ip)=get_ip_by_hostname($hnm);
		if( ! $ip ) {
			_diagmsg( "Skipping $hnm as No Ip Address Available\n" );
			next;
		}

		if( ! $HostType_By_Ip{$ip} ) {
			_statusmsg( "Skipping $hnm as ip $ip doesnt exist (no ypcat hosts?)\n" );
			next;
		}

		if( ! $HostType_By_UcName{uc($hnm)} ) {
			_statusmsg( "Skipping $hnm as it doesnt exist (no ypcat hosts?)\n" );
			next;
		}

		# process each ip once
		if( $allready_found{$ip} ) {
			_statusmsg("Skipping $hnm as This Ip was allready saved as ",$allready_found{$ip},"\n");
			next;
		}
		$allready_found{$ip} = $hnm;

		# host mismatch... impossible combos according to rules
		if( $HostType_By_UcName{$hnm} eq "WIN32" or $HostType_By_UcName{$hnm} eq "UNIX" ) {
			if( $HostType_By_UcName{$hnm} ne  $HostType_By_Ip{$ip} ) {
				push @configuration_errors, "Hosttype Mismatch? hostname=$hnm ip=$ip byname=$HostType_By_UcName{$hnm} byip=$HostType_By_Ip{$ip} - could your win32 box be in .ssh/known_hosts?";
			}
		} elsif( $HostType_By_Ip{$ip} eq "WIN32" or $HostType_By_Ip{$ip} eq "UNIX" ) {
			$HostType_By_UcName{$hnm} = $HostType_By_Ip{$ip};
		} else {
			push @configuration_errors,  "NO HOST TYPE BY IP!";
		}

		if( $HostType_By_UcName{$hnm} eq "?" and $ip ) {
			my(@allhosts)=get_all_hostnames_by_ip($ip);
			foreach( @allhosts ) {
				next unless defined $HostType_By_UcName{uc($_)};
				next unless $HostType_By_UcName{uc($_)} ne "?";
				push @configuration_errors, "Replacing Host Type for $hnm with ",$HostType_By_UcName{uc($_)},"\n";
				print "Replacing Host Type for $hnm with ",$HostType_By_UcName{uc($_)},"\n";
				$HostType_By_UcName{$hnm} = $HostType_By_UcName{uc($_)};
				last;
			}
		}

#			foreach my $uchost ( keys %ipcache ) {
#				if( $ipcache{$hnm} eq $ip ) {
#					foreach my $hx ( keys %HostType_By_UcName ) {
#						next unless uc($hx) eq $uchost;
#						next if $HostType_By_UcName{$hx} eq "?";
#						print "Replacing Host Type for $hx with $HostType_By_UcName{$hx}\n";
#						$HostType_By_UcName{$hx} = $HostType_By_UcName{$hnm};
#						last;
#					}
#				}
#			}

		if( $HostType_By_UcName{$hnm} eq "?" ) {
			my($rc)=port_detect_is_win32($hnm);
			if( $rc == 0 ) {
				$HostType_By_UcName{$hnm}="WIN32";
				$HostType_By_Ip{get_ip_by_hostname($hnm)}="WIN32";
			} else {
				# print "DBGDBG: Setting Type OF Host $hnm To UNIX\n";
				$HostType_By_UcName{$hnm}="UNIX";
				$HostType_By_Ip{get_ip_by_hostname($hnm)}="UNIX";
			}
			_statusmsg( "Auto - Detected that $hnm is a $HostType_By_UcName{$hnm} Server\n");
		}

		if( $HostType_By_UcName{$hnm} ne $req_type ) {
			_diagmsg( "Skipping $hnm as its typ= $HostType_By_UcName{$hnm}\n" );
			next;
		}

#		if( $hnm =~ /^\d+\.\d+\.\d+\.\d+$/ ) {
#			_diagmsg("Skipping Host $hnm as its an ip address \n");
#			next;
#		}

		my($BestHostname) = get_registered_hostname_by_ip( $ip );
		$BestHostname = lc($hnm) unless $BestHostname;
		
		my(%info) = get_password_info(-name=>$BestHostname, -type=>$pass_type);
		my(%InvAtts);
		foreach ( keys %info ) { 
			$InvAtts{$_} = $info{$_} unless $_ eq "NOT_FOUND"; 
		}
		my($cmt)=$Host_Comment_By_Ip{$ip};
		$cmt=~s/\n/ /g;
		$InvAtts{COMMENT}=$cmt if $cmt;

		if( $DriveInfo_By_Ip{$ip} ) {
			$InvAtts{CONNECTION_STATE}="OK";
			$InvAtts{DISKS}=$DriveInfo_By_Ip{$ip} if $DriveInfo_By_Ip{$ip};
		} elsif( $req_type eq 'UNIX' ) {
			$InvAtts{CONNECTION_STATE} = "OK";
		} else {
			$InvAtts{CONNECTION_STATE}="FAIL";
		}

		$InvAtts{IP}=$ip;
		$InvAtts{IS_SQLSERVER}=$IS_SQLSERVER{$hnm} if $IS_SQLSERVER{$hnm};
		#$Registered_Hostname_By_Ip{$ip} if $Registered_Hostname_By_Ip{$ip}
		#				and lc($hnm) ne lc($Registered_Hostname_By_Ip{$ip});
		# $InvAtts{IS_VHOST}=$IS_VHOST{$hnm} if $IS_VHOST{$hnm};
		$InvAtts{CLUSTERNODE_OF}=$CLUSTERNODE_OF_By_Ip{$ip} if $CLUSTERNODE_OF_By_Ip{$ip};
		$InvAtts{UNIX_COMM_METHOD}	=	"SSH" 			unless $InvAtts{UNIX_COMM_METHOD};
		$InvAtts{SERVER_TYPE}		=	"PRODUCTION" 	unless $InvAtts{SERVER_TYPE};
		
		print "[save] InvUpdSystem(",$BestHostname,", ", $req_type,")\n";
		print  WCACHE "$HostType_By_UcName{$hnm} ".$BestHostname."\n";
		foreach ( sort keys %InvAtts ) {
			printf WCACHE "\t%s=%s\n", $_ , $InvAtts{$_};
		}

		#InvUpdSystem (	-system_name			=> lc($hnm),
		InvUpdSystem (	-system_name			=> $BestHostname,
							-system_type			=> lc($req_type)."_discovery",
							-attribute_category	=> 'dba_discovery',
							-attributes 			=> \%InvAtts,
							-clearattributes		=> 1);
	}
	close(WCACHE);
}

sub is_dsn {
	my($dsn)=@_;
	foreach ( keys %DBI_HASHREF ) {
		my($a,$b)=split(/:/,$_,2);
		return 1 if $b eq $_;
	}
	return 0;
}

sub get_dsn_list {
	my(@list);
	# foreach ( sort( keys %DBI_HASHREF )) {
	foreach ( sort( keys %HASH_OF_DSN_AND_NATIVE )) {
		#s/^ODBC://g;
		#s/Sybase://gi;
		push @list,$_;
	}
	return @list;
}

sub get_realtype_from_dsn {
	my($dsn)=@_;
	return $Realtype_By_DbName{$dsn} if $Realtype_By_DbName{$dsn};

	#return "sybase" if $dsn =~ /^Sybase:/i;
	#return "oracle" if $dsn =~ /^Oracle:/i;

	foreach ( keys %DBI_HASHREF ) {
		next unless /\:${dsn}$/;
		if( /^Sybase/i ) {
			s/^Sybase://gi;
			$Realtype_By_DbName{$_}="sybase";
			return "sybase";
		}
		if( /^Oracle/i ) {
			s/^Oracle://gi;
			$Realtype_By_DbName{$_}="oracle";
			return "oracle";
		}
		if( /^Mysql/i ) {
			s/^Mysql://gi;
			$Realtype_By_DbName{$_}="mysql";
			return "mysql";
		}
		if( /^Sqlsvr/i ) {
			s/^Sqlsvr://gi;
			$Realtype_By_DbName{$_}="sqlsvr";
			return "sqlsvr";
		}
		s/^ODBC://g;
		die "Unknown DSN Type $dsn";
	}

	# handle native drivers
	return "sqlsvr" if $Port_By_DbName{$dsn} == 1433;
	return "mysql"  if $Port_By_DbName{$dsn} == 3306;
	return "oracle" if $Port_By_DbName{$dsn} == 1521;

	if( $InternalName_By_DbName{$dsn} and $InternalName_By_DbName{$dsn} ne $dsn ) {
		if( $HASH_OF_DSN_AND_NATIVE{ $InternalName_By_DbName{$dsn} } ) {
			return get_realtype_from_dsn($InternalName_By_DbName{$dsn});
		}
	}
}

#sub load_db_info_to_db {
#
#	header("DBG LISTING \n");
#	print "Dumper start\n";
#	use Data::Dumper;
#	print Dumper \%ServerType_List_By_DbName;
#	print "Dumper end\n";
#
#	foreach ( get_dsn_list() ) {
#		print "ODBC=$_ ";
#		my($typ)=get_realtype_from_dsn( $_ );
#		print "TYPE=$typ " if $typ;
#		my($NDCN)=get_nativename_from_DSN($_) || "NOT FOUND";
#		print "NATIVE=$NDCN " if $NDCN;
#		print "\n";
#	}
#
#	my(@todolist);
#	if( $^O eq 'MSWin32' ) {
#		push @todolist, "sqlsvr";
#		push @todolist, "oracle";
#	} else {
#		push @todolist, "sybase";
#		push @todolist, "mysql";
#	}
#	foreach my $stype ( @todolist ) {
#		#my($cfile)=get_writeable_discovery_file($stype."_password");
#		# header("Create $stype Password File - $cfile");
#		#open(DCACHE,">".$cfile) or die "Cant Write File $cfile";
#
#		foreach my $DSN ( get_dsn_list() ) {
#			my(%InvAtts);
#			my($typ)=get_realtype_from_dsn( $DSN );
#			die "WOAH: no drivertype for $DSN "  unless $typ;
#			next unless $typ eq $stype;
#
#
#		   #my($dsn_o)=$DSN;
#		   #my($dsn_n)=get_nativename_from_DSN($DSN) || "NOT FOUND";
#		   #$dsn_n = $DSN unless $dsn_n;
#
#			$InvAtts{Driver_Type}			=	$Realtype_By_DbName{$DSN};
#			$InvAtts{Driver_Hostname}		=	$Hostname_By_DbName{$DSN};
#			$InvAtts{Driver_Port}			=	$Port_By_DbName{$DSN};
#			$InvAtts{Driver_InternalName}	=	$InternalName_By_DbName{$DSN};
#			$InvAtts{Driver_Description}	=	$Description_By_DbName{$DSN};
#	   	$InvAtts{Driver_IS_SQLSERVER}	=	$IS_SQLSERVER{$DSN} if $IS_SQLSERVER{$DSN};
#
#		   foreach ( keys %{$ServerMetadata{ $DSN }} ) {
#		   	if( defined $ServerMetadata{ $DSN }->{$_}
#					and $ServerMetadata{ $DSN }->{$_} !~ /^\s*$/
#					and $_ !~ /^---+$/ ) {
#					$InvAtts{"Metadata_".$_} = $ServerMetadata{ $DSN }->{$_};
#				}
#			}
#
#			print lc($DSN)," ", $stype,"\n";
#			foreach ( sort keys %InvAtts ) { printf "\t%s=%s\n", $_ , $InvAtts{$_}; }
#
#			InvUpdSystem (	-system_name			=> $DSN,
#								-system_type			=> $stype."_discovery",
#								-attribute_category	=> 'dba_discovery',
#								-attributes 			=> \%InvAtts,
#								-clearattributes		=> 1);
#		}
#		#close(DCACHE);
#	}
#
#	#header("Printing Native Driver Config\n");
#	#foreach ( sort ( keys %ServerType_List_By_DbName )) {
#	#	print __LINE__," ",$_," ",
#	#		"TYPE=>",$ServerType_List_By_DbName{$_}," ",
#	#		"INAM=>",$InternalName_By_DbName{$_}," ",
#	#		"PORT=>",$Port_By_DbName{$_}," ",
#	#		"HOST=>",$Hostname_By_DbName{$_},"\n";
#	#	my($h)=$Hostname_By_DbName{$_};
#	#	print __LINE__," HOSTINFO: Host=$h T=$HostType_By_UcName{$h} C=$Host_Comment_By_Ip{$h} \n" if $h;
#	#}
#}

#sub analyze_configuration {
#	header("DSN REPORTS BY TYPE");
#	foreach ( get_servertype_keys()) {
#		DSN_save_database_information($_);
#	}
#
##	my(%output_configuration);
##	use Data::Dumper;
##	print Dumper \%output_configuration;
##	return \%output_configuration;
#}


#sub dbi_connection_report {
#
#	my($outfile)=get_writeable_discovery_file("connection_report");
#	_statusmsg("DBI CONNECTIONS REPORT SAVED IN $outfile");
#
#	open(FH,">".$outfile) or die "Cant write $outfile\n";
#
#	my(%found_native, %found_odbc);
#
#	# foreach ( sort ( keys %$DBI_HASHREF )) {
#	foreach ( get_dsn_list() ) {
#		my($dbi_key)=$_;
#		my($driver_type);
#		if( $dbi_key =~ /ODBC:/ ) {
#			$driver_type='ODBC';
#			$dbi_key =~ s/^ODBC://;
#		} elsif( $dbi_key =~ /Sybase:/i ) {
#			$driver_type='Sybase';
#			$dbi_key =~ s/^Sybase://i;
#		} else {
#			print "WARNING: $dbi_key not managed at line ",__LINE__,"\n";
#			next;
#		}
#
#		printf FH "%s\n",$dbi_key;
#
#		my($found)=0;
#		foreach ( keys %ServerType_List_By_DbName ) {
#		#	print FH "DBGDBG ",__LINE__," Comparing $_ and $dbi_key\n";
#			next unless lc($_) eq lc($dbi_key);
#			$found=$_;
#			printf FH "\tNATIVE: %-30s %s\n",$_,$ServerType_List_By_DbName{$_};
#			$found_native{$dbi_key}=1;
#			last;
#		}
#		printf FH  "\tNATIVE: No Native Driver Found\n" unless $found;
#
#		$found=0;
#		if( $^O eq 'MSWin32' ) {
#			foreach ( keys %Realtype_By_DbName ) {
#		#		print FH "DBGDBG ",__LINE__," Comparing $_ and $dbi_key\n";
#				next unless lc($_) eq lc($dbi_key);
#				$found=$_;
#				printf FH "\t  ODBC: %-30s %s\n",$_,$Realtype_By_DbName{$_};
#				$found_odbc{$dbi_key}=1;
#				last;
#			}
#			printf FH "\t  ODBC: No Odbc Driver Found\n" unless $found;
#		}
#		print FH "DBGDBG ",__LINE__," found_native = $found_native{$dbi_key} - found_odbc = $found_odbc{$dbi_key} \n";
#
#		if( $^O eq 'MSWin32' and defined $found_native{$dbi_key} and defined $found_odbc{$dbi_key} ) {
#			# we are good
#			print FH "\tLooks Ok To Me - Native And Odbc Both Found\n";
#		} elsif( $^O ne 'MSWin32' and defined $found_native{$dbi_key} ) {
#			print FH "\tNo Odbc Driver Found for $dbi_key\n";
#		} elsif ( defined $found_odbc{$dbi_key} ) {
#				# we have an odbc connection but no native driver...
#				# this could be an alias...
#				print FH "\tNo Native Driver found for $dbi_key\n";
#		}
#
#		if( $found_native{$dbi_key} ) {
#			print FH "\tServerType_List_By_DbName : ", $ServerType_List_By_DbName{$dbi_key},"\n";
#			print FH "\tInternalName_By_DbName : ", $InternalName_By_DbName{$dbi_key},"\n";
#			print FH "\tPort_By_DbName : ", $Port_By_DbName{$dbi_key},"\n";
#			print FH "\tHostname_By_DbName : ", $Hostname_By_DbName{$dbi_key},"\n";
#			print FH "\tDescription_By_DbName : ", $Description_By_DbName{$dbi_key},"\n";
#		}
#
#		if( $found_odbc{$dbi_key} ) {
#			print FH "\tRealtype_By_DbName : ", $Realtype_By_DbName{$dbi_key},"\n";
#			print FH "\tHostname_By_DbName : ", 	$Hostname_By_DbName{$dbi_key},"\n";
#			print FH "\tPort_By_DbName : ", 		$Port_By_DbName{$dbi_key},"\n";
#			print FH "\tInternalName_By_DbName : ", $InternalName_By_DbName{$dbi_key},"\n";
#			print FH "\tDescription_By_DbName : ", $Description_By_DbName{$dbi_key},"\n";
#		}
#
#		if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr" ) {
#
##				print "\tPort Detect SqlSvr: ",		port_detect_is_sqlsvr($dbi_key),"\n";
##				print "\tPort Detect Win32: ", 		port_detect_is_win32($dbi_key),"\n";
#
##				foreach( vault_get_credentials( "sqlsvr" ) ) {
##					my($l,$p)=split(/;/,$_,2);
##					_debugmsg( "Trying sqlsvr vault Creds $l/$p on $dbi_key \n" );
##					my($a) = $InternalName_By_DbName{$dbi_key};
##					$a = $dbi_key if $a=~/^\s*$/;
##					$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, $a, 0, $l, $p );
##					last if $ServerMetadata{ $dbi_key }->{BestConnectionMethod} ne "UNKNOWN";
##				}
##
###				foreach ( keys %{$ServerMetadata{ $dbi_key }} ) {
#				foreach ( qw( 	BestConnectionMethod XsqlNativeAuth XsqlLoginPassAuth DBILoginPassAuth DBINativeAuth
#									TcpPort TcpDynamicPorts ComputerNamePhysicalNetBIOS InstanceName MachineName
#									ServerName ClusterNode SharedDrive ActiveSystemName ) ){
#					printf FH "\tMETADATA %20s %s\n", $_, $ServerMetadata{ $dbi_key }->{$_}
#						if defined $ServerMetadata{ $dbi_key }->{$_};
#				}
#
#		} elsif( $Realtype_By_DbName{$dbi_key} eq "sybase" ) {
#
##				foreach( vault_get_credentials( "sybase" ) ) {
##					my($l,$p)=split(/;/,$_,2);
##					_debugmsg( "Trying sybase vault Creds $l/$p on $dbi_key \n" );
##					$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, 0, $l, $p );
##					# print Dumper \%ServerMetadata;	#DBGDBG
##					last if $ServerMetadata{ $dbi_key }->{BestConnectionMethod} ne "UNKNOWN";
##				}
##
##				#foreach ( qw( 	BestConnectionMethod XsqlNativeAuth XsqlLoginPassAuth DBILoginPassAuth DBINativeAuth
##				#					TcpPort TcpDynamicPorts ComputerNamePhysicalNetBIOS InstanceName MachineName
##				#					ServerName ClusterNode SharedDrive ActiveSystemName ) ){
#			foreach ( keys %{$ServerMetadata{ $dbi_key }} ) {
#				printf FH "\tMETADATA %20s %s\n", $_, $ServerMetadata{ $dbi_key }->{$_}  if defined $ServerMetadata{ $dbi_key }->{$_};
#			}
#		}
#	}
#	print FH "\n";
#
#	foreach ( sort ( keys %ServerType_List_By_DbName )) {
#		next unless $found_odbc{$_};
#		if( $^O eq 'MSWin32' ) {
#			print FH "No ODBC DSN Has Been Setup For Database $ServerType_List_By_DbName{$_} $_\n";
#		} else {
#			print FH "No Native Driver Setup For Database $ServerType_List_By_DbName{$_} $_\n";
#		}
#	}
#
#	# i actually dont think this is possbile anymore - if it is then need to adjust the exclude lists
#	if( $^O eq 'MSWin32' ) {
#		foreach ( keys %Realtype_By_DbName ) {
#			die "No Odbc Connection Found For $_ - $Realtype_By_DbName{$_}\n" unless $found_odbc{$_};
#		}
#	}
#	close(FH);
#}

sub dbi_get_metadata {

	#
	# read the list of prefetched metadata
	#
	my($cfile) = get_writeable_discovery_file("DB_metadata");
	if( -r $cfile ) {
		open(CACHE,$cfile) or die "Cant Read File $cfile";
		while(<CACHE>) {
			chomp;chomp;
			my($svr,$ky,$val)=split(/;/,$_,3);
			if( ! defined $ServerMetadata{ $svr } ) {
				my(%x);
				my($a)=is_dsn_combined{$svr};
				$svr=$a if $a;
				$ServerMetadata{ $a }=\%x;
			}
			$ServerMetadata{ $svr }->{$ky} = $val;
		}
		close(CACHE);
	}
	_statusmsg( "\nread DBI METADATA from $cfile\n" );

	my($qexit);
	foreach ( get_dsn_list() ) {
		print "\nProcessing DSN $_\n";
		last if $qexit eq "EXIT";
		my($dbi_key)=$_;
		# $dbi_key =~ s/^ODBC://;
		# $dbi_key =~ s/^Sybase://;
		if( $dbi_key =~ /:/ ) {
			print "WARNING: $dbi_key not managed at line ",__LINE__,"\n";
			next;
		}
		# print "DBGDBG: DBI=$dbi_key\n";

		if( $ServerMetadata{$dbi_key} ) {
			if( $ServerMetadata{ $dbi_key }->{BestConnectionMethod} ne "UNKNOWN" and ! $UPDATECACHE ) {
				_statusmsg( "METADATA EXISTS - BESTCONN=",$ServerMetadata{ $dbi_key }->{BestConnectionMethod}," \n");
				next;
			}
			# _diagmsg("   Fetched $dbi_key from Cache ( $Realtype_By_DbName{$dbi_key} )\n");
			#  next if $ServerMetadata{ $dbi_key }->{BestConnectionMethod} ne "UNKNOWN" and ! $UPDATECACHE;
			# _diagmsg("   ... Continuing as ConnectionMethod==UNKNOWN )\n");
			next if $FAST;
			_statusmsg("METADATA EXISTS FOR $dbi_key - BESTCONN=",
				$ServerMetadata{ $dbi_key }->{BestConnectionMethod},
				" refetching (args)\n");
		} else {
			_statusmsg("NO METADATA EXISTS FOR $dbi_key - REFETCHING\n");
		}

		_statusmsg("$dbi_key type = ",$ServerType_List_By_DbName{$dbi_key},"\n");
	
		my($found)=0;
		my($OK)='';
		if( $Realtype_By_DbName{$dbi_key} ) {
			if( $TYPEMAP{$Realtype_By_DbName{$dbi_key}} eq 'win32' ) {
				$OK=1 if $^O eq 'MSWin32';
			} else {
				$OK=1 unless $^O eq 'MSWin32';
			}			
		}
		if( ! $OK ) {
			_debugmsg( "skipping DSN=$dbi_key \n" );
			next;
		}
		
		if( ! $Realtype_By_DbName{$dbi_key} ) {
				die "No DSN type for $dbi_key\n";
				next;
		}
		
		# print "DBGDBG ", __LINE__," key=$dbi_key Md=",$ServerMetadata{ $dbi_key },"\n";
		if( $Realtype_By_DbName{$dbi_key} eq "sybase" ) {
			if( $ServerType_List_By_DbName{$dbi_key} =~ /Sql_Server/i ) {
				$ServerMetadata{ $dbi_key }->{XsqlLoginPassAuth} = "FAILED";
				$ServerMetadata{ $dbi_key }->{FailedAuthReason} = "Sql Server From Unix";
				$ServerMetadata{ $dbi_key }->{BestConnectionMethod} = "UNKNOWN";
				_statusmsg( "abort. not refetching DSN=$dbi_key ",$ServerMetadata{ $dbi_key }->{FailedAuthReason},"\n" );
				next;
			}
			if( $HostType_By_UcName{uc($dbi_key)} eq "WIN32" and $Port_By_DbName{$dbi_key} eq "1433" ) {
				$ServerMetadata{ $dbi_key }->{XsqlLoginPassAuth} = "FAILED";
				$ServerMetadata{ $dbi_key }->{FailedAuthReason} = "Sql Server port From Unix";
				$ServerMetadata{ $dbi_key }->{BestConnectionMethod} = "UNKNOWN";
				_statusmsg( "abort. not refetching DSN=$dbi_key ",$ServerMetadata{ $dbi_key }->{FailedAuthReason},"\n" );
				next;
			}
			if( $dbi_key=~/_BS$/i or $dbi_key=~/_BACK$/i or $dbi_key=~/_RS$/i or $dbi_key=~/_MON$/i or $dbi_key=~/_RSSD$/i ) {
				$ServerMetadata{ $dbi_key }->{XsqlLoginPassAuth} = "FAILED";
				$ServerMetadata{ $dbi_key }->{FailedAuthReason} = "Not A Sybase Server";
				$ServerMetadata{ $dbi_key }->{BestConnectionMethod} = "UNKNOWN";
				_statusmsg( "abort. not refetching DSN=$dbi_key ",$ServerMetadata{ $dbi_key }->{FailedAuthReason},"\n" );
				next;
			}
		}
	
		# print "DBGDBG ", __LINE__," key=$dbi_key Md=",$ServerMetadata{ $dbi_key },"\n";	
		my($l,$p)=get_password(-type=>$Realtype_By_DbName{$dbi_key},-name=>$dbi_key);
		if( $l  ) {
			_debugmsg( "attempting Creds $l/xxxx from Inventory System Cache\n" );
			if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr"  ) {
				$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, undef, 0, $l, $p );
			} elsif( $Realtype_By_DbName{$dbi_key} eq "mysql"  ) {
				$ServerMetadata{ $dbi_key } = LoginAndFetch_Mysql( $dbi_key, $Hostname_By_DbName{$dbi_key}, 0, $l, $p );
			} elsif( $Realtype_By_DbName{$dbi_key} eq "oracle"  ) {
				$ServerMetadata{ $dbi_key } = LoginAndFetch_Oracle( $dbi_key, undef,  0, $l, $p );
			} elsif( $Realtype_By_DbName{$dbi_key} eq "sybase"  ) {
				$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, undef, 0, $l, $p );
			} else {
				die " Bad type  $Realtype_By_DbName{$dbi_key} for $dbi_key\n";
			}
		} 
		
		# print "DBGDBG ", __LINE__," key=$dbi_key Md=",$ServerMetadata{ $dbi_key },"\n";
		if( ! $ServerMetadata{ $dbi_key } or $ServerMetadata{ $dbi_key }->{BestConnectionMethod} eq "UNKNOWN" ) {
			my($l,$p,@junk)=InvGetSACredByDSNandMonhost( -DSN=>$dbi_key, -monitor_host=>$HOSTNAME) if ! $l;
			if( $l and $p ) {
				_debugmsg( "attempting Creds $l/xxxx from Inventory System Cache\n" );
				if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, undef, 0, $l, $p );
				} elsif( $Realtype_By_DbName{$dbi_key} eq "mysql"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_Mysql( $dbi_key, $Hostname_By_DbName{$dbi_key}, 0, $l, $p );
				} elsif( $Realtype_By_DbName{$dbi_key} eq "oracle"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_Oracle( $dbi_key, undef,  0, $l, $p );
				} elsif( $Realtype_By_DbName{$dbi_key} eq "sybase"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, undef, 0, $l, $p );
				} else {
					die " Bad type  $Realtype_By_DbName{$dbi_key} for $dbi_key\n";
				}
			}
		}
		
		if( ! $ServerMetadata{ $dbi_key } or $ServerMetadata{ $dbi_key }->{BestConnectionMethod} eq "UNKNOWN" ) {		
			if( $USEVAULT ) {
				foreach( vault_get_credentials( $Realtype_By_DbName{$dbi_key} ) ) {
					($l,$p)=split(/;/,$_,2);
					_debugmsg( "Trying vault Creds $l/$p on $dbi_key \n" );
					my($a) = $InternalName_By_DbName{$dbi_key};
					$a = $dbi_key if $a=~/^\s*$/;
					if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr"  ) {
						$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, $a, 0, $l, $p );						
					} elsif( $Realtype_By_DbName{$dbi_key} eq "mysql"  ) {
						$ServerMetadata{ $dbi_key } = LoginAndFetch_Mysql( $dbi_key, $Hostname_By_DbName{$dbi_key}, 0, $l, $p );
					} elsif( $Realtype_By_DbName{$dbi_key} eq "oracle"  ) {
						$ServerMetadata{ $dbi_key } = LoginAndFetch_Oracle( $dbi_key, $a, 0, $l, $p );
					} elsif( $Realtype_By_DbName{$dbi_key} eq "sybase"  ) {
						$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, $a, 0, $l, $p );
					} else {
						die " Bad type  $Realtype_By_DbName{$dbi_key} for $dbi_key\n";
					}
					last if $ServerMetadata{ $dbi_key }->{BestConnectionMethod} ne "UNKNOWN";
				}		
			}
		}
		
		if( ! $ServerMetadata{ $dbi_key } or $ServerMetadata{ $dbi_key }->{BestConnectionMethod} eq "UNKNOWN" ) {		
			if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr" ) {				# native auth for sql server				
				($l,$p)=("","");
				_debugmsg( "Trying native authentication $dbi_key \n" );
				my($a) = $InternalName_By_DbName{$dbi_key};
				$a = $dbi_key if $a=~/^\s*$/;
				# $ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, $a, 0, $l, $p );
				if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, $a, 0, $l, $p );						
				} elsif( $Realtype_By_DbName{$dbi_key} eq "mysql"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_Mysql( $dbi_key, $Hostname_By_DbName{$dbi_key}, 0, $l, $p );
				} elsif( $Realtype_By_DbName{$dbi_key} eq "oracle"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_Oracle( $dbi_key, $a, 0, $l, $p );
				} elsif( $Realtype_By_DbName{$dbi_key} eq "sybase"  ) {
					$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, $a, 0, $l, $p );
				} else {
					die " Bad type  $Realtype_By_DbName{$dbi_key} for $dbi_key\n";
				}							
			}
		}

		if( ! $ServerMetadata{ $dbi_key } or $ServerMetadata{ $dbi_key }->{BestConnectionMethod} eq "UNKNOWN" ) {		
			_statusmsg("Unable to connect by any means to $dbi_key\n");
			next;
		} 
		
#		my($found)=0;
#		my($OK)='';
#		if( $Realtype_By_DbName{$dbi_key} ) {
#			if( $TYPEMAP{$Realtype_By_DbName{$dbi_key}} eq 'win32' ) {
#				$OK=1 if $^O eq 'MSWin32';
#			} else {
#				$OK=1 unless $^O eq 'MSWin32';
#			}			
#		}
#		if( ! $OK ) {
#			_debugmsg( "skipping DSN=$dbi_key \n" );
#			next;
#		}
#		
#		next unless $OK;
		
			#if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr" ) {
#
#		my($l,$p,@junk)=InvGetSACredByDSNandMonhost(-DSN=>$dbi_key, -monitor_host=>$HOSTNAME);
#		if( $l and $p ) {
#			_debugmsg( "attempting Creds $l/xxxx from Inventory System Cache\n" );
#			
#			#$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, 0, $l, $p );
#			if( $Realtype_By_DbName{$dbi_key} eq "sqlsvr"  ) {
#				$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, undef, 0, $l, $p );
#			} elsif( $Realtype_By_DbName{$dbi_key} eq "mysql"  ) {
#				$ServerMetadata{ $dbi_key } = LoginAndFetch_Mysql( $dbi_key, $Hostname_By_DbName{$dbi_key}, 0, $l, $p );
#			} elsif( $Realtype_By_DbName{$dbi_key} eq "oracle"  ) {
#				$ServerMetadata{ $dbi_key } = LoginAndFetch_Oracle( $dbi_key, undef,  0, $l, $p );
#			} elsif( $Realtype_By_DbName{$dbi_key} eq "sybase"  ) {
#				$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, undef, 0, $l, $p );
#			} else {
#				die " Bad type  $Realtype_By_DbName{$dbi_key} for $dbi_key\n";
#			}
#			
#		} elsif( $USEVAULT ) {
#			
#			foreach( vault_get_credentials( "sqlsvr" ) ) {
#				die "ERROR";
#				($l,$p)=split(/;/,$_,2);
#				_debugmsg( "Trying sqlsvr vault Creds $l/$p on $dbi_key \n" );
#				my($a) = $InternalName_By_DbName{$dbi_key};
#				$a = $dbi_key if $a=~/^\s*$/;
#				$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, $a, 0, $l, $p );
#				last if $ServerMetadata{ $dbi_key }->{BestConnectionMethod} ne "UNKNOWN";
#			}
#			
#		} else {
#
#				# native auth
#				($l,$p)=("","");
#				_debugmsg( "Trying native authentication $dbi_key \n" );
#				my($a) = $InternalName_By_DbName{$dbi_key};
#				$a = $dbi_key if $a=~/^\s*$/;
#				$ServerMetadata{ $dbi_key } = LoginAndFetch_SqlServer( $dbi_key, $a, 0, $l, $p );
#
#				# its a sql server that failed... lets dig deeper
#			}
#
#			my(%moreargs);
#			$moreargs{-port} 			= $Port_By_DbName{$dbi_key} 		if $Port_By_DbName{$dbi_key};
#			$moreargs{-hostname} 	= $Hostname_By_DbName{$dbi_key}  if $Hostname_By_DbName{$dbi_key};
#			$moreargs{-system_type} = $Realtype_By_DbName{$dbi_key};
#			$moreargs{-system_name} = $InternalName_By_DbName{$dbi_key} || $dbi_key;
#			InvSetSACreds( -DSN=>$dbi_key, -monitor_host=>$HOSTNAME, -metadata=>$ServerMetadata{ $dbi_key }, %moreargs );
#
#			foreach ( qw( 	BestConnectionMethod XsqlNativeAuth XsqlLoginPassAuth DBILoginPassAuth DBINativeAuth
#								TcpPort TcpDynamicPorts ComputerNamePhysicalNetBIOS InstanceName MachineName
#								ServerName ClusterNode SharedDrive ActiveSystemName ) ){
#				printf "%20s METADATA %20s %s\n",__LINE__, $_, $ServerMetadata{ $dbi_key }->{$_}
#					if defined $ServerMetadata{ $dbi_key }->{$_};
#			}
#			next;
#
#	#	} else {
#	#		print "LINE ",__LINE__," DBGDBG: Not Checking Db Info\n";
#	#		_statusmsg("Skipping MetaData Fetch for $dbi_key as its type=$Realtype_By_DbName{$dbi_key}\n");
#	#		delete $HASH_OF_DSN_AND_NATIVE{$dbi_key}  if $HASH_OF_DSN_AND_NATIVE{$dbi_key};
#	
#		} elsif( $OK and $Realtype_By_DbName{$dbi_key} eq "sybase" ) {
#			if( $ServerType_List_By_DbName{$dbi_key} =~ /Sql_Server/i ) {
#				$ServerMetadata{ $dbi_key }->{XsqlLoginPassAuth} = "FAILED";
#				$ServerMetadata{ $dbi_key }->{FailedAuthReason} = "Sql Server From Unix";
#				$ServerMetadata{ $dbi_key }->{BestConnectionMethod} = "UNKNOWN";
#				_statusmsg( "abort. not refetching DSN=$dbi_key ",$ServerMetadata{ $dbi_key }->{FailedAuthReason},"\n" );
#				next;
#			}
#			if( $HostType_By_UcName{uc($dbi_key)} eq "WIN32" and $Port_By_DbName{$dbi_key} eq "1433" ) {
#				$ServerMetadata{ $dbi_key }->{XsqlLoginPassAuth} = "FAILED";
#				$ServerMetadata{ $dbi_key }->{FailedAuthReason} = "Sql Server port From Unix";
#				$ServerMetadata{ $dbi_key }->{BestConnectionMethod} = "UNKNOWN";
#				_statusmsg( "abort. not refetching DSN=$dbi_key ",$ServerMetadata{ $dbi_key }->{FailedAuthReason},"\n" );
#				next;
#			}
#
#			if( $dbi_key=~/_BS$/i or $dbi_key=~/_BACK$/i or $dbi_key=~/_RS$/i or $dbi_key=~/_RSSD$/i ) {
#				$ServerMetadata{ $dbi_key }->{XsqlLoginPassAuth} = "FAILED";
#				$ServerMetadata{ $dbi_key }->{FailedAuthReason} = "Not A Sybase Server";
#				$ServerMetadata{ $dbi_key }->{BestConnectionMethod} = "UNKNOWN";
#				_statusmsg( "abort. not refetching DSN=$dbi_key ",$ServerMetadata{ $dbi_key }->{FailedAuthReason},"\n" );
#				next;
#			}
#
#			_statusmsg( "FETCHING METADATA FOR SERVER $dbi_key\n" );
#			_debugmsg( "unix processing - dbi_key=$dbi_key \n" );
#			my($l,$p,@junk)=InvGetSACredByDSNandMonhost(-DSN=>$dbi_key, -monitor_host=>$HOSTNAME);
#			if( $l and $p ) {
#				_debugmsg( "attempting Creds $l/xxxx from Inventory System Cache\n" );
#				$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, 0, $l, $p );
#				_debugmsg( "completed LoginAndFetch_Sybase()\n" );
#			} elsif( $USEVAULT ) {
#				_debugmsg( "attempting Vault credentials\n" );
#				foreach( vault_get_credentials( "sybase" ) ) {
#					die "ERROR";
#					my($l,$p)=split(/;/,$_,2);
#					_debugmsg( "attempting sybase vault Creds $l/$p\n" );
#					$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, 0, $l, $p );
#					last if $ServerMetadata{ $dbi_key }->{BestConnectionMethod} ne "UNKNOWN";
#				}
#			} else {
#				# password files
#				($l,$p)=get_password(-type=>'sybase',-name=>$dbi_key) if ! $l;
#				if( $p ) {
#					_debugmsg( "attempting Creds $l/$p\n" );
#					$ServerMetadata{ $dbi_key } = LoginAndFetch_Sybase( $dbi_key, 0, $l, $p );
#				} else {
#					_debugmsg( "no credentials available for server $dbi_key\n" );
#					$ServerMetadata{ $dbi_key }->{BestConnectionMethod} = "UNKNOWN";
#					$ServerMetadata{ $dbi_key }->{FailedAuthReason} = "No Login/Password Provided";
#				}
#			}
		my(%moreargs);
		$moreargs{-port} 			= $Port_By_DbName{$dbi_key} 		if $Port_By_DbName{$dbi_key};
		$moreargs{-hostname} 	= $Hostname_By_DbName{$dbi_key}  if $Hostname_By_DbName{$dbi_key};
		$moreargs{-system_type} = $Realtype_By_DbName{$dbi_key};
		$moreargs{-system_name} = $dbi_key;
		InvSetSACreds( -DSN=>$dbi_key, -monitor_host=>$HOSTNAME, -metadata=>$ServerMetadata{ $dbi_key }, %moreargs );

		if( $ServerMetadata{ $dbi_key }->{BestConnectionMethod} eq "UNKNOWN" )  {
			if( $p ) {
				_statusmsg("Failed to connect to $dbi_key ".$ServerMetadata{ $dbi_key }->{FailedAuthReason}."\n");
			} else {
				_statusmsg("Ignoring $dbi_key - No Credentials Found\n");
			}
		} else {
			foreach ( keys %{$ServerMetadata{ $dbi_key }} ) {
				printf "%20s >METADATA %20s %s\n", __LINE__, $_, $ServerMetadata{ $dbi_key }->{$_}
					if defined $ServerMetadata{ $dbi_key }->{$_};
			}
		}	
	}
	if( $cfile ) {
		_statusmsg( "Writing $cfile \n");
		open(CACHE,">".$cfile) or die "Cant Write File $cfile";
		foreach my $s ( keys %ServerMetadata ) {
			foreach ( keys %{$ServerMetadata{ $s }} ) {
				print CACHE $s,";",$_,";",$ServerMetadata{ $s }->{$_},"\n"
						if defined $ServerMetadata{ $s }->{$_};
			}
		}
		close(CACHE);
	}
}

sub load_pkg {
	my($str)=@_;
	if ( eval "require $str" ) {
		$str->import();
	} else {
		my $err = $@;
		die "warning: unable to load package $str: $err\n";
	}
}

sub fetch_win32_odbc {
	load_pkg("Win32::ODBC");

	my(%DriverList) = Win32::ODBC::Drivers();

#
# driver report - but its kind of useless
#

	if( $DIAGNOSE ) {
		print "The following drivers are installed:\n";
		foreach my $Driver ( keys( %DriverList ) ) {
			print "$Driver\n";
			print "\tAttributes:\n";
			map { print "\t\t$_\n" } ( split( /;/, $DriverList{$Driver} ) );
		}
	}

	my( %DataSources ) = Win32::ODBC::DataSources();
	_debugmsg( "WIN32 DSN's available from this user account:\n" );
	foreach my $DSN ( sort (keys( %DataSources )) )
	{
	  next unless $DataSources{$DSN}=~/sybase/i
	  			or $DataSources{$DSN}=~/adaptive server/i
	  			or $DataSources{$DSN}=~/mysql/i
	  			or $DataSources{$DSN}=~/oracle/i
	  			or $DataSources{$DSN}=~/sql server/i
	  			or $DataSources{$DSN}=~/sql native/i;
	  next if $DSN=~/localserver/i;

	  my($DSNKEY)=$DSN;
	  $DSNKEY=~s/\..+$//;

	  if( defined $DBI_HASHREF{ "ODBC:".$DSNKEY } ) {
	  		$DBI_HASHREF{ "ODBC:".$DSNKEY } = "OK";
	  } else {
	  		$DBI_HASHREF{ "ODBC:".$DSNKEY } = "ODBCONLY";
	  }
	  my %Config = Win32::ODBC::GetDSN( $DSN );

	  _debugmsg( sprintf( "%-20s %s \n",$DSNKEY,$DataSources{$DSN} ));
	  $Realtype_By_DbName{$DSNKEY} 		= get_realtype_from_drivertype($DataSources{$DSN});
	  # _debugmsg( "  Configuration:\n" );
	  $InternalName_By_DbName{$DSNKEY} = $DSNKEY if $DataSources{$DSN} =~ /sql server/i;
	  foreach my $Attrib ( keys( %Config ) )	  {
	  		_diagmsg( "    $Attrib => '$Config{$Attrib}'\n" );
	      $Description_By_DbName{$DSNKEY} = $Config{$Attrib} if $Attrib =~ /description/i;
	      if( $DataSources{$DSN} =~ /sybase/i or $DataSources{$DSN} =~ /adaptive/i) {
	      	if( $Attrib eq "NetworkAddress" ) {
	      		( 	$Hostname_By_DbName{$DSNKEY}, $Port_By_DbName{$DSNKEY} ) = split(/,/,$Config{$Attrib});
	      	}
	      }
	      if( $Attrib eq "ServerName" and $DataSources{$DSN} =~ /oracle/i ) {
	      	$InternalName_By_DbName{$DSNKEY} = $Config{$Attrib};
	      }
	      if( $DataSources{$DSN} =~ /sql server/i ) {
	    		$InternalName_By_DbName{$DSNKEY} = $Config{$Attrib} if $Attrib =~ /server/i;
	      }
	  }
	  set_registered_hostname_for_host($InternalName_By_DbName{$DSNKEY}) if $InternalName_By_DbName{$DSNKEY};
	  _diagmsg( "\n" );
	}


	# was ignore_unsupported_odbc_servers_on_win32();
	if( $^O eq 'MSWin32' ) {
		header( "THE FOLLOWING NONSUPPORTED ODBC DSNs WILL BE IGNORED" );
		# foreach ( keys %$DBI_HASHREF ) {			# the logic here i am pretty sure works
		foreach ( get_dsn_list() ) {
			next if $DBI_HASHREF{"ODBC:".$_} eq "OK";
			# _statusmsg( "\t$_=>",$DBI_HASHREF{$_} ,"\n" );
			#if( $DBI_HASHREF{$_} eq "DBIONLY" ) {
			delete $DBI_HASHREF{"ODBC:".$_};
			_statusmsg( "Ignoring NONSUPPORTED ODBC DSN $_\n" );
		}
	}
}

sub fetch_oracle_native_info {

	#
	# FETCH ORACLE
	#
	my($last_home_id, $tns_admin, $oracle_home);
	$tns_admin   	= $ENV{TNS_ADMIN};
	$oracle_home   = $ENV{ORACLE_HOME};

	if( $^O eq 'MSWin32' ) {
		eval q{
			require Win32::TieRegistry;
			$Win32::TieRegistry::Registry->Delimiter("/");
			$last_home_id = $Win32::TieRegistry::Registry->{"LMachine/Software/Oracle/ALL_HOMES/LAST_HOME"};

			$tns_admin    = $Win32::TieRegistry::Registry->{"LMachine/Software/Oracle/HOME".$last_home_id."/TNS_ADMIN"} unless $tns_admin;
			$tns_admin    = $Win32::TieRegistry::Registry->{"LMachine/Software/Oracle/HOME0/TNS_ADMIN"} unless $tns_admin;

			$oracle_home    = $Win32::TieRegistry::Registry->{"LMachine/Software/Oracle/HOME".$last_home_id."/ORACLE_HOME"} unless $oracle_home;
			$oracle_home    = $Win32::TieRegistry::Registry->{"LMachine/Software/Oracle/HOME0/ORACLE_HOME"} unless $oracle_home;
		};
	}
	$oracle_home =~ s/\\/\//g;
	$tns_admin   =~ s/\\/\//g;
	foreach my $dir ( "/etc","/var/opt/oracle",$tns_admin ) {
		next unless -d $dir;
		_statusmsg(  "Testing $dir for oratab\n" );
		_statusmsg(  "\tFound $dir/oratab\n" ) if -r "$dir/oratab";

	   next unless open(FH, "<$dir/oratab");
	   _debugmsg( "Loading $dir/oratab\n" );
	   my $ot;
	   while (defined($ot = <FH>)) {
	       next unless $ot =~ m/^\s*(\w+)\s*:\s*(.*?)\s*:/;
	       add_server_to_host($1,"ORACLE_FROM_ORATAB_$2");
	       _debugmsg( "Found ORATAB $1 \@ $2.\n" );
	   }
	   close FH;
	   last;
	}

	foreach my $dir ( $tns_admin,".","$oracle_home/network/admin","$oracle_home/net80/admin","/var/opt/oracle" ) {
			next unless -d $dir;
	   	_debugmsg( "Testing $dir for tnsnames.ora\n" );
	      next unless -r "$dir/tnsnames.ora";
			open(FH, "<$dir/tnsnames.ora") or next;
	      _debugmsg( "Parsing $dir/tnsnames.ora\n" );
	      my($current_servername)='';
	      while (<FH>) {
	      	chomp;chomp;chomp;
	      	next if /^\s*#/;
	      	if(  m/^\s*([-\w\.]+)\s*=/ ) {
	      		 $current_servername = $1;
	      		 $current_servername =~ s/\..+$//;

	      		 if( ! TnsPing($current_servername,$DIAGNOSE) ) {
	      		 	_debugmsg( "\tTnsPing Failed For $current_servername\n" );
	      		 	$current_servername='';
	      		 } else {
		      		 _debugmsg( "FOUND TNSNAMES $current_servername\n" );
		      		 $HostSave_By_UcName{uc($current_servername)}  = 1;
		      		 $Realtype_By_DbName{$current_servername}  = "oracle";
		      		 add_server_to_host($current_servername,"ORACLE_FROM_TNSNAMES");
		      	}
	      	} elsif( $current_servername ne '' ) {
	      		if( m/\(HOST\s*=\s*([\w\d]+)\)/i )   {
	      			my($h)=$1;
	      			$h=get_hostname_by_name($h);
	      			$Hostname_By_DbName{$current_servername}=$h unless $Hostname_By_DbName{$current_servername};
	      			$HostType_By_UcName{uc($h)} =  "?" unless $HostType_By_UcName{uc($h)};
	      			my($xip)=get_ip_by_hostname($h);
						$Host_Comment_By_Ip{$xip} 	.= "Used For Oracle Server $current_servername\n";
	      			_diagmsg("\tHostname_By_DbName = $1\n");
	      		}
	      		if( m/\(PORT\s*=\s*(\d+)\)/i )   {
	      			if( ! $InternalName_By_DbName{$current_servername} ) {
		      			$Port_By_DbName{$current_servername}=$1;
		      			_diagmsg("\tPort_By_DbName = $1\n");
						}
	      		}
	      		if( m/\(SERVER\s*=\s*([\w\d]+)\)/i ) {
	      			if( ! $InternalName_By_DbName{$current_servername} ) {
		      			$InternalName_By_DbName{$current_servername}=$1;
		      			_diagmsg("\tInternalName_By_DbName = $1\n");
		      		}
	      		}
	      		if( m/\(SERVICE_NAME\s*=\s*([\w\d]+)\)/i ) {
	      			my($h)=$1;
	      			if( ! $InternalName_By_DbName{$current_servername} ) {
		      			$InternalName_By_DbName{$current_servername}=$h unless $h eq "DEDICATED";
		      			_diagmsg("\tInternalName_By_DbName = $h\n") unless $h eq "DEDICATED";
		      		}
	      		}
	      	}
	      }
	      close FH;
	      last;                                # only first one found is valid
	}

	_diagmsg( "Note: TnsAdmin=$tns_admin\n" );
	_diagmsg( "Note: OraHome =$oracle_home\n" );
}

sub fetch_sybase_interfaces_win32 {
	error_out( "SYBASE ENV not defined") unless defined $ENV{"SYBASE"};

	# fix up sybase env vbl
	$ENV{"SYBASE"} =~ s#\\#/#g;
	my(@inrc)=generic_fetch( -infile=>$ENV{"SYBASE"}."/ini/sql.ini",
					-keyfile=>"interfaces",
					-desc=>"Sybase Unix Interfaces File",
					-min_rows_returned=>15,
					-os=>'MSWin32');

	my($c)=$#inrc;
	$c--;
	_debugmsg("Found $c Potential Sybase Databases\n");

	my($search_string,$filename)=@_;
	error_out( "SYBASE ENV not defined") unless defined $ENV{"SYBASE"};

	my(@srvlist,%port,%host,%found_svr);

	if( defined $filename and ! -e $filename ) {
		print "Warning: File $filename Not Found\n";
	}

	# DOS
	my($cursvr)="";
	foreach( @inrc) {
		next if /^#/ or /^;/ or /^\s*$/;
		chomp;

#
# MUST ALSO HANDLE BIZARROS LIKE THE FOLLOWING
#
#[NYSISW0035]
#$BASE$00=NLMSNMP,\pipe\sybase\query
#$BASE$01=NLWNSCK,nysisw0035,5000
#MASTER=$BASE$00;$BASE$01;
#$BASE$02=NLMSNMP,\pipe\sybase\query
#$BASE$03=NLWNSCK,nysisw0035,5000
#QUERY=$BASE$02;$BASE$03;

		if( /^\[/ ) {
			# IT A SERVER LINE
			s/^\[//;
			s/\]\s*$//;
			s/\s//g;
			$cursvr=$_;
			push @srvlist,$_ unless defined $found_svr{$_};
			$found_svr{$_} = 1;
		} else {
			# IT A DATA LINE
			next if /^master=/i;
			next if /QUERY=\$BASE/i;
			next if /\,.pipe/i;
			next if /NLMSNMP/i;
			(undef,$host{$cursvr},$port{$cursvr})=split /,/;
		}
	}

	my(@rc);
	foreach (sort @srvlist ) {
		if(defined $search_string) {
			next unless /$search_string/i
				or $host{$_} =~ /$search_string/i
				or $port{$_} =~ /$search_string/i;
		}
		push @rc,[ $_,$host{$_},$port{$_} ];
	}
	return @rc;
}

sub fetch_sybase_interfaces_unix {
	_debugmsg("fetch_sybase_interfaces_unix()\n");
	error_out( "SYBASE ENV not defined") unless defined $ENV{"SYBASE"};
	my(@inrc)=generic_fetch(
					-infile=>$ENV{"SYBASE"}."/interfaces",
					-keyfile=>"interfaces",
					-desc=>"Sybase Unix Interfaces File",
					-min_rows_returned=>15,
					-os=>'Unix');
	my($c)=$#inrc;
	$c--;
	_debugmsg("Found $c Potential Sybase Databases\n");
	my($search_string,$filename)=@_;
	my(@srvlist,%port,%host,%found_svr);

	my($srv,$dummy);
	foreach(@inrc) {
		next if /^#/;
		chomp;chomp;chomp;

		if( /^\s/ ) {
			# print __LINE__," $_\n";
			next unless /query/;
			next if $srv eq "";
			chomp;

			my(@vals)=split;

			# Sun Binary Format
			if( $vals[4] =~ /^\\x/ ) {
				#format \x
				$vals[4] =~ s/^\\x0002//;

				my($p) = hex(substr($vals[4],0,4));
				my($pk_ip) = pack('C4',
					hex(substr($vals[4],4,2)),
					hex(substr($vals[4],6,2)),
					hex(substr($vals[4],8,2)),
					hex(substr($vals[4],10,2)));

				my($name,$aliases,$addrtype,$len,@addrs);
				if( defined $found_name{$pk_ip} ) {
					$name=$found_name{$pk_ip};
				} else {
					($name,$aliases,$addrtype,$len,@addrs) = gethostbyaddr($pk_ip,AF_INET);
					$found_name{$pk_ip}=$name if defined $name and $name !~ /^\s*$/;
				}

				my($h) =$name ||
					hex(substr($vals[4],4,2)).".".
					hex(substr($vals[4],6,2)).".".
					hex(substr($vals[4],8,2)).".".
					hex(substr($vals[4],10,2));

				if( defined $host{$srv}  ) {
					#$host{$srv}.="+".$h;
					#$port{$srv}.="+".$p;
				} else {
					$host{$srv}=$h;
					$port{$srv}=$p;
				}
			} else {
				$port{$srv} = $vals[4];
				$host{$srv} = $vals[3];
			}

			push @srvlist,$srv unless defined $found_svr{$srv};
			$found_svr{$srv} = 1;
			# $srv="";
		} else {
			# print __LINE__," $_\n";
			($srv,$dummy)=split;
		}
	}

	my(@rc);
	# print __LINE__,"Starting Sort",join(" ",@srvlist),"\n";
	foreach (sort @srvlist ) {
		# print __LINE__," ",$_,"\n";
		if(defined $search_string) {
			next unless /$search_string/i
				or $host{$_} =~ /$search_string/i
				or $port{$_} =~ /$search_string/i;
		}
		_diagmsg("$_, $host{$_}, $port{$_}\n");
		push @rc,[ $_,$host{$_},$port{$_} ];
	}
	return @rc;
}

sub dump_it {
	my($lineno) = @_;
	
	use Data::Dumper;
	
	print $lineno," Is_Dsn_By_DbName\n";
	print Dumper \%Is_Dsn_By_DbName;
	print "\n";
	
	print $lineno," Is_Native_By_DbName\n";
	print Dumper \%Is_Native_By_DbName;
	print "\n";
	
	print $lineno," Realtype_By_DbName\n";
	print Dumper \%Realtype_By_DbName;
	print "\n";
	
	print $lineno," CLUSTERNODE_OF_By_Ip\n";
	print Dumper \%CLUSTERNODE_OF_By_Ip;	
	print "\n";
	
}

sub fetch_sybase_native_info {
	# print "DEBUG=$DEBUG\n"
	_debugmsg("fetch_sybase_native_info()\n");
	my(@inrc);
	if( $^O eq 'MSWin32' ) {
		@inrc=fetch_sybase_interfaces_win32();
	} else {
		@inrc=fetch_sybase_interfaces_unix();
	}

	my($cnt)=0;
	my(%DUPLICATECHECK);
	foreach (@inrc) {
		my($nm,$host,$port)=@$_;
		$host =~ s/\..?$//;
		chomp $nm; chomp $host; chomp $port;

		$host=get_hostname_by_name($host);

		my($pp) = tcp_portping($host,$port);
		if( $pp == 0 ) {
			_debugmsg("FAIL portping $nm ($host $port) failed\n");
			_statusmsg(sprintf("Deleting DSN %-20s (Failed Portping)\n",$nm)) if $DBI_HASHREF{"Sybase:".$nm};
			delete $DBI_HASHREF{"Sybase:".$nm} if $DBI_HASHREF{"Sybase:".$nm};
			delete $HASH_OF_DSN_AND_NATIVE{$nm}  if $HASH_OF_DSN_AND_NATIVE{$nm};
			next;
		} else {
			_debugmsg("ok   portping $nm ($host $port) \n" );
		}

		$HostType_By_UcName{uc($host)} =  "?" unless $HostType_By_UcName{uc($host)};
		my($xip)=get_ip_by_hostname($host);
		next unless $xip;
		$cnt++;

		if( $DUPLICATECHECK{$xip."|".$port} and $DBI_HASHREF{"Sybase:".$nm} ) {
			_statusmsg(sprintf("Deleting DSN %-20s (Duplicate of %s)\n",$nm,$DUPLICATECHECK{$xip."|".$port}));
			combine_two_dsns($nm,$DUPLICATECHECK{$xip."|".$port});
			#delete $DBI_HASHREF{"Sybase:".$nm} if $DBI_HASHREF{"Sybase:".$nm};
			#delete $HASH_OF_DSN_AND_NATIVE{$nm}  if $HASH_OF_DSN_AND_NATIVE{$nm};
			next;
		}
		$DUPLICATECHECK{$xip."|".$port}=$nm;

		$Is_Native_By_DbName{$nm}=1;
		$Realtype_By_DbName{$nm}="sybase";
		$HASH_OF_DSN_AND_NATIVE{$nm}=$nm;
		$Description_By_DbName{$nm}="Sybase Ase Server";
		$Ip_By_DbName{$nm} = $xip;
		$HostType_By_Ip{$xip}='' unless $HostType_By_Ip{$xip} ne '';

		$Host_Comment_By_Ip{$xip} 	.= "Used For Sybase Server $nm\n";

		$HostType_By_UcName{uc($host)} = $HostType_By_Ip{$xip}
			if $HostType_By_UcName{uc($host)} eq "?" and $xip and $HostType_By_Ip{$xip};

		$Port_By_DbName{$nm}=$port     unless $Port_By_DbName{$nm};
      $Hostname_By_DbName{$nm}=$host unless $Hostname_By_DbName{$nm};

		my($type);
		if( $nm=~/_BACK$/i ) {
			$type="Sybase_Backup_Server";
		} elsif( $nm=~/_HS$/i ) {
			$type="Sybase_Historical_Server";
		} elsif( $nm=~/_XP$/i ) {
			$type="Sybase_XP_Server";
		} elsif( $nm=~/_MON$/i ) {
			$type="Sybase_Monitor_Server";
		} elsif( $nm=~/_BACKUP$/i ) {
			$type="Sybase_Backup_Server";
		} elsif( $nm=~/_RS$/i ) {
			$type="Sybase_Rep_Server";
		} elsif( $nm=~/_BS$/i ) {
			$type="Sybase_Backup_Server";
		} elsif( $port == 1433 ) {
			$type="SQL_Server_From_Interfaces";
			$HostType_By_UcName{uc($host)} = "WIN32" unless $HostType_By_UcName{uc($host)};
			my($xip)=get_ip_by_hostname($host);
			$HostType_By_Ip{$xip} = "WIN32" unless $HostType_By_Ip{$xip};

		} else {
			$type="Possible_Sybase_Server";
		}
		if( $type ne "Possible_Sybase_Server" ) {
			_statusmsg(sprintf("Deleting DSN %-20s (%s)\n",$nm,$type)) if $DBI_HASHREF{"Sybase:".$nm};
			delete $DBI_HASHREF{"Sybase:".$nm} if $DBI_HASHREF{"Sybase:".$nm};
			delete $HASH_OF_DSN_AND_NATIVE{$nm}  if $HASH_OF_DSN_AND_NATIVE{$nm};
		} else {
			add_server_to_host($nm,$type);
		}
	}
	_statusmsg("Found $cnt Pingable Sybase Servers\n");
}


####################################### FUNCTIONS #############################################


#sub get_all_uchosts_by_ip {
#	my($ip)=@_;
#	my(@rc);
#	foreach ( keys %ipcache ) { push @rc,$_ if $ipcache{$_} eq $ip; }
#	return @rc;
#}

sub get_all_hostnames_by_ip {
	my($ip)=@_;
	my(@rc);	my(@uchosts);
	foreach ( keys %ipcache ) { push @uchosts,$_ if $ipcache{$_} eq $ip; }

	my(@unfoundhosts);
	foreach my $a (@uchosts) {
		# case match
		if( $HostType_By_UcName{uc($a)} ) {
			push @rc,$a;
			next;
		}

		# uncased match
		my($found)=0;
		foreach ( keys %HostType_By_UcName ) {
			next unless lc($a) eq lc($_);
			push @rc, $_;
			$found++;
			last;
		}
		next if $found;

		# print "DIAG - unfounded host $a\n";
		push @unfoundhosts,$a;
	}
	return( @rc, @unfoundhosts );
}

sub get_hostname_by_name {
	my($nm)=@_;
	my($ip)=get_ip_by_hostname($nm);
	my($h)=get_registered_hostname_by_ip($ip);
	return $h if $h;
	my(@a)=get_all_hostnames_by_ip($ip);
	return $a[0];
}

sub get_all_hosts_by_name {
	my($nm)=@_;
	my($ip)=get_ip_by_hostname($nm);
	return get_all_hostnames_by_ip($ip);
}

sub get_hostname_by_ip {
	my($ip)=@_;

	my($h)=get_registered_hostname_by_ip($ip);
	return $h if $h;

	my(@h)=get_all_hostnames_by_ip($ip);
	foreach (@h) { my(@x)=get_all_hosts_by_name($_); return $x[0] if $#x>=-1; }
	#foreach ( keys %ipcache ) { return $_ if $ipcache{$_} eq $ip; }
	return undef;
}

sub get_ip_by_hostname {
	my($svr)=@_;

	if( $svr =~ /\d+\.\d+\.\d+\.\d+/ ) {		# ip address
		$ipcache{$svr} = $svr;
		return $svr;
	}
	return $ipcache{ $svr } if $ipcache{ $svr };
	$svr=~s/\..+$//;
	return $ipcache{ $svr } if $ipcache{ $svr };
	return $ipcache{ uc($svr) } if $ipcache{ uc($svr) };
	return undef if $svr =~ /\\/;
	return undef if $IPFAIL{$svr};

	my(@address)=gethostbyname($svr);

	if( $#address<0 ) {
		_diagmsg( "Cant resolve $svr $!\n" );
		$IPFAIL{$svr}=1;
		return undef;
	}

	@address= map { inet_ntoa($_) } @address[4..$#address];
	_diagmsg( "  ==> get_ip_by_hostname($svr) = ",join(" ",@address),"\n" );
	$IPFAIL{$svr}=1 if $#address<0;
	print "DBG DBG: IP FAILED FOR $svr\n" if $#address<0;
	$ipcache{uc($svr)} = $address[0];
	return $address[0];
}

sub error_out { die @_; }
sub _statusmsg { print "        ",@_; }
sub header {
	my($str)=join("",@_);
	$str =~ s/\s*$//;
	print "\n--------------------------------------------------------------------------\n| ",
		$str,
		"\n","--------------------------------------------------------------------------\n";
}

sub _debugmsg { print( "[debug] ",@_) if $DEBUG; }
sub _diagmsg  { print( "[diag]  ",@_) if $DIAGNOSE; }
sub RunCommand {
 	my($cmd)=@_;
 	if( ! open( WINCMD,"$cmd |")) {
		print "Cant Run $cmd\n";
		return undef;
	}
	my(@output);
   while ( <WINCMD> ) {
   	chomp; chomp;
		push @output, $_;
   }
   close(WINCMD);
   return @output;
}

sub generic_fetch {
	my(%args)=@_;
	my($cmd)			=	$args{-cmd};
	my($keyfile)	=	$args{-keyfile};
	my($desc)		=	$args{-desc};
	my($min_rows_returned)=$args{-min_rows_returned};
	my($os)=$args{-os};
	my(@rc);
	my($cfile);

	my($is_os_ok);
	# only two types of os = win and not win
	if( $^O eq $os ) {
		$is_os_ok=1;
#	} elsif( $os eq "MSWin32" ) {
#		$is_os_ok=0;
	} else {
		$is_os_ok=0;
	}

	if( $args{-infile} ) {
		my($k)=get_writeable_discovery_file($keyfile);
		$cfile=$k if -r $k;
	} else {
		$cfile=get_best_readable_discovery_filenm($keyfile);
	}

	my($prefetch);
	if( $is_os_ok and ( $UPDATECACHE or ! $cfile ) ) {
		$prefetch=1;
		_debugmsg( "[func] Running $desc\n" );
		my(@rcc);
		if( defined $args{-infile} ) {
			if( ! $args{-infile} ) {
				_statusmsg("Not copying - no input file\n");
			} else {
				$cfile=get_writeable_discovery_file($keyfile);
				_statusmsg("Copying $args{-infile}\n");
				_statusmsg("     To $cfile\n");
				copy($args{-infile},$cfile) or die "Cant Copy $args{-infile} to $cfile $!\n";
			}
		} else {
			_statusmsg("Executing $cmd\n");
			my($timeout)=7;
			my(@rcc);
			local $SIG{ALRM} = sub { die "CommandTimedout"; };
			eval {
				alarm($timeout);
				@rcc=RunCommand(   $cmd	);
			  	alarm(0);
			};
			if( $@ ) {
				if( $@=~/CommandTimedout/ ) {
					_statusmsg( "[func] $cmd COMMAND TIMED OUT.\n" );
					return();
				} else {
					_statusmsg( "[func] $cmd Command Returned $@\n" );
					alarm(0);
				}
			}

			warn "COMMAND $cmd RETURNED < $min_rows_returned ROWS".join("\n",@rcc) if $#rcc<$min_rows_returned;
			$cfile=get_writeable_discovery_file($keyfile);
			_statusmsg('Writing '.$#rcc.' Rows To File $cfile\n');
			open(CACHE,">".$cfile) or die "Cant Write File $cfile";
			print CACHE join("\n",@rcc);
			close(CACHE);
			_debugmsg( "[func] Done Writing $cfile\n" );
		}
	} else {
		if( ! $is_os_ok ) {
			_statusmsg( "[func] is_os_ok=$is_os_ok - not fetching $desc\n" );
		} else {
			_statusmsg( "[func] is_os_ok=$is_os_ok - but no --UPDATECACH and no cfile $cfile\n" );
		}		
	}

	if( $args{-READALL} ) {
		my(@filelist)=get_best_readable_discovery_filenm($keyfile,1);
		foreach my $f ( @filelist ) {
			if( -r $f ) {
				_statusmsg("Reading CacheFile $f\n");
				open(CACHE,$f) or die "Cant Read File $f";
				while(<CACHE>) { s/\s+$//;chomp;chomp;push @rc,$_; }
				close(CACHE);
			}
		}
	} elsif( -r $cfile ) {
		_statusmsg("Reading Cached Data From File $cfile\n") unless $prefetch;
		open(CACHE,$cfile) or die "Cant Read File $cfile";
		while(<CACHE>) { s/\s+$//;chomp;chomp;push @rc,$_; }
		close(CACHE);
	} elsif( -e $cfile ) {
		die "Woah - File $cfile exists but is not readable \n";
	}
	return @rc;
}

sub fetch_cmd_osql_l {
	my(%svrs);
	my(@rc)=generic_fetch( -cmd=>"osql -L",
					-keyfile=>"osql_l",
					-desc=>"Sql Server Fetch",
					-min_rows_returned=>5,
					-os=>'MSWin32',
					-READALL=>1);
	my(@sqlservers);
	foreach ( @rc ) {
		chomp;chomp;
		s/^\s+//;

		next if /^Server/ or /^----/ or /^\s*$/ or /\\SQLEXPRESS/;
		push @sqlservers,uc($_);
	}
	# print "DBGDBG: returning $#sqlservers\n";
	return @sqlservers;
}

my(@discovery_files,$no_discovery_file);
my(%discovery_ok);
my($ddir);
my($discoveryokcnt)=0;

sub get_writeable_discovery_file {
	my($srchkey,$hostname)=@_;
	$hostname=$HOSTNAME unless $hostname;
	
	my($file) = $ddir."/".$hostname."_".$srchkey.".txt";
	$file= $ddir."/".$hostname."_".$srchkey.".dat" if $srchkey =~ /password$/;
	
	return $file;
}

sub get_best_readable_discovery_filenm {
	return undef if defined $no_discovery_file;
	my($srchkey,$returnall)=@_;

	if( $#discovery_files<0 ) {
		$ddir=$GEM_ROOT_DIR."/discovery";
		opendir(DIR,$ddir) or die("Cant open directory $ddir for reading : $!\n");
		@discovery_files = grep(!/^\./,readdir(DIR));
		closedir(DIR);
		$no_discovery_file = 'TRUE' if $#discovery_files<0;
		foreach ( @discovery_files ) {
			if( /^(.+)_discoveryok.txt$/ ) {
				$discovery_ok{$1}="TRUE";
				_statusmsg("Found Discovery Completed On $1\n");
				$discoveryokcnt++;
			}
		}
	}

	my(@candidates);
	my($pref_cand);
	foreach ( @discovery_files ) {
		next unless /$srchkey.txt$/;
		if( /${HOSTNAME}_$srchkey.txt$/ ) {
			_statusmsg( "Using $_ as file $srchkey\n" );
			$pref_cand = $ddir."/".$_;
		}
		push @candidates,$ddir."/".$_;
	}
	return @candidates if $returnall;
	return $pref_cand if $pref_cand;
	return $candidates[0];
}

sub fetch_cmd_net_view {
	my(@rc)=generic_fetch( -cmd=>"net view",
					-keyfile=>"netview",
					-desc=>"Windows Server Fetch",
					-min_rows_returned=>5,
					-os=>'MSWin32',
					-READALL=>1);
	my(%svrs);
	my($cnt)=0;
	foreach ( @rc ) {
		chomp;
		next if /^Server/ or /^----/ or /^\s+/ or /^The command completed/ or /^\s*$/;
		my($h,$comment)=split(/\s+/,$_,2);
		$h=~s/^\\\\//;
		$svrs{$h} = $comment;
		$cnt++;
	}
	_statusmsg("\t$cnt Rows Fetched\n");
	return %svrs;
}

sub get_servertype_keys {
	my(%rc);
	foreach( values %ServerType_List_By_DbName ) {
		next if /;/;
		$rc{$_}=1;
	}
	return( keys %rc );
}

sub get_servers_by_type {
	my($type)=@_;
	my(%rc);
	return keys(%ServerType_List_By_DbName) unless $type;
	foreach( keys %ServerType_List_By_DbName ) {
		next unless $ServerType_List_By_DbName{$_} =~ $type;
		$rc{$_}=1;
	}
	return keys %rc;
}

sub get_types_by_server {
	my($key)=@_;
	return undef unless defined $ServerType_List_By_DbName{$key};
	return split(/;/, $ServerType_List_By_DbName{$key} );
}

sub add_server_to_host {
	my($host,$new_servertype)=@_;

	if( $host =~ /\d+\.\d+\.\d+\.\d+/ ) {
		$host= get_hostname_by_ip($host);
	} elsif( /\./ ) {
		$host =~ s/\..+$//;
	}

	_diagmsg( "   $host is a $new_servertype\n" );
	if( defined $ServerType_List_By_DbName{$host} ) {
		$ServerType_List_By_DbName{$host} .= ";".$new_servertype;
	} else {
		$ServerType_List_By_DbName{$host} = $new_servertype;
	}
}

#sub remove_server_from_host {
#	my($host,$new_servertype)=@_;
#
#	if( $host =~ /\d+\.\d+\.\d+\.\d+/ ) {
#		$host= get_hostname_by_ip($host);
#	} elsif( /\./ ) {
#		$host =~ s/\..+$//;
#	}
#
#	_debugmsg( "   $host dropping $new_servertype\n" );
#	my($x)=$new_servertype.";";
#	if( $ServerType_List_By_DbName{$host} eq $new_servertype ) {
#		delete $ServerType_List_By_DbName{$host};
#	} elsif( $ServerType_List_By_DbName{$host} =~ /$x/) {
#		$ServerType_List_By_DbName{$host} =~ s/$x//;
#	} else {
#		$x=";".$new_servertype;
#		if( $ServerType_List_By_DbName{$host} =~ /$x/) {
#			$ServerType_List_By_DbName{$host} =~ s/$x//;
#		}
#	}
#}

# sub show_both { my($a,$b,$c)=@_; return $a if $a; return $b if $b; return $c; }

sub fetch_dnsaliases {
	my(@rc)=generic_fetch( -cmd=>"ypcat hosts",
					-keyfile=>"ypcat_hosts",
					-desc=>"Ypcat Hosts",
					-min_rows_returned=>5,
					-os=>'Unix');
	foreach ( @rc ) {
		chomp;chomp;
		my($ip,$firstnm,@addrs) = split;
		$ipcache{ uc($firstnm) } = $ip;
		$Registered_Hostname_By_Ip{$ip} = uc($firstnm) unless $Registered_Hostname_By_Ip{$ip};
		foreach(@addrs) {
			$ipcache{ uc($_) } = $ip;
			$HostType_By_Ip{$ip}='' unless $HostType_By_Ip{$ip};
	#		$Registered_Hostname_By_Ip{$ip} = uc($firstnm) unless $Registered_Hostname_By_Ip{$ip};
		}
	}
}

sub get_registered_hostname_by_ip 	{	my($ip)=@_; return $Registered_Hostname_By_Ip{$ip}; }

sub get_registered_hostname_by_host 	{
	my($hnm)=@_;
	my($ip)=get_ip_by_hostname($hnm);
	return undef unless $ip;
	return $Registered_Hostname_By_Ip{$ip};
}

sub set_registered_hostname_for_host {
	my($hnm,$dont_overwrite)=@_;
	my($ip) = get_ip_by_hostname($hnm);
	return undef unless $ip;
	return if $dont_overwrite and $Registered_Hostname_By_Ip{$ip};
	$Registered_Hostname_By_Ip{$ip} = $hnm;
}

sub set_registered_hostname_for_ip 	{
	my($name,$ip)=@_;
	$Registered_Hostname_By_Ip{$ip} = $name;
}

sub discover_unix {
	return if $^O eq 'MSWin32';
	header("Fetch UNIX Host Info Of ssh enabled hosts");
	foreach my $h ( keys %HostType_By_UcName ) {
		next if $HostType_By_UcName{$h} eq "WIN32";
		print "LINE ",__LINE__," DBGDBG: Not done - fetch unix host info for $h\n";
		my($metadataref)=LoginAndFetch_Unix($h);
	}
}

sub discover_win32 {
	header("discover_win32: Drive Info");
	my($cfile)=get_best_readable_discovery_filenm("win32_driveinfo");
	my($count)=0;
	if( $^O eq 'MSWin32' and ( $UPDATECACHE or ! $cfile ) ) {
		_debugmsg( "[func] Running Win32 Driveinfo to Get Windows Server Drives\n" );
		load_pkg("Win32::DriveInfo");
		foreach my $h ( keys %HostType_By_UcName ) {
			next unless $HostType_By_UcName{$h} eq "WIN32";
			my($metadataref)=LoginAndFetch_Win32($h);			
			my($h_ip) = get_ip_by_hostname($h);
			#$DriveInfo_By_Ip{$h_ip} = "";
			# $DriveInfo_By_Ip{$h_ip} = join(",",@dsks);
			$DriveInfo_By_Ip{$h_ip} = $metadataref->{DISKS};
			if( $metadataref->{DISKS} ) {
				$HostType_By_Ip{$h_ip}="WIN32";
				$HostType_By_UcName{$h}="WIN32";
			}
			$count++ if $metadataref->{DISKS};
			print "Fetched $h - $DriveInfo_By_Ip{$h_ip} \n" if $metadataref->{DISKS};
		}
		_debugmsg( "[func] Done Running Win32 Driveinfo to Get Windows Servers\n" );
		$cfile=get_writeable_discovery_file("win32_driveinfo");
		open(CACHE,">".$cfile) or die "Cant Write File $cfile";
		foreach ( keys %DriveInfo_By_Ip ) {
			print CACHE $_,";",$DriveInfo_By_Ip{$_},"\n";
		}
		close(CACHE);
		_debugmsg( "[func] Done Writing $cfile\n" );

	} elsif( -r $cfile ) {
		_statusmsg("Reading Cache File $cfile\n");
		open(CACHE,$cfile) or die "Cant Read File $cfile";
		while(<CACHE>) { chomp;chomp;
			my(@a)=split(/;/,$_,2);
			$DriveInfo_By_Ip{$a[0]}=$a[1];
			if( $a[1] ) {
				$count++;
				$HostType_By_Ip{$a[0]}="WIN32";
				# $HostType_By_UcName{$h}="WIN32";
			}
		}
		close(CACHE);
	}
	_statusmsg("Found $count Readable FileSystems\n");
}


sub fetch_ssh_profile {
	my($f)=$ENV{HOME}."/.ssh/known_hosts" 
		if -r $ENV{HOME}."/.ssh/known_hosts";
	$f=$ENV{HOMEDRIVE}."/.ssh/known_hosts" 
		if ! $f and -r $ENV{HOMEDRIVE}."/.ssh/known_hosts";
	$f="" unless $f;
	my(@inrc)=generic_fetch(
					-infile=>$f,
					-keyfile=>"ssh_known_hosts",
					-desc=>"Ssh Unix Host Fetch",
					-min_rows_returned=>5,
					-os=>'Unix',
					-READALL=>1 );

	my(%sshrc);
	my($count)=0;
	foreach(@inrc){
		my(@x)=split;
		if( $x[0] =~ /,/ ) {
			my($host,$ip)=split(/,/,$x[0]);
			$host=~s/\..?$//;
			$sshrc{$host}=$ip;
		} else {
			my($host)=$x[0];
			$host=~s/\..?$//;
			$sshrc{$host}= "";
		}
		$count++;
	}
	_statusmsg("Found $count Unix Hosts By Parsing SSH info\n");
	return %sshrc;
}

my(%password_vault);
sub vault_get_credentials {
	my($type)=@_;
	my(@rc);
	push @rc,";" if $type eq "sqlsvr";		# native auth is first
	foreach ( keys %password_vault ) {
		my($a,$b,$c)=split(/;/,$_,3);
		next unless $a eq $type;
		push @rc, $b.";".$c;
	}
	return @rc;
}

sub vault_read {
	my($VAULT)=$GEM_ROOT_DIR."/conf/password_vault.dat";
	if( -r $VAULT ) {
		_debugmsg("VAULT FILE =$VAULT FOUND\n");
		open(V,$VAULT) or die "Cant read password vault $VAULT $!\n";
		while(<V>) {
			chop;chomp;chomp;
			next if /^#/;
			my(@a)=split(/;/,$_,3);
			$password_vault{$a[0].";".$a[1].";".$a[2]}=$a[2];
		}
		close(V);
	} else {
		_debugmsg("NO VAULT FILE FOUND\n");
	}
}

sub get_realtype_from_drivertype {
	my($k)=@_;
	return "sqlsvr" if $k eq "SQL Server"	or  $k eq "SQL Native Client";
	return "mysql"  if $k =~ /mysql/i;
	return "oracle" if $k =~ /oracle/i;
	return "sybase" if $k =~ /sybase/i;
	return undef;
}

#sub discovery_is_ok {
#	my($f)=$GEM_ROOT_DIR."/discovery/${HOSTNAME}_discoveryok.txt";
#	open( F, ">".$f) or die "cant write $f $!";
#	print F "Ok at ".time()."\n";
#	close(F);
#}
#
#sub discovery_ok_report {
#	foreach (keys %discovery_ok) {
#		print "Discovery is ok on server $_\n";
#	}
#}
