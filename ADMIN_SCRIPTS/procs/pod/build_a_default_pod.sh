isql -USYS_INSTALL -PPASSWORD -SSYBASE_LOCAL -otmp1 << EOF
exec $1
go
EOF
isql -USYS_INSTALL -PPASSWORD -SSYBASE_LOCAL -otmp2 << EOF
use sybsystemprocs
go
exec sp__syntax $1
go
EOF

cat > $1.pod << EOF
=head1 NAME

$1 - rewrite

=head2 AUTHOR

Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION

=head2 SEE ALSO

=head2 USAGE

EOF

cat tmp2 >> $1.pod
rm  tmp2
echo "" >> $1.pod
echo "=head2 SAMPLE OUTPUT" >> $1.pod
echo "" >> $1.pod
cat tmp1 >> $1.pod
rm  tmp1

