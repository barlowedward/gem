create view BarlowMachineView
as
select m.*,plcntry,plcity,plbuild,mmake,mmodel, picture=(select picture from pictures where modid = mm.mmodel )
from machines m, places p, makemodel mm
where m.cntrycitybldgnum	*=p.plid
and   m.machinename			!='Name'
and   m.makmodid            *= mm.mid
