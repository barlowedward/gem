-- report_key=OBJECT_USAGE
-- run_frequency=Daily
-- do_diff=Y
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=
-- report_footer=
-- enabled=Y
-- performancetype=int
-- performancekey=object_usage

/*
select servername=@@SERVERNAME,
	dbname=db_name(dbid),
	tablename=object_name(objectid,dbid),
	'',
	execution_count,
	total_worker_time,
	total_logical_reads,
	total_logical_writes,
	max_worker_time
from sys.dm_exec_query_stats s1
	cross apply sys.dm_exec_sql_text(sql_handle) as  s2
where total_logical_reads+total_logical_writes>1000000
and dbid is not null
and objectid is not null
order by total_logical_reads+total_logical_writes desc
*/

select distinct
		@@SERVERNAME,
		case when db_name(dbid) is null
				then 'Adhoc Queries'
		else  db_name(dbid)
	  end as db_name,
	tablename=object_name(objectid,dbid),
	'',
	executions=sum(execution_count),
	logical_reads=sum(total_logical_reads),
	logical_writes=sum(total_logical_writes),
	worker_time=sum(total_worker_time),
	max_worker_time=sum(max_worker_time)
	from sys.dm_exec_query_stats
	cross apply sys.dm_exec_sql_text(sql_handle) as  s2
	where db_name(dbid) not in ('master','model','msdb','tempdb','distribution')
	and total_logical_reads+total_logical_writes > 1
	and object_name(objectid,dbid) is not null
	group by db_name(dbid),object_name(objectid,dbid)
