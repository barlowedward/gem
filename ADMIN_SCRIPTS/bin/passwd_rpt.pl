#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2004 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Std;
use File::Basename;
use Repository;
use DBIFunc;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

sub usage
{
	 return "Usage: $0 [-osxh] -Tsybase|sqlsvr\n-o oper roles only\n-s sa roles only\n-x sso roles only\n-h htmlize output\n-u print by user not server\n-d debug mode\n";
}

use vars qw($DEBUG $passwordfile $opt_v $opt_u $opt_o $opt_s $opt_x $opt_h $opt_d $opt_T);

my($VERSION)=1.02;

die usage() unless getopts('vuosxhdT:');
die usage() if defined $opt_v;
$opt_T="sybase" unless $opt_T;
if( $opt_T ne "sqlsvr" and ! defined $opt_o and ! defined $opt_s and ! defined $opt_x ) {
	 $opt_o = "1";
	 $opt_s = "1";
	 $opt_x = "1";
}
$DEBUG=1 if defined $opt_d;

my($nl)="\n";
$nl="<br>\n" if $opt_h;
print "passwd_rpt.pl version $VERSION",$nl;

my $ct =localtime(time);
print "Run at ",$ct,$nl;
print "Running On Server Type: ",$opt_T, $nl;
print "Sybase=",$ENV{SYBASE},$nl if $opt_T eq "sybase";
print "This program creates a report on roles in the system.",$nl;

my(@srv)=get_password(-type=>$opt_T);
if( $#srv<0 ) {
	print "Hmmmm... no servers found in sybase password file... is it set up";
	print "Unable to continue\n";
	exit(0);
}

my($query)="
   select   name
   from     master.dbo.syslogins l, master..sysloginroles s
   where l.suid = s.suid
   and   srid=";

$query="select name, sysadmin, securityadmin, serveradmin, setupadmin, processadmin, diskadmin, dbcreator
from master..syslogins" if $opt_T eq "sqlsvr";

my(%sysadmin, %securityadmin, %serveradmin, %setupadmin, %processadmin, %diskadmin, %dbcreator, %bulkadmin);
my(%u_sysadmin, %u_securityadmin, %u_serveradmin, %u_setupadmin, %u_processadmin, %u_diskadmin, %u_dbcreator, %u_bulkadmin);

# sa{srv} is space separated list of sa's
# u_sa{usr} is space separated list of servers'
my( %sa,%sso,%oper,%u_sa,%u_sso,%u_oper,@rc,$server );
SERVER: foreach $server (@srv) {

	 print "Working on $server... " if defined $opt_d;
	 $sa{$server}="";
	 $sso{$server}="";
	 $oper{$server}="";

	 my($login,$password)=get_password(-type=>$opt_T,-name=>$server);
	 if( ! defined $login or ! defined $password ) {
		  print "Cant Find Login to $server",$nl;
		  next;
	 }
	 if( ! dbi_connect(-srv=>$server,-login=>$login,-password=>$password) ) {
		  print "Cant Connect to $server",$nl;
		  next;
	 }

	 if( $opt_T eq "sqlsvr" ) {
		  @rc=dbi_query(-db=>"master",-query=>$query);
		  foreach (@rc) {
			my($n, @roles)=dbi_decode_row($_);

			my($a);
			$a = shift @roles;
			if( $a == 1 ) {
			  	$u_sysadmin{$n} = "" if ! defined $u_sysadmin{$n};
				$u_sysadmin{$n} .= $server." " ;
				$sysadmin{$server}.=$n." ";
			}
			$a = shift @roles;
			if( $a == 1 ) {
			  	$u_securityadmin{$n} = "" if ! defined $u_securityadmin{$n};
				$u_securityadmin{$n} .= $server." " ;
				$securityadmin{$server}.=$n." ";
			}
			$a = shift @roles;
			if( $a == 1 ) {
			  	$u_serveradmin{$n} = "" if ! defined $u_serveradmin{$n};
				$u_serveradmin{$n} .= $server." " ;
				$serveradmin{$server}.=$n." ";
			}
			$a = shift @roles;
			if( $a == 1 ) {
			  	$u_setupadmin{$n} = "" if ! defined $u_setupadmin{$n};
				$u_setupadmin{$n} .= $server." " ;
				$setupadmin{$server}.=$n." ";
			}
			$a = shift @roles;
			if( $a == 1 ) {
			  	$u_processadmin{$n} = "" if ! defined $u_processadmin{$n};
				$u_processadmin{$n} .= $server." " ;
				$processadmin{$server}.=$n." ";
			}
			$a = shift @roles;
			if( $a == 1 ) {
			  	$u_diskadmin{$n} = "" if ! defined $u_diskadmin{$n};
				$u_diskadmin{$n} .= $server." " ;
				$diskadmin{$server}.=$n." ";
			}
			$a = shift @roles;
			if( $a == 1 ) {
			  	$u_dbcreator{$n} = "" if ! defined $u_dbcreator{$n};
				$u_dbcreator{$n} .= $server." " ;
				$dbcreator{$server}.=$n." ";
			}
			#$a = shift @roles;
			#if( $a == 1 ) {
			 # 	$u_bulkadmin{$n} = "" if ! defined $u_bulkadmin{$n};
			#	$u_bulkadmin{$n} .= $server." " ;
			#	$bulkadmin{$server}.=$n." ";
			#}
		  }
	 } else {
		 if( defined $opt_s ) {
			  print "Sending $query 0\n" if defined $opt_d;
			  @rc=dbi_query(-db=>"master",-query=>$query."0");
			  foreach (@rc) {
				my($n)=dbi_decode_row($_);
				   $u_sa{$n} = "" if ! defined $u_sa{$n};
				   $u_sa{$n} .= $server." " ;
				   $sa{$server}.=$n." ";
			  }
		 }

		 if( defined $opt_x ) {
			  print "Sending $query 1\n" if defined $opt_d;
			  @rc=dbi_query(-db=>"master",-query=>$query."1");
			  foreach (@rc) {
					my($n)=dbi_decode_row($_);
				   $sso{$server}.=$n." ";
				   $u_sso{$n} = "" if ! defined $u_sso{$n};
				   $u_sso{$n} .= $server." " ;
			  }
		 }

		 if( defined $opt_o ) {
			  print "Sending $query 2\n" if defined $opt_d;
			  @rc=dbi_query(-db=>"master",-query=>$query."2");
			  foreach (@rc) {
					my($n)=dbi_decode_row($_);
				   $oper{$server}.=$n." ";
				   $u_oper{$n} = "" if ! defined $u_oper{$n};
				   $u_oper{$n} .= $server." " ;
			  }
		 }
	 }

	 dbi_disconnect();
}

my($user);

if( $opt_T eq "sqlsvr" ) {
#my(%sysadmin, %securityadmin, %serveradmin, %setupadmin, %processadmin, %diskadmin, %dbcreator, %bulkadmin);
#my(%u_sysadmin, %u_securityadmin, %u_serveradmin, %u_setupadmin, %u_processadmin, %u_diskadmin, %u_dbcreator, %u_bulkadmin);
	printit( "LIST OF USERS WITH SA ROLE", \%u_sysadmin, \%sysadmin);
	printit( "LIST OF USERS WITH SECURITY ADMIN ROLE", \%u_securityadmin, \%securityadmin);
	printit( "LIST OF USERS WITH SERVER ADMIN ROLE", \%u_serveradmin, \%serveradmin);
	printit( "LIST OF USERS WITH SETUP ADMIN ROLE", \%u_setupadmin, \%setupadmin);
	printit( "LIST OF USERS WITH PROCESS ADMIN ROLE", \%u_processadmin, \%processadmin);
	printit( "LIST OF USERS WITH DISK ADMIN ROLE", \%u_diskadmin, \%diskadmin);
	printit( "LIST OF USERS WITH DB CREATOR ROLE", \%u_dbcreator, \%dbcreator);
	#printit( "LIST OF USERS WITH BULK ADMIN ROLE", \%u_bulkadmin, \%bulkadmin);

}

sub printit {
	my($title,$hashref1,$hashref2)=@_;
	 print heading($title);
	 if( defined $opt_u ) {
		  foreach $user (keys %u_sysadmin) {
			   print pretty_print($user,split /\s/,$$hashref1{$user});
		  }
	 } else {
		  foreach $server (@srv) {
			   print pretty_print($server,split /\s/,$$hashref2{$server});
		  }
	 }
	 print end_heading();
}

if( defined $opt_s ) {
	 print heading("LIST OF USERS WITH SA ROLE");
	 if( defined $opt_u ) {
		  foreach $user (keys %u_sa) {
			   print pretty_print($user,split /\s/,$u_sa{$user});
		  }
	 } else {
		  foreach $server (@srv) {
			   print pretty_print($server,split /\s/,$sa{$server});
		  }
	 }
	 print end_heading();
}

if( defined $opt_x ) {
	 print heading("LIST OF USERS WITH SSO ROLE");
	 if( defined $opt_u ) {
		  foreach $user (keys %u_sso) {
			   print pretty_print($user,split /\s/,$u_sso{$user});
		  }
	 } else {
		  foreach $server (@srv) {
			   print pretty_print($server,split /\s/,$sso{$server});
		  }
	 }
	 print end_heading();
}

if( defined $opt_o ) {
	 print heading("LIST OF USERS WITH OPER ROLE");
	 if( defined $opt_u ) {
		  foreach $user (keys %u_oper) {
			   print pretty_print($user,split /\s/,$u_oper{$user});
		  }
	 } else {
		  foreach $server (@srv) {
			   print pretty_print($server,split /\s/,$oper{$server});
		  }
	 }
	 print end_heading();
}

sub end_heading {
	return "</TABLE>\n" if $opt_h;
}
sub heading
{
	my($h)=@_;
	if( $opt_h ) {
		return "<H1>$h</H1>\n<TABLE BORDER=1>";
	}

	my($i);
	my($str)="";
	for( $i=0;$i<80;$i++) { $str .= "*"; }
	$str.= "\n";
	$str.= "* ".sprintf("%45.45s",$h);
	for( $i=0;$i<32;$i++) { $str .= " "; }
	$str.= "*\n";
	for( $i=0;$i<80;$i++) { $str .= "*"; }
	$str.= "\n";
	return $str;
}

# Print In A Nice Format
sub pretty_print
{
	 my($hdr,@data)=@_;
	 if( $opt_h ) {
	 	return "<TR><TD BGCOLOR=BEIGE>$hdr</TD><TD>".join(" ",@data)."</TD></TR>\n";
	 }
	 my($column)=30;
	 my($outstr)=sprintf("%-30.30s",$hdr);
	 my($thirty)="				  ";

	 # add to outstr if it keeps it under 80 columns
	 # else put on new line with 30 spaces at start
	 my($cell);
	 foreach $cell (@data) {
		  my($l)=length($cell);
		  if($column+$l == 80 ) {
			   $outstr.="$cell\n$thirty";
			   $column=30;
		  } elsif($column+$l < 80 ) {
			   $column += $l+1;
			   $outstr.="$cell ";
		  } else {
			   $outstr.="\n$thirty$cell ";
			   $column=30+$l+1;
		  }
	 }
	 $outstr.= "\n";
	 return $outstr;
}

__END__

=head1 NAME

passwd_rpt.pl - multi server report sa/sso/oper roles utility

=head2 DESCRIPTION

Print Roles By Server/User In Nice Format.  Has a variety of ways to decode and print role info.  Servers must be defined in password file for this to work.
See documentation on Repository.pm library.

=head2 USAGE

	 ./passwd_rpt [-ousxh]
		  -o oper roles only
		  -s sa roles only
		  -x sso roles only
		  -h htmlize output
		  -u print by user not server

=head2 EXAMPLE

  > PRINT A LIST OF SA USERS BY USER
  $ passwd_rpt -us

  ***********************************************************
  *			     SA OUTPUT			 *
  ***********************************************************
  kent		 SYBASE_INT
  robp		 SYBASE_NT
  chikki	      SYBASE_DEV
  TestTrigger	 SYBASE_DEV
  SYS_OPER	    SYBASE_VALQA SYBASE_RRDEV SYBASE_QA SYBASE_DEV
			SYBASE_PROD SYBASE_INT SYBASE_NT SYBASE_Y2K
  paulr		SYBASE_VALQA SYBASE_RRDEV SYBASE_QA SYBASE_DEV
			SYBASE_PROD SYBASE_INT
  sa		   SYBASE_VALQA SYBASE_RRDEV SYBASE_QA SYBASE_DEV
			SYBASE_PROD SYBASE_INT SYBASE_NT SYBASE_Y2K
  psr		  SYBASE_INT
  SYS_INSTALL	 SYBASE_VALQA SYBASE_RRDEV SYBASE_QA SYBASE_DEV
			SYBASE_PROD SYBASE_INT SYBASE_NT SYBASE_Y2K
  Shimizu	     SYBASE_INT
  yury		 SYBASE_VALQA SYBASE_RRDEV SYBASE_QA SYBASE_DEV
			SYBASE_PROD SYBASE_INT SYBASE_NT

=cut
