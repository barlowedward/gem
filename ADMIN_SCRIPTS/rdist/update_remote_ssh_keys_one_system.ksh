# set up ssh via rsh
if [ -z "$1" ]
then
	echo "USAGE: $0 HOSTNAME SSH_KEY_FILE REMOTE_ACCOUNT"
	echo "       REMOTE_ACCOUNT defaults to 'sybase'"
fi

HOSTNAME=$1
if [ -z "$HOSTNAME" ]
then
	echo PLEASE ENTER HOST TO CONNECT TO
	read HOSTNAME
fi

FILENAME=$2
if [ -z "$FILENAME" ]
then
	echo PLEASE ENTER FILENAME OF SSH KEY INFO
	read FILENAME
fi

if [ -z "$FILENAME" ]
then
	echo "ERROR: filename is null"
	exit
fi

if [ ! -r $FILENAME ]
then
	echo "ERROR: filename $FILENAME is not readable"
	exit
fi

REMOTEACCOUNT=$3
[ -z "$REMOTEACCOUNT" ] && REMOTEACCOUNT=sybase

echo TESTING SSH CONNECTION TO $HOSTNAME
x=`ssh $REMOTEACCOUNT@$HOSTNAME echo OK CONNECTED 2>/dev/null | grep "OK CONNECTED"`
if [ "$x" = "OK CONNECTED" ]
then
	echo Remote Connection Ok
else
	echo Failed to Connect to $HOSTNAME
	x=`ssh $REMOTEACCOUNT@$HOSTNAME echo OK CONNECTED`
	echo $x
	exit
fi

scp remote_client.ksh $REMOTEACCOUNT@$HOSTNAME:/tmp/remote_client.ksh
scp $FILENAME $REMOTEACCOUNT@$HOSTNAME:/tmp/authkeys.dat
ssh $REMOTEACCOUNT@$HOSTNAME chmod 777 /tmp/remote_client.ksh
ssh $REMOTEACCOUNT@$HOSTNAME /tmp/remote_client.ksh

ssh $REMOTEACCOUNT@$HOSTNAME rm -f /tmp/remote_client.ksh
ssh $REMOTEACCOUNT@$HOSTNAME rm -f /tmp/authkeys.dat

exit
