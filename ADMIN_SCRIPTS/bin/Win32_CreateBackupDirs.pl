#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#GENERIC REPOSITORY RUN

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use Repository;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." 

Additional Arguments
	--DEBUG
   --BATCH_ID=id     [ if set - will log via alerts system ]
   ";
  return "\n";
}

$| =1;

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($rfile)=get_conf_dir()."/win32_backupdirs.dat.sample";
open( Cr,$rfile ) or die "Cant read $rfile $!\n";

my($wfile)=get_conf_dir()."/win32_backupdirs.dat";
open( Cx,">".$wfile ) or die "Cant read $wfile $!\n";

foreach( <Cr> ) { print Cx $_; }
close(Cr);

my($STARTTIME)=time;
repository_parse_and_run(
	PROGRAM_NAME	=> $PROGRAM_NAME,
	AGENT_TYPE		=> 'Gem Monitor',
	NOSTDOUT			=> 1 ,
	DOALL				=> "sqlsvr",
	BATCH_ID			=> $BATCH_ID ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

close(Cx);
exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";

}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;

	my(%hsh) = get_password_info(-type=>"sqlsvr",-name=>$cursvr);
	
	
	my($query)="SELECT database_name,physical_device_name, backup_size
FROM msdb.dbo.backupset b, msdb..backupmediafamily f
where b.type='D'
and datediff(dd,b.backup_finish_date,getdate()) < 7
and b.media_set_id=f.media_set_id";

	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";		
	if( ! $hsh{HOSTNAME} ) {
		my(@rc) = dbi_query( -db=>"master", -query=>"select convert(varchar(30),ServerProperty('MachineName')");
	   foreach (@rc) {
			my($k,$v)=dbi_decode_row($_);
			$hsh{HOSTNAME}=$v;
			# print $k," ",$v,"\n";
		}
	}
	$hsh{HOSTNAME} = $cursvr if ! $hsh{HOSTNAME};
	
	my(@rc) = dbi_query( -db=>"master", -query=>$query );
   
	my( %directories );
	foreach (@rc) {
		my($dnm,$pdn,$size)=dbi_decode_row($_);
		die "ERROR - $dnm $pdn " unless defined $size;
		$pdn =~ s.\\./.g;
		$pdn =~ s/:/\$/;
		my($bn)=dirname(lc($pdn));
		next if $bn eq ".";
		if( $bn =~ /\/\// ) {
		} else {
			if( -r "//".$hsh{HOSTNAME}."/".$bn ) {
				$bn = "//".$hsh{HOSTNAME}."/".$bn;
			} elsif( -r "//".$cursvr."/".$bn ) {
				$bn = "//".$cursvr."/".$bn;
			} else {
				next;
			}
		}
		$directories{$bn}=1;			
	}
	foreach (keys %directories) {	
		print "WARNING Directory $_ is not readable\n" unless -r $_;
		print Cx $cursvr,";",$hsh{SERVER_TYPE},";",$hsh{HOSTNAME},";",$_,"\n";	
	}	 
}

__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

