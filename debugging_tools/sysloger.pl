use Sys::Syslog;

openlog("DBA_Monitor:","nofatal","local0");

# Valid level names are): alert, crit, debug, emerg, err, error (deprecated
#     synonym for err), info, notice, panic (deprecated synonym for emerg),
#     warning, warn (deprecated synonym for warning).  For the priority order
#     and intended purposes of these levels, see syslog(3).

syslog("info","Test - This is an Info Message");
syslog("notice","Test - This is an Warning Message");
syslog("err","Test - This is an Error");
syslog("emerg","Test - This is an Emerg");
closelog();
