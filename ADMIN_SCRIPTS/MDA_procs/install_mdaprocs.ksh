#!/bin/sh
if [ -z "$1" -o -z "$2" ]
then
	echo "Usage $0 SERVER PASSWORD"
	exit
fi

SERVER=$1
PASSWORD=$2

for j in `ls sp__*`
do
	echo $j
	isql -Usa -S$SERVER -P$PASSWORD -i $j
done
