#######################################################################
# Standard Time Formatting
#######################################################################

# copyright (c) 2005-2007 by SQL Technologies.  All Rights Reserved.

package  Do_Time;

use      strict;
use      Carp;
use      vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
require  Exporter;
@ISA     = qw(Exporter);
@EXPORT  = qw(do_time do_diff get_month_id do_diff_file do_time_file get_month_name days_in_month);
$VERSION = '1.0';

# sub get_time
# {
	# my(%OPT)=@_;
	# croak("Must Pass -fmt argument to get_time") unless defined $OPT{-fmt};
	# return time unless defined $OPT{-time};
	# my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime($now);
	# return timelocal( $sec,$min,$hour,$mday,$mon,$year);
# }

my(%mon_hash);
sub load_mon_hash {
	my($c)=1;
	foreach ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec") {
		$mon_hash{$_}=$c++;
	}
}
sub days_in_month {
	my($mon)=@_;	
	my(@days)=(31,28,31,30,31,30,31,30,31,31,30,31);
	return $days[$mon-1];
}
sub get_month_id {
	my($mon,$twochar )=@_;
	load_mon_hash() unless defined $mon_hash{"Jan"};		
	return "0".$mon_hash{$mon} if $twochar and length $mon_hash{$mon} == 1;
	return $mon_hash{$mon};
}
sub get_month_name {
	my( $mon )=@_;		# should be an integer
	$mon=~s/^0//;
	return undef unless int($mon) eq $mon;
	load_mon_hash() unless defined $mon_hash{"Jan"};		
	
	foreach (keys %mon_hash) {		return $_ if $mon_hash{$_} == $mon; }
	return undef;
}

sub do_time
{
	my(%OPT)=@_;
	croak("Must Pass -fmt argument to do_time") unless defined $OPT{-fmt};

	my($now)=$OPT{-time};
	return "" if defined $OPT{-nullonnull} and ! defined $OPT{-time};
	$now=time unless defined $now;

	my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime($now);
	$wday= ('Sun','Mon','Tue','Wed','Thu','Fri','Sat')[$wday];
	
	$mon++;
	$year+=1900;
	if( defined $OPT{-spnozeroes} ) {
		# print "ADDING SPACE\n";
		substr($mon,0,0)=' ' 	if length($mon)==1;
		substr($mday,0,0)=' ' 	if length($mday)==1;
		substr($hour,0,0)=' ' 	if length($hour)==1;
		substr($min,0,0)=' ' 	if length($min)==1;
		substr($sec,0,0)=' ' 	if length($sec)==1;
	} else {
		substr($mon,0,0)='0' 	if length($mon)==1;
		substr($mday,0,0)='0' 	if length($mday)==1;
		substr($hour,0,0)='0' 	if length($hour)==1;
		substr($min,0,0)='0' 	if length($min)==1;
		substr($sec,0,0)='0' 	if length($sec)==1;
	}

	my($rc)=$OPT{-fmt};
	
	warn "Warning : you probably meant hh:mi not hh:mm in do_time\n" if $rc =~ /hh:mm/;
	$rc =~ s/yyyy/$year/g;
	substr($year,0,2)="";
	$rc =~ s/yy/$year/g;
	$rc =~ s/mm/$mon/g;
	$rc =~ s/dd/$mday/g;
	$rc =~ s/hh/$hour/g;
	$rc =~ s/mi/$min/g;
	$rc =~ s/dw/$wday/g;
	$rc =~ s/dy/$yday/g;
	$rc =~ s/ss/$sec/g;
	if( $rc =~ /mon/ ) {
		my($monnm)=get_month_name($mon);
		$rc =~ s/mon/$monnm/g;
	}
	return $rc;
}

sub do_time_file		# returns a sybase time for a file
{
		my($filename,$long_format)=@_;		
		return undef unless -e $filename;
		my($filetime)= -M $filename;
		$filetime *= 24*60*60;		
		$filetime = time - $filetime;
		return do_time( -time => $filetime, -fmt => "yyyy/mm/dd hh:mi");				
}

sub do_diff_file
{
		my($filename,$long_format)=@_;		
		return undef unless -e $filename;
		my($filetime)= -M $filename;
		$filetime *= 24*60*60;		
		return do_diff( $filetime, $long_format);				
}

# takes a seconds and returns a string
sub do_diff
{
	my($t,$long_format)=@_;
	my($str)="";
	return $t unless $t=~/^\d+$/;
	
	if( $t>24*60*60 ) {
		my($d)=int($t/(24*60*60));
		$str.=$d."+ Days";
		return $str;
	}
	
	if( $t>60*60 ) {
		my($h)=int($t/(60*60));
		$str.=$h;
		if( defined $long_format ) { $str.="Hours "; } else { $str.="Hr "; }
		$t -= $h*60*60;
	
		my($m)=int($t/60);
		$str.=$m;
		if( defined $long_format ) { $str.="Minutes"; } else { $str.="Min"; }
		return $str;	
	}
	
	if( $t>60 ) {
		my($m)=int($t/60);
		$str.=$m;
		if( defined $long_format ) { $str.="Minutes"; } else { $str.="Min"; }
		$t -= $m*60;
		$str .= int($t);		
		if( defined $long_format ) { $str.="Seconds"; } else { $str.="Sec"; }
		return $str;
	}
	
	#if( $str eq "" ) {
	$str=int($t);
	if( defined $long_format ) { $str.="Seconds"; } else { $str.="Sec"; }
	#}
	return $str;
}

1;

__END__

=head1 NAME

Do_Time.pm - Misc Time Functions

=head2 DESCRIPTION

Very simple time functions.

  do_time(-time=>secs, -fmt=>formatstring);

  do_diff() - pretty print dime differences
