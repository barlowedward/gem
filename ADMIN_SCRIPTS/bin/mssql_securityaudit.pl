#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use strict;
use File::Basename;
use MlpAlarm;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Repository;

use vars qw( $SYSTEMS $DEBUG );

my($VERSION)=1.0;

$|=1;

sub usage
{
	return "Usage: mssql_shrinklogs.pl --SYSTEMS=system[,system]	\n \tmssql_shrinlogs.pl --SYSTEMS=ALL";
}

die usage() if $#ARGV<0;
die "Bad Parameter List $!\n".usage() unless
   GetOptions( "SYSTEMS=s"	=> \$SYSTEMS, "DEBUG"=>\$DEBUG	);

cd_home();
my(@srv);
if( $SYSTEMS eq "ALL" ) {
	@srv=get_password(-type=>'sqlsvr');
} else {
	@srv=split(/,/,$SYSTEMS);
}
die "No Systems Defined" if $#srv<0;

my($login,$pass,$ctype,$srv,$cmd);
foreach $srv (@srv) {
	($login,$pass,$ctype)=get_password(-type=>'sqlsvr',-name=>$srv);
   	print "Connecting to $srv\n";
   	my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
	if( ! $rcx ) {
      		print "Cant connect to $srv as $login\n";
		next;
	}

	print "Getting Database List\n" if defined $DEBUG;
	my($DATABASE)="%";
	my($server_type,@okdb)=dbi_parse_opt_D($DATABASE,1);

	my(%login_db, %login_lang, %login_ok, %login_type, %login_admin, %login_remote);
	my(%login_roles);		# array

	my($query)='sp__helplogin @dont_format=\'Y\'';
	print "$query\n" if $DEBUG;
	my(@rc)=dbi_query(-db=>"master",-query=>$query, -debug=>$DEBUG);
	foreach ( @rc ) {
		my(@x)=dbi_decode_row($_);
		$x[0]=~s/\s//g;
		$login_db{$x[0]}		=$x[1];
		$login_lang{$x[0]}	=$x[2];
		$login_ok{$x[0]}		=$x[3];
		$login_type{$x[0]}	=$x[4];
		$login_admin{$x[0]}	=$x[5];
		$login_remote{$x[0]}	=$x[6];
	}
	$query='sp_helpsrvrolemember';
	print "$query\n" if $DEBUG;
	my(@rc)=dbi_query(-db=>"master",-query=>$query, -debug=>$DEBUG);
	foreach ( @rc ) {
		my(@x)=dbi_decode_row($_);
		$x[0]=~s/\s//g;
		$x[1]=~s/\s//g;
	#	print "$x[1] - $x[0]\n";
		warn "NO LOGIN $x[1]\n" unless $login_db{$x[1]};
		if( $login_roles{$x[1]} ) {
			push @{$login_roles{$x[1]}}, $x[0];
		} else {
			my(@a);
			push @a, $x[0];
			$login_roles{$x[1]}=\@a;
		}
	}

	#use Data::Dumper;
	#print Dumper \%login_roles;

	my(%user_group);
	foreach my $db (@okdb) {

			next if $db eq "master"	or $db eq "model"	or $db eq "tempdb" or $db eq "msdb"	or $db eq "Northwind" or $db eq "pubs";

			print "Using Database $db\n" if $DEBUG;
			my($a) = dbi_use_database($db);

			my($query)= "sp_helpuser ";
			print "$query\n" if $DEBUG;
	   	my(@rc)=dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG);
	   	my(%login_by_usr, %login_by_usrid);
	   	foreach ( @rc ) {
	   		my($user,$group,$login,$d1,$d2,$userid,$sid) = dbi_decode_row($_);
	   		$login	=~s/\s//g;
	   		$group	=~s/\s//g;
	   		$user		=~s/\s//g;
	   		$userid	=~s/\s//g;
	   		$login=$user unless $login or $login eq "NULL";
	   		$login_by_usr{$user}=$login;
	   		$login_by_usrid{$userid}=$login;
	   		$user_group{$db."~".$login}="= ";
	   	}

	   	if( $DEBUG ) {	foreach (keys %login_by_usr) { print "LoginByUser $_ => $login_by_usr{$_}\n"; } }

	   	my($query)='sp_helpgroup';
	   	my(@groups);
	   	print "$query\n" if $DEBUG;
	   	my(@rc)=dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG);
	   	foreach ( @rc ) {
	   		my(@x)=dbi_decode_row($_);
	   		$x[0]=~s/\s//g;
	   		push @groups,$x[0];
	   	}
	   	foreach (@groups) {
	   		my($query)='sp_helpgroup \''.$_.'\'';
	   		print "$query\n" if $DEBUG;
	   		my(@rc)=dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG);
	   		foreach ( @rc ) {
		   		my($grp,$grpid,$user,$uid)=dbi_decode_row($_);
		   		$grp	=~	s/\s//g;
		   		$user	=~	s/\s//g;
		   		if( $login_by_usr{$user} ) {
		   			$user	=	$login_by_usr{$user};
		   		} else {
		   			warn "no login found for user $user in db $db\n";
		   		}
		   		$user_group{$db."~".$user}.=$grp." ";
		   	}
		   }
			if( $DEBUG ) {
				foreach ( sort keys %user_group ) {
					print ">>> $_ $user_group{$_}\n";
				}
			}
		}

	foreach my $login ( sort keys %login_db ) {
		print "====> $login_type{$login} Login = $login \n";
		print " *** THIS ACCOUNT IS AN ADMINISTRATOR ***\n" if $login_admin{$login} eq "YES";
		print " *** ROLES: ",join(" ",@{$login_roles{$login}}),"\n" if $login_roles{$login};
		foreach my $db (sort @okdb) {
				printf "   %14s %s \n",	 $db, $user_group{$db."~".$login} if $user_group{$db."~".$login};
		}
	}

   dbi_disconnect();
	print "Completed Work on Server $srv\n" if defined $DEBUG;
}
exit(0);
__END__

=head1 NAME

mssql_shrinklogs.pl - shrink data/log files on sql server

=head2 DESCRIPTION

Shrinks your data/log files on your sql servers.  This is a much faster shrink
than you would get if you were using a shrink database.  The program also allows
you to shrink your data files if you pass --DATATOO.

You may specify all systems from the pc config file or specifiy the systems
to use.  Be careful tho.  This will confuse the tran log dump/load programs
if you pass the --DUMPTRANNOLOG option to truncate the log.

=head2 USAGE

Usage: mssql_shrinklogs.pl --DATATOO --STDALARM --DEBUG --SYSTEMS=system[,system] -
 -DOSHRINK --DUMPTRANNOLOG --MAXSIZE=MB --DATABASE=DBLIST

=head2 OPTIONS

 --DUMPTRANNOLOG : dump tran with nolog prior to shrinking (dangerous?)
 --DOSHRINK      : do the shrink
 --SYSTEMS       : comma separated list of systems.  Otherwise does em all
 --MAXSIZE		  : maximum file size for files to shrink (does not to larger ones)

=cut

