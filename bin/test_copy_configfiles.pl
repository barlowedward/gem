use strict;
use Getopt::Long;
use vars qw( $DEBUG $CURDIR );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions( "DEBUG"	=> \$DEBUG, "CURDIR" => \$CURDIR );

my(@errors);

#
# end standard header
#

use File::Basename;
use CommonFunc;
my($curdir)=basename(dirname($0));

use Repository;

my($dir)=get_gem_root_dir();
if( ! $dir or ! -d $dir ) {
	print "ERROR: Cant find configuration directory\n";
	exit(1);
}
print "Root Directory is $dir\n";

opendir(DIR,"$dir/conf")
	or die("test_copy_configfiles.pll() Cant open directory $dir/conf for reading : $!\n");
my(@dirlist)=grep(!/^\./,readdir(DIR));
closedir(DIR);
my(%conf_file);
my($num_copies)=0;
foreach( @dirlist ) { $conf_file{$_}=1;	}
my($samplecount)=0;
print "\n";
my(@copyerrors);
foreach( @dirlist ) {
	next unless /.sample$/;
	$samplecount++;
	next if /crontab.sample/;
	my($f)=$_;
	$f =~ s/.sample$//;
	next if $conf_file{$f};
	print "\tcreating conf/$f from sample file\n";
	use File::Copy;
	# print "copy($dir/conf/$f.sample,$dir/conf/$f\n";
	copy("$dir/conf/$f.sample","$dir/conf/$f") or die("cant copy $dir/conf/$f.sample to $dir/conf/$f : $!");
	chmod 0660, "$dir/conf/$f";
	push @copyerrors,"File $dir/conf/$f does not exist after copy?" unless -r  "$dir/conf/$f";
	push @copyerrors,"File $dir/conf/$f not writable" if -r  "$dir/conf/$f" and ! -w  "$dir/conf/$f";
	# print "\tFile $dir/conf/$f exists and is writable\n" if -w  "$dir/conf/$f";
}

foreach (@copyerrors) { print "ERROR: $_\n"; }
print "ERROR - $dir/conf only contains $samplecount sample files???\n" unless $samplecount>4;
print "SUCCESSFUL RUN\n" if $#copyerrors == -1 and  $samplecount>4;;