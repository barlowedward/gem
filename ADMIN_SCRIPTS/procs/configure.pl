: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

# must be on line 5+
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#
# GENERIC STORED PROCEDURE INSTALLER
#

# Copyright (c) 1995-2011 by Edward Barlow. All rights reserved.

use strict;
use Getopt::Long;
use File::Basename;
my( $curdir,$startdir);

my($print_diags_as_warns);			# set it if you are developing and something is very wrong - bunch of warn()

my($do_not_use_dbi)=1;				# for testing
BEGIN {
	use Cwd;
	$startdir=dirname($0);
	warn __LINE__," startdir Directory=$startdir\n" if $print_diags_as_warns;
	#chdir $startdir or die "ERROR: Cant cd to $startdir: $!\n";
	#$startdir = cwd();
	#chdir ".." or die "ERROR: Cant cd to .. from $curdir: $!\n";
	#chdir ".." or die "ERROR: Cant cd to .. from $curdir: $!\n";
	$curdir = cwd();
	warn __LINE__," Current Directory=$curdir\n" if $print_diags_as_warns;
}

use lib "$curdir/lib";
use lib "$startdir/lib";
use lib "../../lib";
use lib "../lib";
use Sys::Hostname;
use DBIFunc;

my($USER,$PASSWORD,$SERVER,$FILE,$TYPE,$DEBUG,$GENERATE,$SQLSERVERS,$SYBSERVERS,$HTML,
	$UPGRADE_TO_LATEST,$ALLSERVERS,$MINVERSION,$TEST_MODE,$UNINSTALL,$REINSTALL);

sub usage
{
	print "usage: configure.pl --DEBUG --USER=sa --PASSWORD=pass --SERVER=svr --MINVERSION=ver --UNINSTALL --UPGRADE_TO_LATEST --ALLSERVERS|--SQLSERVERS|--SYBSERVERS [-FILE=File]
		\n";
   return "\n";
}

$| =1;		# dont buffer standard output

my @fullproclist = qw(sp__auditdb sp__auditsecurity sp__badindex sp__bcp
sp__block sp__checkkey sp__colconflict sp__collist sp__colnull sp__configure
sp__create_insert sp__date sp__find_missing_index sp__flowchart
sp__datediff sp__dbspace sp__depends sp__diskdevice sp__dumpdevice
sp__get_tmp_error sp__grep sp__groupprotect sp__help sp__helpcolumn
sp__helpdb sp__helpdbdev sp__helpdefault sp__helpobject sp__helpproc
sp__helpdevice sp__helpgroup sp__helpindex sp__helplogin sp__helpmirror
sp__helprule sp__helpsegment sp__helptable sp__helptext sp__helptrigger
sp__helptype sp__helpuser sp__helpuser2 sp__ls sp__marksuspect sp__noindex
sp__helpview sp__id sp__indexspace sp__iostat sp__isactive sp__lock sp__lockt
sp__objprotect sp__proclib_version sp__qspace sp__read_write sp__revalias
sp__revbindings sp__revconfigure sp__revmirror sp__revrole sp__revrule
sp__revdb sp__revdevice sp__revgroup sp__revindex sp__revkey  sp__revlogin
sp__revsegment sp__revtable sp__revtype sp__revuser sp__segment sp__server
sp__show_tempdb sp__size sp__stat sp__who sp__whoactive sp__whodo sp__whoe
sp__stat2 sp__syntax sp__trigger sp__updatestats sp__uptime sp__vdevno
sp__helprotect sp__hexadecimal sp__metadata sp__serverspace sp__helpbackup
sp__DeleteBackupHistory sp__serverproperty sp__fragmentation
);
#
die usage() unless GetOptions(
		"USER=s" 				=> \$USER,
		"PASSWORD=s" 			=> \$PASSWORD,
		"PASS=s" 				=> \$PASSWORD,
		"SYSTEM=s" 				=> \$SERVER,
		"SERVER=s" 				=> \$SERVER,
		"FILE=s" 				=> \$FILE,
		"MINVERSION=s"			=> \$MINVERSION,
		"TYPE=s" 				=> \$TYPE ,
		"REMOVE"    			=> \$UNINSTALL ,
		"UNINSTALL" 			=> \$UNINSTALL ,
		"REINSTALL" 			=> \$REINSTALL ,
		"HTML" 					=> \$HTML ,
		"GENERATE"				=> \$GENERATE ,
		"ALLSERVERS" 			=> \$ALLSERVERS ,
		"SQLSERVERS" 			=> \$SQLSERVERS ,
		"SYBSERVERS" 			=> \$SYBSERVERS ,
		"UPGRADE_TO_LATEST" 	=> \$UPGRADE_TO_LATEST ,
		"TEST_MODE"     		=> \$TEST_MODE,
		"DEBUG"     			=> \$DEBUG );

$UNINSTALL=1 if $REINSTALL;

# print "Changing Directory To $startdir\n";
# chdir $startdir or die "ERROR: Cant cd to $startdir: $!\n";

my($NL)="\n";
$NL="<br>\n" if defined $HTML;
$DEBUG=1 if $TEST_MODE;

my($a,$VERSION)=get_version();

open( LREC,">> installhist.log");
if( $UNINSTALL ) {
	print LREC "ProcInstall\t",	$VERSION,	"\t",	scalar(localtime(time)),"\t",	hostname(),"\t",$SERVER,"\n";
} else {
	print LREC "UnInstall\t",	$VERSION,	"\t",	scalar(localtime(time)),"\t",	hostname(),"\t",$SERVER,"\n";
}
close(LREC);

print $NL,"Extended Stored Procedure Library Installer $VERSION $NL";
print "Copyright (c) 1995-2011 by Sql Technologies $NL";
print "Version $VERSION on Host ",hostname(),$NL;

chomp $MINVERSION;
chomp $VERSION;

if( -e "delete_me" ) {
	unlink "delete_me";
	die "Cant Remove File delete_me" if -e "delete_me";
}

if( defined $UPGRADE_TO_LATEST ) {
	die "Cant pass --UPGRADE_TO_LATEST and --MINVERSION\n" if defined $MINVERSION;
	$MINVERSION=$VERSION;
	$MINVERSION=~s/[a-z]//g;
	print __LINE__,"> MINVERSION=$MINVERSION\n" if $DEBUG;
}

$MINVERSION=~s/\s//g;
print __LINE__,"> MINVERSION=$MINVERSION\n" if $DEBUG and $MINVERSION;
if( defined $MINVERSION and $MINVERSION !~ /^\d+\.\d+$/ ) {
	print "ERROR - MINVERSION ($MINVERSION) is not a floating point number\n";
	exit(0);
}

installmsg() if ! defined $USER or ! defined $SERVER or !defined $PASSWORD;

my(@data);
print __LINE__,"> Reading DATA\n" if $DEBUG;
foreach (<DATA>) {
	chomp;
	s/\s*$//g;
	next if /^\s*$/ or /^\s*$/;
	push @data,$_;
}

if( $UNINSTALL ) {
	print "****************************************\n";
	print "* Removing Stored Procedure Library    *\n";
	print "* This includes all objects named sp__ *\n";
	print "****************************************\n";
}

if( $GENERATE ) {
	print "GENERATING FILES\n";
	foreach my $servertype ("SYBASE_MDA","SQLSVR2000","SQLSVR2005","SYBASE12","SYBASE15") {
		print "\nGenerating File install_".$servertype.".bat\n";
		open(FILE,">install_".$servertype.".bat") or die "Cant write";
		my(@to_install);
		if( $servertype eq "SYBASE_MDA" ) {
			@to_install=get_list_of_files($servertype,0,0);
		} elsif( $servertype eq "SQLSVR2000" ) {
			@to_install=get_list_of_files("SQLSVR",0,8);
		} elsif( $servertype eq "SQLSVR2005" ) {
			@to_install=get_list_of_files("SQLSVR",0,9);
		} elsif( $servertype eq "SYBASE12" ) {
			@to_install=get_list_of_files($servertype,12,0);
		} elsif( $servertype eq "SYBASE15" ) {
			@to_install=get_list_of_files($servertype,15,0);
		}
		foreach my $nm (@to_install) {
			if( ! -r $nm ) {
				my($nm2)="$curdir/$nm" 		if -r "$curdir/$nm";
				$nm2    ="$startdir/$nm" 	if ! $nm2 and -r "$startdir/$nm";
				$nm2    ="./$nm" 				if ! $nm2 and -r "./$nm";
				die "Unable to find $nm in\n$curdir\n$startdir\n." unless $nm2;
				$nm=$nm2;
			}

			if( $servertype eq "SQLSVR" ) {
				print FILE  "osql -E -S\"\$1\" -n -i$nm\n";
			} else {
				print FILE "isql -Usa -S\"\$1\" -P\$2 -i$nm\n";
			}
		}
		close(FILE);
	}
	exit();
}

#
# A BUNCH OF SERVERS FROM THE CONFIG FILES
#
if(  $ALLSERVERS or $SQLSERVERS or $SYBSERVERS ) {
	use Repository;
	print "Installing Multiple Servers Per Comand Line Args\n\n";
	my(@srv)=get_password(-type=>'sybase') unless $SQLSERVERS;
	foreach $SERVER (@srv) {
		if( ! defined $ENV{"SYBASE"} ) {
			print "WARNING: SKIPPING SERVER $SERVER AS SYBASE ENVIRONMENT VARIABLE IS NOT SET\n";
			next;
		}
		($USER,$PASSWORD)=get_password(-type=>'sybase',-name=>$SERVER);
		#print "Installing Sybase $SERVER\n";
		install_a_server($SERVER,$USER,$PASSWORD,'sybase');
	}
	if( is_nt() ) {
		print "Installing Sql Servers\n";
		my(@srv)=get_password(-type=>'sqlsvr') unless $SYBSERVERS;
		foreach $SERVER (@srv) {
			($USER,$PASSWORD)=get_password(-type=>'sqlsvr',-name=>$SERVER);
			#print "Installing SQL Server $SERVER\n";
			install_a_server($SERVER,$USER,$PASSWORD,'sqlsvr');
		}
	}
	exit(0);
}

#
# PROCESS A SINGLE SERVER
#
if( ! defined $SERVER ) {
	print "Please Enter A Server Name: ";
	$SERVER=<STDIN>;
	chomp $SERVER;
	die usage("Must pass server\n")	     unless defined $SERVER and $SERVER !~ /^\s*$/;
}

#
# is the server sybase
#
#my(@sybsrv)=get_password(-type=>'sybase');
#my($issyb)=0;
#foreach my $S (@sybsrv) {
#	next unless $SERVER eq $S;
#	$issyb=1;
#	die "SYBASE ENV VBL MUST BE DEFINED" if ! defined $ENV{"SYBASE"};
#	$TYPE="Sybase";
#	last;
#}


if( is_nt() and ( ! $USER or ! $PASSWORD ) ) {
	my( $native_ok ) = test_osql_native_auth($SERVER) if is_nt();
	if( $native_ok ) { 									# can skip login/pass request
		print "Native Authentication Available\n";
		$PASSWORD='native' unless $PASSWORD;
		$USER='native' 	 unless $USER;
	} else {
		print "Do you wish to use native authentication (y or n)?";
		my($use_native);
		$use_native=<STDIN>;
		chomp $use_native;
		$use_native=lc($use_native);
		if( $use_native =~ /^y/i ) {
			$PASSWORD='native' unless $PASSWORD;
			$USER='native' 	 unless $USER;
		} elsif( $use_native =~ /^\s*$/i ) {
			print "Must pass y or n\nexiting()\n";
			exit(0);
		}
	}
}

if( ! defined $USER ) {
	print "Please Enter A User Name (must have sa_role): ";
	$USER=<STDIN>;
	chomp $USER;
	die usage("Must pass user\n")	       unless defined $USER and $USER !~ /^\s*$/;
}

if( ! defined $PASSWORD ) {
	print "Please Enter A Password  : ";
	$PASSWORD=<STDIN>;
	chomp $PASSWORD;
	die usage("Must pass password\n")	   unless defined $PASSWORD and $PASSWORD !~ /^\s*$/;
}

if( is_nt() and ( ! defined $ENV{"SYBASE"} or $USER eq "native" )) {
	install_a_server($SERVER,$USER,$PASSWORD,"sqlsvr");
} else {
	install_a_server($SERVER,$USER,$PASSWORD,"?");
}
exit(0);

my($best_method);		# N.A., DBI, OSQL, ISQL
my($osql_cmd_to_use,$osql_cmd_to_print);
my($isql_cmd_to_use,$isql_cmd_to_print);
my($connection);
sub run_db_query {
	my(%args)=@_;

	if( $DEBUG ) {
		print __LINE__,"> run_db_query(";
		foreach (keys %args) { print " $_ => $args{$_}"; }
		print ");\n";
	}

	# -db, -query

	if( $best_method eq "DBI" ) {
		return dbi_query(-db=>$args{-db}, -query=>$args{-query},	-connection=>$connection );
	} elsif( $best_method eq "OSQL" ) {
		my($end)= " -d \"$args{-db}\"" if $args{-db};
		$end   .= " -Q \"$args{-query}\"";
		print __LINE__,"> ".$osql_cmd_to_print.$end."\n" if $DEBUG;
		open(CMD,$osql_cmd_to_use.$end." |") or die "Cant run osql\n";
		my(@results);
		my($rowcnt_rtn)=0;
		while(<CMD>) {
			# print "DBGDBG". $_. "<\n";
			$rowcnt_rtn++;
			next if $rowcnt_rtn<2;
			next if /^\s*$/ or /^\s*-----/ or /row affected/;
			next if /sysdepends for the current/
					or /depends on the missing object/
					or /Caution: Changing any part/
					or /The object was renamed/
					or /created./
					or /Msg 156, Level 15, State 1,/			# needed for tran errors
					or /syntax near the keyword /				# needed for tran errors
					or /procedures./
					or /rows affected\)/
					or /THE NEXT PROCEDURE MIGHT/
				#	or /2007/
					or /^Msg 156, Level 15, State 1, Server /;	# cant truncate master on 2008
			s/\s+~//g;
			s/\s+$//g;
			chomp;
			print __LINE__,"> $rowcnt_rtn>". $_. "<\n" if $DEBUG;
			push @results,dbi_encode_row(split(/~/,$_));
		}
		close(CMD);
		return @results;
	} elsif( $best_method eq "ISQL" ) {
		my($end)= " -D \'$args{-db}\'" if $args{-db};
		$end   .= " -i delete_me";
		print __LINE__,"> query=$args{-query}\n" if $DEBUG;
		open(F,">delete_me") or die "Cant write file delete_me\n";
		print F $args{-query}."\ngo\nexit\n";
		close(F);
		print __LINE__,"> ",$isql_cmd_to_print.$end."\n" if $DEBUG;
		open(CMD,$isql_cmd_to_use.$end." |")	or die "Cant Run Query\n";
		my(@results);
		my($rowcnt_rtn)=0;
		foreach (<CMD>) {
			$rowcnt_rtn++;
			next if $rowcnt_rtn<2;
			next if /^[~\s]*$/ or /^[~\s]*----/ or /rows* affected/;
			next if  /sysdepends/
					or /Object name has been changed./
					or /return status = 0/
					or /THE NEXT PROCEDURE MIGHT/
				#	or /2007/
					or /^\d+>\s*$/;
			s/^~//;
			s/\s+~//g;
			s/\s+$//g;
			chomp;
			print __LINE__,">",$rowcnt_rtn.">". $_. "<\n" if $DEBUG;
			push @results,dbi_encode_row(split(/~/,$_));
		}
		close(CMD);
		unlink("delete_me");
		return @results;
	} else {
		die "BAD METHOD $best_method\n";
	}
	return undef;
}

sub run_db_file {
	my(%args)=@_;	# -file

	if( $DEBUG ) {
		print __LINE__,"> run_db_file(";
		foreach (keys %args) { print " $_ => $args{$_}"; }
		print ");\n";
	}

	if( $best_method eq "DBI" ) {
		die "Cant do this\n";
		# return dbi_query(-db=>$args{-db}, -query=>$args{-query},	-connection=>$connection );
	} elsif( $best_method eq "OSQL" ) {
		my($end)= " -d \"$args{-db}\"" if $args{-db};
		$end   .= " -i \"$args{-file}\"";
		print __LINE__,"> ",$osql_cmd_to_print.$end."\n" if $DEBUG;
		open(CMD,$osql_cmd_to_use.$end." |") or die "Cant run osql\n";
		my(@results);
		my($rowcnt_rtn)=0;
		while(<CMD>) {
			# print "DBGDBG". $_. "<\n";
			$rowcnt_rtn++;
			next if $rowcnt_rtn<2;
			next if /^\s*$/ or /^\s*----------/ or /row affected/;
			next if /sysdepends for the current/
					or /depends on the missing object/
					or /Caution: Changing any part/
					or /The object was renamed/
					or /created./
					or /Msg 156, Level 15, State 1,/			# needed for tran errors
					or /syntax near the keyword /				# needed for tran errors
					or /procedures./
					or /rows affected\)/
					or /THE NEXT PROCEDURE MIGHT/
				#	or /2007/
					or /^Msg 156, Level 15, State 1, Server /;	# cant truncate master on 2008
			s/\s+~//g;
			s/\s+$//g;
			chomp;
			print __LINE__.">". $_. "<\n" if $DEBUG;
			push @results,dbi_encode_row(split(/~/,$_));
		}
		close(CMD);
		return @results;
	} elsif( $best_method eq "ISQL" ) {
		my($end)= " -D \"$args{-db}\"" if $args{-db};
		$end   .= " -i \"$args{-file}\"";
		print __LINE__,">  query=$args{-query}\n" if $DEBUG;
		open(F,">delete_me") or die "Cant write file delete_me\n";
		print F $args{-query}."\ngo\nexit\n";
		close(F);
		print __LINE__,"> ",$isql_cmd_to_print.$end."\n" if $DEBUG;
		open(CMD,$isql_cmd_to_use.$end." |")	or die "Cant Run Query\n";
		my(@results);
		my($rowcnt_rtn)=0;
		foreach (<CMD>) {
			$rowcnt_rtn++;
			next if $rowcnt_rtn<2;
			next if /^[~\s]*$/ or /^[~\s]*----------/ or /rows* affected/;
			next if  /sysdepends/
					or /Object name has been changed./
					or /return status = 0/
					or /THE NEXT PROCEDURE MIGHT/
				#	or /2007/
					or /^\d+>\s*$/;
			s/^~//;
			s/\s+~//g;
			s/\s+$//g;
			chomp;
			print __LINE__,">". $_. "<\n" if $DEBUG;
			push @results,dbi_encode_row(split(/~/,$_));
		}
		close(CMD);
		unlink("delete_me");
		return @results;
	} else {
		die "BAD METHOD $best_method\n";
	}
	return undef;
}

sub my_db_version
{
	my($sql2)="select \@\@version";
	my($DBTYPE,$DBVERSION,$PATCHLEVEL);
	foreach( run_db_query(-db=>"master",-query=>$sql2)) {

		#Adaptive Server Enterprise/11.9.2.6/1290/P/EBF 10491 ESD1/Sun_svr4/OS 5.5.1/FBO/Mon Jul 15 06:54:22 2002
		#Microsoft SQL Server  2000 - 8.00.2039 (Intel X86)   May  3 2005 23:18:38   Copyright (c) 1988-2003 Microsoft Corporation  Enterprise Edition on Windows NT 5.2 (Build 3790: Service Pack 1)
		#Microsoft SQL Server  2000 - 8.00.760 (Intel X86)    Dec 17 2002 14:22:05   Copyright (c) 1988-2003 Microsoft Corporation  Enterprise Edition on Windows  NT 5.2 (Build 3790: Service Pack 1)
		#Microsoft SQL Server 2005 - 9.00.1399.06 (Intel X86)   Oct 14 2005 00:33:37   Copyright (c) 1988-2005 Microsoft Corporation  Enterprise Edition on Windows NT 5.2 (Build 3790: Service Pack 1)
		#Microsoft SQL Server 2008 (SP1) - 10.0.2531.0 (X64)   Mar 29 2009 10:11:52   Copyright (c) 1988-2008 Microsoft Corporation   Standard Edition (64-bit) on Windows NT 6.0 <X64> (Build 6002: Service Pack 2)
		my($str)=join("",dbi_decode_row($_));
		# print __LINE__," DBGDBG - $str\n";
		$str=~s/^\s+//;
		if( $str =~ /^Microsoft SQL Server/ ) {
			$DBTYPE="SQL SERVER";
#print __LINE__," STR=$str\n";
			my(@ver) = split(/\n\s*/,$str);
			$ver[0] =~ s/\s\s+/ /g;

			my(@v1parts)=split(/\s+-\s+/,$ver[0]);
			$v1parts[0] =~ s/\s*\(.+$//;
			$v1parts[1] =~ s/\s*\(.+$//;
			$v1parts[1] =~ s/\s+$//;
			$v1parts[1] =~ s/^\s*//;
			my(@v1partsary) = split(/\./,$v1parts[1]);
			#print "DBDDBG: parsed $v1parts[1] into ",join("___",@v1partsary),"\n";
			$DBVERSION = $v1parts[0];
			$PATCHLEVEL= $v1partsary[0].".".$v1partsary[2];
			$PATCHLEVEL =~ s/\s//g;
			#$ver[0] =~ /^(.+)\s+-\s([\d+\.]+)\s\(/;
			#$ver[0] =~ s/^\s+//;
			#$ver[0] =~ s/\s+$//;
			#$ver[0] =~ s/\s*\(.+$//;
			#$ver[0] =~ s/\.00\./\./;

			#my(@y) = split(/\s*-\s*/,$ver[0]);
			#my(@z) = split(/\./,$y[5]);
			#$DBVERSION	=$y[0];
			#$PATCHLEVEL	=$y[1];
			#$PATCHLEVEL =~ s/\.00$//;			# wierd version numbers corrected

			#$DBVERSION=$z[0].".".$z[1].$z[2].$z[3];
			#$PATCHLEVEL=$ver[2];

		} elsif( $str =~ /Adaptive Server Enterprise/ ) {
			$DBTYPE="SYBASE";
			my(@x) = split("\/",$str);
			my(@y) = split(/\./,$x[1]);
			$DBVERSION=$y[0].".".$y[1].$y[2].$y[3];
			$PATCHLEVEL=$x[2];
		} else {
			$DBVERSION=$str;
		}
	}
	return($DBTYPE,$DBVERSION,$PATCHLEVEL);
}

sub is_odbc_dsn {
	my($SERVER)=@_;
	return "FALSE" unless is_nt();
	foreach ( DBI->data_sources("ODBC")) {
		return "TRUE" if $_ =~ /:$SERVER$/;
	}
	return "FALSE";
}

my(@interfaces);
sub install_a_server {
	my($SERVER,$USER,$PASSWORD,$KNOWN_DBTYPE)=@_;
	die "NO SERVER DEFINED\n" unless defined $SERVER;
	printf "\nWorking on Server %-20.20s \n   %-10.10s \n   DbType=%-5s %s",$SERVER,$USER,$KNOWN_DBTYPE,$NL if $DEBUG;
	printf "\nWorking on Server %-20.20s \n",$SERVER unless $DEBUG;

	$best_method='N.A.';
	($osql_cmd_to_use,$osql_cmd_to_print,$isql_cmd_to_use,$isql_cmd_to_print) = (undef,undef,undef,undef);

	if( $KNOWN_DBTYPE eq "?" ) {
		my(@interfaces)=dbi_get_interfaces() if $ENV{SYBASE} and $#interfaces<0;
		foreach ( @interfaces ) {
			if( $SERVER eq $_->[0] ) {
				print "   This is a Sybase Server (based on Interfaces File)\n";
				$KNOWN_DBTYPE='sybase';
				last;
	      }
		}
	}

	my($does_odbc_dsn_exist)='FALSE';
	my($servertype,$DBVERSION,$LEVEL,$DATABASE,$IS_SYS_15,$ADD_MDA);
	if( is_nt() ) {
		# print "Checking ODBC Sources: ",join(" ",DBI->data_sources("ODBC")),"\n" if $DEBUG;
		$does_odbc_dsn_exist = is_odbc_dsn($SERVER);
		#print "FIXME - $does_odbc_dsn_exist \n";

		warn "Unable to find ODBC data source for $SERVER on ".hostname()."\n" if $does_odbc_dsn_exist eq "FALSE";
		if( $does_odbc_dsn_exist eq "FALSE" and $KNOWN_DBTYPE ne "sybase" ) {
			my( $native_ok ) = test_osql_native_auth($SERVER);
			if( $native_ok ) {
				$best_method = 'OSQL';
				$osql_cmd_to_use  = "osql -n -S \"$SERVER\"  -E -s~ -w10000";
				$osql_cmd_to_print= "osql -n -S \"$SERVER\"  -E -s~ -w10000";
			}
		} else {
			#if( $does_odbc_dsn_exist eq "TRUE" ) {
			#	print "   TESTING DBI Connectivity\n" if $DEBUG;
			#	$connection=dbi_connect(
			#		-srv			=>	$SERVER,
			#		-login		=>	$USER,
			#		-password	=>	$PASSWORD,
			#		-connection	=>	1 );
			#	$connection = undef if $do_not_use_dbi;
			#}

			#if ( ! $connection ) {
				if( $KNOWN_DBTYPE eq "sybase") {
					my( $native_ok ) = test_isql_auth($SERVER,$USER,$PASSWORD);
					if( $native_ok ) {
						$best_method = 'ISQL';
						$isql_cmd_to_use  = "isql -S$SERVER -U$USER -P$PASSWORD -w10000 -s~";
						$isql_cmd_to_print= "isql -S$SERVER -U$USER -PXXX -w10000 -s~";
					}
				} else {
					my( $native_ok ) = test_osql_native_auth($SERVER);
					if( $native_ok ) {
						$best_method = 'OSQL';
						# use -D for ODBC DSN not -S for Server
						$osql_cmd_to_use  = "osql -n -D \"$SERVER\"  -E -s~ -w10000";
						$osql_cmd_to_print= "osql -n -D \"$SERVER\"  -E -s~ -w10000";
					}
				}
				if( $KNOWN_DBTYPE eq "?" and $best_method eq "N.A." ) {
					my( $native_ok ) = test_isql_auth($SERVER,$USER,$PASSWORD);
					if( $native_ok ) {
						$best_method = 'ISQL';
						$isql_cmd_to_use  = "isql -S$SERVER -U$USER -P$PASSWORD  -w10000 -s~";
						$isql_cmd_to_print= "isql -S$SERVER -U$USER -PXXX  -w10000 -s~";
					}
				}
			#} else {
			#	$best_method='DBI';
			#}
		}
	} else {		# must be sybase
		# print "   TESTING DBI Connectivity\n" if $DEBUG;
		#$connection=dbi_connect(
		#		-srv			=>	$SERVER,
		#		-login		=>	$USER,
		#		-password	=>	$PASSWORD,
		#		-connection	=>	1 );
		#$connection = undef if $do_not_use_dbi;
		#if ( ! $connection ) {
			my( $native_ok ) = test_isql_auth($SERVER,$USER,$PASSWORD);
			if( $native_ok ) {
				$best_method = 'ISQL';
				$isql_cmd_to_use  = "isql -S$SERVER -U$USER -P$PASSWORD -w10000 -s~";
				$isql_cmd_to_print= "isql -S$SERVER -U$USER -PXXX -w10000 -s~";
			}
		#} else {
		#	$best_method='DBI';
		#}
	}

	if( $best_method eq "N.A." ) {
		print "Unable to connect to server $SERVER - skipping\n";
		print "SYBASE=$ENV{SYBASE}\n" if $DEBUG and ! is_nt();
		print "PATH=$ENV{PATH}\n" 		if $DEBUG and ! is_nt();
		return;
	}

	printf __LINE__,"> Loading %-20.20s using %5s %s",$SERVER,$best_method,$NL if defined $DEBUG;

	($servertype,$DBVERSION,$LEVEL)=my_db_version( -connection=>$connection );
	print "   $servertype Version $DBVERSION  ($LEVEL)\n";

	$servertype="SQLSVR" if $servertype eq "SQL SERVER";

	$DATABASE="sybsystemprocs" if $servertype eq "SYBASE";
	$DATABASE="msdb" if $servertype eq "SQLSVR";
	$ADD_MDA="FALSE";
	$IS_SYS_15="FALSE";

	print __LINE__,"> [$SERVER] server type=$servertype version=$DBVERSION level=$LEVEL\n" if $DEBUG;

	if( $UNINSTALL ) {
		uninstall_server($connection,$servertype);
		print "UNINSTALL OF $SERVER completed\n";
		return unless $REINSTALL;
	}

	if( $servertype eq "SYBASE" and $DBVERSION>=12.5 ) {
		my($enabled)="FALSE";

		# foreach( dbi_query(-db=>"master",
		#	-query=>"select * from sysconfigures where config=356 and value=1",
		#	-connection=>$connection ) ) {
		foreach ( run_db_query(-db=>"master",-query=>"select * from sysconfigures where config=356 and value=1") ) {
			print "[$SERVER] Enable Monitoring Is 1\n";
			$enabled="TRUE";
			# well enable monitoring on... check number of tables

			#foreach( dbi_query(-db=>"master",
			#	-query=>"select count(*) from master..sysobjects where type='U' and name like 'mon%'",
			#	-connection=>$connection ) ) {
			foreach( run_db_query(-db=>"master",
				-query=>"select count(*) from master..sysobjects where type='U' and name like 'mon%'" ) ) {
				my($num)=dbi_decode_row($_);
				if( $num > 15 ) {	 # there are ~20 of these things so this will work
					$ADD_MDA="TRUE";
					print "[$SERVER] SYBASE MDA TABLES FOUND\n";
				} else {
					print "[$SERVER] ONLY $num SYBASE MDA TABLES FOUND (mon%)\n";
				}
			}
			if( $ADD_MDA eq "TRUE" ) {
				# my($mode)=dbi_set_mode("INLINE");
				#foreach( dbi_query(-db=>"master",
				#	-query=>"select count(*) from master..monOpenDatabases",
				#	-connection=>$connection,
				#	-die_on_error=>0 ) ) {
				foreach( run_db_query(-db=>"master",
					-query=>"select count(*) from master..monOpenDatabases" 	) ) {
					my($msg)=dbi_decode_row($_);
					if( $msg =~ /Requested server name not found/ ) {
						print "-------------------------------------------------------------------\n";
						print "MDA TABLES EXIST BUT ARE NOT SET UP        \n";
						print "PLEASE SET UP YOUR MDA TABLES IF YOU WISH MDA PROCS TO BE INSTALLED\n";
						print "-------------------------------------------------------------------\n";
						$ADD_MDA="FALSE";
						last;
					}
				}
				# dbi_set_mode($mode);
			}
		};
		if( $DBVERSION >= 15 ) {			# IT IS SYSTEM 15!!!
			print "[$SERVER] Seting System 15 Flag\n";
			$IS_SYS_15="TRUE";
		};
	}

	#my($osql_cmd)= "osql -n -U$USER -P$PASSWORD -S \"$SERVER\" ";
	#my($osql_cmd_pr)= "osql -n -U$USER -PXXX -S \"$SERVER\" ";
	if( $servertype ne "SYBASE" or ! $DATABASE )  {
		my($found_msdb);
		foreach ( run_db_query(-db=>"master",
						-query=>"select name from sysdatabases where name='msdb'" )) {
			$found_msdb="TRUE";
		}

		$DATABASE="master";
#		$servertype="SQLSVR";

#		warn __LINE__," ",$osql_cmd." -Q\"select 'SUCCESS'\" |\n" if $print_diags_as_warns;
#		open(CMD,$osql_cmd." -Q\"select 'SUCCESS'\" |")
#			or die "Cant test connection\n";
#		my($success)="FALSE";
#		foreach (<CMD>) {
#			$success="TRUE" if /SUCCESS/;
#		}
#		close(CMD);
#
#		if( $success eq "FALSE" ) {				# try native...
#			$osql_cmd= "osql -n -E -S \"$SERVER\" ";
#			$osql_cmd_pr= "osql -n -E -S \"$SERVER\" ";
#
#			warn __LINE__," ",$osql_cmd." -Q\"select 'SUCCESS'\" |\n" if $print_diags_as_warns;
#			open(CMD,$osql_cmd." -Q\"select 'SUCCESS'\" |")
#				or die "Cant test connection\n";
#			my($success)="FALSE";
#			foreach (<CMD>) {
#				$success="TRUE" if /SUCCESS/;
#			}
#			close(CMD);
#			if( $success eq "FALSE" ) {
#				print "*******************************",$NL;
#				print "* Cant connect to $SERVER using osql",$NL;
#				print "*******************************",$NL;
#				return;
#			} elsif( $PASSWORD eq 'unneeded' ) {
#				print "Connected to $SERVER using native authentication\n";
#			} else {
#				print "*******************************",$NL;
#				print "* Bad Sql Server Password Passed to $SERVER",$NL;
#				print "* Successful connection in trusted mode",$NL;
#				print "* Continuing...",$NL;
#				print "*******************************",$NL;
#			}
#		}
	}

	die "UNKNOWN DATABASE - neither msdb or sybsystemprocs found\n"
		unless defined $DATABASE;

	my($proclib)="";
	my(@rc)= run_db_query(-db=>$DATABASE,
			-query=>"select 1 from sysobjects where name='sp__proclib_version'" );
	if( $DATABASE eq "msdb" and $#rc < 0 ) {
		$DATABASE="master";
		@rc= run_db_query( -db => $DATABASE, -query => "select 1 from sysobjects where name='sp__proclib_version'" );
	}

	if($#rc>=0) {
		my($spVERSION);
		foreach( run_db_query(-db=>$DATABASE,-query=>"exec sp__proclib_version" ) ) {
			($spVERSION)=dbi_decode_row($_);
		};
		$spVERSION =~ s/\s//g;
		print __LINE__,"> Retrieved Version $spVERSION \n" if $DEBUG;

		if( defined $MINVERSION ) {
			if( $MINVERSION > $spVERSION ) {
				print "[$SERVER] Current Stored Procedure At Version $spVERSION (installing) $NL";
			} else {
				if( $MINVERSION == $spVERSION ) {
					print "Skipping installation: version is current ($spVERSION) $NL $NL";
				} else {
					print "Skipping installation: current version ($spVERSION) >= $MINVERSION $NL $NL";
				}
				return;
			}
		} else {
			print "[$SERVER] Current Stored Procedure At Version $spVERSION $NL";
		}
	} else {
		print "[$SERVER] Procedure Library is Not Currently installed...\n";
	}

	my($useline,$dbline);
	unlink "database" if -r "database";
	open(WR,">database") or die "cant write file database";
	$useline="use $DATABASE";
	print WR $useline,"\n";
	close(WR);
	chmod 0666, "database";

	unlink "dumpdb" if -r "dumpdb";
	open(WR2,">dumpdb") or die "cant write file dumpdb";
	$dbline="dump tran $DATABASE with no_log\n";
	print WR2 $dbline,"\n"  unless $DBVERSION=~/Sql Server 2008/i;
	close(WR2);
	chmod 0666, "dumpdb";

	# test sybase install if needed
	my($SYB_IS_ISQL_OK)="FALSE";
	if( $servertype ne "SQLSVR" ) {

		die "ERROR cant find get_syb_crdate.sql in $startdir\n" unless -r "$startdir/get_syb_crdate.sql";
		warn __LINE__," isql -U$USER -PXXX -S$SERVER -i$curdir/get_syb_crdate.sql |\n" if $print_diags_as_warns;
		open(CMD,"isql -U$USER -P$PASSWORD -S$SERVER -i$curdir/get_syb_crdate.sql |")
			or die "Cant run isql -U$USER -P<pass> -S$SERVER -i$startdir/get_syb_crdate.sql\n$!\n";
		foreach (<CMD>) {
			$SYB_IS_ISQL_OK="TRUE" if /crdate/;
		}
		close(CMD);
		# dbi_set_mode("INLINE");
	}

	print "[$SERVER] Install Information
   >> DATABASE = $DATABASE
   >> SERVERTYPE=$servertype
   >> IS_SYS_15=$IS_SYS_15
   >> DBVERSION = $DBVERSION
   >> SYB_IS_ISQL_OK=$SYB_IS_ISQL_OK
   >> ADD_MDA=$ADD_MDA\n";

	foreach my $f ( get_list_of_files($servertype, $ADD_MDA, $IS_SYS_15, $DBVERSION, $LEVEL )) {
		warn __LINE__," get_list_of_files returned $f\n" if $print_diags_as_warns;

		if( ! -r $f ) {
			if( -r "$curdir/$f" ) {
				$f = "$curdir/$f";
			} elsif( -r "$startdir/$f" ) {
				$f = "$startdir/$f";
			} elsif( -r "./$f" ) {
				$f = "./$f";
			} else {
				die "Unable to find $f in\n$curdir\n$startdir\n.";
			}
			warn __LINE__," f is now $f\n" if $print_diags_as_warns;
		}

		print "[$SERVER] Installing ",$f,$NL;
		my(@rc) = run_db_file(-db=>$DATABASE, -file=>$f);
		foreach (@rc) { print dbi_decode_row($_),"\n"; }

#		if( $servertype eq "SQLSVR" ) {
#			print "$osql_cmd_pr -i$f\n" if $DEBUG;
#			warn __LINE__," $osql_cmd -i$f 2>&1 |\n" if $print_diags_as_warns;
#			open(CMD,"$osql_cmd -i$f 2>&1 |")
#				or die "Cant run $osql_cmd -i$f\n";
#			foreach (<CMD>) {
#				warn __LINE__," rc=$_\n" if $print_diags_as_warns;
#				print $_
#					unless /sysdepends for the current/
#					or /depends on the missing object/
#					or /Caution: Changing any part/
#					or /The object was renamed/
#					or /created./
#					or /Msg 156, Level 15, State 1,/			# needed for tran errors
#					or /syntax near the keyword /				# needed for tran errors
#					or /procedures./
#					or /rows affected\)/
#					or /THE NEXT PROCEDURE MIGHT/
#					or /2007/
#					or /^Msg 156, Level 15, State 1, Server /;	# cant truncate master on 2008
#			}
#			close(CMD);
#		} elsif( $SYB_IS_ISQL_OK eq "FALSE" ) {
#			print "Calling imitate_isql_cmd()\n" if $DEBUG;
#			imitate_isql_cmd($connection,$f,$DATABASE);
#		} else {
#			print "isql -U$USER -Pxxx -S$SERVER -i$f\n" if $DEBUG;
#			warn __LINE__," isql -U$USER -P$PASSWORD -S$SERVER -i$f |\n" if $print_diags_as_warns;
#			open(CMD,"isql -U$USER -P$PASSWORD -S$SERVER -i$f |")
#				or die "Cant run isql -U$USER -S$SERVER -i$f\n";
#			foreach (<CMD>) {
#				print $_
#					unless /$useline/
#					or /sysdepends/
#					or /$dbline/
#					or /Object name has been changed./
#					or /return status = 0/
#					or /THE NEXT PROCEDURE MIGHT/
#					or /2007/
#					or /^\d+>\s*$/;
#			}
#			close(CMD);
#		}
	}

	unlink "database" if -r "database";
	unlink "dumpdb" if -r "dumpdb";

	print "Installation Completed on $SERVER $NL $NL";
}

sub get_version {
	my($file)=@_;
	my($version_file, $VERSION);
	$version_file=$file if defined $file and -r $file;
	 

	$version_file="$startdir/version" if -e "$startdir/version" and ! defined $version_file;
	$version_file="$startdir/VERSION" if -e "$startdir/VERSION" and ! defined $version_file;
	
	$version_file="version" if -e "version" and ! defined $version_file;
	$version_file="VERSION" if -e "VERSION" and ! defined $version_file;
	
	$version_file="../version" if ! defined $version_file and -e "../version";
	$version_file="../VERSION" if ! defined $version_file and -e "../VERSION";
	
	#my($codedir)=dirname($0);
	#$version_file=$codedir."/VERSION" if ! defined $version_file and -e $codedir."/VERSION";
	#$version_file=$codedir."/version" if ! defined $version_file and -e $codedir."/version";
	die "Cant find a file named VERSION anywhere\nThis file should be located in . or .. or $startdir directories\n" unless defined $version_file;

	open (VER,$version_file) or die "Cant read file $version_file...\n";
	while (<VER>) { chomp; next if /^\s*$/; $VERSION=$_; }
	close VER;

	my($sht_version)=$VERSION;
	$sht_version=~s/\(.+$//;
	$sht_version=~s/\s+$//;
	$sht_version=~s/^[\D\s]+//g;

	return($VERSION, $sht_version);
}

sub imitate_isql_cmd {
	my($connection,$file,$db)=@_;
	my($rc)=open(FILE,$file);
	if( ! $rc ) {	return "Cant read $file : $!\n"; }
	my($batchsql)="";
	my($errormsgs)="";
	my($batchid)=0;

	while(<FILE>) {
		chomp;chomp;
		if(/^go/) {
			if( $batchsql =~ /:r database/ or $batchsql=~/:r dumpdb/ or $batchsql=~/^\s*$/ ) {
				$batchsql="";
				next;
			}
			$batchid++;
			print "LINE ",__LINE__,"  Batch $batchsql\n" if $TEST_MODE;
			my(@rc) = run_db_query(	-query=>$batchsql, -db=>$db );
			my($err)="";
			foreach (@rc) {
				my(@d)=dbi_decode_row($_);
				my($rowdat)=join(" ",@d);
				next if $rowdat =~ /because it does not exist in the system catalog/;
				next if $rowdat =~ /because it doesn't exist in the system catalog/;
				chomp $rowdat;
				$err.=$rowdat."\n";
			}
			print "$file: Batch $batchid: Error $err" if $err ne "";
			$batchsql="";
		} else {
			$batchsql.=$_."\n";
		}
	}
}
sub get_list_of_files {
	my($servertype,$ADD_MDA,$IS_SYS_15,$DBVERSION,$LEVEL)=@_;
	print __LINE__,"> ","get_list_of_files: sty=",$servertype," addmda=",$ADD_MDA," is15=",$IS_SYS_15," dbv=",$DBVERSION," lvl=",$LEVEL,"\n" if $DEBUG;
	my(@rc);
	foreach my $r (@data) {
		chomp $r;
		next if $r=~/^\s*#/ or $r=~/^\s*$/;
		my($ty,$nm)=split(/\s+/,$r);
		# SQLSVR2000 auditdb.sqlsvr
		# SQLSVR2005 auditdb.2005
		# print "DBGDBG: t=$ty nm=$nm\n";
		# TY = SYBASE SYBASE_MDA SYBASE_15 SYBASE_12 SQLSVR
		if( $ty eq "SYBASE" ) {
			next if $servertype eq "SYBASE_MDA";
			next if $servertype eq "SQLSVR";
		} elsif( $ty eq "SYBASE_MDA" ){
			next unless $servertype eq "SYBASE_MDA";
		} elsif( $ty eq "SYBASE_15" ){
			next if $servertype eq "SYBASE_MDA";
			next if $servertype eq "SQLSVR";
			next if $servertype eq "SYBASE12";
			next if $IS_SYS_15 eq "FALSE";
		} elsif( $ty eq "SYBASE_12" ){
			next if $servertype eq "SYBASE_MDA";
			next if $servertype eq "SQLSVR";
			next if $servertype eq "SYBASE15";
			next if $IS_SYS_15 eq "TRUE";
		} elsif( $ty eq "SYBASE_492" ) {
			# depricated
			next;
		} elsif( $ty eq "SQLSVR" or $ty eq "SQLSVR2000" or $ty eq "SQLSVR2005" ){
			next if 	$servertype eq "SYBASE_MDA";
			next if 	$servertype =~ /SYBASE/;
			next if  $ty eq "SQLSVR2000" and $LEVEL>=9;
			next if  $ty eq "SQLSVR2005" and $LEVEL<9;
		} else {
			die "UNKNOWN TY=$ty NM=$nm \n";
		}
		next if $FILE and $nm ne $FILE;
		push @rc, $nm;
	}
	return @rc;
}
my($sv_is_nt);
sub is_nt {
	# set something if you are on NT
	return $sv_is_nt if defined $sv_is_nt;
	$sv_is_nt=0;
	$sv_is_nt=1 if $^O =~ /MSWin32/;
	return $sv_is_nt;
}

sub installmsg {
		print "====================================== $NL";
		print " $NL";
		print "You are installing a FREE set of system stored procedures, designed to $NL";
		print "extend the system procedures in both Sybase ASE and Microsoft SQL Server.  $NL$NL";
		print "Details on your right to use are described in the online documentation. $NL";
		print "Home Page: http://www.edbarlow.com $NL";
		print " $NL";
}

sub test_isql_auth {
	my($SRV,$LOGIN,$PASS)=@_;
	print __LINE__,"> TESTING isql Password Authentication\n" if $DEBUG;

	open(F,">delete_me") or die "Cant write file delete_me\n";
	print F "select 'SUCCESS'\ngo\nexit\n";
	close(F);
	if( $print_diags_as_warns ) {
		warn __LINE__," isql -U$LOGIN -PXXX -S$SRV -idelete_me |\n" if $DEBUG;
	} else {
		print __LINE__,"> isql -U$LOGIN -PXXX -S$SRV -idelete_me |\n" if $DEBUG;
	}

	open(CMD,"isql -U$LOGIN -P$PASS -S$SRV -idelete_me |")	or die "Cant test connection $!\n";
	my($success)="FALSE";
	my( @outputc );
	foreach (<CMD>) {
		print __LINE__,"> isql Authentication Ok\n" if $DEBUG and /SUCCESS/;
		push @outputc, __LINE__."> ".$_ if $DEBUG;
		next unless /SUCCESS/;

		#die "DONE";
		unlink("delete_me");
		return 1;
	}
	close(CMD);
	print __LINE__,"> isql Native Authentication Failed\n",join("\n",@outputc) if $DEBUG;
	#die "DONE";
	unlink("delete_me");
	return 0;
}

sub test_osql_native_auth {
	my($SRV)=@_;
	return 0 unless is_nt();
	print __LINE__,"> TESTING Osql Native Authentication\n" if $DEBUG;
	my($cmd);
	if( is_odbc_dsn($SRV) eq "TRUE" ) {
		$cmd="osql -n -E -D \"$SRV\" -Q\"select 'SUCCESS'\"";
	} else {
		$cmd="osql -n -E -S \"$SRV\" -Q\"select 'SUCCESS'\"";
	}

	print __LINE__,"> $cmd |\n" if $DEBUG;
	open(CMD,"$cmd |")
			or die "Cant test connection\n";
	my($success)="FALSE";
	foreach (<CMD>) {
	#	print $_;
		print __LINE__,"> osql Native AUthentication Ok\n" if $DEBUG and /SUCCESS/;
		return 1 if /SUCCESS/;
	}
	close(CMD);
	print __LINE__,"> osql Native AUthentication Failed\n" if $DEBUG;
	return 0;
}

sub uninstall_server {
	my($connection,$servertype)=@_;
	my(%fpl);
	foreach ( @fullproclist ) { $fpl{$_}=1; }
	my(@nms);
	foreach( run_db_query(-db=>"master",
				-query=>"select name from master..sysobjects where type='P' and name like 'sp*_*_%' escape '*'"	) ) {
		my($nm)=dbi_decode_row($_);
		push @nms,$nm;
	}
	foreach ( @nms ) {
		if( ! $fpl{$_} ) { print "Cant Uninstall master..$_ as not a GEM Procedure\n"; next; }
		print "Uninstalling master..$_\n";
		run_db_query(-db=>"master",-query=>"drop procedure $_" );
	}

	if( $servertype eq "SYBASE" ) {
		my(@nms);
		foreach( run_db_query(-db=>"master",
					-query=>"select name from sybsystemprocs..sysobjects where type='P' and name like 'sp*_*_%' escape '*'" )) {
			my($nm)=dbi_decode_row($_);
			push @nms,$nm;
		}
		foreach ( @nms ) {
			if( ! $fpl{$_} ) { print "Cant Uninstall sybsystemprocs..$_ as not a GEM Procedure\n"; next; }
			print "Uninstalling sybsystemprocs..$_\n";
			run_db_query(-db=>"sybsystemprocs",-query=>"drop procedure $_" );
		}
	} else {
		my(@nms);
		foreach( run_db_query(-db=>"master",
					-query=>"select name from msdb..sysobjects where type='P' and name like 'sp*_*_%' escape '*'" ) ) {
			my($nm)=dbi_decode_row($_);
			push @nms,$nm;
		}
		foreach ( @nms ) {
			if( ! $fpl{$_} ) { print "Cant Uninstall msdb..$_ as not a GEM Procedure\n"; next; }
			print "Uninstalling msdb..$_\n";
			run_db_query(-db=>"msdb",-query=>"drop procedure $_" );
		}
	}
}

__DATA__

SYBASE uptime.sql
SQLSVR uptime.sql
SQLSVR2000 auditdb.sqlsvr
SQLSVR2005 auditdb.2005
SQLSVR auditsecurity.sqlsvr
SQLSVR badindex.492
SQLSVR block.sqlsvr
SQLSVR colconflict.492
SQLSVR collist.492
SQLSVR colnull.492
SQLSVR helpcolumn.sqlsvr
SQLSVR helpindex.sqlsvr
SQLSVR helplogin.sqlsvr
SYBASE helpmirror.sqlsvr
SQLSVR helpprotect.sqlsvr
SQLSVR lock.sqlsvr
SQLSVR lockt.sqlsvr
SQLSVR revindex.sqlsvr
SQLSVR revtable.sqlsvr
SQLSVR syntax.492
SQLSVR whodo.sqlsvr
SQLSVR bcp.sqlsvr
SQLSVR checkkey.sql
SQLSVR date.sql
SQLSVR datediff.sql
SQLSVR dbspace.sqlsvr
SQLSVR depends.sql
SQLSVR diskdevice.sqlsvr
SQLSVR dumpdevice.sql
SQLSVR find_msg_idx.sql
SQLSVR flowchart.sql
SQLSVR get_error.sql
SQLSVR grep.sqlsvr
SQLSVR groupprotect.sql
SQLSVR helpdbdev.sqlsvr
SQLSVR helpdefault.sql
SQLSVR helpgroup.sqlsvr
SQLSVR helpproc.sql
SQLSVR helprule.sql
SQLSVR helptable.sqlsvr
SQLSVR updstats.sqlsvr
SQLSVR helptext.sqlsvr
SQLSVR helptrigger.sql
SQLSVR2000 helpuser.sqlsvr
SQLSVR2000 helpuser2.sqlsvr
SQLSVR2005 helpuser.2005
SQLSVR2005 helpuser2.2005
SQLSVR helpview.sql
SQLSVR id.sqlsvr
SQLSVR indexspace.sqlsvr
SQLSVR2000 iostat.sqlsvr
SQLSVR2005 iostat.2005
SQLSVR isactive.sql
SQLSVR ls.sql
SQLSVR noindex.sqlsvr
SQLSVR objprotect.sqlsvr
SQLSVR proclib.sql
SQLSVR qspace.sqlsvr
SQLSVR read_write.sqlsvr
SQLSVR revalias.sql
SQLSVR revdb.sqlsvr
SQLSVR revdevice.sql
SQLSVR revgroup.sql
SQLSVR2000 revlogin.sqlsvr
SQLSVR2005 revlogin.2005
SQLSVR revmirror.sqlsvr
SQLSVR revsegment.sql
SQLSVR2000 revuser.sqlsvr
SQLSVR2005 revuser.2005
SQLSVR size.sql
SQLSVR stat.sqlsvr
SQLSVR trigger.sql
SQLSVR who.sqlsvr
SQLSVR whoactive.sqlsvr
SQLSVR revrole.sqlsvr
SQLSVR helpobject.sql
SQLSVR helpdb.sqlsvr
SQLSVR help.sqlsvr
SQLSVR helpdevice.sql
SQLSVR spconfig.sqlsvr
SQLSVR server.sqlsvr
SQLSVR deletebackuphistory.sqlsvr
#SQLSVR record.492
#SQLSVR quickstats.sqlsvr
SQLSVR serverproperty.sqlsvr
SQLSVR metadata.sqlsvr
SQLSVR create_insert.sql

SYBASE_492 auditdb.10
SYBASE_492 badindex.10
SYBASE_492 auditsecurity.10
SYBASE_492 bcp.sql
SYBASE_492 dbspace.sql
SYBASE_492 helpdbdev.sql
SYBASE_492 helpsegment.sql
SYBASE_492 helptable.sql

SYBASE_12 auditdb.10
SYBASE_15	auditdb.15
SYBASE_12 auditsecurity.12
SYBASE_15 auditsecurity.15
SYBASE_12 badindex.10
SYBASE_15	badindex.15
SYBASE_12 bcp.sql
SYBASE_15	bcp.15
SYBASE block.10
SYBASE checkkey.sql
SYBASE colconflict.10
SYBASE collist.10
SYBASE colnull.10
SYBASE date.sql
SYBASE datediff.sql
SYBASE_12 dbspace.sql
SYBASE_15 dbspace.15
SYBASE depends.sql
SYBASE_492 diskdevice.492
SYBASE_12 diskdevice.sql
SYBASE_15 diskdevice.15
SYBASE dumpdevice.sql
SYBASE find_msg_idx.sql
SYBASE flowchart.sql
SYBASE get_error.sql
SYBASE grep.sql
SYBASE groupprotect.sql
SYBASE helpcolumn.10
SYBASE_12 helpdbdev.sql
SYBASE_15	helpdbdev.15
SYBASE helpdefault.sql
SYBASE helpgroup.sql
SYBASE helpindex.10
SYBASE helplogin.10
SYBASE helpmirror.sql
SYBASE helpproc.sql
SYBASE helpprotect.10
SYBASE helprule.sql
SYBASE_12 helpsegment.sql
SYBASE_15 helpsegment.15
SYBASE_12 helptable.sql
SYBASE_15 helptable.15
SYBASE_492 helptext.492
SYBASE_12 helptext.sql
SYBASE_15 helptext.sql
SYBASE helptrigger.sql
SYBASE helptype.sql
SYBASE helpuser.sql
SYBASE helpuser2.sql
SYBASE helpview.sql
SYBASE id.sql
SYBASE_492 indexspace.sql
SYBASE_12 indexspace.sql
SYBASE_15	indexspace.15
SYBASE iostat.sql
SYBASE isactive.sql
SYBASE create_insert.sql
SYBASE lock.10
SYBASE lockt.10
SYBASE ls.sql
SYBASE_492 noindex.sql
SYBASE_12 noindex.sql
SYBASE_15	noindex.15
SYBASE objprotect.sql
SYBASE proclib.sql
SYBASE_492 qspace.sql
SYBASE_12 qspace.sql
SYBASE_15 qspace.15
SYBASE_492 read_write.sql
SYBASE_12 read_write.sql
SYBASE_15 read_write.15
SYBASE stat.sql
#SYBASE record.10
SYBASE revalias.sql
SYBASE revbindings.sql
SYBASE_492 revdb.sql
SYBASE_12 revdb.sql
SYBASE_15	revdb.15
SYBASE_492 revdevice.sql
SYBASE_12 revdevice.sql
SYBASE_15 revdevice.15
SYBASE revgroup.sql
SYBASE revindex.10
SYBASE revkey.10
SYBASE revlogin.sql
SYBASE revmirror.sql
SYBASE revrole.sql
SYBASE revrule.10
SYBASE_12 revsegment.sql
SYBASE_15	revsegment.15
SYBASE revtable.10
SYBASE revtype.sql
SYBASE revuser.sql
SYBASE_492 segment.sql
SYBASE_12 segment.sql
SYBASE_15 segment.15
SYBASE size.sql
SYBASE spconfig.sql
SYBASE revconfig.sql
SYBASE syntax.10
SYBASE trigger.sql

SYBASE_492 vdevno.sql
SYBASE_12 vdevno.sql
SYBASE_15 vdevno.15
SYBASE who.sql
SYBASE whoactive.sql
SYBASE whodo.10
SYBASE whoe.sql
SYBASE helpobject.sql
SYBASE helpdb.10
SYBASE help.sql
SYBASE_12 show_tempdb.10
SYBASE_15 show_tempdb.15
SYBASE helpdevice.sql
SYBASE server.sql
#SYBASE quickstats.10
SYBASE_MDA sp__monbackup
SYBASE_MDA sp__moncache
SYBASE_MDA sp__mondump
SYBASE_MDA sp__monengine
SYBASE_MDA sp__monio
SYBASE_MDA sp__monlock
SYBASE_MDA sp__monlocksql
SYBASE_MDA sp__monnet
SYBASE_MDA sp__monobj
SYBASE_MDA sp__monopenobj
SYBASE_MDA sp__monpwaits
SYBASE_MDA sp__monrunning
SYBASE_MDA sp__monserver
SYBASE_MDA sp__monspid
SYBASE_MDA sp__monsql
SYBASE_MDA sp__montableusage
SYBASE_MDA sp__monwaits
SYBASE_MDA sp__monunusedindex
SYBASE_MDA sp__monunusedindex_server
SYBASE_MDA sp__montopn
SYBASE_MDA sp__monusedtables
SYBASE marksuspect.12
SYBASE_12 updstats.12
SYBASE_15 updstats.12
SQLSVR serverspace.sqlsvr
SQLSVR2005 helpbackup.2005
SQLSVR fragmentation.sqlsvr
SQLSVR2005 marksystemobjects.sqlsvr
