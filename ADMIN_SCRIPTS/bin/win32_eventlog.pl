#!/apps/perl/linux/perl-5.8.2/bin/perl

# copyright (c) 2005-2008 by SQL Technologies.  All Rights Reserved.

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use Win32::EventLog;
use Data::Dumper;
use MlpAlarm;
use CommonFunc;
use CommonHeader;
use Getopt::Long;
use vars qw( 	$OUTFILE	$ERRORFILE $NOCONFIGFILE $SYSTEM $HOURS $SERVICE
					$FULLREFRESH $DEVELOPER $LISTEVENTSONLY
					$CONFIGFILE $SHOWINFOMSGS $NOINFOMSGS
					$DEBUG $LOGEVENTS $LOOPSLEEPTIME $BATCHNAME);
use Do_Time;
use Repository;

my(%IGNORELIST_i,%IGNORELIST_w,%IGNORELIST_e);
my(%IGNORELIST_i_svc,%IGNORELIST_w_svc,%IGNORELIST_e_svc);
my($is_sev_upgraded);
my(%cfgtxt,%cfgrowid);
my(%write_cfgtxt,%write_cfgrowid,$real_start_time);

$|=1;

my($curdir)=dirname($0); # changedir to directory in which the executable exists
chdir $curdir or error_out( "Cant chdir to $curdir : $! \n" );

sub usage {
	print @_,"\n";
 	return "Usage: win32_eventlog.pl --OUTFILE=file --ERRORFILE=file --SYSTEM=system --HOURS=hours --SERVICE=service[,service] [--CONFIGFILE=filename]  [--NOINFOMSGS] [--DEBUG] [--LOGEVENTS] [--LOOPSLEEPTIME] --BATCHNAME=name [--FULLREFRESH] [--SHOWINFOMSGS] [--LISTEVENTSONLY]";
}

sub read_my_configfile {
	open(CONFIGFILE,"$CONFIGFILE") or error_out( "Cant read configfile $CONFIGFILE: $!\n" );
	while(<CONFIGFILE>) {
		chop;
		my($system,$logtype,$rowid,$txt)=split(/\|/,$_,4);
		$cfgrowid{$system.":".$logtype}=$rowid;
		$cfgtxt{$system.":".$logtype}=$txt;
		$write_cfgrowid{$system.":".$logtype}=$rowid;
		$write_cfgtxt{$system.":".$logtype}=$txt;
	}
	close(CONFIGFILE);
}

sub write_configfile {
		print "Writing CONFIGFILE $CONFIGFILE\n";
		my($outstr)="";
		foreach my $x ( sort keys %write_cfgtxt ) {
			my($sys2,$logtype)=split( /:/,$x );
			$outstr.=$sys2."|".$logtype."|".$write_cfgrowid{$sys2.":".$logtype}."|".$write_cfgtxt{$sys2.":".$logtype}."\n";
			print $sys2."|".$logtype."|".$write_cfgrowid{$sys2.":".$logtype}."|".$write_cfgtxt{$sys2.":".$logtype}."\n"
				if $sys2 eq $SYSTEM;
		}

		sleep(1) if -e $CONFIGFILE.".tmp";
		sleep(1) if -e $CONFIGFILE.".tmp";
		sleep(1) if -e $CONFIGFILE.".tmp";
		sleep(1) if -e $CONFIGFILE.".tmp";
		unlink $CONFIGFILE.".tmp" if -e $CONFIGFILE.".tmp";

		error_out("Cant remove $CONFIGFILE.tmp")  if -e $CONFIGFILE.".tmp";
		open(  WCONFIGFILE,">$CONFIGFILE".".tmp")
						or error_out( "Cant write ".$CONFIGFILE.".tmp: $!\n" );
		print  WCONFIGFILE $outstr;
		close( WCONFIGFILE);
		rename($CONFIGFILE.".tmp",$CONFIGFILE)
			or return error_out("Cant rename $CONFIGFILE.tmp to $CONFIGFILE : $!");
}

sub error_out {
   my($errormsg)=join("",@_);
   $errormsg = "BATCH ".$BATCHNAME.": ".$errormsg if $BATCHNAME;
   MlpBatchJobErr($errormsg);
   die $errormsg;
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
    GetOptions( "OUTFILE=s"=>\$OUTFILE,
    	"ERRORFILE=s"		=>\$ERRORFILE,
    	"SYSTEM=s"			=>\$SYSTEM,
    	"SERVER=s"			=>\$SYSTEM,
    	"HOURS=s"			=>\$HOURS,
    	"SERVICE=s"			=>\$SERVICE,
    	"DEVELOPER"			=>\$DEVELOPER,
    	"CONFIGFILE=s"		=>\$CONFIGFILE,
    	"NOCONFIGFILE"		=>\$NOCONFIGFILE,
    	"FULLREFRESH"		=>\$FULLREFRESH,
    	"LISTEVENTSONLY"	=>\$LISTEVENTSONLY,
    	"SHOWINFOMSGS"		=>\$SHOWINFOMSGS,
    	"NOINFOMSGS"		=>\$NOINFOMSGS,
    	"DEBUG"				=>\$DEBUG,
    	"LOGEVENTS"			=>\$LOGEVENTS,
		"BATCHNAME=s"		=>\$BATCHNAME,
    	"LOOPSLEEPTIME=i"	=>\$LOOPSLEEPTIME );

undef $NOINFOMSGS if $SHOWINFOMSGS;
$BATCHNAME="Win32Eventlog" unless $BATCHNAME;
undef $CONFIGFILE if $NOCONFIGFILE;

die usage("Must pass --HOURS") unless defined $HOURS;

if( defined $LOOPSLEEPTIME ) {
	cd_home();
	if( Am_I_Up() ) {  print "Program Running - Aborting\n"; exit(0); }
	I_Am_Up();
}
use Sys::Hostname;

print "BATCH: ",$BATCHNAME,"\n" if $BATCHNAME;
print "PROGRAM: win32_eventlog.pl Started at ".localtime(time)."\n";
print "[",__LINE__,"] (dbg) Program called in debug mode\n" if $DEBUG;
print "   HOSTNAME     => ",hostname(),"\n";
print "   OUTFILE      => $OUTFILE \n" 	   if $OUTFILE;
print "   ERRORFILE    => $ERRORFILE\n"     if $ERRORFILE;
print "   SYSTEM       => $SYSTEM\n"        if $SYSTEM;
print "   HOURS        => $HOURS\n"         if $HOURS;
print "   SERVICE      => $SERVICE\n"       if $SERVICE;
print "   DEVELOPER    => $DEVELOPER\n"     if $DEVELOPER;
print "   CONFIGFILE   => $CONFIGFILE\n"    if $CONFIGFILE;
print "   FULLREFRESH  => $FULLREFRESH\n"   if $FULLREFRESH;
print "   SHOWINFOMSGS => $SHOWINFOMSGS\n"  if $SHOWINFOMSGS;
print "   NOINFOMSGS   => $NOINFOMSGS\n"    if $NOINFOMSGS ;
print "   DEBUG        => $DEBUG\n"         if $DEBUG;
print "   LOGEVENTS    => $LOGEVENTS\n"     if $LOGEVENTS;
print "   BATCHNAME    => $BATCHNAME\n"     if $BATCHNAME;
print "   LOOPSLEEPTIME=> $LOOPSLEEPTIME\n" if $LOOPSLEEPTIME ;

my(@systems);
if( defined $SYSTEM and $SYSTEM ne "ALL" ) {
	@systems= split(/,/,$SYSTEM);
} else {
	@systems=get_password(-type=>"win32servers");
}

# read the data section for ignore
while( <DATA> ) {
	next if /^#/ or /^\s*$/;
	chomp;
	my($svc,$data)=split(/\s+/,$_,2);
	$data=~s/\s//g;
	my(@dat)=split(/,/,$data);
	#print "Override/Ignore: svc=$svc data=$data\n" if $DEBUG;
	foreach (@dat) {
		if( /^I/i ) {
			s/^\w//;
			$IGNORELIST_i{$svc.$_}=1;
			$IGNORELIST_i_svc{$svc}=1;
		} elsif( /^W/i ) {
			s/^\w//;
			$IGNORELIST_w{$svc.$_}=1;
			$IGNORELIST_w_svc{$svc}=1;
		} elsif( /^E/i ) {
			s/^\w//;
			$IGNORELIST_e{$svc.$_}=1;
			$IGNORELIST_e_svc{$svc}=1;
		} else {
			error_out( "Invalid Data Line $svc $data\n" );
		}
	}
}

my(%MESSAGES,$DATA_IS_READ);
sub read_data_file {
	return if $DATA_IS_READ;
	$DATA_IS_READ=1;
	my($rc)=open(DAT,$curdir."/win32_eventlog.messagedata");
	if( ! $rc ) {
		warn " cant read $curdir/win32_eventlog.messagedata\n";
		return;
	}
	while(<DAT>) {
		my($src,$message_id,$message)=split(/,/,$_,3);
		$MESSAGES{$message_id} = $message;
	}
	close(DAT);
}

sub convert_msg_to_txt {
	my($m)=@_;
	my($txt)=substr(join(' ',split(/\0/,$m)),0,40);
	$txt=~s/\n/ /g;
	$txt=~s/\r/ /g;
	return $txt;
}

my(%SERVICE_DO);
foreach ( split(/,/,$SERVICE )) { $SERVICE_DO{lc($_)}=1; }

while( 1 ) {

	if( defined $CONFIGFILE and -e $CONFIGFILE and ! $FULLREFRESH ) {
		read_my_configfile();
	}

	my(%errorcount, %warningcount, %sourcelist);
	my($haserrors)=0;
	my(%message,%errors);
	my(%writemsgs);
	my(%start_time, %end_time);
	$real_start_time = time;
	foreach my $sys (sort @systems) {
		my %info = get_password_info(-name=>$sys, -type=>'win32servers');
		if( $info{IS_CLUSTERNODE} eq "Y" ) {
			print "SKIPPING CLUSTERNODE $sys - WE DONT COLLECT LOGS ON CLUSTERNODES\n";
			print " -- clusternode detected from win32_passwords.dat file\n";
			next;
		}
		$start_time{$sys}=time;
		MlpBatchJobStart( -BATCH_ID=>$BATCHNAME, -SUBKEY=>$sys ) if defined $LOGEVENTS;
		my($sys_start_time)=time;
		$writemsgs{$sys}=0;
		print "\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n>>> Working on system $sys at ".localtime()."\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n";
		foreach my $logtype ("Application","System","Security") {
			my($alarmlvl)="ok";
			my($alarmmsg)="";
			print ">>> (dbg) Working on log type=$logtype\n" if $DEBUG;

			my $handle = Win32::EventLog->new($logtype,$sys )
				or error_out( "Cant open $logtype Eventlog on $sys" );

			my($base,$recs);
			$handle->GetOldest($base);
			$handle->GetNumber($recs);
			$Win32::EventLog::GetMessageText=1;		# formatted messages

			my($hashRef);
			my($message_string);

		 	# ok lets describe this.  $base is used as the offset for Read().
		 	# $recs is the number of records in the log.  Therefore the most
		 	# recent record is $base+$recs.  We can position too using the offset
		 	# in Read()

			if( defined $CONFIGFILE and ! $FULLREFRESH )  {
				my($verified)="UNKNOWN";
				if( defined $cfgrowid{$sys.":".$logtype} ) {			# Check row given in config file
					my($logstr)= sprintf("-> Logtype=%-11.11s GetOldest()=%-6s GetNumber()=recs=%-6s MaxRecNo=%-6s\n-> SavedRecNo=%-10s\n-> SavedRecText=%s\n",
												$logtype,
												$base,
												$recs,
												$base+$recs,
												$cfgrowid{$sys.":".$logtype},
												$cfgtxt{$sys.":".$logtype}
											);
					if( $cfgrowid{$sys.":".$logtype} < $base ) {
		 				print $logstr,"-> Passed Log Marker - Adding Whole Tbl (filtered)\n";
						$verified="PASSED";
					} else {
						my($rid)= $cfgrowid{$sys.":".$logtype};
						print $logstr,"-> Reading Record $rid\n";
						$handle->Read(EVENTLOG_FORWARDS_READ|EVENTLOG_SEEK_READ,$rid,$hashRef);
#						print "DBG DBG ################## DUMPER AT LINE=",__LINE__,"\n";
#						print "DBG DBG ",Dumper(\$hashRef),"\n";
#						print "DBG DBG ##################\n";

						#parse_EventMsg($hashRef,1);

						my($txt)=convert_msg_to_txt($$hashRef{Message});
						if( $cfgtxt{$sys.":".$logtype} eq $txt ) {
							if( $cfgrowid{$sys.":".$logtype} == ($base+$recs-1) ) {
		 						print $logstr,"-> No New Records Found In Log\n\n";
								next;
							}
		 					print $logstr, "-> Starting Reading At Last Record\n";
							$verified="TRUE";
						} else {
							$verified="FALSE";
		 					print $logstr, "-> Verification Failed - Must redo the whole eventlog\n";
							print "  -- Event Log Failed Verification Test\n";
							print "  -- SAVED ROWID: {",$cfgrowid{$sys.":".$logtype},"}\n";
							print "  -- SAVED TEXT : {",$cfgtxt{$sys.":".$logtype},"}\n";
		 					print "  -- DBMS ROW: ",$txt,"\n";
							print "DBMS ROW NULL\n" if ! defined $txt ;
							print "DBMS ROW EMPTY\n" if $txt eq "";
						}
					}
				} else {
		 			print "-> Logtype=$logtype Base_Record_# = $base Num_Recs=$recs\n"
							unless $LISTEVENTSONLY;
		 			print "-> Configfile specified but no record for $sys $logtype\n"
							unless $LISTEVENTSONLY;
		 			delete $cfgrowid{$sys.":".$logtype};
				}

				my($rid)= $base+$recs-1;
				next if $rid<0;
				if( $verified eq "TRUE" ) {
					print "-> There are ".($rid - $cfgrowid{$sys.":".$logtype})." new log rows\n";
				} elsif( $verified eq "FALSE" ) {
					$cfgrowid{$sys.":".$logtype}=0;
				} elsif( $verified eq "PASSED" ) {
					$cfgrowid{$sys.":".$logtype}=0;
				} else {
					$verified = "UNKNOWN";
					print "-> New Log Load Adding Whole Tbl (filtered)\n";
					$cfgrowid{$sys.":".$logtype}=0;
				}

				#? if( $verified ne "FALSE" and $verified ne "PASSED" ) {
					print "  -- (dbg) Getting Last Row (row id=$rid)\n";
					$handle->Read(EVENTLOG_FORWARDS_READ|EVENTLOG_SEEK_READ,$rid,$hashRef);
					#print "DBG DBG ################## ",__LINE__,"\n";
					#print "DBG DBG ",Dumper \$hashRef,"\n";
					#print "DBG DBG ##################\n";

					#parse_EventMsg($hashRef,1);
					$write_cfgtxt{$sys.":".$logtype}=convert_msg_to_txt($$hashRef{Message});
					#$message_string = join(' ',split(/\0/,$$hashRef{Message}));
					#$write_cfgtxt{$sys.":".$logtype}=substr($message_string,0,40);
					#$write_cfgtxt{$sys.":".$logtype}=~s/\n/ /g;
					#$write_cfgtxt{$sys.":".$logtype}=~s/\r/ /g;
					print "  -- Setting write_cfgrowid{",$sys,",",$logtype,"}=$rid; \n";
					$write_cfgrowid{$sys.":".$logtype}=$rid;
			 		print "  -- (dbg) LAST ROW: ",$write_cfgtxt{$sys.":".$logtype},"\n" if $DEBUG;
			 	#?} else {
			 	#?	print " -- Not Saving Record as Verified=$verified\n";
			 	#?	print " current id - ", $write_cfgrowid{$sys.":".$logtype}," new id would be $rid\n";
			 	#?	print " current txt- ", $write_cfgtxt{$sys.":".$logtype}," new txt would be ",convert_msg_to_txt($$hashRef{Message},"\n";
			 	#?}
			} else {
		 		print "-> Logtype=$logtype Base_Record_# = $base Num_Recs=$recs\n" unless $LISTEVENTSONLY;
			}

			print "-> System=$sys Logtype=$logtype\n";
			my($recno)=0;
			my(%usesubsys, %usecount, %usetime, %useheartbeat);		# For Repeats
			while( $recno<$recs ) {
				$recno++;
				if( $DEBUG ) {
					print "
	###########################################################
	##### $sys - Reading $logtype EVENTLOG RECORD $recno ######
	###########################################################\n" unless $LISTEVENTSONLY;
				#} else {
				#	print $recno."] " unless $LISTEVENTSONLY;
				}

				# read sequentially from most recent
				# print "-> Reading Record \n";
				$handle->Read(EVENTLOG_BACKWARDS_READ|EVENTLOG_SEQUENTIAL_READ,undef,$hashRef) or next; # die "Cant read Eventlog entry $recno of $recs from $sys $!";
#				print "DBG DBG ################## DUMPER AT LINE=",__LINE__,"\n";
#				print "DBG DBG ",Dumper(\$hashRef),"\n";
#				print "DBG DBG ##################\n";

				$message_string = join(' ',split(/\0/,$$hashRef{Message}));

				# STANDAD POST EVENT PROCESSING
				$$hashRef{'EventID'} &= 0xffff;		# Make Event Id Readable
				#$$hashRef->{'Strings'} =~ tr/\0/\n/;

				if($DEBUG) {
					print $recno."] " unless $LISTEVENTSONLY;
					print "[",__LINE__,"] (dbg) ================ RAW EVENT DETAILS ====================\n";
					print "[",__LINE__,"] (dbg) $message_string\n" if $message_string;
					foreach ( keys %$hashRef ) {
						print "[",__LINE__,"] (dbg) ---> $_ $$hashRef{$_} \n" unless $_ eq "Message";
					}
				}

				if( defined $CONFIGFILE )  {
					if( $recno > $base+$recs-1 - $cfgrowid{$sys.":".$logtype} ) {
						print $recno."] Last Row Found - From Configfile ",$cfgrowid{$sys.":".$logtype},"\n";
						last;
					}
				}

				# when the time limit has been reached... quit
				if( time - $$hashRef{TimeGenerated} > $HOURS*60*60 ) {
					my($hrsold)=int((time - $$hashRef{TimeGenerated}) / 3600);
					print $recno."]\n##########################################\n# DONE REC# $recno is $hrsold hours old - our threshold is $HOURS hours\n##########################################\n\n"
						unless $LISTEVENTSONLY;;
					last;
				};

				$sourcelist{$$hashRef{Source}}=1;

				if( $$hashRef{EventType} == EVENTLOG_AUDIT_SUCCESS ) {
					print $recno."] REC# $recno: skipping EVENTLOG_AUDIT_SUCCESS message\n" if $DEBUG;
					next;
				}

				unless(  $$hashRef{EventType} == EVENTLOG_INFORMATION_TYPE
					   or $$hashRef{EventType} == EVENTLOG_AUDIT_FAILURE
					   or $$hashRef{EventType} == EVENTLOG_ERROR_TYPE
					   or $$hashRef{EventType} == EVENTLOG_WARNING_TYPE ) {
					print $recno."] REC# $recno: (failed event type check - not INFO,AUDIT,ERROR,WARNING)\n";
					if( $DEBUG ) {
						foreach ( keys %$hashRef ) { print "[",__LINE__,"] (dbg) >>> message key=$_ value=$$hashRef{$_} \n"; }
					}
					next;
				};

				if( defined $SERVICE and ! defined $SERVICE_DO{lc($$hashRef{Source})} ) {
					print $recno."] " unless $LISTEVENTSONLY;
					print "REC# $recno: (failed service check.  $$hashRef{Source} not in $SERVICE)\n";
					next;
				};

				if( $$hashRef{Source} eq "SymantecAntiVirus" ) {
					if( $message_string =~ /Could not scan 1 files inside .+ due to extraction errors/
					or $message_string =~ /Scan could not open file/
					or $message_string =~ /Could not scan % files inside/
					or $message_string =~ /Scan could not access path/ ) {
						print  $recno."] Skipping Predefined AntiVirus Message - $message_string\n";
						next;
					}
				}


				# my(@words) = split(/\0/,$$hashRef{Strings});
				my(@words)=split(/\0/,($$hashRef{Message}||$$hashRef{Strings}));

				my($tag);
				my($eventID)= $$hashRef{'EventID'};		# Make Event Id Readable

				# 2/2/10 - put back in this override... not sure of the impact
				if( $eventID eq "17055" ) {		# sql server flexible event
					next;		# sorry im ignoring them all!
					$words[0]=~ /^(\d+)/; 		# calculate Event ID from Strings provided they start with one...
					$eventID=$1;					# Make Event Id Readable
					print $recno."] Diag: Event Id Reset from 17055 to $eventID\n";
				}

				if( defined $DEBUG ) {
					print "[",__LINE__,"] (dbg) ======== SYSTEM $sys LOG $logtype REC # $recno ===== eventid=$eventID ====\n";
					if( $eventID != $$hashRef{'EventID'} ) {
						print $recno."]",__LINE__,"] (dbg) Overwrote Event Id - Set to $eventID From ",$$hashRef{'EventID'}," \n";
					}
					my($lineno)=1;
					foreach (keys %$hashRef){
						if( $$hashRef{$_} =~ /[\r\n]$/ ) {
							printf "[".__LINE__."] (dbg) #%2d:%2d) %20s => %s", $recno,$lineno++,$_ ,$$hashRef{$_};
						} elsif( $_ =~ /^Time/ )  {
							printf "[".__LINE__."] (dbg) %d-%2d) %20s => %s - %s\n", $recno,$lineno++,$_ ,$$hashRef{$_}, scalar localtime($$hashRef{$_});
						} else {
							printf "[".__LINE__."] (dbg) %d-%2d) %20s => %s\n", $recno,$lineno++,$_ ,$$hashRef{$_};
						}
					}
				}

				print $recno."] " unless $LISTEVENTSONLY;

				# THIS IS NOT PARTICULARLY ACCURATE
				# print "RETRIEVING Event Id $eventID from $$hashRef{Strings} \n";
				my($SrcNoSp)=$$hashRef{Source};
				$SrcNoSp =~ s/\s//g;

				if( $$hashRef{EventType} == EVENTLOG_ERROR_TYPE
				or  $$hashRef{EventType} == EVENTLOG_AUDIT_FAILURE ) {
					if( $IGNORELIST_e{$SrcNoSp.$eventID}
					or  $IGNORELIST_e{$SrcNoSp."%"} ) {
						print "REC# $recno: (msg num $eventID src $SrcNoSp is on Error ignore list)\n" if $DEBUG;
						next;
					}
					$tag="ERROR";
				} elsif( $$hashRef{EventType} == EVENTLOG_WARNING_TYPE ) {

					if( $IGNORELIST_w_svc{$SrcNoSp} ) {
						# print "DBGW: Warning Found ($SrcNoSp)\n";
						# foreach ( keys %$hashRef ) { print "DBGH: k=$_ v=$$hashRef{$_} \n"; }
						# foreach ( keys %IGNORELIST_w ) {
							# print "DBGI: $_ v=$IGNORELIST_w{$_} \n";
						# }
						if( #$IGNORELIST_w{$SrcNoSp.$$hashRef{EventID}} or
							$IGNORELIST_w{$SrcNoSp.$eventID}
						or  $IGNORELIST_w{$SrcNoSp."%"} ) {
							#print "REC# $recno: (msg num $eventID src $SrcNoSp is on this services ignore list)\n"
							#	if $DEBUG and $IGNORELIST_w{$SrcNoSp.$$hashRef{EventID}};
							print "REC# $recno: (msg num $eventID src $SrcNoSp is on this services ignore list)\n"
								if $IGNORELIST_w{$SrcNoSp.$eventID} and $DEBUG;
							print "REC# $recno: (msg num *any* src $SrcNoSp is on this services ignore list)\n"
								if $IGNORELIST_w{$SrcNoSp."%"} and $DEBUG;
							next;
						} else {
							print "REC# $recno: (msg $eventID/$$hashRef{EventID} src $SrcNoSp NOT on ignore list - continuing)\n" if $DEBUG;
						}
					}
					$tag="WARNING";
				} else {
					if( $IGNORELIST_i{$SrcNoSp.$eventID}
					or  $IGNORELIST_i{$SrcNoSp."%"} ) {
						print "REC# $recno: (msg num $eventID src $$hashRef{Source} is on info ignore list)\n" if $DEBUG;
						next;
					}
					$tag="INFORMATION";
				}

				print "[",__LINE__,"] Event $eventID Msg=$message_string\n" if $DEBUG;
				if( !  $message_string or $message_string=~/^\s*$/ ) {
					if( $$hashRef{Source} eq "MSSQLSERVER" ) {
						read_data_file();
						if( $MESSAGES{$eventID} ) {
							$message_string = sprintf($MESSAGES{$eventID}, @words);
							print "[",__LINE__,"] Setting msg_string=$message_string\n" if $DEBUG;
						} else {
							$message_string = join(' ',split(/\0/,$$hashRef{Strings}));
							print "[",__LINE__,"] Setting msg_string=$message_string\n" if $DEBUG;
							warn "CANT FIND MSSQLSERVER EVENT $eventID - set to $message_string\n"
								unless $eventID == 19019 or $eventID == 17052;
						}
					} elsif(	$$hashRef{Source} eq "SQLSERVERAGENT" and $eventID==208 ) {
						$message_string = sprintf("SQL Server Scheduled Job '%s' (%s) - Status: %s - Invoked on: %s - Message: %s.  %s",@words);
					} else {
						$message_string = join(' ',split(/\0/,$$hashRef{Strings})) if !  $message_string or $message_string=~/^\s*$/;
					}
				}
				print "[",__LINE__,"] Event $eventID Msg=$message_string\n" if $DEBUG;

			#	print "[",__LINE__,"] MESSAGE=" unless $LISTEVENTSONLY;
			#	print $message_string."\n";

				#remove ctrl m's
				$message_string =~ s/\r\n+/\n/g;
				#$message_string =~ tr/\0/\n/g;
				chomp $message_string;
				chomp $message_string;

				my($lmsg)=(scalar localtime($$hashRef{TimeGenerated})).",".
							$$hashRef{Computer}.",".
							$tag.",".
							$SrcNoSp.",".$message_string;
				$lmsg =~ s/\s*\r\n/\n/g;
				$lmsg =~ s/\s+$//g;
				$lmsg =~ s/,\s+/, /g;
				$lmsg =~ s/\n\n/\n/g;

				print "[",__LINE__,"] EventId=$eventID\n"       if $DEBUG;
				print "[",__LINE__,"] msg_string=$message_string\n" if $DEBUG;

				# UPTAG WARNINGS THAT SHOULD BE ERRORS
				if( $eventID==208 and $tag eq "WARNING"
				and $$hashRef{Source} eq "SQLSERVERAGENT"
				and $message_string=~"The job failed" ) {
					print "Upgrading WARNING event to ERROR event\n" if $DEBUG;
					$tag="ERROR";
				}

				#
				# skip file not found messages for sql server as these
				#  are normally associated with by hand redos
				#
				if( $eventID == 18204  and
					$message_string=~/Operating system error/ ) {
					# $message_string=~/The system can\s*not find the file specified./ ) {
					print "Ignoring File Not Found Message. These are not errors.\n" if $DEBUG;
					next;
				}

				#print "(".$tag.") ".$lmsg,"\n"	if  ! $LISTEVENTSONLY and ($tag eq "WARNING" or $tag eq "ERROR");
				my($rectime)=do_time(-time=>$$hashRef{TimeGenerated},-fmt=>"mm/dd/yyyy hh:mi:ss");

				if( $LISTEVENTSONLY or $tag eq "WARNING" or $tag eq "ERROR" ) {
					my($x)=$message_string;
					$x=~s/[\r\n]//g;
					$x=~s/\s\s+/ /g;
					print "(".$tag.") $rectime ".$x."\n";
				}


				# CHECK FOR REPEATED EVENTS
				if( defined $message_string ) {
					# these are the events we plan to save
					if( defined $usecount{$message_string} ) {
						$usesubsys{$message_string}=$SrcNoSp;
						print "\tSkipped - This message has been seen $usecount{$message_string} times\n\tMsgText=$message_string\n"
							unless $LISTEVENTSONLY;
						$usecount{$message_string}++;
						next;
					} else {
						$usecount{$message_string}=1;
						$usetime{$message_string}=$rectime;
						$usesubsys{$message_string}=$SrcNoSp;
					}
				}

				if($eventID == 8957	)  {
					print "*** Unhandled DBCC Message Found\n";
				}

				$is_sev_upgraded='FALSE';
				parse_EventMsg($hashRef,$DEBUG);

				# tran log loaded gives a heartbeat too
				#if($message_string =~ /^18268/ or $message_string =~ /^18264/)
				if($eventID == 18268	or $eventID == 18264)  {
					$message_string =~ /Database:\s+(\w+),/;
					my($db)=$1;

					if( ! $db ) {
						print "[",__LINE__,"] (dbg) Sql2005 Words:" if $DEBUG;
						# my(@words)=split(/\0/,($$hashRef{Message}||$$hashRef{Strings}));
						if( $DEBUG ) {
							foreach (@words) { print $_,"|"; }
							print "\n";
						}
						$db = $words[0];
						print "[",__LINE__,"] (dbg) Database Set to $db\n" if $DEBUG;
					}

					if( ! $db ) {
						print "MESSAGE = $message_string\n";
						print "MESSAGE ERROR: CANT RETRIEVE DATABASE NAME\n";
						next;
					}

					print "MISSMATCH: Database ($db,$$hashRef{o_Database})\n" 		if $db 		ne $$hashRef{o_Database};
					my($sev)="ERROR";
					$sev="OK" if $tag eq "INFORMATION";
					# skip if you have used heartbeat
					my($subsys)=$db;
					if($eventID == 18268 ) {
						$subsys.=" log restore"
					} else {
						$subsys.=" backup"
					}
					if( defined $useheartbeat{$subsys} ) {
						print "[",__LINE__,"] (dbg) Duplicate 18264/18268 Dump/Load Tran - $subsys \n";
						next;
					}
					$useheartbeat{$subsys}=1;

					#print "TRAN LOG MESSAGE FOUND EventId=$eventID - SAVING SPECIAL HEARTBEAT\n\t-> SYS=$sys SUBSYSTEM=$subsys STATE=$sev\n\t-> BATCH=$BATCHNAME TIME=$rectime\n\t-> TEXT=$message_string\n";
					#print "[",__LINE__,"] (dbg) Heartbeat $sys. $subsys. Time=".  $rectime." ".$message_string."\n" if $DEBUG;

					#if( $LOGEVENTS ) {
						if( defined $DEBUG ) {
							print "[",__LINE__,"] (dbg) ======== SAVING TRAN LOG HEARTBEAT MESSAGE =======\n";
							print "[",__LINE__,"] (dbg) monitor_program => $BATCHNAME\n";
						  	print "[",__LINE__,"] (dbg)          system => $sys\n";
						  	print "[",__LINE__,"] (dbg)       subsystem => $subsys\n";
						  	print "[",__LINE__,"] (dbg)      event_time => $rectime\n";
						  	print "[",__LINE__,"] (dbg)           state => $sev\n";
						  	print "[",__LINE__,"] (dbg)    message_text => $message_string \n";
						}

						MlpHeartbeat( -monitor_program=>$BATCHNAME,
						  -system=>$sys,
						  -subsystem=>$subsys,
						  -event_time=> $rectime,
						  -state=>$sev,
						  -message_text=> $message_string );
					 #}
				}

				#if( $NOINFOMSGS and $$hashRef{EventType} == EVENTLOG_INFORMATION_TYPE ) {
				if( $NOINFOMSGS and $tag eq "INFORMATION" ) {
					print "REC# $recno: NotSaving As --NOINFOMSGS SET.\n" if $DEBUG;
					next;
				}

				print "WARNING: Eventid's do not match ($eventID,$$hashRef{o_eventID})\n"
						if $eventID != $$hashRef{o_eventID};
				print "WARNING: Severitys Do Not Match ($tag,$$hashRef{o_Severity})\n"
					if $tag 		ne $$hashRef{o_Severity} and $is_sev_upgraded ne 'TRUE';
				print "WARNING: Time does not match ($rectime,$$hashRef{o_EventTime})\n" if $rectime ne $$hashRef{o_EventTime};

				if( defined $message_string and defined $LOGEVENTS ){
					my($t)=$message_string;
					$t=~s/[^\w\d\s:_()\*\&\^\%\$\#\@\!\\\|]//g;
					$eventID='' unless $eventID;
					print "      ======== SAVING EVENT MESSAGE =======\n";
					print "               system => $sys\n";
					print "             severity => $tag\n";
					print "         message_text => $message_string \n";
					print "           Transformed=> $t\n";

					if( defined $DEBUG ) {
						print "[",__LINE__,"] (dbg) monitor_program => $BATCHNAME\n";
					  	print "[",__LINE__,"] (dbg)        event_id => $eventID\n";
					  	print "[",__LINE__,"] (dbg)       subsystem => $SrcNoSp\n";
					  	print "[",__LINE__,"] (dbg)      event_time => $rectime\n";
					  	print "[",__LINE__,"] (dbg)    message_text => $message_string \n";
						print "[",__LINE__,"] (dbg)      Transformed=> $t\n";
					}

					MlpEvent(
						-monitor_program=>$BATCHNAME,
					  	-system=>$sys,
					  	#-subsystem=>$logtype,
					  	-debug => $DEBUG,
					  	-event_id=>$eventID,
					  	-subsystem=>$SrcNoSp,
					  	-event_time=> $rectime,
					  	-severity=>$tag,
					  	-message_text=> $message_string
					);
					print "      ======== SAVE COMPLETED =======\n";

					$writemsgs{$sys}++;
				} elsif( ! defined $LOGEVENTS ) {
					print "Not Logging This Event as no --LOGEVENTS flag - but if i was... it would be
	=>       system=$sys
	=>     event_id=$eventID
	=>    subsystem=$SrcNoSp
	=>     severity=$tag
	=> message_text=$message_string\n"
						unless $LISTEVENTSONLY;
				} else {
					print "Message has no text - sys=$sys subsystem=$SrcNoSp severity=$tag\n";
				}

				if( $$hashRef{EventType} == EVENTLOG_ERROR_TYPE
				or  $$hashRef{EventType} == EVENTLOG_AUDIT_FAILURE ) {
					#$tag="ERROR,";
					if( defined $errorcount{$SrcNoSp.",".$sys} ){
						$errorcount{$SrcNoSp.",".$sys}++;
					} else {
						$errorcount{$SrcNoSp.",".$sys}=1;
					}
					my($msgtime)=$$hashRef{TimeGenerated};
					while( defined $message{$msgtime} ) { $msgtime++; }
					$message{$msgtime} = $lmsg;
					$errors{$msgtime}  = $lmsg;

					$haserrors++;
				} else {
					# $tag="WARNING,";
					if( defined $warningcount{$SrcNoSp.",".$sys} ){
						$warningcount{$SrcNoSp.",".$sys}++;
					} else {
						$warningcount{$SrcNoSp.",".$sys}=1;
					}

					my($msgtime)=$$hashRef{TimeGenerated};
					while( defined $message{$msgtime} ) { $msgtime++; }
					$message{$msgtime} = $lmsg;
				}
			}
			$handle->Close();

			next if $LISTEVENTSONLY;
			# Update Repeated Events

			print "\n############################################################\n# Completed Reading $logtype Event Log - Processing Duplicates\n############################################################\n\n";
			foreach ( keys %usecount ) {
				next if $usecount{$_}==1;

				print "Updating Message (Subsystem=$usesubsys{$_} Time= $usetime{$_})\n\tOriginal=$_\nUpdate=[$usecount{$_} repeats] $_\n";
				#print "Repeated Event: system=$sys subsystem=",$logtype," \n" if defined $LOGEVENTS;
				#print "    *  Updating Use Count=$usecount{$_} Time=$usetime{$_}\n";
				#my($t)=$_;
				#$t=~s/^[\W\d\s]//g;

				MlpEventUpdate(
					-debug=>$DEBUG,
					-monitor_program=>$BATCHNAME,
					-system=>$sys,
					-subsystem=>$usesubsys{$_},
					-event_time=> $usetime{$_},
					-message_text=> "[$usecount{$_} repeats] $_" ) if defined $LOGEVENTS;
			}
		};

		# MlpBatchRunning( -monitor_program=>$BATCHNAME, -system	=> $sys) if defined $LOGEVENTS;

		write_configfile() if defined $CONFIGFILE;

		print "Completed Work On System $sys in ",(time-$sys_start_time)," seconds\n";
		MlpBatchJobEnd() if defined $LOGEVENTS;
		$end_time{$sys}=time;
	} # added by emb due to missing colon - is it bad???

	# Write a report to the OUTFILE
	if( defined $OUTFILE ) {
		open(OUTF,"> $OUTFILE") or error_out( "Cant Write To $OUTFILE : $!\n" );
		print OUTF "PC Errorlog Report\n";
		print OUTF "Filtered for Services: $SERVICE\n" if defined $SERVICE;
		print OUTF "Run At: ",(scalar localtime(time))."\n\n";
		print OUTF "-------------------------------------------------\n";
		print OUTF "SUMMARY STATISTICS\n";
		printf OUTF "%20s %20s %4s %4s\n","System","Source","Err","Warn";
		foreach my $sys (sort @systems) {
			foreach my $src (keys %sourcelist) {
				if( defined $errorcount{$src.",".$sys} or defined $warningcount{$src.",".$sys} ){
					printf OUTF "%20s %20s %4d %4d\n",$sys, $src,
						$errorcount{$src.",".$sys} ,
						$warningcount{$src.",".$sys};
				}
			}
		}
		print OUTF "-------------------------------------------------\n";
		foreach (sort keys %message) { print OUTF $message{$_},"\n"; }
		close(OUTF);
	}

	# Write The Errorfile
	if( defined $ERRORFILE ) {
		unlink $ERRORFILE if -w $ERRORFILE;
		if( $haserrors ) {
			open(ERRF,"> $ERRORFILE") or error_out( "Cant Write To $ERRORFILE : $!\n" );
			foreach (sort keys %errors) { print ERRF $errors{$_},"\n"; }
			close(ERRF);
		}
	}

	#if( $LOGEVENTS ) {
	#	print "\n";
	#	foreach (sort keys %writemsgs ) {
	#		print "$_ saved $writemsgs{$_} messages\n";
	#	}
	#}

	foreach (sort keys %start_time) {
		printf("System %-20s Duration %4d seconds %5d messages\n",$_,$end_time{$_}-$start_time{$_},$writemsgs{$_});
	}


	last unless $LOOPSLEEPTIME;
	print "Sleeping for $LOOPSLEEPTIME at ".localtime(time)."\n" if defined $LOOPSLEEPTIME;
	sleep($LOOPSLEEPTIME) if defined $LOOPSLEEPTIME;
	I_Am_Up() if defined $LOOPSLEEPTIME;		# must mark file to prevent restarts
}
print "PROGRAM COMPLEDED | Total Run Time=",(time-$real_start_time)," | EXITING SUCCESS\n";
exit(0);

use Do_Time;

sub parse_EventMsg {

	my($hashRef,$DEBUG)=@_;

	$$hashRef{o_Severity}="INFORMATION" if  $$hashRef{EventType} == EVENTLOG_INFORMATION_TYPE
													or  $$hashRef{EventType} == EVENTLOG_AUDIT_SUCCESS;
	$$hashRef{o_Severity}="ERROR" 	 	if  $$hashRef{EventType} == EVENTLOG_ERROR_TYPE
													or  $$hashRef{EventType} == EVENTLOG_AUDIT_FAILURE;
	$$hashRef{o_Severity}="WARNING" 		if  $$hashRef{EventType} == EVENTLOG_WARNING_TYPE;

	$$hashRef{Strings}=~ /^(\d+)/; 		# calculate Event ID from Strings provided they start with one...
	$$hashRef{o_eventID} = $1 || $$hashRef{'EventID'} & 0xffff;		# Make Event Id Readable

	my(@words)=split(/\0/,($$hashRef{Message}||$$hashRef{Strings}));

	$$hashRef{o_Message} = join("\n",@words);
	$$hashRef{o_Message} =~ s/\r\n+/\n/g;
	chomp $$hashRef{o_Message};
	chomp $$hashRef{o_Message};

	if( $$hashRef{Source} eq "MSSQLSERVER" ) {
		$$hashRef{o_Message} =~ /Database:\s+(\w+),/;
		$$hashRef{o_Database}= $1 || $words[0];
		read_data_file();
	}

	$$hashRef{o_EventTime}=do_time(-time=>$$hashRef{TimeGenerated},-fmt=>"mm/dd/yyyy hh:mi:ss");

	$is_sev_upgraded='FALSE';
	# UPTAG WARNINGS THAT SHOULD BE ERRORS
	if( $$hashRef{eventID}==208
	and $$hashRef{o_Severity} eq "WARNING"
	and $$hashRef{Source} eq "SQLSERVERAGENT"
	and $$hashRef{o_Message}=~"The job failed" ) {
		print "[",__LINE__,"] (dbg) Upgrading WARNING event to ERROR event\n" if $DEBUG;
		$$hashRef{o_Severity}="ERROR";
		$is_sev_upgraded='TRUE';
	}

	if( $DEBUG ) {
		print "[",__LINE__,"] ================ DISPLAY EVENT ========================\n";
		my($lineno)=1;
		foreach (sort keys %$hashRef){
			my($value)=$$hashRef{$_};
			chomp $value;
			chomp $value;
			chomp $value;
			$value=~s/\r\n/\<NL\>/g;
			$value=~s/[\r\n]/\<NL\>/g;

			#if( $value =~ /[\r\n]$/ ) {
			#	printf "| #%2d) %20s => %s", $lineno++,$_ ,$value;
			#} els
			if( $_ =~ /^Time/ )  {
				printf "[".__LINE__."] | %2d) %20s => %s - %s\n", $lineno++,$_ ,$value, scalar localtime($value);
			} else {
				printf "[".__LINE__."] | %2d) %20s => %s\n", $lineno++,$_ ,$value;
			}
		}
		print  "[".__LINE__."] ================ END DISPLAY EVENT LINE ========================\n";
	}
}

	#$handle->Read(EVENTLOG_FORWARDS_READ|EVENTLOG_SEEK_READ,$base+$recs-20,$hashRef) or next;
	#$handle->Read(EVENTLOG_FORWARDS_READ|EVENTLOG_SEQUENTIAL_READ,undef,$hashRef) or next;

=head1 NAME

win32_eventlog.pl - NT Event Log Parser

=head2 DESCRIPTION

Uses Win32 Libraries to check out eventlogs on remote NT servers.  Output
is saved using alarming functions.

=head2 USAGE

  Usage: win32_eventlog.pl --OUTFILE=file --ERRORFILE=file --SYSTEM=system
   --HOURS=hours --SERVICE=service[,service] [--CONFIGFILE=filename]
   [--DEBUG] [--LOGEVENTS]  [--LOOPSLEEPTIME] [--NOINFOMSGS]

 OUTFILE:   	Output Report File
 ERRORFILE:   	Output Report for just errors
 SYSTEM:    	List of systems to work on - comma separated
 HOURS:
 DEBUG:   		Debug
 LOGEVENTS:   	Save Heartbeats using MlpAdmin
 LOOPSLEEPTIME

=head2 CONFIG

Reads Repository data file which looks like:

=head2 IGNORE LIST

The program reads the __DATA__ section at the bottom of the program
to find out messages that it can ignore by service, severity, and
error number.  Each service can specify by Error/Warning/Info whether
it wants to ignore all messages (wildcard %) or a specific list of
messages to ignore.

=cut

__DATA__
# FORMAT - Program - I/W/E for severities
#  - you dont need spaces in the source
#  - Comments start with # or white space
#  - Format ($svc\s+$data)
#    foreach (split(',',$data)) { I/W/E -> info/warn/error }
#
Perflib		I%,W%,E%
W3Ctrs		I%,W%,E%
Userenv		I%,W%,E%
OtMan5		I%
Security		I515,I562,I560
# Error 18278 is truncated tran log - just ignore these
MSSQLSERVER I18454,I18453,I17055,I17550,I18278,E18278,E18204,I18268,I18264
BROWSER		E8032
DCOM			E907
NortonAntiVirus	W6,W-2130771962
TermServDevices I%,W%,E%
LicenseService I%,W%
Print I%,W%
BlackBerryPolicyService W200
