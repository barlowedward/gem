#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Win32API::Net qw( LocalGroupEnum LocalGroupGetMembers LocalGroupGetInfo UserEnum UserGetInfo);
use Repository;
use MlpAlarm;
use File::Basename;
use DBIFunc;
use Getopt::Long;
use strict;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);
use vars qw($BATCH_ID $DEBUG);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID

   --BATCH_ID=id     [ if set - will log via alerts system ]
   --DEBUG \n";
  return "\n";
}

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

my(@vars)= qw(comment passwordAge passwordExpired fullName Disabled DontExpirePassword Lockout usrComment lastLogon numLogons lastLogoff badPwCount acctExpires);

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Credential Mgt' ) if $BATCH_ID;

my($connection)=MlpGetDbh();
my($q)= "delete INV_group_member where group_type = 'win32 local group'";
MlpAlarm::_querynoresults($q,1);
print "\n";

my(@servers)=get_password(-type=>'win32servers');
foreach( @servers ) { collect_server($_); }

sub collect_server {
	my($server)=@_;

	print "***********************\n";
	print "* $server              \n";
	print "***********************\n";

	MlpAlarm::_querynoresults("\tdelete INV_credential where system_name = ".dbi_quote(-connection=>$connection,-text=>$server)." \n\tand system_type = 'win32servers' and credential_type='win32 local'",1 );
	print "\n";
	MlpAlarm::_querynoresults("\tdelete INV_credential_attribute where system_name = ".dbi_quote(-connection=>$connection,-text=>$server)." \n\tand system_type  = 'win32servers' and credential_type='win32 local'", 1);
	print "\n";

	my(@a);
	UserEnum($server,\@a);
	foreach ( @a ) {
		print "==> USER: $_\n";

		my($base)=	dbi_quote(-connection=>$connection,-text=>$_).",".			# login
						"'win32 local',".
						dbi_quote(-connection=>$connection,-text=>"0").",".		# sid
						dbi_quote(-connection=>$connection,-text=>$server)	.",".			# system
						dbi_quote(-connection=>$connection,-text=>'win32servers');		# systype

		my(%h);
		UserGetInfo($server, $_, 3, \%h) or warn "$! $^E";

		#use Data::Dumper;
		#print Dumper \%h;

		if ($h{flags} & Win32API::Net::UF_ACCOUNTDISABLE()) {
			$h{Disabled}=1;
	   } else {
  			$h{Disabled}=0;
	   }

		if ($h{flags} & Win32API::Net::UF_DONT_EXPIRE_PASSWD()) {
			$h{DontExpirePassword}=1;
	   } else {
  			$h{DontExpirePassword}=0;
	   }

		if ($h{flags} & Win32API::Net::UF_LOCKOUT()) {
			$h{Lockout}=1;
	   } else {
  			$h{Lockout}=0;
	   }

		# die "DONE - ",Win32API::Net::UF_ACCOUNTDISABLE();

		my($q)= "\tinsert INV_credential
		(credential_name, credential_type, credential_id, system_name, system_type, collection_date, collected_by )
		values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
		MlpAlarm::_querynoresults($q,1);
		print "\n";

		foreach (@vars) {
			next if $h{$_}=~/^\s*$/;
			$h{$_} =~ s/\s+$//;
			chomp $h{$_};
			$h{$_}=~s/\s+$//;
			next if $_ eq "lastLogon" and $h{$_}==0;
			next if $_ eq "lastLogoff" and $h{$_}==0;
			print "\t",uc($_),": ",$h{$_} if defined $h{$_};

			my($q)= "\tinsert INV_credential_attribute values \n\t( $base ,\n\t".
						dbi_quote(-connection=>$connection,-text=>$_).",".		# sid
						dbi_quote(-connection=>$connection,-text=>$h{$_}).")";
			MlpAlarm::_querynoresults($q,1);
			print "\n";
		}
	}

	print "FETCHING GROUPS\n";
	my(@grps);
	LocalGroupEnum($server,\@grps) or warn "$! $^E";
	#print Dumper \@grps;

	sleep(1);

	foreach my $g (@grps) {
		print "==> GROUP: $g\n";
		my(@users);
		LocalGroupGetMembers($server,$g,\@users) or warn "$! $^E";

		next if $#users< 0; 					# no members
		print "\tMEMBERS: ",join(", ",@users),"\n";

		my(%h);
		LocalGroupGetInfo($server,$g,1,\%h) or warn "$! $^E";
		chomp $h{comment};
		print "\tCOMMENT: ",$h{comment},"\n" if $h{comment};

		my(%usersl);
		foreach my $u ( @users ) {
			next if $usersl{$u};
			$usersl{$u}=1;
			my($q)= "\tinsert INV_group_member values \n\t( ".
							dbi_quote(-connection=>$connection,-text=>$g).",".
							"'win32 local group',null,".
							dbi_quote(-connection=>$connection,-text=>$u).",\n\t".
							"'win32 local',null,".
							"'".$server."','win32servers')";
			MlpAlarm::_querynoresults($q,1);
			print "\n";
		}
	}
}

MlpBatchJobEnd() if $BATCH_ID;
print "Successful Completion\n";
exit(0);

__END__

=pod

=head1 NAME

INV_win_getsystemusers - extract windows server, group and user information into inventory system

=head1 DESCRIPTION

Clear out from INV_credential INV_credential_attribute any
records with credential type 'win32 local'

Enumerate the users for each server

Insert into INV_credential:

  user name
  'win32 local'
  0
  server name
  'win32servers'

Insert into INV_credential_attribute:

  user name
  'win32 local'
  0
  server name
  'win32servers'

  attribute name
  attribute value

where the attribute name is among:

  comment
  passwordAge
  passwordExpired
  fullName
  usrComment
  lastLogon		[omit if value is 0]
  numLogons
  lastLogoff		[omit if value is 0]
  badPwCount
  acctExpires

Clear out from INV_group_member any
records with group type = 'win32 local group'

Enumerate the groups for each server

For each member of each group, insert into INV_group_member:

  group name
  'win32 local group'
  null
  user name
  'win32 local'
  null
  server name
  'win32servers'

=cut
