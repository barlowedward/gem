create procedure get_possible_alarms
  @test_mode varchar(1) = 'N'
as
begin
    -- set from 'I'nitial to 'P'rocessing
	update 	VH_ALARMABLE
	set 		internal_state='P'
	where 	internal_state='I'
	and 		@test_mode = 'N'
	and   	isnull(reviewed_until,getdate())<=getdate()

	update 	VE_ALARMABLE
	set 		internal_state='P'
	where 	internal_state='I'
	and 		@test_mode = 'N'

	if @test_mode != 'N' and @test_mode != 'H' and @test_mode != 'E'
	begin
		print 'TEST MODE MUST BE N,H, or E'
		return
	end

	select 	distinct h.monitor_program,h.system,h.subsystem,h.state,h.message_text,n.container,h.monitor_time,
				emergency_minutes, fatal_minutes, error_minutes, warning_minutes, internal_state, reviewed_by, reviewed_until,
				deleted_reason=convert(varchar(30),'')
	into 		#tmp
	from 		VH_ALARMABLE h, Notification n
	where 	( n.monitor_program = h.monitor_program or n.monitor_program='All Programs' )
	and   	isnull(n.system,isnull(h.system,'XXX')) = isnull(h.system,'XXX')
	and   	(n.container is null
			or  h.system in ( select system_name from INV_system_heirarchy where business_system_name=n.container))
	and   	( h.internal_state='P' or @test_mode='H' )
	and   	h.state not in ('OK','COMPLETED','INFORMATION','RUNNING')
	--	and     isnull(reviewed_until,getdate())<=getdate()

	if @test_mode != 'N'
		update #tmp set deleted_reason=reviewed_by where isnull(reviewed_until,getdate())>getdate()

	insert #tmp
    select distinct e.monitor_program,e.system,e.subsystem,e.severity,e.message_text,'',e.event_time,
			emergency_minutes, fatal_minutes, error_minutes, warning_minutes, internal_state, null, null, ''
	from VE_ALARMABLE e, Event_Filter f, Notification n
	where e.severity in ('ERROR','CRITICAL')
    and f.monitor_events='y'
	 and e.monitor_program	=	isnull(f.monitor_program,e.monitor_program)
	 and e.system					= isnull(f.system,e.system)
	 and ( e.subsystem is null or e.subsystem	= isnull(f.subsystem,e.subsystem))
	 and ( n.monitor_program = e.monitor_program or n.monitor_program='All Programs' )
	 and   	isnull(n.system,isnull(e.system,'XXX')) = isnull(e.system,'XXX')
	 and   	(n.container is null
	 		or  e.system in ( select system_name from INV_system_heirarchy where business_system_name=n.container ))
	 and   	( e.internal_state='P' or @test_mode='E' )
	 and  e.message_text not like '%Login failed for user%'
	 and  e.message_text not like '%Logon login failed%'

	-- only production servers
	update 	#tmp
	set 	deleted_reason='NotProd'+' '+deleted_reason
	from 	#tmp h, GEM_servers p
	where h.system=p.system_name
	and   p.production_state != "PRODUCTION"
	and   p.production_state != "DR"
	and	h.system is not null
	and	h.system != ''
	-- and  	p.is_production=0

-- EMERGENCY/CRITICAL/ALERT/ERROR/WARNING
	update 	#tmp
	set 	deleted_reason='EMERGENCY Exists'+' '+deleted_reason
	where state != 'EMERGENCY'
	and   system in ( select system from #tmp where state='EMERGENCY' )

	update 	#tmp
	set 	deleted_reason='CRITICAL Exists'	+' '+deleted_reason
	where state != 'CRITICAL' and state!='EMERGENCY'
	and   system in ( select system from #tmp where state='CRITICAL' )

	update 	#tmp
	set 	deleted_reason='ALERT Exists'+' '+deleted_reason
	where state != 'CRITICAL' and state!='EMERGENCY' and state!='ALERT'
	and   system in ( select system from #tmp where state='ALERT' )

	update #tmp set emergency_minutes=fatal_minutes where state in ('CRITICAL','ALERT','FATALS')
	update #tmp set emergency_minutes=error_minutes where state in ('ERROR','ERRORS','FATALS')
	update #tmp set emergency_minutes=warning_minutes where state in ('WARNING','WARNINGS','FATALS')

	update #tmp
	set deleted_reason = 'Heartbeat'+' '+deleted_reason
	where reviewed_by='HEARTBEAT' and reviewed_until>getdate()

	-- ok now we need to purge the repeats
	-- remove stuff that has been sent recently
	update 	#tmp
	set 	deleted_reason='Sent@'+convert(varchar,notify_time,0)+' '+deleted_reason
	from  	#tmp h, SentNotification s
	where 	s.monitor_program = h.monitor_program
	and	s.system	= h.system
	and	s.subsystem	= h.subsystem
	and 	s.severity	= h.state
	and 	datediff(mi,s.monitor_time,h.monitor_time)<=emergency_minutes

-- select top 20 * from SentNotification

	-- add mneumonic
	update 	#tmp
	set 		system=system+'-'+mneumonic
	from 		#tmp h, GEM_servers p
	where 	h.system=p.system_name
	and		h.system is not null
	and		h.system != ''
	and		p.mneumonic is not null
	and		p.mneumonic != ''

	if 	@test_mode != 'N'
	begin
		update #tmp set deleted_reason='Processed '+deleted_reason where internal_state='C' and deleted_reason not like 'Sent%'

		select deleted_reason,h.monitor_program,h.system,h.subsystem,h.state,h.message_text,h.container,
		h.monitor_time, reviewed_by,reviewed_until
		from #tmp h
		order by system,monitor_program,subsystem
	end
	else
		select h.monitor_program,h.system,h.subsystem,h.state,h.message_text,h.container,h.monitor_time
		from #tmp h
		where deleted_reason=''
end
go
grant execute on get_possible_alarms to public
go
