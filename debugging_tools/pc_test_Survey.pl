use lib qw(G:/dev/lib);

use CommonFunc;
use Repository;
use GemData;

# my(%CONFIG)=read_configfile(-nodie=>1);

#dbi_set_mode("INLINE");

my(@servers)=get_password(-type=>'sqlsvr');
my($name)=$servers[0];
my($login,$password)=get_password(-type=>'sqlsvr',-name=>$name);
print "WORKING ON SERVER $name\n";

my($gd)=new GemData();
my($gem_root_dir)=get_gem_root_dir();
my(@rc)=$gd->survey(
	-name=>$servers[0],
	#-debug=>1,
	-login=>$login,
	-password=>$password,
	-type=>'sqlsvr', 
	-local_directory=>$gem_root_dir."/data/system_information_data");

print "Printing Output Info\n";
foreach (@rc) { 
	print "Survey: ",$_;
}

print "Getting Info\n";
my($d)=$gd->get_set_data(-class=>sqlsvr, -name=>$name, -debug=>1);
print $d;
