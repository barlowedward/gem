: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

#############################################################################
#
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
#
# Copyright (c) 2002 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed
#
# See Perldoc (type perldoc reperl.pl)
#
#############################################################################

use	strict;
use	Getopt::Std;
use	vars qw( $opt_u $opt_D $opt_d );
die	"Usage: $0 -u [-Ddir]\n" unless getopts('duD:');
use	File::Copy;
use	File::Basename;
use	Cwd;

die "Directory $opt_D does not exist\n" if defined $opt_D and ! -d $opt_D;

my($perl)="#!".$^X."\n";
if( $^X eq "perl") {
	my($path)=$ENV{"PATH"};
	if( $path =~ /[C-Z]:/ )  { 		# NT/Windoze
		foreach( split /;/,$path ) {
			next unless -x $_."/perl.exe";
			s.\\./.g;
			$perl="#!".$_."/perl.exe\n";
			last;
		}
	} else { 				# UNIX
		foreach( split /:/,$path ) {
			next unless -x $_."/perl";
			$perl="#!".$_."/perl\n";
			last;
		}
	}
	die "perl program not found in your path\n" unless $perl ne "";
}

if ( defined $opt_u ) {
	print "Resetting Hash Bangs To Long Format\n";
	$perl = "\: # use perl\n\teval \'exec perl -S \$0 \"\$@\"\'\n\tif \$running_under_some_shell;\n\n";
} else {
	print "SETTING PERL TO $perl\n";
}

# get $startdir and $curdir
# lib path is ./lib ../lib $codedir/lib $codedir/../lib
my $startdir=cwd();
print "Startdir is $startdir\n" if defined $opt_d;

my $libdir=$startdir."/lib" if -d "$startdir/lib";
if( ! defined $libdir and -d "../lib" ) { chdir ".."; $libdir=cwd()."/lib"; }
print "Libdir is $libdir\n" if defined $opt_d;

my $codedir=dirname($0);
chdir $codedir or die "Cant cd to $codedir: $!\n";
$codedir=cwd();

$libdir=$codedir."/lib" if ! defined $libdir and -d "lib";
if( ! defined $libdir and -d "../lib" ) { chdir ".."; $libdir=cwd()."/lib"; }

chdir $startdir or die "Cant cd to $startdir: $!\n (try using full path to reperl.pl)";

die "Cant find lib subdirectory in . .. $codedir and $codedir/.." unless defined $libdir;

$libdir="use lib \'$libdir\';\n";
print "SETTING LIB TO $libdir\n";

my( @files );
push @files, "." if $#ARGV<0;
push @files, $opt_D if defined $opt_D;
push @files,@ARGV;
foreach ( @files ) { reformat_file($_) };
exit(0);

# reformat_file a file => passed in with relative or full path name
sub reformat_file
{
	my($file)=@_;

	if( -d $file ) {

		return if $file =~ /data$/;

		print "(Dir)";
		opendir(DIR,$file) || die("Can't open directory $file for reading\n");
		my(@dirlist) = grep(!/^\./,readdir(DIR));
		closedir(DIR);

		foreach (@dirlist) { reformat_file($file."/".$_); }

	} elsif( -T $file ) { 	# text file => also remove ctrl m

		if( ! -w $file ) {
			print "(warning: cant write $file - skipping)";
			next;
		}
		print ".";

		my($is_perlfile)=0;
		$is_perlfile=1 if $file =~ /.pl$/ or $file=~/.dist$/;

		my($linenum)=0;
		my($is_use_perl)=0;
		my($in_file_text)=0;

		# just clean out the hashbangs
		$in_file_text=1 unless $is_perlfile;
		$in_file_text=1 if $file =~ /reperl.pl/;
		$in_file_text=1 if $file =~ /configure.pl/;

		open(INP,"$file") 		or die "Cant open $file: $!\n";
		unlink "$file.cpy" if -r "$file.cpy";
		open(OUT,"> $file.cpy") or die "Cant open temp file $file.cpy: $!\n";
		while( <INP> ) {
			chop;
			s/\r$// unless defined $ENV{PROCESSOR_LEVEL};

			if( $in_file_text ) {
				print OUT $_,"\n";
				next;
			}

			$linenum++;
			next if $is_use_perl and $linenum<=3;	# skip first 3 lines
			if( $is_perlfile ) {
				if( $linenum==1  ) {
					# case 1) first line has #! and ends in perl
					if( /^\#\!/ and /perl\s*$/i ) {
						print OUT $perl;
					# case 2) first line has #! and ends in perl.exe
					} elsif( /^\#\!/ and /perl.exe\s*$/i ) {
						print OUT $perl;
					} elsif( /^:\s*#\s*use\s*perl/ ) {
						if( defined $opt_u ) {
							print OUT $_;
							next;
						}
						print OUT $_,"\n";
						$is_use_perl=1;
					} else {
						print OUT $_,"\n";
						$is_perlfile=0;
					}
				} elsif( $linenum<8 and /use lib/ ) {
					print OUT $libdir;
					$in_file_text=1;
				} else {
					print OUT $_,"\n";
				}
			} else {
				print OUT $_,"\n";
			}
		}
		close INP;
		close OUT;

		# move the file back
		copy("$file.cpy",$file)
				or die "Cant Rename Temporary File $file.cpy to $file: $!";
		unlink("$file.cpy") or die("cant remove file $file.cpy");

		# Chmod the file
		chmod 0775,$file if $is_perlfile;

	} else {
		print "+";
	}
}

__END__

=head1 NAME

reperl.pl

=head2 SYNOPSIS

reperl.pl - Rebuild Hashbang And use lib statements

=head2 COPYRIGHT

 Copyright (c) 2002-3 By Edward Barlow
 All Rights Reserved
 Explicit right to use can be found at www.edbarlow.com
 This software is released as free software and should be shared and enjoyed

=head2 DESCRIPTION

Resets the perl hashbang lines at the top of the file.  If the first line starts with a hashbang #! then the next part is a command to reformat_file.  This can either be of the format

    #!/usr/local/bin/perl

or

   : # use perl
       eval 'exec perl -S $0 "$@"'
       if $running_under_some_shell;

These lines are reformat_fileted correctly based on the perl in your path

=head2 USAGE

	reperl.pl file file file...
or
	reperl.pl *
or
	reperl.pl -D.
or
	reperl.pl -u

		to undo changes to generic format

=head2 NOTES

Reset perl on first or first 3 lines as necessary.  If the first line starts with a hashbang #! then that word is reformatted as necessary.  If the first 3 lines looks like:

   : # use perl
	eval 'exec perl -S $0 "$@"'
	if $running_under_some_shell;

skips files named configure.pl or reperl.pl.  Those should always be full path name to the perl executable using the above syntax.

=cut
