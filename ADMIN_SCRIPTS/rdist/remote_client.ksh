echo "Starting Remote Client"

echo PROGRAM is $0
echo HOME is $HOME

[ ! -d "$HOME/.ssh" ] && mkdir $HOME/.ssh
chmod 700 $HOME/.ssh
chmod 700 $HOME

touch $HOME/.ssh/authorized_keys $HOME/.ssh/authorized_keys2
cat   $HOME/.ssh/authorized_keys $HOME/.ssh/authorized_keys2 /tmp/authkeys.dat | uniq > /tmp/authkeys_final.dat

cp /tmp/authkeys_final.dat $HOME/.ssh/authorized_keys
cp /tmp/authkeys_final.dat $HOME/.ssh/authorized_keys2

chmod 700  $HOME/.ssh/authorized_keys
chmod 700  $HOME/.ssh/authorized_keys
