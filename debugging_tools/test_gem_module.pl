use lib 'G:/dev/lib';

use strict;
use Gem;

my($debug);
sub _statusmsg { print @_; }
sub _debugmsg {  print @_ if $debug; }

my($gem) = new Gem( -printfunc=>\&_statusmsg, -debugfunc=>\&_debugmsg, -hostname=>"bob" );

foreach ( sort keys %$gem ) {
	print $_," => ",$gem->{$_},"\n";
}
#$gem->display();
