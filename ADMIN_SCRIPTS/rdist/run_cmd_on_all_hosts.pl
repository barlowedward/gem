#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use File::Basename;
use Net::myRSH;
use Sys::Hostname;
use strict;
use Repository;
use Getopt::Std;
use CommonFunc;

sub usage {
	return join("",@_)."\n".basename($0)."

	-f Fields
	-c cmd
	-s silent mode - no system print
	-h html output
	-T secs timeout seconds - will die if it goes this long
	-S server (comma separated)
	-C secondary command for use if primary fails
	-D tertiary command for use if primary and secondary fail
	-o columnate
	-k use ssh mode
	-t test modes - will define best method
	-n print a newline between servers
	-x command override code - must be in password file - this can be use
	   to allow different commands by server (ie if -xDF then the DF= command
	   in the unix cfg file will override the -c option)

Fields is comma sep list of fields to print (starting col 1)
First field is hostname\n";
}

use vars qw($opt_f $opt_s $opt_D $opt_c $opt_n $opt_o $opt_x $opt_d $opt_h $opt_C $opt_h $opt_S $opt_T $opt_t $opt_k);
die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('f:c:C:dnox:S:T:D:stkh');

#if( $opt_t ) {
#	# TEST MODE IS SPECIAL...
#	die "Can Pass No Other Args if Test Mode (-t) " if $opt_h or $opt_o or $opt_n or $opt_x or $opt_c;
#	$opt_T = 3 unless $opt_T;
#	$opt_c = "echo 'test_completed'";
#}
die usage("No command - must pass with -c\n") unless defined $opt_c;

my($NL)="\n";
$NL="<br>\n" if $opt_h;
my($FS)="";
my($FE)="";
$FS="<FONT COLOR=RED>" if $opt_h;
$FE="</FONT>" if $opt_h;

print "run_cmd_on_all_hosts.pl run at ".localtime(time)." on host ".hostname().$NL
	unless $opt_s;
print "Running: ",$opt_c," on all hosts in unix_passwords.dat",$NL unless $opt_s;
print "Server List: $opt_S",$NL if defined $opt_S and ! defined $opt_s;
print "Command run in DEBUG mode",$NL if $opt_d;

do_rsh_method("ssh") if $opt_k;

my $curhostname = hostname();
my(@hosts);
if( defined $opt_S) {
	@hosts=split(/,/,$opt_S);
} else {
	@hosts=get_password(-type=>"unix");
}

my $hlen=0;
my(@cols);
$cols[100]=1;
if( defined $opt_f ) {
	$opt_f =~ s/\s//g;
	foreach ( split(/,/,$opt_f)) { $cols[$_]=1; }
} else {
	my($i)=0;
	while ($i<100) { $cols[$i]=1; $i++; }
}

my(%column_map_by_host);
my($num_hosts)=0;
my(%OUTPUT);	# by host
my($OUTPUT2);	# only for -t : this would be for command option #2
my(%SSH_OK, %RSH_OK ); # ditto : only for -t option.

my(@all_columns);
for (1..100) { $all_columns[$_]=1; }

my(@warnings);

print "NO TIMEOUT SPECIFIED WITH -T - SETTING TO 120 SECONDS".$NL unless $opt_T;
$opt_T=120 unless $opt_T;

# foreach (@hosts) { print __LINE__," $_\n"; }
foreach my $host (@hosts) {
	print "." unless $opt_d or $opt_t;
	my($login,$password)=get_password(-type=>"unix",-name=>$host);
   my(%args)	=	get_password_info(-type=>"unix",-name=>$host);
#	if( $args{SERVER_TYPE} ne "PRODUCTION"
#	and $args{SERVER_TYPE} ne "DEVELOPMENT"
#	and $args{SERVER_TYPE} ne "QA" ) {
#		die "WARNING:  SERVER TYPE for $host ($args{SERVER_TYPE}) MUST BE PRODUCTION or DEVELOPMENT or QA\nCHECK THAT SERVER_TYPE IS DEFINED IN YOUR UNIX PASSWORD FILE.".$NL;
#	}

	my($method)="SSH";
#	my($method)	=	$args{UNIX_COMM_METHOD};
#	$method=$args{WIN32_COMM_METHOD} if is_nt();
#	if( $method eq "NONE" ) {
#			$OUTPUT{$host} = $FS."host $host: method=NONE - skipping".$FE.$NL;
#			push @warnings, "host $host: method=NONE - skipping".$NL;
#			next;
#	}
#	$method="SSH" if $opt_t;
#   $method="FTP" if ! defined $method or $method eq "";
#	if( $method eq "FTP" ) {
#			$OUTPUT{$host} = $FS."host $host: method=FTP - skipping".$FE.$NL;
#			push @warnings, "host $host: method=FTP - skipping".$NL;
#			next;
#	}
#
	# print "DBG DBG: TESTING $method TO $host".$NL if $opt_t;
	my $cmd = $opt_c;

	if( $opt_x ) {
   	$cmd = $args{$opt_x} if defined $args{$opt_x} and $args{$opt_x} !~ /^\s*$/;
   	my($a)=$opt_x."COL";
   	if( defined $args{$a} and $args{$a} !~ /^\s*$/ ) {
   		$args{$a} =~ s/\s//g;
   		my(@x);
   		$x[100]=1;
			foreach ( split(/,/,$args{$a})) { $x[$_]=1; }
			$column_map_by_host{$host}=\@x;
			# print __LINE__," DBGDBG: setting $host \n";
   	} else {
	   	$column_map_by_host{$host}=\@cols;
	   	# print __LINE__," DBGDBG: setting $host \n";
	   }
	} else {
		$column_map_by_host{$host}=\@all_columns;
  		# print __LINE__," DBGDBG: setting $host \n";
	 }

	local $SIG{ALRM} = sub { die "CommandTimedout"; };
	eval {
		alarm($opt_T);
		run_a_command($host,$login,$password,$method,$cmd);
		alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			#if( $opt_t ) {
			#	$SSH_OK{$host} = "NO";
			#} else {
				print "$method Command Timed Out to $host".$NL if $opt_d;
				push @warnings, "$method Command Timed Out to $host".$NL;
				$OUTPUT{$host} = $FS."$method Command Timed Out to $host".$FE.$NL;
			#}
		} else {
			print "$method Command Returned $@ from $host".$NL if $opt_d;
			push @warnings, "$method Command Returned $@ from $host".$NL;
			$OUTPUT{$host} = $FS."$method Command Returned $@ from $host".$FE.$NL;
			alarm(0);
		}
	}

	# TEST MODE NEED TO DO IT TWICE
#	next unless $opt_t;
#	print "DBG DBG SSH OUTPUT is $OUTPUT{$host}\n";
#	$SSH_OK{$host} = "YES" if $OUTPUT{$host} eq "test_completed";
#
#	$method="RSH";
#	$OUTPUT2=$OUTPUT{$host};
#	$OUTPUT{$host}="";
#	print "DBG DBG: TESTING $method TO $host".$NL if $opt_t;
#
#   #local $SIG{ALRM} = sub { die "CommandTimedout"; };
#	eval {
#		alarm($opt_T);
#		run_a_command($host,$login,$password,$method,$cmd);
#		alarm(0);
#	};
#	if( $@ ) {
#		if( $@=~/CommandTimedout/ ) {
#			if( $opt_t ) {
#				$RSH_OK{$host} = "NO";
#			} else {
#				print "$method Command Timed Out to $host".$NL if $opt_d;
#				push @warnings, "$method Command Timed Out to $host".$NL;
#				$OUTPUT{$host} = $FS."$method Command Timed Out to $host".$FE.$NL;
#			}
#		} else {
#			print "$method Command Returned $@ from $host".$NL if $opt_d;
#			push @warnings, "$method Command Returned $@ from $host".$NL;
#			$OUTPUT{$host} = $FS."$method Command Returned $@ from $host".$FE.$NL;
#			alarm(0);
#		}
#	}
#	print "DBG DBG RSH OUTPUT is $OUTPUT{$host}\n";
#	$RSH_OK{$host} = "YES" if $OUTPUT{$host} eq "test_completed";
#	$OUTPUT{$host} = $OUTPUT2 ."\n". $OUTPUT{$host};
#	print "DBG DBG FINAL OUTPUT is $OUTPUT{$host}\n";
}
# foreach (@hosts) { print __LINE__," $_\n"; }
#
# glue broken lines together ... sigh
#
if( $opt_x eq "DF" ) {
	foreach my $host (@hosts) {
		my($in)=$OUTPUT{$host};
		$OUTPUT{$host}="";
		foreach my $line (split(/\n/,$in)) {
			$line=~s/\s+$//;
			warn $line if  $line=~/no such file or directory/i;
			next if $line=~/no such file or directory/i;
			my(@d)=split(/\s+/,$line);
			if( $#d==0 ) {
				$OUTPUT{$host}.=$line." ";
			} else {
				$OUTPUT{$host}.=$line.$NL
			}
	 	}
	 	#print 'DBGDBG',$OUTPUT{$host},'\n';

	}
}

my(@CELLLEN); 	# by colnum
if( defined $opt_o) {
	foreach my $host (@hosts) {
		my(@colmap) = @{$column_map_by_host{$host}};
		#print $host,join("/",@colmap),"\n";
		#next;

		foreach my $line (split(/\n/,$OUTPUT{$host})) {
			chomp $line;
			warn $line if  $line=~/^failure/;
			next if $line=~/^failure/;
			my $colid = 1;
			foreach my $cell (split(/\s+/,$line)) {
				$CELLLEN[$colid]=length $cell	if defined $colmap[$colid] and $CELLLEN[$colid]<length $cell;
				$colid++;
			}
		}
	}
}
#
#if( $opt_t ) {
#	print "--- PARSABLE TEST MODE OUTPUT ---\n";
#	foreach my $host (@hosts) {
#		print "HOST $host : SSH_OK=$SSH_OK{$host}, RSH_OK=$RSH_OK{$host} \n";
#		foreach (split(/\n/,$OUTPUT{$host})) {
#			next if /^\s*$/;
#			printf "%".$hlen."s ",$host unless $opt_h;
#			print $_,$NL;
#		}
#	}
#	print "--- DONE PARSABLE TEST MODE OUTPUT ---\n";
#	print "\n**** WARNINGS ****\n" if $#warnings>0;
#	foreach(@warnings) { print $_; }
#
#	exit(0);
#}
# foreach (@hosts) { print __LINE__," $_\n"; }
print "\n<TABLE BORDER=1>\n<TR><TH BGCOLOR=BEIGE>HOSTNAME</TH><TH BGCOLOR=BEIGE>MESSSAGE</TH></TR>" if $opt_h;
foreach my $host (@hosts) {
	if( $opt_h ) {
		print "<TR>\n";
		print "\t<TD VALIGN=TOP BGCOLOR=WHITE>";
		print $host;
		print "</TD>\n\t<TD VALIGN=TOP BGCOLOR=WHITE>";
	}
	# print __LINE__," DBGDBG: host=$host $column_map_by_host{$host} \n";
	my(@colmap) = @{$column_map_by_host{$host}};

	foreach my $line (split(/\n/,$OUTPUT{$host})) {
		chomp $line;
		next if $line=~ /^\s*$/;
		if( $line=~/^failure/ ) {
			if( $opt_h ) {
				print $FS."connect ".$line.$FE;
			} elsif( defined $opt_o ) {
				printf "%".$CELLLEN[1]."s ",$host," " if defined $colmap[1];
				print " connect ".$line,$NL;
			} else {
				print $host," " if defined $colmap[1];
				print " connect ".$line,$NL;
			}
		} else {
			printf "%".$hlen."s ",$host unless $opt_h;
			if( defined $opt_f ) {
				my $colid = 1;
				#print "DBGDBG LINE=$line\n";
				foreach my $cell (split(/\s+/,$line)) {
					#print "DBGDBG $colid | $colmap[$colid] | CELL=$cell\n";
					if( defined $opt_o ) {
						printf "%".$CELLLEN[$colid]."s ",$cell," " if defined $colmap[$colid];
					} else {
						print $cell," " if defined $colmap[$colid];
					}
					$colid++;
				}
			} else {
					print $line;
			}
			print $NL;
		}
	}
	print "</TD></TR>\n" if $opt_h;
	print $NL if defined $opt_n and ! $opt_h;
}
print "</TABLE>\n" if $opt_h;

if( $num_hosts == 0 ) {
	print "No hosts found that have the COMM_METHOD=RSH or SSH in the password file....".$NL;
}

print "\n**** WARNINGS ****\n" if $#warnings>0;
foreach(@warnings) { print $_; }
exit(0);

sub run_a_command
{
	my($host,$login,$password,$method,$cmd)=@_;
	$hlen=length $host if length $host > $hlen;
	print "Host=$host Login=$login Method=$method Cmd=$cmd".$NL if $opt_d;
	my $dat;
	# push @warnings,  "host $host: method=$method".$NL;
	do_rsh_method($method);

	$dat=do_rsh( -login=>$login, -hostname=>$host, -command=>$cmd, -debug=>$opt_d);
	if( $dat=~/^failure/ ) {
		if ( $dat =~ /connection timed out/ ) {
			# not much you can do here
			print "host $host connection timed out".$NL;
			$OUTPUT{$host} =$FS."host $host connection timed out".$FE.$NL;
			return;
		} elsif ( $dat =~ /not found/ ) {
			# oops... try $opt_C and then $opt_D
			if( defined $opt_C ) {
				$cmd=$opt_C;
				$dat=do_rsh(-login=>$login, -hostname=>$host, -command=>$cmd, -debug=>$opt_d);
			}
			if ( $dat =~ /not found$/ ) {
				# oops... try $opt_C and then $opt_D
				if( defined $opt_D ) {
					$cmd=$opt_D;
					$dat=do_rsh(-login=>$login, -hostname=>$host, -command=>$cmd, -debug=>$opt_d);
				}
				if ( $dat =~ /not found$/ ) {
					print "host $host: command $opt_c returned $dat".$NL;
					$OUTPUT{$host} =$FS."host $host: command $opt_c returned $dat".$FE.$NL;
					return;
				}
			}
		} else {
			print "$method Connection To $host as $login: $dat!",$NL;
		}
	}

	$OUTPUT{$host} = $dat;
	$num_hosts++;
}
