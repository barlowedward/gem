/* Copyright (c) 1994 by Edward Barlow */

#ifndef _SYB_LIB_
#define _SYB_LIB_   1
#include <stdio.h>
#define MAPTABLENUMROWS         10
typedef struct
{
        int    datatype;
        int    programtype;
} MAPROW;
typedef MAPROW MAPTABLE[MAPTABLENUMROWS];

static MAPTABLE MapTable = {
        {SYBCHAR,      NTBSTRINGBIND },
        {SYBMONEY,     FLT8BIND      },
        {SYBFLT8,      FLT8BIND      },
        {SYBINT4,      INTBIND       },
        {SYBDATETIME,  DATETIMEBIND  },
        {SYBINT2,      SMALLBIND     },
        {SYBINT1,      TINYBIND      },
        {SYBTEXT,      NTBSTRINGBIND },
        {SYBBINARY,    BINARYBIND    },
        {SYBBIT,       BITBIND       }
};
/*
** FIELDLENGTH TABLE SPECIFIES THE LENGTH OF FIELDS GENERATED
**      IN CONVERTING SYBASE DATA TYPES TO PROGRAM DATA TYPES
**      POSITIVE NUMBERS INDICATE ABSOLUTE LENGTH
**      NEGATIVE NUMBERS INDICATE ADDITIONAL LENGTH
*/

#define FIELDLENGTHTABLENUMROWS   13
typedef struct
{
        int    programtype;
        int    length;
        int    alignment;
} FIELDLENGTHROW;
typedef FIELDLENGTHROW FIELDLENGTHTABLE[FIELDLENGTHTABLENUMROWS];

static FIELDLENGTHTABLE FieldLengthTable = {
        {CHARBIND,        0, 1},
        {STRINGBIND,     -1, 1},
        {NTBSTRINGBIND,  -1, 1},
        {VARYCHARBIND,   -2, 2},
        {BINARYBIND,      1, 1},
        {VARYBINBIND,    -2, 2},
        {TINYBIND,        1, 1},
        {SMALLBIND,       2, 2},
        {INTBIND,         4, 4},
        {FLT8BIND,        8, 8},
        {BITBIND,         1, 1},
        {DATETIMEBIND,    8, 4},
        {MONEYBIND,       8, 4}
};


/* DBInit() - Initialize Sybase */
extern void DBInit();
FILE *Debug(int debug_level);
int Row_Results( DBPROCESS *dbproc, void *inrow, int MaxNumRows,int DataSiz, int   RowSize, void *ReturnData, int  *NumRows);
int MapDataType ( int);
int AlignOffset( int, int);
int GetFieldLength ( int,int );




/* ConnectToServer() - return dbprocess connection */
int ConnectToServer(char *, char *, char *,char *, char *,DBPROCESS **);

/* RunQuery() - main query processor */
int RunQuery(DBPROCESS * , char *,int (*UserFunction)(DBPROCESS *,void *,int, int), int, int ,void *, int  *, FILE  *);

/* store_results - store results from select in one srver into another */
extern int store_results(DBPROCESS  *, DBPROCESS  *, char *, char * , int , int (*UserFunction)(DBPROCESS *,void *,int, int) );

/* Print results of query */
#define QueryPrint(dbproc,query,fp) RunQuery(dbproc,query,NULL,10000,0,NULL,NULL,fp)

/* Run Function on each row returned from query */
#define QueryFunc(dbproc,query,func) RunQuery(dbproc,query,func,10000,0,NULL,NULL,NULL)

/* Store results of query into array */
#define QueryStore(dbproc,sqltext,MaxNumRows,RowSize,ReturnData,NumRows) RunQuery(dbproc,sqltext,NULL,MaxNumRows,RowSize,ReturnData,NumRows,NULL)

/* Run query */
#define QueryRun(dbproc,query)  RunQuery(dbproc,query,NULL,0,NULL,NULL,NULL,NULL)

#define     NORMAL               1
#define syb_max(x,y)                    ( (x)>=(y) ? (x) : (y) )
#define syb_min(x,y)                    ( (x)<=(y) ? (x) : (y) )

#define MAXCOLUMNS      30
#define TEXTSIZE        255
#endif
