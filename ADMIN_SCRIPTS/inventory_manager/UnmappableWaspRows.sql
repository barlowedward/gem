declare @x datetime
select @x=getdate()
select distinct
wasp_site = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Site'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_location = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_floor = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Floor'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_row = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Row'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_cabinet = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Cabinet'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type )
into 	 #barlow_place_map
from 	gemalarms..INV_system s
where 	s.system_type ='WASP'

delete #barlow_place_map
from #barlow_place_map b, barlow_place_map m
where b.wasp_floor 	= m.wasp_floor
and b.wasp_location = m.wasp_location
and b.wasp_site		= m.wasp_site
and b.wasp_row 		= m.wasp_row
and b.wasp_cabinet  = m.wasp_cabinet

select * from #barlow_place_map order by wasp_site,wasp_location, wasp_floor, wasp_row, wasp_cabinet

drop table #barlow_place_map

select datediff(ss, @x, getdate()
