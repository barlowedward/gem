#!/usr/local/bin/perl -w
use Tk;
use strict;
use Data::Dumper;

my $mw = MainWindow->new;
$mw->scaling(2) if $mw->scaling() > 2;
$mw->geometry( '300x100' );
#print "SCALING: ",$mw->scaling(),"\n";
#$mw->scaling(1);

# START FONT SELECTION
my(@fontfamilies)=sort $mw->fontFamilies();
my($family);
foreach( @fontfamilies ) {	# remember this is alpha order
	if( lc($_) eq 'arial' ) {
		$family=$_; last;
	}
	if( lc($_) eq 'helvetica' ) {
		$family=$_; last;
	}		
	if( lc($_) eq 'symbol' ) {
		$family=$_; last;
	}		
}
$mw->fontCreate('header', 	-family=>$family, 	-size=>'12', -weight=>'bold');
$mw->fontCreate('large', 	-family=>$family, 	-size=>'12' );
$mw->fontCreate('bold', 	-family=>$family, 	-size=>'10', -weight=>'bold');
$mw->fontCreate('code', 	-family=>'Courier new', 	-size=>'10');
$mw->fontCreate('norm', 	-family=>$family, 	-size=>'10');
$mw->fontCreate('small', 	-family=>$family, 	-size=>'9');
$mw->fontCreate('smbold', 	-family=>$family, 	-size=>'9', -weight=>'bold');
$mw->fontCreate('tiny', 	-family=>$family, 	-size=>'6');

my(@fonts)=('header','large','bold','code','norm','small','smbold','tiny');
$mw->scaling(2) if $mw->scaling() > 2;

print "FAMILIES: ",join("\n\t",@fontfamilies),"\n";
print "FAMILY USED IS $family\n";
print "SCALING: ",$mw->scaling(),"\n";
#$mw->scaling(1.5);

my $l = $mw->Label( -text => 'Hello world' )->pack;
#while( my $font = <DATA> ) {
#	chomp $font;
#   push @fonts,$font;
#}

foreach my $font (@fonts) {
	  my(%attrs)=$mw->fontActual($font);
	  print $font,":\t";
	  
	  printf "Actual Family %s Size %s Weight %s Slant %s\n", $attrs{-family}, $attrs{-size}, $attrs{-weight}, $attrs{-slant};
     $l->configure( -font => $font );     
     $l->idletasks;
     $l->after( 1000 );

}


__DATA__
-*-symbol-medium-r-normal--0-0-100-100-p-0-adobe-fontspecific
-adobe-symbol-medium-r-normal--0-0-100-100-p-0-adobe-fontspecific
-adobe-symbol-medium-r-normal--0-0-75-75-p-0-adobe-fontspecific
-adobe-symbol-medium-r-normal--10-100-75-75-p-61-adobe-fontspecific
-adobe-symbol-medium-r-normal--11-80-100-100-p-61-adobe-fontspecific
-adobe-symbol-medium-r-normal--12-120-75-75-p-74-adobe-fontspecific
-adobe-symbol-medium-r-normal--14-100-100-100-p-85-adobe-fontspecific
-adobe-symbol-medium-r-normal--14-140-75-75-p-85-adobe-fontspecific
-adobe-symbol-medium-r-normal--17-120-100-100-p-95-adobe-fontspecific
-adobe-symbol-medium-r-normal--18-180-75-75-p-107-adobe-fontspecific
-adobe-symbol-medium-r-normal--20-140-100-100-p-107-adobe-fontspecific
-adobe-symbol-medium-r-normal--24-240-75-75-p-142-adobe-fontspecific
-adobe-symbol-medium-r-normal--25-180-100-100-p-142-adobe-fontspecific
-adobe-symbol-medium-r-normal--34-240-100-100-p-191-adobe-fontspecific
-adobe-symbol-medium-r-normal--8-80-75-75-p-51-adobe-fontspecific 
