#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use strict;
use Getopt::Std;
use File::Basename;
use Repository;
use MlpAlarm;

$| = 1;

sub usage
{
	print @_;
	print "parse_schedlgu.pl -FFile -DDir -hd -OFile -BBatchDesignator\n";
	exit 1;
}

#usage("must pass filename\n") unless defined $ARGV[0];
use vars qw($opt_B $opt_F $opt_D $opt_h $opt_d $opt_O);

#usage() if $#ARGV<0;
usage("Bad Parameter List\n") unless getopts('O:F:D:hdB:');


sub error_out {
   my($msg)=@_;
   $msg = "BATCH ".$opt_B.": ".$msg if $opt_B;
   MlpBatchJobErr($msg) if $opt_B;
   MlpEvent(
      -monitor_program=> $opt_B || "parse_a_file.pl",
      -system   => $opt_B || "",
      -message_text=> $msg,
      -severity => "ERROR"
   ) if $opt_B;
   die $msg;
}

my(@files);
if( defined $opt_F ) {
	foreach ( split(/,/,$opt_F) ) {
		if( -r $opt_F ) {
			push @files,$_;
		} elsif( -r get_gem_root_dir()."/data/system_information_data/schedlgu/$opt_F" ) {
			push @files, get_gem_root_dir()."/data/system_information_data/schedlgu/$opt_F";
		} elsif( -r get_gem_root_dir()."/data/system_information_data/schedlgu/".$opt_F."_Win32_Job_Scheduler_Log" ) {
			push @files, get_gem_root_dir()."/data/system_information_data/schedlgu/".$opt_F."_Win32_Job_Scheduler_Log";
		}
	}
	if( $opt_d ) { foreach (@files) { print "Files: $_\n"; } }
} else {
	my($dir) = $opt_D || get_gem_root_dir()."/data/system_information_data/schedlgu";
	error_out( "Cant find $dir - try passing it\n")  unless -d $dir;
	@files=grep(/Job_Scheduler_Log/i,dirlist($dir));
}

if( defined $opt_O ) {
	open(OUTFILE,">$opt_O") or error_out( "Cant write Outfile $opt_O\n" );
}

sub myprint
{
	if( defined $opt_O) {
		print OUTFILE @_;
	} else {
		print @_;
	}
}

#$opt_B = "RptSchedLogs" unless $opt_B;
MlpBatchJobStart(-BATCH_ID=>$opt_B ) if $opt_B;

myprint( "<h1>" ) if $opt_h;
myprint( "Win32 Scheduler Log Report\n" );
myprint( "</h1>" ) if $opt_h;
myprint( "parse_schedlgu.pl Version 1.0\n" );
myprint( "<br>" ) if $opt_h;
myprint( "Printed at ".localtime(time)."\n" );
myprint( "<br>" ) if $opt_h;

myprint( "<table border=1>\n" ) if $opt_h;

foreach my $fname (sort @files ) {
	my(%history);
	my($curdat);

	print "Processing $fname\n" if defined $opt_d;
	open(X,$fname);
	my($doesntcount)="FALSE";
	while( <X> ) {
		chomp;
		s/\0//g;
		chomp;
		s/[^\s\\\/\w\.\'\*\-\:\"\;\(\)]//g;	# explicitly no #
		s/\s+$//;
		next if $_ eq "";
		print ">>>",$_,"\n" if $opt_d;
		if( /Most recent entry is above this line/ ) {
			$doesntcount="TRUE";	# now only get the default
			next;
		}

		if( /^\s/ ) {
			s/^\s+//;
			#push @curbatchlines, $_;
			if( /Result/ and /an exit code/ ) {
				m/\((\d+)\)\./;
				$$curdat{ExitCode}=$1 if $doesntcount eq "FALSE" or ! defined $$curdat{ExitCode};
				print "(dbg) Exitcode = $1\n"  	if defined $opt_d;
			} elsif( /^Started/ ) {
				s/^Started\s+//;
				s/^at\s//;	# optional...
				print "(dbg) S=$_\n"	if defined $opt_d;
				$$curdat{StartTime}=$_ if $doesntcount eq "FALSE" or ! defined $$curdat{StartTime};
			} elsif( /^Finished/ or /^Exited at/ ) {
				s/^Finished\s+//;
				s/^Exited at\s+//;
				print "(dbg) F=$_\n"  		if defined $opt_d;
				$$curdat{EndTime}=$_ if $doesntcount eq "FALSE"
											or ! defined $$curdat{EndTime};
			} else {
				print "(dbg) Text=$_\n" 	if defined $opt_d;
				$$curdat{Text}=$_  if $doesntcount eq "FALSE"
											or ! defined $$curdat{Text};
			}
		} else {
			print "PARSING HEADER LINE $_\n"  if defined $opt_d;
			s/^[\d\.]+//;
			s/^\s+//;
			s/\s+$//;
			#my($j,$t,$err)=split(/\s+/,$_,3);
			next if $_ eq "";
			my($j,$t,$err);
			# job is between quotes.
			if( m/^"(.+)"\s*\((.*)\)\s*(.*)$/ ) {;
				$j=$1; $t=$2; $err=$3;
			} elsif( m/^"(.+)"\s*$/ ) {;
				$j=$1; $t=""; $err="";
			}
			print "Cant parse '".$_."'" unless defined $j;
			next unless defined $j;
			# error_out( "Cant parse $_ " ) unless defined $j;
			$err="" unless defined $err;
			next if $j eq "";

			if( ! defined $history{$j} ) {
				print "(dbg) NEW JOB $j\n"  if defined $opt_d;
				print "(dbg) NEW JOB t=$t\n"  if defined $opt_d;
				print "(dbg) NEW JOB err=$err\n"  if defined $opt_d;
				my(%c);
				$history{$j}=\%c;
				$curdat=\%c;
				#$$curdat{Fname}		= $fname;
			} else {
				$curdat  = $history{ $j };
			}
			$$curdat{JobDesc} =$t
					if $t ne ""
					and ($doesntcount eq "FALSE" or ! defined $$curdat{JobDesc} );
			$$curdat{Err}		=$err;
		}
	}

	if( defined $opt_d ) {
		foreach my $h ( sort keys %history ) {
			my($curdat)=$history{$h};
			foreach ( keys %$curdat ) {
				print "--> H=$h Key=$_ val=$$curdat{$_}\n";
			}
		}
	}

	my($server)=basename($fname);
	$server=~s/_Win32_Job_Scheduler_Log//;

	if( $opt_h ) {
		myprint( "<TR>\n\t<TH COLSPAN=6 BGCOLOR=\"white\">",basename($fname),"</TH>\n</TR>\n" );
		myprint( "<TR>" );
		foreach (qw(JobName ExitCode StartTime EndTime Error JobDesc)) {
			myprint( "<TH>$_</TH>\n" );
		}
		myprint( "</TR>\n" );
	} else {
		myprint( "\n\nFILENAME=",basename($fname),"\n\n" );
	}

	myprint "NOT ALARMING - -B FLAG not Defined\n" unless $opt_B;

	my($numrows)=0;
	foreach my $h ( sort keys %history ) {
		next if $h eq "Task Scheduler Service";
		my($curdat)=$history{$h};
		next if $$curdat{ JobDesc } eq "pmpushWinInstaller_main.exe"
				or $$curdat{ JobDesc } eq "pushInstaller_main.exe";

		my($color)="";
		$color="BGCOLOR=PINK" if defined $$curdat{Text};# or defined $$curdat{Err};
		my($server_state)="OK";
		$server_state="WARNING" if  defined $$curdat{Text};
		$server_state="ERROR" if  defined $$curdat{Text} and $h=~/^gem/i;

		my($msg)=$$curdat{Err}."\n" if defined $$curdat{Err};
		if( $$curdat{Text} ) {
			$msg.=$$curdat{Text};
		} else {
			$msg.="No Message Text";
		}
		$msg.="   \nDescription: ".$$curdat{JobDesc} if defined $$curdat{JobDesc};
		print "DBG Heartbeat($opt_B,$server,$h,$server_state,$msg)\n";
		MlpHeartbeat(
			-monitor_program=>$opt_B,
			-system=>$server,
			-subsystem=>$h,
			# -debug=>$opt_d,
			-state=>$server_state,
			-message_text=>$msg
		) if $opt_B;

		myprint( "<TR $color>\n<TD>" ) if $opt_h;
		myprint( $h );
		myprint( "," ) unless $opt_h;
		myprint( "</TD><TD>" ) if $opt_h;
		foreach (qw(ExitCode StartTime EndTime Err JobDesc)) {
			myprint( "$$curdat{ $_ }" );
			if( $_ eq "Err" ) {
				if( $$curdat{ $_ } eq "" ) {
					myprint( "&nbsp;" ) if ! defined $$curdat{Text}
									or $$curdat{Text} eq "";
				} else {
					myprint( "<br>" )  ;
				}
				myprint( $$curdat{ Text } );
			}

			myprint( "," ) unless $opt_h;
			myprint( "</TD>\n<TD>" ) if $opt_h;
		}
		myprint( "</TR>\n" ) if $opt_h;
		myprint( "\n" );

		#myprint( "KEY=$h\n";
		#foreach (keys %$curdat) {
		#	myprint( "\t$_ => $$curdat{ $_ } \n";
		#}
	}

	if( defined $opt_d ) {
		use Data::Dumper;
		myprint( Dumper \%history );
	}
}
myprint( "</table>\n" ) if $opt_h;
MlpBatchJobEnd();

close(X);
exit(0);

sub dirlist
{
        my($dir,$recurse) = @_;
        opendir(DIR,$dir) || error_out("Can't open directory $dir for reading\n");
        my(@files)=readdir(DIR);
        closedir(DIR);

        my(@out);
        foreach (@files) {
               next if /^\./;
					next if /^install.sh$/ or /^weather$/;
               if( -d $dir."/".$_ and $recurse ) {
                        push @out,dirlist($dir."/".$_,1);
               } else {
                        push @out,$dir."/".$_;
               }
        }
        return @out;
}

1;

