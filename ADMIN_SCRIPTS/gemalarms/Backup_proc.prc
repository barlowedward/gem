--
-- This procedure marks a backup process
--    basically this is a BackupState create or update driver
--
create procedure Backup_proc (
	@monitored_system_name	varchar(30),
	@internal_system_name	varchar(30),
	@dbname						varchar(30),

	@last_fulldump_time		datetime			=null,
	@last_fulldump_file		varchar(127)	=null,
	@last_fulldump_lsn		varchar(20)		=null,

	@last_trandump_time		datetime			=null,
	@last_trandump_file		varchar(127)	=null,
	@last_trandump_lsn		varchar(20)		=null,

	@last_truncation_time	datetime			=null,

	@last_fullload_time		datetime			=null,
	@last_fullload_file		varchar(127)	=null,
	@last_fullload_lsn		varchar(20)		=null,
	@last_fullload_int_sysnm	varchar(30)		=null,

	@last_tranload_time		datetime			=null,
	@last_tranload_file		varchar(127)	=null,
	@last_tranload_lsn		varchar(20)		=null,
	@last_tranload_filetime	varchar(20)		=null,
	@last_tranload_int_sysnm	varchar(30)		=null,

	@is_system_db				char(1)        =null,
   @is_useable					char(1)			=null,
	@is_tran_truncated		char(1)			=null,

	@is_readonly				char(1)			=null,
	@is_select_into			char(1)			=null,
	@is_singleuser				char(1)			=null,
	@is_backupable				char(1)			=null,

	@is_deleted					char(1)			=null,
	@requires_backup			char(1)			=null,

	@max_backup_days			int				=null,
	@max_trandump_min			int				=null,
	@max_tranload_min			int				=null
)
as
begin

-- Handle New Rows Appropriately
	if @last_fullload_int_sysnm is null
	begin
		select 	@last_fullload_int_sysnm = last_fullload_int_sysnm
		from 		BackupState
		where 	last_fullload_int_sysnm is not null
		and   	monitored_system_name = @monitored_system_name
	end

	if not exists ( select 1 from BackupState where monitored_system_name=@monitored_system_name and dbname=@dbname )
	begin
			-- check if there is a database that is semantically equal
			if exists ( select 1 from BackupState where monitored_system_name=@monitored_system_name and upper(dbname)=upper(@dbname) )
			begin
					select @dbname=dbname
					from BackupState where monitored_system_name=@monitored_system_name and upper(dbname)=upper(@dbname)
			end
	end

	if not exists ( select 1 from BackupState
		where monitored_system_name=@monitored_system_name and dbname=@dbname )
			-- and isnull(internal_system_name,@internal_system_name)=@internal_system_name )
	   INSERT BackupState
		VALUES (
			@monitored_system_name	,
			@internal_system_name	,
			@dbname						,

			@last_fulldump_time		,
			@last_fulldump_file		,
			@last_fulldump_lsn		,
			null,

			@last_trandump_time		,
			@last_trandump_file		,
			@last_trandump_lsn		,
			null,

			@last_truncation_time	,

			@last_fullload_time		,
			@last_fullload_file		,
			@last_fullload_lsn		,
			@last_fullload_int_sysnm,
			null,

			@last_tranload_time		,
			@last_tranload_file		,
			@last_tranload_lsn		,
			@last_tranload_filetime ,
			null,
			@last_tranload_int_sysnm,

			getdate(),

			@is_system_db				,
			@is_useable					,
			@is_tran_truncated		,

			@is_readonly				,
			@is_select_into			,
			@is_singleuser				,
			@is_backupable				,
			@is_deleted					,
			@requires_backup			,
			getdate()					,
			null,
			null,
			null
	  )
	else

		UPDATE 	BackupState
		set
			internal_system_name		= isnull( @internal_system_name, internal_system_name),
			max_backup_days			= isnull( @max_backup_days, max_backup_days),
			max_trandump_min			= isnull( @max_trandump_min, max_trandump_min),
			max_tranload_min			= isnull( @max_tranload_min, max_tranload_min),

			last_fulldump_time		= isnull(@last_fulldump_time		,last_fulldump_time),
			last_fulldump_file		= isnull(@last_fulldump_file		,last_fulldump_file),
			last_fulldump_lsn 		= isnull(@last_fulldump_lsn		,last_fulldump_lsn),
			last_fulldump_data_date = case when @last_fulldump_time > last_fulldump_time then getdate()
													 when @last_fulldump_file is not null then getdate()
													 when @last_fulldump_lsn is not null then getdate()
													 else last_fulldump_data_date
											  end,
			last_trandump_time		= isnull(@last_trandump_time		,last_trandump_time),
			last_trandump_file		= isnull(@last_trandump_file		,last_trandump_file),
			last_trandump_lsn			= isnull(@last_trandump_lsn		,last_trandump_lsn),
			last_trandump_data_date = case when @last_trandump_time > last_trandump_time then getdate()
													 when @last_trandump_file is not null then getdate()
													 when @last_trandump_lsn is not null then getdate()
													 else last_trandump_data_date
											  end,
			last_truncation_time		= case when @last_truncation_time is null then last_truncation_time
													 when last_truncation_time is null then @last_truncation_time
													 when @last_truncation_time > last_truncation_time then @last_truncation_time
													 else last_truncation_time
										     end,
			last_fullload_time		= case when @last_fullload_time is null then last_fullload_time
													 when last_fullload_time is null then @last_fullload_time
													 when @last_fullload_time > last_fullload_time then @last_fullload_time
													 else last_fullload_time
										     end,
			last_fullload_file		= isnull(@last_fullload_file		,last_fullload_file),
			last_fullload_lsn			= isnull(@last_fullload_lsn		,last_fullload_lsn),

			last_fullload_data_date = case when @last_fullload_time is not null	then getdate()
													 when @last_fullload_file is not null 	then getdate()
													 when @last_fullload_lsn is not null 	then getdate()
													 else last_fullload_data_date
											  end,

			last_tranload_time		= case when @last_tranload_time is null then last_tranload_time
													 when last_tranload_time is null then @last_tranload_time
													 when @last_tranload_time > last_tranload_time then @last_tranload_time
													 else last_tranload_time
										     end,
			last_tranload_file		= isnull(@last_tranload_file		,last_tranload_file),
			last_tranload_lsn			= isnull(@last_tranload_lsn		,last_tranload_lsn),
			last_tranload_filetime  = case when @last_tranload_filetime is null then last_tranload_filetime
													 when last_tranload_filetime is null then @last_tranload_filetime
													 when @last_tranload_filetime > last_tranload_filetime then @last_tranload_filetime
													 else last_tranload_filetime
										     end,
			last_tranload_data_date = case when @last_tranload_time is not null	then getdate()
													 when @last_tranload_file is not null 	then getdate()
													 when @last_tranload_lsn is not null 	then getdate()
													 else last_tranload_data_date
											  end,

			last_fullload_int_sysnm = isnull(@last_fullload_int_sysnm,last_fullload_int_sysnm),
			last_tranload_int_sysnm	= isnull(@last_tranload_int_sysnm,last_tranload_int_sysnm),
			is_system_db				= isnull(@is_system_db			,is_system_db),
			is_useable					= isnull(@is_useable				,is_useable),
			is_tran_truncated  		= isnull(@is_tran_truncated	,is_tran_truncated),

			is_readonly					= isnull(@is_readonly			,is_readonly),
			is_select_into				= isnull(@is_select_into		,is_select_into),
			is_singleuser				= isnull(@is_singleuser			,is_singleuser),
			is_backupable				= isnull(@is_backupable			,is_backupable),

			-- file_time  					= isnull(@file_time		,file_time),
			row_touch_time 			= getdate(),
			requires_backup			= isnull(@requires_backup			,requires_backup),
			last_static_data_date	= case 	when 	@is_system_db					is not null or
															   @is_useable						is not null or
																@is_tran_truncated       	is not null or

															   @is_readonly             	is not null or
															   @is_select_into          	is not null or
																@is_singleuser           	is not null or
																@is_backupable           	is not null 	then getdate()
														else last_static_data_date
												end

		where monitored_system_name=@monitored_system_name and dbname=@dbname
		-- and internal_system_name=@internal_system_name

update Heartbeat
set 	state='OK'
where monitor_program='HourlyBackupReport'
and 	state='ERROR'
and (( datediff(hh,@last_fulldump_time,getdate())<24 and message_text like 'Production DB%' ) or
        ( datediff(hh,@last_tranload_time,getdate())<24 and message_text like 'LAST TRANLOAD%' ))
and system=@monitored_system_name and subsystem = @dbname+ ' Backup Checker'

end
go
