#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# copyright (c) 2005-2011 by SQL Technologies.  All Rights Reserved.
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com

use strict;
use File::Basename;
use Repository;
use Carp qw(confess cluck);
use Getopt::Long;
use GemReport;
use Do_Time;
use LogFunc;
use Time::Local;
use MlpAlarm;

my($VERSION)="2.0";

#my($mp)="Sybase_Backup_Log";
#my($mp)="Sybase_ASE_Log";
#my($mp)="Sybase_Replication_Log ";
					
sub usage {
	print @_,"process_log_files.pl --FILETYPE=FILE_TYPE 

Required: -FILETYPE=FILE TYPE
      ASE_BACKUP_SERVER 
      ASE_SERVER_LOG
      ASE_HISTORICAL_SERVER 
      ASE_MONITOR_SERVER
      REP_SERVER 
      ENVIRONMENT_FILE
      INTERFACES_FILE
      
      Default=ALL
      
      -PATFILE=paternfile patern files allows restart for logging
      \n";

 	exit(1);
}

$|=1;

use vars qw($FILETYPE $DEBUG $BATCHID $GEMREPORT $SAVE_TO_ALARMDB $HTML $TODAYYESTERDAYONLY $PATFILE $NOERRORFILTER);

GetOptions(
	"FILETYPE=s"				=>\$FILETYPE,		
	"BATCHID=s"					=>\$BATCHID,		
	# "HOSTNAME=s"				=>\$HOSTNAME,		
	"BATCH_ID=s"				=>\$BATCHID,
	"GEMREPORT"					=>\$GEMREPORT,		
	"PATFILE=s"					=>\$PATFILE,		
	"DEBUG"						=>\$DEBUG,
	"NOERRORFILTER"			=>\$NOERRORFILTER,
	"TODAYYESTERDAYONLY"		=>\$TODAYYESTERDAYONLY,
	"HTML"						=>\$HTML,
	"SAVE_TO_ALARMDB"			=>\$SAVE_TO_ALARMDB
	)	or usage( "syntax error\n" );

my($nl)="\n";
$nl="<br>\n" if $HTML;

my(%server_filetimes);		# time a file was read by server
my(%filetime2);
my( %keylist);
my(%rep_dat);

sub error_out {
   my($msg)=join("",@_);
   $msg = "BATCH ".$BATCHID.": ".$msg if $BATCHID;
   MlpEvent(-monitor_program=>$BATCHID, -system=>$BATCHID, -message_text=>$msg, -severity =>"ERROR") if $BATCHID and $SAVE_TO_ALARMDB;
   MlpBatchJobErr( $msg ) if $BATCHID;
   die $msg;
}

my($root_dir)=get_gem_root_dir()."/data/system_information_data/";
my($excl_dir)=get_gem_root_dir()."/ADMIN_SCRIPTS/bin";
die "Woah... no root directory $root_dir"	unless -d $root_dir;

report_init(-file_name=>$BATCHID,-debug=>$DEBUG);

#if( $GEMREPORT ) {
#	my($outfile) = get_gem_root_dir();
#	
#	if($BATCHID) {
#		$outfile .= "/data/html_output/".$BATCHID.".html";
#	} else {
#		$outfile .= "/data/html_output/".$FILETYPE.".html";
#	}
#	open(OUT,">".$outfile ) or die "CANT write File $outfile $!\n";
#	print "GEM REPORT FILE is $outfile\n",$nl;
#}

if( $FILETYPE ) {
	usage "Invalid File Type\n" 
		unless $FILETYPE eq "ASE_BACKUP_SERVER"
      or $FILETYPE eq "ASE_SERVER_LOG"
      or $FILETYPE eq "ASE_HISTORICAL_SERVER"
      or $FILETYPE eq "MONITOR_SERVER"
      or $FILETYPE eq "REP_SERVER" 
      or $FILETYPE eq "ENVIRONMENT_FILE"
      or $FILETYPE eq "INTERFACES_FILE";      
} else {
	usage "Must Pass File Type\n";
}
   
report_print( "process_log_files.pl Version $VERSION",$nl );
report_print( "Called In DEBUG mode",$nl ) if $DEBUG;
report_print( "Batch Id $BATCHID",$nl ) if $BATCHID;
report_print( "No Batch Id ",$nl ) unless $BATCHID;
report_print( "Run At ".localtime(time).$nl );
report_print( "File Type is $FILETYPE",$nl ) if $FILETYPE;
	
MlpBatchJobStart(-BATCH_ID => $BATCHID ) if $BATCHID;

my($dat);
$dat=get_logfiles(-filetype=>'sybase');
if( ! $dat ) {
	print STDERR "no discovered files \n";
	exit(0);
}

#use Data::Dumper;
#print Dumper \$dat;
my %x =%{$dat};
my($number_of_processed_files)=0;
my(@errors);
foreach my $keyname ( keys %x ) {
	my $ref=$$dat{$keyname};
	
	if( $ref->{FETCHFILE} ne "YES" ) {
		print "  Skipping:  $keyname\n" if $DEBUG;
		next;
	}
	if( $FILETYPE and $FILETYPE ne $ref->{File_Type} ) {
      print "Skipping:  $keyname\n" if $DEBUG;
      next;
   }
	
	my($hostname,$file)=split(/:/,$keyname);
	my($path_to_local)=$root_dir."/".$hostname."/".basename($file);	
	
	if( ! -r $path_to_local ) {
		print "NotExist: $keyname\n" if $DEBUG;
		next;
	}
	
	if( $FILETYPE eq "ASE_BACKUP_SERVER" ) {
		read_syb_bkup_svr_logfile($path_to_local, $ref->{Server_Name}, $hostname);	
	}
	if( $FILETYPE eq "ASE_SERVER_LOG" ) {
		read_syb_ase_logfile($path_to_local,$ref->{Server_Name});	
	}
	if( $FILETYPE eq "REP_SERVER" ) {
		my( @dat ) = read_syb_rep_server_logfile($path_to_local,$ref->{Server_Name});	   
		$rep_dat{$ref->{Server_Name}} = \@dat if $#dat>=0;
	}
}

print_syb_bkup_svr_rpt() if $FILETYPE eq "ASE_BACKUP_SERVER";
print_syb_rep_svr_rpt()  if $FILETYPE eq "REP_SERVER";

foreach (@errors) {
	print "ERROR: $_\n";
}
report_close();

exit(0);

#sub report_print {
#	print @_;
#	print OUT @_ if $GEMREPORT;
#}

my( 	%V_BACKUP_TIME, %V_BACKUP_FILE, 	%V_TRANDUMP_TIME, %V_TRANDUMP_FILE,
	%V_RESTORE_TIME, %V_RESTORE_FILE, 	%V_TRANLOAD_TIME, %V_TRANLOAD_FILE, %V_TRANLOAD_FILETIME ); 	# by "server db"


sub print_syb_bkup_row_header {
	my($srvr,$backupage)=@_;
	if( defined $HTML ) {
# # # Colspan=2

		report_print( "<TR>	<TH BGCOLOR=#FFCCFF>Server</TH>
				<TH BGCOLOR=#FFCCFF>File Time</TH>
				<TH BGCOLOR=#FFCCFF>Database</TH>
				<TH BGCOLOR=#FFCCFF >Last Dump</TH>
				<TH BGCOLOR=#FFCCFF >Last Tran Dump</TH>
				<TH BGCOLOR=#FFCCFF >Last Load</TH>
				<TH BGCOLOR=#FFCCFF >Last Tran Load</TH></TR>\n" );
	} else {
		report_print("SERVER=$srvr\nBACKUP FILE FETCH AGE=$backupage\n");
		report_print( sprintf( "%20.20s %17.17s %17.17s %17.17s\n",
			"Database","Last Dump","Last Tran Dump","Last Load","Last Tran Load"));
	}
}
sub print_syb_bkup_svr_rpt {
	print "Running print_syb_bkup_svr_rpt()",$nl if $DEBUG;
	
	my($last_server)="";
	report_print( $nl );

	report_print( "<TABLE CELLPADDING=2 CELLSPACING=2 BORDER=1>" ) if $HTML;

	foreach ( sort keys %keylist ) {
		my($srvr,$db)=split;
		next if $db eq "model" or $db eq "tempdb";
		
    	my $last_operation_days=1000;

#		if(defined $LAST_LOAD{$_} and $LAST_LOAD{$_} != 0 ) {
#			$ll =  do_time(-time=>$LAST_LOAD{$_},-fmt=>"mm/dd/yyyy hh:mi:ss");
#
#			$lldays=int((time - $LAST_LOAD{$_})/(24*60*60));
#			$last_operation_days=$lldays;
#			if( $lldays == 0 ) {
#				$lldays="(today)";
#			} else {
#				$lldays="(".$lldays." days)";
#			}
#		}
#
#		if(defined $LAST_DUMP{$_} and $LAST_DUMP{$_} != 0 ) {
#			$ld =do_time(-time=>$LAST_DUMP{$_},-fmt=>"mm/dd/yyyy hh:mi:ss");
#
#			$days = int((time - $LAST_DUMP{$_})/(24*60*60));
#			$last_operation_days=$days if $last_operation_days < $days;
#			if( $days == 0 ) {
#				$days="(today)";
#			} else {
#				$days="(".$days." days)";
#			}
#		}

		# ignore if > 60 days not used record
		#next if ! defined $realdb and $last_operation_days > 60;

		if( $srvr ne $last_server ) {
			report_print( "\n" ) unless defined $HTML;
			report_print( "<TR><TD COLSPAN=6>&nbsp;</TD></TR>\n" ) if defined $HTML and $last_server ne "";
			print_syb_bkup_row_header($srvr,$server_filetimes{$srvr});			
			$last_server=$srvr;
		}

		my($bt,$tt,$rt,$lt,$days,$bt2,$tt2,$rt2,$lt2);
		$bt2=$tt2=$rt2=$lt2=" ";
		if( $V_BACKUP_TIME{$_}>0 ) {
			$bt=  do_time(-time=>$V_BACKUP_TIME{$_},-fmt=>"mm/dd/yyyy hh:mi");
			$days=int((time - $V_BACKUP_TIME{$_})/(24*60*60));
			$bt2="(yesterday)" if $days<2;
			$bt2="(today)" if $days<1;
			$last_operation_days=$V_BACKUP_TIME{$_} if $last_operation_days<=$V_BACKUP_TIME{$_};
		} else {
			$bt=" ";
		}

		if( $V_TRANDUMP_TIME{$_}>0 ) {
			$tt=  do_time(-time=>$V_TRANDUMP_TIME{$_},-fmt=>"mm/dd/yyyy hh:mi");
			$days=int((time - $V_TRANDUMP_TIME{$_})/(24*60*60));
			$tt2="(yesterday)" if $days<2;
			$tt2="(today)" if $days<1;
			$last_operation_days=$V_TRANDUMP_TIME{$_} if $last_operation_days<=$V_TRANDUMP_TIME{$_};
		} else {
			$tt=" ";
		}

		if( $V_RESTORE_TIME{$_}>0 ) {
			$rt=  do_time(-time=>$V_RESTORE_TIME{$_},-fmt=>"mm/dd/yyyy hh:mi");
			$days=int((time - $V_RESTORE_TIME{$_})/(24*60*60));
			#$rt="(yesterday)" if $days<2;
			#$rt="(today)" if $days<1;
			$rt2="(yesterday)" if $days<2;
			$rt2="(today)" if $days<1;
			$last_operation_days=$V_RESTORE_TIME{$_} if $last_operation_days<=$V_RESTORE_TIME{$_};
		} else {
			$rt=" ";
		}

		if( $V_TRANLOAD_TIME{$_}>0 ) {
			$lt=  do_time(-time=>$V_TRANLOAD_TIME{$_},-fmt=>"mm/dd/yyyy hh:mi");
			$days=int((time - $V_TRANLOAD_TIME{$_})/(24*60*60));
			#$lt="(yesterday)" if $days<2;
			#$lt="(today)" if $days<1;
			$lt2="(yesterday)" if $days<2;
			$lt2="(today)" if $days<1;
			$last_operation_days=$V_TRANLOAD_TIME{$_} if $last_operation_days<=$V_TRANLOAD_TIME{$_};
		} else {
			$lt=" ";
		}

		if( defined $HTML ) {
			my($color);
#			#if( ! defined $realdb ) {
#			#	# not real so orange
#			#	$color="BGCOLOR=ORANGE"
#			#} els
#			if( $days eq "(today)" or $lldays eq "(today)" ) {
#				# dumped or loaded today
#				$color="BGCOLOR=BEIGE"
#			} else {
#				$color="BGCOLOR=PINK";
#			}
#			$ld="&nbsp;" 		if $ld eq "";
#			$days="&nbsp;" 	if $days eq "";
#			$lldays="&nbsp;" 	if $lldays eq "";
#			$ll = "&nbsp;" 	if $ll  eq  "";
#			report_print( "<TR $color>\n\t<TD>\n",$srvr,"</TD>\n\t<TD>",$db,"</TD>\n\t<TD>",$ld,"</TD>\n\t<TD>",$days,"</TD>\n\t<TD>",$ll,"</TD>\n\t<TD>",$lldays,"</TD></TR>\n" );
			$bt="&nbsp;" if $bt eq " ";
			$tt="&nbsp;" if $tt eq " ";
			$rt="&nbsp;" if $rt eq " ";
			$lt="&nbsp;" if $lt eq " ";
			$bt2="&nbsp;" if $bt2 eq " ";
			$tt2="&nbsp;" if $tt2 eq " ";
			$lt2="&nbsp;" if $lt2 eq " ";
			$rt2="&nbsp;" if $rt2 eq " ";

			if( time-$last_operation_days < 24*60*60 )     { $color="BGCOLOR=BEIGE"; }
			else{ $color="BGCOLOR=PINK"; }
			report_print( "<TR $color>\n\t<TD>\n",$srvr,
					"</TD>\n\t<TD>",$server_filetimes{$srvr},
					"</TD>\n\t<TD>",$db,
					"</TD>\n\t<TD>",$bt,' ',
					$bt2,
					"</TD>\n\t<TD>",$tt,' ',
					$tt2,
					"</TD>\n\t<TD>",$rt,' ',
					$rt2,
					"</TD>\n\t<TD>",$lt,' ',
					$lt2,
					"</TD>\n\t
				</TR>\n" );
		} else {
			report_print( sprintf( "%20.20s %17.17s %17.17s %17.17s\n",$db,$bt,$tt,$rt,$lt));
		}

		# save the info
		$V_BACKUP_TIME{$_} = undef	unless $V_BACKUP_TIME{$_};
		$V_TRANDUMP_TIME{$_} = undef	unless $V_TRANDUMP_TIME{$_};
		$V_RESTORE_TIME{$_} = undef	unless $V_RESTORE_TIME{$_};
		$V_TRANLOAD_TIME{$_} = undef 	unless $V_TRANLOAD_TIME{$_};


		$V_BACKUP_TIME{$_} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_BACKUP_TIME{$_})
			if $V_BACKUP_TIME{$_};
		$V_TRANDUMP_TIME{$_} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_TRANDUMP_TIME{$_})
			if $V_TRANDUMP_TIME{$_};
		$V_RESTORE_TIME{$_} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_RESTORE_TIME{$_})
			if $V_RESTORE_TIME{$_};
		$V_TRANLOAD_TIME{$_} = do_time(-fmt=>"mm/dd/yyyy hh:mi:ss",-time=>$V_TRANLOAD_TIME{$_})
			if $V_TRANLOAD_TIME{$_};

		#print "SVR=$srvr DB=$db \n";
		#print "SAVING -fuke     =>",$filetime2{$srvr},"\n";
		#print "SAVING -file_time=>", do_time_file($filetime2{$srvr}),"\n";
		# print "DBG: Running MlpSetBackupState()\n";
		print MlpSetBackupState(
			-system=>$srvr,
			-debug =>$DEBUG,
			-dbname=>$db,
			-last_fulldump_time => $V_BACKUP_TIME{$_},
			-last_fulldump_file => $V_BACKUP_FILE{$_},
			-last_trandump_time => $V_TRANDUMP_TIME{$_},
			-last_trandump_file => $V_TRANDUMP_FILE{$_},
			-last_fullload_time => $V_RESTORE_TIME{$_},
			-last_fullload_file => $V_RESTORE_FILE{$_},
			-last_tranload_time => $V_TRANLOAD_TIME{$_},
			-last_tranload_file => $V_TRANLOAD_FILE{$_},
			-last_tranload_filetime => $V_TRANLOAD_FILETIME{$_},
			-file_time => do_time_file($filetime2{$srvr})
			) if $BATCHID;

	}
	report_print( "</TABLE>"  ) if defined $HTML;
	print "Not Setting MlpSetBackupState() \n" if !$BATCHID and $DEBUG;		
}

sub read_syb_bkup_svr_logfile {
	my($file,$server,$hostnm)=@_;
	
	printf "read_syb_bkup_svr_logfile\n\tserver=%s\n\thost=%s\n\tfile=%s\n\tfileage=%-s)\n",
   	$server,
   	$hostnm,
   	$file,
   	do_diff_file($file) if $DEBUG;
   	
    $server_filetimes{$server}=do_diff_file($file) unless $server_filetimes{$server};
    $filetime2{$server} = $file;

	# report_print( "read_syb_bkup_svr_logfile: $file\n" );
	open( ERRLOG,$file ) or error_out( "Cant Open Error Log File $file\n" );

	my($dumpfile)="";
	my($lastdumpfile)="";
	while (<ERRLOG>) {
		chomp;
		if(/DUMP is complete / or /LOAD is complete / ) {

			#print "DBG",$_,"\n";

			# parse the error log messages
			# Jul 31 18:01:38 2000: Backup Server: 3.42.1.1: DUMP is complete (database appworx).

			my(@d)=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+): Backup Server: \d+.\d+.\d+.\d+: (.+) is complete \(database (\w+)\)\.\s*$/;
			if( $#d <= 6 ) {
				report_print("Bad Parse of Backup Server Data - seek help\n");
				@d=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+)/;
				if( $#d==5 ) {
					report_print( "The Date Format of this row is Ok\n" );
					@d=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+): Backup Server: \d+.\d+.\d+.\d+: (.+) is complete/;
					if( $#d==6 ) {
						report_print( "Found the keyword $d[6] - this should be DUMP or LOAD\n" );
						my(@d)=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+): Backup Server: \d+.\d+.\d+.\d+: (.+) is complete \(database (\w+)\)/;
						report_print( "Could something be wrong with the database name of $d[7]\n" );
					} else {
						report_print( "Cant find word DUMP/LOAD\n" );
					}
				} else {
					report_print( "The line does not start with an acceptable Date. Required Format: Jan 4 12:12:12 2004\n" );
				}
				error_out( "($#d) Unable to parse >>>",$_,"<<<\n" );
			} else {
				# successful parse
				my(@month)=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
				my($mth)=0;
				foreach (@month) {
					last if( $d[0] eq $_ );
					$mth++;
				}

				my($t)=timelocal($d[4],$d[3],$d[2],$d[1],$mth,$d[5]-1900);
				my($db)=$d[7];
				my($dorl)=$d[6];
				error_out( "OOPS no server\n" ) unless defined $server;
				my($key)=$server." ".$db;
				if( ! defined $keylist{$key} ) {
					$V_BACKUP_TIME{$key}=0;
					$V_TRANDUMP_TIME{$key}=0;
					$V_RESTORE_TIME{$key}=0;
					$V_TRANLOAD_TIME{$key}=0;
				}
				$keylist{$key}=1;

				# at this point $t=completion time $d[6]=DUMP or LOAD $db=databasename
				error_out( "OOPS no $t \n" ) unless defined $t;
				my($is_tmp);
				if( $dumpfile eq "" ) {
					$is_tmp=1;
					$dumpfile=$lastdumpfile;
				}
				if( $dorl eq "LOAD" and $dumpfile =~ /logdump/ and int($t)>0) {
					if( $V_TRANLOAD_TIME{$key} < $t ) {
						$V_TRANLOAD_TIME{$key} = $t;
						$V_TRANLOAD_FILE{$key} = $dumpfile unless $is_tmp;;
					}
				} elsif( $dorl eq "LOAD" and int($t)>0) {
					if( $V_RESTORE_TIME{$key} < $t ) {
						$V_RESTORE_TIME{$key} = $t;
						$V_RESTORE_FILE{$key} = $dumpfile unless $is_tmp;
					}
				} elsif( $dorl eq "DUMP" and $dumpfile =~ /logdump/ and int($t)>0 ) {
					if( $V_TRANDUMP_TIME{$key} < $t ) {
						$V_TRANDUMP_TIME{$key} = $t;
						$V_TRANDUMP_FILE{$key} = $dumpfile unless $is_tmp;;
					}
				} elsif( $dorl eq "DUMP" and int($t)>0 ) {
					if( $V_BACKUP_TIME{$key} < $t ) {
						$V_BACKUP_TIME{$key} = $t;
						$V_BACKUP_FILE{$key} = $dumpfile unless $is_tmp;;
					}
				} else {
					my($ct);
					$ct=localtime($t);
					report_print( "Potentiallly bad parse of $_\n" );
					report_print( "Dump or Load: $dorl\n" ) ;
					report_print( "Dumpfile $dumpfile\n" ) ;
					report_print( "Time: $t (must be>0)\n");
					report_print( "ERROR: Server $server DB $d[7] was ".$dorl."ed on $d[0] $d[1] $d[5] at $d[2]:$d[3]:$d[4]\n" );
					report_print( "...time = $t ct=$ct\n" );
				}
				$dumpfile="" if $is_tmp;
			}
			$lastdumpfile=$dumpfile;
			$dumpfile="" ;
		} else {
			#rint "DBG: READ $_\n";
			# Attempting to open byte stream device: file...jjjjjjj
			# Dumpfile name 'xxx' ... mounted on byte stream 'yyy' -  the yyy is our filename(s)
			# can have multiple if there are stripes

			if( /mounted on disk file \'(.+)\'$/ ) {
				$dumpfile=$1;
				$dumpfile =~ s/\:\:\d\d$//;
				#print "DBG FOUND DUMPFILE $dumpfile\n";
			} elsif( /mounted on byte stream \'(.+)\'$/ ) {
				$dumpfile=$1;
				$dumpfile =~ s/::\d\d$//;
				#print "DBG FOUND DUMPFILE $dumpfile\n";
			} elsif( /error/i or /warning/i ) {

				if( defined $SAVE_TO_ALARMDB ) {
					
					my($eventtime, $message);
					my(@d);
					if( /Backup Server:/ ) {
						#print "################################\n";
						#print "Row=$_\n";
						@d=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+): Backup Server: \d+.\d+.\d+.\d+: (.+)$/;
						if( $#d != 6 ) {
							warn "BS PARSE ERROR : Cant parse $_\nFOUND $#d Arguments\n".join("|",@d)."\n"."Mon=$1 Day $2 Hr=$3  Msg=$7\n";
							next;
						}
						$message=$7;
						my(@month)=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
						my($mth)=0;
						foreach (@month) {
							last if( $d[0] eq $_ );
							$mth++;
						}
						my($t)=timelocal($d[4],$d[3],$d[2],$d[1],$mth,$d[5]-1900);
						$eventtime =  do_time(-time=>$t,-fmt=>"mm/dd/yyyy hh:mi");

						#print "\tThis row occurred at ".localtime($t)."\n";
					} elsif( /Open Server Error:/ ) {
						@d=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+): Open Server Error: \d+.\d+.\d+: (.+)$/;
						#print $_,"\n";
						#die "OS ERROR FOUND $#d Mon=$1 Day $2 Hr=$3  Msg=$7\n";

						if( $#d != 6 ) {
							warn "Open Server Error PARSE ERROR : Cant parse $_\nFOUND $#d Arguments\n".join("|",@d)."\n"."Mon=$1 Day $2 Hr=$3  Msg=$7\n";
							next;
						}
						$message=$7;
						next if $message=~/was interrupted by an attention/;
						my(@month)=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
						my($mth)=0;
						foreach (@month) {
							last if( $d[0] eq $_ );
							$mth++;
						}
						my($t)=timelocal($d[4],$d[3],$d[2],$d[1],$mth,$d[5]-1900);
						$eventtime =  do_time(-time=>$t,-fmt=>"mm/dd/yyyy hh:mi");
						#print "Open Server Error $eventtime: $message\n";
					} elsif( /Open Server Server Fatal Error:/ ) {
						@d=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+): Open Server Server Fatal Error: \d+.\d+.\d+: (.+)$/;
						#print $_,"\n";
						#die "OS Fatal ERROR FOUND $#d Mon=$1 Day $2 Hr=$3  Msg=$7\n";
						if( $#d != 6 ) {
							warn "Open Server Server Fatal Error PARSE ERROR : Cant parse $_\nFOUND $#d Arguments\n".join("|",@d)."\n"."Mon=$1 Day $2 Hr=$3  Msg=$7\n";
							next;
						}
						$message=$7;
						my(@month)=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
						my($mth)=0;
						foreach (@month) {
							last if( $d[0] eq $_ );
							$mth++;
						}
						my($t)=timelocal($d[4],$d[3],$d[2],$d[1],$mth,$d[5]-1900);
						$eventtime =  do_time(-time=>$t,-fmt=>"mm/dd/yyyy hh:mi");
						#print "Open Server Error $eventtime: $message\n";
					} elsif( /SYBMULTBUF ERROR:/ ) {
						@d=m/^(\w+)\s+(\d+)\s+(\d+)\:(\d+)\:(\d+)\s+(\d+):\s*\w+:\s*SYBMULTBUF ERROR: (.+)$/;
#						print $_,"\n";
#						die "SYBMULTBUF FOUND $#d Mon=$1 Day $2 Hr=$3  Msg=$7\n";

						if( $#d != 6 ) {
							warn "SYBMULTBUF PARSE ERROR : Cant parse $_\nFOUND $#d Arguments\n".join("|",@d)."\n"."Mon=$1 Day $2 Hr=$3  Msg=$7\n";
							next;
						}
						$message=$7;
						my(@month)=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
						my($mth)=0;
						foreach (@month) {
							last if( $d[0] eq $_ );
							$mth++;
						}
						my($t)=timelocal($d[4],$d[3],$d[2],$d[1],$mth,$d[5]-1900);
						$eventtime =  do_time(-time=>$t,-fmt=>"mm/dd/yyyy hh:mi");
						#print "MULTBUF $eventtime: $message\n";
					} else {
						print "DBG: UNKNOWN: $_\n",$nl;
						next;
					}

					if( $DEBUG ) {
						report_print( "(saving Event from $file in alarm db with key=$BATCHID)\n" );
						report_print( "MlpEvent( -message_text=>$message,
										-system=>$server,
										-event_time=$eventtime,
										-monitor_program=>$BATCHID,
										-severity=>ERROR)\n" );# if defined $DEBUG;
					}
					#$mlpeventcount{$server}++;
					MlpEvent(
						-message_text=>$message,
						-system=>$server,
						-event_time=>$eventtime,
						-monitor_program=>$BATCHID,
						-severity=>"ERROR"
					) if $SAVE_TO_ALARMDB;
				}
#			} elsif( $DEBUG ) {
#				if( /mounted on disk file \'(.+)$/ ) {
#					warn "HMMM3 $_\n";
#				} elsif( /mounted on disk file \'(.+)/ ) {
#					warn "HMMM4 $_\n";
#				} elsif( /mounted on disk file \'/ ) {
#					warn "HMMM2 $_\n";
#				} elsif( /mounted on/ ) {
#					warn "HMMM $_\n";
#				}
			}
		}
	}
	close ERRLOG;
}

sub starterror { 	report_print( "<TABLE BORDER=2><TR><TD BGCOLOR=WHITE>\n" ) if defined $HTML; }
sub enderror { 	report_print( "</TD></TR></TABLE>\n" ) if defined $HTML; }


my(%mlpeventcount);
sub read_syb_ase_logfile {
	my($file,$server)=@_;
	$mlpeventcount{$server}=0 unless defined $mlpeventcount{$server};
  #33printf "read_syb_ase_logfile( server=%-20.20s file=%-24.24s fileage=%-s)\n",$server,basename($file),do_diff_file($file);

	#open( ERRLOG,$file ) or error_out( "Cant Open Error Log File $file\n" );

	my($byte_count,$start_patern)=(0,"");
	if( defined $PATFILE and -e $file.".pat" ) {
		report_print( "  Patern File: ".$file.".pat\n" ) if $DEBUG;
		($byte_count,$start_patern) = log_rd_patern_file(file=>$file.".pat");
		my($patfiletime)= -M $file.".pat";
		$patfiletime *= 24;
		my($patfilestr) = "";
		if( $patfiletime >= 1 ) {
			$patfilestr .= int($patfiletime)." hours ";
			$patfiletime -= int($patfiletime);
		}
		$patfiletime *= 60;
		$patfilestr.= " ".int($patfiletime)." minutes";
		report_print( "  Report Filter : Only Include Messages In The Past ",$patfilestr,$nl ) if $DEBUG;
		report_print( "  PATERN FILE byte count : $byte_count   ",$nl ) if defined $DEBUG;
		report_print( "  PATERN FILE startpat   : $start_patern ",$nl ) if defined $DEBUG;
	} elsif( defined $PATFILE )  {
		report_print( "  Patern File: ".c().$file.ec().".pat (NOT FOUND)",$nl );
	}

	if( defined $TODAYYESTERDAYONLY ) {
		report_print( "  Time Restriction: ".c()."Today and Yesterday".ec()."\n" ) if defined $DEBUG;
		log_grep(search_patern=>"",today=>1,yesterday=>1,date_fmt=>"yy/mm/dd");
	} else {
		report_print( "  Time Restriction: ".c()."none".ec()."\n" ) if defined $DEBUG;
		log_grep(search_patern=>"",today=>undef,yesterday=>undef,date_fmt=>"yy/mm/dd");
	}
# "^\d\d:\d\d\d\d\d:"
	# remove some paterns from the input
	#log_rm_pat("\\d\\d:\\d\\d\\d\\d\\d:\\d\\d\\d\\d\\d:| server | kernel |^\.\$");
	log_rm_pat(" server | kernel |^\.\$");

	if( -r $excl_dir."/sybase10.excl" and ! defined $NOERRORFILTER ){
		log_filter_file(file=>$excl_dir."/sybase10.excl");
		print  "  Filter File=".c()."$excl_dir/sybase10.excl".ec(),$nl;
	} else {
		print "  Filter File=".c()."none".ec(),$nl;
	}

	if( defined $DEBUG ) {
		report_print( "  Debug Mode=SET\n" );
		log_debug();
	}

	my($max_lines)=100000;
	$max_lines = 10000 if defined $TODAYYESTERDAYONLY;
	report_print( "** PROCESSING FILE $file **\n" ) if defined $DEBUG;
	my($n_read,$n_returned,$found_pat,$returned_pat,$byte_cnt,@output_r)=
		log_process_file( file=>$file,
			search_patern=>$start_patern,
			byte_offset=>$byte_count,
			cat_nogrep=>1,
			max_lines=>$max_lines,
			is_sybase_ase_file=>1);

	report_print(    "[ Read $n_read rows from $file.  Filtered to $n_returned ]$nl");
	report_print(    "[ DBG: found_pat=$found_pat (0=false 1=true)]$nl") 	if defined $DEBUG;
	report_print(    "[ DBG: next_run_returned_pat=$returned_pat ]$nl") 	if defined $DEBUG;
	report_print(    "[ DBG: next_run_byte_cnt=$byte_cnt ]$nl" ) 		if defined $DEBUG;
	if( defined $PATFILE and ! defined $DEBUG ) {
		log_wr_patern_file(patern=>$returned_pat, bytes=>$byte_cnt, file=>$file.".pat" );
		report_print( "WRITING PATERN FILE byte count = $byte_cnt   \n" ) if defined $DEBUG;
		report_print( "WRITING PATERN FILE startpat   = $returned_pat \n" ) if defined $DEBUG;
	} elsif( defined $PATFILE ) {
		report_print( "NOT WRITING PATERN FILE - DEBUG MODE",$nl );
		report_print( "IF I DID PATERN FILE byte count = $byte_cnt   \n" );
		report_print( "IF I DID PATERN FILE startpat   = $returned_pat \n" );
	}

	report_print( "<b>WARNING: TRUNCATED OUTPUT TO $max_lines LINES</b>$nl" )
		if $#output_r>=($max_lines-1);

	if( $DEBUG ) {
		report_print("----------- output_r array at line ".__LINE__." of file ".__FILE__." ----------------");
		foreach (@output_r) { report_print("DIAG: $_");	}
		report_print("----------- done output_r array ----------------");
	}
	report_print( "<PRE>\n" ) if defined $HTML;

	my($pat)="";
	my(@output);
	foreach (@output_r) {
		next if /\spc:\s/;
		next if /^\(dbg\)/;
		next if /^\s*$/;
		s/\s+/ /g;
		chomp;chomp;

		if( /(\d\d:\d\d\d\d\d:\d\d\d\d\d:)([\d\/]+)\s+([\d\:\.]+)\s+(.+)$/ ) {
			if( $1 eq $pat ) {
				report_print("DBG DBG: appending as $pat was found at the appropriate spot\n") if $DEBUG;
				#report_print("DBG DBG: $4\n") if $DEBUG;
				my($endstr_no_parens)=$4;
				$endstr_no_parens =~ s/\)/\\)/g;
				$endstr_no_parens =~ s/\(/\\(/g;
				if( $output[$#output] =~ /$endstr_no_parens/ ) {
					#report_print( "HEY DUPLICATE FOUND\n" );
				} else {
					$output[$#output].="\n".$endstr_no_parens;
				}
				#report_print("DBG DBG: DONE\n") if $DEBUG;
			} else {
				push @output,$2." ".$3." ".$4;
			}
			$pat=$1;
		}
	}

	my $count=1;
	my $priorrowid=$#output+1;
	my $saved_string="";
	my $saved_hdr="";
	while( $priorrowid > 0 ) {
		$priorrowid--;
		my($tstr)=$output[$priorrowid];
		chomp $tstr;
		chomp $tstr;
		my($tlen)=length $tstr;
		my($sstr)=$tstr;
		$sstr=~s/[^\s\d\w\p{P}_:\.\,\\\/\'\"\;\|\]\[]//g;
		my($day,$time,$msg)=split(/\s+/,$sstr,3);
		if ( ! defined $msg ) {
			warn "Bad Parse of $tstr\n" unless defined $msg;
			print "Bad Parse of $tstr\n" unless defined $msg;
			next;
		}

		my($severity);
		if( $msg=~/Deadlock Id \d+ detected/ or $msg=~/killed by Hostname/ )  {
			$severity="INFORMATION";
		} elsif( $msg=~ /Msg/i or $msg=~/Err/i or $msg=~/fail/i
				or $msg=~/corrupt/i or $msg=~/warn/i) {
			$severity="ERROR";
		} else {
			$severity="WARNING";
		}

		if( defined $SAVE_TO_ALARMDB and $BATCHID ) {
			report_print( "(saving Event from $file in alarm db with key=$BATCHID)\n" );
			# revisit event time
			my($event_time)=$day;
			$day=~s/^\d+:\d+:\d+://;
			report_print( "MlpEvent( -message_text=>$msg,
					-system=>$server,
					-event_time=>$day $time,
					-monitor_program=>$BATCHID,
					-severity=>$severity)\n" );# if defined $DEBUG;
			$mlpeventcount{$server}++;
			MlpEvent( -message_text=>$msg,
				-system=>$server,
				-event_time=>"$day $time",
				-monitor_program=>$BATCHID,
				-severity=>$severity ) if $SAVE_TO_ALARMDB;
		}
		#print "=================================================\n";
		my($clr)="BLUE";
		$clr="RED" if $severity eq "ERROR";
		#$msg=c("RED").$msg.ec() if $severity eq "ERROR";
		my($str)=sprintf("%s%4s%s%s %s %s %s", c("BLUE"),$count,ec(),c("GREEN"),$day,$time,ec());
		$str.= join("\n".c($clr).$count.ec()."\t",split(/\n/,$msg));
		$count++;
		report_print( $str,$nl );
	}
	if( defined $SAVE_TO_ALARMDB and $mlpeventcount{$server}>=0 ) {
		print "\tSAVED $mlpeventcount{$server} ALARMS\n";
		report_print( "SAVED $mlpeventcount{$server} ALARMS\n" );
	} else {
		my($x)=$#output+1;
		$x++;
		print "\tProcessed $x rows\n";
	}

	report_print( "</PRE><hr>\n" ) if defined $HTML;
}


sub read_syb_rep_server_logfile {
	my($file,$server)=@_;

	printf "read_syb_rep_server_logfile( server=%-20.20s file=%-24.24s fileage=%-s)\n",$server,basename($file),do_diff_file($file);

	my($byte_count,$start_patern)=(0,"");
	if( defined $PATFILE and -e $file.".pat" ) {
		report_print( "  Patern File: ".c().$file.ec().".pat\n" );
		($byte_count,$start_patern) = log_rd_patern_file(file=>$file.".pat");
		my($patfiletime)= -M $file.".pat";
		$patfiletime *= 24;
		my($patfilestr) = "";
		if( $patfiletime >= 1 ) {
			$patfilestr .= int($patfiletime)." hours ";
			$patfiletime -= int($patfiletime);
		}
		$patfiletime *= 60;
		$patfilestr.= " ".int($patfiletime)." minutes";
		report_print( "  Report Filter : Only Include Messages In The Past ",$patfilestr,$nl ) if $DEBUG;
		report_print( "  PATERN FILE byte count : $byte_count   ",$nl ) if defined $DEBUG;
		report_print( "  PATERN FILE startpat   : $start_patern ",$nl ) if defined $DEBUG;
	} elsif( defined $PATFILE )  {
		report_print( "  Patern File: ".c().$file.".pat (NOT FOUND)".ec().$nl );
	}

	if( defined $TODAYYESTERDAYONLY ) {
		report_print( "  Time Restriction: ".c()."Today and Yesterday".ec()."\n" ) if defined $DEBUG;
		log_grep(search_patern=>undef,today=>1,yesterday=>1,date_fmt=>"yyyy/mm/dd");
	} else {
		report_print( "  Time Restriction: ".c()."none".ec()."\n" ) if defined $DEBUG;
		log_grep(search_patern=>undef,today=>undef,yesterday=>undef,date_fmt=>"yyyy/mm/dd");
	}

	# remove some paterns from the input
	###log_rm_pat(" server | kernel |^\.\$");

	#if( -r "../bin/sybase10.excl" and ! defined $O_NOERRORFILTER ){
	#	log_filter_file(file=>"../bin/sybase10.excl");
	#	report_print( "  Filter File=../bin/sybase10.excl",$nl ) if $DEBUG;
	#} else {
	#	report_print( "  Filter File=none",$nl );
	#}

	if( defined $DEBUG ) {
		report_print( "  Debug Mode=SET\n" );
		log_debug();
	}

	my($max_lines)=100000;
	$max_lines = 10000 if defined $TODAYYESTERDAYONLY;
	report_print( "** PROCESSING FILE **\n" ) if defined $DEBUG;
	my($n_read,$n_returned,$found_pat,$returned_pat,$byte_cnt,@output_r)=
		log_process_file(
			file=>$file,
			search_patern=>$start_patern,
			byte_offset=>$byte_count,
			cat_nogrep=>1,
			max_lines=>$max_lines);

	report_print(    "[ Read $n_read rows from $file.  Filtered to $n_returned ]$nl");
	report_print(    "[ DBG: found_pat=$found_pat (0=false 1=true)]$nl") 	if defined $DEBUG;
	report_print(    "[ DBG: next_run_returned_pat=$returned_pat ]$nl") 	if defined $DEBUG;
	report_print(    "[ DBG: next_run_byte_cnt=$byte_cnt ]$nl" ) 		if defined $DEBUG;
	if( defined $PATFILE and ! defined $DEBUG ) {
		log_wr_patern_file(patern=>$returned_pat, bytes=>$byte_cnt, file=>$file.".pat" );
		report_print( "WRITING PATERN FILE byte count = $byte_cnt   \n" ) if defined $DEBUG;
		report_print( "WRITING PATERN FILE startpat   = $returned_pat \n" ) if defined $DEBUG;
	} elsif( defined $PATFILE ) {
		report_print( "NOT WRITING PATERN FILE - DEBUG MODE",$nl );
		report_print( "[ IF I DID PATERN FILE ] byte count = $byte_cnt   \n" );
		report_print( "p IF I DID PATERN FILE ] startpat   = $returned_pat \n" );
	}

	report_print( "<b>WARNING: TRUNCATED OUTPUT TO $max_lines LINES</b>$nl" )
		if $#output_r>=($max_lines-1);

	#report_print( "<PRE>\n" ) if defined $HTML;

	my($pat)="";
	my(@warnings);
	my(@output);
	my($curline,$curlinetype);
	foreach (@output_r) {
		chomp;
		if( /^\w\./ ) {			# save the last line
			#if($curlinetype ne "I" ) {
			push @warnings,$curline if $curline;
			#}
			$curline=$_;
			$curlinetype=substr($_,0,1);
		} else {
			$curline.="\n".$_;
		}
	}
	return @warnings;

}

sub print_syb_rep_svr_rpt
{
	foreach my $d ( keys %rep_dat ) {
		my(@dat)=@$d;
		if( $#dat< 0 ) {
			report_print( "NO WARNINGS OR ERRORS FOUND\n" );
		} else {
			print "read completed - dat=$#dat\n";
			if( defined $HTML ) {
				report_print( "<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=1>\n<TR><TH BGCOLOR=#FFCCFF>Server</TH><TH BGCOLOR=#FFCCFF>Message</TH></TR>\n" );
			} else {
				report_print( sprintf( "%-20.20s %s","Server","Message"));
			}

			foreach( @dat ) {
				# dates allready done
				#next if defined $TODAYYESTERDAYONLY and ! /^\w\.\s$t1/ and ! /^\w\.\s$t2/;

				# the following messages are actually CREATED by our own monitoring
				# apparently DBD::Sybase runs some stuff when it connects - commands that
				# are apparently NOT acceptable rep server sql.  So we just ignore these messages
				next if /Executor does not allow the entered command in the current mode./;
				next if /Connection to server/ and /has been faded out/;

				my($severity)="WARNING";
				$severity="INFO"  if /^I\./;
				$severity="ERROR" if /^E\./;

				# lets parse out a date...
				/(\d\d\d\d\/\d\d\/\d\d\s\d\d\:\d\d\:\d\d)/;
				my($dt)=$1;
				##$_="DATE=$dt ".$_;

				report_print( "<TR><TD>" ) if defined $HTML;
				report_print( $severity );
				report_print( "\t" ) unless $HTML;
				report_print( "\n\t</TD><TD>" ) if defined $HTML;
				report_print( $_."\n");
				report_print( "\t</TD></TR>\n" ) if defined $HTML;

				if( defined $SAVE_TO_ALARMDB ) {
					next if $severity eq "INFO";
					report_print( "MlpEvent( -message_text=>$_, -event_time=>$dt, -system=>$d, -severity=>WARNING)\n" );
					MlpEvent(
						-message_text=>$_,
						-event_time=>$dt,
						-system=>$d,
						#-event_time=>"$day $time",
						-monitor_program=>$BATCHID,
						-severity=>$severity
					) ;
				}
			}
			report_print("</TABLE>") if defined $HTML;
		}
		print "file completed\n";
	}
}
sub c {
 my($clr)=@_;
 return "" unless $HTML;
 return "<FONT COLOR=$clr>";
}

sub ec {
 return ""  unless $HTML;
 return "</FONT>";
}
