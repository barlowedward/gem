#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Basename;
use Sys::Hostname;
use Getopt::Long;
use Repository;
use Net::myRSH;
use Net::myFTP;
use File::Path;
use MlpAlarm;
use CommonFunc;

use vars qw($SYSTEM $SOURCE_FILE $ACTION $TARGET_FILE $NOEXEC $COMMAND $DEBUG $RUNMETHOD $TARGET_DIR);
use vars qw($TIMEOUT $COLUMNATE $COMMANDOVERRIDE $TESTMODE $SILENTMODE $HTML $FIELDS_TO_SHOW $COMMAND_2 $COMMAND_3);
use vars qw($ADDNEWLINE $BATCHID $REPORTFILE $TESTMODERPT $TESTMODEUPDATE );

sub usage {
	my($msg)=@_;
   print $msg,"\n";
   print "putget.pl  GENERIC UNIX FILE MANAGER\n";

   print '
   --DEBUG              --NOEXEC
   --COLUMNATE          --FIELDS_TO_SHOW=a,b [ comma separated ]
   --SILENTMODE         --HTML	--BATCHID=AlarmBatchId

   --SYSTEM=sys         [ optional - comma separated list ]
   --RUNMETHOD=ssh|ftp  [ override default behaviors of servers ]
   --TIMEOUT=SECS       [ timeout for subcommands ]
   --COMMANDOVERRIDE    [ use config file directive to override command ]
   --ADDNEWLINE         [ add a new line between server output ]

   --SOURCE_FILE=file     --TARGET_FILE=file        --TARGET_DIR=dir
   --COMMAND=cmd          --COMMAND_2               --COMMAND_3
   --REPORTFILE=file      --TESTMODE					--TESTMODERPT=file
   --TESTMODEUPDATE update configfiles with best comm method (TESTMODE ONLY)

   --ACTION=REPORT		[ include $REPORTFILE;
        foreach(data/system_information_data/$host/$TARGET_FILE)
              run process_file($filename);
        Run report_file() ]

   --ACTION=REMOVE      [ cd $TARGET_DIR; remove $TARGET_FILE ]
   --ACTION=RENAME      [ cd $TARGET_DIR; rename($SOURCE_FILE,$TARGET_FILE); ]
   --ACTION=LS          [ cd $TARGET_DIR; ls $TARGET_FILE or ls ]
   --ACTION=STORE       [ cp $SOURCE_FILE
        ..data/system_information_data/$host/$TARGET_FILE ]
   --ACTION=GET         [ cp $SOURCE_FILE to [$TARGET_DIR/]$TARGET_FILE ]
   --ACTION=PUT         [ rcp $SOURCE_FILE $TARGET_DIR/$TARGET_FILE ]
   --ACTION=RUN         [ exec $COMMAND on remote system ]';

   return "\n";
}

die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless GetOptions(
         "SOURCE_FILE=s"  		=> \$SOURCE_FILE	,
         "ACTION=s" 				=> \$ACTION	,
         "DEBUG"  				=> \$DEBUG	,
         "TESTMODE"  			=> \$TESTMODE,
         "TESTMODERPT=s" 		=> \$TESTMODERPT,
         "TESTMODEUPDATE"		=> \$TESTMODEUPDATE,

         "HTML" 	 				=> \$HTML	,
         "COMMANDOVERRIDE" 	=> \$COMMANDOVERRIDE	,
         "ADDNEWLINE" 			=> \$ADDNEWLINE	,
         "BATCHID=s" 			=> \$BATCHID	,
         "REPORTFILE=s" 		=> \$REPORTFILE	,
         "RUNMETHOD=s" 			=> \$RUNMETHOD	,
         "SYSTEM=s"  			=> \$SYSTEM	,
         "COMMAND=s"  			=> \$COMMAND	,
         "TARGET_FILE=s"  		=> \$TARGET_FILE ,
         "TARGET_DIR=s"  		=> \$TARGET_DIR ,
         "NOEXEC"					=> \$NOEXEC,
        	"TIMEOUT=s"  			=> \$TIMEOUT	,
      	"COLUMNATE"  			=> \$COLUMNATE	,
      	"SILENTMODE"  			=> \$SILENTMODE	,
      	"FIELDS_TO_SHOW=s"  	=> \$FIELDS_TO_SHOW	,
      	"COMMAND_2=s"  		=> \$COMMAND_2	,
      	"COMMAND_3=s"  		=> \$COMMAND_3
);

if( $TESTMODE ) { # $TESTMODE IS SPECIAL
	die "Cant pass any other arguments if --TESTMODE\n" if $ACTION or $COMMAND;
	$TIMEOUT=10 unless $TIMEOUT;
	$ACTION="RUN";
	$COMMAND="echo 'test_completed'";
}
die usage("MUST SPECIFY AN ACTION") unless $ACTION;

die "CAN NOT FIND REPORTFILE=$REPORTFILE" if $REPORTFILE and ! -r $REPORTFILE;
require $REPORTFILE if -r $REPORTFILE;
die "WOAH: REPORTFILE WAS NOT INCLUDED\n" if $ACTION eq "REPORT" and ! $REPORTFILE;

my($NL)="\n";
$NL="<br>\n" if $HTML;
my($FS)="";
my($FB)="";
my($FE)="";
$FB="<FONT COLOR=BLUE>" if $HTML;
$FS="<FONT COLOR=RED>" 	if $HTML;
$FE="</FONT>" if $HTML;

my $curhostname = hostname();

my(@hosts) = get_password(-type=>"unix");
@hosts=split(/,/,$SYSTEM) if $SYSTEM;

$BATCHID="putget_file.pl" unless $BATCHID;

print "$BATCHID run at ".localtime(time)." on host ".$curhostname.$NL;
if( ! $SILENTMODE ) {
	print "Testmode has been set to test connectivity",$NL if $TESTMODE;
	if( $SYSTEM ) {
		print "Running on: ".join(" ",@hosts),$NL;
	} else {
		my($c)=$#hosts;
		$c++;
		print "Running on $c Hosts",$NL;
	}
	if( ! $TESTMODE ) {
		print "ACTION=$ACTION ";
		print "COMMAND=",$COMMAND if $COMMAND;
		print $NL;
	} else {
		print "RUNNING IN TEST CONNECTIVITY MODE",$NL;
		print "This command will run $FB ssh user\@system $FE",$NL;
		print "Rsh will be tested if ssh fails",$NL;
	}
	print "TARGET_FILE=$TARGET_FILE TARGET_DIR=$TARGET_DIR \n"
		if ( $TARGET_FILE or $TARGET_DIR ) and ! $REPORTFILE;
	print "REPORT FILE =$REPORTFILE\n" if $REPORTFILE;
	print "Server List: $SYSTEM",$NL if defined $SYSTEM;
	print "Command run in DEBUG mode",$NL if $DEBUG;
	print "\n";
}

# BUILD COLUMN MAP
my $hlen=0;
my(@show_column);
if( $ACTION eq "RUN" ) {
	$show_column[100]=1;
	if( defined $FIELDS_TO_SHOW ) {
		$FIELDS_TO_SHOW =~ s/\s//g;
		foreach ( split(/,/,$FIELDS_TO_SHOW)) { $show_column[$_]=1; }
	} else {
		my($i)=0;
		while ($i<100) { $show_column[$i]=1; $i++; }
	}
}

# OUTPUT VARIABLES
#
my( %OUTPUT );							# by host
my( $file_count_for_host )=1;

# THE FOLLOWING VARIABLES MOSTLY RELATE TO TESTMODE
# THE _OK VARS ARE yes | no while the reason are a textual reason
my( %FTP_OK, %SSH_OK, %RSH_OK );
my( %FTP_REASON, %SSH_REASON, %RSH_REASON );
my( %SSH_COMMAND, %RSH_COMMAND );
my( %LOGIN_USED, %DEFAULT_METHOD );

my(@warnings);
#if( ! $TIMEOUT ) {
#	push @warnings, "NO TIMEOUT SPECIFIED WITH -T - SETTING TO 120 SECONDS".$NL unless $ACTION eq "RUN";
#	$TIMEOUT=120;
#}
foreach my $host (sort @hosts) {
	my($login,$password)=get_password(-type=>"unix",-name=>$host);

	$LOGIN_USED{$host}=$login;
	if( ! $login ) {
		$FTP_OK{$host} = $RSH_OK{$host} = $SSH_OK{$host} = "NO";
		$FTP_REASON{$host} = $RSH_REASON{$host} = $SSH_REASON{$host} = $OUTPUT{$host} = "Unable to get login\n";
		next;
	}
	if( ! $password ) {
		$FTP_OK{$host} = $RSH_OK{$host} = $SSH_OK{$host} = "NO";
		$FTP_REASON{$host} = $RSH_REASON{$host} = $SSH_REASON{$host} = $OUTPUT{$host} = "Unable to get password\n";
		next;
	}

	my(%args)=get_password_info(-type=>"unix",-name=>$host);
	my($method);
	if( is_nt() ) {
		$method=$args{WIN32_COMM_METHOD};
	} else {
		$method=$args{UNIX_COMM_METHOD};
	}
	$DEFAULT_METHOD{$host}=$method;

	print "Working on host $host (default method=$method)",$NL if $DEBUG;

	$method=uc($RUNMETHOD) if $RUNMETHOD;							# FROM THE COMMAND LINE
	$method="SSH" if ! defined $method or $method eq "";

	if( $method eq "NONE" and ! $TESTMODE ) {
		$RSH_OK{$host} = $SSH_OK{$host} = "NO";
		$RSH_REASON{$host} = $SSH_REASON{$host} = $OUTPUT{$host} = $FS."host $host: method=NONE - skipping".$FE;
		push @warnings, "host $host: method=NONE - skipping";
		next;
	}

	local $SIG{ALRM} = sub { die "CommandTimedoutPG"; };

	#
	# IF TESTMODE RUN FTP FIRST
	#
	if( $TESTMODE ) {
		print "[$host] TESTING SERVER $host - DFLT METHOD=$method",$NL  unless $SILENTMODE;

		$method="FTP";
		$TARGET_DIR="/";
		$! = "";
		eval {
			alarm($TIMEOUT) if $TIMEOUT;
			do_host( $host, $login, $password, "FTP", "LS", undef,$DEBUG );
			alarm(0);
		};
		if( $@ ) {
			if( $@=~/CommandTimedoutPG/ ) {
				#if( $TESTMODE ) {
					$FTP_OK{$host} = "NO";
					$OUTPUT{$host} = $FTP_REASON{$host} = "FTP Command Timed Out";
				#} else {
				#	print "$method Command Timed Out to $host".$NL if $DEBUG;
				#	push @warnings, "$method Command Timed Out to $host";
				#	$OUTPUT{$host} = $FS."$method Command Timed Out to $host".$FE;
				#}
			} else {
				if( $@ !~ /^\s*$/ ) {
					print "$method Command Returned $@ from $host".$NL if $DEBUG;
					push @warnings, "$method Command Returned (".$@.") from $host";
					$OUTPUT{$host} = $FS."$method Command Returned $@ from $host".$FE;
				} else {
					print "DBG DBG ODDITY $@";
				}
				alarm(0);
			}
		}
		alarm(0);
		if( ! $FTP_OK{$host} and $OUTPUT{$host} =~ /\s\/*dev\s/ ) {
			$FTP_OK{$host} = "YES";
			print "[$host $method] $host FTP ***OK***",$NL unless $SILENTMODE;
		} else {
			$FTP_OK{$host} = "NO";
			$FTP_REASON{$host} = join("",split(/\n/,$OUTPUT{$host})) unless $FTP_REASON{$host};
			print "[$host $method] FTP OUTPUT is ".join("",split(/\n/,$OUTPUT{$host}))."\n"  unless $SILENTMODE;
		}
		$OUTPUT{$host}="";
	}

#		my($rc)=do_host( $host, $login, $password, $method, $ACTION, $COMMAND,$DEBUG );
#		chomp $rc;
#		chomp $rc;
#   	if( $rc =~ /^failure :/ ) {
#   		$errorlist{$host}=$rc;
#   		$numerrors++;
#   	} else {
#   		$numok++;
#   		print "$host: $rc\n";
#   	}

	#
	# IF TESTMODE RUN SSH NEXT
	#
   $method="SSH" if $TESTMODE;
   $method="FTP" if ! defined $method or $method eq "";

	my $cmd = $COMMAND;
   $cmd = $args{$COMMANDOVERRIDE} if defined $COMMANDOVERRIDE
				and defined $args{$COMMANDOVERRIDE}
				and $args{$COMMANDOVERRIDE} !~ /^\s*$/;
	eval {
		alarm($TIMEOUT) if $TIMEOUT;
		do_host( $host, $login, $password, $method, $ACTION, $COMMAND,$DEBUG );
		alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedoutPG/ ) {
			if( $TESTMODE ) {
				$SSH_OK{$host} = "NO";
				$OUTPUT{$host} = $SSH_REASON{$host} = "$method Command Timed Out";
			} else {
				print "$method Command Timed Out to $host".$NL if $DEBUG;
				push @warnings, "$method Command Timed Out to $host";
				$OUTPUT{$host} = $FS."$method Command Timed Out to $host".$FE;
			}
		} else {
			if( $@ !~ /^\s*$/ ) {
				print "$method Command Returned $@ from $host".$NL if $DEBUG;
				push @warnings, "$method Command Returned (".$@.") from $host";
				$OUTPUT{$host} = $FS."$method Command Returned $@ from $host".$FE;
			} else {
				print "DBG DBG ODDITY $@";
			}
			alarm(0);
		}
	}
	alarm(0);

	#
	# IF TESTMODE THEN DO RSH! LAST
	#
	next unless $TESTMODE;

	# ONLY NEED TO SAVE TESTMODE RESULTS HERE (After the next) - IF ITS NOT TESTMODE WE DONT USE THEM
	if( $OUTPUT{$host} eq "test_completed" ) {
		$SSH_OK{$host} = "YES";
		print "[$host $method] $host SSH ***OK***",$NL unless $SILENTMODE;
		$OUTPUT{$host}="";
	} else {
		$SSH_OK{$host} = "NO";
		$SSH_REASON{$host} = join("",split(/\n/,$OUTPUT{$host})) unless $SSH_REASON{$host};
		$SSH_COMMAND{$host} = rsh_lastcommand();
		print "[$host $method] COMMAND = ",rsh_lastcommand(),"\n" unless $SILENTMODE;
		print "[$host $method] SSH OUTPUT is ".join("",split(/\n/,$OUTPUT{$host}))."\n"  unless $SILENTMODE;
	}

	$method="RSH";
	$OUTPUT{$host}="";

   eval {
		alarm($TIMEOUT) if $TIMEOUT;
		do_host( $host, $login, $password, $method, $ACTION, $COMMAND,$DEBUG );
		alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedoutPG/ ) {
			$RSH_OK{$host} = "NO";
			$OUTPUT{$host} = $RSH_REASON{$host} = "$method Command Timed Out";
			print "$method Command Timed Out to $host".$NL if $DEBUG;
		} else {
			if( $@ !~ /^\s*$/ ) {
				$RSH_OK{$host} = "NO";
				print "$method Command Returned $@ from $host".$NL if $DEBUG;
				$RSH_REASON{$host} = $FS."$method Command Returned $@".$FE;
			} else {
				print "DBG DBG ODDITY $@";
			}
			alarm(0);
		}
	}
	alarm(0);

	# SAVE TESTMODE RESULTS FOR THE RSH RUN
	if( $OUTPUT{$host} eq "test_completed" ) {
		$RSH_OK{$host} = "YES";
		print "[$host $method] $host RSH ***OK***",$NL  unless $SILENTMODE;
		$OUTPUT{$host}="";
	} else {
		$RSH_OK{$host} = "NO";
		$RSH_REASON{$host} = join("",split(/\n/,$OUTPUT{$host})) unless $RSH_REASON{$host};
		print "[$host $method] RSH OUTPUT is ".join("",split(/\n/,$OUTPUT{$host}))."\n" unless $SILENTMODE;
		$RSH_COMMAND{$host} = rsh_lastcommand();
		print "[$host $method] COMMAND = ",rsh_lastcommand(),"\n" unless $SILENTMODE;
	}
	print "[$host] FTP=$FTP_OK{$host} RSH=$RSH_OK{$host} SSH=$SSH_OK{$host}\n" unless $SILENTMODE;
}

if( $REPORTFILE ) {
 	report_file($HTML);
 	exit();
}

#
# glue broken lines together ... sigh
#
if( $COMMANDOVERRIDE eq "DF" ) {
	foreach my $host (@hosts) {
		my($in)=$OUTPUT{$host};
		$OUTPUT{$host}="";
		foreach my $line (split(/\n/,$in)) {
			$line=~s/\s+$//;
			next if $line=~/no such file or directory/i;
			my(@d)=split(/\s+/,$line);
			if( $#d==0 ) {
				$OUTPUT{$host}.=$line." ";
			} else {
				$OUTPUT{$host}.=$line
			}
	 	}
	}
}

my(@CELLLEN); 	# by colnum
if( defined $COLUMNATE) {
	foreach my $host (@hosts) {
		foreach my $line (split(/\n/,$OUTPUT{$host})) {
			chomp $line;
			next if $line=~/^failure/;
			my $colid = 1;
			foreach my $cell (split(/\s+/,$line)) {
				$CELLLEN[$colid]=length $cell
					if defined $show_column[$colid] and $CELLLEN[$colid]<length $cell;
				$colid++;
			}
		}
	}
}

if( $TESTMODE ) {
	my(@alerts);
	my(%recommendedupgrades);
	my(%recommendeddowngrades);
	my($rptstr)="";
	if( $HTML ) {
		my($HEADER_COLOR)="brown";
		$rptstr .=  "<TABLE BORDER=1>";
		$rptstr .=  "<TR BGCOLOR=$HEADER_COLOR><TH COLSPAN=7>CONNECTIVITY TEST RESULTS</TH></TR>\n";
		$rptstr .=  "<TR BGCOLOR=$HEADER_COLOR><TH>HOSTNAME</TH><TH>FTP OK</TH><TH>SSH OK</TH><TH>RSH OK</TH><TH>DFLT METHOD</TH><TH>LOGIN</TH><TH>ERRORS</TH></TR>\n";
		foreach my $host (@hosts) {
			my($CURRENT_COLOR)=("beige");
			$SSH_OK{$host}=$FS."NO".$FE if $HTML and $SSH_OK{$host} eq "NO";
			$RSH_OK{$host}=$FS."NO".$FE if $HTML and $RSH_OK{$host} eq "NO";
			$FTP_OK{$host}=$FS."NO".$FE if $HTML and $FTP_OK{$host} eq "NO";
			my($d_color,$e_color);
			$d_color="<FONT COLOR=RED><b>" if $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} ne "YES";
			$d_color="<FONT COLOR=RED><b>" if $DEFAULT_METHOD{$host} eq "RSH" and $RSH_OK{$host} ne "YES";
			$d_color="<FONT COLOR=RED><b>" if $DEFAULT_METHOD{$host} eq "FTP" and $FTP_OK{$host} ne "YES";
			$e_color="</b></FONT" if $d_color;
			$CURRENT_COLOR="PINK" if $d_color;
			$rptstr .= sprintf( "<TR BGCOLOR=$CURRENT_COLOR><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>",
					$FB.$host.$FE,
					$FTP_OK{$host},
					$SSH_OK{$host},
					$RSH_OK{$host},
					$d_color.$DEFAULT_METHOD{$host}.$e_color,
					$LOGIN_USED{$host});
			$rptstr .=  $FS."\tFTP_REASON=$FTP_REASON{$host} ".$FE.$NL if $FTP_REASON{$host} and $FTP_OK{$host} ne "YES";
			$rptstr .=  $FS."\tSSH_COMMAND=$SSH_COMMAND{$host} ".$FE.$NL if $SSH_COMMAND{$host} and $SSH_OK{$host} ne "YES";
			$rptstr .=  $FS."\tSSH_REASON=$SSH_REASON{$host} ".$FE.$NL if $SSH_REASON{$host} and $SSH_OK{$host} ne "YES";
			$rptstr .=  $FS."\tRSH_COMMAND=$RSH_COMMAND{$host} ".$FE.$NL if $RSH_COMMAND{$host} and $RSH_OK{$host} ne "YES";
			$rptstr .=  $FS."\tRSH_REASON=$RSH_REASON{$host} ".$FE.$NL if $RSH_REASON{$host} and $RSH_OK{$host} ne "YES";
			$rptstr =~ s/<br>\n$//;
			$rptstr =~ s/\n$//;
			$rptstr .=  "&nbsp;</TD></TR>\n";

			my($upgrdstr);

			$recommendedupgrades{$host}="FSH" if $DEFAULT_METHOD{$host} eq "NONE" and $FTP_OK{$host} eq "YES";
			$recommendedupgrades{$host}="RSH" if $DEFAULT_METHOD{$host} eq "NONE" and $RSH_OK{$host} eq "YES";
			$recommendedupgrades{$host}="RSH" if $DEFAULT_METHOD{$host} eq "FTP" and $RSH_OK{$host} eq "YES";
			$recommendedupgrades{$host}="SSH" if $DEFAULT_METHOD{$host} ne "SSH" and $SSH_OK{$host} eq "YES";

			$recommendeddowngrades{$host}="FTP" if $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} eq "NO" and $FTP_OK{$host} eq "YES";
			$recommendeddowngrades{$host}="FTP" if $DEFAULT_METHOD{$host} eq "RSH" and $RSH_OK{$host} eq "NO" and $FTP_OK{$host} eq "YES";
			$recommendeddowngrades{$host}="RSH" if $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} eq "NO" and $RSH_OK{$host} eq "YES";

			$rptstr .= $host. " should be upgraded to ".$recommendedupgrades{$host}.$NL if $recommendedupgrades{$host};
			$rptstr .= $host. " should be down graded to ".$recommendeddowngrades{$host}.$NL if $recommendeddowngrades{$host};

			my($username)=$ENV{USER} || $ENV{USERNAME};
			$username="USER=".$username if $username;

			if( $d_color ) {
				my($reason);
				$reason = $SSH_REASON{$host}." ".$SSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "SSH";
				$reason = $RSH_REASON{$host}." ".$RSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "RSH";
				$reason = $FTP_REASON{$host} if $DEFAULT_METHOD{$host} eq "FTP";

				$rptstr .=  $FS."DEFAULT CONNECTION TO $host FAILED - $reason".$FE.$NL;
				push @alerts, $FS."ALERT: DEFAULT CONNECTION $DEFAULT_METHOD{$host} TO $host FAILED - $reason".$FE.$NL;
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "ERROR",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host FAILED from $curhostname $username - $reason" )
								if $BATCHID ne "putget_file.pl";
			} else {
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "OK",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host SUCCEEDED from $curhostname $username" )
								if $BATCHID ne "putget_file.pl";
   		}
	#		foreach (split(/\n/,$OUTPUT{$host})) {
	#			next if /^\s*$/;
	#			printf "%".$hlen."s ",$host unless $HTML;
	#			print $_,$NL;
	#		}
		}
		$rptstr .=  "</TABLE>";

	} else {
		$rptstr .=  "--- PARSABLE TEST MODE OUTPUT ---".$NL unless $TESTMODERPT;
		foreach my $host (@hosts) {
			$rptstr .= sprintf( "HOST %-16s FTP_OK=%-3.3s SSH_OK=%-3.3s RSH_OK=%-3.3s METHOD=%s LOGIN=%s",
				$host, $FTP_OK{$host}, $SSH_OK{$host}, $RSH_OK{$host}, $DEFAULT_METHOD{$host}, $LOGIN_USED{$host});
			$rptstr .=  $NL."\tFTP_REASON=$FTP_REASON{$host} " if $FTP_REASON{$host} and $FTP_OK{$host} ne "YES";
			$rptstr .=  $NL."\tSSH_REASON=$SSH_REASON{$host} " if $SSH_REASON{$host} and $SSH_OK{$host} ne "YES";
			$rptstr .=  $NL."\tRSH_REASON=$RSH_REASON{$host} " if $RSH_REASON{$host} and $RSH_OK{$host} ne "YES";

			if (( $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} ne "YES" )
			or (  $DEFAULT_METHOD{$host} eq "RSH" and $RSH_OK{$host} ne "YES" )
			or (  $DEFAULT_METHOD{$host} eq "FTP" and $FTP_OK{$host} ne "YES" )){
				my($reason);
				$reason = $SSH_REASON{$host}." ".$SSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "SSH";
				$reason = $RSH_REASON{$host}." ".$RSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "RSH";
				$reason = $FTP_REASON{$host} if $DEFAULT_METHOD{$host} eq "FTP";

				$rptstr .=  $NL."DEFAULT CONNECTION TO $host FAILED - $reason";
				push @alerts, "ALERT: DEFAULT CONNECTION $DEFAULT_METHOD{$host} TO $host FAILED - $reason".$NL;
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "ERROR",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host FAILED from $curhostname - $reason" )
								if $BATCHID ne "putget_file.pl";
			} else {
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "OK",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host SUCCEEDED from $curhostname" )
								if $BATCHID ne "putget_file.pl";
   		}
			$rptstr .=  $NL;
		}
		$rptstr .=  "--- DONE PARSABLE TEST MODE OUTPUT ---".$NL unless $TESTMODERPT;
	}
	if( $TESTMODERPT ) {
		open(RPT,"> $TESTMODERPT") or die "Cant write to $TESTMODERPT $! \n";
		foreach ( sort keys 	%recommendeddowngrades ) {
			print RPT $_. " should be downgraded to ".$recommendeddowngrades{$_}.$NL;
		}
		foreach ( sort keys 	%recommendedupgrades ) {
			print RPT $_. " should be upgraded to ".$recommendedupgrades{$_}.$NL;
		}

		print RPT $rptstr;
		#print RPT @alerts;
		close RPT;
	} else {
		print $rptstr;
		foreach ( sort keys 	%recommendeddowngrades ) {
			print RPT $_. " should be downgraded to ".$recommendeddowngrades{$_}.$NL;
		}
		foreach ( sort keys 	%recommendedupgrades ) {
			print RPT $_. " should be upgraded to ".$recommendedupgrades{$_}.$NL;
		}
		print @alerts;
	}
	if( $TESTMODEUPDATE ) {
		#we can update everything in the config file!
		print "UPDATING CONFIGURATION FILES AS TESTMODEUPDATE SET\n";
		my(%hashkeys);
		foreach ( sort keys 	%recommendeddowngrades ) {
			$hashkeys{$_."|UNIX_COMM_METHOD"}=$recommendeddowngrades{$_};
		}
		foreach ( sort keys 	%recommendedupgrades ) {
			$hashkeys{$_."|UNIX_COMM_METHOD"}=$recommendedupgrades{$_};
		}
		add_edit_item(-type=>'unix',-op=>'update',-values=>\%hashkeys);	#,-debug=>1);
		print "Completed\n";
	}
	exit(0);
}

print "\n<TABLE BORDER=1>\n<TR><TH BGCOLOR=BEIGE>HOSTNAME</TH><TH BGCOLOR=BEIGE>MESSSAGE</TH></TR>" if $HTML;
foreach my $host (@hosts) {
	if( $HTML ) {
		print "<TR>\n";
		print "\t<TD VALIGN=TOP BGCOLOR=WHITE>";
		print $host;
		print "</TD>\n\t<TD VALIGN=TOP BGCOLOR=WHITE>";
	}

	foreach my $line (split(/\n/,$OUTPUT{$host})) {
		chomp $line;
		next if $line=~ /^\s*$/;
		if( $line=~/^failure/ ) {
			if( $HTML ) {
				print $FS."connect ".$line.$FE;
			} elsif( defined $COLUMNATE ) {
				printf "%".$CELLLEN[1]."s ",$host," " if defined $show_column[1];
				print " connect ".$line,$NL;
			} else {
				print $host," " if defined $show_column[1];
				print " connect ".$line,$NL;
			}
		} else {
			printf "%".$hlen."s ",$host unless $HTML;
			if( defined $FIELDS_TO_SHOW ) {
				my $colid = 1;
				foreach my $cell (split(/\s+/,$line)) {
					if( defined $COLUMNATE ) {
						printf "%".$CELLLEN[$colid]."s ",$cell," " if defined $show_column[$colid];
					} else {
						print $cell," " if defined $show_column[$colid];
					}
					$colid++;
				}
			} else {
					print $line;
			}
			print $NL;
		}
	}
	print "</TD></TR>\n" if $HTML;
	print $NL if defined $ADDNEWLINE and ! $HTML;
}
print "</TABLE>\n" if $HTML;

print $NL,"**** WARNINGS ****",$NL if $#warnings>=0;
foreach(@warnings) { print $_,$NL; }
exit(0);

#####################################################################################

#sub mywarn {
#	my($host)=shift;
#	my($msg)=join("",@_);
#	# print __LINE__," DBG DBG MYWARN($host,$msg)\n";
#	chomp $msg;
#	print $msg,"\n";
#	return $msg;
#}

sub do_host {
	my($host,$login,$password,$method,$ACTION,$COMMAND,$DEBUG_DOHOST)=@_;
	$hlen=length $host if length $host > $hlen;
	$OUTPUT{$host} = "";
	print __LINE__," do_host(Host=$host Action=$ACTION Login=$login Method=$method Cmd=$COMMAND)".$NL if $DEBUG_DOHOST;

	my $cmd;
	if( $ACTION eq "GET" or $ACTION eq "STORE" ) {
		die usage("Invalid SOURCE_FILE") 		unless $SOURCE_FILE;
		my($FNAME,$FDIR);
		$TARGET_FILE=basename($SOURCE_FILE) unless $TARGET_FILE;

		if( $ACTION eq "STORE" ) {
			$FDIR  = get_gem_root_dir()."/data/system_information_data/".$host."/";
			$FNAME = get_gem_root_dir()."/data/system_information_data/".$host."/".$TARGET_FILE;
		} else {
			$FDIR  = ".";
			$FNAME = "${file_count_for_host}_${host}_$TARGET_FILE";
			$file_count_for_host++;
			$FDIR  = $TARGET_DIR 						if $TARGET_DIR;
			$FNAME = $TARGET_DIR."/".$TARGET_FILE 	if $TARGET_DIR;
		}

		mkpath($FDIR) unless -d $FDIR;
		if( ! -w $FDIR ) {
				print __LINE__," WOAH: Directory $FDIR is not writable\n";
				print "<br>" if $HTML;
				$OUTPUT{$host} = red("Directory $FDIR is not writable");
				push @warnings, $OUTPUT{$host};
      		return;
		}
		printf("%4d Working on %-20.20s (login $login, method=$method)\n", __LINE__,$host ) if $DEBUG_DOHOST;
		print "." unless $DEBUG_DOHOST;
		my(%args);
		$args{Debug}=1 if $DEBUG_DOHOST;
   	my($ftp) = Net::myFTP->new($host,METHOD=>$method,%args);
	        	 # or die "Failed To Create FTP Object $@";

   	if( ! $ftp )  {
      		$OUTPUT{$host} = red("Failed To Connect To $host");
      		push @warnings, $OUTPUT{$host};
      		return;
   	}
   	my($rc,$err)=	$ftp->login($login,$password);
   	if( ! $rc ) {
   		chomp $err;
	   	$OUTPUT{$host} = red($err || "Failed To Login");
      	return;
      	# mywarn( $host, $err ) unless $rc;
   	}

   	$ftp->ascii();

   	# Double Check Source File Existence
   	my(@files)=$ftp->ls($SOURCE_FILE);
   	my($found)="FALSE";
   	foreach (@files) {
	   	next unless $SOURCE_FILE eq $_;
	   	$found="TRUE";
	   	last;
	   }
	   if( $found eq "FALSE" ) {
	   	$OUTPUT{$host} = red("Source File $SOURCE_FILE Not Found");
      	return;
      }

   	$OUTPUT{$host}="$method copy $SOURCE_FILE to $FNAME";
   	print __LINE__," Copying $SOURCE_FILE to $FNAME\n" if $DEBUG_DOHOST;
   	my($rc)=$ftp->get($SOURCE_FILE,$FNAME) unless $NOEXEC;

   	# $OUTPUT{$host} .= "(rc=$rc)" if $rc;
	  	if( -r $FNAME ) {
			print __LINE__," Copying Ok.\n" if $DEBUG_DOHOST;
   		$OUTPUT{$host} .= " ok.";
		} else {
			print __LINE__," Copying Failed\n" if $DEBUG_DOHOST;
   		$OUTPUT{$host} .= " Failed. $rc";
   		$OUTPUT{$host} = red($OUTPUT{$host});
		}
	  	$ftp->close();

	  	process_file($FNAME,$host,$HTML) if $REPORTFILE and -r $FNAME;
		return $rc;
	} elsif( $ACTION eq "REPORT" ) {
		my($FDIR)  = get_gem_root_dir()."/data/system_information_data/".$host."/";
		my($FNAME) = get_gem_root_dir()."/data/system_information_data/".$host."/".$TARGET_FILE;
		if( -r $FNAME ) {
			print __LINE__," FOUND FILE FOR $host\n" if $DEBUG_DOHOST;
			process_file($FNAME,$host,$HTML);
		} else {
			print __LINE__," DID NOT FIND FILE FOR $host\n" if $DEBUG_DOHOST;
		}
	} elsif( $ACTION eq "PUT" ) {
		 	# in this case we put $SOURCE_FILE to $TARGET_FILE
	   	die usage("Invalid SOURCE_FILE") 	unless $SOURCE_FILE;
	   	die usage("Invalid TARGET_FILE") 	unless $TARGET_FILE;
			printf("%4d Working on %-20.20s (login $login)  method=$method\n", __LINE__,$host ) if $DEBUG_DOHOST;
			print "." unless $DEBUG_DOHOST;

			my(%args);
			$args{Debug}=1 if $DEBUG_DOHOST;
			my($ftp) = Net::myFTP->new($host,METHOD=>$method,%args);
		        	 #or die "Failed To Create FTP Object $@";
	      if( ! $ftp )  {
	      		$OUTPUT{$host} = "Failed To Connect To $host";
	      		push @warnings, $OUTPUT{$host};
	      		return;
	   	}
	   	my($rc,$err)=	$ftp->login($login,$password);
	   	if( ! $rc )  {
	      		$OUTPUT{$host} = "Failed To Login To $host $err";
	      		push @warnings, $OUTPUT{$host};
	      		return;
	   	}
	   	$ftp->ascii();
	   	if( $TARGET_DIR) {
	   		$ftp->mkdir($TARGET_DIR);
	   		$ftp->cwd($TARGET_DIR);
	   		$OUTPUT{$host} = "$method copy $SOURCE_FILE to $TARGET_DIR/$TARGET_FILE\n";
	   	} else {
	   		$OUTPUT{$host} = "$method copy $SOURCE_FILE to $TARGET_FILE\n";
	   	}

			die usage("Must Specify SOURCE_FILE") 		unless $SOURCE_FILE;
			die usage("Must Specify TARGET_FILE") 		unless $TARGET_FILE;
			$ftp->put($SOURCE_FILE,$TARGET_FILE) unless $NOEXEC;
			my(@files)=$ftp->ls();
	   	my($found)="FALSE";
	   	foreach (@files) { next unless $TARGET_FILE eq $_; $found="TRUE"; last; }
	   	$ftp->close();
	   	if( $found eq "TRUE" ) {
	   		$OUTPUT{$host} .= "Copy of $TARGET_FILE Successfull\n";
	   	} else {
	   		$OUTPUT{$host} .=  "failure : $TARGET_FILE Copy Failed \n"
	   			. "Current Directory is ",$ftp->pwd(),"\n"
					. "\tDirectory Contains: \n\t".join("\n\t",$ftp->ls()),"\n";
   		}

	} elsif( $ACTION eq "LS" or $ACTION eq "RENAME" or $ACTION eq "REMOVE" ) {
			$TIMEOUT=120 unless $TIMEOUT;

			my(%args);
			$args{Debug}=1 if $DEBUG_DOHOST;
	   	print __LINE__," Processing ACTION=LS new Net::myFTP".$NL if $DEBUG_DOHOST;
			my($ftp) = Net::myFTP->new($host,METHOD=>$method,%args);
	   	print __LINE__," Done new Net::myFTP - $ftp".$NL if $DEBUG_DOHOST;
			if( ! $ftp )  {
	      	print __LINE__," Failed To Connect: ".$NL if $DEBUG_DOHOST;
					$OUTPUT{$host} = "Failed To Connect To $host";
	      		push @warnings, $OUTPUT{$host};
	      		return;
	   	}

	   	print __LINE__," myftp->login($login)\n" if $DEBUG_DOHOST;
	   	my($rc,$err)=	$ftp->login($login,$password);
	   	if( ! $rc ) {
	   		chomp $err;
	   		$OUTPUT{$host} = $err || "Failed To Login";
      		return;
	   		# return mywarn( $host, $err ) unless $rc;
	   	}

	   	$ftp->ascii();
	   	print __LINE__," myftp->cwd($TARGET_DIR)\n" if $TARGET_DIR and $DEBUG_DOHOST;
	   	$ftp->cwd($TARGET_DIR) if $TARGET_DIR;

			if( $ACTION eq "LS" ) {
				my(@files);
				if( $TARGET_FILE ) {
					print __LINE__," myftp->ls($TARGET_FILE)\n" if $DEBUG_DOHOST;
					@files=$ftp->ls($TARGET_FILE);
				} else {
					print __LINE__," myftp->ls($TARGET_DIR)\n" if $DEBUG_DOHOST;
					@files=$ftp->ls($TARGET_DIR);
				}
				$OUTPUT{$host} = join(" ",@files)."\n";
				$ftp->close();
				if( $#files<0 ) {
					$OUTPUT{$host} .= "Returning failure : no file found\n";
					return "failure : No File Found\n";
				}
		   	if( $TARGET_FILE ) {
		   		if( $files[0] eq $TARGET_FILE ) {
		   			$OUTPUT{$host} .= "Found File\n";
		   			return;
		   		}
		   	#} else {
		   	#	$OUTPUT{$host} .= "Found ".($#files+1)." Files\n";
		   	}
			} elsif( $ACTION eq "RENAME" ) {
			   	my(@x)=$ftp->rename($SOURCE_FILE,$TARGET_FILE);
			   	$OUTPUT{$host} = "rename succeeded";
			   	return;
			} elsif( $ACTION eq "REMOVE" ) {
			   	my(@x)=$ftp->delete($TARGET_FILE);
			   	$OUTPUT{$host} = "delete succeeded";
			   	return;
			}

	} elsif( $ACTION eq "RUN" ) {
			$TIMEOUT=120 unless $TIMEOUT;
			if( $method eq "FTP" ) {
					$OUTPUT{$host} = $FS."host $host: method=FTP - skipping".$FE;
					push @warnings, "host $host: method=FTP - skipping";
					return;
			}

	   	if( $method eq "NONE" ) {
	   			$OUTPUT{$host} = $FS."host $host: method=NONE - skipping".$FE;
					push @warnings, "host $host: method=NONE - skipping";
					return;
	   	}
			die usage("Must Pass COMMAND if ACTION=RUN") 	unless $COMMAND;
	   	# print "$host : login=$login - method=$method running command $COMMAND\n" if $DEBUG_DOHOST;
	   	do_rsh_method($method);
	   	my($dat)= do_rsh(-hostname=>$host, -command=>$COMMAND, -debug=>$DEBUG_DOHOST, -noexec=>$NOEXEC,
	   			-login=>$login, -password=>$password);
	   	print __LINE__," do_rsh(Host=$host Login=$login Cmd=$COMMAND) returned $dat".$NL if $DEBUG_DOHOST;

			if( $dat=~/^failure/ ) {
				if ( $dat =~ /connection timed out/ ) {
					# not much you can do here
					print "host $host connection timed out".$NL;
					$OUTPUT{$host} =$FS."host $host connection timed out".$FE;
					return;
				} elsif ( $dat =~ /not found/ ) {
					# oops... try $COMMAND_2 and then $COMMAND_3
					if( defined $COMMAND_2 ) {
						$cmd=$COMMAND_2;
						$dat=do_rsh(-login=>$login, -hostname=>$host, -command=>$cmd, -debug=>$DEBUG_DOHOST);
					}
					if ( $dat =~ /not found$/ ) {
						# oops... try $COMMAND_2 and then $COMMAND_3
						if( defined $COMMAND_3 ) {
							$cmd=$COMMAND_3;
							$dat=do_rsh(-login=>$login, -hostname=>$host, -command=>$cmd, -debug=>$DEBUG_DOHOST);
						}
						if ( $dat =~ /not found$/ ) {
							print "host $host: command $COMMAND returned $dat".$NL;
							$OUTPUT{$host} =$FS."host $host: command $COMMAND returned $dat".$FE;
							return;
						}
					}
				#} else {
				#	print "DBG DBG: $dat!\n";
				}
			}
			$OUTPUT{$host} = $dat;
	} else {
		die "ERROR : INVALID ACTION $ACTION\n";
	}
}

sub red {
	if( $HTML ) {
		return "<FONT COLOR=RED>".join("",@_)."</FONT>";
	} else {
		return join("",@_);
	}
}

__END__

=head1 NAME

pugtet_file.pl - unix file manager

=head2 SYNOPSIS

Allows you to do a bunch of things on all your unix systems.  This should supersceed all the other files in
the rdist directory.

=head2 EXAMPLE

 perl putget_file.pl --DEBUG -ACTION=RUN -COMMAND=id

will Run the unix command "id" on all hosts that are in your unix_passwords.dat file and
have COMM_METHOD = SSH or RSH

   perl putget_file.pl -ACTION=PUT --SOURCE_FILE=authorized_keys2 --TARGET_FILE=authorized_keys --TARGET_DIR=.ssh
    perl putget_file.pl -ACTION=RUN -COMMAND="chmod 700 .ssh"

Example to collect all .rhosts files from systems home directories for future use (stored in data/system_information_data)

   perl putget_file.pl -ACTION=STORE --SOURCE_FILE=.rhosts --TARGET_FILE=rhosts --RUNMETHOD=rsh

=head2 TEST PLAN

Test plan for full backwards compatibility

a)    put a file onto all systems   - using each of the methods

      perl putget_file.pl -ACTION=RENAME --RUNMETHOD=ssh --SOURCE_FILE=delete_me --TARGET_FILE=delete_me

   see that the file is there

      perl putget_file.pl -ACTION=LS --RUNMETHOD=ssh --TARGET_FILE=delete_me

pwd
cwd /
pwd home
file list

	perl putget_file.pl -ACTION=LS --RUNMETHOD=ssh --TARGET_FILE=delete_me

fspec list

rename file

      perl putget_file.pl -ACTION=RENAME --RUNMETHOD=ssh --SOURCE_FILE=delete_me --TARGET_FILE=delete_me2

remove file
   mkdir
   rmdi

perl putget_file.pl --TARGET_DIR=/export/home --ACTION=LS --DEBUG

=head2 HOW TO TEST CONNECTIVITY

This program is also a connectivity tester.  Run

putget_file.pl --TESTMODE --TESTMODRPT=file.html --HTML
or
putget_file.pl --TESTMODE --TESTMODRPT=file.html --HTML --TESTMODEUPDATE
  to actually update the config files

