exec sp_executesql @stmt=N'begin try
declare @jobcount bigint;
declare @processorcount int;
use msdb;
select @jobcount = count(distinct job_id) from sysjobs;

use master;
Select  @processorcount = count(*) from sys.dm_os_schedulers where is_online = 1 and scheduler_id < 255

select 0 as error_no
,       convert(nvarchar,login_time) as value_col1
,       0 as error_state
,       convert(sysname, serverproperty(''collation'')) as value_col2
,       1 as pos
from sys.sysprocesses where spid=1
union
select 0 as error_no
,       @@servername as value_col1
,       0 as error_state
,       case when convert(sysname, serverproperty(''IsClustered'')) = ''0'' then ''No'' else ''Yes'' end as value_col2
,       2 as pos
union
select 0 as error_no
,       convert(sysname, serverproperty(''ProductVersion'')) as value_col1
,       0 as error_state
,       case when convert(sysname,serverproperty(''IsFullTextInstalled'')) = ''0'' then ''No'' else ''Yes'' end as value_col2
,       3 as pos
union
select 0 as error_no
,       convert(sysname, serverproperty(''edition'')) as value_col1
,       0 as error_state
,       case when convert(sysname,serverproperty(''IsIntegratedSecurityOnly'')) = ''0'' then ''No'' else ''Yes'' end  as value_col2
,       4 as pos
union
select 0 as error_no
,       convert(sysname, serverproperty(''ProcessID'')) as value_col1
,       0 as error_state
,       case when value = 1 then ''Yes'' else ''No'' end as value_col2
,       5 as pos
from sys.sysconfigures where config = 1548
union
select 0 as error_no
,       convert (nvarchar(20),@jobcount) as value_col1
,       0 as error_state
,       convert (nvarchar(20),@processorcount) as value_col2
,       6 as pos
order by pos
end try
begin catch
select ERROR_NUMBER() as error_no
,       ERROR_SEVERITY() as value_col1
,       ERROR_STATE() as error_state
,       ERROR_MESSAGE() as value_col2
,       -100 as pos
end catch',@params=N'
