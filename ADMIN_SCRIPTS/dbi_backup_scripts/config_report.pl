#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#use vars qw($opt_d $opt_O $opt_U $opt_S $opt_P $opt_o $opt_h $opt_Q $opt_l
#								$opt_n $opt_e $opt_b $opt_v $opt_B $opt_R );
use vars qw(%CONFIG);
use vars qw( $DEBUG $OPTDIAG $OUTDIR $USER $SERVER $PASSWORD $OUTFILE $HTML $STDLOCATION $LOGFILE
                        $ERRFILE $NOBCP $KEEPVERSIONS $BATCHID $TYPE );

use strict;
use Do_Time;
use Getopt::Long;
use Logger;
use CommonFunc;
use DBIFunc;
use File::Basename;
use Repository;

sub usage
{
	print(@_);
	print("config_report.pl -USER=SA_USER -SERVER=SERVER -PASSWORD=SA_PASS [-OUTFILE=FILE]
	-or-
config_report.pl -TYPE=sybase|sqlsvr|all [-OUTDIR]

	-BATCHID=BatchDesignator
	-OPTDIAG=path_to_optdiag # if set run optdiag output
	-OUTDIR=dir			# directory for output - otherwise stdlocation
	-STDLOCATION		# make the output file be in standard location
							# ~gem/data/...
	-OPTDIAG=path_to_optdiag
	-HTML             # output in html format
	-NOBCP 				# Do not bcp out key files
	-ERRORLOG=errorlog   (optional)
	-LOGFILE=sessionlog (optional)
	-KEEPVERSIONS={versions to keep (default=7)}

	Creates a configuration report for a server that can be used
	to recreate that server from scratch.  \n");
	return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";
chdir "C:/" if is_nt();

die(usage("")) if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
#die(usage("Bad Parameter List\n")) unless getopts('U:S:P:o:he:l:Qbdv:B:R:nO:');

die usage("Bad Parameter List\n") unless GetOptions(
			"DEBUG"			=>	\$DEBUG ,
			"OPTDIAG=s"		=>	\$OPTDIAG ,
			"USER=s"			=>	\$USER ,
			"SERVER=s"		=>	\$SERVER ,
			"PASSWORD=s"	=>	\$PASSWORD ,
			"OUTFILE=s"		=>	\$OUTFILE ,
			"HTML"			=>	\$HTML ,
			"OUTDIR=s"		=>	\$OUTDIR ,
			"STDLOCATION"	=>	\$STDLOCATION ,
			"LOGFILE=s"		=>	\$LOGFILE ,
			"ERRFILE=s"		=>	\$ERRFILE ,
			"NOBCP"			=>	\$NOBCP ,
			"KEEPVERSIONS=s"	=>	\$KEEPVERSIONS ,
			"BATCHID=s"		=>	\$BATCHID ,
			"TYPE=s"			=>	\$TYPE );

print "config_report.pl Run At ".localtime(time)."\n";

die "Cant pass OUTDIR and STDLOCATION" if $OUTDIR and $STDLOCATION;
die "Cant pass OUTDIR and OUTFILE" if $OUTDIR and $OUTFILE;

if( ! $TYPE ) {
	die(usage("Must pass sa password\n"))  unless defined $PASSWORD;
	die(usage("Must pass server\n")    )   unless defined $SERVER;
	die(usage("Must pass sa username\n"))  unless defined $USER;
	# die(usage("Must pass -OUTFILE if -b is passed\n")) if defined $NOBCP and ! defined $OUTFILE;
} else {
	die "Cant pass -OUTFILE if you pass -TYPE\n" if $OUTFILE and ! $SERVER;
}

$KEEPVERSIONS=7 unless $KEEPVERSIONS;

logger_init(-logfile=>  $LOGFILE,
            -errfile=>  $ERRFILE,
            -debug =>   $DEBUG,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

my($NL)="\n";
$NL="<br>\n" if defined $HTML;

my($STHEAD,$ENDHEAD)=("","\n");
($STHEAD,$ENDHEAD)=("<H2>","</H2>\n") if defined $HTML;
if( $STDLOCATION and $SERVER and ! $OUTFILE ) {
	my($rootdir)= get_gem_root_dir()."/data/BACKUP_LOGS/";
	mkdir($rootdir."$SERVER",0777) unless -d $rootdir."$SERVER";
	mkdir($rootdir."$SERVER/audits",0777) unless -d $rootdir."$SERVER/audits";
	$OUTFILE=$rootdir."$SERVER/audits/$SERVER.".today();
}

if( defined $SERVER and ! $USER and ! $PASSWORD ) {
	($USER,$PASSWORD)=get_password(-type=>'sqlsvr',-name=>$SERVER) unless $USER;
	($USER,$PASSWORD)=get_password(-type=>'sybase',-name=>$SERVER) unless $USER;
	$OUTFILE="$OUTDIR/$SERVER.".today() if $OUTDIR and ! $OUTFILE;
	run_an_audit($SERVER,$USER,$PASSWORD);
} elsif( $TYPE ) {
	my($rootdir)= get_gem_root_dir()."/data/BACKUP_LOGS/";
	if( is_nt() and ($TYPE eq "sqlsvr" or $TYPE eq "all")) {
		foreach my $srv (get_password(-type=>'sqlsvr')) {
			if( $OUTDIR ) {
				$OUTFILE=$OUTDIR."/$srv.".today();
			} else {
				die "Root directory for backupscripts is $rootdir and doenst exist\n" unless -d $rootdir;
				mkdir($rootdir."$srv",0777) unless -d $rootdir."$srv";
				mkdir($rootdir."$srv/audits",0777) unless -d $rootdir."$srv/audits";
				$OUTFILE=$rootdir."$srv/audits/$srv.".today();
			}
			my($login,$password)=get_password(-type=>'sqlsvr',-name=>$srv);
			run_an_audit($srv,$login,$password);
		}
	}
	if( $TYPE eq "sybase" or $TYPE eq "all" ) {
		foreach my $srv (get_password(-type=>'sybase')) {
			if( $OUTDIR ) {
				$OUTFILE=$OUTDIR."/$srv.".today();
			} else {
				die "Root directory for backupscripts is $rootdir and doenst exist\n" unless -d $rootdir;
				mkdir($rootdir."$srv",0777) unless -d $rootdir."$srv";
				mkdir($rootdir."$srv/audits",0777) unless -d $rootdir."$srv/audits";
				$OUTFILE=$rootdir."$srv/audits/$srv.".today();
			}
			my($login,$password)=get_password(-type=>'sybase',-name=>$srv);
			run_an_audit($srv,$login,$password);
		}
	}
} else {
	run_an_audit($SERVER,$USER,$PASSWORD);
}
exit(0);

sub run_an_audit {
	my($server,$login,$password)=@_;
	if( ! $server ) {
		warn "Must pass server to audit server\n";
		return;
	}
	if( ! $password ) {
		warn "Must pass password to audit server $server\n";
		return;
	}
	info("Starting Audit of Server $server\n");
	info("\tConnecting To Dataserver\n");

#print "DBG DBG Start dbi_connect\n";
	my($rc)=dbi_connect(-srv=>$server,-login=>$login,-password=>$password,-debug=>$DEBUG,-die_on_error=>0);
	#print "DBG DBG End dbi_connect $rc\n";
	if( ! $rc ) {
		print("Cant connect to $server as $login\n");
		return;
	}
	dbi_set_mode("INLINE");

	#my($SERVER_TYPE,$VERSION)=dbi_db_version();

	# GET DB INFO
	my($server_type,@databaselist)=dbi_parse_opt_D("%",1);
	print "DB LIST=",join(" ",@databaselist),"\n" if $DEBUG;

	my(%alldb);
	foreach ( dbi_query(-db=>"master",-query=>"select name from sysdatabases",-debug=>$DEBUG) ) {
		my($nm)=dbi_decode_row($_);
		$alldb{$nm}="BAD";
	}
	foreach(@databaselist) { $alldb{$_}="OK"; }

	# ok i guess we can log in and use databases.... so... start output file
	if( defined $OUTFILE ) {
		archive_file($OUTFILE, $KEEPVERSIONS);
		my($rc)=open(OUTFILE,">$OUTFILE");
		if( ! $rc ) { print("Cant open $OUTFILE: $!\n"); return; }
		info("\tOutputFile=$OUTFILE\n");
	}

	output( "<H2>" ) if defined $HTML;
	output( "$server_type SERVER CONFIGURATION REPORT" );
	output( "</H2>" ) if defined $HTML;
	output( "\n" );
	output( "Created by: config_report.pl ".$NL );
	output( "Server: $server",$NL );
	output( "Server Type=$server_type" ,$NL);
	#output( "Server Version=$VERSION",$NL);
	output( "Report Run At: ".localtime().$NL );
	output( "Output File $OUTFILE",$NL ) if defined $OUTFILE;
	output( $NL );
	dbi_msg_ok(1000);

	pr_query("master","select 'version='+\@\@version","$server_type VERSION");
	pr_query("master","exec sp__helpdb \@dont_format=\'Y\'","DATABASE LAYOUT");
	pr_query("master","exec sp__helpdbdev \"ND\"","DEVICE BY DB");
	#dbi_msg_exclude(1000);

	pr_query("master","exec sp__configure","CONFIGURATION OPTIONS");
	pr_query("master","exec sp__helpmirror","MIRRORING INFORMATION")
		if $server_type eq "SYBASE";
	pr_query("master","exec sp__vdevno 'Y','Y'","VIRTUAL DEVICE LAYOUT")
		unless $server_type eq "SQLSERVER";
	pr_query("master","exec sp__diskdevice","DISK LAYOUT");
	pr_query("master","exec sp__dumpdevice","DUMP DEVICE LAYOUT");
	pr_query("master","exec sp__helplogin \@filter=\"A\"","SPECIAL SYSTEM LOGINS")
		if $server_type eq "SYBASE";
	pr_query("master","exec sp__helplogin \@dont_format='Y'","SYSTEM LOGINS")
		if $server_type eq "SQLSERVER";

	line();
	output( $STHEAD,"REVERSE ENGINEERING PROCEDURES",$ENDHEAD  );
	line();
	pr_query("master","exec sp__revdb");
	pr_query("master","exec sp__revdevice");
	pr_query("master","exec sp__revmirror")
		unless $server_type eq "SQLSERVER";
	pr_query("master","exec sp__revlogin");
	pr_query("master","exec sp__revrole")
		unless $server_type eq "SQLSERVER";

	foreach my $db (@databaselist) {
		if( ! dbi_use_database($db) ) {
			warn("Cant Use Database $db on $server\n");
			next;
		}
		pr_query($db,"exec sp__revsegment") if $server_type eq "SYBASE";
		pr_query($db,"exec sp__revgroup") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revuser") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revalias") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revtype") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revbindings") if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revrule") 	if $server_type eq "SYBASE";;
	}

	#
	# PRINT TABLES
	#
	line();
	output( $STHEAD,"SELECTS FROM SYSTEM TABLES",$ENDHEAD  );
	line();
	pr_query("master","select type,id,csid,status,name,description from syscharsets");
	pr_query("master","select * from sysconfigures");
	if( 	$server_type eq "SQLSERVER" ) {
		pr_query("master","select name,dbid,mode,status,status2,crdate,reserved,category,cmptlevel,filename,version from sysdatabases");
	} else {
		pr_query("master","select * from sysdatabases");
	}

	pr_query("master","select * from sysdevices");

	if( 	$server_type eq "SQLSERVER" ) {
		pr_query("master","select STATUS,CREATEDATE,UPDATEDATE,ACCDATE,TOTCPU,TOTIO,SPACELIMIT,TIMELIMIT,RESULTLIMIT,NAME,DBNAME,LANGUAGE,DENYLOGIN,HASACCESS,ISNTNAME,ISNTGROUP,ISNTUSER,SYSADMIN,SECURITYADMIN,SERVERADMIN,SETUPADMIN,PROCESSADMIN,DISKADMIN,DBCREATOR,BULKADMIN,LOGINNAME from syslogins");
	} else {
		pr_query("master","select * from syslogins");
	}
	pr_query("master","select * from sysloginroles") if $server_type eq "SYBASE";;
	pr_query("master","select * from sysremotelogins");
	pr_query("master","select * from sysservers");
	pr_query("master","select * from sysusages order by vstart") if $server_type eq "SYBASE";
	pr_query("master","select * from sysusages order by dbid, lstart") if $server_type eq "SYBASE";

	if( ! defined $NOBCP ) {
		bcp_table("master","syscharsets",$OUTFILE,$server,$login,$password,$server_type);
		bcp_table("master","sysconfigures",$OUTFILE,$server,$login,$password,$server_type);
		bcp_table("master","sysdatabases",$OUTFILE,$server,$login,$password,$server_type);
		bcp_table("master","sysdevices",$OUTFILE,$server,$login,$password,$server_type);
		bcp_table("master","syslogins",$OUTFILE,$server,$login,$password,$server_type);
		bcp_table("master","sysloginroles",$OUTFILE,$server,$login,$password,$server_type) if $server_type eq "SYBASE";
		bcp_table("master","sysremotelogins",$OUTFILE,$server,$login,$password,$server_type);
		bcp_table("master","sysservers",$OUTFILE,$server,$login,$password,$server_type);
		bcp_table("master","sysusages",$OUTFILE,$server,$login,$password,$server_type) if $server_type eq "SYBASE";
	}

	my($rc1151)=dbi_query(-db=>"master",-query=>"select name from sysobjects where name='systimeranges'",-debug=>$DEBUG);
	my(@is_1151)=dbi_decode_row($rc1151);
	if( $#is_1151>=0 ) {
		pr_query("master","select * from sysresourcelimits") if $server_type eq "SYBASE";
		pr_query("master","select * from systimeranges ") if $server_type eq "SYBASE";
		bcp_table("master","sysresourcelimits",$OUTFILE,$server,$login,$password,$server_type)
			if ! defined $NOBCP and $server_type eq "SYBASE";
		bcp_table("master","systimeranges",$OUTFILE,$server,$login,$password,$server_type)
			if ! defined $NOBCP and $server_type eq "SYBASE";
	}
	dbi_disconnect();

	if( defined $OUTFILE and defined $OPTDIAG and $server_type eq "SYBASE" ) {
		#my($d)=dirname($OUTFILE);
		#my($b)=basename($OUTFILE);
		foreach my $db (@databaselist) {
			next if $db eq "master" or $db eq "model" or $db eq "tempdb"  or $db eq "sybsystemdb";
			my($f)=$OUTFILE.".OPTDIAG.".$db;
			print "**> OPTDIAG OUTPUT TO $f\n";
			output( "**> OPTDIAG OUTPUT TO $f\n" );
			archive_file($f, $KEEPVERSIONS);
			$OPTDIAG = "optdiag" unless $OPTDIAG;
			my($cmd)="$OPTDIAG statistics $db -o$f -U$login -S$server -P$password";
			my($cmdnopass)="$OPTDIAG statistics $db -o$f -U$login -S$server -PXXX";
			output( "running $cmdnopass\n" );
			print $cmdnopass."\n";
			my($rc)=open( CMD, $cmd." |");
			if( ! $rc ) {
				my($err)=$!;
				if( $err =~ /No such file or directory/ ) {
					warn "Cant find optdiag in the path ($ENV{Path}) Please Set With -O or using the GEM interface\n";
				}
				warn "Cant run '".$cmdnopass."' : $err\n";
			}
			foreach (<CMD>) {
				print "optdiag> ",$_,"\n";
			}
			close(CMD);
		}
	} else {
		output("Not running optdiag as server type != SYBASE ($server_type)\n")
			if $server_type ne "SYBASE";
		output("Not running optdiag as no output file was passed in with $OUTFILE\n")
			unless $OUTFILE;
	}

	close(OUTFILE) if $OUTFILE;
	info("Audit of $server Completed\n");
}

sub line
{
	if( defined $HTML ) {
		# output( "<hr>\n" );
	} else {
		output( "---------------------------------------------\n");
	}
}
my($bcp);
my($loginpass);

sub bcp_table
{
	my($db,$table,$outfile,$server,$login,$password,$server_type)=@_;

	if( ! $outfile ) {
		warn "WARNING - CANT BCP TABLE $table AS OUTFILE IS NOT DEFINED\n";
		return;
	}
	# modify $OUTFILE for filename
	if( ! defined $bcp ) {
		if( $server_type eq "SYBASE" ) {
			$bcp=$ENV{SYBASE}."/ASE-15_0/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/ASE-15_0/bin/bcp";
			$bcp=$ENV{SYBASE}."/OCS-15_0/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/OCS-15_0/bin/bcp";
			$bcp=$ENV{SYBASE}."/ASE-12_5/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/ASE-12_5/bin/bcp";
			$bcp=$ENV{SYBASE}."/OCS-12_5/bin/bcp" if ! $bcp and  -x $ENV{SYBASE}."/OCS-12_5/bin/bcp";
			$bcp=$ENV{SYBASE}."/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/bin/bcp";

			# we could be on nt
			if( ! $bcp ) {
				$bcp=$ENV{SYBASE}."/ASE-15_0/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/ASE-15_0/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/OCS-15_0/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/OCS-15_0/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/ASE-12_5/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/ASE-12_5/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/OCS-12_5/bin/bcp.exe" if ! $bcp and  -x $ENV{SYBASE}."/OCS-12_5/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/bin/bcp.exe";
			}

			if( $bcp eq "" and -d $ENV{SYBASE} ) {
				opendir(DIR,$ENV{SYBASE})
					or die("Can't open directory (\$SYBASE) $ENV{SYBASE} : $!\n");
				my(@dirlist) = grep((!/^\./ and -d "$ENV{SYBASE}/$_") ,readdir(DIR));
				closedir(DIR);
				foreach ( @dirlist ) {
					if( -x "$_/bin/bcp" ) {
						$bcp = "$_/bin/bcp";
						last;
					}
				}
			}
			$loginpass="-U$login -PXXX";
		} else {
			foreach ( split(/;/,$ENV{PATH} )) {
				print "TESTING PATH $_\n" if $DEBUG;
				next unless -d $_;
				if( -e "$_/osql.exe" ) {
					print "FOUND OSQL in $_\n" if $DEBUG;
					$bcp="./bcp.exe";
					#$bcp=~s.\\.\/.g;
					my($rc)=chdir($_);
					if( ! $rc ) {
						warn "Cant chdir $_ $!\n";
						next;
					}
					last;
				}
			}
			$loginpass="-T ";
		}
	}

	if( ! defined $bcp ) {
		output("BCP NOT FOUND - CANT COPY TABLE $table\n");
		return;
	}

	my($filename)=$outfile.".".$table."_native.bcp";
	my($cmd)="$bcp $db..$table out $filename -n  -S$server $loginpass\n";
	print "Copying Table $table (native mode)\n";
	output("Copying Table $table to File $filename (native mode)\n");
	output(" == $cmd\n");
	print $cmd;
	$cmd=~s/PXXX\n/P$password\n/;
	run_bcp_cmd($cmd);

	($filename)=$outfile.".".$table."_char.bcp";
	($cmd)="$bcp $db..$table out $filename -c -t~ -S$server $loginpass\n";
	print "Copying Table $table (char mode)\n";
	output("Copying Table $table to File $filename (char mode)\n");
	output(" == $cmd\n");
	print $cmd;
	$cmd=~s/PXXX\n/P$password\n/;
	run_bcp_cmd($cmd);
}

sub run_bcp_cmd {
	my($cmd)=@_;
	open(CMD,"$cmd |") or die "Cant run $cmd $!\n";
	while(<CMD>) {
		chop;
		next if /^\s*$/;
		next if /^Starting copy.../ or /^Network packet size/ or /^Clock Time/;
		print ">> $_\n";
	}
	close(CMD);
}
sub pr_query
{
	my($db,$query,$title)=@_;
	my(@results)= dbi_query(-db=>$db,-query=>$query,-print_hdr=>1,-add_batchid=>1,-debug=>$DEBUG);
	if( $#results<0 ) {
		output("$title: No results for query $query\n")	if $title;
		output("No results for query $query\n")	unless   $title;
	}
	print "Query returned $#results Rows\n" if defined $DEBUG;

	line();
	if( defined $title ) {
		$title=~s/\s+$//;
		output( $STHEAD,"$title",$ENDHEAD );
	} else {
		output( $STHEAD,"QUERY: $query IN DATABASE $db",$ENDHEAD );
	}
	line();

	# my(@rc);
	# foreach (@results) {
		# my(@dat)=dbi_decode_row($_);
		# $dat=~s/\s+\~\~/\~\~/g;
		# $dat=~s/\s+$//g;
		# push @rc,$dat;
	# }

	my(@widths)=(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	#foreach my $dat ( @rc ) {
	foreach my $dat ( @results ) {
		# next unless $dat =~ /^\s*BATCH/;
		my @rcdat=dbi_decode_row($dat);
		shift @rcdat;
		my $count=0;
		foreach ( @rcdat ) {
			$widths[$count] = length($_) if $widths[$count] < length($_);
			$count++;
		}
	}

	my($fmt)="";
	foreach (@widths) {
		last if $_ == 0;
		$fmt.="%-".$_."s ";
	}

	$fmt.="\n";
	output( "<PRE><b>" ) if defined $HTML;
	my($rowcnt)=0;
	foreach( @results ) {
		$rowcnt++;
		my @rcdat=dbi_decode_row($_);
		if( $rowcnt==1 ) {
			foreach(@rcdat){
				$_=uc;
			}
		}
		if( $#rcdat==0 ) {
			chomp $rcdat[0];
			print OUTFILE $rcdat[0],"\n" if defined $OUTFILE;
			info($rcdat[0],"\n") unless defined $OUTFILE;
			if( $rowcnt==1 ) {
				print "</b>" if defined $HTML;
			}
			next;
		}
		shift @rcdat;
		printf OUTFILE ($fmt,@rcdat) if defined $OUTFILE;
		die "FMT IS UNDEFINED" unless $fmt;
		die "RCDAT IS UNDEFINED" if @rcdat<0;
		infof($fmt,@rcdat) unless defined $OUTFILE;
		if( $rowcnt==1 ) {
			print "</b>" if defined $HTML;
		}

		#chomp;
		#if( /^\s*BATCH/ ) {
			#s/^\s*BATCH\s+\d+//;
			## next if /^\s*$/;
			#my(@rcdat)=dbi_decode_row($_);
			#shift @rcdat;
			#next if $rcdat[0] =~ /^txt/;
			#printf OUTFILE ($fmt,@rcdat) if defined $OUTFILE;
			#infof($fmt,@rcdat) unless defined $OUTFILE;
		#} else {
			## next if /^\s*$/;
			#next if /^txt/;
			#output( $_,"\n");
		#}
	}
	output( "</PRE>" ) if defined $HTML;
	output("\n");
}

sub output
{
	if( defined $OUTFILE ) {
		print OUTFILE @_ ;
	} else {
		info(@_);
	}
}

sub archive_file {
	my($from, $versions)=@_;
	return unless -r $from;
	my($curdate)=do_time(-fmt=>'yyyymmdd_hhmiss');
	my($tofile)=$from.".".$curdate;
	print("Archived $from\n" );
	print("      to $tofile\n" ) if defined $DEBUG;
	rename( $from , $tofile ) or die "Cant rename $from to $tofile : $!\n";

	my($dir)=dirname($from);
	my($file)=basename($from);
	opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
	my(@dirlist) = sort grep(/^$file\.\d\d\d\d\d\d\d\d/,readdir(DIR));
	closedir(DIR);

	if( $#dirlist>$versions ) {
		$#dirlist-=$versions;
		foreach (sort @dirlist) {
			print("Removing $dir/$_\n" );
			unlink $dir."/".$_;
		}
	}
	return $tofile;
}

__END__

=head1 NAME

config_report.pl - Server Configuration Report

=head2 SYNOPSIS

Creates a configuration report for a server that can be used to recreate that server from scratch.

=head2 AUTHOR

By Ed Barlow

=head2 USAGE

config_report.pl -USER=SA_USER -SERVER=SERVER -PASSWORD=SA_PASS [-OUTFILE=FILE]
        -or-
config_report.pl -TYPE=sybase|sqlsvr|all [-OUTDIR]

	-BATCHID=BatchDesignator
	-OPTDIAG=path_to_optdiag # if set run optdiag output
	-OUTDIR=dir              # directory for output - otherwise stdlocation
	-STDLOCATION             # make the output file be in standard location ~gem/data/...
	-OPTDIAG=path_to_optdiag
	-HTML             		 # output in html format
	-NOBCP                   # Do not bcp out key files
	-ERRORLOG=errorlog   (optional)
	-LOGFILE=sessionlog (optional)
	-KEEPVERSIONS={versions to keep (default=7)}

Creates a configuration report for a server that can be used to recreate that server from scratch.

=head2 DESCRIPTION

This program creates a configuration report for a server that can be used to recreate that server from scratch.
This is intended as documentation for use by the system administrator in case your server crashes or has a problem.
This program should produce virtually everything about your server that you might care about except user object DDL.

Requires: Requires the extended stored procedure library

=head2 WHAT IT PRINTS
	SERVER:  srvname
	@@version
	helpdb
	configure
	helpmirror
	vdevno
	helpdevice
	helplogin
	helpuser by db
	all the reverse engineering routines

=cut
