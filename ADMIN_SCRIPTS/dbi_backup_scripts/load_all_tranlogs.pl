#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use File::Basename;
use MlpAlarm;
use DBIFunc;
use Net::myFTP;
use CommonFunc;
use Logger;
use Repository;
use Sys::Hostname;

use vars qw($opt_J $opt_Y $opt_n $opt_U $opt_S $opt_P $opt_D $opt_d $LIBDEBUG $opt_i $BATCH_ID $MINFILES
$opt_h $opt_t $opt_r $DELETE %CONFIG $opt_X $opt_M $opt_O $opt_z $opt_R $TESTRUN $RENAME $SKIPPRIOR $XFER_FROM_DB $XFER_TO_DB
$FILEPATERN $NOSKIPDONE $SKIPDONE $MICROSOFT $NOSKIPPRIOR $NOSKIPDOG741NE $HOSTNAME $UNIXLOGIN $UNIXPASSWORD $UNIXMETHOD $FULL
$SOURCESERVER $COMPRESSED $SHOWDETAILS $STOP_ON_ERROR $ISQL_FILE $NOEXEC);

my($load_state,$load_msg);
my(@month_name)=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

# Copyright (c) 2003-8 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_,"\n";
   print "load_all_tranlogs.pl
   -USER=SA_USER      -SERVER=SERVER      -PASSWORD=SA_PASS
   -DATABASE=db       -INDIR=file_root    -TYPE=Type
   -DESTDIR=dest_dir  -JOB=job            -HOSTNAME
   -UNIXLOGIN=xxx     -DELETE             -UNIXPASSWORD=xxx
   -UNIXMETHOD=SSH|RSH|FTP     -STRIPES=num --MINFILES=num
   -SOURCESERVER - (for reporting only - this is the source server)
   -FULL   - full database loads (only on microsoft for now)
   -DEBUG debug mode  --FILEPATERN=pat --COMPRESSED (sybase compression)
   -SKIPPRIOR skip errors ( also --NOSKIPPRIOR)
   -SKIPDONE skip files with .done extension --NOSKIPDONE
   -MICROSOFT microsoft sql svr format (see below)
   -RENAME rename files to .done extension  --LIBDEBUG (debug libs too)
   -INDIR identifies a directory / files as seen from this program
   -DESTDIR identifies the same directory/files as seen by remote server
      (or pass HOSTNAME)  --STOP_ON_ERROR --ISQL_FILE=file --NOEXEC

   Will Load, in order, tranlogs that have the following name format
      {SERVER}.{DATABASE}.dbdump.yyyymmdd-hhmmss
   or {DB}_backup_yyyymmddhhmm.TRN (sql server 2005 - if --MICROSOFT passed)
   or {DB}_tlog_yyyymmddhhmm.TRN (sql server format - if --MICROSOFT passed)\n";
   return "\n";
}

$| =1;
my($curdir)=cd_home();
my($system_state)="COMPLETED";
my($system_msg)="(failed)";

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
        "SERVER=s"  		=> \$opt_S,
        "SOURCESERVER=s"=> \$SOURCESERVER,
        "USER=s" 			=> \$opt_U ,
        "DATABASE=s"   	=> \$opt_D ,
        "MINFILES=s"   	=> \$MINFILES ,
        "ISQL_FILE=s"   	=> \$ISQL_FILE ,
        "PASSWORD=s"   	=> \$opt_P ,
        "FULL"   			=> \$FULL ,
        "JOB=s"   		=> \$opt_J ,
        "TESTRUN"			=> \$TESTRUN,
        "HOSTNAME=s"		=> \$HOSTNAME,
        "UNIXLOGIN=s"	=> \$UNIXLOGIN,
        "UNIXMETHOD=s"	=> \$UNIXMETHOD,
        "FILEPATERN=s"	=> \$FILEPATERN,
        "SKIPPRIOR"		=> \$SKIPPRIOR,
        "SHOWDETAILS"		=> \$SHOWDETAILS,
        "NOSKIPPRIOR"	=> \$NOSKIPPRIOR,
        "COMPRESSED"		=> \$COMPRESSED,
        "DELETE"			=> \$DELETE,
        "STOP_ON_ERROR"	=> \$STOP_ON_ERROR,
        "NOEXEC"			=> \$NOEXEC,
        "UNIXPASSWORD=s"=> \$UNIXPASSWORD,
        "INDIR=s"    	=> \$opt_i ,
        "TYPE=s"    		=> \$opt_M ,
        "STRIPES=s"   	=> \$opt_n ,
        "MICROSOFT"   	=> \$MICROSOFT ,
        "BATCH_ID=s"		=> \$BATCH_ID,
        "SKIPDONE"    	=> \$SKIPDONE ,
        "NOSKIPDONE"    => \$NOSKIPDONE ,
        "RENAME"			=> \$RENAME,
        "XFER_FROM_DB=s"	=> \$XFER_FROM_DB,
        "XFER_TO_DB=s"		=> \$XFER_TO_DB,
        "DESTDIR=s"		=> \$opt_R,
        "LIBDEBUG"      => \$LIBDEBUG,
		  "DEBUG"         => \$opt_d );

undef $SKIPDONE 	if $NOSKIPDONE;
undef $SKIPPRIOR 	if $NOSKIPPRIOR;

if( $ISQL_FILE ) {	open(ISQLF,">".$ISQL_FILE) or die "CANT WRITE $ISQL_FILE $!\n"; }

die "INCOMPATIBLE OPTIONS --MINFILES and --SKIPDONE : --MINFILES ONLY IS USEFUL IF YOU ARE NOT SKIPPING DONE FILES\n"
	if $MINFILES and $SKIPDONE;

$BATCH_ID="LoadAllTranlogs" unless defined $BATCH_ID;
print $CONFIG{COMMAND_RUN},"\n";

sub pgheader
{
	my($VERSION,$SHTVER)=get_version();
	info( "load_all_tranlogs.pl: $VERSION \nStarted at ".localtime(time)."\n" );
	info( "run on hostname=",hostname(),"\n" );
	info( "Reading Configfile For Job $opt_J\n" );
	return ($VERSION,$SHTVER);
}

if( defined $opt_J ) {
	%CONFIG=read_configfile( -job=>$opt_J );
	$XFER_FROM_DB = $CONFIG{XFER_FROM_DB} 	unless $XFER_FROM_DB;
	$XFER_TO_DB   = $CONFIG{XFER_TO_DB} 	unless $XFER_TO_DB;

	logger_init(-logfile=>  undef,
	   -errfile=>  undef,
	   -database=> $opt_D,
	   -debug =>   $LIBDEBUG,
	   -system =>  $opt_J,
	   -subsystem =>$opt_J,
	   -command_run => $CONFIG{COMMAND_RUN},
	   -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
	   -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
	   -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

	#
	# GET LOG SHIPPING VARIABLES
	#
	$opt_S=$CONFIG{XFER_TO_SERVER} 	unless defined $opt_S;
	$opt_i=$CONFIG{XFER_TO_DIR} 	unless defined $opt_i;
	$opt_R=$CONFIG{XFER_TO_DIR_BY_TARGET}||$CONFIG{XFER_TO_DIR} unless defined $opt_R;
	$HOSTNAME=$CONFIG{XFER_TO_HOST} unless $HOSTNAME;
	$SOURCESERVER=$CONFIG{SERVER_NAME} 	unless defined $SOURCESERVER;
	if( $HOSTNAME and $CONFIG{XFER_BY_FTP} eq 'y' ) {
		($UNIXLOGIN,$UNIXPASSWORD)=get_password(-type=>"unix",-name=>$HOSTNAME)
			unless $UNIXLOGIN or $UNIXPASSWORD;
		my(%args)=get_password_info(-type=>"unix",-name=>$HOSTNAME);
		if( is_nt() ) {
			$UNIXMETHOD=$args{WIN32_COMM_METHOD} if is_nt();
		} else {
			$UNIXMETHOD=$args{UNIX_COMM_METHOD};
		}
		die "COMM_METHOD NOT DEFINED for SERVER $HOSTNAME\n" unless $UNIXMETHOD;

		if( $UNIXMETHOD eq "NONE" ) {	# hmm cant talk
			print "host $HOSTNAME: METHOD=NONE - skipping\n";
			next;
		}
	}

	$COMPRESSED=1 if $CONFIG{SYBASE_COMPRESSION_LEVEL} =~ /^\d$/	and $CONFIG{SYBASE_COMPRESSION_LEVEL} ne "0";

	if( ! defined $CONFIG{XFER_TO_SERVER} ) {
		foreach (sort keys %CONFIG) { print "$_ $CONFIG{$_} \n"; }
		logdie( "XFER_TO_SERVER does not exist\n" );
	}

	my($server_type)="SYBASE";
	($opt_U,$opt_P)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sybase") if ! defined $opt_U;
	if( defined $opt_U ) {
	  	if( is_nt() ) {
	  			$opt_M="ODBC" unless defined $opt_M;
	  	} else {
	  			$opt_M="Sybase" unless defined $opt_M;
	  	}
	} else {
	  	($opt_U,$opt_P)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sqlsvr");
	  	$opt_M="ODBC";
	  	$server_type="SQLSERVER";
	}
	$RENAME=1;
	if( ! $NOSKIPDONE ) {
		info( "Setting SKIPDONE option\n" );
		$SKIPDONE=1;
	}
} else {
	logger_init(-logfile=>  undef,
            -errfile=>  undef,
            -database=> $opt_D,
            -debug =>   $LIBDEBUG,
            -system =>  $opt_S,
            -subsystem =>$opt_D,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );
}

	my($VERSION,$SHTVER) = pgheader();
	print "Arguments For load_all_tranlogs.pl set to:
			-SERVER           => $opt_S,
			-SOURCESERVER     => $SOURCESERVER,
			-USER             => $opt_U ,
			-DATABASE         => $opt_D ,
			-PASSWORD         => XXX,
			-FULL             => $FULL ,
			-JOB              => $opt_J ,
			-HOSTNAME         => $HOSTNAME,
			-UNIXLOGIN        => $UNIXLOGIN,
			-UNIXMETHOD       => $UNIXMETHOD,
			-DELETE           => $DELETE,
			-UNIXPASSWORD     => XXX,
			-COMPRESSED       => $COMPRESSED,
			-INDIR            => $opt_i ,
			-TYPE             => $opt_M ,
			-STRIPES          => $opt_n ,
			-DEBUG            => $opt_d ,
			-MICROSOFT        => $MICROSOFT ,
			-SKIPDONE         => $SKIPDONE ,
			-SKIPPRIOR        => $SKIPPRIOR,
			-RENAME           => $RENAME,
			-DESTDIR          => $opt_R\n";

	logdie usage("Must pass sa password\n" )	   unless defined $opt_P;
	logdie usage("Must pass server\n" )				unless defined $opt_S;
	logdie usage("Must pass sa username\n" )	   unless defined $opt_U;
	logdie usage("Must pass input root file\n" ) unless defined $opt_i;
	$opt_M = "ODBC" if ! $opt_M and is_nt();
	logdie usage("Must pass server type with -M\n") unless defined $opt_M;
	logdie usage("Must pass directory as seen by target server --DESDIR or --HOSTNAME=\n")
		unless defined $opt_R or defined $HOSTNAME;
	logdie "Type must be Sybase or ODBC"
		unless $opt_M eq "Sybase" or $opt_M eq "ODBC";

	$opt_i=~s/\s+$//;
	undef $MINFILES if $opt_D;

	print "Saving batch info to Alarm Database\n"
		unless defined $FULL;
	if( defined $SOURCESERVER ) {
		MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-SUBKEY=>$SOURCESERVER." Tran Log Load")
			unless defined $FULL;
	} else {
		MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-SUBKEY=>$opt_S." Tran Log Load")
			unless defined $FULL;
	}

	info "Connecting to $opt_M Server $opt_S\n" if defined $opt_d;
	dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -type=>$opt_M, -debug=>$LIBDEBUG)
		or logdie "Cant connect to $opt_M $opt_S as $opt_U\n";
	dbi_msg_exclude("Invalid cursor state");
	my($server_type)="SQLSERVER";
	foreach( dbi_query(-db=>"master", -query=>"select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
		($server_type)=dbi_decode_row($_);
	}
	info("Connected to Server type=$server_type\n") if defined $opt_d;

	dbi_msg_ok(1000);
	dbi_set_mode("INLINE");

my(%ToLoadDb_By_SourceDb);
my(%ToLoadDb_By_SourceDbLc);
my(%SourceDb_By_ToLoadDb);

# removed Job From If Here
my($transfer_is_defined)='FALSE';
if( $XFER_FROM_DB and $XFER_TO_DB and $XFER_FROM_DB ne "*" and $XFER_TO_DB ne "*" ) {
	my(@fr_dblist) = split( /[\,\|]/, $XFER_FROM_DB );
	info "-- From DB List=".join(" ",@fr_dblist)."\n";
	my(@to_dblist) = split( /[\,\|]/, $XFER_TO_DB   );
	info "--   To DB List=".join(" ",@to_dblist)."\n";
	$transfer_is_defined='TRUE';

	logdie "ERROR XFER_FROM_DB=$XFER_FROM_DB\n\tXFER_TO_DB=$XFER_TO_DB\n" if $#fr_dblist != $#to_dblist;
	my($count)=0;
	while ( $count <= $#fr_dblist ) {
		my($fr_db)=$fr_dblist[$count];
		my($to_db)=$to_dblist[$count];
		$count++;
		$ToLoadDb_By_SourceDb{$fr_db}=$to_db;
		$ToLoadDb_By_SourceDbLc{lc($fr_db)}=$to_db;
		$SourceDb_By_ToLoadDb{$to_db}=$fr_db;
	}
}

#
# BUILD DATABASE LIST - Tran Dump only appropriate databases
#
my($whereclause)="";
my( @Opt_D_DBLIST )=split(/[\,\|]/,$opt_D);
if( defined $opt_D ) {
   foreach ( @Opt_D_DBLIST ) {
   	my($tdb)= $ToLoadDb_By_SourceDb{$_} || $_;
   	if( $whereclause eq "" ) {
			$whereclause="and ( name like \'$tdb\' or name = \'$tdb\'";
		} else {
			$whereclause .= " or name like \'$tdb\' or name = \'$tdb\'";
		}
   }
   $whereclause.=")" if $whereclause ne "";
}

debug("processing ignore_list\n");
my(%ignore);
$CONFIG{DATABASE_IGNORE_LIST} =~ s/\s//g;
foreach ( split(/\|/,$CONFIG{DATABASE_IGNORE_LIST}) ) { $ignore{$_} = 1; }

info "Selecting Databases\n";
my($q)="select name from sysdatabases where 1=1 $whereclause";
debug("$q\n");
my(@TargetServerDbList);
print "\tSENT=$q\n" if $SHOWDETAILS;
# print ISQLF $q."\ngo\n" if $ISQL_FILE;
foreach ( dbi_query(-db=>"master",-query=>$q ) ) {
	my($db)= dbi_decode_row($_);
#print "Retrieved $db\n";
	next if $db eq "master" or $db eq "sybsystemprocs" or $db eq "tempdb"
		or $db eq "sybsystemdb" or $db eq "model" or defined $ignore{$db};
	if( $transfer_is_defined eq 'TRUE' and ! $SourceDb_By_ToLoadDb{$db}  ) {
		info "Ignoring Database $db - Not in XFER list\n";
	} else {
		push @TargetServerDbList,$db;
		$ToLoadDb_By_SourceDb{$db}=$db unless $ToLoadDb_By_SourceDb{$db};
		$ToLoadDb_By_SourceDbLc{lc($db)}=$db unless $ToLoadDb_By_SourceDbLc{lc($db)};
	}
};
info "Target Server Database List (",join(" ",@TargetServerDbList),")\n";

my(%SourceServerDbHashLc);

foreach my $db ( @TargetServerDbList ) {
	#info "Mapping $_ => $ToLoadDb_By_SourceDb{$_} \n";
	# print "$_ -> $SourceDb_By_ToLoadDb{$_} -> $ToLoadDb_By_SourceDb{$_}\n";
	if( $SourceDb_By_ToLoadDb{$db} ) {
		$SourceServerDbHashLc{lc($SourceDb_By_ToLoadDb{$db})} =  $db;
	} else {
		$SourceServerDbHashLc{lc($db)} = $db;
	}
}

info "Source Server Database List (",
		join(" ",sort keys %SourceServerDbHashLc),
		")\n";

# ok Find The Files
my($rootdir, $root);
my(@dirlist);
my(%baddb);
my(%lastgoodtime_bydb,%lasttime_bydb, %firstbadmsg_bydb, %firstbadtime_bydb, %firstbadfile_bydb);
my(%laststate_bydb,%lastmsg_bydb);
my($ftpcon);
my(%config);
$config{Debug} = $LIBDEBUG;
$config{Timeout}=90000;
if( defined $HOSTNAME and $CONFIG{XFER_BY_FTP} eq 'y' ) {
	info "Hostname = $HOSTNAME and CONFIG_BY_FTP is set to y\n";
	info "Grabbing The File List Via FTP from $HOSTNAME dir=$opt_i\n";
  	$ftpcon= Net::myFTP->new($HOSTNAME,METHOD=>$UNIXMETHOD,%config)
                 or logdie "Failed To Create FTP to $HOSTNAME Object $@";
   logdie( "Failed To Connect To $HOSTNAME" ) unless $ftpcon;
   my($r,$err)=$ftpcon->login($UNIXLOGIN,$UNIXPASSWORD);
   logdie( "Cant Login To Server As $UNIXLOGIN : $err \n") unless $r;
   $ftpcon->cwd($opt_i)
         or logdie( "Cant cd to $opt_i  : $! \n");
	info "Logged in via FTP to $HOSTNAME\n";
	$opt_R=$opt_i;
	@dirlist = $ftpcon->ls();
	info(($#dirlist+1)," Files Read Via FTP From Directory on $HOSTNAME\n");

	$rootdir=$opt_i;
	$root="";

	if( defined $SKIPDONE ) {
		info( "SKIPDONE specified... not processing .done files\n");
		@dirlist= grep((!/^\./ and ! /.done/ and ! /.gz/),@dirlist);
		info(($#dirlist+1)," Files after removing .done and .gz \n");
		@dirlist= grep( /logdump.\d+.\d+/,@dirlist);
		info(($#dirlist+1)," Files after logdump.\\d+.\\d+\n");
	} else {
		info( "SKIPDONE not specified... processing .done files\n");
		@dirlist= grep((!/^\./ and ! /.gz/ and /logdump.\d+.\d+/),@dirlist);
		info(($#dirlist+1)," Files after ! .gz and logdump.\\d+\\d+\n");
	}

} else {
	debug("Hostname not defined...\n");
	if( -d $opt_i ) {
		$rootdir=$opt_i;
		$root="";
		info "Grabbing The File List from dir=$rootdir\n";
	} else {
		$rootdir=dirname($opt_i);
		$root=basename($opt_i);
		info "Grabbing The File List from dir=$rootdir fileroot=$root\n";
	}
	$config{IS_REMOTE}="n";
	# info "ARGS=$HOSTNAME,METHOD=>$UNIXMETHOD,%config\n";
  	$ftpcon= Net::myFTP->new($HOSTNAME,METHOD=>$UNIXMETHOD,%config)
       or logdie "Failed To Create FTP to $HOSTNAME Object $@";
	debug( "Dir=$rootdir FileRoot=$root\n" );

	opendir(DIR,$rootdir) or die("Can't open directory $rootdir : $!\n");
	@dirlist=readdir(DIR);
	info(($#dirlist+1)." Files Read In Directory $rootdir\n");
	if( defined $SKIPDONE ) {
		info( "SKIPDONE specified... not processing .done files\n");
		my(@files)= grep((!/^\./ and ! /.done/ and ! /.gz/ ),@dirlist);
		info(($#files+1)." Files Without .done and .gz Extensions\n");
		if( $#files<0 ) {
			info "All Files Have Been Loaded\n" ;
			MlpBatchJobEnd() unless defined $FULL;
			exit(0);
		}

		if( defined $FULL ) {
			@dirlist= grep(( /$root/ and /_db_\d+.dump/i),@files);
			info(($#dirlist+1)." Files matching $root and _db_\\d+.dump\n")
				if defined $opt_d;
		} else {
			@dirlist=      grep(( /$root/ and /_tlog_\d+.trn/i),@files);
			push @dirlist, grep(( /$root/ and /_backup_\d+.trn/i),@files);
			push @dirlist, grep(( /$root/ and /_tlog_\d+.zip/i),@files);
			push @dirlist, grep(( /$root/ and /_backup_\d+.zip/i),@files);
			push @dirlist, grep(( /$root/ and /logdump.\d+.\d+/),@files);
			info(($#dirlist+1)." Files matching $root and tlog_\\d+.trn or backup_\\d+.trn or logdump.\\d+\\d+\n");
				#if defined $opt_d;
		}
	} else {
		info( "SKIPDONE not specified... processing .done files as well\n");
		my(@files)= grep((!/^\./ and ! /.gz/ ),@dirlist);
		info(($#dirlist+1)." Files Without .gz Extensions\n");
		@dirlist= grep((/$root/ and /_tlog_\d+.trn/i),@files) unless defined $FULL;
		push @dirlist, grep((/$root/ and /_backup_\d+.trn/i),@files) unless defined $FULL;
		push @dirlist, grep((/$root/ and /_tlog_\d+.zip/i),@files) unless defined $FULL;
		push @dirlist, grep((/$root/ and /_backup_\d+.zip/i),@files) unless defined $FULL;
		push @dirlist, grep((/$root/ and /logdump.\d+.\d+/),@files) unless defined $FULL;
		@dirlist= grep(( /$root/ and /_db_\d+.dump/i),@files) if defined $FULL;
	}
	closedir(DIR);
}
@dirlist= grep((/$FILEPATERN/),@dirlist) if $FILEPATERN;
if( $#dirlist<0 ) {
	print "No Files Found In Directory $rootdir Matching Spec\n";
	MlpBatchJobEnd() unless defined $FULL;
	exit(0);
}

my(%db_file_count);
if( $MINFILES ) { # ok require that number of files per db before proceeding
	foreach my $file ( sort @dirlist ) {
		my($srv,$db,$day,$tm);
		if( defined $FULL ) {
			next unless $file =~ /(\w+)_db_(\d\d\d\d\d\d\d\d)(\d+)/;
			($db,$day,$tm)=($1,$2,$3);
		} else {
			if( $file =~ /(\w+)_tlog_(\d\d\d\d\d\d\d\d)(\d+)/ ) {
				($db,$day,$tm)=($1,$2,$3);
				$srv="";
			} elsif( $file =~ /(\w+)_backup_(\d\d\d\d\d\d\d\d)(\d+)/ ) {
				($db,$day,$tm)=($1,$2,$3);
				$srv="";
			} elsif( $file =~ /(\w+).(\w+).logdump.(\d+).(\d+)/ ) {
				($srv,$db,$day,$tm)=($1,$2,$3,$4);
			} else {
				next;
			}
		}
		$db_file_count{$db}++ unless $file=~/.done$/ or $file=~/.gz$/;
	}
	foreach ( keys %db_file_count ) {
		info("\tWILL WORK ON DB=$_ - UNAPPLIED FILE COUNT=$db_file_count{$_} \n") if $db_file_count{$_}>$MINFILES;
	}
}

info( "Sorting File List\n" );
@dirlist = sort @dirlist;

info( "Loading ",($#dirlist+1)," Unapplied Log Files at ".localtime(time)."\n" );

my($curdbmsg,$curdb)=("\n","");
my($lastping)=time;
my($prior_error_found)="";
my($prior_count)=0;

foreach my $file ( @dirlist ) {
	my($srv,$db,$day,$tm);

	if( defined $FULL ) {
		debug( "   (bad name) Skipping $file\n" )
			unless $file =~ /(\w+)_db_(\d\d\d\d\d\d\d\d)(\d+)/;
		next unless $file =~ /(\w+)_db_(\d\d\d\d\d\d\d\d)(\d+)/;
		($db,$day,$tm)=($1,$2,$3);
	} else {
		if( $file =~ /(\w+)_tlog_(\d\d\d\d\d\d\d\d)(\d+)/ ) {
			($db,$day,$tm)=($1,$2,$3);
			$srv="";
		} elsif( $file =~ /(\w+)_backup_(\d\d\d\d\d\d\d\d)(\d+)/ ) {
			($db,$day,$tm)=($1,$2,$3);
			$srv="";
		} elsif( $file =~ /(\w+).(\w+).logdump.(\d+).(\d+)/ ) {
			($srv,$db,$day,$tm)=($1,$2,$3,$4);
		} else {
			debug( "   (bad name) Skipping $file\n" );
			next;
		}
	}

	if( $MINFILES ) {
		next unless $db_file_count{$db} > $MINFILES;
	}

	if( $db eq $prior_error_found and ! $SKIPPRIOR ) {
		info( "   (prior error) Skipping $file\n" );
		next;
	}

	if( time - $lastping > 60 ) {
		MlpBatchJobStep() unless defined $FULL;
		$lastping=time;
	}

	if( ! defined $SourceServerDbHashLc{lc($db)} ) {
		debug( "DB $db Not In List For $file - ignored\n" );
		next;
	};
	#print "DBGX $file at line ",__LINE__,"\n";

	my($server_file)=$opt_R."/".basename($file);
	if( $curdb ne $db ) {
		info($curdbmsg) unless $curdb eq "";
		$curdb=$db;
		$curdbmsg="\n";
	}

	if( defined $baddb{$db} ) {
		info "$db $day $tm (auto skipped - $baddb{$db})\n";
		next;
	}
	#print "DBGX $file at line ",__LINE__,"\n";

	info "$db $day $tm \n" if defined $opt_d;
	my(@rc)=load_database($server_type, $db, $server_file );

	if( $load_state eq "COMMFAIL" ) {
		info "*** ReConnecting to Server $opt_S\n";
		dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -type=>$opt_M, -debug=>$LIBDEBUG)
			or logdie "Cant connect to $opt_M $opt_S as $opt_U\n";
		$prior_error_found=$db;
		next;
	}
	# print "RESULTS=",join("RESULTS=",@rc);
	my($load_ok)=0;
	my($file_not_found)=0;
	foreach (@rc) {
		next if /^load_database.pl v/;
		next if /^load_database.pl :/;
		next if /^run on hostname=/;
		next if /^NO MAIL/i or /^\s*$/;
		next if /3137:/ or /3479:/;
		chomp;
		chomp;
		info(">".$_."\n");
		if( /: file not found/ ) {
			$file_not_found=2 if $file_not_found==0;
			next;
		}

		if( /\sNeeds LSN / or $load_state eq "BAD" ) {
			$prior_error_found=$db;
		}

		$file_not_found=1;
		$load_ok=1 if /\sGOOD\s/;
		$load_ok=0 if $load_state eq "BAD";
		$lastgoodtime_bydb{$db}=$day.".".$tm if /\sGOOD\s/;
		if( ! /\sGOOD\s/ and ! defined $firstbadtime_bydb{$db} ) {
			$firstbadtime_bydb{$db}=$day.".".$tm ;
			$firstbadfile_bydb{$db}=$file;
			$firstbadmsg_bydb{$db}=$_;
			# $firstbadmsg_bydb{$db}="DBG DBG Db=$db\nFilename:$server_file" .$_;
		}
		$lasttime_bydb{$db}   =$day.".".$tm;
		$laststate_bydb{$db} = $load_ok;
		$lastmsg_bydb{$db}   = $_;
	}

	if( $load_ok == 1 and defined $DELETE and $file!~/.done$/ ) {
		info("delete $rootdir/$file")  if defined $opt_d;
		my($rc)=$ftpcon->delete($rootdir."/".$file) ;
	} elsif( $load_ok == 1 and defined $RENAME and $file!~/.done$/ ) {
		info("rename $rootdir/$file, $rootdir/$file.done\n")  if defined $opt_d;
		my($rc)=$ftpcon->rename($rootdir."/".$file, $rootdir."/".$file.".done") unless $NOEXEC;
	} else {
		info("LoadOk=$load_ok File=$file\n") if defined $opt_d;
	}
	save_db_state( $db ) unless $file_not_found==2;
	last if $load_ok == 0 and $STOP_ON_ERROR;
}
info($curdbmsg) unless $curdb eq "";
info "Transaction Log Recovery Complete\n\n";

info "Printing Completion Messages\n";
foreach (sort keys %firstbadtime_bydb) {
	info( $firstbadmsg_bydb{$_}."\n" );
}

info "Saving Completion Heartbeat\n";
if( $system_state eq "COMPLETED" ) {
	MlpBatchJobEnd() unless defined $FULL;
} else {
	MlpBatchJobErr($system_msg) unless defined $FULL;
}

dbi_disconnect();
print "load_all_tranlogs.pl: Program Completed at ".localtime(time)."\n";
exit(0);

sub	save_db_state {
	my($db)=@_;
	my($state,$msg);
	if( $laststate_bydb{$db} != 1 ) {
		$state="ERROR" ;
		$system_state="ERROR";
		if( ! defined $lastgoodtime_bydb{$db} ) {
			if( ! $firstbadtime_bydb{$db} ) {
				$msg= "Couldnt Load - Msg=".$lastmsg_bydb{$db};
			} else {
				$msg= "Bad File Time=$firstbadtime_bydb{$db} Msg=".$lastmsg_bydb{$db};
			}
		} else {
			$msg= "LastGood=$lastgoodtime_bydb{$db} LastFile=$lasttime_bydb{$db} Msg=$lastmsg_bydb{$db}";
		}
		$system_msg.="{ db=$db bad file\@$firstbadtime_bydb{$db} }\n"
			unless $system_msg=~/$db bad file/;
	} else {
		$state="COMPLETED";
      # $msg = "Logs Restored Until " .substr($lastgoodtime_bydb{$db} ,0,2).":".substr($lastgoodtime_bydb{$db} ,2,2)." at ".scalar(localtime(time));
		# my($tm)=$lastgoodtime_bydb{$db};	# yyyymmdd.hhmmss
		my($tm)=substr($lastgoodtime_bydb{$db},4,2);
		$tm.="/".substr($lastgoodtime_bydb{$db},6,2);
		$tm.=" ".substr($lastgoodtime_bydb{$db},9,2);
		$tm.=":".substr($lastgoodtime_bydb{$db},11,2);

      $msg = "Logs Restored Until " .$tm." at ".scalar(localtime(time));

      # OVERRIDE HourlyBackupReport which normally comes from Mssql_Backup_Crosscheck_Report.pl run by the hourly batch
      # Kind of a kludge but that report has some lag and this heartbeat is correct...
      MlpHeartbeat(
      	-monitor_program => "HourlyBackupReport",
			-system=> $opt_S,
			-subsystem=> $db." Backup Checker",
			-state=> "OK",
			-message_text=> "load_all_tranlogs: $msg"
		);
	};
	if( ! defined $FULL ) {
		if( defined $opt_d ) {
			info( "DB=$db State=$state (".$laststate_bydb{$db}.") Msg=$msg\n") unless $state eq "COMPLETED";
		}

		MlpHeartbeat(-monitor_program   =>  $BATCH_ID,
      	-system            =>      $opt_S,
      	-subsystem         =>      "$db Log Restore",
      	-state             =>      $state,
      	-message_text      =>      $msg,
			# -batchjob=>1
		);
	}
	info( "Save State Completed\n" ) if defined $opt_d;
}

my(@rc);
my($db);
my(%db_undergoing_load);

sub load_database {
	my($server_type,$database,$filename)=@_;
	@rc=();
	$db=$database;

	$filename=~s/\s+$//;
	my($NL)="\n";
	$NL="<br>\n" if defined $opt_h;

	print "***>\tSTARTING FILE $filename\n" if $SHOWDETAILS;
	if( defined $db_undergoing_load{$db} ) {
		push @rc, "ERROR: Skipping $filename - $db Undergoing Full Load\n";
		return @rc;
	}

	my($count)=0;
	while( $count<10) {
		my($st)="OK";
		push @rc,  "Checking for a running load".$NL if defined $opt_d;
		foreach( dbi_query(-db=>"master",-query=>"select * from sysprocesses where cmd in ( 'LOAD DATABASE', 'LOAD TRANSACTION' ) and dbid=db_id('".$db."')") ) {
			$st="LOADING";
			if( /Communication link failure/ ) {
				print "COMMUNICATIONS FAILURE\n";
				$load_state="COMMFAIL";
				return;
			}
			push @rc, "Database $db is currently undergoing load - sleeping 10 seconds\n";
			print "Failed sysprocesses check on $db - Db is LOADING\n";
			print "Perhaps another program is loading this database?  Please Seek Help! (sleeping 10 seconds)\n";
			my($msg)=join("|",dbi_decode_row($_));
			print "RESULTS=",$msg,"\n";
			if( $msg =~ /^Cant Use Database master/ ) {
				print "DBG DBG: MUST RECONNECT HERE\n";
				dbi_disconnect();
				dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -type=>$opt_M, -debug=>$LIBDEBUG)
					or logdie "Cant connect to $opt_M $opt_S as $opt_U\n";
			}
			sleep(10);
		}
		$count++;
		last if $st eq "OK";
	}
	if( $count>=10 ) {
		$db_undergoing_load{$db}=1;
		push @rc, "ERROR: Can not load tran logs into $db... Database Undergoing Full Load\n";
		return @rc;
	}

	push @rc,  "Killing Possible Users in $db (server $opt_S)\n" if defined $opt_d;
	my($num_killed)=kill_users_in_db($database);
	my($count)=0;
	push @rc,  "Number of killed processes=$num_killed".$NL if defined $opt_d;

	if( $num_killed>0 ) {
		push @rc, "Aborting Load - There is a user in the database (use -k flag to override)\n";
		push @rc, "This indicates an active user connection or that multiple loads are running\n";
		return @rc;
	}

	logdie("ERROR: Cant Kill All Processes in $database\n")
		if $count>=5 and $num_killed>0;

	push @rc,  "<h2>" if defined $opt_h;

	my($start_time)=time;
	push @rc,  "</h2>" if defined $opt_h;
	my($compress_ext)="";
	$compress_ext="compress\:\:" if defined $opt_X or $CONFIG{SYBASE_COMPRESSION_LEVEL} =~ /^\d$/;

	my($command, $formatcmd);

	if( $server_type eq "SQLSERVER" ) {
		if( $ToLoadDb_By_SourceDb{$db} ) {
			$command="restore log ".$ToLoadDb_By_SourceDb{$db}." from DISK=\'".$filename."\' with norecovery";
		} elsif( $ToLoadDb_By_SourceDbLc{lc($db)} ) {
			# this was put in to handle case mismatches on sql server databases
			$command="restore log ".$ToLoadDb_By_SourceDbLc{lc($db)}." from DISK=\'".$filename."\' with norecovery";
		} else {
			push @rc, "ERROR: Skipping $filename - NO TARGET DB for $db\n";
			#foreach (keys %ToLoadDb_By_SourceDbLc ) { print $_,"\n"; }
			#die "DBGDBG DONE - DB=$db";
			return @rc;
		}

	} else {
		die "NO TARGET DB for $db (check XFER_TO_DB & XFER_FROM_DB)"  unless $ToLoadDb_By_SourceDb{$db};
		$command="load transaction ".$ToLoadDb_By_SourceDb{$db}." from \'".$compress_ext.$filename."\'";
	}

	# ok this is lame-ass but if you are restoring to a SQL Server then
	# you might be hosed unless you remap.  Check log device matches (ignore
	# case), then check if 1 data segment on each, then 1 log each
	#
	# but... oops... cant get this info if database isnt recovered... so if it
	# isnt recovered just skip it...

#	if( defined $formatcmd ) {
#		my(%dfilenm_by_nm, %dfiletyp_by_nm, %dmatched);
#		my($num_sfiles, $num_dfiles)=(0,0);
#
#		my($isloading);
#		push @rc,  "Command=>select status&(32+64+128+256+512+1024+4096+32768)
#			from master..sysdatabases where name='$ToLoadDb_By_SourceDb{$db}'".$NL if defined $opt_d;
#		foreach( dbi_query(-db=>"master",-query=>"select status&(32+64+128+256+512+1024+4096+32768)
#			from master..sysdatabases where name = '".$ToLoadDb_By_SourceDb{$db}."'") )
#		{
#			($isloading)=dbi_decode_row($_);
#			push @rc, "The Database Is LOADING!\n" if defined $opt_d;
#		}
#		die "Woah - no database $ToLoadDb_By_SourceDb{$db} found\n" if ! defined $isloading;
#
#		if( $isloading == 0 ) {
#
#			# use prior defined command to retrieve files in the dump
#			push @rc,  "Command=>$formatcmd".$NL if defined $opt_d;
#			foreach( dbi_query(-db=>"master",-query=>$formatcmd) ) {
#				my($lnm,$pnm,$ty,$fgnm,$sz,$maxsz)=dbi_decode_row($_);
#				push @rc,  "Dump: $lnm, $pnm, $ty, $fgnm, $sz, $maxsz \n";
#				$dfilenm_by_nm{$lnm}  =  $pnm;
#				$dfiletyp_by_nm{$lnm} =  $ty;
#				$num_dfiles++;
#			}
#
#			my(%sfilenm_by_nm, %sfiletyp_by_nm, %smatched );
#			# get the files in the server
#			push @rc,  "Command=>select name,filename,status from sysfiles" if defined $opt_d;
#			foreach( dbi_query(-db=>$ToLoadDb_By_SourceDb{$db},-query=>"select name,filename,status from sysfiles") ) {
#				my($nm,$fnm,$stat)=dbi_decode_row($_);
#				$nm=~s/\s+$//;
#				$fnm=~s/\s+$//;
#				$stat=~s/\s+$//;
#				push @rc,  "Server: $nm, $fnm, $stat,".($stat&0x2).", ".($stat&0x40)."\n" ;
#				$sfilenm_by_nm{$nm}  =  $fnm;
#				$sfiletyp_by_nm{$nm} =  ($stat&0x40);
#				$num_sfiles++;
#			}
#
#			die "Unable to Map: Server has $num_sfiles files and Dump has $num_dfiles" if $num_sfiles != $num_dfiles;
#
#			my($movecmd)="";
#			my(%done);
#			foreach my $sfile ( keys %sfilenm_by_nm ) {
#				my($match)="";
#
#				foreach ( keys %dfilenm_by_nm ) {
#					if( $sfile =~ /^$_$/i ) {
#						# they have the same name! check types
#						next if $dfiletyp_by_nm{$_} =~ /D/ and $sfilenm_by_nm{$sfile} =~ /.ldf$/;
#						next if $dfiletyp_by_nm{$_} =~ /L/ and $sfilenm_by_nm{$sfile} =~ /.mdf$/;
#
#						$match=$_;
#						last;
#					};
#				}
#
#				if( $match ne "" ) {
#					# well $sfile and $match match
#					$movecmd .= "move '".$match."' to '".$sfilenm_by_nm{$sfile}."',";
#					$smatched{$sfile}=1;
#					$dmatched{$match}=1;
#					$num_dfiles--;
#					$num_sfiles--;
#					next;
#				}
#			}
#
#			# no match... see if there is one data/data match or one log/log match
#			my($server_data, $server_log) = ("","");
#			foreach ( keys %sfilenm_by_nm ) {
#				next if defined $smatched{$_};
#				my($f)= $sfilenm_by_nm{$_};
#				if( $f=~/.mdf$/i ) {
#					if( $server_data eq "" ) {
#						$server_data=$_;
#					} else {
#						$server_data="x";
#					}
#				}
#				if( $f=~/.ldf$/i ) {
#					if( $server_log eq "" ) {
#						$server_log=$_;
#					} else {
#						$server_log="x";
#					}
#				}
#			}
#
#			my($dump_data, $dump_log) = ("","");
#			foreach ( keys %dfilenm_by_nm ) {
#				next if defined $dmatched{$_};
#				my($f)= $dfilenm_by_nm{$_};
#				if( $f=~/.mdf$/i or $dfiletyp_by_nm{$_}=~/D/ ) {
#					if( $dump_data eq "" ) {
#						$dump_data=$_;
#					} else {
#						$dump_data="x";
#					}
#				}
#				if( $f=~/.ldf$/i  or $dfiletyp_by_nm{$_}=~/L/ ) {
#					if( $dump_log eq "" ) {
#						$dump_log=$_;
#					} else {
#						$dump_log="x";
#					}
#				}
#			}
#
#			if( $dump_data ne "x"
#			and $dump_data ne ""
#			and $server_data ne "x"
#			and $server_data ne "" ) { 				# we can match a dump
#					$movecmd .= "move '".$dump_data."' to '".$sfilenm_by_nm{$server_data}."',";
#					$smatched{$server_data}=1;
#					$dmatched{$dump_data}=1;
#					$num_dfiles--;
#					$num_sfiles--;
#			}
#
#			if( $dump_log ne "x"
#			and $dump_log ne ""
#			and $server_log ne "x"
#			and $server_log ne "" ) { 				# we can match a dump
#					$movecmd .= "move '".$dump_log."' to '".$sfilenm_by_nm{$server_log}."',";
#					$smatched{$server_log}=1;
#					$dmatched{$dump_log}=1;
#					$num_dfiles--;
#					$num_sfiles--;
#			}
#
#			$movecmd =~ s/,$//;
#			$movecmd = ", ".$movecmd;
#			push @rc,  "Movecmd: $movecmd\n";
#
#			die "Hmmm You Have Not Matched All The Files... "
#				unless $num_dfiles==0 and $num_sfiles==0;
#
#			$command.=$movecmd;
#		} else {
#			push @rc,  "DB Status $isloading : $ToLoadDb_By_SourceDb{$db} is recovering. Not mapping files\n";
#		}
#	}
	# $command .= ", replace" if $server_type eq "SQLSERVER" and lc($ToLoadDb_By_SourceDb{$db}) ne lc($db);

#	print  __LINE__," DBG DBG Command=>$command $NL";
	push @rc,  "Command=>$command $NL"	if defined $opt_d;
	$load_state = "UNKNOWN";
	$load_msg   = "";
	dbi_msg_ok(1000);
	dbi_msg_ok(911);

	print "\tSENT=$command\n" if $SHOWDETAILS;
	print ISQLF $command."\ngo\n" if $ISQL_FILE;
	my($doover)='FALSE';
	if( $NOEXEC ) {
		$load_state = "GOOD";
		$load_msg = "(NOEXEC)";
	} else {
		foreach( dbi_query(-db=>"master",-query=>$command, -debug=>$LIBDEBUG) ) {
			my($msg)=dbi_decode_row($_);
			chomp $msg;
			next if $msg =~ /^\s*$/;
			$msg=~s/\s+$//;
			print "\tRCVD=$msg\n" if $SHOWDETAILS;
	#		if( $msg =~ /^42000:\s+A previous restore operation was interrupted and did not complete processing on file/
	#		or  $msg =~ /^42000:\s+RESTORE LOG is terminating abnormally/ ) {
	#			print "\tSTATE: Skipping Msg - state=$load_state\n" if $SHOWDETAILS;
	#			$doover='TRUE';
	#			next;
	#		}

			if( $msg =~ /A previous restore operation was interrupted and did not complete processing on file/ ) {
				$load_state = "GOOD" if $load_state eq "UNKNOWN";
				print "\tSTATE: Skipping message - state=$load_state\n" if $SHOWDETAILS;
				next;
			}
			if( $msg =~ /^42000:\s+RESTORE LOG is terminating abnormally/ ) {
				print "\tSTATE: Skipping message - state=$load_state\n" if $SHOWDETAILS;
			#	$doover='TRUE';
				next;
			}
			process_a_logmsg($database,$filename,$command,$msg);
			print "\tSTATE: $load_state $load_msg\n" if $SHOWDETAILS;
			push @rc,  "$msg (Msg=$load_msg State=$load_state)\n" if defined $opt_d;
		}
	}
#	if( $doover eq "TRUE" and $load_state ne "GOOD" ) {
#		print "\t*************** DO OVER *******************\n" if $SHOWDETAILS;
#		print "\tSENT=$command\n" if $SHOWDETAILS;
#		print ISQLF $command."\ngo\n" if $ISQL_FILE;
#		foreach( dbi_query(-db=>"master",-query=>$command, -debug=>$LIBDEBUG) ) {
#			my($msg)=dbi_decode_row($_);
#			chomp $msg;
#			next if $msg =~ /^\s*$/;
#			print "\tRCVD=$msg\n" if $SHOWDETAILS;
#			if( $msg =~ /^42000:\s+A previous restore operation was interrupted and did not complete processing on file/ ) {
#				$load_state = "GOOD" if $load_state eq "UNKNOWN";
#				print "\tSTATE: Skipping - state=$load_state\n" if $SHOWDETAILS;
#			}
#			if( $msg =~ /^42000:\s+RESTORE LOG is terminating abnormally/ ) {
#				print "\tSTATE: Skipping - state=$load_state\n" if $SHOWDETAILS;
#			#	$doover='TRUE';
#				next;
#			}
#			process_a_logmsg($database,$filename,$command,$msg);
#			print "\tSTATE: $load_state $load_msg\n" if $SHOWDETAILS;
#			push @rc,  "$msg (Msg=$load_msg State=$load_state)\n" if defined $opt_d;
#		}
#		print "\t*************** DO OVER DONE *******************\n" if $SHOWDETAILS;
#		$load_state = "BAD" if $load_state = "UNKNOWN";
#		$doover='FALSE';
#	}

	print "\tFINAL STATE: $load_state\n" if $SHOWDETAILS and ! $NOEXEC;
	push @rc,  "Load Command Completed\n" if defined $opt_d  and ! $NOEXEC;

	#if( $server_type eq "SYBASE" and $database and ! defined $opt_O ) {
		#push @rc, "Online Database $opt_D\n";
		#foreach( dbi_query(-db=>"master",-query=>"online database $opt_D", -no_results=>1) ) {
			#my($msg)=dbi_decode_row($_);
			#push @rc,  "  > ".$msg."\n";
		#}
	#}

	if( $load_state eq "GOOD" or $load_state eq "SKIP" ) {
	   $start_time=time-$start_time;
	   my $minutes=int($start_time/60);
	   $start_time-=60*$minutes;
	   push @rc,  basename($filename)." ".$load_state." : $load_msg".$NL.$NL;
		return @rc;
	} else {
		push @rc,  basename($filename)." ".$load_state." : $load_msg".$NL.$NL;
	   return @rc;
	}
}

#  $load_state = UNKNOWN | GOOD | BAD | SKIP
#  $load_msg	= text message about load
sub process_a_logmsg
{
	my($database)=shift;
	my($filename)=shift;
	my($query)=shift;
	($_) = @_;
	$_ =~ s/\s+$//;
	if( $server_type eq "SQLSERVER" ) {
#  		print __LINE__," DBG DBG - $_\n";
      if( /RESTORE LOG successfully processed (\d+)/ ) {
            $load_msg   ="Loaded $1 Pages";
            $load_state = "GOOD" if $load_state eq "UNKNOWN";
            return 0;
      } elsif(/which is too early to apply to the database. A more recent/){
          /LSN (\d+),/;
          my($lsn)=$1;
          /LSN (\d+) can be restored/;
          my($wantlsn)=$1;
          #$load_msg="LSN $lsn Skipped. (ok)";
          $load_msg="LSN Skipped. (ok)";
          $load_state = "GOOD" if $load_state eq "UNKNOWN";
          return 0;
      } elsif( /Processed (\d+) pages for database '/ ) {
          $load_msg="loaded $1 pages";
          $load_state = "GOOD" if $load_state eq "UNKNOWN";
          return 0;
      } elsif( /RESTORE LOG is terminating abnormally/ ) {
          return 0;    # dont know if its good or bad jsut from this
      } elsif( /The log in this backup set terminates at LSN/ and
          /which is too late to apply to the database./ ) {
          $load_msg   ="Skipped Log ";
          $load_state = "GOOD" if $load_state eq "UNKNOWN";
          return 0;
      } elsif( /was only partially restored by a database or file restore./ ) {
          $load_state= "BAD";
          $load_msg="DB $ToLoadDb_By_SourceDb{$db} (Prior Load Failed)\n\n";
          return 0;
      } elsif( /Exclusive access could not be obtained because/ ) {
          $load_state= "BAD";
          $load_msg="DB $ToLoadDb_By_SourceDb{$db} In Use (Load Aborted)\n\n";
          return 0;
      } elsif( /The log in this backup set begins at LSN/ ) {
          /LSN (\d+),/;
          my($lsn)=$1;
          /LSN (\d+) can be restored/;
          my($wantlsn)=$1;
          # $load_msg="LSN $lsn Too Late. (abrt)";
          $load_state= "BAD";
          $load_msg="DB $ToLoadDb_By_SourceDb{$db} Needs LSN $wantlsn\n\n";
          return 0;
       } elsif( /Cannot open backup device/ and /Device error or device off-line/) {
       	s/Cannot open backup device//;
		 	s/Device error or device off-line\. See the SQL Server error log for more details\.//;
			push @rc, $_." : file not found";
         $load_state= "GOOD";
			return 0;
       } else {
       	print "\tUNHANDLED MESSAGE\n" if $SHOWDETAILS;
         $load_msg="Error: $_";
         $load_state= "BAD";
       }
       warn "Database: $database\nFilename:$filename\nQuery: $query\n$_\nFatal Error: This shouldnt happen unless the above was a unhandled message."
       	unless /The preceding restore operation did not specify WITH NORECOVERY/;
   } else {	# SYBASE
   	#print "++++++++++++++++++++++\n";
		#my($i)=1;
		#foreach (@rc) { print $i++.") ".__LINE__," ",$_; }
		#print "++++++++++++++++++++++\n";

		return 0 if /Use this value when executing the \'sp_volchanged\' system stored procedure after ful/;
      return 0 if /WARNING: In order to LOAD the master database, the /;
      s/Backup Server:\s//;
		if( /LOAD is complete/ ) {
      	$load_msg="LOAD is Complete";
         $load_state = "GOOD" if $load_state eq "UNKNOWN";
		}
		return 0 if /mounted on disk file/;
		if( /^930:/ ) {			# last database load incomplete
      	$load_msg="Prior LOAD DATABASE incomplete";
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^911:/ ) {			# cant use datbase
      	$load_msg=$_;
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^9\d\d:/ ) {			# other 900 series messages
      	$load_msg=$_;
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^3137:/ ) {			# use online db message
			return;
		} elsif( /^31\d\d:/ ) {			# other 3100 series messages
      	$load_msg=$_;
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^4305/ ) {	# out of sequence
			push @rc,  "Msg $_ \n" if defined $opt_d;
			push @rc,  "     OUT OF SEQUENCE MSG FOUND - PARSING DATES\n" if defined $opt_d;
			/Current time stamp is (\w\w\w[\d\s:]+\w\w)/;
			my($dbtime)=$1;
			$dbtime =~ /(\w\w\w)\s+(\d+)\s+(\d+)\s+(\d+):(\d+)[\d\:]*(\w+)/;
			my($mon,$day,$yr,$hr,$min,$AM)=($1,$2,$3,$4,$5,$6);
	push @rc, "     dbtime=$dbtime m=$mon d=$day hr=$hr min=$min am=$AM\n"  if $opt_d;
			$hr+=12 if $AM eq "PM" and $hr != 12;
			# $hr-=12 if $AM eq "PM" and $hr == 12;
			#$hr-=12 if $AM eq "AM" and $hr == 0;
			$hr=0 if $AM eq "AM" and $hr == 12;
			my($moncnt)=0;
			while($moncnt<=12) {
				last if $month_name[$moncnt] eq $mon;
				$moncnt++;
			}
			die "CANT FIND MONTH ID FOR $mon in @month_name" if $moncnt==13;

			use Time::Local;
			my($dbtm)=timelocal(0,$min,$hr,$day,$moncnt,$yr);
			push @rc, "     Database Timestamp=$dbtime\n" if $opt_d;
			#push @rc, "     m=$mon d=$day hr=$hr min=$min am=$AM moncnt=$moncnt\n" if $opt_d;
			push @rc, "     Converted to $dbtm which is ".scalar(localtime $dbtm)."\n" if defined $opt_d;

			/dump was from (\w\w\w[\d\s:]+\w\w).$/;
			my($dumptime)=$1;
			push @rc,  "     *** Dump Time     =$dumptime \n" if defined $opt_d;
			$dumptime =~ /(\w\w\w)\s+(\d+)\s+(\d+)\s+(\d+):(\d+)[\d\:]*(\w+)/;
			($mon,$day,$yr,$hr,$min,$AM)=($1,$2,$3,$4,$5,$6);
	push @rc, "     dumptime=$dumptime m=$mon d=$day hr=$hr min=$min am=$AM\n" if $opt_d;
			$hr+=12 if $AM eq "PM" and $hr != 12;
			#$hr-=12 if $AM eq "PM" and $hr == 12;
			$hr=0 if $AM eq "AM" and $hr == 12;
			($moncnt)=0;
			while($moncnt<=12) {
				last if $month_name[$moncnt] eq $mon;
				$moncnt++;
			}
			die "CANT FIND MONTH ID FOR $mon in @month_name" if $moncnt==13;
			my($dumptm)=timelocal(0,$min,$hr,$day,$moncnt,$yr);

			push @rc, "     *** File Timestamp=$dbtime\n" if $opt_d;
			#push @rc, "     m=$mon d=$day hr=$hr min=$min am=$AM moncnt=$moncnt\n" if $opt_d;
			push @rc, "     Converted to $dumptm which is ".scalar(localtime $dumptm)."\n" if defined $opt_d;

			if( $dumptm > $dbtm ) {
      		push @rc, "     *****************************\n" if $opt_d;
				push @rc, "     *** THIS IMPLIES A GAP!!! ***\n" if $opt_d;
				push @rc, "     *****************************\n" if $opt_d;
				$load_msg="Cant Load - Tranloads Have Gap (".
      				$dumptm."=".
      				scalar(localtime($dumptm)).
      				" > $dbtm=".
      				scalar(localtime($dbtm)).
      				") \n";
         	$load_state = "BAD";
			} else {
      		push @rc, "     *** ALL IS WELL!!! ***\n" if $opt_d;
				$load_msg="Log Load Allready applied (db=$dbtime)";
         	$load_state = "GOOD" if $load_state eq "UNKNOWN";
			}

			#print "++++++++++++++++++++++\n";
			#my($i)=1;
			#foreach (@rc) { print $i++.") ".$_; }
			#print "++++++++++++++++++++++\n";
			return 0;
		}
      push @rc, "  > ".$_."\n" unless /mounted on byte stream/ or /Attempting to open byte stream device:/;
   }
}

sub kill_users_in_db
{
	my($db)= @_;
	my(@processes)=dbi_query(-db=>"master",-query=>"select spid
			from master..sysprocesses
			where dbid=db_id(\'$opt_D\')
			and \'$opt_D\' != \'master\'");

	return 0 if $#processes<0;
	return ($#processes+1);
}

__END__

=head1 NAME

load_all_tranlogs.pl - Load Transaction Logs In Mass

=head2 SYNOPSIS

This is the primary driver to load more than one transaction log.  It applies file
filters and runs load_database.pl on each file that survives the filtering in order.
It renames the files to append .done to the ones applied.

=head2 USAGE

load_all_tranlogs.pl -JJOBNAME

or

load_all_tranlogs.pl
   -USER=SA_USER      -SERVER=SERVER      -PASSWORD=SA_PASS
   -DATABASE=db       -INDIR=file_root    -TYPE=Type
   -DESTDIR=dest_dir  -JOB=job            -HOSTNAME --SKIPPRIOR
   -UNIXLOGIN         -DELETE             -UNIXPASSWORD -STRIPES
   -SOURCESERVER - (for reporting only - this is the source server)
   -FULL   - full database loads (only on microsoft for now)
   -DEBUG debug mode  --FILEPATERN=pat --COMPRESSED (sybase compression)
   -SKIPDONE skip files with .done extension
   -MICROSOFT microsoft sql svr format (see below)
   -RENAME rename files to .done extension
   -INDIR identifies a directory / files as seen from this program
   -DESTDIR identifies the same directory/files as seen by remote server
		(or pass HOSTNAME)

   Will Load, in order, tranlogs that have the following name format
      {SERVER}.{DATABASE}.dbdump.yyyymmdd-hhmmss
   or {DB}_tlog_yyyymmddhhmm.TRN (sql server format - if --MICROSOFT passed)

=head2 DESCRIPTION

Works on standard SQL server and My standard file naming conventions.
If your files are not using standard naming (yyyymmdd.hhmmss) then that
needs to be looked at.

=head2 OTHER NOTES

If you get in a situation where your files are jumbled, just run with --SKIPPRIOR.  This
option will stop the check that skips logs once an error is found.  All logs will be applied
not in order!  This can be used to unjumble tran logs when there are ordering issues.

=cu
