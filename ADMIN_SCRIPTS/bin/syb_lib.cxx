/* This Library Is Copyright  (c) 1994 by Edward Barlow */

/* Header Section */
#include <stdio.h>
#include <stdlib.h>
#include <sybfront.h>
#include <sybdb.h>
#include <malloc.h>
#include <string.h>
#include <syb_lib.h>

/* DBExit Codes */
#define ERR_HNDLR_ABRT          11001
#define MSG_HNDLR_ABRT          11002
#define DBG_FILE_ABRT           11003
#define MAX_COLS_ABRT           11006
#define TXT_IMG_ABRT            11007
#define BCPINIT_FAIL_ABRT       11008
#define CHAR_BIND_ABRT          11009
#define INT_BIND_ABRT           11010
#define FLT_BIND_ABRT           11011
#define MON_BIND_ABRT           11012
#define DATE_BIND_ABRT          11013
#define BIN_BIND_ABRT           11014
#define BIT_BIND_ABRT           11015
#define BAD_TYP_ABRT            11016

/*extern int ProcessRow();*/
extern int ProcessResults(DBPROCESS * dbproc, int  (*UserFunction)(DBPROCESS *,void *,int, int),
                           int  MaxNumRows, void *ReturnData,
                           int   RowSize, int  *NumRows, FILE *fp);
extern int RegularRowLength(DBPROCESS * dbproc);
extern int ComputedRowLength(DBPROCESS * dbproc, int clause_id );
extern int MaxComputedRowLength( DBPROCESS * dbproc);
extern int BindRegularRows(DBPROCESS * dbproc, char * buffer );
extern int BindComputedRows(DBPROCESS * dbproc, char * buffer);

static int      BatchId;                                                        /* Current Result Set (for ext functions */
static int      sybase_initialized=FALSE;       /* TRUE if sybase initialized */
static int      debug=FALSE;                                    /* printfs if debugging */
static FILE     *fpdbg;                                                 /* fp to print to */
static char     separator='\0';

#define MAX_IGNORE_LIST 26
static int              ignore_list[MAX_IGNORE_LIST];
static int              max_ignore_list=0;              /* First Free On List */

/*
***************************************************************************
** FUNCTION NAME : ignore_error
** SYNOPSIS      : is error to be ignored
***************************************************************************
*/
int ignore_error(int error_no)
{
        int i;
        for(i=0;i<max_ignore_list;i++)
                if( ignore_list[i]==error_no ) return TRUE;
        return FALSE;
}

/*
***************************************************************************
** FUNCTION NAME : add_to_ignore_list
** SYNOPSIS      : add error to ignore list
***************************************************************************
*/
int add_to_ignore_list(int new_error)
{


        if(max_ignore_list>=MAX_IGNORE_LIST) {
                fprintf(stderr,"Cant Add To Ignore List\n");
                return FALSE;
        }

        if( ignore_error(new_error) ) {
                fprintf(stderr,"Error %d Already Ignored\n",new_error);
                return FALSE;
        }

        ignore_list[max_ignore_list]=new_error;
        max_ignore_list++;
        return TRUE;
}

/*
***************************************************************************
** FUNCTION NAME : SybExit
** SYNOPSIS      : exit sybase
***************************************************************************
*/
void SybExit()
{
        if( sybase_initialized ) dbexit();
        sybase_initialized=FALSE;
}

void separate(char x)
{
        separator=x;
}

/*
***************************************************************************
** FUNCTION NAME : err_handler
** SYNOPSIS      : Generic error handler
***************************************************************************
*/
int CS_PUBLIC err_handler( DBPROCESS * dbproc, int severity, int errno, int oserrno, char *dberrstr, char *oserrstr)
{


        if( ignore_error(errno) ) {
                if( debug ) fprintf(fpdbg,"Ignoring error #%d\n",errno);
                return INT_CANCEL;
        }

        /* Not Login Failed Message */
        if( errno!=20009 && errno!=20014 && ((dbproc==NULL) || (DBDEAD(dbproc))))
        {
                fprintf(stderr,"\nServer Connection Died\n");
        fprintf(stderr,"\n");
        fprintf(stderr,"Sybase Error #%d (severity %d oserrno %d)",
                errno,severity,oserrno);
        fprintf(stderr,"\n------------------------------------\n%s\n", dberrstr );

       }
        if( oserrno != DBNOERR )
                fprintf(stderr,"operating system message:\t%s\n",oserrstr);

/*      if( dbproc!=NULL && !DBDEAD(dbproc) )
                dbcancel(dbproc); */

        /* DBExit("err_handler() aborting program","",ERR_HNDLR_ABRT); */
return 1;
}
/*
***************************************************************************
** FUNCTION NAME : Debug
** SYNOPSIS      : set debug level for future print statements
** Severity can be 1 (normal) or 2 (print everything)
***************************************************************************
*/
FILE *Debug(int debug_level)
{
        debug=debug_level;
#ifdef _WINDOWS
                fpdbg = fopen("c:\\debug","w");
#else
                fpdbg = fopen("debug","w");
#endif
        if(fpdbg == NULL)
                {
                        dbexit();
                        return NULL;
                /* DBExit("Cant Open Debug File: ","debug",DBG_FILE_ABRT); */
                }
        fprintf(fpdbg,"Debug File Truncated\n");
        if( debug==2 )
                fprintf(fpdbg,"FILE: %s LINE %d\n",__FILE__,__LINE__);
        return fpdbg;
}

/*
***************************************************************************
** FUNCTION NAME : msg_handler
** SYNOPSIS      : Generic message handler
***************************************************************************
*/
int CS_PUBLIC msg_handler( DBPROCESS *dbproc, int msgno, int msgstate, int severity, char *msgtext, char *srvname, char *procname, int line)
{

        if( ignore_error(msgno) ){
                if( debug ) fprintf(fpdbg,"Ignoring error #%d\n",msgno);
                return INT_CANCEL;
        }

        /* if(debug) fprintf( stderr,"message %d: ", msgno );
        fprintf( stderr,"%s\n",msgtext ); */

        if ( DBDEAD( dbproc ) || (dbproc == NULL))
                fprintf( stderr, "Server Connection Died : \n%s\n",msgtext );

        if( severity > 16 )
          dbexit();
                /* DBExit("msg_handler(): Program Ter*/

        return(INT_CANCEL  );
}


/*
***************************************************************************
** FUNCTION NAME : DBInit
** SYNOPSIS      : Initialize
***************************************************************************
*/
void DBInit()
{
        if( sybase_initialized==TRUE )
           dbexit();
                /* DBExit("DBInit(): called multiple times","",__LINE__); */

        if (dbinit() == FAIL)
           dbexit();
/*              DBExit("DBInit(): Failure on dbinit",NULL,__LINE__);*/

        dbmsghandle( (MHANDLEFUNC)msg_handler );
        dberrhandle( err_handler );
        add_to_ignore_list(5701);       /* Use Db Message */
        add_to_ignore_list(5703);
        add_to_ignore_list(5704);       /* Change Language */

        sybase_initialized=TRUE;
}

/*************************************************************************/
/* FUNCTION NAME : ConnectToServer                                                      */
/* SYNOPSIS      : Logs into a specified Sybase Data Server              */
/*                 and returns a pointer to the sybase proc ( dbproc )   */
/*************************************************************************/
int ConnectToServer(char *server,char *user,char *password,char *applname, char *Db, DBPROCESS **dbproc )
{
        LOGINREC  *login;

        DBInit();
        if( (login = dblogin()) == NULL) {
                fprintf( stderr, "ConnectToServer: dblogin() failed\n" );
                if(debug) fprintf( fpdbg, "ConnectToServer: dblogin() failed\n" );
                return 0;
        }

        DBSETLUSER( login, user );
        DBSETLPWD(  login, password );
        DBSETLAPP(  login, applname);

        if( (*dbproc = dbopen(login,server) ) == NULL )
         {
          fprintf( fpdbg, "ConnectToServer : user %s login to %s failed\n",
                  user, server );
          dbexit();
        }
        if( dbuse(*dbproc, Db) != SUCCEED )
        {
                if(debug) fprintf( fpdbg, "UseDB : failed to open database %s\n", Db );
                return 0;
        }
        return  1 ;
}


/*
***************************************************************************
** FUNCTION NAME : RunQuery
** SYNOPSIS      : RunQuery runs simple or composite SQL queries and/or
**                 stored procedures and retrieves each row invoking an
**                 application-defined function after each row.
***************************************************************************
*/

int RunQuery( DBPROCESS * dbproc, char *sqltext, int (*UserFunction)(DBPROCESS *,void *,int, int), int  MaxNumRows, int RowSize,void *ReturnData, int  *NumRows, FILE  *fp)
{
        int rc;

        if(debug)
        {
           fprintf(fpdbg,"%d %s RunQuery(%s)\n", __LINE__,__FILE__,sqltext);
           fprintf(fpdbg,"MaxNumRows =%d,RowSize=%d\n",MaxNumRows,RowSize);
        }

        if( dbproc==NULL || DBDEAD(dbproc) ) {
                fprintf(stderr,"DBPROCESS IS DEAD - CANT RUN QUERY\n%s\n",sqltext);
                return FAIL;
        }

        if( NumRows != NULL ) *NumRows = 0;

        /* CLEAR PENDING RESULTS FROM SERVER */
        if( dbcancel(dbproc) != SUCCEED )
        {
                fprintf( stderr, "dbcancel: error while executing\n");
                if(debug) fprintf( fpdbg, "dbcancel: error while executing\n");
                return FAIL;
        }

        /* CLEAR COMMAND BUFFER */
        dbfreebuf( dbproc );

        /* PLACE SQL TEXT IN COMMAND BUFFER */
        if( dbcmd(dbproc, sqltext) != SUCCEED )
        {
                fprintf( stderr, "dbcmd: error while executing...\n");
                if(debug) fprintf( fpdbg, "dbcmd: error while executing...\n");
                return FAIL;
        }

        /* SEND COMMAND BUFFER TO DATA SERVER FOR EXECUTION */
        if( dbsqlexec(dbproc) != SUCCEED )
        {
                fprintf( stderr, "dbsqlexec: error while executing...\n");
                if(debug) fprintf( fpdbg, "dbsqlexec: error while executing...\n");
                return FAIL;
        }

        /* FOR EACH SET OF QUERY RESULTS ... */
        BatchId=0;
        while( ( rc=dbresults(dbproc) ) != NO_MORE_RESULTS )
        {
                if( rc==SUCCEED )
                {
                        /* PROCESS THE ROWS FROM THE CURRENT SET OF RESULTS */
                        rc= ProcessResults(dbproc,UserFunction,MaxNumRows,ReturnData,RowSize,NumRows,fp);
                        if( rc != SUCCEED )
                        {
                                if( rc!=0) {
                                fprintf(stderr,"error %d while executing...\n%s\n", rc,sqltext );
                                if(debug) fprintf(fpdbg,"error %d while executing...\n%s\n", rc,sqltext );
                                } else {
                                fprintf(stderr,"error while executing...\n%s\n", sqltext );
                                if(debug) fprintf(fpdbg,"error while executing...\n%s\n",sqltext );
                                }
                                dbcancel( dbproc );
                                return rc;
                        }

                } else {
                        fprintf( stderr, "dbresults: error while executing...\n");
                        if(debug) fprintf( fpdbg, "dbresults: error while executing...\n");
                        return FAIL;
                }
        }

        /* Abort if Results Return Other Than 0 */
        if( dbhasretstat(dbproc)) {
                rc=dbretstatus(dbproc);
                if(rc!=0 ) {
                        if(debug) fprintf(fpdbg,"Procedure Return Status=%d (not=0)\n",rc );
                        if(rc==SUCCEED)
                                return FAIL;
                        else return(rc);
                }
        }

        return SUCCEED;
}

/*
***************************************************************************
** FUNCTION NAME : ProcessResults
** SYNOPSIS      : ProcessResults retrieves the current set of results one row
**                 at a time invoking an application-defined function after
**                 each row.
***************************************************************************
*/

int
ProcessResults( DBPROCESS * dbproc, int  (*UserFunction)(DBPROCESS *,void *,int, int),
                           int  MaxNumRows, void *ReturnData,
                           int   RowSize, int  *NumRows, FILE *fp)
{
        char   *buffer;
        int    RowLength  = 0;
        auto int  RowStatus;
        int      DataSiz=0;

        if( debug==2 )
        {
        fprintf(fpdbg,"FILE: %s LINE %d ProcessResults()\n",__FILE__,__LINE__);
           fprintf(fpdbg,"MaxNumRows =%d,RowSize=%d\n",MaxNumRows,RowSize);
        }

        /* INCREASE BUFFER SIZE IF NOT LARGE ENOUGH TO HOLD LARGEST ROW */
        RowLength = syb_max(RegularRowLength(dbproc),MaxComputedRowLength(dbproc));

        buffer = (char *)malloc    ((RowLength/512)*512 + 512 );
        if (debug)
           fprintf(fpdbg,"%d MaxNumRows =%d,RowSize=%d\n",__LINE__,MaxNumRows,RowSize);

        if( buffer == NULL )
        {
                fprintf(stderr,"error allocating memory\n" );
                if(debug) fprintf(fpdbg,"error allocating memory\n" );
                return FAIL;
        }

        /* BIND RESULT COLUMNS TO GIVEN BUFFER */
        if( (DataSiz=BindRegularRows(dbproc, buffer)) < 0 )
        {
                fprintf(stderr,"error binding regular row\n" );
                if(debug) fprintf(fpdbg,"error binding regular row\n" );
                return FAIL;
        }
        if( debug )
          fprintf(fpdbg,"%d MaxNumRows =%d,RowSize=%d\n",__LINE__,MaxNumRows,RowSize);

        /* BIND COMPUTE COLUMNS TO GIVEN BUFFER */
        if( (BindComputedRows(dbproc, buffer)) < 0 )
        {
                fprintf(stderr,"error binding computed row\n" );
                if(debug) fprintf(fpdbg,"error binding computed row\n" );
                return FAIL;
        }

        /* FOR THE CURRENT SET OF RESULTS, GET THE DATA FROM SERVER */
        /* ONE ROW AT A TIME, AND CALL THE USER-SUPPLIED ROUTINE    */
        if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d\n",__FILE__,__LINE__);
        memset(buffer, ' ', RowLength);
        if( debug )
           fprintf(fpdbg,"%d MaxNumRows =%d,RowSize=%d\n",__LINE__,MaxNumRows,RowSize);
        while( (RowStatus = dbnextrow(dbproc)) != NO_MORE_ROWS ) {
                if( RowStatus == FAIL ) {
                        if(debug)
                        fprintf(fpdbg,"%d %s ProcessResults: RowStatus=FAIL\n",__LINE__,__FILE__);
                        fprintf(stderr,"error obtaining next row\n" );
                        if(debug) fprintf(fpdbg,"error obtaining next row\n" );
                        return FAIL;

                } else if( RowStatus == BUF_FULL ) {

                        if(debug)
                                fprintf(fpdbg,"%d %s ProcessResults: Buffer Full\n",__LINE__,__FILE__);
                        dbclrbuf(dbproc, 10);

                } else if( fp != NULL ) {

                        if(debug) fprintf(fpdbg,"%d %s Processing Printable Row {%s}\n",__LINE__,__FILE__,buffer);
                        if( strlen(buffer)>0 ) {
                                if( NumRows != NULL ) (*NumRows)++;
                                fprintf(fp, buffer);
                                if( separator!='\0' ) fputc(separator,fp);
                        }   else if(debug) fprintf(fpdbg,"Null Returned\n");

                } else if( UserFunction==NULL ) {
                        if(debug)
                        {
                                fprintf(fpdbg,"Internal Row Handling - No User Function\n" );
                                fprintf(fpdbg,"MaxNumRows =%d,RowSize=%d\n",MaxNumRows,RowSize);
                                fflush(fpdbg);
                        }
                        if(Row_Results(dbproc,buffer,MaxNumRows,DataSiz,RowSize,ReturnData,NumRows)!=NORMAL){
                                fprintf(stderr,"Bad return code from RowResults() Function\n" );
                                if(debug) fprintf(fpdbg,"Bad return code from RowResults() Function\n" );
                                return FAIL;
                        }
                        if(debug && NumRows!=NULL) fprintf(fpdbg,"%d Rows Returned From User Function\n",*NumRows );
                } else {
                        if(debug)
                        {
                                fprintf(fpdbg,"Calling user function\n" );
                        }
                        if((*UserFunction)(dbproc,buffer,RowStatus,BatchId)!=NORMAL){
                                fprintf(fpdbg,"Bad return code from User Function\n" );
                                return FAIL;
                        }
                }
                if( DBCOUNT(dbproc) > 0 ) BatchId++;
                memset(buffer, ' ', RowLength);
        }
        free(buffer);

        return SUCCEED;
}

/*
***************************************************************************
** FUNCTION NAME : Row_Results
** SYNOPSIS      : Process the results of one row
***************************************************************************
*/
int Row_Results( DBPROCESS *dbproc, void *inrow, int MaxNumRows,int DataSiz, int   RowSize, void *ReturnData, int  *NumRows)
 {
        char *row;

        if(debug==2)  {
                fprintf(fpdbg,"%d %s Row_Results() CurRow=%d Size=%d Max=%d\n",__LINE__,__FILE__,(int) DBCURROW(dbproc),RowSize, MaxNumRows);
        }

        if( MaxNumRows==0 ) {
                fprintf(stderr,"PROGRAM ERROR: No Rows Should Have Been Returned\n");
                if(debug) fprintf(fpdbg,"PROGRAM ERROR: No Rows Should Have Been Returned\n");
                return FAIL;
        }

        /* currow starts at 1, Max at 0 */
        if( MaxNumRows!=0 && (int) DBCURROW(dbproc) >  MaxNumRows ) {
                fprintf(stderr,"PROGRAM ERROR: Too Many Rows Returned (%d)\nArray Bound s Will Be Exceeded - Aborting\n",MaxNumRows);
                if(debug) fprintf(fpdbg,"PROGRAM ERROR: Too Many Rows Returned (%d)\nArray Bounds Will Be Exceeded - Aborting",MaxNumRows);
                return FAIL;
        }

        row  =  (char *) ReturnData;

        if( NumRows == NULL ) {
                row +=  (RowSize * ( (int) DBCURROW(dbproc)-1 ));
        } else {
                row +=  (RowSize * ( *NumRows ));
                (*NumRows)++;
        }

        memcpy( row,inrow,DataSiz);

        /* if(debug==2) fprintf(fpdbg,"%d %s row=%s\n", __LINE__,__FILE__,inrow); */

        return NORMAL;
}

/*
***************************************************************************
** FUNCTION NAME : RegularRowLength
** SYNOPSIS      : returns the total length of all columns in the current
**                 set of results
***************************************************************************
*/

int
RegularRowLength( DBPROCESS * dbproc)
{
        int    num_columns, column_num, offset = 0, bind;

        /* GET THE NUMBER OF COLUMNS IN CURRENT QUERY */
        num_columns = dbnumcols( dbproc );
        if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d RegularRowLength() #columns=%d\n",__FILE__,__LINE__,num_columns);

        /* FOR EACH COLUMN ... */
        for( column_num = 1; column_num <= num_columns; column_num++ )
        {
                bind = MapDataType ( dbcoltype(dbproc, column_num) );

                /* ALIGN CURRENT COLUMN IF NEEDED */
                offset += AlignOffset( offset, bind );

                /* COMPUTE OFFSET IN BUFFER FOR NEXT COLUMN */
                offset += GetFieldLength( dbcollen(dbproc,column_num), dbcoltype(dbproc,column_num) );
        }

        return offset;
}

/*
***************************************************************************
** FUNCTION NAME : ComputedRowLength
** SYNOPSIS      : Returns total length of all columns in a sql "compute"
**                 row in the current set of results
***************************************************************************
*/

int
ComputedRowLength( DBPROCESS * dbproc, int clause_id )
{
        int    num_compute_cols,
        col_id,
        offset = 0,
        bind;
        if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d ComputedRowLength()\n",__FILE__,__LINE__);

        /* GET THE NUMBER OF COLUMNS IN COMPUTE CLAUSE */
        num_compute_cols = dbnumalts( dbproc, clause_id );
        offset = 0;

        /* FOR EACH COLUMN IN COMPUTE CLAUSE ... */
        for( col_id = 1; col_id <= num_compute_cols; col_id++ )
        {
                bind = MapDataType ( dbalttype(dbproc, clause_id, col_id) );

                /* ALIGN CURRENT COLUMN IF NEEDED */
                offset += AlignOffset( offset, bind );

                /* COMPUTE OFFSET IN BUFFER FOR NEXT COMPUTE COLUMN */
                offset += GetFieldLength( dbaltlen(dbproc,clause_id,col_id), dbalttype(dbproc,clause_id,col_id) );
        }

        return offset;
}

/*
***************************************************************************
** FUNCTION NAME : MaxComputedRowLength
** SYNOPSIS      : Returns max length of any "computed" rows in current results set
***************************************************************************
*/

int
MaxComputedRowLength( DBPROCESS * dbproc)
{
        int    num_compute_clauses,
        clause_id,
        maxoffset = 0;

        if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d MaxComputedRowLength()\n",__FILE__,__LINE__);

        /* GET THE NUMBER OF COMPUTE CLAUSES IN QUERY */
        num_compute_clauses = dbnumcompute( dbproc );

        /* FOR EACH COMPUTE CLAUSE ... */
        for( clause_id = 1; clause_id <= num_compute_clauses; clause_id++ )
                maxoffset = syb_max( maxoffset, ComputedRowLength(dbproc, clause_id) );

        return maxoffset;
}

/*
***************************************************************************
** FUNCTION NAME : BindRegularRows
** SYNOPSIS      : Binds columns of regular rows in the current result set
                                                 to a given contiguous buffer
** RETURNS       : the length of the row or <0 if error
***************************************************************************
*/

int BindRegularRows( DBPROCESS * dbproc, char * buffer )
{
        int    num_columns, col_id, bind, offset = 0;

        if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d BindRegularRows()\n",__FILE__,__LINE__);
        /* GET THE NUMBER OF COLUMNS IN CURRENT QUERY */
        num_columns = dbnumcols( dbproc );

        /* FOR EACH COLUMN ... */
        for( col_id = 1; col_id <= num_columns; col_id++ )
        {
                bind = MapDataType ( dbcoltype(dbproc, col_id) );

                /* ALIGN CURRENT COLUMN IF NEEDED */
                offset += AlignOffset( offset, bind );

                /* BIND COLUMN TO GIVEN BUFFER AT CURRENT OFFSET */
                if ( dbbind(dbproc,col_id,bind,0, (BYTE *)&buffer[offset]) == FAIL )
                {
                        fprintf( stderr, "dbbind: error while binding column %d\n", col_id );
                        if(debug) fprintf( fpdbg, "dbbind: error while binding column %d\n", col_id );
                        return ( -col_id );
                }

                /* COMPUTE OFFSET IN BUFFER FOR NEXT COLUMN */
                offset += GetFieldLength( dbcollen(dbproc,col_id), dbcoltype(dbproc,col_id));
        }

        return offset;
}

/*
***************************************************************************
** FUNCTION NAME : BindComputedRows
** SYNOPSIS      : Binds columns of each "compute" clause in current results
**                 set to a given contiguous buffer
** RETURNS       : the length of the maximum "compute" row
***************************************************************************
*/

int BindComputedRows( DBPROCESS * dbproc, char * buffer )
{
        int    num_compute_clauses, clause_id, maxoffset = 0;
        int    num_compute_cols, col_id, bind, offset = 0;

        if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d BindComputedRows\n",__FILE__,__LINE__);

        /* GET THE NUMBER OF COMPUTE CLAUSES IN QUERY */
        num_compute_clauses = dbnumcompute( dbproc );

        /* FOR EACH COMPUTE CLAUSE ... */
        for( clause_id = 1; clause_id <= num_compute_clauses; clause_id++ )
        {
                 /* GET THE NUMBER OF COLUMNS IN COMPUTE CLAUSE */
                 num_compute_cols = dbnumalts( dbproc, clause_id );
                 offset = 0;

                  /* FOR EACH COLUMN IN COMPUTE CLAUSE ... */
                  for( col_id = 1; col_id <= num_compute_cols; col_id++ )
                  {
                          bind = MapDataType ( dbalttype(dbproc, clause_id, col_id) );

                          /* ALIGN CURRENT COLUMN IF NEEDED */
                          offset += AlignOffset( offset, bind );

                          /* BIND COLUMN TO GIVEN BUFFER AT CURRENT OFFSET */
                          if( dbaltbind(dbproc,clause_id,col_id,bind,0,(BYTE *)&buffer[offset]) == FAIL )
                          {
                                  fprintf( stderr, "dbaltbind: error while binding clause %d column %d\n", clause_id, col_id );
                                  if(debug) fprintf( fpdbg, "dbaltbind: error while binding clause %d column %d\n", clause_id, col_id );
                                  return ( -clause_id );
                          }

                          /* COMPUTE OFFSET IN BUFFER FOR NEXT COMPUTE COLUMN */
                          offset += GetFieldLength( dbaltlen(dbproc,clause_id,col_id), dbalttype(dbproc,clause_id,col_id) );
                  }

                  maxoffset = syb_max( maxoffset, ComputedRowLength(dbproc, clause_id) );
        }

        return maxoffset;
}

/*
***************************************************************************
** FUNCTION NAME : MapDataType
** SYNOPSIS      : Returns program data type given a server data type
***************************************************************************
*/

int
MapDataType ( int coltype )
{
        int i;

        for ( i = 0; i < MAPTABLENUMROWS; i++ )
                if ( coltype == MapTable[i].datatype )
                        return (MapTable[i].programtype);

        fprintf ( stderr, "\nMapDataType : FATAL ERROR - Unknown column type" );
        if(debug) fprintf ( fpdbg, "\nMapDataType : FATAL ERROR - Unknown column type" );
        exit (0);
        return 0;
}


/*
***************************************************************************
** FUNCTION NAME : GetFieldLength
** SYNOPSIS      : returns the length of the field
***************************************************************************
*/
int
GetFieldLength ( int collen,int coltype )

{
        int length, i, maptype;

   maptype = MapDataType(coltype);
        for ( i = 0; i < FIELDLENGTHTABLENUMROWS; i++ )
                if ( maptype == FieldLengthTable[i].programtype ) {
                        length = FieldLengthTable[i].length;
                        break;
                }

        if( i==FIELDLENGTHTABLENUMROWS ) {
                fprintf(stderr,"\nGetFieldLength : FATAL ERROR - Unknown bind type %d map=%d" ,coltype,maptype);
                if(debug) fprintf ( fpdbg, "\nGetFieldLength : FATAL ERROR - Unknown length for dbbind" );
                exit (0);
        }

        if   ( length > 0 )
                        return (length);
        else    return (collen - length);
}


/*
***************************************************************************
** FUNCTION NAME : AlignOffset
** SYNOPSIS      : Aligns data on proper boundary for compilers
** RETURNS       : # of bytes added to offset so that proper alignment results
***************************************************************************
*/

int
AlignOffset( int offset, int programtype )

{

# if   vax
        return (0);
# else
        int d, r;
        int i;

        for ( i = 0; i < FIELDLENGTHTABLENUMROWS; i++ )
                if ( programtype == FieldLengthTable[i].programtype ) {
                        d = FieldLengthTable[i].alignment;
                        break;
                }

        if( i==FIELDLENGTHTABLENUMROWS ) {
                fprintf ( stderr, "\nBindAlignment : FATAL ERROR - Unknown type for dbbind" );
                if(debug) fprintf ( fpdbg, "\nBindAlignment : FATAL ERROR - Unknown type for dbbind" );
                exit (0);
        }

        if ( (r = (offset % d)) == 0 )
                return (0);
        else
                return (d - r);
# endif

}

/*
***************************************************************************
** FUNCTION NAME : store_results
** SYNOPSIS      : Bulk results from one server to another
***************************************************************************
*/
int store_results( DBPROCESS  *to_proc, DBPROCESS  *fr_proc, char *table, char *query, int batchsize, int (*UserFunction)(DBPROCESS *,void *,int, int))
{
        DBCHAR         dbchar     [MAXCOLUMNS][TEXTSIZE];
        DBINT          dbint      [MAXCOLUMNS];
        DBBIT          dbbit      [MAXCOLUMNS];
        DBFLT8         dbflt8     [MAXCOLUMNS];
        DBMONEY        dbmoney    [MAXCOLUMNS];
        DBDATETIME     dbdatetime [MAXCOLUMNS];
        int            coltype    [MAXCOLUMNS];
        int            no_columns;
        int            batch = 0;
        int            rows = 0;
        int            i;

        if( debug ) fprintf(fpdbg,"FILE: %s LINE %d store_results(%s)\n",__FILE__,__LINE__,query);

        /* CLEAR ANY PENDING RESULTS FROM SERVER */
        if( dbcancel(fr_proc) != SUCCEED )
        {
                fprintf( stderr, "dbcancel : error while executing\n");
                if(debug) fprintf( fpdbg, "dbcancel : error while executing\n");
                return FAIL;
        }

        dbfreebuf( fr_proc );                   /* CLEAR COMMAND BUFFER */

        /* PLACE SQL TEXT IN COMMAND BUFFER */
        if( dbcmd(fr_proc, query) != SUCCEED )
        {
                fprintf( stderr, "dbcmd : error while executing...\n");
                if(debug) fprintf( fpdbg, "dbcmd : error while executing...\n");
                return FAIL;
        }

        /* SEND COMMAND BUFFER TO DATA SERVER FOR EXECUTION */
        if( dbsqlexec(fr_proc) != SUCCEED )
        {
                fprintf( stderr, "store_results(): dbsqlexec error while executing...\n");
                if(debug) fprintf( fpdbg, "store_results(): dbsqlexec error while executing...\n");
                return FAIL;
        }

        if( debug ) fprintf(fpdbg,"FILE: %s LINE %d query executed\n",__FILE__,__LINE__);
        while(dbresults(fr_proc) != NO_MORE_RESULTS)
        {
                no_columns = dbnumcols(fr_proc);
                if ( no_columns >= MAXCOLUMNS )
        /*              DBExit ("Maximum Columns Exceeded.  Command aborted.\n","", MAX_COLS_ABRT);*/

                for (i=1; i<=no_columns; i++)
                {
                        coltype[i] = dbcoltype(fr_proc, i);
                        if ( coltype[i] == SYBTEXT || coltype[i] == SYBIMAGE )
                          dbexit();
                /*              DBExit ("Text & Image BCP Not Allowed.  Command aborted.\n","", TXT_IMG_ABRT);*/
                }

                if (bcp_init(to_proc, table, NULL, NULL, DB_IN) == FAIL)
        /*              DBExit("BCP Init Failed. Command Aborted.\n","",BCPINIT_FAIL_ABRT); */

                /*** Set-Up BCP Binds ***/
                if( debug ) fprintf(fpdbg,"FILE: %s LINE %d setting up binds\n",__FILE__,__LINE__);
                for (i=1; i<=no_columns; i++)   {
                        if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d store_results() column=%d\n",__FILE__,__LINE__,i);
                        switch (coltype[i]) {
                        case SYBCHAR:
                                if(bcp_bind(to_proc,(BYTE *)dbchar[i],0,0,(BYTE*)NULL,0,0,i)==FAIL)
                                        /*DBExit ("BCP Bind Failed. Command Aborted.\n","",CHAR_BIND_ABRT);*/
                                break;
                        case SYBINT1:
                        case SYBINT2:
                        case SYBINT4:
                                if(bcp_bind(to_proc,(BYTE *)&dbint[i],0,-1,(BYTE*)NULL,0,0,i)==FAIL)
                        /*              DBExit ("BCP Bind Failed. Command Aborted.\n", "",INT_BIND_ABRT);*/
                                break;
                        case SYBFLT8:
                                if ( bcp_bind (to_proc, (BYTE *)&dbflt8[i], 0, -1, (BYTE *) NULL, 0, 0, i) == FAIL )
                        /*              DBExit ("BCP Bind Failed. Command Aborted.\n", "",FLT_BIND_ABRT);*/
                                break;
                        case SYBMONEY:
                                if ( bcp_bind (to_proc, (BYTE *)&dbmoney[i], 0, -1, (BYTE *) NULL, 0, 0, i) == FAIL )
                        /*              DBExit ("BCP Bind Failed. Command Aborted.\n", "",MON_BIND_ABRT);*/
                                break;
                        case SYBDATETIME:
                                if ( bcp_bind (to_proc, (BYTE *)&dbdatetime[i], 0, -1, (BYTE *) NULL, 0, 0, i) == FAIL )
                        /*              DBExit ("BCP Bind Failed. Command Aborted.\n", "",DATE_BIND_ABRT);*/
                                break;
                        case SYBBINARY:
                                if ( bcp_bind (to_proc, (BYTE *)dbchar[i], 0, 0, (BYTE *) NULL, 0, 0, i) == FAIL )
                        /*              DBExit ("BCP Bind Failed. Command Aborted.\n", "",BIN_BIND_ABRT); */
                                break;
                        case SYBBIT:
                                if ( bcp_bind (to_proc, (BYTE *)&dbbit[i], 0, -1, (BYTE *) NULL, 0, 0, i) == FAIL )
                        /*              DBExit ("BCP Bind Failed. Command Aborted.\n", BIT_BIND_ABRT);*/
                                break;
                        default:
                          dbexit();
                /*              DBExit ("ERROR: Inavlid Column Type.\n", BAD_TYP_ABRT);*/
                        }
                }     /*** End Switch                    ***/

                if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d getting results\n",__FILE__,__LINE__);
                while (dbnextrow(fr_proc) != NO_MORE_ROWS)
                {
                        for (i=1; i<=no_columns; i++) {
                                bcp_collen (to_proc, (int)dbdatlen(fr_proc, i) ,i);
                                if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d store_results(col=%d datlen=%d)\n",__FILE__,
                                    __LINE__,i,(int) dbdatlen(fr_proc,i));

                                switch (coltype[i]) {
                                case SYBCHAR:
                                        strncpy(dbchar[i], (DBCHAR *) dbdata (fr_proc, i), (int)dbdatlen(fr_proc, i));
                                        break;
                                case SYBINT1:
                                case SYBINT2:
                                case SYBINT4:
                                        if ((int)dbdatlen(fr_proc, i) > 0)
                                                dbint[i]  = *((DBINT *) dbdata (fr_proc, i));
                                        break;
                                case SYBFLT8:
                                        if ((int)dbdatlen(fr_proc, i) > 0){
                                                dbflt8[i]  = *((DBFLT8 *) dbdata (fr_proc, i));
                                                if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d store_results(col %d float=%f)\n",
                                                    __FILE__,__LINE__,i, *((DBFLT8 *) dbdata (fr_proc, i)));
                                        }
                                        else if( debug==2 ) fprintf(fpdbg,"FILE: %s LINE %d store_results(null float col=%d)\n",
                                            __FILE__,__LINE__,i);
                                        break;
                                case SYBMONEY:
                                        if ((int)dbdatlen(fr_proc, i) > 0)
                                                dbmoney[i]  = *((DBMONEY *) dbdata (fr_proc, i));
                                        break;
                                case SYBDATETIME:
                                        if ((int)dbdatlen(fr_proc, i) > 0)
                                                dbdatetime[i]  = *((DBDATETIME *) dbdata (fr_proc, i));
                                        break;
                                case SYBBINARY:
                                        strncpy(dbchar[i], (char *)(DBBINARY *) dbdata (fr_proc, i),
                                            (int)dbdatlen(fr_proc, i)   );
                                        /* bcp_collen (to_proc, (int)dbdatlen(fr_proc, i) ,i);*/
                                        break;
                                case SYBBIT:
                                        if ((int)dbdatlen(fr_proc, i) > 0)
                                                dbbit[i]  = *((DBBIT *) dbdata (fr_proc, i));
                                        break;
                                }
                        }
                        if(UserFunction!=NULL) {}
                /*              UserFunction(no_columns,coltype,dbchar,dbint,dbbit,dbflt8,dbmoney,dbdatetime);*/ /*?*/
                        ++rows;
                        if( debug==2 )
                                fprintf(fpdbg,"FILE: %s LINE %d sending row\n",__FILE__,__LINE__);

                        bcp_sendrow (to_proc);
                        if ( rows%batchsize == 0 ) bcp_batch (to_proc);
                }
                bcp_done (to_proc);
                if(debug) fprintf (stderr,"\n(%d rows affected, return status = %d)\n", rows, (DBINT) dbretstatus(to_proc));
                if(debug) fprintf (fpdbg,"\n(%d rows affected, return status = %d)\n", rows, (DBINT) dbretstatus(to_proc));
        }
        return SUCCEED;
}

