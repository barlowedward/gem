# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package monitor;
@ISA = qw(Plugin);

use strict;
use DBIFunc;

use Tk::ErrorDialog;
use Tk::After;
use Tk::Optionmenu;
use Tk::DBI::Table;
use CommonFunc;

use Repository;

use vars qw($VERSION);
$VERSION = ".01";

my($modulename)		="monitor.pm";
my($std_page_name)	="monitoring";
my($currentRefRate)	=30;
my($last_entrypath, $last_query)=("unknown",undef);
my($results_frame, $current_main_frame);
my($afterid);
my($is_frozen)		=0;
my($currentServer,$currentMonitor,$currentServerType,$isMdaEnabled,$textObject);
my($prior_monitor)="";
my(%save_data_array);
my($freeze_btn);
my(@menuitems) = ( "Configuration/Register Sybase Server","Configuration/Register SQL Server","Configuration/Deregister Current Server","Sybase/Monitor",	"Sql Server/Monitor");
my($last_event_ptr);
my($pagetop_notes_text);	# the text for the notes item
my($rowtwo_frame);
my($optionmenu_widget);
my(@optionmenu_items);
my($optionmenu_text);

my %SStyles;
my $show_system=1;

my(%reportitems)=(
	 " Show Processes"	=>	"Processes"	 ,
	 " Show Running Processes"=>	"Activity"	 ,
	 " Show All Locks"	=>	"Locks"		 ,
	 " Show Blocking Locks"	=>	"Blocks"	 ,
 	 "MDA Engine Activity"  =>	"Engine"	 ,
	 "MDA System Waits"     =>	"Waits"		 ,
	 "MDA Cache Usage"	=>	"Cache"		 ,
	 "MDA Cache Pool"	=>	"Pool"		 ,
	 "MDA Top Objects"	=>	"Objects"		 ,
	 "MDA IO Activity"	=>	"IO" 	 	 ,
	 "MDA IO Queue Info"	=>	"IO Queue"	 ,
	 "MDA Network Activity"	=>	"Network"	 ,
	 "MDA Processes"	=>	"Procs"		 	);

my($dflt_reportitem)=" Show Running Processes";

sub event_handler
{
	my($package)=shift;
	my(%args)=@_;
	$last_event_ptr=\%args;

	if( defined $$package{-modulename} and $args{-curtab} ne $std_page_name ) {
		$package->ignore_events(1);
		$main::notebook_f->raise(ucfirst($std_page_name));
		$args{-curtab} = $std_page_name;
		$package->ignore_events(undef);
	}

	# freeze if you are tabbing out of the page
	if( $args{-oldtab} eq $std_page_name and $is_frozen==0 ) {
		$package->do_freeze_btn();
	}

	if( $args{-eventtype} eq "menu" ) {
		my($found)="FALSE";
		foreach ( @menuitems ) { $found="TRUE" if $args{-menuitem} eq $_; }
		return unless $found eq "TRUE";

		$package->statusmsg( "[$modulename] event_handler() called\n" );
		$package->statusmsg( "[$modulename] menuclick($args{-menuitem})\n" );

		my($typ);
		if( $args{-menuitem} eq "Sybase/Monitor" ) {
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
			$typ='sybase';
		} elsif( $args{-menuitem} eq "Sql Server/Monitor") {
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
			$typ='sqlsvr';
		} elsif( $args{-menuitem} eq "Configuration/Register Sybase Server"
		or  $args{-menuitem} eq "Configuration/Register SQL Server" ) {
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
			$package->ignore_events(1);
			my(%configopts);
			if( $args{-menuitem} =~ /Sybase/ ) {
				$configopts{"Server Type"} = "Sybase ASE Server on Unix";
				$typ='sybase';
			} else {
				$configopts{"Server Type"} = "Microsoft SQL Server";
				$typ='sqlsvr';
			}
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";

		#	if( $$de_servercatref eq "Unix Server" ) {
		#		$configopts{"Server Type"} = "Unix or Linux Server";
		#	} else {
		#		$configopts{"Server Type"} = "Windows Server";
		#	} } elsif(  $$de_servercatref eq "Sybase Server" ) {
		#		$configopts{"Server Type"} = "Sybase ASE Server on Unix";
		#	} elsif( $$de_servercatref eq "Microsoft SQL" ) {
		#		$configopts{"Server Type"} = "Microsoft SQL Server";
		#	} else {
		#		$configopts{"Server Type"} = "Oracle Server on Unix";
		#	}
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
			main::tk_add_a_server(%configopts);
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
			$package->ignore_events(undef);
		} elsif( $args{-menuitem} eq "Configuration/Deregister Current Server" ) {
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
		} else {
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
		}


		$package->ignore_events(1);
		if( $args{-context} ne $typ ) {
			#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
			$package->refresh_tree($typ);
			$args{-context}=$typ;		# the refresh_tree sets global variable but we are not
							# triggering additional event so we do this.
			$args{-entrypath}="/$typ";
		}

#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
		my($dummy,$typ2,$server,$db)=split(/\//,$args{-entrypath});
		$currentServer=$server  if defined $server;
		$currentServerType=$args{-context} unless $args{-context} eq "Unknown";

		if( $args{-curtab} ne $std_page_name ) {
        		$package->statusmsg( "[$modulename] event_handler() raising tab $std_page_name\n" );
			$main::notebook_f->raise(ucfirst($std_page_name));
		}

		#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
		$package->redraw();
		$package->ignore_events(undef);
		#print "DBG DBG - LINE ",__LINE__," of ",__FILE__,"\n";
	} elsif( $args{-eventtype} eq "tabchange" ) {

		return if $args{-curtab} ne $std_page_name;
		$package->statusmsg( "[$modulename] event_handler() called\n" );
		$package->statusmsg(  "[$modulename] monitor tab has been raised\n" );

		my($ok);
		foreach( main::get_server_types() ) { next if $args{-context} ne $_; $ok=1; last; }
		if( ! $ok ) {
			$package->refresh_tree('sqlsvr');
			$args{-context}='sqlsvr';	# the refresh_tree sets global variable but we are not
							# triggering additional event so we do this.
		}

		my($dummy,$typ,$server,$db)=split(/\//,$args{-entrypath});
		$currentServer=$server if defined $server;
		$currentServerType=$args{-context} unless $args{-context} eq "Unknown";
		$package->redraw();

	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {
		return if $args{-curtab} ne $std_page_name;

		$package->statusmsg( "[$modulename] event_handler(tree browse) called\n" );
		my($dummy,$typ,$server,$db)=split(/\//,$args{-entrypath});

		if( $args{-context} ne "Unknown" ) {
			$currentServerType=$args{-context};
		} elsif( $typ eq "Sql Server" ) {
			$currentServerType="sqlsvr";
		} elsif( $typ eq "Sybase ASE" ) {
			$currentServerType="sybase";
		} else {
			warn "Warning: Unknown Type $typ\n";
		}
		$currentServer=$server if defined $server;
		$package->redraw();

	} elsif( $args{-eventtype} eq "treertclick" ) {

		return if $args{-curtab} ne $std_page_name;
		$package->statusmsg( "[$modulename] event_handler() called\n" );
		my($dummy,$typ,$server,$db)=split(/\//,$args{-entrypath});
		$currentServer=$server if defined $server;
		$currentServerType=$args{-context} unless $args{-context} eq "Unknown";
		$package->redraw();

		#my($entrypath, $clktext)=($args{-entrypath},$args{-clicktext});
		#$package->statusmsg(  "[$modulename] rtclick(Path=$entrypath Txt=$clktext)\n" );
		#$package->browse($entrypath);

	} elsif( $args{-eventtype} eq "expbutton" ) {
		if( defined $$package{-modulename} ) {
			if( $currentServerType ne $args{-context} ) {
				#print "DBG: Setting type to $args{-context}\n";
				$currentServerType = $args{-context};
				$currentServer=undef;
				#$currentMonitor=undef;
				$package->refresh_tree();
				$package->set_topLabel();
				$afterid->cancel if defined $afterid;
				$afterid=undef;
			}
		}

	} elsif( $args{-eventtype} eq "imgbutton" ) {
	}
}

sub getPluginPanes { return $std_page_name; }

sub getTreeRtclk {
	my($package,$curTreeType,$treeitmptr)=@_;
	return( undef ) unless $curTreeType eq "sybase" or $curTreeType eq "sqlsvr";
	my(@rightclicks);
	foreach my $itm ( @$treeitmptr ) {
		my($dummy,$typ,$server,$db)=split(/\//,$itm);
		if( defined $server ) {
			my( @db_rclk )= ("Monitor $server");
			push @rightclicks, \@db_rclk;
		} else {
			push @rightclicks, undef;
		}
	}
	return \@rightclicks;
}
# 1 Button For Each Type Of Server
sub getExplorerButtons {
	my($package)=shift;
	my(%labels);
	if( defined $$package{-modulename} ) {
		$labels{"Sybase ASE"}="sybase";
		$labels{"Sql Server"}="sqlsvr";
	}
	return( %labels );
}

sub getTreeItems {
	my($package,$curTreeType)=@_;
	if( defined $$package{-modulename} ) {
		my(@x);
		if( $curTreeType eq "sybase" ) {
			push @x, "/Sybase ASE";
			$currentServerType="sybase";
			foreach ( get_password(-type=>"sybase") ) { push @x, "/Sybase ASE/$_"; }
		} else {
			$currentServerType="sqlsvr";
			push @x, "/Sql Server";
			foreach ( get_password(-type=>"sqlsvr") ) { push @x, "/Sql Server/$_"; }
		}
		return \@x;
	}
	return undef;
}

sub getMenuItems { return @menuitems; }

sub do_freeze_btn {
	my($package)=@_;
	if( $is_frozen ) {
		$is_frozen=0;
		$freeze_btn->configure(-text=>'Freeze  ');
		$package->statusmsg( "[$modulename] un freezing monitor\n" );
		$package->redraw();
	}else {
		$is_frozen=1;
		$freeze_btn->configure(-text=>'UnFreeze');
		$package->statusmsg( "[$modulename] freezing monitor\n" );
		#print "DBG: Canceling afterid is $afterid\n";
		$afterid->cancel if defined $afterid;
		$afterid=undef;
	}
	$package->set_topLabel();
}

my(%serverstatus);
sub init_server {
	my($package)=@_;
	return unless defined $currentServer;
	$package->statusmsg( "[$modulename] init_server(server=$currentServer, menu=$optionmenu_text)\n" );

	$currentMonitor  =  $reportitems{$optionmenu_text};
	if( ! defined $serverstatus{$currentServer} ) {
		if( $currentServerType eq "sqlsvr" ) {
			$serverstatus{$currentServer}="SQLSVR";
		} else {
			my($connectionmanager)=$$package{-connectionmanager};
			$package->statusmsg(  "[$modulename] CONNECTING TO $currentServer TYPE as $currentServerType \n" );
			my($c,$errormsg)=$connectionmanager->connect(-name=>$currentServer, -type=>$currentServerType );
			if( ! $c ) {
				$package->statusmsg("[$modulename] unable to connect to $currentServerType Server $currentServer: $errormsg\n");
				return;
			}
			main::colorize_tree($currentServer);

			$serverstatus{$currentServer}="SYBASE";
			my(@rc) = $connectionmanager->query(
					-connection=>$c,
					-db=>'master',
					-query=>"select * from sysconfigures where config=356 and value=1" );

		 	if( $#rc == 0 ) {

		 		# well enable monitoring is on... check number of tables
		 		@rc = $connectionmanager->query( -connection=>$c, -db=>'master',
			-query=>"select count(*) from master..sysobjects where type='U' and name like 'mon%'" );
				foreach (@rc) {
					my($num)=$connectionmanager->decode_row($_);

            				$serverstatus{$currentServer}="SYBASEMDA" if $num > 15;
            				# there are ~20 of these things so this will work
            			}
		 	};
            	}
         }

         $isMdaEnabled="FALSE";
         $isMdaEnabled="TRUE" if $serverstatus{$currentServer} eq "SYBASEMDA";

	my($numitms)=$#optionmenu_items;
	if(  $serverstatus{$currentServer} eq "SYBASEMDA" ) {
       		@optionmenu_items =sort keys %reportitems;
       	} else {
       		@optionmenu_items =grep( !/^MDA/, sort keys %reportitems );
       	}

       	if( $numitms != $#optionmenu_items ) {
       		my($found)=0;

       		foreach ( @optionmenu_items) { next unless $_ eq $optionmenu_text; $found++; last; }
       		if( ! $found ) {
			$package->debugmsg( "[$modulename] Resetting Option Menu as $optionmenu_text not found ($currentMonitor)\n" );
       			$optionmenu_text = $dflt_reportitem;
       		}
       		my($x)=$optionmenu_text;
		$currentMonitor  =  $reportitems{$optionmenu_text};
		# this funny little code line prevents resizing graphics glitches i had the easy way
		my($optionmenu_widget2)=	$rowtwo_frame->Optionmenu(
				-variable => \$optionmenu_text,
				-options=>	[@optionmenu_items],
				-command=> sub  { $package->selectOptionMenu(@_); });
		main::dohelp($optionmenu_widget2, "Select the Report You Wish To Run\n" );

		$optionmenu_widget->packForget()  if defined $optionmenu_widget and Tk::Exists($optionmenu_widget);
		$optionmenu_widget2->pack( -side=>'left', -padx=>3, -pady=>0);

		$optionmenu_widget->destroy() if defined $optionmenu_widget and Tk::Exists($optionmenu_widget);
		$optionmenu_widget=$optionmenu_widget2;

#print "DBG: LINE ",__LINE__," Option Menu Text=$optionmenu_text ($currentMonitor)\n";
		$optionmenu_text = $x;

		$currentMonitor  =  $reportitems{$optionmenu_text};
#print "DBG: LINE ",__LINE__," Option Menu Text=$optionmenu_text ($currentMonitor)\n";

	}

#print "DBG: LINE ",__LINE__," Option Menu Text=$optionmenu_text ($currentMonitor)\n";

#print "DBG: DoneOption Menu Text to $optionmenu_text ($currentMonitor)\n";
#print "DBG: DOne init_server\n";
}

sub selectOptionMenu {
	my($package)=@_;
	$package->debugmsg( "[$modulename] selectOptionMenu(".join(" ",@_).") called\n" );

	print "DBG: LINE ",__LINE__," $optionmenu_text ($currentMonitor) \n";

	my($found)=0;
	foreach ( @optionmenu_items) { next unless $_ eq $optionmenu_text; $found++; last; }
	$package->debugmsg( "[$modulename] Resetting Option Menu Text as $optionmenu_text not found ($currentMonitor)\n") unless $found;
	$optionmenu_text =  $dflt_reportitem unless $found;
	$currentMonitor  =  $reportitems{$optionmenu_text};
    	return unless $found;

	my($old_isfrozen)= $is_frozen;
    	$is_frozen 	 = 0;
    	$package->redraw();
    	$is_frozen 	 = $old_isfrozen;
}

# Handle the whole top of page thing
# returns 1 if things are ok, 0 if they are not ok
sub set_topLabel {
	my($package)=@_;
	if( ! defined $currentServer ) {
		$pagetop_notes_text="Monitor (no server selected)";
	} elsif( ! defined $currentMonitor ) {
		$pagetop_notes_text="Monitor (no monitor selected)";
	} else {
		$pagetop_notes_text ="$optionmenu_text Monitor";
		$pagetop_notes_text .= " (Frozen)" if $is_frozen;
	}
}

sub onDblClick
{
	my($package,%rowdata)=@_;

	$package->debugmsg( "[$modulename] onDblClick()  \n" );

	if( defined $rowdata{spid} and $rowdata{spid} =~ /^\d+\s*$/) {
		$package->debugmsg( "[$modulename] spid -> $rowdata{spid}\n" );
		my($connectionmanager)=$$package{-connectionmanager};
		my($c,$errormsg)=$connectionmanager->connect(-name=>$currentServer, -type=>$currentServerType );
		if( ! $c ) {
			$package->statusmsg("[$modulename] unable to connect to $currentServerType Server $currentServer: $errormsg\n");
			return;
		}
		my($sql_query);
		if( $serverstatus{$currentServer} eq "SQLSVR" ) {
			return;
		#} elsif( $serverstatus{$currentServer} eq "SYBASEMDA" ) {
		#	$sql_query="exec sp__showplan ".$rowdata{spid}.",null,null,null";
		} else {
			$sql_query="exec sp__showplan ".$rowdata{spid}.",null,null,null";

		}

		$package->debugmsg( "[$modulename] executing: $sql_query\n" );
		my(@rc) = $connectionmanager->query(
				-connection=>$c,
				-db=>'master',
				-print_hdr=>1 ,
				-query=>$sql_query );

		if( $#rc<= 0 ) {
			$package->debugmsg( "[$modulename] insufficient rows returned... not displaying\n" );
			foreach (@rc) {
				my(@x)=$connectionmanager->decode_row($_);
				print join(" ",@x);
			}
			return;
		}

		my($hdr)=shift @rc;
		my(%dlg_svrvar);
		foreach (@rc) {
			my(@x)=$connectionmanager->decode_row($_);
			$package->debugmsg( "[$modulename] ",join(",",@x),"\n" );
			my($cnt)=0;
			foreach ( @$hdr ) {
				if( defined $dlg_svrvar{$_} ) {
					$dlg_svrvar{$_} = reword($dlg_svrvar{$_}." ".$x[$cnt],80,120)
						if $dlg_svrvar{$_} ne $x[$cnt];
					$cnt++;
				} else {
					$dlg_svrvar{$_} = reword($x[$cnt++],80,120);
				}
			}
	 	};

		# sql server must be a language event

		my $d= $package->DialogBox(	-title =>'Showplan Details',-buttons => [ "Ok"] );
		my($cnt)=1;
		foreach ( @$hdr ) { main::dialog_addvar($d,\%dlg_svrvar,$_,undef, $cnt++,"label"); }
		$package->do_freeze_btn() unless $is_frozen;
		$package->debugmsg( "[$modulename] Displaying Dialog Box\n" );

		$d->Show();
	 } else {
	 	$package->debugmsg( "[$modulename] No Spid Field Passed\n" );
	 }
}

sub onClick {
	my($package,%rowdata)=@_;

	$package->debugmsg( "[$modulename] onClick()  \n" );

	if( defined $rowdata{spid} and $rowdata{spid} =~ /^\d+\s*$/) {
		$package->debugmsg( "[$modulename] spid -> $rowdata{spid}\n" );
		# SQL TEXT
		#foreach (keys %rowdata) { $package->debugmsg( "[$modulename] $_ -> $rowdata{$_}\n" ); }

		my($connectionmanager)=$$package{-connectionmanager};
		my($c,$errormsg)=$connectionmanager->connect(-name=>$currentServer, -type=>$currentServerType );
		if( ! $c ) {
			$package->statusmsg("[$modulename] unable to connect to $currentServerType Server $currentServer: $errormsg\n");
			return;
		}
		my($sql_query);
		if( $serverstatus{$currentServer} eq "SQLSVR" ) {
			$sql_query="dbcc inputbuffer(".$rowdata{spid}.")";
		} elsif( $serverstatus{$currentServer} eq "SYBASEMDA" ) {
			$sql_query="exec sp__monsql ".$rowdata{spid};
		} else {
			$sql_query="dbcc sqltext(".$rowdata{spid}.")";
		}

		$package->statusmsg( "[$modulename] executing: $sql_query\n" );
		my(@rc) = $connectionmanager->query(
				-connection=>$c,
				-db=>'master',
				-print_hdr=>1 ,
				-query=>$sql_query );

		if( $#rc<= 0 ) {
			$package->debugmsg( "[$modulename] insufficient rows returned... not displaying\n" );
			foreach (@rc) {
				my(@x)=$connectionmanager->decode_row($_);
				print join(" ",@x);
			}
			return;
		}

		my($hdr)=shift @rc;
		my(%dlg_svrvar);
		foreach (@rc) {
			my(@x)=$connectionmanager->decode_row($_);
			$package->debugmsg( "[$modulename] ",join(",",@x),"\n" );
			if( $serverstatus{$currentServer} eq "SQLSVR" and $x[0] ne "Language Event" ) {
				$package->debugmsg( "[$modulename] Not a language event ($x[0])\n" );
				next;
			}
			my($cnt)=0;
			foreach ( @$hdr ) {
				#next if $cnt>0 and $_ ne "SQLText" and
				if( defined $dlg_svrvar{$_} ) {
					$dlg_svrvar{$_} = reword($dlg_svrvar{$_}." ".$x[$cnt],80,120)
						if $dlg_svrvar{$_} ne $x[$cnt];
					$cnt++;
				} else {
					$dlg_svrvar{$_} = reword($x[$cnt++],80,120);
				}
			}
	 	};

		if( $serverstatus{$currentServer} eq "SQLSVR"
				and ! defined $dlg_svrvar{EventType}	) {
				$package->statusmsg( "[$modulename] Not a language event\n" );
				return;
		}

		# sql server must be a language event
		my $d= $package->DialogBox(	-title =>'Process Details',-buttons => [ "Ok"] );
		my($cnt)=1;
		foreach ( @$hdr ) { main::dialog_addvar($d,\%dlg_svrvar,$_,undef, $cnt++,"label"); }

		if( $serverstatus{$currentServer} ne "SQLSVR" ) {
			$sql_query="exec sp_showplan ".$rowdata{spid}.",null,null,null";

			dbi_msg_ok(1000);
			$package->statusmsg( "[$modulename] executing: $sql_query\n" );
			my(@rc) = $connectionmanager->query(
				-connection=>$c,
				-db=>'master',
				#-debug=>1,
				-query=>$sql_query );
			#print "$$#rc rows found\n";


			$dlg_svrvar{Showplan}="";
			foreach (@rc) {
				my(@x)=$connectionmanager->decode_row($_);
				#print "0: $x[0] || 1: $x[1] || 2: $x[2]\n";
				$x[0]=~ s/^\s*01000:\s*//;
				$x[0]=~s/\s+$//;
				$dlg_svrvar{Showplan} .= $x[0]."\n" if $x[0] ne "";
			}
			main::dialog_addvar($d,\%dlg_svrvar,"Showplan",undef, $cnt++,"label")
				if $dlg_svrvar{Showplan} ne "";
			dbi_msg_exclude(1000);
		}

		$package->do_freeze_btn() unless $is_frozen;
		$package->debugmsg( "[$modulename] Displaying Dialog Box\n" );

		$d->Show();
		#print "Done Working on spid \n";
	 } else {
	 	$package->debugmsg( "[$modulename] No Spid Field Passed\n" );
	 }
}

sub init {
	my($package)=@_;
	$package->debugmsg( "[$modulename] init()\n" );
	dbi_msg_ok(2528);

	$afterid->cancel if defined $afterid;
	$afterid=undef;

	$current_main_frame = $package->clear_tab($std_page_name);

	# we need to do this because welcome tab is not defined maybe
	if( defined $$package{-modulename} ) {
		$package->set_default_tab_to_raise($std_page_name);
		$main::notebook_f->raise(ucfirst($std_page_name));
	}

	my($rowone_frame)=$current_main_frame->Frame( -relief=>'ridge' )->pack( -fill=>'x', -side=>'top');
	$rowone_frame->Label( -text=>"Server:",-justify=>'left', -anchor=>'w',-padx=>2)->pack(-side=>'left',  -ipadx=>2);
	$rowone_frame->Label(
					-justify=>'left', -anchor=>'w',-textvariable=>\$currentServer,
					-relief=>'sunken',
					-background=>"white", -width=>20,
					-padx=>4)->pack(-side=>'left',  -ipadx=>2);
	$rowone_frame->Label( -text=>"Status:",-justify=>'left', -anchor=>'w',-padx=>2)->pack(-side=>'left',  -ipadx=>2);
	$rowone_frame->Label(
					-justify=>'left', -anchor=>'w',
					-relief=>'sunken',-textvariable=>\$pagetop_notes_text,
					-background=>"white",
					-padx=>4)->pack(-side=>'left', -fill=>'x', -expand=>1, -ipadx=>2);

	# the second line
	$rowtwo_frame=$current_main_frame->Frame( -relief=>'ridge' )->pack( -fill=>'x', -side=>'top');

	@optionmenu_items =sort keys %reportitems;

	if( ! $optionmenu_text ) {
		$optionmenu_text = $dflt_reportitem;
		$currentMonitor  = $reportitems{$dflt_reportitem};
	}

	$optionmenu_widget=	$rowtwo_frame->Optionmenu(
				-variable => \$optionmenu_text,
				-options=>	[@optionmenu_items],
				-command=> sub  { $package->selectOptionMenu(@_); })->pack( -side=>'left', -padx=>3, -pady=>0);
	main::dohelp($optionmenu_widget, "Select the Report You Wish To Run\n" );

	$rowtwo_frame->Entry(-textvariable=>\$currentRefRate, -relief=>'sunken', -bg=>'yellow', -width=>3 )->pack(-side=>'right',-padx=>3, -pady=>8 );
	$rowtwo_frame->Label(-text=>"Refresh\nRate")->pack(-side=>'right',-padx=>3, -pady=>8 );

	$freeze_btn = $package->make_button(-widget=>$rowtwo_frame,
    			-help=>'Freeze Auto Redraw',
    			-text=>"Freeze",
    			-command=> sub  { $package->do_freeze_btn(); } )->pack( -side=>'right', -padx=>3, -pady=>8 );

	$package->make_button(-widget=>$rowtwo_frame,
    			-help=>'Show Blocks By Server',
    			-text=>"Refresh",
    			-command=> sub {
    				my($old_isfrozen)=$is_frozen;
    				$is_frozen = 0;
    				$package->redraw();
				$is_frozen = $old_isfrozen;
    				#$package->browse( $entrypath, $query );
    		} )->pack( -side=>'right', -padx=>3, -pady=>8 );

	$rowtwo_frame->Checkbutton(-text=>"Show 'sa'", -variable=>\$show_system, -onvalue=>1, -offvalue=>0)->pack( -side=>'right', -padx=>3, -pady=>8 );

	#$current_main_frame->Entry( -bg=>'white', -relief=>'sunken', -font=>'code',
	#	-textvariable => \$entryText)->pack(-side => 'top', -fill => 'x', -padx=>2, -pady=>2, -ipadx=>1  );

	$textObject = $current_main_frame->DBITable(
		-sql		=> "",
		#-dbh   		=> $dbh,
		#-debug  	=> 1,
		-display_id	=> 1,
		#-srtColumnStyle => $SStyles{'sorted_column'},
		-maxchars	=> 35,
		-headerbackground => "white",
		-headerforeground => "white",
		-selectedcolor    	=> "blue",
		-selectedbackground     => "red",
		-rowcolorfunc		=> \&rowColorFunc,
		-filterfunc		=> \&filterRowFunc,
		-command => sub{  $package->onClick(@_); },
		-browsecmd => sub { $package->onClick(@_); },
		)->pack(	-side   => "top",
                              	-anchor => 'w',
                              	-expand => 1,
                               	-fill   => 'both');

        sub filterRowFunc {
        	my(%hash)=@_;
        	#foreach (keys %hash ) { print "filterRowFunc(key=$_)=$hash{$_}\n"; }
        	if( $show_system==0 ) {
        		if( exists $hash{login} ) {
        			if( ! defined $hash{login} or $hash{login}=~ /^\s*$/ or $hash{login}=~/^sa\s*$/ ) {
        				#print "DBG DBG: filter fails $hash{login}\n";
        				$package->debugmsg( "Row Filter Fails as login=$hash{login} \n" );
        				return 0;
        			}
        		} elsif( exists $hash{loginame} ) {
        			if( ! defined $hash{loginame} or $hash{loginame}=~ /^\s*$/ or $hash{loginame}=~/^sa\s*$/) {
        				#print "DBG DBG: filter fails $hash{login}\n";
        				$package->debugmsg( "Row Filter Fails as login=$hash{loginame} \n" );
        				return 0;
        			}
        		}
        	}
        	#print "DBG DBG: filter ok $hash{login}\n";
        	return 1;
        }

        sub rowColorFunc {
        	my(%hash)=@_;

        	$hash{"lock type"}=$hash{type} if defined $hash{type};
        	#foreach ( keys %hash) {	print "rowColorFunc : $_ => $hash{$_} \n"; }

        	return $SStyles{'error'} if defined $hash{bk} and int($hash{bk})>0;
        	if( defined $hash{cmd} ) {	# sp_who type output
        		my($style);
        		if( $hash{cmd} =~ /AWAITING COMMAND/i ) {
        			$style ='sleeping';
        		} elsif( $hash{cmd} =~ /SUSPEND/i ) {
        			$style ='error';
        		} elsif( $hash{status} =~ /lock sleep/i ) {
        			$style ='error';
        		} elsif( defined $hash{status} and  $hash{status} =~ /run/ ) {
        			$style ='running';
        		} elsif( defined $hash{status} and  $hash{status} eq "sleeping" ) {
        			$style ='sleeping';
        		} elsif( defined $hash{status} and  $hash{status} eq "background" ) {
        			$style ='sleeping';
        		} elsif( $hash{cmd} =~ /^HK/ ) {
        			$style ='admin';
        		} elsif( defined $hash{login} and  $hash{login} eq "" ) {
        			$style ='admin';
        		} elsif( defined $hash{"lock type"} ) {	# sp_who type output
        			$style ='error' if $hash{"lock type"} =~ /blk/;
        		} else {
        			$style ='sleeping';
        		}
        		#$package->debugmsg( "DBG DBG: ($style) cmd=$hash{cmd} status=$hash{status} login=$hash{login} \n" );
        		return $SStyles{$style};

        	}
        	return $SStyles{sleeping};
        }

        my %Styles = (
        	'admin'		=> [ 	-background => 'white',
        				-foreground=>'grey', 	 	],
        	'running'	=> [ 	-background => 'white',
        				-foreground=>'green', 	 	],
        	'error'	 	=> [ 	-background => 'red',
        				-foreground=>'white' ],
        	'sleeping'	=> [ 	-background => 'white',
        				-foreground=>'blue' 	],
        );
        foreach my $state (keys %Styles) {
        	$SStyles{$state} = $current_main_frame->ItemStyle('text', @{$Styles{$state}});
        }
        $package->debugmsg( "[$modulename] done init(om=$optionmenu_text cm=$currentMonitor) called\n" );
}

sub redraw {
	my( $package) = @_;
	main::busy();
	$package->debugmsg( "[$modulename] redraw(om=$optionmenu_text cm=$currentMonitor) called\n" );

	$afterid->cancel if defined $afterid;
	$afterid=undef;

	if( ${main::notebook_f}->raised() ne "Monitoring" ) {
		$package->statusmsg( "[$modulename] Monitoring Stopped: Raised Tab has changed to ",
						${main::notebook_f}->raised(),"\n" );
		main::unbusy();
		return;
	}
	if( ! defined $textObject ) {
		main::unbusy();
		return;
	}

	$package->statusmsg( "[$modulename] redraw(server=$currentServer, currentMonitor=$currentMonitor) \n" );
	$package->init_server();

	if( $currentServerType ne "sybase" and $currentServerType ne "sqlsvr" ) {
		$package->statusmsg( "[$modulename] Cant Paint - select server type is '".$currentServerType."'\n" );
		#$textObject->delete("1.0","end");
		#$entryText="Current server type $currentServerType - can only monitor Sybase and SQL Server.";
		$package->set_topLabel();
		main::unbusy();
		return;
	}

	if( ! defined $currentServer) {
		$package->statusmsg( "[$modulename] Cant Paint - no server selected\n" );
		#$textObject->delete("1.0","end");
		#$entryText="You Must Select a Server.";
		$package->set_topLabel();
		main::unbusy();
		return;
	}

	if( ! defined $currentMonitor ) {
		$package->statusmsg( "[$modulename] Cant Paint - no monitor selected\n" );
		#$textObject->delete("1.0","end");
		#$entryText="You Must Select A Monitor.";
		$package->set_topLabel();
		main::unbusy();
		return;
	}

	if( $is_frozen ) {
		main::unbusy();
		return;
	}

	$package->set_topLabel();

	# schedule it again
	if( $currentRefRate > 0 ) {
		$currentRefRate=30 if $currentRefRate =~ /\D/;
		$afterid=$current_main_frame->after( $currentRefRate*1000,
			sub {
				$package->redraw();	#($results_frame, $current_main_frame );
			}
		);
		#print "DBG: afterid is $afterid\n";
	}
	my($cmd_column,$db_column, $blocked_column);

	my($query);
	my($report_type)="Query";
	my(@data_cols, $key_cols);
	if( $currentMonitor eq "Blocks" ) {
		$query="sp__block \@dont_format='Y'";
	} elsif( $currentMonitor eq "Processes" ) {
		$query="sp__who \@dont_format='Y'";
	} elsif( $currentMonitor eq "Locks" ) {
		$query="sp__lock \@dont_format='Y'";
	} elsif( $currentMonitor eq "Activity" ) {
		$query="sp__whodo \@dont_format='Y'";
	} elsif( $currentMonitor eq "Cache" ) {
		$query="select CacheName, PhysicalReads, PhysicalWrites, LogicalReads
			from master..monDataCache";
		$report_type="Top";
		$key_cols=0;
		@data_cols=(1,2,3);
	} elsif( $currentMonitor eq "Pool" ) {
		$query="select Pool=CacheName+' ('+convert(varchar,IOBufferSize)+')', MB=AllocatedKB/1024,
			PhysReads=PhysicalReads, Stalls, PagesTouched, PagesRead, MRU=BuffersToMRU, LRU=BuffersToLRU
			from master..monCachePool";
		$report_type="Top";
		$key_cols=0;
		@data_cols=(2,3,5,6,7);
	} elsif( $currentMonitor eq "Network" ) {
		$report_type="Timestamp_Stack";
		@data_cols=(0,1,2,3);
		$query="select PacketsSent,PacketsReceived,BytesSent,BytesReceived from master..monNetworkIO";
	} elsif( $currentMonitor eq "Engine" ) {
		$query="select Engine=EngineNumber, Affinity=AffinitiedToCPU, Status, Connections, CPUTime,
			SystemCPUTime, UserCPUTime, IdleCPUTime from master..monEngine";
		$report_type="Top";
		$key_cols=0;
		@data_cols=(4,5,6,7);
	} elsif( $currentMonitor eq "Waits" ) {
		 $query="select distinct EvId=i.WaitEventID,Event=substring(i.Description,1,40),WaitTime,Waits
   			from master..monWaitEventInfo i, master..monSysWaits s
   			where s.WaitEventID= i.WaitEventID
   			order by i.Description";
   		$report_type="Top";
		$key_cols=0;
		@data_cols=(2,3);

   	} elsif( $currentMonitor eq "IO Queue"  ) {
   		$report_type="Top";
		$key_cols=0;
		@data_cols=(1,2);
	   	$query="select 'Device/Type'=LogicalName+' - '+IOType,IOs,IOTime
	   	from master..monIOQueue order by LogicalName,IOType ";
	} elsif( $currentMonitor eq "Procs") {
   		$report_type="Top";
		$key_cols=0;
		@data_cols=(2,3,4,5,6,7,8,9,10,11,12);
	   	$query="select a.SPID, Login, CPUTime, WaitTime,
	   		PReads=PhysicalReads, LReads=LogicalReads, PgReads=PagesRead,
	   		PWrites=PhysicalWrites, PgWrites=PagesWritten,MemKb=MemUsageKB, NumLocks=LocksHeld,
	   		Commits,Rollbacks from master..monProcessActivity a, master..monProcessLookup p
	   		where a.KPID=p.KPID";
	} elsif( $currentMonitor eq "IO"  ) {
		$report_type="Top";
		$key_cols=0;
		@data_cols=(1,2,3);
	   	$query="select LogicalName,Reads,Writes,IOTime
   			from   master..monDeviceIO
   			order by PhysicalName";
	} elsif( $currentMonitor eq "Objects"  ) {
		$report_type="Top";
		$key_cols=0;
		@data_cols=(5,6,7,8,9);
	   	$query="select ObjName=db_name(DBID)+' '+object_name(ObjectID,DBID),IndexID,RowsIns=RowsInserted,RowsDel=RowsDeleted,RowsUpd=RowsUpdated,
	PWrites=PhysicalWrites,LReads=LogicalReads,PReads=PhysicalReads,SelCnt=OptSelectCount,OpCnt=Operations 
from master..monOpenObjectActivity
where DBID>1
order by LastUsedDate desc";
   } else {
		die "ERROR ; INVALID MONITOR $currentMonitor";
	}

	# run the query
	my($connectionmanager)=$$package{-connectionmanager};

	$package->statusmsg(  "[$modulename] CONNECTING TO $currentServer TYPE as $currentServerType \n" );
	my($c,$errormsg)=$connectionmanager->connect(-name=>$currentServer, -type=>$currentServerType );
	if( ! $c ) {
		$package->statusmsg("[$modulename] unable to connect to $currentServerType Server $currentServer: $errormsg\n");
		main::unbusy();
		return;
	}
	$package->debugmsg(  "[$modulename] Correcting Tree Frame Color\n" );
	main::colorize_tree($currentServer);


	$package->statusmsg("[$modulename] running ($report_type) $query on $currentServer\n");
	my($q_starttime)=time;
	my(@rc) = $connectionmanager->query(
		-query=>$query,
		-connection=>$c,
		-db=>'master',
		-print_hdr=>1 );
	$package->statusmsg("[$modulename] query completed : ".($#rc+1)." rows returned\n");
	#print "DBG DBG\n", join("\n",dbi_reformat_results(@rc)),"\n";

	if( $report_type eq "Top" or $report_type eq "Timestamp_Stack" ) {
		#print "HEY - THIS IS A TOP REPORT!!!\n";
		my(@newrc);
		if( $currentMonitor ne $prior_monitor ) {
			#print "Restarting Counters $currentMonitor ne $prior_monitor\n";
			%save_data_array=();
		}
		push @newrc, shift @rc;
		foreach my $d (@rc) {
			my(@row_data)=dbi_decode_row($d);
			#print "Retrieved row: ",join(" ",@row_data),"\n";
			my($row_key);
			if( defined $key_cols ) {
				$row_key = $row_data[$key_cols];
			} else {
				$row_key="N.A.";
			}
			my($last_data_row_for_key)=$save_data_array{$row_key};
			$last_data_row_for_key = \@row_data if ! defined $last_data_row_for_key;
			$save_data_array{$row_key} = \@row_data;
			#print "Saved row: ",join(" ",@$last_data_row_for_key),"\n";
			my(@new_row_data);

			foreach( @row_data ) {	push @new_row_data,$_; }
			foreach( @data_cols) { $new_row_data[$_] -= $$last_data_row_for_key[$_]; }
			push @newrc, dbi_encode_row(@new_row_data);
			#print "Resetting To row: ",join(" ",@new_row_data),"\n";
		}
		@rc=();
		push @rc,@newrc;
		#@rc=dbi_reformat_results(@newrc);
	}else {
		#@rc=dbi_reformat_results(@rc);
	}

	#$entryText="Monitor Run At ".localtime(time).".";
	# OK BY REPORT TYPE
	if( $report_type eq "Query" or $report_type eq "Top" ) {

		#$textObject->delete("1.0","end");
		my($hdr)=shift @rc;
		if( $#$hdr==0 and $#rc<0 ) {
			$package->messageBox(-title=>'Warning', -message=>reword($$hdr[0],45,60), -type=>'OK' );
			main::unbusy();
			return;
		}
		#use Data::Dumper;
		#print Dumper "Hdr is ",Dumper $hdr,"\n";
		#print "=================================\n";
		#print Dumper "Data is ",Dumper \@rc,"\n";
		$textObject->setData($hdr,\@rc);
	} elsif( $report_type eq "Timestamp_Stack" ) {
		if(  $currentMonitor ne $prior_monitor ) {
			my($hdr)=shift @rc;
			$textObject->setData($hdr,\@rc);
		} else {
			$textObject->appendData($rc[$#rc]);
			$textObject->refresh();
		}
	}

	$prior_monitor=$currentMonitor;
	# doesnt work
	#print "DBG DBG Repacking\n";
	#$main::notebook_f->packForget;
	#$main::notebook_f->pack(-side=>'right', -padx=>15, -pady=>15, -expand=>1, -fill=>'both', -anchor=>'nw');
	#print "DBG DBG done Repacking\n";

	main::unbusy();
}

1;
