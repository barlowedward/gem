#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2006-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com

use strict;
use Getopt::Long;
use Sys::Hostname;
use DBIFunc;
use Repository;
use MlpAlarm;
use CommonFunc;
use File::Basename;
use CommonHeader;

use vars qw( $DOALL $BATCH_ID $ERRFILE $USER $SERVER $PRODUCTION $NOPRODUCTION $PASSWORD $NOSTDOUT $OUTFILE $DEBUG);
my($VERSION)=1;

my($PROGRAM_NAME)="CheckServerDaily";

############## LOCAL VARS ############
my(%SETUPVARS,%ERRFILE_DAT,%UPDATE_STATS_DAYS);
my($gem_root_dir);
my(@proposed_errfile_additions);
#########################################

sub usage
{
	print @_;
	print $PROGRAM_NAME.".pl  -BATCH_ID=id -USER=SA_USER -SERVER=SERVER --PRODUCTION|--NOPRODUCTION -PASSWORD=SA_PASS [-debug] -OUTFILE=Outfile --DOALL=sybase|sqlsvr \n\nDOALL=sybase|sqlsvr - run on all servers in the repository\n";
  return "\n";
}

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"USER=s"				=> \$USER ,
		"ERRFILE=s"			=> \$ERRFILE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"			=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die( usage("Must pass server\n" ))
	unless defined $SERVER or defined $DOALL;
$DOALL=lc($DOALL)
	if defined $DOALL and $DOALL ne "sqlsvr" and $DOALL ne "sybase";
die( usage("DOALL must be sqlsvr or sybase\n" ))
	if defined $DOALL and $DOALL ne "sqlsvr" and $DOALL ne "sybase";
die( usage("Must Pass --DOALL or --SERVER\n" ))
	if defined $SERVER and $SERVER =~ /^\s*$/;

if( defined $OUTFILE ) {
	unlink($OUTFILE);
	open(OUT,">> $OUTFILE") or die("Cant Write to $OUTFILE: $!");
}

consolemsg( $PROGRAM_NAME.".pl v1.0\n" );
reportmsg( $PROGRAM_NAME.".pl v1.0<br>\n" );
reportmsg( "Run at ".localtime(time)."<br>\n" );

init();

if( defined $DEBUG ) {
	debugmsg( "Called in Debug Mode\n");
	debugmsg( "DOALL		=$DOALL \n");
	debugmsg( "USER		=$USER \n");
	debugmsg( "SERVER		=$SERVER \n");
	debugmsg( "OUTFILE	=$OUTFILE \n");
}

my(@server_list);
my($subkey);
if( defined $DOALL ) {
  die( usage("DOALL must be sqlsvr or sybase\n" )) if $DOALL ne "sqlsvr" and $DOALL ne "sybase";
 	$subkey = $DOALL;
 	if( $PRODUCTION ) {
 		$subkey .= " (production)";
 		push @server_list, get_password(-type=>$DOALL,PRODUCTION=>1);
 	} elsif( $NOPRODUCTION ) {
 		$subkey .= " (noproduction)";
 		push @server_list, get_password(-type=>$DOALL,NOPRODUCTION=>1);
 	} else {
		push @server_list, get_password(-type=>$DOALL);
	}
} elsif( defined $SERVER ) {
	$subkey = $SERVER;
	push @server_list, split(/[,\|]/,$SERVER);
}
MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -SUBKEY=>$subkey )if $BATCH_ID;

my(%RUN_ON_SERVER_HASH);
foreach my $cursvr (@server_list ) {
	$RUN_ON_SERVER_HASH{$cursvr}=1;
	consolemsg("Running on $cursvr at ".localtime(time)."\n");

   debugmsg("Fetching Password Info","\n");
   if( defined $DOALL ) {
      ($USER,$PASSWORD)=get_password(-type=>$DOALL, -name=>$cursvr);
   } else {
      ($USER,$PASSWORD)=get_password(-name=>$cursvr, -type=>'sybase')
      	if ! defined $USER or ! defined $PASSWORD;
      ($USER,$PASSWORD)=get_password(-name=>$cursvr, -type=>'sqlsvr')
      	if ! defined $USER or ! defined $PASSWORD;
   }

   if( ! $USER and ! $PASSWORD ) {
   	warn "ERROR: Cant retrieve login/password for server $cursvr\n";
		MlpHeartbeat(
			-state=>"CRITICAL",
			-debug=>$DEBUG,
			-subsystem=>$PROGRAM_NAME,
			-system=>$cursvr,
			-monitor_program=>$BATCH_ID,
			-message_text=>"GEM_010: ERROR: Cant retrieve login/password for server $cursvr.  Check Config Files.")if $BATCH_ID;
		next;
   }

	#
	# CONNECT
	#
	my($rc)=dbi_connect(-srv=>$cursvr,-login=>$USER,-password=>$PASSWORD);
	if( ! $rc ) {
		consolemsg("Unable To Connect To Server... Trying Again in 20 Seconds\n");
		sleep(20);
		$rc=dbi_connect(-srv=>$cursvr,-login=>$USER,-password=>$PASSWORD);
		if( ! $rc ) {
			my($msg)="Cant connect to $cursvr as $USER from ".hostname()." ".dbi_has_login_failed()."\n";
			MlpHeartbeat(
				-state=>"CRITICAL",
				-debug=>$DEBUG,
				-subsystem=>$PROGRAM_NAME,
				-system=>$cursvr,
				-monitor_program=>$BATCH_ID,
				-message_text=>"GEM_010: ".$msg)if $BATCH_ID;

			consolemsg($msg);
			next;
		}
	}
	consolemsg("\tConnected To $cursvr\n");
	dbi_set_mode("INLINE");
	server_command($cursvr);
	dbi_disconnect();
}

if( $ERRFILE ) {
		my(@old_rows);
		if( -r $ERRFILE ) {
			open( ERRFILE_IN, $ERRFILE ) or die("Cant Read $ERRFILE: $!");
			while ( <ERRFILE_IN> ) {
				my(@x)=split(/,/);
				next if $RUN_ON_SERVER_HASH{$x[0]};
				push @old_rows,$_;
			}
		}
		close( ERRFILE_IN );
		open( ERRFILE, "> $ERRFILE" ) or die("Cant Write $ERRFILE: $!");
		print ERRFILE @old_rows;
		print ERRFILE @proposed_errfile_additions;
		close(ERRFILE);
}

MlpBatchJobEnd();
consolemsg( "Program completed successfully.\n" );
dbi_disconnect();
exit(0);

sub set_attribute {
}

sub init {
	my($vl,$required_proclib)=get_version(get_gem_root_dir()."/ADMIN_SCRIPTS/procs/VERSION" );
	debugmsg( "Procedure Library Version Should Be ",$required_proclib,"<br>\n" );
	$SETUPVARS{required_proclib}=$required_proclib;
	consolemsg( "Reading Configfile $PROGRAM_NAME.dat\n");
	foreach my $row ( get_conf_file_dat($PROGRAM_NAME) ) {
		chomp;
		if( $row=~/UPDSTATSDAYS/ ) {
			my(@x)=split(/,/,$row);
			die "ERROR LINE $row NOT IN 3 PARTS" unless $#x==2;
			$UPDATE_STATS_DAYS{$x[0]}=$x[2];
		} else {
			$ERRFILE_DAT{$row}=1;
		}
	}

	#my($h)=hostname();
	#sub _statusmsg {    print "[statusmsg] ",join("",@_); }
	#sub _debugmsg {     print "[debugmsg]  ",join("",@_); }
	#$gd = new GemData( -printfunc=>\&_statusmsg, -debugfunc=>\&_debugmsg, -hostname=>$h );
	$gem_root_dir=get_gem_root_dir();
}

sub showitem {
	my($key,$value,$color)=@_;
	reportmsg( "<TR><TD>$key</TD><TD><FONT COLOR=$color>$value</FONT></TD></TR>\n" );
}

sub get_updated_values {
	my($key)=@_;
	my($q)="exec sp_configure '".$key."'";
	debugmsg("\t=> $q\n");
	my(@rc)=dbi_query(-db=>"master",-query=>$q);
	foreach(@rc) {
		my($option,$dflt,$memusd,$cfgval,$runval,$unit,$type)=dbi_decode_row($_);
		$cfgval=~s/\s//g;
		$runval=~s/\s//g;
		return ($cfgval,$runval) if $cfgval=~/^\d+$/ and $runval=~/^\d+$/;
	}
	return (undef,undef);
}

sub server_command {
	my($cursvr)=@_;
	my(%passfile_args);
	my($cursvr_type);

	my($DBTYPE,$DBVERSION)=dbi_db_version();
	consolemsg("\tDBTYPE=$DBTYPE DBVERSION=$DBVERSION\n");

	if( $DBTYPE eq "SYBASE" ) {
		%passfile_args =	get_password_info(-type=>'sybase',-name=>$cursvr);
		$cursvr_type="sybase";
	} elsif( $DBTYPE eq "SQL SERVER" ) {
		%passfile_args	=	get_password_info(-type=>'sqlsvr',-name=>$cursvr);
		$cursvr_type="sqlsvr";
	} else {
		die "UNKNOWN SERVER TYPE $DBTYPE";
	}

	my($sp__id_server_name, $sp__id_db, $sp__id_login, $sp__id_suid, $sp__id_user_name);		# output of sp__id
	my($sql)="sp__id";
  	my(@rc)=dbi_query(-db=>"master",-query=>$sql);
  	foreach( @rc ) {
  		($sp__id_server_name, $sp__id_db, $sp__id_login, $sp__id_suid, $sp__id_user_name)=dbi_decode_row($_);
  	}
  	$sp__id_server_name =~ s/\s+$//;
  	set_attribute($cursvr,"SERVERNAME",$sp__id_server_name);

  	reportmsg( "<h2>SERVER=$cursvr</h2>\n","SERVER TYPE=$DBTYPE     VERSION=$DBVERSION","<br>\n" );
  	reportmsg( "<TABLE>\n" );

	if( $sp__id_server_name eq $cursvr ) {
  		showitem( "Server Name",$sp__id_server_name,"BLACK" );
  		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"OK",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"InternalName",
				-message_text=>"GEM_013: Sybase Internal Name Set Correctly") if $BATCH_ID;
  	} elsif( $sp__id_server_name =~ /^\s*$/ ) {
  		showitem( "Server Name","(ERROR) INTERNAL NAME NOT SET","RED" );
  		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"ERROR",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"InternalName",
				-message_text=>"GEM_013: Sybase Internal Name Not Set") if $BATCH_ID;
  	} else {
  		showitem( "Server Name",$sp__id_server_name . " (does not match $cursvr)","BLUE" );
  		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"WARNING",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"InternalName",
				-message_text=>"GEM_013: Sybase Internal Name $sp__id_server_name does not match GEM name $cursvr") if $BATCH_ID;
  	}

 	foreach ( keys %passfile_args ) {
			showitem( $_, $passfile_args{$_}, "BLACK" ) unless $_ eq "SERVER_TYPE";
	}
	if( ! $passfile_args{SERVER_TYPE} ) {
		showitem( 'SERVER_TYPE', "(ERROR) SERVER_TYPE not set in password file", "RED" );
		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"ERROR",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"SERVR_TYPE",
				-message_text=>"GEM_014: SERVER_TYPE not set in GEM Password File") if $BATCH_ID;
	} elsif( $passfile_args{SERVER_TYPE} ne "PRODUCTION"
	and $passfile_args{SERVER_TYPE} ne "DEVELOPMENT"
	and $passfile_args{SERVER_TYPE} ne "DR"
	and $passfile_args{SERVER_TYPE} ne "QA" ) {
		showitem( 'SERVER_TYPE', "(ERROR) SERVER_TYPE set to $passfile_args{SERVER_TYPE}", "RED" );
		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"ERROR",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"SERVR_TYPE",
				-message_text=>"GEM_014: SERVER_TYPE ($passfile_args{SERVER_TYPE}) not PRODUCTION/DEVELOPMENT/DR/QA in GEM Password File") if $BATCH_ID;
	} else {
		showitem( 'SERVER_TYPE', $passfile_args{SERVER_TYPE}, "BLACK" );
		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"OK",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"SERVR_TYPE",
				-message_text=>"GEM_014: SERVER_TYPE set appropriately in GEM Password File") if $BATCH_ID;
	}

   #
   # STORED PROCEDURE LIBRARY CHECK
   #
   my($stored_proc_is_installed, $stored_proc_version);
	consolemsg( "\tTesting Extended Stored Procedure Library\n" );
   if( $DBTYPE eq "SYBASE" ) {
		set_attribute($cursvr,"SERVERNAME",$sp__id_server_name);
  		my(@rc)=dbi_query(-db=>"sybsystemprocs",-query=>"select name from sysobjects where name='sp__proclib_version'");
		foreach(@rc) { my(@row)=dbi_decode_row($_); $stored_proc_is_installed=1 if $row[0] eq "sp__proclib_version"; }
	} else {
		my(@rc)=dbi_query(-db=>"master",-query=>"select name from sysobjects where name='sp__proclib_version'");
		foreach(@rc) { my(@row)=dbi_decode_row($_); $stored_proc_is_installed=1 if $row[0] eq "sp__proclib_version"; }
	}
	if( $stored_proc_is_installed ) {
		my(@rc)=dbi_query(-db=>"master",-query=>"exec sp__proclib_version");
		my(@version);
		foreach(@rc) { @version=dbi_decode_row($_); }
		$stored_proc_version=$version[0];

		if( $stored_proc_version eq $SETUPVARS{required_proclib} ) {
			showitem( "Stored Procedure Lib","LIBRARY OK","BLACK" );
			MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"OK",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"ProcLib",
				-message_text=>"GEM_010: Stored Procedure Library Ok") if $BATCH_ID;
		} else {
			showitem( "Stored Procedure Lib","(ERROR) LIBRARY OUTDATED (ver=$stored_proc_version should be $SETUPVARS{required_proclib})","RED" );
			MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"ERROR",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"ProcLib",
				-message_text=>"GEM_011: Library Version=".$stored_proc_version." Should Be=".$SETUPVARS{required_proclib}) if $BATCH_ID;
		}
	} else {
		showitem( "Stored Procedure Lib","(ERROR) LIBRARY NOT INSTALLED","RED" );
		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"ERROR",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"ProcLib",
				-message_text=>"GEM_011: Stored Procedure Library Is Not Installed") if $BATCH_ID;
	}

		# MONITOR CONFIG ALL CHECK
	if( $DBTYPE eq "SYBASE" ) {
		my($monitorconfig_errors)="";
		consolemsg( "\tTesting: exec sp_monitorconfig 'all'\n" );
   	my(@rc)=dbi_query(-db=>"master",-query=>"exec sp_monitorconfig 'all'",-debug=>$DEBUG);
		foreach(@rc) {
			my($option,$num_free,$num_active,$pct_active,$max_usd,$reused)=dbi_decode_row($_);

			debugmsg("Fetched Monitorconfig Option $option\n");
			if( $option=~/^\s*$/ or $option =~ /^----/ or $option =~ /^Name/ or $option =~ /^Usage infor/ ) {
				debugmsg( "\t... Skipped Header Line\n" );
				next;
			};
			if( ! defined $num_free and ! defined $num_active) {
				if( ! defined $pct_active and ! defined $max_usd and ! defined $reused ) {
					debugmsg("SOMETHING TERRIBLY WRONG: $option\n");
					next;
				}
				debugmsg("\tolder monitor config format found\n");
				debugmsg("\tLINE=$option\n");
				my(@d)=split(/\s+/,$option);
				$reused=pop(@d);
				$reused=~s/\s//g;
				$max_usd=pop(@d);
				$max_usd=~s/\s//g;
				$pct_active=pop(@d);
				$pct_active=~s/\s//g;
				$num_active=pop(@d);
				$num_active=~s/\s//g;
				$num_free=pop(@d);
				$num_free=~s/\s//g;
				$option=join(" ",@d);
			}
			debugmsg("\tactive=$num_active num_free=$num_free reused=$reused pct_active=$pct_active\n");
			if( ! $max_usd ) {
				debugmsg( "\t... Skipped max_used=$max_usd\n" );
				next;
			};
			if( $option =~ /^----/ or $option =~ /^Name/ or ! $max_usd or $option =~ /^Usage infor/ ) {
				debugmsg( "\t... Skipped option=$option\n" );
				next;
			}
			if( $option eq "max memory"
				or $option eq "max online engines"
				or $option eq "permission cache entries"
				or $option eq "additional network memory"
				or $option eq "disk i/o structures"
				or $option eq "procedure cache size"
				or $option eq "number of mailboxes" ) {
					debugmsg("\tIgnorable Option $option\n");
					next;
			}

			#printf "MONITORCONFIG_A %30s pct=%5s reuse=%8s fr=%8s act=%8s max=%8s\n",$option, $pct_active,$reused,$num_free,$num_active,$max_usd;

			my($reuse_error)= int($reused) || 0;
			#$reuse_error=int($reused);
			$reuse_error=1 if lc($reused) eq "yes";

			#my($option,$num_free,$num_active,$pct_active,$max_usd,$reused)=dbi_decode_row($_);

			if( $option =~ /number of locks/ and $max_usd > .8 * ($num_free+$num_active)) {
				debugmsg("\tERROR: LOCKS max=$max_usd avail= $num_free $num_active\n");
			} elsif( ($num_free+$num_active==$max_usd) or	$pct_active >= 90 or	$reuse_error ) {
				debugmsg("\tERROR: Reuse=$reuse_error\n") if $reuse_error;
				debugmsg("\tERROR: Pct Active Currently=$pct_active\n") if $pct_active>=90;
				debugmsg("\tERROR: Free=$num_free Active=$num_active Max=$max_usd => ALL WERE USED\n");
			} else {
				debugmsg("\tSkipping - All Is Well\n");
				next;
			}

			my($cfgval,$runval)=get_updated_values($option);
			if( $cfgval != $runval ) { # something changed - its probably ok
				print "\tOPTION=$option CFGVAL=$cfgval RUNVAL=$runval\n";
				print "\tPresuming someone ran sp_configure to update the value\n";
				next;
			}

			debugmsg("\t(from sp_configure) OPTION=$option CFGVAL=$cfgval RUNVAL=$runval\n");

#			if( $num_free + $num_active == $runval ) {
#				print "DBG DBG: all is well\n";
#			} else {
#				print "DBG DBG: hmmm $num_free + $num_active != $runval\n";
#			}

			# added != runval as max_usd seems wrong - it exceeds runval sometimes
			#if($num_free+$num_active<$max_usd and $num_free+$num_active!=$runval and $option !~ /number of sort buffers/) {
			if( $runval != $num_free + $num_active and $option != /additional network memory/) {
				consolemsg("*****************************************************\n");
				consolemsg("***> Option $option sp_configure=>$runval but monitorconfig=>",($num_free+$num_active),"\n");
				consolemsg("*****************************************************\n");
				$monitorconfig_errors.="Option $option sp_configure=>$runval monitorconfig=>".($num_free+$num_active)."<br>\n";
				next;
			} elsif($num_free+$num_active==$max_usd and $option !~ /number of sort buffers/) {
				consolemsg("*****************************************************\n");
				consolemsg("***> Option $option Max Config Setting Was Used\n");
				consolemsg("*****************************************************\n");
				$monitorconfig_errors.="Option $option Max Config Setting Was Used"."<br>\n";
			} elsif(	$pct_active >= 90 ){
				consolemsg("*****************************************************\n");
				consolemsg("***> Option $option PctActive=$pct_active\n");
				consolemsg("*****************************************************\n");
				$monitorconfig_errors.="Option $option PctActive=$pct_active"."<br>\n";
			} elsif(	$reuse_error ) {
				consolemsg("*****************************************************\n");
				consolemsg("***> Option $option Reuse=$reused\n");
				consolemsg("*****************************************************\n");
				$monitorconfig_errors.="Option $option Reuse=$reused"."<br>\n";
			} else {
				next;
			}
		}

		if( $monitorconfig_errors ne "" ) {
			consolemsg( "\tMONITORCONFIG ERROR $monitorconfig_errors\n" );
			MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"ERROR",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"MonitorConfig",
				-message_text=>"GEM_015: $monitorconfig_errors") if $BATCH_ID;
		} else {
			MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"OK",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"MonitorConfig",
				-message_text=>"GEM_015: sp_monitorconfig 'all' ok") if $BATCH_ID;
		}
	}
	#print "MONITORCONFIG NEXT\n";
#	next;


#	if( $DOSURVEY ) {
#		consolemsg( "\tRunning Survey\n" );
#		my(@rc)=$gd->survey(
#					-type			=>	$cursvr_type,
#					-name			=>	$cursvr,
#					#-login		=>	$l,
#					-connection => dbi_getdbh(),
#					#-password	=>	$p,
#					-debug		=>	$DEBUG,
#					-local_directory	=>	$gem_root_dir.'/data/system_information_data');
#		foreach (@rc) { print $_; }
#	}

#	print "LINE ",__LINE__,"\n";
#	my($results) = $gd->get_set_data( -class=>$cursvr_type,-name=>$cursvr,-debug=>1 );
#	use Data::Dumper;
#	print "DBG DBG",Dumper $results;
#
#	# ok so now we go through the survey data and check for errors
#
#	# Test Last Survey Time
#	my $last_survey = $results->{Last_Survey_Time};
#	if( (time  - $last_survey) > 3*24*60*60 ) {
#		# die "Time=",time," LAST=$last_survey DIFF=",time-$last_survey,"\n";
#		showitem( "Survey","(ERROR) LAST SURVEY AT ".localtime($last_survey)." t=".time." l=".$last_survey,"RED" );
#		MlpHeartbeat(
#				-debug=>$DEBUG,
#				-state=>"ERROR",
#			 	-monitor_program=>$BATCH_ID,
#				-system=>$cursvr,
#				-subsystem=>"LastSurvey",
#				-message_text=>"GEM_010: LAST SURVEY AT ".localtime($last_survey)." please run gem_MssqlSurvey" ) if $BATCH_ID;
#
#				#-message_text=>"GEM_010: LAST SURVEY AT ".localtime($last_survey)." DBGDBG: S=$last_survey T=".time." DIFF=".(time  - $last_survey)) if $BATCH_ID;
#
#	} else {
#		showitem( "Survey","(OK) LAST SURVEY AT ".localtime($last_survey),"BLACK" );
#		MlpHeartbeat(
#				-debug=>$DEBUG,
#				-state=>"OK",
#			 	-monitor_program=>$BATCH_ID,
#				-system=>$cursvr,
#				-heartbeat=>(60*24*3),
#				-subsystem=>"LastSurvey",
#				-message_text=>"GEM_010: THE LAST OK SURVEY WAS AT ".localtime($last_survey)) if $BATCH_ID;
#	}

	consolemsg( "\tAuditing $cursvr using sp__auditsecurity \n" );
	my(@audit_res)=dbi_query(-db=>"master",-query=>"exec sp__auditsecurity 1,\"$cursvr\",\"hostname\",\"Y\"");

##	my(%db_log_data_same_device);
##	if( $DBTYPE ne "SQL SERVER" ) {
#		consolemsg( "\tTesting databases with data & log on same device\n" );
#		foreach( dbi_query(-db=>"master",-query=>"select distinct db_name(dbid) from master..sysusages where segmap&7=7")) {
#			my(@d)=dbi_decode_row($_);
#			$db_log_data_same_device{$d[0]} = 1;
#		}
#	}

	reportmsg("</TABLE>");

	my($dbtype,@dblist) = dbi_parse_opt_D('%',1);

	my(%status1val,%status2val);
	my($qry)="select name,status,status2 from master..sysdatabases";
	$qry="select name,status1,status2 from master..sysdatabases" if $DBTYPE eq "SYBASE";

	foreach ( dbi_query(-db=>"master",-query=>$qry) ) {
		my @dat=dbi_decode_row($_);
		$status1val{$dat[0]} = $dat[1];
		$status2val{$dat[0]} = $dat[2];
	}

#	my $Database_Info = $results->{Database_Info};
#	foreach my $db (keys %$Database_Info ) {
	foreach my $db (@dblist) {
		consolemsg("\t-- Checking Database $db\n");

		#if( $passfile_args{SERVER_TYPE} eq "PRODUCTION" and $DBTYPE eq "SYBASE" ) {
			# CHECK FOR DATA & LOG ON SAME DEVICE
	#	}

		if( $db ne "tempdb" and $db ne "model" ) {
#			if( $$Database_Info{$db}->{HasDbAccess} eq "Yes" ) {
				my($query)="exec sp__auditdb '".$cursvr."','hostname','Y',";

				if( $UPDATE_STATS_DAYS{$cursvr} ) {
					$query.= $UPDATE_STATS_DAYS{$cursvr};
				} elsif( $passfile_args{SERVER_TYPE} eq "PRODUCTION" ) {
					$query.="3"
				} else {
					$query.="14"
				}
				debugmsg( "\t\t$query\n" );

				push @audit_res,dbi_query(-db=>$db,-query=>$query);

# use Data::Dumper; print "DBGDBG",Dumper \@audit_res; print "\n";

				#foreach( dbi_query(-db=>$db,-query=>$query)) {
				#	push @audit_res,$_;
				#}
#			}
		}

		# Check Tempdb Size
		# Check Procedure Cache
		# Check Memory

		my($status1)=$status1val{$db};	#=$$Database_Info{$db}->{status1};
		my($status2)=$status2val{$db};	#=$$Database_Info{$db}->{status2};
		debugmsg("\t\tDATABASE STATUS BITS ARE $status1/$status2 \n");

		if( $DBTYPE eq "SYBASE" ) {
			# DETAILS FOR SYBASE
			# STATUS1		4 SI	8 TRUNC	16 NOCHKPT	32 FOR LOAD	256 SUSPECT	1024 READ ONLY 2048 DBO 4096 SU
			# STATUS2		16 OFFLINE 32 OFFLINE4RECOVERY 64 INTERNAL RECOVERY 128 SUSPECTPAGES -32768 MIXEDLOG

			push @audit_res,dbi_encode_row("hostname","servername","10001","$db","options","date","Database $db No Chkpt On Recovery")
				if ($status1&16) == 16;
			#push @audit_res,dbi_encode_row("hostname","servername","10002","$db","options","date","Database $db Created For Load")
			#	if ($status1&32) == 32;
			push @audit_res,dbi_encode_row("hostname","servername","10003","$db","options","date","Database $db Suspect")
				if ($status1&256) == 256;
			push @audit_res,dbi_encode_row("hostname","servername","10004","$db","options","date","Database $db Dbo Use Only")
				if ($status1&2048) == 2048;
			push @audit_res,dbi_encode_row("hostname","servername","10005","$db","options","date","Database $db Single User")
				if ($status1&4096) == 4096;

			push @audit_res,dbi_encode_row("hostname","servername","10006","$db","options","date","Database $db Offline"  .$status2." ".($status2&16))
				if ($status2&16) == 16;
			push @audit_res,dbi_encode_row("hostname","servername","10007","$db","options","date","Database $db Offline During Recovery"  .$status2." ".($status2&16))
				if ($status2&32) == 32;
			push @audit_res,dbi_encode_row("hostname","servername","10008","$db","options","date","Database $db is being recovered"  .$status2." ".($status2&16))
				if ($status2&64) == 64;
			push @audit_res,dbi_encode_row("hostname","servername","10008","$db","options","date","Database $db has suspect pages"  .$status2." ".($status2&16))
				if ($status2&128) == 128;

			if(	 $db ne 'model'
			and $db ne 'sybsecurity'
			and $db ne 'sybsyntax'
			and $db ne 'sybsystemprocs'
			and $db ne 'master'
			and $db ne 'sybsystemdb'
			and $db ne 'tempdb' ) {
				# IF DATA&LOG Same device then
				# IF tl!=si then warn
				# if mixed data & log then must have si and tl else si=tl
				# if ( defined $db_log_data_same_device{$db} ) {
				if ( defined $status2<0 ) {
					push @audit_res,dbi_encode_row("hostname","servername","10011","$db","options","date","Data&Log On Same Device and trunc log off")  unless ($status1 & 8) == 8;
				} else {
					if ( ($status1&12) == 4 )  {
						push @audit_res,dbi_encode_row("hostname","servername","10013","$db","options","date","Select Into != Trunc Log On Checkpoint");
					}
				}
			}
		} else {
			# DETAILS FOR SQLSVR
			#Status bits, some of which can be set by using ALTER DATABASE as noted:
			#4 = select into/bulkcopy (ALTER DATABASE using SET RECOVERY)
			#8 = trunc. log on chkpt (ALTER DATABASE using SET RECOVERY)
			#16 = torn page detection (ALTER DATABASE)
			#32 = loading
			#64 = pre recovery
			#128 = recovering
			#256 = not recovered
			#512 = offline (ALTER DATABASE)
			#1024 = read only (ALTER DATABASE)
			#2048 = dbo use only (ALTER DATABASE using SET RESTRICTED_USER)
			#4096 = single user (ALTER DATABASE)
			#32768 = emergency mode
			#4194304 = autoshrink (ALTER DATABASE)
			#1073741824 = cleanly shutdown
			#
			#STATUS2
			#16384 = ANSI null default (ALTER DATABASE)
			#65536 = concat null yields null (ALTER DATABASE)
			#131072 = recursive triggers (ALTER DATABASE)
			#1048576 = default to local cursor (ALTER DATABASE)
			#8388608 = quoted identifier (ALTER DATABASE)
			#33554432 = cursor close on commit (ALTER DATABASE)
			#67108864 = ANSI nulls (ALTER DATABASE)
			#268435456 = ANSI warnings (ALTER DATABASE)
			#536870912 = full text enabled (set by using sp_fulltext_database

			push @audit_res,dbi_encode_row("hostname","servername","10002","$db","options","date","Database $db Created For Load")
				if ($status1&32) == 32;
			push @audit_res,dbi_encode_row("hostname","servername","10009","$db","options","date","Database $db Pre-Recovery")
				if ($status1&64) == 64;
			push @audit_res,dbi_encode_row("hostname","servername","10010","$db","options","date","Database $db recovering")
				if ($status1&128) == 128;

			push @audit_res,dbi_encode_row("hostname","servername","10003","$db","options","date","Database $db Not Recoverd")
				if ($status1&256) == 256;
			push @audit_res,dbi_encode_row("hostname","servername","10003","$db","options","date","Database $db Offline")
				if ($status1&512) == 512;
			push @audit_res,dbi_encode_row("hostname","servername","10004","$db","options","date","Database $db Dbo Use Only")
				if ($status1&2048) == 2048;
			push @audit_res,dbi_encode_row("hostname","servername","10005","$db","options","date","Database $db Single User")
				if ($status1&4096) == 4096;
		}
	}

	consolemsg("Processing Audit Results\n");
	my($hdr)=0;
	my($AlarmableMsg)="";
	my($q)="DELETE INV_system_audit_result
		where system_name = '".$cursvr."' and system_type = '".$cursvr_type."' and monitor_program='".$BATCH_ID."'";
	print $q,"\n" if $DEBUG;
	_querynoresults($q,1);

# use Data::Dumper; print "DBGDBG",Dumper \@audit_res; print "\n";
	foreach (@audit_res) {
		my @dat=dbi_decode_row($_);
		print "Processing : ",join('//',@dat),"\n" if $DEBUG;
		if( $#dat<=1 and ! $hdr ) {
			reportmsg("ERROR:".join("",@dat)."\n");
			next;
		} elsif( $#dat<=1 and $hdr ) {
			reportmsg("<TR><TD>ERROR</TD><TD></TD><TD>".join("",@dat)."</TD></TR>\n");
			next;
		}

		$dat[6] =~ s/\s+$//;		# remove spaces from end
		$dat[4]="" if $dat[4] eq "a";
		$dat[6]=~ s/\s\s+/ /g;
		$dat[2]=~ s/\s/ /g;
		shift @dat;
		shift @dat;

		# SAVE THIS ITEM INTO THE DATABASE
		my($q)="INSERT INV_system_audit_result
( system_name, system_type, monitor_program, database_name, error_level, message_id, message_text, mod_date, mod_by )
values
( '".$cursvr."', '".
$cursvr_type."', '".
$BATCH_ID."', ".
dbi_quote(-text=>$dat[1]).", '', ".
$dat[0].", ".
dbi_quote(-text=>$dat[4]).", getdate(), 'CheckServerDaily.pl' ) ";

		print $q,"\n" if $DEBUG;
		_querynoresults($q,1);

		# 32105	user is member of public
		# 32106	public execute access
		# 32104	no groups in db
		# 31004	master database as default
		next if $dat[0] == 32105 or $dat[0] == 32106 or $dat[0] == 32104 or $dat[0] == 31004;
		next if $dat[0]==31101 # user owns objects
			or $dat[0]==32104 # *
			or $dat[0]==32105 # *
			or $dat[0]==31004 # master db as default
			or $dat[0]==32104 # no groups exist in db

			or $dat[0]==31010 # allow updates set
			or $dat[0]==32005 # allow updates
			or $dat[0]==32108 # allow updates
			or $dat[0]==31010 # allow updates set
			or $dat[0]==31013 # db created for laod
			or $dat[0]==32106 # Group Public Access To Object
			#or $dat[0]==10002
			#or $dat[0]==10001
			or $dat[0]==31026 # Configuration Value Has Been Reset = mssql
			or $dat[0]==31025 # Configuration Value Has Been Reset = sybase
			or $dat[0]==10002 # Db created for load
			or $dat[0]==32140 # running as LocalSystem - thats GOOD!

	  		or $dat[0]==31004 # *
	  		or $dat[0]==31012 # sa user trusted
	  		or $dat[0]==31009 # bad password
	  		or $dat[0]==31001;# locked login

		if( $hdr==0 ) {
			reportmsg( "<TABLE BORDER=1>\n" );
			reportmsg( "<TR><TH>Message</TH><TH>Database</TD><TH>Violation</TH></TR>\n");
			$hdr++;
		}

		if( $dat[3]=~/^\s*$/ and $dat[4]=~/^\s*$/ and $dat[6]=~/^\s*$/) {
			reportmsg("<TR><TD>ERROR</TD><TD></TD><TD>".join("",@dat)."</TD></TR>\n");
			next;
		}
		reportmsg("<TR><TD>$dat[0]</TD><TD>$dat[1]</TD><TD>$dat[4]</TD></TR>\n");
		#print join("|",@dat),"|\n";

		next unless $passfile_args{SERVER_TYPE} eq "PRODUCTION";
		next if $dat[0] == 31008		# allow updates
			or	$dat[0] == 32119		# procs in master
			or $dat[0] == 32013
			or $dat[0] == 32101
			or $dat[0] == 31025;

		my($fullerrstr)="$cursvr,$dat[0],$dat[1],$dat[4]";
		next if $ERRFILE_DAT{$fullerrstr} or $ERRFILE_DAT{"$cursvr,$dat[0],$dat[1],*"};
		push @proposed_errfile_additions,"$fullerrstr\n" if $ERRFILE;
		if( $dat[1] eq "master" ) {
			$AlarmableMsg .= $dat[4]."<br>\n";
		} else {
			$AlarmableMsg .= $dat[1].":".$dat[4]."<br>\n";
		}
	}

	reportmsg("</TABLE>\n");
	if( $AlarmableMsg ne "" ) {
		consolemsg("THE FOLLOWING ERRORS WERE FOUND\n$AlarmableMsg");
		reportmsg("THE FOLLOWING ERRORS WERE FOUND\n$AlarmableMsg<br><hr><p>\n");
		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"ERROR",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"MiscErrors",
				-message_text=>"GEM_010: ".$AlarmableMsg ) if $BATCH_ID;
	} else {
		consolemsg("THIS SERVER PASSES ALL TESTS\n");
		reportmsg("<p><b>THIS SERVER PASSES ALL TESTS</b><hr>\n");
		MlpHeartbeat(
				-debug=>$DEBUG,
				-state=>"OK",
			 	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-subsystem=>"MiscErrors",
				-message_text=>"GEM_010: No Errors Found") if $BATCH_ID;
	};
}

sub debugmsg { 	print @_ if defined $DEBUG;	}

sub reportmsg {
	if( $OUTFILE ) {
		print OUT @_;
	} elsif( $NOSTDOUT) {
		print @_;
	} else {
		print "***",@_;
	}
}

sub consolemsg {	print @_ unless $NOSTDOUT; }

__END__

=head1 NAME

CheckServerDaily.pl - daily check servers

=head2 USAGE

CheckServerDaily.pl -BATCH_ID=id
		[-debug]
		-OUTFILE=Outfile
		-ERRFILE=Outputs error messages appropriate for the configfile

Server Selection via:
	-USER=SA_USER -SERVER=SERVER -PASSWORD=SA_PASS
	--PRODUCTION|--NOPRODUCTION
	--DOALL=sybase|sqlsvr

=head2 CONFIGFILE

Reads the config file CheckServerDaily.dat.  This file takes excludes in the format generated by --ERRFILE
which happens to be:

SERVER,ERRORNO,DATABASE,MESSAGE

=head2 DESCRIPTION

Checks everything i could think of to check on a daily basis.

	* Checks stored procedure library
	* run sp__auditsecurity
	* check wierd select_into/trunc option definition on databases
	* foreach database
	->  space used (data&log) from sp__qspace
	->  sp__auditdb
	->  database options

=cu
