use strict;

# HEADER - TO MODIFY

use lib qw( \\adsrv141\CopyDb\perl\lib );

my($sourcedir)= "//adsrv141/dumps";
my($codedir)  = "//adsrv141/CopyDb";
my($searchstring)="fiaprod_backup";
my($targetserver)="ADSRV258";

# DO NOT MODIFY BELOW THIS LINE
opendir(dd,$codedir)	|| die("Can't open(directory) $codedir for reading\n");
my(@ddirlist) = grep(!/^\./,readdir(dd));
closedir(dd);
my(@load_db_list);
foreach( @ddirlist ) {
	next unless /^load_flag/;
	s/^load_flag_//;
	die "WOAH - flag file contains a period???" if /\./;
	push @load_db_list,$_;
}

if( -r "$codedir/load_flag" ) {
	print "Not Copying File As Stop Flag Exists\n";
	exit();
}

opendir(DIR,$sourcedir)	|| die("Can't open(directory) $sourcedir for reading\n");
my(@dirlist) = grep(!/^\./,readdir(DIR));
closedir(DIR);

my($latest_file);
my($latest_time) = 100000;
foreach( @dirlist ) {
	next unless /$searchstring/;
	if( $latest_time > -M "$sourcedir/$_" ) {
		$latest_time = -M "$sourcedir/$_";
		$latest_file = "$sourcedir/$_";
	}
}
if( $#load_db_list < 0 ) {
	print "Program Completed And Finished Executing - no flag files found\n";
	exit();
}

$latest_file =~ s.\/.\\.g;
print "Latest Backup File is $latest_file\n";
print "The script will run the following commands\n";
my($tstmp)=get_tstamp();
open(WW,"> $codedir/script_$tstmp.sql") or die "Cant write $codedir/script_$tstmp.sql $!\n";
foreach ( @load_db_list ) {
	print "CMD> restore database $_ from disk=\'".$latest_file."\' with replace,stats=1\nCMD> go\n";
	print WW "restore database $_ from disk=\'".$latest_file."\' with replace,stats=1\ngo\n";
}
close(WW);

print "Output in $codedir/script_$tstmp.sql\n";
print "The command run is: osql -E -S$targetserver -i$codedir/script_$tstmp.sql -o$codedir/script_$tstmp.out\n";
system("osql -E -S$targetserver -i$codedir/script_$tstmp.sql -o$codedir/script_$tstmp.out");

foreach (@load_db_list) {
	print 'unlinking load_flag_'.$_."\n";
	unlink "load_flag_".$_;
}

sub get_tstamp
{
	my($now)=time;

	my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime($now);
	$mon++;
	$year+=1900;
	substr($mon,0,0)='0' 	if length($mon)==1;
	substr($mday,0,0)='0' 	if length($mday)==1;
	substr($hour,0,0)='0' 	if length($hour)==1;
	substr($min,0,0)='0' 	if length($min)==1;
	substr($sec,0,0)='0' 	if length($sec)==1;

	my($rc)=$year.$mon.$mday.$hour.$min;;
	return $rc;

