BEGIN {
   if( ! defined $ENV{SYBASE} or ! -d $ENV{SYBASE} ) {
      if( -d "/export/home/sybase" ) {
         $ENV{SYBASE}="/export/home/sybase";
      } else {
         $ENV{SYBASE}="/apps/sybase";
      }
   }
}
