use strict;
use Repository;
use Getopt::Long;
use vars qw( $DEBUG $TYPE);
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless
   GetOptions(  "DEBUG"		=> \$DEBUG, "TYPE=s"=>\$TYPE);

$| = 1;

print "test_ping_databases.pl\n";
print "This test will ping all your databases... This may take a while\n";
my($dir)=get_gem_root_dir();
chdir $dir 	or die "Cant cd to $dir: $!\n";

my(@perl_includes);
foreach (@INC) { push @perl_includes, $_ if /$dir/;  }

my($cmd)="$^X -I".join(" -I",@perl_includes)." $dir/ADMIN_SCRIPTS/bin/ping_server.pl --NOLOCK --NOLOG";
$cmd.=" --TYPE=$TYPE" if $TYPE;
print $cmd,"\n";

my(@warnings);
open( OUT, $cmd." |" ) or die "Cant Run Command $cmd : $!\n";
foreach (<OUT>){
	chomp;
	print ">> $_\n";
	next if /ok\.\.\./ or /^Found .+Servers$/;
	next if /^ping_server.pl/ or /^BatchId=/;
	next if /reachable/;
	next if /Adding/ and /Servers/;
	s/^\s+//;
	my($svr,$state)=split(/\s+/,$_,2);
	push @warnings, sprintf("Server=%-20s %s", $svr, $state);
}
close(OUT);

foreach (@warnings) { print "ERROR: $_\n"; }
if( $#warnings<0 ) {  print "SUCCESSFUL: Connectivity OK\n"; }
exit( $#warnings+1);
