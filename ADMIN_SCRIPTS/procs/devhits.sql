use sybsystemprocs
go

IF EXISTS (SELECT * FROM sysobjects
	   WHERE  name = "sp__devhits"
	   AND    type = "P")
   DROP PROC sp__devhits

go

CREATE PROC sp__devhits(
		@num_unit_delay int=NULL,
		@num_iter int=NULL,
		@unit char(1)=NULL )
AS
/******************************************************************************
**
** Name        : sp__devhits
**
** Created By  : Mich Talebzadeh based on sp__monio proc by Ed Barlow (GEM LIbrary)
** Date        : 16/03/2009
** Purpose     : Works out various device I/O matrix from monDeviceIO
**
******************************************************************************/
set nocount on
declare @delay char(8)
/*
** Compute number of Pages in a Megabyte.
*/
declare @low    int, @pgsmb float      /* Number of Pages per Megabytes */

select @pgsmb = (1048576. / v.low) from  master.dbo.spt_values v
where v.number = 1 and   v.type = "E"


select
	 name
	,"SizeMB" = convert(char(6),convert(int,((1. + high - low)/@pgsmb)))
into #devsize
from master..sysdevices

if @unit is not null
begin
   if UPPER(@unit) not in ('S','M','H')
   begin
	print 'The unit has to be in S (seconds), M (minutes) or H (hours)'
	return -1
   end
   else
      begin
        set @unit = UPPER(@unit)
	if(@unit = 'H' and @num_unit_delay < 10) set @delay=convert(char(1),@num_unit_delay)+":00:00"
	if(@unit = 'H' and @num_unit_delay >= 10) set @delay=convert(char(2),@num_unit_delay)+":00:00"
	if(@unit = 'H' and @num_unit_delay > 99) return -1
	if(@unit = 'M' and @num_unit_delay < 10) set @delay="00:"+convert(char(1),@num_unit_delay)+":00"
	if(@unit = 'M' and @num_unit_delay >= 10) set @delay="00:"+convert(char(2),@num_unit_delay)+":00"
	if(@unit = 'M' and @num_unit_delay > 99) return -1
	if(@unit = 'S' and @num_unit_delay < 10) set @delay ="00:00:0"+convert(char(1),@num_unit_delay)
	if(@unit = 'S' and @num_unit_delay >= 10) set @delay="00:00:"+convert(char(2),@num_unit_delay)
	if(@unit = 'S' and @num_unit_delay >= 60) return - 1
      end
end
declare @date datetime

if @num_unit_delay is null
begin
	set @date = getdate()
	print " Starting at %1!",@date
	print " "
	select
		 "Device" =  substring(m.LogicalName+" ("+d.SizeMB+" MB)",1,30)
		,m.Reads
		,m.APFReads
		,m.Writes
		--,"SMPRequests" = m.DevSemaphoreRequests
		--,"SMPWaits" = m.DevSemaphoreWaits
		,"Total IOTime/ms" = m.IOTime
		,"IOTime per ReadWrite" = convert(numeric(10,2),(IOTime*1.0)/(Reads+APFReads+Writes)*1.)
		,"No ReadWrite in 1 ms" = convert(numeric(10,2),(Reads+APFReads+Writes)*1./(IOTime*1.0))
	from   master..monDeviceIO m, #devsize d
	where  m.LogicalName = d.name
	order by (m.Reads+m.APFReads+m.Writes)*1./(m.IOTime*1.0)
end
else
begin
	select
		 LogicalName
		,Reads
		,APFReads
		,Writes
		,IOTime
	into #tmp
	from   master..monDeviceIO

	if @num_iter is null
		select @num_iter=100
	while @num_iter>0
	begin
		set @date = getdate()
		print " %1!, waiting for %2! %3! to collect statistics",@date,@num_unit_delay,@unit
		print " "
		waitfor delay @delay

		print ""
		print "Below IOTime is Total amount of time (in milliseconds) spent waiting for I/O requests to be satisfied"
		print "Devices incurring IOTime"
		select
		                "Device" =  substring(m.LogicalName+" ("+d.SizeMB+" MB)",1,30)
				,Reads=	m.Reads-t.Reads
				,APFReads=	m.APFReads-t.APFReads
				,Writes=	m.Writes-t.Writes
				,IOTime=	m.IOTime-t.IOTime
                                ,"IOTime per I/O in ms" =
				CASE when m.IOTime-t.IOTime > 0
				  THEN
						convert(numeric(10,2),
		                 ((m.IOTime-t.IOTime)*1.0)/((m.Reads-t.Reads)+(m.APFReads-t.APFReads)+(m.Writes-t.Writes))*1.)
                                ELSE
				              0
				END
                                ,"No of I/O per ms" =
				CASE when m.IOTime-t.IOTime > 0
				  THEN
						convert(numeric(10,2),
                                              ((m.Reads-t.Reads)+(m.APFReads-t.APFReads)+(m.Writes-t.Writes))*1./((m.IOTime-t.IOTime)*1.0))
                                ELSE
				              0
				END
		from master..monDeviceIO m,#tmp t, #devsize d
		where
		m.LogicalName = t.LogicalName
		and
		m.LogicalName = d.name
		and m.IOTime-t.IOTime > 0
		order by ((m.Reads-t.Reads)+(m.APFReads-t.APFReads)+(m.Writes-t.Writes))*1./((m.IOTime-t.IOTime)*1.0)

		print ""
		print "Devices incurring no IOTime or inactive"
		select
		                "Device" =  substring(m.LogicalName+" ("+d.SizeMB+" MB)",1,30)
				,Reads=	m.Reads-t.Reads
				,APFReads=	m.APFReads-t.APFReads
				,Writes=	m.Writes-t.Writes
				,IOTime=	m.IOTime-t.IOTime
				,"IOTime per ReadWrite" = 0
                                ,"No of I/O per ms" = 0
		from master..monDeviceIO m,#tmp t, #devsize d
		where
		m.LogicalName = t.LogicalName
		and
		m.LogicalName = d.name
		and m.IOTime-t.IOTime = 0
		order by m.LogicalName

		delete #tmp

		insert #tmp
		select LogicalName,Reads,APFReads,Writes,IOTime
		from   master..monDeviceIO

		select @num_iter = @num_iter - 1
	end
end

return

go

GRANT EXECUTE ON sp__devhits  TO public
go
sp_help sp__devhits
go
