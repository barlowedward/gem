#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Copy::Recursive qw(rcopy pathrmdir);
use Repository;
use Do_Time;
use Getopt::Long;
use MlpAlarm;
use vars qw( $BATCHID );

sub usage {
	print "usage ConsoleArchiver.pl --BATCHID=id\n";
	return "";
}
die usage("Bad Parameter List\n") unless GetOptions( "BATCHID=s" => \$BATCHID );
die "MUST PASS --BATCHID!\n" unless $BATCHID;

MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor');

my($OVERWRITE)=1;		# set to 1 if you want to remove and overwrite

my($dir)=get_gem_root_dir();
die "NO CONSOLE_REPORTS DIRECTORY" unless -d $dir."/data/CONSOLE_REPORTS";

my($curdate)=do_time(-fmt=>'yyyy_mon');
my($sourcedir)= $dir."/data/CONSOLE_REPORTS";
my($targetdir)= $dir."/data/CONSOLE_REPORTS_".$curdate;

die "Source Directory $sourcedir Does Not Exist!" unless -d $sourcedir;
if(  -d $targetdir ) {
	if( $OVERWRITE ) {
		pathrmdir( $targetdir )						or die "cant pathrmdir $targetdir: $!";
	} else {
		die "ERROR Target Directory $targetdir Exists - use --OVERWRITE to continue\n";
	}
}

print "POPULATING TARGET DIRECTORY $targetdir\n";
rcopy(  $sourcedir, $targetdir )			or die "cant copy to $targetdir: $!";

print "POPULATING AUDIT DIRECTORIES\n";
my($rdir)=$dir."/data/BACKUP_LOGS";
opendir(DIR,$rdir) || return l_warning("Cant read $rdir Directory\n");
my(@dirlist) = grep(!/^\./,readdir(DIR));
closedir(DIR);

foreach ( @dirlist ) {
	next unless -d $rdir."/".$_;
	next unless -d $rdir."/".$_."/audits";
	print "Copying ".$rdir."/".$_."/audits to ".$targetdir."/".$_."_audit\n";
	rcopy( $rdir."/".$_."/audits", $targetdir."/".$_."_audit" );
}

print "Successful Completion";
MlpBatchJobEnd();
exit(0);

__END__

=head1 NAME

ConsoleArchiver.pl - Archives The Console

=head2 USAGE

	ConsoleArchiver.pl

=head2 DESCRIPTION

Archives the console. I have found that keeping backups of the console is invaluable for
disaster situations.  The specific reasons:

  you can lose the configuration information you care about because it get overwritten

  you may need to look up configuration information from last month

  disk space is cheap!

This will be run from a weekly batch, but only the first week of the month will actually do the
overwrite.  After that it just exits out...

=cut

