use lib "G:/dev/Win32_perl_lib_5_6";
use strict;
use Data::Dumper;

use Win32::PerfMon;

my $xxx = Win32::PerfMon->new("\\\\10.5.108.183");
if($xxx) {
	my $Data = $xxx->ListObjects();
	foreach my $index (@$Data) {
		print "INDEX: $index\n";
	}
	print "\n\n";
	$Data = $xxx->ListCounters("System");
	if($Data == -1)	{
		print "ERROR: ". $xxx->{'ERRORMSG'} ."\n";
	} else {
		foreach my $index (@$Data) {
			print "System Counters: $index\n";
		}
	}
	$Data = $xxx->ListInstances("System");
	if($Data == -1)	{
		print "ERROR: ". $xxx->{'ERRORMSG'} ."\n";
	} else 	{
		foreach my $index (@$Data) {
			print "$index\n";
		}
	}
}
