-- update the places table based on tag matches

use racks
go
-- select * into barlow_place_map_XXX from barlow_place_map
go
delete barlow_place_map
go
/*
insert barlow_place_map
select distinct
wasp_site,wasp_location, wasp_floor, wasp_row, wasp_cabinet, cntrycitybldgnum, floor, row, cabinet
from barlow_precompute p, machines m
where convert(varchar,m.machnumber = convert(int,p.asset_tag)
and ( lower(wasp_floor)=lower(floor) and lower(wasp_row)=lower(row) and lower(wasp_cabinet)=lower(cabinet)  )
order by wasp_site,wasp_floor,wasp_row,wasp_cabinet
*/
insert barlow_place_map
select distinct
wasp_site,wasp_location, wasp_floor, wasp_row, wasp_cabinet, cntrycitybldgnum, floor, row, cabinet
from barlow_precompute p, machines m
where convert(varchar,m.machnumber) = p.asset_tag
-- and ( lower(wasp_floor)=lower(floor) and lower(wasp_row)=lower(row) and lower(wasp_cabinet)=lower(cabinet)  )
-- and  wasp_site='Ridgefield Park' and machinename='md23b666'
order by wasp_site,wasp_floor,wasp_row,wasp_cabinet

go

select * from barlow_precompute where asset_tag like 'use%'
-- select * from barlow_place_map
