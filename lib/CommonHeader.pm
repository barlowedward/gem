#######################################################################
# Standard Header
#  - Begin block is useful
#######################################################################

# copyright (c) 2005-2007 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

package  CommonHeader;

use MlpAlarm;
use File::Basename;

use      strict;
use      Carp;

BEGIN {
   # alarm unsupported in perl 5.6 from activestate
   alarm(7200) if $]>5.007001 or ! defined $ENV{PROCESSOR_LEVEL};

  if( ! defined $ENV{SYBASE} or $ENV{SYBASE}=~/^\s*$/ or ! -d $ENV{SYBASE} ) {
		if( 1==2 ) {
     	# *****************************
   	# WARNING - DO NOT REMOVE THIS COMMENT
	#******************
		} elsif( -d "/apps/sybase" ) {
			$ENV{SYBASE}="/apps/sybase";
		} elsif( -d "C:/sybase" ) {
			$ENV{SYBASE}="C:/sybase";
		} elsif( -d "/export/home/sybase" ) {
			$ENV{SYBASE}="/export/home/sybase";
		} elsif( -d "D:/sybase" ) {
			$ENV{SYBASE}="D:/sybase";
		} elsif( -d "/apps/sybase.linux/12.5.1b" ) {
			$ENV{SYBASE}="/apps/sybase.linux/12.5.1b";
		} elsif( -d "C:/sybase" ) {
			$ENV{SYBASE}="C:/sybase";
		} elsif( -d "C:/sybase" ) {
			$ENV{SYBASE}="C:/sybase";
		} elsif( -d "C:/sybase" ) {
			$ENV{SYBASE}="C:/sybase";
		} elsif( -d "C:/sybase" ) {
			$ENV{SYBASE}="C:/sybase";
	#******************
   	# WARNING - THANK YOU FOR NOT REMOVING THIS EITHER
   	# *****************************
   	# EVERYTHING BELOW HERE IS GRAVY - JUST IN CASE YOU DIDNT SET THINGS UP VIA GEM!
  		} elsif( -d "/export/home/sybase" ) {
  			$ENV{SYBASE}="/export/home/sybase";
  		} elsif( -d "/opt/sybase" ) {
  			$ENV{SYBASE}="/opt/sybase";
  		} elsif( -d "/apps/sybase" ) {
  			$ENV{SYBASE}="/apps/sybase";
  		} elsif( -d "C:/sybase" ) {
  			$ENV{SYBASE}="C:/sybase";
  		} elsif( -d "D:/sybase" ) {
  			$ENV{SYBASE}="D:/sybase";
  		}
  	}
}

$SIG{ALRM} = sub {
	my($cmd)=$0." ".join(" ",@ARGV);
	chomp $cmd;
	$cmd=~s/-P\w+/-PXXX/;
	$cmd=~s/-PASSWORD=\w+/-PASSWORD=XXX/;

	MlpEvent(
		-monitor_program=>"GEM",
		-system=>"GEM",
		-message_text=>"* * * 2 HOUR TIMEOUT DETECTED * * *\nCommand = $cmd\n",
	 	-severity => "ERROR",
	);
	die "* * * TIMEOUT DETECTED * * *\n Applications may run for 2 Hours Maximum.\nCommand = $cmd\n";
};

1;

__END__

=head1 NAME

CommonHeader.pm - 2 HOUR TIMEOUT!

=head2 DESCRIPTION

This module will be included in most batch jobs.  It is currently used to set
an alarm to reap processes that get stuck and to define your SYBASE variables.

use alarm(0) to cancel if you want - i use --NOTIMEOUT as a stanadard arg for this
