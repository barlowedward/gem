in with "N.A." instead of the actual password.

=head1 NAME

sp__revmirror - Reverse engineer mirrors on current server

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


reverse engineers mirror information

=head2 USAGE


sp__revuser

=head2 SEE ALSO


sp__revalias, sp__revdb,
sp__revdevice, sp__revgroup,
sp__revindex, sp__revlogin,
sp__revmirror, sp__revuser
WARNING:

Run all the reverse engineering utilities in at least 180 column mode
(-w180) to prevent line wrapping.

=head2 SAMPLE OUTPUT


 1> sp__revmirror
 ----------------------------------------------------------
 disk mirror name='data2',mirror='/home/programs/sybase/datax'
