#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# unbuffer
$|=1;

use strict;
use File::Basename;
use Getopt::Std;
use Getopt::Long;
use Repository;
use DBIFunc;
use Sys::Hostname;
use CommonFunc;

use vars qw($opt_e $opt_h $opt_m $opt_D $opt_U $opt_S $opt_P $opt_d $opt_s $opt_u $opt_p $opt_t $BATCH_ID $opt_X $opt_Q $ONE_ROW $opt_T $opt_f
	$opt_q $DELAY_SECS $NUMERIC $MAILHOST $ALERTTO $ALERTFROM $MAIL_ERROR_ONLY $MAILSUBJECT);
use vars qw($VERBOSE);

sub usage
{
	print @_,"\n";
	print
"Usage:
query_compare.pl -X -SRC_SERVER=usr -SRC_USER=srv -SRC_PASSWORD=pass -SRC_DATABASE=db -SRC_QUERY=query
						  -TGT_SERVER=usr -TGT_USER=srv -TGT_PASSWORD=pass -TGT_DATABASE=db -TGT_QUERY=query

				- or -
query_compare.pl (above args) -ffile
				- or -
query_compare.pl (above args) -TABLES=table_list

-t output table (on source system/db)
-h to htmlize output
-X option to set verbose mode
-q query (defaults to $opt_Q)
-e exclude column numbers (separated by ,) - first column is 1
--DELAY_SECS seconds delay between running query on primary & seconary
--ONE_ROW - the output is a single row numeric i.e. a count(*) or max(id)
--NUMERIC - the output is numeric - ONE_ROW must be defined
--ALERTTO - send an alert if/when the query outputs are not equal.  If --NUMERIC then the secondary must be
          - greater or equal to the primary value.  This can be used for replication testing.  Requires --ALERTFROM
          - and --MAILHOST variables also. Optionally uses --MAILSUBJECT

If you only pass in one database - they are assumed the same.  Table List is space separated and in quotes.\n";
	exit 0;
}

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SRC_SERVER=s"			=> \$opt_S,
		"SRC_USER=s"			=> \$opt_U,
		"SRC_PASSWORD=s"		=> \$opt_P,
		"SRC_DATABASE=s"		=> \$opt_D,
		"TGT_SERVER=s"			=> \$opt_s,
		"TGT_USER=s"			=> \$opt_u,
		"TGT_PASSWORD=s"		=> \$opt_p,
		"TGT_DATABASE=s"		=> \$opt_d,
		"VERBOSE"				=> \$opt_X,
		"DEBUG"					=> \$opt_X,

		"HTML"					=> \$opt_h,
		"ONE_ROW"				=> \$ONE_ROW,
		"NUMERIC"				=> \$NUMERIC,
		"ALERTTO=s"				=> \$ALERTTO,
		"ALERTFROM=s"			=> \$ALERTFROM,
		"MAILHOST=s"			=> \$MAILHOST,
		"MAILSUBJECT=s"		=> \$MAILSUBJECT,
		"BATCH_ID=s"			=> \$BATCH_ID,
		"SRC_QUERY=s"			=> \$opt_Q,
		"TABLES=s"				=> \$opt_T,
		"EXCLUDE_COLS=s"		=> \$opt_e,
		"FILE=s"					=> \$opt_f,
		"TGT_QUERY=s"			=> \$opt_q,
		"DELAY_SECS=s"			=> \$DELAY_SECS,
		"MAIL_ERROR_ONLY"		=> \$MAIL_ERROR_ONLY
);

# usage("Bad Parameter List") unless getopts('chXU:S:P:D:u:s:p:d:Q:T:e:f:q:');

# Can pass only 1 parm
$opt_u=$opt_U if defined $opt_U and ! defined $opt_u;
$opt_U=$opt_u if defined $opt_u and ! defined $opt_U;

$opt_p=$opt_P if defined $opt_P and ! defined $opt_p;
$opt_P=$opt_p if defined $opt_p and ! defined $opt_P;

$opt_s=$opt_S if defined $opt_S and ! defined $opt_s;
$opt_S=$opt_s if defined $opt_s and ! defined $opt_S;

usage("Must Pass Srv with -S") unless defined $opt_S;
usage("Must Pass Srv with -s") unless defined $opt_s;

$opt_q = $opt_Q if defined $opt_Q and ! defined $opt_q;
$opt_Q = $opt_q if defined $opt_q and ! defined $opt_Q;

$opt_d = $opt_D if defined $opt_D and ! defined $opt_d;
$opt_D = $opt_d if defined $opt_d and ! defined $opt_D;

$VERBOSE=1 if defined $opt_X;

if( defined $opt_S and ! defined $opt_U ) {
	print "Getting Password for $opt_S \n" if defined $VERBOSE;
   ($opt_U,$opt_P)=get_password(-name=>$opt_S,-type=>"sybase");
   ($opt_U,$opt_P)=get_password(-name=>$opt_S,-type=>"sqlsvr")
   	unless defined $opt_U and defined $opt_P;

	die "Unable To Retrieve User/Password for $opt_S"
		unless defined $opt_U and defined $opt_P;
}

if( defined $opt_s and ! defined $opt_u ) {
	print "Getting Password for $opt_s \n" if defined $VERBOSE;
   ($opt_u,$opt_p)=get_password(-name=>$opt_s,-type=>"sybase");

   ($opt_u,$opt_p)=get_password(-name=>$opt_s,-type=>"sqlsvr")
   	unless defined $opt_u and defined $opt_p;

	die "Unable To Retrieve User/Password for $opt_s"
		unless defined $opt_u and defined $opt_p;
}

if( defined $opt_f and ! defined $opt_Q ) {
	# open file
	die "File Not Found" unless -r $opt_f;
	$opt_Q="";
	open(FILE,$opt_f) or die "Cant open $opt_f : $!\n";
	while (<FILE>) { $opt_Q .= $_; }
	close FILE;
	$opt_q=$opt_Q;
}

usage("Must Pass User with -U" ) unless defined $opt_U;
usage("Must Pass Pass with -P" ) unless defined $opt_P;
usage("Must Pass User with -u" ) unless defined $opt_u;
usage("Must Pass Pass with -p" ) unless defined $opt_p;
usage("Must Pass Query with -Q") unless defined $opt_Q or defined $opt_T;

usage("Must Pass Database") unless defined $opt_d or defined $opt_D;
$opt_d=$opt_D unless defined $opt_d;
$opt_D=$opt_d unless defined $opt_D;

my($NL)="\n";
$NL="<BR>\n" if defined $opt_h;

if( defined $VERBOSE ) {
	print "<h1>\n" if defined $opt_h;
	print "QUERY OUTPUT COMPARISON REPORT\n";
	print "</h1>\n" if defined $opt_h;
	print "SERVER #1 = $opt_S  DATABASE = $opt_D $NL\n";
	print "SERVER #2 = $opt_s  DATABASE = $opt_d $NL\n";
	print "COLUMNS TO EXCLUDE = $opt_e $NL\n" if defined $opt_e;
	print "<hr>" if defined $opt_h;
}

print "Attempting to connect to $opt_S\n" if $VERBOSE;
my($connection1)=dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -debug=>$VERBOSE, -connection=>1);
if( ! defined $connection1 ) {
	die("Can't connect to the $opt_S Sybase server as login $opt_U.\n")
}
print "Connected To $opt_S $NL";

print "Attempting to connect to $opt_s\n" if $VERBOSE;
my($connection2)=dbi_connect(-srv=>$opt_s,-login=>$opt_u,-password=>$opt_p, -connection=>1);
if( ! defined $connection2 ) {
	die("Can't connect to the $opt_s Sybase server as login $opt_u.\n")
}
print "Connected To $opt_s $NL";

print "Using Database $opt_D in $opt_S $NL" if defined $VERBOSE;
dbi_use_database($opt_D,$connection1) or die "CANT USE DATABASE $opt_D\n";

print "Using Database $opt_d in $opt_s $NL" if defined $VERBOSE;
dbi_use_database($opt_d,$connection2)  or die "CANT USE DATABASE $opt_d\n";

my($t,@tables);
if( defined $opt_T ) {
	print "Found -T Tables => $opt_T $NL" if defined $VERBOSE;
	@tables=split /\s+/, $opt_T;
	print "Working on ",join(" ",@tables),$NL if defined $VERBOSE;
	$t=pop @tables;
	print $NL,"Next Table is $t",$NL if defined $t;
	$opt_Q="select * from $t\n" unless defined $opt_Q;
	$opt_q="select * from $t\n" unless defined $opt_q;
}

my(@OUTPUT_OF_1,@OUTPUT_OF_2);
while( defined $opt_Q ) {
	@OUTPUT_OF_1=();
	@OUTPUT_OF_2=();

	print "QUERY: ",$opt_Q,$NL if defined $VERBOSE;
	my(@rc1)=dbi_query(-query=>$opt_Q,-db=>$opt_D,-connection=>$connection1);
	print "Query 1 Returned ", ($#rc1 +1)," ROWS $NL" if defined $VERBOSE;
	print "Sleeping $DELAY_SECS seconds\n" if $DELAY_SECS;
	sleep($DELAY_SECS) if $DELAY_SECS;
	my(@rc2)=dbi_query(-query=>$opt_q,-db=>$opt_d,-connection=>$connection2);
	print "Query 2 Returned ", $#rc2+1," ROWS $NL" if defined $VERBOSE;

	my($cnt)=0;
	foreach (@rc1 ) {
		my(@dat)=dbi_decode_row($_);
		print "Query 1 Returned ", join(" ",@dat)," $NL" if defined $VERBOSE;
		# Undefine Columns That Are To Be Excluded
		if( defined $opt_e ) {
			foreach (split(",",$opt_e)) { $dat[$_ - 1]=""; }
		}
		grep(s/\s+$//, @dat);
		push @OUTPUT_OF_1,join("|",@dat);
		$cnt++;
		print "." if $cnt % 100 == 0 and defined $VERBOSE;
	}

	$cnt=0;
	foreach (@rc2 ) {
		my(@dat)=dbi_decode_row($_);
		print "Query 2 Returned ", join(" ",@dat)," $NL" if defined $VERBOSE;
		# Undefine Columns That Are To Be Excluded
		if( defined $opt_e ) {
			foreach (split(",",$opt_e)) { $dat[$_ - 1]=""; }
		}
		grep(s/\s+$//, @dat);
		push @OUTPUT_OF_2,join("|",@dat);
		$cnt++;
		print "." if $cnt % 100 == 0 and defined $VERBOSE;
	}

	if( $ONE_ROW ) {
		die "OUTPUT of Query 1 is NOT 1 Row\n".join("\n",@OUTPUT_OF_2)."\n" if $#OUTPUT_OF_1!=0;
		die "OUTPUT of Query 2 is NOT 1 Row\n".join("\n",@OUTPUT_OF_2)."\n" if $#OUTPUT_OF_2!=0;
		my($v1)=$OUTPUT_OF_1[0];
		my($v2)=$OUTPUT_OF_2[0];
		die "NO OUTPUT From Server 1\n" unless $v1;
		die "NO OUTPUT From Server 2\n" unless $v2;

		print "V1=$v1 V2=$v2 - Comparing - NUMERIC=$NUMERIC\n" if $VERBOSE;
		if( $NUMERIC ) {
			die "OUTPUT of Query 1 is NOT numeric\n".join("\n",@OUTPUT_OF_2)."\n" if int($v1) ne $v1;
			die "OUTPUT of Query 2 is NOT numeric\n".join("\n",@OUTPUT_OF_2)."\n" if int($v2) ne $v2;

			if( $v1 > $v2 ) {
				print "ERROR: $v2 < $v1\n";
				my($txt)="The Output of Query $opt_Q on $opt_S is: $v1\nThe Output of Query $opt_q on $opt_s is: $v2\n";
				my($subject)="Query ERROR";
				$subject="$BATCH_ID ERROR" if $BATCH_ID;
				$subject = $MAILSUBJECT if $MAILSUBJECT;
				my($prog)= $BATCH_ID || "query_compare.pl";
				send_mail_message($ALERTTO,$ALERTFROM,$MAILHOST,$subject,"<h2>$BATCH_ID ERROR $MAILSUBJECT</h2>$txt<p>Message Generated By $prog at ".localtime(time)."<br>\nFrom Host ".hostname()."\n"	);
			} else {
				print "OK : $v2 >= $v1\n";
				if( ! $MAIL_ERROR_ONLY ) {
					my($txt)="The Output of Query <font color=blue>$opt_Q</font> on <font color=blue>$opt_S</font> is: <font color=blue>$v1</font><br>\nThe Output of Query <font color=blue>$opt_q</font> on <font color=blue>$opt_s</font> is: <font color=blue>$v2</font>\n";
					my($subject)="Query OK";
					$subject="$BATCH_ID OK" if $BATCH_ID;
					$subject = $MAILSUBJECT if $MAILSUBJECT;
					my($prog)= $BATCH_ID || "query_compare.pl";
					send_mail_message($ALERTTO,$ALERTFROM,$MAILHOST,$subject,"<h2>$BATCH_ID OK $MAILSUBJECT</h2>$txt<p>Message Generated By $prog at ".localtime(time)."<br>\nFrom Host ".hostname()."\n"	);
				}
			}
		} else {
			if( $v1 ne $v2 ) {
				print "ERROR: $v2 != $v1\n";
				my($txt)="The Output of Query $opt_Q on $opt_S is: $v1\nThe Output of Query $opt_q on $opt_s is: $v2\n";
				my($subject)="Query ERROR";
				$subject="$BATCH_ID ERROR" if $BATCH_ID;
				$subject = $MAILSUBJECT if $MAILSUBJECT;
				my($prog)= $BATCH_ID || "query_compare.pl";
				send_mail_message($ALERTTO,$ALERTFROM,$MAILHOST,$subject,"<h2>$BATCH_ID ERROR $MAILSUBJECT</h2>$txt<p>Message Generated By $prog at ".localtime(time)."<br>\nFrom Host ".hostname()."\n"	);
			} else {
				print "OK\n";
				if( ! $MAIL_ERROR_ONLY ) {
					my($txt)="The Output of Query <font color=blue>$opt_Q</font> on <font color=blue>$opt_S</font> is: <font color=blue>$v1</font><br>\nThe Output of Query <font color=blue>$opt_q</font> on <font color=blue>$opt_s</font> is: <font color=blue>$v2</font>\n";
					my($subject)="Query OK";
					$subject="$BATCH_ID OK" if $BATCH_ID;
					$subject = $MAILSUBJECT if $MAILSUBJECT;
					my($prog)= $BATCH_ID || "query_compare.pl";
					send_mail_message($ALERTTO,$ALERTFROM,$MAILHOST,$subject,"<h2>$BATCH_ID OK $MAILSUBJECT</h2>$txt<p>Message Generated By $prog at ".localtime(time)."<br>\nFrom Host ".hostname()."\n"	);
				}
			}
		}

	} else {
		run_diff();
	}

	undef $opt_Q if defined $opt_Q;

	if( $#tables>=0 ) {
		$t=pop @tables;
		print $NL,"Next Table is $t",$NL;
		$opt_Q="select * from $t \n";
		$opt_q="select * from $t \n";
	}
}
dbi_disconnect(-connection=>$connection1);
dbi_disconnect(-connection=>$connection2);
exit(0);

# =====================
# Compare Output Results Rows
# =====================
sub run_diff
{
	my( %ROW_NUMBER_1, %ROW_NUMBER_2);		# ROW NUMBER
	my( %ROW_IN_1, %ROW_IN_2);		# ROW & NUMBER OF MATCH
	my($identical_row_cnt)=0;

	#
	# OUTPUT_OF_1 & OUTPUT_OF_2 are our input arrays
	#
	my($cnt)=1;
	foreach (@OUTPUT_OF_1) { $ROW_NUMBER_1{$_}=$cnt++; }
	$cnt=1;
	foreach (@OUTPUT_OF_2) { $ROW_NUMBER_2{$_}=$cnt++; }

	foreach ( @OUTPUT_OF_1 ) {
		next unless defined $ROW_NUMBER_2{$_};

		# IDENTICAL ROW
		$identical_row_cnt++;
		$ROW_IN_2{$_} = $ROW_NUMBER_2{$_};
		$ROW_IN_1{$_} = $ROW_NUMBER_1{$_};
	}

	# PRINT OUTPUT
	if( defined $opt_h ) {
		print "<TABLE WIDTH=100%>\n<TR><TH>ROW</TH><TH>In $opt_S - $opt_D</TH><TH>In $opt_s - $opt_d</TH></TR>\n";

		#
		# OK - NOW PRINT ROWS 1 AT TIME
		#  IF THEY HAD MATCH PRINT THEM IN BLUE FOR NOW
		my(%linenums);
		my($cnt)=0;
		my($found)=1;
		while( $cnt <= $#OUTPUT_OF_1 or $cnt <= $#OUTPUT_OF_2 ) {
			print "<TR><TD>line ", $cnt+1,"</TD>" if $found;
			$found=0;
			if( defined $ROW_IN_2{$OUTPUT_OF_1[$cnt]} ) {
				#print "<TD><FONT COLOR=BLUE>",$OUTPUT_OF_1[$cnt],"</TD>\n";
			} else {
				$found++;
				print "<TD>",$OUTPUT_OF_1[$cnt],"</TD>\n";
			}
			if( defined $ROW_IN_1{$OUTPUT_OF_2[$cnt]} ) {
				#print "<TD><FONT COLOR=BLUE>",$OUTPUT_OF_2[$cnt],"</TD>\n";
			} else {
				print "<TD></TD>" if $found==0;
				$found++;
				$found++;
				print "<TD>",$OUTPUT_OF_2[$cnt],"</TD>\n";
			}
			print "</TR>" if $found;
			$cnt++;
		}
		print "</TABLE>\n";
	} else {
		print "$identical_row_cnt IDENTICAL ROWS FOUND $NL";
		print "ROWS ONLY IN OUTPUT FROM $opt_S $opt_D $NL";
		foreach ( @OUTPUT_OF_1 ) {
			print "<<",$_," $NL" unless defined $ROW_IN_2{$_};
		}
		print "ROWS ONLY IN OUTPUT FROM $opt_s $opt_d $NL";
		foreach ( @OUTPUT_OF_2 ) {
			print ">>",$_," $NL" unless defined $ROW_IN_2{$_};
		}
	}
}

# sub numerically { $a <=> $b; };

__END__

=head1 NAME

query_compare.pl - Compare Query Results in Two DB's

=head2 DESCRIPTION

This perl script runs a query in two servers and then compares the output.  This could be quite useful in a variety of situations, specifically when you want to regression test a new procedure.

Query does not care about the order of the output

Unless you use the B<-h> (html flag), output columns are separated by the pipe | character which might be slightly confusing.

=head2 USAGE

Usage: query_compare -X -Uusr -Ssrv -Ppass -uusr -ppass -ssrv -Ddb -ddb -Qquery

If you ignore the B<-s> parameter, it is assumed that you want to use the same dataserver for both sides of the comparison.

=head2 Output Table

If you have -t<TBL> defined, the output will be sored specially in a table named <TBL> in the source server/database.  The format of
this table and its creation are up to you but should be of the following syntax

create table XXX (
  mon_time        datetime not null,
  dsn_source      varchar(30) not null,
  dsn_secondary   varchar(30) not null,
  rslts_source    int not null,
  rslts_secondary int not null
)

the query obviously must return one and only one row containing an int.  a count(*) query or max(id) query is appropriate.

=head2 SAMPLE RUNS

The following query compares servers SYBASE_QA and SYBASE_INT in database xrm_db to see which tables are in one server but not in the other

query_compare -SSYBASE_QA -sSYBASE_INT -Dxrm_db -Qselect name from sysobjects  where type="U" uid=1 order by name

The following shell program compares data in  $1 on two servers

	#!/bin/sh
	TBLS="tbl1 tbl2 tbl3"
	SRV1=SYBASE_INT
	SRV2=SYBASE_QA
	for tbl in $TBLS
	do
	query_compare -Usa -S$SRV1 -PPASSWORD -Dxrm_db -usa \
		-s$SRV2 -pPASSWORD -dxrm_db -Q"select * from $tbl"
	mv data/outS data/$tbl.$SRV1
	mv data/outs data/$tbl.$SRV2
	done

=cut
