#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use Repository;
use DBIFunc;
my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw($BATCH_ID $DEBUG);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID

   --BATCH_ID=id     [ if set - will log via alerts system ]
   --DEBUG \n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

# die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"		=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

my(%ATT_MAP)=(
		'password' 			=> 1,
		#'uid' 				=> 2,
		'gid' 				=> 3,
		'description' 		=> 4,
		'home directory'	=> 5,
		'dflt shell' 		=> 6
);


my($connection)=MlpGetDbh();
my($dir)=get_gem_root_dir();

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Credential Mgt' ) if $BATCH_ID;

my($q)= "delete INV_credential where credential_type = '/etc/passwd'";
MlpAlarm::_querynoresults($q);

my($q)= "delete INV_credential_attribute where credential_type = '/etc/passwd'";
MlpAlarm::_querynoresults($q);

#/* , is_deleted       , display_name     */
my($cred_sql) = "(credential_name, credential_type, credential_id, system_name, system_type, collection_date, collected_by )";

my(%found_logins); 		# sigh - etc passwd can have duplicates

my($q)=$^X." $dir/ADMIN_SCRIPTS/rdist/run_cmd_on_all_hosts.pl ".'-c "cat /etc/passwd"';
print "Executing ",$q,"\n";
open(X,$q." |") or die "Cant run $q $!\n";
while(<X>) {
	chomp;
	next if /NO TIMEOUT SPEC/;
	next unless /^\s/;
	s/^\s+//g;
	my($h,$line)=split(/\s+/,$_,2);
	print $h,"::",$line,"\n" if $DEBUG;
	my(@vals)=split(/:/,$line);

	next if $found_logins{$h.":".$vals[0]};
	$found_logins{$h.":".$vals[0]} = 1;

	my($base)=			dbi_quote(-connection=>$connection,-text=>$vals[0]).",".			# login
							"'/etc/passwd',".
							dbi_quote(-connection=>$connection,-text=>$vals[2]).",".			# sid
							dbi_quote(-connection=>$connection,-text=>$h).			# hostname
							",'unix'";									# system, systype

	# print "V0=$vals[0] BASE=$base\n";
	my($q)= "insert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
	MlpAlarm::_querynoresults($q);

	foreach my $str (keys %ATT_MAP ) {
		my($col)=$ATT_MAP{$str};
		$vals[$col] = "N.A." if $vals[$col] =~ /^\s*$/;
		$q="insert INV_credential_attribute values \n\t( $base ,\n\t '".$str."',".
						 dbi_quote(-connection=>$connection,-text=>$vals[$col])	.")";
		MlpAlarm::_querynoresults($q);
	}
	print "\n";
}

print "Successful Completion Of Batch $BATCH_ID\n";
MlpBatchJobEnd() if $BATCH_ID;

__END__

=head1 NAME

INV_get_etcpasswd.pl - import /etc/password into inventory system

=head1 SYNOPSIS

INV_get_etcpasswd.pl --BATCH_ID=s --DEBUG

=head1 DESCRIPTION

Updates INV_credential and INV_credential_attribute for credential type='/etc/passwd'

Information imported based on the command:

   .../ADMIN_SCRIPTS/rdist/run_cmd_on_all_hosts.pl -c "cat /etc/passwd"

Info is stored in INV_credential.  Data Fields from /etc/password are stored in INV_credential_attribute with the following key

	credential_name      Field_Id
	'password' 			=> 1,
	'gid' 				=> 3,
	'description' 		=> 4,
	'home directory'	=> 5,
	'dflt shell' 		=> 6

=head1 BUGS

There is not yet handling for + or other nis directives.

=cut
