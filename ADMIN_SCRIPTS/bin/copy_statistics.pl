: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use CommonFunc;
use MlpAlarm;
use File::Basename;
use Do_Time;

use vars qw( $TABLE $USER $SERVER $PASSWORD $EXCLUDE $SVSTATSFILE $MAXSIMUSERS $MAXUSERS %CONFIG
	$DEBUG $RUNUPDSTATS $DATABASE $TEMP_DIRECTORY $TOUSER $TOSERVER $OPTDIAG $TOPASSWORD $TODATABASE $NOEXEC );

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"TABLE=s"	=> \$TABLE,
		"USER=s"	=> \$USER,
		"DATABASE=s"	=> \$DATABASE,
		"EXCLUDE=s"	=> \$EXCLUDE,
		"OPTDIAG=s"	=> \$OPTDIAG,

		"PASSWORD=s" 	=> \$PASSWORD,

		"TOSERVER=s"	=> \$TOSERVER,
		"TOUSER=s"	=> \$TOUSER,
		"TODATABASE=s"	=> \$TODATABASE,
		"TOPASSWORD=s" 	=> \$TOPASSWORD,
		"SVSTATSFILE=s" => \$SVSTATSFILE,
		"NOEXEC"      	=> \$NOEXEC,

		#"TYPE=s" 	=> \$TYPE,
		#"RUNUPDSTATS" 	=> \$RUNUPDSTATS,
		#"STATSTYPE=s"	=> \$STATSTYPE,
		"TEMP_DIRECTORY=s" 	=> \$TEMP_DIRECTORY,
		"DEBUG"      	=> \$DEBUG );

die ("Must pass server\n" ) 				unless defined $SERVER;
die ("Must pass username\n" )	  			unless defined $USER;
die ("Must pass password\n" )	  			unless defined $PASSWORD;

die ("Must pass database\n" )	  			unless defined $DATABASE;
$TEMP_DIRECTORY=get_tempdir() unless $TEMP_DIRECTORY;
die ("Must pass temporary directory\n" ) 			unless defined $TEMP_DIRECTORY;
die ("Must $TEMP_DIRECTORY not a directory\n" ) 		unless -d $TEMP_DIRECTORY;

print "copy_statistics.pl : run at ".localtime(time)."\n\n";my(%excludes);

if( $EXCLUDE ) {
	foreach ( split(/[,\|]/,$EXCLUDE) ) {
		$excludes{$_}=1;
		print "Post Processing Will Exclude $_\n";
	}
}

my($DOIMPORT);
if( defined $TOSERVER  ) {
	$TOUSER=$USER		unless defined  $TOUSER;
	$TODATABASE=$DATABASE	unless defined  $TODATABASE;
	$TOPASSWORD=$PASSWORD	unless defined  $TOPASSWORD;
	$DOIMPORT=1;
}

if( ! $TABLE ) {
print "Fetching Additional Excludes\n";
my($cmd)= "$^X $curdir/query_compare.pl --SRC_USER=$TOUSER --SRC_SERVER=$TOSERVER -SRC_PASSWORD=$TOPASSWORD -SRC_DATABASE=$TODATABASE -TGT_USER=$USER -TGT_SERVER=$SERVER -TGT_PASSWORD=$PASSWORD -TGT_DATABASE=$DATABASE -SRC_QUERY=\"select name from sysobjects where type='U'\"";

print "Running $cmd\n";
open( RUN, $cmd." |" ) or die "cant run $cmd $!\n";
while (<RUN>) {
	if( /^>>/ ) {
		chomp;
		s/\s+$//;
		s/^>>//g;
		print "Adding $_ To The Exclude List\n" unless $excludes{$_};
		$excludes{$_}=1;
	}
}
close(RUN);
}

my(%time_taken);

#$TYPE="Sybase" unless defined $TYPE;
my($query);
my $starttime;
#if( defined $RUNUPDSTATS ) {
#	die "RUN STATS (--RUNUPDSTATS) NOT AVAILABLE\n";
#
#	foreach my $tbl ( split(/,/,$TABLE )) {
#		$query="isql -U$USER -P$PASSWORD -S$SERVER << EOF\nuse $DATABASE\n";
#		$query.="select convert(varchar(20),getdate())+' upd stats $tbl'\ngo\n"
#			if defined $DEBUG;
#		$query.="update $STATSTYPE statistics $tbl\ngo\n";
#		$query.="select convert(varchar(20),getdate())+' completed'\ngo\n"
#			if defined $DEBUG;
#		$starttime=time;
#		print "Running Update $STATSTYPE Statistics $tbl\n";
#		my(@rc)=run_cmd( -cmd=>$query, -printcmd=>$DEBUG, -printres=>1 );
#		$time_taken{"UPDSTATS\t$tbl"}=time - $starttime;
#	}
#}

$OPTDIAG="optdiag" unless $OPTDIAG;
my(%time_taken);

$starttime=time;
if( ! defined $TABLE ) {
	my($ff)=$TEMP_DIRECTORY."/$DATABASE.dat";
	my($tf)=$TEMP_DIRECTORY."/".$DATABASE.".dat2";

	if( $SVSTATSFILE ) {
		$ff = $SVSTATSFILE;
		$tf = $SVSTATSFILE.".tmp";
	}
	unlink $ff if -r "$ff";
	unlink $tf if -r "$tf";

	$starttime=time;
	$query="$OPTDIAG binary statistics $DATABASE -o$ff -U$USER -P$PASSWORD -S$SERVER";

	print "\nCopying $SERVER Stats For $DATABASE\n\tinto $ff\n";
	my(@rc)=run_cmd( -cmd=>$query, -printcmd=>$DEBUG, -printres=>1 );

	if( $DOIMPORT ) {
		print "Changing $SERVER to $TOSERVER in file\n";
		print "Eliminating Tables $EXCLUDE\n" if $EXCLUDE;
		# first thing we need do is overwrite the Server name:
		open( RD, $ff ) or die "Cant Read $ff";
		open( WR, ">$tf" ) or die "Cant Read $tf";
		my($in_exclude)=0;
		while (<RD> ) {
			if( /^Server name:/ ) {
				s/$SERVER/$TOSERVER/g;
			}
			if( $EXCLUDE and /^Table name:/ ) {
				$in_exclude=0;
				my($x)=$_;
				$x=~s/^Table name:\s+//;
				$x=~s/\"//g;
				$x=~s/\s//g;
				if( $excludes{$x} ) {
					$in_exclude=1;
					print "Excluding Table $x\n";
				}
			}
			print WR $_ unless $in_exclude;
		}
		close(RD);
		close(WR);
		unlink $ff;
		rename  $tf, $ff 	or warn "Cant rename $tf to $ff";
		print "Copying Stats Into $TOSERVER $DATABASE\n\tfrom $ff\n";
		$query= "$OPTDIAG binary statistics  -i$ff -U$TOUSER -P$TOPASSWORD -S$TOSERVER";
		if( $DEBUG ) {
			$query.=" -T6";
		} else {
			$query.=" -T1";
		}

		print "(debug) running $query\n" if $DEBUG or $NOEXEC;
		my(@rc)=run_cmd( -cmd=>$query, -printcmd=>$DEBUG ) unless $NOEXEC;
		print "  Command completed at ".localtime(time)."\n" ;
		my($failed)=0;
		foreach (@rc) {
			print "<msg>".$_,"\n";
			next if /^Server Message:/ and /Msg 0,/;
			next if /^Configuration option changed. The SQL/;
			next if /^Changing the value of 'allow upd/;
			next if /^Adaptive Server Enterprise/;
			next if /^\s*$/;
			next if /^Server name:/;
			chomp;
			chomp;
			$failed=1 if /connection failed./;
			print ">>".$_,"\n";
		}
print "AT LINE ",__LINE__,"\n";
		if( ! $failed and ! $SVSTATSFILE ) {
print "UNLINKING AT LINE ",__LINE__,"\n";
			unlink $ff if -r "$ff";
			unlink $tf if -r "$tf";
		}
		print "  Output saved in $ff\n" if $SVSTATSFILE;
print "AT LINE ",__LINE__,"\n" if -r $SVSTATSFILE;
	} else {
print "AT LINE ",__LINE__,"\n";
		print "  Output saved in $ff\n";
	}

print "AT LINE ",__LINE__,"\n";
	$time_taken{$DATABASE}=time - $starttime;

} else {
print "AT LINE ",__LINE__,"\n";
	foreach my $tbl ( split( /[,\|]/, $TABLE )) {
		my($ff)=$TEMP_DIRECTORY."/".$tbl.".dat";
		my($tf)=$TEMP_DIRECTORY."/".$tbl.".dat2";
		if( $SVSTATSFILE ) {
			$ff = $SVSTATSFILE;
			$tf = $SVSTATSFILE.".tmp";
		}
		unlink $ff if -r "$ff";
		unlink $tf if -r "$tf";

		$starttime=time;
		$query="$OPTDIAG binary statistics $DATABASE..$tbl -o$ff -U$USER -P$PASSWORD -S$SERVER";

		print "  Copying Out Stats For $tbl\n";
		my(@rc)=run_cmd( -cmd=>$query, -printcmd=>1, -printres=>1 );

		if( $DOIMPORT ) {
			print "  Changing $SERVER to $TOSERVER in optdiag output file\n";
			# first thing we need do is overwrite the Server name:
			open( RD, $ff ) or die "Cant Read $ff";
			open( WR, ">$tf" ) or die "Cant Read $tf";
			while (<RD> ) {
				if( /^Server name:/ ) {
					s/$SERVER/$TOSERVER/g;
				}
				print WR $_;
			}
			close(RD);
			close(WR);
			unlink $ff;
			rename  $tf, $ff 	or warn "Cant rename $tf to $ff";
			print "  Copying In Stats For $tbl\n";
			$query= "$OPTDIAG binary statistics -i$ff ";
			if( $DEBUG ) {
				$query.=" -T6";
			} else {
				$query.=" -T1";
			}
			$query.= " -U$TOUSER -P$TOPASSWORD -S$TOSERVER";

			my(@rc)=run_cmd( -cmd=>$query,  -printcmd=>1, -printres=>1 );
			print "  Command completed at ".localtime(time)."\n" ;
			foreach (@rc) {
				print "<msg>".$_,"\n";
				next if /^Server Message:/ and /Msg 0,/;
				next if /^Configuration option changed. The SQL/;
				next if /^Changing the value of 'allow upd/;
				next if /^Adaptive Server Enterprise/;
				next if /Msg 5806, Level 16/;
				next if /^\s*$/;
				next if /^Server name:/;
				chomp;
				chomp;
				print ">>".$_,"\n";
			}
			# print $query,"\n";
			# system("optdiag binary statistics -i$ff -U$TOUSER -P$TOPASSWORD -S$TOSERVER");
			if( ! $SVSTATSFILE ) {
				unlink $ff if -r "$ff";
				unlink $tf if -r "$tf";
			}
			print "  Output saved in $ff\n" if $SVSTATSFILE;
		} else {
			print "  Output saved in $ff\n";
		}
		$time_taken{$tbl}=time - $starttime;
	}
}

print "$SVSTATSFILE FILE EXISTS AT LINE ",__LINE__,"\n" if -r $SVSTATSFILE;
print "$SVSTATSFILE FILE NOT EXISTS AT LINE ",__LINE__,"\n" unless -r $SVSTATSFILE;

printf  "\n%30s %s\n","TABLE","TIME TAKEN";
foreach ( sort keys %time_taken ) {
	printf "%30s %s\n",$_,do_diff($time_taken{$_});
}
print "copy_statistics.pl : finished at ".localtime(time)."\n";
exit(0);

sub run_cmd
{
	my(%OPT)=@_;
	my(@rc);
	chomp $OPT{-cmd};

	#if( 1 or defined $OPT{-printcmd} ) {
	   	my $cmd_no_pass=$OPT{-cmd};
	   	$cmd_no_pass=~s/-P\w+/-PXXX/;
	   	print "(".localtime(time).") ".$cmd_no_pass."\n";
	#}
	$OPT{-cmd} .= " 2>&1 |";

	open( SYS2STD,$OPT{-cmd} ) || die("Unable To Run $OPT{-cmd} : $!\n");
	while ( <SYS2STD> ) {
		chop;
		next if /Msg 0, Level 10, State 1/
			or /Changing the value of 'allow updates to system tables' does not increase/
			or /To prevent this rename, move, or delete old configuration files/
			or /Msg 5806, Level 16, State 1/
			or /Configuration option changed. The SQL Server need not be rebooted since the/
			or /^\s*$/
			or /this rename, move, or delete old configuration files. See System Administration Guide/;
		push @rc, $_;
	}
	close(SYS2STD);

	my($cmd_rc)=$?;
	if( $cmd_rc != 0 ) {
		if( defined $OPT{-err} ) {
			print "completed (failed) return_code=$cmd_rc\n";
			my($str)="Error: ".$OPT{-err}."\n";
			$str.="Return Code: $?\n";
			$str.="Command: ".$OPT{-cmd}."\n";
			$str.="\n\nResults:\n";
			die( $str."\n-- ".join("\n-- ",@rc)."\n");
		} elsif( defined $OPT{-printres} ) {
			print "Return Code:$?\n";
			print "Results:\n-- ".join("\n-- ",@rc)."\n";
		}
		unshift @rc,"Error: Return Code $?";
		return @rc;
	} else {
		print "  Command completed at ".localtime(time)."\n"
			if defined $OPT{-printres};
		print "-- ".join("\n-- ",@rc)."\n" if defined $OPT{-printres} and $#rc>=0;
		return @rc;
	}
}
__END__

=head1 NAME

copy_statistics.pl - Copy optdiag stats around

=head2 USAGE

	copy_statistics.pl.pl -UUSER -SSERVER -PPASS -nmax_num_users

=head2 EXAMPLE

/usr/local/bin/perl-5.6.1 /apps/sybmon/dev/ADMIN_SCRIPTS/bin/copy_statistics.pl
	-SERVER=SYB1 -USER=sa -PASSWORD=xxx -DATABASE=clientmlp --TOSERVER=SYB2
	--TABLE=hts_exec_control,hts_ord_control,hts_exec_adj

Copies stats for the 3 three listed tables from SYB1 to SYB2

=cut


