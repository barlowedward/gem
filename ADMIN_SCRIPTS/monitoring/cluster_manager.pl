#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#GENERIC REPOSITORY RUN

# Copyright (c) 2009-2011 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use Sys::Hostname;
use Repository;
use CommonFunc;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $BATCH_ID $DEBUG $INITIALIZE);
my($MODIFIED)=0;

sub usage
{
	print @_;

	print "$PROGRAM_NAME Version $VERSION\n\n";
	print @_;
	print "SYNTAX:\n";
	print $PROGRAM_NAME." -BATCH_ID=BATCH_ID -INITIALIZE -DEBUG";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("Bad Parameter List\n") unless GetOptions(
		"INITIALIZE"    	=> \$INITIALIZE,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

my(@clusterheader,@serverlist);
my(%ServerName,%ActiveSystemName,%SharedDrive,%ClusterNode);
my(%GemRegisteredSystems);

my($dir)=get_conf_dir();
my($file)=get_conf_dir()."/win32_clusternodes.dat";

die "no $file found" 					unless -e $file;
die "cant read $file" 					unless -r $file;
die "cant write $file" 					unless -w $file;

my($STARTTIME)=time;
repository_parse_and_run(
	PROGRAM_NAME	=> $PROGRAM_NAME,
	AGENT_TYPE		=> 'Gem Monitor',
	DOALL				=> 'sqlsvr' ,
	BATCH_ID			=> $BATCH_ID ,
	NOSTDOUT			=> 1,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

if( $INITIALIZE or $MODIFIED ) {
	print "MODIFYING $file\n";
	open(CLUSTERS,">$file") or die "Cant write $file $!\n";
	print CLUSTERS join("\n",@clusterheader);
	print CLUSTERS "\n";
	foreach (sort @serverlist) {
		print CLUSTERS $_,"\n";
		print CLUSTERS "\tServerName:       ",$ServerName{$_},"\n";
		print CLUSTERS "\tActiveSystemName: ",$ActiveSystemName{$_},"\n";
		print CLUSTERS "\tSharedDrive:      ",$SharedDrive{$_},"\n";
		print CLUSTERS "\tClusterNode:      ",$ClusterNode{$_},"\n";
	}
	close(CLUSTERS);
} else {
	print "NO CHANGES FOUND - NOT MODIFYING $file\n";
}
exit(0);

sub init {
	#warn "INIT!\n";
	print 	"$PROGRAM_NAME version $VERSION run on ",hostname(),"\n\n";
	die	 	"Can not run cluster_manager.pl on unix\n" unless is_nt();
	print 	"Batch Id=$BATCH_ID\n" 				if defined $BATCH_ID;
	print 	"Run in debug mode\n" 	if defined $DEBUG;


	open( C1,$file ) or die "Cant read $file $!\n";
	my($cursvr);
	while(<C1>) {
		chop;chomp;
		push @clusterheader,$_ if /^#/;
		next if /^#/;
		if( ! /^\s/ ) {
			push @serverlist,$_;
			$cursvr=$_;
		} else {
			s/^\s+//;
			my($a,$b)=split(/:\s+/,$_,2);
			if( /^ServerName:\s+/ ) {
				$ServerName{$cursvr}=$b;
			} elsif( /^ActiveSystemName:\s+/ ) {
				$ActiveSystemName{$cursvr}=$b;
			} elsif( /^SharedDrive:\s+/ ) {
				$SharedDrive{$cursvr}=$b;
			} elsif( /^ClusterNode:\s+/ ) {
				$ClusterNode{$cursvr}=$b;
			} else {
				die "Cant parse $_\n";
			}
		}
	}
	close(C1);

	foreach ( get_password(-type=>'win32servers') ) {
		$GemRegisteredSystems{uc($_)} = 1;
	}

	#use Data::Dumper;
	#print Dumper \%ServerName;
	#print Dumper \%$ActiveSystemName;
	#print Dumper \%SharedDrive;
	#print Dumper \%ClusterNode;
	#die "DONE";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;

	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	if( $DBTYPE eq "SQL SERVER" ) {
		next unless $INITIALIZE or $ServerName{$cursvr};		# it is a cluster
		print "   SERVER $cursvr OF TYPE $DBTYPE $PATCHLEVEL\n";

		my(@rc)=dbi_query(-db=>"master",-query=>'exec sp__metadata');
		my(%ThisServerData);
		my($found)=0;
		foreach (@rc) {
			my(@vv)=dbi_decode_row($_);
			next unless $vv[0] =~ /ClusterNode/ or $vv[0] =~ /SharedDrive/ or $vv[0] =~ /ActiveSystemName/ or $vv[0] =~ /ServerName/;
			$found++ if $vv[0] =~ /ClusterNode/ or $vv[0] =~ /SharedDrive/;
			$vv[0]=~s/\s+$//;
			$vv[1]=~s/\s+$//;
			if( defined $ThisServerData{$vv[0]} ) {
				$ThisServerData{$vv[0]} .= " ".$vv[1];
			} else {
				$ThisServerData{$vv[0]} = $vv[1];
			}
		}

		return unless $found;

		if( $DEBUG ) {
			print "ServerName                 = $ThisServerData{ServerName} \n";
			print "   Was ServerName          = $ServerName{$cursvr} \n";
			print "ActiveSystemName           = $ThisServerData{ActiveSystemName} \n";
			print "   Was ActiveSystemName    = $ActiveSystemName{$cursvr} \n";
			print "SharedDrive                = $ThisServerData{SharedDrive} \n";
			print "   Was SharedDrive         = $SharedDrive{$cursvr} \n";
			print "ClusterNode                = $ThisServerData{ClusterNode} \n";
			print "   Was ClusterNode         = $ClusterNode{$cursvr} \n";
		}
		die "No ServerName" 			unless $ThisServerData{ServerName};
		die "No ActiveSystemName" 	unless $ThisServerData{ActiveSystemName};
		die "No SharedDrive" 		unless $ThisServerData{SharedDrive};
		die "No ClusterNode" 		unless $ThisServerData{ClusterNode};

		foreach ( split(/\s+/,$ThisServerData{ClusterNode}) ) {
			if( $GemRegisteredSystems{uc($_)} ) {
				MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> $_,
					-state=> "OK",
					-message_text=> "Cluster Node $_ is Registered" ) if $BATCH_ID;
			} else {
				print "Cluster Node $_ is NOT Registered\n";
				MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> $_,
					-state=> "ERROR",
					-message_text=> "Cluster Node $_ is NOT Registered" ) if $BATCH_ID;
			}
		}

		# is this a new server
		if( ! $ServerName{$cursvr} ) {
			print "NEW SERVER $cursvr FOUND\n";
			push @serverlist,$_;
		}

		my($has_server_changed)=0;
		if( $ServerName{$cursvr} ne $ThisServerData{ServerName} ) {
			$MODIFIED=1;
			$has_server_changed=1;
			print "*** Server $cursvr has changed ServerNames!\n";
			print "***   Now $ThisServerData{ServerName} Was $ServerName{$cursvr} \n";
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "ServerName",
					-state=> "ERROR",
					-message_text=> "Cluster Has Changed ServerName - Now $ThisServerData{ServerName} Was $ServerName{$cursvr}" )
						if $BATCH_ID;
			$ServerName{$cursvr}=$ThisServerData{ServerName};
		} else {
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "ServerName",
					-state=> "OK",
					-message_text=> "Cluster Has Same Server Name" ) if $BATCH_ID;
		}

		if( $ActiveSystemName{$cursvr} ne $ThisServerData{ActiveSystemName} ) {
			$MODIFIED=1;
			$has_server_changed=1;
			print "*** Server $cursvr has changed ActiveSystemNames!\n";
			print "***   Now $ThisServerData{ActiveSystemName} Was $ActiveSystemName{$cursvr} \n";
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "ActiveSystemName",
					-state=> "ERROR",
					-message_text=> "Cluster Has Changed ActiveSystemName - Now $ThisServerData{ActiveSystemName} Was $ActiveSystemName{$cursvr}" )
						if $BATCH_ID;
			$ActiveSystemName{$cursvr}=$ThisServerData{ActiveSystemName};
		} else {
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "ActiveSystemName",
					-state=> "OK",
					-message_text=> "Cluster Has Same ActiveSystemName" ) if $BATCH_ID;
		}

		if( $SharedDrive{$cursvr} ne $ThisServerData{SharedDrive} ) {
			$MODIFIED=1;
			$has_server_changed=1;
			print "*** Server $cursvr has changed SharedDrives!\n";
			print "***   Now $ThisServerData{SharedDrive} Was $SharedDrive{$cursvr} \n";
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "SharedDrive",
					-state=> "ERROR",
					-message_text=> "Cluster Has Changed SharedDrive - Now $ThisServerData{SharedDrive} Was $SharedDrive{$cursvr}" )
						if $BATCH_ID;
			$SharedDrive{$cursvr}=$ThisServerData{SharedDrive};
		} else {
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "SharedDrive",
					-state=> "OK",
					-message_text=> "Cluster Has Same SharedDrive" ) if $BATCH_ID;
		}

		if( $ClusterNode{$cursvr} ne $ThisServerData{ClusterNode} ) {
			$MODIFIED=1;
			$has_server_changed=1;
			print "*** Server $cursvr has changed ClusterNodes!\n";
			print "***   Now $ThisServerData{ClusterNode} Was $ClusterNode{$cursvr} \n";
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "ClusterNode",
					-state=> "ERROR",
					-message_text=> "Cluster Has Changed ClusterNode - Now $ThisServerData{ClusterNode} Was $ClusterNode{$cursvr}" )
						if $BATCH_ID;
			$ClusterNode{$cursvr}=$ThisServerData{ClusterNode};
		} else {
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> $cursvr,
					-subsystem=> "ClusterNode",
					-state=> "OK",
					-message_text=> "Cluster Has Same ClusterNode" ) if $BATCH_ID;
		}

		if( $has_server_changed==1 ) {
			print "\tDetected Changes On Cluster $cursvr\n";
		} else {
			print "\tNo Detected Changes On Cluster $cursvr\n";
		}

		#	print CLUSTERS $cursvr,"\n";
		#	print CLUSTERS "\tServerName: ",$ThisServerData{ServerName},"\n";
		#	print CLUSTERS "\tActiveSystemName: ",,"\n";
		#	print CLUSTERS "\tSharedDrive: ",,"\n";
		#	print CLUSTERS "\tClusterNode: ",,"\n";

		return;
	}
}


__END__

=head1 NAME

cluster_manager.pl

=head1 SYNOPSIS

Installs win32_clusternodes.dat if --INITIALIZE set

Sets a Heartbeat when one of several cluster attributes have changed

keeps win32_clusternodes.dat in sync

=head1 SYNOPSIS
