#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Carp;
use MlpAlarm;
use CommonFunc;
use Getopt::Long;
use File::Basename;
use Do_Time;
use Time::Local;

use vars qw($OUTFILE $DEBUG $ALARMDELAYSECS $SLEEPTIME $REALARMSECS $SINGLETHREAD );

my(%month) = (
	Jan => 0,
	Feb => 1,
	Mar => 2,
	Apr => 3,
	May => 4,
	Jun => 5,
	Jul => 6,
	Aug => 7,
	Sep => 8,
	Oct => 10,
	Nov => 11,
	Dec => 12,
);

$|=1;

sub usage {
	print @_,"\n";
 	return "Usage: ",basename($0)." --ALARMDELAYSECS=num --SLEEPTIME=num [--DEBUG] [--LOGEVENTS] --REALARMSECS=num --SINGLETHREAD

The system alarms when state=down for ALARMDELAYSECS and will send another alarm every REALARMSECS.  If --SINGLETHREAD then wont start if the program is running.";
}

die usage("Bad Parameter List $!") unless
    GetOptions( 	 "DEBUG"					=>\$DEBUG,
						 "ALARMDELAYSECS=i"	=>\$ALARMDELAYSECS,
						 "SLEEPTIME=i"			=>\$SLEEPTIME,
						 "OUTFILE=s"			=>\$OUTFILE,
						 "REALARMSECS=i"		=>\$REALARMSECS,
						 "SINGLETHREAD"		=>\$SINGLETHREAD );

$SLEEPTIME=30 		  unless defined $SLEEPTIME;
$ALARMDELAYSECS=30  unless defined $ALARMDELAYSECS;
$REALARMSECS=300    unless defined $REALARMSECS;
$ALARMDELAYSECS=0   if  $SLEEPTIME==0;
$REALARMSECS=0      if 	$SLEEPTIME==0;
$OUTFILE="pong.pl.out" unless defined $OUTFILE;

my($curdir)=cd_home();
if( $SINGLETHREAD ) {
	#print "Confirming If Program IS Up\n";
	if( is_up() ) {
		print "Program Running ... Stopping (to ignore this remove $curdir/pong.pl.lck. \n" if is_interactive();
		exit(0);
	}
}
mark_up();

my($ping_interval)=300;
my($lastpingtime)=time;
MlpBatchJobStart(-BATCH_ID=>"pong.pl");

# read  the ignore list
my(%server_list);
while (<DATA>) {
	next if /^#/ or /^\s*$/;
	chomp;
	$server_list{$_}=1;
}

open(OUT,">$OUTFILE") or die "Cant write $OUTFILE: $!\n";
print "Output in $OUTFILE\n";

croak("Pongstat not found in /monitor/server/bin/pongstat")
	unless -r "/monitor/server/bin/pongstat";
croak("Pongstat not executable (/monitor/server/bin/pongstat)")
	unless -x "/monitor/server/bin/pongstat";

my(%pongs);

while(1) {
	print OUT "Running /monitor/server/bin/pongstat\n";
	open( SYSCMD, "/monitor/server/bin/pongstat |" )
			|| croak "Cant Run Pongstat $!";
	while ( <SYSCMD>) {
		chomp;
		my($system,$garbage,$status,$times) = split(/\s+/,$_,4);
		next if $system eq "Host";
		next if $system eq "------------";
		next unless defined $server_list{$system};
		my($lastup,$lastdown);
		$lastup=substr($times,0,24);
		$lastdown=substr($times,26,24);
		if( $status eq "up" ) {
			print OUT ".";
		} elsif( $lastup!~/^\s*$/ or $lastdown!~/^\s*$/ ) {
			print OUT "System $system Status=$status LASTUP=$lastup LASTDOWN=$lastdown\n";
		} else {
			print OUT "System $system Status=$status\n";
		}

		my($dat);
		if( defined $pongs{$system} ) {
			$dat = $pongs{$system};
		} else {
			my(%x);
			$dat = \%x;
		}

		# Send Heartbeat Message
		# we care about state change
		if( $status eq "down" ) {
			my($t)="";
			if( ! defined $$dat{lastup} ) {
				my($sec,$min,$hours,$mday,$mon,$year);
				if( defined $lastup and $lastup ne "" )  {
					$t=$lastup;
					$lastup =~ /^(\w+)\s+(\w+)\s+(\d+)\s+(\d+):(\d+):(\d+)\s+(\d+)$/;
					($sec,$min,$hours,$mday,$mon,$year)=($6,$5,$4,$3,$2,$7);
					$mon = $month{$mon};
					print OUT "$lastup -> timelocal($sec,$min,$hours,$mday,$mon,$year)\n";
					$$dat{lastup} = timelocal($sec,$min,$hours,$mday,$mon,$year);
				} elsif( defined $lastdown and $lastdown ne "" )  {
					$t=$lastdown;
					$lastdown =~ /^(\w+)\s+(\w+)\s+(\d+)\s+(\d+):(\d+):(\d+)\s+(\d+)$/;
					($sec,$min,$hours,$mday,$mon,$year)=($6,$5,$4,$3,$2,$7);
					$mon = $month{$mon};
					$$dat{lastup} = timelocal($sec,$min,$hours,$mday,$mon,$year);
				}
			} else {
				$t=localtime($$dat{lastup});
			}
			my($msgtxt)="Pong.pl System=$system has been DOWN since $t (".do_diff( time-$$dat{lastup}).")";
			print OUT $msgtxt,"\n";
			MlpHeartbeat ( -system=>$system, -subsystem=>"pong",
								-monitor_program=>"Pong",
								-state=>"EMERGENCY",
								# -debug=>$DEBUG,
								-message_text=>$msgtxt);
		} else {
			# print OUT "+";
			my($msgtxt)="Pong.pl System=$system is up";
			# print OUT $msgtxt,"\n";
			MlpHeartbeat ( -system=>$system, -subsystem=>"pong",
								-monitor_program=>"Pong",
								# -debug=>$DEBUG,
								-state=>"OK",
								-message_text=>$msgtxt );
		}

		# state change - this should generate an event
		if( $status ne $$dat{status} and defined $$dat{status} ) {
			$$dat{lasteventtime}=0 unless defined $$dat{lasteventtime};
			if( $status eq "down" ) {

				# dont do anything until its confirmed
				# if( time - $$dat{lasteventtime} >= $REALARMSECS
					# and time - $$dat{lastup} >= $ALARMDELAYSECS ) {
					# alarm if not alarmed
					print OUT "Sending Event : System $system has gone down - Alarming\n";
					MlpEvent (  -system=>$system, -subsystem=>"pong",
									-monitor_program=>"Pong",
									# -debug=>$DEBUG,
									-severity=>"CRITICAL",
									-message_text=>"Pong.pl System=$system has gone down");
					$$dat{lasteventtime}=time;
				# } else {
					# print OUT "State Change to $status : doesnt pass time checks\n";
					# print OUT ( time - $$dat{lasteventtime} ),
								# " < $REALARMSECS or ",
								# ( time - $$dat{lastup} ),
								# " < $ALARMDELAYSECS \n";
				# }
			} elsif( defined $$dat{status} ) {

				print OUT "State Change to up\n";
				$$dat{lasteventtime}=time;
				print OUT "Sending Event : System $system is now up\n";
				MlpEvent (  -system=>$system, -subsystem=>"pong",
								-monitor_program=>"Pong",
								# -debug=>$DEBUG,
								-severity=>"OK",
								-message_text=>"Pong.pl System=$system is now up" );

			}
		}

		$$dat{lastup}=time if $status eq "up";
		$$dat{status}   = $status;
		$pongs{$system} = $dat;
	}
	close (SYSCMD);

	last if ! defined $SLEEPTIME or $SLEEPTIME<=0;
	print OUT "\nSleeping $SLEEPTIME seconds at ".localtime(time)."\n";

	if( time - $lastpingtime > $ping_interval ) {
		$lastpingtime=time;
		MlpBatchRunning();
	}
	sleep($SLEEPTIME);

	mark_up() if $SINGLETHREAD;
}
close(OUT);

MlpBatchJobEnd();

__DATA__

infra1
infra2
infra3
bs1
bs2
bs3
bs4
bs5
bs6
bs7
bs8
bs9
jms1
# Alex Magid
kent
wayne
grayson
# Kurt Riedel
poke1
poke2
poke3
poke4
poke5
poke6
gordon
henry
thomas
falcon
percy
# Jim McTernan
massena
murat
nineteen
garrett
ney
davout
murat
# Scott Gietler
drumset
guitar1
# Bob Mcgee
kirin
sapporo
molson
amstel2
labatt
harp
tecate
corona
asahi
# Igor Tulchinsky
tulch2
tulchinsky-morg
tulch-ultra-new
tulch1
tulchtest-new
tulchtest
tulch-ultra
sim1
sim2
sim3
lauprete
# Feldshuh
feldsun1
feldsun2
feldsun3
feldsun4
feldshome
# Paul Crowley tlw dev box
crowprod1
crowprod2
crowprod3
crowprod4
crowprod5
millsrv4
# Yossi Friedman
homedepot2
homerun2
homedepot
homemade
homesick
homely
homekeeper
homerun
homeland
homebrew
# Trade Support
td
td2
# Mike Halem ( Rocket )
rktlinux1
rktlinux2
rktlinux3
rktlinux4
rktlinux5
rktlinux5
rktlinux6
rktlinux7
rktlinux8
rktlinux9
rktlinux10
rktunix1
# Steve Armatas
fiji
tahiti
bimini
bermuda
richards
hendrix
armatas
page
jett
moon
# YuanJun Miao
smny1
smny2
smny3
smny4
smlx1
