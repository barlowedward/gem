#!/usr/local/bin/perl-5.6.1
use lib qw(/apps/sybmon/dist/lib /apps/sybmon/dist/ADMIN_SCRIPTS/lib /apps/sybmon/dist/plugins /apps/sybmon/dist/cpan);

use strict;
use Data::Dumper;
use MlpAlarm;

$|=1;

print "======================================================\n";
print "Getting Categories\n";
print join("\n",MlpGetCategory()),"\n";

print "======================================================\n";
print "Getting Screens\n";
my(@screens)=MlpGetScreens();
print join("\n",@screens),"\n";

print "======================================================\n";
print "Running Each Screen\n";
foreach (@screens) {
	print "Running $_\n";
	#print MlpRunScreen( -screen_name=>$_, -debug=>1 ),"\n";
	my($x) = MlpRunScreen( -screen_name=>$_ ),"\n";
	my($rowid)=1;
	foreach (@$x) {
		print $rowid.":".ref($_).":".Dumper($_)."\n";
		$rowid++;
	}
}
