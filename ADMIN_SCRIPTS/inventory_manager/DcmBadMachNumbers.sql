use racks
go

-- select top 5 * from barlow_precompute

select distinct dcmtag=machnumber,wasptag=pp.asset_tag,machinename, plcity,plbuild, floor, row, cabinet, wasp_location, wasp_floor, wasp_row, wasp_cabinet
from 	machines m,places p, barlow_precompute pp
where  	p.plid = m.cntrycitybldgnum
and  lower(pp.system_name) =* lower(m.machinename)
and  machnumber not in ( select convert(int,asset_tag) from barlow_precompute )
and not exists ( select * from machines where machinename=m.machinename and machnumber!=m.machnumber )
and  lower(machinename) not like '%panel%'
and  lower(machinename) not like '%chasis%'
and  lower(machinename) not like '%chassis%'
and  lower(machinename) not like '%patch%'
and  lower(machinename) not like '%clear%cube%'
and  lower(machinename) not like '%lan drop%'
and  lower(machinename) not like '%no name%'
and  lower(machinename) not like '%power%'
and  lower(machinename) not like '%wire%'
and  lower(machinename) not like '%telco%'
and  lower(machinename) not like '%empty%'
and  lower(machinename) not like '%monitor%'
and  lower(machinename) not like '%shelf%'

and  lower(machinename) not like '%spare%'
and  lower(machinename) not like ''
and 	p.plcntry='US'
order by machinename,cntrycitybldgnum
go
