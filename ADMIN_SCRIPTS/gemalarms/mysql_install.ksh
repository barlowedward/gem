#!/bin/sh

PERL=/usr/local/bin/perl-5.8.2
if [ -z "$1" -o -z "$2" ]
then
	echo "USAGE $0 HOST PASSWORD OBJECT"
	exit
fi

MY_SQLFILE=$0.tmp
MY_TMPFILE=$0.tmp2
HOST=$1
LOGIN=inventorydbo
PASSWORD=$2
DATABASE=inventory
rm -f $MY_SQLFILE $MY_TMPFILE

echo create  install file $MY_SQLFILE
echo "use $DATABASE;" > $MY_SQLFILE

echo "Creating Drops"
mysql -u$LOGIN -h$HOST -p$PASSWORD -D$DATABASE --skip-column-names -e "show tables;" > $MY_TMPFILE 2>/dev/null
for i in `cat $MY_TMPFILE`
do
	echo "drop table $i;" >> $MY_SQLFILE
done

dos2unix *.tbl 1>/dev/null 2>/dev/null
cat *.tbl | sed -e "s/^go/;/" -e "s/nonclustered//g;" -e "s/clustered//g;" -e "s/gemalarms/inventory/g;" -e "/to public/d;" -e "/CONSTRAINT /d;" -e "s/directory,/directory\(512\),/" -e "s/with ALLOW_DUP_ROW//i;" -e "s/identity//ig;" -e "s/, message_text/, message_text(512)/;" >> $MY_SQLFILE


mysql -h$HOST -u$LOGIN -p$PASSWORD < mysql_install.ksh.tmp

# ls -1 *.tbl | sed -e "s/^/source /" | sed -e "s/$/;/" >> $MY_SQLFILE
# mysql -huxprocs -uinventorydbo -pinventorydbo123
# rm -f $MY_SQLFILE
