#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Repository;
use File::Basename;
use File::Copy;
use DBIFunc;
use MlpAlarm;
use Data::Dumper;
use Sys::Hostname;
use CommonFunc;
use Do_Time;
use RepositoryRun;
use RunTiming;
use RunOn;

my $GemConfig;
my( $DO_SQLSVR,$DO_SYBASE,$DO_ORACLE,$DO_MYSQL,$PRODUCTION,$NOPRODUCTION,$FIXUP_METADATA,$ROLLUP,$REPORTFILE,$ROLLUPTEST );
my( %IP,%HOSTNAME,%PORT );
$|=1;
my( $MAX_CLOCK_SKEW )=0;		# in minutes
my( $starttime )=time;

my($use_rollout_dir)="YES";	# dont use it if its not available - permitting rollout on one side of SAMBA only

sub usage
{
	print @_;
	die "console_v2.pl

Build The Console

REQUIRED ARGUMENTS
	--PROMOTE
	--ROLLUP | ROLLUPTEST
	--RUN_DBQUERIES
	--RUN_COMMANDS
	--FIXUP_METADATA
	--CLEAR_SUBDIRECTORIES

OPTIONAL ARGUMENTS
   --SYSTEM_TYPE=mysql|oracle|sqlsvr|win32servers|unix|sybase
   --PUSH    overwrite all
   --TENMINUTE 10 Minute Run
   --WEEKLY run
	--HOURLY	run hourly reports
	--DAILY	run daily reports
	--PRODUCTION only prod servers on the db reports
	--NOPRODUCTION no prod servers on the db reports
	--PRINTTIMING      : print timing information for reports
	--PROGRAMNAME=PROGRAMNAME
   --BATCHID
   --HTML
   --REPORTFILE=file   (only run one particular report)
   ";
}

# cd to the right directory
#my($curdir)=dirname($0);
#chdir $curdir or die "Cant cd to $curdir: $!\n";;

$| = 1;

use vars qw($PROGRAMNAME $PARAMETER_FILE $PUSH $FIXUP_METADATA $RUN_STANDALONE_REPORTS $RUN_COMMANDS $RUN_DBQUERIES $PROMOTE $CLEAR_SUBDIRECTORIES $TENMINUTE $WEEKLY $PRINTTIMING $BATCHID $SYSTEM_TYPE $SERVER $HTML $DEBUG $ROLLOUT_DIRECTORY $CONSOLE_DIRECTORY $HOURLY $DAILY );

GetOptions(
	"BATCHID=s"						=> \$BATCHID,
	"BATCH_ID=s"					=> \$BATCHID,
	"SYSTEM_TYPE=s"				=> \$SYSTEM_TYPE,
	"PARAMETER_FILE=s"			=> \$PARAMETER_FILE,
	"REPORTFILE=s"					=> \$REPORTFILE,
	"server=s"						=> \$SERVER,
	"system=s"						=> \$SERVER,
	"servers=s"						=> \$SERVER,
	"systems=s"						=> \$SERVER,
	"html"							=> \$HTML,
	"HOURLY"							=> \$HOURLY,
	"TENMINUTE"						=> \$TENMINUTE,
	"WEEKLY"							=> \$WEEKLY,
	"PUSH"							=> \$PUSH,
	"DAILY"							=> \$DAILY,
	"PRODUCTION"					=> \$PRODUCTION,
	"ROLLUP"							=> \$ROLLUP,
	"ROLLUPTEST"					=> \$ROLLUPTEST,
	"NOPRODUCTION"					=> \$NOPRODUCTION,
	"FIXUP_METADATA"				=> \$FIXUP_METADATA,
	"PROMOTE"						=> \$PROMOTE,
	"RUN_DBQUERIES"				=> \$RUN_DBQUERIES,
	"RUN_COMMANDS"					=> \$RUN_COMMANDS,
	"RUN_STANDALONE_REPORTS"	=> \$RUN_STANDALONE_REPORTS,
	"CLEAR_SUBDIRECTORIES"		=> \$CLEAR_SUBDIRECTORIES,
	"DEBUG"							=> \$DEBUG,
	"PROGRAMNAME"					=> \$PROGRAMNAME,
	"PRINTTIMING"					=> \$PRINTTIMING
) or usage( "syntax error\n" );

$PROGRAMNAME='console_v2.pl' unless $PROGRAMNAME;
$PRINTTIMING=1;
my($NL)="\n";
$NL="<br>\n" if $HTML;

# Standard Include Path Block
my $includes="";
foreach (@INC) {	$includes.=" -I$_" if -d $_; }

MlpBatchJobStart(-BATCH_ID=> $BATCHID) if $BATCHID;

print "Running ".basename($0).$NL;
print "  Run At ",scalar localtime,$NL;
print "  Perl is $^X",$NL;
print "  hostname is ",hostname(),$NL;
print "  user is ",$ENV{USER},$NL;
print "  --DEBUG=$DEBUG\n" if $DEBUG;
print "  --BATCHID=$BATCHID\n" if $BATCHID;
print "  --PRINTTIMING=$PRINTTIMING\n" if $PRINTTIMING;
print "  --TENMINUTE\n" if $TENMINUTE;
print "  --WEEKLY\n" if $WEEKLY;
print "  --DAILY\n" if $DAILY;
print "  --HOURLY\n" if $HOURLY;

print "  --PUSH\n" if $PUSH;
print "  --FIXUP_METADATA\n" if $FIXUP_METADATA;
print "  --ROLLUP\n" if $ROLLUP;
print "  --PROMOTE\n" if $PROMOTE;

print "  --RUN_DBQUERIES\n" if $RUN_DBQUERIES;
print "  --RUN_COMMANDS\n" if $RUN_COMMANDS;
print "  --RUN_STANDALONE_REPORTS\n" if $RUN_STANDALONE_REPORTS;
print "  --CLEAR_SUBDIRECTORIES\n" if $CLEAR_SUBDIRECTORIES;

my(@cgi_bin_dirs)=('styles','img');
my(@servertypes)=('oracle','sqlsvr','mysql','sybase','win','unix');

my($is_header_read)="FALSE";	# flag on header files being read
my( $pageheader, $pagefooter);

MlpBatchJobStep(-step=>"Start Web Page Creation...") if $BATCHID;

my($gem_root_dir)=get_gem_root_dir();
die "No Gem Root Dir" unless -d $gem_root_dir;
print "  GEM ROOT=$gem_root_dir",$NL;

my($PERLCMD)=$^X;
foreach (@INC) {
	next unless -d $_ and /$gem_root_dir/;
	$PERLCMD .= " -I$_";
}
print "  PERLCMD=$PERLCMD",$NL;

l_die("No Input Directory $gem_root_dir/data/system_information_data" )
	unless -d   "$gem_root_dir/data/system_information_data";

# my(%CONFIG)=read_configfile();

my($PROGRAM_NAME)=basename($0);
initialize();

my(@doall_server_list,$doall_server_type);
if( $^O =~ /Win32/ ) {
	$doall_server_type='sqlsvr';
} else {
	$doall_server_type='sybase';
}

if( $PRODUCTION ) {
 	push @doall_server_list, get_password(-type=>$doall_server_type,-PRODUCTION=>1);
} elsif( $NOPRODUCTION ) {
 	push @doall_server_list, get_password(-type=>$doall_server_type,-NOPRODUCTION=>1);
} else {
	push @doall_server_list, get_password(-type=>$doall_server_type);
}

run_standalone_reports() if $RUN_STANDALONE_REPORTS;

run_a_command(
		-category=>'HOURLY',
		-rptname=>"Host Uptime",
	  	-title=>"Host Uptime (from /usr/bin/rup command)",
	  	-cmd=>"/usr/bin/rup -t",
	  	-outfile=>"rup.txt",
	  	-die=>undef) if $HOURLY and -e "/usr/bin/rup" and ! is_nt();

create_interfaces_rpt() if $DAILY and $DO_SYBASE;

my(%skipfile);
if( $CLEAR_SUBDIRECTORIES ) {
	starttiming("clear subdirectories");

	print "clear_external_files()\n";
	opendir(DD,$gem_root_dir."/ADMIN_SCRIPTS/console/console_template")
		or die "Cant read Template\n";
	foreach ( grep( !/^\./, readdir(DIR) ) ) {
		$skipfile{$_}=1;
	}
	closedir(DIR);

	clear_a_directory('/gem_batchjob_logs', 	$CONSOLE_DIRECTORY);
	clear_a_directory('/cronlogs', 				$CONSOLE_DIRECTORY);
	clear_a_directory('/html_output', 			$CONSOLE_DIRECTORY);
	clear_a_directory('/depends', 				$CONSOLE_DIRECTORY);
	clear_a_directory('/backup_logs', 			$CONSOLE_DIRECTORY);

	clear_a_directory('/gem_batchjob_logs', 	$ROLLOUT_DIRECTORY) if $use_rollout_dir eq "YES";
	clear_a_directory('/cronlogs', 				$ROLLOUT_DIRECTORY) if $use_rollout_dir eq "YES";
	clear_a_directory('/html_output', 			$ROLLOUT_DIRECTORY) if $use_rollout_dir eq "YES";
	clear_a_directory('/depends', 				$ROLLOUT_DIRECTORY) if $use_rollout_dir eq "YES";
	clear_a_directory('/backup_logs', 			$ROLLOUT_DIRECTORY) if $use_rollout_dir eq "YES";
	endtiming("clear subdirectories");

}

if( $RUN_COMMANDS ) {
	print "\n";
	if( $^O =~ /Win32/ ) {
		if( $WEEKLY ) {
			starttiming("Systeminfo");
			my(@win32servers);
			if( $PRODUCTION ) {
			 	push @win32servers, get_password(-type=>"win32servers",-PRODUCTION=>1);
			} elsif( $NOPRODUCTION ) {
			 	push @win32servers, get_password(-type=>"win32servers",-NOPRODUCTION=>1);
			} else {
				push @win32servers, get_password(-type=>"win32servers");
			}

			foreach ( @win32servers ) {
			   eval {
	      		local $SIG{ALRM} = sub { die "CommandTimedout\n"; }; # \n required
					alarm(20);
	         	run_a_command(
			       	-category=>'RUN_COMMANDS',
						-rptname=>"Systeminfo $_",
			       	-nostderr=>1,
				      -cmd=> "systeminfo \/s ".$_." 2>&1",
				      -outfile=>$CONSOLE_DIRECTORY."/console_win/".$_."_systeminfo.txt" ,
				      -die=>undef);
			      alarm(0);
			   }

			   # if ( $@ ){
			   	# warn "Command systeminfo \/s $_ timed out";
			   # }
        	}
			endtiming();
		}

		starttiming("End User Database Report");
		run_a_command(
       	-category=>'RUN_COMMANDS',
			-rptname=>"End User Database Report",
	      -cmd=> "$PERLCMD $gem_root_dir/ADMIN_SCRIPTS/bin/end_user_database_report.pl --DOALL=sqlsvr" ,
	      -die=>undef);
    	endtiming();

		starttiming("DailyServerPageBuild for SQL Server");
		foreach my $srvr ( @doall_server_list ) {
			DailyServerPageBuild( $srvr, "sqlsvr", "Sql Server" );
		}
		endtiming();

	} else {

		starttiming("End User Database Report");
		run_a_command(
       	-category=>'RUN_COMMANDS',
			-rptname=>"End User Database Report",
	      -cmd=> "$PERLCMD $gem_root_dir/ADMIN_SCRIPTS/bin/end_user_database_report.pl --DOALL=sybase" ,
	      -die=>undef);
	   endtiming();

    	starttiming("DailyServerPageBuild for SYBASE");
		foreach my $ssrvr ( @doall_server_list ) {
			DailyServerPageBuild( $ssrvr, "sybase", "Sybase Server" );
		}
		endtiming();

	}
}

my(@REPORT_INFO);
if( $RUN_DBQUERIES ) {
	print "\n";
	parse_queryfiles();

	print "\n";
	my($DOALL);
	if( $^O =~ /Win32/ ) {
		$DOALL='sqlsvr' unless $SERVER;
		repository_parse_and_run(
			SERVER			=> $SERVER,
			# OUTFILE		=> $OUTFILE ,
			# USER			=> $USER ,
			# PASSWORD 		=> $PASSWORD ,

			PROGRAM_NAME	=> $PROGRAM_NAME,
			AGENT_TYPE		=> 'Gem Monitor',
			PRODUCTION		=> $PRODUCTION,
			NOPRODUCTION	=> $NOPRODUCTION,
			NO_CONNECTION_ALERTS => 1,
			NOSTDOUT			=> undef,
			DOALL				=> $DOALL ,
			BATCH_ID			=> $BATCHID ,
			DEBUG      		=> $DEBUG,
			init 				=> undef,
			server_command => \&run_db_reports
		);
		endtiming()
	} else {
		starttiming("Run Commands On Each Server");
		$DOALL='sybase' unless $SERVER;
		repository_parse_and_run(
			SERVER			=> $SERVER,
			# OUTFILE		=> $OUTFILE ,
			# USER			=> $USER ,
			# PASSWORD 		=> $PASSWORD ,

			PROGRAM_NAME	=> $PROGRAM_NAME,
			AGENT_TYPE		=> 'Gem Monitor',
			PRODUCTION		=> $PRODUCTION,
			NOPRODUCTION	=> $NOPRODUCTION,
			NOSTDOUT			=> undef,
			NO_CONNECTION_ALERTS => 1,
			DOALL				=> $DOALL,
			BATCH_ID			=> $BATCHID ,
			DEBUG      		=> $DEBUG,
			init 				=> undef,
			server_command => \&run_db_reports
		);
		endtiming();
	}
}

if( $FIXUP_METADATA ) {
	print "UPDATING METADATA\n";
	parse_queryfiles();
	foreach (@REPORT_INFO) {
		if( $_->{do_diff} eq "y" ) {
			print "RUNNING ",$_->{filename},"\n";
			_querynoresults ( "insert PER_lastintstatistics
			(  system_name, system_type, key_2, key_3, key_4, perf_key, perf_time )
			select distinct system_name, system_type, key_2, key_3, key_4, perf_key, getdate()
			from PER_intstatistics i
			where perf_key = '".$_->{performancekey}."'
			and  not exists (
				select * from PER_lastintstatistics l
				where l.system_name=i.system_name
				and	l.system_type=i.system_type
				and 	l.key_2 = i.key_2
				and	l.key_3 = i.key_3
				and	l.key_4 = i.key_4
				and   l.perf_key = i.perf_key )",0);
		} else {
			print "DD = ",$_->{do_diff}," FILE=",$_->{filename},"\n";
		}
	}
}

if( $ROLLUPTEST ) {
	print "ROLLING UP PERFORMANCE RESULTS\n";
	parse_queryfiles();
	rollup_queryfiles_test();
}

if( $ROLLUP ) {
	starttiming("rollup");
	print "ROLLING UP PERFORMANCE RESULTS\n";
	parse_queryfiles();
	rollup_queryfiles();
	endtiming();
}

if( $PROMOTE ) {
	starttiming("promote");
	if( $use_rollout_dir ne "YES" ) {
		print "CANT PROMOTE unless $ROLLOUT_DIRECTORY is available";
	} else {
		print "Copying Files\n";
		copy_a_directory( $gem_root_dir."/data/GEM_BATCHJOB_LOGS", 	$CONSOLE_DIRECTORY."/gem_batchjob_logs", 	"" );
		copy_a_directory( $gem_root_dir."/data/cronlogs", 	  			$CONSOLE_DIRECTORY."/cronlogs", 				"" );
		copy_a_directory( $gem_root_dir."/data/html_output", 			$CONSOLE_DIRECTORY."/html_output", 			"" );
		copy_a_directory( $gem_root_dir."/data/depends", 				$CONSOLE_DIRECTORY."/depends", 				".html" );
		copy_a_directory( $gem_root_dir."/data/BACKUP_LOGS", 			$CONSOLE_DIRECTORY."/backup_logs",  		"sessionlog", 1 );

		# populate the directories for the console_ reports with base stuff
		foreach ( @servertypes )  {
			my($dir)="console_".$_;
			copy_a_directory( $CONSOLE_DIRECTORY."/".$dir, 	$ROLLOUT_DIRECTORY."/".$dir, 	"" );
		}
		foreach ( 'gem_batchjob_logs','cronlogs','depends','backup_logs' ) {
			my($dir)=$_;
			copy_a_directory( $CONSOLE_DIRECTORY."/".$dir, 	$ROLLOUT_DIRECTORY."/".$dir, 	"" );
		}

		copy_a_directory( $CONSOLE_DIRECTORY."/html_output", 	$ROLLOUT_DIRECTORY."/html_output", 	"", undef, undef, 1 );
	}
}

#dbi_msg_exclude(2409);
#dbi_msg_exclude("No conversions will be done");

printtiming();
print "Successful Run",$NL;
exit(0);

sub run_db_reports {
	my($cursvr)=@_;

	# dbi_getdbh()->{LongReadLen} = 32768; # some big number

	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	#if( $DBTYPE eq "SQL SERVER" ) 	{

		print "=> SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";
		starttiming("--RUN_DBQUERIES on ".$cursvr) unless $DEBUG;

		foreach (@REPORT_INFO) {

			next if $DBTYPE eq "SQL SERVER" and $_->{server_type} ne "sqlsvr";
			next if $DBTYPE eq "SYBASE" and $_->{server_type} ne "sybase";
			my($fname)=$_->{filename};
			print "\nRunning Report File=".$fname."\n";
			my($failreason)='';
			$failreason = "Weekly Run But --WEEKLY not specified"   	if $_->{run_frequency} eq "weekly"  and ! $WEEKLY;
			$failreason = "Daily Run But --DAILY not specified"   	if $_->{run_frequency} eq "daily"  	and ! $DAILY;
			$failreason = "Hourly Run But --HOURLY not specified" 	if $_->{run_frequency} eq "hourly" 	and ! $HOURLY;
			$failreason = "15min Run But --TENMINUTE not specified" 	if $_->{run_frequency} eq "15min"  	and ! $TENMINUTE;
			$failreason = "5min Run But --TENMINUTE not specified" 	if $_->{run_frequency} eq "5min"  	and ! $TENMINUTE;
			die "Unknown Run Frequency ".$_->{run_frequency}
				unless	$_->{run_frequency} eq "daily"
				or $_->{run_frequency} eq "weekly"
				or $_->{run_frequency} eq "hourly"
				or $_->{run_frequency} eq "15min"
				or $_->{run_frequency} eq "5min";
			$failreason = "Report Not Enabled " unless	$_->{enabled} eq "y";
			$failreason = "Server Min Version ".$_->{server_min_version}." > ".$PATCHLEVEL
				 if	defined $_->{server_min_version} and $_->{server_min_version} > $PATCHLEVEL;
			$failreason = "Server Max Version ".$_->{server_max_version}." < ".$PATCHLEVEL
				 if	defined $_->{server_max_version} and $_->{server_max_version} < $PATCHLEVEL;
			$failreason = "FATAL: No Query Found" unless $_->{query};
			if( $_->{run_on_server_csv} ) {
				my($found)='N';
				foreach ( split( ',', $_->{run_on_server_csv}) ) {
					$found='Y' if $cursvr eq $_;
				}
				$failreason="server not in the run_on_server_csv list" if $found eq 'N';
			}
			if( $_->{exclude_server_csv} ) {
				my($found)='N';
				foreach ( split( ',', $_->{exclude_server_csv}) ) {
					$found='Y' if $cursvr eq $_;
				}
				$failreason="server is in exclude_server_csv list" if $found eq 'Y';
			}

			my($query)=$_->{query};
			$failreason = "FATAL: NO QUERY FOUND!!!\n" unless $query;

			if( $failreason ) {
				print " -> Skipped - $failreason\n";
				if( $failreason=~/FATAL/ ) {
					use Data::Dumper;
					print Dumper \$_;
				}
				next;
			}

			starttiming("Run ".$_->{filename}." on ".$cursvr) if $DEBUG;

			my(@dblist) = ('master');
			my($st);
			if( $_->{run_query_each_db} =~ /y/ ) {
				print "Running On Each Database\n";
				my($exclude_sys_db)=0;
				$exclude_sys_db = 1 if $_->{run_query_in_sys_db} !~ /y/;
				($st,@dblist) = dbi_parse_opt_D('%',1,$exclude_sys_db);
			}

			my($savestatsasrate)= $_->{savestatsasrate};
			my($performancetype)= $_->{performancetype};
			my($performancekey) = $_->{performancekey};
			my($debug) = $_->{debug};

			my(@colhdr,@datarows);

			my($outfile)= $_->{outfile};
			my(@colnamecsv)= split(/,/,$_->{colnamecsv}) if $_->{colnamecsv};

			open(FF,">".$CONSOLE_DIRECTORY."/console_sqlsvr/".$cursvr."_".$outfile.".html") if $outfile;
			print "Writing Output To File ",
						$CONSOLE_DIRECTORY."/console_sqlsvr/".$cursvr."_".$outfile.".html",
						"\n" if $outfile;
			print FF pagehead($_->{report_header});
			my($numcols)= -2;
			foreach my $db ( @dblist ) {
				print "\nWorking On Database $db\n" if $#dblist>0;

				my(@rc) = dbi_query( -db=>$db, -print_hdr=>1, -query=> $query );
				if( $rc[0] =~ /^42000:/ ) {
					warn "$query\nBAD RETURN CODE $rc[0]";
					last;
				}

				my($rowcnt)=$#rc;
				$rowcnt++;
				print "  Retrieved $rowcnt Rows\n";
				next if $rowcnt==0;
				# if( $debug or $DEBUG ) {
					foreach (@rc) {
						my(@x)=dbi_decode_row($_);
						die "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\nFilename=$fname\nDatabase=$db\n".
							 "$query\nBAD RETURN CODE $x[0]" if $x[0] =~ /^42000:/;
						$numcols=$#x if $numcols == -2;

						print "(dbg) $db -> ",join(" | ",@x),"\n" if $debug or $DEBUG;

						# only diagnose the data if --DEBUG is on
						if( $#x != $numcols ) {
							use Data::Dumper;
							warn "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n$fname\n$query\n";
							warn Dumper \@rc;
							die "WOAH: Numcols should be $numcols But is $#x";
						}
					}
					print "NumCols=$numcols\n"  if $rowcnt and ($debug or $DEBUG);
				# }

				my($hrow)=shift @rc;
				@colhdr=dbi_decode_row($hrow);
				push @datarows,@rc;

				if( $performancekey ) {
					die "WOAH - NO PERFORMANCE TYPE!" unless $performancetype;
					# print "PERFORMACE KEY - $performancekey\n" if $DEBUG;
					# rc is servername, k1,k2,k3 v1,v2,v3...
					my($i,$j)=(0,0);
					foreach (@rc) {
						my(@x)=dbi_decode_row($_);

						if( $#colnamecsv>=0 ) {
							my(@y);
							my($i)=0;
							foreach (@colnamecsv) {
								if( lc($x[$_]) eq "x" and $i==0 ) {
									push @y,$cursvr;
								} elsif( lc($x[$_]) eq "x" and $i==1 ) {
									push @y,$db;
								} else {
									push @y,$x[$_];
								}
								$i++;
							}
							@x=@y;
							print "Reformatted Output Per Spec ",join(",",@colnamecsv),"\n";
						}

						my($q)="exec PER_save".$performancetype."stat";
						$q.="rate" if $savestatsasrate =~ /y/i;
						$q.=	" ".dbi_quote(-text=>$cursvr).",'sqlsvr',\n\t".
								dbi_quote(-text=>$x[1]).",\n\t".
								dbi_quote(-text=>$x[2]).",\n\t".
								dbi_quote(-text=>$x[3]).",\n\t".

			  					dbi_quote(-text=>$performancekey).",null,\n\t";

						my($i)=4;
						while ( defined $x[$i] ) {
							$q .= $x[$i].",\n\t";
							$i++;
						}
						$q=~s/,\n\t$/\n\t/;
						if( $DEBUG ) {
							print $q,"\n"
						} else {
					#		print "+";
					#		if( $j++ > 40 ) {	print "\n";	$j=0;	}
						}
						_querynoresults( $q, 1 );
					}
				}
			}

			if( $#datarows >= 0 ) {
				if( $performancekey ) {
					print "  Skipping Results Print - performance report ($performancekey)\n";
				} elsif( ! $outfile  )	{
					print "  Skipping Results Print (no outfile)\n";
				} else {
					use HTML::Dashboard;
					my $dash = HTML::Dashboard->new();
					$dash->set_captions( @colhdr );
					$dash->set_data_without_captions(\@datarows);
					if( $outfile ) {
						print FF $_->{report_header};
						print FF $dash->as_HTML();
						close(FF);
						print FF $_->{report_footer};
					} elsif( ! $performancekey ) {
						print $_->{report_header};
						print $dash->as_text();
						print $_->{report_footer};
					}
				}
			} else {
				print "  NO DATA FOUND FOR REPORT\n";
				if( $outfile ) {
					print FF $_->{report_header};
					print FF "<p><b>NO DATA WAS RETRIEVED FOR THIS REPORT</b>";
					print FF $_->{report_footer};
				}
			}

			#if( ! $outfile ) {
			#	print "HEADER -> ",join(" | ",@colhdr),"\n";
			#	print "DATA   -> ";
			#	use Data::Dumper;
			#	print Dumper \@datarows;
			#}

			close(FF) if $outfile;
			endtiming() if $DEBUG;
		}
		print "Reports Completed\n";
		endtiming() unless $DEBUG;

	#} else {

	#	print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";

		# get current locked/expired status from the repository
		#my($q)="select credential_name, attribute_key, attribute_value
		#	print "(noexec)" if $NOEXEC and $DEBUG;
		#	print $q,"\n" if $DEBUG;
		#	MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
		#  my(@lastresults)=MlpAlarm::_querywithresults($q,1)  unless $NOEXEC
		#  foreach (@lastresults) {
		#		my(@vals) = dbi_decode_row($_);
	#}
}

# the rules - doubledash means k=v pair and once query starts it goes
sub parse_queryfiles {
	return if $#REPORT_INFO >= 0;
	my($fromdir) = $gem_root_dir."/ADMIN_SCRIPTS/database_queries";
	print "Reading Directory $fromdir\n";
	opendir(DIR,$fromdir) or l_die("Cant Open Directory $fromdir For Listing\n");
	my(@qfiles)=grep( !/^\./, readdir(DIR) );
	closedir(DIR);

	foreach ( @qfiles ) {
		next if -d $fromdir."/".$_;
		print "  Reading File $_\n" if $DEBUG;
		next if defined $REPORTFILE and $_ ne $REPORTFILE;
		open(FF,$fromdir."/".$_) or die "Cant read $fromdir/$_\n";
		my($query)='';
		my(%attrib);
		$attrib{filename}=$_;
		my($in_query);
		while(<FF>) {
			chomp;chomp;chomp;
			if( $in_query ) {
				$query.=$_."\n";
			} elsif( /^--/ ) {
			 	/^--\s*(.+)=\s*(.+)\s*$/;

				if( $1 =~ /header/ or $1 =~ /footer/ ) {
					$attrib{lc($1)}=$2 if $1 and $2;
				} else {
					$attrib{lc($1)}=lc($2) if $1 and $2;
				}

			} elsif( /^\s*$/ ) {
			} else {
				$in_query='Y';
				$query.=$_."\n";
			}
		}
		close(FF);
		if( $DEBUG ) {
			foreach ( keys %attrib ) {
				print "    ATTRIB ",$_," => ",$attrib{$_},"\n" unless $ROLLUPTEST;
			}
		}
		$attrib{query}=$query;
		die "WOAH - NO QUERY" if $query =~ /^\s*$/;
		if( $DEBUG ) {
			print "    ATTRIB query length => ",length($attrib{query}),"\n" unless $ROLLUPTEST;;
		}
		push @REPORT_INFO, \%attrib;
		# print "    QUERY is $query\n";
	}
	print "Found ",$#REPORT_INFO," Reports\n";
}

#sub sybase_command {
#	my($cursvr)=@_;
#
#	#
#	# Get Database Type & Level
#	#
#	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
#	if( $DBTYPE eq "SQL SERVER" ) {
#		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";
#
#	} else {
#		die "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";
#
#		# get current locked/expired status from the repository
#		#my($q)="select credential_name, attribute_key, attribute_value
#		#	print "(noexec)" if $NOEXEC and $DEBUG;
#		#	print $q,"\n" if $DEBUG;
#		#	MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
#		#  my(@lastresults)=MlpAlarm::_querywithresults($q,1)  unless $NOEXEC
#		#  foreach (@lastresults) {
#		#		my(@vals) = dbi_decode_row($_);
#
#	}
#}

sub initialize {

	print "Initialization...\n";

	$GemConfig=read_gemconfig( -debug=>$DEBUG);
	$ROLLOUT_DIRECTORY = $GemConfig->{MIMI_FILENAME_WIN32};
	print "  ROLLOUT_DIRECTORY  is $ROLLOUT_DIRECTORY\n";
	if( ! -d $ROLLOUT_DIRECTORY ) {
		$use_rollout_dir = "NO";
		die 	"\nERROR: $ROLLOUT_DIRECTORY is not a directory.  \nFrom gem.dat MIMI_FILENAME_WIN32\n" if is_nt();
		# print "\nERROR: $ROLLOUT_DIRECTORY is not a directory.  \nFrom gem.dat MIMI_FILENAME_WIN32\n";
	}
	if( ! -w $ROLLOUT_DIRECTORY ) {
		die 	"\nERROR: $ROLLOUT_DIRECTORY is not writable.  \nFrom gem.dat MIMI_FILENAME_WIN32\n" if is_nt();
		# print "\nERROR: $ROLLOUT_DIRECTORY is not writable.  \nFrom gem.dat MIMI_FILENAME_WIN32\n";
	}

	$CONSOLE_DIRECTORY = $gem_root_dir."/data/CONSOLE_REPORTS";
	print "  CONSOLE_DIRECTORY is $CONSOLE_DIRECTORY\n";
	if( ! -d $CONSOLE_DIRECTORY ) {
		die "\nERROR: $CONSOLE_DIRECTORY is not a directory.  \n";
	}
	if( ! -w $CONSOLE_DIRECTORY ) {
		die "\nERROR: $CONSOLE_DIRECTORY is not writable.  \n";
	}

	foreach ( @servertypes )  {
		my($dir)="console_".$_;

		if(  $use_rollout_dir eq "YES" ) {
			mkdir( "$ROLLOUT_DIRECTORY/$dir", 0777 ) unless -d "$ROLLOUT_DIRECTORY/$dir";
		}
		mkdir( "$CONSOLE_DIRECTORY/$dir", 0777 ) unless -d "$CONSOLE_DIRECTORY/$dir";

		my($s1,$s2)=(-s $CONSOLE_DIRECTORY."/".$dir."/index.htm",-s $gem_root_dir."/ADMIN_SCRIPTS/console/console_template/index.htm");
		if( ! $s1 ) {
			print "    Recopying Directory - No $CONSOLE_DIRECTORY/$dir/index.htm \n";
			copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $CONSOLE_DIRECTORY."/".$dir);
		} elsif(  $s1 != $s2 ) {
			print "index.htm size=$s1 bytes but template=$s2 bytes (recopying)\n";
			copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $CONSOLE_DIRECTORY."/".$dir);
			($s1,$s2)=(-s $CONSOLE_DIRECTORY."/".$dir."/index.htm",-s $gem_root_dir."/ADMIN_SCRIPTS/console/console_template/index.htm");
			print "CONSOLE index.htm size=$s1 - template=$s2\n";
		}

		if(  $use_rollout_dir eq "YES" ) {
			my($s1,$s2)=(-s $ROLLOUT_DIRECTORY."/".$dir."/index.htm",-s $gem_root_dir."/ADMIN_SCRIPTS/console/console_template/index.htm");
			if( ! $s1 ) {
				print "    Recopying Directory - No $ROLLOUT_DIRECTORY/$dir/index.htm \n";
				copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $ROLLOUT_DIRECTORY."/".$dir);
			} elsif(  $s1 != $s2 ) {
				print "index.htm size=$s1 bytes but template=$s2 bytes (recopying)\n";
				copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $ROLLOUT_DIRECTORY."/".$dir);
				($s1,$s2)=(-s $ROLLOUT_DIRECTORY."/".$dir."/index.htm",-s $gem_root_dir."/ADMIN_SCRIPTS/console/console_template/index.htm");
				print "CONSOLE index.htm size=$s1 - template=$s2\n";
			}
		}
	}
	foreach ( 'gem_batchjob_logs','cronlogs','html_output','depends','backup_logs' ) {
		my($dir)=$_;
		if(  $use_rollout_dir eq "YES" ) {
			mkdir( "$ROLLOUT_DIRECTORY/$dir", 0777 ) unless -d "$ROLLOUT_DIRECTORY/$dir";
		}
		mkdir( "$CONSOLE_DIRECTORY/$dir", 0777 ) unless -d "$CONSOLE_DIRECTORY/$dir";
		my($s1,$s2)=(-s $CONSOLE_DIRECTORY."/".$dir."/index.htm",-s $gem_root_dir."/ADMIN_SCRIPTS/console/console_template/index.htm");
		if( ! $s1 ) {
			print "    Recopying Directory - No $CONSOLE_DIRECTORY/$dir/index.htm \n";
			copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $CONSOLE_DIRECTORY."/".$dir);
		} elsif(  $s1 != $s2 ) {
			print "index.htm sizes differ $s1 - $s2 (template)\n";
			copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $CONSOLE_DIRECTORY."/".$dir);
			($s1,$s2)=(-s $CONSOLE_DIRECTORY."/".$dir."/index.htm",-s $gem_root_dir."/ADMIN_SCRIPTS/console/console_template/index.htm");
			print "index.htm sizes are now $s1 - $s2 (template)\n";
		}
		if( ! $s1 ) {
			if(  $use_rollout_dir eq "YES" ) {
				print "    Recopying Directory - No $ROLLOUT_DIRECTORY/$dir/index.htm \n";
				copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $ROLLOUT_DIRECTORY."/".$dir);
			}
		} elsif(  $s1 != $s2 ) {
			print "index.htm sizes differ $s1 - $s2 (template)\n";
			copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", $ROLLOUT_DIRECTORY."/".$dir);
			($s1,$s2)=(-s $ROLLOUT_DIRECTORY."/".$dir."/index.htm",-s $gem_root_dir."/ADMIN_SCRIPTS/console/console_template/index.htm");
			print "index.htm sizes are now $s1 - $s2 (template)\n";
		}
	}

	#foreach ( @cgi_bin_dirs ) {
	#	next if -d "$ROLLOUT_DIRECTORY/gifs";
	#	mkdir( "$ROLLOUT_DIRECTORY/$_", 0777 );
	#	die "Cant Write To $ROLLOUT_DIRECTORY/$_" unless -w "$ROLLOUT_DIRECTORY/$_";
	#}
}

sub run_a_command
{
	my(@rc);
	my(%OPT)=@_;
	my($cat)="(".$OPT{-category}.")" if $OPT{-category};
	starttiming( "**> $OPT{-rptname} $cat" );
	print "   Output=".$OPT{-outfile}."\n" if defined $OPT{-outfile};
	my($cmd)=$OPT{-cmd};
	my($cmd_no_pass)=$cmd;
	if( $cmd_no_pass =~ /-PASSWORD/ ) {
		$cmd_no_pass=~s/-+PASSWORD=[\w]+/-PASSWORD=xxx/;
	} else {
		$cmd_no_pass=~s/-P\w+/-Pxxx/;
	}
	chomp $cmd_no_pass;
	print "   Command=>$cmd_no_pass\n";
	if( $OPT{-outfile} ) {
		 if( $OPT{-outfile}=~/\.txt$/ ) {
	      $OPT{-outfile} =~ s/txt$/html/;
	      $OPT{-is_txt} = 1;
	   }
	}

	my($output_text)="";
	if( defined $OPT{-outfile} ) {
		my($builtfrom)="Built from $cmd_no_pass" if $DEBUG;
		$output_text .= pagehead($OPT{-title},$OPT{-outfile},$builtfrom);
		if( defined $OPT{-is_txt} ) {
			$output_text .= "\n<PRE>";
		} else {
			$output_text .= "\n";
		}
	}

	my($len)=length $output_text;
	$cmd.= " 2>&1 " if defined $OPT{-nostderr};
	open( OUT, $cmd." |" ) or l_die("Cant Run Command $OPT{-cmd} : $!\n");
	foreach (<OUT>) {
	   chomp;
		next if defined $OPT{-exclude} and $_ =~ /$OPT{-exclude}/;
		push @rc, $_;
	   if(defined $OPT{-outfile} ) {
			$output_text .= $_."\n";
	   } else {
			print " -- ".$_."\n";
	   };
	}
	close OUT;

	$output_text.="<h2>WARNING COMMAND RETURNED NO DATA</h2>\n" if $len == length $output_text;
	$output_text .= "</PRE>" if defined $OPT{-is_txt};
	$output_text .= pagefoot() if defined $OPT{-outfile};

	if( defined $OPT{-outfile} ) {
		unlink($OPT{-outfile}) if -r $OPT{-outfile};
		print "Writing to $OPT{-outfile}\n" if $DEBUG;
	  	open( WR,"> ".$OPT{-outfile})
			      or l_die("Cant Write to File ".$OPT{-outfile}."\n");
		print WR $output_text;
		close(WR)  if defined $OPT{-outfile};
		chmod(0666,$OPT{-outfile});	# or die "Cant Chmod File $OPT{-outfile}\n";
	}

	if( ! defined $OPT{-die} or $OPT{-die}==0 ) {
	   warn("======= Warning ========\n| ($cmd_no_pass)\n| Subcommand Had A Bad Return Code ".($?/256)."\n| Command Output in ".$OPT{-outfile}."\n==============\n\n") if $? != 0 and $cmd !~ /systeminfo/;
	} else {
	   l_die("======= Fatal Error ========\n| ($cmd_no_pass)\n| Subcommand Had A Bad Return Code ".($?/256)."\n| Command Output in ".$OPT{-outfile}."\n==============\n\n") if $? != 0;
	}
	endtiming();
	return @rc;
}
#
#my(%starttime,%endtime,@tasksinorder); # try 2
#my($timing_label,$timing_starttime,%timinghash);
#
#sub starttiming {
#	($timing_label)=@_;
#	#chomp $timing_label;
#	if( $PRINTTIMING ) {
#		$starttime{$timing_label}=time;
#		push @tasksinorder,$timing_label;
#		print "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
#		print "+ Starting: ",$timing_label," at ".do_time(-fmt=>'hh:mi:ss')."\n";
#		# localtime(time)."\n";
#		print "++++++++++++++++++++++++++++++++++++++++++++++++\n";
#		$timing_starttime=time;
#	} else {
#		print "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
#		print $timing_label," at ".do_time(-fmt=>'hh:mi:ss')."\n";
#		return;
#	}
#}
#
#sub endtiming {
#	return unless $PRINTTIMING;
#	$endtime{$timing_label}=time;
#	my($fmt)=fmt_time(time,$timing_starttime);
#	chomp $timing_label;
#	$timinghash{$timing_label}=$fmt;
#	print "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
#	print "+ Completed: ",$timing_label," finished in ",$fmt,"\n";
#	print "++++++++++++++++++++++++++++++++++++++++++++++++\n\n";
#}
#
#sub printtiming {
#	return unless $PRINTTIMING;
#	print "\n========= TIMING INFORMATION ============\n";
#	my($mins,$maxe)=(time()+100000,0);
#	foreach ( sort keys %timinghash ) {
#		# my $s = localtime( $starttime{$_} );
#		# my $e = localtime( $endtime{$_}   );
#
#		$mins=$starttime{$_} if ! $mins or $mins > $starttime{$_};
#		$maxe=$endtime{$_}  if ! $maxe or $maxe < $endtime{$_} ;
#
#		# printf("%10s %20s %12s %12s\n", $timinghash{$_},$_,$s,$e);
#		printf("%10s %20s \n", $timinghash{$_},$_);
#	}
#
#	print "Ran For Time    - ",fmt_time($maxe,$mins),"\n";;
#	$starttime = localtime( $starttime );
#	$maxe = localtime( time );
#	print "Program Started - $starttime \n";
#	print "Program Ended   - $maxe \n";
#	print "========= DONE TIMING INFORMATION ============\n\n";
#}
#
#sub fmt_time
#{
#	my($endtime,$starttime)=@_;
#	my $minutes=int(($endtime-$starttime)/60);
#	return sprintf "%3s min %2s sec.", $minutes, ($endtime-$starttime-60*$minutes);
#}
#
# read header and footer
sub read_page_header_and_footer
{
	$is_header_read="TRUE";

	# could do this in one line but i forget which $? variable to set to slurp
	open( HDR, $gem_root_dir."/ADMIN_SCRIPTS/console/page_header.htm") or l_die( "cant read page_header.htm" );
	while( <HDR> ) { $pageheader .= $_; }
	close(HDR);

	open( FTR, $gem_root_dir."/ADMIN_SCRIPTS/console/page_footer.htm") or l_die( "cant read page_footer.htm" );
	while( <FTR> ) { $pagefooter.=$_; }
	close(FTR);

	my($h_m)=hostname();
	my $t_m =localtime(time);

	$pagefooter =~ s/CURRENT_HOST/$h_m/;
	$pagefooter =~ s/CURRENT_TIME/$t_m/;
}

sub pagehead
{
	my($title,$key,$notes)=@_;
	read_page_header_and_footer() if $is_header_read eq "FALSE";

	my($x)=$pageheader;
	if( defined $title ) {
		$x=~s/CURRENT_TITLE/$title/;
		$x.= "<TABLE CELLPADDING=2 CELLSPACING=2 BORDER=1 WIDTH=100%><TR><TD BGCOLOR=BEIGE ALIGN=CENTER> <h1>$title</h1> </TD> </TR></TABLE>\n";
	}
	$x.= "This report was created at ".do_time(-fmt=>'mm/dd/yyyy hh:mi:ss')." on host ". hostname()."\n";
	$x.= " (userid=$ENV{USER} " if $ENV{USER};
	$x.= " (batchid=$BATCHID) " if $BATCHID;
	$x.= "<br>".$notes if defined $notes;
	$x.= "<br>";

	return $x;
}

sub pagefoot {
	read_page_header_and_footer() if $is_header_read eq "FALSE";
	return $pagefooter;
}


sub l_die {
	my($msg)="################# FATAL ERROR #########################\n";
	my(@x)=@_;
	foreach (@x) {
		chomp;chomp;
		$msg.="# ".$_.$NL;
	}
	$msg .= "#########################################################\n";
	die $msg;
}

sub l_warning {
	my($msg)="################# NONFATAL WARNING #########################\n";
	my(@x)=@_;
	foreach (@x) {
		chomp;chomp;
		$msg.="# ".$_.$NL;
	}
	$msg .= "#########################################################\n";
	warn $msg;
	return undef;
}

my($skipped)=0;
# skip files that have 0 size or have changed recently
sub copy_a_directory {
	my($fromdir, $todir, $required_string, $recurse, $prefix, $do_amreview )=@_;
	starttiming("Copy $todir");

	print "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
	print "+ copy_a_directory\n+ FROM: $fromdir\n+ TODIR: $todir\n";
	print "+ SEARCHSTRING=$required_string\n" if $required_string;
	print "+ PREFIX=$prefix" if $prefix;
	print "+ AMREVIEW=$do_amreview" if $do_amreview;
	print "++++++++++++++++++++++++++++++++++++++++++++++++\n";

	die "Error Directory $todir Does Not Exist\n" if ! -d $todir;

	print "      Reading Directory $fromdir";
	opendir(DIR,$fromdir) or l_die("Cant Open Directory $fromdir For Listing\n");
	my(@to_copy_files)=grep( !/^\./, readdir(DIR) );
	closedir(DIR);
	my($x)=$#to_copy_files;
	$x++;
	print " - $x files\n";

	my($cnt,$unreadable)=(0,0);
	$skipped=0;
	foreach my $f ( sort ( @to_copy_files )) {
		my($longfname)=$fromdir."/".$f;
		if( -d $longfname ) {	# skip directories
			if( $recurse ) {
				$prefix.="." if $prefix;
				copy_a_directory( $fromdir."/$f" , $todir, $required_string, $recurse, $prefix.$f, $do_amreview );
				endtiming();
				return;
			}
		}
		next if $required_string and $longfname !~ /$required_string/;

		my($outfile);
		if( -r $longfname ) {	# file readable
			$cnt++;
			my($filesize)= -s $longfname;
			next if $filesize == 0;

			if( -M $longfname< .0004 and $todir !~ /$ROLLOUT_DIRECTORY/ ) {		# .0001 days = 8.6 seconds - probably good enough
				#printf "      Skipping %40s - Too New A File\n",$f;
				$skipped++;
				FileStatusMsg("Skip",$f,"Too New");
				next;
			}

			$outfile=$f;
			$outfile=~s/.txt$/.html/;
			$outfile=~s/.log$/.html/;
			$outfile=~s/.err/.html/;

			CopyWithNameChange(  $longfname,	$todir."/".$outfile, "Log File $outfile" );
		} else {
			print "Unreadable: $longfname\n";
			$unreadable++;
			next;
		}

		# if its a html_output report then set GEM_amreview
		if( $do_amreview ) {
			my($category)="Unknown";
			if( $outfile =~ /^syb/i ) {
				$category="Sybase";
			} elsif( $outfile =~ /^ase/i ) {
				$category="Sybase";
			} elsif( $outfile =~ /^mysql/i ) {
				$category="Mysql";
			} elsif( $outfile =~ /^sqlsvr/i ) {
				$category="SqlSvr";
			} elsif( $outfile =~ /^mssql/i ) {
				$category="SqlSvr";
			} elsif( $outfile =~ /^oracle/i ) {
				$category="Oracle";
			} elsif( $outfile =~ /^unix/i ) {
				$category="Unix";
			} elsif( $outfile =~ /^win/i ) {
				$category="Windows";
			} elsif( $outfile =~ /^INV_/i ) {
				$category="AssetMgt";
			} elsif( $outfile =~ /^dcm/i ) {
				$category="DCM";
			} elsif( $outfile =~ /^gem/i or $outfile =~ /^sys_/i ) {
				$category="GEM";
			} elsif( $outfile =~ /win/i ) {
				$category="Windows";
			}
			if( $category eq "Unknown" and $outfile =~ /\_/ ) {
				$outfile=~/^(.+?)\_/;
				$category=$1;
			}

			# update record
			my($query)="
if not exists ( select * from GEM_amreview where report_name='".$outfile."' )
insert GEM_amreview ( report_category, report_name, report_file )
values ( '".$category."','".$outfile."', '".$outfile."' )

update GEM_amreview set last_copy_time=getdate() where report_file='".$outfile."'";
			# print "$query\n";
			_querynoresults($query,1);
		}
	}
	$cnt-=$skipped;
	print "      Copied $cnt Files, Skipped $skipped";
	print "($unreadable were unreadable)" if $unreadable;
	print "\n";
	endtiming();
}

sub FileStatusMsg {
	my($state,$file,$status)=@_;
	printf " %-6s %-42s %-25s\n",$state,$file,$status  if $state ne "Skip" or $DEBUG;
}

# rename .txt file to .html file placing .txt files into a <PRE> block and adding a title and footer
sub CopyWithNameChange {
	my($in,$out,$title)=@_;
	chomp $in;
	chomp $out;
	chomp $out;

	my($inmin)	=	-M $in;
	my($outmin)	=	-M $out;
	$inmin *= 24*60;
	$outmin *= 24*60;

	$inmin = int($inmin);
	$outmin= int($outmin);

	if( -e $in and ! -s $in ) {
		FileStatusMsg("Skip",basename($in),"Zero Size");
		unlink $out if -e $out;
		unlink $in;
		return;
	}
	if( -e $in and -e $out and $inmin >= ( $outmin + $MAX_CLOCK_SKEW ) and ! defined $PUSH  ) {
		$skipped++;
		FileStatusMsg("Skip",basename($out),"Tgt more recent");
		#printf "      Skipping %40s - Target is more recent\n",basename($out);
		printf "         In $in : %s min\n", int($inmin) 	if $DEBUG;
		printf "        Out $out: %s min\n", int($outmin) 	if $DEBUG;
		return;
	}

#	if( -e $in and -e $out and -M $in <= -M $out ) {
#		my($i)= -M $in;
#		my($o)= -M $out;
#		#$i *= 100;
#		#$i = int($i);
#		#$i/=100;
#		#$o *= 100;
#		#$o = int($o);
#		#$o/=100;
#		printf "      --> from %30s ( %5.4f days old )\n      --> to    %30s ( %5.2f days old )\n", basename($in), $i, basename($out), $o;
#	}

	if( $in=~/\.html*$/ or $in=~/\.pl$/ or $in=~/\.jpg$/ or $in=~/\.gif$/ ) {
		# print "      Copy: ",basename($in),"\n";
		FileStatusMsg("Copy",basename($in),"");
		printf "         In $in : %s min\n", int($inmin) 	if $DEBUG;
		printf "        Out $out: %s min\n", int($outmin) 	if $DEBUG;

		unlink($out) if -w $out;
		my($rc)=copy($in,$out);
		if( ! $rc ) {
			my($dname)  = dirname($out);
			my($dexists)= -e $dname;
			my($dwrite) = -w $dname;
			l_warning("Cant copy $in to $out\n\tdir nm/ex/wr=$dname/$dexists/$dwrite\n\tErr=$!");
			return;
		}
	}  else {
		#print "      ";
		#print "CopyWithNameChange: ",basename($in),"\n";
		FileStatusMsg( "Rename", basename($in), "");

		printf "         In $in : %s min\n", int($inmin) 	if $DEBUG;
		printf "        Out $out: %s min\n", int($outmin) 	if $DEBUG;
		unlink($out) if -w $out;
		open(RD,$in) or return l_warning ("Cant read $in");
		open(WR,">".$out) or return l_warning("WARNING: $ENV{USER} on ".hostname()." Cant write $out : $!");
		print WR pagehead($title , basename($out), "The Source is the External File $in" );
		print WR "<PRE>" unless $in=~/\.htm$/ or $in=~/\.html$/;
		print WR "<FONT SIZE=+1>" if $in=~/\.htm$/ or $in=~/\.html$/;
		foreach (<RD>) { print WR $_; }
		print WR "</PRE>" unless $in=~/\.htm$/ or $in=~/\.html$/;
		print WR "<FONT SIZE=-1>" if $in=~/\.htm$/ or $in=~/\.html$/;
		print WR pagefoot();
		close(WR);
		chmod(0666,$out);
		close(RD);
	}
}

sub clear_a_directory {
	my($dir,$DIRECTORY)=@_;
	print "\tCleaning Old Files From $dir\n";
	return unless -d "$DIRECTORY/$dir";
	my($rc)=opendir(DIR,"$DIRECTORY/$dir");
	if( ! $rc ) {
		l_warning("Cant Open Directory $DIRECTORY/$dir For Listing $!\n");
		return;
	}

	my(@dlist)=grep( /\.html*$/, readdir(DIR) );
	closedir(DIR);
	foreach (@dlist) {
		next unless -M "$DIRECTORY/$dir/$_" > 2;
		next if $skipfile{$_};
		print "\tRemoving $dir/$_\n" if $DEBUG;
		unlink "$DIRECTORY/$dir/$_";
	}
}

sub DailyServerPageBuild {
	my($srvr,$srvtype,$srvlongtype)=@_;
	return unless $DAILY;

	my($root)="\\console_sqlsvr\\";
	$root='/console_sybase/' if $srvtype eq 'sybase';
	my($login,$password)   = get_password(-type=>$srvtype,-name=>$srvr);

	 my $cfgrpt="$PERLCMD ".$gem_root_dir."/ADMIN_SCRIPTS/dbi_backup_scripts/config_report.pl"
		       if -e $gem_root_dir."/ADMIN_SCRIPTS/dbi_backup_scripts/config_report.pl" ;
	 die "HMMM ".$gem_root_dir."/ADMIN_SCRIPTS/dbi_backup_scripts/config_report.pl not found\n"
			unless defined $cfgrpt;

	 if( ! -e $CONSOLE_DIRECTORY.$root.$srvr."_configuration_audit_for_dbas.html" or -M $CONSOLE_DIRECTORY.$root.$srvr."_configuration_audit_for_dbas.html" > .25 ) {
		 print "*";
		 print "FILE AGE=",-M $CONSOLE_DIRECTORY.$root.$srvr."_configuration_audit_for_dbas.html";
		 run_a_command(
		 			-category=>"RUN_COMMANDS",
			      -rptname=>"System Configuration Report for $srvr",
			      -cmd=> $cfgrpt." -SERVER=$srvr -USER=$login -PASSWORD=$password -h --NOBCP",
			      -outfile=>$CONSOLE_DIRECTORY.'/audits/'.$srvr."_configuration_audit_for_dbas.html",
			      -die=>undef);
	 } else {
	 	print "Skipping ".$CONSOLE_DIRECTORY.$root.$srvr."_configuration_audit_for_dbas.html as it is recent\n";
	 }

	 if( ! -e $CONSOLE_DIRECTORY.$root.$srvr."_users.html" or -M $CONSOLE_DIRECTORY.$root.$srvr."_users.html" > .25 ) {
		 print "*";
  		 print "FILE AGE=",-M $CONSOLE_DIRECTORY.$root.$srvr."_users.html";
		 run_a_command(
		 	-category=>"RUN_COMMANDS",
       	-rptname=>"User Report for $srvr",
	      -cmd=> "$PERLCMD $gem_root_dir/ADMIN_SCRIPTS/bin/custom_rpt.pl -S$srvr -U$login -P$password -h -R3" ,
	      -outfile=>$CONSOLE_DIRECTORY.$root.$srvr."_users.html" ,
	      -die=>undef);
	 } else {
	 	print "Skipping ".$CONSOLE_DIRECTORY.$root.$srvr."_users.html as it is recent\n";
	 }

    if( ! -e $CONSOLE_DIRECTORY.$root.$srvr."_logins.html" or -M $CONSOLE_DIRECTORY.$root.$srvr."_logins.html" > .25 ) {
    	print "*";
    	print "FILE AGE=",-M $CONSOLE_DIRECTORY.$root.$srvr."_logins.html";
		run_a_command(
			-category=>"RUN_COMMANDS",
       	-rptname=>"Login Report for $srvr",
	      -cmd=> "$PERLCMD $gem_root_dir/ADMIN_SCRIPTS/bin/custom_rpt.pl -S$srvr -U$login -P$password -h -R6" ,
	      -outfile=>$CONSOLE_DIRECTORY.$root.$srvr."_logins.html" ,
	      -die=>undef);
    } else {
    	print "Skipping ".$CONSOLE_DIRECTORY.$root.$srvr."_logins.html as it is recent\n";
    }

    if( ! -e $CONSOLE_DIRECTORY.$root.$srvr."_largetables.html" or -M $CONSOLE_DIRECTORY.$root.$srvr."_largetables.html" > .25 ) {
    	print "*";
    	print "FILE AGE=",-M $CONSOLE_DIRECTORY.$root.$srvr."_largetables.html";
		run_a_command(
			-category=>"RUN_COMMANDS",
			-rptname=>"Biggest Tables Report for $srvr",
	      -cmd=> "$PERLCMD $gem_root_dir/ADMIN_SCRIPTS/bin/biggest_table.pl -xh -S$srvr -U$login -P$password -n20 -s" ,
	      -outfile=>$CONSOLE_DIRECTORY.$root.$srvr."_largetables.html" ,
	      -die=>undef);
	} else {
		print "Skipping ".$CONSOLE_DIRECTORY.$root.$srvr."_largetables.html as it is recent\n";
	}

#	if( $BATCHID ) {
#  		if( ! -e $CONSOLE_DIRECTORY.$root.$srvr."_CheckServerDaily.html" or -M  $CONSOLE_DIRECTORY.$root.$srvr."_CheckServerDaily.html" > .25 ) {
#	    	print "*";
#			print "FILE AGE=",-M $CONSOLE_DIRECTORY.$root.$srvr."_CheckServerDaily.html";
#			#my($outfile)= $OUTDIRECTORY."/"."byserver/".$srvr."_debug.html";
#			#my($c)="$PERLCMD $gem_root_dir/ADMIN_SCRIPTS/bin/debug_one_server.pl -h -S$srvr -U$login -P$password ";
#			my($c)="$PERLCMD $gem_root_dir/ADMIN_SCRIPTS/monitoring/CheckServerDaily.pl -ERRFILE=$gem_root_dir/conf/CheckServerDaily.proposed -SERVER=$srvr -USER=$login -PASSWORD=$password --NOSTDOUT -BATCH_ID=$BATCHID";
#			#$c.= "-ADbDebug" if defined $DO_ALARM;
#			#$c.= "-ADbDebug" if defined $DO_ALARM and ! $BATCHID;
#			run_a_command(
#				   -rptname=>"CheckServerDaily: $srvr",
#				   -cmd=>$c,
#				   -outfile=>$CONSOLE_DIRECTORY.'/audits/'.$srvr."_CheckServerDaily.html",
#				   -die=>undef);
#	   	#chmod(0666,$outfile);
#	   }
#   } else {
#   	warn "WARNING : Skipping CheckServerDaily - Requires a BATCHID";
#	}
}

sub rollup_queryfiles_test {
	_querynoresults("set ansinull off",0);
	run_rollup( 'space_monitor', 'n', 'n', 1);
}

sub rollup_queryfiles {
	_querynoresults("set ansinull off",0);
	run_rollup( 'win32_diskspace', 'n', 'n', 1);
	run_rollup( 'space_monitor',   'n', 'n', 1);
	foreach (@REPORT_INFO) {
		next unless $_->{enabled} eq "y";
		print "Skipping $_->{filename} ( no performancekey) \n"  unless 	$_->{performancekey};
		next unless 	$_->{performancekey};
		print "RUNNING ",	$_->{filename},			"\n";
		print "KEY=",	$_->{performancekey},	"\n";

		#my($query)="delete PER_intsummary where perf_key='".$_->{performancekey}."'"; #"' and category='".$category."'";
		#_querynoresults($query,0);

		$_->{keep_hours} = 'n' unless $_->{keep_hours};
		$_->{keep_6}     = 'n' unless $_->{keep_6};

		run_rollup( $_->{performancekey}, $_->{keep_hours}, $_->{keep_6},3);
	}

	# set up Rollup Metadata
	starttiming("Rollup Metadata") if $PRINTTIMING;
	my($query)="delete PER_summary\n";
	_querynoresults($query,0);

	my($query)="insert PER_summary select distinct system_name, system_type, perf_key from PER_intsummary \n";
	_querynoresults($query,0);

	endtiming() if $PRINTTIMING;
}

sub run_rollup {
		my($key,$kh,$k6,$max_num_keys)=@_;

		print "run_rollup($key,$kh,$k6,$max_num_keys) \n\n";

		my($show_queries)=0;
		$show_queries=0 if $ROLLUPTEST;

		# make a control table
		starttiming("$key set rollupkeys") if $PRINTTIMING;
		my($query)="select distinct rolldate=convert(char(8),perf_time,112) into #ROLLUPKEYS from PER_intstatistics where perf_key='".$key."'\n";
		_querynoresults($query,$show_queries);
		endtiming() if $PRINTTIMING;

		starttiming("$key q1") if $PRINTTIMING;
		$query="delete PER_intsummary where perf_key='".$key."' and ( category in ( select rolldate from #ROLLUPKEYS )
		or category like '%Rollup%') \n";
		#"' and category='".$category."'";
		_querynoresults($query,$show_queries);
		endtiming() if $PRINTTIMING;

		my($dow_qry,$bucket_qry,$rollup)=("","","");
		my($dow_whr,$bucket_whr)=("","");
		if( $kh eq 'n' and $k6 eq 'n' ) {
			$rollup = 	", convert(char(8),perf_time,112)"; # to group it by day
		} elsif( $kh = "y" and $k6 eq "y" ) {
			$dow_qry   = ',datepart(dw,perf_time)';
			$bucket_qry = ',datepart(hh,perf_time)*10+datepart(mi,perf_time)/10';
		} elsif( $kh='y' ) {
			$bucket_qry = ',datepart(hh,perf_time)*10+datepart(mi,perf_time)/10';
		} else {
			$dow_qry   = ',datepart(dw,perf_time)';
		}

		$dow_whr    	= ",0" unless $dow_qry;
		$bucket_whr    = ",0" unless $bucket_qry;
		$dow_whr    	= $dow_qry    if $dow_qry;
		$bucket_whr    = $bucket_qry if $bucket_qry;

		#print "DOW=$dow_whr / $dow_qry \n";
		#print "BUC=$bucket_whr / $bucket_qry \n";

#		my($query) = "insert dbo.PER_intsummary
#(system_name  ,system_type  ,key_2        ,key_3        ,key_4
#,perf_key     ,category     ,dayofweek    ,time_bucket  ,weight
#,perf_int_1   ,perf_int_2   ,perf_int_3   ,perf_int_4   ,perf_int_5
#,perf_int_6   )
#select distinct
#   system_name, system_type, key_2, key_3, key_4
#   ,perf_key, convert(char(8),perf_time,112) $dow_whr $bucket_whr , count(*)
#   ,avg(perf_int_1)      ,avg(perf_int_2)      ,avg(perf_int_3)
#   ,avg(perf_int_4)      ,avg(perf_int_5)
#   ,avg(perf_int_6)
#from    PER_intstatistics
#where perf_key='".$key."'
#and isnull(key_4,'')!=''
#group by system_name, system_type, key_2, key_3, key_4, perf_key $dow_qry $bucket_qry $rollup
#";
		# _querynoresults($query,0) if $max_num_keys>=3;

#		$query = "insert dbo.PER_intsummary
#(system_name     ,system_type     ,key_2           ,key_3           ,key_4
#,perf_key        ,category        ,dayofweek       ,time_bucket     ,weight
#,perf_int_1      ,perf_int_2      ,perf_int_3      ,perf_int_4      ,perf_int_5
#,perf_int_6      )
#select distinct
#   system_name, system_type, key_2, key_3, 'Total'
#   ,perf_key, convert(char(8),perf_time,112) $dow_whr $bucket_whr , count(*)
#   ,avg(perf_int_1)      ,avg(perf_int_2)   ,avg(perf_int_3)
#   ,avg(perf_int_4)      ,avg(perf_int_5)   ,avg(perf_int_6)
#from    PER_intstatistics
#where perf_key='".$key."'
#and isnull(key_3,'')!=''
#group by system_name, system_type, key_2, key_3, perf_key $dow_qry $bucket_qry $rollup
#";
		# _querynoresults($query,0) if $max_num_keys>=2;

		$query = "insert dbo.PER_intsummary
(system_name     ,system_type     ,key_2           ,key_3           ,key_4
,perf_key        ,category        ,dayofweek       ,time_bucket     ,weight
,perf_int_1      ,perf_int_2      ,perf_int_3      ,perf_int_4      ,perf_int_5
,perf_int_6      )
select distinct
   system_name, system_type, key_2, 'Total', 'Total'
   ,perf_key, convert(char(8),perf_time,112) $dow_whr $bucket_whr , count(*)
   ,avg(perf_int_1)      ,avg(perf_int_2)    ,avg(perf_int_3)
   ,avg(perf_int_4)      ,avg(perf_int_5)	   ,avg(perf_int_6)
from    PER_intstatistics
where perf_key='".$key."'
and isnull(key_2,'')!=''
group by system_name, system_type, key_2, perf_key $dow_qry $bucket_qry $rollup
";
		starttiming("$key q2") if $PRINTTIMING;
		_querynoresults($query,$show_queries) if $max_num_keys>=1;

		endtiming() if $PRINTTIMING;

#$query = "insert dbo.PER_intsummary
#(system_name     ,system_type     ,key_2           ,key_3           ,key_4
#,perf_key        ,category        ,dayofweek       ,time_bucket     ,weight
#,perf_int_1      ,perf_int_2      ,perf_int_3      ,perf_int_4      ,perf_int_5
#,perf_int_6      )
#select distinct
#   system_name, system_type, 'Total', 'Total', 'Total'
#   ,perf_key, convert(char(8),perf_time,112) $dow_whr $bucket_whr , count(*)
#   ,avg(perf_int_1)      ,avg(perf_int_2)   ,avg(perf_int_3)
#   ,avg(perf_int_4)      ,avg(perf_int_5)   ,avg(perf_int_6)
#from    PER_intstatistics
#where perf_key='".$key."'
#group by system_name, system_type,perf_key $dow_qry $bucket_qry $rollup
#";

#		$query = "insert dbo.PER_intsummary
#(system_name     ,system_type     ,key_2           ,key_3           ,key_4
#,perf_key        ,category        ,dayofweek       ,time_bucket     ,weight
#,perf_int_1      ,perf_int_2      ,perf_int_3      ,perf_int_4      ,perf_int_5
#,perf_int_6      )
#select distinct
#    system_name, system_type, 'Total', 'Total', 'Total'
#   ,perf_key, convert(char(8),perf_time,112) $dow_whr $bucket_whr , count(*)
#   ,sum(perf_int_1/weight)      ,sum(perf_int_2/weight)   ,sum(perf_int_3/weight)
#   ,sum(perf_int_4/weight)      ,sum(perf_int_5/weight)   ,sum(perf_int_6/weight)
#from    PER_intstatistics
#where perf_key='".$key."'
#group by system_name, system_type,perf_key $dow_qry $bucket_qry $rollup
#";

		$query = "insert dbo.PER_intsummary
(system_name     ,system_type     ,key_2           ,key_3           ,key_4
,perf_key        ,category        ,dayofweek       ,time_bucket     ,weight
,perf_int_1      ,perf_int_2      ,perf_int_3      ,perf_int_4      ,perf_int_5
,perf_int_6      )
select distinct
    system_name, system_type, 'Total', key_3, key_4
   ,perf_key, category, dayofweek, time_bucket, sum(weight)
   ,sum(perf_int_1)      ,sum(perf_int_2)   ,sum(perf_int_3)
   ,sum(perf_int_4)      ,sum(perf_int_5)   ,sum(perf_int_6)
from    PER_intsummary
where perf_key='".$key."' and category in ( select rolldate from #ROLLUPKEYS )
group by system_name, system_type,perf_key , category, dayofweek, time_bucket
";

	starttiming("$key q3") if $PRINTTIMING;
	_querynoresults($query,$show_queries);

	endtiming() if $PRINTTIMING;

	# 30 day rollup
	my($category)=do_time(-fmt=>'yyyymmdd',-time=> (time - 30*24*60*60) );
	$query = "insert dbo.PER_intsummary
(system_name  ,system_type  ,key_2        ,key_3        ,key_4
,perf_key     ,category     ,dayofweek    ,time_bucket  ,weight
,perf_int_1   ,perf_int_2   ,perf_int_3   ,perf_int_4   ,perf_int_5
,perf_int_6   )
select distinct
   system_name, system_type, key_2, 'Total', 'Total'
   ,perf_key, '30DayRollup',dayofweek       ,time_bucket     , sum(weight)
   ,avg(perf_int_1)      ,avg(perf_int_2)   ,avg(perf_int_3)
   ,avg(perf_int_4)      ,avg(perf_int_5)   ,avg(perf_int_6)
from    PER_intsummary
where perf_key='".$key."'
and category >= '".$category."' and category not like '%Rollup%'
group by system_name, system_type, key_2, perf_key ,dayofweek       ,time_bucket
";
	starttiming("$key q4") if $PRINTTIMING;
	_querynoresults($query,$show_queries);

	endtiming() if $PRINTTIMING;

	# 90 day rollup
	my($category)=do_time(-fmt=>'yyyymmdd',-time=> (time - 30*24*60*60) );
	$query = "insert dbo.PER_intsummary
( system_name  ,system_type  ,key_2        ,key_3        ,key_4
,perf_key     ,category     ,dayofweek    ,time_bucket  ,weight
,perf_int_1   ,perf_int_2   ,perf_int_3   ,perf_int_4   ,perf_int_5
,perf_int_6 )
select distinct
   system_name, system_type, key_2, 'Total', 'Total'
   ,perf_key, '90DayRollup' ,dayofweek      ,time_bucket     , sum(weight)
   ,avg(perf_int_1)      ,avg(perf_int_2)   ,avg(perf_int_3)
   ,avg(perf_int_4)      ,avg(perf_int_5)   ,avg(perf_int_6)
from    PER_intsummary
where perf_key='".$key."'
and category >= '".$category."' and category not like '%Rollup%'
group by system_name, system_type, key_2, perf_key ,dayofweek       ,time_bucket
";
	starttiming("$key q5") if $PRINTTIMING;
	_querynoresults($query,$show_queries);
	endtiming() if $PRINTTIMING;

	starttiming("$key delete") if $PRINTTIMING;
	my($query)="delete PER_intstatistics where perf_key='".$key."' and datediff(dd,perf_time,getdate())>0\n";
	_querynoresults($query,$show_queries);
	endtiming() if $PRINTTIMING;

	_querynoresults("drop table #ROLLUPKEYS",$show_queries);
}

sub run_standalone_reports
{
	print "get_console_info(ConsoleInfo)\n";
	my(%CONSOLE_INFO)=get_console_info("ConsoleInfo");

	print "run_standalone_reports() Started at ".do_time(-fmt=>'hh:mi:ss'),"\n";

# The report key (first value) can contain W (is_nt()=1), U (!is_nt()), Y (DO_SYBASE) or S (DO_SQLSVR) or O (DO_ORACLE)
#  so Y2 means --REPORTNAME=2 and --DO_SYBASE
#
# THE NEXT 2 LINES ARE MY NEW SYNTAX USING putget_file TO FETCH & REPORT ON A SET OF FILES
#16;Backup Job Summary;KEY_PERL KEY_GEMROOT/ADMIN_SCRIPTS/console/BackupJobRunInfoRpt.pl;Backup Job Summary;BackupJobRunInfoRpt.html;

	foreach my $ConsoleKey ( keys %CONSOLE_INFO ) {
		my($hash)=$CONSOLE_INFO{$ConsoleKey};
		print "Processing $ConsoleKey : $$hash{SubMenuItem}\n" if $DEBUG;
		if( $$hash{Created_By} ne "CONSOLE_EXTERNAL" ) {
			print "\tIgnored $ConsoleKey : $$hash{SubMenuItem} as Created_By=$$hash{Created_By}...\n" if $DEBUG;
			if( ! $$hash{Created_By} ) {
				foreach ( sort keys %$hash ) {
					print "\tKEY=$_ VAL=",$$hash{$_},"\n";
				}
			}
			next;
		}
		my($rptnums)=$$hash{ReportRunId};
		my($rptname)=$$hash{SubMenuItem};
		my($cmd)=$$hash{Command};
		my($title)=$$hash{SubMenuItem};
		my($outfile)=$$hash{Actual_Filename};

		die "ERROR $ConsoleKey does not contain a Command\n" 		unless $cmd;
		die "ERROR $ConsoleKey does not contain a ReportRunId\n" unless $rptnums;
		die "OUTFILE not defined for internally configured report\n$rptname!!!(".$_.")\nRPTNUMS=$rptnums\nNAME=$rptname\nCMD=$cmd\nTITLE=$title\nOUTFILE=$outfile\n" unless $outfile;
		die "COMMAND is not Perl for internally configured report $rptname!!!(".$_.")\n" unless $cmd=~/KEY_PERL/;

#		my($OUTDIRECTORY)=$CONSOLE_DIRECTORY;

		# NEW ALGO - BASED ON RunOn.pm and Actual_Filename
		die "NO GemConfig\n"  unless defined $GemConfig;
		
		if( $^O eq 'MSWin32' ) {		# WINDOWS
			if( $outfile =~ /^Unix_/i ) {
				next;
			} elsif( $outfile =~ /^GemRpt/ or $outfile =~ /^Sys/i ) {
				next unless gemsys_is_ok_on_win32();
			} elsif( $outfile =~ /^Win/i ) {
			} elsif( $outfile =~ /^Ora/i ) {
				next unless $GemConfig->{USE_ORACLE} eq "Y";
				next unless oracle_is_ok_on_win32();
			} elsif( $outfile =~ /^SqlSvr/ or $outfile =~ /^Mssql/i ) {
				next unless $GemConfig->{USE_SQLSERVER} eq "Y";
				next unless sqlsvr_is_ok_on_win32();
			} elsif( $outfile =~ /^Syb/i ) {
				next unless sybase_is_ok_on_win32();
				next unless $GemConfig->{USE_SYBASE_ASE} eq "Y";
			} elsif( $outfile =~ /^Mysql/i ) {
				next unless mysql_is_ok_on_win32();
				next unless $GemConfig->{USE_MYSQL} eq "Y";				
			} else {
				warn "Cant determine $rptname ($rptnums) RunOn Host From $outfile\n";
				next;
			}
		} else {
			if( $outfile =~ /^Unix_/i ) {
			} elsif( $outfile =~ /^GemRpt/ or $outfile =~ /^Sys/i ) {
				next unless gemsys_is_ok_on_unix();
			} elsif( $outfile =~ /^Win/i ) {
				next;
			} elsif( $outfile =~ /^Ora/i ) {
				next unless $GemConfig->{USE_ORACLE} eq "Y";
				next unless oracle_is_ok_on_unix();
			} elsif( $outfile =~ /^SqlSvr/ or $outfile =~ /^Mssql/i ) {
				next unless $GemConfig->{USE_SQLSERVER} eq "Y";
				next unless sqlsvr_is_ok_on_unix();
			} elsif( $outfile =~ /^Syb/i ) {
				next unless $GemConfig->{USE_SYBASE_ASE} eq "Y";
				next unless sybase_is_ok_on_unix();
			} elsif( $outfile =~ /^Mysql/i ) {
				next unless mysql_is_ok_on_unix();
				next unless $GemConfig->{USE_MYSQL} eq "Y";				
			} else {
				warn "Cant determine $rptname ($rptnums) RunOn Host From $outfile\n";
				next;
			}
		}
		
#		# The report key (first value) can contain W (is_nt()=1), U (!is_nt()), Y (DO_SYBASE) or S (DO_SQLSVR)
#		if( $rptnums=~ 'W' ) {
#			print "\tSkipping $rptname ($rptnums) as you are not on win32\n" if $DEBUG and ! is_nt();
#			next unless is_nt();
#			$OUTDIRECTORY .= "/console_win32";
#			$rptnums=~s/W//;
#		}
#		if( $rptnums=~ 'U' ) {
#			print "\tSkipping $rptname ($rptnums) as you are not on unix\n" if $DEBUG and is_nt();
#			next unless ! is_nt();
#			$OUTDIRECTORY .= "/console_unix";
#			$rptnums=~s/U//;
#		}
#		if( $rptnums=~ 'Y' ) {
#			print "\tSkipping $rptname ($rptnums) as --DO_SYBASE not defined\n" if $DEBUG and ! $DO_SYBASE;
#			next unless $DO_SYBASE;
#			$OUTDIRECTORY .= "/console_sqlsvr";
#			$rptnums=~s/W//;
#		}
#		if( $rptnums=~ 'S' ) {
#			print "\tSkipping $rptname ($rptnums) as --DO_SQLSVR not defined\n" if $DEBUG and ! $DO_SQLSVR;
#			next unless $DO_SQLSVR;
#			$OUTDIRECTORY .= "/console_sqlsvr";
#			$rptnums=~s/W//;
#		}
#		if( $rptnums=~ 'O' ) {
#			print "\tSkipping $rptname ($rptnums) as --DO_ORACLE not defined\n" if $DEBUG and ! $DO_ORACLE;
#			next unless $DO_ORACLE;
#			$OUTDIRECTORY .= "/console_oracle";
#			$rptnums=~s/O//;
#		}

		#my($rptid)=$rptnums;
		#$rptid=~s/[WUYS]//g;

		#print "\tSkipping $rptname ($rptnums) as $rptid not being run\n" if $DEBUG and ! $RUN{$rptid};
		#next unless $RUN{$rptid};

		print "Processing $ConsoleKey : $$hash{SubMenuItem}\n";

		$cmd=~s/KEY_PERL/$PERLCMD/g;
		$cmd=~s/KEY_GEMROOT/$gem_root_dir/g;
#		$cmd=~s/KEY_OPTA/$DO_ALARM/g;
#		$cmd=~s/KEY_OPTO/$OUTDIRECTORY/g;
		run_a_command(  -category=>'RUN_STANDALONE_REPORTS', -rptname=>$rptname, -cmd=>$cmd, -title=>$title, 
			-outfile=>"$CONSOLE_DIRECTORY/html_output/$outfile", -die=>undef);
	}

   print "done running standalone reports at ".do_time(-fmt=>'hh:mi:ss') if defined $PRINTTIMING;
}

sub get_gemconfig {
	my $GemConfig=read_gemconfig( -debug=>$DEBUG);

	# FIGURE OUT WHAT DATABASE TO RUN ON BASED ON CONFIG FILE
	my($nativemessages)="";
	$nativemessages .=  "\nSYSTEM CONFIGURATION OPTIONS:\n";
	$nativemessages .=  "   INSTALLTYPE=".$GemConfig->{INSTALLTYPE}."\n";	# UNIX,WINDOWS,SAMBA
	$nativemessages .=  "   USE_ORACLE=".$GemConfig->{USE_ORACLE}."\n";
	$nativemessages .=  "   USE_SQLSERVER=".$GemConfig->{USE_SQLSERVER}."\n";
	$nativemessages .=  "   USE_SYBASE_ASE=".$GemConfig->{USE_SYBASE_ASE}."\n";

	$DO_SQLSVR=0;
	$DO_SYBASE=0;
	$DO_ORACLE=0;

	if( $GemConfig->{INSTALLTYPE} =~ /^WINDOWS/ ) {
		if( is_nt() ) {
			$DO_SQLSVR=1 if $GemConfig->{USE_SQLSERVER} eq "Y";
			$DO_SYBASE=1 if $GemConfig->{USE_SYBASE_ASE} eq "Y";
			$DO_ORACLE=1 if $GemConfig->{USE_ORACLE} eq "Y";
		} else {
			die "WOAH: Running A Win32 Only Installation of GEM from a UNIX Server\n";
		}
	} elsif( $GemConfig->{INSTALLTYPE} =~ /^UNIX/ ) {
		if( is_nt() ) {
			die "WOAH: Running A UNIX Only Installation of GEM from a Windows Server\n";
		} else {
			$DO_SYBASE=1 if $GemConfig->{USE_SYBASE_ASE} eq "Y";
			$DO_ORACLE=1 if $GemConfig->{USE_ORACLE} eq "Y";
		}
	} elsif( $GemConfig->{INSTALLTYPE} =~ /^SAMBA/ ) {
		if( is_nt() ) {
			$DO_SQLSVR=1 if $GemConfig->{USE_SQLSERVER} eq "Y";
			$DO_SYBASE=0;
			$DO_ORACLE=1 if $GemConfig->{USE_ORACLE} eq "Y";
		} else {
			$DO_SYBASE=1 if $GemConfig->{USE_SYBASE_ASE} eq "Y";
		}
	} else {
		# foreach ( sort keys %CONFIG ) { print "configure.cfg variable $_ -> $CONFIG{$_}\n"; }
		die "WOAH: INSTALLTYPE NOT SET ";
	}
	$nativemessages .=  "\nFINAL FLAG SETTINGS:\n";
	$nativemessages .=  "   DO_SYBASE=$DO_SYBASE\n";
	$nativemessages .=  "   DO_ORACLE=$DO_ORACLE\n";
	$nativemessages .=  "   DO_SQLSVR=$DO_SQLSVR\n";

	print $nativemessages;
}

sub create_interfaces_rpt
{
	print "\tReading Local Interfaces File\n";
	my(@interfaces_lines);
	foreach( dbi_get_interfaces() ) {
			# print "dbi_get_interfaces: ",join("~",@$_),"\n";
			if( $_->[1] =~ /\+/ and $_->[2] =~ /\+/ ) {
				my($a,$b) = split( /\+/, $_->[1] );
				my($c,$d) = split( /\+/, $_->[2] );
				if( $a eq $b and $c eq $d ) {		# dont ask - its possible...
					($HOSTNAME{$_->[0]}, $IP{$_->[0]}) = convert_to_host_ip($a);
					 $_->[1] = $a;
					 $_->[2] = $c;
				} elsif( $c eq $d ) {
					my($h,$i)  =convert_to_host_ip($a);
					my($h2,$i2)=convert_to_host_ip($b);
					if( $h eq $h2 or $i eq $i2 ) {
						 $_->[1] = $a;
						 $_->[2] = $c;
						($HOSTNAME{$_->[0]}, $IP{$_->[0]}) = ($h,$i);
					} else {
						print "\tSKIPPING TWO-LISTENER INTERFACES ENTRY $_->[1] $_->[2]\n";
						next;
					}
 				} else {
					print "\tSKIPPING TWO-LISTENER INTERFACES ENTRY $_->[1] $_->[2]\n";
					next;
 				}
				#my(@x)=split(/\+/,$_[1]);
				#foreach(@x) {
	       	#($HOSTNAME{$_->[0]}, $IP{$_->[0]}) = convert_to_host_ip($_);
				#}
	       	#printf(INTERFACES "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</tr>\n", $_->[0], $HOSTNAME{$_->[0]} , $IP{$_->[0]}, $_->[2]);

			} else {
				#print "DBG: Converting $_->[0],$_->[1]\n" if defined $DEBUG;
	       	($HOSTNAME{$_->[0]}, $IP{$_->[0]}) = convert_to_host_ip($_->[1]);
				if( ! defined $HOSTNAME{$_->[0]} ) {
					print "\tINTERFACES FILE LINE MAY HAVE AN ERROR - CANT GET HOSTNAME FROM ",$_->[1],"\n";
					print "\terror: ",join("\t",@$_),"\n";
	       		push @interfaces_lines,
	       			sprintf( "<tr BGCOLOR=RED><td>%s</td><td>%s</td><td>%s</td><td>%s</tr>\n", $_->[0], $_->[1] , $_->[2]);
					next;

				}
			}
	      push @interfaces_lines,sprintf( "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</tr>\n", $_->[0], $HOSTNAME{$_->[0]} , $IP{$_->[0]}, $_->[2]);
	      $PORT{$_->[0]} = $_->[2];
	}

	print "\tCreating interfaces.html\n";
	my($file)=$CONSOLE_DIRECTORY."/console_sybase/interfaces_".hostname().".html";
	my($rc)=open(INTERFACES,">".$file);
	if( ! $rc ) {
       print STDERR "Cant write ",$CONSOLE_DIRECTORY."/console_sybase/interfaces_".hostname().".html $!";
       return;
	}

	print INTERFACES pagehead("Server Names & Ports","interfaces.html","Built From Interfaces File"),
			"SYBASE environment variable is ",$ENV{SYBASE},"<p>",
			"\n<TABLE BORDER=1>\n";
	printf(INTERFACES "<tr><TH BGCOLOR=BEIGE>%s</th><TH BGCOLOR=BEIGE>%s</th><TH BGCOLOR=BEIGE>%s</th><TH BGCOLOR=BEIGE>%s</th></tr>",
				     "SERVER","HOSTNAME","IP ADDR","PORT");
	print INTERFACES @interfaces_lines;
	print INTERFACES "</TABLE>\n",pagefoot();
	close(INTERFACES);
	chmod(0666,$file);
}

