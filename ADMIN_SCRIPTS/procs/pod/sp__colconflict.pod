
=head1 NAME

sp__colconflict  - Analyze conflicting columns definitions in current database

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


Reports column with multiple definitions (for example, one. defined in
table A as an int and in B as a smallint).

=head2 USAGE


sp__colconflict [ @objectname ]

@objname specifies objects to select (query like "%@objname%")

=head2 SEE ALSO


sp__helpcolumn, sp__collist

sp__helpnull

=head2 SAMPLE OUTPUT


 1> exec sp__colconflict
 Column               Table                Defn            Null
 -------------------- -------------------- --------------- --------
 attribute            schedule_history     char(30)        null
 attribute            schedule_attributes  varchar(127)    null
 description          error_severity       char(18)        null
 description          system               char(255)       null
 description          disks                char(30)        null
 description          user_view            char(30)        null
 description          remarks              varchar(127)    null
 description          hardware             varchar(127)    null
 group_name           schedule_defn        char(18)        not null
 group_name           schedule_groups      char(18)        not null
 group_name           comn_sysusers        char(30)        not null
 group_name           model                char(30)        null
