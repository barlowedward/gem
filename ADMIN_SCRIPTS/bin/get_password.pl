#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2006 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "Usage: get_password.pl -SERVER=SERVER -TYPE=servertype -ACTION=LOGIN|PASSWORD|DIRECTORY]\n";
}

use strict;
use Getopt::Long;
use Repository;

use vars qw($SERVER $TYPE $ACTION);

die "Bad Parameter List $!\n" unless
   GetOptions(  "SERVER=s"	=> \$SERVER,
		"ACTION=s"	=> \$ACTION,
		"TYPE=s"	=> \$TYPE );

if( $ACTION eq "DIRECTORY" ) {
	print get_gem_root_dir(),"\n";
	exit(0);
}
die "MUST PASS SERVERNAME or ACTION=DIRECTORY" unless $SERVER;
die "ACTION = DIRECTORY|LOGIN|PASSWORD\n" unless $ACTION eq "LOGIN" or $ACTION eq "PASSWORD";
my($LOGIN,$PASSWORD);

if( ! $TYPE or $TYPE eq "sybase" ) {
	($LOGIN,$PASSWORD)=get_password(-type=>"sybase",-name=>$SERVER);
}
if( ! $TYPE or $TYPE eq "sqlsvr" ) {
	($LOGIN,$PASSWORD)=get_password(-type=>"sqlsvr",-name=>$SERVER) 			unless $LOGIN;
}
if( ! $TYPE or $TYPE eq "oracle" ) {
	($LOGIN,$PASSWORD)=get_password(-type=>"oracle",-name=>$SERVER) 			unless $LOGIN;
}
if( ! $TYPE or $TYPE eq "unix" ) {
	($LOGIN,$PASSWORD)=get_password(-type=>"unix",-name=>$SERVER) 				unless $LOGIN;
}
if( ! $TYPE or $TYPE eq "win32servers" ) {
	($LOGIN,$PASSWORD)=get_password(-type=>"win32servers",-name=>$SERVER) 	unless $LOGIN;
}
die "CANT FIND LOGIN FOR SERVER $SERVER\n" unless $LOGIN;

print $LOGIN."\n" 		if $LOGIN and $ACTION eq "LOGIN";
print $PASSWORD."\n" 	if $LOGIN and $ACTION eq "PASSWORD";
exit(0);

