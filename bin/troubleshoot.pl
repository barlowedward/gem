#!/usr/local/bin/perl-5.6.1

use strict;
use Carp;
use vars qw( $TESTSQLSVR $TESTDATABASE $TESTUNIX $TESTALL );
my(%starttime,%endtime,%status);
use vars qw( $TESTSQLSVR $TESTDATABASE $TESTUNIX $DEBUG $NOPAUSE );

BEGIN {
	use Getopt::Long;
	die "Bad Parameter List $!\nAllowed arguments to are --TESTSQLSERVER --TESTDATABASE --TESTUNIX\nor --TESTALL" unless
   GetOptions(  "TESTSQLSVR"	=> \$TESTSQLSVR,
		"TESTDATABASE"	=> \$TESTDATABASE,
		"TESTALL"	=> \$TESTALL,
		"NOPAUSE"	=> \$NOPAUSE,
		"DEBUG"		=> \$DEBUG,
		"TESTUNIX"	=> \$TESTUNIX	);

	$starttime{"BEGIN BLOCK"}=time;
	print "troubleshoot.pl - Generic Enterprise Manager troubleshooter\n";
	print "\nSTARTING MODULE TESTS\n";
	select STDOUT;$|=1;

	print "   Your perl version is $]\n";
   	print "   Your Perl Library Include Path is\n\t",join("\n\t",@INC),"\n";

	if( $] < 5.008001 and defined $ENV{PROCESSOR_LEVEL} ) {
		if( defined $^V and $^V!~/\s*$/) {
			die "Error : You must have perl 5.8.1 installed under Win32.  \nYou are running $^V \n";
		}
		my($x)=$];
		$x=~s/0+/\./g;
		$x=~s/\.\.+/\./g;
		die "Error : You must have perl 5.8.1 installed.  \nYou are running $x ($])\n";
	}

	if( ! defined $ENV{DISPLAY} and ! defined $ENV{PROCESSOR_LEVEL} ) {
		die "Error : DISPLAY environment variable not set!\n";
	}

	my(%badpackage)=();
	sub testit {
		my($pkg_arryref, $msgbad, $msgok)=@_;
		my $err;
		my $numbad=0;
		foreach my $str (@$pkg_arryref) {

			print "Testing $str\n" if $DEBUG;
			if ( eval "require $str" ) {
				$str->import();
			} else {
				my $err = $@;
				chomp $err;
				$numbad++;
				print "--$str Results $err\n" if $DEBUG;
				$badpackage{$str}=$err;
			}
		}

		if ($numbad ) {
			print $msgbad;
			foreach ( keys %badpackage) {
				print "Package Did Not Load: $_ \n";
				if( defined $ENV{PROCESSOR_LEVEL} ) {
					print "\tplease run: ppm install DBI\n"	if $_ eq "DBI";
					print "\tplease run: ppm install DBD-ODBC\n" if $_ eq "DBD::ODBC";

					if(  $]<5.008 ) {
						print "\tplease run: ppm install Win32-TaskScheduler --location=http://taskscheduler.sourceforge.net/perl/\n"
							if $_ eq "Win32::TaskScheduler";
					} else {
						print "\tplease run: ppm install http://taskscheduler.sourceforge.net/perl58/Win32-TaskScheduler.ppd\n"
							if $_ eq "Win32::TaskScheduler";
					}
				}

				foreach ( split( /\n/,$badpackage{$_} )) {
					next if /^Can't locate /;
					print "\t=> $_\n";
				}
			}

			print "\nThe package(s) listed above MUST be installed before you may proceed.\n\n";
			exit(1);
		} else {
			print $msgok;
		}
	}

	my @std_packages=qw(CGI Getopt::Std Carp File::Copy File::Basename DBM::Deep
			Sys::Hostname Net::FTP Data::Dumper File::Find File::Path LWP::Simple
			Socket Tk URI::Escape Log::Dispatch Log::Dispatch::Screen
			Log::Dispatch::File Fcntl Getopt::Long Data::Dumper );
	my @dbi_packages=qw(DBI);
	if( defined $ENV{PROCESSOR_LEVEL}) {
		push @dbi_packages,"DBD::ODBC";
	} else {
		push @dbi_packages,"DBD::Sybase";
	}

	my @tk_packages=qw(Tk::HList Tk::Tree Tk::DialogBox Tk::ItemStyle Tk::NoteBook Tk::BrowseEntry Tk::Listbox
				Tk::ErrorDialog Tk::Menu Tk::ROText Tk::Scrollbar Tk::Pixmap Tk::Label Tk::Balloon );

	if( defined $ENV{PROCESSOR_LEVEL}) {
		push @dbi_packages,"Win32::TaskScheduler";
	}

	my @gem_packages=qw(ConnectionManager CommonFunc);
	my @my_packages=qw(Net::myFTP Net::myRSH Repository DBIFunc Do_Time MlpAlarm CommonFunc Do_Time RunCommand GemData LogFunc RosettaStone);

	print "   Testing Perl Builtin Package Integrity\n      ";
	testit( \@std_packages,
		"\nERROR: At least one pre-requisite required package was not found!\nThese are required parts of perl and must be available\nContact your system administrator\n",
		"   OK: All Required Builtin Perl Packages Found!!\n" );

	print "   Testing Perl Database Connectivity Package Integrity\n      ";
	testit( \@dbi_packages,
		"\nERROR: Database Tests FAILED\n",
		"   OK: All Required Perl/DBI Packages Found!!\n" );

	if( ! defined $badpackage{DBI}) {
		if( $DBI::VERSION < 1.0 ) {
			print "   ERROR - DBI Version ($DBI::VERSION) Must be at 1.0 or later";
		} else {
			print "         OK: DBI Version is ($DBI::VERSION)\n";
		}
	}

	if( defined $ENV{PROCESSOR_LEVEL}) {
		warn "   DBD::ODBC Version ($DBD::ODBC::VERSION) Should be at 1.0 or later"
			if $DBD::ODBC::VERSION < 1.0;
		print "         OK: DBD::ODBC Version is ($DBD::ODBC::VERSION)\n";
	} else {
	   if( $DBD::Sybase::VERSION < 1.0 ) {
		   print "\n   ***************************************\n";
		   print "   * WARNING - DBD::Sybase Version ($DBD::Sybase::VERSION) Should Probably be at 1.0 or later\n";
		   print "   * This will not stop your install but a more stable version is better\n";
		   print "   ***************************************\n";
	   } else {
		   print "         OK: DBD::Sybase Version is ($DBD::Sybase::VERSION)\n";
	   };
	}

	print "   Testing Perl/Tk Package Integrity\n   ";
	testit( \@tk_packages,
		"\nERROR: At least one Tk Package did not load found!\nThese are usually shipped with Tk - Is your version recent?\nYour Perl Tk Version is $Tk::VERSION",
		"      OK: All Required Perl/Tk Packages Found!!\n" );
	if( $Tk::VERSION < 800.000 ) {
		   print "\n   ***************************************\n";
		   print "   * ERROR - Tk Version ($Tk::VERSION) MUST be 8.24 or later\n";
		   print "   ***************************************\n";
	} elsif( $Tk::VERSION < 800.024 ) {
		   print "\n   ***************************************\n";
		   print "   * WARNING - Tk Version ($Tk::VERSION) Should Probably be at 8.24 or later\n";
		   print "   * This will not stop your install but a more stable version is better\n";
		   print "   ***************************************\n";
	   } else {
		   print "         OK: Tk Version is ($Tk::VERSION)\n";
		}

	print "   Testing Gem Add On Module Integrity\n      ";
	testit( \@gem_packages,
		"\nERROR: At least one gem module did not load!\n",
		"   OK: All Gem Packages Found!!\n" );

	print "   Testing Toolkit Built In Module Integrity\n      ";
	testit( \@my_packages,
		"\nERROR: At least one gem module did not load!\n",
		"   OK: All Builtin Packages Found!!\n" );
	$endtime{"BEGIN BLOCK"}=time;
}

my($install_state);
my($gemrootdir);
my(@perl_includes);
my(@errors);
my(@warnings);
my(@messages);	

if( 0 ) {
	test_initialize();	
	test_dir_structure();
	if( $install_state ne "INSTALLED" ) {
			myprint( "**************************\n");
			myprint( "* GEM Has Not Been Installed Yet\n" );
			myprint( "* Normal Exit\n" );
			myprint( "**************************\n");
			exit(0);
	}
	test_configuration_files();
	test_include_path();
	
	#opendir(DIR,"./bin") || die("reformat() Can't open directory debugging_tools for reading\n");
	#my(@testlist) = grep(/^test/,readdir(DIR));
	#closedir(DIR);
	
	# The interface
	#  all tests run in the gem root directory
	#  if it returns the word SKIPPED:
	#  if it returns the word ERROR:
	#  if it returns the word SUCCESSFUL:
	#
	my(@testlist)=qw(test_01_required_commands.pl test_alarms.pl test_dbi_pl test_dbi_connectivity.pl test_ping_databases.pl test_ping_systems.pl test_repository.pl test_unix_config.pl);
	foreach my $file (sort @testlist) {		vrun_a_command($file); 	}
	
	myprint( "\n\nTEST RESULTS\n" );
	foreach (sort keys %status ) {
		myprint(sprintf "%7s %20s %20s seconds\n",$status{$_},$_,$endtime{$_}-$starttime{$_} );
	}
	myprint( "\n\ntroubleshoot.pl : Completed\n" );
	close(LOG);
	exit(0);
}

sub vrun_a_command {
	my($file)=@_;	
	chdir($gemrootdir);
	my($txt)=$file;
	$txt=~s/^test_//;
	$txt=~s/^\d+\_//;
	$txt=~s/.pl$//;
	start_test($txt);
	my($str)="$^X -I".join(" -I",@perl_includes). " $gemrootdir/bin/$file";
	$str .= " --DEBUG " if $DEBUG;
	
	myprint( "\tThis may take a few seconds... hang on...\n" ) if $file eq "test_ping_systems.pl";
	myprint( "\t(executing) $str\n");	# if $DEBUG;
	open(VAL,$str." 2>&1 |" ) or die("ERROR CANT RUN $str\n");
	my $vstate="UNKNOWN";
	while( <VAL> ) {
		chomp;
		chomp;
		if( /ERROR:/ ) {
			$vstate="ERROR";
			push @errors,"$file: $_\n";
		} elsif( /SUCCESSFUL:/ ) {
			$vstate="SUCCESS";
		} elsif( /SKIPPED:/ ) {
			$vstate="SKIPPED";
		}
		myprint( "\t",$_,"\n");
	}
	close(VAL);
	end_test($vstate);
}

sub myprint {
	print @_;
	print LOG @_;
}

my($testid)=1;
my($current_test);
sub start_test {
	my($id)=@_;
	$current_test=$id;
	myprint( "\ntest ",$testid++,") Testing $id\n" );
	$starttime{$id}=time;

}
sub end_test {
	my($fstate)=@_;
	if( $fstate eq "SKIPPED" ) {
		myprint( "This Test was SKIPPED\n" );
	} else {
		myprint( "\tTest $current_test returned $fstate\n" );
	}
	$endtime{$current_test}=time;
	$status{$current_test}=$fstate;
}

sub validate_directories {
	my($rootdir)=@_;
	#
	# Create Directories If Needed
	#
	# fix the directory structure if necessary - winzip apparently does not expand null files
	sub md {
		my($dirtomake)=@_;
		my(@err);
		if( -d $dirtomake ) {
			if( ! -r $dirtomake ) {
				return 1, "WARNING: Directory $dirtomake is not readable\n";
			}
			if( ! -w $dirtomake ) {
				return 1, "WARNING: Directory $dirtomake is not writable\n";
			}
			return 0;
		#} else {
			#myprint( "Making Directory $dirtomake" );
			#mkdir( $dirtomake, 0755 ) or die "Cant mkdir $dirtomake : $!\n";
		}


		if( ! -d "$gemrootdir/$_" ) {
			return "\tDirectory $gemrootdir/$_ not found\n";
		} elsif( ! -r "$gemrootdir/$_" ) {
			return 1,"\tDirectory $gemrootdir/$_ is not readable\n";
		} elsif( ! -w "$gemrootdir/$_" ) {
			return 1, "\tDirectory $gemrootdir/$_ is not writable\n";
		}
		return 0,"";
	}

	my(@dirs)= qw?conf data ADMIN_SCRIPTS doc plugins Win32_perl_lib_5_8 bin
		win32_batch_scripts win32_batch_scripts/interactive win32_batch_scripts/batch win32_batch_scripts/plan_interactive
		win32_batch_scripts/plan_batch data lib unix_batch_scripts unix_batch_scripts/interactive
		unix_batch_scripts/batch unix_batch_scripts/plan_interactive unix_batch_scripts/plan_batch
		build_tools?;

	my(@data_dirs)=qw?BACKUP_LOGS cronlogs misc CONSOLE_REPORTS depends patern_files
			backup_reports html_output system_information_data misc
				system_information_data/schedlgu system_information_data/hosts 
				lockfiles GEM_BATCHJOB_LOGS?;

	my($num_errors)=0;
	my(@errs);
	foreach (@dirs)      {
		my($rc,$msg)=md("$rootdir/$_");
		$num_errors+=$rc;
		push @errs,$msg if $msg ne "";
	}
	foreach (@data_dirs) {
		my($rc,$msg)=md("$rootdir/data/$_");
		$num_errors+=$rc;
		push @errs,$msg if $msg ne "";
	}

	return $num_errors,@errs;
}

sub test_include_path 
{
	start_test("Perl Include Path");	
	die "\nERROR Cant calculate module perl path - no gem libraries in \@INC\nThe program searches for $gemrootdir in\n".join("\n\t",@INC)."\n"
		if $#perl_includes<0;
	end_test("OK");
}

sub test_configuration_files {
	start_test("Configuration Files");
	if( ! -r "$gemrootdir/conf/gem.dat" )  {
		myprint( "\n" );
		myprint( "Missing Configuration File conf/gem.dat\n" );
		myprint( "*** THIS IS A NORMAL/CLEAN EXIT\n*** IT IMPLIES THAT YOU HAVE NOT RUN configure.pl\n***" );
		myprint( " PLEASE RUN configure.pl NEXT\n\n" );
		close(LOG);
		exit(1);
	}
	
	if( ! -r "$gemrootdir/conf/configure.cfg" )  {
		warn "Configuration file configure.cfg is missing from the conf subdirectory.\n";
		warn "This is normal: please run the GEM setup utility configure.pl.\n";
		close(LOG);
		exit(1);
	}
	
	foreach (qw(unix_passwords.dat sybase_passwords.dat oracle_passwords.dat sqlsvr_passwords.dat win32_passwords.dat)) {
		if( ! -r "$gemrootdir/conf/$_" )  {
			my($str)="Warning: File $_ Not Found - This will not stop your install but may be of concern\n";
			myprint( $str );
			push @warnings,$str;
		}
	}
	
	
	foreach (qw(console.dat win32_cleanup.dat win32_service.dat port_monitor.dat threshold_overrides.dat)) {
		if( ! -r "$gemrootdir/conf/$_" )  {
			my($str)="Note: Extended Functionality Configuration File $_ Not Found\nYou may wish to set up this file by hand to add GEM functionality\n";
			myprint( $str );
		}
	}
	end_test("OK");
}

sub test_dir_structure {
	start_test("Directory Structure");
	chdir($gemrootdir);
	
	my($found_error,@messages)=validate_directories($gemrootdir);
	push @messages, "\tGEM Root Directory = $gemrootdir\n";
	myprint( @messages );
	if( $found_error) {
		if( $found_error > 6 ) {
			myprint( "**************************\n");
			myprint( "* Troubleshoot Completed *\n");
			myprint( "**************************\n");
			close(LOG);
			exit(0);
		}
		
		if( $install_state ne "INSTALLED" ) {
			myprint( "**************************\n");
			myprint( "* GEM Has Not Been Installed Yet\n" );
			myprint( "* Normal Exit\n" );
			myprint( "**************************\n");
			exit(0);
		}
	
		warn "Exiting - The directory structure contains errors (above).\n";
		warn "This indicates that you have not successfully run the GEM Configuration Utility.\nIf you have not run configure.pl, run it now.\n";
		close(LOG);
		exit(0);
	}
	end_test("OK");
}

sub test_initialize {
	open(LOG,">troubleshoot.log") or die "Cant write file troubleshoot.log\n";
	print "OUTPUT IS LOGGED TO troubleshoot.log\n";
	
	$install_state=get_installation_state();
	myprint( "\n\nSTARTING CONFIGURATION TESTS ($install_state)\n" );
	
	$gemrootdir=get_gem_root_dir();
	if( ! -d $gemrootdir ) {
		myprint( "ERROR: Something is wrong - can not parse application root directory from its include path\n" );
		close(LOG);
		exit(0);
	}
	
	foreach (@INC) { push @perl_includes, $_ if /$gemrootdir/; }
}

1;
