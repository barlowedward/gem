#######################################################################
# Standard Header
#  - Begin block is useful
#######################################################################

# copyright (c) 2005-2007 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

package  JobScheduler;

my($VERSION) = 1.0;

@ISA	    	= qw(Exporter);
@EXPORT 	= qw(fetch_jobscheduler save_jobscheduler report_htmlscheduler dump_jobscheduler report_realjobscheduler report_jobscheduler);

use      strict;
use      Carp;
use 		Repository;
use	File::Basename;
my(@keysstr)= qw(enabled base run_type frequency job_type noredirect ok_types enabled command scheduled_on);
my($separator)=";;";

sub fetch_jobscheduler {
	my($dir)=get_gem_root_dir();
	my($file)=$dir."/conf/jobscheduler.dat";
	my(%x);	
	if( -e $file ) {
		die "Cant read $file" 	unless -r $file;
		die "Cant write $file" 	unless -w $file;		
		open( F, $file ) or die "Cant read $file $!\n";
		foreach my $row (<F>){
			chomp $row;
			chomp $row;
			my(@data)= split($separator, $row);
			my($key) = shift @data;
			my($cnt)=0;
			
			my(%a);
			foreach (@data) {
				my($val) =  $keysstr[$cnt];
				$val     =~ s/\s+$//;
				$val     =~ s/NEWLINE/\n/g;
				next unless $val;
				#print "\tSetting Value $val to $_\n";
				$a{ $val } = $_;
				$cnt++;
			}
			$x{$key} = \%a;
		}
		close(F);
	}
	
	return \%x;	
}


sub report_realjobscheduler {
	my($job_scheduler)=@_;
	foreach my $job_name (	sort keys %$job_scheduler ) {
		my($x)=$job_name;
		$x =~ s/\.job$//;
		printf "%32s => %s\n", $x,basename($$job_scheduler{$job_name}->{Application});
	}
	#use Data::Dumper;
	#print Dumper $job_scheduler;
	print "\n";
}

sub report_htmlscheduler {
	my($job_scheduler)=@_;
	
	my($dir)=get_gem_root_dir();
	my($file)=$dir."/doc/source_files/BATCHES_AUTO.pod";
	print "Writing Documentation To $file\n";
	open(FF, ">".$file ) or die "Cant write to $file $!\n";
	
print FF "=head1 NAME

SUMMARY OF BATCH JOBS

The following table is a summary of the jobs that can be created by GEM's configuration utility.
";

	my(%titles) = (
	"INV_"	=> "Inventory Manager (CMDB)",
	"Mssql_"=> "Sql Server Databases",
	"Oracle_"=> "Oracle Databases",
	"Sybase_"=> "Sybase Databases",
	"Mysql_"=> "Mysql Databases",
	"Sys_"=> "GEM",
	"Unix_"=> "Unix Systems",
	"Win32_"=> "Windows Systems",
	);
			
	foreach my $prefix ( sort keys %titles ) {
		
	print FF '<table cellpadding="2" cellspacing="2" border="1"
style="text-align: left; width: 100%;">
<tbody>
<tr><td colspan=4 style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>';
	print FF   "Batch Job Spec For ".$titles{$prefix};
	print FF   '</b></td></tr>
<tr>
<td style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>Job Name</b></td>
<td style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>Win32 or<br>Unix</b></td>
<td style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>Description</b></td>
<td style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>Recommended<br>Frequency</b></td>
</tr>';
		
		foreach my $job_name (	sort keys %$job_scheduler ) {
			next unless $job_name=~/^$prefix/i;
			next if $job_name =~ /CheckServer_/ and $job_name !~ /\*/;
			my($ot)=$$job_scheduler{$job_name}->{ok_types};
			$ot="Both" if $ot eq "both";
			$ot="Win" if $ot eq "nt";
			$ot="Unix" if $ot eq "unix";
			printf FF "<TR><TD style=\"vertical-align: top; background-color: rgb(255, 204, 102);\">%32s</TD><TD>%20s</TD><TD>%20s</TD><TD>%20s</TD></TR>\n", 
				$job_name,
				$ot,
				$$job_scheduler{$job_name}->{Description},
				ucfirst($$job_scheduler{$job_name}->{frequency})				;
				
			# print FF   "<TR><TD COLSPAN=3>",$$job_scheduler{$job_name}->{command},"</TD></TR>";
		}
		print FF   "</TABLE>";
			
		#use Data::Dumper;
		#print Dumper $job_scheduler;
		print FF  "<p>\n";
		print FF  '<table cellpadding="2" cellspacing="2" border="1"
style="text-align: left; width: 100%;">
<tbody>
<tr><td colspan=4 style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>';
	print FF  "Code Executed By ".$titles{$prefix}." Scripts";
	print FF  '</b></td></tr>
<tr>
<td style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>Job Name</b></td>
<td style="vertical-align: top; background-color: rgb(51, 102, 255);"><b>Command</b></td>
</tr>';
		foreach my $job_name (	sort keys %$job_scheduler ) {
			next unless $job_name=~/^$prefix/i;
			next if $job_name =~ /CheckServer_/ and $job_name !~ /\*/;
			my($cmd)=$$job_scheduler{$job_name}->{command};
			$cmd=~s/^__PERL__\s*//;
			$cmd=~s/^__CODELOCATION__//;
			$cmd=~s/NEWLINE$//;
			$cmd=~s/--BATCHID=__BATCHID__\s*//;
			$cmd=~s/--BATCH_ID=__BATCHID__\s*//;
			$cmd=~s?\\?\/?g;
			$cmd=~s?\/\/?\/?g;
			$cmd=~s/^\/ADMIN_SCRIPTS\///g;
			printf FF "<TR><TD  style=\"vertical-align: top; background-color: rgb(255, 204, 102);\">%32s</TD><TD>%20s</TD></TR>\n", 
				$job_name,
				$cmd;
		}
		print FF  "</TABLE>";
		print FF  "<p>\n";	
	}
	close(FF);
}

sub report_jobscheduler {
	my($job_scheduler)=@_;
	foreach my $job_name (	sort keys %$job_scheduler ) {
		printf "%32s => %20s %20s\n", $job_name,
			$$job_scheduler{$job_name}->{scheduled_on},
			$$job_scheduler{$job_name}->{frequency};
	}
	
	#use Data::Dumper;
	#print Dumper $job_scheduler;
	print "\n";
}

sub dump_jobscheduler {
	my($job_scheduler)=@_;
	use Data::Dumper;
	print Dumper $job_scheduler;
	print "\n";
}

sub save_jobscheduler {
	my($job_scheduler)=@_;
	my($dir)=get_gem_root_dir();
	my($file)=$dir."/conf/jobscheduler.dat";
	my(%keyhash);	foreach ( @keysstr ) { $keyhash{$_}=1; }
	
	print "Writing Scheduler To $file\n";
	open(FF, ">".$file ) or die "Cant write to $file $!\n";
	foreach my $s ( keys %{$job_scheduler} ) {		
		my($outstr)=$s.$separator;
		foreach (@keysstr) { 
			my($val)=$$job_scheduler{$s}->{$_};
			$val=~s/\\n$//;
			$val=~s/\n/NEWLINE/g;
			$outstr.= $val.$separator; 
		}
		print FF $outstr,"\n";;
		foreach my $k ( keys %{$$job_scheduler{$s}} ) {			
			next if $keyhash{$k};
			print "$k => $$job_scheduler{$s}->{$k} 	\n";
		}		
	}
	close(FF);	
};

1;

__END__

=head1 NAME

JobScheduler.pm 

=head2 DESCRIPTION

Persists job scheduler info to conf directory file $dir."/conf/jobscheduler.dat"

 save_jobscheduler
 fetch_jobscheduler
 
$jobscheduler is a hash of hashes.