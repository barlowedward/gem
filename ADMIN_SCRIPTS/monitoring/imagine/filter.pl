#!/apps/perl/linux/perl-5.8.2/bin/perl

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use Do_Time;
use Logger;
use MlpAlarm;
use File::Find;

use Getopt::Long;
use vars qw( $NOMAIL $DEBUG );

sub usage { return "$0 --NOMAIL --DEBUG\n"; }

die usage("Bad Parameter List $!\n") unless
    GetOptions( "NOMAIL"	=>	\$NOMAIL,
		"DEBUG"		=>	\$DEBUG
	);

$|=1;

sub debug { print_line( @_ ) if defined $DEBUG; }

my($outstring)="";
sub print_line {
	$outstring.=join("",@_);
	print @_;
}

# my($dir)="/apps/imagine/its50/objroot/logroot/compute_servers";
my($dir)="/dat/imaginelogs";
my($today)=do_time(-fmt=>"_yymmdd_");
my(@user_watch_list)=qw(slorne hchehime ienglander cnewman mmeskin dnolan kevinkish nmoniodes);
my($timethreshmin)=5;
my(@error_ignore_list);		# regexp's to ignore

# logger_init( -debug=>$DEBUG, -logfile => undef, -mail_host=>"mail1.mlp.com", -command_run => $0, -mail_to =>"ebarlow\@mlp.com,mlipsits\@mlp.com", -fail_subject=>"Imagine Users Error Report"  );
if( defined $NOMAIL )  {
	logger_init( -debug=>$DEBUG, -logfile => undef, -mail_host=>"mail1.mlp.com", -command_run => $0,
		-success_mail_to =>undef  );
} else {
	logger_init( -debug=>$DEBUG, -logfile => undef, -mail_host=>"mail1.mlp.com", -command_run => $0,
		-success_mail_to =>"ebarlow\@mlp.com,mlipsits\@mlp.com"  );
}

print_line( join("\n",MlpManageData(
		Operation=>"del",
		Admin=>"Delete By Monitor",
		Monitor		=> "Imagine Log Filter",
		#-debug=>1

		#OkId=>$action_id,
		#Server		=> $keys{System},
		#System	=> $keys{System},
		#Program		=> $keys{Program},
		#Subsystem	=> $keys{Subsystem},
		#State		=> $keys{State},
				)));

MlpBatchStart(-name=>"Imagine Log Filter", -monitorjob=>1);

########### START THE PROGRAM ##############
print_line( "IMAGINE LOGFILE ERROR REPORT\n" );
print_line( "Report run at ",scalar(localtime(time)),"\n" );

while(<DATA>){
	chomp;
	chomp;
	next if /^#/ or /^\s*$/;
	push @error_ignore_list,$_;
}

my(%watchlist);
my(%user_by_filename);
my(%num_sessions_by_user,%num_completed_by_user);
foreach (@user_watch_list) {
	$watchlist{$_}=1;
	$num_sessions_by_user{$_}=0;
	$num_completed_by_user{$_}=0;
}

sub wanted {
	return unless /$today/;
	return unless /out.its/;

	#print "DBG: dir= $File::Find::dir ";
	#print " \$_= $_ ";
	#print " name= $File::Find::name \n";

	my($file)=$File::Find::name;
	MlpBatchRunning(-message=>"Processing File $file\n");
	$file=~/out\.its\.([^_]+)\_/;
	my($user)=$1;
	next unless $watchlist{$user};
	my($moddays)=  -M $file;
	$moddays*=24*60;
	# print "Working on File $file ($moddays minutes old)\n";
	# print "User=$user\n";
	if( $moddays<$timethreshmin ) {
		print "... user=$user\n" if $DEBUG;
		$num_sessions_by_user{$user}++ ;
		$user_by_filename{$file}=$user;
	}

}

find({ wanted=>\&wanted,follow=>1},$dir);

#print "Reading $dir\n" if defined $DEBUG;
#opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
#my(@subdirs) = readdir(DIR);
#closedir(DIR);
#
#my(@files);
#foreach my $sd (@subdirs)  {
#	print "Subdirectory = $sd\n" if defined $DEBUG;
#	opendir(DIR,$dir."/$sd") || die("Can't open directory $dir/$sd for reading\n");
#	my(@filelist) = grep(/$today/,readdir(DIR));
#	foreach( @filelist ) {
#		push @files, $dir."/$sd/".$_;
#		print "Found $dir/$sd/$_ \n" if defined $DEBUG;
#	}
#	closedir(DIR);
#}

#my(%user_by_filename);
#foreach my $file (@files) {
#	MlpBatchRunning(-message=>"Processing File $file\n");
#	$file=~/out\.its\.([^_]+)\_/;
#	my($user)=$1;
#	next unless $watchlist{$user};
#	my($moddays)=  -M $file;
#	$moddays*=24*60;
#	# print "Working on File $file ($moddays minutes old)\n";
#	# print "User=$user\n";
#	if( $moddays<$timethreshmin ) {
#		$num_sessions_by_user{$user}++ ;
#		$user_by_filename{$file}=$user;
#	}
#}

my(%is_initialized,%book_by_file);
foreach (keys %user_by_filename) {
	process_file($_,$user_by_filename{$_});
}

MlpBatchRunning(-message=>"Computing Results\n");
my($alarmstring)="";
foreach (@user_watch_list) {
	my($msg)= "User $_ has $num_sessions_by_user{$_} imagine sessions $num_completed_by_user{$_} completed\n";

	my($state)="COMPLETED";
	$state="WARNING" if $num_sessions_by_user{$_}==0;
	$state="WARNING" if $num_sessions_by_user{$_}!=$num_completed_by_user{$_};

	$alarmstring.=$state." ".$msg;
	print $state." ".$msg;

	MlpHeartbeat( -monitor_program=>"Imagine Log Filter",
					-system=>"IMAGINE",
					-subsystem=>"User: $_",
					-state=>$state,
					-message_text=>$msg
	);
}

ok_email($alarmstring."\n\n".$outstring);
MlpBatchDone(-message=>"mailing output");
exit(0);

sub process_file
{
	my($fname,$user)=@_;
	print_line( "\n###############################\n" );
	print_line( "# processing $fname\n" );
	print_line( "###############################\n" );
	open(FP,$fname) or die "Cant open $fname : $!\n";
	while(<FP>) {
		# Figure out which ledger was brought up
		# hts_xact_hist, -M- [ics]
		# the string for this is: New Frame
		#print_line( substr($_,18,9),"\n" );

		if( /select \* from hts_xact_history where hist_timestamp/ ) {
			# THIS SESSION INITIALIZED
			if( ! defined $is_initialized{$fname} ) {
				$is_initialized{$fname}=substr($_,0,17);
				$num_completed_by_user{$user_by_filename{$fname}}++;
			}
		}
		# # check book is loaded
		# if( substr($_,18,9) eq "-M- \[ics\]" and /New frame \</ and /Ledger\>/) {
			# /([\w\_\~]+) - Ledger\>/;
			# my($book)=$1;
			# if( defined $book_by_file{$fname} ) {
				# $book_by_file{$fname}.=" ".$book
					# unless $book_by_file{$fname}=~/$book/;
			# } else {
				# $book_by_file{$fname}=$book;
			# }
		# }
		# search for -E- -W-
		next unless / -E- / or / -W- /;

		# exclude msgs from filter file
		my($found)="FALSE";
		chomp;
		my($line)=$_;
		foreach my $str ( @error_ignore_list ) {
			# print_line( " Comparing $line \n" );
			# print_line( " to $str \n" );
			if( $line =~ /$str/) {
				$found="TRUE";
				last;
			}
		}
		next if $found eq "TRUE";
		print_line( "[",$fname,"\] ",$_,"\n" );

		MlpEvent( -monitor_program=>"Imagine Log Filter",
					-system=>"IMAGINE",
					-subsystem=>"User: $user",
					-severity=>"WARNING",
					-message_text=>$_
		);
	}
	close(FP);
}
__DATA__

Could not get Global:ITS exchange rate asof 0
Your License Server is Back Up;
Correlation table NOT Loaded
Reuters error <The record could not be found>
You are NOT permissioned for <RESET_WIZ> feature
# Attempt to retrieve index of nonexistent total field:
Could not set up field map for database table
Invalid app Field <Arbitrage Account>.
Reuters error <Record not service permissioned> for symbol
exchange rate asof 19991231
Changed database context
Changed language setting
No such feature exists (-5,357)
CORBA::Exception binding server
Too many holdings returned, cancel query
The scenario DEFAULT.ITS was modified. Do you want to save it to the database
Attempt to retrieve index of nonexistent total field: 'CapReq'
Attempt to retrieve index of nonexistent total field: 'CapReqPaired'
Could not get Global:ITS exchange rate asof 0
Invalid app Field <Arbitrage Account>.
gen_dbx.c_853:  table id < 0.
-W- Reuters error <Item name: too long>
-W- Reuters error <Non-updating item> for symbol
Correlation Table NOT Loaded
-W- Stale Symbol:
-E- SQL-Main Message Handler\[3604\]\: \<Duplicate key was ignored.\>
-E- Error evaluating name rule; moving holding to <UNASSIGNED> portfolio
Ignoring query
exceeds HARD limit for:
Reuters error <Access Denied:
Currency drift record for date
