#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Basename;
my($curdir)=dirname($0);

# take $1 and run with -P, -S -U on all servers
use Repository;
use Sybase::SybFunc;
use vars qw(@ARGV);

sub usage
{
        print @_;
        print "Usage: allsrv_query.pl Query

Generic program to run a query on all servers.  If you type
        \"$0 Query\"
you will end up executing
        Query
on all your servers from the master database.
\n";

        return "\n";
}

my($query)=join(" ",@ARGV);
die usage("Must Pass Full Query To Run In All Servers\n" )
        if $query =~ /^\s*$/;
print "Query is $query\n";

db_set_web_page(0);

my(@srv)=get_password(-type=>"sybase",-name=>undef);

my($login,$pass,$srv,$cmd);
foreach $srv (@srv) {
   ($login,$pass)=get_password(-type=>"sybase",-name=>$srv);
        print "Connecting to $srv\n";
        my($rc)=db_connect($srv,$login,$pass);
			if( ! $rc ) {
					print "Cant connect to $srv as $login\n";
					next;
			}

        my(@rc)=db_reformat_results(db_query("master",$query));
        foreach (@rc) { my(@x)=db_decode_row($_); print "->",join(" ",@x),"\n"; }

        db_close_sybase();
}

__END__

=head1 NAME

allsrv_query.pl - loop through your servers utility

=head2 DESCRIPTION

Run a program in all your servers.  This is useful with the other utilities provided in this directory.  It requires that the run utility take B<-S>/B<-U>/B<-P> parameters.  The server should be in your password file as per Repository.

In other words... if you wish to run a program to remove user paulr on a server you might type

        adduser -SSYBASE -Usa -PXXX -x -upaulr

but with this utility, you could run that command on ALL your servers in one fell swoop with:

        AllSrv adduser -x -upaulr

As you can see, the B<-U>,B<-P>, and B<-S> parameters are added automatically

=head2 USAGE

        Usage: ./AllSrv Parameters

Generic program  to run on all servers.  If you type

                "./AllSrv MY_PROG OTHER_PARM"

you will end up executing

                "MY_PROG -SSRV -PPASS -UUSR OTHER_PARM"

for all the servers in your password file.

=cut
