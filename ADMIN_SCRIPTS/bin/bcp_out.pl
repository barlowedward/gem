#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed


use strict;
use Getopt::Long;
use File::Path;
use DBIFunc;
use Do_Time;
use File::Basename;
use RunCommand;

use vars qw( $BCP_COMMAND $BCP_ARGS $DATABASE $EXCLUDE $OUTDIR $USER $SERVER $PASSWORD $NOEXEC $opt_d);

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

usage ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"EXCLUDE=s"		=> \$EXCLUDE ,
		"USER=s"			=> \$USER ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"DATABASE=s" 	=> \$DATABASE ,
		"NOEXEC" 	=> \$NOEXEC ,
		"OUTDIR=s" 		=> \$OUTDIR ,
		"BCP_COMMAND=s"=> \$BCP_COMMAND ,
		"BCP_ARGS=s"=> \$BCP_ARGS ,
		"DEBUG"      	=> \$opt_d );

sub usage {
	print @_;
   die "bcp_out.pl [-DEBUG] [-BCP_COMMAND=cmd] [-BCP_ARGS=args] [--NOEXEC] --SERVER=xxx --USER=xxx --PASSWORD=xxx --DATABASE=db -EXCLUDE=TBL,TBL -OUTDIR=OUTDIR Tbl Tbl Tbl

     Extracts data from a database

	  -BCP_ARGS=args for bcp - default is -n
     -EXCLUDE=exclude tables (separated by comma)
     -DEBUG debug mode

     If no TBLs defined, will bcp out all tables in the db. OUTDIR is created if it does not exist.
";
     return "\n";
}

usage("Must pass server\n" ) 		   unless defined $SERVER;
usage("Must pass username\n" )	  	unless defined $USER;
usage("Must pass password\n" )	  	unless defined $PASSWORD;
usage("Must Pass Outdir with -OUTDIR\n")        unless defined $OUTDIR;
mkpath( $OUTDIR, 1, 0777 ) unless -d $OUTDIR;
usage("Outdir $OUTDIR not a directory\n")        if -e $OUTDIR and ! -d $OUTDIR;

$BCP_ARGS="-n" unless $BCP_ARGS;

my(%exclude_file);
if( defined $EXCLUDE ) {
        foreach (split /,/,$EXCLUDE) {
                $exclude_file{$_}=1;
        }
}

my(@TABLES);
foreach ( @ARGV) {
	if( /\./ ) {
		push @TABLES,$_;
	} else {
		push @TABLES,$DATABASE."..".$_;
	}
}

if( $#TABLES<0 ) {
	print "Connecting to the server to fetch table list\n";
	dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD)
		or die "Cant connect to $SERVER as $USER\n";
	foreach ( dbi_query(-db=>$DATABASE,-query=>"select name from sysobjects where type='U'")) {
			my @dat=dbi_decode_row($_);
			push @TABLES,$DATABASE."..".$dat[0]
				unless $exclude_file{$dat[0]} or $exclude_file{"DATABASE..".$dat[0]};
	}
	dbi_disconnect();
}

if( $#TABLES<0 ) {
	die "NO TABLES FOUND TO BCP!";
}

print "Starting Table BCP's\n";
foreach my $tbl (sort @TABLES) {
	die "ERROR: table ($tbl) specified but it is not fullpath spec (ie. db..tblname)\n"
		unless $tbl=~/\./;
	print "Copying out table $tbl\n";
	my($starttime)=time;
	$tbl =~ s/^\s+//;
	$tbl =~ s/\s+$//;
	my($cmd)="bcp";
	$cmd=$BCP_COMMAND if $BCP_COMMAND;

	$cmd.=" $tbl out $OUTDIR/$tbl.bcp -U$USER -P$PASSWORD -S$SERVER $BCP_ARGS\n";
	#print $cmd;
	run_cmd(-cmd=>$cmd, -print=>1, -err=>"bcp failed");

	print "Duration: ",do_diff(time - $starttime,1),"\n";
}
print "Table Level BCP's Completed\n";
exit(0);

sub run_cmd
{
   my(%OPT)=@_;
   my(@rc);
   chomp $OPT{-cmd};

   $OPT{-cmd}=$^X." ".$OPT{-cmd} if $OPT{-cmd} =~ /^[\~\:\\\/\w_\.]+\.pl\s/;

   my $cmd_no_pass=$OPT{-cmd};
   $OPT{-cmd} .= " 2>&1 |" unless defined $ENV{WINDIR};
   if( $cmd_no_pass=~ /-PASSWORD=/ ) {
   	$cmd_no_pass=~s/-PASSWORD=\w+/-PASSWORD=XXX/;
   } else {
   	$cmd_no_pass=~s/-P\w+/-PXXX/;
   }
   print "(".localtime(time).") ".$cmd_no_pass."\n" if  defined $OPT{-print};

   if( ! $NOEXEC ) {
	   $OPT{-cmd} .= " |" 		    if defined $ENV{WINDIR};

	   open( SYS2STD,$OPT{-cmd} ) || die("Unable To Run $OPT{-cmd} : $!\nPath is $ENV{PATH}\n");
	   while ( <SYS2STD> ) {
	      chop;
	      push @rc, $_;
	   }
	   close(SYS2STD);
	} else {
		print "NOEXEC MODE - SKIPPING EXECUTION OF STEP\n";
		#print "The Command Would Have Been: $cmd_no_pass\n";
	}

   my($cmd_rc)=$?;
   if( $cmd_rc != 0 ) {
	if( defined $OPT{-err} ) {
  		print "completed (failed) return_code=$cmd_rc\n";
			my($str)="Error: ".$OPT{-err}."\n";
			$str.="Return Code: $?\n";
      		#$str.="Command: ".$cmd_no_pass."\n";
			$str.="\n\nResults:\n";
      		die( $str."\n-- ".join("\n-- ",@rc)."\n");
	} elsif( defined $OPT{-print} ) {
		print "Return Code:$?\n";
      		print "Results:\n-- ".join("\n-- ",@rc)."\n";
	}
	unshift @rc,"Error: Return Code $?";
		return @rc;
	} else {
		print "-- ".join("\n-- ","Command completed at ".localtime(time)."\n",@rc)
			if defined $OPT{-print};
		return @rc;
	}
}
#       if( $#ARGV<0 ) {
#                $EXCLUDE="select name from sysobjects where type = 'U' order by name";
#        } else {
#                $EXCLUDE="select name from sysobjects where type in ('U','V','S') and name in (";
#                foreach (@ARGV) { $EXCLUDE.="\"$_\","; }
#                chop $EXCLUDE;
#                $EXCLUDE.=")";
#        }
#        # print "DBG: Db $db Proc $EXCLUDE\n";
#
#        die "Cant Use Database $db" if $dbproc->dbuse($db) == 0;
#        $dbproc->dbcmd($EXCLUDE);
#        die "dbsqlexec() on $EXCLUDE failed\n" if $dbproc->dbsqlexec() == 0;
#
#        my(@objlist)=();
#        while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
#        while ( @dat=$dbproc->dbnextrow()) {
#                if( defined $exclude_file{$dat[0]} ) {
#                        print "Excluding $_\n";
#                        next;
#                }
#                push @objlist,$dat[0];
#        }
#        }
#
#        foreach (@objlist) {
#
#                if( defined $exclude_file{$_} ) {
#                        print "Excluding $_\n";
#                        next;
#                }
#
#                open(OUTFILE,">".$opt_o."/$_.bcp")
#                        or die "Cant write to $opt_o/$_.bcp\n";
#                my($count)=0;
#                printf( "%7s %30s ",($cnt++)."/".($#objlist+1),$_ );
#
#                $EXCLUDE="select * from $_\n";
#                $dbproc->dbcmd($EXCLUDE);
#                die "dbsqlexec() on $EXCLUDE failed\n" if $dbproc->dbsqlexec() == 0;
#
#                while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
#                while ( @dat=$dbproc->dbnextrow()) {
#                        $count++;
#                        s/\n/INSERT_RETURN_HERE/g;
#                        print "." if $count%1000 == 0;
#                        print OUTFILE join('~~',@dat),"\n";
#                }
#                }
#                printf( " (%s rows saved)\n",$count );
#                close OUTFILE;
#        }
#}
#
#$dbproc->dbclose();
#print "Bcp Completed Successfully\n";

__END__

=head1 NAME

bcp_out.pl - copy files out of a database

=head2 DESCRIPTION

Bcp all tables out of a database and into flat files
The files can be read with bcp_in.pl.

=head2 USAGE

bcp_out.pl [-DEBUG] [-BCP_COMMAND=cmd] [-BCP_ARGS=args] [--NOEXEC] --SERVER=xxx --USER=xxx --PASSWORD=xxx --DATABASE=db -EXCLUDE=TBL,TBL -OUTDIR=OUTDIR Tbl Tbl Tbl

     Extracts data from a database

     -BCP_ARGS=args for bcp - default is -n
     -EXCLUDE=exclude tables (separated by comma)
     -DEBUG debug mode

=cut
