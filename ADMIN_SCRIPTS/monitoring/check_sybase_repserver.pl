#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
use MlpAlarm;
use CommonFunc;
use Repository;
use Logger;
use File::Basename;
use Sys::Hostname;
use CommonHeader;

BEGIN { $ENV{SYBASE}="/export/home/sybase" unless defined $ENV{SYBASE}; }

use vars qw( $TYPE $THRESHOLD $USER $MAILTO $RUNALL $SERVER $PASSWORD $OUTFILE $DEBUG $BATCHID);

# Copyright (c) 1997-2004 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
	print "check_sybase_repserver.pl -RUNALL -OUTFILE=Outfile --MAILTO=a,b,c\n";
	print "check_sybase_repserver.pl -USER=USER -SERVER=SERVER -PASSWORD=PASS -THRESHOLD=secs [-debug] -TYPE=Sybase|ODBC -OUTFILE=Outfile --MAILTO=a,b,c

THRESHOLD is the threshold for space per queue (ie they must be older than -THRESHOLD arg) before a CRITICAL event is generated\n";

   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("No Arguments Passed\n") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"OUTFILE=s"		=> \$OUTFILE ,
		"RUNALL"			=> \$RUNALL,
		"USER=s"			=> \$USER ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"MAILTO=s" 		=> \$MAILTO,
		"BATCHID=s" 	=> \$BATCHID,
		"BATCH_ID=s" 	=> \$BATCHID,
		"TYPE=s" 		=> \$TYPE ,
		"THRESHOLD=s" 	=> \$THRESHOLD ,
		"DEBUG"      	=> \$DEBUG );

$BATCHID= "CheckSybaseReplication" unless $BATCHID;
my($NL)="\n";
my($log_msgs_to_stdout);
$log_msgs_to_stdout=1 if defined $DEBUG;

unlink($OUTFILE) if defined $OUTFILE;
logger_init( -debug=> $DEBUG, -logfile => $OUTFILE, -command_run => $0, -mail_to =>$MAILTO, -fail_subject=>"Failed Rep Server Check For $SERVER"  );

if( defined $SERVER and defined $USER and defined $PASSWORD ) {
	monitor_a_server($SERVER,$USER,$PASSWORD,$THRESHOLD);
} else {
	my($rt_ptr,$sv_ptr)=read_rep_configfile();
	foreach ( keys %$sv_ptr ) {
		my($svrptr)=$$sv_ptr{$_};
		next unless $$svrptr{type} eq "repserver";
		my($t)= $THRESHOLD || $$svrptr{threshold};
		info( "MONITORING SERVER $_ (threshold=$t)\n");
		monitor_a_server($_,$$svrptr{login},$$svrptr{password},$t);
		#print "$_ LOGIN: $$svrptr{login} \n";
		#print "$_ password: $$svrptr{password} \n";
		#print "$_ threshold: $$svrptr{threshold} \n";
	}
}
debug( "program completed successfully.\n" );
MlpBatchJobEnd();
exit(0);

sub monitor_a_server {
	my($SERVER,$USER,$PASSWORD,$THRESHOLD)=@_;

	if( ! $SERVER or ! $USER or ! $PASSWORD or ! $THRESHOLD ) {
		logdie( usage("Must pass server\n" ))		unless defined $SERVER;
		logdie( usage("Must pass username\n" ))		unless defined $USER;
		logdie( usage("Must pass password\n" ))		unless defined $PASSWORD;
		logdie( usage("Must pass threshold MB\n" ))	unless defined $THRESHOLD;
	}

	MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor',-SUBKEY=>$SERVER);
	#
	# CONNECT
	#
	debug( "Connecting to $SERVER\n" );
	$TYPE="Sybase" unless defined $TYPE;
	#dbi_msg_exclude(
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD,-type=>$TYPE);
	if( ! $rc ) {
		MlpEvent(
			-severity=>"CRITICAL",
			-debug=>$DEBUG,
			-system=>$SERVER,
			-monitor_program=>$BATCHID,
			-message_text=>"Cant connect to $SERVER as $USER from ".hostname()."\n");
		# MlpBatchDone();
		MlpBatchJobEnd(-EXIT_CODE => "FAILED", -EXIT_NOTES => "Cant connect to $SERVER as $USER from ".hostname()."\n");
		logdie("Cant connect to $SERVER as $USER from ".hostname()."\n");
	}
	debug("Connected to $SERVER\n");

	dbi_set_mode("INLINE");
	my($badstr);

	my $sql = "admin who_is_down";
	debug( "running: $sql\n" );
	my(@query_data)=dbi_query(-db=>"",-query=>$sql);
	debug( ($#query_data+1)." Rows Returned\n" );
	if( $#query_data < 0 ) {
		MlpHeartbeat(
			-state=>"OK",
			-debug=>$DEBUG,
			-system=>$SERVER,
			-subsystem=>"admin who_is_down",
			-monitor_program=>$BATCHID,
			-message_text=>"admin who_is_down ran successfully\n");
	} else {
		my($str)="";
		foreach( @query_data ) {
			my @dat=dbi_decode_row($_);
			$str.=join(" ",@dat)."\n";
			$badstr.="admin who_is_down: $str \r\n";
			debug( "-->".join(" ",@dat)."\n" );
		}
		MlpHeartbeat(
			-state=>"ERROR",
			-debug=>$DEBUG,
			-system=>$SERVER,
			-subsystem=>"admin who_is_down",
			-monitor_program=>$BATCHID,
			-message_text=>"admin who_is_down failed : $str \n");
	}

	$sql = "admin disk_space";
	debug( "running: $sql\n" );
	@query_data=dbi_query(-db=>"",-query=>$sql);
	debug( ($#query_data+1)." Rows Returned\n" );
	my($total_avail,$total_used)=(0,0);
	my($state,$msg)=("OK","");
	foreach( @query_data ) {
		my($partition,$logical,$partid,$totalsegs,$usedsegs,$parstate)=dbi_decode_row($_);
		$total_avail+= $totalsegs;
		$total_used += $usedsegs;
		$parstate =~ s/\///g;
		debug( "-->"." partition=",$partition," logical=",$logical," id=",$partid," totalsegs=",$totalsegs," usedsegs=",$usedsegs," parstate=",$parstate,"\n");

		#if( $usedsegs >= .9*$totalsegs ) {
		#	$state="ERROR";
		#	$msg  ="Partition $logical $parstate $usedsegs MB Used / $totalsegs MB (almost full)";
		#	$badstr.=$msg.".\r\n";
		#}

		if( $parstate !~ /ON-LINE/ ) {
			$state="ERROR";
			$msg  .="Partition $logical $parstate (not online)";
			$badstr.=$msg.".\r\n";
		}

		#if( defined $THRESHOLD and $usedsegs >= $THRESHOLD ) {
		#	$state="ERROR";
		#	$msg  ="Partition $logical $parstate $usedsegs MB Used / $totalsegs MB (>Threshold=$THRESHOLD)";
		#	$badstr.=$msg.".\r\n";
		#}

	#	MlpHeartbeat(
	#		-state=>$state,
	#		-debug=>$DEBUG,
	#		-system=>$SERVER,
	#		-subsystem=>"queue $logical",
	#		-monitor_program=>$BATCHID,
	#		-message_text=>$msg);
	#
	#	info( "--> ($state) ".$msg."\n" );
	}

	if( defined $THRESHOLD and $total_used >= $THRESHOLD ) {
		$state="ERROR";
		$msg  .="admin disk_space: $total_used MB Used / $total_avail MB Avail (>Threshold=$THRESHOLD)";
		$badstr.="admin disk_space: $total_used MB Used / $total_avail MB Avail (>Threshold=$THRESHOLD)";
	}

	if( $state eq "OK" ) {
		$msg  .="admin disk_space: $total_used MB Used / $total_avail MB Avail (Threshold=$THRESHOLD)";
	}

	MlpHeartbeat(	-state=>$state,
			-debug=>$DEBUG,
			-system=>$SERVER,
			-subsystem=>"admin disk_space",
			-monitor_program=>$BATCHID,
			-message_text=>$msg);

	my($dd)=get_gem_root_dir()."/data/system_information_data";
	if( -d $dd ) {
		open(OOO,">> $dd/sybase_rep_queue_size.dat");
		my($x)=time;
		print OOO $x,"\t",$SERVER,"\t",$total_used,"\t",$total_avail,"\n";
		close(OOO);
	} else {
		print "Unable to write $dd/sybase_rep_queue_size.dat";
	}

	$sql = "admin health";
	debug( "running: $sql\n" );
	@query_data=dbi_query(-db=>"",-query=>$sql);
	debug( ($#query_data+1)." Rows Returned\n" );
	foreach( @query_data ) {
		my($mode,$quiesce,$parstatus)=dbi_decode_row($_);
		my($state,$msg)=("OK","Rep Server healthy");
		if( $mode !~ /NORMAL/ or $parstatus !~ /HEALTHY/ ) {
			$state="ERROR";
			$msg  ="Server Mode=$mode Quiesce=$quiesce Status=$parstatus\n";
			$badstr.=$msg.".\r\n";
		}

		MlpHeartbeat(
			-state=>$state,
			-debug=>$DEBUG,
			-system=>$SERVER,
			-subsystem=>"health",
			-monitor_program=>$BATCHID,
			-message_text=>$msg);

		info( "--> ($state) ".$msg."\n" );
	}

	dbi_disconnect();
	if( $badstr ) {
		$badstr=substr($badstr,0,250);
		bad_email($badstr) if defined $badstr;
		MlpBatchJobEnd(-EXIT_CODE => "OK", -EXIT_NOTES => $badstr);
	} else {
		MlpBatchJobEnd();
	}
}

sub debug { print @_ if defined $DEBUG; }
sub output { print @_; }

# this func is in more than one configfile - should be moved to a library
sub read_rep_configfile {
	my($dir)=get_conf_dir();
	die "replication.dat does not exist in $dir\n" unless -r "$dir/replication.dat";
	my(%routes, %servers);
	open( CONF,"$dir/replication.dat" ) or die "cant read $dir/replication.dat $!\n";
	while(<CONF>) {
		next if /^\s*#/ or /^\s*$/;
		chomp;chomp;
		my(@dat)=split(/\s+/,$_);
		die "Column 1 of replication.dat must be REPSERVER or REPROUTE"
			unless $dat[0] eq "REPSERVER" or $dat[0] eq "REPROUTE";
		if( $dat[0] eq "REPSERVER" ) {
			my(%hash);
			$hash{login}=$dat[2];
			$hash{password}=$dat[3];
			$hash{threshold}=$dat[4];
			$hash{type}="repserver";
			$servers{$dat[1]} = \%hash;
		} elsif( $dat[0] eq "REPROUTE" ) {
			my(%hash);
			$hash{sourcesvr}=$dat[1];
			$hash{sourcedb} =$dat[2];
			$hash{targetsvr}=$dat[3];
			$hash{targetdb} =$dat[4];
			$hash{repserver}=$dat[5];
			$routes{$dat[1].".".$dat[2].".".$dat[3].".".$dat[4]} = \%hash;

			if( ! defined $servers{$dat[1]} ) {
				# grab source password
				output( "Fetching Source Password Info For $dat[1] $NL") if $DEBUG;
				my($l,$p)=get_password(-type=>"sybase", -name=>$dat[1] );
				die "No password found for $dat[1]\n" unless $l and $p;
				my(%x);
				($x{login},$x{password})=($l,$p);
				$x{type}="dataserver";
				$servers{$dat[1]}=\%x;
			}

			if( ! defined $servers{$dat[3]} ) {
				# grab source password
				output( "Fetching Dest Password Info For $dat[3] $NL" ) if $DEBUG;
				my(%x);
				my($l,$p)=get_password(-type=>"sybase", -name=>$dat[3] );
				die "No password found for $dat[3]\n" unless $l and $p;
				($x{login},$x{password})=($l,$p);
				$x{type}="dataserver";
				$servers{$dat[3]}=\%x;
			}
		}
	}
	close(CONF);
	return( \%routes, \%servers);
}
__END__

=head1 NAME

CheckSybaseReplication.pl - utility to check databases

=head2 USAGE

	check_sybase_repserver.pl -UUSER -SSERVER -PPASS -ttime

=head2 DESCRIPTION

 Checks for blocks > time seconds old
 Checks for status in in ('infected','bad status', 'log suspend','stopped')
 Checks for circular deadlocks

=cut
