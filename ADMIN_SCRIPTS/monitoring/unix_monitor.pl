#!/apps/perl/linux/perl-5.8.2/bin/perl

#
# Based On Generic Log File Filteration Script
#
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use     	strict;
use     	File::Basename;
use     	LogFunc;
use 		Do_Time;
use 		Getopt::Long;
use 		MlpAlarm;
use 		Repository;
use 		CommonFunc;
use 		CommonHeader;

my($VERSION)			=1.0;
my($PROGNAME)  		="unix_monitor.pl";
my($PROGNAME_ENGLISH)="Unix Log File Monitor";
my($HB_MONITOR)		="UnixLogmon";
my($FILE_DATE_FMT)	="mon ds";
use vars qw( $DEBUG $NOSAVEPATFILE $MAXLINES $PATFILE $INFILE $HTML $TODAYONLY $EXCLUDEFILE $OUTFILE $DEBUG2 $NOLOG  );
my($WRITEFILE);
my($ML_MAX_LINES)		=1000;

my  $LOG="/monitor/server/log";

# these are for duplicates
my(%duplicates, %ignore_count);
my($current_line_no)=0;
my($max_line_sep)=500;					# this number of lines between saves
my($year)=do_time(-fmt=>"yyyy");		# the messages have no year so use current year... yes i know... its
												# gonna produce problems on new years ... oh well...
$|=1;
sub usage
{
	return "Usage: $PROGNAME --NOSAVEPATFILE --TODAYONLY --HTML --OUTFILE=file --EXCLUDEFILE=file --DEBUG --NOLOG [--PATFILE=file] --INFILE=file\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(	"INFILE=s"	=> \$INFILE,
		"PATFILE=s"		=> \$PATFILE,
		"MAXLINES=s"	=> \$MAXLINES,
		"EXCLUDEFILE=s"=> \$EXCLUDEFILE,
		"TODAYONLY"		=> \$TODAYONLY,
		"OUTFILE=s"		=> \$OUTFILE,
		"NOSAVEPATFILE"=> \$NOSAVEPATFILE,
		"NOLOG" 			=> \$NOLOG,
		"HTML" 			=> \$HTML,
		"DEBUG2"			=> \$DEBUG2,
		"DEBUG"			=> \$DEBUG );

die usage("Must pass INFILE\n" ) unless defined $INFILE;

my($nl)="\n";
$nl="<br>\n" if $HTML;
$DEBUG=1 if $DEBUG2;

if( Am_I_Up() and ! $DEBUG2 ) {
	print "Program Running - Only One instance can run at once\n" if is_interactive();
	print "LOCK INFO=",join("|",GetLockfile()),"\n" if is_interactive();
	exit(0);
}

MlpBatchJobStart(-BATCH_ID=>$HB_MONITOR ) unless defined $NOLOG;
$PATFILE=get_gem_root_dir()."/data/patern_files/$HB_MONITOR.pat" unless $PATFILE;

my($tmpfile);
if( defined $OUTFILE ) {
	$tmpfile=get_tempfile($OUTFILE);
	print "Temporary Output File : $tmpfile\n";
	print "Results will be copied to $OUTFILE\n";
	die "ERROR: Cant write to $OUTFILE\n" if -e $OUTFILE and ! -w $OUTFILE;
	open(OUTFILE,">$tmpfile" ) or die( "Cant write $tmpfile \n" );
}
pr_timing("Started Monitor");

pr_output( "<h1>" ) if $HTML;
pr_output( $PROGNAME_ENGLISH," version $VERSION",$nl );
pr_output( "</h1>" ) if $HTML;

pr_output( "<b>" ) if $HTML;
pr_output( "  Run At : ".localtime(time),$nl );
pr_output( "  Input File: $INFILE",$nl );
pr_output( "</b>" ) if $HTML;

#my(@TestExceptions);
#open(EFILE,get_gem_root_dir()."/ADMIN_SCRIPTS/monitoring/ExceptPatternTest.dat") or die "Cant Read ExceptPatternTest.dat";
#while(<EFILE>) { chop; push @TestExceptions,$_; }
#close(EFILE);

# ============================
# get start patern
# ============================
pr_timing("Get Start Patern");
my($byte_count,$start_patern)=(0,"");
if( defined $PATFILE and -e $PATFILE ) {
	pr_output( "  Patern File: ".$PATFILE."$nl" );
	($byte_count,$start_patern) = log_rd_patern_file(file=>$PATFILE);
	my($patfiletime)= -M $PATFILE;
	$patfiletime *= 24;
	my($patfilestr) = "";
	if( $patfiletime >= 1 ) {
		$patfilestr .= int($patfiletime)." hours ";
		$patfiletime -= int($patfiletime);
	}
	$patfiletime *= 60;
	$patfilestr.= " ".int($patfiletime)." minutes";
	pr_output( "  Report Filter : Only Include Messages In The Past ",$patfilestr,$nl );
	pr_output( "  PATERN FILE byte count : $byte_count   ",$nl ) if defined $DEBUG;
	pr_output( "  PATERN FILE startpat   : $start_patern ",$nl ) if defined $DEBUG;
} elsif( defined $PATFILE )  {
	pr_output( "  Patern File: ".$PATFILE." (NOT FOUND)",$nl );
}

if( defined $TODAYONLY ) {
	pr_output( "  Time Restriction: Today and Yesterday",$nl );
	pr_timing("Setup Today Only");
	pr_output("Calling function log_grep()\n");
	log_grep( search_patern=>undef,today=>1,yesterday=>1,date_fmt=>$FILE_DATE_FMT );
	pr_output("Done    function log_grep()\n");
} else {
	pr_output( "  Time Restriction: none",$nl );
	pr_timing("Setup Time Restriction");
	pr_output("Calling function log_grep()\n");
	log_grep(search_patern=>undef,today=>undef,yesterday=>undef,date_fmt=>$FILE_DATE_FMT);
	pr_output("Done    function log_grep()\n");
}

if( defined $DEBUG ) {
	pr_output( "  SETTING DEBUG MODE",$nl );
	log_debug();
}

I_Am_Up();
if( defined $EXCLUDEFILE ) {
	pr_timing("Setup ExcludeFile");
	if( ! -r $EXCLUDEFILE ) {
		die "Cant Find Patern File $EXCLUDEFILE\n"
	      unless -r get_gem_root_dir()."/ADMIN_SCRIPTS/bin/".$EXCLUDEFILE;
		$EXCLUDEFILE=get_gem_root_dir()."/ADMIN_SCRIPTS/bin/".$EXCLUDEFILE;
	}
	pr_output("log_filter_file(file=>$EXCLUDEFILE)\n");
	log_filter_file(file=>$EXCLUDEFILE);
	pr_output("Completed log_filter_file(file=>$EXCLUDEFILE)\n");
}

my(%look_for_lines_by_host);	# for multi line pattern stuff... ignore stuff on key until value shows up
my(%look_for_line_linecount);	# linecount of start row - we only go 1000 lines after this before quiting

my($max_lines)=100000;
$max_lines = 10000 if defined $TODAYONLY;
$max_lines = $MAXLINES if $MAXLINES;
pr_output( "** PROCESSING FILE **",$nl ) if defined $DEBUG;
I_Am_Up();
pr_timing("Running log_process_file()");

pr_output("log_process_file()\n");

#sub I_Am_Up_With_Print {
#	print "I_Am_Up_With_Print() at ".localtime()."\n";
#	I_Am_Up();
#}

my($n_read,$n_returned,$found_pat,$returned_pat,$byte_cnt,@output_r)=
	log_process_file(
		file				=> $INFILE,
		search_patern	=> $start_patern,
		byte_offset		=> $byte_count,
#		i_am_up_func	=> \&I_Am_Up_With_Print,
		i_am_up_func	=> \&I_Am_Up,
		i_am_up_rows	=>	10000,
		cat_nogrep		=> 1,
		max_lines		=> $max_lines);
pr_output("Done log_process_file()\n");
pr_timing("Writing patern files");
I_Am_Up() unless $DEBUG2;

pr_output(    $nl."[ Read $n_read rows from $INFILE.  Filtered to $n_returned ]$nl");
if( $DEBUG ) {
	pr_output(    "[ DBG: found_pat=$found_pat (0=false 1=true)]$nl");
	pr_output(    "[ DBG: next_run_returned_pat=$returned_pat ]$nl");
	pr_output(    "[ DBG: next_run_byte_cnt=$byte_cnt ]$nl" );
}

if( $DEBUG2 ) {
	print "THE FOLLOWING MESSAGES PASSED FILTER TESTS\n";
	foreach (@output_r) {
		chomp;
		pr_output("$_\n");
	}
	pr_timing_report();
	exit(0);
}

if( ! defined $NOSAVEPATFILE and ! defined $DEBUG ) {
	log_wr_patern_file(patern=>$returned_pat, bytes=>$byte_cnt, file=>$PATFILE );
	pr_output( "WRITING PATERN FILE byte count = $byte_cnt   ",$nl ) if defined $DEBUG;
	pr_output( "WRITING PATERN FILE patern   = $returned_pat ",$nl ) if defined $DEBUG;
} else {
	pr_output( "NOT WRITING PATERN FILE - NOSAVE/DEBUG MODE",$nl );
	pr_output( "[ PATERN FILE ] byte count = $byte_cnt   ",$nl );
	pr_output( "[ PATERN FILE ] patern   = $returned_pat ",$nl );
}

# read the DATA section
pr_timing("Read <DATA>");
my @error_paterns;
foreach (<DATA>) {
	next if /^\s*$/ or /^#/;
	chomp;
	push @error_paterns,$_;
}

my(%months);
foreach ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec") { $months{$_}=1; }

#pr_output( "<b>TRUNCATING OUTPUT TO $max_lines LINES</b>$nl" )
#	if $#output_r>=($max_lines-1);

# AT THIS POINT YOU HAVE @output_r which contains your rows
my(%msgcat);

my(@outdata);
pr_output("Processing Returned Result Set",$nl) if $DEBUG;
pr_timing("Processing Result Set");
my($rc)=0;
foreach (@output_r) {
	$rc++;
	I_Am_Up() if $rc%1000 == 1;
	chomp;
	if( /^\(dbg\)/ ) {
		pr_output("DBG Msg: $_\n");
	} else {
		manage_message($_);
		$current_line_no++;
	}
}
pr_timing("Printing Report");
I_Am_Up();

pr_output("<TABLE BORDER=1>\n<TR><TH>Severity</TH><TH>Date</TH><TH>Host</TH><TH>SubSystem</TH><TH>Message</TH></TR>\n") if $HTML;
foreach ( reverse @outdata ) { pr_output( $_ ); }
pr_output("</TABLE>") if $HTML;
pr_output($nl."Program Completed... ",$nl);

pr_output( "<h1>" ) if $HTML;
pr_output( "\nMessage Category Report For File $INFILE\n" );
pr_output( "</h1>" ) if $HTML;

if( $HTML ) {
	pr_output("<TABLE BORDER=1>");
	pr_output( "<TR><TH>Category</TH><TH>Messages</TH></TR>\n" );
	pr_output( sprintf("<TR><TD>%s</TD>\n\t<TD>%s </TD></TR>\n", "# ROWS PARSED",$n_read)  );
	pr_output( sprintf("<TR><TD> %s</TD>\n\t<TD>%s </TD></TR>\n", "BASIC FILTERS REMOVE",$n_read - $n_returned)  );
	pr_output( sprintf("<TR><TD> %s</TD>\n\t<TD>%s</TD></TR>\n", "ROWS TO PROCESS",$n_returned)  );
	foreach (sort keys %msgcat) {
		pr_output( sprintf("<TR><TD>%s</TD>\n\t<TD>%s</TD></TR>", $_,$msgcat{$_},$nl)  );
	}
	pr_output("</TABLE>");
} else {
	pr_output( sprintf("Category %-27s : %s messages\n", "# ROWS PARSED",$n_read)  );
	pr_output( sprintf("Category %-27s : %s messages\n", "BASIC FILTERS REMOVE",$n_read - $n_returned)  );
	pr_output( sprintf("Category %-27s : %s messages\n", "ROWS TO PROCESS",$n_returned)  );
	foreach (sort keys %msgcat) {
		pr_output( sprintf("Category %-27s : %s messages %s", $_,$msgcat{$_},$nl)  );
	}
}


pr_output($nl);
pr_output( "<h1>" ) if $HTML;
pr_output("Printing Row Frequency for Frequent Rows (>100 ignored events)",$nl);
pr_output( "</h1>" ) if $HTML;

if( $HTML ) {
	pr_output("<TABLE BORDER=1>");
	pr_output( "<TR><TH>Msg Count</TH><TH>Host</TH><TH>Text</TH></TR>\n" );
	foreach ( sort keys %ignore_count ) {
		next unless $ignore_count{$_}>100;
		my($h,$m)=split(/:/,$_,2);
		pr_output( sprintf( "<TR><TD>%-6d</TD>\n\t<TD>%-12s</TD>\n\t<TD>%s</TD></TR>",$ignore_count{$_}, $h, $m));
	}
	pr_output("</TABLE>");
} else {
	foreach ( sort keys %ignore_count ) {
		next unless $ignore_count{$_}>100;
		my($h,$m)=split(/:/,$_,2);
		pr_output( sprintf( "Msg occurred %-6d Times: host=%-12s Text=%s %s",$ignore_count{$_}, $h, $m, $nl));
	}
	pr_output($nl);
}
#pr_output("Printing Hosts That Started but do not seem to have Finished Start\n");
#foreach (keys %look_for_lines_by_host) {
#	pr_output( $_,$nl );
#}
pr_timing("Completed Reports");
pr_output("Completed Reports $nl");
I_Am_Up();

if( $OUTFILE ) {
	pr_output($nl."Renaming temporary file $tmpfile to $OUTFILE.",$nl);
	close(OUTFILE);
	my($rc)=rename($tmpfile,$OUTFILE);
	if( ! $rc ) {
		warn "ERROR: cant rename $tmpfile to $OUTFILE : $!",$nl;
	} else {
		print "Renamed file to $OUTFILE",$nl;
		unlink $tmpfile if -r $tmpfile;
	}
}

MlpBatchJobEnd() unless defined $NOLOG;
I_Am_Down();

chmod( 0777, $OUTFILE ) if -w $OUTFILE;
pr_timing_report();

exit(0);

sub manage_message {
	my($line) = @_;
	next if $line=~/^\(dbg\)/;

	#pr_output("Managing Message\n") if $DEBUG;
	pr_output("\nProcessing: {$_}\n") if $DEBUG;
	#pr_output("==============================================\n");

	chomp $line;
	my($Mon,$Day,$Time,$Host,$SubSystem,$Text)=split(' ', $line, 6 );

	# what if the subsystem is actually a datestamp... yech... remove it.
	# not many messages do this but some sure do
	if( $months{ $SubSystem } ) {
		$SubSystem = "";
		$Text=~s/\d+\s\d+\s\d\d:\d\d:\d\d:*//;
	}

	if( $Text =~ /^\s*$/ ) {
		$msgcat{"BLANK MESSAGE IGNORED"}++;
		pr_output("[Blank] Ignoring $line\n") if $DEBUG;
		return;
	}

	my($Text2) = $Text;
	$Text2 =~ s/\d\d\d\d+/XXXX/g;

	# remove lines that arrive too close together
	if( $duplicates{$Host.$Text2} ) {
		if( $current_line_no < $duplicates{$Host.$Text2} + $max_line_sep ) {
			$ignore_count{$Host.":".$Text2}++;
			$duplicates{$Host.$Text2} = $current_line_no;
			$msgcat{"DUPLICATE MESSAGES IGNORED"}++;
			pr_output("[Duplicate] Ignoring $line\n") if $DEBUG;
			return;
		} else {
			$msgcat{"DUPLICATE MESSAGES SAVED"}++;
		}
	}
	$duplicates{$Host.$Text2} = $current_line_no;

	# handle multi line paterns (these are usually at server startup)
	if( defined $look_for_lines_by_host{$Host} ) {
		print "DBG ($Host): lineno=$current_line_no line=$line\n" 	if defined $DEBUG;
		if( $Text =~ /$look_for_lines_by_host{$Host}/
		or $look_for_line_linecount{$Host} < $current_line_no ) {
			if( $DEBUG ) {
				print "DBG ($Host): ENDING STARTUP LINES FOR $Host\n";
				print "  DBG ($Host): Text=$Text\n";
				print "  DBG ($Host): Cur Line = $current_line_no \n";
				print "  DBG ($Host): Look For Text is $look_for_lines_by_host{$Host}\n";
				print "  DBG ($Host): Max Look For Linecount is $look_for_line_linecount{$Host}\n";
			}
			$msgcat{"STARTUP MESSAGES IGNORED"}++;
			delete $look_for_lines_by_host{$Host};
			delete $look_for_line_linecount{$Host};
			pr_output("[Linux Startup] Ignoring $line\n") if $DEBUG;
			return;
		} else {
			$msgcat{"STARTUP MESSAGES IGNORED"}++;
			$look_for_line_linecount{$Host} = $current_line_no + $ML_MAX_LINES;
			print "DBG ($Host): setting Look for linenum $look_for_line_linecount{$Host}\n" if $DEBUG;
			pr_output("[Linux Startup] Ignoring $line\n") if $DEBUG;
			return;
		}
	}

	# handle routine unix messages
	if( 	        $Text=~/\d+\s(\w+)(\.warning)\]/
		or 	$Text=~/\d+\s(\w+)(\.error)\]/
		or 	$Text=~/\d+\s(\w+)(\.err)\]/
		or 	$Text=~/\d+\s(\w+)(\.notice)\]/
		or 	$Text=~/\d+\s(\w+)(\.alert)\]/
		or 	$Text=~/\d+\s(\w+)(\.crit)\]/
		or 	$Text=~/\d+\s(\w+)(\.info)\]/
		or 	$Text=~/\d+\s(\w+)(\.debug)\]/
		or 	$Text=~/\d+\s(\w+)(\.none)\]/
		or 	$Text=~/\d+\s(\w+)(\.emerg)\]/ ) {

			my($facility,$level)=($1,$2);

			if( $level eq ".debug" or $level eq ".none" ) {
				pr_output("[.debug/.none] Ignoring $line\n") if $DEBUG;
				return;
			}

			my($SEVERITY)="OK" 	if $level eq ".info" or $level eq ".notice";
			$SEVERITY="WARNING" 	if $level eq ".warning" or  $level eq ".alert";
			$SEVERITY="WARNING" 	if $level eq ".crit";
			$SEVERITY="ERROR"	if $level eq ".err" or $level eq ".error";
			$SEVERITY="CRITICAL"	if $level eq ".emerg";

	   		$msgcat{"UNIX MESSAGE (".$SEVERITY.")"}++;
			save_an_event($SEVERITY,$Mon,$Day,$Time,$Host,$SubSystem,$Text);
			return;

	}

	# some hard to generecize ignore messages
	if(
		( $SubSystem =~ /^named/ and $Text=~/^client/ and $Text=~/denied$/ ) or
		( $SubSystem eq "scsi:"  and $Text=~/WARNING: \/pci\@/ ) or
		( $SubSystem =~ /^rshd/ and $Text =~ /^PAM/ ) or
		( $Text =~ /^kernel:/ and $Text=~/OK$/ ) or
		( $Text =~ /^PAM/ ) or
	#	( $Text eq "qla2x00" ) or
		( $Text =~ /^BUG/ ) or
		( $SubSystem eq "kernel" and $Text=~/^\s*$/ )

	#	( $Text =~ /^PCI:/ ) or
	#	( $Text =~ /^mtrr:/ ) or
	#	( $Text =~ /^mapping CPU\#/ )
	) {
		$msgcat{"SPECIAL PARSE MESSAGES IGNORED"}++;
		pr_output("[Special Parse] Ignoring $line\n") if $DEBUG;
		return;
	}

	# finish up the code block for linux startup messages
	if( $Text =~ /^Linux version/ and /Red Hat Linux / ) {
		$msgcat{"STARTUP MESSAGES IGNORED"}++;
		$look_for_lines_by_host{$Host}="ip_tables: (C) ";
		$look_for_line_linecount{$Host}=$current_line_no + $ML_MAX_LINES;
		if( $DEBUG ) {
			print "DBG ($Host): Found Server Start Line (RED HAT LINUX)\n";
			print "DBG ($Host): line=$line\n";
			print "DBG ($Host): cur line = $current_line_no\n";
			print "DBG ($Host): setting Max Look for linenum $look_for_line_linecount{$Host}\n";
		}
		pr_output("[Linux Startup] Ignoring $line\n") if $DEBUG;
		return;
	}


	# SUBSYSTEM FILTERS
	if ( $SubSystem eq "dfstats.pl:" ) {
		$msgcat{$SubSystem}++;
		return;

		# from dfstats.pl - disk space monitor
		my($dummy,$pid,$sev,$fs,$pct,$sz,$usd,$avl);
		pr_output(  "==========================\n" );
		if( $Text=~/^\[/ ) {
			($dummy,$pid,$sev,$fs,$pct,$sz,$usd,$avl)=split(/\s+/, $Text);
			pr_output(  "> regular solaris dfstats.pl line\n" )
				if defined $DEBUG;
		} else {
			($fs,$pct,$sz,$usd,$avl)=split(/\s+/, $Text);
			my($p)=$pct;
			$p=~s/Pct=//;
			$sev=".warning]" if $p>=90;
			$sev=".err]" if $p>=97;
			pr_output(  "> regular linux dfstats.pl line setting sev to $sev\n" )
				if defined $DEBUG;
		}
		my($SEVERITY)="OK";
		$SEVERITY="WARNING" 	if $sev=~/\.warning\]/;
		$SEVERITY="ERROR"	  	if $sev=~/\.err\]/ or $sev=~/\.error\]/;
		$fs=~s/FS=//;
		my($msg)="$fs $pct $sz $usd $avl";
		print  "> dfstats.pl input=",$line,"\n";
		print  "> dfstats.pl system=$Host state=$SEVERITY subsystem=$fs msg=$msg\n";
		print  "==========================\n";
		Heartbeat(	-monitor_program	=> "dfstats.pl",
				-system				=> $Host,
				-state				=> $SEVERITY,
				-subsystem			=> $fs,
				-message_text		=> $msg );
	}

    	if (( $line =~ /Memory/  and $line =~ /available/ )
    	or (  $line =~ /memory/ and $line =~ /vmnet/ )
    	or (  $line =~ /memory/ and $line =~ /Freeing/ )
    	or (  $line =~ /memory/ and $line =~ /bttv/ )) {
    		$msgcat{"MEMORY MESSAGES IGNORED"}++;
		pr_output("[Memory] Ignoring $line\n") if $DEBUG;
		return;
	}

	my($severity)="WARNING";
	foreach ( @error_paterns ) {
		next unless $Text=~/$_/;
		$severity="ERROR";
		$msgcat{"MISC ERRORS SAVED"}++;
		last;
	}
	$msgcat{"MISC WARNINGS SAVED"}++ if $severity eq "WARNING";
	save_an_event ($severity,$Mon,$Day,$Time,$Host,$SubSystem,$Text);

	return;
}

sub save_an_event {
	my($severity,$Mon,$Day,$Time,$Host,$SubSystem,$Text)=@_;
	my($dt)=get_month_id($Mon,2)."/";
	$dt.="0" if length $Day == 1;
	$dt.=$Day."/".$year." ".$Time;

	$SubSystem=~s/\[*\d*\]*:$//;

	if( $HTML ) {
		my($color) = " BGCOLOR=PINK"
			if $severity eq "ERROR"	or $severity eq "CRITICAL";
		push @outdata, "<TR".$color."><TD>\n\t".
			$severity.
			"</TD>\n\t<TD>".
			$dt.
			"</TD>\n\t<TD>".
			$Host.
			"</TD>\n\t<TD>".
			$SubSystem.
			"</TD>\n\t<TD>".
			$Text.
			"</TD></TR>\n";
	} else {
		pr_output(
			join("|",($severity,$dt,$Host,$SubSystem,$Text)),$nl
		);
	}

	pr_output("Saving $severity Event $Host:$Text\n") if $DEBUG;
	MlpEvent( -monitor_program	=> $HB_MONITOR,
		-event_time		=> $dt,
		-system			=> $Host,
		-severity		=> $severity,
		-subsystem		=> $SubSystem,
		#-nosave=>1, -debug=>1,
		-message_text		=> $Text ) unless defined $NOLOG;
	return;
}

my(%time_endtime, %time_starttime, @timeorder);
my($curtiming);
sub pr_timing {
	my($timingstr)=@_;
	print "Started $timingstr at ".localtime(time)."\n";
	$time_endtime{$curtiming}=time if $curtiming;
	$curtiming=$timingstr;
	$time_starttime{$curtiming}=time;
	push @timeorder,$timingstr;
}
sub pr_timing_report {
	$time_endtime{$curtiming}=time if $curtiming;
	print "\nRUN TIME REPORT FOR BATCH\n";
	foreach ( @timeorder ) {
		printf "%20s : %s seconds\n", $_, ($time_endtime{$_}-$time_starttime{$_});
	}
}

sub pr_output {
	if( defined $OUTFILE ) {
		print OUTFILE @_;
		print @_ if defined $DEBUG;
	} else {
		print @_;
	}
}

__END__

=head1 NAME

unix_monitor.pl - Read Unix Logs And Alarm Appropriately

=head2 DESCRIPTION

This perl module parses and reads the Unix logs.  It needs some thought to
set up - it is recommended that you use a common syslogd host and then
just schedule this job once on the combined file.

=head2 USAGE

Usage: unix_monitor.pl --NOSAVEPATFILE --TODAYONLY --HTML --OUTFILE=file --EXCLUDEFILE=file --DEBUG --NOLOG [--PATFILE=file] --INFILE=file

 --HTML : html output
 --NOSAVEPATFILE : dont save the patern file when u are done
 --OUTFILE : do you want to save output to a file.  I would put this file into
   data/html_output so the console will automatically include it
 --EXCLUDEFILE : paterns to exclude.  A few .excl files are saved in the bin directory
 --NOLOG : dont save messages through the standard error log
 --TODAYONLY : a misnomer - it filters for today and yesterday

=head2 SAMPLE SCHEDULE

You should be sure to include the appropriate options when you schedule this job.

5,15,25,35,45,55 * * * * /usr/local/bin/perl-5.8.1 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/unix_monitor.pl --INFILE=/var/log/messages --TODAYONLY --EXCLUDEFILE=/apps/sybmon/dev/ADMIN_SCRIPTS/bin/solaris.excl --MAXLINES=100000000 --HTML --OUTFILE=/apps/sybmon/dev/data/html_output/unix_monitor.html >/apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/unix_monitor.log 2>/apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/unix_monitor.err

=cut

# THE DATA BLOCK DOWN BELOW WILL IDENTIFY PATERNS THAT WILL RESULT IN AN ERROR.  Other Paterns will
# result in a warning unless it is a syslogd message which has its own status attached.

__DATA__
atf_restore
exited due to signal 1
WARNING: \/pci
file system full
File system full
file system is full
realloccg
panic on cpu
panic on cpu 0: zero
[Pp]arity
[Mm]emory
low-memory static kernel pages
disk not responding to selection
unit does not respond to selection
needs maintenance
SCSI transport failed
fatal system fault
Multiple softerrors
out of dblks
out of mblks
tmp_memalloc: out of space
out of anon space
output_echo_char: out of blocks
out of virtual memory
out of kernelmap for devices
out of mapping resources
out of memory
out of physical memory
out of tmds - packet dropped
out of transmit buffers - packet dropped
out of cache slots
out of blocks
out of kernel memory
Out of Context
out of streams
table is full
mbuf map full
mbutl map full
kmem_alloc.* failed
mb_mapalloc failed
Cache Consistency Error
THERMAL WARNING DETECTED
killed due to lack of swap space
killed due to swap problems
Babble error
Receive: giant packet from
All tagged cron
queue max run limit exceeded
MAXRUN .* procs reached
Can\'t fork
server failing \(looping\), service terminated
blocking lock denied
rebooted
halted
requires attention
NIS server not responding
ax_isolated
pptp
ppp
vx
system restarted
SCSI disk error
sendmail down
port_monitor
SYS-1-
SYS-2-
SYS-5-SYS_RESET
SYS-5-SYS_INSUFFPWR
SYS-5-SYS_TIMECHNG
SYS-5-MOD_OK
SYS-5-MOD_RESET
SYS-5-MOD_PWRDEN
SYS-5-MOD_REMOVE
SYS-5-RELOAD
SNMP-5-COLDSTART
SNMP-5-CHASSISALARM
DTP-5-NONTRUNKPORTON
LINEPROTO-5-UPDOWN
MGMT-5-LOGIN_FAIL
MGMT-5-ENABLE_FAIL
Service Sendmail
vxfs msgcnt
Microcode Panic
Failed!
bs2 scsi: [ID 107833 kern.warning] WARNING:
NFS: failing over
log daemon terminating.
No space left on device
