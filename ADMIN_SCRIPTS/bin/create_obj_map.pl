#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com

use strict;
use Getopt::Std;
use File::Basename;

sub usage {
	my($msg)=@_;
	print "ERROR: $msg\n" if defined $msg;

	print
"Usage: $0 -UUSER -PPASSWD -SSVR -DDB -VTOP_DIR

DB is comma separated list of dbs to check out

Create a csv mapping based on
  - ddl files
  - objects in data dictionary

\n";

  exit(1);
}

use vars qw($opt_U $opt_S $opt_P $opt_D $opt_V );

# ================================
# TEST ARGUMENTS
# ================================
usage() if $#ARGV<0;
usage("Bad Parameter List") unless getopts('U:S:P:D:V:');

usage("Must pass database\n" )  unless defined $opt_D;
usage("Must pass password\n" )  unless defined $opt_P;
usage("Must pass server\n")	  unless defined $opt_S;
usage("Must pass username\n" )  unless defined $opt_U;

die("$opt_V/tables not a directory\n" )  unless -d $opt_V."/tables";
die("$opt_V/procs not a directory\n" )  unless -d $opt_V."/procs";
die("$opt_V/views not a directory\n" )  unless -d $opt_V."/views";
die("$opt_V/trigs not a directory\n" )  unless -d $opt_V."/trigs";

my($curdir)=dirname($0);

use Sybase::SybFunc;

my(%TABLES,%TRIGGERS,%VIEWS,%PROCS);
my(%DEFTABLES,%DEFTRIGGERS,%DEFVIEWS,%DEFPROCS);
my(@suffixes)=(".sql",".prc",".proc",".view",".vie",".tbl",".trig");
foreach ( dirlist($opt_V."/tables",1 )) {
	$TABLES{basename($_,@suffixes)}=0;
	$TABLES{basename($_,@suffixes)}=32 if /defunct/;
}

foreach ( dirlist($opt_V."/procs",1 ))  {
	$PROCS{basename($_,@suffixes)}=0;
	$PROCS{basename($_,@suffixes)}=32 if /defunct/;
}

foreach ( dirlist($opt_V."/views",1 ))  {
	$VIEWS{basename($_,@suffixes)}=0;
	$VIEWS{basename($_,@suffixes)}=32 if /defunct/;
}

foreach ( dirlist($opt_V."/trigs",1 ))  {
	$TRIGGERS{basename($_,@suffixes)}=0;
	$TRIGGERS{basename($_,@suffixes)}=32 if /defunct/;
}

# CONNECT TO DATABASE
db_connect($opt_S,$opt_U,$opt_P)
        or die "Cant connect to $opt_S as $opt_U\n";

my($count)=1;
foreach my $db ( split(/,/,$opt_D) ) {

 	my(@objdata)=db_query($db,"select name from sysobjects where type='TR'");
	foreach (@objdata) {
		if( defined $TRIGGERS{$_} ) {
			$TRIGGERS{$_} += $count;
		} elsif( defined $TRIGGERS{"***".$_."***"} ) {
			$TRIGGERS{"***".$_."***"} += $count;
		} else {
			$TRIGGERS{"***".$_."***"} = $count;
		}
	}

 	@objdata=db_query($db,"select name from sysobjects where type='U'");
	foreach (@objdata) {
		if( defined $TABLES{$_} ) {
			$TABLES{$_} += $count;
		} elsif( defined $TABLES{"***".$_."***"} ) {
			$TABLES{"***".$_."***"} += $count;
		} else {
			$TABLES{"***".$_."***"} = $count;
		}
	}

 	@objdata=db_query($db,"select name from sysobjects where type='V'");
	foreach (@objdata) {
		if( defined $VIEWS{$_} ) {
			$VIEWS{$_} += $count;
		} elsif( defined $VIEWS{"***".$_."***"} ) {
			$VIEWS{"***".$_."***"} += $count;
		} else {
			$VIEWS{"***".$_."***"} = $count;
		}
	}

 	@objdata=db_query($db,"select name from sysobjects where type='P'");
	foreach (@objdata) {
		if( defined $PROCS{$_} ) {
			$PROCS{$_} += $count;
		} elsif( defined $PROCS{"***".$_."***"} ) {
			$PROCS{"***".$_."***"} += $count;
		} else {
			$PROCS{"***".$_."***"} = $count;
		}
	}
	$count *= 2;
}

db_close_sybase();

my($headerline)="\nKey,Object";
foreach my $db ( split(/,/,$opt_D) ) { $headerline.=",$db"; }
$headerline.="\n";

showgrp("TABL",%TABLES);
showgrp("TRIG",%TRIGGERS);
showgrp("VIEW",%VIEWS);
showgrp("PROC",%PROCS);

sub showgrp
{
my($hdr,%arr)=@_;
print $headerline;
foreach ( sort keys %arr )    {
	print $hdr,",", $_ ,",";
	# print $arr{$_},",";
	print ( (($arr{$_} & 0x1) == 1 ) ? "YES," : ",") ;
	print ( (($arr{$_} & 0x2) == 2 ) ? "YES," : ",") ;
	print ( (($arr{$_} & 0x4) == 4 ) ? "YES," : ",") ;
	print ( (($arr{$_} & 8) == 8 ) ? "YES," : ",") ;
	print ( (($arr{$_} & 16) == 16 ) ? "YES," : ",") ;
	print ( (($arr{$_} & 32) == 32 ) ? "YES," : ",") ;
	print "OBJECT NOT IN SERVER," if $arr{$_} == 0;
	print "OBJECT NOT IN DDL,"    if $_ =~ /^\*\*\*/;
	print "\n";
}
}

sub dirlist
{
        my($dir,$recurse) = @_;
        opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
        my(@files)=readdir(DIR);
        closedir(DIR);

        my(@out);
        foreach (@files) {
               next if /^\./;
					next if /^install.sh$/ or /^weather$/;
               if( -d $dir."/".$_ and $recurse ) {
                        push @out,dirlist($dir."/".$_,1);
               } else {
                        push @out,$dir."/".$_;
               }
        }
        return @out;
}

