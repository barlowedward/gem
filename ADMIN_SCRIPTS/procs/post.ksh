isql -Usa -P$1 -S$2 << EOF
sp_addlogin dba_monitoring,mygemr0cks
go
use sybsystemprocs
go
sp_adduser dba_monitoring
go
grant execute on sp__auditsecurity to dba_monitoring
grant execute on sp__auditdb to dba_monitoring
go
use master
go
grant select on syslogshold to public
go
