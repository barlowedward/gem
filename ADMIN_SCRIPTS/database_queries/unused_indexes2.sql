-- report_key=UNUSED_INDEXES2
-- report_type=HTML_AGGREGATE
-- run_frequency=daily
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- server_min_version=9
-- server_max_version=
-- exclude_server_csv=
-- run_on_server_csv=
-- run_query_each_db=Y
-- run_query_in_sys_db=N
-- report_header=This Report Lists Indexes that have never been used (alt)
-- report_footer=
-- enabled=Y
-- outfile=Unused_Indexes2

select	@@SERVERNAME as servername,
		db_name() as [dbname],
		object_schema_name(i.object_id) as SchemaName,
		object_name(i.object_id) as ObjectName,
		i.name as IndexName
from sys.indexes i left outer join sys.dm_db_index_usage_stats u
				on  u.object_id= i.object_id
				and u.index_id = i.index_id
				and database_id=db_id()
where i.index_id>0
and isnull(u.last_user_seek,0)=0
and isnull(u.last_user_scan,0)=0
and isnull(u.last_user_lookup,0)=0
and objectproperty(i.object_id,'IsUserTable')=1
order by SchemaName, ObjectName, IndexName
