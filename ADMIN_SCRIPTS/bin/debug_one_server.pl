#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use Getopt::Std;
use strict;
use DBIFunc;
use CommonFunc;
use MlpAlarm;
use Repository;

my $space_threshold=80;

use vars qw($opt_h $opt_d $opt_S $opt_U $opt_P $opt_q $opt_e $opt_A);

sub usage
{
  return "@_
	USAGE: debug_one_server.pl [-edhq] -UUSER -PPASSWORD -SSERVER
		-q is quiet mode - no status messages printed
		-h html output
		-d debug mode
		-AProgram - monitor this server
		-e errors only (no trivial warnings)
";
}

$|=1;
getopts('eqdhS:U:P:A:') or die usage("Bad Parameter List");
my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

my($opt_M);
if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}

if( defined $opt_S and ! defined $opt_U ) {
	use Repository;
	($opt_U,$opt_P)=get_password(-type=>"sybase",-name=>$opt_S);
	($opt_U,$opt_P)=get_password(-type=>"sqlsvr",-name=>$opt_S)
			unless $opt_U;
}

if( !defined($opt_S)) { die usage("Error: Must Pass Server\n"); }
if( !defined($opt_U)) { die usage("Error: Must Pass User\n"); }
if( !defined($opt_P)) { die usage("Error: Must Pass Password\n"); }

print "debug_one_server.pl: Database Debugger V2.02",$NL,$NL unless defined $opt_q;

print "Connecting to sybase server $opt_S $NL" if defined $opt_d and ! defined $opt_q;
dbi_connect(-srv=>$opt_S,-type=>$opt_M,-login=>$opt_U,-password=>$opt_P, -type=>"ODBC")
	or die "Cant connect to $opt_S as $opt_U\n";

dbi_set_web_page(undef) unless defined $opt_h;

my($server_type)="SQLSERVER";
foreach( dbi_query(-db=>"master", -query=>"select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
	($server_type)=dbi_decode_row($_);
}
print "Target Server: $opt_S $NL";
print "Target Server Type: $server_type $NL";
dbi_msg_exclude("String or binary data would be truncated.") if $server_type eq "SQLSERVER";

my($vl,$required_proclib)=get_version(get_gem_root_dir()."/ADMIN_SCRIPTS/procs/VERSION" );
print "Required Procedure Library Version = ",$required_proclib,"\n";

print "Fetching Datbase Type/Version\n" if defined $opt_d;
my($DBTYPE,$DBVERSION)=dbi_db_version();
print "Type=$DBTYPE\nVERSION=$DBVERSION\n" if defined $opt_d;

print "Is Proclib Installed? $NL" if defined $opt_d and ! defined $opt_q;
my($is_installed);
if( $DBTYPE ne "SQL SERVER" ) {
	my(@rc)=dbi_query(-db=>"sybsystemprocs",-query=>"select name from sysobjects where name='sp__proclib_version'");
	foreach(@rc) { my(@row)=dbi_decode_row($_); $is_installed=1 if $row[0] eq "sp__proclib_version"; }
} else {
	my(@rc)=dbi_query(-db=>"master",-query=>"select name from sysobjects where name='sp__proclib_version'");
	foreach(@rc) { my(@row)=dbi_decode_row($_); $is_installed=1 if $row[0] eq "sp__proclib_version"; }
}
if( ! $is_installed ) {
	MlpHeartbeat(
			-debug=>$opt_d,
			-state=>"ERROR",
		 	-monitor_program=>$opt_A,
			-system=>$opt_S,
			-subsystem=>"ProcLib",
			-message_text=>"Stored Procedure Library Is Not Installed") if $opt_A;
	die "Procedure Library is NOT installed";
}

print "Testing Extended Stored Procedure Version $NL" if defined $opt_d and ! defined $opt_q;
my(@rc)=dbi_query(-db=>"master",-query=>"exec sp__proclib_version");
my(@version);
foreach(@rc) { @version=dbi_decode_row($_); }
print "Procedure Version is $version[0]\n" if defined $opt_d;

if( $opt_A ) {
	if( $version[0] < $required_proclib ) {
		print "Outdated Version - $version[0] - required version=$required_proclib \n";
		MlpHeartbeat(
			-debug=>$opt_d,
			-state=>"ERROR",
		 	-monitor_program=>$opt_A,
			-system=>$opt_S,
			-subsystem=>"ProcLib",
			-message_text=>"Outdated Stored Procedure Library - version=$version[0] (should be $required_proclib)");

		die( "Error - You Should be using the Extended Stored Procedure Library Version $required_proclib or later.  Found version $version[0]. The latest can be downloaded from http://www.edbarlow.com\n" )
			if ! $version[0] or $version[0]<= 6.2;
	} else {
		print "Ok Version - $version[0] - req version=$required_proclib \n";
		MlpHeartbeat(
			-debug=>$opt_d,
			-state=>"OK",
		 	-monitor_program=>$opt_A,
			-system=>$opt_S,
			-subsystem=>"ProcLib",
			-message_text=>"Stored Procedure Library Version Up To Date");
	}
}

#
# GET DATABASES WITH DATA & LOG ON SAME DEVICE
#
my(%db_log_data_same_device);
if( $DBTYPE ne "SQL SERVER" ) {
	foreach( dbi_query(-db=>"master",-query=>"select distinct db_name(dbid) from master..sysusages where segmap&7=7")) {
		my(@d)=dbi_decode_row($_);
		$db_log_data_same_device{$d[0]} = 1;
	}
}

my(%status,%status2,@databaselist);
print "Retrieving Database info $NL" if defined $opt_d and ! defined $opt_q;
foreach( dbi_query(-db=>"master",
	-query=>"select name,si=status,status2&(16+32+64) from master..sysdatabases
	where status & 0x1120 = 0 and status2&0x0030=0 and status2&16!=16")) {
		my @dat=dbi_decode_row($_);
		push @databaselist,$dat[0];
		$status{$dat[0]}=$dat[1];
		$status2{$dat[0]}=abs($dat[2]);
}

print "Auditing $opt_S using sp__auditsecurity $NL" if defined $opt_d and ! defined $opt_q;
my(@audit_res)=dbi_query(-db=>"master",-query=>"exec sp__auditsecurity 1,\"$opt_S\",\"hostname\",\"Y\"");

if( $DBTYPE ne "SQL SERVER" ) {
	print "Checking Process Table $NL" if defined $opt_d and ! defined $opt_q;
	foreach ( dbi_query(-db=>"master",-query=>"select spid,cmd,blocked,status from sysprocesses ") ) {
		my @dat=dbi_decode_row($_);
		push @audit_res,dbi_encode_row("hostname","servername","10101","master","blocked","date","Blocked Process (spid=".$dat[0].")") if $dat[2] > 0;
		push @audit_res,dbi_encode_row("hostname","servername","10102","master","suspend","date","LOG SUSPEND (spid=".$dat[0].")") if $dat[1] =~ /LOG/ or $dat[3] =~ /^log/;
		push @audit_res,dbi_encode_row("hostname","servername","10104","master","stopped","date","Process Stopped (spid=".$dat[0].")") if $dat[3] =~ /^stopped/;
		push @audit_res,dbi_encode_row("hostname","servername","10105","master","infected","date","Process Infected (spid=".$dat[0].")") if $dat[3] =~ /^infected/;
	}
}


my(%alerts);
dbi_set_mode("INLINE");
foreach my $db (sort @databaselist) {
	next if $db eq "tempdb" or $db eq "model";

	$alerts{$db}="";
	print "Running Audit on $db ( exec sp__auditdb \"$opt_S\",\"hostname\") \n" if defined $opt_d and ! defined $opt_q;
	foreach( dbi_query(-db=>$db,-query=>"exec sp__auditdb \"$opt_S\",\"hostname\"")) {
		push @audit_res,$_;
		#my(@d)=dbi_decode_row($_);
		#print "RETURNED ",join(" ",@d),"\n";
	}

#	foreach (@audit_res) {
#		my @dat=dbi_decode_row($_);
#		print "++ ",join(" ",@dat),"\n";
#	}
#	die;


	if( $DBTYPE ne "SQL SERVER" ) {
		print "Checking Space on $db \n" if defined $opt_d;
		foreach( dbi_query(-db=>$db,-query=>"exec sp__qspace")) {
			my @space=dbi_decode_row($_);
			$space[1]=int $space[1];
			$space[2]=int $space[2];
			push @audit_res,dbi_encode_row("hostname","servername","10001","$db","space","date","Data Space at $space[1]%")
				if $space[1]>$space_threshold;
			push @audit_res,dbi_encode_row("hostname","servername","10002","$db","space","date","Log Space at $space[2]%")
				if $space[2]>$space_threshold;
		}
	}

#	push @audit_res,dbi_encode_row("hostname","servername","10003","$db","options","date","Single User Mode set")
#		if ($status{$db}&4096) == 4096;
#	push @audit_res,dbi_encode_row("hostname","servername","10004","$db","options","date","Dbo Use Only")
#		if ($status{$db}&2048) == 2048;
#	push @audit_res,dbi_encode_row("hostname","servername","10005","$db","options","date","Read Only")
#		if ($status{$db}&1024) == 1024;
	push @audit_res,dbi_encode_row("hostname","servername","10006","$db","options","date","Created For Load")
		if ($status{$db}&32) == 32;
	push @audit_res,dbi_encode_row("hostname","servername","10007","$db","options","date","Database Suspect")
		if ($status{$db}&256) == 256;
	push @audit_res,dbi_encode_row("hostname","servername","10008","$db","options","date","Database Offline"  .$status2{$db}." ".($status2{$db}&16))
		if ($status2{$db}&16) == 16;
	push @audit_res,dbi_encode_row("hostname","servername","10009","$db","options","date","Offline During Recovery"  .$status2{$db}." ".($status2{$db}&16))
		if ($status2{$db}&32) == 32;
	push @audit_res,dbi_encode_row("hostname","servername","10010","$db","options","date","Database is being recovered"  .$status2{$db}." ".($status2{$db}&16))
		if ($status2{$db}&64) == 64;

	next if		$db eq 'model'
		or $db eq 'sybsecurity'
		or $db eq 'sybsyntax'
		or $db eq 'sybsystemprocs'
		or $db eq 'master'
		or $db eq 'sybsystemdb'
		or $db eq 'tempdb';

	# IF DATA&LOG Same device then
	# IF tl!=si then warn
	# if mixed data & log then must have si and tl else si=tl
	if ( defined $db_log_data_same_device{$db} ) {
		push @audit_res,dbi_encode_row("hostname","servername","10011","$db","options","date","Data&Log On Same Device and trunc log off")  unless ($status{$db} & 8) == 8;
		# push @audit_res,dbi_encode_row("hostname","servername","10012","$db","options","date","Data&Log On Same Device and select into off.") unless ($status{$db} & 4) == 4;
	} else {
		if ( ($status{$db}&12) == 4 )  {
			push @audit_res,dbi_encode_row("hostname","servername","10013","$db","options","date","Select Into != Trunc Log On Checkpoint");
		}
	}
}

print "Warning - Extended Stored Procedure Library Version should be $required_proclib.  Found version $version[0]. Please upgrade.\n"
	if $version[0]< $required_proclib;

# Format of audit_res is hostname,servername,error_no,db,type,date,message
print "SUCCESS! NO AUDITING PROBLEMS FOUND $NL"
	if defined $opt_q and $#audit_res<1;

my($hdr)=0;
foreach (@audit_res) {
	my @dat=dbi_decode_row($_);
	$dat[4]="" if $dat[4] eq "a";
	$dat[6]=~ s/\s\s+/ /g;
	$dat[2]=~ s/\s/ /g;

	# 32105	user is member of public
	# 32106	public execute access
	# 32104	no groups in db
	# 31004	master database as default
	next if defined $opt_e and ( $dat[2] == 32105 or $dat[2] == 32106 or $dat[2] == 32104 or $dat[2] == 31004 );

	print "<TABLE BORDER=1>" if defined $opt_h and $hdr==0;
	printf("Printing Audit Results...\n%5s %15s %s\n","Msg","Database","Violation") if $hdr==0 and ! defined $opt_h and ! defined $opt_q;
	print "<TR><TH>Message</TH><TH>Database</TD><TH>Violation</TH></TR>" if $hdr==0 and defined $opt_h;
	$hdr++;

	if( $dat[3]=~/^\s*$/ and $dat[4]=~/^\s*$/ and $dat[6]=~/^\s*$/) {
		printf("<TR><TD>ERROR</TD><TD></TD><TD>%s</TD></TR>\n",join("",@dat));
		next;
	}

	if( defined $opt_h ) {
		printf("<TR><TD>%s</TD><TD>%s</TD><TD>%s</TD></TR>\n",$dat[2],$dat[3],$dat[6]);
	} else {
		printf("%5s %15s %s\n",$dat[2],$dat[3],$dat[6]);
	}

	my($msgid)=$dat[2];
	next unless defined $opt_A;
	next if 	   $msgid==31101 # user owns objects
			or $msgid==32104 # *
			or $msgid==32105 # *
			or $msgid==31004 # master db as default
			or $msgid==32104 # no groups exist in db

			or $msgid==31010 # allow updates set
			or $msgid==32005 # allow updates
			or $msgid==32108 # allow updates
			or $msgid==31010 # allow updates set
			or $msgid==31013 # db created for laod
			or $msgid==32106 # Group Public Access To Object
			or $msgid==10002
			or $msgid==10001
	  		or $msgid==31004 # *
	  		or $msgid==31012 # sa user trusted
	  		or $msgid==31009 # bad password
	  		or $msgid==31001;# locked login

	warn "$opt_S: Warning Database For Alert ",join(",",@dat)," is null" if $dat[3]=~/^\s*$/;
	$dat[6] =~ s/ERROR://;
	$alerts{$dat[3]}.=$dat[6];
}

foreach ( keys %alerts ) {
	my($state)="OK";
	$state="WARNING" if $alerts{$_} ne "";
	MlpHeartbeat(
		-debug=>$opt_d,
		-state=>$state,
	 	-monitor_program=>$opt_A,
		-system=>$opt_S,
		-subsystem=>$_,
		-message_text=>$alerts{$_}
	) if defined $opt_A;
}

print "</TABLE>\n\n" if defined $opt_h and $hdr>0;
print "Program Completed.\n" unless defined $opt_q;
exit(0);

__END__

=head1 NAME

debug_one_server.pl - utility to debug servers

=head2 DESCRIPTION

Check out your server!  Should audit as much as it can!

=head2 USAGE

	debug_one_server.pl [-dh] -UUSER -PPASSWORD -SSERVER

=head2 DETAILS

	* Checks stored procedure library
	* run sp__auditsecurity
	* check wierd select_into/trunc option definition on databases
	* checks sysprocesses for blocked, log suspend, infected, sleeping io,
		or stopped status
	* foreach database
	->  space used (data&log) from sp__qspace
	->  sp__auditdb
	->  database options

  database options

  configuration (default memory, allow updates, connections, updates)
	 -> not implemented
=cut
