#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;

foreach (@ARGV) {
        die "NO FILE" unless -e $_;
        clean_ctrl_char($_);
}

sub clean_ctrl_char
{
        my($file)=@_;

        open(FILE,$file) or die "Cant open $file\n";
        open(OUTFILE,">".$file.".out") or die "Cant open $file.out\n";
        while ( <FILE> ) {
                my $str = $_;
                my $val = ord;
                foreach ( split(//,$str) ) {
                        print OUTFILE if /\w/ or /\s/ or /\d/ or /\*/ or /[\!\@\#\$\%^\&\*\(\)\_\+\=\-\[\]\\\{\}\|\;\'\:\"\,\.\<\>\/\?\~\`]/;

                }
        }
        close FILE;
        close OUTFILE;

        unlink $file or die "Cant remove $file";
        rename($file.".out",$file)
                or die "Cant rename $file.out to $file: $!";
}

__END__

=head1 NAME

clean_ctrl_char.pl - cleanup control characters from files

=head2 USAGE

clean_ctrl_char.pl FILE FILE FILE ...

=head2 DESCRIPTION

Clean up control characters from files.

=cut
