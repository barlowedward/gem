
-- report_key=RunningThreads
-- run_frequency=15min
-- do_diff=N
-- keep_hours=Y
-- keep_6=Y
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=
-- server_max_version=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=
-- report_footer=
-- enabled=Y
-- performancetype=int
-- performancekey=Running_Threads
-- colname_csv

select distinct  @@SERVERNAME as server_name, dbname = db_name(dbid), '','', count(*)
	        	from   master..sysprocesses
	        	where  cmd not in ( 'AWAITING COMMAND', 'NETWORK HANDLER', 'MIRROR HANDLER',
	              'AUDIT PROCESS','CHECKPOINT SLEEP','TASK MANAGER','LOG WRITER',
						'LAZY WRITER','SIGNAL HANDLER','LOCK MONITOR','CHECKPOINT',
						'BRKR TASK','BRKR EVENT HNDL','DB MIRROR','RESOURCE MONITOR', 'TRACE QUEUE TASK', 'BRKR EVENT HNDLR' )
						and  loginame != 'sa'
						group by db_name(dbid)
