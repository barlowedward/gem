#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use CGI qw( :standard escapeHTML escape);
use CGI::Carp qw(fatalsToBrowser);
use CommonFunc;

$|=1;

cd_home();
my($logfile)='server.log';
open(LOG,">> $logfile") or die "Cant Write Logfile \n";
print LOG localtime(time).":";
foreach(param()) {
	print LOG " ",$_,"=>",param($_);
}
my($op)=param('op');
print LOG " ---> operation = $op\n";
print LOG "\n";
close(LOG);

if( $op eq "catalog" ) {
   print header;
	if( -r "plugins/catalog.xml" ) {
		open(PLUGINS,"plugins/catalog.xml");
		foreach ( <PLUGINS> ) { print; }
		close PLUGINS;
	} else {
		print "Error: No Catalog Found",br;
	}
} elsif( $op eq "register" ) {
	my($new_name)=param('name');
	my($new_company)=param('company');
	my($new_phone)=param('phone');
	my($new_email)=param('email');
	open(REG,"register.dat");
	my($ID, $maxid);
	foreach(<REG>) {
		my($id,$name,$company,$phone,$email)=split(/~~/);
		if( $email eq $new_email
		and $company eq $new_company
		and $phone eq $phone
		and $name eq $name ) 		{ $ID=$id; last; }
		$maxid = $id if $maxid>$id;
	}
	close REG;
	if( ! defined $ID ) {
		$maxid++;
		$ID=$maxid;
		open(REG2,">>register.dat");
		print REG2 join(/~~/,	$maxid,
									$new_name,
									$new_company,
									$new_phone,
									$new_email);
		close REG2;
	}
   print header;
	print "<ID value=$ID/>"
} elsif( $op eq "fetch" ) {
   print header;
	my($file)=param('file').".pm";
	if( -r "plugins/".$file ) {
		open(PLUGINS,"plugins/".$file);
		foreach ( <PLUGINS> ) {
			chomp;
			print $_,"\n";
		}
		close PLUGINS;
	} else {
		print "Error: No File ".$file." Found",br;
	}
}

__END__

=head1 NAME

server.pl - web server manager

=head2 DESCRIPTION

Handles registration and plugin management.  Options are as follows

Key option: op=register|fetch|catalog

  register:  	name=xxx company=xxx phone=xxx email=xxx
		returns your sustomer id

  fetch:  	file=package custidid=id

  catalog: 	shows the catalog

=cut

