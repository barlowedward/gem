#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
#use MlpAlarm;
use CommonFunc;
use File::Basename;

use vars qw( $SUBJECT $TYPE $TABULATE $DATABASE $MAILFROM $PRINTHDR $HTML $SMTPSERVER $MAILTO $QUERY $USER $SERVER $PASSWORD %CONFIG $opt_d $SUBJECT );

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

usage ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"QUERY=s"		=> \$QUERY ,
		"USER=s"			=> \$USER ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"DATABASE=s" 	=> \$DATABASE ,
		"HTML" 		=> \$HTML ,
		"PRINTHDR" 	=> \$PRINTHDR ,
		"TYPE=s" 		=> \$TYPE ,
		"MAILTO=s" 		=> \$MAILTO ,
		"SMTPSERVER=s"	=> \$SMTPSERVER ,
		"SUBJECT=s"		=> \$SUBJECT ,
		"MAILFROM=s" 	=> \$MAILFROM ,
		"TABULATE" 		=> \$TABULATE ,
		"DEBUG"      	=> \$opt_d );

sub usage {
	print @_;
	die "RunQuery.pl --SERVER=xxx --USER=xxx --PASSWORD=xxx --DATABASE=db
			--HTML --QUERY=xxx --TABULATE --SMTPSERVER=x SUBJECT=x

--TABULATE will print the output formatted by columns (spaces)\n";
}
usage("Must pass server\n" ) 		   unless defined $SERVER;
usage("Must pass username\n" )	  	unless defined $USER;
usage("Must pass password\n" )	  	unless defined $PASSWORD;
usage("Must pass query\n" )	  		unless defined $QUERY;

# $TYPE="Sybase" unless defined $TYPE;
dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD, -type=>$TYPE)
	or die "Cant connect to $SERVER as $USER\n";

dbi_set_mode("INLINE");
$DATABASE="master" unless $DATABASE;

my(%argforquery);
$argforquery{-print_hdr}=1 if $PRINTHDR;
# $argforquery{-add_batchid}=1  if $PRINTHDR;
my(@rc);
if($HTML) {
	@rc=dbi_query_to_table(-db=>$DATABASE,-query=>$QUERY,-table_options=>"border=1",%argforquery);
} else {
	@rc=dbi_query(-db=>$DATABASE,-query=>$QUERY,%argforquery);
}

my(@OUTPUT);
sub output {
	my($str)=join("",@_);
	print $str;
	push @OUTPUT,$str;
}
my($NL)="\n";
$NL="<br>\n" if $HTML;

if( $#rc<0 ) {
	output("NO RESULTS FOUND FROM THE QUERY\n");
} elsif( $TABULATE ) {
	output( join($NL,dbi_reformat_results(@rc)).$NL);
} elsif( $HTML ) {
	foreach( @rc) {
		output( join(" ",$_) );
	}
} else {
	foreach( @rc) {
		my @dat=dbi_decode_row($_);
		output( join("\t",@dat),$NL );
	}
}

dbi_disconnect();

if( $MAILTO and $#rc>=0 ) {
	die "MAILFROM IS NOT SET" unless $MAILFROM;
	die "MAILTO IS NOT SET" unless $MAILTO;
	die "SMTPSERVER IS NOT SET ON WINDOWS" if ! $SMTPSERVER and $^O =~ /MSWin32/;
	$SUBJECT='NO SUBJECT DEFINED' unless $SUBJECT;
	my($msgtxt)=join("",@OUTPUT);
	send_mail_message($MAILTO,$MAILFROM,$SMTPSERVER,$SUBJECT,$msgtxt)
}
exit(0);

__END__

=head1 NAME

runquery.pl - rum a query on a server

=head2 USAGE

RunQuery.pl --SERVER=xxx --USER=xxx --PASSWORD=xxx --DATABASE=db
  --HTML --QUERY=xxx --TABULATE

  --TABULATE will print the output formatted by columns (spaces)

  --MAILTO will mail the output to the users ONLY if the query returns >= 1 row.
    This can be used for alarming.!

=head2 DESCRIPTION

This is a very basic program that uses standard arguments.  It
takes a passed in query, runs it, and prints the output to standard
output with minimal formatting.  Currently, this is only useful to
test connectivity.

If --TABULATE passed it pretty prints the output which is probably what
you want.  If its not passed, a tab is used as a delimeter.

=head2 EXAMPLE

	perl RunQuery.pl --SERVER=SYBPROD -QUERY=sp__id --DATABASE=master --USER=sa --PASSWORD=xxx --SUBJECT=hi --MAILTO=noname@abc.com --MAILFROM=a@b.com --SMTPSERVER=xxx.xxx.com

=cut
