#!/usr/local/bin/perl-5.6.1

use strict;
use File::Basename;
use DBI;
use Getopt::Std;

use vars qw($opt_S $opt_U $opt_P $opt_d);

die "DBIping.pl - Simple utility to test your DBI install

usage DBIping.pl [-d] -Uuser -Ppass -SSERVER\n" if $#ARGV<0 or ! getopts('S:U:P:d');

#die "Must pass -Uuser" unless $opt_U;
#die "Must pass -Ppass" unless $opt_P;
die "Must pass -Ssrvr" unless $opt_S;

$|=1;

DBI->trace(1) if $opt_d;

my($ctype)="Sybase";
$ctype="ODBC" if defined $ENV{PROCESSOR_LEVEL};

my($dbh)= DBI->connect("dbi:$ctype:$opt_S",$opt_U,$opt_P);
if( defined $dbh ) {
	print "Successful Connection Made To $opt_S\n";
	my($ary_ref)=$dbh->selectall_arrayref("select name from sysobjects where name in ('sysobjects','sysusers','syscomments')");
	my($cnt)=0;
	foreach my $row (@$ary_ref) {
		$cnt++;
		#print "--->",join(" ",@$row),"\n";
	}
	$dbh->disconnect();
	if( $cnt==3 ) {
		print "Successful Query Made On $opt_S\n";
		exit(0);
	} else {
		print "Error: Connect looks successful but basic Sybase/SQL Server query did not return correctly\n";
		exit(1);
	}
} else {
	print "ERROR: FAILED TO CONNECT to $ctype server $opt_S\n";
	exit(1);
}

__END__

=head1 NAME

DBIping.pl - Simple utility to test your DBI install

=head2 USAGE

usage DBIping.pl [-d] -Uuser -Ppass -SSERVER\n";

=head2 SYNOPSIS

Very simple utility that just logs in to your server.  Should work if you
have a good DBI installation.  If not, try running with -d option and check
out your DBI and DBD libraries.

=cut

