#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
use strict;
use Getopt::Std;
use DBIFunc;
use CommonFunc;
use Repository;
my($curdir)=dirname($0);

sub usage
{
        print @_;
        print
"copy_syslogins.pl  -Fsrv [ -Uusr -Ppass ] -tsrv [ -uusr -ppass ] -nM -Bbcp_cmd

-o (override version mismatch)
-Acnt append with suid=suid+cnt
-M (microsoft sql server)
-n noexec
Note the syntax -F (FROM) server with login/pass from (-U/-P)
           and  -t (to) server with login/pass from (-u/-p)
Copy New Logins Over (they must be mostly identical to start) \n";
        return "\n";
}


use vars qw($opt_A $opt_F $opt_o $opt_U $opt_P $opt_B $opt_t $opt_u $opt_p $opt_d $opt_n $opt_M);
die usage("Bad Parameter List\n") unless getopts('B:U:F:P:u:p:t:nMT:A:do');

my($tmpdir)="/tmp" if -w "/tmp";
$tmpdir="C:/tmp" if ! defined $tmpdir and -w "C:/tmp";
$tmpdir="C:/temp" if ! defined $tmpdir and -w "C:/temp";

my($table_name)="syslogins";
my($column_key)="suid";
$table_name="sysxlogins" if defined $opt_M;
$column_key="sid" if defined $opt_M;

$opt_u=$opt_U unless $opt_u;
$opt_p=$opt_P unless $opt_p;


die usage("Invalid Source Args\n")
	unless defined $opt_U and $opt_P and $opt_F;
die usage("Invalid Target Args\n")
	unless defined $opt_u and $opt_p and $opt_t;


die usage("Bad User Info -U\n") unless defined $opt_U;
die usage("Bad Srv Info -F\n")  unless defined $opt_F;
die usage("Bad Pass Info -P\n") unless defined $opt_P;
die usage("Bad User Info -u\n") unless defined $opt_u;
die usage("Bad Srv Info -t\n")  unless defined $opt_t;
die usage("Bad Pass Info -p\n") unless defined $opt_p;

print "Copy Login Utilitity v2.0\n\n";
#die "-B opt does not exist\n" if $opt_B and ! -r $opt_B;

dbi_connect(-srv=>$opt_F,-login=>$opt_U,-password=>$opt_P)
   or die "Cant connect to $opt_F as $opt_U\n";
dbi_set_mode("INLINE");
my($stypeF,$sverF)=dbi_db_version();
print "Server $opt_F is type $stypeF version $sverF\n";
dbi_disconnect();

my($query);
dbi_connect(-srv=>$opt_t,-login=>$opt_u,-password=>$opt_p)
   or die "Cant connect to $opt_t as $opt_u\n";
dbi_set_mode("INLINE");
my($stype,$sver)=dbi_db_version();
print "Server $opt_t is type $stype version $sver\n";

die localtime()." Server $opt_F ($stypeF) and $opt_t ($stype) are different types\n" if $stypeF ne $stype and ! $opt_o;
die localtime()." Server $opt_F ($sverF) and $opt_t ($sver) are different versions\n" if $sverF ne $sver and ! $opt_o;

#########################################################################
print "checking existence of tempdb..syslogins_copy\n";
$query="select 1 from tempdb..sysobjects where name='syslogins_copy'";
my($it_exists)=0;
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) { $it_exists=1; }

#########################################################################
if( $it_exists==1 ) {
	$query="drop table tempdb..syslogins_copy\n";
	print "$query";
	foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
		my($rc)=dbi_decode_row($_);
		print "Error - Query $query Returned $rc\n";
	}
}
#########################################################################

print "creating tempdb..syslogins_copy\n";
$query="select * into syslogins_copy from master..$table_name where 1=2";
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
	my($rc)=dbi_decode_row($_);
	print "Error - Query $query Returned $rc\n";
}

#########################################################################
unlink "$tmpdir/syslogins.dat" if -e "$tmpdir/syslogins.dat";

print "bcp'ing out of $opt_F (master..$table_name)\n";
#print "Command: $opt_B master..$table_name out $tmpdir/syslogins.dat -U$opt_U -P$opt_P -S$opt_F -n\n"
#	if $opt_d;
bcp_table("master",$table_name,"$tmpdir/syslogins.dat",$opt_F,$opt_U,$opt_P,"out");

#my($cmd)="$opt_B master..$table_name out $tmpdir/syslogins.dat -U$opt_U -P$opt_P -S$opt_F -n";
#open(CMD, $cmd." |")
#	or die "Cant run $cmd\n$!";
#while( <CMD> ) {
#	chomp;
#	print "Returned: $_\n" unless /^\s*$/;
#}
#close(CMD);
#print "\n";
die "\n Error: $tmpdir/syslogins.dat Was Not Created...\n" unless -e "$tmpdir/syslogins.dat";

#########################################################################
print "bcp'ing in to $opt_t (tempdb..syslogins_copy)\n";
bcp_table("tempdb",'syslogins_copy',"$tmpdir/syslogins.dat",$opt_t,$opt_u,$opt_p,"in");

#print "Command: $opt_B tempdb..syslogins_copy in $tmpdir/syslogins.dat -U$opt_u -P$opt_p -S$opt_t -n" if $opt_d;
#system("$opt_B tempdb..syslogins_copy in $tmpdir/syslogins.dat -U$opt_u -P$opt_p -S$opt_t -n");
#print "\n";

$query = "select count(*) from tempdb..syslogins_copy";
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
	die "Error Copy OF Login Data Failed\n" if $_ == 0;
}

print "removing $tmpdir/syslogins.dat\n";
unlink "$tmpdir/syslogins.dat";

#########################################################################
print "checking viability of update\n";
print " -- deleting identical rows in syslogin_copy\n";
$query = "delete tempdb..syslogins_copy
	from 		master..$table_name l, tempdb..syslogins_copy c
	where 	isnull(c.$column_key,22222)=isnull(l.$column_key,22222)
 		and  	isnull(c.name,'22222')=isnull(l.name,'22222')";
print $query,"\n";
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
	my($rc)=dbi_decode_row($_);
	print "Error - Query $query Returned $rc\n";
}

if( defined $opt_M ) {
print " -- deleting identical rows in syslogin_copy\n";
$query = "delete tempdb..syslogins_copy
	from 	master..$table_name l, tempdb..syslogins_copy c
	where 	c.name=l.name";
print $query,"\n";
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
	my($rc)=dbi_decode_row($_);
	print "Error - Query $query Returned $rc\n";
}
}

if( defined $opt_A ) {
	$query="update tempdb..syslogins_copy set suid=suid+$opt_A";
	dbi_query(-db=>"tempdb", -query=>$query, -no_results=>1);
}

print " -- finding conflicts\n";
$query = "select l.name,l.$column_key,c.$column_key
	from 	master..$table_name l, tempdb..syslogins_copy c
	where c.name=l.name";
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
	my($lgin,$tsuid,$ssuid)=dbi_decode_row($_);
	die "Error - login $lgin exists $opt_F with $column_key $ssuid and $opt_t with $column_key $tsuid\n";
}

$query = "select l.name,c.name,l.$column_key
	from 	master..$table_name l, tempdb..syslogins_copy c
	where c.$column_key=l.$column_key";
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
	my($tlgin,$slgin,$suid)=dbi_decode_row($_);
	die "Error - $column_key $suid exists $opt_F with login $slgin and $opt_t with login $tlgin\n";
}

$query = "select count(*) from tempdb..syslogins_copy";
my($num_to_copy)=0;
foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
	my($rc)=dbi_decode_row($_);
	$num_to_copy=$rc;
	print "$rc Logins Ready To Be Copied\n";
}

if( $num_to_copy>0 ) {

	#########################################################################
	if( ! defined $opt_n ) {
		$query="sp_configure 'allow updates',1";
		print $query,"\n";
		dbi_query(-db=>"master", -query=>$query);

		if( $opt_M ) {
		$query="reconfigure with override";
		print $query,"\n";
		dbi_query(-db=>"master", -query=>$query);
		}
	}

	#########################################################################
	print "the following logins will be moved\n";
	$query = "select name from tempdb..syslogins_copy";
	foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
		my($rc)=dbi_decode_row($_);
		print "(not) " if defined $opt_n;
		print "Adding Login $rc \n";
	}
	if( ! defined $opt_n ) {
		print "updating syslogins\n";
my($collist)="*";
$collist="srvid,sid,xstatus,xdate1,xdate2,name,password,dbid,language"
	if defined $opt_M;
		$query = "insert master..$table_name select $collist from tempdb..syslogins_copy\n";
		foreach  ( dbi_query(-db=>"tempdb", -query=>$query) ) {
			my($rc)=dbi_decode_row($_);
			print $query,"Fatal Error $rc \n";
		}
	}
	if( defined $opt_n ) {
		print "No Logins Copied as -n option was selected\n";
	}

	#########################################################################
	if( ! defined $opt_n ) {
		$query="sp_configure 'allow updates',0";
		print $query,"\n";
		dbi_query(-db=>"master", -query=>$query);
	}
	#########################################################################
}

my($bcp);
my($loginpass);

sub bcp_table
{
	my($db,$table,$outfile,$server,$login,$password,$inout)=@_;

	if( ! defined $bcp ) {
		if( ! $opt_M ) {
			$bcp=$ENV{SYBASE}."/ASE-15_0/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/ASE-15_0/bin/bcp";
			$bcp=$ENV{SYBASE}."/OCS-15_0/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/OCS-15_0/bin/bcp";
			$bcp=$ENV{SYBASE}."/ASE-12_5/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/ASE-12_5/bin/bcp";
			$bcp=$ENV{SYBASE}."/OCS-12_5/bin/bcp" if ! $bcp and  -x $ENV{SYBASE}."/OCS-12_5/bin/bcp";
			$bcp=$ENV{SYBASE}."/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/bin/bcp";

			# we could be on nt
			if( ! $bcp ) {
				$bcp=$ENV{SYBASE}."/ASE-15_0/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/ASE-15_0/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/OCS-15_0/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/OCS-15_0/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/ASE-12_5/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/ASE-12_5/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/OCS-12_5/bin/bcp.exe" if ! $bcp and  -x $ENV{SYBASE}."/OCS-12_5/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/bin/bcp.exe";
			}

			if( $bcp eq "" and -d $ENV{SYBASE} ) {
				opendir(DIR,$ENV{SYBASE})
					or die("Can't open directory $ENV{SYBASE} : $!\n");
				my(@dirlist) = grep((!/^\./ and -d "$ENV{SYBASE}/$_") ,readdir(DIR));
				closedir(DIR);
				foreach ( @dirlist ) {
					if( -x "$_/bin/bcp" ) {
						$bcp = "$_/bin/bcp";
						last;
					}
				}
			}
			$loginpass="-U$login -PXXX";
		} else {
			foreach ( split(/;/,$ENV{PATH} )) {
				print "TESTING PATH $_\n" if $opt_d;
				next unless -d $_;
				if( -e "$_/osql.exe" ) {
					print "FOUND OSQL in $_\n" if $opt_d;
					$bcp="./bcp.exe";
					#$bcp=~s.\\.\/.g;
					chdir($_) or die "Cant chdir $_ $!\n";;
					last;
				}
			}
			$loginpass="-T ";
		}
	}

	if( ! defined $bcp ) {
		print "BCP NOT FOUND - CANT COPY TABLE $table\n";
		return;
	}

	my($cmd)="$bcp $db..$table $inout $outfile -n  -S$server $loginpass\n";
	print "Copying Table $table $inout to File $outfile (native mode)\n";
	print $cmd;
	$cmd=~s/PXXX\n/P$password\n/;
	system($cmd);

#	my($filename)=$opt_o.".".$table."_char.bcp";
#	my($cmd)="$bcp $db..$table $inout $filename -c -t~ -S$server $loginpass\n";
#	print "Copying Table $table $inout to File $filename (char mode)\n";
#	print $cmd;
#	$cmd=~s/PXXX\n/P$password\n/;
#	system($cmd);
}

__END__

=head1 NAME

copy_syslogins.pl - maintain syslogins on servers that should be identical

=head2 DESCRIPTION

Some servers should have identical syslogins.  This simple utility maintains
a slave server based on a master server.  The program will abort if the
logins can not be maintained.

=head2 USAGE

copy_syslogins.pl  -Fsrv [ -Uusr -Ppass ] -tsrv [ -uusr -ppass ]

Note the syntax -F (FROM) server with login/pass from (-U/-P)
           and  -t (to) server with login/pass from (-u/-p)
Copy New Logins Over (they must be mostly identical to start)

=head2 NOTES

Requires select into/bcp on tempdb in target server.  Requires source
and target server to be identical versions.  This is what i use to maintain
servers that should have exact copies of logins from produciton (eg. hot
backups, systems used for split nightly batch runs, and
some developement servers)

=cut
