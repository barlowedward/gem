# Copyright (c) 2004-2008 by Edward Barlow. All rights reserved.

package RunCommand;

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use Win32;
use File::Basename;
use Win32::Process;
use Win32::DriveInfo;
use Tk::ROText;
use Do_Time;

$VERSION= 1.0;
@ISA      = ( 'Exporter' );
@EXPORT   = ( 'RunCommand', "show_html","get_win32_disks");

sub RunCommand
{
   my(%OPT)=@_;
   my(@rc);
   chomp $OPT{-cmd};
   $OPT{-err}="" unless $OPT{-err};   

   $OPT{-statusfunc}->( "[RunCommand] Win32 RunCommand Called\n" )
   	if defined $OPT{-statusfunc};
   my($starttime)=time;
   my($dur);

   my $cmd_no_password=$OPT{-cmd};
   if( $cmd_no_password=~/-PASSWORD=/ ) {
   	$cmd_no_password=~s/-PASSWORD=\w+/-PASSWORD=XXX/;
   } elsif( $cmd_no_password=~/-PASS=/ ) {
   	$cmd_no_password=~s/-PASS=\w+/-PASS=XXX/;
   } else {
   	$cmd_no_password=~s/-P\w+/-PXXX/;
   }

   if( length($cmd_no_password)>70 ) {
   	my(@words)=split(/ /,$cmd_no_password);
   	my($curlen)=0;
   	$cmd_no_password="";
   	foreach(@words) {
   		if( $curlen+length() > 70 ) {
   			if( $curlen+length()>75 ) {
   				$cmd_no_password.="\n\t".$_;
   				$curlen=length();
   			} else {
   				$cmd_no_password.=" ".$_."\n\t";
   				$curlen=0;
   			}
   		} else {
   			$curlen+=length()+1;
   			$cmd_no_password.=" ".$_;
   		}
   	}
		$cmd_no_password=~s/^\s+//;
		$cmd_no_password=~s/\s+$//;
   }
   

   $OPT{-statusfunc}->( "[RunCommand] ".$cmd_no_password."\n" )
   	if  defined $OPT{-statusfunc};

   if( ! open( WINCMD,"$OPT{-cmd} |")) {
		print "debug: Cant Run $OPT{-cmd}\n" if $OPT{-debug};		
		
		my($tmsg)= $OPT{-err}."Command Failed To Run\n".
      				$cmd_no_password."\n".
						"Error: $!\n";		
		$tmsg=~s/\t/   /g;		
		my($xwidth)=20;		
		foreach ( split(/\n/,$tmsg )) { 
			my($l2)=length; 			
			$xwidth=$l2 if $l2>$xwidth; 
		}		
		rotext_box( -mw=>$OPT{-mainwindow}, -title => 'Command Failed', -text=>$tmsg, -width=>$xwidth );		
		
		#$OPT{-mainwindow}->messageBox(
		#	-title => 	'Command Failed',
		#	-width => 	$xwidth, 
		#	-message =>	$tmsg,
		#	-type => "OK" ) if defined $OPT{-mainwindow};
		
		return(0,undef);
	}
	
   print "debug: Processing Output For $OPT{-cmd}\n" if $OPT{-debug};
	my(@output);	
   while ( <WINCMD> ) {
   	chomp;  chomp; 	
		$OPT{-printfunc}->($OPT{-printhdr}.$_."\n") if defined $OPT{-printfunc};
		push @output, $_;		
   }   
   close(WINCMD);	
   
#   if( 1 ) {
#   	print "EXECUTING ExecuteCommand\n";
#   	my($cmd)=$OPT{-mainwindow}->ExecuteCommand( -command=>$OPT{-cmd}, -entryWidth=>100, -height=>30, -label=>"Hi",
#   		-text=>"My Do It!", -width=>100);
#   	$cmd->execute_command();
#   	$cmd->update;
#   	return;
#   }
#if( 0 ) {
#   my($rc)=Win32::Process::Create($Win32::Process::Create::ProcessObj,
#		$^X,
#		"$OPT{-cmd}",
#		0,
#	# CREATE_NO_WINDOW - use if no window needed
#	# CREATE_NEW_CONSOLE - create new console window
#	# if neither of the above, it goes to your other console
#		#DETACHED_PROCESS,
#		0,
#		"." );
#	print "DBG:",Win32::FormatMessage( Win32::GetLastError() ),"\n";
#
#   if( ! $rc ){
#		$OPT{-mainwindow}->messageBox(
#			-title => 'Command Failed',
#			-message => $OPT{-err}."Command Failed To Run\n".
#      					"Command: ".$cmd_no_password."\n".
#							"Error: $!\n",
#			-type => "OK" ) if defined $OPT{-mainwindow};
#		return(0,undef);
#	}
#
#	my(@output);
#
#	my($cmd_rc)=$?;
#   if( $cmd_rc != 0 ) {
#		$OPT{-mainwindow}->messageBox(
#					-title => 'Command Failed',
#					-message => $OPT{-err}."Command Failed - return code=$cmd_rc\n".
#      							"Command: ".$cmd_no_password."\n".
#									"\n\nResults:\n".join("\n",@rc),
#					-type => "OK" );
#		return(0,\@output);
#	}
#}

	
	if( defined $OPT{-showinwin} ) {
		$OPT{-title} = $OPT{-title} || $OPT{-showinwin};
		my(@output2);
		my($ignore_count)=0;
		foreach (@output){
			if( $OPT{-ignore_string} and /$OPT{-ignore_string}/) {
				push @output2,".";
				$ignore_count++;
				if( $ignore_count>40 ) {
					push @output2,"\n";
					$ignore_count=0;
				}
			} else {
				if( $ignore_count>0 ) {
					push @output2,"\n";
					$ignore_count=0;
				}
				push @output2,$_."\n";
			};
		}

		# ORIGINAL MECHANISM TO SHOW RESULTS
#		$OPT{-mainwindow}->messageBox(
#			-title => $OPT{-title},
#			-message => $OPT{-showinwin}." (Succeeded)\n\nResults:\n".join("",@output2),
#			-type => "OK" );

		# CURRENT MECHANISM
		my $d = $OPT{-mainwindow}->DialogBox(-title=>$OPT{-title}, -buttons=>["OK"]);
		$d->add('Frame')->grid;
		my $frame = $d->Subwidget('frame');

		$frame->Label(-font=>"large",-text=>"COMMAND:", -justify=>'left', -bg=>'beige',
			-relief=>'groove')->grid(-column=>1,-row=>1,-sticky=>'ew');
		$frame->Label(-bg=>'white',-font=>"large",-textvariable=>\$OPT{-showinwin},
			-width=>35, -relief=>'groove')->grid(-column=>2,-row=>1, -sticky=>'ew');

		$frame->Label(-font=>"large",-text=>"DURATION:", -justify=>'left', -bg=>'beige',
			-relief=>'groove')->grid(-column=>1,-row=>2,-sticky=>'ew');
		$dur=do_diff(time - $starttime, 1);
		$frame->Label(-bg=>'white',-font=>"large",-text=>$dur,
				-width=>35, -relief=>'groove')->grid(-column=>2,-row=>2, -sticky=>'ew');
		

		my($textObject) = $frame->Scrolled("ROText",
			-relief => "sunken", -wrap=>'none',
			-bg=>'white',#-font=>'code',
			-scrollbars => "osoe")->grid(-column=>1,-row=>3, -sticky=>'ew',-columnspan=>2, -rowspan=>3);
		#$textObject->tagConfigure("t_red",   -foreground=>'red');
		$textObject->insert("end",$OPT{-showinwin}." (Succeeded)\n\nResults:\n".join("",@output2));
		$OPT{-statusfunc}->( "[RunCommand] "."Creating Window With Results... Click Ok To Continue\n" )
   			if  defined $OPT{-statusfunc};

		$d->Show();
		
	} else {
		$dur=do_diff(time - $starttime, 1);		
	}
	
	$OPT{-statusfunc}->( "[RunCommand] "."Command completed at ".localtime(time)." (Duration=$dur)\n")
		if defined $OPT{-statusfunc};
	print "debug: Returning ",$#output, " Rows\n" if $OPT{-debug};
	
	return(1,\@output);
}

# -title 
# -label (optional)
# -text
# -width (optional)
sub rotext_box {
	my(%args)=@_;
	return unless $args{-mw};
	my $d = $args{-mw}->DialogBox(-title=>$args{-title}, -buttons=>["OK"]);
	$d->add('Frame')->grid;
	my $frame = $d->Subwidget('frame');
	$args{-label} = $args{-title} unless $args{-label};

	$frame->Label(-font=>"large",-text=>$args{-label}, -justify=>'left', -bg=>'beige',
		-relief=>'groove')->pack(-side=>'top', -fill=>'x');
	my(%nargs);
	$nargs{-width}=$args{-width} if $args{-width};
	my($textObject) = $frame->Scrolled("ROText",
			-relief => "sunken", -padx=>5, %nargs,	-wrap=>'none',	-bg=>'white',	-scrollbars => "osoe"
			)->pack(-side=>'bottom', -fill=>'x', -expand=>1);
	
	$textObject->insert("end",$args{-text});
	$d->Show();
}

my($ftype_results);
sub show_html {
	my($file)=@_;
	#$file = $XMLDATA{gem_root_directory}."/".$file unless $file=~/^http/;

	if( ! defined $ftype_results ) {
		$ftype_results=`ftype htmlfile`;
    		chomp $ftype_results;
    		$ftype_results=~s/^htmlfile[=\s]*//;;
    	}

#    	my($cmd)=$ftype_results."  $file";
#    	print "CMD is ",$cmd,"\n";
#    	`$cmd`;
#

	my($ie)="C:/Program Files/Internet Explorer/iexplore.exe";
	# print "DBG DBG Original ie is $ie ***\n";
	if( ! -r $ie ) {
		#print "DBG DBG which is not readable\n";
		#print "DBG DBG: Rebuilding ie\n";
		$ie=`ftype htmlfile`;
		#print "DBG DBG: ie raw = $ie\n";
    		chomp $ie;
    		$ie=~s/^htmlfile[=\s]*\"//;
    		$ie=~s/\"\s+\-[a-z]+\s*$//g;
    		$ie=~s/\\/\\\\/g;
    		# print "DBG: Resetting Ie to $ie!\n";
    	} else {
    		# print "DBG DBG which is readable\n";
    	}
    	# print "DBG DBG : at line ",__LINE__,"\n";
	my($b)=basename($ie);

	#print "DBG DBG: $ie\n";
	#print "DBG DBG: $b\n";
	#print "DBG DBG: $file\n";
	my($rc)=Win32::Process::Create($Win32::Process::Create::ProcessObj,
		$ie,
		$b." ".$file,
		0,
	# CREATE_NO_WINDOW - use if no window needed
	# CREATE_NEW_CONSOLE - create new console window
	# if neither of the above, it goes to your other console
		#DETACHED_PROCESS,
		DETACHED_PROCESS,
		"." );
	# print "DBG DBG: rc=$rc msg=" ,Win32::FormatMessage( Win32::GetLastError() ),"\n";


#	Win32::Process::Create($Win32::Process::Create::ProcessObj
#		$ie
#		'iexplore.exe '.$file
#		0
#		DETACHED_PROCESS
#		"." ) or print_error()

#   	if( ! $rc ){
#		$OPT{-mainwindow}->messageBox(
#			-title => 'Command Failed',
#			-message => $OPT{-err}."Command Failed To Run\n".
#      					"Command: ".$cmd_no_password."\n".
#							"Error: $!\n",
#			-type => "OK" ) if defined $OPT{-mainwindow};
#		return(0,undef);
#	}

}

sub get_win32_disks {
	my($system)=@_;
	my(@rc);
	foreach my $drive ( 'c$','d$','e$','f$','g$','h$','i$' ) {
		my($url)="\\\\".$system."\\".$drive;
		my(@diskdat)=Win32::DriveInfo::DriveSpace($url);
		push @rc,$drive if defined $diskdat[5];
	}
	return @rc;
}

#sub print_error()
#   print "PROCESS CREATION FAILED\n".Win32::FormatMessage( Win32::GetLastError() )
#   exit(0);
#

#	#my($file)="G:/gem/doc/Gem_Specification.html"
#	my($ie)="C:\\Program Files\\Internet Explorer\\iexplore.exe"
#	#print "Original ie is $ie!\n"
#	if( ! -r $ie )
#		$ie=`ftype htmlfile`
#    		chomp $ie
#    		$ie=~s/^htmlfile[=\s]*\"//
#    		$ie=~s/\"\s+\-[a-z]+\s*$//
#    		#$ie=~s/\s*-nohome$//
#    		$ie=~s/\\/\\\\/g
#    	#	print "New      ie is $ie!\n"
#    		_statusmsg( "Resetting Ie to $ie!\n" )
#

#	Win32::Process::Create($Win32::Process::Create::ProcessObj
#		$ie
#		'iexplore.exe '.$file
#		0
#		DETACHED_PROCESS
#		"." ) or print_error()

#    	#$cmd="start iexplore.exe $file"
#    	#print "CMD is ",$cmd,"\n"
#    	#`$cmd`

#    	#$cmd=~s.\/.\\.g
#    	#print "CMD is ",$cmd,"\n"
#    	#`$cmd`
#    	#`"C:/Program Files/Internet Explorer/iexplore.exe" G:/gem/doc/Gem_Specification.html`
#

	#system('doc\Gem_Specification.html')
#sub print_error() {
   #return Win32::FormatMessage( Win32::GetLastError() );
#}
1;