
# copyright (c) 2010-2011 by SQL Technologies.  All Rights Reserved.

package  GemReport;

use      strict;
use      Carp;
use      vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
require  Exporter;
use 		Repository;
@ISA     = qw(Exporter);
@EXPORT  = qw( report_init report_print report_close report_table report_color);
$VERSION = '1.0';

my($outfile);
my($nl)="\n";
my($debug);
my($fontcolor)='';
my($is_table)='';

# -file_name
# -debug
my($is_initialized);
sub report_init {
	my(%args)=@_;
	
	return unless $args{-file_name};
	$outfile = get_gem_root_dir() . "/data/html_output/".$args{-file_name}.".html";	
	print "OUTPUT OF THIS PROGRAM IS SAVED AS A GEM REPORT in $outfile\n"
		unless $args{-nofileprint};
	return if $is_initialized;
	$is_initialized = 1;
	
	$nl="\n";
	$nl="<br>\n" if $args{-addbreaks};	
	unlink $outfile if -w $outfile;
	open(GEMREPORT,">".$outfile ) or die "CANT write File $outfile $!\n";
	$debug = $args{-debug};
	return $outfile;
}

sub report_table { 
	my($args);
	($is_table,$args)=@_; 
	die "report_init() must be called before report_table()" unless $is_initialized;
	if( $is_table ) {
		print GEMREPORT "<TABLE $args>"; 	
	} else {
		print GEMREPORT "</TABLE>"; 	
	}
}
sub report_color { ($fontcolor)=@_; }

sub report_print {
	my($do_stdout,@pargs)=@_;
		
	if( $outfile ) {
		if( $is_table ) {
			print join(" ",@pargs) if $do_stdout or $debug;		
			
			print GEMREPORT "<TR>\n";
			if( $fontcolor ) {
				foreach ( @pargs ) {
					print GEMREPORT "<TD><FONT color=$fontcolor>",$_,"&nbsp;", "</font></TD>\n";									
				}
			} else {
				foreach ( @pargs ) {
					
					print GEMREPORT "<TD>",$_,"&nbsp;", "</TD>\n";									
				}				
			}			
			print GEMREPORT "</TR>\n";
		} else {
			my($str)=join("",@pargs);
			print $str if $do_stdout or $debug;		
			$str =~ s/\n/$nl/g;
			print GEMREPORT "<FONT color=$fontcolor>" if $fontcolor;
			print GEMREPORT $str;			
			print GEMREPORT "</FONT>" if $fontcolor;
		}		
	} else {
	# 	$str =~ s/\n/$nl/g;
		my($str)=join("",@pargs);
		print $str;			
	}
}

sub report_close {
	close(GEMREPORT) if $outfile;
}

1;