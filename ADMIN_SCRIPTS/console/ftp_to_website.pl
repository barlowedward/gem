#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

#
# Copy output_for_website directory to website.
# ITEMS IN CAPS ARE INTERPOLATED BY configure.pl
#

use strict;
use File::Basename;
use Net::myFTP;
use File::Copy;
use Repository;
use CommonFunc;

my($gr_dir)=get_gem_root_dir();
die "No Gem Root Dir" unless -d $gr_dir;
my($wp_dir)= "$gr_dir/data/CONSOLE_REPORTS";
die "No Input File $wp_dir" unless -d $wp_dir;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";
my(@dirs);
foreach (@ARGV) { push @dirs,$_; }
push @dirs, "$wp_dir" if $#dirs<0;
my($str)=join(" ",@dirs);
foreach (@dirs) { my $x=$_; copy_a_dir($x); }
print "Ftp of $str Completed\n" unless "" eq "";
exit(0);

sub copy_a_dir
{
my($dir)=@_;
print "Copying $dir\n";
#
# First the redundant case where you are copying
#

if( "" eq "") {

	# print " (redundant - nothing to copy)\n";

} elsif( "" eq "") {

	print " To  \n";

	opendir(DIR,"../".$dir) || die("Can't open directory $dir for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);
	foreach (@dirlist) {
		if( ! copy("../$dir/$_","/".$_ )) {
			warn("WARNING: Couldnt Copy File $_ to : $!\n");
		}
	}


} else {

	print " using FTP to server= dir=\n";

	my($login,$password)=get_password(-type=>"unix",-name=>"");
	# print "  Creating FTP Connection To \n";

	my(%args)=get_password_info(-type=>"unix",-name=>"");
	my($method);
	if( is_nt() ) {
		$method=$args{WIN32_COMM_METHOD} if is_nt();
	} else {
		$method=$args{UNIX_COMM_METHOD};
	}
	die "COMM_METHOD NOT DEFINED for SERVER \n" unless $method;

	if( $method eq "NONE" ) {	# hmm cant talk
		print "host : METHOD=NONE - skipping\n";
		return
	}

	my($ftp) = Net::myFTP->new("",METHOD=>$method,Debug=>undef)
			or die "Failed To Create FTP Object $@";
	if( ! $ftp )  {
		die "Failed To Connect To \n";
	}
	my($rc,$err)=$ftp->login($login,$password);
	die "Failed To Login To  $err\n" unless $rc;

	$ftp->ascii();
	if( ! $ftp->cwd("") ) {
		die("ERROR: Cant Chdir To  on machine ");
	}

	# just copy it
  	if( -e "../".$dir and ! -d "../".$dir ) {
      chdir ".." or die "Cant cd to ..: $!\n";
      $ftp->binary() if $dir=~/gif$/ or $dir=~/jpg$/;
      if( ! $ftp->put($dir) ) {
         warn("WARNING: Cant Copy File $dir to : $!\n");
      }
		$ftp->close();
		$ftp->quit();
		return;
	}

	if( ! $ftp->cwd("$dir") ) {
		$ftp->mkdir("$dir");
		if( ! $ftp->cwd("$dir") ) {
			die("ERROR: Cant Chdir To $dir on machine ");
		}
	}

	chdir( "../$dir") or die "Cant change directory\n";
	opendir(DIR,".") || die("Can't open directory . for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);
	foreach (@dirlist) {
		if( -d $_ ) {
			$ftp->mkdir($_);
			next;
		} else {
			$ftp->binary() if /gif$/ or /jpg$/;
			print ".";
			if( ! $ftp->put($_) ) {
				warn("WARNING: Couldnt Copy File $_ from : $!\n");
			}
			$ftp->ascii() if /gif$/ or /jpg$/;
		}
	}

	$ftp->close();
	$ftp->quit();
}
}

__END__

=head1 NAME

ftp_to_website.pl - copy reports to target directory

=head2 SYNOPSIS

This program copys the files to your web site - as specified
in the GEM configuration utility.  Sometimes, the local directory
is not where you wish to publish your console.  This program does
the copy, or ftp if needed, to your actual web site.
This modified version of ftp_to_website.dist is customized during the configuration
process to include your server
information.

=cut

