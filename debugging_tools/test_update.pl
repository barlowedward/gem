: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;


use strict;
use Fcntl;
use Getopt::Long;
use File::Basename;

use Tk 8.0;
use Tk::HList;
use Tk::Tree;
use Tk::DialogBox;
use Tk::ItemStyle;
use Tk::NoteBook;
use Tk::BrowseEntry;
use Tk::ErrorDialog;
use Tk::Menu;
use Tk::ROText;
use Tk::Scrollbar;
use Tk::Pixmap;
use Data::Dumper;
use Repository;
use CommonFunc;
use DBI;

my($print_stdout)="TRUE";		# Print to stdout - this is diagnostic mode

$|++;      # Unbuffered output to STDOUT

my $splashscreen;
my($status_bar_ready)="FALSE";

BEGIN {
	require 5.6.1;
	eval {
		require Tk::ProgressSplash;
		$splashscreen=Tk::ProgressSplash->Show(-splashtype=>'fast',"doc/gem.gif",350,200,"Gem V1.0",0);
	};
	warn $@ if $@;
}

END {		# We do this next thing to allow running right from the
		# command line with a doubleclick on windows.
                if( defined $ENV{PROCESSOR_LEVEL} ) {
                        print "<Hit Enter Key To Continue>";
                        <STDIN>;
                }
}

our(%CONFIG,%CURRENT_CONFIG,%server_info,$DEBUG,$statusmsgbar,$notebook_f,$connectionmanager,
	$DO_DUMP_XML,$gem_data,@perl_includes);
use vars qw/%BackupPlansHash %BackupTabByKey_DATA %DeletedPlans/;

my $product_name    	= "Generic Enterprise Manager";
my( $tab_width, $tab_height) = (700,400);
my(%TREESTYLE);

my $tree;            # scrolled tree Tk widget
my $_statusmsg_idle;
my $curTreeType;
my $curExplorerItem;
my( $explorer_f, $bitmap_f, $mid_f,  $statusmsg_f);
my( $menu, %submenu ); 			# main menu and a hash of menu items under it
                          		# added submenus may have a / separator
my( %notebook_tab, %notebook_frame );
my( $sql_text, $log_text, $debug_text); # text widgets on Sql and Log
my %rtbtnlbl_bypath;
my %rtbtncmd_bypath;
my($cursplash,$sumsplash)=(0,8);	# steps to update splash screen
my(@pre_log_ok_msgs);
my(@pre_dbg_ok_msgs);
my(%user_dfnd_frames);			# by tab
my(@welcome_itm);			# the three ADMIN menu items on the welcome tab
my $NUM_STATUS_LINES=1; 	# number of lines on status line - constant

sub usage {
   print @_;
   print "gem.pl - Enterprise Data Manager.\n";
   print "  --REPOSITORY=repository (default to repository.xml) \n";
   print "  --QUIET\n";
   print "  --DEBUG \n";
   print "  --DUMP - dump xml into .dat file and exit \n";
   exit();
}

my $firstnode = "/";

use vars qw( $opt_q);
GetOptions("debug" => \$DEBUG, "quiet"=>\$opt_q, "dump"=>\$DO_DUMP_XML ) or usage( "syntax error\n" );

$print_stdout="TRUE"  if defined $DEBUG;
$print_stdout="FALSE" if defined $opt_q;
_debugmsg( "Debug mode set\n" );

$CURRENT_CONFIG{gem_root_directory}		= get_gem_root_dir();
$CURRENT_CONFIG{gem_configuration_directory} = $CURRENT_CONFIG{gem_root_directory}."/conf";
require $CURRENT_CONFIG{gem_root_directory}."/lib/GemFunctions.pl";

$CURRENT_CONFIG{log_file}   = $CURRENT_CONFIG{gem_root_directory}."/gem.log";
my($id)=0;
while( -r $CURRENT_CONFIG{log_file}.".".$id ) { $id++; }
$id=0 if $id>=10;
$CURRENT_CONFIG{log_file} = $CURRENT_CONFIG{log_file}.".".$id;
unlink $CURRENT_CONFIG{log_file} if -r $CURRENT_CONFIG{log_file};

print "Log Output to $CURRENT_CONFIG{log_file}\n";
open(LOGFP,"> $CURRENT_CONFIG{log_file}") or die "Cant Open Log File $CURRENT_CONFIG{log_file}\n";

splashupdate();
_statusmsg("[gem.pl] Welcome to GEM - Generic Enterprise Manager\n");
_statusmsg("[gem.pl] copyright (c) 1995-2004 by SQL Technologies\n");
_statusmsg("[gem.pl] Reading XML Configuration File - conf/\n");

_statusmsg("[gem.pl] Reading Gem Function Library\n" );
read_all_configfiles("FULL",$DEBUG);
splashupdate();

$_statusmsg_idle 	 = "GEM Build ".global_data(-class=>'version',-name=>'Build')." (".global_data(-class=>'version',-name=>'Date').")";
$_statusmsg_idle 	.= " : ready";
_statusmsg("[gem.pl] "  .$_statusmsg_idle);
splashupdate();
_statusmsg("[gem.pl] Initializing Plugin Manager\n");
splashupdate();

initWindow();
splashupdate();


_statusmsg("[gem.pl] Initializing PluginManager\n");
print __FILE__," ",__LINE__," DBG Test\n";
print __FILE__," ",__LINE__," DBG Test\n";

splashupdate();
print __FILE__," ",__LINE__," DBG Test\n";
_statusmsg( "[gem.pl] Drawing Menubar\n" );
build_menubar();
splashupdate();

_statusmsg( "[gem.pl]   Initializing Help Menu\n" );
initHelpMenu();		# finish off the menu (want this after other items)
_statusmsg( "[gem.pl]   Completed Drawing menubar\n" );
splashupdate();

refresh_tree($curTreeType);
splashupdate();

collapseAll();
splashupdate();

_statusmsg( "[gem.pl] Total Splashscreen Steps=$cursplash \n" );
$splashscreen->Destroy();

$notebook_f->raise("Welcome");
$CURRENT_CONFIG{mainWindow}->deiconify();
setDebugMode();

$CURRENT_CONFIG{mainWindow}->maxsize(1024,800);
$CURRENT_CONFIG{mainWindow}->packPropagate(0);

_statusmsg( "[gem.pl] Ready For Input\n" );
$status_bar_ready="TRUE";
MainLoop();
print "$product_name exited successfully\n";
exit();


sub _helpdialog {
	 show_html('http://www.edbarlow.com/gem/index.html');
}

sub initHelpMenu{
	$submenu{help} = $menu->Menubutton(-text   => '~Help', -tearoff => 0);

	my(%other_menus)=(
		'Your Configuration Reports' 	=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'data/DAILY_REPORTS/index.html',
		"What Is the G.E.M."		=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/SalesPresentation.htm'    ,
    		"Change Log"			=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/CHANGE_LOG.html',
    		"F.A.Q."			=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/FAQ.html',
    		"G.E.M. Manual"			=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/gem/index.htm',
    		"Stored Procedure Library Manual"	=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/procs/index.htm',
    		"Utilities & ToolsLibrary Manual"	=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/bin/index.htm',
    		"G.E.M. Specification"		=> 'file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/gem/index.htm',
    		"Home Base"			=> 'http://www.edbarlow.com/gem/index.html'
    	);
	foreach my $k (keys %other_menus) {
		$submenu{help}->command(-label => $k, -command  =>  sub {
			print "working on $k -> $other_menus{$k} \n";
			show_html($other_menus{$k});
			});
	}

	$submenu{help}->command(-label => 'Web Based Help', -command  =>  sub { _helpdialog();});

	$submenu{help}->command(-label => 'G.E.M. Version Information', -command =>  sub  {
		my $dialog = $CURRENT_CONFIG{mainWindow}->DialogBox(-title=>"About", -buttons=>["Ok"]);
		$dialog->Label(-text=>"Generic Enterprise Manager\nBuild ".
   			global_data(-class=>'version',-name=>'Build'). " Date ".
   			global_data(-class=>'version',-name=>'Date').  "\n".
   			global_data(-class=>'copyright',-name=>'notice'). "\n")->pack();
		$dialog->Show;
	});
}

sub initWindow {
	# WINDOW LAYOUT
	#	$CURRENT_CONFIG{mainWindow} = Mainwindow is split into 4 parts.  A top menu bar
    	_statusmsg("[gem.pl] Initializing Window Manager");
    	$CURRENT_CONFIG{mainWindow} = MainWindow->new(-title => $product_name); 	# create main window
    	$CURRENT_CONFIG{mainWindow}->iconify();					# shrink main window

    	_statusmsg("[gem.pl]   initializing menu bar");


    	$menu = $CURRENT_CONFIG{mainWindow}->Menu;	    # add menubar
   	$CURRENT_CONFIG{mainWindow}->configure(-menu => $menu);

	_debugmsg("[gem.pl] Creating Fonts\n") if defined $DEBUG;

	$CURRENT_CONFIG{mainWindow}->fontCreate('header', -family=>'Ariel', -size=>'12', -weight=>'bold');
	$CURRENT_CONFIG{mainWindow}->fontCreate('large', -family=>'Ariel', -size=>'12' );

	$CURRENT_CONFIG{mainWindow}->fontCreate('bold', -family=>'Ariel', -size=>'9', -weight=>'bold');
	$CURRENT_CONFIG{mainWindow}->fontCreate('code', -family=>'Courier', -size=>'9');
	$CURRENT_CONFIG{mainWindow}->fontCreate('norm', -family=>'Ariel', -size=>'9');

	$CURRENT_CONFIG{mainWindow}->fontCreate('small', -family=>'Ariel', -size=>'9');
	$CURRENT_CONFIG{mainWindow}->fontCreate('smbold', -family=>'Ariel', -size=>'9', -weight=>'bold');

	$CURRENT_CONFIG{mainWindow}->fontCreate('tiny', -family=>'Ariel', -size=>'6');

    	# build menubar
    	$submenu{file}  = $menu->Menubutton(-text => '~File', -tearoff => 0);
    	$submenu{file}->command(-label     => 'Save All Changes', -underline => 0,
    		-command   => sub {
    			save_all_configfiles();
    		});
    	$submenu{file}->command(-label     => 'Feedback', -underline => 0,
    		-command   => sub { unimplemented("Feedback"); 	});

	$submenu{file}->command(-label     => 'Report Bug', -underline => 0,
    		-command   => sub { unimplemented("Report Bug"); });

	$submenu{file}->command(-label     => 'Convert gem.xml to gem.dat', -underline => 0,
    		-command   => sub {
    			_statusmsg("[gem.pl] Saving Data to gem.dat");
    			busy();
    			$gem_data->dump();
    			unbusy();
    			_statusmsg("[gem.pl] Done Saving gem.dat");
    			});
	$submenu{file}->command(-label     => 'Exit', -underline => 0,   -command   => sub { exit; });
	$submenu{configure}=$menu->Menubutton(-text=>'~Configuration',-tearoff=>0);
    	$submenu{configure}->command(-label => 'Global Setup', -underline => 0, -command   => sub {
    		_statusmsg("Painting Administrator Global Setup Screen");

 		my $admin_db=$CURRENT_CONFIG{mainWindow}->DialogBox(-title=>"Global Setup",-buttons=>["Ok", "Cancel"]);
    		$admin_db->Label(-text=>"Administrator Global Information Setup\n")->grid(-column=>1, -row=>1,-columnspan=>2,-sticky=>'ew');
   		my($nm)=    global_data(-class=>'administrator',-name=>'name');
   		my($email)= global_data(-class=>'administrator',-name=>'email');
   		my($phone)= global_data(-class=>'administrator',-name=>'phone');
		my($company)= global_data(-class=>'administrator',-name=>'company');

   		$admin_db->add("Label",-text=>"Administrator Name",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>2,-sticky=>'ew');
		$admin_db->add("Label",-text=>"Administrator Email",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>3,-sticky=>'ew');
		$admin_db->add("Label",-text=>"Administrator Phone",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>4,-sticky=>'ew');
		$admin_db->add("Label",-text=>"Company Name",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>5,-sticky=>'ew');

		my($dlg_name) = $admin_db->add("Entry",-textvariable=>\$nm,-width=>35)->grid(-column=>2,-row=>2,-sticky=>'w');
		my($dlg_email) = $admin_db->add("Entry",-textvariable=>\$email,-width=>35)->grid(-column=>2,-row=>3,-sticky=>'w');
		my($dlg_phone) = $admin_db->add("Entry",-textvariable=>\$phone,-width=>12)->grid(-column=>2,-row=>4,-sticky=>'w');
		my($dlg_company) = $admin_db->add("Entry",-textvariable=>\$company,-width=>40)->grid(-column=>2,-row=>5,-sticky=>'w');

   		my( $button )=$admin_db->Show;
		if( $button eq "Ok" ) {
			global_data(-class=>'administrator',-name=>'name',-value=>$nm);
			global_data(-class=>'administrator',-name=>'email',-value=>$email);
			global_data(-class=>'administrator',-name=>'phone',-value=>$phone);
			global_data(-class=>'administrator',-name=>'company',-value=>$company);

			_statusmsg("Ok Button Pressed - Saving Data");
    			write_CURRENT_CONFIG();

   			$welcome_itm[0]->configure(-text => $nm );
       			$welcome_itm[1]->configure(-text => $email);
       			$welcome_itm[2]->configure(-text => $phone);
       			$welcome_itm[3]->configure(-text => $company);
    		}
	});

	my $dbgitm = $submenu{configure}->cascade(-label => '~Debug', -tearoff=>0);
    	$dbgitm->radiobutton ( -label     => 'Turn Off Debugging',
            	-variable => \$DEBUG, -command => \&setDebugMode, -value => 0 );
    	$dbgitm->radiobutton ( -label     => 'General Messages',
            	-variable => \$DEBUG, -command => \&setDebugMode, -value => 1 );
    	$dbgitm->radiobutton ( -label     => 'Detailed Messages',
            	-variable => \$DEBUG, -command => \&setDebugMode, -value => 2 );
	$submenu{configure}->separator();

    	$submenu{tools} = $menu->Menubutton(-text => '~Tools', -tearoff => 0);


   	# create frame for explorer, notebook, and buttons
	#	$window -> $mid_f (top) -> $explorer_f (left)
	#               -> $explorer_ft (top)
	#	      	-> tree
	#               -> $button_f (bottom)
   	#           	-> $bitmap_f
   	#               -> notebook_f (right)

   	# placing the status message first forces it to be shown even if the right side is too big
   	$statusmsg_f	= $CURRENT_CONFIG{mainWindow}->Frame()->pack(-side=>'bottom', -fill=>'x');
    	my($mid_f) 	= $CURRENT_CONFIG{mainWindow}->Frame()->pack(-side=>'top', 	  -fill => 'both', -expand=>1);
   	my($left_f) 	= $mid_f->Frame()->pack(-side=>'left',  -fill => 'both', -expand=>1 );
   	my($right_f) 	= $mid_f->Frame()->pack(-side=>'right', -fill => 'both', -expand=>1);

    	#$explorer_f 	= $left_f->Frame()->pack(-side   => 'left');
   	#my $explorer_ft = $explorer_f->Frame()->pack(-side => 'top');
   	my $bitmap_f 	= $left_f->Frame()->pack(-side   =>'top', -fill   => 'x');
    	my $tree_f      = $left_f->Frame()->pack(-side   => 'top', -expand=>1);
	my $button_f    = $left_f->Frame()->pack(-side   => 'bottom');

   	# explorer tree
   	_statusmsg("[gem.pl]   initializing explorer tree");

   	# add some views
   	_statusmsg("[gem.pl]   getting left side button list");

    	$tree = $tree_f->Scrolled( "Tree",
               -width           => 30,
               -height          => 40 ,
               -background       => 'white',
               -selectbackground => 'beige',
               -itemtype        => 'text',
               -separator       => '/',
               -selectmode      => 'single',
               -relief          => 'sunken',
               -scrollbars      => 'osoe',
               -browsecmd  	=> \&browse,
               		# -closecmd     	=> \&_close,
               		# -opencmd      	=> \&_open,
               -command         => \&command
            )->pack( -fill => 'x', -expand => 1 );

    	$TREESTYLE{ERROR}=$tree->ItemStyle('text',
    		#-foreground=>'red',
    		-background =>'white',
    		-foreground=>'red',
    		-selectforeground=>'red'
    		);
	$TREESTYLE{DISCONNECTED}=$tree->ItemStyle('text',
		-selectforeground=>'orange',
		-foreground=>'orange',
		-background =>'white',
    		#-foreground=>'orange'
		);
	#$TREESTYLE{CONNECTED}=$tree->ItemStyle('text',
	#	-activebackground=>'orange',
	#	-activeforeground=>'yellow');

    _statusmsg("[gem.pl]   initializing tabs for right side notebook");
    $notebook_f = $right_f->NoteBook( -relief=>'sunken', -ipadx=>2, -ipady=>2 );
    $notebook_tab{welcome} = create_welcome_tab();
    # $notebook_tab{detail}  = create_generic_tab('detail');
    # $notebook_tab{configure}  = create_generic_tab('configuration');
    $notebook_tab{sql}     = create_sql_tab();
    $notebook_tab{log}     = create_log_tab();
    $notebook_tab{debug}   = create_debugmsg_tab();
    $notebook_f->pack(-side=>'right', -padx=>15, -pady=>15, -expand=>1, -fill=>'both', -anchor=>'nw');
    $notebook_f->raise('Welcome');

    _statusmsg("[gem.pl]   retrieving bitmaps");

    #
    # ADD TWO LOCAL ICONS TO EXPAND OR COLLAPSE THE TREE
    #
   my(%itm);
   $itm{name} 	  = "expandall";
   $itm{order}	  = 1000;
   $itm{image} 	  = '#define expandall_width 16
   #define expandall_height 16
   static unsigned char expandall_bits[] = {
      0x00, 0x00, 0xfe, 0x03, 0x02, 0x02, 0x22, 0x1e, 0x22, 0x12, 0xfa, 0x72,
      0x22, 0x52, 0x22, 0x52, 0x02, 0x52, 0x02, 0x52, 0xfe, 0x53, 0x08, 0x50,
      0xf8, 0x5f, 0x20, 0x40, 0xe0, 0x7f, 0x00, 0x00};
   ';
	$itm{help_text}   = "Expand All Elements Of The Tree";
	$itm{baloon_text} = "Expand All Elements Of The Tree";
	$itm{plugin}	  = "";
	my(@items);
	push @items, \%itm;

   	my(%itm2);
	$itm2{name} 	  = "collapseall";
	$itm2{order}	  = 1001;
	$itm2{image} 	  = '#define collapseall_width 16
   #define collapseall_height 16
   static unsigned char collapseall_bits[] = {
      0x00, 0x00, 0xfe, 0x03, 0x02, 0x02, 0x02, 0x1e, 0x02, 0x12, 0xfa, 0x72,
      0x02, 0x52, 0x02, 0x52, 0x02, 0x52, 0x02, 0x52, 0xfe, 0x53, 0x08, 0x50,
      0xf8, 0x5f, 0x20, 0x40, 0xe0, 0x7f, 0x00, 0x00};
   ';
	$itm2{help_text}   = "Collapse All Elements Of The Tree";
	$itm2{baloon_text} = "Collapse All Elements Of The Tree";
	$itm2{plugin}	  = "";
	push @items, \%itm2;

    setBitmaps( $bitmap_f, \@items );

    # _statusmsg bar
    _statusmsg("[gem.pl]   initializing _statusmsg bar");
    $statusmsgbar = $statusmsg_f->Label(
    				-text=> $_statusmsg_idle,
    				-relief => 'sunken',
    				-background=>'white',
    				-anchor=>'w')->pack(
    						-side => 'left',
    						-fill => 'x',
    						-expand => 1,
    						-anchor=>'nw' );

    # key bindings
    _statusmsg("[gem.pl]   initializing key bindings");
    $CURRENT_CONFIG{mainWindow}->bind('<F1>',sub { _helpdialog() });
    $CURRENT_CONFIG{mainWindow}->bind('<Control-c>',sub { _statusmsg( "program ending: ctrl c was pressed\n"); exit(0); });
    $tree->bind('<Button-3>',[ \&rt_button_pressed,Ev('X'),Ev('Y') ] );
    $CURRENT_CONFIG{mainWindow}->bind('<ButtonPress-2>',sub { unimplemented( "middle button pressed\n"); });
    _statusmsg("[gem.pl] Completed Window Manager Initialization");
}

sub rt_button_pressed {
   my($lb,$x,$y)=@_;
   my $entryPath = $tree->nearest($y-$tree->rooty);
   _debugmsg( "rt button pressed at ",$x-$tree->rootx," ",$y-$tree->rooty," at $entryPath\n");

   my($pmenu) = $CURRENT_CONFIG{mainWindow}->Menu( -tearoff=>0, -disabledforeground=>'blue',
                  -activebackground=>'white', -activeforeground=>'black' );
   if( defined $rtbtnlbl_bypath{$entryPath} ) {
      my($lbl)= $rtbtnlbl_bypath{$entryPath};
      my($cmd)= $rtbtncmd_bypath{$entryPath};

      my($i)=0;
      while( $i<=$#$lbl ) {
         if( $$lbl[$i] eq "sep" ) {
            $pmenu->separator()
         } else {
            my $func = $$cmd[$i];
            my $label= $$lbl[$i];
            if( $i == 0 ) {
            $pmenu->command(-label => "[ ".$label." ]", -command =>
            	sub { &$func($entryPath,$label); }, -state=>'disabled' );
            } else {
            $pmenu->command(-label => $label, -command =>
            	sub { &$func($entryPath,$label); } );
            }
         }
         $i++;
      }
   } else {
      _statusmsg( "No rtclick items defined for $entryPath\nValid Keys For Popups are: ",join(",",keys(%rtbtnlbl_bypath)),"\n");
      $pmenu->command(-label     => 'What is This', -command =>
      	sub { _rtclick($entryPath,"What Is This");});
   }
   $pmenu->post($x+5,$y);
}

sub create_generic_tab {
	my( $name, $label)=@_;

	$name=ucfirst(lc($name));
	_statusmsg( "[gem.pl]   Adding Tab $name \n");
	_debugmsg( "[gem.pl]  tab name=$name, label=$label\n");
	$label=$name unless $label;
	my $tab;
	$tab=$notebook_f->add($name,-label=>$label,-underline=>0);

	$name=lc($name);
    	$notebook_frame{$name}  = $tab->Frame( -width=>$tab_width, -height=>$tab_height)->pack(
    		-anchor=>'ne',
    		-side=>'top',
    		-fill=>'both',
    		-expand=>1);
    	return $tab;
}

sub create_welcome_tab {
    my $tab = create_generic_tab("Welcome");

    $notebook_frame{welcome}->Label(-text=>"Welcome To Ed Barlow's Generic Enterprise Manager\nBuild ".
    	global_data(-class=>'version',-name=>'Build')." Date ".
    	global_data(-class=>'version',-name=>'Date')."\n".
    	global_data(-class=>'copyright',-name=>'notice'), -relief=>"groove",-bg=>'white')->pack(-side => "top", -pady=>3, -padx=>4);
    my $splashphoto=$CURRENT_CONFIG{mainWindow}->Photo(-file=>"doc/gem.gif");
    $notebook_frame{welcome}->Label(-image=>$splashphoto, -bd=>10)->pack(-side => "top", -pady=>2);

    my $str="The Generic Enterprise Manager (GEM) is a full featured enterprise database systems management \nutility.  The GEM provides common security, configuration data, navigation, and look and feel for a \nvariety of downloadable functionality \"plugins\".  These plugins are shared from our central plugin \nstore.  You are welcome to design and publish your own plugins using our simple, well specified, \nopen plugin interface. For more information see: www.edbarlow.com";
    $notebook_frame{welcome}->Label(-padx=>6,-relief=>'groove',-justify=>'left',-text=>$str,-bg=>'white')->pack(-side => "top", -pady=>0);

    my $f = $notebook_frame{welcome}->Frame()->pack(-side=>"top");
    $f->Label(-text=> "Local GEM Administrator Name: ")->grid(  -row=>0, -column=>0, -padx=>5 );
    $welcome_itm[0]=$f->Label(-text=> global_data(-class=>'administrator',-name=>'name'),-anchor=>'w' )->grid( -row=>0, -column=>1, -padx=>5, -sticky=>'ew' );
    $f->Label(-text=> "Local GEM Administrator Email: ")->grid( -row=>1, -column=>0, -padx=>5 );
    $welcome_itm[1]=$f->Label(-text=> global_data(-class=>'administrator',-name=>'email'),-anchor=>'w' )->grid(  -row=>1, -column=>1, -padx=>5, -sticky=>'ew' );
    $f->Label(-text=> "Local GEM Administrator Phone: ")->grid( -row=>2, -column=>0, -padx=>5 );
    $welcome_itm[2]=$f->Label(-text=> global_data(-class=>'administrator',-name=>'phone'),-anchor=>'w' )->grid( -row=>2, -column=>1, -padx=>5, -sticky=>'ew' );
    $f->Label(-text=> "Company Name: ")->grid( -row=>3, -column=>0, -padx=>5 );
    $welcome_itm[3]=$f->Label(-text=> global_data(-class=>'administrator',-name=>'company'),-anchor=>'w' )->grid( -row=>3, -column=>1, -padx=>5, -sticky=>'ew' );

    my $welcome_bot_f  = $notebook_frame{welcome}->Frame()->pack(-side=>'bottom', -padx=>15, -pady=>15, -expand=>1, -fill=>'x');
    my($button_design) = $welcome_bot_f->Button( -text=>"Reports", -activebackground=>'beige',
    	-command=> sub {
    		show_html($CURRENT_CONFIG{gem_root_directory}."/".'data/DAILY_REPORTS/index.html');
    	} )->grid( -row=>0, -column=>0, -padx=>5 );
    my($button_overview) = $welcome_bot_f->Button( -text=>"Overview", -activebackground=>'beige',
    	-command=> sub {
    		show_html($CURRENT_CONFIG{gem_root_directory}."/".'doc/SalesPresentation.htm');
    	} )->grid( -row=>0, -column=>1, -padx=>5 );

    my($button_log) = $welcome_bot_f->Button( -text=>"Change Log", -activebackground=>'beige',
    	-command=> sub {
    		show_html($CURRENT_CONFIG{gem_root_directory}."/".'doc/CHANGE_LOG.html');
    	} )->grid( -row=>0, -column=>2, -padx=>5 );
    my($button_spec) = $welcome_bot_f->Button( -text=>"F.A.Q.", -activebackground=>'beige',
    	-command=> sub {
    			show_html('file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/FAQ.html');
    	} )->grid( -row=>0, -column=>3, -padx=>5 );
    my($button_guide) = $welcome_bot_f->Button( -text=>"G.E.M. Manual", -activebackground=>'beige',
    	-command=> sub {
    		show_html('file:\\'.$CURRENT_CONFIG{gem_root_directory}."/".'doc/gem/index.htm');
    	} )->grid( -row=>0, -column=>4, -padx=>5 );
    my($home_base) = $welcome_bot_f->Button( -text=>"Home Base", -activebackground=>'beige',
    	-command=> sub {
    		show_html('http://www.edbarlow.com/gem/index.html');
    	} )->grid( -row=>0, -column=>5, -padx=>5 );


    $button_design->bind('<Enter>',  	sub { _statusmsg("noprint","Show plugin design specification"); });
    $button_design->bind('<Leave>',  	sub { _statusmsg("noprint",""); });
    $button_log->bind('<Enter>',  	sub { _statusmsg("noprint","Show Application Change Log"); });
    $button_log->bind('<Leave>',  	sub { _statusmsg("noprint",""); });
    $button_overview->bind('<Enter>',  	sub { _statusmsg("noprint","Show Overview Powerpoint Presentation"); });
    $button_overview->bind('<Leave>',  	sub { _statusmsg("noprint",""); });
    $button_spec->bind('<Enter>',  	sub { _statusmsg("noprint","Show GEM specification"); });
    $button_spec->bind('<Leave>',  	sub { _statusmsg("noprint",""); });
    $button_guide->bind('<Enter>',  	sub { _statusmsg("noprint","GEM Users Guide"); });
    $button_guide->bind('<Leave>',  	sub { _statusmsg("noprint",""); });
    $home_base->bind('<Enter>',  	sub { _statusmsg("noprint","GEM Home Page"); });
    $home_base->bind('<Leave>',  	sub { _statusmsg("noprint",""); });

    return $tab;
}

sub browse {
	$curExplorerItem = shift;
	_statusmsg( "[gem.pl] browse(item=$curExplorerItem, Notebook Tab=".$notebook_f->raised().")\n" );
}



sub create_log_tab {
    my $tab = create_generic_tab("log","Log Messages");

    #my $tab = $notebook_f->add('Log',-label=>'Log Messages',-underline=>0);
    #$notebook_frame{log}  = $tab->Frame( -width=>$tab_width, -height=>$tab_height)->pack(
    #	-anchor=>'nw', -fill=>'both', -expand=>1 );
    $log_text= $notebook_frame{log}->Scrolled("Text")->pack(-expand=>1,-fill=>'both');
	 $log_text->tagConfigure('red',foreground=>'red');
	 $log_text->tagConfigure('blue',foreground=>'blue');
	 $log_text->tagConfigure('green',foreground=>'green');

    $notebook_frame{log}->Button( -text=>"Clear Message Log", -activebackground=>'beige', -command=> sub {
    	_clear_message_log("Button Pressed: Clear Message Log"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    $notebook_frame{log}->Button( -text=>"Save Message Log", -activebackground=>'beige', -command=> sub {
    	unimplemented("save message log"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    $notebook_frame{log}->Button( -text=>"Show Errors", -activebackground=>'beige', -command=> sub {
    	unimplemented("show only errors"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    $notebook_frame{log}->Button( -text=>"Show All Messages", -activebackground=>'beige', -command=> sub {
    	unimplemented("show all logs"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );

    return $tab;
}

sub create_sql_tab {
    my $tab = create_generic_tab("sql","SQL Text");
    #my $tab = $notebook_f->add('Sql',-label=>'Sql Text',-underline=>0);
    #$notebook_frame{sql}  = $tab->Frame( -width=>$tab_width, -height=>$tab_height)->pack(
    # 	-anchor=>"nw", -fill=>'both', -expand=>1 );
    $sql_text= $notebook_frame{sql}->Scrolled("Text")->pack(-expand=>1,-fill=>'both');
    $sql_text->tagConfigure('blue',foreground=>'blue');
    $sql_text->tagConfigure('red',foreground=>'red');

    $notebook_frame{sql}->Button( -text=>"Clear Sql Log", -activebackground=>'beige', -command=> sub { unimplemented("clear sql log"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    $notebook_frame{sql}->Button( -text=>"Save SQL Log", -activebackground=>'beige', -command=> sub { unimplemented("save sql log"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    $notebook_frame{sql}->Button( -text=>"Show All Sql", -activebackground=>'beige', -command=> sub { unimplemented("show all sql"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    return $tab;
}

sub create_debugmsg_tab {
    	my $tab = create_generic_tab("Debug","Debug Msgs");

    	#my $tab  = $notebook_f->add('Debug',-label=>'Debug Msgs',-underline=>0);
    	#$notebook_frame{debug}  = $tab->Frame( -width=>$tab_width, -height=>$tab_height)->pack(
    	#	-anchor=>"nw", -fill=>'both', -expand=>1 );
    	$debug_text= $notebook_frame{debug}->Scrolled("Text")->pack(-expand=>1,-fill=>'both');
    	#$debug_line=1;
	$debug_text->configure(-state=>'disabled');

	$debug_text->tagConfigure('red',foreground=>'red');
	$debug_text->tagConfigure('blue',foreground=>'blue');
	$debug_text->tagConfigure('green',foreground=>'green');
    	$notebook_frame{debug}->Button( -text=>"Clear Debug Log", -activebackground=>'beige',
    		 -command=> sub { _clear_debug_log("Button Pressed : Clear Debug Log"); })->pack( -side=>'left', -padx=>3, -pady=>8 );
    	$notebook_frame{debug}->Button( -text=>"Save Message Log", -activebackground=>'beige',
    		-command=> sub { unimplemented("save debug log"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    	$notebook_frame{debug}->Button( -text=>"Show Errors", -activebackground=>'beige',
    		-command=> sub { unimplemented("show only errors"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    	$notebook_frame{debug}->Button( -text=>"Show All Messages", -activebackground=>'beige',
    		-command=> sub { unimplemented("show all logs"); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    	return $tab;
}

sub _clear_message_log
{
	my($msg)=@_;
	$log_text->delete('1.0','end');
	@pre_log_ok_msgs=();
	_statusmsg("clearing message log");
   	_statusmsg( $msg ) if defined $msg;
}

sub _clear_debug_log
{
	my($msg)=@_;
	_statusmsg("[gem.pl] Clearing Debug Messages Window");
	$debug_text->configure(-state=>'normal') if defined $debug_text;
	$debug_text->delete('1.0','end');
	#$debug_line=0;
	@pre_dbg_ok_msgs=();
	_debugmsg( $msg ) if defined $msg;
	$debug_text->configure(-state=>'disabled') if defined $debug_text;
}


sub setBitmaps {
   my($frame, $item_aryref)=@_;
   _statusmsg("[gem.pl]   setBitmaps() - painting bitmaps");
   my(@items)=@$item_aryref;

   #my $balloon = $CURRENT_CONFIG{mainWindow}->Balloon;

   my( %order );
   my($i)=0;
   foreach my $itmhsh ( @items ) {
   	my(%x)=%$itmhsh;
   	if( defined $DEBUG and ! defined $x{order} ) {
   		foreach ( keys %x ) {
			_debugmsg( "k=",$_, " v=",$x{$_},"\n" );
		}
		die "Required Field Order Not Defined For Bitmap Item\n";
	}
   	$order{$x{order}} = $i++;
   }
   my @orderary = sort { $a <=> $b } keys %order;
   # print "DBG: ORDER ARRAY = ",join(" ",@orderary),"\n";
   foreach ( @orderary ) {
   	my($idx)=$order{$_};
   	my(%x) = %{$items[$idx]};

   	#if( defined $DEBUG ) {
   	#	_debugmsg( " -------- Bitmap Item Number $idx ----------\n");
	#	foreach ( keys %x ) {
	#		_debugmsg( "k=",$_, " v=",$x{$_},"\n" );
	#	}
	#}

   	$CURRENT_CONFIG{mainWindow}->Bitmap($x{name},-data => $x{image});
   	my $func;
   	if( $x{plugin} eq "" ) {
   		if( $x{name} eq "collapseall" ) {
   			$func = sub { collapseAll(); };
   		} else {
	   		$func = sub { expandAll(); };
   		}
   	} else {
	 	$func = sub {	};
	}
   	my $bbuttons = $frame->Button(
   	       -text       => $x{help_text},
               -image      => $x{name},
               -activebackground=>'beige',
               -command    => $func,
	       -height       => 16,
               -width        => 16 )->pack(-side => 'left', -padx => 2, -pady => 2);

        if( defined $x{baloon_text}) {
		$bbuttons->bind('<Enter>',  sub { _statusmsg("noprint",$x{baloon_text}); });
		$bbuttons->bind('<Leave>',  sub { _statusmsg("noprint",""); });
	}
   	#$balloon->attach($bbuttons{$x{name}},  -balloonmsg =>$x{baloon_text});
   }
}



   # button bitmaps


sub _rtclick {
   	my($entryPath,$text)=@_;
   	_statusmsg( "[gem.pl] _rtclick(path=$entryPath,text=$text)\n");
   	$curExplorerItem = $entryPath;

   	# ok is this a special item
   	if( $text eq "What Is This" ) {
   		my $dialog=$CURRENT_CONFIG{mainWindow}->DialogBox(-title=>"What Is This",-buttons=>["Ok"]);
   		$dialog->Label(-text=>"No Help Embedded Yet\nfor $entryPath\n$text\n")->pack();
   		$dialog->Show;
		return;
   	}

   	## ok is it an add or delete item?
   	#if( $text=~/^add\s/i or $text=~/^edit\s/i or $text=~/^delete\s/i ) {
	#	my(@x)=split(" ",$text);
	#	my(@y)=split("\/",$entryPath);
	#	die "Error add/edit/delete rtclick may only be internally generated"
	#		unless $#x == 1;
	#	$packagemanager->item_mgr(lc($x[0]),$y[1],$y[d2]);
	#	return;
   	#}
	#
   	#my $dialog=$CURRENT_CONFIG{mainWindow}->DialogBox(-title=>"Rt Button Clicked",-buttons=>["Ok"]);
   	#$dialog->Label(-text=>"Button Clicked\n$entryPath\n$text\n")->pack();
   	#$dialog->Show;
}

sub _statusmsg {
    my $cmd = shift;
    my $msg = join("",@_);

    if( $cmd ne "noprint" ) {
    	$msg = $cmd.$msg;
    	$cmd = "";
    }

    chomp $msg;
    if( $msg =~ /\[/ ) {
    	my($p,$m)=split(/\s/,$msg,2);
    	$msg=sprintf("%-12.12s| %s",$p,$m);
    }
    my(@msgary)=split(/\n/,$msg);
    #print ">> $msg \n" unless $cmd ne "" or $print_stdout ne "TRUE";
    #if( $cmd eq "" and $print_stdout eq "TRUE" ) {
    #	foreach( @msgary ) { print ">> $_\n"; }
    #}

    if( $cmd eq "" ) {
    	if( $print_stdout eq "TRUE" ) {
    		foreach( @msgary ) { print ">> $_\n"; }
    	}
    	foreach( @msgary ) { print LOGFP ">> $_\n"; }
    }

    if ( $status_bar_ready eq "TRUE" and defined $statusmsgbar and $msg !~ /^\#\#\#\#/ ) {
       $msg = $_statusmsg_idle unless $msg;
       #print "STATUSMSG is $statusmsgbar - MSG is $msgary[0] \n";
       print __FILE__," ",__LINE__," DBG ERROR - $status_bar_ready STATUSMSGBAR is not a tk object\n" unless Tk::Exists($statusmsgbar);
       $statusmsgbar->configure( -text => $msgary[0] );
       $statusmsgbar->update;
print "UPDATE JUST RUN\n";
       return if $msg eq $_statusmsg_idle;
    }

    if( defined $log_text ) {
   	if( $#pre_log_ok_msgs>=0 ) {
      		foreach ( @pre_log_ok_msgs ) {
   			$log_text->insert('1.0',": ".$_."\n");
 	   		$log_text->insert('1.0',do_time(-fmt=>"yyyy.mm.dd hh:mi"),'blue');
 			_debugmsg("(status)".$_);
      		}
      		@pre_log_ok_msgs=();
   	}
   	if( $msg ne $_statusmsg_idle ) {
   		$log_text->insert('1.0',": ".$msg."\n");
 		$log_text->insert('1.0',do_time(-fmt=>"yyyy.mm.dd hh:mi"),'blue');
   		_debugmsg("noprint","status:".$msg);
   	}
    } else {
   	push @pre_log_ok_msgs,$msg;
    }
}

sub _debugmsg {
    my $cmd = shift;
    my $msg = join("",@_);

    if( $cmd ne "noprint" ) {
    	$msg = $cmd.$msg;
    	$cmd = "";
    }

    chomp $msg;
    return if ! defined $DEBUG and $cmd eq "";

    $debug_text->configure(-state=>'normal') if defined $debug_text;
    print "dbg>> $msg \n" unless $cmd eq "noprint" or $print_stdout ne "TRUE";

    if( defined $debug_text ) {
   	if( $#pre_dbg_ok_msgs>=0 ) {
      	foreach ( @pre_dbg_ok_msgs ) {
   		$debug_text->insert('end',do_time(-fmt=>"yyyy.mm.dd hh:mi"),'blue');
		if( $cmd eq "noprint" ) {
   			$debug_text->insert('end',": ".$_."\n",'green');
		} else {
   			$debug_text->insert('end',": ".$_."\n");
		}
		#$debug_line++;
      	}
      	@pre_dbg_ok_msgs=();
   	}
   	$debug_text->insert('end',do_time(-fmt=>"yyyy.mm.dd hh:mi"),'blue');
	if( $cmd eq "noprint" ) {
   		$debug_text->insert('end',": ".$msg."\n",'green');
	} else {
   		$debug_text->insert('end',": ".$msg."\n");
   	}
	#$debug_line++;
    } else {
   	push @pre_dbg_ok_msgs,$msg;
    }
    $debug_text->configure(-state=>'disabled') if defined $debug_text;
}

sub menuclick {
	my($packagename,$txt) = @_;
}

sub create_rtclick_vars {
   my($path)=@_;
   #_statusmsg( "[gem.pl] create_rtclick_vars( $path )\n");
   my $y=[ basename($path),"sep" ];
   $rtbtnlbl_bypath{$path} = \@$y;
   my $z=[ \&_rtclick, undef ];
   $rtbtncmd_bypath{$path} = \@$z;
}

sub add_rtclick {
   my($path,$text,$cmd)=@_;
   push @{$rtbtnlbl_bypath{$path}} , $text;
   push @{$rtbtncmd_bypath{$path}} , $cmd;
}

sub clear_tab {
	my($tabname)=@_;
	#$is_in_clear_tab="TRUE";
	die "No Pane Passed to [gem.pl] clear_tab()\n" unless defined $tabname;
	my($curtab)=$notebook_f->raised();
	_statusmsg( "[gem.pl] Clearing $tabname Tab (current raised tab=$curtab)\n" );
	my($frm)= $user_dfnd_frames{$tabname};

	my(@packinfo);
	if( defined $frm ) {
		@packinfo = $frm->packInfo();
		#print "PACK INFO FOR user defined frame $tabname: $frm ",join(" ",@packinfo),"\n";
	} else {
		@packinfo = (-fill=>'both', -expand=>1, -side=>'top');
		#print "PACK INFO no user defined frame $tabname: ",join(" ",@packinfo),"\n";
	}
	#print "PACK INFO FOR notebook tab $tabname $notebook_frame{$tabname}: ",join(" ",$notebook_frame{$tabname}->packInfo()),"\n";
	$user_dfnd_frames{$tabname}=undef;
	$frm->destroy() if defined $frm;

   	if( ! defined $notebook_frame{$tabname} ) {
   		_statusmsg("Odd No Frame Defined For $tabname");
   		foreach ( keys %notebook_frame ) {
   			_statusmsg( "... key=$_ val=$notebook_frame{$_} \n");
   		}
   	}
   	#my($uframe) = $notebook_frame{$tabname}->Frame(-bg=>'blue')->pack( -fill=>'x', -expand=>1, -side=>'top');
   	if( ! defined $notebook_frame{$tabname} ) {
   		_statusmsg("[gem.pl] ERROR no notebook frame named $tabname.\n" );
   		_statusmsg("[gem.pl] Allowed values are:\n" );
   		foreach ( keys %notebook_frame ) { _statusmsg("[gem.pl] $_\n"); }
   		#$is_in_clear_tab="FALSE";
   		die "Can not continue.  Aborting program.\n";
   	}

   	my($uframe) = $notebook_frame{$tabname}->Frame()->pack( @packinfo );
	# print "DBG34: PACK INFO FOR new frame $uframe in $tabname : ",join(" ",$uframe->packInfo()),"\n";
	$user_dfnd_frames{$tabname}=$uframe;
	#_statusmsg( "[gem.pl] Raising Tab $tabname\n" );
	#$notebook_f->raise(ucfirst($tabname)) unless $tabname eq $curtab;

	# _statusmsg( "[gem.pl] clear_tab() completed - returning $uframe \n" );
	print "DBG gem.pl WOAH - $uframe DOES NOT EXIST\n" unless Tk::Exists($uframe);

	#$is_in_clear_tab="FALSE";

	return $uframe;
}


sub command {
    _debugmsg( "command(",join(" ",@_),")\n" );
    my($path)=@_;
    if( $tree->getmode( $path ) eq "open" ) {
   	$tree->setmode( $path, "close" );
   	$tree->close( $path );
    } else {
   	$tree->setmode( $path, "open" );
   	$tree->open( $path );
    }
    _statusmsg();
}

sub expandAll {
    _debugmsg( "expandAll(",join(" ",@_),")\n");
    my $entryPath = $firstnode;
    while ($tree->info('exists',$entryPath)) {
   	$tree->open($entryPath);
   	$entryPath = $tree->info('next',$entryPath);
    }
    $tree->autosetmode();
    $tree->update; # forces scrollbar to update
print "UPDATE JUST RUN\n";
}

sub collapseAll {
    _debugmsg( "collapseAll(",join(" ",@_),")\n" );
    my $entryPath = $firstnode;
    while ($tree->info('exists',$entryPath)) {
    	my(@ct)=split(/\//,$entryPath);
   	$tree->close($entryPath) unless $#ct<2;
   	$entryPath = $tree->info('next',$entryPath);
    }
    $tree->open("/");
    $tree->selectionClear;
    $tree->autosetmode();
    $tree->update; # forces scrollbar to update
print "UPDATE JUST RUN\n";
}

sub select {
    _debugmsg( "select(",join(" ",@_),")\n" );
    my $entryPath = shift;
    return unless ($tree->info('exists',$entryPath));
    my $parent = $entryPath;
    $tree->open($parent) while ($parent = $tree->info('parent',$parent));
    $tree->selectionClear;
    $tree->see($entryPath);
    $tree->selectionSet($entryPath);
    $tree->update;
print "UPDATE JUST RUN\n";
}

sub splashupdate {
	$splashscreen->Update( $cursplash++/$sumsplash);
}

sub paint_a_menubar_item {
	my( $menupath,$packagename )=@_;
	_debugmsg( "[gem.pl] Building menuitem $menupath \n");

	my( @paths ) = split(/\//, $menupath);
	die "Too Many Dropdowns For Menu $menupath of $packagename" if $#paths>3;
	die "Too Few Dropdowns For Menu $menupath of $packagename"  if $#paths<0;
	_debugmsg( "[gem.pl]      Pkg=$packagename Nel=$#paths \n" );

	if( ! defined $submenu{$paths[0]} ) {
		_debugmsg("[gem.pl]      reated menu toplevel ($paths[0])\n");
    		$submenu{$paths[0]} = $menu->Menubutton(-text =>$paths[0], -tearoff => 0);
		die "Could Not Create Menu Item : $! " 	unless defined $submenu{$paths[0]};
	}

	if( $#paths >= 1 ) {
      		_debugmsg( "[gem.pl]      creating dropdown ".$paths[1]."\n");
		my($root,$parent)=($paths[0]."/".$paths[1],$paths[0]);
		if( $#paths == 1 ) {
			$submenu{$root} = $submenu{$parent}->command(-label=>$paths[1],
				-command=>sub { menuclick( $packagename,$menupath);})
							unless defined $submenu{$root};
		} else {
			$submenu{$root} = $submenu{$parent}->cascade(
				-label=>$paths[1],
				-tearoff=>0,
				-command=>sub {
					unimplemented("dropdown for $root at line ",__LINE__);
					}					)
							unless defined $submenu{$root};
		}
   	}

	if( $#paths >= 2 ) {
      		_debugmsg( "[gem.pl]      creating dropdown ".$paths[2]."\n");
		my($root,$parent)=($paths[0]."/".$paths[1]."/".$paths[2],$paths[0]."/".$paths[1]);

		if( $#paths == 2 ) {
			$submenu{$root} = $submenu{$parent}->command(-label=>$paths[2],
				-command=>sub { menuclick( $packagename,$menupath);})
							unless defined $submenu{$root};
		} else {
			$submenu{$root} = $submenu{$parent}->cascade(-label=>$paths[2],
				-tearoff=>0,
				-command=>sub { unimplemented("dropdown for $root at line ",__LINE__); })
							unless defined $submenu{$root};
		}
	}

	if( $#paths == 3 ) {
      		_debugmsg( "[gem.pl]      creating dropdown ".$paths[3]."\n");
		my($root,$parent)=($paths[0]."/".$paths[1]."/".$paths[2]."/".$paths[3],
			$paths[0]."/".$paths[1]."/".$paths[2]);

    		$submenu{$root} = $submenu{$parent}->command(-label=>$paths[3],
				-command=>sub { menuclick( $packagename,$menupath);})
							unless defined $submenu{$root};
	}
}

sub refresh_tree {
	my($view)=@_;
	_statusmsg( "[gem.pl] refresh_tree(curTreeType=$curTreeType, tab=".$notebook_f->raised().", view=$view)");
	$curTreeType=$view if defined $view;

	die "No Tree" unless defined $tree;
	$tree->delete('all');

		# Get Data For The Tree
	# returns pointer to array of text ($ti_ref), and ptr to arrayref of
	# right click items
	_statusmsg("[gem.pl]   Getting Tree Data");

	print __FILE__," ",__LINE__," DBG ERROR: - STATUSMSGBAR is a tk object\n" if Tk::Exists($main::statusmsgbar);
        print __FILE__," ",__LINE__," DBG ERROR: - STATUSMSGBAR is not a tk object\n" unless Tk::Exists($main::statusmsgbar);

	my($i)=0;
	#_statusmsg("[gem.pl]    CHECKING TREE STATE\n" );
	$tree->add('/',-text=>'/');

     	print __FILE__," ",__LINE__," DBG ERROR: - STATUSMSGBAR is a tk object\n" if Tk::Exists($main::statusmsgbar);
        print __FILE__," ",__LINE__," DBG ERROR: - STATUSMSGBAR is not a tk object\n" unless Tk::Exists($main::statusmsgbar);
        print __FILE__," ",__LINE__," DBG ERROR: Perl is $] - $^X\n";
        foreach (@INC) { print "Include $_\n"; }

     	$tree->update(); # forces scrollbar to update
print "UPDATE JUST RUN\n";
     	# browse("/") unless defined $is_same_type;
   	$tree->autosetmode();

	print __FILE__," ",__LINE__," DBG ERROR: - STATUSMSGBAR is a tk object\n" if Tk::Exists($main::statusmsgbar);
        print __FILE__," ",__LINE__," DBG ERROR: - STATUSMSGBAR is not a tk object\n" unless Tk::Exists($main::statusmsgbar);

   	_statusmsg("[gem.pl]   completed refresh_tree($curTreeType)");
}

sub build_menubar {
   	_statusmsg("[gem.pl]   Retrieving Dynamic Menu Items");
}

sub unimplemented {
	my($string)=@_;
	_statusmsg("(not) $string");
   my $dialog = $CURRENT_CONFIG{mainWindow}->DialogBox(-title=>"Unimplemented", -buttons=>["Ok"]);
   $dialog->Label(-text=>"<To Do> $string")->pack();
   $dialog->Show;
}

sub setDebugMode {
   $DEBUG=undef if defined $DEBUG and $DEBUG==0;
   if( defined $DEBUG ) {
	_statusmsg("[gem.pl] Turning On Debug mode - enabling Debug tab");
	$notebook_f->pageconfigure("Debug",-state=>"normal" );
   } else {
	_statusmsg("[gem.pl] Turning Off Debug mode - disabling Debug tab");
	$notebook_f->pageconfigure("Debug",-state=>"disabled" );
   }
}
