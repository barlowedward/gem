#!/apps/perl/linux/perl-5.8.2/bin/perl

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use Do_Time;
use MlpAlarm;

$|=1;

my($dir)="/apps/imagine/its50/objroot/logroot/compute_servers";
my($today)=do_time(-fmt=>"_yymmdd_");
my(@user_watch_list)=qw(slorne ienglander roger tfeeney cnewman clively mmeskin  nmoniodes);
my($timethreshmin)=5;
my(@error_ignore_list);		# regexp's to ignore

########### START THE PROGRAM ##############
print "IMAGINE LOGFILE ERROR REPORT\n";
print "Report run at ",scalar(localtime(time)),"\n";

opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
my(@dirlist) = grep(/$today/,readdir(DIR));
closedir(DIR);

my(%watchlist);
my(%num_sessions_by_user,%num_completed_by_user);
foreach (@user_watch_list) {
	$watchlist{$_}=1;
	$num_sessions_by_user{$_}=0;
	$num_completed_by_user{$_}=0;
}

while(<DATA>){
	chomp;
	chomp;
	next if /^#/ or /^\s*$/;
	push @error_ignore_list,$_;
}

my(%user_by_filename);
foreach my $file (@dirlist) {
	$file=~/out\.its\.([^_]+)\_/;
	my($user)=$1;
	next unless $watchlist{$user};
	my($moddays)=  -M $dir."/".$file;
	$moddays*=24*60;
	# print "Working on File $file ($moddays minutes old)\n";
	# print "User=$user\n";
	if( $moddays<$timethreshmin ) {
		$num_sessions_by_user{$user}++ ;
		$user_by_filename{$file}=$user;
	}
}

my(%is_initialized,%book_by_file);
foreach (keys %user_by_filename) {
	process_file($_);
}

foreach (@user_watch_list) {
	my($msg)= "User $_ has $num_sessions_by_user{$_} imagine sessions $num_completed_by_user{$_} completed\n";
	print $msg;
	my($state)="COMPLETED";
	$state="ALERT" if $num_sessions_by_user{$_}==0;
	$state="ALERT" if $num_sessions_by_user{$_}!=$num_completed_by_user{$_};

	MlpHeartbeat( -monitor_program=>"ImagineMonitor",
					-system=>"IMAGINE",
					-subsystem=>"User: $_",
					-state=>$state,
					-message_text=>$msg
	);
}
exit(0);

sub process_file
{
	my($fname)=@_;
	print "\n###############################\n";
	print "# processing $fname\n";
	print "###############################\n";
	open(FP,$dir."/".$fname) or die "Cant open $fname : $!\n";
	while(<FP>) {
		# Figure out which ledger was brought up
		# hts_xact_hist, -M- [ics]
		# the string for this is: New Frame
		#print substr($_,18,9),"\n";

		if( /select \* from hts_xact_history where hist_timestamp/ ) {
			# THIS SESSION INITIALIZED
			if( ! defined $is_initialized{$fname} ) {
				$is_initialized{$fname}=substr($_,0,17);
				$num_completed_by_user{$user_by_filename{$fname}}++;
			}
		}
		# # check book is loaded
		# if( substr($_,18,9) eq "-M- \[ics\]" and /New frame \</ and /Ledger\>/) {
			# /([\w\_\~]+) - Ledger\>/;
			# my($book)=$1;
			# if( defined $book_by_file{$fname} ) {
				# $book_by_file{$fname}.=" ".$book
					# unless $book_by_file{$fname}=~/$book/;
			# } else {
				# $book_by_file{$fname}=$book;
			# }
		# }
		# search for -E- -W-
		next unless / -E- / or / -W- /;

		# exclude msgs from filter file
		my($found)="FALSE";
		chomp;
		my($line)=$_;
		foreach my $str ( @error_ignore_list ) {
			# print " Comparing $line \n";
			# print " to $str \n";
			if( $line =~ /$str/) {
				$found="TRUE";
				last;
			}
		}
		next if $found eq "TRUE";
		print "[",$fname,"\] ",$_,"\n";
	}
	close(FP);
}
__DATA__

Could not get Global:ITS exchange rate asof 0
Your License Server is Back Up;
Correlation table NOT Loaded
Reuters error <The record could not be found>
You are NOT permissioned for <RESET_WIZ> feature
# Attempt to retrieve index of nonexistent total field:
Could not set up field map for database table
Invalid app Field <Arbitrage Account>.
Reuters error <Record not service permissioned> for symbol
exchange rate asof 19991231
Changed database context
Changed language setting
No such feature exists (-5,357)
CORBA::Exception binding server
Too many holdings returned, cancel query
The scenario DEFAULT.ITS was modified. Do you want to save it to the database
Attempt to retrieve index of nonexistent total field: 'CapReq'
Attempt to retrieve index of nonexistent total field: 'CapReqPaired'
Could not get Global:ITS exchange rate asof 0
Invalid app Field <Arbitrage Account>.
gen_dbx.c_853:  table id < 0.
-W- Reuters error <Item name: too long>
-W- Reuters error <Non-updating item> for symbol
Correlation Table NOT Loaded
-W- Stale Symbol:
-E- SQL-Main Message Handler\[3604\]\: \<Duplicate key was ignored.\>
-E- Error evaluating name rule; moving holding to <UNASSIGNED> portfolio
Ignoring query
