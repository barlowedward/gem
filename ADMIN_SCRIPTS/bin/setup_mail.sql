declare @m1 varchar(255)
declare @m2 varchar(255)
exec sp_configure 'Database Mail XPs', 1
RECONFIGURE
--Set up the profile
EXECUTE msdb.dbo.sysmail_add_profile_sp
@profile_name = 'Database Mail',
@description = 'Profile used for administrative mail.';
--Set up the account which will be used to send the mail to operators
EXECUTE msdb.dbo.sysmail_add_account_sp
@account_name = 'SQLMail',
@description = 'SQLMail',
@email_address = 'SQLMail@mlp.com',
@display_name = 'SQLMail',
@mailserver_name = 'EXCHUS003.AD.MLP.COM'
--Add the account to corresponding profile
EXECUTE msdb.dbo.sysmail_add_profileaccount_sp
@profile_name = 'Database Mail',
@account_name = 'SQLMail',
@sequence_number = 1
--Test the mail whether works
select @m1 = 'The database mail has been set up successfully on '+@@SERVERNAME
select @m2 = 'Database Mail Testing on '+@@SERVERNAME
EXEC msdb.dbo.sp_send_dbmail
@profile_name = 'Database Mail',
@recipients = 'ebarlow@mlp.com',
@body = @m1,
@subject = @m2
EXEC sp_configure 'show advanced option', '0';
RECONFIGURE
