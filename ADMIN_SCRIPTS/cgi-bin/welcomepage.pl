use lib qw(.);

use strict;
use MlpAlarm;
use CommonFunc;
use Do_Time;
use DBIFunc;

use CGI qw( :standard escapeHTML escape);
use CGI::Carp qw(fatalsToBrowser);
use File::Basename;

my($COMPANYNAME)="Millenium Partner\'s";
my($LOGON_USER)=$ENV{LOGON_USER};
$LOGON_USER=~s/^ad[\\\/]//i;

print header, start_html(-title=>$COMPANYNAME, 	-style=>'styles/main.css', -style=>'styles/Customer.css');

print "<center>",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;";
print "<b><h1>WELCOME TO THE ENTERPRISE DASHBOARD</h1></b>",p;
my($rc)=MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>1);
if( $rc ne "Y" ) {
	my($str)="$ENV{LOGON_USER}: ACCESS DENIED".br."Please Contact Support".br;
	my(@rc)=MlpGetPermission(-samid=>$ENV{LOGON_USER}, -canaccess=>0);
	foreach( @rc ) {
		$str.="Ok: $_".br;
	}
	if( $#rc<0 ) {
		$str.="You have NO permissions".p;
		my(@x)=MlpGetVars();
		$str.="Repository Server=$x[0] Repository Login=$x[1] Repository DB=$x[3]".p;
	}
	print "<center><FONT face=sans-serif size=+1 color=red>$str</FONT></center>\n";
	print end_html;
	exit(0);
}

my(@p)=MlpGetPermission(-samid=>$ENV{LOGON_USER}, -canaccess=>0);

# copied to welcomepage
print "<BR><B>YOUR PERMISSIONS</B><BR>\n";
my(%page);
print "<TABLE><TR><TD VALIGN=TOP>";
foreach ( sort @p ) {
	# print "P=$_",br;
	print i($_).br."\n" unless /^Dflt/ or /^\s*$/ or /PG$/;
	if( /PG$/ ) {
		$page{$_}=1;
	}
}
print "</TD><TD WIDTH=15>&nbsp;&nbsp;</TD><TD VALIGN=TOP>";
#print br,"\n";
foreach ( sort ( keys %page )) {
#	if( $page{$_} eq "ADMIN" ) {
#		print i($_)." (admin)".br."\n";
#	} else {
		print i($_).br."\n";
#		print i($_)." (view)".br."\n";
#	}
}
print "</TD></TR></TABLE>";


print p,"&nbsp;",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;\n";
print "Authenticated as ".$LOGON_USER,p,"</center>\n";

print end_html;
exit(0)
