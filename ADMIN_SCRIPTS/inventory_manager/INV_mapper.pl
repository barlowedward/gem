#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use DBIFunc;
my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw($BATCH_ID $DEBUG $STEP $SHOWSQL);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID
	--STEP=num			[ only do the noted step ]
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --SHOWSQL
   --DEBUG \n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"STEP=s"				=> \$STEP ,
		"SHOWSQL"			=> \$SHOWSQL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

$STEP=0 unless $STEP;

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Credential Mgt' ) if $BATCH_ID;

print "INVENTORY SYSTEM MAPPER\n";
print " - this program will map saved credentials to principals\n";
print " - this program is restartable\n\n";

sub helptext {
	my($msg)=@_;
	foreach ( split(/\n/,$msg) ) {
		print "   helptext: $_\n";
	}
}

sub diagnose {
	my($line)=@_;

	return;
	#"select credential_name,principal_name,principal_type,is_deleted from INV_principal_map where system_name='ADSRV088' and match_notes_long='MATCHED LDAP USER'";

	my($query)=
"select m.is_ldap_user, n.is_ldap_user,m.principal_name,m.principal_type,n.ldap_user
from INV_principal_map m, INV_credential_notes n
where m.system_name='SSDB' and m.credential_name like 'tlm_mpant' and n.credential_name = m.credential_name
and   n.credential_type  = m.credential_type";

	my(@rc)=MlpAlarm::_querywithresults($query,1);
	foreach (@rc) { print $line,' ',join("\t",dbi_decode_row($_)),"\n";	}
}

my($step,$step2,$step3)=(0,1,1);
sub banner1 {
	$step++;
	print "\n############################################################################\n";
	print "# Step ",$step," : ",join(" ",@_),"\n";
	print "############################################################################\n";
	$step2=1;
}

sub status_done {
	my($msg,$is_ok);
	print "*** COMPLETED Step ",$step," : ",join(" ",@_),"\n";
}

sub banner2 {
	print "** Step $step.$step2 : ",join(" ",@_),"\n";
	$step2++;
	$step3=1;
	# diagnose(__LINE__);
}

sub banner3 {
	print "*** Step $step.$step2.$step3 : ",join(" ",@_),"\n";
	$step3++;
}

sub show_query {
	return unless $DEBUG;
	my($q)=join("",@_);
	chomp $q;
	$q=~s/^\s*//;
	$q=~s/\t/   /g;
	$q=~s/\n\n//g;
	my(@qry)=split(/\n/,$q);
	print ":: ".join("\n:: ",@qry),"\n\n";
}

# ALGORITHM
# REFRESH PRINCIPAL MAP
#  - mark INV_principal_map.is_deleted
#  - insert new INV_principal_map rows

#my($connection)=MlpGetDbh();
#my($LDAP_KEY) = "ldap TEMP";
#LDAPinit($connection, $SHOWSQL, $PROGRAM_NAME, $DEBUG, $LDAP_KEY);

banner1("Deleting INV_principal_map");
helptext("Clear Principal Map For Overwrite");
my($query)='delete INV_principal_map';
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner1("Routine Cleanup");
banner2("Updating is_deleted on credentials where lock% disabl% or expire% attribute is set");
$query="UPDATE INV_credential
set is_deleted='Y'
from INV_credential c, INV_credential_attribute a
where ( lower(attribute_key) like 'disabl%' or lower(attribute_key) like 'lock%' or lower(attribute_key) like 'expire%')
and   attribute_value in ( 'Y','yes', '1','YES')
and   c.credential_name = a.credential_name
and   c.credential_type = a.credential_type
and   isnull(c.system_name,'x') = isnull(a.system_name,'x')
and   isnull(c.system_type,'x') = isnull(a.system_type,'x')";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner2("Updating Display Name On LDAP users/groups ONLY FOr Actual Users");
helptext("Update Credential Display Name For ldap users to 'display' attribute - parens");
$query="UPDATE INV_credential
set 		display_name=rtrim(substring(attribute_value,1,charindex('(', attribute_value)-1))
from   	INV_credential m, INV_credential_attribute a
where		m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and      m.credential_type = 'ldap user'
and     	a.attribute_key  = 'display'
and      m.display_name is null
and 		charindex(',',attribute_value)!=0
and 		charindex('(',attribute_value)!=0
and 		attribute_value != ''
-- and 		patindex('%disabled%',lower(attribute_value))=0
";
show_query($query);
MlpAlarm::_querynoresults($query,1);

helptext("Update Credential Display Name For ldap users to 'display' attribute - noparens");
$query="UPDATE INV_credential
set 		display_name=rtrim(attribute_value)
from   	INV_credential m, INV_credential_attribute a
where		m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and      m.credential_type = 'ldap user'
and     	a.attribute_key  = 'display'
and      m.display_name is null
and 		charindex(',',attribute_value)!=0
and 		charindex('(',attribute_value)=0
and 		attribute_value != ''
-- and 		patindex('%disabled%',lower(attribute_value))=0
";
show_query($query);
MlpAlarm::_querynoresults($query,1);

# the following is my best guess at credentials that represent real accounts
# the display_name field is *ONLY* used to populate drop downs...
helptext("Update Display Name On the Credential = credential name For Usable ldap groups");
$query="UPDATE INV_credential
set 		display_name=credential_name
where 	credential_type='ldap group'
and		credential_name not like 'service\%'
and 		lower(credential_name) not like 'zz\%'
and 		credential_name not like '~_\%' escape '~'
and 		credential_name not like 'acl-\%'
and 		credential_name not like 'drv-\%'
and 		credential_name not like 'prn_\%'
and 		credential_name not like 'prv_\%'
and 		credential_name not like '\%ScreenSaver\%'
and 		credential_name not like 'Pub_\%'
and 		credential_name not like 'SPAM\%'
and 		credential_name not like '\$\%'	escape '~'";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner1("Creating User Name Temporary Table");
helptext("Create Temporay Table #NAMES (names) and #NAMESP (permuted)");
$query="select distinct credential_name, ln=lower(attribute_value), fn=convert(varchar(60),'XXX'), initial='a'
into   #NAMES
from   INV_credential_attribute a
where  credential_type='ldap user' and attribute_key='ln' and attribute_value!=''";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="UPDATE #NAMES
set  fn = lower(attribute_value), initial=substring(lower(attribute_value),1,1)
from   #NAMES n, INV_credential_attribute a
where  n.credential_name=a.credential_name
and    a.credential_type='ldap user'
and 	 a.attribute_key='fn' and attribute_value!=''";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="DELETE #NAMES where fn=ln or fn='XXX' or fn is null or ln is null";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

$query="select credential_name, permutation=n.initial+n.ln INTO #NAMEP from #NAMES n";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="INSERT #NAMEP select credential_name, n.fn+n.ln from #NAMES n";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="INSERT #NAMEP select credential_name, n.ln+n.initial from #NAMES n";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="INSERT #NAMEP select credential_name, n.ln+n.fn from #NAMES n";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="UPDATE #NAMEP set permutation=lower(permutation)";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner1("Processing SQL Server Credentials");

banner2("Setup Temp Table #SQL_SERVER_CREDS");
helptext("Create Temp Table #SQL_SERVER_CREDS as select * from INV_credential where type = sqlsvr_syslogin");
$query="select distinct c.credential_name,
	newcred=lower(substring(c.credential_name,1+charindex('\\',c.credential_name),1000)),
	c.credential_type, c.credential_id, c.system_name, c.system_type,
	is_group='?', isntname='?',
	domainname=lower(substring(c.credential_name,1, charindex('\\',c.credential_name)))
into   #SQL_SERVER_CREDS
from 	 INV_credential c
where  c.credential_type = 'sqlsvr_syslogin'";
show_query($query);
MlpAlarm::_querynoresults($query,1);

helptext("Update #SQL_SERVER_CREDS set is_group=attribute_value From isntgroup attribute");
$query="UPDATE #SQL_SERVER_CREDS
SET    is_group=a.attribute_value
FROM   #SQL_SERVER_CREDS c, INV_credential_attribute a
WHERE	 a.attribute_key='isntgroup'
and	 c.credential_name= a.credential_name
and	 c.credential_type= a.credential_type
and	 c.system_name 	= a.system_name
and 	 c.system_type 	= a.system_type";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

helptext("Update #SQL_SERVER_CREDS set is_ntname=attribute_value From isntname attribute");
$query="UPDATE #SQL_SERVER_CREDS
SET    isntname=a.attribute_value
FROM   #SQL_SERVER_CREDS c, INV_credential_attribute a
WHERE	 a.attribute_key='isntname'
and	 c.credential_name= a.credential_name
and	 c.credential_type= a.credential_type
and	 c.system_name 	= a.system_name
and 	 c.system_type 	= a.system_type";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

# PROCESS NORMAL NON-NATIVE-AUTH LOGINS
banner2("Processing Sql Logins");
helptext("Insert Principals (credential_subtype='ldap user?')");
helptext("   select INV_credential where sqlsvr_syslogin and is_group != '1' and isntname != '1' ");
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 credential_subtype, is_deleted, match_notes_short, match_notes_long,
	  	 principal_name, principal_type )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 'sql login', 'N', 'sql login', 'Normal Sql Login',
		 c.credential_name, c.credential_type
from 	 #SQL_SERVER_CREDS c
where  is_group != '1' and isntname != '1'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);
clear_mapped_sqlsvr_logins(__LINE__);

# PROCESS NATIVE-AUTH LOGINS WITH LDAP USER MATCH
helptext("INSERT INV_principal_map select #SQL_SERVER_CREDS where sqlsvr_syslogin and is_group != '1' and isntname != '1' and ldap name match ");
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 credential_subtype, is_deleted, match_notes_short, match_notes_long,
	  	 principal_name, principal_type  )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 'ldap user', c2.is_deleted, 'MATCHED', 'MATCHED LDAP USER',
		 c2.credential_name, c2.credential_type
from 	 #SQL_SERVER_CREDS c, INV_credential c2
where  is_group != '1' and isntname = '1'
and 	 c.domainname != lower(c.system_name)+'\'
and    c2.credential_type='ldap user'
and    c2.credential_type!='sqlsvr_syslogin'
and    lower(c.credential_name) = lower(c.domainname+c2.credential_name)";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);
clear_mapped_sqlsvr_logins(__LINE__);

helptext("INSERT INV_principal_map select #SQL_SERVER_CREDS where sqlsvr_syslogin and is_group != '1' and isntname != '1' and ldap name match ");
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 credential_subtype, is_deleted, match_notes_short, match_notes_long,
	  	 principal_name, principal_type  )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 'ldap user', c2.is_deleted, 'MATCHED', 'MATCHED LDAP USER',
		 c2.credential_name, c2.credential_type
from 	 #SQL_SERVER_CREDS c, INV_credential c2
where  is_group != '1' and isntname = '1'
and 	 c.domainname != lower(c.system_name)+'\'
and    c2.credential_type in ('win32 local')
and    c2.credential_type!='sqlsvr_syslogin'
and    lower(c.credential_name) = lower(c.domainname+c2.credential_name)";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);
clear_mapped_sqlsvr_logins(__LINE__);

helptext("Clobbering NT Authority Accounts");
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 credential_subtype, is_deleted, delete_reason, match_notes_short, match_notes_long,
	  	 principal_name, principal_type  )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 'builtin', 'Y', 'Bultin Login', 'MATCHED', 'MATCHED NT AUTHORITY',
		 c.credential_name, c.credential_type
from 	 #SQL_SERVER_CREDS c
where  credential_name like 'NT AUTHORITY\\%'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);
clear_mapped_sqlsvr_logins(__LINE__);

# THE ABOVE CAN BE NATIVE OR NON NATIVE LOGIN
#banner2("Updating non nt logins");
#helptext("update INV_principal_map set P=C where 'ldap user ?' and isntname=0 but names matches a ldap user");
#$query="update INV_principal_map
#set   	credential_subtype = 'sql login', principal_name=m.credential_name, principal_type=m.credential_type,
#			match_notes_short  = 'sql login', match_notes_long='Normal Sql Login'
#from 		INV_principal_map m, INV_credential_attribute c
#where 	m.credential_type='sqlsvr_syslogin'
#and	  	m.credential_name= c.credential_name
#and	  	m.credential_type= c.credential_type
#and	 	m.system_name    = c.system_name
#and   	m.system_type    = c.system_type
#and  	 	m.credential_subtype = 'ldap user ?'
#and	   c.attribute_key = 'isntname'
#and   	c.attribute_value='0' ";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

#banner2("Updating ldap logins for native accounts");
#helptext("update INV_principal_map set P=C if sqlsvr_syslogin & ldapuser? and lc(deprefexed name) matches a ldap user");
#$query="update INV_principal_map
#set   	credential_subtype = 'ldap user', principal_name=c.credential_name, principal_type=c.credential_type,
#			match_notes_short  = 'Ldap User', match_notes_long='Ldap User Match'
#from 		INV_principal_map m , INV_credential c
#where 	m.credential_subtype = 'ldap user ?'
#and	   m.credential_type='sqlsvr_syslogin'
#and   	lower(substring(m.credential_name,1+charindex('\\',m.credential_name),1000))=lower(c.credential_name)
#and   	c.credential_type = 'ldap user'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);
#
#banner2("Updating ldap logins for sql accounts");
#helptext("update INV_principal_map set P=C if lc() of names matches a ldap user");
#$query="update INV_principal_map
#set   	credential_subtype = 'sql login match', principal_name=c.credential_name, principal_type=c.credential_type,
#			match_notes_short  = 'Ldap User', match_notes_long='Ldap User Match (case)'
#from 		INV_principal_map m , INV_credential c
#where 	m.credential_subtype = 'sql login'
#and	   m.credential_type='sqlsvr_syslogin'
#and      m.principal_name is null
#and  	 	m.credential_subtype = 'ldap user ?'
#and	   c.credential_type = 'ldap user'
#and	   m.credential_name = c.credential_name";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

#banner2("Identifying Local Windows Logins");
#$query="update INV_principal_map
#set 		credential_subtype='win32 local',
#			principal_name=lower(substring(m.credential_name,1+charindex('\\',m.credential_name),1000)),
#			principal_type=c.credential_type,
#			match_notes_short  = 'local', match_notes_long='local win32 login account'
#from 		INV_principal_map m, INV_credential c, INV_system_attribute a
#where		credential_subtype = 'ldap user ?'
#and 	 	m.credential_type='sqlsvr_syslogin'
#and   	lower(m.credential_name) like lower(m.system_name+'\\%')
#and		lower(substring(m.credential_name,1+charindex('\\',m.credential_name),1000)) = lower(c.credential_name)
#-- and		m.system_name	= c.system_name
#and	   m.system_name   = a.system_name
#and      a.attribute_name like '%HOSTNAME' and      a.system_type='sqlsvr'
#and		c.system_name = a.attribute_value";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

#banner2("Clobbering NT Authority Accounts");
#$query="update INV_principal_map
#set   	credential_subtype = 'builtin', principal_name=credential_name, principal_type=credential_type,
#			match_notes_short  = 'ntauthority', match_notes_long='ntauthority accounts for the system',
#			is_deleted='Y', delete_reason='Bultin Login'
#where 	credential_type='sqlsvr_syslogin'
#and		credential_name like 'NT AUTHORITY\\%'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

#banner2("Annotating Missing LDAP Logins");
#$query="update 	INV_principal_map
#set    	principal_name='(No LDap Info)', principal_type='ldap user', is_error='Y',
#			match_notes_long='ERROR: LDAP login but no credential found'
#from  	INV_principal_map m, INV_credential_attribute c
#where 	m.credential_type	='sqlsvr_syslogin'
#and	  	m.credential_name = c.credential_name
#and	  	m.credential_type = c.credential_type
#and	 	m.system_name     = c.system_name
#and   	m.system_type     = c.system_type
#and  	 	m.credential_subtype = 'ldap user ?'
#and		c.attribute_key   = 'isntuser'
#and      c.attribute_value = '1'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

#banner2("Clobbering Crosscheck Data That Has Matched");
#diagnose(__LINE__);

#banner2("DIAGNOSTICS - UNMATCHED USERS");
#$query= "SELECT * FROM #SQL_SERVER_CREDS where is_group='0'";
#my(@rc)=MlpAlarm::_querywithresults($query,1);
#if( $#rc >= 0 ) {
#	print "ERROR - A SQL CREDENTIAL WAS MISFORMED\n$query\n";
#	print $#rc, "ROWS RETURNED\n";
#	foreach (@rc) { print join("|",dbi_decode_row($_)),"\n";	}
#	die "DONE";
#}

status_done("COMPLETED LOGINS STEP");

#
# INV_CREDS should only be groups at this point
#
banner1("Processing GROUPS");
banner2("Inserting Exact Match LDAP Groups");
##c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
#$query="
#select distinct  g.credential_type
#from 	 INV_group_member g,  #SQL_SERVER_CREDS c
#where  c.newcred   = lower(g.group_name)
#and    g.group_type='ldap group'
#";
#show_query($query);
#my(@rc)=MlpAlarm::_querywithresults($query,1);
#if( $#rc >= 0 ) {
#	print $#rc, "ROWS RETURNED\n";
#	foreach (@rc) {
#		print join("\t",dbi_decode_row($_)),"\n";
#	}
#	die "DONE";
#}
#g.credential_type
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 principal_name, principal_type, is_group,
	  	 credential_subtype, is_deleted, match_notes_short, match_notes_long )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 g.credential_name, g.credential_type, 'Y',
       'ldap group', 'N', 'MATCHED', 'Matched LDAP Group'
from 	 INV_group_member g,  #SQL_SERVER_CREDS c
where  c.newcred   = lower(g.group_name)
and    g.group_type='ldap group'
and    c.is_group='1'
";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);
clear_mapped_sqlsvr_logins(__LINE__);

banner2("Match SQL Server Logins That Use Local Windows Groups");
# The next query matches sql server logins that are mapped to win32 local groups
diagnose(__LINE__);
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 principal_name, principal_type, is_group,
	  	 credential_subtype, is_deleted, match_notes_short, match_notes_long )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 g.credential_name, g.credential_type, 'Y',
       'win32 group', 'N', 'MATCHED', 'Matched LOCAL WIN32 Group'
from 	 INV_group_member g,  #SQL_SERVER_CREDS c
where  lower(c.credential_name)  = lower( g.system_name+'\\'+g.group_name )
and    g.group_type='win32 local group'
and    c.is_group='1'
";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);
clear_mapped_sqlsvr_logins(__LINE__);
#clear_mapped_sqlsvr_logins(__LINE__);		# 140 rows

# WHAT ABOUT SYSTEMS THAT ARE ALIASED - SAY ADXXX/SqlServer on system YYY
banner2("Inserting Aliased Local Windows Groups");
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 principal_name, principal_type, is_group,
	  	 credential_subtype, is_deleted, match_notes_short, match_notes_long )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 g.credential_name, g.credential_type, 'Y',
       'win32 group', 'N', 'MATCHED', 'Matched LOCAL WIN32 Group'
from 	 INV_group_member g,  #SQL_SERVER_CREDS c, INV_system_attribute a
where  a.attribute_name like '%HOSTNAME' and a.system_type='sqlsvr'
and  	 a.system_name = c.system_name
and    g.group_type='win32 local group'
and	 c.system_name = g.system_name
and    lower(c.credential_name) like lower( a.attribute_value+'\\'+g.group_name )";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);
clear_mapped_sqlsvr_logins(__LINE__);

## Update NT AUTHORITY ACCOUNTS!
#$query="UPDATE INV_principal_map
#set    is_deleted='Y', delete_reason='System Account'
#from   INV_principal_map
#where	 principal_name = 'SYSTEM'
#and 	 principal_type = 'win32 group'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

banner2("Deleting SQL Server Builtin Logins");
$query="
update 	INV_principal_map
set    	credential_subtype = 'builtin', principal_name='builtin', principal_type=credential_type,
			match_notes_short='builtin',match_notes_long='Builtin SQL Server Login',
			is_deleted='Y', delete_reason='Builtin Login',display_name='Builtin SQL Login'
where  credential_type='sqlsvr_syslogin'
and    ( credential_name = '##MS_AgentSigningCertificate##' or  credential_name like '%\\SQLServer2005%' )";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

# for some reason... the above ends up with type=sqlsvr and principal_name=xxx so make it ldap if that matches
$query="UPDATE INV_principal_map
set    principal_type='ldap user'
from   INV_principal_map m, INV_credential a
where	 m.principal_name = a.credential_name
and	 m.principal_type != a.credential_type
and 	 a.credential_type = 'ldap user'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Inserting BUILTIN Windows Groups");
$query="Insert INV_principal_map
	  ( credential_name, credential_type, credential_id, system_name, system_type,
	  	 principal_name, principal_type,is_group,
	  	 credential_subtype, is_deleted, match_notes_short, match_notes_long )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
		 g.credential_name, g.credential_type, 'Y',
       'builtin group', 'N', 'MATCHED', 'Matched BUILTIN Group'
from 	 INV_group_member g,  #SQL_SERVER_CREDS c
where  lower(c.credential_name)  = lower( 'BUILTIN\\'+g.group_name )
and    c.system_name=g.system_name
and    g.group_type='win32 local group'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Inserting Empty LDAP Groups");
#$query="UPDATE #SQL_SERVER_CREDS
#set 	 is_group='E'
#from   #SQL_SERVER_CREDS c, INV_credential_attribute m
#where  c.credential_name= m.credential_name
#and	 c.credential_type= m.credential_type
#and	 c.credential_id  = m.credential_id
#and	 c.system_name 	= m.system_name
#and 	 c.system_type 	= m.system_type
#and	 is_group='1'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#
#$query="UPDATE #SQL_SERVER_CREDS
#set 	 c.is_group='M'
#from   #SQL_SERVER_CREDS c, INV_group_member m
#where  c.credential_name= m.group_name
#and	 c.credential_type= m.group_type
#and	 c.credential_id  = m.group_id
#and	 c.system_name 	= m.system_name
#and 	 c.system_type 	= m.system_type
#and	 is_group='E'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

# THE 'E' rows have no members but have appropriate credentials
clear_mapped_sqlsvr_logins(__LINE__);
$query="Insert INV_principal_map
	( credential_name, credential_type, credential_id, system_name, system_type,
	principal_name, principal_type, display_name, is_group,
	credential_subtype, is_deleted, match_notes_short, match_notes_long )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
	'(no members)','(no members)', credential_name+' (Empty Group)', 'Y',
	'ldap group', 'Y', 'MATCHED', 'Empty Group'
from 	 #SQL_SERVER_CREDS c
where  is_group = '1'";
show_query($query);
MlpAlarm::_querynoresults($query,1);

clear_mapped_sqlsvr_logins(__LINE__);
$query="Insert INV_principal_map
	( credential_name, credential_type, credential_id, system_name, system_type,
	principal_name, principal_type, is_group, is_deleted, delete_reason,
	credential_subtype, match_notes_short, match_notes_long )
select distinct c.credential_name, c.credential_type, c.credential_id, c.system_name, c.system_type,
	c.credential_name, c.credential_type, 'Y', 'Y', 'No Ldap Match',
	'no match', 'NOMATCHED', 'No Match'
from 	 #SQL_SERVER_CREDS c";
show_query($query);
MlpAlarm::_querynoresults($query,1);

delete_and_count_remaining(__LINE__);		# also 140 rows -> above had no impact?
diagnose(__LINE__);

#
#$query= "SELECT * FROM #SQL_SERVER_CREDS where is_group='?'";
#my(@rc)=MlpAlarm::_querywithresults($query,1);
#if( $#rc >= 0 ) {
#	print "ERROR - A SQL CREDENTIAL WAS MISFORMED\n$query\n";
#	print $#rc, "ROWS RETURNED\n";
#	foreach (@rc) {print join("|",dbi_decode_row($_)),"\n";}
#	die "DONE";
#}

sub  clear_mapped_sqlsvr_logins {
	my($line)=@_;
	my($query)="DELETE #SQL_SERVER_CREDS
	FROM    #SQL_SERVER_CREDS c, INV_principal_map m
	WHERE	 c.credential_name= m.credential_name
	and	 c.credential_type= m.credential_type
	and	 c.credential_id  = m.credential_id
	and	 c.system_name 	= m.system_name
	and 	 c.system_type 	= m.system_type";
	show_query($query);
	MlpAlarm::_querynoresults($query,1);

	my($query)= "SELECT count(*) FROM #SQL_SERVER_CREDS";
	my(@rc)=MlpAlarm::_querywithresults($query,1);
	foreach (@rc) {
		print "\t",join("\t",dbi_decode_row($_))," Rows Returned At Line $line\n";
	}
}

sub  delete_and_count_remaining {
	my($line)=@_;
	clear_mapped_sqlsvr_logins($line);
	my($query)= "SELECT credential_name,is_group,isntname,system_name FROM #SQL_SERVER_CREDS";
	my(@rc)=MlpAlarm::_querywithresults($query,1);
	if( $#rc >= 0 ) {
		#print "\t",$#rc, " Rows Returned At Line$line\n";
		foreach (@rc) {
			print join("\t",dbi_decode_row($_)),"\n";
		}
		print "\t",$#rc, " Rows Returned At Line $line\n";
		die "DONE at line $line\n";
	}
}

banner2("DIAGNOSTICS - MISFORMED GROUP INFO");
delete_and_count_remaining(__LINE__);
$query = "Drop table #SQL_SERVER_CREDS";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner1("Processing Windows Group Access To The Box");
#$query="Insert INV_principal_map
#	 ( 	credential_name, credential_type, credential_id, system_name, system_type, principal_name, principal_type )
#select   group_name, group_type, group_id, system_name, system_type, credential_name, credential_type
#from INV_group_member where group_type = 'win32 local group'";


# HEAVY DUTY QUERY TO DO LOCAL GROUPS

$query="
-- PROCESS TO COPY THE win32 local groups INTO THE PRINCIPAL MAP

-- Get the Local Group Information
-- one problem here is that we dont trust the credential_type in group_member
-- another is that we do not have the DOMAIN for the local groups...
-- Users removed by policy
select 	ln=lower(credential_name),type=credential_type,* into #INV_group_member
from 	INV_group_member  where group_type = 'win32 local group'
and 	credential_name != '' and group_name != 'Users'
-- and 	lower(credential_name)='sqlaccount'

update #INV_group_member set type=''

update 	#INV_group_member set type=c.credential_type
from 	#INV_group_member m, INV_credential c
where 	(	m.credential_name = c.credential_name or ln=lower(c.credential_name) )
and	  	m.system_name = c.system_name
and	  	m.system_type = c.system_type
and   	c.credential_type = 'win32 local'

update #INV_group_member set type=c.credential_type
from #INV_group_member m, INV_credential c
where (m.credential_name = c.credential_name or ln=lower(c.credential_name))
and	  type = ''
and   c.credential_type in ('ldap group','ldap user')

update #INV_group_member set type='BUILTIN' where type=''

-- select * from #INV_group_member
-- select * from INV_credential where credential_type='win32 local'

-- REPLACE THEM ALL
delete INV_principal_map where credential_type='win32 local group'

-- now update the principal map - Users
insert INV_principal_map
	 ( 	credential_name, credential_type, credential_id, system_name, system_type, principal_name, principal_type )
select   distinct group_name, group_type, group_id, system_name, system_type, credential_name, type
from #INV_group_member where type in ( 'ldap user','win32 local','BUILTIN')
union
-- now update it for the groups
--insert INV_principal_map
	-- ( 	credential_name, credential_type, credential_id, system_name, system_type, principal_name, principal_type )
select m.group_name, m.group_type, m.group_id, m.system_name, m.system_type, g.credential_name, g.credential_type
from #INV_group_member  m, INV_group_member g
where 	type = 'ldap group'
and 	g.group_type='ldap group' and g.group_name = m.credential_name

update INV_principal_map set is_sysadmin='Y' where credential_type='win32 local group' and credential_name='Administrators'
update INV_principal_map set is_deleted='Y', delete_reason='System Login' where credential_type='win32 local group' and principal_type='BUILTIN'

drop table #INV_group_member
";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#$query="select top 20 credential_name, credential_type, principal_name, principal_type
#from 	 INV_principal_map
#where  credential_type='win32 local group'";

#my(@rc)=MlpAlarm::_querywithresults($query,1);
#if( $#rc >= 0 ) {
#	print $#rc, "ROWS RETURNED\n";
#	foreach (@rc) {
#		print join("\t",dbi_decode_row($_)),"\n";
#	}
#	die "DONE";
#}

banner1("Processing Credentials Tagged /etc/passwd");
$query="Insert INV_principal_map
	 ( 	credential_name, credential_type, credential_id, system_name, system_type )
select   credential_name, credential_type, credential_id, system_name, system_type
from INV_credential where credential_type = '/etc/passwd'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Delete UNIX logins that dont end in *sh");
$query="
update   INV_principal_map
set      principal_name=m.credential_name, principal_type='/etc/passwd',
         is_deleted='Y', delete_reason='No Default Shell',
			match_notes_long='ignored - no shell', match_notes_short='no shell'
from     INV_principal_map m, INV_credential_attribute a
where    m.credential_name = a.credential_name
and      m.credential_type = a.credential_type
and      m.credential_id   = a.credential_id
and      m.system_name     = a.system_name
and      m.system_type     = a.system_type
and      a.credential_type='/etc/passwd'
and      m.credential_name not like '\+%'
and      a.attribute_key='dflt shell'
and      a.attribute_value not like '%sh' ";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Updating uid<100 unix logins as system logins");
$query="Update INV_principal_map
set  	principal_name=credential_name, principal_type='/etc/passwd', match_notes_short='uid<100',
		match_notes_long='unix uid<100 is a system login',
		credential_subtype = 'builtin',is_deleted='Y',delete_reason='Unix uid < 100'
where credential_type = '/etc/passwd' and credential_id is not null and credential_id!=''
and 	credential_id!='N.A.' and convert(float,credential_id)<100
and   credential_name != 'root'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Updating sybase & oracle system logins");
$query="Update INV_principal_map
set  	principal_name=credential_name, principal_type='/etc/passwd', match_notes_short='db login',
		match_notes_long='database login'
where credential_type = '/etc/passwd'
and 	credential_name in ('sybase','oracle','mysql','gemalarms')";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner1("Processing Credentials Tagged sybase_login");
$query="Insert INV_principal_map
	 ( 	credential_name, credential_type, credential_id, system_name, system_type )
select   credential_name, credential_type, credential_id, system_name, system_type
from INV_credential where credential_type = 'sybase_login'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Delete SYBASE Logins that are locked/expired");
$query="
update 	INV_principal_map
set    	is_deleted='Y', delete_reason='Locked Login',
			match_notes_short='Locked', match_notes_long='Locked Sybase Login'
from   	INV_principal_map m, INV_credential_attribute a
where  	m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and		m.credential_id   = a.credential_id
and  		m.system_name     = a.system_name
and  		m.system_type     = a.system_type
and		a.credential_type='sybase_login'
and 		( a.attribute_key='expired' or a.attribute_key='locked' )
and     	a.attribute_value = 'Y'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Updating sybase system logins");
$query="Update INV_principal_map
set  	principal_name=credential_name, principal_type=credential_type, match_notes_short='db login',
		match_notes_long='database login'
where credential_type = 'sybase_login' and credential_name in ('sa','gemalarms')";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

$query="Update INV_principal_map
set  	principal_name=credential_name, principal_type='sybase_login', match_notes_short='db login',
		match_notes_long='database login', credential_subtype = 'builtin', delete_reason='Probe is uneusable',
		is_deleted='Y', principal_notes=credential_name+' is a builtin login'
where credential_type = 'sybase_login' and credential_name in ('probe')";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner1("Processing Credentials Tagged win32 local");
$query="Insert INV_principal_map
	 ( 	credential_name, credential_type, credential_id, system_name, system_type, is_deleted )
select   credential_name, credential_type, credential_id, system_name, system_type, 'N'
from INV_credential where credential_type = 'win32 local'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#
#banner2("Disable Win32 Local Disabled Principals");
#$query="UPDATE INV_principal_map
#set 		is_deleted='Y',  delete_reason='disabled'
#from   	INV_principal_map m, INV_credential_attribute a
#where    m.credential_name = a.credential_name
#and      m.credential_type = a.credential_type
#and      m.credential_id   = a.credential_id
#and      m.system_name     = a.system_name
#and      m.system_type     = a.system_type
#and		m.credential_type='win32 local'
#and      a.attribute_key='Disabled'
#and	   a.attribute_value='1'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

#
# SAFE??? ALL LDAP LOGINS IN?
#

banner2("Disable Where Underlying Is Deleted");
$query="UPDATE INV_principal_map
set 		is_deleted='Y',  delete_reason='disabled'
from   	INV_principal_map m, INV_credential a
where    m.credential_name = a.credential_name
and      m.credential_type = a.credential_type
--and      m.credential_id   = a.credential_id
and      isnull(m.system_name,'x')     = isnull(a.system_name,'x')
and      isnull(m.system_type,'x')     = isnull(a.system_type,'x')
and		a.is_deleted='Y'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Marking Win32 Local Builtin Principals");
# first query are the disabled logins
$query="UPDATE INV_principal_map
set  		principal_name='builtin', principal_type='win32 local', match_notes_short='builtin login',
			match_notes_long='builtin win32 login',credential_subtype = 'builtin',
			is_deleted='Y', delete_reason='Builtin Login'
from   	INV_principal_map
where	 	credential_type='win32 local'
and    ( credential_name in ('SQLDebugger','ASPNET')
		or credential_name like 'IUSR_%'
		or credential_name like 'IWAM_%' )";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="UPDATE INV_principal_map
set  		is_deleted='Y', delete_reason='Builtin Login'
from   	INV_principal_map
where	 	principal_type='win32 local'
and    ( principal_name in ('SQLDebugger','ASPNET','Autenticated Users','INTERACTIVE')
		or principal_name like 'IUSR_%'
		or principal_name like 'IWAM_%' )";
show_query($query);
MlpAlarm::_querynoresults($query,1);

# aliased version of the above
# REMOVED BECAUSE I REMOVED SYSNAME IN THE ABOVE MATCH...
#$query="UPDATE INV_principal_map
#set  		principal_name='builtin', principal_type='win32 local', match_notes_short='builtin login',
#			match_notes_long='builtin win32 login',credential_subtype = 'builtin',
#			is_deleted='Y', delete_reason='Builtin Login'
#from   INV_principal_map m, INV_aliases a
#where  credential_type='win32 local'
#and    principal_name is null
#and    m.system_name=a.system_name
#and    ( credential_name in ('SQLDebugger','ASPNET')
#		or credential_name like 'IUSR_'+a.alias_name
#		or credential_name like 'IWAM_'+a.alias_name )";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

# 2nd query are the possibly used logins
$query="UPDATE INV_principal_map
set  		principal_name=credential_name, principal_type='win32 local', match_notes_short='builtin login',
			match_notes_long='builtin win32 login',credential_subtype = 'builtin',
			is_deleted='Y', delete_reason='System Account'
from   	INV_principal_map
where		credential_type='win32 local'
and      credential_name in ('Guest','SUPPORT_388945a0') ";
show_query($query);
MlpAlarm::_querynoresults($query,1);

#banner1("Processing Credentials Tagged ldap user");
#$query="Insert INV_principal_map
#	 ( 	credential_name, credential_type, credential_id, system_name, system_type )
#select   credential_name, credential_type, credential_id, system_name, system_type
#from INV_credential where credential_type = 'ldap user'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

#banner1("Processing Credentials Tagged ypcat passwd");
#$query="Insert INV_principal_map
#	 ( 	credential_name, credential_type, credential_id, system_name, system_type )
#select   credential_name, credential_type, credential_id, system_name, system_type
#from INV_credential where credential_type = 'ypcat passwd'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

banner1("Processing IMPORTED Credentials");
$query="
-- handle principals that are IMPORTED
select distinct m.*
into #INV_principal_map
from INV_group_member_import g, INV_principal_map m
where lower(m.credential_name) = lower(g.group_name)
and 	m.credential_type = g.group_type

-- delete the original credentials... we need to reinsert
delete INV_principal_map
from   INV_principal_map p, #INV_principal_map x
where  p.credential_name = x.credential_name
and    p.credential_type = x.credential_type

insert INV_principal_map
select m.credential_name,	m.credential_type   ,m.credential_id       ,m.system_name,	m.system_type,
	g.credential_name      ,'app user'      ,m.credential_subtype
	,m.is_deleted          ,m.delete_reason       ,m.match_notes_short   ,m.match_notes_long
	,m.credential_notes    ,m.principal_notes     ,m.system_notes ,
	m.is_operrole, m.is_sysadmin, m.is_securityadmin, m.is_readonly, m.is_group,
	m.display_name,
	m.is_unuseable, m.is_serviceaccount, m.is_operator, m.is_ldap_group, m.is_ldap_user, m.is_shared, m.is_error, m.user_notes
from 	#INV_principal_map m, INV_group_member_import g
where lower(m.credential_name)=lower(g.group_name) and m.credential_type=g.group_type

drop table #INV_principal_map
";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("App User IS an Exact Match of LDAP USER");
$query="
update 	INV_principal_map
set   	principal_name=c.credential_name, principal_type='ldap user',
         match_notes_short  = 'App User Match',  match_notes_long='Application Extract Matches LDAP'
from 		INV_principal_map m, INV_credential c
where 	m.principal_type = 'app user'
and      c.credential_type= 'ldap user'
and 	   lower(m.principal_name) = lower(c.credential_name)";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("App User IS an First 6 Character Match of LDAP USER");
$query="
update 	INV_principal_map
set   	principal_name=c.credential_name, principal_type='ldap user',
         match_notes_short  = 'App User Match',  match_notes_long='Application Extract Matches LDAP'
from 		INV_principal_map m, INV_credential c
where 	m.principal_type = 'app user'
and      c.credential_type= 'ldap user'
and    lower(c.credential_name) like lower(m.principal_name+'%')
and char_length(m.principal_name)>7";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("App User IS a Heuristic Match of Permuted Names");
$query="
update 	INV_principal_map
set   	principal_name=n.credential_name, principal_type='ldap user',
			match_notes_short  = 'App User Match',  match_notes_long='Application Extract Matches Permuted Name'
from 		INV_principal_map m, #NAMEP n
where 	m.principal_type = 'app user'
and 	   lower(m.principal_name) = n.permutation";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#banner1("Processing GLOBALSHARED Credentials");
#helptext("process globalshared credentials
#any lc(credentials) that match values in GLOBALSHARED groups get expanded");
#$query="
#-- handle principals that are GLOBALSHARED
#
#-- save map entries
#select distinct m.* , ctype=g.credential_type, cgroup=g.credential_name
#into #INV_principal_map
#from INV_group_member g, INV_principal_map m
#where g.group_type='GLOBALSHARED'
#and lower(m.credential_name) = lower(g.group_name)
#and m.system_type = g.system_type
#
#-- delete the original credentials... we need to reinsert
#delete INV_principal_map
#from   INV_principal_map p, #INV_principal_map x
#where p.credential_name = x.credential_name
#and   p.credential_type = x.credential_type
#and   isnull(p.system_name,'x') = isnull(x.system_name,'x')
#and   p.system_type = x.system_type
#and   isnull(p.principal_name,'x') = isnull(x.principal_name,'x')
#and   isnull(p.principal_type,'x') = isnull(x.principal_type,'x')
#
#-- rows in tmp table now
#insert INV_principal_map
#select m.credential_name,m.credential_type   ,m.credential_id       ,m.system_name
#,m.system_type         ,m.cgroup      ,m.ctype      ,m.credential_subtype
#,m.is_deleted          ,m.delete_reason       ,m.match_notes_short   ,m.match_notes_long
#,m.credential_notes    ,m.principal_notes     ,m.system_notes        ,m.is_operator
#,m.is_sysadmin         ,m.is_securityadmin    ,m.is_readonly         ,m.is_group
#,m.display_name        ,m.is_error            ,m.error_notes
#from #INV_principal_map m
#where  m.ctype='ldap user'
#
#union
#select
#m.credential_name     ,m.credential_type     ,m.credential_id       ,m.system_name
#,m.system_type         ,principal_name=g.credential_name      ,principal_type=g.credential_type      ,m.credential_subtype
#,m.is_deleted          ,m.delete_reason       ,m.match_notes_short   ,m.match_notes_long
#,m.credential_notes    ,m.principal_notes     ,m.system_notes        ,m.is_operator
#,m.is_sysadmin         ,m.is_securityadmin    ,m.is_readonly         ,m.is_group
#,m.display_name        ,m.is_error            ,m.error_notes
#from #INV_principal_map m, INV_group_member g
#where m.ctype='ldap group'
#and   m.ctype = g.group_type
#and   m.cgroup = g.group_name
#
#drop table #INV_principal_map";

banner2("Update principals from notes");
$query="update INV_principal_map
set   is_unuseable 		= c.is_unuseable,
		is_serviceaccount = c.is_serviceaccount,
	   is_error    		= c.is_error,
	   is_operator 		= c.is_operator,
	   is_ldap_user		= c.is_ldap_user,
	   is_ldap_group		= c.is_ldap_group,
	   is_shared			= c.is_shared,
	   user_notes 			= c.user_notes
from INV_principal_map m, INV_credential_notes c
where m.credential_name = c.credential_name
and   m.credential_type = c.credential_type";
show_query($query);
MlpAlarm::_querynoresults($query,1);

# removed principal notes and display name fields...

banner2("Update principals from notes - those mapped ldap users");
$query="
update INV_principal_map
set   is_ldap_user	=  c.is_ldap_user,
	   is_ldap_group	=  c.is_ldap_group,
	   is_shared		=  c.is_shared
from INV_principal_map m, INV_credential_notes c
where m.credential_name = c.credential_name
and   m.credential_type = c.credential_type

select m.*
into #INV_principal_map_userdup
from INV_principal_map m , INV_credential_notes  n
where n.is_ldap_user='Y'
and   m.credential_name = n.credential_name
and   m.credential_type = n.credential_type
and   ldap_user is not null

-- delete the original credentials... we need to reinsert
-- only way to handle duplicates
delete INV_principal_map
from   INV_principal_map p, #INV_principal_map_userdup x
where p.credential_name = x.credential_name
and   p.credential_type = x.credential_type

update #INV_principal_map_userdup set principal_name=ldap_user,  principal_type='ldap user',is_ldap_user='Y'
from  INV_principal_map m, INV_credential_notes  n
where n.ldap_user is not null and n.is_ldap_user='Y'
and   m.credential_name = n.credential_name
and   m.credential_type = n.credential_type

insert INV_principal_map
select distinct * from #INV_principal_map_userdup
";

show_query($query);
MlpAlarm::_querynoresults($query,1);

banner2("Update principals from notes - those mapped ldap groups");
$query="
select *
into #INV_principal_map2
from INV_principal_map m
where is_ldap_group='Y'

-- delete the original credentials... we need to reinsert
delete INV_principal_map
from   INV_principal_map p, #INV_principal_map2 x
where p.credential_name = x.credential_name
and   p.credential_type = x.credential_type

insert INV_principal_map
select distinct
	m.credential_name     ,m.credential_type     ,m.credential_id       ,m.system_name         ,
	m.system_type         ,principal_name=g.credential_name      ,principal_type=g.credential_type      ,
	m.credential_subtype  ,
	is_deleted  = case when n.is_unuseable = 'Y' then 'Y' 	else  m.is_deleted  end,
	m.delete_reason       ,m.match_notes_short   ,m.match_notes_long    ,
	m.credential_notes    ,
	principal_notes='',
	m.system_notes        ,
	m.is_operrole, 	m.is_sysadmin         ,m.is_securityadmin    ,m.is_readonly         ,m.is_group       ,
	display_name='',
	n.is_unuseable, n.is_serviceaccount, n.is_operator, n.is_ldap_group, n.is_ldap_user , n.is_shared, n.is_error,
	n.user_notes
from #INV_principal_map2 m, INV_group_member g, INV_credential_notes n
where g.group_type='ldap group'
and   g.group_name=n.ldap_group
and   m.is_ldap_group='Y' and n.is_ldap_group='Y' and n.ldap_group is not null
and   n.credential_name = m.credential_name
and   n.credential_type = m.credential_type

drop table #INV_principal_map2";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#my($query)="select distinct credential_name,principal_name
#from #INV_principal_map2 where credential_name like 'sa' ";
#my(@rc)=MlpAlarm::_querywithresults($query,1);
#foreach (@rc) { print __LINE__,' ',join("\t",dbi_decode_row($_)),"\n";	}
#print "EXIT";
#exit(0);

banner1("Credential Matches");
banner2("Exact Match of Unmapped Credentials to LDAP Logins");
$query="
update INV_principal_map
set   credential_subtype = 'ldap user', principal_name=c.credential_name, principal_type=c.credential_type,
		match_notes_short  = 'Ldap User',  match_notes_long='Ldap User Match'
from INV_principal_map m, INV_credential c
where 	m.principal_name is null
and    	lower(m.credential_name)=lower(c.credential_name)
and		c.credential_type = 'ldap user' and c.display_name is not null";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("REMAP OF credential_notes to principals instead of to credentials");
banner2("Update principals from notes - principals mapped ldap users");
$query="
update INV_principal_map
set   is_ldap_user	=  c.is_ldap_user,
	   is_ldap_group	=  c.is_ldap_group,
	   is_shared		=  c.is_shared
from INV_principal_map m, INV_credential_notes c
-- changed 3/15/10 by ed
where ( m.credential_name = c.credential_name and   m.credential_type = c.credential_type )
or    ( m.principal_name = c.credential_name  and   m.principal_type = c.credential_type )
";
#where m.credential_name = c.credential_name
#and   m.credential_type = c.credential_type

show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="
select m.*
into #INV_principal_map_userdup2
from INV_principal_map m , INV_credential_notes  n
where n.is_ldap_user='Y'
and   lower(m.principal_name) = lower(n.credential_name)
and   m.principal_type = n.credential_type
and   n.ldap_user is not null

-- delete the original credentials... we need to reinsert
-- only way to handle duplicates
delete INV_principal_map
from   INV_principal_map p, #INV_principal_map_userdup2 x
where lower(p.principal_name) = lower(x.credential_name)
and   p.principal_type = x.credential_type

update #INV_principal_map_userdup2
set principal_name=ldap_user,
	principal_type='ldap user',
	is_ldap_user='Y'
from  #INV_principal_map_userdup2 m, INV_credential_notes  n
where n.ldap_user is not null and n.is_ldap_user='Y'
and   lower(m.principal_name) = lower(n.credential_name)
and   m.principal_type = n.credential_type
and   ldap_user is not null

insert INV_principal_map
select distinct * from #INV_principal_map_userdup2

drop table #INV_principal_map_userdup2
";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner2("Update principals from notes - principals mapped ldap groups");
$query = "
-- principal matches to groups
select  distinct new_principal_name=g.credential_name, new_principal_type=g.credential_type, m.*
into 	#W2
from 	INV_principal_map m , INV_credential_notes  n, INV_group_member g
where n.is_ldap_group='Y'
and   lower(m.principal_name) = lower(n.credential_name)
and   m.principal_type = n.credential_type
and   n.ldap_group is not null
and   g.group_type='ldap group'
and   g.group_name=n.ldap_group
and   n.is_ldap_group='Y'

-- delete the original credentials
delete INV_principal_map
from  #W2 x, INV_principal_map p
where p.credential_type = x.credential_type
and   p.credential_name = x.credential_name
and   p.principal_name = x.principal_name
and   p.principal_type = x.principal_type

-- delete NEW credentials that allready exist
delete #W2
from   #W2 x, INV_principal_map p
where   p.credential_type = x.credential_type
and   p.credential_name = x.credential_name
and   p.principal_name = x.new_principal_name
and   p.principal_type = x.new_principal_type

Insert INV_principal_map
select distinct
	m.credential_name     ,m.credential_type     ,m.credential_id       ,m.system_name         ,
	m.system_type         ,principal_name=new_principal_name      ,principal_type=new_principal_type      ,
	m.credential_subtype  ,
	m.is_deleted ,
	m.delete_reason       ,m.match_notes_short   ,m.match_notes_long    ,
	m.credential_notes    ,
	principal_notes='',
	m.system_notes        ,
	m.is_operrole, m.is_sysadmin, m.is_securityadmin, m.is_readonly, m.is_group,
	display_name='',
	m.is_unuseable, m.is_serviceaccount, m.is_operator, m.is_ldap_group, m.is_ldap_user, m.is_shared, m.is_error, m.user_notes

from #W2 m
where new_principal_name != principal_name
and   new_principal_type != principal_type

-- the last inqequality required or you reinsert the data you just dropped!

drop table #W2";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#banner2("Exact Match of Unmapped To Unfetched 'ldap user's");
#$query="
#update INV_principal_map
#set   credential_subtype = 'ldap user', principal_name=c.credential_name, principal_type=c.credential_type,
#		match_notes_short  = 'Ldap User',  match_notes_long='Ldap User Match'
#from INV_principal_map m, INV_credential c
#where 	m.principal_name is null
#and    	lower(m.credential_name)=lower(c.credential_name)
#and		c.credential_type = 'ldap user'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

banner2("Fetched Ldap Users Underscores Included");
$query="
update 	INV_principal_map
set   	credential_subtype = 'ldap user', principal_name=c.credential_name, principal_type=c.credential_type,
			match_notes_short  = '% Ldap User',  match_notes_long='Wildcard Ldap User Match'
from 		INV_principal_map m, INV_credential c
where 	m.principal_name is null
and    (	lower(m.credential_name) like '%[~_@]'+lower(c.credential_name)  escape '~'
	or 	lower(m.credential_name) like lower(c.credential_name)+'[~_@]%'  escape '~' )
and     m.credential_name not like '+%'
and		c.credential_type = 'ldap user'  and c.display_name is not null";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#banner2("Unfetched Ldap Users Underscores Included");
#$query="
#update 	INV_principal_map
#set   	credential_subtype = 'ldap user', principal_name=c.credential_name, principal_type=c.credential_type,
#			match_notes_short  = '% Ldap User',  match_notes_long='Wildcard Ldap User Match'
#from 		INV_principal_map m, INV_credential c
#where 	m.principal_name is null
#and    (	lower(m.credential_name) like '%[~_@]'+lower(c.credential_name)  escape '~'
#	or 	lower(m.credential_name) like lower(c.credential_name)+'[~_@]%'  escape '~' )
#and     m.credential_name not like '+%'
#and		c.credential_type = 'ldap user'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#diagnose(__LINE__);

#$query="DELETE INV_credential where credential_type='ldap TEMP'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#
#$query="DELETE INV_credential_attribute where credential_type='ldap TEMP'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#
#$query="select distinct credential_name from INV_principal_map where principal_type = 'ldap user'";
#my(@rc)=MlpAlarm::_querywithresults($query,1);
#foreach (@rc) {
##	print join("|",dbi_decode_row($_)),"\n";
#	my($n)=dbi_decode_row($_);
#	$n=basename($n);
#	print "==> PROCESSING USER $n\n";
#	if( is_cached_user_samid($n) )  { print "skipped - allready loaded\n";	next;	}
#	# if( $cache_ldap_user_by_samid{lc($n)} ) { print "skipped - allready loaded\n";	next;	}
#	my($DN)=get_user_dn_from_samid($n);
#	if( $DN eq "" ) {	print "skipped - no dn returned\n\n";	next;	}
#	my($DATA)=get_user_info_by_dn_and_save($DN,$n);
#	if( $DEBUG and defined $DATA ) {	foreach (keys %$DATA ) { print " -- $_  :: $$DATA{$_} \n"; }}
#}
#
#$query="DELETE INV_credential where credential_type='ldap TEMP'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#
#$query="DELETE INV_credential_attribute where credential_type='ldap user'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#
#$query="UPDATE INV_credential_attribute set credential_type='ldap user' where credential_type='ldap TEMP'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

banner2("Heuristic Match of Permuted Names");
$query="
update 	INV_principal_map
set   	principal_name=c.credential_name, principal_type=c.credential_type,
			match_notes_short  = 'Ldap User',  match_notes_long='Ldap User Match'
from 		INV_principal_map m, INV_credential c, #NAMEP n
where 	m.principal_name is null
and		c.credential_type = 'ldap user'
and     	c.credential_name = n.credential_name
and 	   lower(m.credential_name) = n.permutation";
# and 	( lower(m.credential_name) = n.initial+n.ln or lower(m.credential_name) = n.fn+n.ln or  lower(m.credential_name) = n.ln+n.initial or  lower(m.credential_name) = n.ln+n.fn )";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Heuristic Match of Permuted Names - with like clause");
$query="
update 	INV_principal_map
set   	principal_name=c.credential_name, principal_type=c.credential_type,
			match_notes_short  = 'Ldap User',  match_notes_long='Ldap User Match'
from 		INV_principal_map m, INV_credential c, #NAMEP n
where 	m.principal_name is null
and		c.credential_type = 'ldap user'
and     	c.credential_name = n.credential_name
and 	   lower(m.credential_name) like n.permutation+'%'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#
# AT THIS POINT WE SHOULD HAVE ALL THE PRINCIPALS MATCHED
#
# DO THE LOCAL ONES FIRST - THEN THE GLOBAL ONES FROM THE SUBTYPE
banner1("Post Cleanup - NO PRINCIPAL OR CREDENTIAL CHANGES BEYOND THIS POINT");
banner2("Clobbering Disabled LDAP Principals");
diagnose(__LINE__);
#$query="update 	INV_principal_map
#set    	is_deleted='Y', delete_reason='Disabled Ldap Act'
#from  	INV_principal_map p, INV_credential_attribute a
#where 	a.attribute_key='disabled' and a.attribute_value='yes' and a.credential_type='ldap user'
#and	   p.principal_type='ldap user' and p.principal_name = a.credential_name";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

$query="UPDATE INV_principal_map
set 		is_deleted='Y',  delete_reason='disabled'
from   	INV_principal_map m, INV_credential a
where    m.principal_name = a.credential_name
and      m.principal_type = a.credential_type
and		a.is_deleted='Y'
and 	   m.is_deleted='N'
and	   a.credential_type='ldap user'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

#banner2("Clobbering Disabled LDAP Credentials");
#$query="UPDATE INV_principal_map
#set 		is_deleted='Y',  delete_reason='disabled'
#from   	INV_principal_map m, INV_credential a
#where    m.credential_name = a.credential_name
#and      m.credential_type = a.credential_type
#--and      m.credential_id   = a.credential_id
#and      isnull(m.system_name,'x')     = isnull(a.system_name,'x')
#and      isnull(m.system_type,'x')     = isnull(a.system_type,'x')
#and		a.is_deleted='Y'";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

banner2("Fix UP NULL Principals");
diagnose(__LINE__);
#$query="update INV_principal_map
#set   	credential_subtype = 'sql login', principal_name=m.credential_name, principal_type=m.credential_type,
#			match_notes_short  = 'sql login', match_notes_long='Normal Sql Login'
#from 		INV_principal_map m
#where 	m.credential_type='sybase_login'
#and      principal_name is null
#and      match_notes_short is null";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

diagnose(__LINE__);
$query="update INV_principal_map
set   	credential_subtype = 'default', principal_name=m.credential_name, principal_type=m.credential_type
from 		INV_principal_map m
where 	principal_name is null";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner1("UPDATE COMMENTS AND STATIC DATA");
diagnose(__LINE__);

banner2("Update Win32 Local Comment Field");
$query="UPDATE INV_principal_map
set 		credential_notes=a.attribute_value+' '
from   	INV_principal_map m, INV_credential_attribute a
where    m.credential_name = a.credential_name
and      m.credential_type = a.credential_type
and      m.credential_id   = a.credential_id
and      m.system_name     = a.system_name
and      m.system_type     = a.system_type
and		m.credential_type = 'win32 local' and      a.attribute_key   = 'comment'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Update Unix  Comment Field");
$query="UPDATE INV_principal_map
set 		credential_notes=a.attribute_value+' '
from   	INV_principal_map m, INV_credential_attribute a
where    m.credential_name = a.credential_name
and      m.credential_type = a.credential_type
and      m.credential_id   = a.credential_id
and      m.system_name     = a.system_name
and      m.system_type     = a.system_type
and		m.credential_type = '/etc/passwd'
and      a.attribute_key   = 'description'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2("Update Group Comment Field");
$query="UPDATE INV_principal_map
set 		credential_notes=a.attribute_value+' '
from   	INV_principal_map m, INV_credential_attribute a
where		m.credential_subtype = 'ldap group'
and     	m.credential_type='sqlsvr_syslogin'
-- and	   m.credential_notes is null
-- and     lower(m.credential_name) = lower(a.credential_name)
and   	lower(substring(m.credential_name,1+charindex('\\',m.credential_name),1000))=lower(a.credential_name)
and     	a.attribute_key   = 'desc' and     	a.credential_type = 'ldap group'";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner2("Update User Comment Field");
$query="UPDATE INV_principal_map
set 		principal_notes=a.attribute_value
from   	INV_principal_map m, INV_credential_attribute a
where		m.principal_type = a.credential_type
and		m.principal_name = a.credential_name
-- and     	a.credential_type= 'sqlsvr_syslogin'
-- and   	lower(substring(m.credential_name,1+charindex('\\',m.credential_name),1000))=lower(a.credential_name)
and     	( a.attribute_key   = 'desc'
	or a.attribute_key   = 'comment'
	or a.attribute_key = 'description')";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner2("Update System Comment Field");
$query="UPDATE INV_principal_map
set 		system_notes=a.attribute_value
from   	INV_principal_map m, INV_system_attribute a
where		m.system_type = a.system_type
and		m.system_name = a.system_name
and     	a.attribute_name  = 'LDAP_desc' 	";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="UPDATE INV_principal_map
set 		system_notes=a.attribute_value
from   	INV_principal_map m, INV_system_attribute a
where		m.system_type = a.system_type
and		m.system_name = a.system_name
and     	a.attribute_name   = 'DESCRIPTION' 	";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="UPDATE INV_principal_map
set 		principal_notes=''
where		principal_notes = credential_notes";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner1("Set Permissions And Display Names For Accounts");
helptext("Set SYSADM for root attribute");
$query="Update INV_principal_map
set  	is_sysadmin='Y', credential_notes='Super-User On Unix'
where system_type='unix' and credential_name = 'root'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

helptext("Set OPER for oper_role=1 attribute");
$query="UPDATE INV_principal_map
set 		is_operrole='Y'
from   	INV_principal_map m, INV_credential_attribute a
where		m.system_type = a.system_type
and		m.system_name = a.system_name
and		m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and     	a.attribute_key   = 'oper_role'
and	   a.attribute_value    = 'Y' 	";
show_query($query);
MlpAlarm::_querynoresults($query,1);

helptext("Set SSO for sso_role=1 attribute");
$query="UPDATE INV_principal_map
set 		is_securityadmin='Y'
from   	INV_principal_map m, INV_credential_attribute a
where		m.system_type = a.system_type
and		m.system_name = a.system_name
and		m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and     	a.attribute_key   = 'sso_role'
and	   a.attribute_value    = 'Y' 	";
show_query($query);
MlpAlarm::_querynoresults($query,1);

diagnose(__LINE__);

helptext("Set SYSADM for sa_role=1 attribute");
$query="UPDATE INV_principal_map
set 		is_sysadmin='Y'
from   	INV_principal_map m, INV_credential_attribute a
where		m.system_type = a.system_type
and		m.system_name = a.system_name
and		m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and     	a.attribute_key   = 'sa_role'
and	   a.attribute_value    = 'Y' 	";
show_query($query);
MlpAlarm::_querynoresults($query,1);

helptext("Set SYSADM for sysadmin=1 attribute");
$query="UPDATE INV_principal_map
set 		is_sysadmin='Y'
from   	INV_principal_map m, INV_credential_attribute a
where		m.system_type = a.system_type
and		m.system_name = a.system_name
and		m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and     	a.attribute_key   = 'sysadmin'
and	   a.attribute_value    = '1' 	";
show_query($query);
MlpAlarm::_querynoresults($query,1);

# processadmin is not an operator
#helptext("Set OPER for processadmin=1 attribute");
#$query="UPDATE INV_principal_map
#set 		is_operrole='Y'
#from   	INV_principal_map m, INV_credential_attribute a
#where		m.system_type = a.system_type
#and		m.system_name = a.system_name
#and		m.credential_name = a.credential_name
#and		m.credential_type = a.credential_type
#and     	a.attribute_key   = 'processadmin'
#and	   a.attribute_value    = '1' 	";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);
#
#diagnose(__LINE__);

helptext("Set SSO for securityadmin=1 attribute");
$query="UPDATE INV_principal_map
set 		is_securityadmin='Y'
from   	INV_principal_map m, INV_credential_attribute a
where		m.system_type = a.system_type
and		m.system_name = a.system_name
and		m.credential_name = a.credential_name
and		m.credential_type = a.credential_type
and     	a.attribute_key   = 'securityadmin'
and	   a.attribute_value    = '1' 	";
show_query($query);
MlpAlarm::_querynoresults($query,1);

banner2("Display Name");
helptext("Update Principal Display Name For ldap users to 'display' attribute");
$query="UPDATE INV_principal_map
set    display_name=a.display_name
from   INV_principal_map m, INV_credential a
where	m.principal_name = a.credential_name
and		m.principal_type = a.credential_type
and     m.principal_type = 'ldap user'
and     a.display_name is not null";
show_query($query);
MlpAlarm::_querynoresults($query,1);

$query="UPDATE INV_principal_map
set    display_name=a.display_name
from   INV_principal_map m, INV_credential a
where	m.credential_name = a.credential_name
and		m.principal_type = a.credential_type
and     m.credential_type = 'ldap user'
and     m.display_name is null
and     a.display_name is not null";
show_query($query);
MlpAlarm::_querynoresults($query,1);


#$query="UPDATE INV_principal_map
#set 		display_name=attribute_value
#from   	INV_principal_map m, INV_credential_attribute a
#where		m.credential_name = a.credential_name
#and		m.credential_type = a.credential_type
#and      m.credential_type = 'ldap user'
#and     	a.attribute_key  = 'display'
#and      m.display_name is null
#and 		charindex(',',attribute_value)!=0
#and 		attribute_value != ''
#and 		patindex('%disabled%',lower(attribute_value))=0";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

#helptext("Update Principal Display Name For ldap users to 'display' attribute");
#$query="UPDATE INV_principal_map
#set 		display_name=a.attribute_value+', '+b.attribute_value
#from   	INV_principal_map m, INV_credential_attribute a, INV_credential_attribute b
#where		m.principal_name = a.credential_name
#and		m.principal_name = b.credential_name
#and		m.principal_type = a.credential_type
#and		m.principal_type = b.credential_type
#and      m.principal_type = 'ldap user'
#and     	a.attribute_key   = 'ln'
#and     	b.attribute_key   = 'fn'
#and      m.display_name is null and a.attribute_value != '' and b.attribute_value != ''";
#show_query($query);
#MlpAlarm::_querynoresults($query,1);

helptext("Update Principal Display Name to Principal Name For All Remaining");
$query="UPDATE INV_principal_map
set 		display_name=principal_name
from   	INV_principal_map
where		display_name is null";
show_query($query);
MlpAlarm::_querynoresults($query,1);

helptext("Set Mapped LDAP Users");
$query="update INV_principal_map
set   is_ldap_user			= c.is_ldap_user,
	   principal_name			= c.ldap_user,
	   principal_type       = 'ldap user'
from INV_principal_map m, INV_credential_notes c
where (( m.credential_name = c.credential_name and   m.credential_type = c.credential_type )
or    ( m.principal_name = c.credential_name  and   m.principal_type = c.credential_type ))
and   c.is_ldap_user='Y' and c.ldap_user is not null
";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

helptext("UPdate NOtes From Credential NOtes");
$query="update INV_principal_map
set   is_unuseable 		= c.is_unuseable,
		is_serviceaccount = c.is_serviceaccount,
	   is_error    		= c.is_error,
	   is_operator 		= c.is_operator,
	   is_shared			= c.is_shared,
	   user_notes		 	= c.user_notes
from INV_principal_map m, INV_credential_notes c
where ( m.credential_name = c.credential_name and   m.credential_type = c.credential_type )
or    ( m.principal_name = c.credential_name  and   m.principal_type = c.credential_type )";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2('final - mark principals disabled if needed');
$query="UPDATE INV_principal_map
set 		is_deleted='Y',  delete_reason='disabled'
from   	INV_principal_map m, INV_credential a
where    m.principal_name = a.credential_name
and      m.principal_type = a.credential_type
and      m.principal_type = 'ldap user'
and		a.is_deleted='Y'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2('final - mark display name');
$query="UPDATE INV_principal_map
set 		display_name=a.display_name
from   	INV_principal_map m, INV_credential a
where    m.principal_name = a.credential_name
and      m.principal_type = a.credential_type
and      m.principal_type = 'ldap user'
and		a.display_name is not null";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

# remove everything to the right of parenthesis
$query="UPDATE INV_principal_map
set display_name=rtrim(substring(display_name,1,charindex('(', display_name)-1))
where display_name like '%(%'";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2('mark system administrators');
$query="select samid, permission into #TMP from INV_systemuser where  permission like '\%ADMIN'
delete 	#TMP from #TMP t, INV_credential_notes c where t.samid=c.credential_name and c.credential_type='ldap user'
insert 	INV_credential_notes ( credential_name, credential_type ) select samid,'ldap user' from #TMP
drop table #TMP

update 	INV_credential_notes
set   	is_operator='Y', user_notes=isnull(user_notes, 'IT: '+permission)
from 		INV_credential_notes m, INV_systemuser s
where 	permission like '\%ADMIN'
and   	s.samid = m.credential_name";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

banner2('final - update notes field');
$query="UPDATE INV_principal_map
set 		user_notes = coalesce(user_notes,delete_reason,principal_notes,credential_notes)";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);

# FINAL STUFF

banner2('add rows to INV_cert_credential');
$query="
select distinct system_name,credential_name,principal_name,display_name,status='Unknown'
-- ,comment='',approved_by=null,approved_time=null
into #add_certs
from INV_principal_map m, INV_required_certification r
where m.system_name =r.certification_target
and r.certification_type='DATABASE'
and  m.principal_type='ldap user'

delete #add_certs
from #add_certs t, INV_cert_credential c
where t.system_name = c.system_name
and	t.credential_name = c.credential_name
and t.principal_name = c.principal_name
and t.display_name = c.display_name

update #add_certs
set status='Remove'
from #add_certs t, INV_credential_attribute a
where a.attribute_key='disabled' and a.attribute_value='yes' and a.credential_type='ldap user'
and	t.principal_name = a.credential_name

insert INV_cert_credential
select *,'',null,null from #add_certs

drop table #add_certs";
show_query($query);
MlpAlarm::_querynoresults($query,1);
diagnose(__LINE__);


MlpBatchJobEnd() if $BATCH_ID;

exit(0);
