#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# cd to the current directory
use File::Basename;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

use strict;
use Doc;

die "Directory html does not exist\n"
        unless -d 'html';

doc_init(
        out_dir         => 'html'            ,
        colorscheme     => 'LTBLUE',
        color_h1                         => 1,
        home_page       => 'http://www.edbarlow.com',
        copyright_years => '1994-1999'                        ,
        company_name    => 'SQL Technologies'                 ,
        product_name    => 'STORED PROCEDURE LIBRARY'
);

doc_dir(
        files     => ['TOP.pod','INSTALLATION.pod','PLUG.pod','PROCEDURES.pod','RIGHT_TO_USE.pod','UNINSTALL.pod','WARRANTY.pod'],
        dir     => "pod"     ,
        section => "DOCUMENTATION"
);

doc_dir(
        ignore  => ['TOP.pod','INSTALLATION.pod','PLUG.pod','PROCEDURES.pod','RIGHT_TO_USE.pod','UNINSTALL.pod','WARRANTY.pod','main.pod'],
        dir     => "pod",
        section         => "PROCEDURES"
);

doc_finish();

exit(0);

