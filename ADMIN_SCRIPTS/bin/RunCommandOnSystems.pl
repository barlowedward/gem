#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2009-2011 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);
use Sys::Hostname;
my($hostname)=hostname();

use vars qw( $BATCH_ID $USER $FIELDS $SERVER $HTML $TIMEOUT $DOALL $COMMAND $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $DEBUG $NOEXEC);

local $SIG{ALRM} = sub { die "CommandTimedout"; };

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." <ARGUMENTS> Command Command Arguments

The Keyword SERVERNAME is replaced with the name of the server

$PROGRAM_NAME -DOALL=sqlsvr isql -Uxxx -Pxxx -SSERVERNAME -ixxx

will run that isql command on all your sql servers

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --TIMEOUT=seconds - dflt 60
   --DOALL=unix|win32servers
   --NOEXEC\n";
  return "\n";
}

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,		
	#	"USER=s"				=> \$USER ,
	#	"DATABASE=s"		=> \$DATABASE ,
		"HTML"					=> \$HTML,
		"COMMAND=s"			=> \$COMMAND ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOEXEC"				=> \$NOEXEC ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"FIELDS=s"		=> \$FIELDS ,
		"TIMEOUT=s"			=> \$TIMEOUT ,
		"BATCHID=s"			=> \$BATCH_ID ,
		"BATCH_ID=s"			=> \$BATCH_ID ,
	#	"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "Must pass DOALL=unix|win32servers|sybase|oracle|sqlsvr|mysql\n" unless $DOALL;
die "Must pass DOALL=unix|win32servers|sybase|oracle|sqlsvr|mysql\n" 
	unless $DOALL eq "win32servers" or $DOALL eq "unix" or $DOALL eq "sybase" or $DOALL eq "sqlsvr"  or $DOALL eq "mysql"  or $DOALL eq "oracle";
# die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

print "RunCommandOnSystems.pl\n" if $DEBUG;
$TIMEOUT=60 unless $TIMEOUT;

my(@cols);
$cols[100]=1;
if( defined $FIELDS ) {
	$FIELDS =~ s/\s//g;
	foreach ( split(/,/,$FIELDS)) { $cols[$_]=1; }
} else {
	my($i)=0;
	while ($i<100) { $cols[$i]=1; $i++; }
}
my(@all_columns);
for (1..100) { $all_columns[$_]=1; }

# my($program)=shift @ARGV;
my($BASE_COMMAND_TO_RUN)= $COMMAND || join(" ",@ARGV);
print "BASE COMMAND=$BASE_COMMAND_TO_RUN\n";
die "NO COMMAND " unless $BASE_COMMAND_TO_RUN;

my($STARTTIME)=time;
my(@errors);
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	NOSTDOUT			=> 1,
	returnonlycmdline => 1,
	server_command => \&server_command
);

if( $#errors<0 ) {
	print "Program Succeeded - no errors\n";
} else {
	print "\nERRORS OCCURRED\n",join("\n",@errors),"\n";
}

exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	print "INIT!\n" if $DEBUG;
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my( $host, $svrtype, $login, $passwd )=@_;

	my($cmd)=$BASE_COMMAND_TO_RUN;
	$cmd=~s/SERVERNAME/$host/;
	print "==> $cmd \n";
	
	next if $NOEXEC;
	eval {
		alarm($TIMEOUT);
		open( XX, $cmd ." 2>&1 |" ) or die "Cant Run $cmd $!\n";
		while(<XX>) {
			chomp;chomp;
			print "*** ",$_,"\n";	
		}
		close(XX);
		
		
		alarm(0);
	};		
	
	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			print "Command Timed Out\n";			
		} else {
			print "Command Returned $@\n";
		}
	}
	
	
}

__END__

=head1 NAME

RunCommandOnSystems.pl - loop through your servers utility

=head2 DESCRIPTION

Run a program in all your servers.  

In other words... if you wish to run a program to remove user paulr on a server you might type

        adduser -SSYBASE -Usa -PXXX -x -upaulr

but with this utility, you could run that command on ALL your servers in one fell swoop with:

        RunCommandOnSystems.pl --DOALL=sybase "adduser -SSERVERNAME -x -upaulr"

=head2 USAGE

To Run Update Statistics on All msdb databases out there:

perl RunCommandOnSystems.pl --DOALL=sqlsvr perl G:/gem/ADMIN_SCRIPTS/dbi_backup_scripts/update_stats.pl -Dmsdb -Uxxx -Pxxx -SSERVERNAME

=cut
