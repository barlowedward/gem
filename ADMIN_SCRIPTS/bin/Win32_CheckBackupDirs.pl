#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#GENERIC REPOSITORY RUN

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use Repository;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $PUBLISH $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." 

Additional Arguments
	--DEBUG
   --BATCH_ID=id     [ if set - will log via alerts system ]
   ";
  return "\n";
}

$| =1;

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"			=> \$BATCH_ID ,
		"PUBLISH"			=> \$PUBLISH ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($publishdir)=get_gem_root_dir()."/data/CONSOLE_REPORTS/console_sqlsvr/";
die "NO PUBLICATION DIRECTORY $publishdir" if $PUBLISH and ! -d $publishdir;
my($rfile)=get_conf_dir()."/win32_backupdirs.dat";
open( Cr,$rfile ) or die "Cant read $rfile $!\n";
my(@backup_dir_dat);
foreach( <Cr> ) { 
	next if /^\s*#/ or /^\s*$/;
	chomp;chomp;
	push @backup_dir_dat,$_;
	#my($db,$system_type,$host,$dir)=split(/;/,$_,4);	
}
close(Cr);

my($rfile2)=get_conf_dir()."/sqlsvr_nodumpdb.dat";
print "Reading Override Directives from $rfile2\n";
my(%nodumpdb);
if( -r $rfile2 ) {
	open( ND,$rfile2 ) or die "Cant read $rfile2 $!\n";
	while(<ND>) {
		chomp;
		next if /^\s*#/ or /^\s*$/;
		$nodumpdb{$_}=1;
	}
	close(ND);
} else {
	print "File $rfile2 does not exist\n" if ! -e $rfile2;
	die "File $rfile2 not readable \n" if  -e $rfile2;
}

my($rfile3)=get_conf_dir()."/sqlsvr_nodumpdb.dat.proposed";
print "Writing Proposed Override Directives to $rfile3\n";
open( NDD,">".$rfile3 ) or die "Cant write $rfile3 $!\n";
print NDD "# The following are proposed databases to be ignored by Win32_CheckBakcupDirs\n";
print NDD "# Copy this file to sqlsvr_nodumpdb.dat to implement\n";
print NDD "# Format = Server;Database\n\n";

my(@errors);
my($STARTTIME)=time;
repository_parse_and_run(
	PROGRAM_NAME	=> $PROGRAM_NAME,
	AGENT_TYPE		=> 'Gem Monitor',
	NOSTDOUT			=> 1 ,
	DOALL				=> "sqlsvr",
	BATCH_ID			=> $BATCH_ID ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

foreach (@errors) { print "ERROR:",$_; }
close(NDD);
close(Cx);



exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";

}

###############################
# USER DEFINED server_command() FUNCTION!
###############################

sub server_command {
	my($cursvr)=@_;

	my(%hsh) = get_password_info(-type=>"sqlsvr",-name=>$cursvr);
	
	print "Skipping Dr System $cursvr" if $DEBUG and $hsh{SERVER_TYPE} eq "DR";
	print "Skipping Qa System $cursvr" if $DEBUG and $hsh{SERVER_TYPE} eq "QA";
	print "Skipping Dev System $cursvr" if $DEBUG and $hsh{SERVER_TYPE} eq "DEVELOPMENT";
	return if $hsh{SERVER_TYPE} eq "DR";		# skip dr
	return if $hsh{SERVER_TYPE} eq "QA";		# skip qa
	return if $hsh{SERVER_TYPE} eq "DEVELOPMENT";		# skip qa
		
	if( $PUBLISH ) {
		open(PD,">".$publishdir.$cursvr."_CheckBackupDir.html");
		print PD "<h1>Backup Check on Server $cursvr</h1>";
		print PD "Win32_CheckBackupDirs.pl checks the backup directories (stored in win32_backupdirs.dat, generated 1ce a week by Win32_CreateBackupDirs.pl) ";
		print PD "and correlates those backups to the actual non-read only databases on each server.  There is an override ";
		print PD "file in conf/sqlvr_nodump.dat. <p>";
	}
	
	
	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	print "\n\n", uc($hsh{SERVER_TYPE}) , " ";	
	print "SERVER $cursvr VER $DBVERSION PAT $PATCHLEVEL\n\n";	
	print PD "<h3>	SERVER $cursvr VER $DBVERSION PAT $PATCHLEVEL\n\n</h3>" if $PUBLISH;
	
	#my($server_type,@databaselist)=dbi_parse_opt_D("%",1);
	
	my($dbquery)="select name from master..sysdatabases
where status&(32+64+128+256+512+1024+4096+32768) = 0
and name not in ('syblogins','model','tempdb','pubs','pubs2','Northwind',
			'sybsystemdb','sybsecurity','sybsystemprocs','ReportServer','ReportServerTempDB') 
and has_dbaccess(name)=1
order by name";

	my(@rc) = dbi_query( -db=>"master", -query=>$dbquery );
	my(@databaselist);
	foreach (@rc) {
		push @databaselist,dbi_decode_row($_);;
	}

	# print $cursvr," ",$server_type," ",join(" ",@databaselist),"\n";
	
	my(%dbstatus);
	foreach (@databaselist) { 	
#		s/[\._]backup//i;
#		s/[\._]dump//i;
#		s/[\._\d][\._\d][\._\d][\._\d][\._\d]+//ig;
#		s/[\._]\d\d\d\d+//i;
#		s/\.bak//i;
#		s/\.zip//i;
#		s/[\._]db//i;
		next if /model/i or /tempdb/i or /_test$/ or /_stage$/ or /_restore$/ or /\d\d\d\d\d\d\d\d/;
		next if /profiler/i or /reportserver/ or /_save$/i;
		$dbstatus{$_}="NOT DUMPED"; 
		$dbstatus{$_}="OVERRIDE" if $nodumpdb{$cursvr.";".$_}; 
	}		
		
	# my($foundbackupstr)='';
	my(@allfiles);
	print PD "<TABLE BORDER=1>"  if $PUBLISH;
	print PD "<TR><TH>NUMBER OF FILES</TD><TH>TYPE</TD><TH>DIRECTORY</TD></TR>" if $PUBLISH;
		
	foreach ( @backup_dir_dat ) {
		my($db,$system_type,$host,$dir)=split(/;/,$_,4);
		
		next unless $db eq $cursvr;
		# $foundbackupstr .= "Directory $dir\n";	
		
		my($rc)=opendir(DIR,$dir);
		if( ! $rc ) {
			print STDERR "Cant Open Directory $dir For Listing $!\n";
			print PD "<font color=RED>FATAL ERROR:	Cant Open Directory $dir For Listing $!</font>" if $PUBLISH;
			close(PD);
			return;
		}
		
		my(@oldfiles,@newfiles);
		foreach ( readdir(DIR) ) {
			next if /^\./ or /\.txt$/i or /\.trn$/i or /\.ldf$/i or /\.mdf$/i or /\_tlog\_/i;
			if( -M "$dir/$_">=14 ) {
				push @oldfiles,$_;
			} else {
				push @newfiles,$_;
			}
		}
		print "OLD FILES (over 14 days) FOUND in $dir\n";
		my($c)=$#oldfiles+1;
		print PD "<TR><TD>$c</TD><TD>OLD FILES (over 14 days)</TD><TD>directory $dir</td></tr>" if $PUBLISH;
		print "\t",join("\n\t",@oldfiles ),"\n"   if $#oldfiles>=0;
		#		print PD "<TABLE><TR><TD>",join("</TD><TD>\t",@oldfiles ),"</TD></TR></TABLE>" if $#oldfiles>=0 and $PUBLISH;
		print "BACKUP FILES FOUND in $dir\n";
		my($c)=$#newfiles+1;
		print PD "<TR><TD>$c</TD><TD>BACKUP FILES</TD><TD>directory $dir</td></tr>" if $PUBLISH;
		print "\t",join("\n\t",@newfiles ),"\n"   if $#newfiles>=0;
		# print PD "<TABLE><TR><TD>",join("</TD><TD>\t",@newfiles ),"</TD></TR></TABLE>" if $#newfiles>=0 and $PUBLISH;
		push @allfiles,@newfiles;
		closedir(DIR);
	}
	print PD "</TABLE>" if $PUBLISH;
		
	print "\n";
	print PD "<p>" if $PUBLISH;
			
#	foreach (sort @allfiles) {		
#		# parse
#		next if -d "$dir/$_";		# not a directory
#		next if /\.txt$/i or /\.trn$/i or /\.ldf$/i or /\.mdf$/i;
#		my($fileage)= -M "$dir/$_";
#		next if $fileage>7;			
#		$foundbackupstr.= sprintf "%20s %s\n","",$_;
#		s/\....$//;
#		s/[\._]backup//i;
#		s/[\._]dump//i;
#		s/[\._\d][\._\d][\._\d][\._\d][\._\d]+//ig;
#		
#		#s/\.bak//i;
#		#s/\.zip//i;
#		s/[\._]db//i;
#		if( $dbstatus{$_} ) {
#			$dbstatus{$_}="OK" if $dbstatus{$_} eq "NOT DUMPED";
#		} else {
#			my(@words=split(/\s\.\_/,$_);
#			foreach(@words) {
#				$dbstatus{$_}="OK" if $dbstatus{$_} eq "NOT DUMPED";
#			}
#		}
#	}	
	
	# backstop
	foreach my $undumped ( keys %dbstatus) {
		next if $dbstatus{$undumped} ne "NOT DUMPED";
		# its still not found
		foreach (@allfiles) {
			next unless /$undumped/;
			$dbstatus{$undumped} = "OK";
		}
	}
	
	my($ok)='YES';
	print "DATABASE ANALYSIS\n";
	print PD "<h3>DATABASE ANALYSIS<h3><TABLE BORDER=1>" if $PUBLISH;
	foreach (sort ( keys %dbstatus)) { 
		printf "%20s %26s %s\n",  $cursvr,$_,$dbstatus{$_}; # if $dbstatus{$_} eq "NOT DUMPED"; 
		if( $PUBLISH ) {
			if( $dbstatus{$_} eq "NOT DUMPED" ) {
				printf PD "<TR BGCOLOR=YELLOW><TD>%20s</TD><TD>%26s</TD><TD>%s</TD></TR>\n",  $cursvr,$_,$dbstatus{$_};
			} else {
				printf PD "<TR><TD>%20s</TD><TD>%26s</TD><TD>%s</TD></TR>\n",  $cursvr,$_,$dbstatus{$_};
			}			
		}
		next if $dbstatus{$_} ne "NOT DUMPED";
		if( $ok eq "YES" ) {
			$ok="$_";
		} else {
			$ok.=" ".$_;
		}
		
		print NDD "$cursvr;$_\n";
		push @errors,"$cursvr;$_\n";
	}
	
	if( $ok eq "YES" ) {
		print "\n*** SERVER IS OK\n";
		print PD "</TABLE><br>\n*** SERVER IS OK\n" if $PUBLISH;
		MlpHeartbeat(
				-state=>"OK",
			  	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-message_text=>"System $cursvr has all backups"
			) if $BATCH_ID;
	
	} else {
		print "\n*** SERVER IS NOT OK - HAS MISSING DUMPS\n";
		print PD "</TABLE><br><font color=red><b>\n*** SERVER IS NOT OK - HAS MISSING DUMPS</b></font>\n" if $PUBLISH;
		
		MlpHeartbeat(
				-state=>"ERROR",
			  	-monitor_program=>$BATCH_ID,
				-system=>$cursvr,
				-message_text=>"System $cursvr Missing Backups For Db $ok (override file=sqlsvr_nodumpdb\*)"
			) if $BATCH_ID;
	#	print $foundbackupstr;
	}
	
	if( $PUBLISH ) {
		close(PD);
	}
	
}

__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

