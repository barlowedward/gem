#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
	print "rev_db.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST -OOutDir [-E] [objects...]

		-E - no permissions

		if no objects passed, then it will do all objects except built
			in constraints that are found in the database.

		DB_LIST may include wildcards - but you probably dont want this as
			there is only one output directory and the files will overwrite
			each other.

		The directory passed with -O must exist

	Reverse Engineer To Files... Extensions based on object name\n";
	return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_U $opt_S $opt_P $opt_D $opt_O $opt_E);
die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('U:D:S:P:O:E');

use Sybase::SybFunc;

$opt_D="%" unless defined $opt_D;
die usage("Must pass output directory\n") unless defined $opt_O;
die usage("Must pass sa password\n" )	   unless defined $opt_P;
die usage("Must pass server\n")
	unless defined $opt_S;
die usage("Must pass sa username\n" )
	unless defined $opt_U;

$opt_O .= "/" unless $opt_O=~/\/$/;

if( defined $opt_E ) {
	$opt_E=undef;
} else {
	$opt_E=1;
}

die "No directory $opt_O\n" unless -d $opt_O;

print "Connecting to sybase\n";
db_connect($opt_S,$opt_U,$opt_P)
	or die "Cant connect to $opt_S as $opt_U\n";
db_msg_exclude("Object does not have any declarative constraints.");

my(@databaselist)=db_query("master","select name from sysdatabases where name like \"$opt_D\"  and status & 0x1120 = 0 and status2&0x0030=0");

foreach my $db (@databaselist) {
	print "Working On Database $db\n\n";
	my(%objtype);
	foreach (db_query($db,"select name,type from sysobjects where type!='S' and uid=1")){
		my($n,$t)=db_decode_row($_);
		next if $n=~/\d\d\d\d\d$/;
		$t=~s/\s//g;
		$t="def" if $t eq "D";
		$t="prc" if $t eq "P";
		$t="trg" if $t eq "TR";
		$t="tbl" if $t eq "U";
		$t="rul" if $t eq "R";
		$t="vie" if $t eq "V";
		$objtype{$n} = $t;
	}
	@ARGV=sort keys %objtype if $#ARGV<0;
	foreach (@ARGV) {
		if( ! defined $objtype{$_} ) {
			print "*** ERROR - OBJECT $_ NOT FOUND\n";
			next;
		}
		print "--- Working On Object $_\n";
		my($fname)=$opt_O.$_.".".$objtype{$_};
		open(OUTFILE,"> ".$fname) or die "Cant open file $fname: $!\n";
		print OUTFILE db_get_sql_text($_,1,$opt_E,1);
		close OUTFILE
	}
}

__END__

=head1 NAME

rev_db.pl - Reverse engineer sql to individual files

=head2 DESCRIPTION

Reverse engineer  objects to individual files.  If you dont pass a set
=cut
=head2 USAGE

rev_db.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST -OOutDir [-E] [objects...]

		-E - no permissions
		if no objects passed, then it will do all objects except built
			in constraints that are found in the database.
		DB_LIST may include wildcards - but you probably dont want this as
			there is only one output directory and the files will overwrite
			each other.
		directory passed with -O must exist

	Reverse Engineer To Files... Extensions based on object name

=cut
