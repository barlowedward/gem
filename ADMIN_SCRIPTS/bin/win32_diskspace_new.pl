#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use MlpAlarm;
use CommonFunc;
use Win32::DriveInfo;
use Repository;
use Sys::Hostname;
use Getopt::Long;
use vars qw( $OUTFILE $ERRFILE $BATCHNAME $SYSTEMS $ALARMWHO $LOGALARM $ALARMTHRESH $NORETRY $WARNTHRESH
				 $THRESHFILE $SLEEPTIME $DEBUG $NOSTDOUT $LOCKDIR $SHOWERR $CREATEDAT);

my($VERSION)="1.0";
my(@dfltdisks)=('c$','d$','e$','f$','g$','h$','i$');

$|=1;

my($curdir)=cd_home();

sub usage {
 	return "Usage: win32_diskspace.pl --OUTFILE=file --LOGALARM --NORETRY --ERRFILE=file --SYSTEMS=system[,system] [--DEBUG] [--NOSTDOUT] --ALARMWHO=netsend[,netsend] --ALARMTHRESH=pct --WARNTHRESH=pct --SLEEPTIME=seconds --LOCKDIR=dir --CREATEDAT --SHOWERR\n";
}

sub error_out {
   use MlpAlarm;
   my($msg)=join("",@_);
   $msg = "BATCH ".$BATCHNAME.": ".$msg if $BATCHNAME;

	MlpBatchJobErr( $msg ) if $BATCHNAME;
   MlpEvent(
      -monitor_program=> $BATCHNAME,
      -debug=>$DEBUG,
      -system=> "",
      -message_text=> $msg,
      -severity => "ERROR"
   );
   die $msg;
}

sub output {
	print OUTF @_ if $OUTFILE;
	print @_ unless $NOSTDOUT;
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
   GetOptions(  "OUTFILE=s"	=> \$OUTFILE,
		"ERRFILE=s"		=> \$ERRFILE,
		"SYSTEM=s"		=> \$SYSTEMS,
		"SERVER=s"		=> \$SYSTEMS,
		"SYSTEMS=s"		=> \$SYSTEMS,
		"ALARMWHO=s"	=> \$ALARMWHO,
		"LOGALARM"		=> \$LOGALARM,
		"THRESHFILE=s"	=> \$THRESHFILE,
		"NORETRY"		=> \$NORETRY,
		"ALARMTHRESH=s"=> \$ALARMTHRESH,
		"NOSTDOUT"		=> \$NOSTDOUT,
		"DEBUG"			=> \$DEBUG,
		"SHOWERR"		=> \$SHOWERR,
		"CREATEDAT"		=> \$CREATEDAT,
		"WARNTHRESH=s"	=> \$WARNTHRESH,
		"BATCHNAME=s"	=> \$BATCHNAME,
		"BATCH_ID=s"	=> \$BATCHNAME,
		"LOCKDIR=s"		=> \$LOCKDIR,
		"SLEEPTIME=s"	=> \$SLEEPTIME	);

unlink $ERRFILE if defined $ERRFILE and -w $ERRFILE;
$ALARMTHRESH=90 unless defined $ALARMTHRESH;
$WARNTHRESH=$ALARMTHRESH  unless defined $WARNTHRESH;
$WARNTHRESH=80  unless defined $WARNTHRESH;
$BATCHNAME="PcDiskspace" unless $BATCHNAME;

if( defined $SLEEPTIME and is_up(-dir=>$LOCKDIR) ) {
	print "Program Is Allready Running - Aborting\n" 		unless defined $NOSTDOUT;
	output("\nSuccessful Completion\n");
	exit(0);
}
mark_up(-dir=>$LOCKDIR) if defined $SLEEPTIME;

if( defined $OUTFILE ) {
	open(OUTF,"> $OUTFILE") or error_out( "Cant Write To $OUTFILE : $!\n" );
}
if( $BATCHNAME ) {
	output("PC Disk Space Report - Batch=$BATCHNAME - version $VERSION\n");
} else {
	output("PC Disk Space Report - ",basename($0)," - version $VERSION\n");
}

print "win32_diskspace.pl: run at ". localtime(time). " on host ". hostname(). "\n";
output("Run At: ",(scalar localtime(time))."\n\n");
output("Alarms generated when the disk is at ",$ALARMTHRESH,"%\n") unless $CREATEDAT;
output("Warnings generated when the disk is at ",$WARNTHRESH,"%\n") unless $CREATEDAT;
output("(These values are configurable by the report)\n\n") unless $CREATEDAT;
output("--CREATEDAT specified -> searching for existence of disks\n") if $CREATEDAT;

my(@systems);
my(%disks_by_svr);
if( ! defined $SYSTEMS ) {
	@systems=get_password(-type=>'win32servers');
	foreach my $sys (@systems) {
		my %info = get_password_info(-name=>$sys, -type=>'win32servers');
		my(@x)=split(/\s+/,$info{DISKS});
		$disks_by_svr{$sys} = \@x;
	}
	#close(DISKFILE);
} else {
	@systems=split(/,/,$SYSTEMS);
	foreach ( @systems ) {
		$disks_by_svr{$_} = \@dfltdisks;
	}
}

MlpBatchJobStart( -BATCH_ID => $BATCHNAME ) if $BATCHNAME;

output("Reading Override File\n") if $DEBUG;
my(%warnthresh, %alarmthresh, %critthresh);
if( defined $THRESHFILE) {
	if( ! -r $THRESHFILE and -r "$THRESHFILE.sample" ) {
		use File::Copy;
		output("Copying Sample File To $THRESHFILE\n");
		copy "$THRESHFILE.sample",$THRESHFILE;
	}
        if( -r $THRESHFILE ) {
      		output("Reading $THRESHFILE\n");
         	open (TR, $THRESHFILE ) or die "Cant open $THRESHFILE\n";
                while (<TR>) {
                   chomp;
                   chomp;
                   next if /^#/ or /^\s*$/;
                   next unless /,\s*PcDiskspace\s*,/;

                   if( /,PcDiskspace,/ ) {
                   	my(@x)=split(/,/);
                   	$x[0]=~s/\s//g;
                   	$x[2]=~s/\s//g;
                   	output("   Setting $x[2] threshold for system $x[0] to $x[3] \n");
                   	$warnthresh{$x[0]}=$x[3] 	if $x[2]=~/WARNING/i;
                   	$alarmthresh{$x[0]}=$x[3] 	if $x[2]=~/ERROR/i;
                   	$critthresh{$x[0]}=$x[3] 	if $x[2]=~/CRITICAL/i;
                   }
                }
         	close(TR);
      }
}

my($m)=sprintf "%20s %10s %10s %3s\n", "Drive", "UsedMb", "TotalMB", "Pct";
output($m) if ! defined $CREATEDAT;
output("\nTESTING FOR DISK EXISTENCE\n\n") if $CREATEDAT;
my(@founddisks);
my(%foundbackupdirs);		# 1=exists in cfg, 0=new, 2=in config but not exists
#my(@foundbackupdirsARY);	# match to above

#
# GET CLUSTER INFO
#
my(%GemNames,%ActiveNames,%SharedDrives,%NodeNames,%ClusterNodeList);
if( defined $CREATEDAT ) {
	print "Reading Cluster Information\n";
	my($dir)=get_conf_dir();
	my($file)=get_conf_dir()."/win32_clusternodes.dat";

	die "no $file found" 					unless -e $file;
	die "cant read $file" 					unless -r $file;
	die "cant write $file" 					unless -w $file;

	open( C1,$file ) or die "Cant read $file $!\n";
	my($cursvr);
	while(<C1>) {
		chop;chomp;
		next if /^#/;
		if( ! /^\s/ ) {
			$cursvr=$_;
		} else {
			my($a,$b)=split(/: /,$_,2);
			if( /^\tServerName: / ) {
				$GemNames{$cursvr}=$b;
				# print "DBGDBG - gem name= $cursvr / $b\n";
			} elsif( /^\tActiveSystemName: / ) {
				$ActiveNames{$cursvr}=$b;
			} elsif( /^\tSharedDrive: / ) {
				$SharedDrives{$cursvr}=$b;
			} elsif( /^\tClusterNode: / ) {
				foreach ( split(/\s+/,$b) ) {
					$NodeNames{$cursvr}=$_;
					$ClusterNodeList{uc($_)}=$cursvr;
					# print "DBGDBG - node name $_ \n";
				}
			} else {
				die "Cant parse $_\n";
			}
		}
	}
	close(C1);

#X	$file=get_conf_dir()."/win32_backupdirs.dat";
#X	open( C2,$file ) or die "Cant read $file $!\n";
#X	while(<C2>) {
#X		chop;chomp;
#X		$foundbackupdirs{$_} = "CFG" unless /^\s*#/;
#X		#push @foundbackupdirsARY,$_;
#X	}
#X	close(C2);
}

while ( 1 ) {
	foreach my $sys (sort @systems) {
		my($system_alarm)="Ok";
		my($system_msg)  ="";

		if( defined $CREATEDAT ) {
			my(%found);		# FOUND DISKS ARE DFLT DISKS PLUS EXISTING DISKS

			if( $ClusterNodeList{uc($sys)} ) {	# we are a cluster node not a real server
				print "\t$sys is a cluster node\n";
				$found{'C$'}=1;
			} else {
				foreach (@dfltdisks) { $found{uc($_)}=1; }
				foreach (@{$disks_by_svr{$sys}} ) { $found{uc($_)}=1; }
				if( $GemNames{$sys} ) {		# we are a cluster
					print "\t$sys is a cluster\n";
					foreach ( split(/\s+/,$SharedDrives{$sys} )) { $found{uc($_.'$')}=1; }
					delete $found{'C$'};
					#foreach ( keys %found ) { print "DBGDBG: ",$_," \n"; }
					#print "\n";
				} else {
					print "\t$sys is not a cluster node\n";
				}
			}

			foreach my $dsk (sort keys %found) {
				$_="\\\\".$sys."\\".$dsk;
				my(@diskdat)= Win32::DriveInfo::DriveSpace($_);
				next unless defined $diskdat[5];
				push @founddisks,$sys."::".$dsk;
				output( "Discovered $_\n" );
			}

			next;
		}

		if( ! defined $disks_by_svr{$sys} )  {
			output("No Disks Defined For $sys\n");
			next;
		}

		my($wt)=$warnthresh{$sys}  || $WARNTHRESH;
      my($at)=$alarmthresh{$sys} || $ALARMTHRESH;
      my($ct)=$critthresh{$sys}  || 98;

		output("DBG: $sys -- $wt/$at/$ct\n") if defined $DEBUG;

		my(@disks) = @{$disks_by_svr{$sys}};
		foreach my $dsk (@disks) {
			$_="\\\\".$sys."\\".$dsk;
			my(@diskdat)= Win32::DriveInfo::DriveSpace($_);
			if( ! defined $diskdat[5] ) {
				sleep(2) unless defined $NORETRY;
				@diskdat= Win32::DriveInfo::DriveSpace($_);
				if( ! defined $diskdat[5] ) {
					my($m)=sprintf "%20s Can Not Fetch Data $!\n", $_;
					output($m) if defined $SHOWERR;
					next;
				}
			}
			my($diskpct)=100 - int((100*$diskdat[6])/$diskdat[5]);
			# output("DBG: $sys $dsk - $diskpct \n");
			my($alarm)="Ok";
			if($diskpct == 100) {
		   		$alarm = "Emergency"  ;
				$system_alarm = "Emergency";
			} elsif($diskpct >= $ct) {
		   		$alarm = "Critical"   ;
				$system_alarm = "Critical" unless $system_alarm eq "Emergency";
			} elsif($diskpct >= $at) {
				$alarm = "Error";
				$system_alarm = $alarm
					unless 	$system_alarm eq "Emergency"
						or 	$system_alarm eq "Critical";
			} elsif($diskpct >= $wt) {
				$alarm = "Warning";
				$system_alarm = $alarm if $system_alarm eq "Ok";
			}

			my($a)=sprintf "%20s %10d %10d %2d   %10s\n",
				$_,
				int(($diskdat[5]-$diskdat[6]) / (1024*1024)),
				int($diskdat[5] / (1024*1024)),
				$diskpct,$alarm;
			output($a);

			# format: time, server, state, msg
			my($msg)=sprintf("Drive %s at %s%% (UsedMb=%s TotalMb=%s)",
					$_,$diskpct,
					int(($diskdat[5]-$diskdat[6]) / (1024*1024)),
					int($diskdat[5] / (1024*1024)));
			if( defined $LOGALARM ) {	# save using alarming system

				my($q)="exec PER_saveintstat '".
								$sys  ."','win32servers',\n\t'".
								$dsk  ."',\n\t'','','win32_diskspace',null,\n\t".
								int(($diskdat[5]-$diskdat[6]) / (1024*1024)).",".
								int($diskdat[5] / (1024*1024));
				# print $q;
				_querynoresults( $q, 1 );

				MlpHeartbeat(
					-debug=>$DEBUG,
					-state=>uc($alarm),
				   -monitor_program=>$BATCHNAME,
					-system=>$sys,
					-subsystem=>$dsk,
					-message_text=>$msg
				);
			}

			if( $alarm ne "Ok" ) {	$system_msg.=$alarm.": ".$msg."\n"; }
		}

		if( $system_alarm ne "Ok" and $system_alarm ne "Warning" and defined $ALARMWHO ) {
			my(@targets)=split(/,/,$ALARMWHO);
			foreach my $tgt (@targets) {
				output("############### NET SEND $tgt ####################\n");
				output("ALARM($tgt) From System $sys\n$system_msg\n");
				chdir("C:/");
				foreach( split(/\n/,$system_msg) ) {
					system("net send $tgt \'$sys $_\n\'");
				}
				cd_home();
				output("############### NET SEND DONE ####################\n");
			}
			if( defined $ERRFILE ) {
				open(ERRF,">> $ERRFILE") or error_out( "Cant Write To $ERRFILE : $!\n" );
				# format: time, server, state, msg
				printf ERRF "%s,%s,%s,Diskspace,%s\n",
					(scalar localtime(time)),
					$sys,
					$system_alarm,$system_msg;
				close(ERRF);
			}
		}
	}
	last unless defined $SLEEPTIME;
	output("Sleeping For $SLEEPTIME seconds at ".localtime(time)."\n");
	sleep($SLEEPTIME);
	mark_up(-dir=>$LOCKDIR) if defined $SLEEPTIME;
};

close(OUTF) 		if defined $OUTFILE;
chmod(0666,$OUTFILE)  	if defined $OUTFILE;

if( $CREATEDAT ) {
	output("WRITING DISK DRIVE INFO TO THE CONFIGURATION FILE\n");
	my(%svrlines);
	foreach my $dd (sort @founddisks) {
		my($s,$d)=split(/\:\:/, $dd);
	#	print "dd=$dd s=$s d=$d\n";
		if( $svrlines{$s."|DISKS"} ) {
			$svrlines{$s."|DISKS"} .= " $d";
		} else {
			$svrlines{$s."|DISKS"} = "$d";
		}

		# ok now check for backup directories
#		my($drive)="//$s/$d";
#		print "TESTING for backup directories on $drive\n";
#
#		my($rc)=opendir(DIR,$drive);
#		if( ! $rc ) {
#			print STDERR "Cant Open Directory $drive For Listing $!\n";
#			return;
#		}
#		my(@dlist)=grep( !/^\./, readdir(DIR) );
#		closedir(DIR);
#		foreach (@dlist) {		# is it a backup dir
#			if( /dump/i or /backup/i ) {
#				next if /report/i or /tran/i or /log/i;
#				my($directory)="$drive/$_";
#				next unless -d $directory;
#				print "\tFound Dump Directory $directory\n";
#				$foundbackupdirs{$directory}="NEW" unless $foundbackupdirs{$directory};
#
#				my($rc)=opendir(DIR,$directory);
#				if( ! $rc ) {
#					print STDERR "Cant Open Directory $directory For Listing $!\n";
#					next;
#				}
#				my(@xlist)=grep( !/^\./, readdir(DIR) );
#				closedir(DIR);
#				foreach ( @xlist ) {
#					next unless -d "$directory/$_";
#					next if /report/i or /tran/i or /log/i;
#					$foundbackupdirs{"$directory/$_"}="NEW" unless $foundbackupdirs{"$directory/$_"};
#				}
#			}
#		}
	}


	#my(%update_dat);
	#foreach (sort keys %svrlines) {
		#print "EDITING ==> ",$svrlines{$_},"\n";
		#add_edit_item(-debug=>1, -type=>'win32servers', -name=>$server, DISKS=>"C$ D$ E$");
	#	$x{$_."|DISKS"}=$srvlines{$_}
	#}
	add_edit_item(-debug=>$DEBUG, -type=>'win32servers', -values=>\%svrlines );

	# my(@disks) = @{$disks_by_svr{$sys}};

#	my($wfile)=get_conf_dir()."/win32_backupdirs.dat";
#	open( Cx,">>".$wfile ) or die "Cant read $wfile $!\n";

	#foreach( @foundbackupdirsARY ) {
	#	print $_,"\n";
	#	$foundbackupdirs{$_} = -1;
	#}
#	foreach ( keys %foundbackupdirs ) {
#		next if $foundbackupdirs{$_} eq "NEW";
#		print "Writing $_ to Cfg File\n";
#		print Cx $_,"\n";
#	}

	#my($q)='select distinct last_fulload_file from BackupState union select last_fulldump_file from BackupState';
	#my(@rc)=_querywithresults($query,1);
	#foreach (@rc) { print $line,' ',join("\t",dbi_decode_row($_)),"\n";	}
	#close(Cx);
}
#foreach (@systems) {
#	MlpBatchDone( -system=>$BATCHNAME,
#		      -debug=>$DEBUG,
#		      -system	=> $_)
#			if defined $LOGALARM;
#}
# MlpBatchJobEnd() if $LOGALARM;
MlpBatchJobEnd() if $BATCHNAME;
output("\nSuccessful Completion\n");
exit(0);

__END__

=head1 NAME

win32_diskspace.pl - NT Diskspace Report

=head2 DESCRIPTION

Uses Win32 Libraries to check out a list of disks on remote NT servers.

=head2 USAGE

Usage: win32_diskspace.pl --OUTFILE=file --LOGALARM --ERRFILE=file --SYSTEMS=system[,system] [--DEBUG] --ALARMWHO=netsend[,netsend] --ALARMTHRESH=pct --WARNTHRESH=pct --SLEEPTIME=seconds [--NOSTDOUT]

 OUTFILE:   	Output Report File
 LOGALARM:   	Save Heartbeats using MlpAdmin
 ERRFILE:   	Output Report for just errors
 SYSTEMS:   	List of systems to work on - comma separated
 ALARMWHO:   	Comma separated list of netsend addresses to send to (built in)
 ALARMTHRESH:  Alarm percent
 WARNTHRESH:   Warning percent
 SLEEPTIME:   	Seconds in a loop
 DEBUG:   		Debug
 NOSTDOUT:	Dont print stuff to stdout

=head2 CONFIG

Reads win32_password.cfg (from sw directory or one above it) file which contains lines as follows

	ADSRV060 C$ D$ E$ F$
	ADSRV088 C$ D$ E$ F$
	ADSRV089 C$ D$ E$ F$
	ADSRV056 C$ D$ E$ F$
	PLATINUM C$ D$ E$ F$
	ADSRV061 C$ D$ E$ F$
=cut

