#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use MlpAlarm;
use Repository;
use Getopt::Long;

use vars qw( $BATCHID );

sub usage {
	print "usage CleanupFiles.pl --BATCHID=id\n";
	return "";
}
die usage("Bad Parameter List\n") unless GetOptions( "BATCHID=s" => \$BATCHID );
die "MUST PASS --BATCHID!\n" unless $BATCHID;

MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor');

my($working_directory)=get_gem_root_dir()."/data/GEM_BATCHJOB_LOGS";
print "Cleaning $working_directory\n";
opendir(DIR,$working_directory) || die("CleanupFiles() Can't open directory $working_directory for reading\n");
my(@dirlist) = grep(!/^\./,readdir(DIR));
closedir(DIR);
foreach (@dirlist) {
	next if /CleanupFiles/ or -M $working_directory."/$_" < .001;
	if( -z $working_directory."/".$_ ) {
		print "Removing $working_directory/$_\n";
		unlink($working_directory."/".$_);
	} else {
		chmod 0666,$working_directory."/".$_;
	}
}

MlpBatchJobEnd();

__END__

=head1 NAME

CleanupFiles.pl - Basic Cleanup

=head2 USAGE

	CleanupFiles.pl

=head2 DESCRIPTION

Removes GEM_BATCHJOB_LOGS files that are over a minute old and have 0 size.
chmod 666 the others.  Useful because we dont want these 0 sized files out there.

=cut

