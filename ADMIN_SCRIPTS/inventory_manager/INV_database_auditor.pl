#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use MlpAlarm;
use DBIFunc;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $DIAG $NOSAVE );

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- \n".
basename($0)." -DOALL=sybase
-or- \n".
basename($0)." -DOALL=sqlsvr
-or- \n".
basename($0)." -DOALL=oracle

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOSAVE dont save results - print save statements

   --DIAG will show output results in short form\n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOSAVE"				=> \$NOSAVE ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG,
		"DIAG"      		=> \$DIAG );

#/* , is_deleted       , display_name     */
my($cred_sql) = "(credential_name, credential_type, credential_id, system_name, system_type, collection_date, collected_by )";

#MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Credential Mgt',-SUBKEY=>$DOALL ) if $BATCH_ID;

# MlpBatchJobEnd() if $BATCH_ID;
# exit(0);

my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	AGENT_TYPE		=> 'Credential Mgt',
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL ,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);
# MlpBatchJobEnd() if $BATCH_ID;
exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

sub hdr {
	print "============================================================\n";
	print "| ",@_;
	print "============================================================\n";
	print "\n";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;
	sub myformat {
		my(@x)=@_;
		my($str)=join("||",@x);
		$str=~ s/\s*\|\|\s*/\|\|/g;
		$str =~ s/\s+$//;
		return $str;
	}
	hdr("[$cursvr] STARTED SERVER AUDIT\n");
	if( $DOALL eq "oracle" ) {

		hdr("[$cursvr, oracle] COLLECTING SECURITY CREDENTIALS\n");
		my($q)="delete INV_credential \n\twhere system_name = ".dbi_quote(-text=>$cursvr)." and system_type = ".dbi_quote(-text=>$DOALL);
		print $q,"\n" if $DEBUG or $NOSAVE;
		MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

		$q="select USERNAME from dba_users where ACCOUNT_STATUS!='EXPIRED' and ACCOUNT_STATUS!='EXPIRED & LOCKED'";
		my(@rc)=dbi_query(-db=>'', -query=>$q);
		foreach (@rc) {
			my(@vals) = dbi_decode_row($_);
			#print join("~",dbi_decode_row($_) ),"\n";

			my($base)=			dbi_quote(-text=>$vals[0]).",".			# login
									"'sqlsvr_syslogin',".
									dbi_quote(-text=>'N.A.').",".			# sid is binary so N.A. works ...
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$DOALL);				# systype

			my($q)= "insert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'INV_database_auditor.pl')";
			print $q,"\n";#k if $DEBUG;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
		}
		print "Successful Competion of $cursvr\n";
		return;
	}
	my($sparg)=' @srvname=\''.$cursvr.'\', @hostname=\'xxx\', @dont_format=\'Y\'';

	my($typ,@db)=dbi_parse_opt_D($DATABASE,1);
	$typ='sqlsvr' if $typ eq "SQLSERVER";
	$typ='sybase' if $typ eq "SYBASE";
	die "HUMMM typ=$typ" unless $typ eq "sybase" or $typ eq "sqlsvr";

	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();

#	my($q)="delete INV_system_audit_result \n\twhere system_name = ".dbi_quote(-text=>$cursvr)." and system_type = ".dbi_quote(-text=>$typ);
#	print $q,"\n" if $DEBUG;
#	MlpAlarm::_querynoresults($q,1);
#
#	hdr("[$cursvr, $typ] RUNNING SYSTEM SECURITY AUDIT (sp__auditsecurity)\n");
#	wr_debug( "exec sp__auditsecurity $sparg \n") unless $DIAG;
#	my(@rc)=dbi_query(-db=>'', -query=>"exec sp__auditsecurity $sparg");
#	foreach( @rc ) {
#			my(@vals);
#			foreach ( dbi_decode_row($_) ) {
#				s/\s+$//;
#				push @vals,$_;
#			}
#
#			if( $#vals < 5 ) {
#				print "FATAL ERROR - BAD RESULTS ROW FROM QUERY\n";
#				print "QUERY=exec sp__auditsecurity $sparg\n";
#				print "RC=".join(" ",@vals),"\n";
#				die "FATAL ERROR";
#			}
#			#wr_debug( "SEC0||".join("||",@d),"\n" ) unless $DIAG;
#			#my(@vals) = grep( s/\s+$//,@d);
#			#wr_debug( "SEC1||".join("||",@vals),"\n" ) unless $DIAG;
#			wr_report( "sp_auditsecurity||".$vals[2]."||".$vals[6]."\n" ) if $DIAG;
#			# ok so _querynoresults is a shitty backdoor but its easy to code...
#
#			my($q)= "insert INV_system_audit_result values (\n\t".
#									dbi_quote(-text=>$cursvr)	.",".				# system
#									dbi_quote(-text=>$typ)		.",".				# systyp
#									dbi_quote(-text=>'sp__auditsecurity')	.",".		# monitor
#									dbi_quote(-text=>$vals[3])	.",".				# database
#									dbi_quote(-text=>'?')	.",".					# errorlvl
#									dbi_quote(-text=>$vals[2])	.",\n\t".		# msgid
#									dbi_quote(-text=>$vals[6])	.					# msgtxt
#									",\n\tgetdate(),".								# moddt
#									dbi_quote(-text=>$PROGRAM_NAME)	.")";		# modby
#			print $q,"\n" if $DEBUG;
#			MlpAlarm::_querynoresults($q,1);
#	}

	hdr("[$cursvr, $typ] COLLECTING SECURITY CREDENTIALS\n");
	my($q)="delete INV_credential \n\twhere system_name = ".dbi_quote(-text=>$cursvr)." and system_type = ".dbi_quote(-text=>$typ);
	print $q,"\n" if $DEBUG or $NOSAVE;
	MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

	$q="delete INV_credential_attribute \n\twhere system_name = ".dbi_quote(-text=>$cursvr)." and system_type = ".dbi_quote(-text=>$typ);
	print $q,"\n" if $DEBUG or $NOSAVE;
	MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

	#print "exec sp__helplogin\n";
	if( $DBTYPE eq "SQL SERVER" ) {
		#	if( $PATCHLEVEL>9 ) {
		#	} elsif( $PATCHLEVEL>8 ) {

			my(%ATT_MAP)=(
					'default database' 	=> 1,
					'language' 				=> 2,
					'sysadmin' 				=> 3,
					'securityadmin' 		=> 4,
					'processadmin' 		=> 5,
					'isntname' 				=> 6,
					'isntgroup' 			=> 7,
					'isntuser' 				=> 8,
					'createdate' 			=> 9,
					'accdate' 				=> 10,
					'dbcreator' 			=> 11
				);

			my(%loginsfnd);
			foreach( dbi_query(-db=>"master",-query=>"select distinct name,dbname,
						language, sysadmin, securityadmin, processadmin, isntname,
						isntgroup,isntuser, createdate, accdate,dbcreator,
						convert(int,sid)
						from master..syslogins where hasaccess=1 and denylogin=0")) {
				my(@vals) = dbi_decode_row($_);
				next if $loginsfnd{$vals[0]};
				$loginsfnd{$vals[0]} = 1;
				wr_debug( "syslogins||".myformat(@vals),"\n" ) unless $DIAG;
				wr_report( "syslogins||".$vals[0]."\n" ) if $DIAG;

				my($base)=	dbi_quote(-text=>$vals[0]).",".			# login
								"'sqlsvr_syslogin',".
								dbi_quote(-text=>'N.A.').",".			# sid is binary so N.A. works ...
								dbi_quote(-text=>$cursvr)	.",".			# system
								dbi_quote(-text=>$typ);				# systype

				my($q)= "insert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'INV_database_auditor.pl')";
				print $q,"\n" if $DEBUG or $NOSAVE;
				MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

				# audit the login record
				my($q)= "exec INV_track_config_info_sp ".
										dbi_quote(-text=>$cursvr)	.",".			# system
										dbi_quote(-text=>$typ)		.",".			# systype
										"'login',".
										dbi_quote(-text=>$vals[0]).",".			# login
										"'',".
										dbi_quote(-text=>$vals[12]);				# sid
				print $q,"\n" if $DEBUG or $NOSAVE;
				MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

				foreach my $str (keys %ATT_MAP ) {
					my($col)=$ATT_MAP{$str};
					$vals[$col] = "N.A." if $vals[$col] =~ /^\s*$/;
					$q="insert INV_credential_attribute values \n\t( $base ,\n\t '".$str."',".
									 dbi_quote(-text=>$vals[$col])	.")";
				 	print $q,"\n" if $DEBUG or $NOSAVE;
					MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
				}

				$vals[3]=~s/\s//g;
				$vals[4]=~s/\s//g;
				$vals[5]=~s/\s//g;
				# audit the sql server roles
				my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'role',".
									dbi_quote(-text=>'sysadmin').",".		# rolename
									dbi_quote(-text=>$vals[0]).",".			# login_name
									dbi_quote(-text=>$vals[3]);				# true or false
				print $q,"\n" if $DEBUG or $NOSAVE;
				MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

				my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'role',".
									dbi_quote(-text=>'securityadmin').",".		# rolename
									dbi_quote(-text=>$vals[0]).",".			# login_name
									dbi_quote(-text=>$vals[4]);				# true or false
				print $q,"\n" if $DEBUG or $NOSAVE;
				MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

				my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'role',".
									dbi_quote(-text=>'processadmin').",".		# rolename
									dbi_quote(-text=>$vals[0]).",".			# login_name
									dbi_quote(-text=>$vals[5]);				# true or false
				print $q,"\n" if $DEBUG or $NOSAVE;
				MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
			}
		#}
	} elsif( $DBTYPE eq "SYBASE" ) {
		my(%ATT_MAP)=(
				'default database' 	=> 2,
				'short password' 		=> 3,
				'locked' 				=> 4,			# handled elsewhere via lockmanager
				'expired' 				=> 5,			# handled elsewhere via lockmanager
				'sa_role' 				=> 6,
				'sso_role' 				=> 7,
				'oper_role' 			=> 8,
				'remote' 				=> 9
			);
		foreach( dbi_query(-db=>"master",-query=>"exec sp__helplogin \@dont_format='Y'")) {
			my(@vals) = dbi_decode_row($_);
			wr_debug( "sp__helplogin||".myformat(@vals),"\n" ) 		unless $DIAG;
			wr_report( "sp__helplogin||".$vals[0]."||".$vals[1]."\n" ) if $DIAG;

			if( $vals[4] =~ /y/i ) {
				wr_report("Ignoring Locked Login $vals[1]\n");
				next;
			}

			# ignore locked logins
			my($base)=			dbi_quote(-text=>$vals[1]).",".			# login
									"'sybase_login',".
									dbi_quote(-text=>$vals[0]).",".			# sid
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ);						# systype

			my($q)= "insert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'INV_database_auditor.pl')";
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;


			# audit the login record
			my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'login',".
									dbi_quote(-text=>$vals[1]).",'',".			# login
									dbi_quote(-text=>$vals[0]);				# sid
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

			foreach my $str (keys %ATT_MAP ) {
				my($col)=$ATT_MAP{$str};
				$vals[$col] = "N" if $vals[$col] =~ /^\s*$/;
				$q="insert INV_credential_attribute values \n\t( $base ,\n\t '".$str."',".
								 dbi_quote(-text=>$vals[$col])	.")";
			   print $q,"\n" if $DEBUG or $NOSAVE;
				MlpAlarm::_querynoresults($q,1)  unless $NOSAVE;
			}

			my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'role',".
									dbi_quote(-text=>'sa_role').",".			# rolename
									dbi_quote(-text=>$vals[1]).",".			# login_name
									dbi_quote(-text=>$vals[6]);				# true or false
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

			my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'role',".
									dbi_quote(-text=>'sso_role').",".		# rolename
									dbi_quote(-text=>$vals[1]).",".			# login_name
									dbi_quote(-text=>$vals[7]);				# true or false
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

			my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'role',".
									dbi_quote(-text=>'oper_role').",".		# rolename
									dbi_quote(-text=>$vals[1]).",".			# login_name
									dbi_quote(-text=>$vals[8]);				# true or false
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
		}
	} else {
		die "UNKNOWN TYPE $DBTYPE\n";
	}

	hdr("[$cursvr, $typ] PERFORMING SYSCONFIGURES AUDIT\n");
	my(%confkeys);
	foreach( dbi_query(-db=>"master",-query=>"select comment,value from master..sysconfigures")) {
		my(@vals) = dbi_decode_row($_);
		wr_debug( "sysconfigures||".myformat(@vals),"\n" ) unless $DIAG;
		wr_report( "sysconfigures||".$vals[0]."||".$vals[1]."\n" ) if $DIAG;
		next if $confkeys{$vals[0]};
		$confkeys{$vals[0]} = 1;
		$vals[0] =~ s/\s+$//;
		$vals[1] =~ s/\s+$//;
		$vals[0] =~ s/^\s+//;
		$vals[1] =~ s/^\s+//;

		$q= "exec INV_track_config_info_sp ".
						dbi_quote(-text=>$cursvr)	.",".			# system
						dbi_quote(-text=>$typ)		.",".			# systype
						"'config',".
						dbi_quote(-text=>$vals[0]).",'',".		# configname
						dbi_quote(-text=>$vals[1]);				# configvalue
		print $q,"\n" if $DEBUG or $NOSAVE;
		MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
	}

	if( $typ eq "sybase" ) {
		hdr("[$cursvr, $typ] PERFORMING sp__diskdevice AUDIT \n");
		foreach( dbi_query(-db=>"master",-query=>"exec sp__diskdevice \@dont_format='Y'")) {
			my(@vals) = dbi_decode_row($_);
			wr_debug( "sp__diskdevice||".myformat(@vals),"\n" ) unless $DIAG;
			wr_report( "sp__diskdevice||".$vals[0]."||".$vals[1]."\n" ) if $DIAG;
			$vals[0] =~ s/\s//g;
			$vals[1] =~ s/\s//g;
			$vals[2] =~ s/\s//g;
			$q= "exec INV_track_config_info_sp ".
						dbi_quote(-text=>$cursvr)	.",".			# system
						dbi_quote(-text=>$typ)		.",".			# systype
						"'devices',".
						dbi_quote(-text=>$vals[0]).",".			# devicename
						dbi_quote(-text=>$vals[1]).",".			# physname
						dbi_quote(-text=>$vals[2]);				# size
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
		}
	}

	hdr("[$cursvr, $typ] PERFORMING sp_helpserver AUDIT \n");
	foreach( dbi_query(-db=>"master",-query=>"sp_helpserver")) {
		my(@vals) = dbi_decode_row($_);
		wr_debug( "sp_helpserver||".myformat(@vals),"\n" ) unless $DIAG;
		wr_report( "sp_helpserver||".$vals[0]."||".$vals[1]."\n" ) if $DIAG;
		my($statuscol)=2;
		$statuscol=3 if $typ eq "sybase";
		my($q)= "exec INV_track_config_info_sp ".
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ)		.",".			# systype
									"'sysservers',".
									dbi_quote(-text=>$vals[0]).",'',".		# name,null
									dbi_quote(-text=>$vals[$statuscol]);	# status
		print $q,"\n" if $DEBUG or $NOSAVE;
		MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
	}

	hdr("[$cursvr, $typ] PERFORMING UPTIME AUDIT\n");
	foreach( dbi_query(-db=>"master",
		-query=>"select crdate from master..sysdatabases where name='tempdb'")) {
		my(@vals) = dbi_decode_row($_);
		my($q)= "exec INV_track_config_info_sp ".
										dbi_quote(-text=>$cursvr)	.",".			# system
										dbi_quote(-text=>$typ)		.",".			# systype
										"'uptime',".
										"'',".
										"'',".
										dbi_quote(-text=>$vals[0]);
		print $q,"\n" if $DEBUG or $NOSAVE;
		MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
	}

	hdr("[$cursvr, $typ] PERFORMING VERSION AUDIT\n");
	foreach( dbi_query(-db=>"master",
		-query=>'select @@VERSION')) {
		my(@vals) = dbi_decode_row($_);
		my($q)= "exec INV_track_config_info_sp ".
										dbi_quote(-text=>$cursvr)	.",".			# system
										dbi_quote(-text=>$typ)		.",".			# systype
										"'version',".
										"'',".
										"'',".
										dbi_quote(-text=>$vals[0]);
		print $q,"\n" if $DEBUG or $NOSAVE;
		MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
	}


	# only audit sybase sysdatabases - autogrowth on sql server makes it meaningless there
	if( $typ eq "sybase" ) {
		hdr("[$cursvr, $typ] PERFORMING SYSDATABASES AUDIT\n");
		foreach( dbi_query(-db=>"master",-query=>"sp__helpdb 'NoPrint',\@dont_format='Y'")) {
			my(@vals) = dbi_decode_row($_);
			wr_debug( "sysdatabases||".myformat(@vals),"\n" ) unless $DIAG;
			$vals[0]=~s/\s//g;
			$vals[1]=~s/\s//g;
			wr_report( "sysdatabases||".$vals[0]."||".$vals[1]."\n" ) if $DIAG;
			my($q)= "exec INV_track_config_info_sp ".
										dbi_quote(-text=>$cursvr)	.",".			# system
										dbi_quote(-text=>$typ)		.",".			# systype
										"'dataspace','',".
										dbi_quote(-text=>$vals[0]).",".
										dbi_quote(-text=>$vals[1]);
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;

			my($q)= "exec INV_track_config_info_sp ".
										dbi_quote(-text=>$cursvr)	.",".			# system
										dbi_quote(-text=>$typ)		.",".			# systype
										"'logspace','',".
										dbi_quote(-text=>$vals[0]).",".
										dbi_quote(-text=>$vals[2]);
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
		}
	}

	hdr("[$cursvr, $typ] PERFORMING DATABASE AUDIT\n" );
	my($curdbid,$maxdbid)=(0,$#db);
	$maxdbid++;
	foreach my $curdb (sort @db) {
		next if $curdb =~ /\./; 	# no periods in the database name
		$curdbid++;
#		printf " %-40.40s %3s/%-3s ==> %s\n",$curdb,$curdbid,$maxdbid,"sp_auditdb";
		my($db_is_notok)=0;
#		foreach( dbi_query(-db=>$curdb,
#			-query=>"exec sp__auditdb \@dont_format='Y',\@srvname='".$cursvr."',\@hostname='xxx'")) {
#			my(@vals) = dbi_decode_row($_);
#			if( $#vals < 4 ) {
#				$db_is_notok=1;
#				# next if (  $curdb eq "model" or $curdb eq "dbccdb" ) and $vals[0] =~ /10351/;		# ignore cant use model
#				# woah its an error!
#				warn "sp_auditdb on $cursvr/$curdb returned ",join("|",@vals),"\n";
#				my($q)= "insert INV_system_audit_result values (\n\t".
#									dbi_quote(-text=>$cursvr)	.",".				# system
#									dbi_quote(-text=>$typ)		.",".				# systyp
#									dbi_quote(-text=>'sp__auditdb')	.",".		# monitor
#									dbi_quote(-text=>$curdb)	.",".				# database
#									dbi_quote(-text=>'?')	.",".					# errorlvl
#									dbi_quote(-text=>0)	.",\n\t".		# msgid
#									dbi_quote(-text=>"sp_auditdb error: ".join(" ",@vals))	.					# msgtxt
#									",\n\tgetdate(),".								# moddt
#									dbi_quote(-text=>$PROGRAM_NAME)	.")";		# modby
#				print $q,"\n" if $DEBUG;
#				MlpAlarm::_querynoresults($q,1);
#				next;
#			}
#			#wr_report( "sp_auditdb||".myformat(@vals),"\n" );
#			$vals[6]=~s/\s+$//;
#			wr_report( "sp_auditdb||".$vals[2].'||'.$vals[6]."\n" ) if $DIAG;
#			my($q)= "insert INV_system_audit_result values (\n\t".
#									dbi_quote(-text=>$cursvr)	.",".				# system
#									dbi_quote(-text=>$typ)		.",".				# systyp
#									dbi_quote(-text=>'sp__auditdb')	.",".		# monitor
#									dbi_quote(-text=>$curdb)	.",".				# database
#									dbi_quote(-text=>'?')	.",".					# errorlvl
#									dbi_quote(-text=>$vals[2])	.",\n\t".		# msgid
#									dbi_quote(-text=>$vals[6])	.					# msgtxt
#									",\n\tgetdate(),".								# moddt
#									dbi_quote(-text=>$PROGRAM_NAME)	.")";		# modby
#			print $q,"\n" if $DEBUG;
#			MlpAlarm::_querynoresults($q,1);
#		}
		next if $db_is_notok;

		printf " %-40.40s %3s/%-3s ==> %s\n",$curdb,$curdbid,$maxdbid,"sp_helpuser";
#		printf " %-40s         ==> %s\n","","sp_helpuser";
		my(%helpuser_groups);		# track the groups that come back in dup rows
		foreach( dbi_query(-db=>$curdb,-query=>"sp_helpuser")) {
			my(@vals)  = dbi_decode_row($_);
			if( $#vals == 0 ) {
				printf " %-20.20s   %s\n","","(skip - cant use database)";
				$db_is_notok = 1;
				last;
			}
			my($logcol)= 2;	$logcol=0 if $typ eq "sybase";
			my($grpcol)= 1;	$grpcol=3 if $typ eq "sybase";
			$vals[$logcol] =~ s/\s+$//;
			$vals[$logcol] =~ s/^\s+//;
			$vals[$grpcol] =~ s/\s+$//;
			$vals[$grpcol] =~ s/^\s+//;
			next if ! $vals[$logcol] or ! $vals[$grpcol];
			wr_debug( "sp_helpuser||".myformat(@vals),"\n" ) unless $DIAG;
			wr_report( "sp_helpuser||".$vals[$logcol]."||".$vals[$grpcol]."\n" ) if $DIAG;
			if( defined $helpuser_groups{$vals[$logcol]}) {
				$helpuser_groups{$vals[$logcol]} .= ",".$vals[$grpcol];
			} else {
				$helpuser_groups{$vals[$logcol]} = $vals[$grpcol];
			}
		}
		next if $db_is_notok;

		foreach ( keys %helpuser_groups ) {
			my($q)= "exec INV_track_config_info_sp ".
										dbi_quote(-text=>$cursvr)	.",".			# system
										dbi_quote(-text=>$typ)		.",".			# systype
										"'sysusers',".
										dbi_quote(-text=>$_).",".			# username
										dbi_quote(-text=>$curdb).",".
										dbi_quote(-text=>$helpuser_groups{$_});		# groupname
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
		}

		#printf "[%-20s %3s of %3s] ==> %s\n",$curdb,$curdbid,$maxdbid,"sp_helpgroup";
		printf " %-40s         ==> %s\n","","sp_helpgroup";
		my(%foundgrp);
		foreach( dbi_query(-db=>$curdb,-query=>"sp_helpgroup")) {
			my(@vals) = dbi_decode_row($_);
			if( $#vals == 0 ) {
				printf " %-20.20s   %s\n","","(skip - sp_helpgroup not found)";
				last;
			}

		#	next if $foundgrp{$vals[0]};
		#	$foundgrp{$vals[0]} = 1;
			wr_debug(  "sp_helpgroup||".myformat(@vals),"\n" ) unless $DIAG;
			wr_report( "sp_helpgroup||".$vals[0]."||".$vals[1]."\n" ) if $DIAG;

			if( defined $foundgrp{$vals[0]}) {
				$foundgrp{$vals[0]} .= ",".$vals[1];
			} else {
				$foundgrp{$vals[0]} = $vals[1];
			}
		}

		foreach ( keys %foundgrp ) {
			my($q)= "exec INV_track_config_info_sp ".
										dbi_quote(-text=>$cursvr)	.",".			# system
										dbi_quote(-text=>$typ)		.",".			# systype
										"'sysgroup',".
										dbi_quote(-text=>$_).",".
										dbi_quote(-text=>$curdb).",".
										dbi_quote(-text=>$foundgrp{$_});
			print $q,"\n" if $DEBUG or $NOSAVE;
			MlpAlarm::_querynoresults($q,1) unless $NOSAVE;
		}
	}
}

__END__

=head1 NAME

INV_database_auditor.pl - perform a sybase or sql server database audit

=head1 SYNOPSIS

Main auditor for sybase and sql server.  Will run the following queries

	Saves sp_auditsecurity results into INV_system_audit_result
	Saves sp_auditdb results for each database into INV_system_audit_result
	Saves credential information from database with sqlsvr_syslogin or sybase_syslogin as key
	Track system configuration table changes, sp_configure, syslogins,  etc...
		select * form sysconfigures
		sp_helpserver
		sp__diskdevice for database size/log (sybase only)
		sp__auditdb
		@@VERSION

	populates INV_systemuser with ldap accounts that have admin permissions

	tracks configuration changes to all the above as well

=head1 SYNTAX

Uses Standard repository_parse_and_run()

INV_database_auditor.pl --USER=sa_user  --DATABASE=database  --SERVER=server  --PASSWORD=sa_password
	- or -
INV_database_auditor.pl	-DOALL=sybase|sqlsrvr

  --BATCH_ID=id	[ to log via alerts system ]
  --PRODUCTION|--NOPRODUCTION
  --DEBUG
  --NOSTDOUT		[ suppress console messages ]
  --OUTFILE=outfile

=head1 BUGS

=cut
