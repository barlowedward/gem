use strict;
use Getopt::Long;
use Repository;
use File::Basename;
use File::Copy;
use Sys::Hostname;
use CommonFunc;

my( $MAX_CLOCK_SKEW )=3;		# in minutes
my($DEVELOPERMODE);

my($PROGRAM_DESCRIPTION)="Create GEM Batch Scripts";
my($VERSION)=1.0;
use vars qw( $DEBUG $ROLLOUT_DIRECTORY);

$|=1;

die "ERROR: Bad Parameter List $!\n" 
	unless GetOptions( "DEBUG"	=> \$DEBUG,
		"ROLLOUT_DIRECTORY=s"   => \$ROLLOUT_DIRECTORY );
print "$0 - v",$VERSION,"\n";
print "Run at ".localtime()."\n";

print "Rollout Directory is $ROLLOUT_DIRECTORY\n" if $ROLLOUT_DIRECTORY;
print "   Reading gem.dat\n";
my $GemConfig=read_gemconfig( -debug=>$DEBUG);
$ROLLOUT_DIRECTORY = $GemConfig->{MIMI_FILENAME_WIN32} unless $ROLLOUT_DIRECTORY;

die "ERROR: CANT FIND ROLLOUT DIRECTORY - $ROLLOUT_DIRECTORY\n" unless -d $ROLLOUT_DIRECTORY;
die "ERROR: CANT WRITE ROLLOUT DIRECTORY - $ROLLOUT_DIRECTORY\n" unless -d $ROLLOUT_DIRECTORY;

my($gem_root_dir)=get_gem_root_dir();
die "ERROR: No Gem Root Dir" unless -d $gem_root_dir;
print "  GEM ROOT=$gem_root_dir\n";

foreach my $rolout ($ROLLOUT_DIRECTORY,"$gem_root_dir/data/CONSOLE_REPORTS") {
	copy_a_directory( $gem_root_dir."/lib", 	$rolout, 	"" );	
	copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/cgi-bin", 	$rolout, 	"" );	
	mkdir( $rolout."/img" );
	copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/cgi-bin/img", 	$rolout."/img", 	"" );	
	mkdir( $rolout."/styles" );
	copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/cgi-bin/styles", 	$rolout."/styles", 	"" );	
	
	foreach my $dir ( "/console_mysql","/console_oracle","/console_sqlsvr","/console_sybase",
							"/cronlogs","/backup_logs","/gem_batchjob_logs","/html_output" ) {
		mkdir( $rolout.$dir );
		copy_a_directory( $gem_root_dir."/ADMIN_SCRIPTS/console/console_template", 	$rolout.$dir, 	"" );	
	}
}

print "SUCCESSFUL: Rollout Succeeded\n";
exit(0);

#mkdir( $ROLLOUT_DIRECTORY."/console_mysql" );
#mkdir( $ROLLOUT_DIRECTORY."/console_oracle" );
#mkdir( $ROLLOUT_DIRECTORY."/console_sqlsvr" );
#mkdir( $ROLLOUT_DIRECTORY."/console_sybase" );
#mkdir( $ROLLOUT_DIRECTORY."/cronlogs" );
#mkdir( $ROLLOUT_DIRECTORY."/backup_logs" );
#mkdir( $ROLLOUT_DIRECTORY."/gem_batchjob_logs" );
#mkdir( $ROLLOUT_DIRECTORY."/html_output" );

# skip files that have 0 size or have changed recently
sub copy_a_directory {
	my($fromdir, $todir, $required_string, $recurse, $prefix )=@_;
	print "\n++++++++++++++++++++++++++++++++++++++++++++++++\n";
	print "+ copy_a_directory\n+ FROM: $fromdir\n+ TODIR: $todir\n";
	print "+ SEARCHSTRING=$required_string\n" if $required_string;
	print "+ PREFIX=$prefix" if $prefix;
	print "++++++++++++++++++++++++++++++++++++++++++++++++\n";

	if( ! -d $todir ) {
		die "ERROR: Error Directory $todir Does Not Exist\n"
		#print "(not) " if $DEVELOPERMODE;
		#print "Creating Directory $todir\n";
		#mkdir( $todir, 0777 ) unless $DEVELOPERMODE;
	}

	print "   Reading Directory $fromdir";
	opendir(DIR,$fromdir) or die("ERROR: Cant Open Directory $fromdir For Listing\n");
	my(@to_copy_files)=grep( !/^\./, readdir(DIR) );
	closedir(DIR);
	my($x)=$#to_copy_files;
	$x++;
	print "   $x files\n";

	my($cnt,$unreadable)=(0,0);
	foreach my $f ( sort ( @to_copy_files )) {
		my($longfname)=$fromdir."/".$f;
		if( -d $longfname ) {	# skip directories
			if( $recurse ) {
				$prefix.="." if $prefix;
				copy_a_directory( $fromdir."/$f" , $todir, $required_string, $recurse, $prefix.$f);
				return;
			}
		}
		next if $required_string and $longfname !~ /$required_string/;
		if( -r $longfname ) {	# file readable
			$cnt++;
			my($filesize)= -s $longfname;
			next if $filesize == 0;
			next if $f =~ /depricated/ or $f =~ /201\d/;
			unlink "$todir/$f" if -w "$todir/$f";
			print "    - Copying $f\n";			
			copy(  $longfname,	$todir."/".$f ) or die "ERROR: Cant copy $longfname to $todir/$f $!";
		} else {
			print "Unreadable: $longfname\n";
			$unreadable++;
		}
	}
	print "      Copied $cnt Files ";
	print "($unreadable were unreadable)" if $unreadable;
	print "\n";
}
