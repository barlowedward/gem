# Database Surveyer
#
# copyright (c) 2010 by Edward Barlow
#
# Surveys & Autodiscovers Stuff a Dba Needs
#
# It all requires appropriate db variables
#   like SYBASE and ORAHOME and whatever
#
use 	strict;
use 	DBI;
use   DBIFunc;
use   File::Copy;
use   Sys::Hostname;
use 	Carp qw/confess/;
use 	Getopt::Long;
use   IO::Socket;
use   File::Basename;

use	Repository;
use	RepositoryRun;
use	CommonFunc;
use   DBI_discovery;
use   Inventory;

my(%TYPEMAP)=(
	'sybase' => 'unix',
	'oracle' => 'win32',
	'mysql'  => 'unix',
	'sqlsvr' => 'win32',
);
my @configuration_errors;
my  $curdir=dirname($0);
die "You Must Map Samba Shares To A Drive Letter.\nCurdir=$curdir\n"
	if $^O =~ /MSWin32/ and $curdir =~ /\\\\/;
chdir($curdir) or die "Cant cd to $curdir: $!\n";

my($PROGRAM_NAME)	=	'survey.pl';
my($HOSTNAME)		=	hostname();
my(%found_name);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $DIAGNOSE $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage	{
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- (native authentication) \n".
$PROGRAM_NAME." -DATABASE=db  -SERVER=SERVER
-or- \n".
basename($0)." -DOALL=sybase
-or- \n".
basename($0)." -DOALL=sqlsvr

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
   return "\n";
}

$| =1;    			# unbuffered standard output
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
					
		"DEBUG"				=> \$DEBUG 	,
		"DIAGNOSE"			=> \$DIAGNOSE,		
		);

header( "Starting $PROGRAM_NAME at ".localtime()."\n" );
my $GEM_ROOT_DIR=get_gem_root_dir();

_statusmsg("  DEBUG           => $DEBUG    \n") if $DEBUG;
_statusmsg("  DIAGNOSE        => $DIAGNOSE\n") if $DIAGNOSE;

header("Reading GEM Configuration");

my	$GEMCONFIG=read_gemconfig(-debug=>$DEBUG);
	if( ! defined $GEMCONFIG ) {
		print "The GENERIC ENTERPRISE MANAGER IS NOT CONFIGURED\n";
		print "Please Run configure.pl prior to running $PROGRAM_NAME\n";
		exit(1);
	}

	if( $GEMCONFIG->{INSTALLSTATE} ne "INSTALLED" ) {
		die "THIS GEM CONFIGURATION's STATE IS ",$GEMCONFIG->{INSTALLSTATE},"\nPlease run configure.pl to install an et it to INSTALLED\n";
	}

	_statusmsg( "Using Gem Configuration Value - USE_ORACLE = ",		$GEMCONFIG->{USE_ORACLE},"\n" );
	_statusmsg( "Using Gem Configuration Value - USE_SYBASE_ASE = ",	$GEMCONFIG->{USE_SYBASE_ASE},"\n" );
	_statusmsg( "Using Gem Configuration Value - USE_MYSQL = ",			$GEMCONFIG->{USE_MYSQL},"\n" );
	_statusmsg( "Using Gem Configuration Value - USE_SQLSERVER = ",	$GEMCONFIG->{USE_SQLSERVER},"\n" );

	_statusmsg( "Using Gem Configuration Value - NT_MONITOR_SERVER   = ",
			$GEMCONFIG->{NT_MONITOR_SERVER},"\n" );
	_statusmsg( "Using Gem Configuration Value - UNIX_MONITOR_SERVER = ",
			$GEMCONFIG->{UNIX_MONITOR_SERVER},"\n" );
	_statusmsg( "Using Gem Configuration Value - ALT1_MONITOR_SERVER = ",
			$GEMCONFIG->{ALT1_MONITOR_SERVER},"\n" );
	_statusmsg( "Using Gem Configuration Value - ALT2_MONITOR_SERVER = ",
			$GEMCONFIG->{ALT2_MONITOR_SERVER},"\n" );

	if( $HOSTNAME eq $GEMCONFIG->{NT_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Win32 Monitor Station\n") ;
	} elsif( $HOSTNAME eq $GEMCONFIG->{UNIX_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Unix Monitor Station\n") ;
	} elsif( $HOSTNAME eq $GEMCONFIG->{ALT1_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Win32 Monitor Station\n") ;
	} elsif( $HOSTNAME eq $GEMCONFIG->{ALT2_MONITOR_SERVER} ) {
		_statusmsg( "You are running on your Unix Monitor Station\n") ;
	} else {
		_statusmsg( "You are not running on a Monitor Station\n") ;
	}

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL ,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	returnonlycmdline => 1,
	init 				=> \&init,
	server_command => \&server_command
);

exit(0);


###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr,$curtyp,$login,$pass)=@_;	
	print "==> svr=$cursvr typ=$curtyp log=$login pass=$pass\n";	
	
	my(%SERVER_PROPERTIES);
	my($rcptr);
	if( $curtyp eq "oracle" ) {
		$rcptr=FetchMetadataOracle($cursvr,$login,$pass,%SERVER_PROPERTIES);
	} elsif( $curtyp eq "sybase" ) {
		$rcptr=FetchMetadataSybase($cursvr,$login,$pass,%SERVER_PROPERTIES);
	} elsif( $curtyp eq "sqlsvr" ) {
		$rcptr=FetchMetadataSqlsvr($cursvr,$login,$pass,%SERVER_PROPERTIES);
	} elsif( $curtyp eq "mysql" ) {
		$rcptr=FetchMetadataMysql($cursvr,$login,$pass,%SERVER_PROPERTIES);
	} elsif( $curtyp eq "unix" ) {
		$rcptr=FetchMetadataUnix($cursvr,$login,$pass,%SERVER_PROPERTIES);
	} elsif( $curtyp eq "win32servers" ) {
		$rcptr=FetchMetadataWin32($cursvr,$login,$pass,%SERVER_PROPERTIES);
	}
	
	use Data::Dumper;
	print Dumper $rcptr;
	print "\n";	
	
	foreach ( keys %$rcptr ) {
		printf " -- %25s = %s\n", $_, $rcptr->{$_};
	}
	#	$SERVER_PROPERTIES{$_} = $rcptr->{$_};
	#}
	
	print "\n";	
	print	"InvUpdSystem(",$cursvr,", ", $curtyp."_discovery)\n";
	InvUpdSystem (	-system_name			=> $cursvr,
						-system_type			=> $curtyp."_discovery",
						-attribute_category	=> 'survey',
						-attributes 			=> $rcptr,
						-clearattributes		=> 1);
}

sub error_out { die @_; }
sub _statusmsg { print "        ",@_; }
sub header {
	my($str)=join("",@_);
	$str =~ s/\s*$//;
	print "\n--------------------------------------------------------------------------\n| ",
		$str,
		"\n","--------------------------------------------------------------------------\n";
}

sub _debugmsg { print( "[debug] ",@_) if $DEBUG; }
sub _diagmsg  { print( "[diag]  ",@_) if $DIAGNOSE; }
sub RunCommand {
 	my($cmd)=@_;
 	if( ! open( WINCMD,"$cmd |")) {
		print "Cant Run $cmd\n";
		return undef;
	}
	my(@output);
   while ( <WINCMD> ) {
   	chomp; chomp;
		push @output, $_;
   }
   close(WINCMD);
   return @output;
}
