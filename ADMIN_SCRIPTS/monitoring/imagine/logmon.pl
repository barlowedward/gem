#!/apps/perl/linux/perl-5.8.2/bin/perl

use strict;
use IO::Wrap;
use Net::SMTP;

my $loop = 1; $SIG{INT} = 'exitloop'; sub exitloop() {$loop--}

die "usage: $0 <config file>\n" if ($#ARGV < 0);

my $config = conf($ARGV[0]);

die "corrupt configuration $ARGV[0]\n" unless ref($config);

my $conf = $config->{conf}->{global};
my $logs = $config->{logs};
my $type = $config->{type};

my $hist = [
	    {
		tstamp =>	tstamp(),
		file =>		'watching '.scalar(keys %{$logs}).' logfiles',
		pos =>		'',
		errmsg =>	'information',
		action =>	'starting log monitor',
		colour =>	'darkslategray3',
		line =>		'',
	    },
	    ];





init_rgb();
init_logs(1);

my $file;
while ($loop) {
    print "I am in the while loop\n";
    for $file (keys %{$logs}) {
      print "My file is $file  \n";
	if (open(FH,$file)) {
            print "Checking logs $file \n";
	    $logs->{$file}->{status} = 1;
	    tail_log($file,\*FH);
	    close(FH);
	}
	else {
	    if ($logs->{$file}->{status}) {
#		raise_error($file,1,"can't open file, $!");
		unshift(@{$hist},
			{
			    tstamp => tstamp(),
			    file => $file,
			    pos => '',
			    errmsg => "can't open file, $!",
			    action => 'suspended',
			    colour => 'maroon',
			    line => '',
			});
		$#{$hist} = $conf->{history};
		$logs->{$file}->{status} = 0;
	    }
	}
    }
    write_html();
    sleep($conf->{interval});
}

print STDOUT "\n\ndone monitoring.\n\n";

unshift(@{$hist},
	{
	    tstamp => tstamp(),
	    file => '',
	    pos => '',
	    errmsg => "information",
	    action => 'stopped',
	    colour => 'darkslategray3',
	    line => '',
	});
$#{$hist} = $conf->{history};
write_html();

exit 0;




sub conf($ ) {
    my $file = shift;
    my %conf;
    my %dffn = (
		conf => \&conf_conf,
		file => \&conf_file,
		type => \&conf_type,
		);
    my $line;
    my $tokn;
    my @key = ();
    my @val = ();

    open(CONF,"$file") || return 0;

    while (defined($line = <CONF>)) {
	next if ($line =~ /^\s*($|\#)/o);	# discard null and comment lines
	while ($line =~ /\G\s*(
			       ("(?:(?:\\")|[^"])*")
			       |(<(?:(?:\\[<>])|[^<>])*>)
			       |[\w+-.\@\/]+
			       |[\{\}]
			       |(?:\#.*$ )
			       )/ogx) {
	    $tokn = $1;
	    last if ($tokn =~ /^\#/o);		# discard comments

	    if ($tokn eq '{') {push(@key,shift(@val))}
	    elsif ($tokn eq '}') {
		if ($#key) {&{$dffn{$key[0]}}(pop(@key),\@val,\%conf)}
		else {@key = ()}
		@val = ();
	    }
	    else {push(@val,$tokn)}
	}
    }

    close(CONF);

    return \%conf;
}




sub conf_conf($ ) {
    my $key = shift;
    my $val = shift;
    my $cnf = shift;

    if ($#{$val} % 2) {
	my($ckey,%cval);
	while ($ckey = shift(@{$val})) {$cval{$ckey} = shift(@{$val})}
	for $ckey (keys %cval) {
	    $cval{$ckey} =~ s/^\"(.*)\"$/$1/o;
	    $cnf->{conf}->{$key}->{$ckey} = $cval{$ckey};
	}
    }
    else {print STDERR "corrupt configuration at conf $key\n"}

    return 0;
}




sub conf_file($ ) {
    my $key = shift;
    my $val = shift;
    my $cnf = shift;

    for (@{$val}) {
	s/^\"(.*)\"$/$1/o;
	if (/^\<(.*)\>$/o) {
	    if (open(FILE,$1)) {
		my $file;
		while ($file = <FILE>) {
		    chomp($file);
		    $cnf->{logs}->{$file} = {type => $key}}
	    }
	    else {print STDERR "can't open configuration file $1, $!\n"}
	}
	else {$cnf->{logs}->{$_} = {type => $key}}
    }

    return 0;
}




sub conf_type($ ) {
    my $key = shift;
    my $val = shift;
    my $cnf = shift;

    if ($#{$val} % 2) {
	my($tkey,%tval);
	while ($tkey = shift(@{$val})) {$tval{$tkey} = shift(@{$val})}
	if (defined($tval{regexp})) {
	    $tval{regexp} =~ s/^\"(.*)\"$/$1/o;
	    for $tkey (keys %tval) {
		next if ($tkey eq 'regexp');
		$tval{$tkey} =~ s/^\"(.*)\"$/$1/o;
		$cnf->{type}->{$key}->{$tval{regexp}}->{$tkey} = $tval{$tkey};
	    }
	}
	else {print STDERR "no regexp defined in config for type $key\n"}
    }
    else {print STDERR "corrupt configuration at type $key\n"}

    return 0;
}





sub init_rgb() {
    my($r,$g,$b,$n);

    if (open(RGB,$conf->{rgb_file})) {
	while(<RGB>) {
	    if (($r,$g,$b,$n) = /^\s*(\d+)\s*(\d+)\s*(\d+)\s*(.*)$/o) {
		$n =~ tr/A-Z/a-z/;
		$conf->{colour}->{$n} = d2h($r).d2h($g).d2h($b);
	    }
	}
	close(RGB);
    }
    else {
	print STDERR "rgb file not found; $conf->{rgb_file}, $!\n";
	$conf->{colour}->{black} = '000000';
	$conf->{colour}->{gray} = 'cccccc';
	$conf->{colour}->{white} = 'ffffff';
	$conf->{colour}->{red} = 'ff0000';
	$conf->{colour}->{blue} = '0000ff';
	$conf->{colour}->{green} = '00ff00';
	$conf->{colour}->{yellow} = 'ffff00';
	$conf->{colour}->{purple} = 'ffff00';
	$conf->{colour}->{brown} = '808000';
    }

    return 0;
}




sub d2h($ ) {return unpack("H2",chr($_[0]))}




sub init_logs($ ) {
    my $tail = shift || 0;
    my $stat = 1;

    for (keys %{$logs}) {
	my @stat = stat($_);
	if (defined($stat[1])) {
	    $logs->{$_}->{pos} = ($stat[7] * $tail);
	    $logs->{$_}->{status} = $stat;
	    $logs->{$_}->{inode} = $stat[1];
	    $logs->{$_}->{size} = $stat[7];
	}
	else {
	    $logs->{$_}->{pos} = 0;
	    $logs->{$_}->{inode} = 0;
	    $logs->{$_}->{status} = 0;
	    $logs->{$_}->{size} = 0;
	}
    }

#    $conf->{hreflink} = $conf->{htmlfile};
#    $conf->{hreflink} =~ s/[^\/]//og;
#    $conf->{hreflink} = '..'.'/..'x(length($conf->{hreflink})-2);
#    print STDOUT $conf->{hreflink}."\n";
    $conf->{hreflink} = '';

    return scalar(keys %{$logs});
}





sub tail_log($ ) {
    my $file = shift;
    my $fh = shift;
    my @stat = stat($file);
    my $line;
    my $rgxp;

    $fh = wraphandle($fh);

    if (!defined($stat[1]) ||				# if no stat
	($stat[1] != $logs->{$file}->{inode}) ||	# or new inode
	($stat[7] < $logs->{$file}->{size})) {		# or smaller size
	$logs->{$file}->{pos} = 0;			# reset file
	$logs->{$file}->{inode} = $stat[1];		# and inode
    }
    else {						# else
	if ($logs->{$file}->{pos}) {			# if inside file
	    $fh->seek(($logs->{$file}->{pos} - 1),0);	# back up
	    $fh->read($_,1);				# read one char
	    if (!/\n/o) {				# if not newline
		$logs->{$file}->{pos} = 0;		# reset file
	    }
	}
    }
    $logs->{$file}->{size} = $stat[7];			# remember current size

    $fh->seek($logs->{$file}->{pos},0);			# position ourselves
    while ($line = $fh->getline()) {
	chomp($line);
	for $rgxp (keys %{$type->{$logs->{$file}->{type}}}) {
	    if ($line =~ /$rgxp/i) {
		raise_event($file,$rgxp,$line);
	    }
	}
	$logs->{$file}->{pos} = $fh->tell();
#	print STDOUT "$file: $.\n";
    }


							# race condition?
							# just remember the
							# new size for now
    if ($logs->{$file}->{pos} > $logs->{$file}->{size}) {
	$logs->{$file}->{size} = $logs->{$file}->{pos};
    }

    return 0;
}




sub raise_event(@ ) {
    my $file = shift;
    my $rgxp = shift;
    my $line = shift;

    my $errm = $type->{$logs->{$file}->{type}}->{$rgxp}->{errmsg};
    my $actn = $type->{$logs->{$file}->{type}}->{$rgxp}->{action};
    my $stat = 0;

    #--- perform action first, set $stat
    #--- append state to action string in hist hash
    if ($actn =~ /^(\w+)\s+(.*)$/o) {
	if ($1 eq 'echo') {
	    #--- no action
	}
	elsif ($1 eq 'mail') {
	    if ($2) {
		my $smtp = Net::SMTP->new('mailhost');
		my $rc = $smtp->mail($ENV{USER}.'@mailhost');
		$rc &= $smtp->to($2);
		$rc &= $smtp->
		    data(("To: $2\n",
			  "Subject: logmon: $errm\n",
			  "\n",
			  "error encountered at ".tstamp()."\n",
			  "\n",
			  "   file: $file\n",
			  "   errm: $errm\n",
			  "   line: $line\n",
			  ));
		$rc &= $smtp->quit();
		if ($rc) {$actn .= ' - sent'}
		else {$actn .= ' - failed'}
	    }
	    else {$actn .= ' - no address'}
	}
	elsif ($1 eq 'call') {
	    if ($2) {
		my $rc = system($2);
		if ($rc) {$actn .= ' - failed: $rc'}
		else {$actn .= ' - ok'}
	    }
	}
	else {
	    $actn =  'unknown: '.$actn;
	}
    }

    unshift(@{$hist},
	    {
		tstamp => tstamp(),
		file => $file,
		pos => $logs->{$file}->{pos},
		errmsg => $errm,
		action => $actn,
	        anchor => $type->{$logs->{$file}->{type}}->{$rgxp}->{anchor},
	        colour => $type->{$logs->{$file}->{type}}->{$rgxp}->{colour},
		line => $line,
	    });
    $#{$hist} = ($conf->{history} - 1);

    return $#{$hist};
}




sub tstamp() {
    my @stamp = (localtime);
    splice(@stamp,6); $stamp[5] += 1900; $stamp[4]++;
    return sprintf("%d/%02d/%02d %02d:%02d:%02d",reverse(@stamp));
}





sub write_html() {
    my $hsth = '';

    unlink($conf->{tempfile});
    if (open(HTML,">".$conf->{tempfile})) {
	write_html_header();

	for $hsth (@{$hist}) {
	    last unless ref($hsth);
	    write_html_item($hsth);
	}

	write_html_footer();
	close(HTML);
    }
    else {die "can't write htmlfile, $!\n"}

    (-e $conf->{origfile}) &&
	(unlink($conf->{origfile}) ||
	 die "can't remove old backup of htmlfile, $!\n");
    if (link($conf->{htmlfile},$conf->{origfile})) {
	unlink($conf->{htmlfile}) || die "can't remove old htmlfile, $!\n";
	if (link($conf->{tempfile},$conf->{htmlfile})) {
	    unlink($conf->{origfile});
	    unlink($conf->{tempfile});
	}
	else {
	    die "couldn't create new htmlfile, nor restore old, $!\n"
		unless (link($conf->{origfile},$conf->{htmlfile}));
	    die "couldn't create new htmlfile, $!\n";
	}
    }
    else {die "can't backup old htmlfile, $!\n"}

    return 0;
}





sub write_html_header() {
    my $fnts = $conf->{font_start};
    my $fnte = $conf->{font_end};
    my $colr = $conf->{colour}->{seashell3};
    my $gray = $conf->{colour}->{gray};
    my $time = tstamp();

    print HTML <<"EOF"; #------------------------------- here doc; header starts
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>
<HEAD>
<TITLE>result</TITLE>
</HEAD>

<BODY BGCOLOR="#ffffff">

<TABLE BORDER=0 CELLPADDING=3 CELLSPACING=1>
   <TR>
      <TD BGCOLOR="#$colr" ALIGN=right>${fnts}updated${fnte}</TD>
      <TD BGCOLOR="#$gray" COLSPAN=2>${fnts}$time${fnte}</TD>
      <TD BGCOLOR="#$colr" ALIGN=right>${fnts}interval${fnte}</TD>
      <TD BGCOLOR="#$gray">${fnts}$conf->{interval}${fnte}</TD>
   </TR>
   <TR>
      <TD BGCOLOR="#$colr">${fnts}timestamp${fnte}</TD>
      <TD BGCOLOR="#$colr">${fnts}logfile${fnte}</TD>
      <TD BGCOLOR="#$colr">${fnts}pos${fnte}</TD>
      <TD BGCOLOR="#$colr">${fnts}error${fnte}</TD>
      <TD BGCOLOR="#$colr">${fnts}action${fnte}</TD>
   </TR>
<!-- imagine log monitor: automated content starts here -->
EOF
    #----------------------------------------------------- here doc; header ends

    return 0;
}




sub write_html_item($ ) {
    my $hsth = shift;
    my $fnts = $conf->{font_start};
    my $fnte = $conf->{font_end};
    my $colr = $conf->{colour}->{$hsth->{colour}};
    my $gray = $conf->{colour}->{gray};

    print HTML <<"EOF"; #----------------------------- here doc; log item starts
   <TR>
      <TD BGCOLOR=#$gray>${fnts}$hsth->{tstamp}${fnte}</TD>
      <TD BGCOLOR=#$gray>${fnts}<A HREF="$conf->{hreflink}$hsth->{file}">$hsth->{file}</A>${fnte}</TD>
      <TD BGCOLOR=#$gray>${fnts}$hsth->{pos}${fnte}</TD>
      <TD BGCOLOR=#$colr>${fnts}$hsth->{errmsg}${fnte}</TD>
      <TD BGCOLOR=#$gray>${fnts}$hsth->{action}${fnte}</TD>
   </TR>
EOF
    #--------------------------------------------------- here doc; log item ends
    if ($hsth->{line}) {
	print HTML <<"EOF"; #--------------------- here doc; logfile line starts
   <TR>
      <TD ALIGN=right>${fnts}${fnte}</TD>
      <TD COLSPAN=4>${fnts}$hsth->{line}${fnte}</TD>
   </TR>
EOF
    }

    return 0;
}




sub write_html_footer() {
    my $fnts = $conf->{font_start};
    my $fnte = $conf->{font_end};

    print HTML <<"EOF"; #------------------------------- here doc; footer starts
<!-- imagine log monitor: automated content stops here -->
</TABLE>

</BODY>
</HTML>
EOF
    #----------------------------------------------------- here doc; footer ends

    return 0;
}





sub raise_error($ ) {
    my $fil = shift;
    my $err = shift || 0;
    my $msg = shift || '';

    if ($err) {
	print STDERR "[$fil]  $err: $msg\n";
    }

    return $err;
}

