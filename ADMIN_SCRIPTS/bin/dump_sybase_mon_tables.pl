: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
use Do_Time;
use CommonFunc;

use vars qw( $USER $SERVER $PASSWORD $OUTDIR $DEBUG);

$| =1;

die ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"USER=s"	=> \$USER,
		"PASSWORD=s" 	=> \$PASSWORD,
		"OUTDIR=s" 	=> \$OUTDIR,
		"DEBUG"      	=> \$DEBUG );

die ("Must pass server\n" ) 		unless defined $SERVER;
die ("Must pass username\n" )	  	unless defined $USER;
die ("Must pass password\n" )	  	unless defined $PASSWORD;
die ("Must pass out directory\n" ) 	unless defined $OUTDIR;
die ("Must $OUTDIR not a directory\n" ) unless -d $OUTDIR;

my(%queries);
#-- RESULT #1
$queries{OLDEST_TRAN} = 'declare @spid int, @dbid int, @dbname varchar(30), @sttime datetime
select @sttime=min(starttime) from master..syslogshold
--select * from master..syslogshold where starttime=@sttime
select @spid=spid, @dbname=db_name(dbid), @dbid=dbid from master..syslogshold where starttime=@sttime
select "Earliest spid"=@spid, dbid=@dbid, DB=@dbname, Time=@sttime, CurTime=getdate()';
$queries{SYSLOGSHOLD} = 'select * from master..syslogshold';
#-- get the wait events
#
#-- current process table
#--select "Wait Class"=c.Description,"Wait Reason"=i.Description,p.*
#--from  master..monProcess p, master..monWaitEventInfo i, master..monWaitClassInfo c
#--where p.WaitEventID = i.WaitEventID
#--and   i.WaitClassID = c.WaitClassID
#
#-- RESULT #2
$queries{WAITS_BY_SPID}='select "Wait Class"=c.Description,"Wait Reason"=i.Description,p.*,l.*
from  master..monProcess p, master..monWaitEventInfo i, master..monWaitClassInfo c,
		master..monProcessLookup l
where p.WaitEventID = i.WaitEventID
and   i.WaitClassID = c.WaitClassID
and   p.SPID=l.SPID
and   p.KPID=l.KPID
order by p.SPID,p.KPID';
#
#-- RESULT #3 currently executing sql info
$queries{RUNNING_SQL}='select * from master..monProcessSQLText order by SPID';

#-- RESULT #4  recent sql information
$queries{RECENT_SQL}='select * from master..monSysSQLText
order by SPID,BatchID,SequenceInBatch';
#
#-- RESULT #5
$queries{WAIT_EVENTS}='select * from master..monProcessWaits w,master..monWaitEventInfo i
where w.WaitEventID= i.WaitEventID
        and     w.WaitTime>1000
        and   i.WaitEventID!=250
order by w.SPID,w.KPID';
#
#-- RESULT #6 Stack Trace
$queries{PROCEDURES}='select * from master..monProcessProcedures';
#
#-- RESULT #7 OPEN OBJECTS
$queries{OPEN_OBJECTS}='select * from   master..monProcessObject order by SPID,KPID';
#
#-- RESULT #8
$queries{PLAN_TEXT}='select * from master..monSysPlanText order by SPID,KPID,BatchID,ContextID,SequenceNumber';
#
#-- RESULT #9
$queries{ERROR_LOG}='select * from monErrorLog order by Time,SPID,KPID';
#
#-- RESULT #10
$queries{LOCKS}='select db=db_name(DBID),obj=object_name(ObjectID,DBID),* from monLocks  order by SPID,KPID';
#
#--  RESULT #11
$queries{ENGINE_ACTIVITY}='select * from monEngine';
#
#--  RESULT #12
$queries{DEADLOCKS}='select * from monDeadLock';

$queries{SP_LOCK}='exec sp__lock @dont_format=\'Y\'';
$queries{SP_WHODO}='exec sp__whodo';
$queries{SP_WHO}='exec sp__who';

$queries{monProcess}='select * from monProcess';
$queries{monProcessActivity}='select * from monProcessActivity';
$queries{monProcessLookup}='select * from monProcessLookup';
$queries{monProcessNetIO}='select * from monProcessNetIO';
$queries{monProcessStatement}='select * from monProcessStatement';
$queries{monProcessWorkerThread}='select * from monProcessWorkerThread';

my($TYPE)="Sybase";
$TYPE="ODBC" if is_nt();

my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD, -type=>$TYPE);
if( ! $rc ) {
	die "OOPS - CANT CONNECT TO SERVER $SERVER as use $USER\n";
}

my($tstamp)=do_time(-fmt=>'yyyymmdd.hhmi');
# die "TSTAMP=$tstamp\n";

foreach my $q (keys %queries) {
	print "Creating $OUTDIR/$q.$tstamp.csv\n";
	open(OUT,"> $OUTDIR/$q.$tstamp.csv") or die "Cant write $OUTDIR/$q.$tstamp.csv\n";
	print OUT $q,"\n";
	my(@rc)= dbi_query(-db=>"master", -query=>$queries{$q}, -print_hdr=>1, -debug=>$DEBUG);
	@rc=dbi_reformat_results(@rc);
	foreach (@rc) {
		s/\n/ /g;
		print OUT $_,"\n";
	}

#	foreach( dbi_query(-db=>"master", -query=>$queries{$q}, -print_hdr=>1, -debug=>$DEBUG)) {
#		my @dat=dbi_decode_row($_);
#
#		my($string)=join("\|\|",@dat);
#		$string=~s/\n/ /g;
#		$string=~s/\t/ /g;
#		$string=~s/\s\s+/ /g;
#		$string=~s/\|\|/\t/g;
#		print OUT $string,"\n";
#	}
	close(OUT);
}
print "Process Completed\n";

__END__

=head1 NAME

dump_sybase_mon_tables.pl - dump sybase monitoring tables out to .csv files for investigation

=head2 USAGE

	dump_sybase_mon_tables.pl -USER=USER -SERVER=SERVER -PASSWORD=PASS -OUTDIR=dir

=head2 DESCRIPTION

Creates about 10 files in .csv format that can be used to interogate server state in the event of
an emergency.  Basically it creates a set of .csv files that you can browse (with excel ) to see what
was going on at the time of the “incident”.  This includes very detailed process information – like showplans and
the sql for all running processes.  It creates a lot of output that needs to be pieced together – but when
something weird happens and you have no time for diagnostics because you need to fix production, this is the
program to run (quickly) – before you fix the problem.  Takes about 10 seconds to run.

The program requires MDA tables to be set up and should be tested PRIOR to trying it in an emergency.  Run it on
a production server tho or you will have limited results.

=cut
