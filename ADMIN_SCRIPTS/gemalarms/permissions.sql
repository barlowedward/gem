if not exists (select * from sysusers where name='public' )
	exec sp_adduser 'public'
go
grant insert on Event to public
go
grant insert,select,delete on BackupJobHistory to public
grant update,insert,select,delete on BackupJobStep to public
go
grant update on Event to public
go
grant delete on Event to public
go
-- grant select on AgentHistory  to public
-- go
-- grant insert on AgentHistory  to public
-- go
-- grant update on AgentHistory  to public
-- go
-- grant delete on AgentHistory  to public
-- go
grant insert on Heartbeat to public
go
grant update on Heartbeat to public
go
grant delete on Heartbeat to public
go
grant insert on HeartbeatThresh to public
go
grant update on HeartbeatThresh to public
go
grant delete on HeartbeatThresh to public
go
grant select on HeartbeatThresh to public
go
grant select on Severity to public
go
grant select on SentNotification to public
go
grant delete on SentNotification to public
go
--grant select on ContainerMap to public
--go
--grant insert on ContainerMap to public
--go
--grant update on ContainerMap to public
--go
--grant delete on ContainerMap to public
--go
grant select on Notification to public
go
grant insert on Notification to public
go
grant delete on Notification to public
go
grant update on Notification to public
go
grant select on Operator to public
go
grant insert on Operator to public
go
grant delete on Operator to public
go
grant update on Operator to public
go
--grant select on ContainerOverride to public
--go
--grant insert on ContainerOverride to public
--go
--grant delete on ContainerOverride to public
--go
--grant update on ContainerOverride to public
--go
--grant select on ProductionServers to public
--go
--grant insert on ProductionServers to public
--go
--grant delete on ProductionServers to public
--go
--grant update on ProductionServers to public
--go
grant select on ReportArgs to public
go
grant insert on ReportArgs to public
go
grant delete on ReportArgs to public
go
grant update on ReportArgs to public
go
-- grant select on ProgramLookup to public
go
-- grant insert on ProgramLookup to public
go
-- grant delete on ProgramLookup to public
go
-- grant update on ProgramLookup to public
go
grant execute on Heartbeat_proc  to public
go
-- grant execute on Performance_proc  to public
go
grant execute on Event_proc  to public
go
--grant execute on build_container to public
--go
-- grant select on MessageDef to public
go
-- grant select on MessageLookup  to public
go
-- grant select on ValueConversion  to public
go
grant select on Event  to public
go
grant select on Heartbeat  to public
go
-- grant select on PerformanceDefn  to public
go
-- grant select on Performance  to public
go
-- grant select on PerformanceDesc  to public
--go
--grant select on Container  to public
--go
--grant select on Container_full  to public
go
grant select on Screen  to public
go
grant select,insert on SentNotification  to public
go
grant all on IgnoreList  to public
go
grant all on BackupState  to public
go
grant all on ServerInfo to public
go
grant all on ServerChanges to public
go
grant select on Severity  to public
go
grant execute on get_possible_alarms to public
go
grant execute on get_alarm_version to public
go
grant execute on clear_possible_alarms to public
go
grant execute on Backup_proc to public
go
update Notification set use_blackberry='NO' where use_blackberry is null
go

grant all on Event_Filter to public
go
grant all on VE_ALARMABLE to public
go
grant all on VH_ALARMABLE to public
go
grant all on VH_NOTALARMABLE to public
go

if not exists ( select * from INV_cutoffdates	where cutoffkey='SAS70 Cutoff Date' )
begin
	insert INV_cutoffdates values ( 'SAS70 Cutoff Date', getdate() )
end
go
