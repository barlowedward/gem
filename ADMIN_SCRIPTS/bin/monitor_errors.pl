#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2003 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Long;
# use File::Copy;
use CommonFunc;
use Time::Local;

use vars qw($INITTIME);
die "usage: $0 [--INITTIME time] "
		unless GetOptions( "INITTIME=s"=>\$INITTIME );

$|=1;

my($cwd)=cd_home();

# Read The ConfigFile
die "Config File monitoring.cfg not found" unless -r "../monitoring.cfg";
my(@Pat_Server,@Pat_Error,@Pat_Monitor);
open(OUT,"../monitoring.cfg") or die "Cant read monitoring.cfg\n";
while(<OUT>) {
	next if /^\s*#/;
	chop;
	my(@dat)=split /,/;
	push @Pat_Server ,lc($dat[1]);
	push @Pat_Error  ,lc($dat[2]);
	push @Pat_Monitor,lc($dat[3]);
}
close(OUT);

chdir("../monitoring_data");

print "Home Directory is $cwd\n";
opendir(DIR,".")
	|| die("Can't open directory monitoring_data reading\n");
my(@dirlist) = grep(!/^\./,readdir(DIR));
closedir(DIR);
print "Files are ",join(" ",@dirlist),"\n";

my(%sent_alarms);
my(%file_sizes,%file_age);
my($initrun)="FALSE";
while( 1 ) {
	foreach my $file (@dirlist ) {
		my(@statinfo)=stat $file;

		print "date=",$statinfo[9]," size=",$statinfo[7],"\n";

		next if $file_age{$file}   == $statinfo[9]
		    and $file_sizes{$file} == $statinfo[7];

		print "Working On File $file";

		$file_age{$file}   = $statinfo[9];
		$file_sizes{$file} = $statinfo[7];

		open(OUT,$file) or die "Cant read $file\n";
		while(<OUT>) {
			chop;
			next if $sent_alarms{$_};
			my(@dat)=split /,/;
			# scan the line and print it if it matches
			for ( my($i)=0; $i<=$#Pat_Server; $i++ ) {
				next if $Pat_Server[$i] ne "*"
					and lc( $dat[1] ) ne $Pat_Server[$i];
				next if $Pat_Error[$i] ne "*"
					and lc( $dat[2] ) ne $Pat_Error[$i];
				next if $Pat_Monitor[$i] ne "*"
					and lc( $dat[3] ) ne $Pat_Monitor[$i];
				$sent_alarms{$_}=1;
				if( $initrun eq "FALSE" ) {
					my(@dates)=split(" ",$dat[0]);
					# format 0:Tue 1:Nov 2:11 3:hh:mi:ss 4:yyyy
					my($hr,$mi,$ss)=split(/:/,$dates[3]);
					next if length($dates[4]) != 4;
					my($msgtime)=timelocal($ss,$mi,$hr,$dates[2],$mon,$dates[4]);
					if( defined $INITTIME ) {
						handle_error(@dat) if time - $msgtime < $INITTIME;
					} else {
						handle_error(@dat) if time - $msgtime < 60*60;
					}
				} else {
					handle_error(@dat);
				}
				last;
			}
		}
		close(OUT);
	}
	sleep(60);
}

sub handle_error {
	my(@dat)=@_;
	print join(",",@dat),"\n";
}

__END__

=head1 NAME

monitor_errors.pl - read files in monitor_data and compares to config file

=head2 DESCRIPTION

This program reads files from monitoring_data that have .err extesnsions and it will parse the standard format for specific types of errors.  These errors will then be handled.

=cut

