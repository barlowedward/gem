-- report_key=TRANDUMPTIMES
-- report_type=HTML_AGGREGATE
-- run_frequency=daily
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=This Report Lists 6 Days Of Database Tran Log Backup Information Aggregated
-- report_footer=
-- enabled=Y
-- outfile=TranTimes_History

SELECT 
	 convert(varchar,backup_start_date,112) as [Day],
    datepart(hour,backup_start_date) as [Hour],
    convert(int,sum(backup_size)/(1024*1024)) as [Mega Bytes],
    sum(datediff(ss,backup_start_date,backup_finish_date)) as [Total Seconds]
FROM msdb.dbo.backupset
where type='L'
and datediff(dd,backup_finish_date,getdate()) < 7
group by 
	convert(varchar,backup_start_date,112),
    datepart(hour,backup_start_date)    
order by Day desc,Hour desc