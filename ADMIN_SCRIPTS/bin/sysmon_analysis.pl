#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# ==========================
# MAIN SP_SYSMON ANALYZER
#    uses results from sp_configure, sp__audit* (extended stored lib), and
#    sp_sysmon.  See file README.
# ==========================

require	5.002;
use     	Sybase::DBlib;
use     	strict;
use     	Sys::Hostname;
use     	Getopt::Std;
use     	File::Basename;
require  "sybutil.pl";

# BEGIN SECTION OF DEFINITIONS REQUIRED FOR sysmon_func.pl
#
# A section is a group of things on a page
# An Item is a subheading - indented 1 or 2 spaces
#
my( %section_count );	# section count is num rows per section
my( %item_count );	   # item count is num rows per section:item
my( %item_data );	     	# item data is data for section:item
my( %item_keys );	     	# keys are keys to item_data for section:item
my($section_name, $item_name, $last_subitem, $section_line,$section_num)=("","",0,1,0);
my(@rawsysmon);
my($rundate,$runtime,$interval,$srvversion,$srvname);
use vars qw($opt_d  );

#
# END SECTION OF DEFINITIONS REQUIRED FOR sysmon_func.pl
#

my($curdir)=dirname($0);
require( "$curdir/sysmon_func.pl" );

my($dbproc,$NL,$SYBASE,$USER,$PASSWORD);
my(@dblist);
use vars qw( $DSQUERY  %found_audit @audit_res $hostname @cfg_res $special_item);

my($sep)="~";   # separator of rows from audit queries

select (STDOUT); $| = 1;		# make unbuffered

use vars qw($opt_p $opt_y $opt_S $opt_r $opt_s $opt_U $opt_P $opt_h $opt_i $opt_a $opt_c $opt_t $opt_o $opt_q $opt_x);

sub usage
{
	print @_ if $#_>=0;
   print "sysmon_analysis.pl [-s\$SYBASE] -SDSQUERY -UUSER -PPASSWORD [-ifile -hayc -ttime -ofile]

Analyze the output from sp_sysmon (either from a file or from server).
If you have extracted output to a file, simply use the -i option, otherwise
sp_sysmon will be run if you pass -x.

-p      : only print database
-d      : debug mode
-i file : use file name as sysmon input instead of running directly (sets -y)
-x      : run sp_sysmon
-y      : analyze sp_symon
-a      : run audit analyzer
-c      : run sysconfigure monitor
-r      : print raw sysmon output too (defined automatically if -o used)
-h      : html'ize output for a web browser to view
-o file : output file for raw sysmon output
-q      : quiet mode - print little to screen
-t time : time is the time interval for sp_sysmon (default = 10 minutes)

\n";

	exit();
};

usage() if $#ARGV<0;
usage( "Incorrect Usage" ) unless getopts('pdrt:acS:s:U:P:hi:yo:qx');
$opt_t = 10 unless defined $opt_t;
$opt_r = 1 if defined $opt_o;

s_debugmsg( "DEBUG MODE" );

# if -y must have -x or -i
usage( "-y defined but not -x or -i\n" )
	if defined $opt_y and ( ! defined $opt_x and ! defined $opt_i );
undef $opt_x if defined $opt_i;
$opt_y=1     if defined $opt_i;

$SYBASE    = $ENV{"SYBASE"};
$DSQUERY   = $ENV{"DSQUERY"};
$USER      = $ENV{"USER"};
$PASSWORD  = $ENV{"PASSWORD"};

$SYBASE		 = $opt_s if defined $opt_s;
$ENV{"SYBASE"} = $opt_s if defined $opt_s;
$DSQUERY	= $opt_S if defined $opt_S;
$USER	   = $opt_U if defined $opt_U;
$PASSWORD       = $opt_P if defined $opt_P;

$NL = "\n" unless defined $opt_h;
$NL = "<BR>\n" if defined $opt_h;

s_debugmsg( "initializing... " );
initialize("Sybase System Analyzer");

usage( "SYBASE UNDEFINED" )     unless defined $SYBASE ;

$hostname = hostname();

# NO NEED TO CONNECT IF JUST -y and a file was passed
if( $opt_a or $opt_c or ! defined $opt_i ) {
	s_debugmsg( "connecting to sybase... " );
	usage( "SERVER UNDEFINED" )     unless defined $DSQUERY;
	usage( "USER UNDEFINED" )	       unless defined $USER;
	usage( "PASSWORD UNDEFINED" )   unless defined $PASSWORD;
	s_debugmsg("Connecting To $DSQUERY as $USER");
	($dbproc = new Sybase::DBlib $USER, $PASSWORD, $DSQUERY) or
	s_quit("Can't connect to $DSQUERY as login $USER.\n");
	$dbproc->dbuse("master") or s_quit("Cant Use Master DB");
}

# check if system 11
if( defined $opt_x ) {
	s_debugmsg( "checking sybase version 11... " );
	$dbproc->dbcmd("select 1 from sybsystemprocs..sysobjects where name='sp_sysmon'");
	$dbproc->dbsucceed(1);
	my($is_sys_11)=0;
	while ($dbproc->dbnextrow) {  $is_sys_11++; }

	if( $is_sys_11==0 ) {
		s_quit( "Error - cant use -x option to run sp_sysmon.  This server is not System 11 or Later\n" );
	}
}

# AUDITING
if( defined $opt_a ) {
	s_debugmsg( "checking lib version for auditing... " );
	$dbproc->dbcmd("exec sp__proclib_version");
	$dbproc->dbsucceed(1);
	my($proc_ver)=0;
	$proc_ver=$dbproc->dbnextrow();

	if( $proc_ver<4.4 ) {
		s_quit( "Error - Must Use Extended Stored Procedure Library Version 4.4 or later.  Found version $proc_ver. The latest can be downloaded from http://www.edbarlow.com.\n" );
	}
}

# SHOW CONFIGURATION
if( defined $opt_c ) {
	s_debugmsg( "getting config info... " );

	$dbproc->dbuse("master") or s_quit("Cant Use Master DB");

	my($query)="select * from syscolumns where id=object_id('syscurconfigs') and name='defvalue'";
	$dbproc->dbcmd($query);
	$dbproc->dbsucceed(1);
	my($is_sys_11)=0;
	my(@rc);
	while (@rc=$dbproc->dbnextrow) {
		$is_sys_11++;
	}

	if( $is_sys_11==0 ) {
		s_quit( "Error - Cant Use -c option to test configuration on this system - sybase not at appropriate version\n" );
	}
}

s_run_audit()	   if defined $opt_a;
s_check_configures()    if defined $opt_c;

if( defined $opt_x or defined $opt_y ) {
	s_debugmsg( "sysmon analysis... " );
   dbmsghandle ("s_msg_hdlr");
   dberrhandle ("s_err_hdlr");
   s_do_a_sysmon();

	# print raw output
	if( defined $opt_o ) {
		statusmsg( "writing output to $opt_o" );
		open(OUTFILE,"> ".$opt_o) or s_quit( "Unable to open $opt_o: $!" );
		foreach( @rawsysmon ) { print OUTFILE "$_\n"; }
		close OUTFILE;
	}
}

section_hdr(1,"Audit Results")   if defined $opt_a;
s_print_audit()			 if defined $opt_a;

section_hdr(1,"Changed sp_configure Values") if defined $opt_c;
s_print_configures()			 if defined $opt_c;

if( ! defined $opt_y ) {
   print "Done Analysis<BR>\n</BODY></HTML>\n";
	exit();
}

# IF YOU ARE HERE U ARE ANALYZING SYSMON OUTPUT

# the next step does the work for sp_sysmon
statusmsg( "ANALYZING SYSMON OUTPUT" );
print "DATA HAS",keys %item_data," items\n";
require( "$curdir/sysmon_rules.pl" );
print "DATA HAS",keys %item_data," items\n";

# print raw unformatted sysmon output
if( defined $opt_r ) {
	section_hdr(1,"Raw sp_sysmon output");
	print "<PRE>\n" if defined $opt_h;
	my($opt_h_sv)=$opt_h;
	undef $opt_h;
	$NL="\n";
	foreach( @rawsysmon ) { msgln($_); }
	$opt_h=$opt_h_sv;
	print "</PRE>\n" if defined $opt_h;
	$NL = "<BR>\n" if defined $opt_h;
}

print "Done Analysis<BR>\n</BODY></HTML>\n";
exit();

############################# SUBROUTINE SECTION ###########################
sub initialize()
{
   my($title)=@_;
   return unless defined $opt_h;
   print "<HTML>\n<HEAD>\n";
   print "<TITLE>$title</TITLE>\n";
   print "<META NAME='description' CONTENT='Web Server Analyzer Results'>\n"
;
   print "<META NAME='generator' CONTENT='WebMonitor'>\n";
   print "<META NAME='copyright' CONTENT='copyright (c) 1997-8 By SQL Technologies'>\n";
   print "<META NAME='keywords' CONTENT='Sybase, Unix, Performance, Monitoring'>\n";
   print "</HEAD>\n";
}

sub s_print_audit
{
   return unless defined $opt_a;
   statusmsg( "Printing Audit Results..." );
   my(@dat);
   if( defined $opt_h ) {
      my($db);
      foreach $db (@dblist) {
	 print "<h2>Database $db</h2>\n";
			print "no messages found<br>" if defined $found_audit{$db};
			next    if defined $found_audit{$db};
	 print "<TABLE>\n<TR><TH>database</TH><TH>error message</TH></TR>\n";
	 foreach (@audit_res) {
	    @dat = split /$sep/;
	    $dat[6] =~ s/[\r\n\s]+$//;
	    $dat[6] =~ s/\s[\s]+/ /;
	    next unless $dat[3] eq $db;
	    print "<TR><TD>$dat[3]</TD><TD>$dat[6]</TD></TR>\n";
	 }
	 print "</TABLE>\n"
      }
   } else {
      foreach (@audit_res) {
	 @dat = split /$sep/;
	 $dat[6] =~ s/[\r\n\s]+$//;
	 $dat[6] =~ s/\s[\s]+/ /;
	 my($str)=sprintf("%-20.20s %s",$dat[3],$dat[6]);
	 warning( $str );
      }
   }
}

sub s_run_audit
{
   @audit_res=();
   @dblist=();

   my(@data);
   statusmsg( "Executing audit..." );
   $dbproc->dbcmd("select name from master..sysdatabases  where status & 0x1120 = 0 and status2&0x0030=0");
   $dbproc->dbsucceed(1);
   while (@data = $dbproc->dbnextrow) {
      next if $data[0] eq "model" or $data[0] eq "tempdb";
      push(@dblist,@data);
   }

	print "\texecuting sp__auditsecurity $NL";
   $dbproc->dbcmd("exec sp__auditsecurity 1,\"$DSQUERY\", \"$hostname\",\"Y\"");
   $dbproc->dbsqlexec;			     # execute sql
   while($dbproc->dbresults != NO_MORE_RESULTS) {  # copy all results
      while (@data = $dbproc->dbnextrow) {
	 push(@audit_res,join($sep,@data));
      }
   }

   foreach (@dblist) {
		print "\texecuting sp__auditdb in $_ $NL";
      my($db)=$_;
      my($rc)=1;
      $rc=$dbproc->dbuse($db);
      next if $rc==0;
      $dbproc->dbcmd("exec sp__auditdb \"$DSQUERY\", \"$hostname\"");
      $dbproc->dbsqlexec;
      while($dbproc->dbresults != NO_MORE_RESULTS) {
	 while (@data = $dbproc->dbnextrow) {
				$found_audit{$db}=1;
	    push(@audit_res,join($sep,@data));
	 }
      }
   }
}

sub s_check_configures
{
   s_debugmsg( "Checking Configuration..." );
   @cfg_res=();

   my(@data);
   my($msg)="select c.config,
	   cmt=substring(x.comment,1,40),
	   c.value,
	   dflt=substring(c.defvalue,1,10)
   from    master..syscurconfigs c,master..sysconfigures x
   where   c.value2 != c.defvalue
   and     c.config = x.config
   and     c.config != 122
\n";

   $dbproc->dbcmd($msg);
   $dbproc->dbsucceed(1);
   while (@data = $dbproc->dbnextrow()) {
       #print ">",@data,"\n";
       push(@cfg_res,join($sep,@data));
   }
}

sub s_print_configures
{
   return unless defined $opt_c;
   s_debugmsg( "Printing Configure Results..." );
   my(@dat);
   if( defined $opt_h ) {
      print "<TABLE><TR><TH>config value<TH>value<TH>default</TR>\n";
      foreach (@cfg_res) {
	 @dat = split /$sep/;
	 print "<TR><TD>$dat[1]</TD><TD>$dat[2]</TD><TD>$dat[3]</TD></TR>\n";
      }
      print "</TABLE>\n"
   } else {
      msgln("config value					   value      default");
      msgln("============					   =====      =======");
      foreach (@cfg_res) {
	 @dat = split /$sep/;
	 my($str)=sprintf("%-45.45s %-10.10s %-10.10s",$dat[1],$dat[2],$dat[3]);
	 warning( $str );
      }
   }
}

# ==================================================================
#
# SYSMON FUNCTIONS
#
# ==================================================================
sub s_do_a_sysmon
{
   if( defined $opt_i ) {
      statusmsg("reading input file $opt_i");
      open(INFILE,$opt_i) or s_quit( "Unable to open $opt_i: $!" );
      while(<INFILE>) { s_read_sysmon_inline($_); }
      close INFILE;
   } else {
		# TEST IF SERVER IS 11.5 or later
		# IF IT IS THEN MAKE DIFFERENT FORMAT
		s_debugmsg( "Testing Server Version (is it 11.5 or later)..." );
		my($query)="select o.name from sybsystemprocs..syscolumns c, sybsystemprocs..sysobjects o where c.id=o.id and o.name=\'sp_sysmon\' and c.colid=1 and c.type=47";
      statusmsg("checking type of sp_sysmon argument");
		s_debugmsg( "Query is: ".$query );
      $dbproc->dbcmd($query);
		$dbproc->dbsqlexec();
      # $dbproc->dbsucceed(1);

		while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
			while ( $dbproc->dbnextrow() ) {
				statusmsg("Character Argument To sp_sysmon Found");
				$opt_t=~s/\s//g;
				substr($opt_t,0,0)='0' if length($opt_t)==1;
				$opt_t="\"00:$opt_t:00\"";
			}
		}

      $dbproc->dbcancel();

      statusmsg("running sp_sysmon $opt_t");
      $query = "exec sp_sysmon $opt_t\n";
      print "Executing ($query)...$NL";
      $dbproc->dbcmd($query);
      $dbproc->dbsucceed(1);
      $dbproc->dbcancel();
   }
}

#######################################################################
#
# SYBASE ERROR HANDLERS
#    we use the default error handler until we need to handle sp_sysmon
#    output, which is in the form of print statements.  At that point we
#    change over and do the following
#
#######################################################################

sub s_msg_hdlr
{
   my ($db, $message, $state, $severity, $text, $server, $procedure, $line) = @_;

   s_debugmsg( "MESSAGE HANDLER CALLED (message num=$message ) $text\n" )
		if $message!=0;

   # use database successful message
   return 0 if( $message==5701 or $message==5703 or $message==5704);

   # login failed message
   return 0 if( $message==4002 );

   # Unable To Use Db
   return 0 if ($message==916);

   # Don't display 'informational' messages:
   if ($severity > 10)
   {
      print "Sybase message ", $message, ", Severity ", $severity,
	  ", state ", $state;
      print "\nServer `", $server, "'" if defined ($server);
      print "\nProcedure `", $procedure, "'" if defined ($procedure);
      print "\nLine ", $line if defined ($line);
      print "\n    ", $text, "\n\n";

      # &dbstrcpy returns the command buffer.

      if(defined($db))
      {
	  my ($lineno, $cmdbuff) = (1, undef);
	  $cmdbuff = &Sybase::DBlib::dbstrcpy($db);
	  foreach (split (/\n/, $cmdbuff)) {
	    print (sprintf ("%5d",$lineno ++),"> ",$_,"\n");
	  }
      }
    } elsif ($message == 0) {
      s_read_sysmon_inline($text);
    }

    0;
}

# ==================================================================
# MAIN SYSMON ERROR HANDLER PROCESSOR
# see documentation at end
# ==================================================================
#sub s_read_sysmon_inline
#{
#   my($msg) = @_;
#	$_ = $msg;
#   chomp;
#
#   push @rawsysmon, $_;				  # save raw input
#   return if /^\s*$/;
#   return if /\-\-\-\-\-\-\-\-\-/;
#
#   s/\s+$//g;
#	s/%//g;
#   s_debugmsg( "\tread: $_" );
#
#   $section_line++;
#
#   if( /\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=/ ) {
#
#		#
#		# FOUND NEW SECTION
#		#
#      s_debugmsg("\tsection divider found" ) if defined $opt_d;
#      $section_num++;
#      $section_name   = "";
#      $section_line   = 0;
#      $item_name   = "";
#      $last_subitem= "";
#
#   } elsif( $section_num <=2 ) {
#
#      s_debugmsg("\tstartup section" ) if defined $opt_d;
#		s/://;
#
#      if( /Run Date/ ) {
#	 		$rundate = $_ ;
#	 		$rundate =~ s/Run Date:*\s*//;
#      }
#
#      if( /Server Version:/ ) {
#	 		$srvversion = $_ ;
#	 		$srvversion =~ s/Server Version:*\s*//;
#      }
#
#      if( /Server Name:/ ) {
#	 		$srvname  = $_ ;
#	 		$srvname =~ s/Server Name:*\s*//;
#      }
#
#      if( /Statistics Sampled at/ ) {
#	 		$runtime  = $_ ;
#	 		$runtime =~ s/Statistics Sampled at:*\s*//;
#      }
#
#      if( /Sample Interval/ ) {
#	 		$interval = $_ ;
#	 		$interval =~ s/Sample Interval:*\s*//;
#	 		$interval =~ s/min.//;
#	 		$interval =~ s/\s//;
#      }
#
#   } elsif( $section_line == 1 ) {
#
#		#
#		# YOU HAVE THE SECTION NAME
#		#
#      # some of the sections have garbage at the end
#      s/ment[\s\w\%]+/ment/;
#		s/^\s+|\(|\)|\s+$//g;
#      $section_name  = $_;
#      s_debugmsg("\tFound section heading => $section_name" )
#			if defined $opt_d;
#      $section_line++;
#      $item_name   = "";
#		$last_subitem= "";
#
#   } elsif( $section_name eq "Housekeeper Task Activity" ) {
#		return;
#
#   } elsif( /^  \w/ or /^ \w/ ) {
#
#		s/\s\s*/ /g;
#
#		#
#		# 1 or 2 spaces - ITEM
#		#
#		my($ItemLineText)=$_;
#
#		# extract first field (words?) which is item name
#
#		s/^\s+|\(|\)|\s+$//g;
#      s/\s*per sec[\W\w]*//;	    # remove stuff from per sec to right
#      s.n/a..g;
#      s/ [\%\.\s\d]+//g;
#      s/:[\s\w]*//g;
#
#      s_quit( "Cant Parse Down $ItemLineText" ) if $_ eq "";
#
#      my($first_field)=$_;
#      $item_name = $first_field;
#
#		# we need to handle special items here...
#		if( $section_name eq "Disk I/O Management" ){
#			return if /Total I\/Os/;
#			if( $first_field  =~ /Device/ ) {
#				$item_name=$first_field;
#				return;
#			}
#		}
#
#		# Ok we have an item name (probably) and maybe some data
#		# WARN OF DUPS HERE
#		if( defined $item_data{$section_name.":".$item_name}
#		and $item_data{$section_name.":".$item_name} ne "") {
#			statusmsg("WARNING DUPLICATE SECTION" );
#			statusmsg("\tName=>$section_name      Item =>$item_name" );
#			#statusmsg("\t\tDATA ".$item_data{$section_name.":".$item_name}."\n" );
#			#statusmsg("\t\tADDING  $ItemLineText\n" );
#
#			$section_count{$section_name}++;
#			$item_count{$section_name.":".$item_name}++;
#			$item_data{$section_name.":".$item_name} .= $ItemLineText."\n";
#			$item_keys{$section_name.":".$item_name} .= $_."\n";
#		} else {
#			$section_count{$section_name}=0
#	    		unless defined $section_count{$section_name};
#			$item_count{$section_name.":".$item_name}=0
#	    		unless defined $item_count{$section_name.":".$item_name};
#			$item_data{$section_name.":".$item_name}=""
#				unless defined $item_data{$section_name.":".$item_name}
#				and $ItemLineText !~ /per sec/;
#
#			$section_count{$section_name}++;
#			$item_count{$section_name.":".$item_name}++;
#			$item_data{$section_name.":".$item_name} .= $ItemLineText."\n"
#				unless $ItemLineText=~/per sec/;
#			$item_keys{$section_name.":".$item_name}="";
#			s_debugmsg("\tnew item |$section_name->$item_name|" );
#			s_debugmsg("\titem data=>".join("|",split(/\n/,$item_data{$section_name.":".$item_name} )));
#		}
#   } else {
#
#		print "DBG: No spaces at start of $_\n" if /^\S/;
#
#		#
#		# NO SPACES AT START OF LINE OR > 2 SPACES
#		#
#		# Note - if no data then dont save item - store it for next time
#		#
#		return if /per sec/;
#		s/\s\s*/ /g;
#
#		if( $item_name eq "Device" ) {
#			$item_name.= ": ".$_;
#			$section_count{$section_name}=0
#	    		unless defined $section_count{$section_name};
#			$item_count{$section_name.":".$item_name}=0
#	    		unless defined $item_count{$section_name.":".$item_name};
#			$item_data{$section_name.":".$item_name}=""
#				unless defined $item_data{$section_name.":".$item_name};
#			return;
#		}
#
#		s/^\s+|\(|\)|\s+$//g;
#		my $data = $_;
#		s/\s*\d*\d\.\d[\w\W]*$//g;
#		s/n\/a[\w\W]*$//g;
#
#		if(/^\s*$/) {
#			# use last subitem if you dont have any text
#			print "DBG: found special case in $last_subitem => $data\n"
#				if defined $opt_d;
#			$data = $last_subitem;
#			$_ = $last_subitem . " ". $_;
#		};
#
#		$last_subitem=$data;
#
#      s_quit( "Cant Save $data - current item name is null\n" )
#			if $item_name eq "";
#      $section_count{$section_name}++;
#      $item_count{$section_name.":".$item_name}++;
#      $item_data{$section_name.":".$item_name} .= $data."\n";
#		$item_keys{$section_name.":".$item_name} .= $_."\n";
#      s_debugmsg("\tappend data for |$section_name->$item_name|" );
#      s_debugmsg("\titem data=>".join("|",split(/\n/,$item_data{$section_name.":".$item_name} )));
#   }
#}

sub s_err_hdlr {
    my ($db, $severity, $error, $os_error, $error_msg, $os_error_msg) = @_;

    my($Sybase_Error) = "";

    s_debugmsg( "SYBASE ERROR HANDLER (error=$error severity=$severity)\n" );

    $Sybase_Error = "ERROR # $error: $error_msg";
    $Sybase_Error .= "\nOS ERROR $os_error: ".$os_error_msg if defined $os_error_msg;

    return INT_CANCEL if $error==5701 or $error==20012 or $error==20014 or $error==20018 or $error==20009;

    print $Sybase_Error,"\n" if $error != SYBESMSG;

    INT_CANCEL;
}

##############################################################################
#  PRINT STATEMENTS
#       print statements are of the following types: debug, error, warning,
#	   advice, section_header, quit, and normal.  Code should not
#	   ever just do a print
##############################################################################

# its a debug message
sub s_debugmsg
{
   my($msg)=join("",@_);
   return unless defined $opt_d;
   if( defined $opt_h ) {
      print "<FONT COLOR=ORANGE>@_</FONT><BR>\n";
      return;
   } else {
      return if defined $opt_q;
      print "debug: $msg\n";
   }
}

sub statusmsg
{
   if( defined $opt_h ) {
      print "<FONT COLOR=BLUE>@_</FONT><BR>\n";
      return;
   }
   print "@_",$NL;
}

my(@rowitem);
my(@rowwidth);
sub table
{
	print "<CENTER><TABLE BORDER=1>\n" if defined $opt_h;
	print "\n" unless defined $opt_h;
	undef @rowitem;
	undef @rowwidth;
}

sub endtable
{
	my($fmt)="";
	foreach (@rowwidth) { $fmt .= "\%".$_."\.".$_."s   "; }
	$fmt.="\n";

	foreach (@rowitem) { printf($fmt,@$_); }

	print "</TABLE></CENTER>\n" if defined $opt_h;
	print "\n" unless defined $opt_h;
}

sub row
{
	if( defined $opt_h ) {
		print "<TR><TD>",join("</TD><TD>",@_),"</TD></TR>\n";
	} else {
		my($count)=0;
		my(@dat)=@_;
		foreach (@dat) {
			s/\n//g;
			s/\s+$//;
			s/^\s+//;
			$rowwidth[$count]       = length() if $rowwidth[$count] < length();
			$count++;
		}
		push @rowitem,\@dat;
	}
}

sub bold
{
   if( defined $opt_h ) {
		return "<b>@_</b>\n";
   }
   return if defined $opt_q;
   return "\t@_";
}

# print a regular message
sub msgln
{
   if( defined $opt_h ) {
      print "<FONT COLOR=BLACK>@_</FONT><BR>\n";
      return;
   }
   return if defined $opt_q;
	s/<.*?>//g;
   print "@_",$NL;
}

# print an error message
sub error
{
   if( defined $opt_h ) {
      print "<FONT COLOR=RED>ERROR: @_</FONT><BR>\n";
      return;
   }
   return if defined $opt_q;
	print "===================================================\n";
   print "ERROR: ";
   print @_;
	print "\n===================================================\n";
}

# print a warning message
sub warning
{
   if( defined $opt_h ) {
      print "<FONT COLOR=DARKRED>WARNING: @_</FONT><BR>\n";
      return;
   }
   return if defined $opt_q;
   print "WARNING: ";
   print  @_;
   print $NL;
}

# print some advice as necessary
sub advice
{
   if( defined $opt_h ) {
      print "<FONT COLOR=OLIVE>ADVICE: @_</FONT><BR>\n";
      return;
   }
   return if defined $opt_q;
   print "ADVICE: ";
   print @_;
   print $NL,$NL;
}

# local quit & die
sub s_quit
{
   if( defined $opt_h ) {
      print "<FONT COLOR=RED>ABORTING: @_</FONT><BR>\n</BODY></HTML>\n";
      exit;
   }
   die @_;
}

# ===========================================================================
#
#		      DATABASE FUNCTIONS
#
# ===========================================================================

sub get_items_in_section
{
   my($section)=@_;
   $section .= ":";
   my(@dat)=();
   foreach ( keys %item_count ) {
      if( /$section/ ) {
	 s/$section//;
	 push @dat,$_;
      }
   }
   return @dat;
}

# The generic method of getting data
#       specify the section, superheading, subkey for line in question
#  if subkey not found, then looks at superheading data only
#  if you have no linetext then the item is the linetext
#
# note section,item,and text all lose () and are rtrim and ltrim
sub Data
{
   my($section,$item,$linetext,$col,$warn)=@_;

	$section=~s/^\s+|\(|\)|\s+$//g;
	$item   =~s/^\s+|\(|\)|\s+$//g;
   $linetext=$item unless defined $linetext;
	$linetext=~s/^\s+|\(|\)|\s+$//g;

   if( ! defined $item_data{$section.":".$item} ) {
      return undef if $warn == 0;
      my $subitems=join(",",&get_item_keys($section,$item));
		$subitems=~s/\n//g;
		if( defined $opt_h ) {
	error(" Cant Find Data<br>\n\tsection=><FONT COLOR=BLUE>$section</FONT><br>\n\titem=><FONT COLOR=BLUE>$item</FONT><br>\n\tlinetext=><FONT COLOR=BLUE>$linetext</FONT><br>\n\tLegal Items=><FONT COLOR=BLUE>".join(",",get_items_in_section($section))."</FONT><br>\n\tLegal SubItems=><FONT COLOR=BLUE>".join(",",$subitems)."</FONT>");
		} else {

			print "=>=>=>=>=>=>=>=>=>=>=>=>\n";
			print format_section($section);
	error(" Cant Find Data\n\tsection=>$section\n\titem=>$item\n\tlinetext=>$linetext\n\tLegal Items=>".join(",",get_items_in_section($section))."\n\tLegal SubItems=>".join(",",$subitems));
		}
   }

   my($idx)=index($item_data{$section.":".$item},$linetext);

   error("FETCHING $section|$item|$linetext: string $linetext not found : string is ".$item_data{$section.":".$item}) if defined $opt_d and $warn != 0 and $idx < 0;
   return undef if $idx < 0;
   my($idx2)=index($item_data{$section.":".$item},"\n",$idx);
   error("FETCHING $section|$item|$linetext: newline not found after item was found???") if defined $opt_d and $warn != 0 and $idx2 < 0;
   return undef if $idx2 < 0;
   my($line) = substr($item_data{$section.":".$item},$idx,$idx2-$idx);
   $line =~ s/$linetext//g;
   my(@dat) = split /\s+/,$line;

   s_debugmsg("FETCHING section:$section|item:$item|txt:$linetext|col=$col|line=$line|output=$dat[$col]");

   error("Cant Find Data For section $section item $item line $linetext col $col") if $warn == 1 and ! defined $dat[$col];

   return undef if $dat[$col] eq "n/a";
	$dat[$col]=~ s/[A-Za-z\s%]//g;
	$dat[$col]=undef if $dat[$col] eq "";
   return $dat[$col];
}

sub get_item_keys
{
   my($section,$item)=@_;
   my(@dat) = split(/\n/,$item_keys{$section.":".$item});
   return @dat;
}

sub get_lines
{
   my($section,$item)=@_;
   my(@dat) = split(/\n/,$item_data{$section.":".$item});
   return @dat;
}

sub s_print_output
{
  statusmsg( "Printing sp_sysmon Results..." );
  my($cur_section)=("");
  foreach $cur_section ( sort keys %section_count ) {
    print format_section($cur_section);
  }
}

sub format_section
{
   my($cur_section)=@_;

	my $str="";
	my($dat1,$dat2,$dat3,$dat4);

	if( defined $opt_h ) {
		$str.= "\n<FONT SIZE=-1><CENTER><TABLE BORDER=1 BGCOLOR=BEIGE>";
	$str.= "<TR><TD COLSPAN=6 ALIGN=CENTER><FONT SIZE=+1><b>Section: $cur_section ($section_count{$cur_section} Rows)</b></FONT></TD></TR>\n";
		$str.= "<TR><TH>Item</TH><TH>SubItem</TH><TH>1</TH><TH>2</TH><TH>3</TH><TH>4</TH></TR>\n";
	} else {
	  $str.= "=============================================================\n";
     $str.= "Section: $cur_section ($section_count{$cur_section} Rows)\n";
	  $str.= "=============================================================\n";
	}

   foreach my $cur_item ( sort &get_items_in_section($cur_section) ) {

		# FIRST TEST ITEM ON ITSELF
		my $dat1= Data($cur_section,$cur_item,$cur_item,1,undef);
		# $str.= "<TR><TD>$cur_item</TD></TR>";
		if( defined $dat1 ) {

			# NOW ITEM ON SUBITEMS
			$dat2= Data($cur_section,$cur_item,$cur_item,2,undef);
			$dat3= Data($cur_section,$cur_item,$cur_item,3,undef);
			$dat4= Data($cur_section,$cur_item,$cur_item,4,undef);

			if( defined $opt_h ) {

				$str.= "<TR><TD>"."$cur_item".
							"</TD><TD>".$cur_item.
							"</TD><TD>".$dat1.
							"</TD><TD>".$dat2.
							"</TD><TD>".$dat3.
							"</TD><TD>".$dat4.
							"</TD></TR>\n";

			} else {

		$str.= "Item: $cur_item\n";
				$str.= sprintf( " %21s 1=> %-8s","",$dat1);
				$str.= sprintf( " 2=> %-8s",$dat2) if defined $dat2;
				$str.= sprintf( " 3=> %-8s",$dat3) if defined $dat3;
				$str.= sprintf( " 4=> %-8s",$dat4) if defined $dat4;
				$str.= "\n";

			}
		}

		my(@ik)=get_item_keys($cur_section,$cur_item);
		foreach (@ik) {

			# NOW ITEM ON SUBITEMS
			$dat1=  Data($cur_section,$cur_item,$_,1,undef);
			$dat2=  Data($cur_section,$cur_item,$_,2,undef);
			$dat3=  Data($cur_section,$cur_item,$_,3,undef);
			$dat4=  Data($cur_section,$cur_item,$_,4,undef);

			if( defined $opt_h ) {
				$str.= "<TR><TD>$cur_item</TD><TD>"."$_".
							"</TD><TD>".$dat1.
							"</TD><TD>".$dat2.
							"</TD><TD>".$dat3.
							"</TD><TD>".$dat4.
							"</TD></TR>\n";
			} else {
				$str.=sprintf( "  %-20s",$_);
				$str.= sprintf( " 1=> %-8s",$dat1) if defined $dat1;
				$str.= sprintf( " 2=> %-8s",$dat2) if defined $dat2;
				$str.= sprintf( " 3=> %-8s",$dat3) if defined $dat3;
				$str.= sprintf( " 4=> %-8s",$dat4) if defined $dat4;
				$str.= "\n";
			}
		}
   }

	$str.= "\n</TABLE></CENTER></FONT>\n" if defined $opt_h;

	return $str;
}

my($section_1_cnt)=0;
my($section_2_cnt)=0;

sub section_hdr
{
   my($section,$s) = @_;
   if( $section == 1 ) {
	$section_1_cnt++;
	$section_2_cnt=0
   } else {
	$section_2_cnt++
   }
   my($str)=$section_1_cnt;
   $str .= ".".$section_2_cnt if $section_2_cnt > 0;
   if( defined $opt_h ) {
      print "<hr><h$section>Section $str: $s</h$section>\n";
   } elsif( defined $opt_d ) {
      return if defined $opt_q;
      print "=================================\n";
      print "Section $str: $s\n";
      print "=================================\n";
		my(@items)=get_items_in_section($s);
		foreach my $item (@items) {
			my(@k)=get_item_keys($s,$item);
			print "KEYS: ",join(" ",@k),"\n";
			if( $#k> 0 ) {
				foreach (@k) {
					printf( "Item=%-22.22s : 1=%-14.14s 2=%-14.14s 3=%-14.14s\n",
						$item,
						Data($s,$item,$_,1,0),
						Data($s,$item,$_,2,0),
						Data($s,$item,$_,3,0));
				}
			} else {
				printf( "Item=%-22.22s : 1=%-14.14s 2=%-14.14s 3=%-14.14s\n",
					$item,
					Data($s,$item,undef,1,0),
					Data($s,$item,undef,2,0),
					Data($s,$item,undef,3,0));
			}
		}
      print "=================================\n";
   }
   print format_section($s) if defined $opt_p;
}

sub data_row
{
	my($s,$i,$si,$col)=@_;
	$col=3 unless defined $col;
	my $dp= Data($s,$i,$si,$col,0);
	row( bold($i), $dp);
}
__END__

=head1 NAME

sysmon_analysis.pl - SP_SYSMON SYSTEM ANALYZER

=head2 AUTHOR

Copyright &copy; 1998 by Edward Barlow

This work is a complete rewrite of code by Lars Kaarlson, who
created a prototype that analyzed sp_sysmon output.

=head2 SUMMARY

There are several sources of information that can be used to audit systems.
This code attempts to do a rules based check of your servers and to identify
problems that may be in existance on the servers.  The rules are based on
experience and sp_sysmon output is analyzed according to the (very loose)
rules defined by Sybase in their system 11 documentation.  They should,
hopefully allow you to better understand your server performance without
wading through tons of paper.

This analyzer is based on my experiences tuning a variety of servers.  I
intend to use it to assist me on contracts and as a part of the webmonitor
package.  You are welcome to use this code to analyze your servers, either
as a part of webmonitor or as a stand alone package.  If you notice any
testable information that is missing from this analyzer, please contact
the author and I will endeavor to put those rules into the system.

Successful analysis in many production environments is the only way this rules
base can converge on a stable point, and ideas that you have can greatly help
others.  Please note that many of the thresholds are completely made up by me,
and while they seem to work, i would appreciate feedback on how to come up
with better numbers.

=head2 INSTALLATION

The extended stored procedure library must be installed in your servers for that part of the auditing to work.  This can be accomplished by downloading the library from

http://www.edbarlow.com

=head2 PARSER RULES

The following are the rules the parser uses to store data from sp_sysmon
into variables.  The actual subroutine name is s_read_sysmon_inline.

  *) ignore blank lines or lines with -----------
  *) a line of === is a new section.  The first 2 sections are headings.
     and are treated separately.
  *) the first non blank line after --- is section heading
     (section_line==1)
  *) Indented 1 or 2 Characters and we are an item (within a section)
	  parse down to first field (or set of words - the key)
	 - set item_name
	   - set section_count{sctn} = ++
	   - item_count{sctn:itemnm}= 1
	   - item_data{sctn:itemnm}= $msg\n
  *) otherwise must have an item
	  $section_count{$section_name}++;
	  $item_count{$section_name.":".$item_name}++;
	  $item_data{$section_name.":".$item_name} .= $msg."\n";

=head2 THE RULES

The main body of rules is included as a fairly easy to read perl code file and
in the audit stored procedures.  Either may be modified.  You should not need
to modify other programs.  Note the simple interface to the output of
sp_sysmon, provided by Data() and get_lines().  A section (the first parameter
is the highest level ("Kernel Utilization")  and an item ("Engine Busy
Utilization:") is indented by two spaces in showplan info.  The third item
of the Data function is the line info, the fourth the column id, and the
last is a flag that determines if the code should produce an error if it
is unable to find that data element (not all elements are always produced by
sp_sysmon).  The format should be simple enough for any decent programmer
to understand and change.

=head2 USAGE

sysmon_analysis.pl [-s$SYBASE] -SDSQUERY -UUSER -PPASSWORD [-ifile -hayc -ttime -ofile]

 -d      : debug mode
 -i file : file name of sp_sysmon input (sets -y)
 -x      : run sp_sysmon
 -y      : analyze sp_symon
 -a      : run audit analyzer
 -c      : run sysconfigure monitor
 -r      : print raw sysmon output too (defined automatically if -o used)
 -h      : html'ize output for a web browser to view
 -o file : output file for raw sysmon output
 -q      : quiet mode - print little to screen
 -t time : time is the time interval for sp_sysmon (default = 10 minutes)

The SYBASE,DSQUERY,USER, and PASSWORD items are taken from variables by the
the same name if they are not passed directly into the program.

=cut
