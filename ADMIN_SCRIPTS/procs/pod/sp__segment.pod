
=head1 NAME

sp__segment - Segment information

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


show segmentation

=head2 USAGE


sp__segment [ @segment ]

if @segment is passed, will only print info for the given segment

=head2 SAMPLE OUTPUT


 1> exec sp__segment

 segment       Data KB     Indx KB     Total
 ------------- ----------- ----------- -----------
 default              2874        1432        4306
 logsegment             16          16          32
 system                  0          16          16
               sum         sum
               =========== ===========
                      2890        1464

 segment       type       indexname                        Size KB
 ------------- ---------- -------------------------------- ----------
 default       CLUSTERED  alerts.XPKalerts                 176
 default       CLUSTERED  audit_trail.XPKaudit_trail       62
 default       CLUSTERED  comn_database.XPKdatabase        32
 default       CLUSTERED  comn_dumpdevices.XPKcomn_dumpd   32
 default       CLUSTERED  comn_syscolumns.XPKcomn_syscol   160
