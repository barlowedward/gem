create procedure INV_track_config_info_sp

   @system_name      varchar(30),
   @system_type      varchar(30),
   @config_type      varchar(30),         -- config, login
                                          -- dataspace (sybonly),
                                          -- logspace (sybonly), role
                                          -- sysservers, devices (sybonly),
                                          -- sysusers, sysgroup, uptime, version

   @config_name      varchar(255),         -- config:       name of the config
                                          -- login:        name of the login
                                          -- dataspace:    ''
                                          -- logspace:    ''
                                          -- role:        rolename
                                          -- sysservers: servername
                                          -- devices:    devicename
                                          -- sysusers:    username
                                          -- sysgroups:    groupname
                                          -- uptime:     ''
                                          -- version:     ''

   @config_name2    varchar(255),            -- config:       ''
                                          -- login:        ''
                                          -- dataspace:    dbname
                                          -- logspace:    dbname
                                          -- role:       login name
                                          -- sysservers: network_name
                                          -- devices:    physname
                                          -- sysusers:    dbname
                                          -- sysgroups:    dbname
                                          -- uptime:     ''
                                          -- version:     ''

   @config_value   varchar(255)               -- config:       value of the config
                                          -- login:        suid of the login
                                          -- dataspace:    data space allocated
                                          -- logspace:     log space allocated
                                          -- role:        true or false
                                          -- sysservers: status
                                          -- devices:    device size
                                          -- sysusers:    groupuserisin
                                          -- sysgroups:  groupid
                                          -- uptime:     datetime of last db start
                                          -- version:    @@version
as
begin
   declare @msg varchar(255)

   -- IF no information exists for the SYSTEM, add initialization record
   if not exists ( select * from INV_configuration_tracker
      where system_name      =@system_name
      and   system_type    =@system_type   )
   begin
      INSERT INV_configuration_history
      SELECT getdate(),'initial audit' ,@system_name,@system_type,'','','',''

      INSERT INV_configuration_tracker
      select    @system_name,@system_type,@config_type,@config_name,
               @config_name2,@config_value

      return
   end


   if not exists (
      select * from INV_configuration_tracker
      where system_name    =    @system_name
      and   system_type      =  @system_type
      and   config_type      =  @config_type
      and   config_name2    =    @config_name2
      and   config_name      =  @config_name )
   begin
      -- the RECORD does not exist, so this is an ADD of something
      -- we want to audit.  Put a 'New' row in history & insert to tracker

      -- Since its a new row, we only need to put stuff into 'history'
      -- if we need to report it.  We also only put stuff into 'history'
      -- if its > 10 minutes since the initial record was saved, presuming
      -- that everything existing before 'initial record' was ok and
      -- that this system is supposed to track changes
      if exists ( select *
                  from INV_configuration_history
                  where change_message='initial audit'
                  and    system_name=@system_name
                  and    system_type=@system_type
                  and   datediff( mi, change_time,getdate()) > 10 )
      begin
         -- config:       cant happen
         -- login:        new login found
         -- dataspace:    new database found
         -- logspace:     ignore as the dataspace record will cover it
         -- role:        new role found
         -- sysservers: new server found
         -- devices:    new device found
         -- sysusers:    new user in db found
         -- sysgroups:  new group in db found
         if @config_type='config'
            select @msg='New Config '+@config_name
         else if @config_type = 'login'
            select @msg='New Login '+@config_name+' '+@config_name2
         else if @config_type = 'devices'
            select @msg='New Device '+@config_name
         else if @config_type = 'dataspace'
            select @msg='New Database '+@config_name2
         else if @config_type = 'logspace'
            select @msg=null
         else if @config_type = 'role' and @config_value='Y'
            select @msg='New Role '+@config_name+' For '+@config_name2+' Set to '+@config_value
         else if @config_type = 'sysservers'
            select @msg='New Remote Server '+@config_name
         else if @config_type = 'sysusers'
            select @msg='New User '+@config_name+' In Database '+@config_name2
         else if @config_type = 'sysgroups'
            select @msg='New Group '+@config_name+' In Database '+@config_name2
         else if @config_type = 'uptime'
            select @msg='Server Was Last Booted At '+@config_value
         else if @config_type = 'version'
            select @msg='Server Version '+@config_value


         if @msg is not null
         INSERT   INV_configuration_history
            select getdate(), @msg,
               @system_name,@system_type,@config_type,@config_name,
               @config_name2,@config_value
      end

      INSERT    INV_configuration_tracker
      select    @system_name,@system_type,@config_type,@config_name,
               @config_name2,@config_value
   end
   else if exists ( select * from INV_configuration_tracker
      where system_name    =    @system_name
      and   system_type      =  @system_type
      and   config_type      =  @config_type
      and   config_name      =  @config_name
      and   config_name2   =  @config_name2
      and   config_value   !=  @config_value )
   begin
      -- the RECORD exists but the value has changed.
      -- log it and update the tracker

      if @config_type='config'
         select @msg='Configuration Option '+@config_name+' reset to '+@config_value
      else if @config_type = 'login' and @config_value='N.A.'
         select @msg='Login '+@config_name+' Updated'
      else if @config_type = 'login'
         select @msg='Login '+@config_name+' Updated (Suid='+@config_value+')'
      else if @config_type = 'devices'
         select @msg='Device '+@config_name+' '+@config_name2+' Size='+@config_value
      else if @config_type = 'dataspace'
         select @msg='Data Space Changed On '+@config_name2+' To '+@config_value
      else if @config_type = 'logspace'
         select @msg='Log Space Changed On '+@config_name2+' To '+@config_value
      else if @config_type = 'role'
         select @msg='Role '+@config_name+' For '+@config_name2+' reset to '+@config_value
      else if @config_type = 'sysservers'
         select @msg='Remote Server '+@config_name+' Updated'
      else if @config_type = 'sysusers'
         select @msg='User '+@config_name+' In Database '+@config_name2+' Group Now='+@config_value
      else if @config_type = 'sysgroups'
         select @msg='Group '+@config_name+' In Database '+@config_name2+' Group Id='+@config_value
      else if @config_type = 'uptime'
         select @msg='Server Was Last Rebooted At '+@config_value
      else if @config_type = 'version'
            select @msg='Server Version Is '+@config_value

      if @msg is not null
      INSERT   INV_configuration_history
      select getdate(), @msg,
         @system_name,@system_type,@config_type,@config_name,
         @config_name2,@config_value

      UPDATE INV_configuration_tracker
      set   config_value=   @config_value
      where system_name =    @system_name
      and   system_type   =  @system_type
      and   config_type   =  @config_type
      and   config_name   =  @config_name
      and   config_name2   =  @config_name2
   end
   -- else
   -- begin
      -- THE RECORD & VALUE ARE IDENTICAL - NO NEED TO DO ANYTHING
   -- end
end
go
grant execute on INV_track_config_info_sp to public
go
