#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2004-5 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use 	IO::Socket;
use 	MlpAlarm;
use 	CommonFunc;
use  	File::Basename;
use 	Getopt::Long;
use  	Repository;
my 	$VERSION = 1.2;
use vars qw( $GENERATE $DEBUG $SLEEPTIME $TEST $LOGALARM $ITERATIONS $DEBUG2 $UPDATE_GEM $discovery_ok_file
		$CFGFILE $INTERFACE_FILE $SYSTEM $NORECHECK $NOLOCK $BATCHID);

# SPECIAL INCLUDE PATH AS THIS PROGRAM CAN BE CALLED FROM CONFIGURE.PL
# BASICALLY - THIS OVERRIDES THE NORMAL INCLUDE PATH SET IN THE ABOVE 'use lib'
# STATEMENT IF THE WORD SPECIALCODE is found in that include path!
#BEGIN {
#foreach (@INC) { print "OLDINC=$_\n"; }
#        my($FROMCONFIGURE)=0;
#        my(@NEWINC, %NEW);
#        foreach (@INC) {
#                push @NEWINC,$_ if $FROMCONFIGURE or -d $_;
#                $FROMCONFIGURE=1 if /SPECIALCODE/;
#        }
#        @INC=@NEWINC if $FROMCONFIGURE;
#foreach (@INC) { print "INC=$_\n"; }
#}
#
#foreach (@INC) { print "INC=$_\n"; }
#die "DONE";

$| =1;
my(%IPFAIL,%ipcache);		# from ip.dat

sub usage {
	print @_,"\n";
 	return "Usage: port_monitor.pl [--LOGALARM] [--DEBUG] --SLEEPTIME=secs

	--CFGFILE=FILENAME
	--SYSTEM=SYS	[ returns all responses just for one system ]
	--DEBUG			[ if --GENERATE will NOT actually do the writes ]
	--BATCHID=id	(default PortMonitor)
	--NOLOCK
	--NORECHECK	(dont recheck during generate)
	--LOGALARM
	--ITERATIONS=num (loop num times slepeing SLEEPTIME seconds betwen loops)
	--GENERATE	   Generates/Update config file
	--INTERFACE_FILE=FILENAME
	--SLEEPTIME=SECONDS
	--UPDATE_GEM   updates the gem configruration file (requires --GENERATE)

Only loops if SLEEPTIME defined > 0
The system sends an event when state changes\n";
}

die usage("Bad Parameter List $!\n") unless
    GetOptions("DEBUG"	=>\$DEBUG,
    		"DEBUG2"			=>\$DEBUG2,
			"LOGALARM"		=>\$LOGALARM,
	#		"UPDATE_GEM"	=> \$UPDATE_GEM,
			"CFGFILE=s"		=>\$CFGFILE,
			"TEST"			=>\$TEST,
			"NORECHECK"		=>\$NORECHECK,
			"SYSTEM=s"		=>\$SYSTEM,
			"SUCCESSFILE=s"	=> \$discovery_ok_file,
			"GENERATE"		=>\$GENERATE,
			"NOLOCK"			=>\$NOLOCK,
			"BATCHID=s"		=> \$BATCHID,
			"INTERFACE_FILE=s"=>\$INTERFACE_FILE,
			"SLEEPTIME=i"	=>\$SLEEPTIME,
			"ITERATIONS=i"	=>\$ITERATIONS			 );

$BATCHID = "PortMonitor" unless $BATCHID;

print "port_monitor.pl $VERSION - run at ".localtime(time)."\n";
print "Copyright (c) 2001-2009 by Sql Technologies\n\n";

if( $SYSTEM ) {
	print "Checking Ports 1..2000 for System $SYSTEM\n";
	for my $i (1..2000) {
		if( check_up_quick($SYSTEM,$i,1) ) {
			print "Port $i Responded\n";
		}
	}
	print "Completed\n";
	exit(0);
}

my($root_dir)=get_gem_root_dir();
$CFGFILE="$root_dir/conf/port_monitor.dat" if ! defined $CFGFILE and -r "$root_dir/conf/port_monitor.dat";
$GENERATE=1 unless defined $CFGFILE;
$CFGFILE="$root_dir/conf/port_monitor.dat" if ! defined $CFGFILE and -d "$root_dir/conf"	and defined $GENERATE;

die "Must pass configuration file (no $root_dir/conf directory)\n"	unless defined $CFGFILE;

my(@orig_file_lines);
my @new_file_lines;
my(%portping_conf_additions_state);
my(@file_top_comment);
my(@interface_file_data_rows);
my(@portping_conf_additions);

# THESE ARE THE PARSED VALUES THAT ARE READ FROM THE CONFIG FILE
my(@orig_host_port_keys);		# key=$host.$port;
my(%orig_logical_name);
my(%servernm_by_hostport);
my(%orig_portnum);
my(%orig_description);
my(%orig_host_port_status);
my(%orig_host_port_line);
my(%orig_host_port_is_commented);

my(%long_service_desc);

#
# GENERATE THE PORT_MONITOR.DAT FILE
#
if( defined $GENERATE ) {
	read_port_configfile($CFGFILE,1) if -r $CFGFILE;
	# read_ip_data();
	use DBIFunc;
	print "
==============================================
|  PORT MONITOR CALLED IN --GENERATE MODE    |
==============================================
";

#print "
#==============================================
#| PORT MONITOR CONFIG FILE *WILL* BE UPDATDED |
#==============================================
#" if $UPDATE_GEM;

	if( defined $ENV{SYBASE} ) {
		print "SYBASE Environment = $ENV{SYBASE}\n";
		print "Reading Interfaces File\n";
		# This comes back as SERVER HOST PORT
		push @interface_file_data_rows, dbi_get_interfaces(undef,$INTERFACE_FILE);
		print " => Adding ",($#interface_file_data_rows+1)," Elements From Interfaces Files\n";
#	} else {
#		die "FATAL ERROR - CAN NOT GENERATE GEM INFO - SYBASE ENVIRONMENT VARIABLE IS NOT SET\n" if $UPDATE_GEM;
	}

	print "Reading GEM Configuration Files\n";
	my(%server_type);
	my($cnt)=0;
	foreach (get_password(-type=>'sqlsvr') ) { $cnt++;$server_type{$_} = "SQL Server"; 	}
	print " => Read $cnt SQL Server Definitions\n";
	
	$cnt=0;
	foreach (get_password(-type=>'sybase') ) { $cnt++;$server_type{$_} = "Sybase Server"; 	}
	print " => Read $cnt Sybase Server Definitions\n";

	# you reset the type above so should be no sql servers unless they are missed
	# we dont know the sybase server ports but we do for the sql server ports
	print "Adding Microsoft Sql Servers to port monitoring\n";
	foreach (keys %server_type ) {
		next unless $server_type{$_} eq "SQL Server";
		if( defined $servernm_by_hostport{$_."1433"} ) {		# allready monitored
			#print "<dbg1> Generate Skipping $_: monitored as ",$servernm_by_hostport{$_."1433"},"\n" if $DEBUG;
			#next;
		} else {
			my($l)=sprintf("%-30s %-10s %s %s\n", $_,1433,$_,"Microsoft SQL Server");
			push @portping_conf_additions,$l;
			$portping_conf_additions_state{$l} = 1;		# its a registered server - of course it needs to be monitored
			$servernm_by_hostport{$_.".1433"}=$_;
		}

		# dunno why - but the agent never registers this way so i guess i should ignore em
#		if( defined $servernm_by_hostport{$_."1434"} ) {
#			# print "<dbg1> Generate Skipping $_: monitored as ",$servernm_by_hostport{$_."1433"},"\n" if $DEBUG;
#			#next;
#		} else {
#			my($f)=sprintf("%-30s %-10s %s %s\n", $_,1434,$_,"SQL Server Agent");
#			#my($f)= $_."    1434   $_ SQL Server Agent\n";
#			push @portping_conf_additions,$f;
#			$servernm_by_hostport{$_.".1434"}=$_;
#
#			#
#			#WARNING - FOR SOME REASON PINGING 1434 ALWAYS FAILS...
#			#
#			$portping_conf_additions_state{$f} = check_up_quick($_,1434,1);
#		}
	}

	my( %possible_unix_host, %possible_sybase_server );		# these represent the 'up' servers
																				# key=name, value for sybase=host/port
	my(%INTERFACE_INFO);			# key = host,port  value = array of if names listed on that host port
	my(%INTERFACE_HOSTPORT);	# key = nm value=hostport => needed because of unix system aliasing sigh
	print "\nTesting Data From Interfaces File\n";
	foreach ( @interface_file_data_rows ) {
		chomp $_->[0];	# server
		chomp $_->[1];	# host
		chomp $_->[2];	# port
		$_->[0] =~ s/\+.+$//;
		$_->[1] =~ s/\+.+$//;
		$_->[2] =~ s/\+.+$//;

		next unless $_->[0];
		next unless $_->[1];
		next unless $_->[2];

		my($nm)   = $_->[0];
		my($Xhost) = $_->[1];
		my($Xport) = $_->[2];

		if( $Xhost =~ /\d+\.\d+\.\d+\.\d+/ ) {		# ip address
			my($a)=GetHostFromIp($Xhost);
			$Xhost = $a if $a;
		}
		$Xhost =~ s/\.[a-z\.]+$//;		# remove .xxx.com type formats

		my($type)="";
		$nm=~s/\s+$//;
		#if( defined $servernm_by_hostport{$Xhost.$Xport} ) {
		#	print "  Testing... $nm skipped duplicate or sql server\n" if $DEBUG2;
		#	next;
		#}
		if( defined $server_type{$nm} ) {
			$type=$server_type{$nm};
			if( $server_type{$nm} eq "SQL Server" ) {
				$server_type{$nm}="MS SQL Server";
			}
		} elsif( $nm=~/_BACK$/i ) {
			$nm=~s/_BACK$//i;
			$type="Backup Server";
		} elsif( $nm=~/_HS$/i ) {
			$nm=~s/_HS$//i;
			$type="Historical Server";
		} elsif( $nm=~/_XP$/i ) {
			$nm=~s/_XP$//i;
			$type="XP Server";
		} elsif( $nm=~/_MON$/i ) {
			$nm=~s/_MON$//i;
			$type="Monitor Server";
		} elsif( $nm=~/_BACKUP$/i ) {
			$nm=~s/_BACKUP$//i;
			$type="Backup Server";
		} elsif( $nm=~/_RS$/i ) {
			$nm=~s/_RS$//i;
			$type="Rep Server";
		} elsif( $nm=~/_BS$/i ) {
			$nm=~s/_BS$//i;
			$type="Backup Server";
		} elsif( $Xport == 1433 ) {
			$type="SQL Server";
		} else {
			$type="";
		}
		#print "DBG DBG onm=$_->[0] nm=$nm type=$type\n";
		# my($len)=50 - length($nm);
		# $len-= length($type)+3 if $type ne "";
		if( defined $type and $type ne "" ) {
			printf "  Testing %-30.30s %28.28s",$_->[0]." (".$type.")"," [".$Xhost.":".$Xport."]";
		} else {
			printf "  Testing %-20s %38s",$nm," [$Xhost:$Xport]";
		}

		my($line)=sprintf("%-30s %-10s %s %s\n", $Xhost,$Xport,$nm,$type);
		if( check_up_quick($Xhost,$Xport,1) ) {
			print " -- ok ";
			$portping_conf_additions_state{$line}=1;
			$possible_unix_host{$Xhost}=1;
			$INTERFACE_HOSTPORT{$nm} = $Xhost."~".$Xport;
			if( defined $INTERFACE_INFO{$Xhost."~".$Xport} ) {
				push @{$INTERFACE_INFO{$Xhost."~".$Xport}}, $nm;
			} else {
				my(@x);
				push @x,$nm;
				$INTERFACE_INFO{$Xhost."~".$Xport} = \@x;
			}			# key = host,port  value = array of if names listed on that host port
			#print "\nDBG DBG - ADDING POSSIBLE UNIX SERVER $Xhost";
			$possible_sybase_server{$_->[0]}=$Xhost."~".$Xport if $type eq "" or $type eq "Sybase Server";
			#print "\nDBG DBG - ADDING POSSIBLE SYBASE SERVER $nm";
		} else {
			$portping_conf_additions_state{$line}=0;
			print " -- fail";
   	}
		print "\n";
		#print "Adding Line $line\n" if $TEST;
		if( ! defined $servernm_by_hostport{$Xhost.$Xport} ) {
			push @portping_conf_additions,$line;
			$servernm_by_hostport{$Xhost.$Xport} = $nm;
		}
	}

	print "\nChecking Existing Port Monitor Config File\n";
	if( ! $NORECHECK ) {
		foreach ( @orig_host_port_keys ) {
			if( $orig_description{$_} ) {
				printf "  Testing %-30s %28s",$orig_logical_name{$_}." ($orig_description{$_})","[$servernm_by_hostport{$_}:$orig_portnum{$_}]";
	     	} else {
				printf "  Testing %-20s %38s",$orig_logical_name{$_}," [$servernm_by_hostport{$_}:$orig_portnum{$_}]";
	     	}
			$orig_host_port_status{$_}=check_up_quick($servernm_by_hostport{$_},$orig_portnum{$_},1 );
	     	if( $orig_host_port_status{$_} ) {
	     		print " -- ok  \n";
	     		$possible_unix_host{$servernm_by_hostport{$_}}=1;
				$possible_sybase_server{ $orig_logical_name{$_} }=  $servernm_by_hostport{$_}."~".$orig_portnum{$_};
			} else {
	      	print " -- fail\n";
	      }
	   }
	}
	
	# ARE ANY OF THOSE HOSTS MISSING IN THE GEM CONFIGURATION FILE?
#	
#	foreach (get_password(-type=>'unix') )   {
#		delete $possible_unix_host{$_};
#		# delete the host if the ip matches something existing
#		foreach ( get_ip_alias_list($_) ) {
#			delete $possible_unix_host{$_}; delete $possible_unix_host{lc($_)};
#		}
#	}
#	foreach (get_password(-type=>'sqlsvr') )   {
#		delete $possible_unix_host{$_};delete $possible_sybase_server{$_};
#		delete $possible_unix_host{lc($_)};delete $possible_sybase_server{lc($_)};
#	}
#	foreach (get_password(-type=>'win32servers') )   {
#		delete $possible_unix_host{$_};delete $possible_sybase_server{$_};
#		delete $possible_unix_host{lc($_)};delete $possible_sybase_server{lc($_)};
#		foreach ( get_ip_alias_list($_) ) {
#			delete $possible_unix_host{$_}; delete $possible_unix_host{lc($_)};
#			delete $possible_sybase_server{$_}; delete $possible_sybase_server{lc($_)};
#		}
#	}
#	
#	my(%NEWSYBASE);
#
#	my(%sybase_password_list);		# password list for crack attempts in isql format
#	my(%sybase_servernames);		# @@SERVERNAME by $key
#	foreach my $s (get_password(-type=>'sybase') ) {
#		my($hostport) = $possible_sybase_server{$s};
#		delete 			 $possible_sybase_server{$s};
#
#		# print "DBG DBG - DELETING REGISTERED $s\n";
#		#print "DBG DBG - hostport is $hostport\n";
#		# DELETE POSSIBLE SERVERS MAPPED TO THIS ONE!
#		if( $hostport and $INTERFACE_INFO{$hostport} ) {
#			foreach ( @{$INTERFACE_INFO{$hostport}} ) {
#		#		print "DBG DBG - DELETING $_\n";
#				delete $possible_sybase_server{$_};
#			}
#		}
#		if( $INTERFACE_HOSTPORT{$s} and $INTERFACE_INFO{$INTERFACE_HOSTPORT{$s}} ) {
#			foreach ( @{$INTERFACE_INFO{$INTERFACE_HOSTPORT{$s}}} ) {
#		#		print "DBG DBGX - DELETING $_\n";
#				delete $possible_sybase_server{$_};
#			}
#		}
#
#		my($l,$p)=get_password(-type=>'sybase',-name=>$s);
#		$sybase_password_list{"isql -U$l -P$p "} = 1;
#
#		my(%x)=get_password_info(-type=>'sybase',-name=>$s);
#		if( ! $x{SERVERNAME} ) {
#			my($SERVERNAME)=test_isql_connection($s,"isql -U$l -P$p ");
#			next unless $SERVERNAME;
#			$NEWSYBASE{ $s."|SERVERNAME" }   = $SERVERNAME;
#			$sybase_servernames{$SERVERNAME} = $s;
#		}
#	}
#
#	my(%NEWUNIX);
#	my($numunix,$numsybase)=(0,0);
#	if( $^O ne 'MSWin32' ) { 
#		print "\nTESTING POSSIBLE NEW UNIX HOSTS\n";
#		# print __LINE__,Dumper \%possible_unix_host;
#		foreach my $h ( keys %possible_unix_host ) {
#			next if $h =~ /\d+\.\d+\.\d+\.\d+/; 			# no ip addresses
#	
#			# CHECK IF ITS AN ALIAS
#			my($ip)=GetIpAddress($h);
#			print "UNABLE TO FIND ip address for $h $ip\n" unless $ip;
#			next unless $ip;
#	
#			my($isdup)='FALSE';
#			foreach my $rh ( get_password(-type=>'unix') ) {
#				if( $ip eq $ipcache{ uc($rh) } ) {
#					print "     Skipped: $h is a duplicate of $rh\n";
#					$isdup=$rh;
#					last;
#				}
#			}
#			next if $isdup ne "FALSE";
#	
#			my($is_ok) = test_ssh_connection($h);
#			printf "  Testing %-20s %-38s\n",$h,"ssh connectivity $is_ok";
#			next if $is_ok eq "Failed";
#	
#			# print "   SERVER $h PASSES TESTS\n";
#			$numunix++;
#			$NEWUNIX{$h."|LOGIN"}  		= "sybase";
#			$NEWUNIX{$h."|PASSWORD"}  	= "xxx";
#			$NEWUNIX{$h."|IP"}   		= $ip;
#			$NEWUNIX{$h."|SERVER_TYPE"}   		= "PRODUCTION";
#			$NEWUNIX{$h."|UNIX_COMM_METHOD"}   	= "SSH";
#			$NEWUNIX{$h."|WIN32_COMM_METHOD"}  	= "NONE";
#		}
#	}

#print "
#==============================================
#| TESTING POSSIBLE NEW SYBASE DATABASES      |
#==============================================
#
#The following servers may be sybase ase databases available for monitoring.
#Please enter the sa password for the system to enable monitoring.  If you
#hit enter, the server will be skipped and not monitored.
#
#";
#	foreach my $h ( keys %possible_sybase_server ) {
#		print "  Testing Possible Sybase Server: $h \n";
#		my($is_ok, $passitem);
#		foreach my $c ( sort keys %sybase_password_list ) {
#			# print "TESTING $c\n";
#			$is_ok = test_isql_connection($h, $c);
#			$passitem = $c;
#			last if $is_ok;
#		}

#
#		COMMENTED OUT - no need for interactive passwords
#
#		if( ! $is_ok ) {
#			print "Enter sa password for $h ( -enter- will ignore server)\n";
#			my $PASSWORD;
#			$PASSWORD = <STDIN>;
#			# replaced this 4/2/09 but ReadKey appears to be a special download
#			# so passwords will show for now...
#			#use Term::ReadKey;
#			#ReadMode('noecho');
#			#$password=ReadLine(0);
#			#ReadMode('normalecho');
#
#			next unless $PASSWORD =~ /[\w\d]/;
#			chomp $PASSWORD;
#			chomp $PASSWORD;
#			$passitem = "isql -Usa -P$PASSWORD ";
#			$sybase_password_list{$passitem}=1;
#			$is_ok = test_isql_connection($h, $passitem );
#		}

#		if( ! $is_ok ) {
#	#		print "  => Unable to connect to $h - ignoring server\n";
#		} else {
#
#			# IS THE SERVERNAME ALLREADY USED
#			if( $sybase_servernames{ $is_ok } ) {
#				print "    SKIPPED DUPLICATE SERVER $sybase_servernames{ $is_ok }\n";
#				next;
#			}
#
#			$sybase_servernames{ $is_ok }  = $h;
#
#			print "    SUCCESSFUL connection to $h - $is_ok \n";
#			$numsybase++;
#			my($hostnm,$p)=split(/~/,	$INTERFACE_HOSTPORT{$h});
#			$h =~ s/\s//g;
#			$hostnm =~ s/\s//g;
#			$p =~ s/\s//g;
#			$is_ok =~ s/\s//g;
#			$NEWSYBASE{$h."|PORTNUMBER"} = $p;
#			$NEWSYBASE{$h."|HOSTNAME"} = $hostnm;
#			$passitem =~ /\s-P(.+)\s*$/;
#			$NEWSYBASE{$h."|PASSWORD"} = $1;
#			die "WOAH - NO PASSWORD FOR $h FROM $passitem\n" unless $1;
#
#			$NEWSYBASE{$h."|SERVERNAME"}=$is_ok;
#			$NEWSYBASE{$h."|LOGIN"} = "sa";
#			$NEWSYBASE{$h."|SERVER_TYPE"} = "PRODUCTION";
#		}
#	}
#
#	if( $DEBUG ) {
#		use Data::Dumper;
#		if( $^O ne 'MSWin32' ) { 
#			print "++++++++++++++++++++++++++++\n";
#			print Dumper \%NEWUNIX;
#			print "++++++++++++++++++++++++++++\n";
#		}
#		print "++++++++++++++++++++++++++++\n";
#		print Dumper \%NEWSYBASE;
#		print "++++++++++++++++++++++++++++\n";
#	}

	# print __LINE__,Dumper \%INTERFACE_INFO,"\n";
	# print __LINE__,Dumper \%possible_unix_host,"\n";
	# print __LINE__,Dumper \%possible_sybase_server,"\n";
	# die "DONE";

#	if( $numunix and $UPDATE_GEM and $^O ne 'MSWin32' ) { 
#		print "SAVING $numunix UNIX SERVERS TO THE GEM REPOSITORY\n";
#		add_edit_item( -type=>'unix', -op=>'update', -values=>\%NEWUNIX );
#		print " -> changes applied\n";
#	}
#
#	if( $numsybase and $UPDATE_GEM  ) {
#		print "SAVING $numsybase Sybase ASE Servers TO THE GEM REPOSITORY\n";
#		add_edit_item( -type=>'sybase', -op=>'update', -values=>\%NEWSYBASE );
#		print " -> changes applied\n";
#	}

	my $output="";
	$output .= "# FINAL OUTPUT FILE WILL BE:\n" if $DEBUG;
	$output .= join("\n",@file_top_comment)."\n";
	#print sort @new_file_lines;
	$output .= "# ORIG LINES ===================\n" if $DEBUG;
	foreach( @orig_host_port_keys ) {
		if( $NORECHECK ) {
			$output.="#" if $orig_host_port_is_commented{$_};
			$output .= $orig_host_port_line{$_}."\n";
			next;
		}

		my($status)=$orig_host_port_status{$_};
		die "NO STATUS for $_ \n" unless defined $status;
		if( $status ==1 and $orig_host_port_is_commented{$_} ) {
			print "Removing Commented Out Key $_\n" if $DEBUG;
			$output .= $orig_host_port_line{$_}."\n";
		} else {
			print "Not Commenting Out Failed Key $_\n" if $DEBUG;
			$output.="#" if $orig_host_port_is_commented{$_};
			$output .= $orig_host_port_line{$_}."\n";
		}
	}

	$output .= "# ADDITIONS ===================\n" if $DEBUG;
	foreach ( sort @portping_conf_additions ) {
		if( $portping_conf_additions_state{$_} ) { #ok
			$output .= $_;
		} else {
			$output .= "#".$_;
		}
	}

	$output .= "# END ===================\n" if $DEBUG;
	$output=~s/\r//g;

	if( $DEBUG ) {
		if( ! $DEBUG2 ) {
			print "NOT WRITING OUT FILE TO STANDARD OUT AS THAT COULD BE BIG.\n";
			print "IF YOU WANT TO SEE THE ACTUAL PORT OUTPUT USE --DEBUG2\n";
		}
		print "port_monitor.dat> ",$output if $DEBUG2;
	} else {
		print "SAVING PORT MONITOR DATA TO $CFGFILE\n";
		open( CFG , "> $CFGFILE" );
		print CFG $output;
		close(CFG);
		print "GENERATION OF CONFIG FILE COMPLETED\n";
	}
#	unlink "portmon_test.sql" if -r "portmon_test.sql";
#	if( $discovery_ok_file ) {
#		open(X, "> $discovery_ok_file") or die "Cant Write $discovery_ok_file $!\n";
#		print X localtime(time);
#		close(X);
#	}
	exit(0);
}		

#
# END GENERATE
#

# die "CANT PASS --UPDATE_GEM unless you pass --GENERATE\n" if $UPDATE_GEM;

read_port_configfile($CFGFILE,0) if -r $CFGFILE;
cd_home();

if( ! $NOLOCK ) {
	print "NOLOCK IS NOT DEFINED!\n" if $DEBUG;
	if( Am_I_Up(-debug=>$DEBUG,-prefix=>$BATCHID) ) {
		my($fname,$secs,$thresh)=GetLockfile(-prefix=>$BATCHID);
		print "Cant Start port_monitor.pl - it is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nFile=$fname secs=$secs thresh=$thresh secs\n";
		exit(0);
	}
} else {
	print "NOLOCK IS DEFINED!\n" if $DEBUG;
}

# print "DBG DBG: LINE ",__LINE__," PORT KEYS:\n\t",join("\n\t",@orig_host_port_keys),"\n";

I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;

my($ping_interval)=300;
my($lastpingtime)=time;
MlpBatchJobStart(-BATCH_ID => $BATCHID ) if defined $LOGALARM;

print "Pings Started at " .scalar(localtime) ."\n";
while(1) {
	my(@downlist);
	# print "DBG DBG: LINE ",__LINE__," PORT KEYS:\n\t",join("\n\t",@orig_host_port_keys),"\n";

	foreach my $k (@orig_host_port_keys) {
		I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;
		# print "DBG DBG: Checking $k\n";
		push @downlist,$k unless check_up($k);
	}
	print "Port Pings Completed\n";
	complain (@downlist);

	last if ! defined $SLEEPTIME or $SLEEPTIME<=0;
	last if $ITERATIONS-- <= 0;
	I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;

	print "sleeping $SLEEPTIME seconds...";
	print " $ITERATIONS iterations remaining " if defined $ITERATIONS;
	print "\n";

	if( time - $lastpingtime > $ping_interval ) {
		$lastpingtime=time;
		# MlpBatchRunning() if defined $LOGALARM;
	}

	sleep($SLEEPTIME);
}

I_Am_Down(-prefix=>$BATCHID) unless $NOLOCK;
MlpBatchJobEnd() if defined $LOGALARM;
print "port monitor completed at ".localtime(time)."\n";
exit 0;

sub GetHostFromIp {
	my($ip)=@_;
	foreach ( keys %ipcache ) { return $_ if $ipcache{$_} eq $ip; }
	return undef;
}

sub get_ip_alias_list {
	my($host)=@_;
	my($ip)=GetIpAddress($host);
	my(@list);
	foreach ( keys %ipcache ) { push @list, $_ if $ipcache{$_} eq $ip; }
	return @list;
}

sub GetIpAddress {
	my($svr)=@_;
	return $ipcache{ uc($svr) } if $ipcache{ uc($svr) };
	return undef if $svr =~ /\\/;
	return undef if $IPFAIL{$svr};

	my(@address)=gethostbyname($svr);
	if( $#address<0 ) {
		warn "Cant resolve $svr $!\n";
		return undef;
	}
	@address= map { inet_ntoa($_) } @address[4..$#address];
	print "  ==> GetIpAddress($svr) = ",join(" ",@address),"\n" if $DEBUG;
	$IPFAIL{$svr}=1 if $#address<0;
	print "DBG DBG: IP FAILED FOR $svr\n" if $#address<0;
	$ipcache{uc($svr)} = $address[0];;
	return $address[0];
}

sub myprint { 	print "-> ",@_ if $DEBUG; }
sub test_ssh_connection {
	my($svr)=@_;
#	my($cmd)="ssh sybase\@".$svr." ls -d /etc";
#	open(CC,$cmd."|") or die "Cant run $cmd\n";
	my($ok)="Failed";
#	while(<CC>) {
#		chomp;
#		if ( /^\/etc$/ ) { $ok="Ok"; last; }
#		print "SSH: $_\n";
#	}
#	close(CC);

	use Net::myRSH;
	my(@rc)=do_rsh(-login=>'sybase', -method=>'ssh',-hostname=>$svr, -command=>" ls -d /etc", -debug=>$DEBUG, -debugfunc=>\&myprint );
	if( $rc[0] eq "/etc" ) {
		#print "SUCCESS\n";
		$ok="Ok";
	} else {
		#print "FAILED\n";s
		my(@rc)=do_rsh(-method=>'ssh',-hostname=>$svr, -command=>" ls -d /etc", -debug=>$DEBUG, -debugfunc=>\&myprint );
		if( $rc[0] eq "/etc" ) {
			#print "SUCCESS\n";
			$ok="Ok";
		} else {
			#print "FAILED\n";
		}
	}
	return $ok;
}

# returns @@SERVERNAME or undef
#sub test_isql_connection {
#	my($svr, $cmd)=@_;
#	if( ! -r "portmon_test.sql" ) {
#		open(T,">portmon_test.sql") or die "cant write portmon_test.sql $!\n";
#		print T "select \@\@servername\ngo\nexit\n";
#		close(T);
#		sleep(1);
#	}
#	# print "test_isql ( $svr, $cmd ) \n";
#	my($cmd2) = "$cmd -S$svr -iportmon_test.sql\n";
#	# print "(dbg) $cmd2\n";# if $DEBUG;
#	open(CC,$cmd2."|") or die "Cant run $cmd\n";
#	my($ok)=0;
#	while(<CC>) {
#		chomp;
#		next if /^\s*$/;
#		# print "(dbg) $_ \n";# if $DEBUG;
#		next if  /-----/;
#		last if /Requested server name not found/ or /CT-LIBRARY error:/;
#		last if /Msg 4002/;		# login incorrect is ok...
#		last if /Msg \d+, Level \d+, State \d+/ or /Unknown command./;
#		last if /Unknown command./;
#		$ok=$_;
#		#print "SERVERNAME FOR $svr is $_\n";
#		last;
#	}
#	close(CC);
#	return 0 if $ok=~/Msg \d+, Level \d+, State \d+/ or $ok=~/Unknown command./;
#	return $ok;
#}

#sub read_ip_data {
#	my($CONFIGFILE_ROOT)=get_gem_root_dir()."/conf/A_";
#	my($file)=$CONFIGFILE_ROOT."IP.dat";
#	print "Reading $file\n";
#	if( -r $file ) {
#		open(XX,$file) or die "Cant read $file $!\n";
#		while(<XX>) {
#			next if /^#/ or /^\s*$/;
#			my($svr,$ipadds,$flag)=split(/\s+/,$_);
#			if( $ipadds eq "FAILED" ) {
#				$IPFAIL{$svr}=1;
#			} else {
#				$ipcache{ uc($svr) } = $ipadds;
#			}
#		}
#		close(XX);
#	}
#}

sub read_port_configfile {
   my($CFGFILE, $READ_COMMENTS_TOO)=@_;
   print "Reading $CFGFILE (READ_COMMENTS=$READ_COMMENTS_TOO)\n";
   open( DAT, $CFGFILE ) or die "Cant read configuration file $CFGFILE : $!\n";

   my(@cfg_file_read_errors);
   my($found_first_comment_line)=0;
   while( <DAT> ) {
			chomp;
			chomp;
			s/\s+$//;
			next if /^\s*$/;

			if( ! $found_first_comment_line ) {
				push @file_top_comment,$_;
				$found_first_comment_line=1 if /###############/;
				next;
			}

			# WE ARE IN DATA SECTION NOW
			push @orig_file_lines,$_ 	if $READ_COMMENTS_TOO;

			my($iscomment)=1 if /^#/;
			next if $iscomment and ! $READ_COMMENTS_TOO;
			s/^#\s*//;

			push @orig_file_lines,$_ unless $READ_COMMENTS_TOO;

        	my($host,$port,$orig_logical_name,$desc)=split(/\s+/,$_,4);

        	# skip the line unless its fully formed
        	if( ! defined $orig_logical_name or $orig_logical_name=~/^\s*$/ or ! defined $host or ! defined $port ) {
				push @cfg_file_read_errors,$_;
				next;
			}

			$host =~ s/\.[a-z\.]+$//;		# remove .xxx.com type formats
			my($key)=$host.$port;
			push @orig_host_port_keys,$key;
			$servernm_by_hostport{$key}=$host;
			$orig_host_port_line{$key}=$_;
			$orig_host_port_is_commented{$key}=$iscomment;
			$orig_portnum{$key}=$port;
			$orig_logical_name{$key}=$orig_logical_name;
			$desc=~s/\s+$//;
			chomp $desc;
			$desc="" unless defined $desc;
			$orig_description{$key}=$desc;
   }
   close(DAT);

   die "ERROR NO LINE WITH ############## FOUND IN $CFGFILE" unless $found_first_comment_line;
   # print "Finished Configuration File $CFGFILE\n" if $DEBUG;

   if( $#cfg_file_read_errors>=0  and $DEBUG ) {
   	warn "Warning : bad config line (needs 4 values)\n==> [".join(" \]\n==> \[",@cfg_file_read_errors)."\n";
   }

   print " => Read ",($#orig_host_port_keys+1)," Existing Port Ping Definitions\n";
   #foreach ( @orig_host_port_keys ) { print "-- DBG DBG $_\n"; }
   #exit(0);
}

sub complain
{
   my(@downlist)= @_;
   return unless @downlist;
   foreach my $key (@downlist ) {
		print "Service $long_service_desc{$key} not responding\n";
   }
}

my(%CHECK_UP_QUICK_RESULTS);
sub check_up_quick {
	my($name,$port,$timeout)=@_;

	return $CHECK_UP_QUICK_RESULTS{$name."|".$port} if defined $CHECK_UP_QUICK_RESULTS{$name."|".$port};
	$timeout=1 unless $timeout;
   #local $SIG{__WARN__} = sub { };
   local $SIG{ALRM} = sub { die "timeout"; };
	$CHECK_UP_QUICK_RESULTS{$name."|".$port} = 0;

	my($connection);
	eval {
		alarm(2*$timeout);
   	$connection = IO::Socket::INET->new(Proto=>'tcp', PeerAddr=>$name, PeerPort=>$port, Timeout=>$timeout);
   	alarm(0);
	};
	if( $@ ) {
		# print "DBG DBG $@\n";
		if( $@=~/timeout/ ) {
			return 0;
		} else {
			return 0;
			alarm(0);
		}
	}

   return 0 unless $connection;
   $CHECK_UP_QUICK_RESULTS{$name."|".$port} = 1;
	return 1;
}

sub check_up
{
   my($key)= @_;
   my @sock_config = ( Proto => 'tcp', PeerAddr  => $servernm_by_hostport{$key}, PeerPort => $orig_portnum{$key}, Timeout => 30);

   my $desc;
   if( $orig_description{$key} ne "" ) {
   	chomp $orig_description{$key};
   	$desc=$orig_logical_name{$key}." (".$orig_description{$key}."\)  \[".$servernm_by_hostport{$key}.":".$orig_portnum{$key}."]";
   } else {
   	$desc=$orig_logical_name{$key}."  [".$servernm_by_hostport{$key}.":".$orig_portnum{$key}."]";
   }
   #print "DBG: check_up() Addr=$servernm_by_hostport{$key} Port=$orig_portnum{$key}\n";
   #print "DBG: logname=$orig_logical_name{$key}\n";
   #print "DBG: descs=$orig_description{$key}\n";
   #print "DBG: desc=$desc\n";

   local $SIG{__WARN__} = sub { };
   my $connection = IO::Socket::INET->new(@sock_config);
   if( !$connection ) {
		printf "   pinging %60.60s:  failed\n",$desc;
		$long_service_desc{$key}=$desc;

		my($txt)= "Port Ping Failed: ".$desc;
		MlpHeartbeat(
		   -monitor_program => $BATCHID,
			-debug => $DEBUG,
			-system=> $orig_logical_name{$key},
			-subsystem=> $orig_description{$key},
			-state=> "ERROR",
			-message_text=> $txt ) if defined $LOGALARM;
      	return 0;
   } else {
		printf "   pinging %60.60s:  okay\n",$desc;
		close $connection;
		my($txt)= "Port Ping Ok: ".$desc;
		MlpHeartbeat(
		-monitor_program => $BATCHID,
			-debug => $DEBUG,
			-system=> $orig_logical_name{$key},
			-subsystem=> $orig_description{$key},
			-state=> "OK",
			-message_text=> $txt) if defined $LOGALARM;
	      	return 1;
   }
}

__END__

=head1 NAME

port_monitor.pl - utility to ping ports

=head2 USAGE

	port_monitor.pl -USA_USER -SSERVER -PSA_PASS -ttime

=head2 SYNTAX

Usage: port_monitor.pl [--LOGALARM] [--DEBUG] --SLEEPTIME=secs

   --CFGFILE=FILENAME
   --SYSTEM=SYS    [ returns all responses just for this system ]
   --DEBUG
   --LOGALARM
   --ITERATIONS=num (loop num times slepeing SLEEPTIME seconds betwen loops)

   --GENERATE       - Generate/Update config file
   --INTERFACE_FILE=FILENAME
   --SLEEPTIME=SECONDS

Only loops if SLEEPTIME defined > 0
The system sends an event when state changes

=head2 OTHER OPTIONS

	--GENERATE

Generate a data file if one does not exist using your sybase interface file.  The
optional --INTEFACE_FILE parameter can help here

	--SYSTEM

Generates a listing of all ports that respond between 1 and 2000.  This has some usefulness.

=head2 DESCRIPTION

 Checks ports as needed

 By default reads configuration file port_monitor.dat in the conf directory

=cut

