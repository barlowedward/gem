#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use File::Basename;

use vars qw( $TYPE $USER $SERVER $PASSWORD $opt_d);

BEGIN {
   if( ! defined $ENV{SYBASE} or ! -d $ENV{SYBASE} ) {
      if( -d "/export/home/sybase" ) {
         $ENV{SYBASE}="/export/home/sybase";
      } else {
         $ENV{SYBASE}="/apps/sybase";
      }
   }
}

sub usage
{
	print @_;
	print basename($0)." -USER=SA_USER -SERVER=SERVER -PASSWORD=SA_PASS [-debug] -TYPE=Sybase|ODBC\n";

   return "\n";
}

$| =1; 				# unbuffered standard output

# change directory to the directory that contains THIS file
my($curdir)=cd_home();

die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"USER=s"	=> \$USER ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"TYPE=s" 	=> \$TYPE ,
		"DEBUG"      	=> \$opt_d );
die usage("Must pass server\n" )	unless defined $SERVER;
die usage("Must pass username\n" )	unless defined $USER;
die usage("Must pass password\n" )	unless defined $PASSWORD;

$TYPE="Sybase" unless defined $TYPE;

#
# CONNECT TO THE DATABASE
#
debug( "Connecting\n" );
$TYPE="Sybase" unless defined $TYPE;
my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD,-type=>$TYPE);
die "Cant connect to $SERVER as $USER\n" if ! $rc;

debug("Connected\n");

my $sql = "exec sp_who";
debug( "query  is $sql\n" );
foreach( dbi_query(-db=>"master",-query=>$sql)) {
	my @dat=dbi_decode_row($_);
	print "Query Returned : ",join(" ",@dat),"\n";
}

debug( "program completed successfully.\n" );
dbi_disconnect();
exit(0);

sub debug { print @_ if defined $opt_d; }

__END__

=head1 NAME

sample_db_script.pl - sample utility

=head2 USAGE

	sample_db_script.pl -USA_USER -SSERVER -PSA_PASS -debug

=head2 DESCRIPTION

	[ insert your description here ]

=cut
