#
# TEMPLATE IS MY STANDARD MODULE FOR GEM TEMPLATE
#
use strict;
use Getopt::Long;
use CommonFunc;

my($PROGRAM_DESCRIPTION)="Procedure Library Installer";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions( "DEBUG"	=> \$DEBUG );

print "$0 - v",$VERSION,"\n";
print "   Reading gem.dat\n";
my($GemConfig)=read_gemconfig(-debug=>$DEBUG);
# START BODY


# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "SUCCESSFUL: $PROGRAM_DESCRIPTION\n";
close_gemconfig();
exit(0);

sub validate_console {

	if( $GemConfig->{documenter_do_copy} eq "N" ) {
	 	 $GemConfig->{EXTERNAL_WEB_SERVER_HOSTNAME} ="";
	 	 $GemConfig->{EXTERNAL_WEB_SERVER_DIRECTORY}="";
	 	 return "";
	 }

	if( ! $GemConfig->{publishing_monitor_server} ) {
 		return "Console Setup Error - Must select a server to publish from";
 	}

 	if( is_nt() ) {
 		if( $GemConfig->{publishing_monitor_server} eq "WIN32" ) {
 			if( ! -d $GemConfig->{EXTERNAL_WEB_SERVER_DIRECTORY} ) {
 				return "Console Setup Error - Copy is defined but Destination Directory ($GemConfig->{EXTERNAL_WEB_SERVER_DIRECTORY}) is not a directory";
 			}
 		}
 	} else {
 		if( $GemConfig->{publishing_monitor_server} eq "UNIX" ) {
 				# SHOULD VALIDATE HERE
 		}
 	}

 	return "Product Console\nDirectory must be defined."
			unless defined $GemConfig->{EXTERNAL_WEB_SERVER_DIRECTORY}
			and $GemConfig->{EXTERNAL_WEB_SERVER_DIRECTORY} !~ /^\s*$/;

	return "Product Console\nHostname must be defined."
			unless defined $GemConfig->{EXTERNAL_WEB_SERVER_HOSTNAME}
			and $GemConfig->{EXTERNAL_WEB_SERVER_HOSTNAME} !~ /^\s*$/
			and $GemConfig->{publishing_monitor_server} eq "UNIX";

	return "";
}