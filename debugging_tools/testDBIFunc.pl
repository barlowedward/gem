#!/usr/local/bin/perl-5.6.1

# THIS WRITES AN ERROR TO THE ALARM DB AND THEN DELETES IT!

use lib qw(/apps/sybmon/dev/lib);

use strict;
use DBIFunc;
use MlpAlarm;

my($DEBUG);

my($server,$login,$password,$db)=MlpGetVars();
my($server_type)="Sybase";

MlpHeartbeat( -monitor_program => "testmsg", -system=>"testmsg", -state=>"OK" );

print "SERVER	= $server \n";
print "LOGIN	= $login \n";
print "PASSWORD= $password \n";
print "DB		= $db \n";

die "Server Not Found In Alarms Module MlpAlarms	\n" unless defined $server;
die "Login Not Found In Alarms Module MlpAlarms	   \n" unless defined $login;
die "Password Not Found In Alarms Module MlpAlarms \n" unless defined $password;
die "Database Not Found In Alarms Module MlpAlarms \n" unless defined $db;

my($rc) = dbi_connect(-srv=>$server, -login=>$login, -password=>$password,
		-debug=>$DEBUG, -die_on_error=>0);
if( ! $rc ) {
	print "dbi_connect() Failed\n";
	print "Monitoring Error: Cant Connect To Server -  Marking Connection Failed\nUnable to connect to $server_type server $server as login $login\nHost=".hostname()."\nPERL=$^X \nSYBASE=$ENV{SYBASE}\nerror number=".$DBI::err."\nerrstr=".$DBI::errstr,"\n";
	die "Unable to connect to $server_type server $server: err=".$DBI::err." errstr=".$DBI::errstr unless $rc;	
};

dbi_set_debug(1);
#my($connection)=$rc;
print "Connected as $login / $password - Trying to use $db\n";
$rc=dbi_use_database($db);
print "Use Db Failed to Use $db\n" if $rc == 0;

my($query)="Select * from Heartbeat where monitor_program='testmsg'";
print "$query\n";
foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG )) { 
	print "Test Message: ",join(" ",dbi_decode_row($_)),"\n";
}

my($query)="Delete Heartbeat where monitor_program='testmsg'";
print "$query\n";
foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$DEBUG )) { 
	print "WARNING QUERY RETURNED: ",join(" ",dbi_decode_row($_)),"\n";
}

my($typ,@db)=dbi_parse_opt_D(undef,0,0,1);
print "DATABASES:",@db,"\n";
