use lib qw(.);

use strict;
use MlpAlarm;
use CommonFunc;

$|=1;

#die "Unable to Determine Login - No USERDOMAIN\n" unless $ENV{USERDOMAIN};
#my($LOGON_USER)=$ENV{USERDOMAIN}."\\".$ENV{USERNAME} ;
my($LOGON_USER)=$ENV{USERNAME}||$ENV{USER} ;
die "Unable to Determine Login - No USERNAME\n" unless $LOGON_USER;

print "Web Administration Command Line Interface\n\n";
print "Your Id = $LOGON_USER\n";

my($rc)=MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>1);
print "Your access to The System is: $rc\n";

my(@rc)=MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>0);
foreach( @rc ) {
	print "- You have permission ",i($_),"\n" unless /^Dflt~([a-zA-Z_]+):(.+)/;
}

print "Would you like to give $LOGON_USER web administrator permissions?\n";
my $x;
$x=<STDIN>;
chomp $x;

if( $x =~ /^y/i ) {
	print "\nAdding Login $LOGON_USER\n";

#WINDOWS SYSADMIN
#DBA ADMIN
#UNIX SYSADMIN
#DASHBOARD POWERUSER
#DASHBOARD POWERUSER
#APPROVER
#DASHBOARD ADMINISTRATOR
#DEMO ALL FEATURES
#SYSTEMPOWERUSER
#AUDITOR

	my($q)="insert INV_systemuser ( samid, permission ) select '".$LOGON_USER."','DASHBOARD ADMINISTRATOR'";
	_querynoresults($q);

#	my($q)="insert INV_systemuser ( samid, permission ) select '".$LOGON_USER."','SHOW DIAGNOSTICS'";
#	_querynoresults($q);

}

