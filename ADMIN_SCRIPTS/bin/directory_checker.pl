#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2009-2011 by Edward Barlow. All rights reserved.

sub usage {
	print @_;
	print "usage $0 --dir=<dir> --days=<days> --batchid=<id>
	alerts the contents of any .err files
	alerts .log files older than days
	removes 0 sized .log and .err files
	\n";
	exit(0);
}

use strict;
use Getopt::Long;
use Repository;
use MlpAlarm;
use CommonHeader; 	# this gives a two hour timeout which we may not want
use File::Basename;
use CommonFunc;

use vars qw( $days $dir $BATCH_ID $DEBUG $GEM );

my($GemConfig);
die usage("") if $#ARGV<0;
$days=0;
$|=1;
die usage("Bad Parameter List\n") unless GetOptions(
	"debug"			=>	\$DEBUG,
	"gem"				=>	\$GEM,
	"days=i"			=> \$days,
	"dir=s" 			=> \$dir,
	"BATCHID=s"		=> \$BATCH_ID,
	"BATCH_ID=s"	=> \$BATCH_ID);

print "directory_checker.pl - version 1.0\n";
print " - processes .log and .err Files\n";
print "\n";
die usage("Must pass --Dir\n") unless $dir or $GEM;

# MlpMonitorStart( -system=>$BATCH_ID )  if defined $BATCH_ID;
# MlpBatchStart( -system=>$BATCH_ID, -monitorjob=>1 ) if defined $BATCH_ID;
MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -AGENT_TYPE=>'Gem Monitor' ) if defined $BATCH_ID;

$BATCH_ID="directory_checker" unless $BATCH_ID;

if( $GEM ) {
	$GemConfig	=	read_gemconfig( -debug=>$DEBUG, -nodie=>1, -READONLY=>1 );
#	foreach (keys %$GemConfig ) {
#		print "keys ",$_," ",$GemConfig->{$_},"\n";
#	}

	my($root)=get_gem_root_dir();
	print "GEM ROOT DIRECTORY is $root\n\n";
	die "Root is not a directory\n" unless -d $root;

	my($d)=$root."/data/GEM_BATCHJOB_LOGS";
	die "$d is not a directory\n" unless -d $d;
	print "##############################################\n";
	print "# CHECKING $d\n";
	print "##############################################\n";
	print join("\n",check_a_directory($d,14));

	my($d)=$root."/data/cronlogs";
	die "$d is not a directory\n" unless -d $d;
	print "##############################################\n";
	print "# CHECKING $d\n";
	print "##############################################\n";
	print join("\n",check_a_directory($d,14));

	my($d)=$root."/data/html_output";
	die "$d is not a directory\n" unless -d $d;
	print "##############################################\n";
	print "# CHECKING $d\n";
	print "##############################################\n";
	print join("\n",check_a_directory($d,14));

} elsif( $dir ) {
	print join("\n",check_a_directory($dir,$days));
} else {
	die "ERROR - MUST pass --DIR or --GEM\n";
}
print "Completed \n";
#MlpBatchDone( ) if defined $BATCH_ID;
MlpBatchJobEnd(-EXIT_CODE => 'OK') if defined $BATCH_ID;

exit(0);

sub check_a_directory {
	my($dir,$days)=@_;
	my($db)=basename($dir);
	print "Checking --DIR=$dir\n";

	die "Directory $dir is not a directory\n" unless -d $dir;
	opendir(DIR,$dir) || die("Cant open directory $dir for reading\n");
	my(@d)=grep((!/^\./ and ! -d $dir.$_), readdir(DIR));
	closedir DIR;

	print "\n";
	printf "%5s %45s %5s %9s\n","State","File","Age","Size";
	my(%ALERTSTATE,%ALERTMSG,%ERRFILEEXISTS);
	foreach (sort @d) {
		next unless /.log$/ or /.err$/;
		my($base)=basename($_);
		$base =~ s/.log$//;
		$base =~ s/.err$//;
		my($state)="OK";
		my($age) = -M "$dir/$_";
		$age*=100;
		$age=int($age)/100;
		$state="ERROR" if /.log$/ and $age>$days;
		my($size)= -s "$dir/$_";
		$state="ERROR" if /.err$/ and $size>0;
		if( $ALERTSTATE{$base} ) {
			if( $state eq "ERROR" ) {
				$ALERTSTATE{$base}="ERROR";
				my($msg);
				if( /.log$/ ) {
					$ALERTMSG{$base} .= "File $dir/$_ is $age days old\n";
				} else {
					$ALERTMSG{$base} .= readfile($dir,$_) if $size;
					$ERRFILEEXISTS{$base}=1 if $size;
				}
			}
		} else {
			$ALERTSTATE{$base}=$state;
			if( /.log$/ ) {
				$ALERTMSG{$base}="File $dir/$_ is $age days old\n";
			} else {
				$ALERTMSG{$base}=readfile($dir,$_) if $size;
				$ERRFILEEXISTS{$base}=1 if $size;
			}
		}
		printf "%5s %45s %5s %9s\n",$state,$_,$age,$size if ! $GEM or $state eq "ERROR";
		unlink "$dir/$_" if /.err$/ and $size==0;
	}

	foreach ( keys %ALERTSTATE ) {
#		$ALERTMSG{$_} = "$db Error File Exists: ".$ALERTMSG{$_} if $ERRFILEEXISTS{$_};
#		$ALERTMSG{$_} = substr($ALERTMSG{$_},0,255);
#		if( $ALERTSTATE{$_} eq "OK" ) {
#			MlpHeartbeat(
#				-monitor_program=>$BATCH_ID,
#				-system=>"GEM",
#				-subsystem=>$_,
#				-state=>"OK",
#				#-debug=>$x{-debug},
#				#-batchjob=>$x{-batchjob},
#				-message_text=>$ALERTMSG{$_});
#		} else {
#			MlpHeartbeat(
#				-monitor_program=>$BATCH_ID,
#				-system=>"GEM",
#				-subsystem=>$_,
#				-state=>"ERROR",
#				#-debug=>$x{-debug},
#				#-batchjob=>$x{-batchjob},
#				-message_text=>$ALERTMSG{$_});

		if( $ALERTSTATE{$_} ne "OK" ) {
			print "processing mail message for key $_\n";

			$ALERTMSG{$_} = "<b>$db/$_ Error File Exists</b><br>\n<PRE>".$ALERTMSG{$_}."</PRE>" if $ERRFILEEXISTS{$_};
			$ALERTMSG{$_} = substr($ALERTMSG{$_},0,4096);
			my($MAIL_TO	 )		= $GemConfig->{ADMIN_EMAIL};
			my($MAIL_FROM)		= $GemConfig->{ALARM_MAIL} ;
			my($MAIL_HOST)		= $GemConfig->{MAIL_HOST} 	;
			send_mail_message($MAIL_TO,$MAIL_FROM,$MAIL_HOST,"Error: $db/$_",$ALERTMSG{$_});
		}
	}
	print "\n";
	return ();
}

sub readfile {
	my($dir,$file)=@_;
	open(F,$dir."/".$file) or die "Cant read file $file $!\n";
	undef $/;
	my $txt=<F>;  # slurp...
	close(F);
	return $txt;
}

