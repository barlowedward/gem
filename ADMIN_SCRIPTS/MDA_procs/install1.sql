-- First, ensure that the configuration parameter
-- 'enable cis' is set to 1 (if not, an ASE
-- restart is needed)
sp_configure 'enable cis', 1
go


-- Add 'loopback' server name alias (assuming @@servername
-- is also defined in the interfaces file)
use master
go
sp_addserver loopback, null, @@servername
go

-- Test this configuration:
set cis_rpc_handling on
go
--
-- Alternatively, run:
--     sp_configure 'cis rpc handling', 1
-- ...and disconnect/reconnect your session

exec loopback...sp_who  -- note: 3 dots!
go

