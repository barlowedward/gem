#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

##########################
#
# DEPRICATED CODE LINE
#   - FOR EXAMPLE PURPOSES ONLY
#
##########################

use strict;
use Getopt::Long;
use Sys::Hostname;
use MIME::Lite;

my($VERSION)="1.0";
my($MAILTO,$PAGETO,$MESSAGE,$SUBJECT,$DEBUG,$FROM,$SMTPMAILSERVER);

# List of beeper ids
# Pagers starting with 800 or 888 are last 7 numbers only
# Pagers with 877 are all 10 numbers
#

sub usage
{
	print "send_a_page.pl --FROM=from --DEBUG --MAILTO=a,b,c -PAGETO=a,b,c --MESSAGE=msg --SUBJECT=subj \n";
   return "\n";
}

$| =1;		# dont buffer standard output

die usage() unless GetOptions(
		"MAILTO=s"  => \$MAILTO,
		"PAGETO=s"  => \$PAGETO,
		"FROM=s"    => \$FROM,
		"SMTPMAILSERVER=s"    => \$SMTPMAILSERVER,
		"MESSAGE=s" => \$MESSAGE,
		"SUBJECT=s" => \$SUBJECT,
		"DEBUG"     => \$DEBUG );

print "send_a_page.pl - version $VERSION\n" if defined $DEBUG;
die usage() unless defined $MESSAGE;
$SUBJECT="(No Errors) Alarm Message" if ! defined $SUBJECT;
$FROM=$ENV{LOGNAME} unless $FROM;

if( defined $MAILTO ) {
	my($msg)=MIME::Lite->new(
		To=>$MAILTO,
		From=>$FROM,
		Subject=>$SUBJECT,
		Type=>'multipart/related' );
	$msg->attach(Type=>'text/html', Data=>$MESSAGE);
	if( defined $ENV{PROCESSOR_LEVEL} ) {
		# windows SMTPSERVER = mail1.abc.com
		$msg->send('smtp',$SMTPMAILSERVER) || die "You DONT have MAIL!!!";
	} else {
		unix
		$msg->send() || die "You DONT have MAIL!!!";
	}
} else {
	print "Skipping Mail Step as --MAILTO undefined\n"
		if defined $DEBUG;
}
if( defined $PAGETO ) {
	die "No Beep Program in /apps/bin/beep" unless -x "/apps/bin/beep";
	foreach my $target ( split(/,/,$PAGETO) ) {
		my($pager_id)=$pagers{lc($target)};
		die "ERROR: Invalid Pager - No Data For $target\n"
			unless $pager_id;

		system("/apps/bin/beep $pager_id \'$MESSAGE\'");
	}
}
