use strict;
use Win32::TaskScheduler;

my $scheduler = Win32::TaskScheduler-> New();

my %hourTrigger = (
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'TriggerType' =>  $scheduler->TASK_EVENT_TRIGGER_AT_SYSTEMSTART,
);

my %dailyTrigger = (
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'StartHour' => 14,
 'StartMinute' => 10,
 'TriggerType' =>  $scheduler->TASK_TIME_TRIGGER_DAILY,
 'Type' => {
		'DaysInterval'=>3,
	}
);

my %weeklyTrigger = (
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'StartHour' => 14,
 'StartMinute' => 10,
 'TriggerType' =>  $scheduler->TASK_TIME_TRIGGER_WEEKLY,
 'Type' => {
		'DaysOfTheWeek'=>$scheduler->TASK_SUNDAY,
		'DaysInterval'=>7,
	}
);

my %bootTrigger = (
 'BeginYear' =>  2004,
 'BeginMonth' =>  12,
 'BeginDay' =>  1,
 'TriggerType' =>  $scheduler->TASK_EVENT_TRIGGER_AT_SYSTEMSTART,
);

sub schedule_it {
	my(%args)=@_;
	die "Must pass -name to schedule_it()" unless defined $args{-name};
	die "Must pass -name to schedule_it()" unless defined $args{-name};
	
	my($trigger);
	if( $args{schedule eq "hourly" ) {
		$trigger = \%hourTrigger;
	} elsif( $args{schedule eq "weekly" ) {
		$trigger = \%weeklyTrigger;
	} elsif( $args{schedule eq "daily" ) {
		$trigger = \%dailyTrigger;
	} elsif( $args{schedule eq "boot" ) {
		$trigger = \%bootTrigger;
	} else {
	die "schedule must be hourly,weekly,daily,or boot\n";
	}

	$scheduler-> NewWorkItem($args{job_name,$trigger);
   $scheduler-> CreateTrigger($trigger);
   #$scheduler-> SetApplicationName("${installdir}$exefile");
   #$scheduler-> SetWorkingDirectory($installdir);
   $scheduler-> SetAccountInformation("","");  #the SYSTEM account will be used
   $scheduler-> Save();
}

#schedule_it("WEEKLY TASK","weekly",1);
#schedule_it("DAILY TASK","daily",1);
#schedule_it("HOURLY TASK","hourly",1);

#my($targetcomputer)="BROKERSQL1";
#if( defined $targetcomputer ) {
	#my($rc)=$scheduler->SetTargetComputer('\\'.$targetcomputer);
	#die "Cant set target to $targetcomputer" unless $rc==1;
#}

get_task_list();

sub get_task_list {
	my @tasks = $scheduler->Enum();
	my(%task_list);
	foreach my $task (@tasks) {
		my(%results);
      $scheduler->Activate($task);


		$results{Creator}=$scheduler->GetCreator();
		$results{Comment}=$scheduler->GetComment();
		$results{UserName} = $scheduler->GetAccountInformation();
		$results{Application} = $scheduler->GetApplicationName();
		$results{Parameters} = $scheduler->GetParameters();
		$results{Working_Directory} = $scheduler->GetWorkingDirectory();

		my($p);
		$scheduler->GetPriority($p);
		$results{Priority}=$p;

		$p=$scheduler->GetFlags();
		$results{Flags} ="";
		$results{Flags}.="INTERACTIVE " if $p=~$scheduler->TASK_FLAG_INTERACTIVE;
		$results{Flags}.="DISABLED " if $p=~$scheduler->TASK_FLAG_DISABLED;
		$results{Flags}.="DELETE_WHEN_DONE " if $p=~$scheduler->TASK_FLAG_DELETE_WHEN_DONE;
		$results{Flags}.="HIDDEN " if $p=~$scheduler->TASK_FLAG_HIDDEN;

      $scheduler->GetStatus($p);
		$results{Status}=$p;
      $scheduler->GetExitCode($p);
		$results{ExitCode}=$p;

      my(@time)=$scheduler->GetNextRunTime();
      $results{Next_Run_Time}= $time[7].$time[6].$time[4]." $time[3]:$time[2]:$time[1]";
      $results{Next_Run_Time}= "Disabled" 
			if $results{Next_Run_Time} eq "000 0:0:0";

      @time=$scheduler->GetMostRecentRunTime();
      $results{Last_Run_Time}= $time[7].$time[6].$time[4]." $time[3]:$time[2]:$time[1]";
      $results{Last_Run_Time}= "Never" 
			if $results{Last_Run_Time} eq "000 0:0:0";
	
		$results{MaxRunTime} = $scheduler->GetMaxRunTime();

		print "###########################\n";
		print "# TASK : $task\n";
		print "###########################\n";
	   foreach (sort keys %results) {
			  print "$_ -> $results{$_} \n";
	   }

		my($trigger_count)=$scheduler->GetTriggerCount()-1;
		for my $i (0..$trigger_count) {
			my(%trigger);
			next if $scheduler->GetTrigger($i,\%trigger)==0;
			foreach (keys %trigger) {
				print "   trigger $i $_ $trigger{$_} \n";
			}
	   }
   }
}
