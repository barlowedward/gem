#!/bin/bash

run ()
{
  grant.pl badpass01 SYBMON gemalarms INV_$1 select public all gemalarms
}

run attribute
run cleanup
run component
run constant
run contact
run possible_attribute
