#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# -*-Perl-*-
# $Id: sybmon.pl,v 1.4 2000/01/15 18:05:52 dowen Exp $
#
# Copyright (c) 1999, 2000    David Owen
#
#   You may copy this under the terms of the GNU General Public License,
#   or the Artistic License, copies of which should have accompanied
#   your Perl kit.
#
# sybmon.pl   A process monitoring script for Sybase.
#
# Written by:   Written by David Owen (dowen@midsomer.org)
# Last Mods:    14th January 2000
#
# Usage:        sybmon.pl [-U username] [-P password] [-S server] [-D]
#                         [-s seconds]
#
#                 -U         Sybase account to connect to server with (defaults
#                            to $USER).
#                 -P         Password of above account (prompts if not supplied).
#                 -S         Server to connect to (defaults to DSQUERY)
#                 -s seconds Refresh interval in seconds.  (0 is same as "freeze",
#                            special value of -1 for "sa_role's" only says "just
#                            hammer it!
#                 -D         Enable debugging.
#
#
#                 -I         An alternative interfaces file (defaults to
#                            $SYBASE/interfaces on Unix and $SYBASE/ini/sql.ini
#                            in W32).
#
# [-I interfaces_file]
# -geom ?
#
# Known Faults: Cannot change font, and will not save options to an options file.
#               I also know that different
#               versions of Sybase have slightly different column formats for
#               "sp_who" (fid is latest change).  I would like to be able to
#               organise by "fid" to show parent and child processes.
#
# Changes:      99/03/12 - David Owen      - Initial version.
#               00/01/13 - David Owen      - Added the ability to resize the windows.
#
# Contact:      If you wish to report a fault, suggest a change, offer me a job,
#               please feel free to do so at dowen@midsomer.org.
#
#------------------------------------------------------------------------------
#

require 5.005;             # This is a requirement for Tk.
use Tk;

use Sybase::CTlib;
use Sybase::Login;
use Carp;
use English;
use Getopt::Std;
use strict;
use vars qw($opt_U $opt_P $opt_S $opt_D $opt_s);

### Global variables.

my($hostname, $appname);
my($lineLength, $refresh, $defRefresh, $displayType, $sysProcs, $freeze, $logging);
my($textHeight);

# Status line variables.

my($linesOfText, $activeUsers, $blockedUsers, $sleepingUsers, $suspendedUsers, $blockingUsers);
my($newSrv, $srv, @servers);
my($logFile);
my($fontSize);
my($Login, $pwd, $uid, $User);

my($header1, $header2, $header3, $hdr);

### Windows and frames.

my($f, $f2, $f3, $statusFrame, $menf);

my($ccw);      # Colour chooser window.
my(%ccwFrames, %ccwLabels);

my($f1_ccw, $f2_ccw, $f3_ccw);
my($f1Lbl_ccw, $f2Lbl_ccw, $f3Lbl_ccw);

# General variables

my($freezeB, $loggingB);
my($status1, $status2);
my($t);
my(%users, %serverConnections, %upTime, %userRole);
my($afterID);
my($lineNumber);
my($now_time);
my($rc,$restype, @dat);
my($indxStart, $indxEnd, $tagName, $frmText, $logTag);
my($menu, @sortOptions, $sortItem, $sortArg, $lastSort, $descend);
my(@loggingOptions, $loggingSelection);
my($filterOptions);
my($men1, $men2, $men3, $men4);
my($dropDown1, $dropDown2);
my($item);
my($MSDros);
my($DEBUG);
my($rcFile);

### Colour type definitions for...

# Colours

my(%defaultColours, %currentColours, %ccwColours, @colourNames);

$DEBUG = 0;  # Used to turn on the callback printouts.  Maybe they should be windows?

$hostname   = "sybmon";
$appname    = "sybmon";

# Initialise application default variables.

$lineLength   = 120;        # Display width for text box
$displayType  =   0;        # 0:Processes 1:Locks 2:Blocked Processes
$sysProcs     =   1;        # 1:Display System Processes 0:Don't display them...
$freeze       =   0;        # 1:System frozen (no refreshes) 0:Not frozen
$logging      =   0;        # 0:Do not log results 1:Log results.
$linesOfText  =   1;        # Used to clean the display and other things.
$activeUsers  =   0;        # Number of active connections to DB.
$blockedUsers =   0;        # Number of blocked connections to DB.
$newSrv       =  "";        # Server selection variable.  This is the server the user
                            # wants to move to.
$srv          =  "";        # Currently connected server.

if ($OSNAME =~ /^MSWin/) {
    $MSDros = 1; # True
}
else {
    $MSDros = 0;
}

# Grab any command line options.

getopt('DU:S:P:s:');

# Set up the rc or ini file.

if ($MSDros) {
    # Just use a simple $HOME/sybmon.ini to start with.
    $rcFile = "$ENV{'HOME'}/sybmon.ini";
} else {
    # .sybmonrc is the default.
    $rcFile = "$ENV{'HOME'}/.sybmonrc";
}

# Initialise user preferences.

&getPreferences;

# If the user provided a number with "-s", update the refresh rate.

if (defined($opt_s)) {
    if ($opt_s =~ /\A\d+\Z/) {  # Ignore all but numeric rates (Don't allow -1 from startup.)
        $refresh = $opt_s;
    }
    else {
        &usage("Refresh rates should be numberic.");
    }
} else {
    $refresh = $defRefresh;
}

# Reset the debug flag if -D entered.

if (defined($opt_D)) {
    $DEBUG = 1;
}

# Header for process monitor
$header1 =
    "PID Status       Login-Name      Hostname   DB-Name     Command             CPU-Time Phys-I/O Mem-Usage Blocked By";

# Header for lock monitor
$header2 =
    "PID Lock-Type    Table-Name           Page       DB-Name     Class              Login-Name";

# Header for block monitor
$header3 =
    "PID Status      Blocked-User  Blocking-User  BlkdHost   DB-Name     Table-Name          Time-Blocked  Blocking-PID";

# Build the sort variables

$sortItem = "<None>";             # Button name
$sortArg  = 0;                    # 0 => no sort
$lastSort = 0;
$descend  = 0;
push(@sortOptions, "<None>");
push(@sortOptions, "Pid");
push(@sortOptions, "Status");
push(@sortOptions, "Login Name");
push(@sortOptions, "Host Name");
push(@sortOptions, "Database");
push(@sortOptions, "Command");
push(@sortOptions, "CPU Time");
push(@sortOptions, "Phys I/O");
push(@sortOptions, "Mem Usage");
push(@sortOptions, "Blocked By");

# Initialise the logging stuff.

$loggingSelection = "<None>";
push(@loggingOptions, "<None>");
push(@loggingOptions, "Blocks Only");
push(@loggingOptions, "All");

$logFile = "c:/temp/log.log";  # Need to find a way to do logging that is useful
                               # and has a good "look & feel"

# Read in the server list from the INI file or interfaces file.  This is needed
# for both the initial Sybase connection and subsequently by Tk

@servers = &readServerList;

## The Tk stuff.  This may be a little bit simplistic, since I have stolen most of
##                it from books and other peoples examples.  I am getting there, but
##                don't slam me!
##

# Create the main window and stop it being resizable (next release!).

if ($MSDros) {  # Some flavour of MS Windows
    $fontSize = 8;
}
else {                      # Currently the rest = UNIX.
    $fontSize = 12;
}

# Declare and create the main window.

my $mw = MainWindow->new;

# If for some reason resizing does not work in your environment (fonts or
# something, uncommenting the following line puts the app back into 1024x768
# mode.
#$mw->resizable("no", "no");

# Seems to take this a little bit with a pinch of salt :-)

$mw->minsize(800, 200);

# Fonts and colours

$mw->fontCreate('niceText',
                -family => 'courier',
                -size   => $fontSize);

# Login, either using the command line options or with the pop-up screen.  I am
# currently using the Login widget from CPAN, but this uses a strange idea of
# logining into servers dependent upon which databases they have in them.  I
# want to get rid of this sometime, or get the owner to change it or
# *something*.

if (defined($opt_U) && defined($opt_P) && defined($opt_S)) {
    $pwd = $opt_P;
    $uid = $opt_U;
    $srv = $opt_S;
}
else {
    $Login = $mw->Login;

    $Login->addDatabase('master',  @servers );

    $User = (defined($opt_U)) ? $opt_U : $ENV{USER};
    $User = "sa" if !defined($User);

    if (defined($opt_S)) {
        $Login->configure(-User   => $User,
                          -ULabel => 'User Name:',
                          -Server => $opt_S,
                          -Title  => 'Please Login');
    }
    else {
        $Login->configure(-User   => $User,
                          -ULabel => 'User Name:',
                          -Title  => 'Please Login');
    }

    if ($Login->getVerification(-Force => 1)) {
        $pwd = $Login->cget(-Password);
        $uid = $Login->cget(-User);
        $srv = $Login->cget(-Server);
    } else {
        exit;
    }
}

$newSrv = $srv;  # Initialise the switch to server.

$mw->configure(-width => $lineLength);

$menf = $mw->Frame->pack(-side => 'top',
                         -fill => 'x');

$men1 = $menf->Menubutton(-text               => "File",
                          -highlightthickness => 1,
                          -tearoff            => 0,
                          -borderwidth        => 1,
                          -menuitems          => [[ 'command' => "Disconnect",
                                                    -command  => \&spDisconnect],
                                                  [ 'command' => "Preferences...",
                                                    -command  => \&preferences],
                                                    "-",
                                                  [ 'command' => "Exit",
                                                    -command  => sub{exit;}]]);

$men2 = $menf->Menubutton(-text               => "Sort",
                          -borderwidth        => 1,
                          -relief             => "flat",
                          -highlightthickness => 1,
                          -tearoff            => 0);
#
#$men3 = $menf->Menubutton(-text               => "Logging",
#                          -borderwidth        => 1,
#                          -relief             => "flat",
#                          -highlightthickness => 1,
#                          -tearoff            => 0);

# Now build the sort Menu buttons from the array of possible values.

foreach $item (@sortOptions) {
    $men2->radiobutton(-label    => $item,
                       -command  => \&sortBy,
                       -variable => \$sortItem,
                       -value    => $item);
}

#foreach $item (@loggingOptions) {
#    $men3->radiobutton(-label    => $item,
#                       -command  => \&toggleLogging,
#                       -variable => \$loggingSelection,
#                       -value    => $item);
#}

$men1->pack(-side   => "left",
            -anchor => 'w');

$men2->pack(-side   => "left",
            -anchor => 'w');

#$men3->pack(-side   => "left",
#            -anchor => 'w');

# Create a frame to hold the top button bar and then add all of the buttons,
# radio buttons and check boxes (together with an entry field).

$f = $mw->Frame->pack(-side => 'top',
                      -fill => 'x');

$f->Button(-text        => "Refresh",
           -command     => \&spRefresh,
           -borderwidth => 1)->pack(-side => 'left');

$f->Label(-text => "Refresh Rate:")->pack(-side   => 'left',
                                          -anchor => 'w',
                                          -padx   => 5);

$f->Entry(-textvariable => \$refresh,
          -borderwidth  => 1,
          -width        => 3,
          -font         => "niceText")->pack(-side   => 'left',
                                             -anchor => 'w',
                                             -padx   => 5);

$freezeB = $f->Button(-text    => "Freeze",
                      -bd      => 1,
                      -command => \&spFreeze)->pack(-side => 'left');

# Put a nice "grouping" frame around the radio buttons.

$f2 = $f->Frame(-relief      => "groove",
                -borderwidth => 2)->pack(-side => 'right');

$f2->Radiobutton(-text        => "Blocks",
                 -value       => 2,
                 -borderwidth => 1,
                 -command     => \&spWrapper,
                 -variable    => \$displayType)->pack(-side   => 'right',
                                                      -anchor => 'w');
$f2->Radiobutton(-text        => "Locks",
                 -borderwidth => 1,
                 -value       => 1,
                 -command     => \&spWrapper,
                 -variable    => \$displayType)->pack(-side   => 'right',
                                                      -anchor => 'w');
$f2->Radiobutton(-text        => "Processes",
                 -value       => 0,
                 -borderwidth => 1,
                 -command     => \&spWrapper,
                 -variable    => \$displayType)->pack(-side   => 'right',
                                                      -anchor => 'w');

$f->Checkbutton(-variable    => \$sysProcs,
                -text        => "System Procs?",
                -borderwidth => 1,
                -command     => \&spWrapper )->pack(-side   => 'right',
                                                    -anchor => 'w');

# Create a drop down list box for the server list.

$f3 = $f->Frame->pack(-side => 'right',
                      -fill => 'x');

if (@servers < 21) {
    $dropDown1 = $f3->Optionmenu(-textvariable       => \$newSrv,
                                 -options            => [@servers],
                                 -highlightthickness => 1,
                                 -borderwidth        => 1,
                                 -command            => \&switchServer,
                                 -relief             => "raised")->pack(-side   => 'right',
                                                                        -anchor => 'e');
}
else {
    $f3->Button(-text        => "Server",
                -borderwidth => 1,
                -command     => \&listServers)->pack(-side => 'left');
}

# Disable the server drop down if there is only one server in the interfaces/ini file.

$dropDown1->configure(-state => 'disabled') if 1 == @servers;

# Default the header to "monitor processes"

$hdr = $mw->Label(-width  => $lineLength,
                  -anchor => 'w',
                  -text   => $header1,
                  -font   => "niceText")->pack(-side   => "top",
                                               -fill   => "y",
                                               -anchor => 'w',
                                               -padx   => 9);

# Put a frame at the bottom to display the bits of status information that
# we wish to display.

$statusFrame = $mw->Frame->pack(-side => 'bottom',
                                -fill => 'x');

$status1 = $statusFrame->Label(-width  => ($lineLength * 20.0)/30,
                               -relief => "sunken",
                               -bd     => 1,
                               -anchor => 'c')->pack(-side => "left",
                                                     -fill => "y",
                                                     -padx => 2,
                                                     -pady => 1);


$status2 = $statusFrame->Label(-width  => ($lineLength * 9.0) / 30,
                               -relief => "sunken",
                               -bd     => 1,
                               -anchor => 'c')->pack(-side => "right",
                                                     -fill => "y",
                                                     -padx => 2,
                                                     -pady => 1);

# The main window is a text window with optional scroll bars.

$t = $mw->Scrolled("Text",
                   -scrollbars => "osoe")->pack(-side   => "top",
                                                -anchor => 'w',
                                                -expand => 1,
                                                -fill   => 'both');

$t->configure(-height      => $textHeight,
              -width       => $lineLength,
              -foreground  => $currentColours{'defaultFG'},
              -borderwidth => 1,
              -font        => "niceText",
              -background  => $currentColours{'background'},
              -relief      => "sunken",
              -wrap        => "none");

# Add the popup menu that allows the user to sort the data anywhich way.

#$menu = $t->Menu(-tearoff   => 0,
#                  -menuitems => [["command" => "Hello",
#                                  -command  => \&spFreeze ]]);

# Bind the menu to the text box

#$t->bind("<Button-3>", sub { $menu->Popup(-popover => 'cursor')});

# Set the title bar for the main window to reflect the attached server.

$mw->title("User Monitor for $srv");

# Install a couple of callbacks and connect to Sybase.  Most of the callbacks
# are commented out, but they are quite useful for debugging.  One of the next
# releases might try to trap some of these.

ct_callback(CS_CLIENTMSG_CB, \&msg_cb);
ct_callback(CS_SERVERMSG_CB, "srv_cb");

$serverConnections{$srv} = new Sybase::CTlib $uid, $pwd, $srv, $appname,
                                             { CON_PROPS => { CS_HOSTNAME   => $hostname}
                                           };

if (!defined($serverConnections{$srv})) {
    &usage("Could not connect to server $srv.");
}

$users{$srv}             = $uid;

&getRole;

&upTime;  # This is a little lazy, but saves typing it twice.  It is OK to
          # call since we are guaranteed that there is only one server in the
          # list at this stage.

# Ensure that the user is in the master database for viewing, this
# then means that you can monitor DB loads of your default DB :-)

$serverConnections{$srv}->ct_sql("use master");

# Finally call the wrapper function, that will default to spWho in the first
# instance and then leap into the main Tk loop, ensuring that the main window
# is on top and visible.

$mw->raise();

&spWrapper;

MainLoop;

################################################################################
# Function defintions
#####################

sub spWrapper{

    my $minute;

    # Branches according to display type, calling the appropriate display
    # function.  I am sure that this could be done more efficiently using
    # methods, but this will do for now.

  SWITCH: {
      last SWITCH if $freeze == 1;                       # Don't redisplay if they are on freeze
      last SWITCH if $mw->state() eq "iconic";           # or it is minimized.
      last SWITCH if !defined($serverConnections{$srv}); # or he has disconnected.
      if ($displayType == 0) { $men2->configure(-state => 'active');
                               &spWho; last SWITCH; }
      if ($displayType == 1) { $men2->configure(-state => 'disabled');
                               &spLock; last SWITCH; }
      if ($displayType == 2) { $men2->configure(-state => 'disabled');
                               &spBlock; last SWITCH; }
  }

    # Cancel any outstanding auto scheduled events that may be pending.
    # Remember that this is called by the refresh button as well as
    # automatically.

    if (defined($afterID)) {
        $afterID->cancel();
    }

    # Reset the uptimes once an hour on the hour.  Do this before rescheduling,
    # since that guarantees that we will not have a mutex situation.  This will
    # happen even if the app is minimized.

    (undef,$minute,undef,undef,undef,undef,undef,undef)=localtime(time);

    if (0 == $minute) {
        &upTime;
    }

    # The autoscheduling method is very simplistic: if there is an event
    # already scheduled, cancel it and then schedule another.  This means that
    # the "refresh" button can be hit as many times as the user likes and only
    # one "auto" refresh will remain pending.

    if ($refresh !~ /\A(-1|\d+)\Z/) {  # Reset if non-numeric
        $refresh = $defRefresh;
    }

    if ($refresh == -1 && $userRole{$srv} ne "sa_role") {
        $refresh = $defRefresh;
    }

    if ($refresh > 0) {     # Only schedule if the time is a real number.
        $afterID = $mw->after($refresh * 1000, \&spWrapper);
    }
    elsif (-1 == $refresh) { # If the time period is -1 then thrash the guts out of the machine!
        $afterID = $mw->after(10, \&spWrapper);
    }

}


sub spWho{

    # Deals with the process monitor, effectively calling an enhanced copy of
    # sp_who

    # Reset the main header to correspond to this display.

    $hdr->configure(-text => $header1);

    # I have had to embed the call to 'sp_who' in the code since different
    # versions of Sybase do different things.  (11.5 has different bits!)

    my($selectString) = "

    select spid,
           status,
           loginame=suser_name(suid),
           origname=suser_name(suid),
           hostname,
           blk=convert(char(5),blocked),
           dbname=db_name(dbid),
           cmd,
           cpu,
           physical_io,
           memusage
      from master..sysprocesses ";

    if ($sortArg == 0) {
        $selectString = $selectString . " order by spid,dbname";
    }

    if ($sortArg != 0) {
        $selectString = $selectString . " order by " . $sortArg;
    }

    if ($descend == 1) {
        $selectString = $selectString . " desc"
    }

    $serverConnections{$srv}->ct_execute($selectString);

    # Store the current window position so that we can go back there later.

    my($top, $bottom) = $t->yview();

    # Clear the current display.

    $t->delete("1.0", ++$linesOfText . ".$lineLength");

    $lineNumber    = 0;
    $activeUsers   = 0;
    $blockedUsers  = 0;
    $sleepingUsers = 0;
    $suspendedUsers = 0;

    if ($logging == 1) {
        $now_time = localtime;
        print LOGFILE "Scan started at $now_time\n";
        print LOGFILE "  $header1\n";
    }

    while(($rc = $serverConnections{$srv}->ct_results($restype)) == CS_SUCCEED) {
        next if($restype == CS_CMD_DONE || $restype == CS_CMD_FAIL ||
                $restype == CS_CMD_SUCCEED);

        while(@dat = $serverConnections{$srv}->ct_fetch) {
            if (defined(@dat) && $dat[0] != 0) {
                next if (!$sysProcs && (defined($dat[2]) && length($dat[2])) == 0);

                ++$lineNumber;
                $indxStart = $lineNumber . ".0";
                $indxEnd   = $lineNumber . "." . $lineLength;
                $tagName   = "TAG" . $lineNumber;

                if (!defined($dat[2])) {
                    $dat[2] = " ";
                }

                if ($dat[5] == 0) {
                    $frmText = sprintf(" %3u %-12.12s %-15.15s %-10.10s %-11.11s %-18.18s %9u %8u %9u\n",
                                       $dat[0], $dat[1], $dat[2], $dat[4], $dat[6], $dat[7], $dat[8], $dat[9], $dat[10]);
                }
                else {
                    $frmText = sprintf(" %3d %-12.12s %-15.15s %-10.10s %-11.11s %-18.18s %9d %8d %9d     %3d\n",
                                       $dat[0], $dat[1], $dat[2], $dat[4], $dat[6], $dat[7], $dat[8], $dat[9], $dat[10], $dat[5]);

                }

                if ($logging == 1) {
                    &printLogMessage($frmText);
                }

                $t->insert('end', $frmText);

                if ($dat[1] eq "running     " ||
                    $dat[1] eq "runnable    " ) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName,
                                     -foreground, $currentColours{'running'});
                    ++$activeUsers;
                }

                if ($dat[1] eq "sleeping    " ||
                    $dat[1] eq "background  ") {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'sleeping'});
                    ++$sleepingUsers;
                }

                if ($dat[1] eq "remote i/o  ") {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'remoteIO'});
                    ++$activeUsers;
                }

                if ($dat[1] eq "send sleep  ") {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'sendSleep'});
                }

                if ($dat[1] eq "alarm sleep " ) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'alarmSleep'});
                }

                if ($dat[1] eq "lock sleep  " ||
                    $dat[5] != 0) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'lockSleep'});
                    ++$blockedUsers;
                }

                if ($dat[7] eq "LOG SUSPEND     ") {  # Notice: $dat[7]
                    $logTag = "LOG" . $lineNumber;
                    $t->tagAdd($logTag, $lineNumber . ".56", $lineNumber . ".68");
                    $t->tagConfigure($logTag, -foreground, $currentColours{'logSuspend'});
                    ++$suspendedUsers;
                }

            }
        }

    }

    $linesOfText = $lineNumber;

    $status1->configure(-text => "$users{$srv}/$srv   $linesOfText processes ($activeUsers active, $blockedUsers blocked, $sleepingUsers sleeping, $suspendedUsers log suspended)");
    $status2->configure(-text => "Uptime: $upTime{$srv}");

    $t->yview(moveto => $top);
}

sub spBlock{

    $hdr->configure(-text => $header3);

# I was originally simply going to call "sp_who" and process the results.
# However, with 11.5 onwards Sybase introduced the "fid" column and so this
# causes code that uses absolute column numbers to mis-interpret the results.
# I would like to do a test and then run a different query for different
# versions, organising the output by process tree.

    $serverConnections{$srv}->ct_execute("
        select distinct
               s1.spid
              ,s1.status
              ,waiting=suser_name(s1.suid)
              ,blocking=suser_name(s2.suid)
              ,waithost=isnull(s1.hostname,'')
              ,blk=isnull(convert(char(5),s1.blocked),'')
              ,dbname=isnull(db_name(s1.dbid),'')
              ,s1.time_blocked
              ,table_id = isnull(object_name(l.id, l.dbid),'')
              ,s2.blocked
          from master..sysprocesses s1
              ,master..sysprocesses s2
              ,master..syslocks l
         where s1.blocked != 0
           and s1.blocked  = s2.spid
           and s1.spid    *= l.spid
         order by s1.spid,s1.dbname");

    my($top, $bottom) = $t->yview();

    $t->delete("1.0", ++$linesOfText . ".$lineLength");

    $lineNumber = 0;

    while(($rc = $serverConnections{$srv}->ct_results($restype)) == CS_SUCCEED) {
        next if($restype == CS_CMD_DONE || $restype == CS_CMD_FAIL ||
                $restype == CS_CMD_SUCCEED);

        while(@dat = $serverConnections{$srv}->ct_fetch) {
            if (defined(@dat) && $dat[0] != 0) {
                ++$lineNumber;
                $indxStart = $lineNumber . ".0";
                $indxEnd   = $lineNumber . "." . $lineLength;
                $tagName   = "TAG" . $lineNumber;

                if ($dat[9] == 0) {
                    $dat[9] = "*";
                } else {
                    $dat[9] = " ";
                }

                $frmText = sprintf("%-1.1s%3d %-11.11s %-13.13s %-14.14s %-10.10s %-11.11s %-20.20s %11d           %3d\n",
                                   $dat[9], $dat[0], $dat[1], $dat[2], $dat[3], $dat[4], $dat[6],
                                   $dat[8], $dat[7], $dat[5]);

                $t->insert('end', $frmText);

                if ($dat[1] eq "running     " ||
                    $dat[1] eq "runnable    " ) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'running'});
                }

                if ($dat[1] eq "sleeping    ") {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'sleeping'});
                }

                if ($dat[1] eq "remote i/o  ") {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'remoteIO'});
                }

                if ($dat[1] eq "send sleep  " ||
                    $dat[1] eq "alarm sleep " ) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'sendsleep'});
                }

                if ($dat[1] eq "lock sleep  " ||
                    $dat[5] != 0) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'locksleep'});
                }

            }
        }

    }

    $linesOfText = $lineNumber;

    $status1->configure(-text => "$users{$srv}/$srv   $linesOfText blocks");
    $status2->configure(-text => "Uptime: $upTime{$srv}");

    $t->yview(moveto => $top);

}

sub spLock{

    $hdr->configure(-text => $header2);

    my($top, $bottom) = $t->yview();

    $serverConnections{$srv}->ct_execute(

    "select l.spid,
            locktype = name,
            table_id = object_name(l.id, l.dbid),
            page,
            dbname = db_name(l.dbid),
            class,
            suser_name(s.suid)
      from master..syslocks l,
           master..spt_values v,
           master..sysprocesses s
     where l.type = v.number
       and v.type = 'L'
       and l.spid = s.spid
     order by l.spid, dbname, table_id, locktype, page");

    $t->delete("1.0", ++$linesOfText . ".$lineLength");

    $lineNumber     = 0;
    $blockingUsers  = 0;

    while(($rc = $serverConnections{$srv}->ct_results($restype)) == CS_SUCCEED) {
        next if($restype == CS_CMD_DONE || $restype == CS_CMD_FAIL ||
                $restype == CS_CMD_SUCCEED);

        while(@dat = $serverConnections{$srv}->ct_fetch) {
            if (defined(@dat) && $dat[0] != 0) {
                ++$lineNumber;
                $indxStart = $lineNumber . ".0";
                $indxEnd   = $lineNumber . "." . $lineLength;
                $tagName   = "TAG" . $lineNumber;

                $dat[0] = "" if !defined($dat[0]);
                $dat[1] = "" if !defined($dat[1]);
                $dat[2] = "" if !defined($dat[2]);
                $dat[3] = "" if !defined($dat[3]);
                $dat[4] = "" if !defined($dat[4]);
                $dat[5] = "" if !defined($dat[5]);
                $dat[6] = "" if !defined($dat[6]);

                $frmText = sprintf(" %3d %-12.12s %-20.20s %-10.10s %-11.11s %-18.18s %-12.12s\n",
                                   $dat[0], $dat[1], $dat[2], $dat[3], $dat[4], $dat[5], $dat[6]);

                $t->insert('end', $frmText);

                if ($dat[1] =~ /Ex_page/ ||
                    $dat[1] =~ /Ex_row/ ) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'exPage'});
                }

                if ($dat[1] =~ /Sh_table/) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'shTable'});
                }

                if ($dat[1] =~ /Sh_page/) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'shPage'});
                }

                if ($dat[1] =~ /Update_page/ ) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'updatePage'});
                }

                if ($dat[1] =~ /Ex_intent/ ) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'exIntent'});
                }

                if ($dat[1] =~ /Ex_table/) {
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'exTable'});
                }

                if ($dat[1] =~ /-blk/) {
                    ++$blockingUsers;
                    $t->tagAdd($tagName, $indxStart, $indxEnd);
                    $t->tagConfigure($tagName, -foreground, $currentColours{'blocked'});
                }

            }
        }

    }

    $linesOfText = $lineNumber;

    my($msgText) = "$users{$srv}/$srv   $linesOfText lock";

    if ($lineNumber > 1) {
        $msgText = $msgText . "s ";
    }
    else {
        $msgText = $msgText . "  ";
    }

    $msgText = $msgText . " ($blockingUsers blocking)";

    $status1->configure(-text => $msgText);
    $status2->configure(-text => "Uptime: $upTime{$srv}");

    $t->yview(moveto => $top);
}

sub spFreeze {

    $freeze = 1 - $freeze;

  FROZEN: {
      if ($freeze == 0) { $freezeB->configure(-text => "Freeze"); last FROZEN; }
      if ($freeze == 1) { $freezeB->configure(-text => "Unfreeze"); last FROZEN; }
  }
    &spWrapper;
}

sub spRefresh {

# Force a refresh, even if frozen.  Remember to reset state of frozen at end.

    my($isFrozen) = $freeze;

    $freeze = 0;

    &spWrapper;

    $freeze = $isFrozen;

}

sub toggleLogging {

    # Toggle the logging command flag, reset the button and then open or close
    # the log file appropriately.  Not sure how to implement this so that it
    # works usefully.

}

sub readServerList {

    # Read in the server list, either from the sql.ini file or from the interfaces file
    # depending on system type.

    # NT

    my(@servers);

    if ($OSNAME =~ /MSWin/) {
        @servers = &readMSServerList;
    }
    else {
        @servers = &readUnixServerList;
    }

    return sort @servers;
}

sub readUnixServerList {

    my(@server_string, $server);

    open(INTERFACES, "$ENV{'SYBASE'}/interfaces") || die "Cannot open interfaces file\n";

    while (<INTERFACES>) {

        if (/^[A-Za-z]/) { # Found a major header (aka Server), add it to the stack
            chomp;
            ($server = $_) =~ s/[   ]*$//;
            push @server_string, $server;
        }
    }

    close INTERFACES;

    return @server_string;
}

sub readMSServerList {

    my(@server_string, $server);

    open(SQLINI, "$ENV{'SYBASE'}/ini/sql.ini") || die "Cannot open interfaces file\n";

    while (<SQLINI>) {

        if (/^[     ]*\[/) { # Found a major header (aka Server), add it to the stack
            chomp;
            ($server = $_) =~ s/^[  ]*\[(..*)\]/$1/;
            push @server_string, $server;
        }
    }

    close SQLINI;

    return @server_string;
}

sub switchServer {

    my($pwd, $userID, $db);

    # Switch to the selected server, if not already connected to it, pop up the
    # login command

    if (defined($serverConnections{$newSrv})) {

        $srv = $newSrv;

        # Reset the title bar for the main window to reflect the attached server.

        $mw->title("User Monitor for $srv");

        &spWrapper;
    }
    else {
        # OK, so the server is not connected to currently, so let's create
        # a new connection.

        my($loginMw) = MainWindow->new;

        my($Login) = $loginMw->Login;

        $Login->addDatabase('master',  $newSrv );

        $Login->configure(-User   => $uid,
                          -ULabel => 'User Name:',
                          -Title  => 'Please Login');

        if ($Login->getVerification(-Force => 1)) {
            $pwd     = $Login->cget(-Password);
            $userID  = $Login->cget(-User);
            $db      = $Login->cget(-Database);
        } else {
            $newSrv = $srv;  # Reset the drop selection text.
            $loginMw->destroy;
            return;
        }

        $loginMw->destroy;

        $serverConnections{$newSrv} = new Sybase::CTlib $userID, $pwd, $newSrv, $appname,
                                             { CON_PROPS => { CS_HOSTNAME   => $hostname}
                                           };  # Connect to the new server.

        if (!defined($serverConnections{$newSrv})) {
            return;
        }

        # Login is valid, so set the server variable.

        $srv         = $newSrv;
        $users{$srv} = $userID;

        # Reset the title bar for the main window to reflect the attached server.

        $mw->title("User Monitor for $srv");

        # Get the uptime for the server.  It is not safe to call upTime, since that resets the time
        # for all of the servers and we cannot guarantee that there will not be a pending request for
        # one of the other servers in the list.

        $serverConnections{$srv}->ct_execute("select convert(varchar, datediff(hh,crdate,getdate())/24) +
                                             ' days ' +
                                             convert(varchar, datediff(hh,crdate,getdate())% 24) +
                                             ' hours.'
                                        from master..sysdatabases where name='tempdb'");

        while(($rc = $serverConnections{$srv}->ct_results($restype)) == CS_SUCCEED) {
            next if($restype == CS_CMD_DONE || $restype == CS_CMD_FAIL ||
                    $restype == CS_CMD_SUCCEED);

            while(@dat = $serverConnections{$srv}->ct_fetch) {
                if (defined(@dat)) {
                    $upTime{$srv} = $dat[0];
                }
            }
        }

        &getRole;

        # Ensure that the user is in the master database for viewing, this
        # then means that you can monitor DB loads of your default DB :-)

        $serverConnections{$srv}->ct_sql("use master");

        &spWrapper;
    }
}

sub sortBy {

    # Subroutine to apply sort by's to the various screens.

  SORT: {
    if ($sortItem eq "<None>")     {$sortArg = 0;  last SORT; }
    if ($sortItem eq "Pid")        {$sortArg = 1;  last SORT; }
    if ($sortItem eq "Status")     {$sortArg = 2;  last SORT; }
    if ($sortItem eq "Login Name") {$sortArg = 3;  last SORT; }
    if ($sortItem eq "Host Name")  {$sortArg = 5;  last SORT; }
    if ($sortItem eq "Database")   {$sortArg = 7;  last SORT; }
    if ($sortItem eq "Command")    {$sortArg = 8;  last SORT; }
    if ($sortItem eq "CPU Time")   {$sortArg = 9;  last SORT; }
    if ($sortItem eq "Phys I/O")   {$sortArg = 10; last SORT; }
    if ($sortItem eq "Mem Usage")  {$sortArg = 11; last SORT; }
    if ($sortItem eq "Blocked By") {$sortArg = 6;  last SORT; }
  }

    if ($lastSort == $sortArg) {
        $descend = 1 - $descend;
    }
    else {
        $descend = 0;
    }

    $lastSort = $sortArg;

    &spWrapper;
}

sub spDisconnect {

    # Disconnect the current connection and refresh the server.

    # This is a little bit of a simplistic solution, but hey, it
    # works!

    $t->delete("1.0", ++$linesOfText . ".$lineLength");
    $mw->title("User Monitor for <UNKNOWN>");
    $serverConnections{$srv} = undef;
}

sub listServers {

    my $subwin = MainWindow->new;

    $subwin->resizable("no", "no");

    my $listBox1 = $subwin->Scrolled("Listbox", -scrollbars => "oe");

    $listBox1->configure(-selectmode => 'single',
                         -bd         => 1);

    $listBox1->insert('end', @servers);

    $listBox1->pack();

    my $f = $subwin->Frame->pack(-side => 'top',
                                 -fill => 'x');

    $f->Button(-text    => "Select",
               -command => sub {my $item   = $listBox1->curselection();
                                   $newSrv = $servers[$item];
                                   &switchServer;
                                   $mw->raise();},
               -bd      => 1,
               )->pack(-side => 'left',
                       -padx => 10);

    $f->Button(-text    => "Cancel",
               -bd      => 1,
               -command => sub {#$mw->raise();
                                 $subwin->destroy();}
               )->pack(-side => 'right',
                       -padx => 10);


    $subwin->raise();

}

sub printLogMessage {

    my($logMessage) = @_;

    # This will be used to print out some nice text to a log file soon.

}

sub usage {
    my $text = shift;

    chomp($text);
    print "$text\n";

    if ($OSNAME =~ /^MSWin/) {  # Some flavour of MS Windows
        die "Usage: perl sybmon [-U username] [-S server] [-P password] [-s rate]\n";
    }
    else {
        die "Usage: sybmon [-U username] [-S server] [-P password] [-s rate]\n";
    }
}

sub upTime {

    # Update the uptime for all of the current server connections on an hourly basis.

    my $srv;
    my $hSrv;

    foreach $srv (keys %serverConnections) {
        $hSrv = $serverConnections{$srv};
        $hSrv->ct_execute("select convert(varchar, datediff(hh,crdate,getdate())/24) +
                                  ' days ' +
                                  convert(varchar, datediff(hh,crdate,getdate())% 24) +
                                  ' hours.'
                                  from master..sysdatabases where name='tempdb'");


        while(($rc = $hSrv->ct_results($restype)) == CS_SUCCEED) {
            next if($restype == CS_CMD_DONE || $restype == CS_CMD_FAIL ||
                    $restype == CS_CMD_SUCCEED);

            while(@dat = $hSrv->ct_fetch) {
                if (defined(@dat)) {
                    $upTime{$srv} = $dat[0];
                }
            }
        }
    }
}

sub getRole {

    $serverConnections{$srv}->ct_execute("select proc_role('sa_role')");

    $userRole{$srv} = "none";  # Ensure that he gets *nothing*.

    while(($rc = $serverConnections{$srv}->ct_results($restype)) == CS_SUCCEED) {
        next if($restype == CS_CMD_DONE || $restype == CS_CMD_FAIL ||
                $restype == CS_CMD_SUCCEED);

        while(@dat = $serverConnections{$srv}->ct_fetch) {
            if (defined(@dat)) {
                if ($dat[0] eq "1") {
                    $userRole{$srv} = "sa_role";
                }
            }
        }
    }
}

sub getPreferences {

    my($colourName);

# Push the set of colours onto an array for the preferences

    @colourNames = ("background"
                    ,"defaultFG"
                    ,"running"
                    ,"sleeping"
                    ,"remoteIO"
                    ,"sendSleep"
                    ,"alarmSleep"
                    ,"lockSleep"
                    ,"logSuspend"
                    ,"exPage"
                    ,"shTable"
                    ,"shPage"
                    ,"updatePage"
                    ,"exIntent"
                    ,"exTable"
                    ,"blocked");

# Defaults.
#                                       R G B
    %defaultColours = (background  => "#c0bcc8",
                       defaultFG   => "darkgreen",
                       running     => "#8f008f",
                       sleeping    => "#008f8f",
                       remoteIO    => "#9f8000",
                       sendSleep   => "#888800",
                       alarmSleep  => "#000088",
                       lockSleep   => "red",
                       logSuspend  => "red",

                       exPage      => "#800080",
                       shTable     => "#189f9f",
                       shPage      => "#00008f",
                       updatePage  => "#009f00",
                       exIntent    => "#008f8f",
                       exTable     => "#8f0000",
                       blocked     => "#ff0000");

# Initialise the set of displayed colours (current) and other settings
# from the defaults or from the rc file.

    if (! -f $rcFile) {
        foreach $colourName (@colourNames) {
            $currentColours{$colourName} = $defaultColours{$colourName};
        }

        $defRefresh   = 5;          # Default refresh rate.
        $textHeight   = 32;

    } else {
        do "$rcFile";
    }

# Initialise the colours

    foreach $colourName (@colourNames) {
        $ccwColours{$colourName} = $currentColours{$colourName};
    }
}

sub putPreferences {

    my($colourName);

    open(RCFILE, ">$rcFile") || die "Cannot open $rcFile.\n";

    # Now write the options.  I know that this version has nuked the
    # old file, but that will have to do for a start.

    # The options file is just a set of perl code.

    print RCFILE "\$defRefresh = $defRefresh;\n";
    print RCFILE "\$textHeight = $textHeight;\n";

    print RCFILE "\%currentColours = (\n";

    foreach $colourName (@colourNames) {
        print RCFILE "                   $colourName  => '";
        print RCFILE "$currentColours{$colourName}";
        print RCFILE "'" . (($colourName eq "blocked") ? ");" : ",") . "\n";
    }

    # May add the following at some stage.

    #$displayType  = 0;          # 0:Processes 1:Locks 2:Blocked Processes
    #$sysProcs     = 1;          # 1:Display System Processes 0:Don't display them...
    #$logging      = 0;          # 0:Do not log results 1:Log results.
    #$linesOfText  = 1;          # Used to clean the display and other things.

    close RCFILE;
}

sub preferences {

    # Proc to set the colours for the various levels for each of the
    # screens.  Eventually, these will be saved as the users defaults,
    # but for now they are solely dynamic.

    my($colourName, $callbackProc, @colourFrame, $frameSwitch );

    my($mainFrame, $bottomFrame, $middleFrame, $midLower, $midUpper);

    $frameSwitch = 0;

    $ccw = MainWindow->new;  # Create a new colour chooser window.

    $ccw->resizable("no", "no");

    $mainFrame = $ccw->Frame->pack(-side => 'top',
                                   -fill => 'x');

    $colourFrame[0] = $mainFrame->Frame->pack(-side   => 'left',
                                              -anchor => 'w');

    $colourFrame[1] = $mainFrame->Frame->pack(-side   => 'right',
                                              -anchor => 'w');

    foreach $colourName (@colourNames) {

        $callbackProc = sub {

            $ccwColours{$colourName} =
                $ccw->chooseColor(-initialcolor => $currentColours{$colourName},
                                  -title        => "Choose color");

            if (!defined($ccwColours{$colourName})) {
                $ccwColours{$colourName} = $currentColours{$colourName};
            }

            $ccwLabels{$colourName}->configure(-foreground => $ccwColours{$colourName});

        };

        $ccwFrames{$colourName} = $colourFrame[$frameSwitch]->Frame->pack(-side => 'top',
                                                                          -fill => 'x');

        $ccwLabels{$colourName} =
            $ccwFrames{$colourName}->Label(-text => $colourName,
                                           -foreground => $currentColours{$colourName}
                                           )->pack(-side   => 'left',
                                                   -anchor => 'w',
                                                   -padx   => 5);

        $ccwFrames{$colourName}->Button(-text        => "Set...",
                                        -command     => $callbackProc,
                                        -borderwidth => 1)->pack(-side => 'right');

        $frameSwitch = 1 - $frameSwitch;
    }

    $middleFrame = $ccw->Frame->pack(-side => 'top',
                                     -fill => 'x',
                                     -pady => 5);

    $midUpper = $middleFrame->Frame->pack(-side => 'top',
                                          -fill => 'x',
                                          -pady => 5);

    $midUpper->Label(-text => "Default Refresh Rate:")->pack(-side   => 'left',
                                                             -anchor => 'w',
                                                             -padx   => 5);

    $midUpper->Entry(-textvariable => \$defRefresh,
                     -borderwidth  => 1,
                     -width        => 3,
                     -font         => "niceText")->pack(-side   => 'left',
                                                        -anchor => 'w',
                                                        -padx   => 5);

    $midLower = $middleFrame->Frame->pack(-side => 'top',
                                          -fill => 'x',
                                          -pady => 5);

    $midLower->Label(-text => "Default Screen Lines:")->pack(-side   => 'left',
                                                             -anchor => 'w',
                                                             -padx   => 5);

    $midLower->Entry(-textvariable => \$textHeight,
                     -borderwidth  => 1,
                     -width        => 3,
                     -font         => "niceText")->pack(-side   => 'left',
                                                        -anchor => 'w',
                                                        -padx   => 5);

    $bottomFrame = $ccw->Frame->pack(-side => 'bottom',
                                     -fill => 'x');

    $bottomFrame->Button(-text        => "OK",
                         -command     => sub {setNewColours();
                                              putPreferences();
                                              $ccw->destroy;},
                         -borderwidth => 1)->pack(-anchor => 'n',
                                                  -side   => 'left');


    $bottomFrame->Button(-text        => "Apply",
                         -command     => \&setNewColours,
                         -borderwidth => 1)->pack(-anchor => 'n',
                                                  -side   => 'left',
                                                  -padx   => 18);


    $bottomFrame->Button(-text        => "Cancel",
                         -command     => sub {$ccw->destroy;},
                         -borderwidth => 1)->pack(-anchor => 'n',
                                                  -side   => 'right');

    $bottomFrame->Button(-text        => "Reset",
                         -command     => \&resetDefaultColours,
                         -borderwidth => 1)->pack(-anchor => 'n',
                                                  -side   => 'right',
                                                  -padx   => 18);



}

sub setNewColours {

    # Sub to set the colours in the main screen based on the colours
    # we have just chosen.

    my($colourName);

    $t->configure(-background => $ccwColours{'background'},
                  -foreground => $ccwColours{'defaultFG'});

    foreach $colourName (@colourNames) {
        $currentColours{$colourName} = $ccwColours{$colourName};
    }

}

sub resetDefaultColours {

    # Sub to reset the colours to the defaults.

    my($colourName);

    $t->configure(-background => $currentColours{'background'},
                  -foreground => $currentColours{'defaultFG'});

    foreach $colourName (@colourNames) {
        $ccwColours{$colourName} = $currentColours{$colourName};
    }

}

sub msg_cb {
    my($layer, $origin, $severity, $number, $msg, $osmsg, $dbh) = @_;
    if ($DEBUG == 1) {
        printf STDERR "\nOpen Client Message: (In msg_cb)\n";
        printf STDERR "Message number: LAYER = (%ld) ORIGIN = (%ld) ",
        $layer, $origin;
        printf STDERR "SEVERITY = (%ld) NUMBER = (%ld)\n",
        $severity, $number;
        printf STDERR "Message String: %s\n", $msg;
        if (defined($osmsg)) {
            printf STDERR "Operating System Error: %s\n", $osmsg;
        }
    }
        CS_SUCCEED;
}

sub srv_cb {
    my($dbh, $number, $severity, $state, $line, $server, $proc, $msg) = @_;

    if ($DEBUG == 1 || $severity > 10) {
        # If $dbh is defined, then you can set or check attributes
        # in the callback, which can be tested in the main body
        # of the code.
        printf STDERR "\nServer message: (In srv_cb)\n";
        printf STDERR "Message number: %ld, Severity %ld, ",
        $number, $severity;
        printf STDERR "State %ld, Line %ld\n", $state, $line;
        if (defined($server)) {
            printf STDERR "Server '%s'\n", $server;
        }
        if (defined($proc)) {
            printf STDERR " Procedure '%s'\n", $proc;
        }
        printf STDERR "Message String: %s\n", $msg;
    }
    CS_SUCCEED;
}
