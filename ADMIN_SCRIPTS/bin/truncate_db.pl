#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2006 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "USAGE: truncate_db.pl -UUSER -SSERVER -PPASS -DDB\n
        -E TABLES      (comma separated list of tables to EXCLUDE)
   	-I TABLES      (comma separated list of tables to INCLUDE)
   	-n (noexec)
   ";
        return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_d $opt_U $opt_S $opt_P $opt_D $DEBUG $opt_E $opt_I $opt_n);
die usage("Bad Parameter List") unless getopts('dU:D:S:P:E:I:n');

use File::Basename;
my($curdir)=dirname($0);

use DBIFunc;

die usage("Must Pass DB\n")                                     unless defined $opt_D;
die usage("Must pass sa password\n" )   unless defined $opt_P;
die usage("Must pass server\n")                         unless defined $opt_S;
die usage("Must pass sa username\n" )   unless defined $opt_U;
$DEBUG=1 if defined $opt_d;

#dbi_connect($opt_S,$opt_U,$opt_P) or die "Cant Connect to $opt_S\n";
if( ! dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -debug=>$opt_d) ) {
      	die "Cant connect to $opt_S as $opt_U\n";
}

my($db)=$opt_D;
die "Cant Delete System Database\n"
        if $db eq "master"
                or $db eq "model"
                or $db eq "sybsystemprocs"
                or $db eq "sybsecurity"
                or $db eq "tempdb";

print "Database $db\n" if defined $opt_d;

#dbi_eachobject($db,"truncate table","U",$opt_d);

   	dbi_eachobject(
				-db=>$db,
				-cmd=>"truncate table",
				-no_exec=>$opt_n,
				-type=>"U",
				-do_print=>1,
				#-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);


dbi_disconnect();

__END__

=head1 NAME

trunctate_db.pl - truncate database utility

=head2 DESCRIPTION

truncate all tables in a database.  Does not handle ri, but you can probably run 2 times in a row to get through that.  The user must have permission to truncate.

=head2 USAGE

       USAGE: truncate_db.pl -UUSER -SSERVER -PPASS -DDB

        -E TABLES      (comma separated list of tables to EXCLUDE)
        -I TABLES      (comma separated list of tables to INCLUDE)
        -n (noexec)


=cut

