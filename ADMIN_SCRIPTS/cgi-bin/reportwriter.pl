#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use lib qw(. lib);

# possible colors for table
# FFFFEE
# BGCOLOR=#f0e9d9 WIDTH=100% BORDER=1

# THE MASTER CONTROL FOR DIAGNOSTICS THAT APPEAR FOR DASHBOARD ADMINISTRATORS
#
# set development_mode to 2 for code_section messages!
my($development_mode);
my($production_mode);
my($results_dumper_mode);		# Data::Dumper of results
my($diagnose_mode);				# diagnose the table layout

my($COMPANYNAME)="Millenium Partner\'s";
my($starttime)=time;

BEGIN {
	if( ! defined $ENV{SYBASE} or ! -d $ENV{SYBASE} ) {
		if( -d "/export/home/sybase" ) {
			$ENV{SYBASE}="/export/home/sybase";
		} else {
			$ENV{SYBASE}="/apps/sybase";
		}
	}
}

############ MODIFICATION INSTRUCTIONS FOR YOUR WEB SERVER  ###########
# This program needs to be modified by YOU... yes you...
# It should run from the command line so
#
#   a) look at line #1 - the hashbang - make sure it is your perl
#   b) look at line #3 - the use lib line... make sure it points to the
#      correct location for the GEM perl libraries.  If you are on a system
#      where these libs do not exist, copy the whole tree locally.
#   c) the begin block starting on line #9 should correctly set your SYBASE
#      environment variable.  I have multiple lines in an if/then/else
#      because i run this code on multiple machines
#
############# DONT MODIFY BELOW THIS LINE ###################

use strict;
use MlpAlarm;
use CommonFunc;
use Do_Time;
use DBIFunc;

use CGI qw( :standard escapeHTML escape);
use CGI::Carp qw(fatalsToBrowser);
use File::Basename;

my($header_printed);    # defined if we have printed the cgi header
my(@saved_messages);    # screen lines that should have been printed
								# usually debug messages but couldnt be printed
								# because header() has not yet been called.

$|=1;

my($curdir)=cd_home();

# remove error 9558 - char length 2 long msg that occurs due to limits between
# database varchar(255) (plus null) and perl able only to handle 255 bytes
# per column.
DBIFunc::dbi_msg_exclude(9558);

my($del_id,$add_id,$action_id);		# delete/add id
my($dflt_system)  	= "[ Select System ]";
my($dflt_systemtype) = "[ Select System Type ]";
my($dflt_program) 	= "[ Select Program ]";
my($dflt_user) 		= "[ Select User ]";
my($dflt_group) 		= "[ Select Group ]";
my($ROWLIMIT)			= 50;

my(@hb_values)=('Blackout3','Blackout6','Blackout24','Blackout72','Blackout1Wk'
,'IgnoreSvr','MsgOk3hr','MsgOk','MsgOk3dy','MsgOk7dy','MsgOkPerm','MsgDelete',
'NotProd','DeleteAll','DeleteMon','MsgOkMonitor3hr','MsgOkMonitor1Dy',
'MsgOkMonitor3Dy');
my(%hb_operations)=(
	'Blackout3'	=>"3 Hr Srvr Blackout",
	'Blackout6'	=>"6 Hr Srvr Blackout",
	'Blackout24'	=>"24Hr Srvr Blackout",
	'Blackout72'	=>"72Hr Srvr Blackout",
	'Blackout1Wk'	=>"1Week Srvr Blackout",
	'IgnoreSvr'	=>"Ignore Srvr",
	'MsgOk3hr'	=>"3 Hour Msg Ok",
	'MsgOk'		=>"1 Day Msg Ok",
	'MsgOk3dy'		=>"3 Day Msg Ok",
	'MsgOk7dy'		=>"7 Day Msg Ok",
	'MsgOkPerm'		=>"Perma Msg Ok",
	'MsgOkMonitor3hr'		=>"Ok By Monitor 3 Hr",
	'MsgOkMonitor1Dy'		=>"Ok By Monitor 1 Dy",
	'MsgOkMonitor3Dy'		=>"Ok By Monitor 3 Dy",
	'NotProd'	=>"Srvr Not Production",
	'MsgDelete'	=>"Delete Message",
	'DeleteAll'	=>"Del All Srvr Msgs",
	'DeleteMon'	=>"Del All Monitor Msgs");

#
# For Developer Prints
#
my($i_am_able_to_understand_sql)='?';
my($dev_diag_str)='';				# populated via developer_diag

#
# varkeys %PARGS will be translated to variables directly...
#    The following variables are named for their keys %PARGS exact name match
#
my($debug);					# From the Debug Checkbox
my($admin) = param("admin") || param("REPORTNAME") || param("sv_admin") ||param("svsv_admin");
my($LOGON_USER)=GetLogonUser();

my($butncnt)=0;	# current add/delete button - keyed by add_$butncnt or
						# del_$butncnt.  the sv_$butncnt_$key=$value fields will
						# define the row you are hitting

my(@allsysD,@allsysH,@allsys,@allprg, @containers, @all_ldap_grp, @all_ldap_usr);	# saved values

code_section("STARTING PROCESSING",__LINE__);

# CHANGE ALL param() TO PARGS
my(%PARGS);
$PARGS{"DIAGNOSE"}=1 if $diagnose_mode;
if( ! $admin ) {
	my_Header();
	my_Nav_Bar();
#	if( $development_mode ) {
#		my($argstr)='<TABLE BORDER=1><TR><TD COLSPAN=3 BGCOLOR=PEACH>LINE='.__LINE__.
#			" SQL PRINT FOR DASHBOARD ADMINISTRATOR $LOGON_USER<br>Skips HEADER, TITLE, DEBUG ";
#		$argstr.="</TD></TR><TR>";
#		my($argstrlong)='';
#		my($cnt)=0;
#		foreach ( param() ) {
#			if( length(param($_)) > 30 ) {
#				$argstrlong.= "<TR><TD COLSPAN=3>$_ => <FONT COLOR=RED>",param($_),"</FONT> </TD></TR>";
#			} else {
#				$argstr.= "<TD>Arg $_ => <FONT COLOR=RED>",param($_),"</FONT></TD>";
#				$cnt++;
#				$argstr .= "</TR><TR>" if $cnt%3 == 0;
#			}
#		}
#		$argstr .= "</TR>".$argstrlong."</TABLE>";
#		print "<FONT COLOR=BLUE><PRE>",$argstr,"</PRE></FONT>";
#		print "<FONT COLOR=BLUE><PRE>Line ",__LINE__," No Reportname Provided</PRE></FONT>";
#	}
}


my($prstring)='';
# my_Header();		# rmv
#open(F,">> C:/text.txt") or die "CAnt open C:/text.txt\n";
#rint F "\n";
my $key = localtime(time);

$key .= " ".$LOGON_USER." ";

my($cnt)=0;
foreach ( param() ) {
	$cnt++;
#	print F $key," ",__LINE__," P=$_ V=",join("|",param($_)),"\n"; # unless /^sv_/ or /^dat_/;
	if( /multiparam_/ ) {
		$PARGS{$_} = join("|",param($_)); 	# checkboxes should be named this
	} else {
		$PARGS{$_} = param($_) if param($_) !~ /^\[/
			or $_ eq 'filter_credentials' 	# the like filters must be set...
			or $_ eq 'filter_likefilter'
			or $_ eq 'filter_num_days';
	}
	$prstring .= " param($_) => ".param($_).br if /^filter/;
	if( $_ =~ /^filter_/ or $_ eq "REPORTNAME"
		or ( /^add_/ and param($_) !~ /^\[/ and param($_) !~ /^\s*$/ ) ) {
		param(-name=>$_,-value=>$PARGS{$_}) if $_ =~ /^filter/;				# fixed duplication bug
#		print F "Resetting $_ parg=$PARGS{$_} param=",join("", param($_)),"\n"  if $_ =~ /^filter/;
	} else {
		Delete($_) unless param("NOARGFETCH");	# if /^sv_\d+_/ or /^dat_\d+_/; }
	}
}
#print F "$key Cnt=$cnt\n";
#close(F);

if( ! $admin ) {		# woah... no page
		my_Header();
		my_Nav_Bar();

		foreach ( keys %PARGS ) { print "PARG: $_ => $PARGS{$_}",br; }
		foreach ( param() ) { print "param: $_ => param($_)",br; }

		print "<center>",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;";
		print "<b><h1>WELCOME TO THE ENTERPRISE DASHBOARD</h1></b>",p;

		my($rc)=MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>1);
		if( $rc ne "Y" ) {
			my($str)=GetLogonUser().": ACCESS DENIED".br."Please Contact Support".br;
			my(@rc)=MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0);
			foreach( @rc ) {
				$str.="Ok: $_".br;
			}
			$str.="You have NO permissions".p if $#rc<0;
			print "<center><FONT face=sans-serif size=+1 color=red>$str</FONT></center>\n";
			errorout('');
		}

		if( $PARGS{'permission'} ) {
				print "simulated permission: ".i($PARGS{'permission'}),br;
		} else {
			foreach( MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0) ) {
				print "You have permission ",i($_),br unless /^Dflt~([a-zA-Z_]+):(.+)/;
			}
		}

		print p,"&nbsp;",p,"&nbsp;",p,"&nbsp;",p,"&nbsp;";
		print "Authenticated as ".$LOGON_USER,p,"</center>";
		my_end_form();
		print end_html;
		exit(0);
}

# This means that if REPORTNAME param is passed, it GRABs & SETs
# THE PARAMETERS FROM THE DATABASE
#my(@REPORTARGS);
my_Header();

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});
if( defined $PARGS{"REPORTNAME"} ) {
	debugmsg( "RUNNING REPORT ".$PARGS{"REPORTNAME"});

	my($r)=$PARGS{"REPORTNAME"}||'';
	#print "<PRE>";
	my(%x)=MlpReportArgs(-reportname=>$r);
	#foreach ( sort keys %x ) { print $_," ",$x{$_},br; }
	#print "</PRE>";
	my($prstring)='';
	foreach (sort keys %x) {
		next if defined $PARGS{$_};
		next if $_ eq "NONAVIGATION" and ! $x{$_};

		if( /^HEADER/ or /^TITLE/ )		{
			$prstring .= " => Overriding Argument $_ (not showing value)".br;
		} else {
			$prstring .= " => Overriding Argument $_ With $x{$_}".br;
		}
		$PARGS{$_} = $x{$_};
		$admin = $x{$_} if /^ADMIN$/i;
	}
	code_section("DONE REPLACING ARGUMENTS FROM ReportArgs for ".$PARGS{REPORTNAME}.br.$prstring,__LINE__);
}

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});
debugmsg("REPORT Called In Diagnostic MODE") if defined $PARGS{'debug'} or $PARGS{'DEBUG'};

# handle adds/delete button pushed
foreach ( keys %PARGS ) {
	if( /^actionid_(\d+)$/ ) 			 { $action_id = $1; 	last; }
	if( ! $del_id and /^del_(\d+)$/ ) { $del_id = $1; 		last; }
	if( ! $add_id and /^add_(\d+)$/ ) { $add_id = $1; 		last; }
}

code_section("DONE REMOVING DEFAULT ARGUMENTS FROM CONSIDERATION",__LINE__);

$debug=1 	if defined $PARGS{'debug'} or $PARGS{'DEBUG'};

# DO NOT REMOVE THIS BLOCK - IT WORKS FINE JUST IS DEPRICATED
# change your defaults
#
#if( ( $PARGS{ 'submit' } eq "Run Report" or $PARGS{ 'submit' } eq "Reset" )
#		and param('NOARGFETCH') ne "1" ) {
#
#	foreach ('filter_severity','filter_likefilter','filter_num_days',
#				'filter_credentials','filter_showdeleted','filter_prodchkbox',
#				'filter_container','filter_system','filter_system_type','filter_program_name',
#				'filter_ldap_group','filter_ldap_user','filter_time') {
#
#		print join(br,MlpManageData(
#					Admin=>'INV_DEFINE_SYSUSERS',
#					-debug=>1,
#					INV_LDAP_USER_LU=>$LOGON_USER,
#					Operation=>"del",
#					INV_PERMISSION_LU=>"Dflt~$_:%" ));
#
#		if( $PARGS{$_} ) {
#			if( $PARGS{'submit'} eq "Reset" and $_ ne "filter_severity" ) {
#				delete($PARGS{$_})
#			} else {
#				print join(br,MlpManageData(
#					Admin=>'INV_DEFINE_SYSUSERS',
#					-debug=>1,
#					INV_LDAP_USER_LU=>$LOGON_USER,
#					Operation=>"add",
#					INV_PERMISSION_LU=>'Dflt~'.$_.':'.$PARGS{$_} ));
#			}
#		}
#	}
#}
# END DO NOT REMOVE THIS BLOCK

# #developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});
code_section("Checking Your Permissions",__LINE__);
errorout("FATAL ERROR - LOGON_USER IS UNDEFINED") unless $LOGON_USER;
my($rc)=MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>1);
if( $rc ne "Y" ) {
	print errorbox("ERR".__LINE__.": ACCESS DENIED - YOU HAVE AUTHENTICATED AS ".GetLogonUser(),
		br,"PLEASE CONTACT SUPPORT");
	errorout("FATAL ERROR");
}

my(%dashboard_permissions);
if( $PARGS{'permission'} ) {
		$dashboard_permissions{$PARGS{'permission'}}=1;
		print "set role: ".i($PARGS{'permission'}),br;
} else {
	foreach( MlpGetPermission(-samid=>$LOGON_USER, -canaccess=>0) ) {
		$dashboard_permissions{$_}=1;
# DO NOT REMOVE THIS BLOCK - IT WORKS FINE JUST IS DEPRICATED
#		if( /^Dflt~([a-zA-Z_]+):(.+)/ ) {
#			my($arg,$v)=($1,$2);
#			next if $v=~/^\s*$/;
#			next if $PARGS{$arg};
#			next if $PARGS{NOARGFETCH} or $PARGS{submit} eq 'Reset';
#			developer_diag("Resetting $arg to $v");
#			$PARGS{$arg}=$v;
#		}
# END DO NOT REMOVE
	}
}

# #developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});
if( ! $development_mode or
 ( ! defined $dashboard_permissions{'DASHBOARD ADMINISTRATOR'} and ! $PARGS{'permission'} )) {
	print "<FONT SIZE=+1 COLOR=RED>TURNING OFF DEVELOPMENT MODE AS $LOGON_USER DOES NOT HAVE DASHBOARD ADMINISTRATOR</FONT>",br
		if $development_mode;
	$debug=0;
	$i_am_able_to_understand_sql='N';
} else {
	statusmsg( "<FONT COLOR=RED>Welcome DASHBOARD ADMINISTRATOR !!!</FONT>" );
	if( ! $production_mode ) {
		set_developer(1);
		$i_am_able_to_understand_sql='Y';
	}
	statusmsg( $dev_diag_str ) if $dev_diag_str;
}

# #developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

sub query_print {
	print "<FONT COLOR=blue SIZE=-2>",@_,"</FONT><br>\n";
}

#sub _diag_show_query_in_green { 	return("<FONT COLOR=green SIZE=-1>@_</FONT><p>"); }

sub developer_diag {
	if( $i_am_able_to_understand_sql eq "Y" ) {
		print "<FONT COLOR=GREEN SIZE=-1><PRE>",@_,"</PRE></FONT>";
	} elsif( $i_am_able_to_understand_sql eq "?" ) {
		$dev_diag_str.="<FONT COLOR=BLUE><PRE>".join('',@_)."</PRE></FONT>\n";
	}
}

diag_showparam(__LINE__,1) if $i_am_able_to_understand_sql eq 'Y';
die "INTERNAL ERROR - CANT HAVE BOTH HDR_ROW_REPEATS AND HDR_FORM_REPEATS SET"
	if $PARGS{HDR_ROW_REPEATS} and $PARGS{HDR_FORM_REPEATS};

if( $PARGS{EXTERNAL_CGI_BTNPRESS} ) {
	code_section("EXTERNAL CGI BTNPRESS",__LINE__);
	debugmsg( "Line=".__LINE__.": CALLING EXTERNAL CGI INLINE- ".$PARGS{EXTERNAL_CGI_BTNPRESS} );
	my($cmd)=$PARGS{EXTERNAL_CGI_BTNPRESS};
	my($codeblock)='';
	open(G,$cmd) or die "CANT READ $cmd";
	while(<G>) { $codeblock.=$_; }
	close(G);
	eval $codeblock;
	print "ERROR".$@ if $@;
}

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

#
# for deletes we copy all values with appropriate save key id's
# which means sv_$del_id* params plus DelId=del_id and Operation=del
#
if( defined $del_id ) {
	code_section("Delete Button Pressed",__LINE__);
	debugmsg("Line ".__LINE__.": Delete Button pushed on $admin");
	print drawbanner( b("Processing Delete"),"", "" );
	my(%varkeys);
	foreach (keys %PARGS) {
		next unless /^sv_${del_id}_/;
		my($k)=$_;
		s/sv_${del_id}_//;
		debugmsg( "Line ".__LINE__.": KEY=$_ VAL=".$PARGS{$k} );
		$varkeys{$_} = $PARGS{$k};
	}
	debugmsg( "Admin=".$PARGS{"sv_admin"} );
	$varkeys{Admin}=$PARGS{"sv_admin"};
	$admin=$PARGS{"sv_admin"};
	my(@rc)=MlpManageData( -canunderstandsql=>$i_am_able_to_understand_sql,
		-debug=>$debug,OkId=>$del_id,Operation=>"del",	%varkeys);
	if( $#rc>=0 ) {
		print "<PRE>";
		foreach ( @rc ) {
			if( /^ERROR:/ ) {
				print errorbox($_);
			} else {
				print $_,"\n";
			}
		}
		print "</PRE>",br;
	}
}

#
# for adds we copy all add_* keys - no need for id field because we
# are only passing one add
#
if( defined $add_id ) {
	code_section("Add Button Pressed",__LINE__);
	debugmsg("Line ".__LINE__.": Add Button pushed for $admin ");
	print drawbanner( b("Processing Add"),"", "" );
	my(%varkeys);

	foreach (keys %PARGS) {
		next unless /^add_/;
		my($k)=$_;
		s/add_//;
		next if /^\d+$/;
		debugmsg( "Line ".__LINE__.": KEY=$_ VAL=".$PARGS{$k} );
		$varkeys{$_} = $PARGS{$k};
	}
	$admin=$PARGS{"sv_admin"};
	my(@rc)=MlpManageData( -canunderstandsql=>$i_am_able_to_understand_sql,
			Operation=>"add",Admin=>$PARGS{"sv_admin"},	-debug=>$debug,%varkeys);
	if( $#rc >= 0 ) {
		print "<PRE>";
		foreach (@rc) {
			if( /^ERROR:/ ) {
				print errorbox($_) if $_;
			} else {
				print ">",$_,br if $_;
			}
		}
		print "</PRE>";
	}
}

if( defined $action_id ) {
	run_action($action_id, $PARGS{"actionval_".$action_id});
	#die "run_action( row=$action_id, action=".$PARGS{"actionval_".$action_id}." ) Completed\n";
}
debugmsg( "Line=".__LINE__.": Status Update" );

#
# The Certify Now Button calls MlpCertifyNow()
#
#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

if( $PARGS{ 'submit' } eq "Certify Now" ) {
	code_section("Certify Now Button Pressed",__LINE__);
	debugmsg( "Line=".__LINE__.": Certify Now" );
	die "MUST BE ON DATABASE ACCESS FORM $admin"  unless $admin =~ /^INV_DEPARTMENT_ACCESS$/;
	die "NO USER IDENTIFIED" unless GetLogonUser();

	print drawbanner(
		b( "Certifying Department ".$PARGS{'filter_ldap_group'} ),
		"",
		"UserName=".GetLogonUser() );

	foreach ( sort keys %PARGS) {
		next unless /^dat_(\d+)_Radio_Approval_Status/;
		my($rowid)=$1;

		# new code - 11/2010 - approve if unapproved
		$PARGS{"dat_".$rowid."_Radio_Approval_Status"} = "Approve"
			if $PARGS{"dat_".$rowid."_Radio_Approval_Status"} eq "Unknown";

		next if $PARGS{"dat_".$rowid."_Radio_Approval_Status"} eq $PARGS{"sv_".$rowid."_Radio_Approval_Status"};

		print "<b><FONT COLOR=GREEN>Change Found For User ",$PARGS{"sv_".$rowid."_User"}," :";
		if( $PARGS{"dat_".$rowid."_Radio_Approval_Status"} eq "Remove" ) {
			print " Global Removing This User From Underlying Systems";
		} elsif( $PARGS{"dat_".$rowid."_Radio_Approval_Status"} eq "Approve" ) {
			print " Global Approving This User On Underlying Systems";
		} else {
			print " New Status=",$PARGS{"dat_".$rowid."_Radio_Approval_Status"};
		}
		print "</font></b>",br;

		MlpCertifyDepartment(
				-poweruser		=>	$i_am_able_to_understand_sql ,
				-displayname	=> $PARGS{"sv_".$rowid."_User"}||'',
				-ldap_group		=>	$PARGS{'filter_ldap_group'}||'',
				-status 			=> $PARGS{"dat_".$rowid."_Radio_Approval_Status"}||'',
				-notes			=>	$PARGS{'certification_notes'}||'');
	}
	# my_Page_Footer();
}
#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

if( $PARGS{ 'submit' } eq "Certify Section" or $PARGS{ 'submit' } eq "Certify Changes") {
	code_section("Certify Section/Page Button Pressed",__LINE__);
	die "MUST BE ON DATABASE ACCESS FORM $admin" unless $admin =~ /^INV\_(.+)\_ACCESS$/;
	die "NO USER IDENTIFIED" unless $LOGON_USER;

	print drawbanner(
		b("Certifying The $1 ".$PARGS{'filter_system'}.$PARGS{'filter_ldap_group'}." Data"),
		"",
		"UserName=$LOGON_USER" );

	# diag_showparam(__LINE__,0) if $i_am_able_to_understand_sql eq 'Y';

	my($controlstring)=$PARGS{'controlstring'};
	errorout("ERROR: controlstring not found for certification.  Please refresh your string and try again.")
		unless $controlstring;
	my(%keycolumns);
	foreach ( split(/\|/,$controlstring )) {
		my($fld,$fldtype)=split(/~/,$_,2);
		$keycolumns{$fld}=$fldtype;
	}

	foreach ( sort keys %PARGS) {
		next unless /^dat_(\d+)_Radio_Approval_Status/;
		my($rowid)=$1;

		# new code - 11/2010 - approve if unapproved
		$PARGS{"dat_".$rowid."_Radio_Approval_Status"} = "Approve"
			if $PARGS{"dat_".$rowid."_Radio_Approval_Status"} eq "Unknown" and $PARGS{ 'submit' } eq "Certify Section";

		next if $PARGS{"dat_".$rowid."_Radio_Approval_Status"} eq $PARGS{"sv_".$rowid."_Radio_Approval_Status"}
		    and $PARGS{"dat_".$rowid."_Notes"} eq $PARGS{"sv_".$rowid."_Notes"};

	#	foreach ( keys %PARGS ) { print $_ if /^sv_${rowid}/; }
		print "<b><FONT COLOR=GREEN>Change Found For User ",$PARGS{"sv_".$rowid."_User"}," :";
		print " New Status=",
			$PARGS{"dat_".$rowid."_Radio_Approval_Status"},
			" "
			 if $PARGS{"dat_".$rowid."_Radio_Approval_Status"} ne $PARGS{"sv_".$rowid."_Radio_Approval_Status"};

		print " NewNotes=",
			$PARGS{"dat_".$rowid."_Notes"} ,
			" "
			 if $PARGS{"dat_".$rowid."_Notes"} ne $PARGS{"sv_".$rowid."_Notes"};

		print "</font></b>",br;

		my(%CertifyArgs);
		my($certifystr)="login=>$LOGON_USER, -filter_system=>$PARGS{'filter_system'},";
		foreach ( keys %keycolumns ) {
			$CertifyArgs{$_} = $PARGS{"dat_".$rowid."_".$_} ||  $PARGS{"sv_".$rowid."_".$_};
			$certifystr.= " ".$_."=>".$CertifyArgs{$_}."," if $CertifyArgs{$_};
		}
		developer_diag( "MlpCertifyUserStatus($certifystr)" );
		MlpCertifyUserStatus(
			login=>$LOGON_USER,
			%CertifyArgs,
			-filter_system => $PARGS{'filter_system'}	);
	}
}

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

#
#  THE SAVE CHANGES BUTTON compares all dat_* params to sv_* params and saves those changes
#
if( $PARGS{"Save Changes"} ) {
	code_section("Save Changes Button Pressed",__LINE__);
	print drawbanner( b("Saving Changes To The Data"),"", '' );

	my($controlstring)=$PARGS{'controlstring'};
	# print "DBGDBG - CONTROLSTRING is $controlstring<br>";
	errorout("ERROR: controlstring not found for updates.  Please refresh your string and try again.") unless $controlstring;
	my(%keycolumns);
	foreach ( split(/\|/,$controlstring )) {
		my($fld,$fldtype)=split(/~/,$_,2);
		diagnose( "CONTROL STRING FOR $fld =$fldtype  N=key P=popup C=chkbox T=textfield" );
		if( $development_mode ) {
			print "<FONT COLOR=BLUE>LINE=",__LINE__," :";
			if( $fldtype eq "N" ) {
				print "!KEY! CONTROL STRING FOR $fld =$fldtype",br;
			} elsif( $fldtype eq "P" ) {
				print "POPUP CONTROL STRING FOR $fld =$fldtype",br;
			} elsif( $fldtype eq "C" ) {
				print "CHKBX CONTROL STRING FOR $fld =$fldtype",br;
			} elsif( $fldtype eq "T" ) {
				print "TXTBX CONTROL STRING FOR $fld =$fldtype",br;
			} else {
				print "CONTROL STRING FOR $fld =$fldtype  N=key P=popup C=chkbox T=textfield",br;
			}
			print "</FONT>";
		}
		$keycolumns{$fld}=$fldtype;
	}

	diag_show(__LINE__,1,
					"SQL PRINT FOR DASHBOARD ADMINISTRATOR $LOGON_USER<br>Skips HEADER, TITLE, DEBUG",
					"Also Skips dat_, sv_ ",%PARGS);

	# FIND CHANGES
	my($num_differences)=0;
	my(%processed_keys);
	foreach ( keys %PARGS ) {
		next unless /^sv_(\d+)_(.+)$/ or /^dat_(\d+)_(.+)$/;
		my($rid)=$1;
		my($var)=$2;
		next if $processed_keys{$var.":".$rid};		# dont know order of changes
		$processed_keys{$var.":".$rid} = 1;
		errorout("ERROR: RID=$rid VAR=$var FROM ARG=$_") unless defined $rid and defined $var;
		errorout("ERROR: Internal error - $_ is not in control string") unless defined $keycolumns{$var};
		my($pname)=$_;

		my($old)= $PARGS{"sv_${rid}_${var}"};
		my($new)= $PARGS{"dat_${rid}_${var}"};

		$old=~s/\s+$//;
		$new=~s/\s+$//;
		$new = 'Y' if $new eq 'on' and $keycolumns{$var} eq "C";
		$old = 'Y' if $old eq 'on' and $keycolumns{$var} eq "C";

		# not quite sure why this is needed but.. it is
		next if $new eq "" and printable_words($pname) eq "LDAP USER";

		next if $keycolumns{$var} eq "N";		# key value
		next if $old eq $new;		# obvious

		if( $keycolumns{$var} eq "C" ) {	# FUN LOGIC!
			diagnose( "FOUND $PARGS{$_} : row=$rid Var=$var Type=$keycolumns{$var} old=$old new=$new" );
			$new = 'N' unless $new;
			$old = 'N' unless $old;
			next if $old eq $new;		# obvious
		}

		$num_differences++;
		my($x)=$PARGS{"sv_admin"};

		my(%hash);
		foreach (keys %keycolumns) {
			$hash{$_}= $PARGS{"sv_".$rid."_".$_};
		}

		$hash{Credential_Type}=$hash{Principal_Type} if $hash{Principal_Type};
		$hash{Credential_Name}=$hash{Principal_Name} if $hash{Principal_Name};


		print "<b>*** Setting Field '".printable_words($pname)."' to '".$new."' ( from '".$old."' ) for ".
					$hash{Credential_Name}." (".
					$hash{Credential_Type}.")</b><br>";

		my(@rc)=MlpManageData( -canunderstandsql=>$i_am_able_to_understand_sql,
					Operation=>"Autoset",Admin=>$x, %hash, -debug=>$debug,
					SetField=>$var,Value=>$new,-NL=>"<BR>\n");
		if( $#rc >= 0 ) {
			print "<PRE>";
			foreach (@rc) {
				if( /^ERROR:/ ) {
					print errorbox($_);
				} else {
					print $_,"\n";
				}
			}
			print "</PRE>",br;
		}
	};

	if( $num_differences == 0 ) {
		print "<p><TABLE BORDER=1 WIDTH=100% BGCOLOR=WHITE><TR><TD ALIGN=LEFT>";
		print "<FONT face=sans-serif size=+1 color=red>";
		print "<h2>SAVE CHANGES BUTTON PUSHED BUT NO CHANGES WERE DETECTED</h2>";
		print "</FONT><FONT face=sans-serif color=blue>";
		if( $i_am_able_to_understand_sql eq "Y" ) {
			foreach ( sort keys %PARGS) { print "DIAG $_ :".$PARGS{$_}.br unless /HEADERTEXT/; }
		}
		print "</FONT></TD>\n</TR></TABLE><p>\n";

	} else {
		$admin=$PARGS{"sv_admin"};
		#my_Page_Footer();
	}
}
#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

my($tm)=do_time( -fmt=>"mm/dd/yy hh:mi" );
if( defined $PARGS{"TITLE"} ) {
	print drawbanner( b($PARGS{"TITLE"}),"", $tm );
} else {
	print drawbanner( b("Alarm Administration Screen"),"", $tm ),br;
}

#diag_showparam(__LINE__,1) if $i_am_able_to_understand_sql eq 'Y';

my($hfont)='';
my($ehfont)='';

my_Nav_Bar();

print "<A NAME=StartOfDataSections>\n";

if( $PARGS{HELPFORM} ) {	# special HELPFORM of this name
	print "HELPFORM Attribute Detected - Fetching Dynamic Help",p;
	use Inventory;
	print INV_gethtmlforms( $PARGS{ADMIN} );
	my_Page_Footer();
}

#
# RUN TYPE 1 - RUN EXTERNAL CODE SNIPPET INLINE VIA EVAL
#
print "\n",$PARGS{"HEADERTEXT"}."\n".
	$PARGS{"HEADERTEXT1"}."\n".
	$PARGS{"HEADERTEXT2"}."\n".
	$PARGS{"HEADERTEXT3"}."\n".
	$PARGS{"HEADERTEXT4"}."\n".
	$PARGS{"HEADERTEXT5"}."\n".
	$PARGS{"HEADERTEXT6"}."\n".
	$PARGS{"HEADERTEXT7"}."\n".
	$PARGS{"HEADERTEXT8"}."\n".
	$PARGS{"HEADERTEXT9"}."\n".
	$PARGS{"HEADERTEXT10"}."\n".
	$PARGS{"HEADERTEXT11"}."\n".p
	if defined $PARGS{"HEADERTEXT"} or defined $PARGS{"HEADERTEXT1"};

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

if( $PARGS{'EXTERNAL_CGI_EVALFILE'} ) {
	code_section("EXTERNAL CGI INLINE",__LINE__);
	debugmsg( "Line=".__LINE__.": CALLING EXTERNAL CGI INLINE- ".$PARGS{'EXTERNAL_CGI_EVALFILE'} );
	my($cmd)=$PARGS{'EXTERNAL_CGI_EVALFILE'};
	my($codeblock)='';
	open(G,$cmd) or die "CANT READ $cmd";
	while(<G>) { $codeblock.=$_; }
	close(G);
		use DBI;
	#	foreach (keys %PARGS) { print "parameter $_ => ",$PARGS{$_}," <br>\n"; }
		# OK INSERT SOME STUFF HERE
	#	print "EVAL CALL FOLLOWS",hr;
		my($xx)="THIS IS A TEST PRINT FROM AN EVAL";
		#my($code)="print \$xx;";
		# $codeblock='print $xx;';
		#print $xx;

		eval $codeblock;
		print "ERROR".$@ if $@;
	#	print hr,"DONE - INSERT SOME STUFF HERE",br;
	my_Page_Footer();

#
# RUN TYPE 2 - system() call using inerpolated parameters
# that means any p(x) get replaced by $PARGS{x}
#
} elsif( $PARGS{'EXTERNAL_CGI_SCRIPT'} ) {
	code_section("EXTERNAL CGI",__LINE__);
	debugmsg( "Line=".__LINE__.": CALLING EXTERNAL CGI - ".$PARGS{'EXTERNAL_CGI_SCRIPT'} );
	my($cmd)=$PARGS{'EXTERNAL_CGI_SCRIPT'};

	foreach ( keys %PARGS ) {
		next unless $cmd =~ /p\($_\)/;
		my($v)=$PARGS{$_};
		$cmd =~ s/p\($_\)/$v/;
	}
	if( $cmd =~ /p\((.+)\)/ ) {
		print errorbox("ERR".__LINE__.": EXTERNAL CGI: THE ".b($PARGS{'TITLE'})." REPORT REQUIRES ARGUMENTS",br,"PLEASE SELECT APPROPRIATE ARGUMENTS AT THE TOP OF THE SCREEN</FONT>\n");
	} else {
		developer_diag($cmd);
		system($cmd);
	}
	my_Page_Footer();
}
#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

#
# RUN TYPE 3 - NORMAL MLPALARM FUNCTION REPORT
#
code_section("FETCH  DATA - Report=$admin",__LINE__);
$!=undef;
$rc=MlpRunScreen(
	-screen_name=>$admin, -html=>1,
	-debug				=>	$debug,
	-severity			=>	$PARGS{'filter_severity'}			||undef,
	-likefilter		 	=>	$PARGS{'filter_likefilter'}		||undef,
	-filter_num_days	=>	$PARGS{'filter_num_days'}			||undef,
	-credentialfilter	=>	$PARGS{'filter_credentials'}		||undef,
	-showdeleted		=>	$PARGS{'filter_showdeleted'}		||undef,
	-production		 	=>	$PARGS{'filter_prodchkbox'}		||undef,
	-userpermission 	=> $PARGS{"filter_userpermission"}	||undef,
	-container			=>	$PARGS{'filter_container'}			||undef,
	-maxtimehours		=>	$PARGS{'filter_time'}				||undef,
	-ldap_user			=>	$PARGS{'filter_ldap_user'}			||undef,
	-ldap_group			=>	$PARGS{'filter_ldap_group'}		||undef,
	-system				=>	$PARGS{'filter_system'}				||undef,
	-program				=>	$PARGS{'filter_program_name'}		||undef,
	-poweruser			=>	$i_am_able_to_understand_sql 		||undef,
	-systype				=>	$PARGS{'filter_system_type'}		||undef,
	-matchstatus		=>	$PARGS{'filter_matchstatus'}		||undef,
	-OTHER_ARG        => $PARGS{filter_approve}				||undef,
	-OTHER_ARG2			=> $PARGS{filter_department}			||undef,
	-NL					=>	"<br>\n",
	-LOGON_USER			=> $LOGON_USER
);
if( ! ref $rc ) {
	print errorbox($rc),p if $rc;
	errorout('');
}

my($skip_no_rows_errors);

if( $#{$rc} == 0 ) { 			# filtered to n rows
	my( @data_rows )= @$rc;
	my( $hdr_row )=shift(@data_rows);
	my( @hdr ) = @$hdr_row;
	$skip_no_rows_errors = "TRUE" if $hdr[$#hdr] eq "Both";
	if( ! defined $skip_no_rows_errors ) {
		print errorbox("ERR".__LINE__.": NO DATA RETURNED FOR REPORT "),br,br;
		my_Page_Footer();
	}
}
code_section("FETCH COMPLETED - ".$#{$rc}." Rows Returned",__LINE__);

#print "\n",$PARGS{"HEADERTEXT"}."\n".
#	$PARGS{"HEADERTEXT1"}."\n".
#	$PARGS{"HEADERTEXT2"}."\n".
#	$PARGS{"HEADERTEXT3"}."\n".
#	$PARGS{"HEADERTEXT4"}."\n".
#	$PARGS{"HEADERTEXT5"}."\n".
#	$PARGS{"HEADERTEXT6"}."\n".
#	$PARGS{"HEADERTEXT7"}."\n".
#	$PARGS{"HEADERTEXT8"}."\n".
#	$PARGS{"HEADERTEXT9"}."\n".
#	$PARGS{"HEADERTEXT10"}."\n".
#	$PARGS{"HEADERTEXT11"}."\n".p
#	if defined $PARGS{"HEADERTEXT"} or defined $PARGS{"HEADERTEXT1"};

debugmsg( br."Line=".__LINE__.": Completed Exec Of Admin Screen ".$admin);
print $! unless defined $rc;
errorout( "ERROR: Screen Returned No Data")	unless defined $rc and $rc != 0;

my( @data_rows )= @$rc;
my( $hdr_row )=shift(@data_rows);
debugmsg( "Line=".__LINE__.": Number Of Report Rows=".$#data_rows);
debugmsg( "Line=".__LINE__.": Report Header=".join(", ",@$hdr_row));
if( $#data_rows<0 and ! defined $skip_no_rows_errors ) { 			# filtered to n rows
	print errorbox("ERR".__LINE__.": NO DATA RETURNED FOR REPORT "),br,br;
	my_Page_Footer();
}

# DIAGNOSTIC BLOCK - PRINT THE RAW DATA CLEANLY - DONT DELETE
if( $results_dumper_mode ) {
	code_section("RUNNING DUMPER",__LINE__);
	use Data::Dumper;
	print "<PRE>";
	print Dumper \@data_rows;
	print "</PRE>";
}

# SET THE OPERATION AS PER THE SPEC (below)
my($operation) = $$hdr_row[$#$hdr_row];
if( 	$operation eq "Both" or $operation eq "Update" or $operation eq "Delete"
	or $operation eq "Add" or $operation eq 'OkDelete'  ){
	pop 	@$hdr_row;
	push 	@$hdr_row,"Operation" unless defined $PARGS{"NONAVIGATION"};
} else {
	debugmsg("DIAG - SETTING OPERATION ($operation) TO NULL\n");
	$operation='';
}

debugmsg( "Line=".__LINE__.": Header is ".join(",",@$hdr_row));
debugmsg( "Line=".__LINE__.": Operation is \"$operation\" " );
debugmsg( "Line=".__LINE__.": NONAVIGATION=". $PARGS{"NONAVIGATION"} );

#code_section("Starting Data Header Processing",__LINE__);
my(%chkbox_mask, %txtbox_mask, %popup_mask, %radio_mask);
my($ROWCOLORFOUND,$HYPERLINKFOUND,$HYPERLINKFOUNDBLANK)=('','','');
my($number_of_header_columns)=0;

#
# Print The Header Row
#
my($HEADER_ROW_TEXT)="<TR class=menuheader>\n";
$HEADER_ROW_TEXT.="<TH>Rid</TH>" if $PARGS{PRINT_ROWID};
my($found_chkbox)=0;
my($found_txtbox)=0;
my($found_popup)=0;
my($found_radio)=0;
my(%FIELDS_AND_TYPES);		# FIELD NAME - VALUE=TYPE CTP
my(@prhdr_row);

# SETUP MASKS + PRINT THE HEADER ROW
foreach ( @$hdr_row ) {

	my($x)=$_;
	if( $_ eq "COLOR" ) {
		$ROWCOLORFOUND = 'TRUE';
		next;
	}
	if( $_ eq "HYPERLINK" ) {
		$HYPERLINKFOUND = 'TRUE';
		next;
	}
	if( $_ eq "HYPERLINKBLANK" ) {
		$HYPERLINKFOUNDBLANK = 'TRUE';
		next;
	}

	push @prhdr_row,$_;
	#
	# IF ^Chkbox_ (CHECKBOX SETUP SO THE SAVE BUTTON WORKS)
	#
	if( /^Chkbox_/ ) {
		$chkbox_mask{$number_of_header_columns}=1;
		$x=~s/^Chkbox_//;
		$found_chkbox=1;
	} elsif( /^Popup_/ ) {
		$popup_mask{$number_of_header_columns}=1;
		$x=~s/^Popup_//;
		$found_popup=1;
	} elsif( /^Radio_/ ) {
		$radio_mask{$number_of_header_columns}=1;
		$x=~s/^Radio_//;
		$found_radio=1;
	#
	# IF ^Txtbox_ (TEXTBOX SETUP SO THE SAVE BUTTON WORKS)
	#
	} elsif( /^Txtbox(\d+)_/ ) {
		$txtbox_mask{$number_of_header_columns}=$1;
		$x=~s/^Txtbox\d+_//;
		$found_txtbox=1;
	} elsif ( /^Txtbox/ ) {
		errorout( 'ERROR: TEXTBOX not defined using Txtbox\d+_ FORMAT' );
	}

	$x =~ s/^NOADD_//;
	$x =~ s/^INV_//;
	$x=~s/_YN_LU$//;
	$x=~s/_LU$//;
	$x=~s/\_/ /g;

	$HEADER_ROW_TEXT.= "\t<TH>".$hfont.MyUcfirst($x).$ehfont."</TH>\n";
	$number_of_header_columns++;
}

my($IS_Sectional_Report)='TRUE' if ${$data_rows[0]}[0] eq "NEXTFORM";
#if( $#data_rows>$ROWLIMIT
#	and $IS_Sectional_Report ne 'TRUE'
#	and ( $found_popup==1 or $found_txtbox ==1 or $found_chkbox==1 or $found_radio==1 ) ) {
#	#print errorbox( "$ROWLIMIT ROW LIMIT ON EDITABLE REPORT - TURNING OFF EDIT MODE <br>USE FILTERS TO REDUCE THE RESULT SET" ),p;
#	#$found_popup= $found_txtbox =	$found_chkbox= $found_radio=0;
#	#%chkbox_mask = %txtbox_mask = %radio_mask = %popup_mask = ();
#	print errorbox( "WARNING $ROWLIMIT ROW LIMIT ON EDITABLE REPORT <br>FOR BEST FUNCTIONALITY USE FILTERS TO REDUCE THE RESULT SET" ),p;
#
#}
$number_of_header_columns++ if $PARGS{PRINT_ROWID};	 # for the RID column
$HEADER_ROW_TEXT .= "</TR>\n";
print "<TABLE WIDTH=100\% BORDER=1>\n\t";
print "<TR class='menuheader'><TH COLSPAN=$number_of_header_columns>",
			submit(-name=>"Save Changes"),
      "</TH></TR>\n" if $PARGS{HDR_SAVE_ON_REPEAT};
print	$HEADER_ROW_TEXT unless $IS_Sectional_Report eq 'TRUE';

#
# END HEADER SECTION
#

# if the last item on the header row is "Operation" then include add line
my(%EXPAND_YN_FIELDS);
if( ! defined $PARGS{"NONAVIGATION"} ) {
	debugmsg( "Line=".__LINE__.": Header is ".join(",",@$hdr_row)." Operation=\"$operation\" " );

	if( $operation eq "Both" or $operation eq "Update"	or	$operation eq "Add" ) {
		debugmsg( "Line=".__LINE__.": Printing $operation Add/Update Rows" );
		print "<TR>";
		my($cnt)= -1;
		foreach my $c ( @prhdr_row ) {
			debugmsg( "Line=".__LINE__.": Col Header is $c" );
			next if $c eq "COLOR" or $c eq "HYPERLINK" or $c eq "HYPERLINKBLANK";
			$cnt++;
			debugmsg( "Line=".__LINE__.": Col Header is $c" );

			$EXPAND_YN_FIELDS{$cnt}=1 if $c=~/^Use/ or $c=~/YN_LU/ or $c=~/^Is_/ or $c=~/^Enabled/;
			$EXPAND_YN_FIELDS{$cnt}=1 if $c=~/^Chkbox/ and  $#data_rows>$ROWLIMIT;

			print "<TD>",$hfont;
			if( $c eq "Operation" ) {
				if( $operation eq "Update" ) {
					print submit(-name=>"add_$butncnt",-value=>"Update Row");
				} else {
					print submit(-name=>"add_$butncnt",-value=>"Add Row");  # Both or Add
				}
				$butncnt++;
			} elsif( $c=~/^NOADD/ ) {
				print "<Center>&nbsp;[cant change]&nbsp;</center>";
			} elsif( $c=~/_LU$/ ) {
				my($x)=$c;
				my($colnm)=strip_words($c);
				my($cellnm)="add_".strip_words($c);
				Delete($cellnm);
				print process_lookup_field($cellnm,$cnt);
			} else {
			debugmsg( "Line=".__LINE__.": Textbox for $c" );
				my($length)=15;
				$length = 2 if $c=~/Use_/;
				$length = 40 if $c=~/Description/;
				$length = 25 if $c=~/Business_System/;
				$length=$txtbox_mask{$cnt} if $txtbox_mask{$cnt};
				print textfield(-name=>"add_".$c, -size=>$length, -maxlength=>100);
			}
			print $ehfont,"</TD>\n";
		}
		print "</TR>\n";
	} else {
		my($cnt)= -1;
		foreach ( @prhdr_row ) {
			if( $_ eq "COLOR" or $_ eq "HYPERLINK" or $_ eq "HYPERLINKBLANK" ) {
				next;
			}
			$cnt++;

			$EXPAND_YN_FIELDS{$cnt}=1 if ( /^Use/ and !/^User/ ) or /YN_LU/ or /^Is_/ or /^Enabled/;
			$EXPAND_YN_FIELDS{$cnt}=1 if /^Chkbox/ and  $#data_rows>$ROWLIMIT;
		}
	}
}

#print hr;
#foreach ( @$rc ) { print ${$_}[0], br if ${$_}[0] eq "NEXTFORM"; }
#foreach ( keys %EXPAND_YN_FIELDS ) { print "EX",$_," ",$EXPAND_YN_FIELDS{$_},br; }
#print hr;

# DIAGNOSTIC BLOCK - PRINT THE RAW DATA CLEANLY - DONT DELETE
#use Data::Dumper;
#print "<PRE>";
#print Dumper \@data_rows;
#print "</PRE>";
# END BLOCK

code_section("READY TO PRINT REPORT BODY",__LINE__);
debugmsg( "Line=".__LINE__.": Data Rows = ".$#data_rows );
my($true_rowcount)=1;
my($rowcount)=0;
my($sectioncount)=0;
my($cursectionhdrtxt)='';
my($currentpage)=0;
foreach my $row ( @data_rows ) {

	if( $rowcount>3 and $PARGS{"DIAGNOSE"} ) {
		diagnose( "Line=".__LINE__.": DIAGNOSTIC MODE - ENDING AFTER THIS ROW " );
		last;
	}

	my($color)="BGCOLOR=#FFFFEE";
	my($link_start,$link_end)=("","");

	if(( $rowcount%12 == 11 and $PARGS{"HDR_FORM_REPEATS"} )
	or ( $IS_Sectional_Report eq 'TRUE' and ${$row}[0] eq "NEXTFORM")
	) {
		my($is_Token)='TRUE' if ${$row}[0] eq "NEXTFORM";

		# its a section report.
		$sectioncount++ if $is_Token eq 'TRUE';		# goes from 1 up...

		print "</TABLE>\n";

		my_end_form();

		# is it a page report
		if( $is_Token eq 'TRUE' ) {
			$true_rowcount=1;
			$currentpage=0;
		} elsif( $currentpage==0 ) {			#its a paged report!
			$currentpage=2;
		} elsif( $currentpage>0 ) {
			$currentpage++;
		}

		# print __LINE__," PAGE=$currentpage TXT=",${$row}[0],br;
		if( $is_Token eq 'TRUE' and ${$row}[1] =~ /CERTIFY/ ) {
			my($cb)= submit(-name=>"submit",-value=>"Certify Section");
			$cb .= "&nbsp;".submit(-name=>"submit",-value=>"Certify Changes");

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

			$cb.= hidden( -name=>"filter_system", -default=>$PARGS{'filter_system'})."\n"
				if defined $PARGS{'filter_system'};
			$cb.= hidden( -name=>"filter_approve", -default=>$PARGS{'filter_approve'})."\n"
				if defined $PARGS{'filter_approve'};
			$cb.= hidden( -name=>"filter_ldap_group", -default=>$PARGS{'filter_ldap_group'})."\n"
				if defined $PARGS{'filter_ldap_group'};
			$cb.= hidden( -name=>"CANCERTIFY", -default=>$PARGS{'CANCERTIFY'})."\n"
				if defined $PARGS{'CANCERTIFY'};
			${$row}[1] =~ s/CERTIFY/$cb/;
		}

		if( $currentpage==0 ) {					  # must be a section token only
		#	print __LINE__," Setting Section Report",br;
			my($lsec)=$sectioncount-1;  $lsec=1 if $lsec<1;
			my($prev)="<font size=-2><A HREF=#StartOfDataSections>LAST SECTION</A></font>\n"
				if $IS_Sectional_Report eq 'TRUE';	# if $sectioncount>1;
			my($next)="<font size=-2><A HREF=#SECTION".($sectioncount+1).">NEXT SECTION</A></font>\n"
				if $IS_Sectional_Report eq 'TRUE';
			$cursectionhdrtxt = "\n".start_multipart_form()."\n".
			   "<TABLE WIDTH=100\% BORDER=1>
   <TR class='menuheader'>
      <TH COLSPAN=$number_of_header_columns>
         <TABLE WIDTH=100\%><TR>
         <TH ALIGN=LEFT>TAG_PREVSECTN</TH>
         <TH ALIGN=CENTER >";
$cursectionhdrtxt .= "<b>".${$row}[1]."</b>" if $IS_Sectional_Report eq 'TRUE';
$cursectionhdrtxt .= "TAG_PAGETXT</TH>
         <TH ALIGN=RIGHT>TAG_NEXTSECTN</TH>
         </TR></TABLE>
      </TH>
   </TR>\n".
        $HEADER_ROW_TEXT;

			my($reformated)=$cursectionhdrtxt;
			if( $PARGS{HDR_SAVE_ON_REPEAT} ) {
				my($SUBMIT_BUTTON)=submit(-name=>"Save Changes");
				$reformated=~s/TAG_PAGETXT/$SUBMIT_BUTTON/g;
			} else {
				$reformated=~s/TAG_PAGETXT/&nbsp;&nbsp;&nbsp;&nbsp;/g;
			}
			$reformated=~s/TAG_PREVSECTN/$prev/g;
			$reformated=~s/TAG_NEXTSECTN/$next/g;
			print "<A NAME=SECTION".$sectioncount.">".$reformated;
		} elsif( $cursectionhdrtxt eq '' ) {  # this case is themod row - page report

			my($lsec)=$sectioncount-1;  $lsec=1 if $lsec<1;
			my($prev)="<font size=-2><A HREF=#StartOfDataSections>LAST PAGE</A></font>\n";
			my($next)="<font size=-2><A HREF=#SECTION".($sectioncount+1).">NEXT PAGE</A></font>\n";

			$cursectionhdrtxt = "\n".start_multipart_form()."\n".
			   "<TABLE WIDTH=100\% BORDER=1>
   <TR class='menuheader'>
      <TH COLSPAN=$number_of_header_columns>
         <TABLE WIDTH=100\%><TR>
         <TH ALIGN=LEFT>TAG_PREVSECTN</TH>
         <TH ALIGN=CENTER >";
$cursectionhdrtxt .= "<b>".${$row}[1]."</b>" if $IS_Sectional_Report eq 'TRUE';
$cursectionhdrtxt .= "TAG_PAGETXT</TH>
         <TH ALIGN=RIGHT>TAG_NEXTSECTN</TH>
         </TR></TABLE>
      </TH>
   </TR>\n".$HEADER_ROW_TEXT;

			my($reformated)=$cursectionhdrtxt;

			my($pgtxt)="&nbsp; <font size=-2>page $currentpage</font>";
			$pgtxt= submit(-name=>"Save Changes")." ".$pgtxt if $PARGS{HDR_SAVE_ON_REPEAT};
			$reformated=~s/TAG_PAGETXT/$pgtxt/g;
			$reformated=~s/TAG_PREVSECTN/$prev/g;
			$reformated=~s/TAG_NEXTSECTN/$next/g;
			#print "<A NAME=SECTION".$sectioncount.">".$reformated;
			print "<A NAME=PG_".$currentpage."_SEC_".$sectioncount.">".$reformated;

		} else {
			my($reformated)=$cursectionhdrtxt;
			$reformated=~s/\#SECTION(\d+)/\#PG_${currentpage}_SEC_$1/g;
			my($pgtxt)="&nbsp; <font size=-2>page $currentpage</font>";
			$pgtxt= submit(-name=>"Save Changes")." ".$pgtxt if $PARGS{HDR_SAVE_ON_REPEAT};
			$reformated=~s/TAG_PAGETXT/$pgtxt/g;

			my($lsec)=$sectioncount-1;  $lsec=1 if $lsec<1;
			my($prev)="<font size=-2><A HREF=#";
			if( $currentpage==2 ) {
				$prev.="SECTION".$lsec;
			} else {
				$prev.="PG_".($currentpage-1)."_SEC_".$sectioncount;
			}
			$prev.=">LAST PAGE</A><br>";
			$prev.="<A HREF=#SECTION".$lsec.">LAST SECTION</A></font>"
					if $IS_Sectional_Report eq 'TRUE';
			my($next)="<font size=-2><A HREF=#PG_".($currentpage+1)."_SEC_".$sectioncount.">NEXT PAGE</A><br>";
			$next.="<A HREF=#SECTION".($sectioncount+1).">NEXT SECTION</A></font>"
					if $IS_Sectional_Report eq 'TRUE';
			$reformated=~s/TAG_PREVSECTN/$prev/g;
			$reformated=~s/TAG_NEXTSECTN/$next/g;

			#print "ANCHOR=PG_".$currentpage."_SEC_".$sectioncount;
			# print "DBG DBG: Line ",__LINE__,"\n",br;
			print "<A NAME=PG_".$currentpage."_SEC_".$sectioncount.">".$reformated;
			# print "\nDBG DBG: Line ",__LINE__,"\n",br;
		}

		$rowcount=0;
		$currentpage=0 if $is_Token eq 'TRUE';
		next if $is_Token eq 'TRUE';
	}

	print "<TR>\n";
	if( $ROWCOLORFOUND ) {
		my($c)=shift @$row;
		$color='BGCOLOR='.$c if $c;
	}
	$hfont = $ehfont = '';
	if( $HYPERLINKFOUND or $HYPERLINKFOUNDBLANK ) {
		my($h)=shift @$row;
		if( $h !~ /^\s*$/ ) {
			$h =~ s/ /%20/g;
			$hfont = $link_start = "<A HREF=$h>" if $HYPERLINKFOUND;
			$hfont = $link_start = "<A Target=\_blank HREF=$h>" if $HYPERLINKFOUNDBLANK;
			$ehfont = $link_end   = "</A>";
		}
	}

	# not sure what this is for
	#if( $$row[0] eq "_NOTES" ) {
	#	print "<TD $color align=RIGHT>==></TD><TD $color COLSPAN=$number_of_header_columns>",$$row[1],"</TD>";
	#} els
	if( 	$operation eq "Both"
	or 	$operation eq "Ok"
	or		$found_chkbox==1 or $found_popup==1 or	$found_txtbox==1 or $found_radio==1
	or    $operation eq "OkDelete"
	or 	$operation eq "Delete" ) {
		diagnose( "Line=".__LINE__.": Row $rowcount Operation=$operation ckbx=$found_chkbox popup=$found_popup txt=$found_txtbox radio=$found_radio" );

		print "<TD $color>",$true_rowcount++,"</TD>" if $PARGS{PRINT_ROWID};
		# button name del_$butncnt
		# hidden row values are sv_$butncnt_key = value
		my($i)=0;
		foreach my $cell ( @$row ) {

			# must delete or the value from the previous form will overwrite
			$cell=~s/\s+$//;
			my($colnm)=strip_words($prhdr_row[$i]);
			my($svfldname)="sv_".$butncnt."_".$colnm unless $cell =~ /\<br\>/;
			my($cellnm)  ="dat_".$butncnt."_".$colnm;
			Delete($cellnm);
			Delete($svfldname) if $svfldname;

			diagnose( "Line=".__LINE__.": Row $rowcount Col=$colnm FieldValue=$cell CellName=$cellnm SvNm=$svfldname C=$chkbox_mask{$i} T=$txtbox_mask{$i} P=$popup_mask{$i}" );
			#print "Line=".__LINE__.": Row $rowcount Col=$colnm FieldValue=$cell CellName=$cellnm SvNm=$svfldname C=$chkbox_mask{$i} T=$txtbox_mask{$i} P=$popup_mask{$i}",br;

			my($pr_val)=$cell;
			if( $EXPAND_YN_FIELDS{$i} ) {
				$pr_val="YES" if $cell=~/^y$/i or $cell=~/^yes$/i;
				$pr_val="NO"  if $cell=~/^n$/i or $cell=~/^no$/i;
			}
			print "\t<TD $color>";;
			if( defined $chkbox_mask{$i} and $operation ne "Both" and $operation ne "Delete" ) {
				diagnose( "Line=".__LINE__.": Checkbox" );
				print $hfont if $prhdr_row[$i] eq "Chkbox_LDAP_Group" or  $prhdr_row[$i] eq "Chkbox_LDAP_User";
				if( $cell eq "Y" or $cell == 1 ) {
					print checkbox( -name=>$cellnm, -checked=>'checked', -value=>'Y', -label=>'' );
				} else {
					print checkbox( -name=>$cellnm, -label=>'', -value=>'Y'  );
				}
				print $ehfont if $prhdr_row[$i] eq "Chkbox_LDAP_Group" or  $prhdr_row[$i] eq "Chkbox_LDAP_User";
				$FIELDS_AND_TYPES{$colnm} = 'C';
			} elsif( defined $radio_mask{$i} ) {
				diagnose( "Line=".__LINE__.": radio_group" );
				$FIELDS_AND_TYPES{$colnm} = 'R';
				$cell = 'Unknown' unless $cell;
				#print "DBGDBG Cell $cellnm $cell ";
				#my($found);foreach ('Approve','Remove','Unknown') { $found = 'YES' if $_ eq $cell; } print "FOUND=",$found,br;
				print "\n",radio_group(-name=>$cellnm,
                             -values=>['Approve','Remove','Unknown'],
                             -default=>$cell ),"\n";
                             # -linebreak=>'true',
						           # -labels=>\%labels,
			} elsif( defined $txtbox_mask{$i}  and $operation ne "Both" and $operation ne "Delete" ) {
				diagnose( "Line=".__LINE__.": textfield" );
				print textfield(-name=>$cellnm, -size=>$txtbox_mask{$i}, -maxlength=>100, -default=>$cell);
				$FIELDS_AND_TYPES{$colnm} = 'T';
#			} elsif( defined $radio_mask{$i}  and $operation ne "Both" and $operation ne "Delete" ) {
#				diagnose( "Line=".__LINE__.":process_lookup_field");
#				print "\n",process_lookup_field($cellnm,undef,$cell),"\n";
#				$FIELDS_AND_TYPES{$colnm} = 'R';
			} elsif( defined $popup_mask{$i}  and $operation ne "Both" and $operation ne "Delete" ) {
				diagnose("Line=".__LINE__.":process_lookup_field");
				print "\n",process_lookup_field($cellnm,undef,$cell),"\n";
				$FIELDS_AND_TYPES{$colnm} = 'P';
			} else {
				diagnose("Line=".__LINE__.":standard");
				print $hfont.$pr_val.$ehfont;
				$FIELDS_AND_TYPES{$colnm} = 'N';
			}

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

			print "\n",hidden(-name=>$svfldname,-default=>$cell),"\n" if $svfldname;
			print	"</TD>\n";
			$i++;
		}
		if( $operation eq "Ok" ) {
			print "\t<TD $color>",submit(-name=>"ok_$butncnt",-value=>"Ok"),"</TD>\n"
							unless defined $PARGS{"NONAVIGATION"};
		} elsif( $operation eq "OkDelete" ) {

			my($dflt_action)="MsgDelete";		# we want Delete if its on Heartbeat tab
			$dflt_action="MsgOkPerm" if $admin eq "INV_CONSOLE_RPT"
					 or $admin eq "INV_OTHER_HB_RPT"
					 or $admin eq "INV_BACKUP_RPT";
			print "\t<TD $color ALIGN=CENTER>",
						submit(-name=>"actionid_$butncnt",-value=>"Submit"),
						popup_menu(	-name=>"actionval_$butncnt",
								-values=>\@hb_values,
								-default=>$dflt_action,
								-labels=>\%hb_operations ),
						"</TD>\n"
								unless defined param("NONAVIGATION");
		} elsif( $operation ne "" ) {
			print "\t<TD $color>",submit(-name=>"del_$butncnt",-value=>"Delete"),"</TD>\n"
							unless defined $PARGS{"NONAVIGATION"};
		};
		$butncnt++;
	} else {
		diagnose( "Line=".__LINE__.": NO OPERATION SUBCASE  " );
		my($i)=0;
		print "<TD $color>",$true_rowcount++,"</TD>" if $PARGS{PRINT_ROWID};
		foreach my $x (@$row) {
			if( $EXPAND_YN_FIELDS{$i} ) {
				$x="YES" if $x=~/^y/i;
				$x="NO"  if $x=~/^n/i;
			}
			#print $x,br;
			$x="&nbsp" if $x=~/^\[/ and $x!~/^\[\d+\s+repeats/;
			print "<TD $color>",$hfont,$x,$ehfont,"</TD>\n";
			$i++;
		}
	}
	print "</TR>\n";
	$rowcount++;
	print $HEADER_ROW_TEXT if $rowcount%12 == 0 and $PARGS{"HDR_ROW_REPEATS"};
}

#
# END OF ROW
#
print "</TABLE>",p,p;
diagnose( "Line=".__LINE__.": END TABLE " );
code_section("COMPLETED",__LINE__);
my_Page_Footer();

###############################################################################
###########                END PAGE - START SUBROUTINES          ##############
###############################################################################
my(%lookup_cache);
my($ldap_user_labels);

sub process_lookup_field {
	my($cellname,$cnt,$default_value)=@_;
	my($stripped)=strip_words($cellname);
	$default_value=~s/\s+$//;
	$_ = $stripped;
	diagnose(__LINE__.": $cellname");
	#diagnose(__LINE__.": cnt=$cnt");
	diagnose(__LINE__.": $stripped");

	#
	# DEBUG MASTER COMMENT
	#
	if( $lookup_cache{$stripped} ) {
		$default_value=${$lookup_cache{$stripped}}[0] unless $default_value;
		diagnose(__LINE__.": cached");
		#diagnose( "POPUP= -name=>'add_'".$stripped,' ',$_," -default=>$default_value" );

			#use Data::Dumper;
			#print "<PRE>";
			#print Dumper $ldap_user_labels if $ldap_user_labels;
			#print Dumper $lookup_cache{$_};
			#print "</PRE>";
			#die "DONE";

		if( $ldap_user_labels and /INV_LDAP_USER_LU/ ) {
			return popup_menu( -name=>$cellname, -values=>$lookup_cache{$_}, -labels=>$ldap_user_labels, -default=>$default_value);
		} else {
			return popup_menu( -name=>$cellname, -values=>$lookup_cache{$_}, -default=>$default_value );
		}

		#return popup_menu( -name=>$cellname,
		#	-values=>$lookup_cache{$_},
		#	-default=>$default_value );
	} else {
		diagnose(__LINE__.": NOT cached");
		#diagnose( "NO LOOKUP CACHE for $_ cellname=$cellname def=$default_value");
	}
	my(@x);

	if( /Start/ or /End/ ) {
		for (0..24) { push @x, $_; }
	} elsif( /User_LU/ ) {
		@x=MlpGetOperator();
	} elsif( /Program_LU/ ) {
		@x=MlpGetProgram();
		diagnose( __LINE__."Admin=$admin");
		unshift @x, "All Programs" if $admin eq "INV_ALARM_ROUTING";
	} elsif( /INV_LDAP_USER_LU/ ) {
		push @x, $dflt_user;
		my($list);
		if( $PARGS{ShowAll} ) {
			print "[ EXPANDED ]" if $i_am_able_to_understand_sql eq "Y";
			#push @x,	MlpGetPrincipal(-type=>'ldap user');
			($list,$ldap_user_labels)=MlpGetPrincipal(-type=>'ldap user',-displayable=>'Y',-getlabelhash=>1);

		#	foreach (@x) { print $_," " if /^ash/; }
		} else {
			#push @x,	MlpGetPrincipal(-type=>'ldap user',-displayable=>'Y', -sortorder=>'Dname');
			($list,$ldap_user_labels)=MlpGetPrincipal(-type=>'ldap user',-displayable=>'Y',-getlabelhash=>1);
		}
		push @x, @{$list};

			#use Data::Dumper;
			#print "<PRE>";
			#print Dumper $list;
			#print Dumper $hash;
			#print "</PRE>";
			#die "DONE";

	} elsif( /INV_CERTIFIER_LU/ ) {
		push @x, $dflt_user;
		push @x,	MlpGetPrincipal(-type=>'certifier');
	} elsif( /INV_LDAP_GROUP_LU/ ) {
		push @x, $dflt_group;
		if( $PARGS{ShowAll} ) {
			print "[ EXPANDED ]" if $i_am_able_to_understand_sql eq "Y";;
			push @x,	MlpGetPrincipal(-type=>'ldap group');
		} else {
			push @x,	MlpGetPrincipal(-type=>'ldap group',-displayable=>'Y');
		}
	} elsif( /INV_PRODUCTION_LU/ ) {
		push @x,	("PRODUCTION","DEVELOPMENT","QA","?");
	} elsif( /INV_UNMAPPED_GROUPS_LU/ ) {
	} elsif( /INV_Business_System_LU/ ) {
		push @x,$dflt_system;
		push @x,	MlpGetBusiness_System(-type=>$PARGS{REPORTKEYWORD});
	} elsif( /INV_System_LU/ ) {
		push @x,$dflt_system;
		push @x,	MlpGetSystem();
	} elsif( /INV_Database_LU/ ) {
		push @x, $dflt_system;
		push @x,	MlpGetSystem(-type=>'Database');
	} elsif( /INV_Hostname_LU/ ) {
		push @x, $dflt_system;
		push @x,	MlpGetSystem(-type=>'Host');
	} elsif( /System_LU/ ) {
		# @allsys= MlpGetContainer(-screen_name=>'All') if $#allsys==-1;
		@allsys= MlpGetSystem() if $#allsys==-1;
		push @x,@allsys;
	} elsif( /INV_Shared_Account_LU/ ) {
		# if this dude came from a web form... lets set him, and save that value as well
		if( $PARGS{SHOWUSER} ) {
			# print "DBGDBG WAY TO GO $PARGS{SHOWUSER}",br;
			$default_value = $PARGS{SHOWUSER};
			print hidden(-name=>'SHOWUSER',-default=>$PARGS{SHOWUSER});
		}
		push @x, $dflt_user;
		if( $PARGS{ShowAll} ) {
			print "[ EXPANDED ]" if $i_am_able_to_understand_sql eq "Y";
			push @x,	MlpGetPrincipal(-type=>'allcredentials');
		} else {
			push @x,	MlpGetPrincipal(-type=>'globalshared');
		}
	} elsif( /Principal_LU/ ) {
		push @x,	MlpGetPrincipal();
	} elsif( /Severity_LU/ ) {
		@x=MlpGetSeverity();
	} elsif( /^Use/ or /YN_LU/ or /^Is_/ ) {
		@x = ("YES","NO");
		$EXPAND_YN_FIELDS{$cnt}=1 if $cnt;
	} elsif( /^Container_LU/ ) {
		my(@containers)=MlpGetBusiness_System(-type=>$PARGS{REPORTKEYWORD}) if $#containers<0;
		print errorbox( "ERR".__LINE__.": NO BUSINESS SYSTEMS FOUND\n" ) if $#containers<0;
		@x = @containers;
	} elsif( /^Enabled/ ) {
		@x = ("YES","NO");
		$EXPAND_YN_FIELDS{$cnt}=1 if $cnt;
	} elsif( /^Chkbox_/ and /_LU$/ ) {
		# diagnose( "CELLNAME= -name=>$cellname, -checked=>0, -value=>'Y', -label=>'' " );
		return checkbox( -name=>$cellname, -checked=>0, -value=>'Y', -label=>'' );
	} elsif(( /^INV_/ or /^Popup_INV_/ ) and /_LU$/ ) {

		# if this dude came from a web form... lets set him, and save that value as well
		if( /INV_Credential_Type_LU/ and $PARGS{SHOWSYSTYP} ) {
			$default_value = $PARGS{SHOWSYSTYP};
			print hidden(-name=>'SHOWSYSTY',-default=>$PARGS{SHOWSYSTYP});
		}

		s/^Popup_//;
		s/^INV_//;
		s/_LU$//;

		@x=MlpGetLookupInfo($_);
		unshift @x,"[Select A Value]" if $#x>=0;
		$default_value="[Select A Value]" unless $default_value;
		@x = ("NO DATA FOR ${_}_LU","SEEK HELP") if $#x<0;
	} else {
		@x = ("UNABLE TO PROCESS LOOKUP {".$_."}","SEEK HELP") if $#x<0;
	}
	$lookup_cache{$stripped}=\@x;
	$default_value=$x[0] unless $default_value;
	diagnose(__LINE__.": dflt=$default_value");

	#diagnose( "CELLNAME= -name=>$cellname, -values=>\@x, -default=>$default_value " );

	if( $ldap_user_labels ) {
		return popup_menu( -name=>$cellname, -values=>\@x, -labels=>$ldap_user_labels, -default=>$default_value );
	} else {
		return popup_menu( -name=>$cellname, -values=>\@x, -default=>$default_value );
	}
}

sub my_Nav_Bar {

	return if defined param("NOHEADER") or $PARGS{NOHEADER};
	if( $PARGS{NAVFILE} ) {
		code_section("NavFile Found",__LINE__);
		foreach ( param() ) { print "DBGDBG my_Nav_Bar param(): $_ => ".param($_).br; }
		my($navbar)='';
		open(F,$PARGS{NAVFILE}) or die "CANT READ NAVFILE=$PARGS{NAVFILE}";
		while(<F>) { $navbar.=$_; }
		close(F);
		print $navbar;
		return;
	}
	code_section("NAVBAR Found - ".$PARGS{"NAVBAR"},__LINE__);
	my($SAVECHANGESFOUND);
	if( $PARGS{"NAVBAR"} eq "SA" ) {	# special case - dont draw a blank table
			$SAVECHANGESFOUND = 1;
	} elsif( $PARGS{"NAVBAR"} and $PARGS{"NAVBAR"} !~ /^\s*$/ and ! $PARGS{"NONAVIGATION"} ) {
		debugmsg( "Line=".__LINE__." NAVBAR defined - adding dashboard NAV bar ".$PARGS{"NAVBAR"});
		my($SAVE_CHANGES_EXISTS)="";

		my(@CHARARY)=split(//,$PARGS{"NAVBAR"});
		if( $PARGS{'CANCERTIFY'} eq 'Y' and $PARGS{"NAVBAR"} =~ /CE/ ) {
			# ok not a perfect test on the next line but should be no need for it in any case
			#	die "NO CERTIFY NOW BUTTON ON NAVBAR ".$PARGS{"NAVBAR"} unless $PARGS{"NAVBAR"} =~ /CE/;
			print "<TABLE BGCOLOR=#f0e9d9 WIDTH=100% BORDER=1><TR><TD><TABLE WIDTH=95% BORDER=0><TR>\n";
			print "<TD ALIGN=LEFT>",submit(-name=>"submit",-value=>"Certify Now"),"</TD>";

#developer_diag("LINE:",__LINE__." DBGDBG filter_system=".join("|",param('filter_system'))." PARG=".$PARGS{filter_system});

			print hidden( -name=>"filter_system", -default=>$PARGS{'filter_system'}),"\n"	if defined $PARGS{'filter_system'};
			print hidden( -name=>"filter_ldap_group", -default=>$PARGS{'filter_ldap_group'}),"\n"	if defined $PARGS{'filter_ldap_group'};
			print hidden( -name=>"CANCERTIFY", -default=>$PARGS{'CANCERTIFY'}),"\n"	if defined $PARGS{'CANCERTIFY'};
			print "<TD ALIGN=RIGHT>".textarea(-name=>'certification_notes',
                          -default=>'[ Your Notes Go Here ]',
                          -rows=>5,
                          -columns=>80)."</TD>";
         print "</TABLE></TD></TR></TABLE>",br;

		}
		{
			print "<TABLE BGCOLOR=#f0e9d9 WIDTH=100% BORDER=1><TR><TD><TABLE WIDTH=95% BORDER=0><TR>\n";
			my($CELLCOUNT)=0;
			while ( $#CHARARY>0 ) {
				my($ctype)=$CHARARY[0].$CHARARY[1];
				shift @CHARARY;
				shift @CHARARY;
				if( $ctype eq "SA" ) {
					#print "DBG: SAVE CHANGES FOUND",br;
					$SAVECHANGESFOUND=1;
					next;
				}
				$CELLCOUNT++;
				print "</TR><TR>" if $CELLCOUNT==5;
				print "<TD WIDTH=25%><center>",$hfont;

				if( $ctype eq "RR" ) {
					print submit(-name=>"submit",-value=>"Run Report");
				} elsif( $ctype eq "RE" ) {
					print submit(-name=>"submit",-value=>"Reset");
				} elsif( $ctype eq "SV" ) {
					my(@x)=MlpGetSeverity();
					# severity is kind of an oddball case cause you must have one... so i chose errors for default...
					$PARGS{filter_severity}='Errors' unless $PARGS{filter_severity} ;
					$PARGS{filter_severity}='Errors' if $PARGS{filter_severity} eq 'Ok';	# sillycodearound
					print "Severity: ",br.popup_menu( -name=>'filter_severity', -values=>\@x, -default=>$PARGS{filter_severity} );
				} elsif( $ctype eq "OE" ) {
					my(@x)=('ALL','OK','FAILED','RUNNING','NEVER_RUN');
					$PARGS{filter_severity}='FAILED' unless $PARGS{filter_severity};
					print "Severity: ",br.popup_menu( -name=>'filter_severity', -values=>\@x,
						-default=>$PARGS{filter_severity} );
				} elsif( $ctype eq "OX" ) {
					my(@x)=('ALL','ERROR');
					$PARGS{filter_severity}='ERROR' unless $PARGS{filter_severity};
					print "Severity: ",br.popup_menu( -name=>'filter_severity', -values=>\@x,
						-default=>$PARGS{filter_severity} );
				} elsif( $ctype eq "PO" ) {
					if( defined $PARGS{'filter_prodchkbox'} ) {
						print checkbox( -name=>'filter_prodchkbox', -value=>'Production Only', -checked=>1, -label=>"Production Only" );
					} else {
						print checkbox( -name=>'filter_prodchkbox', -value=>'Production Only', -checked=>0, -label=>"Production Only" );
					}
				} elsif( $ctype eq "TI" ) {
					my(@timevalues)=("4 Hours","8 Hours","Since 4PM","1 Day","3 Days", "5 Days","14 Days","30 Days");
					print "Time: ",br.popup_menu( -name=>'filter_time', -values=> \@timevalues, -default=>"1 Day" ),$ehfont,"</TD>";
				} elsif( $ctype eq "BS" ) {
					#my(@containers)=MlpGetContainer( -type=>'c') if $#containers<0;
					my(@containers)=MlpGetBusiness_System(-type=>$PARGS{REPORTKEYWORD});
					print errorbox( "ERR".__LINE__.": NO BUSINESS SYSTEMS FOUND\n" ) if $#containers<0;
					unshift @containers,$dflt_system;
#					# WHAT ARE YOUR PERMISSIONS... IF ONLY FOR 1 SYSTEM THEN DEFAULT THIS
#					# if you pass in a default system, set it, otherwise retrieve it!
					my($d)=$PARGS{filter_container} || $dflt_system;
#					print "\nD is $d FC=",param('filter_container')," Arg=",$PARGS{'filter_container'},"\n";
					Delete('filter_container');
					print "Business System: ",br.
						popup_menu( -name=>'filter_container', -default=>$d, -values=>\@containers );	#
				} elsif( $ctype eq "DE" ) {
					my(@dpts)= MlpGetDepartment();
					unshift @dpts,"All";
					my($dflt)=$PARGS{'filter_department'} || $dpts[0];
					print "Department: ".br.popup_menu( -name=>'filter_department', -values=>\@dpts, -default=>$dflt);
				} elsif( $ctype eq "MA" ) {
					#
					# MATCH STATUS IS SEPARATE - ONLY WORKS IF IT EXISTS
					#
					if( $PARGS{'filter_matchstatus'} ) {
						my(@rc)= ("OK","INVESTIGATE","UNUSEABLE","IT ADMIN","SYSADM","SERVICE","UNMAPPED","ALL");
						my($dflt)=$PARGS{'filter_matchstatus'} || $rc[0];
						print "Match: ".br.popup_menu( -name=>'filter_matchstatus', -values=>\@rc, -default=>$dflt);
					}
				} elsif( $ctype eq "SY" ) {
					@allsys= MlpGetSystem() if $#allsys==-1;
					my($dflt) = $dflt_system;
					unshift @allsys, $dflt_system;
					$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
					print "System: ".br.popup_menu( -name=>'filter_system', -values=>\@allsys, -default=>$dflt);
				} elsif( $ctype eq "SD" ) {
					@allsysD= MlpGetSystem(-type=>'Database') if $#allsysD==-1;
					my($dflt) = $dflt_system;
					unshift @allsysD, $dflt_system;
					$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
					print "Database: ".br.popup_menu( -name=>'filter_system', -values=>\@allsysD, -default=>$dflt);
				} elsif( $ctype eq "Sa" ) {
					@allsysD= MlpGetSystem(-type=>'ApproveRqdDatabase') if $#allsysD==-1;
					my($dflt) = $dflt_system;
					unshift @allsysD, $dflt_system;
					$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
					print "Database: ".br.popup_menu( -name=>'filter_system', -values=>\@allsysD, -default=>$dflt);
				} elsif( $ctype eq "SH" ) {
					if( $#allsysH==-1 ) {
						my($q)="select distinct system_name, system_type from INV_principal_map where system_type not in ('sqlsvr','oracle','sybase') order by system_name";
						foreach ( _querywithresults( $q,1) ) {
							my(@x)=dbi_decode_row($_);
							push @allsysH,$x[0];
						}
					}
					# @allsysH= MlpGetSystem(-type=>'Host')
					my($dflt) = $dflt_system;
					unshift @allsysH, $dflt_system;
					$dflt=$PARGS{'filter_system'} if defined  $PARGS{'filter_system'} and $PARGS{'filter_system'} ne $dflt_system;
					print "Sas70 Hostname: ".br.popup_menu( -name=>'filter_system', -values=>\@allsysH, -default=>$dflt);
				} elsif( $ctype eq "DB" ) {
					my(@aa)=("sqlsvr","sybase","mysql","oracle");
					my(%lbls) = (
					"sqlsvr" => "Microsoft Sql Server",
					"sybase" => "Sybase Sql Server",
					"mysql" => "Mysql Database",
					"oracle" => "Oracle Database"
					);

					unshift @aa,$dflt_systemtype;
					my($dflt)=$PARGS{filter_database} || $dflt_systemtype;
					print "Database: ".br.popup_menu( -name=>'filter_database', -labels=>\%lbls, -values=>\@aa, -default=>$dflt);
				} elsif( $ctype eq "HO" ) {
					my(@aa)=("win32servers","unix");
					my(%lbls) = (
					"win32servers" => "Windows Servers",
					"unix" => "Unix Servers"
					);

					unshift @aa,$dflt_systemtype;
					my($dflt)=$PARGS{filter_system_type} || $dflt_systemtype;
					print "System Type: ".br.popup_menu( -name=>'filter_hostname', -labels=>\%lbls, -values=>\@aa, -default=>$dflt);
				} elsif( $ctype eq "ST" ) {
					my(@filter_system_types)=MlpGetLookupInfo('SYSTEM_TYPE');
					unshift @filter_system_types,$dflt_systemtype;
					my($dflt)=$PARGS{filter_system_type} || $dflt_systemtype;
					#print "DEFAULT=$dflt";
					#print "filter_system_types=",join(" ",@filter_system_types),br;
					print "System Type: ".br.popup_menu( -name=>'filter_system_type', -values=>\@filter_system_types, -labels=> -default=>$dflt);
				} elsif( $ctype eq "AS" ) {
					my(@filter_system_types)=("[Choose Permission]","APPROVER","DASHBOARD POWERUSER");
					print "User Type: ".br.popup_menu( -name=>'filter_userpermission', -values=>\@filter_system_types, -default=>$filter_system_types[0]);
				} elsif( $ctype eq "PR" ) {
					@allprg= MlpGetProgram() if $#allprg == -1;
					my($dflt) = $dflt_program;
					unshift @allprg, $dflt_program;
					$dflt=$PARGS{'filter_program_name'} if defined  $PARGS{'filter_program_name'} and $PARGS{'filter_program_name'} ne $dflt_program;
					print "Program: ".br.popup_menu( -name=>'filter_program_name', -values=>\@allprg, -default=>$dflt);
				} elsif( $ctype eq "CE" ) {
					print "&nbsp;";		# cant certity now here - its above
				} elsif( $ctype eq "__" ) {
					print "&nbsp;";
				} elsif( $ctype eq "AL" ) {
					print checkbox( -name=>'ShowAll', -value=>'Show All Groups/Users', -label=>"Show All Groups/Users" );
				#} elsif( $ctype eq "SA" ) {
				#	$SAVECHANGESFOUND=1;
				#	print submit(-name=>"Save Changes");
#				} elsif( $ctype eq "DY" ) {
#					my($d)=$PARGS{filter_num_days};
#					$d = '30' if $d=~/^\s*$/;
#					print 'Num Days:',br,textfield(-name=>"filter_num_days", -default=>$d,-size=>3, -maxlength=>5);
				} elsif( $ctype eq "LF" ) {
					my($d)=$PARGS{filter_likefilter};
					$d = '' if $d=~/^\s*$/;
					print 'Server Like Filter:',br,textfield(-name=>"filter_likefilter", -default=>$d,-size=>20, -maxlength=>100);
				} elsif( $ctype eq "DL" ) {
					my($d)=$PARGS{filter_showdeleted};
					$d = 'YES' if $d=~/^\s*$/;
					my(@yn)=("YES","NO");
					print "Show Deleted/Locked/Expired: ",br.popup_menu( -name=>'filter_showdeleted', -values=> \@yn, -default=>$d ),$ehfont,"</TD>";
				} elsif( $ctype eq "CF" ) {
					my($d)=$PARGS{filter_credentials};
					$d = '' if $d=~/^\s*$/;
					print 'Credential Like Filter:',br,textfield(-name=>"filter_credentials", -default=>$d,-size=>20, -maxlength=>100);
				} elsif( $ctype eq "LU" ) {
					@all_ldap_usr= MlpGetPrincipal(-type=>'ldap user',-displayable=>'Y') if $#all_ldap_usr == -1;
					my($dflt) = $dflt_user;
					unshift @all_ldap_usr, $dflt_user;
					print "Ldap User: ".br.popup_menu( -name=>'filter_ldap_user', -values=>\@all_ldap_usr, -default=>$dflt_user);
				} elsif( $ctype eq "SU" ) {
					my(@d);
					my($q)="select distinct principal_name  from INV_principal_map order by principal_name";
					foreach ( _querywithresults( $q,1) ) {
						my(@x)=dbi_decode_row($_);
						push @d,$x[0];
					}
					my($dflt) = $dflt_user;
					unshift @d, $dflt_user;
					print "SAS70 Ldap User: ".br.popup_menu( -name=>'filter_ldap_user', -values=>\@d, -default=>$dflt_user);
				} elsif( $ctype eq "LG" ) {
					@all_ldap_grp= MlpGetPrincipal(-type=>'ldap group',-displayable=>'Y') if $#all_ldap_grp == -1;
					my($dflt) = $dflt_user;
					unshift @all_ldap_grp, $dflt_user;
					print "Ldap Group: ".br.popup_menu( -name=>'filter_ldap_group', -values=>\@all_ldap_grp, -default=>$dflt_user);
				} elsif( $ctype eq "SG" ) {
					# @all_ldap_grp= MlpGetPrincipal(-type=>'ldap group',-displayable=>'Y') if $#all_ldap_grp == -1;
					my(@d);
					my($q)="select distinct department  from INV_cert_department order by department";
					foreach ( _querywithresults( $q,1) ) {
						my(@x)=dbi_decode_row($_);
						push @d,$x[0];
					}
					my($dflt) = $dflt_user;
					unshift @d, $dflt_user;
					print "Sas70 Ldap Group: ".br.popup_menu( -name=>'filter_ldap_group', -values=>\@d, -default=>$dflt_user);

				} elsif( $ctype eq "AP" ) {

					#my(@ap)= ('Approved Accounts','Removed Accounts','Unknown Status',"All Accounts");
					my(@ap)= qw(ALL APPROVED REMOVE UNUSEABLE UNAPPROVED UNMAPPED SYSADMIN SERVICEACCOUNT);
					print "Approval Status:<br>".popup_menu( -name=>'filter_approve', -values=>\@ap,
						-default=>"ALL");
				} elsif( $ctype eq "PL" ) {

					# get the policies
					my($q)= "select distinct policy_name from INV_policy_defn ";
					$q   .= " where policy_name like '".$PARGS{AUDTYP}."_%'";
					$q   .= " order by policy_name";
					my(@pol_list) = ("[ Select Policy ]");
					foreach ( _querywithresults( $q,1) ) {
						my(@x)=dbi_decode_row($_);
						push @pol_list,$x[0];
					}
					my($dflt)= $PARGS{filter_audit_policy} || $pol_list[0];
					print "Audit Policy: ".br.popup_menu( -name=>'filter_audit_policy', -values=>\@pol_list,	-default=>$dflt);

				} elsif( $ctype eq "At" ) {
					my(@ap)= ('All Accounts','Service Accounts','Unmapped Accounts','Mapped Accounts',"Unuseable Accounts","Error Accounts","LDAP User","LDAP Group");
					print "Account Status: ".popup_menu( -name=>'filter_container', -values=>\@ap,
						-default=>"All Accounts");
				} else {
					print "&nbsp;";
				}
				print $ehfont,"</center></TD>";
			}
			if( $PARGS{NAVLABEL} ) {
				die "Must have NAVLISTBOX if NAVLABEL" unless $PARGS{NAVLISTBOX};
				die "Must have NAVVARNAME if NAVLABEL" unless $PARGS{NAVVARNAME};
				my(@datary);
				foreach ( _querywithresults($PARGS{NAVLISTBOX},1) ) {
					push @datary, dbi_decode_row($_);
				}

				$CELLCOUNT++;
				print "</TR><TR>" if $CELLCOUNT==5;
				print "<TD WIDTH=25%><center>",$hfont;
				print $PARGS{NAVLABEL}.": ".br.popup_menu( -name=>$PARGS{NAVVARNAME}, -values=>\@datary, -default=>$datary[0]);
				print $ehfont,"</center></TD>";
			}
			while ( $CELLCOUNT++ % 4 != 0 ) { print "<TD>&nbsp;</TD>"; }
		}
		print "</TABLE></TD></TR></TABLE>",br;

	} elsif( $PARGS{"NAVBAR"} ) {
		debugmsg("Line ".__LINE__.": NONAVIGATION DEFINED...");
	} else {
		debugmsg("Line ".__LINE__.": NO NAVBAR WAS DEFINED...");
	}
	print drawbanner( submit(-name=>"Save Changes"),'','Saves Changed Report Data') if $SAVECHANGESFOUND;
}

sub code_section {
	my($msg,$line)=@_;
	return if $development_mode!=2;

	# debugmsg( "Line=",$line,": Time=",time-$starttime,' ',$msg);
	$msg = "Line=".$line.": Time=".(time-$starttime).' '.$msg;
	if( $i_am_able_to_understand_sql eq "Y" ) {
		print "<FONT COLOR=BLUE>$msg</FONT><br>\n";
	} elsif( $i_am_able_to_understand_sql eq "?" ) {
		$dev_diag_str.="<FONT COLOR=BLUE>$msg</FONT><br>\n";
	}
}

sub my_Header {

	return if defined param("NOHEADER") or $PARGS{NOHEADER};

#		my($JSCRIPT)=<<END;
#function submit_form()
#{
#  var x;
#  var y;
#//y = document;
#  x = location;
#//alert(x+' '+y);
#alert(x);
#    location = x;
#}
#END
#my($JSCRIPT);
	return if $header_printed;
	code_section("PAGE HEADER PRINTED",__LINE__);

	if( defined $PARGS{"REPORTNAME"} ) {
		print header,start_html(-title=>$COMPANYNAME.' - '.$PARGS{"REPORTNAME"},
#			-script=>$JSCRIPT,
			-style=>'styles/main.css', -style=>'styles/Customer.css'
			);
	} else {
		print header, start_html(-title=>$COMPANYNAME,
#			-script=>$JSCRIPT,
			-style=>'styles/main.css', -style=>'styles/Customer.css');
	}
	$header_printed=1;
	foreach ( @saved_messages ) { print $_; }
	print start_multipart_form();  # "DBGDBG: start_multipart_form()".br;
	print hidden( -name=>"svsv_admin", -default=>$admin ),"\n"	if defined $admin;

#	foreach ( param() ) { print "DBGDBG $_ => ".param($_).br; }
}


sub my_Page_Footer {
	# print "DBGDBG my_Page_Footer()\n",br;
	if( $PARGS{"NONAVIGATION"} ) {
		Delete("NONAVIGATION");
		print drawbanner(	"&nbsp;",'',$tm );
	} else {
		print drawbanner(	"Authenticated as ".$LOGON_USER, '', $tm );
	}
	my_end_form();
	print end_html;
	exit(0);
}

sub drawbanner
{
	my($left,$center,@right)=@_;
	my($outstr);
	$outstr=   "<TABLE WIDTH=100% BGCOLOR=#f0e9d9 BORDER=1><TR class='menuheader'><TD><TABLE WIDTH=100%>
	  <TR class='menuheader'><TD ALIGN=LEFT><FONT face=sans-serif size=+1 color=black>$left</FONT></TD>\n";
	$outstr.=   "<TD ALIGN=CENTER><FONT face=sans-serif size=+1 color=black>$center</FONT></TD>\n" if $center ne "";
	foreach ( @right ) {
		$outstr.=   "<TD ALIGN=RIGHT><FONT face=sans-serif size=+1 color=black>$_</FONT></TD>\n" if $_ ne "";
	}
	$outstr.=   " </TR></TABLE></TD></TR></TABLE>\n";
	return $outstr;
}

sub errorbox
{
	my($msg)=join('',@_);
	return "<TABLE BORDER=1 WIDTH=100% BGCOLOR=WHITE><TR ><TD ALIGN=LEFT><FONT face=sans-serif size=+1 color=red>@_</FONT></TD>\n</TR></TABLE>\n";
}

sub MyUcfirst {
 	my($str)=@_;
 	my($out)='';
 	foreach my $word ( split(/[\_\s+]/,lc($str)) ) {
 		$out .= ucfirst($word)." ";
	}
	$out=~s/ $//;
	return $out;
}

sub errorout
{
	my($msg)=join('',@_);
	print errorbox($msg),p if $msg;
	# "<FONT COLOR=RED>***An Error Was Encountered***</FONT>"
	if( $PARGS{"TITLE"} ) {
		print drawbanner( b($PARGS{"TITLE"}),'',$tm, );
	} else {
		print drawbanner( "Alarm Reporting Screen",'',$tm, );
	}
	my_end_form();
	print end_html;
	exit(0);
}

sub diagnose
{
	my($msg)=join("",@_);
	return unless defined $PARGS{"DIAGNOSE"};
	if( $header_printed ) {
		print "<FONT COLOR=purple SIZE=-2>$msg</FONT><br>\n";
	} else {
		push @saved_messages, "<FONT COLOR=purple SIZE=-2>$msg</FONT><br>\n";
	}
}

sub statusmsg
{
	my($msg)=join("",@_);
	if( $header_printed ) {
		print "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	} else {
		push @saved_messages, "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	}
}
sub debugmsg
{
	return unless $debug;
	my($msg)=join("",@_);
	if( $header_printed ) {
		print "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	} else {
		push @saved_messages, "<FONT COLOR=red SIZE=-2>$msg</FONT><br>\n";
	}
}


#sub debug_show_args
#{
#	my($flag)=@_;
#	if( $flag ) {
#		my_Header();
#		code_section("print out the parameters if you are in debug mode",__LINE__);
#		print "<FONT COLOR=red SIZE=-2>Line ".__LINE__.": ";
#		print "<TABLE BORDER=1><TR class='menuheader'><TH>PARAMETER_NAME</TH><TH>PARAMETER_VALUE</TH></TR>\n";
#		foreach ( keys %PARGS ) {
#			print "<TR class='menuheader'><TD>parameter $_</TD><TD>value=<b>".$PARGS{$_}."</b></TD></TR>\n"
#				unless /^dat_\d+_/ or /^sv_\d+_/ or /^HEADERTEXT/;
#		}
#		print "</TABLE></FONT>\n",br;
#	}
#}

#sub get_display_name {
#	my($q)="select display_name from INV_credential where credential_name='".$LOGON_USER."' and credential_type='ldap user'";
#	foreach ( _querywithresults($q,1) ) {
#		my(@x) = dbi_decode_row($_);
#		return $x[0];
#	}
#	return $LOGON_USER;
#}

sub printable_words {
	my($rc)=strip_words(@_);
	$rc =~ s/^INV_//g;
	$rc =~ s/^NOADD_//g;
	$rc =~ s/_YN_LU$//;
	$rc =~ s/_LU$//g;
	$rc =~ s/\_/ /g;
	return $rc;
}

sub strip_words {
	my($str)=@_;
	$str =~ s/dat_\d+_//;
	$str =~ s/add_\d+_//;
	$str =~ s/NOADD_\d+_//;
	$str =~ s/add_//;
	$str =~ s/^sv_\d+_//;
	$str =~ s/^Chkbox_//;
	$str =~ s/^Txtbox\d+_//;
	$str =~ s/^Popup_//;
	$str =~ s/\s+$//;
	return $str;
}
sub diag_show	{	# generic
	my($line,$exclude,$title,$title2,%hash)=@_;
	return unless $development_mode;
	my_Header();
	my($argstr)='<TABLE WIDTH=100% BORDER=1><TR><TD COLSPAN=3 BGCOLOR=PEACH>LINE='.$line.
		" ".$title.$title2;
	$argstr.="</TD></TR><TR>";
	my($argstrlong)='';
	my($cnt)=0;
	foreach ( sort keys %hash ) {
		next if /^HEADER/ or /^TITLE/;
		if( $exclude ) {
			next if /^DEBUG/ or /sv_/ or /dat_/;
		}
		# print __LINE__," $_ => $hash{$_}",br if /^sv/;

		if( length($hash{$_}) > 30 ) {
			$argstrlong.= "<TR><TD COLSPAN=3>$_ => <FONT COLOR=RED>$hash{$_}</FONT> </TD></TR>";
		} else {
			$argstr.= "<TD>Arg $_ => <FONT COLOR=RED>$hash{$_} </FONT></TD>";
			$cnt++;
			$argstr .= "</TR><TR>" if $cnt%3 == 0;
		}
	}
	$argstr .= "</TR>".$argstrlong."</TABLE>";
	print "<FONT SIZE=-1 COLOR=BLUE>",$argstr,"</FONT>";
}
sub diag_showparam {
	return unless $development_mode;
	my($line,$exclude)=@_;
	my($t2)="Also Skips dat_, sv_ " if $exclude;
	return diag_show($line,$exclude,
		"SQL PRINT FOR DASHBOARD ADMINISTRATOR $LOGON_USER<br>Skips HEADER, TITLE, DEBUG",
		$t2,%PARGS);

#	my_Header();
#	my($argstr)='<TABLE WIDTH=100% BORDER=1><TR><TD COLSPAN=3 BGCOLOR=PEACH>LINE='.$line.
#		" SQL PRINT FOR DASHBOARD ADMINISTRATOR $LOGON_USER<br>Skips HEADER, TITLE, DEBUG ";
#	$argstr.="Also Skips dat_, sv_ " if $exclude;
#	$argstr.="</TD></TR><TR>";
#	my($argstrlong)='';
#	my($cnt)=0;
#	foreach ( sort keys %PARGS ) {
#		next if /^HEADER/ or /^TITLE/;
#		if( $exclude ) {
#			next if /^DEBUG/ or /sv_/ or /dat_/;
#		}
#		# print __LINE__," $_ => $PARGS{$_}",br if /^sv/;
#
#		if( length($PARGS{$_}) > 30 ) {
#			$argstrlong.= "<TR><TD COLSPAN=3>$_ => <FONT COLOR=RED>$PARGS{$_}</FONT> </TD></TR>";
#		} else {
#			$argstr.= "<TD>Arg $_ => <FONT COLOR=RED>$PARGS{$_} </FONT></TD>";
#			$cnt++;
#			$argstr .= "</TR><TR>" if $cnt%3 == 0;
#		}
#	}
#	$argstr .= "</TR>".$argstrlong."</TABLE>";
#	print "<FONT SIZE=-1 COLOR=BLUE>",$argstr,"</FONT>";
}

sub my_end_form {

	my($controlstring)='';
	foreach (keys %FIELDS_AND_TYPES ) {
		$controlstring.=$_."~".$FIELDS_AND_TYPES{$_}."\|"
			unless $_ eq "COLOR" or $_ eq "HYPERLINK"  or $_ eq "HYPERLINKBLANK";
	}
	$controlstring =~ s/\|$//;
	print hidden( -name=>"sv_admin", -default=>$admin ),"\n"							if defined $admin;
	print hidden( -name=>"REPORTNAME", -default=>$PARGS{'REPORTNAME'} ),"\n"	if defined $PARGS{'REPORTNAME'};
	print hidden(-name=>'controlstring',-default=>$controlstring)."\n";
	foreach ( keys %PARGS ) {
		print hidden( -name=>$_, -default=>$PARGS{$_} ),"\n"	if /^filter_/ and $PARGS{$_} and $PARGS{$_} !~ /^\[/;
	}

	# print "DBGDBG: end_form() Report=",$PARGS{'REPORTNAME'}," Admin=$admin",br;
	print end_form(),p;
}

sub run_action {
	my($action_id, $actionval) =@_;

	my(%varkeys);
	# if the key contains <BR> then parse data for <BR>
	foreach ( sort keys %PARGS ) {
		next unless /^sv_${action_id}/;
		my($I)=$_;
		$I=~s/sv_${action_id}_//;
		if( $I =~ /\<BR\>/ ) {
			my(@k)=split("\<BR\>",$I);
			my(@v)=split("\<BR\>",$PARGS{$_});
			foreach ( @k ) {
				my($val)=shift @v;
				$varkeys{ $_ } = $val;
			}
		} else {
			$varkeys{$I} = $PARGS{$_};
		}
	}

	diag_show(__LINE__,undef,"run_action() Submit Pressed id=$action_id val=$actionval",undef,%varkeys);

	print drawbanner( b("Processing Submit"),"", "" );
#	die "id=$action_id val=$actionval";

#	my($actionval)=param("actionval_".$action_id);
	my($actiontext)=$hb_operations{$actionval};
	# print "AT=$actiontext",br;


#	my(%varkeys);
#	foreach (param()) {
#		next unless /^sv_${action_id}_/;
#		my($k)=$_;
#		s/sv_${action_id}_//;
#		debugmsg( "KEY=$_ VAL=".param($k) );
#		$varkeys{$_} = param($k);
#	}

	if( defined param('sv_admin') ) {
		debugmsg( "Admin=".param("sv_admin") );
		$varkeys{Admin}=param("sv_admin");
		$admin=param("sv_admin");
	}

	if( $actionval eq "MsgDelete") {
		print join(br,MlpManageData(
				OkId=>$action_id,
				Operation=>"del",
				Admin=>"",
				Server		=> $varkeys{System},
				System	=> $varkeys{System},
				Program		=> $varkeys{Program},
				Subsystem	=> $varkeys{Subsystem},
				State		=> $varkeys{State}
				)),br;
	} elsif( $actionval eq "DeleteMon") {
		print join(br,MlpManageData(
				OkId=>$action_id,
				Operation=>"del",
				Admin=>"Delete By Monitor",
				Server		=> $varkeys{System},
				System	=> $varkeys{System},
				Monitor		=> $varkeys{Program},
				Program		=> $varkeys{Program},
				Subsystem	=> $varkeys{Subsystem},
				State		=> $varkeys{State},
				)),br;
	} elsif( $actionval =~ /MsgOkMonitor/ or $actionval =~ /MsgOk/) {
		my($hours)=24;

		$hours=3 		if $actionval=~/3hr$/;
		$hours=72 		if $actionval=~/3dy$/;
		$hours=168		if $actionval=~/7dy$/;
		$hours=876000 	if $actionval=~/Perm$/;

		if( $actionval =~ /MsgOkMonitor/ ) {
			print join(br,MlpManageData(
					OkId=>$action_id,
					Operation=>"Ok",
					Admin=>"",
					Hours		=>$hours,
					System	=> '',					# triggers the monitor only approval.
					Program	=> $varkeys{Program},
					)),br;
		} else {
			print join(br,MlpManageData(
					OkId=>$action_id,
					Operation=>"Ok",
					Admin=>"",
					Hours		=>$hours,
					Server		=> $varkeys{System},
					System	=> $varkeys{System},
					Program		=> $varkeys{Program},
					Subsystem	=> $varkeys{Subsystem},
					State		=> $varkeys{State},
					)),br;
		}
	} elsif( $actionval eq "Blackout3") {
		print join(br,MlpManageData(	Operation=>"setblackout",	Hours	=> 3,	Value	=> 1,	Server=>$varkeys{System},	Admin=>"Production" )),br;
	} elsif( $actionval eq "Blackout6") {
		print join(br,MlpManageData(	Operation=>"setblackout",	Hours	=> 6,	Value	=> 1,	Server=>$varkeys{System},	Admin=>"Production" )),br;
	} elsif( $actionval eq "Blackout24") {
		print join(br,MlpManageData(Operation=>"setblackout",	Days	=> 1,	Value	=> 1,	Server=>$varkeys{System},	Admin=>"Production" )),br;
	} elsif( $actionval eq "Blackout72") {
		print join(br,MlpManageData(	Operation=>"setblackout",Days	=> 3,	Value	=> 1,	Server=>$varkeys{System},	Admin=>"Production" )),br;
	} elsif( $actionval eq "Blackout1Wk") {
		print join(br,MlpManageData(	Operation=>"setblackout",	Days	=> 7,	Value	=> 1,	Server=>$varkeys{System}, Admin=>"Production" )),br;
	} elsif( $actionval eq "IgnoreSvr") {	# ignore server
		print join(br,MlpManageData(	Operation=>"setignore",	Value	=> 1,	Server=>$varkeys{System},	Admin=>"Production" )),br;
	} elsif( $actionval eq "DeleteAll") {
		print join(br,MlpManageData( OkId=>$action_id,	Operation=>"delserver",Program		=> $varkeys{Program},	System=>$varkeys{System})),br;
	} elsif( $actionval eq "NotProd") {	# server is not production
		print join(br,MlpManageData(Operation=>"setproduction",	Value	=> 0,	Server=>$varkeys{System},	Admin=>"Production" )),br;
	}
}

__END__

=head1 NAME

reportwriter.pl - web browser based generic report framework

=head2 SET UP

The scripts in ADMIN_SCRIPTS/cgi-bin must be copied to a script executable
web server directory as per the CGI installation instructions.
These installation instructions can be found in the post-install tasks section of the installation guide.

=head2 PARAMETERS

These reports use parameters to determine what to do.  These parameters can come from the ReportArgs
table in the database.  This table can be loaded from MimiReportData.dat using the load*.pl script

The following cgi parameters are used by reportwriter.pl

REPORTNAME - used to extract predefined values from the database
           - often the key to everything

NAVBAR			- navbar see later

TITLE				- title of the report

DEBUG		  		- diagnostic mode

HEADERTEXT		- html header for report
HEADERTEXT[1-9]- additional html header for report

NONAVIGATION	- printed report - no buttons or navigation items

ADMIN			   - always saved in sv_admin - default to REPORTNAME if not defined
					- this is the name of the main report for the api

=head2 Header Rows

Header rows repeat based on argument HDR_ROW_REPEATS

Large forms can be avoided using HDR_FORM_REPEATS which repeats the form every 10 rows.  You can also
use HDR_FORM_FIELD1 which repeats every time field1 changes.

These arguments are mutually exclusive

=head2 Form Text

The forms are generated by the following variables

	EXTERNAL_CGI_EVALFILE	= evals a block of code stored in this file
	EXTERNAL_CGI_SCRIPT			= runs this command with command substitution
	MlpRunScreen()			= runs this command if the above two variables are undefined

=head2 BUILTIN ADDS and DELETES

Certain params() will result in predefined actions being performed

The main two are arguments named add_(\d+) and del_(\d+)

These represent ADD or DELETE buttons pressed on the form. The digits represent the rowid
to be processed.

if p(add_(\d+)_XXX) or p(del_(\d+)_XXX) then the button add (or something similar) has been pushed -
the row values were sent with hidden fields
so the syntax is MlpManageData( Admin->p(sv_admin), Operation=add|del,
and XXX=p($_).  del_(\d_+)_XXX similarly works.
That function returns html which is printed in <PRE> block.

The above is automatically created using the built in reports

=head2 ACTION ON IMPLEMENTATION

 if Save Changes {
	dat_ fields only come from Chkbox_*
	if there is a dat_(\d+)_XXX matching a sv_(\d+)_XXX that has changed...
	we must do an insert... using MlpManageData (operation=XXX, Value=>newvalue, With all other sv_items on the row passed.
	This must be individually coded
	see setignore, setproducton, and setblackout
 } else {
	my($rc)=MlpRunScreen( -screen_name=>$admin, -html=>1, -debug=>$debug,
			-severity		=>	param('filter_severity'),
			-production =>	param('filter_prodchkbox'),
			-container	=>	param('filter_container'),
			-system			=>	param('filter_system'),
			-program		=>	param('filter_program_name')
		);
 }

=head2 NAVIGATION BAR

The navigation bar is set based on varibles NAVFILE (a file name read verbatim) or NAVBAR.
If the argument NONAVIGATION is provided, no NAVBAR will be shown.
NAVBAR is composed of a sequence of 2 character KEYCODES AS FOLLOWS

	RR - Run Report Button
	RE	- Reset Button
	SV - SEVERITY
	OE - OK/ERROR
	PO - PRODUCTION ONLY CHECKBOX

	TI - TIME FILTER
	BS - BUSINESS SYSTEM
	SY - SYSTEM
	SD - DATABASES
	Sa - DATABASES THAT REQUIRE APPROVAL
	SH - HOSTNAMES
	ST - SYSTYPE
	AS - APPROVER vs SCRUBBER

	PR - PROGRAM
	CE - CERTIFY NOW BUTTON (special)
	__ - EMPTY
	AL - SHOW ALL GROUPS
	DY - NUMBER OF DAYS - defaults to 30
	LF - LIKE FILTER
	DL - DELETED/LOCKED/EXPIRED
	CF - CREDENTIAL LIKE FILTER
	LU - LDAP USERS
	LG - LDAP GROUPS
	AP - APPROVED vs UNAPPROVED
	At	- Type Of Unmapped Account
	DE - LDAP Department
	MA - Match Status

	HO - Host types (unix, win32)
	DB - Db Types (oracle, sybase, sqlsvr, mysql)

	SA - SAVE CHANGES BUTTON

=head2 The Operation Keyword

The first row returned by the library functions is the header.
The last field of the header row is the operation which controls everyting!
Operations can be
 	Both
	Update
	Delete
	Add
	OkDelete
	Ok

The impact of the above is as follows

Both/Update/Delete/Add all change the word to "Operation" on the header row

YN fields, identified by ^use or YN_LU or ^Is_ then cause the y or n from the field to be expanded to Yes/no

=head2 ROW HEADER values Conversion

if the row header ends in _LU$ then its a special case!
The last field is defined as the Operation and must be Update, Both, Add, Ok, OkDelete, DELETE, ChkBox_
there is a function defining lookups

=head2 NOTES

NOADD_ will not add stuff
FIRSTFIELD=COLOR IN HEADER then its the row color override
FIRSTFIELD=HYPERLINK IN HEADER then its a hyperlink... must come AFTER COLOR


insert INV_credential_notes ( credential_name, credential_type, is_ldap_group, ldap_group )
select group_name, system_type, 'Y', credential_name
from INV_group_member where group_type='GLOBALSHARED'
and credential_type='ldap group' and group_name!='hello'

insert INV_credential_notes ( credential_name, credential_type, is_ldap_user, ldap_user )
select group_name, system_type, 'Y', credential_name
from INV_group_member where group_type='GLOBALSHARED'
and credential_type='ldap user' and group_name!='hello'

=cut

