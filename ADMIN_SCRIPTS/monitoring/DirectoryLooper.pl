#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use 		strict;
require 	5.002;
use     	strict;
use		Time::Local;
use     	File::Basename;
use 		Getopt::Long;
use		CommonFunc;
use		Do_Time;
use 		MlpAlarm;

$|=1;
my($VERSION)=1.0;
my($PROGNAME)="DirectoryLoop";		# doubles as the alarm monitor_program label

use vars qw( $DEBUG $NOLOG $CONFIGFILE $STARTUP $DATE $NOTIMECOMPARE $BATCHID );
my($STARTUP_TIME);

# holidays... startup wont write on these days
my(@exclude_startup_dates)=("20050221","20050325", "20050530", "20050704", "20050905", "20051010", "20051111", "20051124", "20051126");

sub usage
{
	return "Usage: $PROGNAME.pl --NOTIMECOMPARE --DEBUG --NOLOG --CONFIGFILE=file --STARTUP --DATE=RUNDATE (yyyymmdd hhmiss)\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
		"DATE=s" 		=> \$DATE,
		"NOLOG" 			=> \$NOLOG,
		"STARTUP" 		=> \$STARTUP,
		"PROGNAME=s" 		=> \$PROGNAME,
		"NOTIMECOMPARE" => \$NOTIMECOMPARE,
		"CONFIGFILE=s"  => \$CONFIGFILE,
		"BATCHID=s"  	=> \$BATCHID,
		"DEBUG"			=> \$DEBUG );

$BATCHID="Exchange Feeds" unless $BATCHID;

die "Must pass --CONFIGFILE ".usage() unless defined $CONFIGFILE;
$|=1;

my($cwd)=cd_home();
print $PROGNAME," version $VERSION\n";

#
# you can override time if you are in test mode
#
my($t);
if( defined $DATE ) {
	$DATE=~/(\d\d\d\d)(\d\d)(\d\d) (\d\d):*(\d\d)/;
	my($y,$mm,$d,$h,$mi)=($1,$2,$3,$4,$5);
	print "y=$y m=$mm d=$d h=$h mi=$mi\n";
	$mm--;
	$t=timelocal(0,$mi,$h,$d,$mm,$y);
	print "DEVELOPMENT MODE\n";
	print "PRETENDING STARTUP TIME = ",scalar(localtime($t)),"\n";
	$STARTUP_TIME=$t;
}

#
# Read the Data Section
#
my(%DATA_FILESPEC,%DATA_CRITTIME,%DATA_FILEFOUND,%DATA_DIRECTORIES,@LABELS);
foreach (<DATA>) {
	chomp;
	next if /^\s*$/ or /^#/;
	#print "LINE is $_\n";

	my($SOURCE,$LABEL,$x,$DATA_CRITTIME)=split;

	$DATA_FILESPEC{$LABEL}=$x;
	$DATA_FILEFOUND{$LABEL}="NO";
	$DATA_CRITTIME{$LABEL}=$DATA_CRITTIME;
	push @LABELS,$LABEL;

	$x=~s/[\{\}]//g;
	$x=dirname($x);
	$DATA_DIRECTORIES{$x}=1;
}

if( defined $STARTUP ) {
	print "STARTUP called in debug mode\nSnapshoting Files\n" if defined $DEBUG;
	my($todayb)	=do_time(-fmt=>"yyyymmdd");
	foreach( @exclude_startup_dates) {
		if($_ eq $todayb) {
			print "exiting - its a holiday\n";
			exit(0);
		}
	}

	my($tm);
	my($st);
	if( defined $DATE ) {
		$st=$t;
		$tm=scalar(localtime($t));
	} else {
		$st=time;
		$tm=scalar(localtime(time));
	}
	print "Setting Startup Date in $CONFIGFILE to ".$tm."\n";
	# MlpBatchStart( -monitor_program=>$PROGNAME, -system=>$BATCHID, -batchjob => 1, -message=> "$BATCHID Start of Day ($tm)", -reset_status=>1) unless defined $NOLOG;
	MlpBatchJobStart(-BATCH_ID=>$BATCHID, -AGENT_TYPE=>'Gem Monitor', -SUBKEY=>$PROGNAME) unless defined $NOLOG;

	my($stepnum)=1;
	foreach (@LABELS) {
		MlpBatchStep( -stepname=>$_, -stepnum=>$stepnum++, -message=>"Waiting on File $_")
					unless defined $NOLOG;
	}

	open(CF,">$CONFIGFILE") or die "Cant Write $CONFIGFILE $!\n";
	print CF "Startup_Time ",$st,"\n";
	print "Startup_Time ",$st,"\n" if defined $DEBUG;
	foreach my $dir (keys %DATA_DIRECTORIES) {
		die "Directory $dir does not exist\n" 		unless -e $dir;
		die "Directory $dir is not a directory\n" 	unless -d $dir;
		die "Directory $dir is not readable\n" 		unless -r $dir;
		opendir(DIR,$dir) or die "Cant Read Directory $dir\n";
		my(@dirlist)=grep(!/^\./,readdir(DIR));
		closedir(DIR);
		foreach (@dirlist ) {			#	stat the file
			my($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
     		  		$atime,$mtime,$ctime,$blksize,$blocks) = stat($dir."/".$_);
			print CF $dir," ",$_," ",$mtime," ",$size,"\n";
			print $dir," ",$_," ",$mtime," ",$size,"\n" if defined $DEBUG;
		}
	}
	close(CF);

	print "Startup Completed Successfully\n";
	exit(0);
}

# MlpBatchStart( -monitor_program=>$PROGNAME, -system=>$BATCHID, -batchjob => 1, -message=> "$BATCHID Check") unless defined $NOLOG;
MlpBatchJobStart(-BATCH_ID=>$BATCHID, -AGENT_TYPE=>'Gem Monitor', -SUBKEY=>$PROGNAME) unless defined $NOLOG;

#
# ok not startup mode... read that configfile
#
my(%CFGFILE_SIZE,%CFGFILE_TIME);
open(CF,$CONFIGFILE)
		or die "Cant Read $CONFIGFILE - Have you initialized using --STARTUP?\n";
foreach (<CF>) {
		chomp;
		my($l,$f,$d,$s)=split;
		if( $l eq "Startup_Time" ) {
			$STARTUP_TIME=$f;
			next;
		}
		$CFGFILE_TIME{$l."/".$f} = $d;
		$CFGFILE_SIZE{$l."/".$f} = $s;
}
close(CF);

#
# OK now read that directory list
#
my(%NEWFILE_SIZE,%NEWFILE_TIME);
my(%ALLFILE_SIZE,%ALLFILE_TIME);
my($filecount)=0;
foreach my $dir (keys %DATA_DIRECTORIES) {
	die "Directory $dir does not exist\n" 		unless -e $dir;
	die "Directory $dir is not a directory\n" 	unless -d $dir;
	die "Directory $dir is not readable\n" 		unless -r $dir;
	opendir(DIR,$dir) or die "Cant Read Directory $dir\n";
	my(@dirlist)=grep(!/^\./,readdir(DIR));
	closedir(DIR);

	foreach (@dirlist ) {
		#	stat the file
		my($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
    		  $atime,$mtime,$ctime,$blksize,$blocks) = stat($dir."/".$_);

		$ALLFILE_TIME{$dir."/".$_} = $mtime;
		$ALLFILE_SIZE{$dir."/".$_} = $size;

		if( ! defined $CFGFILE_TIME{$dir."/".$_} ) {
			$filecount++;
			print "Found new file $dir/$_\n" if $DEBUG;
			$NEWFILE_TIME{$dir."/".$_} = $mtime;
			$NEWFILE_SIZE{$dir."/".$_} = $size;
		} elsif( $CFGFILE_SIZE{$dir."/".$_} != $size ) {
			$filecount++;
			print "Found different sized file $dir/$_\n" if $DEBUG;
			$NEWFILE_TIME{$dir."/".$_} = $mtime;
			$NEWFILE_SIZE{$dir."/".$_} = $size;
		} elsif( ! defined $NOTIMECOMPARE and $CFGFILE_TIME{$dir."/".$_} != $mtime ) {
			$filecount++;
			print "Found different timestamped file $dir/$_\n" if $DEBUG;
			$NEWFILE_TIME{$dir."/".$_} = $mtime;
			$NEWFILE_SIZE{$dir."/".$_} = $size;
		}
	}
}
print "$filecount changed files have been found\n\n";

# for each label
my($stepnum)=1;
my($numbad,$numgood)=(0,0);
my($missing)="";
foreach my $label ( @LABELS ) {
	my($spec) = $DATA_FILESPEC{$label};

	if( $spec =~ /\{/ ) {		#}
		$spec =~ s/\{.+\}//g;
		my($latestfile,$latesttime);
		my($state);
		foreach my $key ( keys %NEWFILE_TIME ) {
			my($x)=$key;
			$x=~s/\d\d\d\d+//;
			next unless $x eq $spec;
			if( ! defined $latesttime or $latesttime<$NEWFILE_TIME{$key} ) {
				$latestfile=$key;
				$latesttime=$NEWFILE_TIME{$key};
				$state="OK";
			}
		}
		foreach my $key ( keys %ALLFILE_TIME ) {
			my($x)=$key;
			$x=~s/\d\d\d\d+//;
			next unless $x eq $spec;
			if( ! defined $latesttime or $latesttime<$ALLFILE_TIME{$key} ) {
				$latestfile=$key;
				$latesttime=$ALLFILE_TIME{$key};
				$state="XX";
			}
		}
		if( $state eq "OK" ) {
			$numgood++;
			MlpBatchDone( -message=>"File $latestfile is available\n",	-stepnum=>$stepnum++, -subsystem=>$label)
				unless defined $NOLOG;
		} else {
			$numbad++;
			$missing.="$label ";
			MlpBatchStep( -stepname=>$label,
					-stepnum=>$stepnum++,
					-message=>"Waiting on File $_ (latest = $latestfile)")
				unless defined $NOLOG;
		}
		printf( "%s %20s %s", $state, $label," special $spec\n");
		print "                         latest file: ",basename($latestfile),"\n                         latest time: ".localtime($latesttime)."\n";
	} else {
		if( -r $spec and defined $NEWFILE_TIME{$spec}) {
			$numgood++;
			printf( "OK %20s %s", $label," file readable\n");
			MlpBatchDone( -message=>"File $spec available\n",
						-stepnum=>$stepnum++, -subsystem=>$label)
				unless defined $NOLOG;
		} else {
			$numbad++;
			if( -r $spec ) {
				printf( "XX %20s %s", $label," file not found\n");
			} else {
				printf( "XX %20s %s", $label," file not changed/nas\n");
			}
			MlpBatchStep( -stepname=>$label,
						-stepnum=>$stepnum++,
						-message=>"Waiting on File $spec")
				unless defined $NOLOG;
		}
	}
}

print "Found $numgood files - missing $numbad files\n";

my($state);
if( $numbad == 0 ) {
	#MlpBatchDone( -message=>"$numgood / ".($numgood+$numbad)." Files have arrived.  Process Completed\n")
	#	unless defined $NOLOG;
	MlpBatchJobEnd(-EXIT_NOTES => "$numgood / ".($numgood+$numbad)." Files have arrived.  Process Completed\n")
		unless defined $NOLOG;

	$state="OK";
} else {
	$missing="Waiting on ".$missing;
	#MlpBatchRunning( -message=>"$numgood / ".($numgood+$numbad)." Files have arrived.  \n")
	MlpBatchJobEnd(-EXIT_NOTES     	=> "$numgood / ".($numgood+$numbad)." Files have arrived.  \n")
		unless defined $NOLOG;
	#MlpBatchDone( -message=>"$numgood / ".($numgood+$numbad)." Files have arrived.  \n")
	#	unless defined $NOLOG;
	$state="WARNING";
	my(@x)=localtime();
	my($hour)=$x[2];
	$state="EMERGENCY" if $hour>6 and $hour<11;
}

MlpHeartbeat(
	-state=>$state,
  	-monitor_program=>"ExchangeFeeds",
	-system=>"Files",
	-message_text=>"$numgood / ".($numgood+$numbad)." Files have arrived.  $missing\n")
		unless defined $NOLOG;

print "$state $numgood / ".($numgood+$numbad)." Files have arrived.  $missing\n"
		if defined $NOLOG;

exit(0);

__DATA__

# FORMAT FOR THE DATA SECTION IS
# <SOURCE> <LABEL> <LABEL_BY_FNAME> <CRITTIME>

NYSE MASTER.txt			/dat/nasdaq/download/nyse/master{today}.txt			7
NYSE ExDateCorpAct.txt	/dat/nasdaq/download/nyse/ExDateCorpAct{todayb}.txt	7
NYSE DailyCorpAct.txt   /dat/nasdaq/download/nyse/DailyCorpAct{todayb}.txt	7
NASDAQ sec46.txt			/dat/nasdaq/download/CUSIP/sec46_{todayb}.txt	7
NASDAQ CUSIP-di			/dat/nasdaq/download/CUSIP/di{todayb}.txt	7
NASDAQ CUSIP-mf			/dat/nasdaq/download/CUSIP/mf{todayb}.txt	7
NASDAQ CUSIP-nq			/dat/nasdaq/download/CUSIP/nq{todayb}.txt	7
TAQ taqtrade				/dat/taq2/download/taqtrade{todayb}	7
TAQ taqmaster					/dat/taq2/download/taqmaster{todayb}	7
TAQ taqquote					/dat/taq2/download/taqquote{todayb}	7
NASDAQ nasdaq.txt   			/dat/nasdaq/download/nasdaq.txt	7
NASDAQ NasdaqSym   			/dat/nasdaq/download/NasdaqSym	7
NASDAQ nasdaq_arcabook   	/dat/nasdaq/download/nasdaq_arcabook	7
NASDAQ nasdaq_bats   		/dat/nasdaq/download/nasdaq_bats 7
NASDAQ nasdaq_directedge   /dat/nasdaq/download/nasdaq_directedge 7
NYSE nyse_bats   				/dat/nasdaq/download/nyse/nyse_bats
NYSE nyse_directedge   		/dat/nasdaq/download/nyse/nyse_directedge	7
#retired 11/27/06 NASDAQ nasdaq_bookstream   /dat/nasdaq/download/nasdaq_bookstream	7
NASDAQ nasdaq_itch   /dat/nasdaq/download/nasdaq_itch	7
NASDAQ nasdaq_brut   /dat/nasdaq/download/nasdaq_brut	7
NASDAQ nasdiv.sym   /dat/nasdaq/download/nasdiv.sym	7
NASDAQ nas.sym   /dat/nasdaq/download/nas.sym	7
NYSE nyse.txt   /dat/nasdaq/download/nyse/nyse.txt	7
NYSE nyse_arcabook   /dat/nasdaq/download/nyse/nyse_arcabook	7
NYSE nyse_itch   /dat/nasdaq/download/nyse/nyse_itch	7
#retired 11/27/06 NYSE nyse_brut   /dat/nasdaq/download/nyse/nyse_brut	7
#retired 11/27/06 NYSE nyse_bookstream   /dat/nasdaq/download/nyse/nyse_bookstream	7
# NYSE nyse_btrd   /dat/nasdaq/download/nyse/nyse_btrd	7
NYSE nyseLoad.sym   /dat/nasdaq/download/nyse/nyseLoad.sym	7
NYSE nyseDailyLoad.sym   /dat/nasdaq/download/nyse/nyseDailyLoad.sym	7
NYSE nyseExLoad.sym   /dat/nasdaq/download/nyse/nyseExLoad.sym	7
