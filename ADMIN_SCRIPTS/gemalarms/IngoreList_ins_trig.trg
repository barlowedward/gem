if exists ( select * from sysobjects where name='IgnoreList_ins_trig' )
drop trigger IgnoreList_ins_trig
go
create trigger IgnoreList_ins_trig on IgnoreList for insert as
begin

	delete 	Heartbeat
	from 		Heartbeat h, inserted d
	where 	isnull(h.monitor_program ,'')
					= isnull(d.monitor_program, isnull(h.monitor_program,''))
	and   	isnull(h.system ,'')
					= isnull(d.system, isnull(h.system,''))
	and   	isnull(h.subsystem,'')
					= isnull(d.subsystem, isnull(h.subsystem,''))
	and		( 	d.subsystem is not null
				or d.system is not null
				or d.monitor_program is not null )

	delete	Event
	from 		Event h, inserted d
	where 	isnull(h.monitor_program ,'')
					= isnull(d.monitor_program, isnull(h.monitor_program,''))
	and   	isnull(h.system ,'')
					= isnull(d.system, isnull(h.system,''))
	and   	isnull(h.subsystem,'')
					= isnull(d.subsystem, isnull(h.subsystem,''))
	and		( 	d.subsystem is not null
				or d.system is not null
				or d.monitor_program is not null )

end
go
