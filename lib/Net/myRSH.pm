
package Net::myRSH;

# Copyright (c) 2001-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use Sys::Hostname;
use Carp;

my($USE_SSH);

# WE MUST HAVE SSH IN ONE OF THE FOLLOWING LOCATIONS - SEARCHED IN ORDER
my(@ssh_path)=('/usr/openssh/bin','/usr/bin','/usr/local/bin','/usr/kerberos/bin');

my($RUNSSHCMD)="ssh";		# full path if needed
my($RUNRSHCMD)="rsh";		# full path if needed
my($RUNSCPCMD)="scp";		# full path if needed
my($RUNRCPCMD)="rcp";		# full path if needed

$VERSION= 1.0;
my($ConnectTimeoutOk);		# if 0 - dont use ConnectTimeout option

@ISA     = qw(Exporter);
@EXPORT  = qw(do_rsh do_rcp do_rsh_method rsh_msg_handler rsh_lastcommand do_command do_copy);

sub do_command { 	return do_rsh(@_);}
sub do_copy	 	{ 	return do_rcp(@_);}

my($lastcommand)="";
sub rsh_lastcommand {
	return $lastcommand;
}

sub do_rsh_method {
	my($method)=@_;
	if( $method ) {
		if( lc($method) eq "ssh" ) {
			$USE_SSH=1;
		} elsif( lc($method) eq "rsh" ) {
			$USE_SSH=undef;
		} else {
			die "Method must be ssh or rsh (got $method)";
		}
	}
	return "rsh" if ! $USE_SSH;
	return "ssh";
}

sub find_cmd_in_path {
		my($cmd)=@_;		
		
		my(@x)=split(/\;/,$ENV{PATH});		
		foreach (@x) { 
			next unless -x $_."/$cmd";
			return $_."/".$cmd;
		}
				
		foreach (@ssh_path) {
			next unless -x $_."/$cmd";
			return $_."/".$cmd;
		}
				
		
		warn "Warning: $cmd not found in search path.  Please adjust \@ssh_path in lib/myRSH.pm\n".
				"SEARCH PATH=".join(" ",@ssh_path)." ".join(" ",@x)."\n";
		return $cmd;
}
   
sub do_rsh {
	my(%args)=@_;
	confess "do_rsh() Must pass -hostname\n"  unless $args{-hostname};
	confess "do_rsh() Must pass -command\n"   unless $args{-command};
#print "XXXDBG AT LINE ",__LINE__,"\n"; $args{-debug}=1;
	if( $args{-debug} ) {
		my($str)="do_rsh(";
		foreach ( keys %args ) { 
			next if /-password/ or /-debugfunc/ or /-show_resultset/ or /-debug/ or ! defined $args{$_};
			$str .= "$_=$args{$_},";
		}
		$str=~s/,$/)\n/;	
		if( defined $args{-debugfunc} ) {
			&{$args{-debugfunc}}($str);
		} else {
			print $str; 
		}					
	}
	
	do_rsh_method($args{-method}) if $args{-method};

	#print "DBG DBG RSH CALLED - DEBUGFUNC is $args{-debugfunc} ",ref $args{-debugfunc}, "\n";
	#&{$args{-debugfunc}}("do_rsh : STARTING *********************\n") if defined $args{-debugfunc};
	
	my $host = hostname();
	my $cmd;
	chomp $args{-command};
	chomp $args{-command};
	# print "DBG DBG: HOST = $host HOSTNAME=$args{-hostname}\n";
	if( $args{-hostname} eq $host ) {
		if( $args{-debug} ) {
			print "do_rsh : local command found..\n";
			&{$args{-debugfunc}}("do_rsh : local command found.\n") if defined $args{-debugfunc};
		}
		$cmd=$args{-command};
	} else {  		
		if( $USE_SSH ) {
			#print "XXXDBG AT LINE ",__LINE__,"\n";
			$RUNSSHCMD=find_cmd_in_path("ssh") if $RUNSSHCMD eq "ssh";
			$cmd="$RUNSSHCMD -oBatchMode=yes  ";
			$cmd .= "-oConnectTimeout=10 " if ! defined $ConnectTimeoutOk or $ConnectTimeoutOk==1;
			$cmd .=" $args{-login}\@" if $args{-login};
			$cmd .= "$args{-hostname} ";
			if( $args{-command} =~ /^\"/ ) {
				$cmd .= $args{-command};
			} else {
				$cmd .= "\"".$args{-command}."\"";
			}
		} else {			
			#print "XXXDBG AT LINE ",__LINE__,"\n"; ;
			$RUNRSHCMD=find_cmd_in_path("rsh") if $RUNRSHCMD eq "rsh";
			$cmd=$RUNRSHCMD;
			$cmd .=" -l $args{-login} " if $args{-login};
			$cmd .= " $args{-hostname} ";
			if( $args{-command} =~ /^\"/ ) {
				$cmd .= $args{-command};
			} else {
				$cmd .= "\"".$args{-command}."\"";
			}
		}
	}
#	if( $cmd =~ /;mv \/export\/home\/sybase-dump/ ) {
#		print $cmd;
#		print "\n";
#		die "COMMAND=$args{-command}\n";
#	}
	
	$lastcommand=$cmd;
	$cmd .= " 2>&1";		
	#print "DBG DBG : $cmd\n";
  	if( defined $args{-debugfunc} ) {
		&{$args{-debugfunc}}("do_rsh : $cmd\n");
	} else {
		print "do_rsh: $cmd\n" if $args{-debug};	
	}
	   	
	return "(noexec) $cmd" if $args{-noexec};
	my(@dat2);
	my($rc)=open(CMD,$cmd." |");		
	if( ! $rc ) {	
		&{$args{-debugfunc}}("do_rsh : no return code?\n") if defined $args{-debugfunc};		
	
		my($err)=$!;
   	
   	if( ! defined $ConnectTimeoutOk ) {	# if here - you have a problem! lets just retry with no ConnecTimeout
			$ConnectTimeoutOk = 0;
			return do_rsh(%args);
		}
		
		my($emsg)="CMD=$cmd RC=$err";
   	if( $err =~ /ssh: not found$/ or $err=~/No such file or directory/i ) {
   		# oops no ssh!  use different ssh
   		my($found)='';   		
   		foreach (@ssh_path) {
   			next unless $_."/ssh" eq $RUNSSHCMD;
   			$found=$_;
   			last;
   		}
   		
   		foreach (@ssh_path) {
   			if( $found eq "" ) {
   				next unless -x $_."/ssh";
   				$RUNSSHCMD=$_."/ssh";
   				$found="OK";
   				last;
   			} else {
   				if( $_ eq $found ) {
   					$found = "";
   					next;
   				}
   			}
   		}
   		$emsg.=" Found=$found\n";   		
   		die "CANT FIND ssh IN PATH=\"".$ENV{PATH}."\" or local=\"".join(" ",@ssh_path)."\"\n".$emsg 
   			if $found ne "OK";
   		return do_rsh(%args);
		}		
		die $emsg."\n";
	}
	
	&{$args{-debugfunc}}("do_rsh : processing result set\n") if defined $args{-debugfunc};				
	while(<CMD>) { 
		chomp;
		push @dat2,$_; 
		&{$args{-debugfunc}}("do_rsh : RC=$_\n") 
				if defined $args{show_resultset} and $args{-debugfunc};
	}
	close(CMD);
	
	if( $USE_SSH and ! defined $ConnectTimeoutOk ) {
		if( $dat2[0] =~ /Bad configuration option/ ) {			
			&{$args{-debugfunc}}("do_rsh : your ssh does not handle -oConnectTimeout - retrying\n") 
				if defined $args{-debugfunc};
			$ConnectTimeoutOk=0;
			return do_rsh(%args);
		} else {
			&{$args{-debugfunc}}("do_rsh : your ssh handles -oBatchmode\n") if defined $args{-debugfunc};
			$ConnectTimeoutOk=1;
			# we need to retry the command
		}			
	}
	
	my($msg) = rsh_msg_handler(@dat2);   	
	if( $msg !~ /^failure :/ ) {
		if( defined $args{-debugfunc} ) {
			&{$args{-debugfunc}}("do_rsh : ok\n");
		} else {
			print "do_rsh: ok\n" if $args{-debug};	
		}
	} else {
		if( defined $args{-debugfunc} ) {
			&{$args{-debugfunc}}("do_rsh : $msg\n");
		} else {
			print "do_rsh: $msg\n" if $args{-debug};	
		}
	}
	
	if( defined $args{-debugfunc} ) {
	   if( $args{-noprintresults} ) {
	   	&{$args{-debugfunc}}( "do_rsh : returning $#dat2 Rows\n" );
	   } else {
	   	if( $msg ) {
	 		  &{$args{-debugfunc}}( "do_rsh : returning ($msg)\n" ); 		
	   	} else {
	   		&{$args{-debugfunc}}( "do_rsh : completed - no output\n" );
	   	}
	   }
	}
   return($msg);
}

sub do_rcp {
	my(%args)=@_;
	die "do_rcp() Must pass -hostname\n" unless $args{-hostname};
	die "do_rcp() Must pass -remotefile\n" unless $args{-remotefile};
	die "do_rcp() Must pass -localfile\n" unless $args{-localfile};
#	if( $args{-debug} ) {
#		foreach ( keys %args ) { 			
#			next if /-password/ or /-debugfunc/ or ! defined $args{$_};		
#			if( defined $args{-debugfunc} ) {
#				&{$args{-debugfunc}}("do_rcp : key=$_ val=$args{$_} \n");
#			} else {
#				print "do_rcp : key=$_ val=$args{$_} \n"; 
#			}		
#			#print "do_rcp : key=$_ val=$args{$_}\n" unless $_ eq /-password/; 
#			#&{$args{-debugfunc}}("do_rcp : key=$_ val=$args{$_}\n") if defined $args{-debugfunc};
#		}
#	}
	do_rsh_method($args{-method}) if $args{-method};

	my $host = hostname();
	my $cmd;
	if( $args{-hostname} eq $host ) {
		die "do_rcp: local command found...\n" if $args{-debug};
		#$cmd=$args{-command}." 2>&1";
	} else {
		if( $USE_SSH ) {
			$RUNSCPCMD=find_cmd_in_path("scp") if $RUNSCPCMD eq "scp";		
			$cmd="$RUNSCPCMD -oBatchMode=yes ";
			$cmd .= "-oConnectTimeout=10 " if ! defined $ConnectTimeoutOk or $ConnectTimeoutOk==1;			
		} else {
			$RUNRCPCMD=find_cmd_in_path("rcp") if $RUNRCPCMD eq "rcp";		
			$cmd=$RUNRCPCMD." ";
		}

		if( $args{-mode} eq "get" ) {
			$cmd.=$args{-login}."\@".$args{-hostname}.":".$args{-remotefile}." ".$args{-localfile};
		} elsif( $args{-mode} eq "put" ) {
			$cmd.="$args{-localfile} $args{-login}"."\@".$args{-hostname}.":".$args{-remotefile};
		}
		$lastcommand=$cmd;
		$cmd .= " 2>&1";
	}
   if( $args{-debug} ) {
		if( defined $args{-debugfunc} ) {
			&{$args{-debugfunc}}("do_rsh : $cmd\n");
		} else {
			print "do_rcp: $cmd\n"; 
		}
	}   
	return "(noexec) $cmd" if $args{-noexec};
	my(@dat2);
	my($rc)=open(CMD,$cmd." |");

	# I LITERALLY DONT UNDERSTAND WHY I NEED TO DO THIS ...
	if( ! $rc ) {
		my($err)=$!;
   	
   	if( ! defined $ConnectTimeoutOk ) {	# if here - you have a problem! lets just retry with no ConnecTimeout
			$ConnectTimeoutOk = 0;
			return do_rcp(%args);
		}
		
		my($emsg)="Cant run $cmd RC=$err";
   	if( $err =~ /: not found$/ or $err=~/No such file or directory/i ) {
   		# oops no ssh!  use different ssh
   		my($found)='';
   		$emsg.=" SSHPATH=".join("~",@ssh_path);
   		foreach (@ssh_path) {
   			next unless $_."/scp" eq $RUNSCPCMD;
   			$found=$_;
   			last;
   		}
   		foreach (@ssh_path) {   			
   			if( $found eq "" ) {
   				$emsg.=" testing $_/scp ";
   				next unless -x $_."/scp";
   				$RUNSCPCMD=$_."/scp";
   				$found="OK";
   				last;
   			} else {
   				if( $_ eq $found ) {
   					$found = "";
   					next;
   				}
   			}
   		}
   		$emsg.=" Found=$found\n";   		
   		die "CANT FIND scp IN PATH=\"".$ENV{PATH}."\" or local=\"".join(" ",@ssh_path)."\"\n".$emsg if $found ne "OK";
   		
   		do_rcp(%args);
   		return;
		}
		
		die $emsg."\n";
	}
	while(<CMD>) { chomp;push @dat2,$_; }
	close(CMD);

	if( $USE_SSH and ! defined $ConnectTimeoutOk ) {
		if( $dat2[0] =~ /Bad configuration option/ or $dat2[0]=~/Local: Bad packet length/ ) {
			&{$args{-debugfunc}}("do_rcp: your ssh does not handle -oBatchmode - retrying\n") if defined $args{-debugfunc};
			$ConnectTimeoutOk=0;
			return do_rsh(%args);	# we need to retry the command
		} else {
			$ConnectTimeoutOk=1;
			&{$args{-debugfunc}}("do_rcp: your ssh handles -oBatchmode") if defined $args{-debugfunc};			
		}			
	}
	
	my($msg) = rsh_msg_handler(@dat2);   	
	print "do_rcp: ok\n" 		if $args{-debug} and $msg!~/^failure :/;
   &{$args{-debugfunc}}("do_rcp: returning ($msg)\n") if defined $args{-debugfunc};
   &{$args{-debugfunc}}("do_rcp: ok\n") if defined $args{-debugfunc};
   return $msg;

#	my(@dat2) = `$cmd`;
#	print "DBG DBG: ",__LINE__," Dat2 is ",join("~",@dat2),"\n" if $args{-debug};
#	
#	if( $USE_SSH and ! defined $ConnectTimeoutOk ) {
#		if( $dat2[0] =~ /Bad configuration option: ConnectTimeout/ ) {
#			&{$args{-debugfunc}}("do_rsh : your ssh does not handle -oBatchmode - retrying") if defined $args{-debugfunc};
#			$ConnectTimeoutOk=0;
#			return do_rsh(%args);
#		} else {
#			$ConnectTimeoutOk=1;
#			&{$args{-debugfunc}}("do_rsh : your ssh handles -oBatchmode") if defined $args{-debugfunc};
#			# we need to retry the command
#		}			
#	}
#	print "DBG DBG: ",__LINE__," Dat2 is ",join("~",@dat2),"\n" if $args{-debug};
#	my($msg) = rsh_msg_handler(@dat2);
#	#print "do_rcp: output=$msg\n" 	if $args{-debug};
#	#print "do_rcp: ok\n" 		if $args{-debug} and $msg!~/failure :/;
#	&{$args{-debugfunc}}("do_rcp: output=$msg\n") if defined $args{-debugfunc};
#	&{$args{-debugfunc}}("do_rcp: ok\n") if defined $args{-debugfunc} and $msg!~/failure :/;
#   return $msg;
}

sub rsh_msg_handler {
	my($line1)=$_[0];
	my($dat2)=join("\n",@_);

	return "" if $dat2 eq "";
	# parse out ssh messages
	my($dat)="";
	foreach ( split(/\n/,$dat2 )) {
		next if /^Using rsh.  WARNING: Connection will not be encrypted./;
		next if /Secure connection to/ and /refused; reverting to insecure method./;
		next if /No host key is known/ and /when running in batch mode.$/;
		$dat .= $_."\n";
	}
	chomp $line1;
	chomp $line1;
	chomp $dat;
   chomp $dat;
   
   #print "\tDBG DBG: L1=$line1\n\tDBG DBG: DAT=$dat\n";
   #if( $dat=~ /Host key verification failed\.*\s*$/i ) {
   #	die "DBG DGB CAUGHT IT\n";
  # }
		
	if( $dat =~ /: unknown host\.*$/i ) {
   	return "failure : $dat";
   } elsif( $dat =~ /: not found\.*$/i ) {
   	return "failure : $dat";
   } elsif( $dat =~ /permission denied\.*$/i ) {
   	return "failure : permission denied";
   } elsif( $dat =~ /Permission denied / ) {
   	return "failure : ssh/scp permission denied";
   } elsif( $dat =~ /connection timed out\.*$/i ) {
   	return "failure : connection timed out";
   } elsif( $dat =~ /Connection refused[\.\s]*$/i or $line1 =~ /Connection refused\s*$/i ) {
   	return "failure : Connection refused";
   } elsif( $line1 =~ /^Local: Bad packet length/i ) {
   	return  "failure : $dat";   	
  } elsif( $dat =~ /not found[\.\s]*$/i ) {
   	return  "failure : dir/command not found";   	
   } elsif( $line1 =~ /^Host key verification failed/i 
   or       $dat   =~ /Host key verification failed\.\s*$/i ) {
   	return  "failure : Host key verification failed";   	
   } elsif( $line1 =~ /^Host key not found from the list/i ) {
   	return  "failure : $dat";   	 
   } elsif( $line1 =~ /Bad configuration option: Batchmode/ ) {
   		return  "failure : bad ssh option! Batchmode";   	
   } elsif( $line1 =~ /^[a-z]?sh:/ and $line1 =~ /:\s*not found$/ ) {
   	return  "failure : directory not found";
   } elsif( $dat =~ /: no address associated with hostname\.*$/i ) {
   	return "failure : $dat";
   } elsif( $line1 =~ /^[a-z]?sh:/ and $line1=~/connection timed out/i ) {
   	return "failure : connection timed out";
   } elsif( $line1 =~ /\scd:\s/ and $line1=~/No such file or directory/i ) {
   	return "failure : Bad Directory For cwd() command"; 
   } elsif( $line1 =~ /:\s*not found*$/ ) {
   	warn  "WARNING 2: (".$line1.") FILE=".__FILE__." LINE=".__LINE__."\n";
   	return "failure : $dat";
   } elsif( $line1 =~ /^[a-z]?sh:/ ) {
   	warn  "WARNING 1: FILE=".__FILE__." LINE=".__LINE__." failure : $dat\n";
   	return "failure : $dat";
   } elsif( $dat =~ /No such file or directory\.*$/i ) {
   	return "failure : No such file or directory";
	} else {
		return $dat;
	}
}

1;

__END__

=head1 NAME

myRSH.pm - unix systems interface

=head2 SYNOPSIS

This library is a wrapper around rsh and ssh such that you can run remote commands directly from perl.

=head2 FUNCTION

do_command 

	my($rc)=do_command(-hostname=>$host, -command=>$COMMAND, -debug=>$DEBUG, -login=>$login);
 	
	-method		optional		rsh or ssh
	-hostname	mandatory	hostname to run on
	-command		mandatory	command to run 
	-login		optional		login to use
	-password	optional		unused - maintained for compatibility
	-noexec		optional		if set - print the messages only
	-debug		optional		print diagnostic messages
	-debugfunc	optional		print function for diagnostic messages (funcref)
	
do_copy

	my($rc)=do_copy(-hostname=>$host, -localfile=>$F, -remotefile=>$G, -mode=put -debug=>$DEBUG, -login=>$login);
	
	-method		optional		rsh or ssh
	-hostname	mandatory	hostname to run on
	-remotefile	mandatory	remote file name
	-localfile	mandatory	localfile file name
	-mode			mandatory	get or put
	-noexec		optional		if set - print the messages only
	-login		optional		login to use
	-debug		optional		print diagnostic messages
	-debugfunc	optional		print function for diagnostic messages (funcref)
		
do_rsh_method($method)	- sets the communications mechanism to "rsh" or "ssh"
	
rsh_lastcommand()	- returns the last command run

=head2 EXAMPLE 1

	sub run_a_command
	{
		my($host,$login,$password,$method,$cmd)=@_;	
		print "Host=$host Login=$login Method=$method Cmd=$cmd".$NL if $opt_d;
		my $dat;
		do_rsh_method($method);
	
		$dat=do_command( -login=>$login, -hostname=>$host, -command=>$cmd, -debug=>$opt_d);
		if( $dat=~/^failure/ ) {
			if ( $dat =~ /connection timed out/ ) {
				# not much you can do here
				print "host $host connection timed out".$NL;
				$OUTPUT{$host} =$FS."host $host connection timed out".$FE.$NL;
				return;
			} elsif ( $dat =~ /not found/ ) {
				print "host $host: command $opt_c returned $dat".$NL;
				$OUTPUT{$host} =$FS."host $host: command $opt_c returned $dat".$FE.$NL;
				return;
			} else {
				print "ERROR: $dat!\n";
			}
		}		
		$OUTPUT{$host} = $dat;
		$num_hosts++;
	}
