#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
use strict;
use Getopt::Std;
use File::Find;
use Repository;
use Net::FTP;
use vars qw($opt_i $opt_d $opt_o $opt_b);

my($curdir)=dirname($0);

sub usage
{
	my($msg)=@_;
	return $msg."
Usage: $0 -o out_interfaces_file -i unix_directory [-d]
	or
		 $0 -b -i interfaces file

UNDER HEAVY DEVELOPMENT

The first syntax builds an interfaces file.  The second broadcasts it
to all your servers (renaming existing interfaces files as needed).

unix_directory should have been previously built with build_unix_db.pl

This program will output queries required to rename your remote
servers so there are no backup server conflicts at the top of the
output interfaces file.
\n";
}

die usage()											     unless getopts('di:o:b');
die usage()											     unless defined $opt_i;
die usage("Directory $opt_i Not Found\n")	       unless -e $opt_i;
die usage("Directory $opt_i Not A Directory\n") unless -d $opt_i;

if( defined $opt_b ) {
	my(@hosts)=get_password(-type=>"unix",-name=>undef);
	foreach my $host (@hosts) {
	}
} else {
	find(\&process_file,$opt_i);
}

my(%DATA,%DATAcount);
sub process_file {
	return unless /^interfaces/;
	print $File::Find::name,"\n";
	open( INFILE,$_ )
			or die "Cant open ".$_." $!\n";
	my($item,$server)=("","");
	while ( <INFILE> ) {
		next if /^\s*#*\s*$/;

		if( /^\s/ ){
			$item.=$_;
		} else {
			if( $server ne "" and $item ne "" ) {
				# save current item
				if( defined $DATAcount{$server} ) {
					my($c)=1;
					while( $c<=$DATAcount{$server} )  {
						if( $DATA{$server." ".$c} eq $item ) {
							# data is ok
							$c=10000;
						} else {
							my($item_no_spaces)=$item;
							my($data_no_spaces)=$DATA{$server." ".$c};

							# remove comments
							$item_no_spaces  =~ s/#+[ \t\S]+\n//g;
							$data_no_spaces  =~ s/#+[ \t\S]+\n//g;

							# remove white space
							$item_no_spaces  =~ s/\s//g;
							$data_no_spaces  =~ s/\s//g;

							print "DBG:$server\n-->$item_no_spaces\n-->$data_no_spaces\n\n"
									if $data_no_spaces ne $item_no_spaces;
							$c=10000 if $data_no_spaces eq $item_no_spaces;
						}
						$c++;
					}

					if( $c < 10000 ) {
						$DATAcount{$server}++;
						$DATA{$server." ".$DATAcount{$server}}=$item;
						print "DBG $server Duplicate...\n";
					} else {
						print "DBG $server Ok...\n";
					}

				} else {
					$DATAcount{$server}=1;
					$DATA{$server." 1"}=$item;
					print "DBG $server New...\n";
				}
				$server="";
				$item="";
			}

			if( ! /^#/ ) {
				my(@tmp)=split;
				$server=$tmp[0];
			}

			$item.=$_;
		}
	}
	close INFILE;
}

open( OUT,">".$opt_o ) or die "Cant write to $opt_o : $!\n";
foreach (sort keys %DATA) {
	my($s,$cnt)=split;
	if( $cnt>1 ) {
		print OUT "## THIS ENTRY MIGHT BE A DUPLICATE\n";
	}
	print OUT $DATA{$_},"\n";
}
close OUT;

__END__

=head1 NAME

brdcst_interfaces.pl - broadcast interfaces file to other servers

=head2 DESCRIPTION

UNDER HEAVY DEVELOPMENT

Broadcasts your interfaces file to other machines.  Requires homgenious
environment.

=cut

