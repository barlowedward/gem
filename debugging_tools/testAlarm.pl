use      strict;
use      Carp;

local $SIG{ALRM} = sub { die "CommandTimedout"; };

eval {
	alarm(2);   	
	sleep(100);
	alarm(0);
};

if( $@ ) {
	if( $@=~/CommandTimedout/ ) {
		die "Command Timed Out\n";			
	} else {
		print "Command Returned $@\n";
	}
}

die "DONE";

