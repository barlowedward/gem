#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2004-5 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use 	IO::Socket;
use 	MlpAlarm;
use 	CommonFunc;
use  	File::Basename;
use 	Getopt::Long;
use  	Repository;
use 	DBIFunc;

my 	$VERSION = 1.2;
use vars qw($DEBUG);

GetOptions( "DEBUG" => \$DEBUG );
#use vars qw( $GENERATE $DEBUG $SLEEPTIME $TEST $LOGALARM $ITERATIONS $DEBUG2 $UPDATE_GEM $discovery_ok_file
#		$CFGFILE $INTERFACE_FILE $SYSTEM $NORECHECK $NOLOCK $BATCHID);
		
my(%IPFAIL,%ipcache);		# from ip.dat

print "port_monitor.pl $VERSION - run at ".localtime(time)."\n";
print "Copyright (c) 2001-2011 by Sql Technologies\n\n";

my($root_dir)=get_gem_root_dir();
my($CFGFILE) ="$root_dir/conf/port_monitor.dat";

die "Must pass configuration file (no $root_dir/conf directory)\n"	unless defined $CFGFILE;

my(@file_original_lines);
my(%portping_conf_additions_state);
my(@file_top_comment);
my(@interface_file_data_rows);
my(@portping_conf_additions);

# THESE ARE THE PARSED VALUES THAT ARE READ FROM THE CONFIG FILE
my(@file_keys);		# key=$host.$port;
my(%file_logicalname_bykey);
my(%file_servername_bykey);
my(%file_portnm_bykey);
my(%file_desc_bykey);
my(%file_pingstatus);
my(%file_original_line_byhostport);
my(%file_iscommented_bykey);

my(%long_service_desc);

	read_port_configfile($CFGFILE,1) if -r $CFGFILE;

	print "
==============================================
|  PORT MONITOR GENERATE CALLED              |
==============================================
";

	if( defined $ENV{SYBASE} ) {
		print "SYBASE Environment = $ENV{SYBASE}\n";
		print "Reading Interfaces File\n";
		# This comes back as SERVER HOST PORT
		push @interface_file_data_rows, dbi_get_interfaces();
		print " => Adding ",($#interface_file_data_rows+1)," Elements From Interfaces Files\n";
	}

	print "Reading GEM Configuration Files\n";
	my(%server_type);
	my($cnt)=0;
	foreach (get_password(-type=>'sqlsvr') ) { 
		$cnt++;
		$server_type{$_} = "SQL Server"; 	
		if( ! defined $file_servername_bykey{$_."1433"} ) {		# allready monitored
			next unless check_up_quick($_,1433,1);
			my($l)=sprintf("%-30s %-10s %s %s\n", $_,1433,$_,"Microsoft SQL Server");
			push @portping_conf_additions,$l;
			$portping_conf_additions_state{$l} = 1;		# its a registered server - of course it needs to be monitored
			$file_servername_bykey{$_.".1433"}=$_;
		}
	}
	print " => Read $cnt SQL Server Definitions\n";
	
	$cnt=0;
	foreach (get_password(-type=>'sybase') ) { 
		$cnt++;
		$server_type{$_} = "Sybase Server"; 	
	}
	print " => Read $cnt Sybase Server Definitions\n";

	my(%INTERFACE_INFO);			# key = host,port  value = array of if names listed on that host port
	my(%INTERFACE_HOSTPORT);	# key = nm value=hostport => needed because of unix system aliasing sigh
	print "\nTesting Data From Interfaces File\n";
	foreach ( @interface_file_data_rows ) {
		chomp $_->[0];	# server
		chomp $_->[1];	# host
		chomp $_->[2];	# port
		$_->[0] =~ s/\+.+$//;
		$_->[1] =~ s/\+.+$//;
		$_->[2] =~ s/\+.+$//;

		next unless $_->[0];
		next unless $_->[1];
		next unless $_->[2];

		my($nm)   = $_->[0];
		my($Xhost) = $_->[1];
		my($Xport) = $_->[2];

		if( $Xhost =~ /\d+\.\d+\.\d+\.\d+/ ) {		# ip address
			my($a)=GetHostFromIp($Xhost);
			$Xhost = $a if $a;
		}
		$Xhost =~ s/\.[a-z\.]+$//;		# remove .xxx.com type formats

		my($type)="";
		$nm=~s/\s+$//;
		if( defined $server_type{$nm} ) {
			$type=$server_type{$nm};
			if( $server_type{$nm} eq "SQL Server" ) {
				$server_type{$nm}="MS SQL Server";
			}
		} elsif( $nm=~/_BACK$/i ) {
			$nm=~s/_BACK$//i;
			$type="Backup Server";
		} elsif( $nm=~/_HS$/i ) {
			$nm=~s/_HS$//i;
			$type="Historical Server";
		} elsif( $nm=~/_XP$/i ) {
			$nm=~s/_XP$//i;
			$type="XP Server";
		} elsif( $nm=~/_MON$/i ) {
			$nm=~s/_MON$//i;
			$type="Monitor Server";
		} elsif( $nm=~/_BACKUP$/i ) {
			$nm=~s/_BACKUP$//i;
			$type="Backup Server";
		} elsif( $nm=~/_RS$/i ) {
			$nm=~s/_RS$//i;
			$type="Rep Server";
		} elsif( $nm=~/_BS$/i ) {
			$nm=~s/_BS$//i;
			$type="Backup Server";
		} elsif( $Xport == 1433 ) {
			$type="SQL Server";
		} else {
			$type="";
		}
		if( defined $type and $type ne "" ) {
			printf "  Testing %-30.30s %28.28s",$_->[0]." (".$type.")"," [".$Xhost.":".$Xport."]";
		} else {
			printf "  Testing %-20s %38s",$nm," [$Xhost:$Xport]";
		}

		my($line)=sprintf("%-30s %-10s %s %s\n", $Xhost,$Xport,$nm,$type);
		if( check_up_quick($Xhost,$Xport,1) ) {
			print " -- ok ";
			$portping_conf_additions_state{$line}=1;
			# $possible_unix_host{$Xhost}=1;
			$INTERFACE_HOSTPORT{$nm} = $Xhost."~".$Xport;
			if( defined $INTERFACE_INFO{$Xhost."~".$Xport} ) {
				push @{$INTERFACE_INFO{$Xhost."~".$Xport}}, $nm;
			} else {
				my(@x);
				push @x,$nm;
				$INTERFACE_INFO{$Xhost."~".$Xport} = \@x;
			}			# key = host,port  value = array of if names listed on that host port
			#print "\nDBG DBG - ADDING POSSIBLE UNIX SERVER $Xhost";
			# $possible_sybase_server{$_->[0]}=$Xhost."~".$Xport if $type eq "" or $type eq "Sybase Server";
			#print "\nDBG DBG - ADDING POSSIBLE SYBASE SERVER $nm";
		} else {
			$portping_conf_additions_state{$line}=0;
			print " -- fail";
   	}
		print "\n";
		#print "Adding Line $line\n" if $TEST;
		if( ! defined $file_servername_bykey{$Xhost.$Xport} ) {
			push @portping_conf_additions,$line;
			$file_servername_bykey{$Xhost.$Xport} = $nm;
		}
	}

	print "\nChecking Existing Port Monitor Config File\n";
	foreach ( @file_keys ) {
		if( $file_desc_bykey{$_} ) {
			printf "  Testing %-30s %28s",$file_logicalname_bykey{$_}." ($file_desc_bykey{$_})","[$file_servername_bykey{$_}:$file_portnm_bykey{$_}]";
     	} else {
			printf "  Testing %-20s %38s",$file_logicalname_bykey{$_}," [$file_servername_bykey{$_}:$file_portnm_bykey{$_}]";
     	}
		$file_pingstatus{$_}=check_up_quick($file_servername_bykey{$_},$file_portnm_bykey{$_},1 );
     	if( $file_pingstatus{$_} ) {
     		print " -- ok  \n";
     		# $possible_unix_host{$file_servername_bykey{$_}}=1;
			# $possible_sybase_server{ $file_logicalname_bykey{$_} }=  $file_servername_bykey{$_}."~".$file_portnm_bykey{$_};
		} else {
      	print " -- fail\n";
      }
   }
	
	my $output="";
	$output .= "# FINAL OUTPUT FILE WILL BE:\n" if $DEBUG;
	$output .= join("\n",@file_top_comment)."\n";
	$output .= "# ORIG LINES ===================\n" if $DEBUG;
	foreach( @file_keys ) {
#		if( $NORECHECK ) {
#			$output.="#" if $file_iscommented_bykey{$_};
#			$output .= $file_original_line_byhostport{$_}."\n";
#			next;
#		}

		my($status)=$file_pingstatus{$_};
		die "NO STATUS for $_ \n" unless defined $status;
		if( $status ==1 and $file_iscommented_bykey{$_} ) {
			print "Removing Commented Out Key $_\n" if $DEBUG;
			$output .= $file_original_line_byhostport{$_}."\n";
		} else {
			print "Not Commenting Out Failed Key $_\n" if $DEBUG;
			$output.="#" if $file_iscommented_bykey{$_};
			$output .= $file_original_line_byhostport{$_}."\n";
		}
	}

	$output .= "# ADDITIONS ===================\n" if $DEBUG;
	foreach ( sort @portping_conf_additions ) {
		if( $portping_conf_additions_state{$_} ) { #ok
			$output .= $_;
		} else {
			$output .= "#".$_;
		}
	}

	$output .= "# END ===================\n" if $DEBUG;
	$output=~s/\r//g;

	if( $DEBUG ) {
		print "port_monitor.dat> ",$output;
	} else {
		print "SAVING PORT MONITOR DATA TO $CFGFILE\n";
		open( CFG , "> $CFGFILE" );
		print CFG $output;
		close(CFG);
		print "GENERATION OF CONFIG FILE COMPLETED\n";
	}
	exit(0);
	
##################################		

sub GetHostFromIp {
	my($ip)=@_;
	foreach ( keys %ipcache ) { return $_ if $ipcache{$_} eq $ip; }
	return undef;
}

sub GetIpAddress {
	my($svr)=@_;
	return $ipcache{ uc($svr) } if $ipcache{ uc($svr) };
	return undef if $svr =~ /\\/;
	return undef if $IPFAIL{$svr};

	my(@address)=gethostbyname($svr);
	if( $#address<0 ) {
		warn "Cant resolve $svr $!\n";
		return undef;
	}
	@address= map { inet_ntoa($_) } @address[4..$#address];
	print "  ==> GetIpAddress($svr) = ",join(" ",@address),"\n" if $DEBUG;
	$IPFAIL{$svr}=1 if $#address<0;
	print "DBG DBG: IP FAILED FOR $svr\n" if $#address<0;
	$ipcache{uc($svr)} = $address[0];;
	return $address[0];
}

sub read_port_configfile {
   my($CFGFILE, $READ_COMMENTS_TOO)=@_;
   print "Reading $CFGFILE (READ_COMMENTS=$READ_COMMENTS_TOO)\n";
   open( DAT, $CFGFILE ) or die "Cant read configuration file $CFGFILE : $!\n";

   my(@file_readparse_errors);
   my($found_first_comment_line)=0;
   while( <DAT> ) {
			chomp;
			chomp;
			s/\s+$//;
			next if /^\s*$/;

			if( ! $found_first_comment_line ) {
				push @file_top_comment,$_;
				$found_first_comment_line=1 if /###############/;
				next;
			}

			# WE ARE IN DATA SECTION NOW
			push @file_original_lines,$_ 	if $READ_COMMENTS_TOO;

			my($iscomment)=1 if /^#/;
			next if $iscomment and ! $READ_COMMENTS_TOO;
			s/^#\s*//;

			push @file_original_lines,$_ unless $READ_COMMENTS_TOO;

        	my($host,$port,$file_logicalname_bykey,$desc)=split(/\s+/,$_,4);

        	# skip the line unless its fully formed
        	if( ! defined $file_logicalname_bykey or $file_logicalname_bykey=~/^\s*$/ or ! defined $host or ! defined $port ) {
				push @file_readparse_errors,$_;
				next;
			}

			$host =~ s/\.[a-z\.]+$//;		# remove .xxx.com type formats
			my($key)=$host.$port;
			push @file_keys,$key;
			$file_servername_bykey{$key}=$host;
			$file_original_line_byhostport{$key}=$_;
			$file_iscommented_bykey{$key}=$iscomment;
			$file_portnm_bykey{$key}=$port;
			$file_logicalname_bykey{$key}=$file_logicalname_bykey;
			$desc=~s/\s+$//;
			chomp $desc;
			$desc="" unless defined $desc;
			$file_desc_bykey{$key}=$desc;
   }
   close(DAT);
	die "ERROR NO LINE WITH ############## FOUND IN $CFGFILE" unless $found_first_comment_line;
   
   if( $#file_readparse_errors>=0  and $DEBUG ) {
   	warn "Warning : bad config line (needs 4 values)\n==> [".join(" \]\n==> \[",@file_readparse_errors)."\n";
   }
   print " => Read ",($#file_keys+1)," Existing Port Ping Definitions\n";
}

my(%CHECK_UP_QUICK_RESULTS);	# key= name|port
sub check_up_quick {
	my($name,$port,$timeout)=@_;

	return $CHECK_UP_QUICK_RESULTS{$name."|".$port} if defined $CHECK_UP_QUICK_RESULTS{$name."|".$port};
	$timeout=1 unless $timeout;
   #local $SIG{__WARN__} = sub { };
   local $SIG{ALRM} = sub { die "timeout"; };
	$CHECK_UP_QUICK_RESULTS{$name."|".$port} = 0;

	my($connection);
	eval {
		alarm(2*$timeout);
   	$connection = IO::Socket::INET->new(Proto=>'tcp', PeerAddr=>$name, PeerPort=>$port, Timeout=>$timeout);
   	alarm(0);
	};
	if( $@ ) {
		if( $@=~/timeout/ ) {
			return 0;
		} else {
			return 0;
			alarm(0);
		}
	}

	print "check_up_quick($name,$port,$timeout) - returning 0\n"  if $DEBUG and ! $connection;
   return 0 unless $connection;
   $CHECK_UP_QUICK_RESULTS{$name."|".$port} = 1;
   print "check_up_quick($name,$port,$timeout) - returning 1\n" if $DEBUG;
	return 1;
}