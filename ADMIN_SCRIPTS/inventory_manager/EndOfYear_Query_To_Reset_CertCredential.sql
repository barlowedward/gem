
update INV_cert_credential
   set    display_name=t.display_name
   from INV_cert_credential c, INV_principal_map t
   where t.system_name = c.system_name
	and	t.credential_name = c.credential_name
	and t.principal_name = c.principal_name
	and t.display_name != c.display_name
	and t.system_name='COMPLIANCEDB'
    and t.system_type in ('sqlsvr','oracle','sybase')

update INV_cert_credential
    set    display_name=c2.display_name
    from   INV_cert_credential c, INV_credential     c2
    where  c2.credential_name = c.principal_name
    and    c2.credential_type='ldap user'
    and    c2.display_name is not null
    and    c.display_name!=c2.display_name

select * into #TMP from INV_cert_credential where 1=2
   insert #TMP select distinct system_name,credential_name,principal_name,display_name,status,'',null,null from INV_cert_credential

update #TMP set comment = c.comment
   from #TMP t, INV_cert_credential c
   where t.system_name = c.system_name
	and	t.credential_name = c.credential_name
	and t.principal_name = c.principal_name
	and t.display_name   = c.display_name

update #TMP set approved_by = c.approved_by, approved_time = c.approved_time
   from #TMP t, INV_cert_credential c
   where t.system_name = c.system_name
	and	t.credential_name = c.credential_name
	and t.principal_name = c.principal_name
	and t.display_name   = c.display_name

delete INV_cert_credential
insert INV_cert_credential select * from #TMP

CREATE UNIQUE CLUSTERED INDEX CXINV_cert_credential ON dbo.INV_cert_credential (system_name,credential_name,principal_name,display_name)

