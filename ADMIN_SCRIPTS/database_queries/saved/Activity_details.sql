select 1 as l1
,       convert(nvarchar,count(distinct(s.session_id))) as col
,               0 as severity
,       1 as state, 1 as msg
from sys.dm_exec_sessions s
where s.is_user_process = 1 and s.status = ''running''
union
select 2 as l1
,       convert(nvarchar,count(at.transaction_id)) as col
,               0 as severity
,       1 as state, 1 as msg
from sys.dm_tran_active_transactions at
where at.transaction_state = 2 or ( at.transaction_type = 4 and at.dtc_state = 1)
union
select 3 as l1
,       convert(nvarchar,count(*)) as col
,               0 as severity
,       1 as state, 1 as msg
from sys.databases where state = 0
union
select 4 as l1
,       convert(nvarchar,cntr_value) as col
,               0 as severity
,       1 as state, 1 as msg
from sys.dm_os_performance_counters
where counter_name like ''%Total Server Memory (KB)%''
union
select 5 as l1
,       convert(nvarchar,count(*)) as col
,              0 as severity
,       1 as state
,       1 as msg
from sys.dm_exec_sessions
where is_user_process = 1 and status = ''sleeping''
union
select 6 as l1
,       convert(nvarchar,count(distinct(request_owner_id))) as col
,               0 as severity
,       1 as state
,       1 as msg
from sys.dm_tran_locks
where request_status = ''WAIT''
union
select 7 as l1
,       convert(nvarchar,count(distinct s.login_name)) as col
,               0 as severity
,       1 as state, 1 as msg
from sys.dm_exec_sessions s
where s.is_user_process = 1
union
select 8 as l1
,       convert(nvarchar,count(id)) as col
,               0 as severity
,       1 as state, 1 as msg
from sys.traces where status = 1
end try
begin catch
select -100 as l1
,       ERROR_NUMBER()  as col
,       ERROR_SEVERITY() as severity
,       ERROR_STATE() as state
,       ERROR_MESSAGE() as msg
end catch',@params=N'
