#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# use subs qw(logdie infof info warning);
use strict;
use Getopt::Std;
use DBIFunc;
use CommonFunc;
use File::Basename;
use Logger;
use MlpAlarm;

use vars qw($opt_M $opt_w $opt_p $opt_n $opt_I $opt_E $opt_i $opt_s $opt_m $opt_k $opt_U $opt_S $opt_P $opt_D $opt_v $opt_e
$opt_l $opt_d $opt_h $opt_a %CONFIG);

sub usage
{
   print @_."\n" if @_;
   print "USAGE: $0 -UUSER -SSERVER -PPASS [-DDB]
   -p samplepct   sample percent for both sybase & sql
   -D DB_LIST     (pipe separated list with wildcards of databases)
   -E TABLES      (comma separated list of tables to EXCLUDE)
   -I TABLES      (comma separated list of tables to INCLUDE)
   -s             (silent mode)
   -k             keep old stats (ie no delete stats)
   -d             (debug mode)
   -vVAL          number of steps/values to use (suggest 60 - dflt 20) [sybase]
   -h             (html output)
   -i             update index stats
   -a             update all stats
   -n             noexec
   -e errorlog    (optional)
   -m					update only missing stats [sybase]
   -w					update only statistics over 1 week old [sybase]
   -l sessionlog  (optional)";

   return "\n";
}

$| =1;
my($db);
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage() if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List") unless getopts('nM:sU:D:S:mwP:kl:e:p:dhiE:I:v:a');

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  undef,
            -subsystem =>undef,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

use Sys::Hostname;
info( "update_stats.pl : run at ".localtime(time)."\n" );
info( "run on hostname=",hostname(),"\n" );

logdie usage("Must pass sa password\n" )  unless defined $opt_P;
logdie usage("Must pass server\n")			unless defined $opt_S;
logdie usage("Must pass sa username\n" )  unless defined $opt_U;

my($show_output);
$show_output=1 unless defined $opt_s;

if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}
#
# CONNECT TO DB
#
info "update_stats.pl version 2\n";
info "started at ".localtime(time)."\n";
info "Connecting to database",$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-type=>$opt_M,-password=>$opt_P,-debug=>$opt_d)
	or logdie "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");
my($server_type,@databaselist)=dbi_parse_opt_D($opt_D,1);

info("Server Type=$server_type\n");
if( $#databaselist<0 ) {
	warning "No Databases Found On Server Matching Spec $opt_D\n";
	my($server_type,@databaselist)=dbi_parse_opt_D("%",1);
	warning "Allowed Databases:",join(" ",@databaselist),"\n";
	exit(0);
}

if( $opt_E ) {
	$opt_E.=',sysgams,syslogs';
} else {
	$opt_E='sysgams,syslogs';
}

dbi_set_option( -croak=>\&mycr, -print=> \&mypr);

my(  %moreargs );
$moreargs{-where} = " id not in ( select distinct id from sysstatistics where moddate > dateadd(dd,-7,getdate()) )" if $opt_w;
$moreargs{-where} = " id not in ( select distinct id from sysstatistics )" if $opt_m;

my($IN_INDEX)=0;
foreach $db (@databaselist) {

  	next if $db eq "master" or
                $db eq "model"
                or $db eq "sybsystemprocs"
                or $db eq "sybsystemdb"
                or $db eq "sybsecurity"
                or $db eq "tempdb";

	my($tstart)=time;
	MlpLogOperationStart('update_stats',$opt_S,$db,'');
	info "Working On Database $db\n";
	my($cmd)="update statistics";
	$cmd="update index statistics" if defined $opt_i and $server_type ne "SQLSERVER";
	$cmd="update all statistics" if defined $opt_a;
	my($systable_cmd)=$cmd;

	$cmd.= " OBJECT " 					if $opt_v or $opt_p;

	my($num_obj_done)=0;
	if( $server_type eq "SQLSERVER" ) {
		die "CANT USE BUCKETS IN SQL SERVER (no -v option to update_stats.pl)" if $opt_v;
		$cmd.= " with sample $opt_p PERCENT" 	if defined $opt_p;
	} else {
		$cmd.= " using $opt_v values" 	if defined $opt_v;
		$cmd.= " with sampling=$opt_p" 	if defined $opt_p;
	}

	if( $server_type eq "SQLSERVER" ) {
		if( $opt_i ) {
			$IN_INDEX=1;
			$num_obj_done += dbi_eachobject(
				-db=>$db,
				-cmd=>"DBCC SHOW_STATISTICS ( OBJECT, INDEX ) with stat_header",
				-cmd1=> $cmd." OBJECT INDEX",
				-cmd2=>"DBCC SHOW_STATISTICS ( OBJECT, INDEX ) with stat_header",
				-no_exec=>$opt_n,
				-type=>"I",
				-sqlsvr=>1,
				-do_print=>$show_output,
				-debug=>$opt_d,
				# -no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
   		$IN_INDEX=0;
   		$num_obj_done += dbi_eachobject(
				-db=>$db,
				-cmd=>"exec sp_recompile",
				-no_exec=>$opt_n,
				-type=>"U",
				-sqlsvr=>1,
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
		} else {
   		$num_obj_done += dbi_eachobject(
				-db=>$db,
				-cmd=> $cmd,
				-cmd2=>"exec sp_recompile",
				-no_exec=>$opt_n,
				-type=>"U",
				-sqlsvr=>1,
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
		}
	} elsif( $opt_k )  {
		$num_obj_done += dbi_eachobject(
				-db=>$db,
				-cmd=>$systable_cmd,
				%moreargs,
			#	-cmd1=>"exec sp_recompile",
				-no_exec=>$opt_n,
				-type=>"S",
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
   	$num_obj_done += dbi_eachobject(
				-db=>$db,
				-cmd=>$cmd,
				%moreargs,
				-cmd1=>"exec sp_recompile",
				-setuser=>1,
				-no_exec=>$opt_n,
				-type=>"U",
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
	} else {
		$num_obj_done += dbi_eachobject(
				-db=>$db,
				-cmd=>"delete statistics",
				-cmd1=>$systable_cmd,
				#-cmd2=>"exec sp_recompile",
				-no_exec=>$opt_n,
				%moreargs,
				-type=>"S",
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);

   	$num_obj_done += dbi_eachobject(
				-db=>$db,
				-cmd=>"delete statistics",
				-cmd1=>$cmd,
				-cmd2=>"exec sp_recompile", %moreargs,
				-no_exec=>$opt_n,
				-setuser=>1,
				-type=>"U",
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
	}
	info "Completed Database $db at ",localtime(time)," [start=",localtime($tstart),"]\n";
	MlpLogOperationEnd('update_stats',$opt_S,$db,'',undef,"Num Tables=".$num_obj_done);
}

info "completed at ".localtime(time)."\n";
dbi_disconnect();
exit(0);

my(%V2, %V3, %V4, %V5);
sub mypr {
	my($tmp)=join("XX",@_);
#	print "RAW=$tmp";
	$_[0]=~s/^\t//;
	if( $IN_INDEX and $#_==8 ) {		# show_statistics messages
		if( defined $V2{$_[0]} ) {
			my($d)=int((1000*$V3{$_[0]})/($V2{$_[0]}+1)) / 1000.0;
			info( sprintf("%21.21s rw=%-9s smpl=%-8s stp=%-8s dnst=%-8s\n",	$_[0],$V2{$_[0]},$V3{$_[0]},$V4{$_[0]},$d));
			my($xs)='';
			my($v2,$v3,$v4,$v5)=($_[2],$_[3],$_[4],$_[5]);
			my($v5)=int((1000*$v3)/($v2+1)) / 1000.0;
			$v2='=' if $v2 eq $V2{$_[0]};
			$v3='=' if $v3 eq $V3{$_[0]};
			$v4='=' if $v4 eq $V4{$_[0]};
			$v5='=' if $v5 eq $d;
			info( sprintf("%21.21s    %-9s      %-8s     %-8s      %-8s\n",	"now=>",$v2,$v3,$v4,$v5));
		} else {
			$V2{$_[0]} = $_[2];	$V3{$_[0]} = $_[3];	$V4{$_[0]} = $_[4];	$V5{$_[0]} = $_[5];

		}
		return;
	}


		#info( localtime(time).":$db: ".join("\t",@_) )
			#unless $_[0] =~ /Each stored procedure and trigger that/ or $_[0] =~/17760: '/;
	#} else {
		info( localtime(time).":$db: ".join("\t",@_) )
			unless $_[0] =~ /Each stored procedure and trigger that/ or $_[0] =~/17760: '/
						or $_[0] =~ /DBCC SHOW_STATISTICS/;
	#}
}

sub mycr {	info( "CR",@_ ); }

__END__

=head1 NAME

update_stats.pl - update statistics on a database

=head2 AUTHOR

By Ed Barlow

=head2 DESCRIPTION

update statistics on all tables in db.  Also recompiles.  If Db Not Passed,
will work on all user databases in the server.  Pass -s if you want no output. Must pass -Usa -Pxxx
if on sql server even though that will, by default, use native authentication - ie. pass any string
in for the password - its not used - but is required.

=head2 USAGE

USAGE: update_stats.pl -UUSER -SSERVER -PPASS [-DDB]
   -p samplepct   adds 'with sampling = samplepct' (sybase 12.5.0.3+)
   -D DB_LIST     (pipe separated list with wildcards of databases)
   -E TABLES      (comma separated list of tables to EXCLUDE)
   -I TABLES      (comma separated list of tables to INCLUDE)
   -s             (silent mode)
   -k             keep old stats (ie no delete stats)
   -d             (debug mode)
   -vVAL                                number of steps/values to use (suggest 60 - dflt 20)
   -h             (html output)
   -i             update index stats
   -a                                   update all stats
   -n                           noexec
   -e errorlog    (optional)
   -l sessionlog  (optional)


=head2 ARGUMENTS

You can control pretty much everything here.  Will do a delete stats, update stats, and then an sp_recompile
on the object in question.

=cut
