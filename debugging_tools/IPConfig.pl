use strict;

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Win32::IPConfig;
use Repository;

my(@servers)=get_password(-type=>'pcdisks');
foreach my $host (@servers) {
    my $ipconfig = Win32::IPConfig->new($host)
        or {
        	warn "Unable to connect to $host\n";
        	next;
        }

    print "Host Name. . . . . . . . . . . . : ", $ipconfig->get_hostname, "\n";
    print "Domain Name (Primary). . . . . . : ", $ipconfig->get_domain, "\n";
    my @searchlist = $ipconfig->get_searchlist;
    print "Search List. . . . . . . . . . . : $searchlist[0]\n";
    print "                                   $searchlist[$_]\n"
        for (1..@searchlist-1);
    print "Node Type  . . . . . . . . . . . : ", $ipconfig->get_nodetype, "\n";
    print "IP Routing Enabled . . . . . . . : ",
        $ipconfig->is_router ? "Yes" : "No", "\n";
    print "WINS Proxy Enabled . . . . . . . : ",
        $ipconfig->is_wins_proxy ? "Yes" : "No", "\n";
    print "LMHOSTS Enabled. . . . . . . . . : ",
        $ipconfig->is_lmhosts_enabled ? "Yes" : "No", "\n";
    print "DNS Enabled for NetBT. . . . . . : ",
        $ipconfig->is_dns_enabled_for_netbt ? "Yes" : "No", "\n";

    for my $adapter ($ipconfig->get_configured_adapters) {
        print "\nAdapter '", $adapter->get_name, "':\n\n";
        print "DHCP Enabled . . . . . . . . . . : ",
            $adapter->is_dhcp_enabled ? "Yes" : "No", "\n";
        print "Domain Name. . . . . . . . . . . : ", $adapter->get_domain, "\n";

        my @ipaddresses = $adapter->get_ipaddresses;
        my @subnet_masks = $adapter->get_subnet_masks;
        for (0..@ipaddresses-1) {
            print "IP Address . . . . . . . . . . . : $ipaddresses[$_]\n";
            print "Subnet Mask. . . . . . . . . . . : $subnet_masks[$_]\n";
        }

        my @gateways = $adapter->get_gateways;
        print "Default Gateway. . . . . . . . . : $gateways[0]\n";
        print "                                   $gateways[$_]\n"
            for (1..@gateways-1);

        my @dns = $adapter->get_dns;
        print "DNS Servers. . . . . . . . . . . : $dns[0]\n";
        print "                                   $dns[$_]\n" for (1..@dns-1);

        my @wins = $adapter->get_wins;
        print "WINS Servers . . . . . . . . . . : $wins[0]\n";
        print "                                   $wins[$_]\n" for (1..@wins-1);
   }
}