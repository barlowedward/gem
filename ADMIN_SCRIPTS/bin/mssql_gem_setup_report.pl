#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use CommonFunc;
use Repository;
use DBIFunc;

use vars qw( $DEBUG $OUTFILE );

$|=1;
my($VERSION)="1.0";

sub usage {
	print @_,"\n";
	return "Usage: mssql_gem_setup_report.pl --DEBUG --OUTFILE=XXX";
}

sub output {
	print OUT @_ if $OUTFILE;
	print @_;
}

sub error_out {
   my($msg)=join("",@_);
   die $msg;
}

die usage("Bad Parameter List $!\n") unless
	GetOptions(	"DEBUG"=>\$DEBUG, "OUTFILE=s"=>\$OUTFILE );

if($OUTFILE) {
	open(OUT,">$OUTFILE) or die "CANT WRITE $OUTFILE $!\n";
}

output(  "AUTOMATICALLY DISCOVERING SQL SERVERS\n";
my($cmd)="osql -L";
open( CMD, $cmd."|" ) or die "Cant discover sql servers\n";
my(%servers);
my(%proclib_version);

my(%dsn_list);
eval {  DBI->data_sources("ODBC");	};
if( $@ ) {
    	output(  "ERROR: can not load DBD::ODBC : $@\n";
     	exit(1);
} else {
  	my(@dat)= grep(s/DBI:ODBC://,DBI->data_sources("ODBC"));
  	foreach (@dat) { $dsn_list{$_}=1;    	}
}

my($count)=0;
while(<CMD>){
	if( /: not found$/ ) {
		die "ERROR: Can not find osql in your path\n";
	}
	chomp;
	s/\s//g;
	next if /^\s*$/ or /^Servers:$/;
	$servers{$_}="UNREGISTERED";
	$count++;
}
output(  " Found $count SQL Servers\n";
output(  " UNREGISTERED => Server Exists But Not In GEM
 NO ODBC ENTRY => IN GEM BUT NO ODBC ENTRY
 CONNECT FAILURE	=> IN GEM BUT CAN NOT CONNECT
 OK			=> IN GEM AND CAN CONNECT\n\n";

foreach ( get_password(-type=>'sqlsvr') ) {
	if( $dsn_list{$_} ) {
		$servers{$_} = "REGISTERED";
	} else {
		$servers{$_} = "NO ODBC ENTRY";
	}
}

output(  "Testing Registered Servers\n";
foreach my $srv ( sort keys %servers) {
	my($login,$pass,$ctype);
	next unless $servers{$srv} eq "REGISTERED";
	output(  ".";

	($login,$pass,$ctype)=get_password(-type=>'sqlsvr',-name=>$srv);
	if( !$login or !$pass ) {
		$servers{$srv}="REGISTRATION FAILURE - no login or password found";
		next;
	}
   my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
	if( ! $rcx ) {
   	$servers{$srv}="CONNECT FAILURE";
		next;
	}
	$servers{$srv}="OK";

	my(@rc)=dbi_query(-db=>"master",-query=>"exec sp__proclib_version", -debug=>$DEBUG);

	foreach ( @rc ) {
		($proclib_version{$srv})=dbi_decode_row($_);
	}

	dbi_disconnect();
	# now get info on the server
}
output(  "\n";

output( sprintf "%-20s %-24s %s\n","Server","Registered/Unregistered","Proc Lib Ver" );
foreach my $srv ( sort keys %servers) {
	output( sprintf "%-20s %-24s %s\n",$srv,$servers{$srv},$proclib_version{$srv} );
}
#	my(@rc)=dbi_query(-db=>"master",-query=>"select name from master..sysobjects where name='sp_enumerrorlogs'", -debug=>$DEBUG);
#	foreach ( @rc ) {
#		my($nm)=dbi_decode_row($_);
#		if( $nm ne "sp_enumerrorlogs" ) {
#			error_out("Server $srv does not have sp_enumerrorlogs procedure in master\n");
#		}
#	}
#
#	# its the date of the second row that matters
#	my(@rc)=dbi_query(-db=>"master",-query=>"exec sp_enumerrorlogs", -debug=>$DEBUG);
#	my($num,$date,$filesize);
#	my($rowid)=0;
#	foreach ( @rc ) {
#		my(@x)=dbi_decode_row($_);
#		if( $rowid == 0 ) {
#			$num		= $x[0];
#			error_out("$srv: Hmmm for some reason - sp_enumerrorlogs did not return arcive 0 as the first row\n")
#				if $num!=0;
#			$filesize	= $x[2];
#			$rowid++;
#		} elsif( $rowid == 1 ) {
#			$date	= $x[1];
#			last;
#		}
#	}
#
#	my(@rc)=dbi_query(-db=>"master",-query=>"select datediff(dd,'".$date."',getdate())", -debug=>$DEBUG);
#	my($num_days);
#	foreach ( @rc ) {
#		($num_days)=dbi_decode_row($_);
#	}
#
#	printf( "%-12s DATE=$date SIZE=%-12d DAYS=%d\n",$srv,$filesize,$num_days );
#
#	if( $num_days>$DAYS ) {	# purge
#		dbi_msg_exclude(24000);
#		dbi_msg_ok('01000');
#		print "*** Executing sp_cycle_errorlog on $srv\n";
#		my(@rc)=dbi_query(-db=>"master",-query=>"sp_cycle_errorlog", -debug=>$DEBUG);
#		foreach ( @rc ) {
#			my($str)=join(" ",dbi_decode_row($_));
#			next if $str=~/^\s*$/;
#			print "$srv: WARNING: ",$str,"\n";
#		}
#	}
#};

output(  "\nCompleted processing at ".localtime(time)."\n";

exit(0);

__END__

=head1 NAME

mssql_gem_setup_report.pl - analyze your sql servers and produce registration report

=head2 DESCRIPTION

Discovers your configuration based on interfaces and osql -L and then
it will produce a report detailing what is registered and the server
status.

=cut
