#
# TEMPLATE IS MY STANDARD MODULE FOR GEM TEMPLATE
#
use strict;
use lib qw(lib);
use Getopt::Long;
use CommonFunc;
use Repository;
use DBIFunc;
use RunOn;
use JobScheduler;
use MlpAlarm;
use Carp qw(confess cluck);

my($PROGRAM_DESCRIPTION)="Create GEM Batch Scripts";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG );

die "Bad Parameter List $!\nThe Only argument allowed is --DEBUG" unless GetOptions( "DEBUG"	=> \$DEBUG );

print "$0 - v",$VERSION,"\n";
print "Run at ".localtime()."\n";
print "   Reading gem.dat\n";

my($GemConfig)=read_gemconfig(-debug=>$DEBUG);
# START BODY
my(%TYPEMAP)=(
	'sybase' => 'unix',
	'oracle' => 'win32',
	'mysql'  => 'unix',
	'sqlsvr' => 'win32',
);

use Cwd;
use File::Basename;

print "create_batch_scripts.pl v1.0\n\n";

print "THIS PROGRAM WILL CREATE WINDOWS BATCH SCRIPTS\n" if 	 $^O =~ /Win32/i;
print "THIS PROGRAM WILL CREATE UNIX BATCH SCRIPTS\n" 	unless $^O =~ /Win32/i;

my($GEM_ROOT_DIR) = get_gem_root_dir();
print "GEM ROOT DIRECTORY=$GEM_ROOT_DIR\n";
my($NT_CODE_FULLSPEC) = $GemConfig->{NT_CODE_LOCATION};
$NT_CODE_FULLSPEC=find_fullspec($GemConfig->{NT_CODE_LOCATION}) if $^O =~ /Win32/i;
print "NT CODE FULLPATH SPEC=$NT_CODE_FULLSPEC\n";

#my  $curdir=dirname($0);
#die "You Must Map Samba Shares To A Drive Letter.\nCurdir=$curdir\n" if $^O =~ /MSWin32/ and $curdir =~ /\\\\/;
#chdir($curdir) or die "Cant cd to $curdir: $!\n";

#my($GEM_ROOT_DIR)=cwd();
#die "ERROR : must run from an appropriate directory\n" unless -d "$GEM_ROOT_DIR/lib" and -d "$GEM_ROOT_DIR/conf";

print "Fetching Scheduling Information\n";
my($job_scheduler) = fetch_jobscheduler();			# ALARM SCHEDULE DATA

#use Data::Dumper;
#print Dumper $job_scheduler;

# this hash is populated by makefile() when you create scheduled jobs - for later
#
# firstly we :
#   dont schedule if USE_SYBASE_ASE=N and name=~/^Syb/ or /Sybase/
#   dont schedule if USE_SQLSERVER=N and name=~/mssql/
#   dont schedule if USE_ORACLE=N and name=~/^Oracle/
#   dont schedule if USE_SYBASE_ASE=N and name=~/^Syb/ or /Sybase/
#
#  MlpSetPL(monitor_program=>$base,	use_system=>'Y',rec_frequency_str=>$recommended_schedule,job_type=>$job_type ) 
#         unless $job_type eq "backup"	or $base eq "AnalyzeDepends" or $base =~ /Init$/;
#
#  passed in from makefile()
# $$job_scheduler{$job_name}->{frequency};   hourly weekly never  
#															daily-10pm daily-Midnight daily-2am daily-4am daily-7am daily-8am daily-8pm
#                                            (fixed for daily-\d+pm and daily-\d+am
#															Every15Min Every10Min 
#															saturday-am sunday-am
#
# $$job_scheduler{$job_name}->{run_type} 	== Win32 Unix
# $$job_scheduler{$job_name}->{enabled}  	== "NO" "YES"  (by dflt YES - no if job_type==backup)
# $$job_scheduler{$job_name}->{scheduled_on}	== hostname();
# $$job_scheduler{$job_name}->{job_type} 	== "monitoring" reporting backup
# $$job_scheduler{$job_name}->{ok_types} 	== unix both nt
#
# values not really used
# $$job_scheduler{$job_name}->{command} 	
# $$job_scheduler{$job_name}->{noredirect} 	
# $$job_scheduler{$job_name}->{base} 		== job name
#

print "Creating Batch Job Information\n";
do_install_create_batchjobs();

print "Saving Scheduler Information\n";
save_jobscheduler($job_scheduler);

# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "SUCCESSFUL: $PROGRAM_DESCRIPTION\n";
close_gemconfig();

exit(0);

sub l_warning {
	my(@x)=$_;
	print "\n";
	cluck  "warning:", @x;
	print "\n";
	print "warning:", @x;
	print "\n";
}

# remove all stuff from the directories
sub clear_directory {
	my($dir)=@_;
	print "Clearing Directory $dir\n";
	opendir(DIR,$dir) || die( "reformat() Can't open directory $dir for reading\n" );
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	foreach ( @dirlist ) {
		l_warning "Warning Found File $_ in directory $dir - This shouldnt happen - did you put that file there?\n" 
			unless /\.bat$/ or /\.ksh$/;
		unlink "$dir/$_" or print "Error unlinking $dir/$_\n";
	}
	closedir(DIR);
	print " -- Cleared\n";
}

sub do_install_create_batchjobs {
	__statusmsg("[configure] building scripts\n");
#	$NEEDS_REFRESH{Scheduler}=1;

	__debugmsg(  "[configure] NT_PERL_LOCATION  = $GemConfig->{NT_PERL_LOCATION} \n" );
	__debugmsg(  "[configure] NT_CODE_LOCATION  = $NT_CODE_FULLSPEC \n" );
	__debugmsg(  "[configure] UNIX_PERL_LOCATION= $GemConfig->{UNIX_PERL_LOCATION} \n" );
	__debugmsg(  "[configure] UNIX_CODE_LOCATION= $GemConfig->{UNIX_CODE_LOCATION} \n" );

	# delete $$job_scheduler{""} if defined $job_scheduler{""} and defined $$job_scheduler{""};
	
	die "NO CHECKSERVER MAILTO - PLEASE SET THIS TO NA IF YOU DO NOT WISH IT TO BE SET!\n"
		unless $GemConfig->{CHECKSERVER_MAILTO};
		
	my($maddress)="--MAILTO=\"".$GemConfig->{CHECKSERVER_MAILTO}."\"";
	$maddress="" if $GemConfig->{CHECKSERVER_MAILTO} eq "NA";
	
	MlpClearPL();

   if( $^O =~ /Win32/i ) {
		clear_directory( "$GEM_ROOT_DIR/win32_batch_scripts/interactive" );
		clear_directory( "$GEM_ROOT_DIR/win32_batch_scripts/batch" );
		clear_directory( "$GEM_ROOT_DIR/win32_batch_scripts/plan_interactive" );
		clear_directory( "$GEM_ROOT_DIR/win32_batch_scripts/plan_batch" );
	} else {
		clear_directory( "$GEM_ROOT_DIR/unix_batch_scripts/interactive" );
		clear_directory( "$GEM_ROOT_DIR/unix_batch_scripts/batch" );
		clear_directory( "$GEM_ROOT_DIR/unix_batch_scripts/plan_interactive" );
		clear_directory( "$GEM_ROOT_DIR/unix_batch_scripts/plan_batch" );
	}
	
	# REMOVE EXISTING ALL JOB FILES!
	my($old_job_scheduler)=$job_scheduler;
	my(%d);
	$job_scheduler=\%d;

	makefile("Oracle_CheckPersistent","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckOracle.pl --BATCH_ID=Oracle_CheckPersistent --PERSIST",
		1, "daily-2am","monitoring");		
	makefile("Sybase_CheckPersistent","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServer.pl --BATCH_ID=Sybase_CheckPersistent --PERSIST",
		1, "daily-2am","monitoring");		
	makefile("Mssql_CheckPersistent","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServer.pl --BATCH_ID=Mssql_CheckPersistent --PERSIST",
		1, "daily-2am","monitoring");		
	makefile("Sys_MailGemErrors","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\MailGemErrors.pl",
		undef, "daily-2am","monitoring");		

	# Diagnostic reports in the byserver section...
	#makefile("Sybase_DailyCheck","NO","YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\document_all.pl -REPORT=25 -DO_SYBASE -BATCHID=SybDailyCheck --QUICK --NATIVE",
	#	undef, "daily-2am","monitoring");		
	#makefile("Mssql_DailyCheck","YES","NO",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\document_all.pl -REPORT=25 -DO_SQLSVR -BATCHID=MssqlDailyCheck --QUICK --NATIVE",
	#	undef, "daily-4am","monitoring");
	makefile("Mssql_ClusterManager","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\cluster_manager.pl  --BATCH_ID=MssqlClusterManager",
		undef, "hourly","monitoring");
	
	makefile("Mssql_UpdateStatsMsdb","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\RunCommandOnSystems.pl --DOALL=sqlsvr --BATCH_ID=__BATCH_ID__ --COMMAND=\"__PERL__ __CODELOCATION__/ADMIN_SCRIPTS/dbi_backup_scripts/update_stats.pl -Dmsdb -Uxxx -Pxxx -SSERVERNAME\"",
		undef, "saturday-am","monitoring");
		
	makefile("Mssql_FixOrphanUsers","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_fix_orphan_users.pl --BATCH_ID=__BATCH_ID__",
		undef, "weekly","monitoring");
	makefile("Mssql_ClusterManagerInit","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\cluster_manager.pl --INITIALIZE --BATCH_ID=MssqlClusterManagerInit",
		undef, "weekly","monitoring");
	makefile("Sybase_LockedLoginDetector","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\LockedLoginDetector.pl --DOALL=SYBASE --BATCH_ID=Sybase_LockedLoginDetector",
		undef, "hourly","monitoring");
	
	#makefile("Sybase_aseDbDebug","YES","YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\document_all.pl --alarm -BATCHID=SybaseDbDebug --REPORT=25 --PRINTTIMING --Native --Alarm --DO_SYBASE\n",
	#	undef, "daily","monitoring");		
	#makefile("Mssql_DbDebug","YES","NO",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\document_all.pl --alarm -BATCHID=MssqlDbDebug --REPORT=25 --PRINTTIMING --Native --Alarm --DO_SQLSVR\n",
	#	undef, "daily","monitoring");

	
	# WE DUMP TRAN MASTER ONCE A WEEK CAUSE FILLING IT JUST IS YUCKY
	makefile("Sybase_TruncateMaster","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\allsrv_query_long.pl --TYPE=sybase --SHOWQUERIES --QUERY=\"dump tran master with no_log\"\n",
		undef, "daily-4am","monitoring");		
	makefile("Sybase_TruncateSybsystemdb","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\allsrv_query_long.pl --TYPE=sybase --SHOWQUERIES --QUERY=\"dump tran sybsystemdb with no_log\"\n",
		undef, "daily-4am","monitoring");		
	makefile("Sybase_TruncateSybsystemprocs","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\allsrv_query_long.pl --TYPE=sybase --SHOWQUERIES --QUERY=\"dump tran sybsystemprocs with no_log\"\n",
		undef, "daily-4am","monitoring");		
	makefile("Mssql_TruncateMaster","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\allsrv_query_long.pl --TYPE=sqlsvr --SHOWQUERIES --QUERY=\"dump tran master with no_log\"\n",
		undef, "daily-4am","monitoring");
	
	makefile("Sybase_StoredProcUpdater","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\procs\\configure.pl --UPGRADE_TO_LATEST --SYBSERVERS\n",
		undef, "daily-2am","monitoring");		
	makefile("Mssql_StoredProcUpdater","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\procs\\configure.pl --UPGRADE_TO_LATEST --SQLSERVERS\n",
		undef, "daily-2am","monitoring");	

	#makefile("Sybase_Check_Development","NO","YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServer.pl --BATCH_ID=SybCheck_Development -TIME=300 --DOALL=sybase --NOPRODUCTION\n",
	#	undef, "hourly","monitoring");
	#	
	#makefile("Mssql_Check_Development","YES","NO",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServer.pl --BATCH_ID=MssqlCheck_Development -TIME=300 --DOALL=sqlsvr --NOPRODUCTION\n",
	#	undef, "hourly","monitoring");
	
	makefile("Mssql_CheckServerDaily","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServerDaily.pl --DOALL=sqlsvr --BATCHID=__BATCHID__",
		undef, "daily-2am","monitoring");		
	makefile("Sybase_CheckServerDaily","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServerDaily.pl --DOALL=sybase --BATCHID=__BATCHID__",
		undef, "daily-2am","monitoring");			
				
	foreach my $svr ( get_password(-type=>'sybase') ) {
		my(%INFO)=get_password_info(-name=>$svr, -type=>'sybase');
	#foreach ( keys %server_info ) {
	#	next unless /:sqlsvr$/ or /:sybase$/;
	#	
	#	my($svr,$typ)=split(/:/,$_);

	#	# NEW - ONLY DO PRODUCTION/DR/CRITICAL SERVERS HERE
	#	next if $INFO{SERVER_TYPE} ne "PRODUCTION" 
	#		and  $INFO{SERVER_TYPE} ne "QA"
	#		and  $INFO{SERVER_TYPE} ne "CRITICAL"
	#		and  $svr ne $GemConfig->{ALARM_SERVER};
	#	next if $svr eq $GemConfig->{ALARM_SERVER};
			
		my($time)=300;
		$time=60 if $INFO{SERVER_TYPE} eq "PRODUCTION" or $INFO{SERVER_TYPE} eq "CRITICAL";
		$time=5  if $svr eq $GemConfig->{ALARM_SERVER};

		my($frequency)="Every15Min";
	#	$frequency="hourly" if $INFO{SERVER_TYPE} ne "PRODUCTION"
	#		and  $INFO{SERVER_TYPE} ne "CRITICAL";

		my($m)="";
		$m=$maddress if $INFO{SERVER_TYPE} eq "PRODUCTION" 
							or $INFO{SERVER_TYPE} eq "CRITICAL"
							or $svr eq $GemConfig->{ALARM_SERVER};							
		
		l_die("ERROR: Unidentified server name $svr") unless defined $svr;
	#	l_die("ERROR: Unidentified server type $svr") unless defined $typ;

	#	if( $typ eq "sqlsvr" ) {
	#		makefile("CheckServer_".$svr,"YES","NO",
	#			"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServer.pl --BATCH_ID=CheckServer_$svr -TIME=$time --SERVER=$svr $m\n",
	#			undef, $frequency,"monitoring");
	#	} else {
	
			# as this is sybase! - look that up
			#my $run_type=ucfirst($TYPEMAP{sybase});
			#my $usent   = "NO";
			#my $useunix = "NO";
			# $usent      = "YES" if $run_type = "win32";
			#$useunix    = "YES" if $run_type = "unix";
			
			makefile("Sybase_CheckServer_".$svr,undef,undef,
				"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServer.pl --BATCH_ID=CheckServer_$svr -TIME=$time --SERVER=$svr $m\n",
				undef, $frequency,"monitoring");
	#	}	
	}
	
	foreach my $svr ( get_password(-type=>'sqlsvr') ) {
		my(%INFO)=get_password_info(-name=>$svr, -type=>'sqlsvr');
	#foreach ( keys %server_info ) {
	#	next unless /:sqlsvr$/ or /:sybase$/;
	#	
	#	my($svr,$typ)=split(/:/,$_);

	#	# NEW - ONLY DO PRODUCTION/DR/CRITICAL SERVERS HERE
	#	next if $INFO{SERVER_TYPE} ne "PRODUCTION" 
	#		and  $INFO{SERVER_TYPE} ne "QA"
	#		and  $INFO{SERVER_TYPE} ne "CRITICAL"
	#		and  $svr ne $GemConfig->{ALARM_SERVER};
	#	next if $svr eq $GemConfig->{ALARM_SERVER};
			
		my($time)=300;
		$time=60 if $INFO{SERVER_TYPE} eq "PRODUCTION" or $INFO{SERVER_TYPE} eq "CRITICAL";
		$time=5  if $svr eq $GemConfig->{ALARM_SERVER};

		my($frequency)="hourly";
		$frequency="Every15Min"  if $INFO{SERVER_TYPE} eq "PRODUCTION" or $INFO{SERVER_TYPE} eq "CRITICAL";
		
		# print ">>> Check SERVER FREQ for $svr IS $frequency $INFO{SERVER_TYPE}\n";
	
		my($m)="";
		$m=$maddress if $INFO{SERVER_TYPE} eq "PRODUCTION" 
							or $INFO{SERVER_TYPE} eq "CRITICAL"
							or $svr eq $GemConfig->{ALARM_SERVER};							
		
		l_die("ERROR: Unidentified server name $svr") unless defined $svr;

		makefile("Mssql_CheckServer_".$svr,"YES","NO",
				"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CheckServer.pl --BATCH_ID=CheckServer_$svr -TIME=$time --SERVER=$svr $m\n",
				undef, $frequency,"monitoring");
	}

	#my($logdir)="$GemConfig->{UNIX_CODE_LOCATION}/data/GEM_BATCHJOB_LOGS";
	#if( ! -d $logdir ) {
	#	my($msg)="ERROR - NO LOG DIRECTORY $logdir\nBe sure To check the directory structure\n";
	#	$CURRENT_CONFIG{mainWindow}->messageBox( -title => "Validation Error", -message => $msg, -type => "OK" );
	#	return "ERR";
	# 

	mkdir($GEM_ROOT_DIR."/win32_batch_scripts",0775) unless -d $GEM_ROOT_DIR."/win32_batch_scripts";
	chmod 0775,$GEM_ROOT_DIR."/win32_batch_scripts";

	mkdir($GEM_ROOT_DIR."/unix_batch_scripts",0775) unless -d $GEM_ROOT_DIR."/unix_batch_scripts";
	chmod 0775,$GEM_ROOT_DIR."/unix_batch_scripts";

	# create some .bat scripts for NT based on:	
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\rdist\\putget_file.pl --BATCHID=__BATCHID__ --SILENTMODE --TIMEOUT=50 --TESTMODE --TESTMODEUPDATE --HTML --TESTMODERPT=__CODELOCATION__\\\\data\\html_output\\Sys_TestDfltConnectivity.html",
	makefile("Sys_TestDfltConnectivity","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\TestDfltUnixConnectivity.pl --BATCHID=__BATCHID__ ",
		undef,"hourly","monitoring");	
#	makefile("Sys_GemIsUp","YES","YES",
#		"__PERL__ -I__CODELOCATION__/lib __CODELOCATION__\\bin\\test_mail.pl --BATCHID=__BATCHID__ ",
#		undef,"hourly","monitoring");	

#
# INVENTORY MANAGEMENT STUFF
#
	makefile("Unix_INV_GetEtcPasswd","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_get_etcpasswd.pl --BATCHID=__BATCHID__",
		undef,"daily","inventory");
	makefile("Unix_INV_YpcatExtractor","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_unix_ypcat_extractor.pl --BATCHID=__BATCHID__",
		undef,"daily","inventory");
	makefile("Sybase_INV_DatabaseAuditor","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_database_auditor.pl --DOALL=sybase --BATCHID=__BATCHID__",
		undef,"daily","inventory");
	makefile("Mysql_INV_DatabaseAuditor","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_database_auditor.pl --DOALL=mysql --BATCHID=__BATCHID__",
		undef,"daily","inventory");
	#makefile("Unix_INV_LoadYpcatHosts","NO","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_load_ypcat_hosts.pl --BATCHID=__BATCHID__",
	#	undef,"daily","inventory");	
	
	makefile("Win32_INV_DsgetComputer","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_dsget_computer.pl --RUNADFIND --BATCHID=__BATCHID__",
		undef,"daily","inventory");
	makefile("Mssql_INV_DatabaseAuditor","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_database_auditor.pl --DOALL=sqlsvr --BATCHID=__BATCHID__",
		undef,"daily","inventory");
	makefile("Oracle_INV_DatabaseAuditor","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_database_auditor.pl --DOALL=oracle --BATCHID=__BATCHID__",
		undef,"daily","inventory");
	makefile("Win32_INV_GetSystemUsers","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_win_getsystemusers.pl --BATCH_ID=__BATCHID__",
		undef,"daily","inventory");
	makefile("Win32_INV_Dsget","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\inventory_manager\\INV_dsget.pl --BATCH_ID=__BATCHID__",
		undef,"daily","inventory");
	
	#my($optit)="-OPTDIAG=".$GemConfig->{OPTDIAG_PATH} if $GemConfig->{OPTDIAG_PATH} and $GemConfig->{OPTDIAG_PATH} !~ /^\s*$/;
	
	#makefile("AuditSybase","YES","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\dbi_backup_scripts\\config_report.pl $optit -BATCHID=AuditSybase -TYPE=sybase -NOBCP\n",
	#	undef, "saturday-am","monitoring" ) if $GemConfig->{USE_SYBASE_ASE} eq "Y";
	#makefile("AuditMssql","YES","NO",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\dbi_backup_scripts\\config_report.pl -BATCHID=AuditMssql -TYPE=sqlsvr -NOBCP\n",
	#	undef, "saturday-am","monitoring" ) if $GemConfig->{USE_SQLSERVER} eq "Y";
	
	makefile("Sybase_Auditor","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\dbi_backup_scripts\\config_report.pl --BATCHID=__BATCHID__ --TYPE=sybase --OUTDIR=__CODELOCATION__\\\\data\\server_audits",
		undef,"saturday-am","reporting");
	makefile("Mssql_Auditor","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\dbi_backup_scripts\\config_report.pl --BATCHID=__BATCHID__ --TYPE=sqlsvr --OUTDIR=__CODELOCATION__\\\\data\\server_audits",
		undef,"saturday-am","reporting");
	makefile("Win32_CleanupFiles","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CleanupFiles.pl --BATCHID=__BATCHID__",
		undef,"hourly","monitoring");
	makefile("Unix_CleanupFiles","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\CleanupFiles.pl --BATCHID=__BATCHID__",
		undef,"hourly","monitoring");
	#makefile("OptdiagAnalyze","NO","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\optdiag_analyze.pl --RUNALL --HTML --NOSTDOUT --OUTFILE=__CODELOCATION__\\\\data\\html_output\\OptdiagAnalyze.html",
	#	undef,"daily","monitoring");

	# archive any files > 5MB once per week
	# they will be date stamped and we will cat /dev/null over them!
#	makefile("Sys_LargeLogFileArchive","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl -BATCHID=Sys_LargeLogFileArchive -HTML -DOREPORT -PURGESIZE=5000000 -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sys_LargeLogFileArchive.html",
#		undef,"saturday-am","monitoring");

	makefile("Sybase_FetchLogFiles","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\fetch_log_files.pl --BATCHID=__BATCHID__ --FILETYPE=sybase ",
		undef,"hourly","monitoring");
	
	
#	makefile("Sybase_MonSvrConfig","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=MONITOR_SERVER -HTML -DOREPORT -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_MonSvrConfig.html",
#		undef,"daily","monitoring");
#	makefile("Sybase_MonSvrLogFetch","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=MONITOR_SERVER -HTML -DOFETCH -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_MonSvrLogFetch.html",
#		undef,"hourly","monitoring");
#	makefile("Sybase_MonSvrLogRpt","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=MONITOR_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_MonSvrLogRpt.html",
#		undef,"hourly","monitoring");
#
#	makefile("Sybase_BkpSvrConfig","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=BACKUP_SERVER -HTML -DOREPORT -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_BkpSvrConfig.html",
#		undef,"daily","monitoring");
#	makefile("Sybase_BkpSvrLogFetch","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=BACKUP_SERVER -HTML -DOFETCH -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_BkpSvrLogFetch.html",
#		undef,"hourly","monitoring");
	makefile("Sybase_BkpSvrLog","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\process_log_files.pl --BATCHID=__BATCHID__ -FILETYPE=ASE_BACKUP_SERVER -HTML -GEMREPORT",
 		undef,"hourly","monitoring");		
	makefile("Sybase_AseSvrLog","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\process_log_files.pl --BATCHID=__BATCHID__ --SAVE_TO_ALARMDB --GEMREPORT --TODAYYESTERDAYONLY --FILETYPE=ASE_SERVER_LOG -HTML",
		undef,"hourly","monitoring");

#	makefile("Sybase_BkpSvrLogRpt","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=BACKUP_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_BkpSvrLogRpt.html",
#		undef,"hourly","monitoring");

	#makefile("Sybase_BkpLogRpt","YES","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=BACKUP_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -OUTFILE=__CODELOCATION__\\\\data\\html_output\\SybBkpLogRpt_GemRpt.html",
	#	undef,"hourly","monitoring");

	# replaced SybaseBackupFileReport with SybBkpSvrConfig
	#makefile("Sybase_aseBackupFileReport","YES","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=BACKUP_SERVER -HTML -DOREPORT -OUTFILE=__CODELOCATION__\\\\data\\html_output\\SybaseBackupFileReport.html",
	#	undef,"daily","monitoring");

	# replaced GetBackupStateSybase with SybBkpSvrLogFetch
	#makefile("GetBackupStateSybase","YES","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=BACKUP_SERVER -HTML -DOFETCH -DOANALYSIS -OUTFILE=__CODELOCATION__\\\\data\\html_output\\GetBackupStateSybase.html",
	#	undef,"hourly","monitoring");


#	makefile("Sybase_HistSvrConfig","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=HISTORICAL_SERVER -HTML -DOREPORT -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_HistSvrConfig.html",
#		undef,"daily","monitoring");
#	makefile("Sybase_HistSvrLogFetch","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=HISTORICAL_SERVER -HTML -DOFETCH -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_HistSvrLogFetch.html",
#		undef,"hourly","monitoring");
#	makefile("Sybase_HistSvrLogRpt","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=HISTORICAL_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_HistSvrLogRpt.html",
#		undef,"hourly","monitoring");
#
#	makefile("Sybase_RepSvrConfig","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=REP_SERVER -HTML -DOREPORT -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_RepSvrConfig.html",
#		undef,"daily","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
#	makefile("Sybase_RepSvrLogFetch","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=REP_SERVER -HTML -DOFETCH -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_RepSvrLogFetch.html",
#		undef,"hourly","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
#	makefile("Sybase_RepSvrLogRpt","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=REP_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_RepSvrLogRpt.html",
#		undef,"hourly","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
#	makefile("Sybase_RepSvrSaveAlarms","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=REP_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -SAVE_TO_ALARMDB -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_RepSvrSaveAlarms.html",
#		undef,"hourly","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
#
#	makefile("Sybase_ErrLogConfig","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=ASE_SERVER -HTML -DOREPORT -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_ErrLogConfig.html",
#		undef,"daily","monitoring");
#	makefile("Sybase_ErrLogFetch","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=ASE_SERVER -HTML -DOFETCH -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_ErrLogFetch.html",
#		undef,"hourly","monitoring");
#	makefile("Sybase_ErrLogRpt","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=ASE_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_ErrLogRpt.html",
#		undef,"hourly","monitoring");
#	makefile("Sybase_ErrLogAllRpt","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=ASE_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -DOFETCH -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_ErrLogAllRpt.html",
#		undef,"hourly","monitoring");
#	makefile("Sybase_ErrLogSaveAlarms","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\gem_file_manager.pl --BATCHID=__BATCHID__ -FILETYPE=ASE_SERVER -HTML -DOANALYSIS -TODAYYESTERDAYONLY -SAVE_TO_ALARMDB -OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_ErrLogSaveAlarms.html",
#		undef,"hourly","monitoring");

	makefile("Win32_CreateBackupDirs","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\Win32_CreateBackupDirs.pl --BATCH_ID=__BATCHID__ ",
		undef,"saturday-am","monitoring");
	makefile("Win32_CheckBackupDirs","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\Win32_CheckBackupDirs.pl --BATCH_ID=__BATCHID__ ",
		undef,"saturday-pm","monitoring");
	makefile("Win32_FileCleanup","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\purge_files.pl --html --reportfile=__CODELOCATION__\\\\data\\html_output\\Win32_FileCleanup.html --BATCH_ID=Win32FileCleanup --ctlfile=__CODELOCATION__\\\\conf\\win32_cleanup.dat",
		undef,"daily","monitoring");
	makefile("Win32_FileCheck","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\purge_files.pl --html --reportfile=__CODELOCATION__\\\\data\\html_output\\Win32_FileCheck.html --BATCH_ID=Win32FileCheck --ctlfile=__CODELOCATION__\\\\conf\\win32_file_check.dat",
		undef,"daily-6am","monitoring");
	makefile("Unix_FileCleanup","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\purge_files.pl --html --reportfile=__CODELOCATION__\\\\data\\html_output\\UnixFileCleanup_GemRpt.html --BATCH_ID=UnixFileCleanup --ctlfile=__CODELOCATION__\\\\conf\\unix_cleanup.dat",
		undef,"daily","monitoring");
	makefile("Unix_FileCheck","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\purge_files.pl --html --reportfile=__CODELOCATION__\\\\data\\html_output\\Unix_FileCheck_GemRpt.html --BATCH_ID=Unix_FileCheck --ctlfile=__CODELOCATION__\\\\conf\\unix_file_check.dat",
		undef,"daily","monitoring");		
	makefile("Win32_TaskScheduler","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_get_file.pl --BATCH_ID=Win32TaskScheduler --INDIR=/winnt,/windows --INFILE=Schedlgu.txt --NEWLABEL=Win32_Job_Scheduler_Log -OUTDIR=__CODELOCATION__\\\\data\\system_information_data\\schedlgu\n".
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\parse_schedlgu.pl -BWin32TaskScheduler -h -O__CODELOCATION__\\\\data\\html_output\\Win32_Scheduler.html -D__CODELOCATION__\\\\data\\system_information_data\\schedlgu\n",
		undef,"hourly","monitoring");
	makefile("Win32_GetHosts","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_get_file.pl --BATCH_ID=Win32_GetHosts --INDIR=/winnt/system32/drivers/etc,/windows/system32/drivers/etc,/windows,/winnt --INFILE=hosts -OUTDIR=__CODELOCATION__\\\\data\\system_information_data\\hosts\n",
		undef,"daily","monitoring");
	makefile("Win32_HostsRpt","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\parse_hosts.pl -BWin32HostsRpt -h -O__CODELOCATION__\\\\data\\html_output\\Win32_Hosts_Report.html -D__CODELOCATION__\\\\data\\system_information_data\\hosts\n",
		undef,"daily","monitoring");
		
	makefile("Mssql_CycleEventLogs","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_cycle_eventlog.pl --DAYS=7 --BATCHID=__BATCHID__\n",
		undef,"daily","monitoring");
#	makefile("Mssql_BackupHistoryRpt","YES","NO",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\OneQueryPerServerReport.pl -TYPE=sqlsvr --QUERYFILE=\"__CODELOCATION__\\ADMIN_SCRIPTS\\CONSOLE\\sqlsvr_backup_query.sql\" --BATCHID=__BATCHID__ --NODUPS --NOSERVERPRINT --HTML --OUTFILE=__CODELOCATION__\\data\\html_output\\MssqlBackupHistory.html",		
#		undef,"daily","reporting");
	makefile("Mssql_Shrinklogs","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_shrinklogs.pl --BATCHID=__BATCHID__ --DOSHRINK --SYSTEMS=ALL\n",
		undef,"nightly","monitoring");
	#makefile("Mssql_ShrinkDatabases","YES","NO",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_shrinklogs.pl --BATCHID=__BATCHID__ --DATATOO --DOSHRINK --SYSTEMS=ALL\n",
	#	undef,"saturday-am","monitoring");
	makefile("Mssql_ScheduledJobRpt","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_scheduled_job_rpt.pl --HTMLFILE=__CODELOCATION__\\\\data\\html_output\\MssqlScheduledJobRpt.html \n",
		undef,"daily","monitoring");
	makefile("Win32_Eventlog","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_eventlog.pl --HOURS=2 --LOGEVENTS --CONFIGFILE=__CODELOCATION__/data/win32_eventlog.dat --NOINFOMSGS --BATCHNAME=Win32Eventlog\n",
		undef,"Every15Min","monitoring" );
	#makefile("Win32_DiskUpdater","YES","NO",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_diskspace.pl --CREATEDAT --BATCHNAME=Win32DiskUpdater\n",
	#	undef,"saturday-am","monitoring");
	makefile("Win32_Diskspace","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_diskspace.pl --ALARMTHRESH=95 --WARNTHRESH=90  --LOGALARM --OUTFILE=__CODELOCATION__/data/html_output/Win32Diskspace.txt --THRESHFILE=__CODELOCATION__\\conf\\threshold_overrides.dat --BATCHNAME=Win32Diskspace\n",
		undef,"Every15Min","monitoring");
	makefile("Win32_ServiceCheckerUpdate","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_service.pl --ACTION=SNAPSHOTUPDATE --BATCHID=__BATCHID__ \n",
		"NOREDIRECT","daily-6am","monitoring" );
	#makefile("Win32_ServiceCheckerReInit","YES","NO",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_service.pl --ACTION=SNAPSHOT --BATCHID=__BATCHID__ \n",
	#	"NOREDIRECT","weekly","monitoring" );
	makefile("Win32_ServiceChecker","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_service.pl --ACTION=VALIDATE --BATCHID=__BATCHID__ \n",
		undef, "hourly","monitoring" );
	makefile("Mssql_JobStatusRpt","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_jobstatus_rpt.pl --NOSTDOUT --HTML --OUTFILE=__CODELOCATION__\\\\data\\html_output\\mssql_jobstatus_rpt.html\n",
		undef, "daily","monitoring" );
	makefile("Mssql_LogMaintPlan","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_logmaintplan.pl -CONFIGFILE=__CODELOCATION__\\data\\logmaintplan.dat \n",
		undef, "hourly","monitoring");
	makefile("Sybase_Ping","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\ping_server.pl -BATCH_ID=SybPing -TYPE=SYBASE\n",
		undef, "Every15Min","monitoring" );
	makefile("Mssql_Ping","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\ping_server.pl -BATCH_ID=MssqlPing -TYPE=SQLSVR\n",
		undef, "Every15Min","monitoring" );
	makefile("Unix_Ping","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\ping_systems.pl --BATCHID=__BATCHID__ --TYPE=unix\n",
		undef, "Every15Min","monitoring" );
	makefile("Sybase_ThresholdManager","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\threshold_manager.pl --BATCHID=__BATCHID__ --HTML --ACTION=REPORT --THRESHFILE=__CODELOCATION__\\conf\\threshold_overrides.dat --OUTFILE=__CODELOCATION__\\\\data\\html_output\\Sybase_Threshold_Report.html\n",
		undef, "Every15Min","monitoring" );
		
	makefile("Sybase_ThresholdManagerReinstall","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\threshold_manager.pl --BATCHID=__BATCHID__ --HTML --ACTION=INSTALL\n",
		undef, "saturday-am","monitoring" );

	makefile("Win32_Ping","YES","NO",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\ping_systems.pl --BATCHID=__BATCHID__ --TYPE=win32 --SLEEPTIME=60 --MAXTIME=14 --ITERATIONS=12\n",
		undef, "Every15Min","monitoring" );
	makefile("Sys_SyncConfigfilesAndDb","YES","YES",
		"__PERL__ __CODELOCATION__\\bin\\sync_configfiles_and_db.pl\n",
		undef, "daily","monitoring" );
	#makefile("Sys_TraceRoute","YES","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\tracert.pl -BATCH_ID=TraceRoute\n",
	#	undef, "daily","monitoring" );
	makefile("Sybase_RepCompare","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\SybRepMonitor.pl --BATCHID=__BATCHID__ --ACTION=COMPARE --HTML --OUTFILE=__CODELOCATION__\\\\data\\html_output\\SybRepCompare.html\n",
		undef,"daily","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
	makefile("Sybase_RepMonReport","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\SybRepMonitor.pl --BATCHID=__BATCHID__ --ACTION=REPORT --HTML --OUTFILE=__CODELOCATION__\\\\data\\html_output\\SybRepMonReport.html\n",
		undef,"Every15Min","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
	makefile("Sybase_RepMonAgent","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\SybRepMonitor.pl --BATCHID=__BATCHID__ --ACTION=MONITOR --ITERATIONS=13 --FREQUENCY=60 --HTML --OUTFILE=__CODELOCATION__\\\\data\\html_output\\SybRepMonAgent.html\n",
		undef,"Every15Min","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
	makefile("Sybase_RepMonCheck","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\SybRepMonitor.pl --BATCHID=__BATCHID__ --ACTION=CHECK  --THRESHFILE=__CODELOCATION__\\conf\\threshold_overrides.dat --ALARM_MINS=15\n",
		undef,"Every15Min","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
	makefile("Sybase_RepServerChecker","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\check_sybase_repserver.pl --RUNALL --BATCHID=__BATCHID__\n",
		undef,"Every15Min","monitoring") if $GemConfig->{USE_SYBASEREP} eq "Y";
	makefile("Sys_DnsMonitor","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\dnsmonitor.pl --DOALARM",
		undef,"Every15Min","monitoring");
		
	#makefile("PurgeMsdbBackupLogs","YES","NO",
	#	"__PERL__ __CODELOCATION__/ADMIN_SCRIPTS/console/OneQueryPerServerReport.pl -TYPE=sqlsvr --NOTIMEOUT --QUERY=\"sp__DeleteBackupHistory\" --BATCHID=__BATCHID__",
	#	undef, "saturday-am","monitoring");
	
#
# SURVEY SCRIPTS
#
#	makefile("Win32_Survey","YES","NO",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\discover.pl -Twin32servers -BWin32Survey\n",
#		undef, "daily","reporting" );
#	makefile("Mssql_Survey","YES","NO",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\discover.pl -Tsqlsvr -BMssqlSurvey\n",
#		undef, "daily","reporting" );
#	makefile("OraSurvey","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\discover.pl -Toracle -BOraSurvey\n",
#		undef, "daily","reporting" );
#	makefile("Sybase_Survey","YES","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\discover.pl -Tsybase -BSybSurvey\n",
#		undef, "daily","reporting" );
#	makefile("UnixSurvey","NO","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\discover.pl -Tunix -BUnixSurvey\n",
#		undef, "daily","reporting" );
	makefile("Sys_PortMonitor","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\port_monitor.pl --LOGALARM --SLEEPTIME=120 --ITERATIONS=6\n",
		undef, "Every15Min","reporting" );
	
#	set_status_label(undef,"Local Script Creation Completed...");

#	if( $CURRENT_CONFIG{sqlsvr_list_ptr} ) {
#		foreach (@{$CURRENT_CONFIG{sqlsvr_list_ptr}}) {
			#my( $dathash ) = $server_info{$_.":sqlsvr"};
			#if( ! defined $dathash ) {
			#	__statusmsg(  "WARNING: Server $_ of type sqlsvr Has been Deleted\n" );
			#	next;
			#}
			#my( %dat ) = %$dathash;
			#my( %dat ) = %{$server_info{$_.":sqlsvr"}};
	
			#my($sqlsvr_bk_rpt_str)="__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_backupreport.pl ".
			#	" --SYSTEM=$_ --HOURS=72 --BATCHID=BackupRpt".$_.
			#	" --HTMLFILE=__CODELOCATION__\\data\\backup_reports\\win32_backup_".$_.".html --REPORT=backup ".
			#	" --HTMLDETAIL=__CODELOCATION__\\data\\backup_reports\\win32_backup_".$_."_details.html\n";
			#makefile("BackupRpt".$_,"YES","NO",$sqlsvr_bk_rpt_str,
			#	"Y", "daily","monitoring");
#		}
#	}


	my(@rc)=MlpAlarm::_querywithresults('select distinct ReportName from ReportArgs where keyname=\'Description\' and value like \'Standard%\'',1);
	if( $#rc >= 0 ) {
		print $#rc, "ROWS RETURNED\n";
		foreach (@rc) { 
			my($reportname)=dbi_decode_row($_);
			my($ra)="__PERL__ __CODELOCATION__/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl --BATCHID=__BATCHID__ --REPORT=$reportname --OUTFILE=__CODELOCATION__/data/html_output/$reportname.html\n";
			makefile("Sys_".$reportname,"YES","YES",$ra,"Y","daily","monitoring");
		}		
	}
	
#	# make a battery of MlpAlarm reports
#	foreach my $rr (	"Sybase_ASE","Unix_Errors","Unix_Warnings","Sybase_BackupServer",
#			"Sybase_RepServer","Sys_BlackoutReport","Sys_ProductionList","Sys_Backups",
#			"Sys_ProductionErrors","Sys_ProductionWarnings","Sys_Agent",	"Sys_BackupReport") {
#		my($aa)= "GEM_".$rr;
#		my($ra)="__PERL__ __CODELOCATION__/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl --BATCHID=__BATCHID__ --REPORT=$aa --OUTFILE=__CODELOCATION__/data/html_output/$rr.html\n";
#		makefile("Sys_".$rr,"YES","YES",$ra,"Y","hourly","monitoring");
#	}
#
#	foreach my $rr (	"Win32_Eventlog","Win32_Diskspace", "Win32_SecurityRpt") {
#		my($aa)="GEM_".$rr;
#		my($ra)="__PERL__ __CODELOCATION__/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl --BATCHID=__BATCHID__ --REPORT=$aa --OUTFILE=__CODELOCATION__/data/html_output/$rr.html\n";
#		makefile($rr."_Rpt","YES","NO",$ra,"Y","hourly","monitoring");
#	}

#	# ok now create some scripts for the backup stuff
#	foreach my $plan_name ( sort( plan_get() ) ) {
#
#		my($dsquery)= $CONFIG{$plan_name."_SERVER_NAME"} || $plan_name;
#		my($td);
#		my($usent,$useunix)=("YES","YES");
#		if( exists $server_info{$dsquery.":sqlsvr"} ){
#			$td="sqlsvr";
#			$useunix="NO";
#		} elsif( exists $server_info{$dsquery.":sybase"} ){
#			$td="sybase";
#		} else {
#			next;
#		}
#
#		makefile("Backup_".$plan_name,$usent,$useunix,
#			"__PERL__  __CODELOCATION__/ADMIN_SCRIPTS/dbi_backup_scripts/backup.pl -J$plan_name\n",
#			undef, "daily","backup");
#		makefile("Trandump_".$plan_name,$usent,$useunix,
#			"__PERL__  __CODELOCATION__/ADMIN_SCRIPTS/dbi_backup_scripts/backup.pl -J$plan_name -t\n",
#			undef, "hourly","backup");
#
#		# only for log shipping
#		next unless defined $CONFIG{$plan_name."_XFER_TO_SERVER"};
#		__debugmsg(  "Found Log Shipping On Plan Named $plan_name\n" );
#
#		my($todir)=$CONFIG{$plan_name."_XFER_TO_DIR"};
#		my($host)=$CONFIG{$plan_name."_XFER_TO_HOST"};
#		#my($server) = $CONFIG{$plan_name."_XFER_TO_SERVER"};
#
#		my( $l,$p,$t );
#
#		if( defined $host and $host !~ /^\s*$/ ) {
#			if( exists $server_info{$host.":win32servers"} ){
#				$t="win32servers";
#				$l=$server_info{$host.":win32servers"}{login};
#				$p=$server_info{$host.":win32servers"}{password};
#			} elsif( exists $server_info{$host.":unix"} ){
#				$t="unix";
#				$l=$server_info{$host.":unix"}{login};
#				$p=$server_info{$host.":unix"}{password};
#			} else {
#				__statusmsg(  "[configure] ERROR - CANT FIND HOST $host - it may have been deleted\n" );
#				next;
#			}
#		}
#
#		my($str2)="__PERL__  __CODELOCATION__/ADMIN_SCRIPTS/dbi_backup_scripts/load_all_tranlogs.pl ";
#		$str2.=" --JOB=$plan_name";
#
#		#if( $td eq "sqlsvr" ) {
#			# i think this is ok... default to dumps from my scripts not from sql server scheduler
#			#$str2.=" --MICROSOFT";
#			makefile("LoadTranlogs_".$plan_name,$usent,$useunix,$str2."\n",
#				undef, "never","backup");
#		#} else {
#		#	makefile("LoadTranlogs_".$plan_name,$usent,$useunix,$str2."\n",
#		#		undef, "never","backup");
#		#}
#
#		$str2="__PERL__  __CODELOCATION__/ADMIN_SCRIPTS/dbi_backup_scripts/fix_logship.pl -J$plan_name";
#		#$str2.="-S".$CONFIG{$plan_name."_CLIENT_PATH_TO_DIRECTORY"}.\\$plan_name\\logdumps -T".$CONFIG{$plan_name."_XFER_TO_DIR}." -ggunzip.exe ";
#		#if( $td eq "sqlsvr" ) {
#			makefile("FixLogship_".$plan_name,$usent,$useunix,$str2."\n",
#				undef, "never","backup");
#		#} else {
#		#	makefile("FixLogship_".$plan_name,"YES","YES",$str2."\n",
#		#		undef, "never","backup");
#		#}
#		#
#		#if( $td eq "sqlsvr" ) {
#		#my($sqlsvr_bk_rpt_str)="__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\win32_backupreport.pl ".
#		#	" --SYSTEM=".$server." --HOURS=72 --REPORT=restore --PRIMARY=".$dsquery." --BATCHID=RestoreRpt_".$plan_name.
#		#	" --HTMLFILE=__CODELOCATION__\\data\\backup_reports\\win32_logship_".$plan_name.".html ".
#		#	" --HTMLDETAIL=__CODELOCATION__\\data\\backup_reports\\win32_logship_".$plan_name."_details.html\n";
#		#makefile("RestoreRpt_".$plan_name,"YES","NO",$sqlsvr_bk_rpt_str,
#		#	"Y", "hourly","monitoring");
#		#}
#	}

#
# BACKUP CROSSCHECK RELATED BATCH JOBS
#
# 1x daily static data fetches
	makefile("Win32_BackupStateStaticInfo","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_UpdateStaticInfo.pl --TYPE=sqlsvr --BATCH_ID=__BATCHID__\n",
		undef, "daily","reporting");		
	makefile("Unix_BackupStateStaticInfo","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_UpdateStaticInfo.pl  --TYPE=sybase --BATCH_ID=__BATCHID__\n",		
		undef, "daily","reporting");				
# win32checks 
#	makefile("Mssql_BkpChk_Hourly","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -HOURS=24 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChk.txt --BATCHID=__BATCHID__\n",
#		undef, "hourly","monitoring");
#	makefile("Mssql_BkpChk_Morning","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -HOURS=24 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChk.txt --BATCHID=__BATCHID__\n",
#		undef, "daily-4am","monitoring");
#	makefile("Mssql_BkpChk_Evening","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -HOURS=24 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChk.txt --BATCHID=__BATCHID__\n",
#		undef, "daily-8pm","monitoring");
#	makefile("Mssql_BkpChk_Weekly","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -DAYS=7 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChkWeekly.txt --BATCHID=__BATCHID__\n",
#		undef, "saturday-am","monitoring");
#	makefile("Mssql_CheckVersion","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\mssql_checkversion.pl --BATCHID=__BATCHID__\n",
#		undef, "saturday-am","monitoring");
		
	makefile("Mssql_BkpChk_ByProc","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Proc.pl --DOALL=sqlsvr --BATCHID=__BATCHID__\n",
		undef, "Every15Min","monitoring");
	
	makefile("Mssql_BkpChk_Report","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Report.pl --SQLSVRONLY --ERROR_REPORT=__CODELOCATION__\\\\data\\html_output\\Mssql_BkpChk_Errors.html --ALL_REPORT=__CODELOCATION__\\\\data\\html_output\\Mssql_BkpChk_Report.html --HTML --BATCHID=__BATCHID__ --TRANHOURS=2\n",
		undef, "hourly","monitoring");
	makefile("Sybase_BkpCheck_Report","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Report.pl --SYBASEONLY --ERROR_REPORT=__CODELOCATION__\\\\data\\html_output\\Sybase_BkpCheck_Errors.html --ALL_REPORT=__CODELOCATION__\\\\data\\html_output\\Sybase_BkpCheck_Report.html --HTML --BATCHID=__BATCHID__ --TRANHOURS=2\n",
		undef, "hourly","monitoring");

#
# MISCELLANEOUS MAINTENANCE ROUTINES
#
	makefile("Sys_AlarmRoutingAgent","YES","YES","__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\MlpAlarmRoutingAgent.pl --SLEEPTIME=10 --MAXTIME=14\n",
		undef, "Every15Min","monitoring");
	makefile("Sys_AlarmDbCleanup","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\monitoring\\MlpAlarmCleanupDB.pl --DAYS=7\n",
		undef, "daily","monitoring" );
	#makefile("Mssql_AnalyzeDepends","YES","NO",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\depends_analyze.pl -R -Tsqlsvr\n",
	#	undef, "saturday-am","monitoring" );
	#makefile("Sybase_AnalyzeDepends","YES","YES",
	#	"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\depends_analyze.pl -R -Tsybase\n",
	#	undef, "saturday-am","monitoring" );

	#makefile("HourlyBackupReport",	"YES","YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Report.pl --NOSTDOUT --ERRORFILE=__CODELOCATION__\\data\\html_output\\MssqlBkpChkErrors.html --OUTFILE=__CODELOCATION__\\data\\html_output\\MssqlBkpChk.html --HTML --BATCHID=__BATCHID__ --TRANHOURS=2 \n",
	#	undef, "hourly","reporting");
	#makefile("HourlyProdBackupReport",	"YES","YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Report.pl --PRODUCTION_ONLY --NOSTDOUT --ERRORFILE=__CODELOCATION__\\data\\html_output\\ProdMssqlBkpChkErrors.html --OUTFILE=__CODELOCATION__\\data\\html_output\\ProdMssqlBkpChk.html --HTML --BATCHID=__BATCHID__ --TRANHOURS=2 \n",
	#	undef, "hourly","reporting");

	makefile("Sys_ConsoleArchiver","YES","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\ConsoleArchiver.pl --BATCHID=__BATCHID__",
		undef,"saturday-am","monitoring");

	makefile( "Unix_Console_15min", "NO", "YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --TENMINUTE --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS",
		undef, "Every15Min", "reporting" );
	makefile( "Unix_Console_Hourly", "NO", "YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --HOURLY --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS",
		undef, "hourly", "reporting" );
	makefile( "Unix_Console_Daily", "NO", "YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__  --DAILY --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS ",
		undef, "daily", "reporting" );
	makefile( "Unix_Console_Standalone", "NO", "YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__  --RUN_STANDALONE_REPORTS ",
		undef, "saturday-am", "reporting" );
	makefile( "Unix_Console_Weekly", "NO", "YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --WEEKLY --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS",
		undef, "saturday-am", "reporting" );	
	#makefile( "Unix_Console_Promote", "YES", "YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --PROMOTE --PRINTTIMING ",
	#	undef, "hourly", "reporting" );
	#makefile( "Unix_Console_Rollup", "YES", "YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --ROLLUP --PRINTTIMING ",
	#	undef, "daily-6am", "reporting" );
	
	makefile( "Win32_Console_15min", "YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --TENMINUTE --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS",
		undef, "Every15Min", "reporting" );
	makefile( "Win32_Console_Hourly", "YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --HOURLY --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS",
		undef, "hourly", "reporting" );
	makefile( "Win32_Console_Daily", "YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__  --DAILY --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS ",
		undef, "daily", "reporting" );
	makefile( "Win32_Console_Standalone", "YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__  --RUN_STANDALONE_REPORTS ",
		undef, "saturday-am", "reporting" );
	makefile( "Win32_Console_Weekly", "YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --WEEKLY --PRINTTIMING --RUN_DBQUERIES	--RUN_COMMANDS",
		undef, "saturday-am", "reporting" );	
	makefile( "Win32_Console_Promote", "YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --PROMOTE --PRINTTIMING ",
		undef, "hourly", "reporting" );	
	makefile( "Win32_Console_Rollup", "YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\console_v2.pl --BATCHID=__BATCHID__ --ROLLUP --PRINTTIMING ",
		undef, "daily-6am", "reporting" );	
	makefile("Unix_GetDiskSpace","NO","YES",
		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\unix_space_monitor_via_rsh.pl -Ah -BUnix_GetDiskSpace -O__CODELOCATION__\\\\data\\html_output\\Unix_GetDiskSpace.html\n",
		undef, "hourly" ,"monitoring");
#	makefile("Unix_GetDiskSpace","NO","YES",
#		"__PERL__ __CODELOCATION__\\ADMIN_SCRIPTS\\console\\document_all.pl -REPORT=14 -ALARM\n",
#		undef, "hourly" ,"monitoring");

	makefile("Sybase_SpaceMonitor5","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\space_monitor.pl --alarm -BATCHID=Sybase_SpaceMonitor -TYPE=sybase\n",
		undef, "daily-9pm","monitoring");
	makefile("Sybase_SpaceMonitor4","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\space_monitor.pl --alarm -BATCHID=Sybase_SpaceMonitor -TYPE=sybase\n",
		undef, "daily-4pm","monitoring");
	makefile("Sybase_SpaceMonitor3","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\space_monitor.pl --alarm -BATCHID=Sybase_SpaceMonitor -TYPE=sybase\n",
		undef, "daily-1pm","monitoring");
	makefile("Sybase_SpaceMonitor2","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\space_monitor.pl --alarm -BATCHID=Sybase_SpaceMonitor -TYPE=sybase\n",
		undef, "daily-9am","monitoring");
	makefile("Sybase_SpaceMonitor1","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\space_monitor.pl --alarm -BATCHID=Sybase_SpaceMonitor -TYPE=sybase\n",
		undef, "daily-5am","monitoring");
	makefile("Sybase_SpaceMonitorRW","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\space_monitor.pl --WRITEHISTORY -BATCHID=__BATCHID__ -TYPE=sybase\n",
		undef, "daily-2am","monitoring");
	makefile("Mssql_SpaceMonitor","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\console\\space_monitor.pl --alarm --WRITEHISTORY -BATCHID=__BATCHID__ -TYPE=sqlsvr\n",
		undef, "daily","monitoring");
	#makefile("UnixDRChecker","NO","YES",
	#	"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\DR_Checker.pl --BATCHID=__BATCHID__ --SYBASEONLY\n",
	#	undef, "daily","monitoring");
	makefile("Sys_DirectoryChecker","YES","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\directory_checker.pl --GEM --BATCHID=__BATCHID__\n",
		undef, "daily-6am","monitoring");
	makefile("Mssql_DrChecker","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\DR_Checker.pl --BATCHID=__BATCHID__ --TOGEMCONSOLE\n",
		undef, "daily","monitoring");
	
	# PUT JOB SCHEDULER BACK THE WAY IT WAS
#	foreach ( keys %$job_scheduler ) {
		
#		print "JOB SCHEDULE $_\n";
		
		# THIS CODE RESET VALUES TO WHAT THEY WERE - IGNORING THAT FEATURE
#		my($t1)='';
#		if( defined $$old_job_scheduler{$_}->{scheduled_on}
#			and        $$old_job_scheduler{$_}->{scheduled_on}!~/^\s*$/ ) {		
#			$$job_scheduler{$_}->{scheduled_on} 	= $$old_job_scheduler{$_}->{scheduled_on}
#			$t1.=" scheduled_on=", $$old_job_scheduler{$_}->{scheduled_on};
#		}
#		if( defined $$old_job_scheduler{$_}->{run_type}
#			and        $$old_job_scheduler{$_}->{run_type}!~/^\s*$/ ) {
#			$$job_scheduler{$_}->{run_type} 	= $$old_job_scheduler{$_}->{run_type}
#			$t1.=" run_type=", $$old_job_scheduler{$_}->{run_type};
#		}
#		
#		#next unless $$old_job_scheduler{$_};
#		#$$job_scheduler{$_}->{frequency} 	= $$old_job_scheduler{$_}->{frequency}
#		#	if defined $$old_job_scheduler{$_}->{frequency};
#		$$job_scheduler{$_}->{scheduled_on} 	= $$old_job_scheduler{$_}->{scheduled_on}
#			if defined $$old_job_scheduler{$_}->{scheduled_on}
#			and        $$old_job_scheduler{$_}->{scheduled_on}!~/^\s*$/;
#		
#		
#		my($t1)='';
#		if( defined $$old_job_scheduler{$_}->{scheduled_on}
#			and        $$old_job_scheduler{$_}->{scheduled_on}!~/^\s*$/ ) {
#				$t1.=" scheduled_on=", $$old_job_scheduler{$_}->{scheduled_on};
#		}
#		if( defined $$old_job_scheduler{$_}->{run_type}
#			and        $$old_job_scheduler{$_}->{run_type}!~/^\s*$/ ) {
#				$t1.=" run_type=", $$old_job_scheduler{$_}->{run_type};
#		}
#		print "Rewriting $_ $t1\n";
#		
		#$$job_scheduler{$_}->{enabled} 		= $$old_job_scheduler{$_}->{enabled}
		#	if defined $$old_job_scheduler{$_}->{enabled};
#	}

#	if( ! defined $NOLOGFILE ) {
#		__statusmsg( "[configure] DUMPING SCHEDULER TO LOG FILE $CURRENT_CONFIG{log_file}\n" );
#		print LOGFP "DUMPING SCHEDULER \n";
#		print LOGFP Dumper $job_scheduler;
#		print LOGFP "Done DUMP\n";
#	}

#	set_install_step( "DO_INSTALL_CREATE_BATCHJOBS" );
	# colorize_install_buttons();
#	$gem_data->get_set_data(-class=>'job_scheduler',-value=>$job_scheduler);
#	__statusmsg("[configure] Script Generation Completed\n");
# 	$mf->pageconfigure("Scheduler",-state=>'normal');
}

sub makefile {
	my($base,$usent,$useunix,$line,$noredirect,$recommended_schedule,$job_type,$batch_id_for_spawner)=@_;
	
	print "DBG: Makefile($base,$recommended_schedule)\n" if $DEBUG;
	print "DBG: OS=$^O \n"	if $DEBUG;
	print "DBG: INSTALLTYPE=$GemConfig->{INSTALLTYPE} \n"	if $DEBUG;
	print "DBG: UNIX_PERL_LOCATION=$GemConfig->{UNIX_PERL_LOCATION} \n"	if $DEBUG;
	
	# if batch_id_for_spawner, then the program creates a batch program with _SPAWN appended
	# and a regular daemon_spawner.pl driver that is actually run
	my($SPAWN_SUFFIX)='';
	if( $batch_id_for_spawner ) {
		$SPAWN_SUFFIX="_SPAWN"; 
		$batch_id_for_spawner = $GEM_ROOT_DIR."/bin/daemon_spawner.pl --COMMANDFILE=$base --BATCH_ID=$batch_id_for_spawner \n";
	}
	
	$noredirect=0 unless defined $noredirect;
	l_die("ERROR : base is undefined or null \n") if ! defined $base or $base eq "";

	# IS THIS AN OPTIONAL REPORT
	# $useunix="NO" 	if $GemConfig->{INSTALLTYPE} eq "WINDOWS";
	# $usent="NO" 	if $GemConfig->{INSTALLTYPE} eq "UNIX";
	print "DBG: useunix=$useunix \n"	if $DEBUG;

	my($is_prefix_ok)=0;
	$is_prefix_ok=1 if  $base=~/^mysql_/i;
	$is_prefix_ok=1 if  $base=~/^sybase_/i;
	$is_prefix_ok=1 if  $base=~/^Mssql_/i;
	$is_prefix_ok=1 if  $base=~/^oracle_/i;
	$is_prefix_ok=1 if  $base=~/^mysql_/i;
	$is_prefix_ok=1 if  $base=~/^sys_/i;
	$is_prefix_ok=1 if  $base=~/^Win32_/i;
	$is_prefix_ok=1 if  $base=~/^Unix_/i;
	die "INVALID PREFIX on $base\n" if $is_prefix_ok==0;
	
	my($is_ok_on_unix)=0;
	$is_ok_on_unix=1 if mysql_is_ok_on_unix() 	and $base=~/^mysql_/i;
	$is_ok_on_unix=1 if sybase_is_ok_on_unix() 	and $base=~/^sybase_/i;
	$is_ok_on_unix=1 if sqlsvr_is_ok_on_unix() 	and $base=~/^Mssql_/i;
	$is_ok_on_unix=1 if oracle_is_ok_on_unix() 	and $base=~/^oracle_/i;
	$is_ok_on_unix=1 if mysql_is_ok_on_unix() 	and $base=~/^mysql_/i;
	$is_ok_on_unix=1 if gemsys_is_ok_on_unix() 	and $base=~/^sys_/i;
	$is_ok_on_unix=0 if $base=~/^Win32_/i;
	$is_ok_on_unix=1 if $base=~/^Unix_/i;
	
	#__statusmsg("[configure] Not Setting Up This Job on Unix\n") 		if $is_ok_on_unix and ! is_nt();
	#__statusmsg("[configure] Not Setting Up This Job on Windows\n") 	if ! $is_ok_on_unix and ! is_nt();

#	if( $useunix eq "NO" and $usent  eq "NO"  ) {
#		__statusmsg("[configure] Not Creating Report $base As No Place To Run It\n");
#		return;
#	}
#	if( ! $useunix and ! $usent ) {
#		__statusmsg("[configure] Not Creating Report $base As No Place To Run It\n");
#		return;
#	}		
	if( $GemConfig->{USE_SQLSERVER} ne "Y" and $base =~ /mssql/i ){		
		__statusmsg("[configure] Not Creating SqlServer Report $base per configuration directives\n");
		return;		
	}	
	if( $GemConfig->{USE_SYBASE_ASE} ne "Y" and ($base=~/Sybase/) ){		
		__statusmsg("[configure] Not Creating Sybase Report $base per configuration directives\n");
		return;		
	}	
	
	if( $GemConfig->{USE_SYBASEREP} ne "Y" and $base=~/Sybase_Rep/ ){	
		__statusmsg("[configure] Not Creating Sybase Replication Report $base per configuration directives\n");
		return;		
	}	

	if( $GemConfig->{USE_ORACLE} ne "Y" and $base =~ /^Oracle/i ){		
		__statusmsg("[configure] Not Creating Oracle Report $base per configuration directives\n");
		return;		
	}
	
	print "DBG: Passed Initial Tests at Line ",__LINE__," \n"	if $DEBUG;
	die "DBGDBG: NO JOB TYPE at line 762" unless $job_type;

	use MlpAlarm;
	#my($plfreq)=$recommended_schedule;
	#$plfreq="frequently" if $plfreq =~ /Every/;
	MlpSetPL(
		monitor_program=>$base,
		use_system=>'Y',
		rec_frequency_str=>$recommended_schedule,
		job_type=>$job_type ) unless $job_type eq "backup" or $base eq "AnalyzeDepends" or $base =~ /Init$/;

	__statusmsg("[configure] Creating Batch Script $base\n");
	my(%hsh);
	$hsh{frequency}=$recommended_schedule;
	
	if( $is_ok_on_unix ) {
		$hsh{run_type}='Unix'; 
	} else {
		$hsh{run_type}='Win32';
	}			

	$hsh{enabled}="YES";
	$hsh{enabled}="NO" if $job_type eq "backup";
	$hsh{job_type}=$job_type;

	# Not used really but might as well keep them
	$hsh{command}=$line;
	$hsh{base}=$base;
	$hsh{noredirect}=$noredirect;
	$$job_scheduler{$base}=\%hsh;	
	
	print "DBG: base=$base ",__LINE__," \n"	if $DEBUG;

	#use Data::Dumper;
	#print Dumper \%hsh;	
	#print "\n";
	#}

	chomp $line;
	$line=~s/\s+$//;
	$line=~s./.\\.g;
	$line=~s.\\\\.\\.g;
	chomp $line;

	my($file_root)="";
	$file_root="plan_" if $job_type eq "backup";
	my($orig_line)=$line;

	#print "INSTALLTYPE=",$GemConfig->{INSTALLTYPE}," ",
	#	$GemConfig->{NT_PERL_LOCATION}," ",
	#	$NT_CODE_FULLSPEC,"\n";	

	# add space at end of this line or unix scripts created under NT fail
	if( $^O =~ /Win32/i and	
		# $usent eq "YES" and
		$hsh{run_type} eq 'Win32' and		
		$GemConfig->{INSTALLTYPE} ne "UNIX" and
		$GemConfig->{NT_PERL_LOCATION} !~ /^\s*$/ and
		$NT_CODE_FULLSPEC !~ /^\s*$/ ) {

		my($redirect)="";
		$redirect=" > __CODELOCATION__\\data\\GEM_BATCHJOB_LOGS\\Win_".$base.".log 2> __CODELOCATION__\\data\\GEM_BATCHJOB_LOGS\\Win_".$base.".err "
			unless $noredirect;

		# BATCH *.bat
		my($file)=$GEM_ROOT_DIR."/win32_batch_scripts/".$file_root."batch/$base".$SPAWN_SUFFIX.".bat";
		my($l)=$line.$redirect;
		$l=~s/__PERL__/$GemConfig->{NT_PERL_LOCATION}/g;
		$l=~s/__CODELOCATION__/$NT_CODE_FULLSPEC/g;
		$l=~s/__BATCHID__/$base/g;
		$l=~s.\/.\\.g;

		unlink($file) if -r $file;
		open( OUT87,">".$file) or return l_warning("Cant write $file : $!");
		print OUT87 "\@echo off\n";
		print OUT87 $l." \n";
		print OUT87 "exit \%ERRORLEVEL\%\n";
		close(OUT87);
		chmod( 0777, $file ) or print "Unable to set permissions on $file $!\n";
		
		if( $batch_id_for_spawner ) {
			my($f2)=$file;
			$f2 =~ s/$SPAWN_SUFFIX//;
			unlink($f2) if -r $f2;
			open( OUT27,">".$f2) or return l_warning("Cant write $f2 : $!");
			print OUT27 "\@echo off\n";
			print OUT27 $batch_id_for_spawner." \n";
			print OUT27 "exit \%ERRORLEVEL\%\n";
			close(OUT27);
			chmod( 0777, $f2 ) or print "Unable to set permissions on $f2 $!\n";			
		}
		
		# BATCH *.ksh
		$file=~s/\.bat$/.ksh/g;
		$l=$line.' $* '.$redirect;
		$l=~s/__PERL__/$GemConfig->{NT_PERL_LOCATION}/g;
		$l=~s/__CODELOCATION__/$NT_CODE_FULLSPEC/g;
		$l=~s/__BATCHID__/$base/g;
		$l=~s.\/.\\.g;
		$l=~s.\\.\/.g;

		unlink($file) if -r $file;
		open(  OUT86,">".$file) or return l_warning("Cant write $file : $!");
		print  OUT86 "$l\n";
		close( OUT86);
		chmod( 0777, $file ) or print "Unable to set permissions on $file $!\n";
		
		if( $batch_id_for_spawner ) {
			my($f2)=$file;
			$f2 =~ s/$SPAWN_SUFFIX//;
			unlink($f2) if -r $f2;
			open( OUT17,">".$f2) or return l_warning("Cant write $f2 : $!");
			print OUT17 $batch_id_for_spawner." \n";
			close(OUT17);
			chmod( 0777, $f2 ) or print "Unable to set permissions on $f2 $!\n";			
		}
		
		#INTERACTIVE *.bat
		$file=$GEM_ROOT_DIR."/win32_batch_scripts/".$file_root."interactive/$base.bat";
		$l=$orig_line;
		$l=~s/__PERL__/$GemConfig->{NT_PERL_LOCATION}/g;
		$l=~s/__CODELOCATION__/$NT_CODE_FULLSPEC/g;
		$l=~s/__BATCHID__/$base/g;
		$l=~s.\/.\\.g;
		unlink($file) if -r $file;

		open( OUT85,">".$file) or return l_warning("Cant write $file : $!");
		print OUT85 $l." \n";
		close(OUT85);

		chmod( 0777, $file ) or print "Unable to set permissions on $file $!\n";

		# INTERACTIVE *.ksh
		$file=~s/\.bat$/.ksh/g;
		$l=~s.\\.\/.g;
		open( OUT84,">".$file) or return l_warning("Cant write $file : $!");
		print OUT84 $l.' $*'." \n";
		close(OUT84);

		chmod( 0777, $file ) or print "Unable to set permissions on $file $!\n";
	} else {
		print "DBG: Not Win Scheduled ",__LINE__," \n"	if $DEBUG;
	}


	#print "INSTALLTYPE=",$GemConfig->{INSTALLTYPE}," ",
	#	$GemConfig->{UNIX_PERL_LOCATION}," ",
	#	$GemConfig->{UNIX_CODE_LOCATION},"\n";
		
	if(  $^O !~ /Win32/i and
		# $useunix eq "YES" and
		$hsh{run_type} eq 'Unix'	and
		
		$GemConfig->{INSTALLTYPE} ne "WINDOWS" and
		$GemConfig->{UNIX_PERL_LOCATION} !~ /^\s*$/ and
		$GemConfig->{UNIX_CODE_LOCATION} !~ /^\s*$/ ) {

		print "DBG: Unix Scheduled ",__LINE__," \n"	if $DEBUG;
		my($redirect)=" > __CODELOCATION__\\data\\GEM_BATCHJOB_LOGS\\Unix_".$base.".log 2> __CODELOCATION__\\data\\GEM_BATCHJOB_LOGS\\Unix_".$base.".err "
			unless $noredirect;
		my($file)=$GEM_ROOT_DIR."/unix_batch_scripts/".$file_root."batch/$base".$SPAWN_SUFFIX.".ksh";
		my($l)=$line.' $*'.$redirect;
		$l=~s/__PERL__/$GemConfig->{UNIX_PERL_LOCATION}/g;
		$l=~s/__CODELOCATION__/$GemConfig->{UNIX_CODE_LOCATION}/g;
		$l=~s/__BATCHID__/$base/g;
		
		$l=~s.\\.\/.g;

		unlink($file) if -r $file;

		open( OUT83,">".$file) or return l_warning("Cant write $file : $!");
		print OUT83 "umask 000\n".$l."\n";	#." \15newline\12";
		close(OUT83);
		chmod( 0777, $file ) or print "Unable to set permissions on $file $!\n";

		if( $batch_id_for_spawner ) {
			my($f2)=$file;
			$f2 =~ s/$SPAWN_SUFFIX//;
			unlink($f2) if -r $f2;
			open( OUTX7,">".$f2) or return l_warning("Cant write $f2 : $!");
			print OUTX7 $batch_id_for_spawner." \n";
			close(OUTX7);
			chmod( 0777, $f2 ) or print "Unable to set permissions on $f2 $!\n";			
		}		

		my($file2)=$GEM_ROOT_DIR."/unix_batch_scripts/".$file_root."interactive/$base.ksh";
		$l=$orig_line;
		$l=~s/__PERL__/$GemConfig->{UNIX_PERL_LOCATION}/g;
		$l=~s/__CODELOCATION__/$GemConfig->{UNIX_CODE_LOCATION}/g;
		$l=~s/__BATCHID__/$base/g;
		
		$l=~s.\\.\/.g;

		unlink($file2) if -r $file2;
		open(  OUT82,">".$file2) or return l_warning("Cant write $file2 : $!");
		print  OUT82 "umask 000\n".$l.' $* '."\n";	#."\15newline\12";
		close( OUT82);

		chmod( 0777, $file2 ) or print "Unable to set permissions on $file2 $!\n";
	} else {
		print "DBG: Not Win Scheduled ",__LINE__," \n"	if $DEBUG;
	}
}
