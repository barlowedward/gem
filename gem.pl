#
# copyright (c) 2011 by Edward Barlow
#
# Requires appropriate db variables
#   like SYBASE and ORAHOME and whatever
 
use strict;
use lib qw(lib);
use lib qw( /apps/sybmon/gem_dev/lib G:/gem_dev/lib //samba/sybmon/gem_dev/lib //samba/sybmon/gem_dev/Win32_perl_lib_5_8 );

use   Repository;
use	MlpAlarm;
use   Sys::Hostname;
use	Getopt::Long;
use   CommonFunc;
use	RunOn;
my($BATCH,$DEBUG,$PREFIX);

my($VERSION)=1.00;

my($GEM_ROOT_DIR) = get_gem_root_dir();
my($HOSTNAME)     = hostname();
my($USERNAME)		= $ENV{USER} || $ENV{USERNAME} || $ENV{LOGNAME} || "unknown" ;
my($STARTTIME)		= time;

sub usage
{
	print "usage: gem.pl --DEBUG --BATCH\n";
   return "\n";
}

$| =1;		# dont buffer standard output

die usage() unless GetOptions(
		"BATCH" 		=> \$BATCH,
		"DEBUG"     => \$DEBUG );

print "gem.pl - version $VERSION\n";
print " -- started at ".localtime(time)." on server ".$HOSTNAME." as user $USERNAME\n";

die "BATCH IS NO LONGER SUPPORTED " if $BATCH;

my($includes)=" -I$GEM_ROOT_DIR/lib ";	
$includes=" -I$GEM_ROOT_DIR/Win32_perl_lib_5_8 ".$includes if $^O =~ /MSWin32/ and $]>=5.008;
my(@newargs);
if( defined $ENV{perlargs} ) {
	$includes .= $ENV{perlargs}." ";
	print "Appending environment variable perlargs ($ENV{perlargs}) to includepath\n";
}
my($PERL)="$^X $includes ";

my($GEMCONFIG)=read_gemconfig();	
if( $HOSTNAME eq $GEMCONFIG->{NT_MONITOR_SERVER} ) {
	print "You are running on $HOSTNAME, the registered Win32 Monitor Station\n";
} elsif( $HOSTNAME eq $GEMCONFIG->{UNIX_MONITOR_SERVER} ) {
	print "You are running on $HOSTNAME, the registered Unix Monitor Station\n";
} elsif( $HOSTNAME eq $GEMCONFIG->{ALT1_MONITOR_SERVER} ) {
	print "You are running on $HOSTNAME, the registered Alternate Win32 Monitor Station\n";
} elsif( $HOSTNAME eq $GEMCONFIG->{ALT2_MONITOR_SERVER} ) {
	print "You are running on $HOSTNAME, the registered Alternate Unix Monitor Station\n";
} else {
	print "You are running on $HOSTNAME, this is NOT a GEM Monitor Station\n";
	print "* * * Hit Enter To Continue * * *";
	<STDIN>;
}
	
my(@menudat,@allmenus,@printmenus);
my($DIAGMODE)=0;
while(<DATA>) {
	next if /^\s*$/ or /^\s*\#/;
	chomp;chomp;	
	my($fmt,$batchty,$key,$cmd,$description,$outfile,$junk)=split(":~",$_,7);
	die "\nMALFORMED INPUT\nfmt=$fmt\nk=$key\nc=$cmd\nd=$description\noutfile=$outfile\nextra=$junk" if $junk;
	if( $^O eq 'MSWin32' ) {
		next unless $fmt =~ /W/ or (sybase_is_ok_on_win32() and $fmt =~ /S/);
	} else {
		next unless $fmt =~ /U/ or (sybase_is_ok_on_unix() and $fmt =~ /S/);
	}		
	push @allmenus,$_;	
	push @menudat ,$_;	
	push @printmenus ,$_ unless $key =~ /^LOADDB/;
}

$PREFIX='';
if( $BATCH ) {
	die "CANT PASS IN BATCH MODE\n";
	print "RUNNING PROGRAMS IN BATCH MODE\n";
	foreach (@menudat) {
		my($fmt,$batchty,$key,$cmd,$description,$outfile,$junk)=split(":~",$_,7);
		die "MALFORMED INPUT $_ " if $junk;
	
		if( $^O eq 'MSWin32' ) {
			next unless $batchty =~ /W/;
		} else {
			next unless $batchty =~ /U/;	
		}		
		$PREFIX=$key." ";
		printf "%8s %s\n",$key,$description;
		# print $cmd,"\n";
		run_a_command($key,$cmd,$description,$outfile);
	}
	print "normal completion\n";
	print " time taken = ".(time()-$STARTTIME)." seconds \n";
	exit(0);
}

sub print_menu {
	my($cnt)=1;
	my(@menu);
	foreach (@printmenus) {
		my($fmt,$batchty,$key,$cmd,$description,$outfile,$junk)=split(":~",$_,7);
		# next if $key=~/^LOADDB/;
		push @menu,sprintf("   %3s] %s\n",$cnt,$description);
		$cnt++;
	}
	#push @menu," \n";
	push @menu,sprintf("     l] Load static data\n");
	push @menu,sprintf("     d] Start or End diagnostic mode\n");
	push @menu,"q or x] exit\n";
	print @menu;
}

my($LOOP)=1;
print "\n\n";
while ($LOOP) {
	print "****************************************************\n";
	my($d)="[ diagnostic mode = on ]" if $DIAGMODE;
	printf "* Gem Administrator v%-3.3f %23.23s *\n",$VERSION,$d;
	print "****************************************************\n";
	print_menu();
	# print join("",@menu);	
	print "\n   INPUT:";
	my($keyin);
	$keyin=<STDIN>;
	chomp $keyin;
	chomp $keyin;
	if( $keyin =~ /d/i ) {
		if( $DIAGMODE ) {
			$DIAGMODE=0;
		} else {
			$DIAGMODE=1;
		}
		next;
	}
	if( $keyin =~ /^l/i ) {
		load_static_data();
		next;
	}
	last if $keyin =~ /^q/i or $keyin=~/^x/i;
	unless ( $keyin =~ /^\d+$/ ) {
		print "Not a number $keyin\n";
		next;
	}
	$LOOP=process_input($keyin);
	print "* * * Hit Enter To Continue * * *";
	<STDIN>;
}

print "\nSuccessful exit of gem.pl\n";
exit(1);

sub process_input {
	my(@in)=@_;	
	print "\nRead Input $in[0]\n";
	my($inline)=$printmenus[$in[0]-1];	
	if( ! $inline ) {
		print "No input $in[0]\n";
		return 1;
	}
	my($fmt,$batchty,$key,$cmd,$description,$outfile,$junk)=split(":~",$inline,6);	
	die "MALFORMED INPUT $inline " if $junk;
	run_a_command($key,$cmd,$description,$outfile);
}
	
sub run_a_command {
	my($key,$cmd,$description,$outfile) = @_;
	$outfile=~s/\s+$//;
	$outfile=~s/\$HOSTNAME/$HOSTNAME/g;
	$outfile=~s/\$ROOTDIR/$GEM_ROOT_DIR/g;
	
	print "****************************************************\n";
	printf "* $PREFIX.$description\n";
	print "****************************************************\n\n";
	
	if( $outfile ) {
		open(OUT,">".$outfile) or die "cant write output file \'".$outfile."\' $!\n";
	}

	my($logfile) = "$GEM_ROOT_DIR/logs/$HOSTNAME.$USERNAME.$key.log";
	print $PREFIX."Creating logfile $logfile\n";
	open(L,">".$logfile) or die "Cant write $logfile\n";
	
	$cmd=~ s/\$ROOTDIR/$GEM_ROOT_DIR/g;
	$cmd=~ s/\$PERL/$PERL/g;
	if( $GEMCONFIG->{UNIX_PERL_LOCATION} ) {
		$cmd=~ s/\$UNIXPERL/$GEMCONFIG->{UNIX_PERL_LOCATION}/g;
	} else {
		$cmd=~ s/\$UNIXPERL/$GEMCONFIG->{NT_PERL_LOCATION}/g;
	}
	
	if( $cmd =~ /\$LIBLIST/ ) {
		my(@paths);
		push @paths,$GEM_ROOT_DIR if is_nt();	
		push @paths,$GEMCONFIG->{ALT1_CODE_LOCATION};
		push @paths,$GEMCONFIG->{NT_CODE_LOCATION};
		push @paths,$GEMCONFIG->{NT_CODE_FULLSPEC};
		
		my(%pathsh);
		my($libpath);
		foreach ( @paths ) {
			next unless $_;
			s?\\?/?g;
			next if $pathsh{$_};
			$pathsh{$_}=1;
			$libpath .= $_."/lib,".$_."/Win32_perl_lib_5_8,";
		}
				
		@paths=();
		push @paths,$GEM_ROOT_DIR unless is_nt();	
		push @paths,$GEMCONFIG->{ALT2_CODE_LOCATION};
		push @paths,$GEMCONFIG->{UNIX_CODE_LOCATION};
		foreach ( @paths ) {
			next unless $_;
			s?\\?/?g;
			next if $pathsh{$_};
			$pathsh{$_}=1;
			$libpath .= $_."/lib,";
		}
		$libpath=~s/,$//;		
		$cmd=~s/\$LIBLIST/$libpath/;	
		# die "Library Path is $libpath\n\ncmd is $cmd";		
	}	
	
	if( $cmd =~ /--LOGINPASS/ ) {
		 my($scheduler_act);	
		print "\nPlease Enter NT Account LoginName For The Scheduled Task: ";
		$scheduler_act=<STDIN>;
		chomp $scheduler_act;
		chomp $scheduler_act;
		die "\nScheduled Account May not be  blank\n" unless $scheduler_act;
		
		my($scheduler_pass);	
		print "\nPlease Enter NT Account Password For The Scheduled Task: ";
		$scheduler_pass=<STDIN>;
		chomp $scheduler_pass;
		chomp $scheduler_pass;
		die "Scheduled Pass May not be  blank\n" unless $scheduler_pass;
		$cmd =~ s/--LOGINPASS/--SCHLOGIN=$scheduler_act --SCHPASS=$scheduler_pass/;		
	}
		
	# $ROOTDIR/lib,$NT_CODE_FULLSPEC/lib,$NT_CODE_FULLSPEC/lib/Win32_perl_lib_5_8,$NT_CODE_LOCATION/lib,$NT_CODE_LOCATION/Win32_perl_lib_5_8 
	if( $DIAGMODE ) {
		print "(diag) adding --DEBUG to command line\n";
		if( $cmd =~ /.pl\s*$/ ) {
			#print "P1";
			$cmd.=" --DEBUG";
		} else {
			#print "P2";
			$cmd=~ s/\.pl\s/.pl --DEBUG /;
		}
	} 
	
	print "\n".$PREFIX."Processing $cmd\n\n";
	
	start_gem_task(TaskName=>$key,  TaskHostName=>$HOSTNAME, TaskCmdLine=>$cmd);
	open(CMD,"$cmd |") or die "Cant run $cmd\n";
	my($cnt)=0;
	while(<CMD>) {
		if( $outfile ) {
			print OUT $_;
			print "." if $cnt%25==0;
		} else {		
			print $PREFIX.$_;		
		}
		print L $_;
		$cnt++ unless $_ =~ /^\s*$/;
	}
	print $PREFIX."[ Command returned $cnt rows. ]\n\n";
	close(CMD);
	close(OUTFILE) if $outfile;
	close(L);
	end_gem_task( TaskName=>$key,  TaskHostName=>$HOSTNAME);
	return 1;
}

#sub get_gem_task {
#	my($q)="select TaskName,TaskEndTime	from GEM_Task_History where TaskHostName='".$HOSTNAME."'";
#	my(%h);
#	while( _querywithresults($q,1) ) {
#		my(@x)=dbi_decode_row($_);
#		$h{$x[0]} = $x[1];
#	}
#	return %h;
#}
	
sub start_gem_task {
	my(%args)=@_;
	my($q)="
if not exists ( 
	select * from GEM_Task_History 
	where TaskName='".$args{TaskName}."' 
	and TaskHostName='".$args{TaskHostName}."' 
) insert GEM_Task_History (TaskName, TaskHostName) values ('".$args{TaskName}."','".$args{TaskHostName}."' )";
	_querynoresults($q,1);
	
	my($q1)="
update GEM_Task_History
	set TaskCmdLine='".$args{TaskCmdLine}."',
		 TaskStartTime=getdate()	
where TaskName='".$args{TaskName}."' 
	and TaskHostName='".$args{TaskHostName}."'";
	_querynoresults($q1,1);
}

sub end_gem_task {
	my(%args)=@_;
	my($q1)="
update GEM_Task_History
	set TaskEndTime=getdate()	,
			TaskDuration_secs = datediff(ss,TaskStartTime,getdate()),
			TaskReturnCode='".$args{TaskReturnCode}."'
	
where TaskName='".$args{TaskName}."' 
	and TaskHostName='".$args{TaskHostName}."'";
	_querynoresults($q1,1);
}

sub load_static_data {
	print "Loading Static Data Into Database\n";
	foreach (@allmenus) {
		my($fmt,$batchty,$key,$cmd,$description,$outfile,$junk)=split(":~",$_,7);
		run_a_command($key,$cmd,$description,$outfile) if $key=~/^LOADDB/;
	}

	# load into INV_constant the key/description pairs
	my($q)="delete INV_constant where name like 'GEM_MENU_%'";
	_querynoresults($q,1);
	foreach (@allmenus) {
		next unless $_;	
		my($fmt,$batchty,$key,$cmd,$description,$outfile,$junk)=split(":~",$_,7);
		next if $key=~/^LOADDB/;
		# print "Key=$key Desc=$description\n";
		my($q)="insert INV_constant values ( 'GEM_MENU_ITEMS', '".$key."' )";
		_querynoresults($q,0);
		my($q)="insert INV_constant values ( 'GEM_MENU_DESCR_".$key."','".$description."' )";
		_querynoresults($q,0);
	}	
}
__DATA__

# Column delimter is :~ (two characters)
# WU is W for WIN32 and U for UNIX
# First field is where gem.pl can run on, second for batch runs
# format WU:~WU:~KEY:~CMD:~Description:~Outfile
# format is: my($fmt,$batchty,$key,$cmd,$description,$outfile,$junk)=split(":~",$_,7);
#
# variables interpolated - PERL ROOTDIR HOSTNAME
UW:~:~TSTMAIL:~$PERL $ROOTDIR/bin/test_mail.pl :~Test - Mail Setup:~
U:~:~TSTUCONN:~$PERL $ROOTDIR/ADMIN_SCRIPTS/bin/TestDfltUnixConnectivity.pl:~Test - ssh To Unix Systems:~
UW:~:~TESTMAIL:~$PERL $ROOTDIR/ADMIN_SCRIPTS/monitoring/Heartbeat.pl --STATE=ERROR --SYSTEM=TEST --MONITOR_PROGRAM=gem.pl --SUBSYSTEM=ALERT --MESSAGE_TEXT="TEST ALERT":~Test - Send A Test Alert:~

UW:~:~REFORMAT:~$PERL $ROOTDIR/ADMIN_SCRIPTS/bin/rebuild.pl --LIBDIR=$LIBLIST --PERL=$UNIXPERL --FILENAME=conf,ADMIN_SCRIPTS:~Tools - Reformat Codeline:~	
UW:~:~LOADCF:~$PERL $ROOTDIR/bin/sync_configfiles_and_db.pl :~Tools - Load Configfiles To Database:~
UW:~:~LOADDB1:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_UpdateAttributeDictionary.pl:~Tools - Load Dictionary:~
UW:~:~LOADDB2:~$PERL $ROOTDIR/ADMIN_SCRIPTS/cgi-bin/LoadConstantData.pl:~Tools - Load Constant Data:~
UW:~:~LOADDB3:~$PERL $ROOTDIR/ADMIN_SCRIPTS/cgi-bin/LoadMimiReportData.pl:~Tools - Load Report Data:~
UW:~:~LOADDB4:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_load_configfiles.pl:~Tools - Load Default Policies:~
UW:~:~REFORMAT:~$PERL $ROOTDIR/ADMIN_SCRIPTS/monitoring/port_monitor.pl --GENERATE:~Tools - Create Port Monitor File:~	
W:~:~SPROCS:~$PERL $ROOTDIR/ADMIN_SCRIPTS/procs/configure.pl --SQLSERVERS:~Tools - Install Sql Server GEM Stored Procedures:~	
S:~:~PROCS:~$PERL $ROOTDIR/ADMIN_SCRIPTS/procs/configure.pl --SYBSERVERS:~Tools - Install Sybase GEM Stored Procedures:~	
W:~:~SPROCSU:~$PERL $ROOTDIR/ADMIN_SCRIPTS/procs/configure.pl --SQLSERVERS --UPGRADE_TO_LATEST:~Tools - Update Sql Server GEM Stored Procedures:~	
S:~:~PROCSU:~$PERL $ROOTDIR/ADMIN_SCRIPTS/procs/configure.pl --SYBSERVERS --UPGRADE_TO_LATEST:~Tools - Update Sybase GEM Stored Procedures:~	
W:~:~ROLLWEB:~$PERL $ROOTDIR/bin/rollout_web_server.pl:~Tools - Rollout Web Server Code:~
W:~:~SNAP:~$PERL $ROOTDIR/ADMIN_SCRIPTS/bin/win32_service.pl --ACTION=SNAPSHOT:~Tools - Snapshot Win32 Services:~

UW:~:~DISCVRS:~$PERL $ROOTDIR/bin/discover_sybase.pl:~Dont Use - Discover Sybase Servers From Interfaces File:~
W:~:~DISCVRS:~$PERL $ROOTDIR/bin/discover_sqlsvr.pl:~Dont Use - Discover Sybase Sql Servers:~

#W:~W:~ADLOAD:~$ROOTDIR/ADMIN_SCRIPTS/inventory_manager/AdFind/adfind -b ou=AD_Servers,dc=AD,dc=MLP,dc=COM -f &(objectCategory=computer)(objectClass=computer):~Load Active Directory (LDAP) Info:~$ROOTDIR/discovery/$HOSTNAME.adfind.txt
# UW:~U:~DISCVR:~$PERL $ROOTDIR/bin/discover.pl --GEM --CREATECFGFILES:~Run Database Discovery:~
U:~:~DCMLOAD:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/copy_attributes.pl --FROMTBL=BarlowMachineView --FROMDB=racks --NORMALEXCLUDE:~Dont Use - Load External Feed From DCM (racks db):~
U:~:~WASPLOAD:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/load_inv_from_csv.pl --FROMFILE=$ROOTDIR/scripts/WASP3.csv --LOAD_SYSTEM_TYPE=WASP --LOAD_SYSTEM_NAME_FIELD="Machine Name":~Dont Use - Load External Feed From WASP (csv file):~
U:~:~SSLOAD:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/load_inv_from_csv.pl --FROMFILE=$ROOTDIR/scripts/SERVERLIST.csv --LOAD_SYSTEM_TYPE=spreadsheet --LOAD_SYSTEM_TYPE_FIELD=TYPE --LOAD_SYSTEM_NAME_FIELD="NAME":~Dont Use - Load External Feed Of Databases (csv file):~
#W:~:~DBAUDSQL:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_database_auditor.pl --DOALL=sqlsvr --BATCH_ID=INV_database_auditor:~Audit/Survey Sql Servers:~
#W:~:~DBAUDORA:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_database_auditor.pl --DOALL=oracle --BATCH_ID=INV_database_auditor:~Audit/Survey Oracle:~
W:~:~GETSYSUS:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_win_getsystemusers.pl --BATCH_ID=INV_win_getsystemusers:~SAS70 - Fetch Windows Credentials:~
W:~:~DSGETUSR:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_dsget.pl:~SAS70 - Fetch LDAP User Information:~
UW:~:~MAPPER:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_mapper.pl --BATCH_ID=INV_mapper:~SAS70 - Map Credentials To Users:~

#U:~:~DBAUDSYB:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_database_auditor.pl --DOALL=sybase --BATCH_ID=INV_database_auditor:~Start INV_database_auditor.pl sybase:~
#U:~:~DBAUDMYSQL:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_database_auditor.pl --DOALL=mysql --BATCH_ID=INV_database_auditor:~Start INV_database_auditor.pl mysql:~
#U:~:~UNXPASS:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_get_etcpasswd.pl --BATCH_ID=INV_get_etcpasswd:~Discover /etc/passwd:~$ROOTDIR/discovery/$HOSTNAME_etcpasswd.txt
#U:~:~UNX_NIS:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_unix_ypcat_extractor.pl --BATCH_ID=INV_ypcat_extractor:~Discover Unix Nis Logins/Groups:~$ROOTDIR/discovery/$HOSTNAME_ypcatextractor.txt
U:~:~UNX_HOST:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_load_ypcat_hosts.pl --BATCH_ID=INV_load_ypcat_hosts:~CMDB - Step 1 - Load Hosts Table:~
#$ROOTDIR/discovery/$HOSTNAME_ypcatextractor.txt
U:~:~UXLOAD:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/load_unix_inv_dat.pl:~CMDB - Step 2 - Load Unix Inventory:~
UW:~:~PROMOTE:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_promote.pl:~CMDB - Step 3 - Run Promotion:~
W:~:~DSGETCPU:~$PERL $ROOTDIR/ADMIN_SCRIPTS/inventory_manager/INV_dsget_computer.pl --RUNADFIND --BATCH_ID=INV_dsget_computer:~CMDB - Step 4 - Load Active Directory Computers:~
U:~:~CRBATUJ:~$PERL $ROOTDIR/bin/create_batch_scripts.pl:~Batch Jobs - Create Unix GEM Batch Scripts 
W:~:~CRBATWJ:~$PERL $ROOTDIR/bin/create_batch_scripts.pl:~Batch Jobs - Create Win32 GEM Batch Scripts 
U:~:~CRCRON:~$PERL $ROOTDIR/bin/create_crontab.pl:~Batch Jobs - Create Crontab To Schedule Unix Batch Jobs
W:~:~WINSCHED:~$PERL $ROOTDIR/bin/schedule_tasks_on_win32.pl --LOGINPASS:~Batch Jobs - Schedule Win32 Batch Jobs
U:~:~DUMPBATR:~$PERL $ROOTDIR/bin/show_crontab.pl:~Batch Jobs - Show GEM Batch Jobs
#U:~U:~DUMPBATR:~$PERL $ROOTDIR/bin/show_scheduled_tasks.pl --showreal:~Batch Jobs - Show Jobs On This Server
W:~:~DUMPBATJ:~$PERL $ROOTDIR/bin/show_scheduled_tasks.pl:~Batch Jobs - Show GEM Batch Jobs



