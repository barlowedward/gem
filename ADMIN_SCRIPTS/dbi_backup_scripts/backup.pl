#!/apps/perl/linux/perl-5.8.2/bin/perl

# this should be automodified by configuration process
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#
# copyright (c) 1997-2009 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com
#
# This software is released as free software and should be shared and enjoyed
# You may modify this code provided this comment block remains intact
#

use strict;
use Getopt::Std;
use File::Basename;
use Sys::Hostname;
use Net::myFTP;
use Do_Time;
use Net::myRSH;
use MlpAlarm;
use Repository;
use Logger;
use CommonFunc;
use Cwd;

BEGIN {
	$SIG{ALRM} = sub { die "ERROR: PROGRAM TIMED OUT\n"; }
}

use vars qw($opt_R $opt_J $opt_l $opt_f $opt_d $opt_b $opt_D $opt_x $opt_v $opt_p $opt_r
	$opt_e $opt_h $opt_s $opt_t %CONFIG $opt_n $opt_m $opt_z);

use DBIFunc;
my($full_cmd_line);
my(@required_dirs)=qw(dbdumps dbcc audits bcp logdumps errors sessionlog);
my(%RUN);

# remove error logs
END { unlink $opt_e if defined $opt_e and -z $opt_e; }

sub usage
{
   my($data)=@_;

   die $data."\nUsage: backup_pl -JJOB -t|-f

The Master backup script. This script performs appropriate tasks as
defined by maintenance plans (specified with -JJOB) in configure.cfg
(located in backup scripts directory or one level above it).

 -d            debug mode
 -z            send a test mail message only (and exit)
 -v            show configuration variables and quit

 -x            noexec. purges will exec but other stuff wont
 -n            copy but dont load
 -p				dont purge tranlogs
 -t            transaction log dumps only
 -f            full backups only (default)
 -mmode        override communication mode (ssh|ftp|rsh)
 -bbatchid     id key for alarming system
 -h            html output.
 -s            skip transfer and load step
 -DDb_List     pipe separated list of databases to work on
 -r				reverse database list (select opposite ones)
 -RlpdDuabCir  run only the (l)oad, (p)urge, (d)ump, (D)bcc, (r)eorg,
      (u)pdate stats, (a)udit, (b)cp, (C)ompress, and/or (i)ndex steps.\n";

}

$| =1;

my($curdir)=cd_home();
my($VERSION,$SHTVER)=get_version();

my(%start_time,%end_time);
$start_time{BEGIN}=time;

# PARSE COMMAND LINE ARGUMENTS
die usage("") if $#ARGV<0;
$full_cmd_line=$0." ".join(" ",@ARGV);
usage("Bad Parameter List")  unless getopts('D:J:fdhtR:snb:m:xvzpr');
usage("Must Pass Job (Server) Name with -J")       unless defined $opt_J;
usage("Cant Pass Both -t and -f") if defined $opt_t and defined $opt_f;
$opt_f = 1 unless defined $opt_t;

# READ THE CONFIGURATION FILE
%CONFIG=read_configfile( -job=>$opt_J );
$CONFIG{COMMAND_RUN}=$full_cmd_line;

if( ! defined $opt_b ) {
	$opt_b="Backup";
	$opt_b.="Tran" if defined $opt_t;
	$opt_b.="Full" unless defined $opt_t;
}

# set up some stuff for the alarm routines
my($tran_log_text)="(tran log)" if defined $opt_t;
$tran_log_text="(full)"     unless defined $opt_t;

my($subsystem)="Job: ".$opt_J." ".$tran_log_text;
$subsystem.=" ".$opt_D if defined $opt_D and $opt_D ne "%";

# SET DEFAULT OPTIONS FOR FTP CONNECTIONS
my(%FTPCONFIG);
$FTPCONFIG{Debug}=$opt_d;
$FTPCONFIG{Timeout}=90000;
$FTPCONFIG{IS_REMOTE}=$CONFIG{IS_REMOTE};
$FTPCONFIG{IS_REMOTE}="n" if $CONFIG{SERVER_HOSTNAME} =~ /^\s*$/;
$FTPCONFIG{PRINTFUNC}=\&info;
$FTPCONFIG{DEBUGFUNC}=\&debug;

# REWRITE DO_* OPTIONS BASED ON -R COMMAND LINE ARGUMENTS
if( defined $opt_R ) {
	foreach ( keys %CONFIG ) {
		if( /^DO_/ and $CONFIG{$_} eq "y" ) {
			next if /^DO_ONLINE/ or /^DO_CLEARLOGSBEFOREDUMP/;
			print "Setting $_ To n\n";
			$CONFIG{$_}='n';
		}
	}
	foreach ( split (//,$opt_R )) {
		my($k)="";
		$k="DO_LOAD"     if $_ eq 'l';
		$k="DO_PURGE"    if $_ eq 'p';
		$k="DO_DUMP"     if $_ eq 'd';
		$k="DO_DBCC"     if $_ eq 'D';
		$k="DO_UPDSTATS" if $_ eq 'u';
		$k="DO_AUDIT"    if $_ eq 'a';
		$k="DO_BCP"      if $_ eq 'b';
		$k="DO_EXTERNAL_COMPRESS" if $_ eq 'C';
		$k="DO_INDEXES"  if $_ eq 'i';
		$k="DO_REORG"    if $_ eq 'r';
		$CONFIG{$k}="y";
		print "Re-Setting $k To y\n";
	}
}

my $progname=basename($0);

# SET UP SOME PATHS
my($srvr_dir_by_dbsvr) 	= $CONFIG{SERVER_DIRECTORY}."/".$CONFIG{SERVER_NAME};
my($dump_dir_by_dbsvr) 	= $CONFIG{SERVER_DIRECTORY}."/".$CONFIG{SERVER_NAME}."/dbdumps";
$dump_dir_by_dbsvr 		= $CONFIG{SERVER_DUMP_DIRECTORY} if defined $CONFIG{SERVER_DUMP_DIRECTORY};

my($log_dir_by_dbsvr) 	= $CONFIG{SERVER_DIRECTORY}."/".$CONFIG{SERVER_NAME}."/logdumps";
$log_dir_by_dbsvr 		= $CONFIG{SERVER_LOG_DIRECTORY} if defined $CONFIG{SERVER_LOG_DIRECTORY};
my($base_bk_dir)       	= $CONFIG{BASE_BACKUP_DIR}."/".$CONFIG{SERVER_NAME};

# THese vars are for use by the myftp module and should represent a full
#   path to a local file if IS_REMOTE=n or the remote path if IS_REMOTE=y
#   fundamentally, we will use this path to get to the file
my($srvr_dir_byclient, $dump_dir_byclient, $log_dir_byclient );
if($CONFIG{IS_REMOTE} eq "n") {
	$srvr_dir_byclient    = $CONFIG{CLIENT_PATH_TO_DIRECTORY}."/".$CONFIG{SERVER_NAME};
	if( defined $CONFIG{CLIENT_PATH_TO_DUMP_DIR} ) {
		$dump_dir_byclient    =  $CONFIG{CLIENT_PATH_TO_DUMP_DIR};
	} else {
		$dump_dir_byclient    = $srvr_dir_byclient."/dbdumps";
	}
	if( defined $CONFIG{CLIENT_PATH_TO_LOG_DIR} ) {
		$log_dir_byclient    =  $CONFIG{CLIENT_PATH_TO_LOG_DIR};
	} else {
		$log_dir_byclient    = $srvr_dir_byclient."/logdumps";
	}
} else {
	$srvr_dir_byclient    = $srvr_dir_by_dbsvr;
	$dump_dir_byclient    = $dump_dir_by_dbsvr;
	$log_dir_byclient     = $log_dir_by_dbsvr;
}

my($ftp);
my($ftplogin,$ftphost,$ftppass,$ftpmethod);   # needed in case you timeout during dbcc's

#
# Define Log Files For The Session
#
my($today_val)=today();
$opt_l  =$base_bk_dir."/sessionlog/$opt_J.";
$opt_l .=$opt_D."." if defined $opt_D and $opt_D ne "%" and $opt_D!~/\|/;
$opt_l .= "tranlog." if defined $opt_t;
$opt_l .= "full." unless defined $opt_t;
$opt_l .=$today_val.".log";

$opt_e  =$base_bk_dir."/errors/$opt_J.";
$opt_e .=$opt_D."." if defined $opt_D and $opt_D ne "%" and $opt_D!~/\|/;
$opt_e .= "tranlog." if defined $opt_t;
$opt_e .= "full." unless defined $opt_t;
$opt_e .=$today_val.".log";;

if( $opt_b ) {
	my($subkey);
	if( $opt_J ) {
		$subkey = $tran_log_text ." ". $opt_J;
	} else {
		$subkey = $tran_log_text ." ". $CONFIG{SERVER_NAME};
	}
	$subkey .= " => ".$opt_D if $opt_D;

	MlpBatchJobStart(-BATCH_ID => $opt_b, -AGENT_TYPE  =>  'Backup Job',  -SUBKEY  => $subkey );
}
#MlpBatchStart( -monitor_program =>	 $opt_b, -system  =>   $CONFIG{SERVER_NAME},
#				-subsystem  =>   "Job: ".$opt_J." ".$tran_log_text,
#				-batchjob	    =>	 1,
#				-message         =>   "$tran_log_text Backup Job Started at ".scalar(localtime(time)));
#MlpBatchJobStart(-BATCH_ID=>$opt_b,-AGENT_TYPE=>'Gem Monitor',-SUBKEY=> $args{-system} )
check_directory_structure();

logger_init(-logfile				=> $opt_l,
            -errfile				=> $opt_e,
            -database			=> $opt_D,
            -debug 				=> $opt_d,
            -jobname				=> $opt_J,
            -system 				=> $opt_J,
            -subsystem 			=>	$opt_J,
            -mail_from 			=> $CONFIG{MAIL_FROM},
            -command_run 		=> $CONFIG{COMMAND_RUN},
            -mail_host   		=> $CONFIG{MAIL_HOST},
            -mail_to     		=> $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to 	=> $CONFIG{SUCCESS_MAIL_TO} );

if( $opt_z ) {
	#foreach ( sort keys %CONFIG ) {
	#		printf("%20s => %s\n", $_ ,$CONFIG{$_});
	#	}
	logdie "TEST MESSAGE SEND FROM backup.pl.\n";
	exit(0);
}

debug "****** DEBUG FLAG SET (debug info to screen only) ******\n";

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

# GET PASSWORD INFO - TRY SYBASE AND IF NO PASSWORD FOUND TRY SQL SERVER PASSWORD FILES
debug("Getting Password Information for $CONFIG{SERVER_NAME}\n");
my($login,$password,$type)   = get_password(-name=>$CONFIG{SERVER_NAME},-type=>"sybase");
my($server_type)="SYBASE";
if( ! defined $login or ! defined $password or $login =~ /^\s*$/ or $password =~ /^\s*$/ ) {
	$server_type="SQL SERVER";
	($login,$password,$type)   = get_password(-name=>$CONFIG{SERVER_NAME},-type=>"sqlsvr")
}
logdie "No Password Found For Server $CONFIG{SERVER_NAME}.  Please edit your configuration file to ensure that an oper_role password is available.\n" unless defined $login and defined $password and $login !~ /^\s*$/ and $password !~ /^\s*$/;

# ERROR MESSAGES SHOULD COME BACK WITH THE BATCH RESULTS - INLINE
dbi_set_mode("INLINE");

my($header_str)=create_page_header();
info($header_str);

# DEBUG MODE - DO SOME PRINTS
if( defined $opt_d or $opt_v) {
	info("VARIABLES\n");
	foreach (sort keys %CONFIG ) {
		infof( "%-23.23s %-54.54s\n",$_,$CONFIG{$_});
	}
	exit(0) if $opt_v;
}

#
# Check if it is a remote server - if so build $SERVER_DIRECTORY if needed
#
if( $CONFIG{IS_REMOTE} eq "y" and defined $CONFIG{SERVER_HOSTNAME}) {
	debug("Creating Ftp Connection to $CONFIG{SERVER_HOSTNAME} \n");
	my($unix_login,$unix_password,$unix_type);
	if( ! $CONFIG{SERVER_HOST_LOGIN}
	or  ! $CONFIG{SERVER_HOST_PASSWORD} ) {
			($unix_login,$unix_password,$unix_type) = get_password(-name=>$CONFIG{SERVER_HOSTNAME},-type=>"unix");
	} else {
	      $unix_login=$CONFIG{SERVER_HOST_LOGIN};
	   	$unix_password=$CONFIG{SERVER_HOST_PASSWORD};
			print "Remote Account Defined from SERVER_HOST_LOGIN/SERVER_HOST_PASSWORD as $unix_login\n";
	}

	my(%svrargs)=get_password_info(-type=>"unix",-name=>$CONFIG{SERVER_HOSTNAME});
	my($method)=$svrargs{UNIX_COMM_METHOD};
	$method=$svrargs{WIN32_COMM_METHOD} if is_nt();
	$method="FTP" if ! defined $method or $method eq "";
	die "Can not run a backup on host $CONFIG{SERVER_HOSTNAME} as COMM_METHOD=NONE"	if $method eq "NONE";

	debug("Logging In Using $unix_login \n");
   die "Unix Login Not Defined for SERVER_HOSTNAME ",$CONFIG{SERVER_HOSTNAME}
		unless defined $unix_login and $unix_login !~ /^\s*$/;
   die "Unix Password Not Defined for SERVER_HOSTNAME ",$CONFIG{SERVER_HOSTNAME}
		unless defined $unix_password and $unix_password !~ /^\s*$/;

	$ftp=ftp_connect($CONFIG{SERVER_HOSTNAME},\%FTPCONFIG,$unix_login,$unix_password,$method);
	($ftphost,$ftplogin,$ftppass,$ftpmethod)=
		($CONFIG{SERVER_HOSTNAME},$unix_login,$unix_password,$method);
	$ftp->ascii();

	# check if remote directories exist
	my($ok)=0;
	info("Listing Files in $CONFIG{SERVER_DIRECTORY} \n" );
	my(@rc)=$ftp->ls( $CONFIG{SERVER_DIRECTORY} );
	foreach (@rc) {
		debug("ls returned: $_\n");
		next if /^\s*$/;
		chomp;
		next unless basename($_) eq $CONFIG{SERVER_NAME};
		$ok=1;
		last;
	}
	if( $ok == 0 ) {
		info("Creating Remote Directories in $srvr_dir_byclient\n");
		$ftp->mkdir($srvr_dir_byclient);
		foreach my $rd (@required_dirs) {
			info("Creating Remote Directories in $srvr_dir_byclient. (".$rd.")\n");
			$ftp->mkdir($srvr_dir_byclient."/$rd");
		}
	} else {
		debug("File Top Level Is Ok\n");
		debug("Listing Files in $CONFIG{SERVER_DIRECTORY} \n" );
		my(@rc)=$ftp->ls($srvr_dir_byclient);
		my(%dirs_found);
		foreach (@rc) {
			debug("Found $_\n");
			$dirs_found{$_}=1;
		}
		foreach (@required_dirs) {
			next if $dirs_found{$_};
			debug( "Creating Remote Directory $srvr_dir_byclient/$_");
			$ftp->mkdir($srvr_dir_byclient."/$_");
		}
	}
} elsif( $CONFIG{IS_REMOTE} eq "y" and ! defined $CONFIG{SERVER_HOSTNAME}) {
	die "Cant have IS_REMOTE defined but have no remote host defined\n";
} else {
	$ftp=ftp_connect("localhost",\%FTPCONFIG, "null", "null",undef);
	($ftphost,$ftplogin,$ftppass,$ftpmethod)= ("localhost","null","null","FTP");
}

# DEFAULT CONNECTION TYPES
if( is_nt() ) {
	$type="ODBC" unless defined $type;
} else {
	$type="Sybase" unless defined $type;
}

#
# CONNECT TO DB
#
info( "Connecting to server=$CONFIG{SERVER_NAME} as $login\n" );
dbi_connect( -srv=>$CONFIG{SERVER_NAME}, -login=>$login, -password=>$password, -type=>$type)
	or logdie "Cant connect to $CONFIG{SERVER_NAME} as $login\n";
dbi_set_mode("INLINE");
my($DBTYPE,$DBVERSION,$PATCHLEVEL) = dbi_db_version();
if( $server_type ne $DBTYPE ) {
	logdie("ERROR SERVER TYPE IS $server_type but native type is $DBTYPE $DBVERSION\n");
}
if( $server_type eq "SQL SERVER" ) {
	undef $CONFIG{NUMBER_OF_STRIPES};
}

#
# BUILD DATABASE LIST - Tran Dump only appropriate databases
#
my($whereclause)="";
my(%arg_d_databases_lc);
#if( defined $opt_D ) {
#   foreach ( split(/[\,\|]/,$opt_D)) {
#   	next if /^\s*$/;
#   	chomp;
#   	info("Passed In Database=$_\n");
#  		$arg_d_databases_lc{lc($_)}=1;
#	    if( $whereclause eq "" ) {
#         $whereclause="and ( name like \'$_\'";
#      } else {
#         $whereclause .= " or name like \'$_\' ";
#      }
#   }
#   $whereclause.=")" if $whereclause ne "";
#}

if( defined $opt_D ) {
  	foreach ( split(/[\,\|]/,$opt_D)) {
 		chomp;
 	 	next if /^\s*$/;
 	 	$arg_d_databases_lc{lc($_)}=1;
 		if( $whereclause eq "" ) {
			$whereclause="and ( name like \'$_\' or name = \'$_\'";
		} else {
			$whereclause .= " or name like \'$_\' or name = \'$_\'";
		}
	}
	$whereclause.=")" if $whereclause ne "";
}

my( $status1bits, $status2bits );
if( $server_type eq "SQL SERVER" ) {
	$status1bits = 32+64+128+256+512+1024+4096+32768;
	$status2bits = 0x0000;
} else {
	# 1120= 1000 (single usr) + 100 (suspect) + 20 (for load)
	#   30= 20 (offline until recovery) + 10 (offline)
	$status1bits = 0x1120;
	$status2bits = 0x0030;
}
info "Selecting Databases\n";
my($q)="select name,status,status2,status&$status1bits, status2&$status2bits from sysdatabases ";
$q.=" where 1=1 $whereclause" if $whereclause ne "";
debug("$q\n");
my(@d)=dbi_query(-db=>"master",-query=>$q );

info "Calculating Databases To Work On Based On\n";
info "DBCC_IGNORE_DB=$CONFIG{DBCC_IGNORE_DB}\n" if $CONFIG{DO_DBCC} eq "y";
info "DATABASE_IGNORE_LIST=$CONFIG{DATABASE_IGNORE_LIST}\n";
info "REVERSE SELECTION SELECTED (ie working on the ones not normally scheduled)\n" if $opt_r;

my(%ignore,%ignore_for_dbcc);
$CONFIG{DATABASE_IGNORE_LIST} =~ s/\s//g;
foreach ( split(/\|/,$CONFIG{DATABASE_IGNORE_LIST}) ) {
		next if $arg_d_databases_lc{lc($_)};
		$ignore{$_} = 1;
		$ignore_for_dbcc{$_}=1;
}

if( defined $CONFIG{DBCC_IGNORE_DB} ) {
	foreach ( split(/\|/,$CONFIG{DBCC_IGNORE_DB}) ) {
		$ignore_for_dbcc{$_} = 1 unless $arg_d_databases_lc{lc($_)};
	}
}

my(@full_db_list,@tran_db_list,@dbcc_db_list);
my(%full_db_list_hash,%tran_db_list_hash);
foreach( @d ) {
	s/\s//g;
   my($nm,$s1,$s2,$s1_new,$s2_new)=dbi_decode_row($_);

	if( $nm =~ /tempdb/ or $nm eq "model" or $nm eq "sybsystemdb" ) {
	  	info sprintf("-- ignoring db      %-30.30s (always ignore)\n",$nm);
	  	next;
	}

   if(  defined $ignore{$nm} and ! $opt_r ) {
	  	info sprintf("-- ignoring db      %-30.30s (per ignore list)\n",$nm);
	  	next;
	 }
	 if(  ! defined $ignore{$nm} and $opt_r ) {
	 	info sprintf("-- ignoring db      %-30.30s (per ignore list with -r)\n",$nm);
	  	next;
	 }

	 if( ! $arg_d_databases_lc{lc($nm)} ) {
	   if( $s1_new != 0 or $s2_new != 0 ) {
	   	info sprintf("-- ignoring db      %-30.30s (single user/suspect/load/or offline)\n",$nm);
	   	next;
	   }
	}

   push @full_db_list,$nm;
   if( ! $opt_r ) {
   	push @dbcc_db_list,$nm unless defined $ignore_for_dbcc{$nm};
   } else {
   	push @dbcc_db_list,$nm if defined $ignore_for_dbcc{$nm};
   }
   $full_db_list_hash{$nm}=1;
	$s2+=32768 if $s2 < -17000 and $server_type eq "SYBASE";

	if( $nm eq "master" or $nm eq "sybsystemprocs" or $nm =~ /^sysaudit/ or $nm eq "msdb" or $nm eq "sybsystemdb" ) {
		info sprintf("-- not tran dumping %-30.30s (illegal tran db)\n", $nm )
	} else {
		#info "FOUND $nm $s1/$s2\n";
		if( ($s2<0) or ($s1 & 4) or ($s1 & 8) ) {
			if($s1 & 4 and $s1 & 8 ) {
				info sprintf("-- not tran dumping %-30.30s (select/into, trunc log on chkpt)\n",$nm);
			} elsif( $s1 & 4 ) {
				info sprintf("-- not tran dumping %-30.30s (select/into)\n",$nm);
			} elsif( $s1 & 8 ) {
				info sprintf("-- not tran dumping %-30.30s (trunc. log on chkpt)\n",$nm);
			} else {
				info sprintf("-- not tran dumping %-30.30s (data/log on same device)\n",$nm);
			}
		} else {
   			push @tran_db_list,$nm;
   			$tran_db_list_hash{$nm}=1;
		}
	};
}
info "DBCC OK FOR DB=(",join(" ",@dbcc_db_list),")\n" if $CONFIG{DO_DBCC} eq "y";
if( defined $opt_t ) {
   	if( $#tran_db_list<0 ) {
      		exitout("Run on host ".hostname()."\nJob Failed: No Databases Found To Tran Dump\n",255,"ERROR");
   	}
	info "TRAN LOG DUMP OK FOR DB=(",join(" ",@tran_db_list),")\n" ;
} else {
	if( $#full_db_list<0 ) {
   		exitout( "Run on host ".hostname()."\nJob Failed: No Databases Found For Full Dump\nQuery Was $q",255,"ERROR" );
	}
	info "FULL DUMP OK FOR DB=(",join(",",@full_db_list),")" ,$NL;
}

my($COMMAND_OPTIONS)="-U$login -S$CONFIG{SERVER_NAME} -P$password ";
$COMMAND_OPTIONS .= " -d" if defined $opt_d;

my($COMMAND_OPTIONS_LONG)="-USER=$login -SERVER=$CONFIG{SERVER_NAME} -PASSWORD=$password ";
$COMMAND_OPTIONS_LONG .= " -DEBUG" if defined $opt_d;

my($COMPRESS_LOAD,$COMPRESS_DUMP)=("","");
if( $CONFIG{SYBASE_COMPRESSION_LEVEL} =~ /^\d$/
	and $CONFIG{SYBASE_COMPRESSION_LEVEL} ne "0") {
	$COMPRESS_DUMP=" -X".$CONFIG{SYBASE_COMPRESSION_LEVEL};
	$COMPRESS_LOAD=" -X";
}

#
# DUMP TRANSACTION LOGS AND EXIT IF -t PASSED
#
if( defined $opt_t ) {
   $start_time{TRAN_DUMPS}=time;

   info("Dumping Transaction Log\n");
   my $cmd = "dump_database.pl -Y -J$opt_J -s$today_val $COMMAND_OPTIONS -o$log_dir_by_dbsvr -M$type -t $COMPRESS_DUMP";
   $cmd.=" -D\"".join("\|",@tran_db_list)."\"";
   $cmd.="\n";

   run_cmd(-cmd=>$cmd,-print=>1, -err=>"dump transaction log command failed");
	info("Dump Transaction Succeeded\n");

   $end_time{TRAN_DUMPS}=time;

	# ok transfer and load the logs if needed...
	do_copy_load("FALSE",$log_dir_byclient) if $CONFIG{DO_LOAD} eq "y" and ! defined $opt_s;

	# info("THE FILES ARE: \n");
	# foreach( @tran_db_list ) {
		# info( $log_dir_byclient."/".$CONFIG{SERVER_NAME}.".".$_.".logdump.".$today_val."\n");
	# }
	info("\n");

	if( $CONFIG{DO_EXTERNAL_COMPRESS} eq "y"
   	and   $CONFIG{COMPRESS} =~ /^\s*$/ ) {
   	$start_time{COMPRESS}=time;
   	info("Compressing LogDumps Files at ".scalar(localtime(time))."\n");
   	run_compression( $ftp, $log_dir_byclient, $log_dir_by_dbsvr);
   	$end_time{COMPRESS}=time;
	}

	info(backup_timing());
	info("Transaction Log Backup Completed\n");

	# remove empty error logs if they exist
	if( defined $opt_e ) {
		if( -z $opt_e ) { unlink $opt_e; }
		if( (-s $opt_e) == 0 ) { unlink $opt_e; }
	}
	info( "\n****************************************************************\n");
	info( "*            Transaction Log Dump Completed                     \n");
	info( "****************************************************************\n");
	info(backup_timing());

   exitout("Succeeded: Transaction Log Backup",0,"COMPLETED");
}

#
# WARN IF NO DATABASES FOUND TO WORK ON
#
if( $#full_db_list<0 ) {
   exitout( "Failed: No Databases Found For Full Dump\nQuery Was select name from sysdatabases where status & 0x1120 = 0 and status2&0x0030=0 $whereclause",0,"ERROR" );
}
debug "Found Databases: ",join(" ",@full_db_list),$NL;

# MARK BATCH AS RUNNING IN THE ALARM DB
MlpBatchJobStep(-step=>"Databases Fetched...");

#
# PURGE OLD BACKUPS
#
info("\n");
if( $CONFIG{DO_PURGE} eq "y" ) {
	$start_time{PURGE}=time;

	# if u are going to dump one more, lower number u want to keep
	mylogdie("NUM BACKUPS TO KEEP MUST BE >= 1") if $CONFIG{NUM_BACKUPS_TO_KEEP}<1;
	$CONFIG{NUM_BACKUPS_TO_KEEP}-=1 if $CONFIG{DO_DUMP} eq "y";

	infobox("Purging Old Backups ($CONFIG{NUM_BACKUPS_TO_KEEP} versions kept)\n");
	info("-- Purge Directory is $dump_dir_byclient\n" );

	my($ft,$fs)=get_files_in_dir_for_server($ftp,$dump_dir_byclient);
	my(%FILE_TIMES)=%$ft;
	my(%FILE_SIZES)=%$fs;

	# Get Day/Times By Server/DB
	my(%ok_roots);      # key = server.db  value=daytime|daytime
	my(%first_dump);   # key = server.db  value=daytime
   if( $CONFIG{NUM_BACKUPS_TO_KEEP} >= 1 ) {
		debug("Calculating File Roots To Maintain A Given # of Backups\n");
		foreach(keys %FILE_TIMES) {
		      	my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
		      	next unless $srv eq $CONFIG{SERVER_NAME} and defined $time;
		      	next if $day=~/\D/ or $time=~/\D/;
		      	next unless defined $full_db_list_hash{$db};
		      	next if $opt_x; # noexec mode

					debug("File $_ \n");

		      	if( defined $ok_roots{$srv.$db} ) {
		         	my($dup)=0;
		         	my(@order)=();      # The Kept DayTimes for This SRV/DB

		         	foreach ( split(/\|/,$ok_roots{$srv.$db} )) {
			            	$dup=1 if $day.$time eq $_;
			            	push @order,$_;
		         	}
		         	if( $dup == 1 ) {
							debug("  (Duplicate Root)\n");
			         	next;          # ignore duplicate roots
		         	}

		         	if( $#order > $CONFIG{NUM_BACKUPS_TO_KEEP} -1 ) {
					# this really shouldnt happen... sooo...

					my($msg)="\nA Internal Error Has Occurred During The Purge\nError: Server $srv Database $db\n";
					foreach (@order) {
						$msg.="Previously Found Root of $_\n";
					}
					$msg.="Current Root is $day$time\n";
					$msg.="This is kind of an error\n";
					$msg.="  ive found ".($#order+1)." roots but can only keep $CONFIG{NUM_BACKUPS_TO_KEEP} files\n";
					$msg.="  how did i not purge previously\n";

					mylogdie($msg);
				} elsif( $#order == $CONFIG{NUM_BACKUPS_TO_KEEP} -1 ) {
			            	if( $first_dump{$srv.$db} < $day.$time ) {
			               	# replace first dump with $day.$time
			               	my($count) = -1;
			               	foreach (@order) {
			                  	$count++;
			                  	next unless $first_dump{$srv.$db}==$_;
			                  	$order[$count]=$day.$time;
			                  	last;
			               	}
					debug("  Replacing first dump with this dump\n");
		               		$first_dump{$srv.$db} = $day.$time;
		            	} else {
					debug("  This dump is before first kept dump and will be removed.\n");
							}
		         	} else {
			            	debug(" -- ".($#order+1)." other roots exist, keeping $CONFIG{NUM_BACKUPS_TO_KEEP} versions, so keep all\n");
			            	push @order,$day.$time;
		         	}
		         	$ok_roots{$srv.$db}=join("|",@order);
		      	} else {
		         	debug("root(srv=>$srv db=>$db day=>$day time=>$time)\n");
		         	$ok_roots{$srv.$db}=$day.$time;
		         	$first_dump{$srv.$db}=$day.$time;
		      	}
	   	}
   	}

	if( $CONFIG{IS_REMOTE} eq "y" ) {
   		info("-- Ready To Purge $dump_dir_byclient On Host $CONFIG{SERVER_HOSTNAME}\n")
	} else {
   		info("-- Ready To Purge $dump_dir_byclient on local system\n");
	}

	my($num_files_deleted)=0;
	foreach(sort keys %FILE_TIMES) {
	      # info the files you want to remove
	      my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
	      debug("checking: $_\n");
	      # debug(" This File Is Not On The Current Server (not purging)\n")
	 			# unless $srv eq $CONFIG{SERVER_NAME} and defined $time;
	      next unless $srv eq $CONFIG{SERVER_NAME} and defined $time;

	      debug(" This File Did Not Parse Correctly (not purging)\n")
	      	if $day=~/\D/ or $time=~/\D/;
	      next if $day=~/\D/ or $time=~/\D/;

	      # debug(" This File Is Not From A Database That Is Being Dumped ($db)\n")
	      	# unless defined $full_db_list_hash{$db};
	      next unless defined $full_db_list_hash{$db};

	      my $ok;
	      foreach ( split(/\|/,$ok_roots{$srv.$db} )) {
	         if( $day.$time eq $_ ) { $ok=1; last; }
	      }

	      debug(" Explicitly Keeping This File Ok\n") if defined $ok;
	      next if defined $ok;

		$num_files_deleted++;
	      info("-- Purging $_\n");
	      $ftp->delete("$dump_dir_byclient/$_")
	      	or info("Cant Delete $_ $!\n");
	}
	info("-- $num_files_deleted Files Deleted\n");

   	# remove tran logs associated with those dumps
	if( $CONFIG{IS_REMOTE} eq "y" ) {
   		infobox("-- Purge Associated Tran Logs On Remote Host $CONFIG{SERVER_HOSTNAME}\n");
	} else {
   		infobox("-- Purge Associated Transaction logs on local system\n");
	}
	info("-- Purge Directory is $log_dir_byclient\n" );
	info("-- source\-\>cwd to $log_dir_byclient\n" );
	$ftp->cwd($log_dir_byclient);
   ($ft,$fs)=get_files_in_dir_for_server($ftp,".");

	undef %FILE_TIMES;
	undef %FILE_SIZES;
	%FILE_TIMES=%$ft;
	%FILE_SIZES=%$fs;
	foreach(keys %FILE_TIMES) {
		my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
		debug( "DBG DBG: skipping $srv != $CONFIG{SERVER_NAME} \n" )
			  unless $srv eq $CONFIG{SERVER_NAME} and defined $time;
		next unless $srv eq $CONFIG{SERVER_NAME} and defined $time;
		debug( "DBG DBG: skipping day/time $day / $time\n" ) if $day=~/\D/ or $time=~/\D/;
		next if $day=~/\D/ or $time=~/\D/;
		debug( "DBG DBG: skipping FIRST DUMP $day.$time >= $first_dump{$srv.$db} \n" )
				if defined $first_dump{$srv.$db} and $day.$time >= $first_dump{$srv.$db};
		next  if defined $first_dump{$srv.$db} and $day.$time >= $first_dump{$srv.$db};
		#debug( "DBG DBG: skipping fulldbhash \n" )
		#	unless defined $full_db_list_hash{$db};
		next unless defined $full_db_list_hash{$db};
		next if $opt_x; # noexec mode
		chomp;
		s/\s//g;
		info("-- Purging ($_)\n");
		$ftp->delete("$log_dir_byclient/$_")
			or info("Cant Delete $_ $!\nIn Remote Directory ".$ftp->pwd());
	}

   	infobox("-- Purging Misc Local Files ($CONFIG{NUM_DAYS_TO_KEEP} days kept)\n");
   	foreach (@required_dirs) {
      		next if $_ eq "dbdumps" or $_ eq "logdumps" or $_ eq "audits";
      		next if $opt_x; # noexec mode
      		purge_local($base_bk_dir."/$_",$CONFIG{NUM_DAYS_TO_KEEP});
   	}
   	purge_local($base_bk_dir."/audits",$CONFIG{AUDIT_PURGE_DAYS});
   	debug("Purging Done\n");
   	$end_time{PURGE}=time;
} else {
   	infobox("Skipping Purge Step As Per Configuration Directive DO_PURGE=",$CONFIG{DO_PURGE}."\n");
}

#
# DBCC
#
info("\n");
MlpBatchJobStep(-step=>"Finished Purge");
if( $CONFIG{DO_DBCC} eq "y" ) {
   $start_time{DBCC}=time;
   infobox("Running Normal DBCC's\n");
   my $cmd=   "dbcc_db.pl $COMMAND_OPTIONS";
   $cmd.=" -D\"".join("\|",@dbcc_db_list)."\"";
   $cmd.=" -o\"".$base_bk_dir."/dbcc\"";
   $cmd.="\n";
   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   debug("(executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-print=>1,-err=>"Dbcc Command Failed\n");
   debug("(command completed executing)");
   $end_time{DBCC}=time;
} else {
   infobox("Skipping Dbcc Step As Per Configuration Directive DO_DBCC=",$CONFIG{DO_DBCC}."\n");
}

#
# TRAN DUMP WITH NO LOG ONLY IF WE ARE NOT LOG SHIPPING
# DO_TRUNCLOG_BEFOREDUMP defaults to y but is n if logs will be shipped
#
info("\n");
if( defined $opt_f and $CONFIG{DO_DUMP} eq "y" and $CONFIG{DO_TRUNCLOG_BEFOREDUMP} eq "y" and $CONFIG{DO_LOAD} ne "y" ) {

   $start_time{CLEAR_TRAN_LOGS}=time;
   infobox("Clearing Transaction Logs\n");
   foreach (@full_db_list) {
      info("-- Clearing Transaction Log For $_\n");
      foreach(dbi_query(-db=>"master",-query=>"dump tran $_ with no_log", -no_results=>1)) {
			my($err)=dbi_decode_row($_);
         error("WARNING: $err\n");
      };
   }
   $end_time{CLEAR_TRAN_LOGS}=time;
} else {
   infobox("Skipping Clearing Transaction Log Step - DO_TRUNCLOG_BEFOREDUMP=",$CONFIG{DO_TRUNCLOG_BEFOREDUMP}."\n");
}

#
# FULL DUMP
#
info("\n");
if( $CONFIG{DO_DUMP} eq "y" ) {
   $start_time{FULL_DB_DUMP}=time;
   infobox("Starting Full Database Dump at ".scalar(localtime(time))."\n");
   my $cmd = "dump_database.pl -Y -J$opt_J -M$type -s$today_val $COMMAND_OPTIONS -o$dump_dir_by_dbsvr -f $COMPRESS_DUMP";
   $cmd.=" -D\"".join("\|",@full_db_list)."\"";
   $cmd.=" -n$CONFIG{NUMBER_OF_STRIPES}"
      if defined $CONFIG{NUMBER_OF_STRIPES}
      and $CONFIG{NUMBER_OF_STRIPES}>1;
   $cmd.=" -v$CONFIG{DUMP_FILES_PER_SUBDIR}"
      if defined $CONFIG{DUMP_FILES_PER_SUBDIR}
      and $CONFIG{DUMP_FILES_PER_SUBDIR}>1
      and defined $CONFIG{NUMBER_OF_STRIPES}
      and $CONFIG{NUMBER_OF_STRIPES}>1;
   $cmd.="\n";
   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   debug("-- (executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-err=> "Dump Database Failed\n",-print=>1);
   debug("-- (completed) $cmd_no_pass");
   $end_time{FULL_DB_DUMP}=time;

} else {
   infobox("Skipping Dump Step As Per Configuration Directive DO_DUMP=",$CONFIG{DO_DUMP}."\n");
}

MlpBatchJobStep(-step=>"Finished Full Backup");

#
# REORG REBUILD/COMPACT
#
info("\n");
if( $CONFIG{DO_REORG} eq "y") {
	#if( $server_type eq "SQL SERVER" ) {
		#info("SKIPPING REORG AS THIS IS A SQL SERVER\n");
	#} else {
	   infobox("Starting Reorg\n");
	   MlpBatchJobStep(-step=>"Starting Reorg");
	   $start_time{REBUILD_REORG}=time;
	   mylogdie("NO REORG ARGS") unless $CONFIG{"REORG_ARGS"};
	   my $cmd=   "reorg.pl $COMMAND_OPTIONS_LONG ".$CONFIG{"REORG_ARGS"};
	   $cmd.=" -DATABASE=\"".join("\|",@full_db_list)."\"";
	   $cmd .= "\n";
	   my $cmd_no_pass=$cmd;
	   $cmd_no_pass=~s/-PASSWORD=\w+/-PASSWORD=XXX/;
	   debug("-- (executing) $cmd_no_pass");
	   run_cmd(-cmd=>$cmd,-err=>"Sybase Reorg Failed\n",-print=>1);
	   $end_time{REBUILD_REORG}=time;
	#}
	MlpBatchJobStep(-step=>"Finished Reorg Step");
} else {
   infobox("Skipping Reorg Step As Per Configuration Directive DO_REORG=",$CONFIG{DO_REORG}."\n");
}

#
# UPDATE STATISTICS
#
info("\n");
if( $CONFIG{DO_UPDSTATS} eq "y" ) {
	MlpBatchJobStep(-step=>"Starting Update Statistics");
   $start_time{UPDATE_STATISTICS}=time;
   infobox("Starting Update Statistics\n");
   my $cmd=   "update_stats.pl $COMMAND_OPTIONS -s ";
   $cmd.=" -D\"".join("\|",@full_db_list)."\"";
	$cmd.=" ".$CONFIG{UPD_STATS_FLAGS};
   $cmd.="\n";
   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   debug("-- (executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-err=>"Update Stat Failed\n",-print=>1);
   $end_time{UPDATE_STATISTICS}=time;
   MlpBatchJobStep(-step=>"Finished Update Statistics");
} else {
   infobox("Skipping Update Statistics Step As Per Configuration Directive DO_UPDSTATS=",$CONFIG{DO_UPDSTATS}."\n");
}

#
# LOAD DATABASE
#  read in server to copy to and db list to copy from/to
#
info("\n");
if( $CONFIG{DO_LOAD} eq "y" ) {
	MlpBatchJobStep(-step=>"Started Copy/Load");
 	if( defined $opt_s ) {
   	infobox("Skipping Copy/Load As -s Option Passed\n");
	} else {
		infobox("Starting Copy/Load from $dump_dir_byclient \n");
   	do_copy_load("TRUE",$dump_dir_byclient);
   	info("Completed Copy/Load\n");
	}
	MlpBatchJobStep(-step=>"Finished Copy/Load");
} else {
	MlpBatchJobStep(-step=>"Skipped Copy/Load");
   infobox("Skipping Load DB Step As Per Configuration Directive DO_LOAD=",$CONFIG{DO_LOAD}."\n");
}

#
# DO AUDITING OF CONFIGURATION
#
info("\n");
if( $CONFIG{DO_AUDIT} eq "y" ) {
   MlpBatchJobStep(-step=>"Starting Audit");
   $start_time{AUDIT}=time;
   infobox("Running Audit Of The Server  at ".scalar(localtime(time))."\n");
   my($audfile)= $base_bk_dir."/audits/$CONFIG{SERVER_NAME}.".$today_val;
   #my $cmd=   "config_report.pl $COMMAND_OPTIONS -b -o$audfile\n";
   my $cmd=   "config_report.pl $COMMAND_OPTIONS_LONG -OUTFILE=$audfile\n";

   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   info("-- (executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-err=>"Audit Failed\n",-print=>1);
   $end_time{AUDIT}=time;
} else {
   MlpBatchJobStep(-step=>"Skipped Audit");
   infobox("Skipping Creation of Audit Report Step As Per Configuration Directive DO_AUDIT=",$CONFIG{DO_AUDIT}."\n");
}

info( "Reconnecting To $CONFIG{SERVER_HOSTNAME}\n");
$start_time{RECONNECT}=time;
#if( $FTPCONFIG{IS_REMOTE} eq "y" ) {
#	debug( "" );
#	info( "Creating Connection To $CONFIG{SERVER_HOSTNAME} \n" );
#}
my($ftp4)= ftp_connect($CONFIG{SERVER_HOSTNAME},\%FTPCONFIG, $ftplogin, $ftppass, $ftpmethod);
$end_time{RECONNECT}=time;

MlpBatchJobStep(-step=>"Before Compression Step");

#
# COMPRESSION
#
info("\n");
if( $CONFIG{SYBASE_COMPRESSION_LEVEL} =~ /\d$/
and $CONFIG{SYBASE_COMPRESSION_LEVEL}  ne "0" ) {
	MlpBatchJobStep(-step=>"Skipping Compression - Internal Sybase Compression Used");
	# Do nothing
	infobox( "Skipping Compression - Using Sybase Internal Compression level ".$CONFIG{SYBASE_COMPRESSION_LEVEL}."\n" );
} elsif( $CONFIG{DO_EXTERNAL_COMPRESS} eq "y"
   and   $CONFIG{COMPRESS} =~ /^\s*$/ ) {
	MlpBatchJobStep(-step=>"Running Compression But Compress Directive Empty");
	# no compression command
   info("Skipping Compress: DO_EXTERNAL_COMPRESS=y but COMPRESS directive empty\n");
} elsif( $CONFIG{DO_EXTERNAL_COMPRESS} eq "y" ) {
	MlpBatchJobStep(-step=>"Starting External Compression");
   $start_time{COMPRESS}=time;
   infobox("Compressing Backup Files at ".scalar(localtime(time))."\n");
   run_compression( $ftp4, $dump_dir_byclient, $dump_dir_by_dbsvr);
   $end_time{COMPRESS}=time;
} else {
	MlpBatchJobStep(-step=>"Skipping Compression Step");
   infobox("Skipping Compress Step DO_EXTERNAL_COMPRESS",$CONFIG{DO_EXTERNAL_COMPRESS}."\n");
}

info("\n");
if( $CONFIG{DO_INDEXES} eq "y") {
   infobox("Rebuilding Indexes\n");
	MlpBatchJobStep(-step=>"Rebuilding Indexes");
   $start_time{REBUILD_INDEXES}=time;
   my($count)=0;
   my(@f) = split(/\|/,$CONFIG{REBUILD_INDEX_FILE});
   foreach my $db ( split(/\|/,$CONFIG{REBUILD_INDEX_DB})) {
      my $file=$f[$count];
      my $cmd=   "rebuild_index.pl $COMMAND_OPTIONS -r -i$file\n";
      info("Rebuilding Indexes Based On File $file\n");
      $count++;
      my $cmd_no_pass=$cmd;
      $cmd_no_pass=~s/-P\w+/-PXXX/;
      debug("-- (executing) $cmd_no_pass");
      run_cmd(-cmd=>$cmd,-err=>"Rebuild Index Failed\n",-print=>1);
   }
   $end_time{REBUILD_INDEXES}=time;
} else {
   infobox("Skipping Index Step As Per Configuration Directive DO_INDEXES=",$CONFIG{DO_INDEXES}."\n");
}

# almost Successful Completion
#  - check directory for sizes of the dumps
info("\nCross Check For Dump File Size ($dump_dir_byclient)\n");
MlpBatchJobStep(-step=>"Cross Check Dump File Sizes");
$start_time{CROSS_CHECK}=time;
my(%FILE_TIMES);
my(%FILE_SIZES);

if( defined $CONFIG{DUMP_FILES_PER_SUBDIR}
      and $CONFIG{DUMP_FILES_PER_SUBDIR}	>1
      and defined $CONFIG{NUMBER_OF_STRIPES}
      and $CONFIG{NUMBER_OF_STRIPES}		>1 ) {
      	my($val)=int(($CONFIG{NUMBER_OF_STRIPES}/$CONFIG{DUMP_FILES_PER_SUBDIR})+.99);
      	for my $cnt (1..$val) {
      		my($ft,$fs)=get_files_in_dir_for_server($ftp4,$dump_dir_byclient."/device_".$cnt);
      		foreach (keys %$ft) { $FILE_TIMES{$_}=$$ft{$_}; }
      		foreach (keys %$fs) { $FILE_SIZES{$_}=$$fs{$_}; }
      	}
} else {
	my($ft,$fs)=get_files_in_dir_for_server($ftp4,$dump_dir_byclient);
	%FILE_TIMES=%$ft;
	%FILE_SIZES=%$fs;
}
my(%num_stripes_at_time,%num_zero_sized_files,%file_sizes);
debug("backup.pl line ",__LINE__," searching through backup files for $CONFIG{SERVER_NAME} and $today_val\n");
foreach (sort keys %FILE_TIMES) {
	debug("Found : File $_ Size=",$FILE_SIZES{$_},"\n");
   my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
   next unless $srv eq $CONFIG{SERVER_NAME}
            and defined $time
            and $day.".".$time eq $today_val;
   $num_stripes_at_time{$db}++;
   $num_zero_sized_files{$db}++ if $FILE_SIZES{$_}==0;
   $file_sizes{$db}+=$FILE_SIZES{$_};
}

my($file_size_str)="";

if( $CONFIG{DO_DUMP} eq "y" ) {
	$file_size_str= "Dump Files Created By This Run:\n".
		sprintf("%30.30s %14.14s %6.6s %s\n","DataBase Name","Stripes-Made","Errors","Dump Size (bytes)");
	foreach (sort @full_db_list) {
   	$file_size_str.=sprintf("%30.30s %14s %6s %s\n",$_,$num_stripes_at_time{$_},$num_zero_sized_files{$_},$file_sizes{$_});
	}
}
$end_time{CROSS_CHECK}=time;

#
# BCP OUT STUFF AS NEEDED
#
MlpBatchJobStep(-step=>"Bcp's");
info("\n");
if( $CONFIG{DO_BCP} eq "y") {
	if( ! defined $CONFIG{BCP_TABLES} or $CONFIG{BCP_TABLES}=~/^\s*$/ ) {
		   infobox("BCP Flag=Y but BCP_TABLES='' - skipping step as nothing to do\n");
	} else {
	   MlpBatchJobStep(-step=>"Starting Bcp");

	   $start_time{BCP}=time;
	   info("Starting Table Level BCP's\n");
	   info("> Table Spec=$CONFIG{BCP_TABLES}\n");
	   if( $CONFIG{BCP_COMMAND} ) {
	   	mylogdie("BCP COMMAND SET TO $CONFIG{BCP_COMMAND} BUT IT DOES NOT EXIST\n") 	 unless -r $CONFIG{BCP_COMMAND};
	   	mylogdie("BCP COMMAND SET TO $CONFIG{BCP_COMMAND} BUT IT IS NOT EXECUTABLE\n") unless -x $CONFIG{BCP_COMMAND};
	   	info("> BCP Command=$CONFIG{BCP_COMMAND}\n") if $CONFIG{BCP_COMMAND};
	   }
	   my(@bcp_tables)=split(/[\|\,]/,$CONFIG{BCP_TABLES});
	   foreach my $tbl (@bcp_tables) {
	   	mylogdie("ERROR: bcp table ($tbl) is not fullpath spec (ie. db..tblname)\n")
	   		unless $tbl=~/\./;
	   	info("Copying out table $tbl\n");
			$tbl =~ s/^\s+//;
			$tbl =~ s/\s+$//;
	   	my($cmd)="bcp";
	   	$cmd=$CONFIG{BCP_COMMAND} if $CONFIG{BCP_COMMAND};

	   	$cmd.=" $tbl out $base_bk_dir/bcp/$tbl.bcp -U$login -P$password -S$CONFIG{SERVER_NAME} -n\n";
	   	run_cmd(-cmd=>$cmd, -print=>1, -err=>"bcp failed");
	   }
	   infobox("Table Level BCP's Completed\n");
	   $end_time{BCP}=time;
	}
} else {
   infobox("Skipping Bcp Step As Per Configuration Directive DO_BCP=",$CONFIG{DO_BCP}."\n");
}

MlpBatchJobStep(-step=>"Almost Complete");

# remove empty error logs if they exist
my($errors_exist)="TRUE";
if( defined $opt_e ) {
	if( -z $opt_e ) {
		$errors_exist="FALSE";
		unlink $opt_e;
	}
	if( (-s $opt_e) == 0 ) {
		$errors_exist="FALSE";
		unlink $opt_e;
	}
}

my($backup_timing_str)= backup_timing();

# send email alert if there was an error
my($str)="";
if( defined $opt_e and -e $opt_e and $errors_exist eq "TRUE" ) {

   $str="The Following Error File Was Found:

===========================================================================

   command: $CONFIG{COMMAND_RUN}
   errorlog: $opt_e
   sessionlog: $opt_l
	errorlog size: ".(-s $opt_e)."

===========================================================================

   $file_size_str

===========================================================================

   $backup_timing_str

===========================================================================
ERROR FILE CONTENTS:
";

   open(ELOG,$opt_e) or mylogdie("Cant open error file $opt_e : $!\n");
   foreach (<ELOG>) { $str.="ERROR: ".$_; }
   close ELOG;
   $str.="\n\n".$header_str;
   bad_email($str);
   print $str;
	exitout("Failed: Backup Job Failed - Check Logs",255,"ERROR");
} else {
	infobox( "Backup Process Completed Successsfully\n");

   info("Job $opt_J Completed With No Errors at ".localtime(time)."\n\n".
         "$file_size_str\n\n".
         $backup_timing_str);
   ok_email("Job $opt_J Completed With No Errors at ".localtime(time)."\n\n".
         "$file_size_str\n\n".
         $backup_timing_str);
}

#
# WE ARE DONE
#
MlpBatchJobStep(-step=>"Complete");
dbi_disconnect();
exitout( "Succeeded: Full Backup Batch\n",0,"COMPLETED");

# BELOW ARE SUBROUTINES USED ABOVE
# EVERYTHING BELOW THE I !!

#                                                                          #
#                                                                          #
############################################################################
#                                                                          #
#                                                                          #
sub ftp_connect {
	my($host,$config,$ftplogin,$ftppass,$method)=@_;
	#my(%args);
	if( $opt_m ) {
		$opt_m=uc($opt_m);
		die "-m must be SSH or RSH or FTP" unless $opt_m eq "SSH" or $opt_m eq "RSH" or $opt_m eq "FTP";
	#	foreach (keys %$config) { 	$args{$_}=$$config{$_}; }
		$method="SSH" if $opt_m eq "SSH";
		$method="FTP" if $opt_m eq "FTP";
		$method="RSH" if $opt_m eq "RSH";
	}

	info("Connecting to host=$host\n");
	my($ftptmporary);
	eval {
		alarm( 30 );
		$ftptmporary= Net::myFTP->new($host,METHOD=>$method,%$config);
      mylogdie( "Failed To Connect To $host" ) unless $ftptmporary;
		debug("ftp_connect() Net::myFTP->new completed to host=$host\n");
		if( $ftplogin ne "null" and $ftppass ne "null" ) {
			debug("ftp_connect() login(login=$login)\n");
			my($erc,$err)=$ftptmporary->login($ftplogin,$ftppass);
			mylogdie( hostname()." Cant $method Connect To Host ($host) As $ftplogin : $! error=$err\n") if ! $erc;
		}
		debug("ftp_connect() connected to host=$host\n");
		alarm( 0 );
	};
	if( $@ ) {
		if( $@ =~ /ERROR: PROGRAM TIMED OUT/ ) {
			info( "host $host connection timed out - reconnecting\n" );
		} else {
			info( "host $host method $method ERROR ERROR $@  - reconnecting\n" );
		}
		alarm(0);
	}
	if( ! $ftptmporary ) {			# try again using a different method
		if( $method eq "FTP" ) {
			$method="RSH";
		} else {
			$method="FTP";
		}
		info("ReConnecting to host=$host with method=$method\n");
		eval {
			alarm( 30 );
			$ftptmporary= Net::myFTP->new($host,METHOD=>$method,%$config);
	      mylogdie( "Failed To Connect To $host" ) unless $ftptmporary;
			debug("ftp_connect() Net::myFTP->new completed to host=$host\n");
			if( $ftplogin ne "null" and $ftppass ne "null" ) {
				debug("ftp_connect() login(login=$login)\n");
				my($erc,$err)=$ftptmporary->login($ftplogin,$ftppass);
				mylogdie( "Cant $opt_m Connect To host ($host) As $ftplogin : $! // $err\n") if ! $erc;
			}
			debug("ftp_connect() connected to host=$host\n");
			alarm( 0 );
		};

		if( $@ ) {
			if( $@ =~ /ERROR: PROGRAM TIMED OUT/ ) {
				mylogdie( "host $host connection timed out\n" );
			} else {
				mylogdie( "host $host method $method ERROR ERROR $@\n" );
			}
			alarm(0);
		}
	}

	#my($ftptmporary)= Net::myFTP->new($host,METHOD=>$method,%$config)
   #           or mylogdie( "Failed To Create FTP to $host Object $@" );

	debug("Connected...\n");
	return $ftptmporary;
}

sub run_compression {
	my($conn,$dir,$remote_dir)=@_;
   my($ft,$fs)=get_files_in_dir_for_server($conn,$dir);
   my(%FILE_TIMES)=%$ft;
   my(%FILE_SIZES)=%$fs;

	# Find the "latest" file if needed (so we dont compress it)
   my(%LATEST_FILE);
   if( $CONFIG{COMPRESS_LATEST} eq "n" ) {
      foreach (sort keys %FILE_TIMES) {
         next if /\.gz$/ or /\.Z/;
         my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
         next unless defined $full_db_list_hash{$db};
         next unless $srv eq $CONFIG{SERVER_NAME} and defined $time;
         if( ! defined $LATEST_FILE{$srv." ".$db}  or
               $LATEST_FILE{$srv." ".$db} < $day.$time ) {
            $LATEST_FILE{$srv." ".$db}=$day.$time;
         }
      }
   }

   foreach (sort keys %FILE_TIMES) {
      if( /\.gz$/ or /\.Z$/ ){
			debug( "(allready compressed) $_\n" );
			next;
		};
		debug("Compress: File $_ Size=",$FILE_SIZES{$_},"\n");
      my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);

      if( ! defined $full_db_list_hash{$db} ) {
			debug("Ignoring $_ (not dumped) \n");
			next;
		};

		debug("Ignoring $_ As Not Server or No Time Defined")
      	unless $srv eq $CONFIG{SERVER_NAME} and defined $time;
      next unless $srv eq $CONFIG{SERVER_NAME} and defined $time;

      if( $CONFIG{COMPRESS_LATEST} eq "n"
            and $day.$time eq $LATEST_FILE{$srv." ".$db} ) {
			debug( "Skipping $_ (COMPRESS_LATEST=n)\n" );
			next;
		};

		debug( "Will attempt to compress $_ \n" );
      my $cmd;
      if( $CONFIG{IS_REMOTE} eq "n" ) {
			unlink "$dir/$_.gz" if -r "$dir/$_.gz";
			unlink "$dir/$_.Z" if -r "$dir/$_.Z";
         $cmd="$CONFIG{COMPRESS} $dir/$_\n";
         info("-- Compressing $_ using $CONFIG{COMPRESS}\n");
         debug("-- (executing) $cmd");
			if( $CONFIG{COMPRESS}=~/gzip/ ) {
        		info("-- Gzip Found.  Running Command $cmd\n");
         		run_cmd(-cmd=>$cmd,-print=>1);
				sleep(10);
        		info("-- Warning: Compress Failed To Remove File\n")
						if -e "$dir/$_";
				if( -e "$dir/$_" and -e "$dir/$_.gz" ) {
         		info("-- Attempting to Remove File $_\n") ;
						# if  -e "$dir/$_.gz" and $CONFIG{COMPRESS}=~/gzip/;
					unlink "$dir/$_";
						# if -e "$dir/$_.gz" and $CONFIG{COMPRESS}=~/gzip/;
         		info("-- Error $!\n") if  -e "$dir/$_";
							# and $CONFIG{COMPRESS}=~/gzip/;
				} elsif( -e "$dir/$_" ) {
         		info("-- File Exists W/No Compressed File - Recompressing\n") ;
         		run_cmd(-cmd=>$cmd,-print=>1);
				}
				if( -e "$dir/$_" ) {
         		info("-- Backup File *STILL* Exists \n") ;
				}
			} else {
         	# run_cmd(-cmd=>$cmd,-err=>"Compress Failed\n",-print=>1);
         	run_cmd(-cmd=>$cmd,-print=>1);
			}
      } else {
         info("-- Remote Compressing $_\n");
			do_rsh(-login=>$CONFIG{SERVER_HOST_LOGIN},
					-password=>$CONFIG{SERVER_HOST_PASSWORD},
					-hostname=>$CONFIG{SERVER_HOSTNAME},
					-debug=>$opt_d,
					-command=>"$CONFIG{COMPRESS} $remote_dir/$_");
		}
      # $cmd="rsh -l $CONFIG{SERVER_HOST_LOGIN} $CONFIG{SERVER_HOSTNAME} $CONFIG{COMPRESS} $remote_dir/$_\n";
      # debug("-- (executing) $cmd");
      # run_cmd(-cmd=>$cmd,-err=>"Compress Failed\n",-print=>1);
      # run_cmd(-cmd=>$cmd,-print=>1);
      #
   }
}

sub by_time {
   $start_time{$a} <=> $start_time{$b};
}


sub saveMlpSaveBackupJobRunInfo {
	my($JobSuccessCode,$JobExitNotes)=@_;
	sub ttx {
		my($t)=@_;
		return -1 unless defined $end_time{$t} and defined $start_time{$t};
		return $end_time{$t} - $start_time{$t};
	}
	MlpSaveBackupJobRunInfo($opt_J,$full_cmd_line,$JobExitNotes,$opt_b,$CONFIG{SERVER_NAME},
			do_time(-time=>$start_time{BEGIN},-fmt=>'mm/dd/yyyy hh:mi:ss'),
			do_time(-time=>time,-fmt=>'mm/dd/yyyy hh:mi:ss'),
			time-$start_time{BEGIN},
			$JobSuccessCode,
			ttx("TRAN_DUMPS"),
			ttx("COMPRESS"),
			ttx("PURGE"),
			ttx("DBCC"),
			ttx("CLEAR_TRAN_LOGS"),
			ttx("FULL_DB_DUMP"),
			ttx("REBUILD_REORG"),
			ttx("UPDATE_STATISTICS"),
			ttx("AUDIT"),
			ttx("RECONNECT"),
			ttx("COMPRESS"),
			ttx("REBUILD_INDEXES"),
			ttx("BCP"),
			ttx("CROSS_CHECK")
	 );
}

sub backup_timing {
	my $seconds=time-$start_time{BEGIN};
	my $minutes=int($seconds/60);
	$seconds-=60*$minutes;
	my($str)= "Job $opt_J completed successfully in $minutes minutes and $seconds secs.".$NL;
	foreach (sort by_time keys %start_time ) {
   	next if $_ eq "BEGIN";
   	if( ! defined $end_time{$_} ) {
      	warning "Hmmmm... No End Time Defined For $_\n";
      	next;
   	}
   	my $seconds = $end_time{$_} - $start_time{$_};
   	my($minutes)= int($seconds/60);
   	$seconds   -= (60*$minutes);
   	$str.=sprintf("%30.30s : %s%s",
                  	" -- $_",
                  	"completed in $minutes minutes $seconds secs.",
                  	$NL);
	}
	return $str;
}

#
# Returns FILE_TIME,FILE_SIZE hashes
#
sub get_files_in_dir_for_server
{
	my($ftp,$directory)=@_;
	debug( "Getting Files In $directory\n" );
	my(%FILE_TIME,%FILE_SIZE);
	my($file_count)=0;
	debug( "ftp\-\>cwd($directory) \n");
	$ftp->cwd($directory) or warn "get_files_in_dir_for_server() Unable to cwd to $directory: $!\n";
	foreach ( $ftp->fspec(".") ) {
		my($filenm,$filetime,$filesize)=@$_;
		next if $filenm =~ /^\./;
		$file_count++;
		$FILE_TIME{$filenm}=$filetime;
		$FILE_SIZE{$filenm}=$filesize;
		debug("$file_count) $filenm  $filetime $filesize\n");
	}
	debug("$file_count Files Found In Directory=$directory\n");
	return(\%FILE_TIME,\%FILE_SIZE);
}

#
# Remove Local Files
#
sub purge_local
{
  	my($dir,$days)=@_;
   	info("   -> Purge $dir ($days Days)\n");

   	opendir(DIR,$dir) || croak("Purge Can't open directory $dir for reading\n");
   	my(@files) = grep(!/^\./,readdir(DIR));
   	closedir(DIR);

	debug( ($#files+1)." Files Found\n" );
   	foreach (@files) {
				my($filedays)= -M "$dir/$_";
      		next  if $filedays < $days or -z "$dir/$_";
      		$filedays = int( $filedays * 10 ) / 10;
      		info "-- Purging $_ : ".$filedays." days old\n";
      		unlink $dir."/".$_ or info( "Cant Unlink $dir/$_  $!\n" ) unless $opt_x;
   	}
   	debug("Done Purging Directory $dir ($days Days)\n");
}

sub run_cmd
{
   my(%OPT)=@_;
   my(@rc);
   chomp $OPT{-cmd};

   $OPT{-cmd} .= " 2>&1 |" unless defined $ENV{WINDIR};
   $OPT{-cmd}=$^X." ".$OPT{-cmd} if $OPT{-cmd} =~ /^[\~\:\\\/\w_\.]+\.pl\s/;

   my $cmd_no_pass=$OPT{-cmd};
   if( $cmd_no_pass=~ /-PASSWORD=/ ) {
   	$cmd_no_pass=~s/-PASSWORD=\w+/-PASSWORD=XXX/;
   } else {
   	$cmd_no_pass=~s/-P\w+/-PXXX/;
   }
   info("(".localtime(time).") ".$cmd_no_pass."\n") if  defined $OPT{-print};

   if( ! $opt_x ) {
	   $OPT{-cmd} .= " |" 		    if defined $ENV{WINDIR};

	   open( SYS2STD,$OPT{-cmd} ) || mylogdie("Unable To Run $OPT{-cmd} : $!\nPath is $ENV{PATH}\n");
	   while ( <SYS2STD> ) {
	      chop;
	      debug("DBG DBG: Returned $_\n");
	      push @rc, $_;
	   }
	   close(SYS2STD);
	   debug("DBG DBG: Command Completed\n");
	} else {
		info("NOEXEC MODE - SKIPPING EXECUTION OF STEP\n");
		info("The Command Would Have Been: $cmd_no_pass\n");
	}

   my($cmd_rc)=$?;
   if( $cmd_rc != 0 ) {
		if( defined $OPT{-err} ) {
	  		info("completed (failed) return_code=$cmd_rc\n");
			my($str)="Error: ".$OPT{-err}."\n";
			$str.="Return Code: $?\n";
	    	$str.="Command: ".$cmd_no_pass."\n";
			$str.="\n\nResults:\n";
	    	mylogdie( $str."\n-- ".join("\n-- ",@rc)."\n");
		} elsif( defined $OPT{-print} ) {
			info( "Return Code:$?\n");
	      info( "Results:\nresults: ".join("\nresults: ",@rc)."\n");
		}
		unshift @rc,"Error: Return Code $?";
		return @rc;
	} else {
		info("Command completed at ".localtime(time)."\n")
			if defined $OPT{-print};
		info( "-- ".join("\n-- ",@rc)."\n" )
			if defined $OPT{-print};
		return @rc;
	}
}

sub check_directory_structure
{
   print "debug: Checking Directory Structure\n" if defined $opt_d;

   #
   # Make sure directory tree is ok locally
   # Make local directories as needed
   #
   die "Error Cant Find Base Directory $CONFIG{BASE_BACKUP_DIR}.\nThis local directory is REQUIRED for the system to function and should exist on the local system prior to runnig this command.\n" unless -d $CONFIG{BASE_BACKUP_DIR};

   if( ! -d $base_bk_dir ) {
      print("Creating Directory $base_bk_dir\n");
      mkdir($base_bk_dir,0775) or mylogdie("Cant Make Directory $base_bk_dir : $!\n" );
   };

   foreach (@required_dirs) {
      next if -d "$base_bk_dir/$_";
      mkdir($base_bk_dir."/$_",0775)
            or die("Cant Make Directory $base_bk_dir/$_ : $!\n");
   }

   die "Error Cant Find Directory $base_bk_dir after mkdir is done\n"
      unless -d $base_bk_dir;
}

#
# Print Header Information
#
sub create_page_header {

   my($header_str)="Backup Scripts:   Run At ".(scalar localtime(time))."\n";
   $header_str.="    $VERSION\n";
   $header_str.="    Command $full_cmd_line\n";
   $header_str.="    Run on hostname=".hostname();
   $header_str.=" as user ".$ENV{USER} if $ENV{USER};
   $header_str.="\n\n" ;

   $header_str.=sprintf( "%-13.13s %-24.24s\n","Job Name:",$opt_J);
   $header_str.=sprintf( "%-13.13s %-24.24s\n","Server:",$CONFIG{SERVER_NAME});
   $header_str.=sprintf( "%-13.13s %-24.24s\n","SYBASE ENV:",$ENV{SYBASE});
   $header_str.=sprintf( "%-13.13s %-s\n","Outdir Path:",$srvr_dir_byclient );
   $header_str.=sprintf( "%-13.13s %-s\n","Ignore Db:",$CONFIG{DATABASE_IGNORE_LIST} );
   $header_str.=sprintf( "%-13.13s %-s\n","SessionLog:",$opt_l);
   $header_str.=sprintf( "%-13.13s %-s\n","ErrorLog:",$opt_e);

   $header_str.=sprintf( "%-13.13s %-s\n","Output To:",$CONFIG{BASE_BACKUP_DIR});
   $header_str.=sprintf( "%-13.13s %-s\n","File Stamp:",$today_val);

   if( defined $opt_f ) {
      $header_str.=sprintf( "%-13.13s %-s\n","Dir (local):",$CONFIG{CLIENT_PATH_TO_DIRECTORY} );
      if( $CONFIG{IS_REMOTE} eq "y" ) {
         $header_str.=sprintf( "%-13.13s %-s\n","Rsh Dir:",$CONFIG{SERVER_DIRECTORY} );
         $header_str.=sprintf( "%-13.13s %-24.24s\n","Rsh Host:",$CONFIG{SERVER_HOSTNAME} );
      }

      $header_str.=sprintf( "%-13.13s %-24.24s ","Purge Keeps:",$CONFIG{NUM_BACKUPS_TO_KEEP});
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Num Stripes:",$CONFIG{NUMBER_OF_STRIPES});

      $header_str.=sprintf( "%-13.13s %-24.24s","Is Remote:",$CONFIG{IS_REMOTE} );
      $header_str.=sprintf( "%-13.13s %-24.24s","Do Compress:",$CONFIG{DO_EXTERNAL_COMPRESS} )
      		unless $CONFIG{SYBASE_COMPRESSION_LEVEL} =~ /^\d$/;
		$header_str.="\n";
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Purge:",$CONFIG{DO_PURGE} );
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Audit:",$CONFIG{DO_AUDIT} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Dbcc:",$CONFIG{DO_DBCC} );
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Bcp:",$CONFIG{DO_BCP} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Upd Stats:",$CONFIG{DO_UPDSTATS});
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Indexes:",$CONFIG{DO_INDEXES} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Dump:",	$CONFIG{DO_DUMP});
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Load:",	$CONFIG{DO_LOAD} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Reorg:",	$CONFIG{DO_REORG} );
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Tran Clear:",$CONFIG{DO_TRUNCLOG_BEFOREDUMP} );
	   $header_str.="UPDATE STATISTICS FLAG: ".$CONFIG{UPD_STATS_FLAGS}."\n";

      if( $CONFIG{SYBASE_COMPRESSION_LEVEL} =~ /^\d$/
      and $CONFIG{SYBASE_COMPRESSION_LEVEL} ne "0" ) {
         $header_str.=sprintf( "%-13.13s Sybase Internal Level %d\n","Compress:",$CONFIG{SYBASE_COMPRESSION_LEVEL} );
      } elsif( $CONFIG{DO_EXTERNAL_COMPRESS} eq "y" ) {
         $header_str.=sprintf( "%-13.13s %-24.24s ","Compress:",$CONFIG{COMPRESS} );
         $header_str.=sprintf( "%-13.13s %-24.24s\n","UnCompress:",$CONFIG{UNCOMPRESS} );
      }

		$header_str.="\n";


		foreach (keys %CONFIG ) {
			next unless /^XFER_/;
			next unless defined $CONFIG{$_};
			next unless $CONFIG{$_} !~ /^\s*$/;
      	$header_str.=sprintf( "%-23.23s %s\n",$_,$CONFIG{$_});
		}

      $header_str.="\n";

   } else {
      $header_str.="\nTransaction Log Dump\n";
   }

	$header_str.="Listing Of Path Config Variables \n";
	$header_str.=" => CLIENT_PATH_TO_DIRECTORY = $CONFIG{CLIENT_PATH_TO_DIRECTORY}\n";
	$header_str.=" => CLIENT_PATH_TO_DUMP_DIR  = $CONFIG{CLIENT_PATH_TO_DUMP_DIR}\n";
	$header_str.=" => CLIENT_PATH_TO_LOG_DIR   = $CONFIG{CLIENT_PATH_TO_LOG_DIR}\n";
	$header_str.=" => SERVER_DIRECTORY         = $CONFIG{SERVER_DIRECTORY}\n";
	$header_str.=" => SERVER_DUMP_DIRECTORY    = $CONFIG{SERVER_DUMP_DIRECTORY}\n";
	$header_str.=" => SERVER_LOG_DIRECTORY     = $CONFIG{SERVER_LOG_DIRECTORY}\n\n";

	$header_str.="Listing Of Defined Paths \n";
	$header_str.=" => srvr_dir_by_dbsvr = $srvr_dir_by_dbsvr \n";
	$header_str.=" => dump_dir_by_dbsvr = $dump_dir_by_dbsvr \n";
	$header_str.=" => log_dir_by_dbsvr  = $log_dir_by_dbsvr \n";
	$header_str.=" => srvr_dir_byclient = $srvr_dir_byclient \n";
	$header_str.=" => dump_dir_byclient = $dump_dir_byclient \n";
	$header_str.=" => log_dir_byclient  = $log_dir_byclient \n\n";

   return $header_str;
}

# do_copy_load($is_full_dump,$source_dir);
#		@cp_files= files to copy
#		if( $CONFIG{XFER_BY_FTP} eq "y" )
#			$ftp_from = new connection to ftphost/SERVER_HOSTNAME
#			cd XFER_SCRATCH_DIR
#			$ftp_to   = new connection to XFER_TO_HOST
#			$ftp_to->cwd(XFER_TO_DIR)
#			foreach (@cp_files) { ftp_from->get(); ftp_to->put(); unlink(); }
#     else
#			foreach (@cp_files) { copy $_, XFER_TO_DIR/...
#
#     foreach pair of xfer databases
#			load_database.pl -SXFER_TO_SERVER -iXFER_TO_DIR/SERVER_NAME.db.type.today
#		foreach @cp_files { ftp_to->delete($_) }
#
#   - source_dir used to calculate the files that you want to fetch/put
#   - you will then fetch to scratch and put to target dir.
sub do_copy_load
{
	my($is_full_dump, $source_dir)=@_;
  	$start_time{REMOTE_SYSTEM_COPY}=time;
  	debug("STARTED do_copy_load($is_full_dump, $source_dir) \n" );
	info( "Copying data for load - Full Dump=$is_full_dump\n-- Source Directory=$source_dir\n");
	my($filetype)="dbdump";

	# if XFER_TO_DB and XFER_FROM_DB both equal * then fill them out
	if( $is_full_dump eq "FALSE" ) {
		$filetype="logdump";
		if( $CONFIG{XFER_FROM_DB} eq "*" and $CONFIG{XFER_TO_DB} eq "*" ) {
			$CONFIG{XFER_TO_DB}=join("|",@tran_db_list);
			$CONFIG{XFER_FROM_DB}=$CONFIG{XFER_TO_DB};
		}
	} else {
		if( $CONFIG{XFER_FROM_DB} eq "*" and $CONFIG{XFER_TO_DB} eq "*" ) {
			$CONFIG{XFER_TO_DB}=join("|",@full_db_list);
			$CONFIG{XFER_FROM_DB}=$CONFIG{XFER_TO_DB};
		}
	}

	# ok check required variables
   if( defined $CONFIG{XFER_TO_SERVER}
   and defined $CONFIG{XFER_TO_DB}
   and defined $CONFIG{XFER_FROM_DB}
   #and defined $CONFIG{XFER_TO_DIR}
   and $CONFIG{DO_DUMP} eq "y" ) {
      info("-- Setting Up Copies\n");
		my($ftp_to,$ftp_from);

		# create expected list of files
		my(@cp_files);
      if( defined $CONFIG{NUMBER_OF_STRIPES}
		and 			$CONFIG{NUMBER_OF_STRIPES}>1
		and 			! defined $opt_t ) {
			debug("Striped Dumps - $CONFIG{NUMBER_OF_STRIPES} stripes\n");
			foreach my $rdb ( split(/[\,\|]/,$CONFIG{XFER_FROM_DB})) {
            debug(" checking $rdb from $CONFIG{XFER_FROM_DB}\n" );
            if( ! defined $full_db_list_hash{$rdb} and $is_full_dump eq "TRUE" ) {
            	debug(" Skipping - not in full_db_list_hash\n");
            	next ;
            }
            if( ! defined $tran_db_list_hash{$rdb} and $is_full_dump ne "TRUE" ){
            	debug(" Skipping - not in tran_db_list_hash\n");
            	next ;
            }

				for (my $stripe=1; $stripe<=$CONFIG{NUMBER_OF_STRIPES}; $stripe++) {
					push @cp_files,$CONFIG{SERVER_NAME}.".".$rdb.".$filetype.".$today_val.".S".$stripe;
				}
			}
		} else {
			debug("UnStriped Dumps - $CONFIG{NUMBER_OF_STRIPES} stripes\n");
			foreach my $rdb ( split(/[\,\|]/,$CONFIG{XFER_FROM_DB})) {
				debug(" checking $rdb from $CONFIG{XFER_FROM_DB}\n" );
            if( ! defined $full_db_list_hash{$rdb} and $is_full_dump eq "TRUE" ) {
            	debug(" Skipping - not in full_db_list_hash\n");
            	next ;
            }
            if( ! defined $tran_db_list_hash{$rdb} and $is_full_dump ne "TRUE" ){
            	debug(" Skipping - not in tran_db_list_hash\n");
            	next ;
            }

				#next if ! defined $full_db_list_hash{$rdb} and $is_full_dump eq "TRUE";
            #next if ! defined $tran_db_list_hash{$rdb} and $is_full_dump ne "TRUE";
				push @cp_files,$CONFIG{SERVER_NAME}.".".$rdb.".$filetype.".$today_val;
			}
		}

		foreach (@cp_files) { info("-- File = $_\n"); }
      my($lunix_login,$lunix_password,$unix_type,$lmethod);
		if( $CONFIG{XFER_BY_FTP} eq "y" ) {
			info("-- Copying These Files\n");

			# if SERVER_HOSTNAME != LOCAL HOST
			# 		connect to system with backup server (SERVER_HOSTNAME)
			# 		copy dump files locally XFER_SCRATCH_DIR

			info("Transfer To Remote System\n");
			info( "   - XFER_BY_FTP: ".$CONFIG{XFER_BY_FTP}."\n");
			info( "   - SOURCE_HOST: ".$CONFIG{SERVER_HOSTNAME}."\n");
			info( "   - SOURCE_DIR: ".$source_dir."\n");
			info( "   - SOURCE_DB: ".$CONFIG{XFER_FROM_DB}."\n");
			info( "   - LOCAL HOST (from hostname()): ".hostname()."\n");
			info( "   - XFER_SCRATCH_DIR:  ".$CONFIG{XFER_SCRATCH_DIR}."\n");
			info( "   - TARGET HOST: ".$CONFIG{XFER_TO_HOST}."\n" );
			info( "   - TARGET DIRECTORY: (".$CONFIG{XFER_TO_DIR}.")\n" );
			info( "   - TARGET_DB: ".$CONFIG{XFER_TO_DB}."\n");
			info( "   - TARGET_SERVER: ".$CONFIG{XFER_TO_SERVER}."\n");

			# its silly but you need to reconnect here because of timeouts
			debug("Creating Another Ftp Connection to $CONFIG{SERVER_HOSTNAME} \n");
			$ftp_from=ftp_connect($ftphost,\%FTPCONFIG, $ftplogin, $ftppass,$ftpmethod);

			debug("source->binary()");
   		$ftp_from->binary();
			debug("source->cwd($source_dir)\n");
         $ftp_from->cwd($source_dir)
				or mylogdie("FTP connection on $CONFIG{SERVER_HOSTNAME} cant cwd to $source_dir");

			# cd on local system
			debug("(local) cd $CONFIG{XFER_SCRATCH_DIR} \n");
			chomp $CONFIG{XFER_SCRATCH_DIR};
			chdir $CONFIG{XFER_SCRATCH_DIR}
				or mylogdie("Cant cd to $CONFIG{XFER_SCRATCH_DIR}: $!\n");

			info( "" );
			info( "... Creating Second FTP Connection To $CONFIG{XFER_TO_HOST} \n" );
      	($lunix_login,$lunix_password,$unix_type)=get_password(-name=>$CONFIG{XFER_TO_HOST},-type=>"unix");
			mylogdie("No Login/Password Available for ($CONFIG{XFER_TO_HOST})\n")
				unless defined $lunix_login and defined $lunix_password;

			my(%args)=get_password_info(-type=>"unix",-name=>$CONFIG{XFER_TO_HOST});
			if( is_nt() ) {
				$lmethod=$args{WIN32_COMM_METHOD} if is_nt();
			} else {
				$lmethod=$args{UNIX_COMM_METHOD};
			}
			mylogdie( "COMM_METHOD NOT DEFINED for SERVER $CONFIG{XFER_TO_HOST}\n" ) unless $lmethod;

			if( $lmethod eq "NONE" ) {	# hmm cant talk
				die "host $CONFIG{XFER_TO_HOST}: METHOD=NONE - aborting\n";
			}

			debug("tgt->login($CONFIG{XFER_TO_HOST},$lunix_login,$lunix_password)\n");
#print "DBGDBG: CONNECTING TO $CONFIG{XFER_TO_HOST}\n";
			$ftp_to=ftp_connect($CONFIG{XFER_TO_HOST},\%FTPCONFIG, $lunix_login, $lunix_password,$lmethod);
			debug("tgt->binary()");
   		$ftp_to->binary();
			debug("tgt->cwd($CONFIG{XFER_TO_DIR})\n");
         $ftp_to->cwd($CONFIG{XFER_TO_DIR})
				or mylogdie("Cant Chdir to $CONFIG{XFER_TO_DIR}");
			info( "... Connection Ok \n" );

			# ok we have connections lets start copying
			foreach my $fl ( @cp_files ) {
				info( "get from $ftphost:$fl\n" );
				$ftp_from->get( $fl, "$CONFIG{XFER_SCRATCH_DIR}/$fl" )
					or mylogdie("Cant Get File $fl from $ftphost : $!\nls on $ftphost returns ".join("\n",sort( $ftp_from->ls()))."\n");
				if( ! -e "$CONFIG{XFER_SCRATCH_DIR}/$fl" ) {
					#use Cwd;
					#my($curdir)=cwd();
					mylogdie("File $fl Not Received after ftp copy from $ftphost ????\nSCRATCH DIR = $CONFIG{XFER_SCRATCH_DIR}\n");
				}
#print "DBGDBG: FILE $CONFIG{XFER_SCRATCH_DIR}/$fl EXISTS\n" if -e "$CONFIG{XFER_SCRATCH_DIR}/$fl";
#die   "DBGDBG: FILE $CONFIG{XFER_SCRATCH_DIR}/$fl EXISTS\n" if ! -e "$CONFIG{XFER_SCRATCH_DIR}/$fl";
				info( "     ... ",-s "$CONFIG{XFER_SCRATCH_DIR}/$fl" , " Bytes Fetched into $CONFIG{XFER_SCRATCH_DIR}\n" );
				info( "put to $CONFIG{XFER_TO_HOST} directory $CONFIG{XFER_TO_DIR}\n" );
				$ftp_to->put("$CONFIG{XFER_SCRATCH_DIR}/$fl",$fl)
					or mylogdie("Cant Put File $fl : $!\nls on $CONFIG{XFER_TO_HOST} returns ".join("\n",sort( $ftp_to->ls()))."\n");
				info( "     Put $fl To Target ".$CONFIG{XFER_TO_HOST}."\n" );
				info( "     Unlink $CONFIG{XFER_SCRATCH_DIR}/$fl\n" );
				unlink "$CONFIG{XFER_SCRATCH_DIR}/$fl"
					or print "Cant unlink $CONFIG{XFER_SCRATCH_DIR}/$fl : $!\n";
			}
			info( ($#cp_files+1)," Files Copied...\n" );
   		undef $ftp_from;
   		chomp $curdir;
   		debug("(local) cd $curdir \n");
			chdir $curdir or die "Cant cd to $curdir: $!\n";
   		info("\n");

		} else {
			if( defined $CONFIG{XFER_TO_DIR} and $CONFIG{XFER_TO_DIR} !~ /^\s*$/){
				info("-- Using Regular Copy of ",($#cp_files+1)," Files XFER_BY_FTP=$CONFIG{XFER_BY_FTP}\n");
				info "-- From Directory=$source_dir\n";
				info "-- To Directory=$CONFIG{XFER_TO_DIR}\n";

				die "Cant Write To XFER_TO_DIR" unless -w $CONFIG{XFER_TO_DIR};
				use File::Copy;
				foreach( @cp_files ) {
					if( ! $opt_x ){ # noexec mode
						die "Hmmm File $_ not in $source_dir\n" unless -r "$source_dir/$_";
	         		info("   -> copying $_\n");
	         		copy("$source_dir/$_",$CONFIG{XFER_TO_DIR}."/".$_)
							or die "File Copy Failed : $!";
					}
				}

			} else {
				infobox("-- Skipping Copy (XFER_BY_FTP=".$CONFIG{XFER_BY_FTP}.")\n\n");
				info("-- Files Stay Local \n");
				if( $CONFIG{XFER_TO_DIR} ) {
					if( defined $opt_t ) {
						$CONFIG{XFER_TO_DIR} = $CONFIG{XFER_TO_DIR}."/logdumps";
					} else {
						$CONFIG{XFER_TO_DIR} = $CONFIG{XFER_TO_DIR}."/dbdumps";
					}
				} else {
					$CONFIG{XFER_TO_DIR}=$source_dir;
				}
			}
		}

  		$end_time{REMOTE_SYSTEM_COPY}=time;
      $start_time{LOAD_DATABASES}=time;

		if( defined $opt_n ) {
   			infobox("\nSkipping Load Database at ".scalar(localtime(time))."\n");
		} else {
   			infobox("Starting Load Database at ".scalar(localtime(time))."\n");
      		info "-- Directory: $CONFIG{XFER_TO_DIR}  \n\n";
		}

		my(@fr_dblist) = split( /[\,\|]/, $CONFIG{XFER_FROM_DB} );
		info "-- From DB List=".join(" ",@fr_dblist)."\n";
		my(@to_dblist) = split( /[\,\|]/, $CONFIG{XFER_TO_DB}   );
		info "--   To DB List=".join(" ",@to_dblist)."\n";

		my($to_syb_login,$to_syb_password) = get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sybase");
		($to_syb_login,$to_syb_password)   = get_password(-name=>$CONFIG{SERVER_NAME},-type=>"sqlsvr")
			unless defined $to_syb_login and defined $to_syb_password and $to_syb_login !~ /^\s*$/ and $to_syb_password !~ /^\s*$/;

		if( ! $opt_n ) {
			mylogdie("No Login Found for server $CONFIG{XFER_TO_SERVER}") unless defined $to_syb_login and $to_syb_login!~/^\s*$/;
			mylogdie("No Password Found For $to_syb_login") unless defined $to_syb_password and $to_syb_password!~/^\s*$/;
		}

		my(@load_msgs);
		my($count)=0;
		while ( $count <= $#fr_dblist ) {
			my($fr_db)=$fr_dblist[$count];
			my($to_db)=$to_dblist[$count];
			$count++;

			if( $is_full_dump eq "TRUE" ) {
				if( ! defined $full_db_list_hash{$fr_db} ) {
					info "Skipping $fr_db As it is not in Full DB List (".join(" ",@full_db_list).")\n";
					next;
				}
			} else {
				if( ! defined $tran_db_list_hash{$fr_db} ) {
					info "Skipping $fr_db As it is not in Tran DB List (".join(" ",@tran_db_list).")\n";
					next;
				}
			}

			info "-- Loading $fr_db from $CONFIG{SERVER_NAME} to $to_db on ",$CONFIG{XFER_TO_SERVER},"\n";

			my($ld_dir)= $CONFIG{XFER_TO_DIR};
			$ld_dir= $CONFIG{XFER_TO_DIR_BY_TARGET}
				if defined $CONFIG{XFER_TO_DIR_BY_TARGET};
			# set it to the dump directory if it is not set
			$ld_dir= $CONFIG{CLIENT_PATH_TO_DIRECTORY} 				unless $ld_dir;

			# Final Check On Options
      	my $cmd = "$curdir/load_database.pl ".
         	" -Y -J$opt_J -U$to_syb_login -S$CONFIG{XFER_TO_SERVER} $COMPRESS_LOAD -P$to_syb_password -l$opt_l -e$opt_e ".
         	" -i".$ld_dir."/".
         	$CONFIG{SERVER_NAME}.".".$fr_db.".$filetype.".$today_val.
         	" -D\"".$to_db."\"";

			$cmd .= " -O" unless $CONFIG{DO_ONLINEDB} eq "y";
			$cmd .= " -R" if $server_type eq "SQL SERVER";
      	$cmd .= " -n$CONFIG{NUMBER_OF_STRIPES}"
         	if defined $CONFIG{NUMBER_OF_STRIPES}
         	and $CONFIG{NUMBER_OF_STRIPES}>1
				and $is_full_dump eq "TRUE";

			$cmd.=" -tk" if $is_full_dump eq "FALSE";
      	$cmd.="\n";

			die "Bad Parameter List\n$cmd\nThis is an internal error pls contact author."
				unless 	defined $opt_J
					and	defined $login
					and 	defined $CONFIG{XFER_TO_SERVER}
					and	defined $password
					and	defined $opt_l
					and	defined $opt_e
					and	defined $to_db
					and	$opt_J	!~ /^\s*$/
					and	$login	!~ /^\s*$/
					and 	$CONFIG{XFER_TO_SERVER}	!~ /^\s*$/
					and	$password	!~ /^\s*$/
					and	$opt_l	!~ /^\s*$/
					and	$opt_e	!~ /^\s*$/
					and	$to_db	!~ /^\s*$/;

      	my $cmd_no_pass=$cmd;
      	$cmd_no_pass =~ s/-P\w+/-PXXX/;
			if( defined $opt_n ) {
     			info("-- (skipping: -n defined) $cmd_no_pass");
			} else {
     			info("-- (executing) $cmd_no_pass");
     			info("Line ",__LINE__," DBGDBG ------------------------ load() ----------------\n");
   			push @load_msgs,run_cmd(-cmd=>$cmd,-print=>1);
   			info("Line ",__LINE__," DBGDBG ------------------------ load() ----------------\n");
			}
		}

		my($load_ok)="NA";
		foreach ( @load_msgs ) {
			if( /Error: Return Code/ ) {
				$load_ok = "FAILED";
				last;
			}
			next unless /^GOOD/;
			info("LOAD SUCCEEDED: $_\n");
			$load_ok="GOOD" if $load_ok eq "NA";
			last;
		}
		$load_ok="GOOD" if $#load_msgs<0;

		# cleanup if needed
		if( defined $ftp_to and ! defined $opt_n and ! $opt_p ) {
			info("   (clean up temporary ftp files on $CONFIG{XFER_TO_HOST}) \n");
			undef $ftp_to;

			# reconnect because full loads may have timed you out
			debug("tgt->login($CONFIG{XFER_TO_HOST},$lunix_login,$lunix_password)\n");
			$ftp_to=ftp_connect($CONFIG{XFER_TO_HOST},\%FTPCONFIG, $lunix_login, $lunix_password,$lmethod);
         $ftp_to->cwd($CONFIG{XFER_TO_DIR}) or info "ERROR: Cant Chdir to $CONFIG{XFER_TO_DIR}";

			foreach( @cp_files ) {
				info("  (deleting) $CONFIG{XFER_TO_DIR}/$_\n");
				$ftp_to->delete($_)	or info("Cant delete $_ $!");
			}
			undef $ftp_to;
			info("  clean up temporary ftp files completed\n");
		}

      mylogdie("LOAD COMMAND FAILED\n".join("\n--",@load_msgs)."\nLOAD COMMAND FAILED")
			if $load_ok ne "GOOD";

		$end_time{LOAD_DATABASES}=time;

   } else {
      info("Configuration Error For Load Part of These Dumps\n");
      info("MUST DEFINE XFER_TO_SERVER\n") 	unless defined $CONFIG{XFER_TO_SERVER};
      info("MUST DEFINE XFER_TO_DB\n") 		unless defined $CONFIG{XFER_TO_DB};
      info("DO_DUMP!=y\n") 			unless $CONFIG{DO_DUMP} eq "y";
      info("MUST DEFINE XFER_FROM_DB\n") 	unless defined $CONFIG{XFER_FROM_DB};
    #  info("MUST DEFINE XFER_TO_DIR\n") 	unless defined $CONFIG{XFER_TO_DIR} ;
   }
}

sub mylogdie {
	info("Line ",__LINE__," DBGDBG ------------------------ mylogdie() ----------------\n");
	saveMlpSaveBackupJobRunInfo("ERROR",join("",@_));
	info("Line ",__LINE__," DBGDBG ------------------------ mylogdie() ----------------\n");
	logdie(@_);
	info("Line ",__LINE__," DBGDBG ------------------------ mylogdie() ----------------\n");
	exit(1);
}
sub exitout {
	my($msg,$rc,$state)=@_;
	if( $rc==0 ) {
		MlpBatchJobEnd();
		# MlpBatchJobEnd();
	} else {
   	warning($msg);
   	MlpBatchJobErr($msg);
		# MlpBatchErr( -message =>  $msg);
	}
	saveMlpSaveBackupJobRunInfo($state,$msg);
#	MlpHeartbeat(-monitor_program   =>  "backup.pl",
#                -system            =>  $CONFIG{SERVER_NAME},
#                -subsystem         =>  $subsystem,
#                -state             =>  $state,
#                #-message_text      =>  $tran_log_text." ".$msg,
#                -message_text      =>  $msg,
#					 -batchjob			  =>	1
#                # -event_time      =>  $EVENT_TIME,
#                # -debug           =>  $DEBUG,
#                # -event_id        =>  $EVENT_ID,
#                # -message_value   =>  $MESSAGE_VALUE,
#                # -document_url    =>  $DOCUMENT_URL,
#	);
	exit($rc);
}

__END__

=head1 NAME

backup.pl - master driver for backup scripts

=head2 USAGE

backup.pl -S SERVER -l|-f

=head2 DESCRIPTION

backup.pl is the main driver script for your operations and will appropriately call the other scripts in this package
to get its work done.  It is probably the only program you will add to your scheduler. It reads the configuration
file and process servers based on the options you have specified. Operations are performed in the following order:

        Purge Old Dumps (if necessary use rsh)
        Dbcc Databases
        Dump Tran Log
        Full Dump
        Check for Required Database Loads
        Update Statistics
        Run Audit
        Optional Table Level Bcps
        Compress The Dumps (if necesary use rsh)
        Rebuild Indexes

=head2 USAGE

Basic Usage:

 Usage: backup_pl -JJOB -t|-f [-dh]

Advanced Usage:

Usage: backup_pl -JJOB -t|-f [-dh] -mmode

This is THE Master backup script.
This script performs appropriate tasks as defined by maintenance plans (specified with -JJOB)
which are defined in configure.cfg (located in backup scripts directory or one level above it).

 -x noexec mode                         (purges will exec but other stuff wont)
 -n copy but dont load for logshipping
 -t for transaction log dumps only
 -f for full backups    (default).
 -m mode = SSH|FTP|RSH  (override communication mode)
 -d is debug mode
 -b batchid (for alarming)
 -h is html output.
 -s skip transfer and load step
 -DDB_LIST (pipe separated) restriction on databases
 -RlpdDuabCir will run only the (l)oad, (p)urge, (d)ump, (D)bcc, (r)eorg,
      (u)pdate stats, (a)udit, (b)cp, (C)ompress, and (i)ndex steps.
 -p dont purge tranlogs

=head2 SYNOPSIS

This is the master driver for the backup scripts.  It relies on the Job being defined in the configure.cfg (located in
backup scripts directory or one level above it).  It follows instructions and performs the appropriate tasks as needed.
-t for transaction log dumps only, -f for full backups. -d is debug mode and -h is html output.

You can also specify -DDB_LIST to restrict databases operations are performed on further.

=head2 Notes

A transaction log dump (with no_log) is always done prior to the full dump.

tempdb and model are NEVER dumped

Optionally Compress the dumps

Update Statistics and Recompile will never work on tempdb/model

Audit Reports require extended procs to be installed

On production servers, you will also wish to run backup.pl -l. This does a transaction log dump on SOME of your databases. The databases that are dumped are those that are "production", which means that "trunc. log on checkpoint" is off and "select into/bcp" is also off. Of course, master, model, and tempdb are never transaction log dumped.  Log dumps are placed in dated files in the .../logdumps sub directory.

If you have pseudo junk databases on your system, you can ignore them with DATABASE_IGNORE_LIST - but maybe you want to
still run update stats and dbcc etc... you can use the -r option for this - which works on the Opposite list.

=cut

