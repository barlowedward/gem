#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
use strict;
use Getopt::Std;
use File::Basename;
use Repository;
use MlpAlarm;
use CommonHeader;

$| = 1;

sub usage
{
	print @_;
	print "parse_hosts.pl -FFile -DDir -hd -OFile -BBatchDesignator\n";
	exit 1;
}

use vars qw($opt_B $opt_F $opt_D $opt_h $opt_d $opt_O);
sub error_out {
   my($msg)=join("",@_);
   $msg = "BATCH RptWin32Hosts: ".$msg;

   MlpBatchJobEnd(-EXIT_CODE=>"ERROR", -EXIT_NOTES=>$msg);

   MlpEvent(
      -monitor_program=> $opt_B || "RptWin32Hosts",
      -system=> $opt_B || "",
      -message_text=> $msg,
      -severity => "ERROR"
   );
   die $msg;
}

usage("Bad Parameter List") unless getopts('O:F:D:hdB:');

my(@files);
if( defined $opt_F ) {
	push @files,split(/,/,$opt_F);
} else {
	my($dir) = $opt_D || get_gem_root_dir()."/data/system_information_data/hosts";
	error_out( "Cant find $dir - try passing it\n" ) unless -d $dir;
	print "Reading $dir\n" if $opt_d;
	@files=grep(/hosts$/i,dirlist($dir));
}

if( defined $opt_O ) {
	open(OUTFILE,">$opt_O") or error_out( "parse_hosts.pl: Cant write Out File $opt_O\n" );
}

MlpBatchJobStart( -BATCH_ID=>$opt_B||"RptHosts" );

myprint( "<h1>" ) if $opt_h;
myprint( "Win32 Hosts Report\n" );
myprint( "</h1>" ) if $opt_h;
myprint( "parse_hosts.pl Version 1.0\n" );
myprint( "<br>" ) if $opt_h;
myprint( "Printed at ".localtime(time)."\n" );
myprint( "<br>" ) if $opt_h;
myprint( "The following report lists entries found in the hosts files from your windows servers.\n");
myprint( "<br>" ) if $opt_h;
myprint( "Servers should typically use DNS - so treat any lines other than 'localhost' on 127.0.0.1 as suspicious.\n");
myprint( "<br>" ) if $opt_h;

if( defined $opt_d) {
	myprint( "Called in debug mode\n" );
	myprint( "<br>" ) if $opt_h;
}

myprint( "<table border=1>\n" ) if $opt_h;
foreach my $fname (sort @files ) {
	myprint( "Processing ",basename($fname),"\n" ) if defined $opt_d;
	open(X,$fname);
	my($doesntcount)="FALSE";
	while( <X> ) {
		chomp;
		next if /^#/ or /^\s*$/;
		my(@d)=split;
		unshift @d,basename($fname);
		if( $opt_h ) {
			myprint( "<TR><TD>",join("</TD><TD>",@d),"</TD></TR>\n");
		} else {
			myprint( join(",",@d),"\n");
		}
	}
}
myprint( "</table>\n" ) if $opt_h;
MlpBatchJobEnd();

close(X);
exit(0);

sub myprint
{
	if( defined $opt_O) {
		print OUTFILE @_;
	} else {
		print @_;
	}
}

sub dirlist
{
        my($dir,$recurse) = @_;
        opendir(DIR,$dir) || error_out("Can't open directory $dir for reading\n");
        my(@files)=readdir(DIR);
        closedir(DIR);

        my(@out);
        foreach (@files) {
               next if /^\./;
					next if /^install.sh$/ or /^weather$/;
               if( -d $dir."/".$_ and $recurse ) {
                        push @out,dirlist($dir."/".$_,1);
               } else {
                        push @out,$dir."/".$_;
               }
        }
        return @out;
}

1;

__END__

=head1 NAME

parse_hosts.pl - Parse hosts files that have been collected

=head2 USAGE

	parse_hosts.pl -FFile -DDir -hd -OFile -BBatchDesignator

=cut
