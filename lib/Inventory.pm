package  Inventory;

use DBIFunc;
#
# This file best viewed at tabstop=3
#
use   vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use   strict;

# the following two must be set for diag messages
my(@system_table_fields);
my($noprint_diags)=1;      # set to 0 if you want added prints
my($print_queries)=0;
my($run_check_database);	# if set will run check_database() 
my($DBGDBG);

my($use_html)="TRUE";		# some programs dont want it... some do...

# INSTALLATION INSTRUCTIONS
#    - edit the setup variables section below for the name and login/password for your database
#    - modify the BEGIN Block  to point to your sybase
#
# The next line contains the public login for the repository - this is a public account and it
# only has permissions on the appropriate tables as per the setup instructions for the database.
#
my($server,$login,$password,$db)=("CTSYBMON","gemalarms","gemalarms","gemalarms");

# THE DONT_USE_MODULE VARIABLE SHOULD BE SET TO 1 IF YOU DO NOT WISH TO USE THE MODULE
my($DONT_USE_MODULE);

#
# you should also add any necessary environment variables in the begin block.
#
BEGIN {
	if( -d "/export/home/sybase" and ! defined $ENV{SYBASE} ) {
		$ENV{SYBASE}="/export/home/sybase";
	} elsif( -d "/apps/sybase" and ! defined $ENV{SYBASE} ) {
		$ENV{SYBASE}="/apps/sybase";
	}
}
#################### END SETUP VARIABLES ################################

use   Carp qw(confess croak);
use  	Do_Time;
use	CommonFunc;
use 	DBIFunc;
use 	File::Basename;
use 	Sys::Hostname;

require  Exporter;
@ISA     = qw(Exporter);
@EXPORT  = qw(InvUpdSystem InvGetSystem InvYpcatHosts  InvReportDisplay InvUpdateAttributeDictionary InvPromote
InvGetSACred InvSetSACreds InvGetSACredByDSNandMonhost InvGetAllSACreds dump_it _webpage_output_print
InvRelateTwo InvGetTypeByName InvUpdateBusinessSystem INV_setup_cores_report INV_sethtmlforms INV_gethtmlforms );

$VERSION = '1.0';

#############################
# GLOBAL VARIABLES          #
#############################
my($this_hostname);
my($i_am_initialized)="FALSE";
my($quiet);
my($monitor_program,$system,$debug);
my($connection_type);
my(%condense_args);


##############################################################################
# METHOD ARGUMENTS                                                           #
# Set to 1 if Required, optional if 0.  key is func:value	                 #
# all method arguments must be in this array which is used for validation    #
##############################################################################
my(%OK_OPTS)= (
	"InvUpdSystem:-debug"						=> 0,
	"InvUpdSystem:-replace"						=> 0,
	"InvUpdSystem:-system_name"				=> 1,
	"InvUpdSystem:-attribute_category"		=> 1,
	"InvUpdSystem:-attributes"					=> 0,
	"InvUpdSystem:-delete"						=> 0,
	"InvUpdSystem:-clearattributes"			=> 0,
	"InvUpdSystem:-production_state"			=> 0,
	"InvUpdSystem:-is_dba_monitored"			=> 0,
	"InvUpdSystem:-is_infra_monitored"		=> 0,
	"InvUpdSystem:-is_spare"					=> 0,
	
	"InvGetSystem:-whereclause"	=> 1,
	"InvGetSystem:-bsystem"						=> 0,
	"InvGetSystem:-getattributes"	=> 0,
	"InvGetSystem:-simple"	=> 0,
	"InvGetSystem:-attribute_category"	=> 0,

	"InvReportDisplay:-col_headers"		=> 1,
 	"InvReportDisplay:-data"				=> 1,

);

my($connection);
sub initialize {
	my(%args)=@_;

	if( $DONT_USE_MODULE ) {
		if( ! $quiet ) {
			print "-------------------------------------------\n";
			print "| PLEASE NOTE THAT THE SYSTEM IS DISABLED |\n";
			print "-------------------------------------------\n";
		}
		$i_am_initialized = "FAILED";
		$quiet=1;
		return 0;
	}
	print "Initializing Repository Connection\n" if $args{-debug};

	if( $args{-debug} ) {
		# print "DBG DBG: INIT( ";
		foreach( keys %args ) {
			print " ".$_."=>".$args{$_}." " unless $_ eq "-NL";
		}
		print ")\n",$args{-NL};
	}

	if( $server eq "" ) {
		if( ! defined $quiet ) {
			print "******************************************************\n";
			print "* Monitoring Error: GEM MONITORING NOT INITIALIZED!!! \n";
			print "* Please 'Save Configuration Changes' in the configuration utility.\n";
			print "* This may indicate that you have not run upgrade.pl after downloading code\n";
			print "* This is a fatal error - aborting.\n";
			print "* If this is a legitimate error please contact GEM Support\n";
			print "******************************************************\n";
			print STDERR "Monitoring Error: GEM IS NOT INITIALIZED!!! \nIf You Are Running From The Configuration Utility, you need to save changes.\nIm sorry to exit here but GEM needs to be initialized.\nIf this is a legitimate error please contact GEM Support\n";
			print "******************************************************\n";
			confess("TRACE");
			exit(1);
		}
		$i_am_initialized = "FAILED";
		return 0;
	}
	$quiet=$args{-quiet} unless $quiet;
	$this_hostname=hostname();

	if( defined $args{-system} ) {
		$system=$args{-system};
	} else {
		$system=hostname();
	}

	if( is_nt() ) {
		$connection_type="ODBC";
	} else {
		$connection_type="Sybase";
	}

	$debug=$args{-debug} if $args{-debug};
	print "dbi_connect(-srv=>$server, -login=>$login, -password=>$password,
			-type=>$connection_type, -debug=>$args{-debug},
	  		-nosybaseenvcheck =>1,	-connection=>1, 	-die_on_error=>0)\n",$args{-NL}	if $args{-debug};

	my($rc) = dbi_connect(-srv=>$server, -login=>$login, -password=>$password,   -nosybaseenvcheck =>1,
		-type=>$connection_type, -debug=>$args{-debug}, -connection=>1, -die_on_error=>0);
	if( ! $rc ) {
		print "dbi_connect() Failed\n",$args{-NL} if $args{-debug};
		if( ! defined $quiet ) {
			print STDERR "******************************************************<br>\n";
			print STDERR "* Error: Cant Connect To Server -  Marking Connection Failed<br>\n";
			print STDERR "* Unable to connect to $connection_type server $server as login $login password $password from Host=".hostname()."<br>\n";
			print STDERR "* PERL=$^X <br>\n";
			print STDERR "* SYBASE=$ENV{SYBASE}<br>\n";
			print STDERR "* srv=$server<br>\n";
			print STDERR "* login=$login<br>\n";
			print STDERR "* password=$password<br>\n";
			print STDERR "* type=$connection_type<br>\n";
			print STDERR "* error number=".$DBI::err."<br>\n";
			print STDERR "* errstr=".$DBI::errstr."<br>\n";
			print STDERR "******************************************************<br>\n";
		}
		$i_am_initialized = "FAILED";
		return 0;
	};
	$connection=$rc;
	dbi_set_mode("INLINE");

	print "Connected as $login / xxxx - Trying to use $db\n",$args{-NL} if $args{-debug};
	$rc=dbi_use_database($db,$connection);
	print "Use Db Failed to Use $db\n" if $rc == 0;
	return 0 if $rc == 0;

 	print "looks good... System Initialization Successful\n",$args{-NL} if $args{-debug};
	$i_am_initialized="TRUE";
	return 1;
}

sub validate_params
{
	my($func,%OPT)=@_;

	# check for bad keys
	foreach (keys %OPT) {

		next if defined $OK_OPTS{$func.":".$_};
		next if /-debug/;

		foreach my $x (keys %OPT) {
			print "validate error: arg $x => $OPT{$x} $OPT{-NL} \n";
		}

		# did you mix up name=>x with -name=>x
		croak( "ERROR: Incorrect $func Arguments - Change $_ argument to -".$_."\n" )
			if defined $OK_OPTS{$func.":-".$_};

		croak( "ERROR: Function $func Does Not Accept Parameter Named $_\n" );
	}

	# check for missing required keys
	foreach (keys %OK_OPTS) {
		next unless /^$func:/;
		next unless $OK_OPTS{$_} == 1;
		$_=~s/$func://;
		next if defined $OPT{$_};
		my($n) = "Passed Args=|";
		foreach ( keys %OPT ) { $n.=$_."|"; }

		confess( "ERROR: Function $func Requires Parameter Named |$_|\n|$n|\n" );
	}
	return %OPT;
}

sub _getcolumns_in_table {
	my($tbl)=@_;
	my(@realrc);
	foreach ( _querywithresults( "select name from syscolumns where id=object_id('". $tbl. "') order by colid",1 ) ) {
		push @realrc,dbi_decode_row($_);
	}
	return @realrc;
}

sub _webpage_output_print {
	print "<FONT COLOR=BLUE>",@_,"</FONT><br>\n";
}

sub _diagprint {
	if( $use_html ne "TRUE" ) {
		print @_,"\n";
		return;
	}
	print "<FONT COLOR=GREEN>",@_,"</FONT><br>\n";
}
sub _quote {
	my($txt)=@_;
	return dbi_quote(-text=>$txt, -connection=>$connection);
}

sub _querywithresults {
	my($query, $dont_show_query)=@_;
	$dont_show_query=1 unless defined $dont_show_query;
	initialize() unless $i_am_initialized eq "TRUE";

	print $query,"\n" unless $dont_show_query;
	my(@rc)= dbi_query(
		-db=>$db,
		-query=>$query,
		#-debug=>$Margs{-debug},
		-connection=>$connection,
		-die_on_error=>0 );
	return @rc;
}

sub _querynoresults {
	my($query, $dont_show_query)=@_;
	$dont_show_query=1 unless defined $dont_show_query;
	initialize() unless $i_am_initialized eq "TRUE";

	print $query,"\n" unless $dont_show_query;
	foreach ( dbi_query(
		-db=>$db,
		-query=>$query,
		#-debug=>$Margs{-debug},
		-no_results => 1,
		-connection=>$connection,
		-die_on_error=>0 )
	) {
		my($rc)=join("\n--> ",dbi_decode_row($_));
		my($msg)="ERROR - query should not have returned data\n --> query=$query\n--> ".$rc."\n";
		# MlpBatchJobEnd(-EXIT_CODE => "FAILED", -EXIT_NOTES =>$msg) 	unless $query=~/INV_batchjob_/;
		die  $msg;
	}
}

sub InvYpcatHosts {
	my(%args)=@_;
	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	if(  $^O =~ /MSWin32/ ) {
		warn "Can not run InvypcatHosts() on win32\n";
		exit();
	}
	_querynoresults("truncate table INV_aliases",$noprint_diags);

	my(%dup_check);
	print "Running ypcat hosts\n";
	my($NUM_HOSTS)=0;
	my($NUM_IP)=0;
	my(%IP_FOUND);
	open(CMD,"ypcat hosts |") or die "Cant run ypcat hosts $!\n";	
	while(<CMD>) {
		chomp;
		next if $dup_check{$_};		# ignore duplicates
			
		$dup_check{$_}=1;
		my(@x)=split;			# ip nm1 nm2 nm3
		my($ip)=shift @x;
		if( ! $IP_FOUND{$ip} ) { $NUM_IP++; $IP_FOUND{$ip}=1; }

		foreach (@x) {
			$NUM_HOSTS++;
			print "." if $NUM_HOSTS % 1000 == 0;
			print " $NUM_HOSTS hosts / $NUM_IP ip addresses loaded \n" if $NUM_HOSTS % 10000 == 0;
			my($query)="insert INV_aliases values (".
				_quote($ip).",".
				_quote(lc($_)).",".
				_quote("dnsalias").",getdate(),'N' )";
			_querynoresults($query,$noprint_diags);
		}
	}
	print "\n\n";
	print $NUM_HOSTS, " host aliases Loaded\n";
	print $NUM_IP,    " ip addresses Loaded\n";
	close(CMD);
}

my($promotetime);
my($stepnum)=1;
#sub ShowTimestamp {
#	return unless $promotetime;
#	print "-- time=".do_diff(time-$promotetime)."\n";
#}	
sub ST {
	return unless $promotetime;
	return "-- [ step ".$stepnum++." @ ".do_diff(time-$promotetime)."] ";
}	

sub check_database {
	my($line)=@_;
	return unless $run_check_database;
	
	print ST()."Diagnostic Data Check\n";
   my($q)="Select distinct system_id,system_name,system_type,count(*) from #ATTS 
   where system_name='ADSRV026' or  system_name='PLATINUM'
   group by system_name,system_type
   order by system_name,system_type";
   
   my($cnt)=0;
   foreach( _querywithresults($q,1) ) {
	   my(@x)=dbi_decode_row($_);
	   # print "   > Line $line: ",join('|',@x),"\n";
	   printf "  $line: #ATTS: id=%4d nm=%20s ty=%20s cnt=%4s \n",$x[0],$x[1],$x[2],$x[3];
	   $cnt++;
	   die "FAILED CHECK > 100 rows" if $cnt > 100;
	}
		
	my($q)="select system_name,system_type from INV_system where system_name='ADSRV026' or system_name='PLATINUM'";
	foreach( _querywithresults($q,1) ) {
	   my(@x)=dbi_decode_row($_);
	   print "   > INV_SYSTEM: ",join('|',@x),"\n";
	}	
	print "\n";
	
	my($q)="select system_name,system_type,count(*) 
from  INV_system_attribute 
where system_name='ADSRV026' or system_name='PLATINUM'
group by system_name,system_type
order by system_name,system_type";
  
	foreach( _querywithresults($q,1) ) {
	   my(@x)=dbi_decode_row($_);
	   print "   > ATTRIBUTES: ",join('|',@x),"\n";
	}	
	print "\n";	
	
	my($q)="select system_name,system_type,attribute_name,attribute_value 
from  #ATTS 
where (system_name='ADSRV026' or system_name='PLATINUM')
and   attribute_name like 'asset%'
order by system_name,system_type,attribute_name";  
	foreach( _querywithresults($q,1) ) {
	   my(@x)=dbi_decode_row($_);
	   print "   > #ATTS: ",join('|',@x),"\n";
	}	
	print "\n";
	
	my($q)="select system_name,system_type,attribute_name,attribute_value 
from  INV_system_attribute 
where (system_name='ADSRV026' or system_name='PLATINUM')
and   attribute_name like 'asset%'
order by system_name,system_type,attribute_name";
  
	foreach( _querywithresults($q,1) ) {
	   my(@x)=dbi_decode_row($_);
	   print "   > ATTRIBUTES: ",join('|',@x),"\n";
	}	
	print "\n";
}

sub InvPromote {
   my($dont_show_query,$dont_update_tables)=@_;
   $promotetime=time unless $dont_show_query;
	$use_html = "FALSE";	# only place we do this 

# runs a full promotion...
#
# Get List of Active Priorities in @pri
#
   print "\n-- INVPromote() - v1.0\n--\n";
   my $t = localtime;
   print "-- Run at $t\n\n";
   #print "-- Fetch The various priorities into \@pri\n";
   #my($q)="select distinct compute_priority from INV_attribute_dictionary
   #where compute_priority is not null and compute_priority!=0 
   #order by compute_priority";
   #my(@pri);
   #foreach ( _querywithresults($q,$dont_show_query) ) {
   #   my(@x)=dbi_decode_row($_);
   #   push @pri,$x[0];
   #}
   #print ST()."Fetched Priorities: ",join(" ",@pri),"\n\n";
	
#
# CREATE #ATTS WITH ADDL FIELDS FROM FEEDS - is_new_attribute=1 & compute_priority= -1
#
   
	print ST()."Create Temp Table #ATTS with all attributes for feeds\n"; 
   my($q)="select distinct a.*,
   is_new_attribute=1, compute_priority= -1, 
   ipaddr=convert(varchar,''), system_id=convert(varchar(30),'')
   into    #ATTS
   from    INV_system_attribute a, INV_constant b
   where   system_type = value   and name = 'FEED_TYPE_LU' \n";
   # where system_type not in ('sybase','sqlsvr','oracle','mysql','unix','win32servers','dnsalias')";   
   _querynoresults($q,$dont_show_query);

	check_database(__LINE__);  # quick custom db check for whatever you want	
	
   print ST()."Creating Where Clause\n";
   $q="select value from INV_constant where name = 'SYSTEM_TYPE_LU'";      
   my($systypes_where)='(';
   foreach ( _querywithresults($q,$dont_show_query) ) {
      my(@x)=dbi_decode_row($_);
      $systypes_where .= "'".$x[0]."',";
   }
   $systypes_where =~ s/,$/)/;
   print ST()."systypes_where = $systypes_where\n";
   

# Set compute_priority=0 for atts existing in the dictionary
   $q="update #ATTS
   set     compute_priority=a.compute_priority
   from    #ATTS p, INV_attribute_dictionary a
   where   p.system_type      = a.system_type
   and     p.attribute_name   = a.attribute_name
   and     a.compute_priority>=1\n";   
   _querynoresults($q,$dont_show_query);
   
   print ST()."Show Attributes Missing Dictionary Entries\n";
   $q="select distinct system_type,attribute_name from #ATTS  where compute_priority= -1";
   
   my(@hd)=("SysType","AttName");
   my($hdr)="-- Attributes Missing Dictionary Entries\n-- \n";
   my($i)=0;
   foreach ( _querywithresults($q,$dont_show_query) ) {
   	$i++;
      my(@x)=dbi_decode_row($_);
      if($hdr) { 
      	print $hdr; 
      	printf "-- %23.23s %23.23s\n",$hd[0],$hd[1];      
   		$hdr=''; 
   	}
      printf "-- %23.23s %23.23s\n",$x[0],$x[1] unless $i>20;      
   }
   if( $hdr eq "" ) {
   	print "\nOutput Limited To 20 Rows\n" if $i>20;
   	print "Can not continue - $i Attributes Missing Data Dictionary Entries Found\n";
   	print "Please check the web site and run the Feed Attributes Page.\n";
   	print "Simply Running that page updates the attributes but you may want to make them sane.\n";
   	exit(0);
   }
   
   print "-- NO ATTS MISSING DICTIONARY FOUND!\n" if $hdr ne "";

# We only promote stuff that has a priority - delete the rest
   print ST()."Delete All Fields W/O Dictionary Entries\n";
   $q="DELETE  #ATTS where compute_priority= -1 or compute_priority=0\n";   
   _querynoresults($q,$dont_show_query);

	check_database(__LINE__);  # quick custom db check for whatever you want	
   
   print ST()."Update Category\n";
   $q="update #ATTS set attribute_category=isnull(attribute_category,system_type)";   
   _querynoresults($q,$dont_show_query);
   
# update the compute into and compute into type fields of all your fields!
   print ST()."Promote\n";
   $q="update #ATTS
   set    attribute_name=a.compute_into_name, system_type=a.compute_into_type
   from   #ATTS p, INV_attribute_dictionary a
   where  p.system_type=a.system_type
   and    p.attribute_name=a.attribute_name
   and    a.compute_priority>=1\n";   
   _querynoresults($q,$dont_show_query);

#   foreach ( @pri ) {
#      print ST()."Updating Priority $_ rules\n";
#      $q = "update #ATTS
#   set attribute_name=a.compute_into_name, system_type=a.compute_into_type
#   from #ATTS p, INV_attribute_dictionary a
#   where a.compute_priority = $_
#   and  p.system_type=a.system_type
#   and  a.compute_priority is not null
#   and  a.compute_into_name is not null
#   and  a.compute_into_type is not null
#   and  p.attribute_name=a.attribute_name";
#      
   print ST()."Promote Again\n";
   _querynoresults($q,$dont_show_query);
#   }

	check_database(__LINE__);  # quick custom db check for whatever you want		   

   print ST()."Change server case to lower case \n";
   $q = "update #ATTS set system_name=lower(system_name)\n";
   _querynoresults($q,$dont_show_query);

sub get_attvalue_string {
	my($typ)=@_;
	my($q)="select value from INV_constant where name='".$typ."'";
	my($s)=" ( ";
	my($cnt)=0;
	foreach( _querywithresults($q,$dont_show_query) ) {
		$cnt++;
   	my(@x)=dbi_decode_row($_);
   	$s.= "'".$x[0]."',";
   }	
   die 'NO ATT VALUE '.$typ unless $s =~ /,$/;
   $s =~ s/,$/)/;
   return $s;
}

   print ST()."Update Asset Tag Based on Key $_\n";   
   $q="update #ATTS set system_id = p.attribute_value
   from    #ATTS a, #ATTS p, INV_constant v
   where   p.system_name=a.system_name
   and     p.system_type=a.system_type
   and     p.attribute_name in ".get_attvalue_string('FEED_TYPE_ASSETTAG');
   
    _querynoresults($q,$dont_show_query);
   
   check_database(__LINE__);  # quick custom db check for whatever you want	
   
   print ST()."Update EXISTS computers by system_id\n";
   $q="update #ATTS set system_type=a.system_type
   from    #ATTS p, INV_system a
   where   p.system_id=a.system_id
   and     p.system_type='EXISTS'
   and     a.system_type in ('unix','win32servers')\n";   
   _querynoresults($q,$dont_show_query);
      
   print ST()."Update EXISTS computers by name match to existing unix/win\n";
   $q = "update #ATTS set system_type=a.system_type
   from    #ATTS p, INV_system a
   where   p.system_name=lower(a.system_name)
   and     p.system_type='EXISTS'
   and     a.system_type in ('unix','win32servers')\n";   
   _querynoresults($q,$dont_show_query);

	print ST()."Update EXISTS computers by name match to new unix/win\n";
   $q = "update #ATTS set system_type=a.system_type
   from    #ATTS p, #ATTS a
   where   p.system_name=lower(a.system_name)
   and     p.system_type='EXISTS'
   and     a.system_type in ('unix','win32servers')\n";   
   _querynoresults($q,$dont_show_query);
   
   check_database(__LINE__);  # quick custom db check for whatever you want	
   
   print ST()."Set ipaddr from yp\n";
   $q = "update    #ATTS set ipaddr=ip_address
   from    #ATTS a, INV_aliases b
   where   lc_system_name=system_name\n";   
   _querynoresults($q,$dont_show_query);

    print ST()."Update EXISTS if its an alias\n";
   $q = "update  #ATTS
   set     system_name=c.system_name, system_type=c.system_type
   from    #ATTS a, INV_aliases b, INV_system c
   where   a.system_type='EXISTS'
   and     a.ipaddr!=''
   and     a.ipaddr=b.ip_address
   and     b.lc_system_name != a.system_name
   and     b.lc_system_name   = lower(c.system_name)
   and     c.system_type in ('unix','win32servers')    -- only computers\n";   
   _querynoresults($q,$dont_show_query);

   #print ST()."Diagnostic Save\n";   
   #_querynoresults("if exists ( select * from tempdb..sysobjects where name='BARLOW_TMP3' ) drop table tempdb..BARLOW_TMP3",$dont_show_query);   
   #_querynoresults("select * into tempdb..BARLOW_TMP3 from #ATTS",$dont_show_query);
   #print "\n";

   print ST()."Delete EXISTS - its not a name match with a computer nor a dnsalias\n";
   $q = "delete #ATTS where     system_type='EXISTS'\n\n";   
   _querynoresults($q,$dont_show_query);

# update is_new_attribute if its a new update
#   print ST()."Setting is_new_attribute\n";
#   $q = "   update #ATTS set is_new_attribute=0
#   from #ATTS c, INV_system a
#   where c.system_type=a.system_type
#   and    c.system_name=lower(a.system_name)
#   and    a.system_type in ('sybase','sqlsvr','oracle','mysql','unix','win32servers','dnsalias')";
#   _querynoresults($q);

#   print ST()."Setting is_new_attribute=0 for existing atts (case equal)\n";
#   $q = "update #ATTS set is_new_attribute=0
#   from     #ATTS p, INV_system_attribute a
#   where    p.system_type in $systypes_where
#   and      a.system_type in $systypes_where
#   and      p.system_type=a.system_type   
#   and      p.system_name=a.system_name
#   and      p.attribute_name=a.attribute_name
#   and      is_new_attribute=1\n";   
#   _querynoresults($q,$dont_show_query);
#
#   print ST()."Setting is_new_attribute=0 for existing atts (case missmatch)\n";
#   $q = "update #ATTS set is_new_attribute=0
#   from     #ATTS p, INV_system_attribute a
#   where    p.system_type in $systypes_where
#   and      a.system_type in $systypes_where
#   and      p.system_type=a.system_type   
#   and      p.system_name=lower(a.system_name)
#   and      p.attribute_name=a.attribute_name
#   and      is_new_attribute=1\n";   
#   _querynoresults($q,$dont_show_query);

    print ST()."Deleting Non Primary Promotions\n";
   $q="delete #ATTS where system_type not in $systypes_where\n";   
   _querynoresults($q,$dont_show_query);
   
   my(@h)=qw(AttName SysName SysType1 Val1 SysType2 Val2);
   $q = "select distinct a1.attribute_name,a1.system_name,a1.system_type,a1.attribute_value,a2.attribute_value
   from   #ATTS a1, #ATTS a2
   where  a1.system_type=a2.system_type
   and    a1.system_name=a2.system_name
   and    a1.attribute_name=a2.attribute_name
   and    a1.attribute_value!=a2.attribute_value
   and    ( a1.compute_priority < a2.compute_priority or a1.system_id<a2.system_id or a1.mod_date < a2.mod_date  )
   order by a1.system_name,a1.attribute_name\n";   
   print ST()."Reporting on Duplicates\n";
   my($hdr)="--   ",join("\t",@h),"\n";
   my($dupcnt)=0;
   foreach ( _querywithresults($q,$dont_show_query) ) {
      my(@x)=dbi_decode_row($_);
      $dupcnt++;
      if($hdr) { print $hdr; $hdr=''; }
      print "--",$dupcnt," ",join("\t",@x),"\n" if $dupcnt<15;
   }
   print "-- $dupcnt DUPLICATES FOUND!\n" if $dupcnt;
   print "-- NO DUPLICATES FOUND!\n" if $hdr ne "";   

   # delete bad priority thingies that are duplicated
   print ST()."Deleting Duplicate Rows - Low Priority First\n";
   $q="delete #ATTS
   from   #ATTS a1, #ATTS a2
   where  a1.system_type=a2.system_type
   and    a1.system_name=a2.system_name
   and    a1.attribute_name=a2.attribute_name
   and    a1.compute_priority < a2.compute_priority\n";   
   _querynoresults($q,$dont_show_query);

   print ST()."Deleting Duplicate Rows - System Id Differences\n";
   $q="delete #ATTS
   from   #ATTS a1, #ATTS a2
   where  a1.system_type=a2.system_type
   and    a1.system_name=a2.system_name
   and    a1.attribute_name=a2.attribute_name
   and    a1.system_id < a2.system_id\n";   
   _querynoresults($q,$dont_show_query);

   print ST()."Deleting Duplicate Rows - Earliest\n";
   $q="delete #ATTS
   from   #ATTS a1, #ATTS a2
   where  a1.system_type=a2.system_type
   and    a1.system_name=a2.system_name
   and    a1.attribute_name=a2.attribute_name
   and    a1.mod_date < a2.mod_date\n";   
   _querynoresults($q,$dont_show_query);

#   print ST()."Fixing up host names\n";
#   $q = "   update #ATTS set system_name=lower(a.system_name), system_type=a.system_type, is_new_attribute=0
#   from #ATTS p, INV_system_attribute a
#   where   p.system_name=lower(a.system_name)
#   and    a.system_type in ('unix','win32servers')    -- only computers
#   and     p.attribute_name=a.attribute_name
#   and   is_new_attribute=1
#   and     p.system_type='EXISTS'";
#   _querynoresults($q,$dont_show_query);

#   print ST()."Fixing up server case pass2 \n";
#   $q = "update #ATTS set system_name=lower(a.system_name)
#   from #ATTS p, INV_system a
#   where   p.system_name=lower(a.system_name)
#   and    a.system_type in ('sybase','sqlsvr','oracle','mysql',   'unix','win32servers','dnsalias')
#   and     p.system_type=a.system_type
#   and   is_new_attribute=1";
#   _querynoresults($q,$dont_show_query);

#   print ST()."Fixing up host names pass2\n";
#   $q = "update #ATTS set system_name=lower(a.system_name), system_type=a.system_type
#   from #ATTS p, INV_system a
#   where   p.system_name=lower(a.system_name)
#   and    a.system_type in ('unix','win32servers')    -- only computers
#   and   is_new_attribute=1
#   and     p.system_type='EXISTS'";
#   _querynoresults($q,$dont_show_query);;

	#print ST()."Diagnostic Save\n";   
   #_querynoresults("if exists ( select * from tempdb..sysobjects where name='BARLOW_TMP' ) drop table tempdb..BARLOW_TMP",$dont_show_query);   
   #_querynoresults("select * into tempdb..BARLOW_TMP from #ATTS",$dont_show_query);
   #print "\n";

   print ST()."Create list of new systems in #NEWSYS \n";
   $q = "select distinct system_id=convert(varchar(30),''),system_name,system_type
   into #NEWSYS
   from #ATTS\n";   
   _querynoresults($q,$dont_show_query);

	check_database(__LINE__);  # quick custom db check for whatever you want	

   print ST()."Purge existing systems from #NEWSYS\n";
   $q = "Delete #NEWSYS from #NEWSYS a, INV_system p
   where  lower(p.system_name)=a.system_name
   and    p.system_type=a.system_type\n";   
   _querynoresults($q,$dont_show_query);   
   
   print ST()."Update System_id for #NEWSYS\n";
   $q = "update #NEWSYS set system_id=a.attribute_value 
   from      #NEWSYS p, #ATTS a
   where   p.system_name=a.system_name
   and     a.attribute_name in ".get_attvalue_string('FEED_TYPE_ASSETTAG')."   
   and     p.system_id is null\n";   
   _querynoresults($q,$dont_show_query);

   print ST()."Insert #NEWSYS into INV_system table\n";
   $q = "INSERT INV_system (system_id,system_name,system_type)
   select system_id,system_name,system_type from #NEWSYS\n";
   if( $dont_update_tables ) {
      print "NOOP: $q\n";
   } else {      
   _querynoresults($q,$dont_show_query);
   }

# print ST()."Updating Base Line Data With The Results\n";

#   my(%hshkeys);
#   foreach ( _querywithresults( "select distinct a.attribute_name, a.system_name, a.system_type
#   from #ATTS a
#   where is_new_attribute=1
#   and   system_type!='EXISTS'",$dont_show_query) ) {
#      my(@x)=dbi_decode_row($_);
#      my($pr)=join(" ",@x);
#      # print "FC: ",$pr,"\n";
#      die "ERROR: Hash Key $pr Duplicated" if $hshkeys{$pr};
#      $hshkeys{$pr}=1;
#   }

	
	print ST()."Delete Matching Atts\n";
   $q="delete INV_system_attribute
   from   INV_system_attribute s, #ATTS a
   where  a.system_name=lower(s.system_name)
   and    a.system_type=s.system_type
   and    a.attribute_name=s.attribute_name\n";
   if( $dont_update_tables ) {
      print ST()."NOOP: $q\n";
   } else {      
       _querynoresults($q,$dont_show_query);
   }

   print ST()."Now we insert NEW attributes\n";
   $q = "insert INV_system_attribute
   ( attribute_name, system_name, system_type,
      attribute_value, attribute_category, mod_date, mod_by )
   select distinct  a.attribute_name, a.system_name, a.system_type,
          a.attribute_value, a.attribute_category, a.mod_date, a.mod_by
   from #ATTS a\n";
   # where is_new_attribute=1\n";   
    if( $dont_update_tables ) {
      print ST()."NOOP: $q\n";
   } else {      
       _querynoresults($q,$dont_show_query);
   }

#   print ST()."Now we update EXISTING attributes\n";
#   $q="update INV_system_attribute
#   set    attribute_value=a.attribute_value
#   from   INV_system_attribute s, #ATTS a
#   where  a.is_new_attribute=0
#   and    a.system_name=lower(s.system_name)
#   and    a.system_type=s.system_type
#   and    a.attribute_name=s.attribute_name
#   and    a.attribute_value!=s.attribute_value\n";
#   if( $dont_update_tables ) {
#      print ST()."NOOP: $q\n";
#   } else {      
#       _querynoresults($q,$dont_show_query);
#   }

   #print "Displaying attributes\n";
   #foreach ( _querywithresults("select system_name, system_type, attribute_name, attribute_value, is_new_attribute from #ATTS order by system_name, system_type, attribute_name",$dont_show_query) ) {
   #   my(@x)=dbi_decode_row($_);
   #   printf "%s ATTS: %s %s=%s\n",$x[4],$x[1].".".$x[0],$x[2],$x[3];
   #}

check_database(__LINE__);  # quick custom db check for whatever you want	

   print ST()."Updating Case For Databases in INV_system_attribute\n";
   $q="update INV_system_attribute set system_name=upper(system_name) 
   where system_type in ('sybase','sqlsvr','oracle','mysql')   \n";   
   _querynoresults($q,$dont_show_query);

	print ST()."Updating Case For Databases in INV_system\n";
   $q="update INV_system set system_name=upper(system_name) 
   where system_type in ('sybase','sqlsvr','oracle','mysql')   \n";
   if( $dont_update_tables ) {
      print ST()."NOOP: $q\n";
   } else {      
   _querynoresults($q,$dont_show_query);
   }

	check_database(__LINE__);  # quick custom db check for whatever you want	

   print ST()."Updating System Id\n";
   $q="update INV_system set system_id=a.attribute_value 
   from    INV_system p, INV_system_attribute a
   where   p.system_name=a.system_name
   and     p.system_type=a.system_type
   and     a.attribute_name in ".get_attvalue_string('FEED_TYPE_ASSETTAG')."   
   and     p.system_id is null
   and     isnull(a.attribute_value,' ') != ' '\n";
   _querynoresults($q,$dont_show_query);
   
   print ST()."PROMOTION COMPLETED\n";
   print ST()."Starting Relationship Extracts\n";

# BUILD RELATIONSHIPS
#my(%RELATIONSHIPS);   # systemnm.systemtyp => @ary
#my($q)="   select system_name,system_type,relationship,relationship_id from INV_relationship\n";
#foreach( _querywithresults($q,$dont_show_query) ) {
#   my(@x)=dbi_decode_row($_);
#   my($k)=$x[0].":".$x[1];
#   my($v)=$x[2].":".$x[3];
#   if( defined $RELATIONSHIPS{$k} ) {
##      print "DBGDBG: k=$k Dat=$v - ",@{$RELATIONSHIPS{$k}},"<br>\n";
#      push @{$RELATIONSHIPS{$k}}, $v;
#   } else {
##      print "DBGDBG: k=$k Dat=$v - undef<br>\n";
#      my(@z);
#      push @z, $v;
#      $RELATIONSHIPS{$k} = \@z;
#   }
#}
#print "\n";
#dump_it(\%RELATIONSHIPS);

print ST()."InvRelateTwo database computer\n";
my($q)="
-- The following query maps attributes of known hostnames to systems case-instensitively
--   only for primary types
--   used to map dbnames to computer names
select distinct Db=a.system_name, DbType=a.system_type, -- attribute_name, attribute_value,
            RelatedSys=s.system_name, RelatedTyp=s.system_type
from INV_system_attribute a, INV_system s
where
 upper(s.system_name) = upper(a.attribute_value)
 and a.system_type in ('sqlsvr','oracle','sybase','mysql')
 and s.system_type in ('win32servers','unix','dnsalias')
 and attribute_name in ".get_attvalue_string('FEED_DB_ATTNM_OF_COMP')."    
 order by a.system_name\n";
#'Metadata_MachineName',
#'Driver_Hostname',
#'Metadata_HOSTNAME',
#'BestHostname'

foreach( _querywithresults($q,$dont_show_query) ) {
   my(@x)=dbi_decode_row($_);
   print ST()."RELATING db $x[0].$x[1] to computer $x[2].$x[3]\n";
   InvRelateTwo($x[0],$x[1],$x[2],$x[3], "database", "computer");

}

print ST()."InvRelateTwo win32servers database clusternode\n";
my($q)="select distinct Db=a.system_name, DbType=a.system_type, Nodes=attribute_value
from INV_system_attribute a, INV_constant v
where attribute_name in ".get_attvalue_string('FEED_DB_ATTNM_OF_CLUSTERNODE')."   
 order by a.system_name\n";
 
#='Metadata_ClusterNode'
#and   a.system_type in ('sybase','sqlsvr','oracle','mysql','unix','win32servers','dnsalias')
# order by a.system_name \n";
foreach( _querywithresults($q,$dont_show_query) ) {
   my(@x)=dbi_decode_row($_);
   my(@nodes)=split(/;/,$x[2]);
   foreach (@nodes) {
      print ST()."RELATING db $x[0].$x[1] to clusternode $_\n";
      InvRelateTwo($x[0],$x[1],$_,'win32servers', "database", "clusternode");
   }
}

check_database(__LINE__);  # quick custom db check for whatever you want	
   
# Related Database Names - One of these are aliases
print ST()."InvRelateTwo database database\n";
my($q)="
select distinct Db=a.system_name, DbType=a.system_type, Nodes=attribute_value
from INV_system_attribute a
where attribute_name in ".get_attvalue_string('FEED_DB_ATTNM_OF_DATABASE')."   
 and a.system_name!=attribute_value
 order by a.system_name\n"; 
 #='Metadata_ServerName'
 
foreach( _querywithresults($q,$dont_show_query) ) {
   my(@x)=dbi_decode_row($_);
   print ST()."RELATING db $x[0].$x[1] to db $x[2]\n";
   InvRelateTwo($x[0],$x[1],$x[2],$x[1], "database", "database");
}

# The following things are cluster nodes
print ST()."InvRelateTwo database clustername\n";
my($q)="
select distinct b.system_name,b.attribute_value, b.system_type
from INV_system_attribute a, INV_system_attribute b
where a.system_name=b.system_name
and   a.system_type=b.system_type
and   a.attribute_name='Metadata_ClusterNode'
and   b.attribute_name='Metadata_ServerName'
and   b.system_type in ('sybase','sqlsvr','oracle','mysql','unix','win32servers','dnsalias')\n";
foreach( _querywithresults($q,$dont_show_query) ) {
   my(@x)=dbi_decode_row($_);
   print ST()."RELATING database $x[0].$x[2] to clustername $x[1]\n";
   InvRelateTwo($x[0],$x[2],$x[1],$x[2], "database", "clustername");
}

check_database(__LINE__);  # quick custom db check for whatever you want	
   
print ST()."Updating ip on relationship tables\n";
my($q)="
update    INV_relationship set ip = a.ip_address
from    INV_relationship i, INV_aliases a
where    i.ip is null
and    lower(i.system_name) = lc_system_name

update    INV_relationship set ip = a.attribute_value
from    INV_relationship i, INV_system_attribute a
where    i.ip is null
and    i.system_name = a.system_name
-- and    lower(i.system_name) = lower(a.system_name)
and     attribute_name in ( 'IP','Driver_Ip_By_DbName')
and     attribute_value like '[0-9]%.[0-9]%.[0-9]%.[0-9]%'

update    INV_relationship set ip = a.attribute_value
from    INV_relationship i, INV_system_attribute a
where    i.ip is null
and    lower(i.system_name) = lower(a.system_name)
and     attribute_name in ( 'IP','Driver_Ip_By_DbName')
and     attribute_value like '[0-9]%.[0-9]%.[0-9]%.[0-9]%'\n";
_querynoresults($q,$dont_show_query);


print ST()."Relating dnsaliases that are missing \n";
($q)="select distinct relationship_id,system_type,lc_system_name,r.ip
into #ALL_ALIASES_BY_RID
from INV_relationship r, INV_aliases a
where r.ip is not null
and   r.ip = a.ip_address

delete    #ALL_ALIASES_BY_RID
from    #ALL_ALIASES_BY_RID a, INV_relationship r
where   lc_system_name = lower(r.system_name)
and     r.relationship_id = a.relationship_id

select relationship_id,lc_system_name,ip from #ALL_ALIASES_BY_RID order by relationship_id\n";

my(%fnd);
foreach( _querywithresults($q,$dont_show_query) ) {
   my($rid,$aliasnm,$ip)=dbi_decode_row($_);
   next if $fnd{$rid.$aliasnm};
   $fnd{$rid.$aliasnm}=1;
   print "-- adding dnsname $aliasnm ($ip) to relationship $rid\n";
   InvSetRelationship($rid,undef,$aliasnm,'dnsalias',$ip,undef,'dnsalias');
}

check_database(__LINE__);  # quick custom db check for whatever you want	
   
print ST()."Fixing Case in INV_relationship\n";
$q="update  INV_relationship
set     r.system_name=s.system_name
from    INV_relationship r, INV_system s
where    lower(r.system_name)=lower(s.system_name)
and      r.system_name != s.system_name
and      r.system_type = s.system_type";
   
_querynoresults($q,$dont_show_query);

print ST()."Fixing Case in INV_system_heirarchy\n";
$q="update  INV_system_heirarchy
set     r.system_name=s.system_name
from    INV_system_heirarchy r, INV_system s
where    lower(r.system_name)=lower(s.system_name)
and      r.system_name != s.system_name
and      r.system_type = s.system_type";

_querynoresults($q,$dont_show_query);

check_database(__LINE__);  # quick custom db check for whatever you want	
   
# ok now - new block 11/9/2010 (my birthday) to update the INV_constant table
print ST()."Deleting existing INV_constant for DROP DOWN LOOKUPS\n";
$q="delete INV_constant where name like 'ATT_LU_%'";
_querynoresults($q,$dont_show_query);

print ST()."Inserting INV_constant for DROP DOWN LOOKUPS\n";
$q="insert INV_constant (name,value) 
select distinct 'ATT_LU_'+d.attribute_name,a.attribute_value
from INV_attribute_dictionary d, INV_system_attribute a
where d.attribute_type='DROP_DOWN_LIST'
and d.system_type     = a.system_type
and d.attribute_name  = a.attribute_name";
_querynoresults($q,$dont_show_query);

check_database(__LINE__);  # quick custom db check for whatever you want	
   
print ST()."Updating Missing Contacts\n";
$q="insert INV_contact (contact, display_name)
select distinct attribute_value,attribute_value 
from INV_system_attribute where system_type='unixinv' and attribute_name='contact' 
and attribute_value not in ( select contact from INV_contact ) ";
_querynoresults($q,$dont_show_query);
   
print ST()."Promotion Completed\n";
}

sub InvUpdateAttributeDictionary {

my($query)="
select distinct attribute_name,system_type,is_new_attribute=1
into #PAIRS
from INV_system_attribute where system_type not in ('sybase','sqlsvr','oracle','mysql','unix','win32servers','dnsalias')

update #PAIRS
set is_new_attribute=0
from #PAIRS p, INV_attribute_dictionary d
where  p.attribute_name = d.attribute_name
and    p.system_type = d.system_type

INSERT INV_attribute_dictionary
(  attribute_name,system_type,attribute_type, compute_into_name, compute_into_type, compute_priority )
select  attribute_name,system_type, 'STRING', attribute_name, 'EXISTS', 6
from    #PAIRS where    is_new_attribute=1 and system_type='DCM'

INSERT INV_attribute_dictionary
(  attribute_name,system_type,attribute_type,  compute_into_name, compute_into_type, compute_priority )
select  attribute_name,system_type, 'STRING', attribute_name, 'win32servers', 3
from    #PAIRS where    is_new_attribute=1 and system_type='ldap_computer'";

_querynoresults($query);
_querynoresults("drop table #PAIRS");
}

sub InvUpdateBusinessSystem {
	my($nm,$ty,$bname,$do_delete)=@_;
	
	print "InvUpdateBusinessSystem( system_name => $nm,  system_type => $ty, b=$bname)<br>\n";
	my($whereclause)=" where system_name='".$nm."' and system_type='".$ty."'";

	# update the relationship to match all values for the current system
	if( ! defined $bname ) {
		foreach(_querywithresults("select business_system_name from INV_system_heirarchy $whereclause",0) ) {
			my(@x)=dbi_decode_row($_);
			die "ERROR: $x[0]" if $x[0]=~/42000:/;
			InvUpdateBusinessSystem($nm,$ty,$x[0],$do_delete);
		};
		return;
	}
	
	#if( $do_delete ) {
	#	_webpage_output_print("Deleting Heirarchy For System\n");
	#	_querynoresults("delete INV_system_heirarchy where system_name='".$PARGS{$FORM_ROOT_KEY."SYSTEM_NAME"}."' and system_type='".$PARGS{$FORM_ROOT_KEY."SYSTEM_TYPE"}."'",1);		
	#}		
	
	my($q)=" select relationship_id from INV_system ".$whereclause;
	#print "Q=$q<br>";
	my($RID) = _onerowquery($q,1);
	print __LINE__," DBGDBG updating RID=".$RID."<br>" if $DBGDBG;
	if( $RID ) {
		foreach(_querywithresults("select distinct system_name,system_type from INV_system s 
		where relationship_id = ".$RID,1 ) ) {
			my(@x)=dbi_decode_row($_);
			print "InvUpdateBusinessSystem(): existing relationship includes $x[0] $x[1]<br>\n";
			#_querynoresults("Delete INV_system_heirarchy where system_name='".$x[0]."' and system_type='".$x[1]."' and business_system_name='".$bname."'",1);
			_querynoresults("Delete INV_system_heirarchy where system_name='".$x[0]."' and system_type='".$x[1]."'",1);
			_querynoresults("Insert INV_system_heirarchy ( business_system_name,system_name,system_type ) values ( '".$bname."','".$x[0]."','".$x[1]."')",1);
		};
	} else {
		print "InvUpdateBusinessSystem() adding system to business system\n";
		#_querynoresults("Delete INV_system_heirarchy $whereclause and business_system_name='".$bname."'",1);
		_querynoresults("Delete INV_system_heirarchy $whereclause",1);
		_querynoresults("Insert INV_system_heirarchy ( business_system_name,system_name,system_type ) values ( '".$nm."','".$ty."','".$bname."')",1);
	}
}

my($formsequence)=0;
sub INV_sethtmlforms {
	my($formkey,@txt)=@_;
	
	if( $formsequence==0 ) {
		my($query)="delete INV_htmlforms where formkey='".$formkey."'";
		_querynoresults($query,1);
	}
	
	$formsequence++;
	my($text)=join("",@txt);
	$text.="<br>" unless $text =~ /<br>\s*$/;	
	$text=~s/\n\s*<br>/<br>/g;
	my($query)="insert INV_htmlforms values ('".$formkey."',".$formsequence.","._quote($text).")";
	print $query,"\n\n";
	_querynoresults($query,1);
} 

sub INV_gethtmlforms {
	my($formkey)=@_;
	my(@rc);
	foreach( _querywithresults( "select htmltext from INV_htmlforms where formkey='".$formkey."' order by sequence", 1) ) {
		my(@x)=dbi_decode_row($_);
		push @rc, $x[0];
	}
	return @rc;
}

sub INV_setup_cores_report {
	
	# print "SETTING UP CORES REPORT\n";
	my($query)="
	
create table #SystemInfo ( 
  system_name   varchar(255),
  system_type   varchar(255),
  contact		 varchar(255) null,
  location		 varchar(255) null,
  cpucount		 int null
)

CREATE TABLE  #AggregateCoreInfo
(
 contact         varchar(255)          NOT NULL,
 location        varchar(255)          NULL,
 audit_date      datetime              NOT NULL,
 num_cores       int              		NOT NULL,
 num_systems     int              		NOT NULL,
 core_type       varchar(30)           NOT NULL,
 is_quarter_end  char(1)              	NOT NULL,
 is_week_end     char(1)              	NOT NULL,
 is_year_end     char(1)              	NOT NULL
)

insert #SystemInfo select distinct system_name,system_type,null,null,null from INV_system where system_type='unixinv'

update #SystemInfo set contact = attribute_value from #SystemInfo t, INV_system_attribute a 
where t.system_name=a.system_name and t.system_type='unixinv' and a.system_type='unixinv' and a.attribute_name='contact'

update #SystemInfo set cpucount = convert(int,attribute_value) from #SystemInfo t, INV_system_attribute a 
where t.system_name=a.system_name and t.system_type='unixinv' and a.system_type='unixinv' and a.attribute_name='cpucount'

-- update #SystemInfo set location = attribute_value from #SystemInfo t, INV_system_attribute a 
-- where t.system_name=a.system_name and t.system_type='unixinv' and a.system_type='DCM' and a.attribute_name='plbuild'
-- and   t.location is null

update #SystemInfo set location = attribute_value from #SystemInfo t, INV_system_attribute a 
where t.system_name=a.system_name and t.system_type='unixinv' and a.system_type='unixinv' and a.attribute_name='location'
and   t.location is null and attribute_value not in ('N/A','TBD')

update #SystemInfo set location = attribute_value from #SystemInfo t, INV_system_attribute a 
where t.system_name=a.system_name and t.system_type='unixinv' and a.system_type='DCM' and a.attribute_name='plcity'
and   t.location is null

update #SystemInfo set location = attribute_value from #SystemInfo t, INV_system_attribute a 
where t.system_name=a.system_name and t.system_type='unixinv' and a.system_type='DCM' and a.attribute_name='plcntry'
and   t.location is null

update #SystemInfo set location = attribute_value from #SystemInfo t, INV_system_attribute a 
where t.system_name=a.system_name and t.system_type='unixinv' and a.system_type='WASP' and a.attribute_name='site'
and   t.location is null

update #SystemInfo set location = 'LON' where contact like 'ln%' and location is null
update #SystemInfo set location = 'SI' where contact like 'si%' and  location is null
update #SystemInfo set location = 'TK' where contact like 'tk%' and  location is null

update #SystemInfo set location = c.location 
from #SystemInfo t, INV_contact c
where t.contact=c.contact
and   t.location is null

update #SystemInfo set location = 'NYC' where location = '666'
update #SystemInfo set location = 'NJ' where location = 'rfp'
update #SystemInfo set location = 'NY' where location = 'elmsford'
update #SystemInfo set location = 'GRN' where location = 'connecticut'
update #SystemInfo set location = 'NJ' where location = 'carteret'
update #SystemInfo set location = 'NJ' where location = 'Weehawken'
update #SystemInfo set location = 'CHI' where location = 'chicago'
update #SystemInfo set location = 'GRN' where location = '1700'
update #SystemInfo set location = 'NJ' where location = 'secaucus'
update #SystemInfo set location = 'EU' where location = 'frankfurt'
update #SystemInfo set location = 'HK' where location = 'hongkong-colo2'
update #SystemInfo set location = 'HK' where location = 'hongkong-colo'
update #SystemInfo set location = 'NYC' where location = 'ring'
update #SystemInfo set location = 'NYC' where location = 'ukchix'
update #SystemInfo set location = 'LON' where location = 'uktc'
update #SystemInfo set location = 'LON' where location = 'ukeq'
update #SystemInfo set location = 'LON' where location = 'uk5'
update #SystemInfo set location = 'LON' where location = 'uktc uktc'
update #SystemInfo set location = 'LON' where location = 'uk'
update #SystemInfo set location = 'LON' where location = 'uklse'
update #SystemInfo set location = 'TK' where location = 'tokyo'
update #SystemInfo set location = 'NYC' where location = '650'
update #SystemInfo set location = 'NY' where location = 'wp'
update #SystemInfo set location = 'NYC' where location = '630'
update #SystemInfo set location = 'TK' where location = 'tokyo-osaka-colo'
update #SystemInfo set location = 'PA' where location = 'paris'
update #SystemInfo set location = 'NJ' where location = 'car'
update #SystemInfo set location = 'HK' where location = 'hongkong-seoul-colo'
update #SystemInfo set location = 'SI' where location = 'singapore'
update #SystemInfo set location = 'SI' where location = 'singapore-colo'
update #SystemInfo set location = 'SI' where location = 'singapore-uob'
update #SystemInfo set location = 'SI' where location = 'singapore-eq'
update #SystemInfo set location = 'TK' where location = 'tokyo-sydney-colo'

update #SystemInfo set location = 'NJ' where location = 'Elmsford'
update #SystemInfo set location = 'NJ' where location = 'Ridgefield Park'
update #SystemInfo set location = 'NJ' where location = 'RFP'
update #SystemInfo set location = 'NJ' where location = 'WHP'
update #SystemInfo set location = 'NJ' where location = 'Carteret'
update #SystemInfo set location = 'CHI' where location = 'Chicago'
update #SystemInfo set location = 'TK' where location = 'Tokyo'
update #SystemInfo set location = 'GRN' where location = 'Greenwich'
update #SystemInfo set location = 'NYC' where location like '666%'
update #SystemInfo set location = 'CHI' where location like 'Chicago'
update #SystemInfo set location = 'NJ' where location like 'Secaucus'

update #SystemInfo set location = '?' where location is null

insert #AggregateCoreInfo 
select distinct contact, location,getdate(), sum(cpucount), count(*), system_type, 'N', 'N', 'N'
from #SystemInfo
group by contact,location

-- select * from #AggregateCoreInfo
-- select * from INV_contact
	
delete INV_core_history where audit_date = convert(datetime,convert(varchar,getdate(),101))
	
update #AggregateCoreInfo set is_quarter_end = 'Y' where 
	( datepart(mm,audit_date)=3 and datepart(dd,audit_date)=31 ) or
	( datepart(mm,audit_date)=6 and datepart(dd,audit_date)=30 ) or
	( datepart(mm,audit_date)=9 and datepart(dd,audit_date)=30 ) or
	( datepart(mm,audit_date)=12 and datepart(dd,audit_date)=31 ) 
update #AggregateCoreInfo set is_week_end = 'Y' where datepart(dw,audit_date)=1
update #AggregateCoreInfo set is_year_end = 'Y' where datepart(mm,audit_date)=12 and datepart(dd,audit_date)=31
	
insert INV_core_history 
select contact, 
	location,
	convert(datetime,convert(varchar,getdate(),101)),
	num_cores,
	num_systems,
	core_type	, 
	is_quarter_end  ,
	is_week_end     ,
	is_year_end     
from #AggregateCoreInfo

select distinct contact,display_name,business_unit,account into #INV_contact from INV_contact
";

#print "QUERY=$query\n";
return $query;
}

#-system_name=>                 # required
#-system_type=>                 # required
#-attribute_category=>          # your key for this data set
#-attributes =>                 # hashref with key value pairs to set
#                               # any keys matching a column in INV_system set that table in preference to INV_system_attribute
#-delete=>                      # optional – if set delete the system
#-clearattributes=>             # optional – if set delete all attributes matching –attribute_category
#-replace								# replace attributes
#-production_state
sub InvUpdSystem {
	my(%args)=@_;
	my($noprint_queryies)=$noprint_diags;
	$noprint_queryies=0 if $args{-debug};
	$noprint_queryies=1 unless $noprint_queryies;

	print __LINE__," DBGDBG: InvUpdSystem( system_name => $args{-system_name} system_type => $args{-system_type})\n" if $args{-debug};
	#foreach ( keys %args ) { print __LINE__," DBGDBG: InvUpdSystem( $_ => $args{$_} )\n"; }

#	use Data::Dumper;
#	print __LINE__," DBGDBG: DUMPER START\n";
#	print Dumper $args{-attributes};
#	print "\nDBGDBG: END\n";

	initialize(%args) unless $i_am_initialized eq "TRUE";

	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	my($relate_attributes)=1 if $args{-attribute_category} eq "Hand Entered" and defined $args{-attributes};

	my($TSTAMP)="'".do_time(-fmt=>'mm/dd/yyyy hh:mi')."'";

	# the following is STOOPID - frigging couldnt couldnt count the atts via: my($cnt)=$#{$args{-attributes}};
	my($cnt)=0;
	foreach ( keys %{$args{-attributes}} ) { $cnt++; }
	print __LINE__," DBGDBG: Number of Related Attributes is $cnt\n" if $args{-debug};
	$relate_attributes=0 if $cnt==0;

	my($whereclause)= "system_name="._quote($args{-system_name})." and system_type="._quote($args{-system_type});

	# get relationship id if its Hand Entered.
	print __LINE__," DBGDBG: select relationship_id from INV_relationship where $whereclause<br>"
		if $relate_attributes and $args{-debug};
	my($RID) = _onerowquery("select relationship_id from INV_relationship where $whereclause")
		if $relate_attributes;
	my(@RELATED_SYSTEM);
	if( $RID =~ /^\d+$/) {
		print __LINE__," DBGDBG: RID=$RID\n"  if $args{-debug};
		my($q)="select system_name,system_type from INV_relationship where relationship_id=$RID";
		foreach( _querywithresults($q,$noprint_queryies) ) {
			my(@x)=dbi_decode_row($_);
			push @RELATED_SYSTEM, $x[0].":".$x[1];
		}
	} else {
		print __LINE__," DBGDBG: NO RELATIONSHIP \n","<br>\n" if $args{-debug} and $relate_attributes;
		push @RELATED_SYSTEM, $args{-system_name}.":".$args{-system_type};
	}

	if( $relate_attributes ) {
		foreach (@RELATED_SYSTEM ) { print __LINE__," DBGDBG: RELATED_SYSTEM: $_ !\n"  if $args{-debug}; }
	}

	if( $args{-delete} ) {
		# _querynoresults("update INV_system set is_deleted='Y' where $whereclause",$noprint_queryies);
		_querynoresults("delete INV_system where $whereclause",$noprint_queryies);
		_querynoresults("delete INV_system_attribute where $whereclause",$noprint_queryies);
		return 1;
	}

	if( $args{-clearattributes} ) {
		die "Must pass -attribute_category if you pass -clearattributes" unless $args{-attribute_category};
		_querynoresults("delete INV_system_attribute where $whereclause and attribute_category = "._quote($args{-attribute_category}),$noprint_queryies);
		# return 1;
	}
	#my(%args)= validate_params("MlpEvent",%args1);
	# @system_table_fields = _getcolumns_in_table("INV_system") if $#system_table_fields < 0;

	# DEREFERENCE ANY ATTRIBUTES THAT ARE REFERENCES
	foreach my $nm ( keys %{$args{-attributes}} ) {
		next unless defined $args{-attributes}->{$nm};
		if( ref $args{-attributes}->{$nm} eq "ARRAY" ) {
			my($cnt)=1;
			foreach ( @{$args{-attributes}->{$nm}} ) {
				$args{-attributes}->{$nm.$cnt++} = $_;
			}
			delete $args{-attributes}->{$nm};
		} elsif( ref $args{-attributes}->{$nm} ) {
			die "Illegal Ref Passed For $nm : ref $args{-attributes}->{$nm}\n";
		}
	}
	# die "NP=",$noprint_queryies,"\n";
	
	# does the system exist?
	my(@x)=_querywithresults("select * from INV_system where $whereclause",$noprint_queryies);
	print __LINE__," DBGDBG: InvUpdSystem() returned ",(1+$#x)," rows...\n"  if $args{-debug};
	if( $#x<0 ) {			# nope
		print __LINE__," DBGDBG: InvUpdSystem() New Row ($whereclause)\n"  if $args{-debug};
		
		$args{-production_state} = "PRODUCTION" unless $args{-production_state};
		$args{-is_dba_monitored} = "N" unless $args{-is_dba_monitored};
		$args{-is_infra_monitored} = "N" unless $args{-is_infra_monitored};
		$args{-is_spare} = "N" unless $args{-is_spare};
		my($q)="INSERT INV_system ( system_name, system_type, system_type_detail, ip_address,
				date_last_surveyed, description, is_spare, is_infra_monitored, is_dba_monitored, production_state ) values (".
				_quote($args{-system_name}).","._quote($args{-system_type}).
				","._quote($args{-system_type_detail}).
				","._quote($args{-ip_address}).				
				","._quote($args{-date_last_surveyed}).
				","._quote($args{-description}).
				","._quote($args{-is_spare}).
				","._quote($args{-is_infra_monitored}).
				","._quote($args{-is_dba_monitored}).
				","._quote($args{-production_state}).
				")";
				
		_querynoresults($q,$noprint_queryies);
		foreach my $nm ( keys %{$args{-attributes}} ) {
			next unless defined $args{-attributes}->{$nm};
			my($is_locked)='N';
			$is_locked='Y' if $args{-attribute_category} eq "Hand Entered";
			$q="insert INV_system_attribute values (".
						_quote($nm).",".
						_quote($args{-system_name}).",".
						_quote($args{-system_type}).",null,".
						_quote($args{-attributes}->{$nm}).",".
						_quote($args{-attribute_category}).",$TSTAMP,'Inventory.pm',".
						_quote($is_locked).
						")";
			 _querynoresults($q,$noprint_queryies);
		}
	} elsif( $#x==0 ) {
		print __LINE__," DBGDBG: InvUpdSystem() One Row ($whereclause)\n"  if $args{-debug};
		# exists so its all cool - lets update the attributes if needed
		foreach my $attrib ( keys %{$args{-attributes}} ) {
			print __LINE__," DBGDBG: InvUpdSystem() Updating $attrib to ",$args{-attributes}->{$attrib},"\n"  if $DBGDBG;
			next unless defined $args{-attributes}->{$attrib};
			if( $args{-replace} ) {
				foreach (@RELATED_SYSTEM) {
					my($snm,$sty)=split(/:/,$_);
					my($q)="delete INV_system_attribute where ".
							"attribute_name = "._quote($attrib)." and ".
							"system_name = "._quote($snm)." and ".
							"system_type = "._quote($sty)." and ".
							"attribute_category = "._quote($args{-attribute_category});
				 	_querynoresults($q,$noprint_queryies);
				 }
			}
			foreach (@RELATED_SYSTEM) {
				my($snm,$sty)=split(/:/,$_);
				my($is_locked)='N';
				$is_locked='Y' if $args{-attribute_category} eq "Hand Entered";
				my($q)="insert INV_system_attribute values (".
							_quote($attrib).", ".
							_quote($snm).", ".
							_quote($sty).", null, ".
							_quote($args{-attributes}->{$attrib}).",".
							_quote($args{-attribute_category}).",$TSTAMP,'Inventory.pm',".
							_quote($is_locked).")";
				 _querynoresults($q,$noprint_queryies);
			}
		}
	} elsif( $#x>=1 ) {
		die "Multiple Results Returned...";
	}
}

# -whereclause -getattributes -attribute_category -simple
# -simple gives only a singleton hash AND its in upper case
sub InvGetSystem {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Connect To Database: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	my(%args)= validate_params("InvGetSystem",%args1);

	@system_table_fields = _getcolumns_in_table("INV_system") if $#system_table_fields < 0;

	my(%out);
	$out{errormsg}="No Data Found For $args{-whereclause}";
	my($query)="select ".join(",",@system_table_fields).
							"\n from INV_system\n where $args{-whereclause}";
	_diagprint($query) if $args1{-debug};
	foreach ( _querywithresults($query,1) ) {
		my($system_id,$system_name,$system_type);
		my(@systemvals)=dbi_decode_row($_);
		my(%rc);

		my($i)=0;
		foreach (@system_table_fields) {
			# print "i=$i F=$_ $system_table_fields[$i] V=$systemvals[$i]<br>";
			if( $args{-simple} ) {
				$rc{uc($system_table_fields[$i])}=$systemvals[$i];
			} else {
				$rc{$system_table_fields[$i]}=$systemvals[$i];
			}
			$system_id=$systemvals[$i] 	if $i==0;
			$system_name=$systemvals[$i] 	if $i==1;
			$system_type=$systemvals[$i] 	if $i==2;
			$i++;
		}
		# _webpage_output_print("Fetching $system_name ($system_type)" );
		delete $out{errormsg};
		#print "InvGetSystem() - line ",__LINE__," dumpfest <br>";
		#dump_it(\%rc);# if $args1{-debug};

		if( $args{-getattributes} ) {
			my($attquery);			
			if( $args{-displayableonly} ) {
				$attquery="select a.attribute_name,a.system_name,a.system_type,a.database_name,a.attribute_value,a.attribute_category,a.mod_date,a.mod_by from INV_system_attribute a,INV_attribute_dictionary d\n".
							" where a.system_name="._quote($system_name).
							" and a.system_type="._quote($system_type).
							" and a.attribute_name=d.attribute_name and a.system_type=d.system_type and a.detail_display_key=1";
				if( $args{-attribute_category} ) {
					$attquery.= " and attribute_category="._quote($args{-attribute_category});
				}
			} else {
				$attquery="select attribute_name,system_name,system_type,database_name,attribute_value,attribute_category,mod_date,mod_by from INV_system_attribute \n".
							" where system_name="._quote($system_name).
							" and system_type="._quote($system_type);
				if( $args{-attribute_category} ) {
					$attquery.= " and attribute_category="._quote($args{-attribute_category});
				}
			}
			_diagprint($attquery) if $args1{-debug};
			foreach ( _querywithresults($attquery,1) ) {
				my(@rowvals)=dbi_decode_row($_);
				die "Woah - syntax error $system_name" if $#systemvals==0;

				# key is category;name
				if( $args{-simple} ) {
					$rc{ uc($rowvals[0]) }=$rowvals[4];
				} elsif( $rowvals[5] ) {
					$rc{ $rowvals[5].";".$rowvals[0] }=$rowvals[4];
				} else {
					$rc{ $rowvals[0] }=$rowvals[4];
				}
			}
		}
		if( $args1{-bsystem} ) {
			foreach(_querywithresults("select business_system_name from INV_system_heirarchy ".
							" where system_name="._quote($system_name).
							" and system_type="._quote($system_type), 1) ) {
				my(@x)=dbi_decode_row($_);
				#print "DBGDBG: Bsystem is $x[0]\n";
				$rc{BUSINESS_SYSTEM}=$x[0];
			}
		}

		#if( $args1{-debug} ) {
		#	print "InvGetSystem() - line ",__LINE__," dumpfest <br>";
		#	dump_it(\%rc);
		#}
		$out{$system_name.";".$system_type} = \%rc;
	}

	if( $args1{-debug} ) {
		print "InvGetSystem() - line ",__LINE__," dumpfest <br>";
		dump_it(\%out);
	}
	return \%out;
}

sub _onerowquery {
	my($q)=@_;
	foreach ( _querywithresults( $q,1 ) ) {
		my(@x)= dbi_decode_row($_);
		# print "DBGDBG: x is ",join(" ",@x),"<br>";
		return(wantarray() ? @x : $x[0] );
	}
	# print "DBGDBG: returning undef\n";
	return undef;
}

my($maxrid);
sub InvSetRelationship {
	my($rid,$sysid,$sysname,$systype,$ip,$description,$relationship)=@_;
	print __LINE__," DBGDBG: CALLING InvSetRelationship(".join(",",@_).")<br>\n"  if $DBGDBG;

	# Fix Cases
	my($s)=_onerowquery("select system_name from INV_system where lower(system_name)="._quote(lc($sysname))." and system_type="._quote($systype));
	$sysname=$s if $s and $s ne $sysname;

	my($oldrid)=_onerowquery("select relationship_id from INV_relationship where system_name="._quote($sysname)." and system_type="._quote($systype));

	#
	# NO ROW ID AT ALL - MUST CREATE ONE
	#
	if( ! $oldrid ) {
		print __LINE__," DBGDBG: No Existing Relationship For $sysname<br>\n" if $DBGDBG;
		if( ! $rid ) {
			if( ! defined $maxrid ) {
				$maxrid= _onerowquery("select max( relationship_id ) from INV_relationship");
				$maxrid=0 unless $maxrid;
			}
			$maxrid=$maxrid+1;
			$rid=$maxrid;
			print __LINE__," DBGDBG: Max+1 Rid = $rid For $sysname<br>\n"  if $DBGDBG;
		}
		print __LINE__," DBGDBG: Updating Rid=$rid For $sysname<br>\n"  if $DBGDBG;
		_querynoresults("insert INV_relationship
(relationship_id,system_id,system_name,system_type,ip,description,relationship)
values (".	$rid. ",". _quote($sysid). ",". _quote($sysname). ",".
_quote($systype). ",". 	_quote($ip). ",". _quote($description). ",". _quote($relationship).")");

	} elsif( $rid and $oldrid and $rid != $oldrid ) {
		print __LINE__," DBGDBG: Updating Rid to $rid\n"  if $DBGDBG;
		my($q)= "update INV_relationship set ";
			$q.=" relationship_id = ". 	$rid.", ";
			$q.=" system_id	= ". 	_quote($sysid).", " if $sysid;
			$q.=" ip= ". 			_quote($ip).", " if $ip;
			$q.=" description= ". _quote($description).", " if $description;
			$q.=" relationship= ". 	_quote($relationship) if $relationship;
			$q=~s/,$//;
			$q.=" where system_name= ". _quote($sysname);
			$q.=" and system_type= ". _quote($systype);
		_querynoresults($q,1);

	} else {

		if( ! $rid ) {
			if( ! defined $maxrid ) {
				$rid= _onerowquery("select max( relationship_id ) from INV_relationship");
				$rid=0 if ! $rid;
			}
			$maxrid=$rid+1;
			$rid=$maxrid;
			print __LINE__," DBGDBG: Max+1 Rid = $rid For $sysname<br>\n"  if $DBGDBG;
		}

		print __LINE__," DBGDBG: Inserting Relationship Rid = $rid <br>\n"  if $DBGDBG;
		my($q)= "update INV_relationship set ";
			$q.=" relationship_id = ". 	$rid.", " if $rid;
			$q.=" system_id	= ". 	_quote($sysid).", " if $sysid;
			$q.=" ip= ". 			_quote($ip).", " if $ip;
			$q.=" description= ". _quote($description).", " if $description;
			$q.=" relationship= ". 	_quote($relationship) if $relationship;
			$q=~s/,$//;
			$q.=" where 1=1";
			$q.=" and relationship_id=".	$oldrid if $oldrid;
			$q.=" and system_name= ". _quote($sysname);
			$q.=" and system_type= ". _quote($systype);
		_querynoresults($q,1);
	}

	$rid = $oldrid unless $rid;
	my($q)="update INV_system set relationship_id = $rid";
			$q.=" where system_name= ". _quote($sysname);
			$q.=" and   system_type= ". _quote($systype);
	_querynoresults($q,1);
	print __LINE__," DBGDBG: $q <br>\n"  if $DBGDBG;
	print __LINE__," DBGDBG: FInished InvSetRelationship() Updating $sysname $systype to RID = $rid<br>\n"  if $DBGDBG;

	return $rid;
}

sub InvGetTypeByName {
	my($name,$type) = @_;
	# print "DBGDBG Getting Type for $name/$type<br>";
	die "Bad Type $type " unless $type eq "host" or $type eq "database";
	if( $type eq "host" ) {
		return scalar(_onerowquery("select system_type from INV_system where system_name='".$name."' and system_type in ('unix','win32servers','dnsalias')"));
	} else {
		return scalar(_onerowquery("select system_type from INV_system where system_name='".$name."' and system_type in ('sybase','sqlsvr','oracle','mysql')"));
	}
}

#
# Relates Two Servers Together - returns relationship id that was used
#
sub InvRelateTwo {
	my($server1,$server1_type,$server2,$server2_type, $role1, $role2)=@_;
	print __LINE__," DBGDBG: CALLING InvRelateTwo(".join(",",@_).")\n"  if $DBGDBG;
	_diagprint( "CALLING InvRelateTwo(".join(",",@_).")") if $print_queries;

	my($q)="select relationship_id,relationship from INV_relationship where system_name="._quote($server1)." and system_type="._quote($server1_type);
	# _diagprint( $q ) if $print_queries;
	my($rid1,$r1)=_onerowquery($q);
	
	$q="select relationship_id,relationship from INV_relationship where system_name="._quote($server2)." and system_type="._quote($server2_type);
	_diagprint( $q ) if $print_queries;
	my($rid2,$r2)=_onerowquery($q);
	$rid1=~s/\s*//;
	$rid2=~s/\s*//;

	$role1 = "dr_".$role1 if $role1 and $server1=~/dr/i and $role1!~ /^dr_/;
	$role2 = "dr_".$role2 if $role2 and $server2=~/dr/i and $role2!~ /^dr_/;

#	print __LINE__," DBGDBG: System $server1 ($server1_type) has RID=$rid1<br>\n";
#	print __LINE__," DBGDBG: System $server2 ($server2_type) has RID=$rid2<br>\n";

	# if existing roles are not equal then update them
	if( $r1 and $role1 and $r1 ne $role1 ) {
		# clusters have priority for rename and dns has low priority
		if( $r1 !~ /cluster/ and $role1 !~ /dnsalias/ ) {
			my($q)="update INV_relationship set relationship="._quote($role1).
			" where relationship_id=$rid1 and system_name="._quote($server1)." and system_type="._quote($server1_type);
			_diagprint( $q ) if $print_queries;
			_querynoresults($q,1);
		} else {
			print "skipping update to role as new=$role1 and old=$r1<br>\n" if $debug;
		}
	}
	if( $r2 and $role2 and $r2 ne $role2 ) {
		if( $r2 !~ /cluster/ and $role2 !~ /dnsalias/ ) {
			my($q)="update INV_relationship set relationship="._quote($role2).
			" where relationship_id=$rid2 and system_name="._quote($server2)." and system_type="._quote($server2_type);
			_diagprint( $q ) if $print_queries;
			_querynoresults($q,1);
		} else {
			print "skipping update to role as new=$role2 and old=$r2<br>\n" if $debug;
		}
	}

	if( $rid1 and $rid2 ) {			# merge
		return $rid1 if $rid1 == $rid2;		# allready mapped

		print __LINE__," DBGDBG: Rid1=$rid1 Rid2=$rid2<br>\n"  if $DBGDBG;
		my($q)="update INV_relationship set relationship_id=$rid1 where relationship_id=$rid2";
		_diagprint( $q ) if $print_queries;
		_querynoresults($q,1);

		my($q)="update INV_system set relationship_id=$rid1 where relationship_id=$rid2";
		_diagprint( $q ) if $print_queries;
		_querynoresults($q,1);

		#InvSetRelationship($rid1,undef,$server1,$server1_type,undef,undef ,$role1);
		#InvSetRelationship($rid1,undef,$server2,$server2_type,undef,undef ,$role2);
		return $rid1;
	}

	if( $rid1 ) {	# set svr two tor rid1
		print __LINE__," DBGDBG: RID1 is set Rid1=$rid1 Rid2=$rid2<br>\n"  if $DBGDBG;
		my($q)="select system_id from INV_system where system_name="._quote($server2)." and system_type="._quote($server2_type);
		_diagprint( $q ) if $print_queries;
		my($sysid)=_onerowquery($q);
		return InvSetRelationship($rid1,$sysid,$server2,$server2_type,undef,undef ,$role2);
	}

	if( $rid2 ) {	# set svr two tor rid1
		print __LINE__," DBGDBG: Rid1=$rid1 Rid2=$rid2<br>\n"  if $DBGDBG;
		my($q)="select system_id from INV_system where system_name="._quote($server1)." and system_type="._quote($server1_type);
		_diagprint( $q ) if $print_queries;
		my($sysid)=_onerowquery($q);
		return InvSetRelationship($rid2,$sysid,$server1,$server1_type,undef,undef ,$role1);
	}

	# new relationship
	if( $use_html eq "TRUE" ) {
		print "<FONT COLOR=GREEN>Defining New Relationship</FONT><br>" if $print_queries;
	} else {
		print "Defining New Relationship\n" if $print_queries;
	}

	# print "<FONT COLOR=GREEN>Defining New Relationship</FONT><br>" if $print_queries;
	$rid1=InvSetRelationship(undef,undef,$server1,$server1_type,undef,undef ,$role1);
	InvSetRelationship($rid1,undef,$server2,$server2_type,undef,undef ,$role2);

	InvRelateAttributes($rid1);
	InvUpdateBusinessSystem($server1,$server1_type);
	return $rid1;
}

sub InvRelateAttributes {
	my($rid)=@_;

	print "-- Updating Related Attributes\n";
	my($q)="select a.* from INV_system_attribute a, INV_system s
where ( a.attribute_category='Hand Entered' or is_locked='Y' )
and   a.system_name=s.system_name
and   a.system_type=s.system_type
and   s.relationship_id=$rid";

	my(%atts);
	my($attcount)=0;
	foreach ( _querywithresults( $q, 1 ) ) {
		my( @rc ) = dbi_decode_row($_);
		$atts{$rc[0]}=$rc[4] if $rc[4];
		$attcount++;
	}
	return if $attcount==0;

#	dump_it(\%atts);

	$q="select distinct system_name,system_type from INV_system s where s.relationship_id=$rid";
	foreach ( _querywithresults( $q, 1 ) ) {
		my( $nm,$ty ) = dbi_decode_row($_);
		InvUpdSystem(  -system_name=> $nm,
   						-system_type=> $ty,
   						-attribute_category=>'Hand Entered',
   						-attributes => \%atts   ,
   						-replace =>1,
   						-clearattributes=> 1 );
	}


}

# -ref to array of data w/o captions
# InvReportDisplay( -col_headers, -data=>
sub InvReportDisplay {
	my(%args)=@_;
	my $dash = HTML::Dashboard->new();
	# print $#{$args{-col_headers}}," ",join(" ",@{$args{-col_headers}} );
	$dash->set_captions( @{$args{-col_headers }} );
	# print $#{$args{-col_headers}}," ",join(" ",@{$args{-col_headers}} );
	$dash->set_data_without_captions($args{-data});
	print $dash->as_HTML();
}

my(@sacreds);
my($sfetched)='FALSE';

# InvGetSACredByDSNandMonhost(-DSN=>  ->monitor_host=>);
# returns (login,pass,state,sysname,systype,hostname,port
sub InvGetSACredByDSNandMonhost {
	my(%args)=@_;
	die "NO DSN " 			  unless $args{-DSN};
	die "NO monitor_host " unless $args{-monitor_host};
	my(@cr)=InvGetSACred(%args);

	if( $#cr < 0 ) {		# no credential... make something up
		my($login,$pass,$state);
		my(@cr)=InvGetSACred( -DSN => $args{-DSN} );
		my($i)=0;
		foreach ( @cr ) {
			if( $cr[$i]->{state} eq "DBI_LOGINPASS" and $cr[$i]->{password}!~ /^\s*$/ ) {
				$login = $cr[$i]->{login};
				$pass  = $cr[$i]->{password};
				$state = $cr[$i]->{state};
				print __LINE__," DBGDBG: InvGetSACredByDSNandMonhost($args{-DSN},$args{-monitor_host})= $login/$pass/$state\n"  if $DBGDBG;
				return ($login,$pass,$state,$cr[$i]->{system_type},$cr[$i]->{hostname},$cr[$i]->{port});
			}
			$i++;
		}
		print __LINE__," DBGDBG: InvGetSACredByDSNandMonhost($args{-DSN},$args{-monitor_host}) none found\n"  if $DBGDBG;
		return (undef,undef,undef,undef,undef,undef);
	}
	if( ! $cr[0]->{login} and ! $cr[0]->{password} and $cr[0]->{state} eq "UNKNOWN" ) {
		print __LINE__," DBGDBG: InvGetSACredByDSNandMonhost($args{-DSN},$args{-monitor_host})= NO CREDENTIALS FOUND\n"  if $DBGDBG;
	} else {
		print __LINE__," DBGDBG: InvGetSACredByDSNandMonhost($args{-DSN},$args{-monitor_host})= $cr[0]->{login}, $cr[0]->{password}, $cr[0]->{state}, $cr[0]->{system_name}\n"  if $DBGDBG;
	}
	return ( $cr[0]->{login}, $cr[0]->{password}, $cr[0]->{state}, $cr[0]->{system_name},
				$cr[0]->{system_type}, $cr[0]->{hostname}, $cr[0]->{port} );
}

# InvGetSACred
#  - gets all sa credentials if needed
#  - returns an array of sacreds
sub InvGetSACred {
	my(%args)=@_;

	# foreach ( keys %args ) { print "InvGetSACred ", $_,"=>",$args{$_},"\n"; }

	# die "NO DSN " 				unless $args{-DSN};
	# die "NO monitor_host " unless $args{-monitor_host};

	InvGetAllSACreds() if $sfetched eq "FALSE";
	my(@rc);
	foreach (@sacreds) {
		my($hashref)=$_;
		next  if $args{-DSN} and $hashref->{DSN} ne $args{-DSN};
		next  if $args{-monitor_host} and $hashref->{monitor_host} ne $args{-monitor_host};
		push @rc, $_;
	}
	return @rc;
}

# InvGetAllSACreds()
#   - sets $sfetched=TRUE
#   - caches all sa creds into @sacreds
#   - each element of @sacreds is a hash of all the values in the GEM_sacred table
sub InvGetAllSACreds {
	my($tbl)='GEM_sacred';
	my($numkeys)=2;				# first two columns

	my(@cols)=_getcolumns_in_table($tbl);
	foreach ( _querywithresults( "select ".join(",",@cols)." from $tbl",1 ) ) {
		my( @rc ) = dbi_decode_row($_);
		my(%datahash);
		my($i)=0;
		foreach (@rc) {
			$datahash{$cols[$i]} = $_;
			$i++;
		}
		push @sacreds, \%datahash;
	}
	$sfetched='TRUE';
	return @sacreds;
}

# InvSetSACreds()
#   - ARGS
#        - REQUIRED ARGS: -DSN / -monitor_host
#        - OPTIONAL ARGS:
#             -state
#             -system_name/-system_type
#             -login/-password
#             -hostname/-port
#
#  InvSetSACreds( -dsn=>$dbi_key, -monitor_host=>$HOSTNAME, -metadata=>$ServerMetadata{ $dbi_key } );
#
sub InvSetSACreds {
	my(%args)=@_;
	my($tbl)='GEM_sacred';

	die "WOAH - NO -DSN passed to InvSetSACreds\n" unless $args{-DSN};
	die "WOAH - NO -monitor_host passed to InvSetSACreds\n" unless $args{-monitor_host};

	# transpose stuff
	if( $args{-metadata} ) {
		print __LINE__," DBGDBG: InvSetSACreds: DSN          => $args{-DSN} \n"  if $DBGDBG;
		print __LINE__," DBGDBG: InvSetSACreds: monitor_host => $args{-monitor_host} \n"  if $DBGDBG;
		print __LINE__," DBGDBG: InvSetSACreds: conn method  => $args{-metadata}->{BestConnectionMethod} \n"  if $DBGDBG;
		if( $args{-metadata}->{BestConnectionMethod} eq "UNKNOWN" ) {
			if( $args{-metadata}->{LoginToUse} and $args{-metadata}->{PasswordToUse} ) {
				$args{-state} = "BAD LOGIN/PASSWORD";
			} else {
				$args{-state} = "NO LOGIN/PASSWORD";
			}
		} elsif( $args{-metadata}->{BestConnectionMethod} eq "ISQL_LOGINPASS" ) {
			$args{-state} = "NO UNIX DSN";
		} elsif( $args{-metadata}->{BestConnectionMethod} eq "DBI_LOGINPASS" ) {
			$args{-state} = "OK";
		} elsif( $args{-metadata}->{BestConnectionMethod} eq "OSQL_NATIVE" ) {
			$args{-state} = "NO ODBC DSN";
		} elsif( $args{-metadata}->{BestConnectionMethod} eq "OSQL_LOGINPASS" ) {
			$args{-state} = "NO ODBC DSN";
		} else {
			die "UNKNOWN CONNECTION METHOD $args{-metadata}->{BestConnectionMethod} \n";
		}
		$args{-login}    = $args{-metadata}->{LoginToUse};
		$args{-password} = $args{-metadata}->{PasswordToUse};
		$args{-port}     = $args{-metadata}->{Reg_MSSQL_TcpPort}
			if ! $args{-port} and $args{-metadata}->{Reg_MSSQL_TcpPort};
		$args{-port}     = $args{-metadata}->{PORT}
			if ! $args{-port} and $args{-metadata}->{PORT};
		$args{-hostname}     = $args{-metadata}->{MachineName}
			if ! $args{-hostname} and $args{-metadata}->{MachineName};
		$args{-hostname}     = $args{-metadata}->{HOSTNAME}
			if ! $args{-hostname} and $args{-metadata}->{HOSTNAME};
		$args{-system_type}     = $args{-metadata}->{DBTYPE}
			if ! $args{-system_type} and $args{-metadata}->{DBTYPE};

		#print __LINE__," DBGDBG: InvSetSACreds: login  => $args{-login} \n" 		if defined $args{-login};
		#print __LINE__," DBGDBG: InvSetSACreds: password  => $args{-password} \n" if defined $args{-password};
	}

	InvGetAllSACreds() if $sfetched eq "FALSE";

	my($crexists)="FALSE";
	foreach (@sacreds) {
		my($hashref)=$_;
		if( $hashref->{DSN} eq $args{-DSN} and $hashref->{monitor_host} eq $args{-monitor_host} ) {
			$crexists='TRUE';
			last;
		}
	}

	my($q);
	if( $crexists eq "TRUE" ) {
		$q = "UPDATE $tbl set ";
		foreach ( keys %args ) {
			next if /-DSN/ or /-monitor_host/ or /-metadata/;
			my($a)=$_;
			$a=~s/-//g;
			$q.= $a ."="._quote($args{$_}).",";
		}
		$q=~s/,$//;
		$q.=" WHERE DSN='".$args{-DSN}."' and monitor_host='".$args{-monitor_host}."'";
		$q.="\n";
	} else {
		my(@cols)=_getcolumns_in_table($tbl);
		$q = "INSERT $tbl ( ".join(",",@cols).") values (";
		foreach (@cols) {
			next if /-metadata/;
			if( $args{"-".$_} ) {
				$q.=_quote($args{"-".$_}).",";
			} else {
				$q.="null,";
			}
		}
		$q=~s/,$//;
		$q.=")\n";
	}
	_querynoresults($q,1);
}

sub dump_it {
	my($x)=@_;
	use Data::Dumper;
	print "<PRE><FONT COLOR=BLUE>";
	print Dumper $x;
	print "</FONT></PRE>";
}

1;

__END__

=head1 NAME

Inventory.pm - inventory system perl function api

=head1 FUNCTIONS

=over 4

=item * InvYpcatHosts()

   loads ypcat hosts if you are on unix into table INV_aliases

=item * InvUpdSystem()

   -system_name=>                 # required
   -system_type=>                 # required
   -attribute_category=>          # your key for this data set
   -attributes =>                 # hashref with key value pairs
                                  # sets keys matching a column in INV_system
                                  # table in preference to INV_system_attribute
   -delete=>                      # optional. if set delete the system
   -clearattributes=>             # optional. if set delete all attributes
                                  # matching attribute_category

=item * InvGetSystem()

   fetch one or more systems plus optionally their data

 my($hashref)=InvGetSystem( -whereclause=> )
 -or-
 my($hashref)=InvGetSystem( -system_name=> -and- -system_type=> )

 Additional/Optional Args:

   -type               - Database | Host | ApproveRqdDatabase
   -where              - if set then this is an added where clause restricting the systems being returned
   -debug              - prints the queries to standard output
   -system_name        - restrict list to systems of this name (wildcards ok)
   -system_type        - restrict list to systems of given system_type

   -attribute_category:    Only returns atts of that category
   -getattributes:         Returns attributes
                           Default only the key rows in INV_system
   -bsystem                Business sytem
   -simple                 SINGLETON return hash and its in upper case


InvGetSystem(-whereclause=>is_infra_monitored=Y, -host_type=>linux) would return a
hashref with keys of system_name:system_type and values which are a hash ref of existing values.

=item * InvSetSACreds()

   - REQUIRED ARGS: -DSN / -monitor_host

   - OPTIONAL ARGS:
      -state
      -system_name/-system_type
      -login/-password
      -hostname/-port

=item * InvGetAllSACreds()

   - sets $sfetched=TRUE
   - caches all sa creds into @sacreds
   - each element of @sacreds is a hash of all the values in the GEM_sacred table

=item * InvGetSACredByDSNandMonhost()

   - arguments -DSN, monitor_host
   - gets password for a given DSN/monitor_host
   - returns (login,pass,state,sysname,systype,hostname,port

=item * InvGetTypeByName()

   - returns type for a passed in system name and 'database' or 'host'

=item * InvRelateTwo()

     - Relates Two Servers Together - returns relationship id that was used
     - Arguments: $server1,$server1_type,$server2,$server2_type,$role1, $role2

=item * InvSetRelationship()

   - Set a relationship - can pass null rid...
   - Arguments; $rid,$sysid,$sysname,$systype,$ip,$description,$relationship

=item * InvReportDisplay()
	- Generic Table Display Widget that uses HTML::Dashboard
	- takes -col_headers and -data arguments

=item * InvUpdateAttributeDictionary()

=item * InvPromote()

=item * InvUpdateBusinessSystem()

=back 

=cut

