#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Repository;
use File::Basename;
use File::Copy;
use DBIFunc;
#use MlpAlarm;
#use Data::Dumper;
use Sys::Hostname;
use CommonFunc;
use CommonHeader;
# use Carp qw/confess/;
# use RepositoryRun;

my $DEVELOPERMODE;
my $DEBUG;
my %cust_ignore_msgs;

my($gem_root_dir)=get_gem_root_dir();
my $OUTDIRECTORY = "$gem_root_dir/data/CONSOLE_REPORTS/html_output";

die "No Gem Root Dir" unless -d $gem_root_dir;
print "  GEM ROOT=$gem_root_dir\n";

# Custom Reports ; name to web page
my(%custom_report_type,%custom_report_name,%custom_report_server,%custom_report_query,%custom_report_database);

read_custom_defined_reports();
run_custom_defined();

sub run_custom_defined
{
	print "run_custom_defined()\n";
	foreach my $count ( keys %custom_report_name ) {
		my(@rc);
		print "\nRUNNING USER DEFINED REPORT\n",
			"REPORT NAME\t",$custom_report_name{$count} ,"\n",
			"REPORT TYPE\t",$custom_report_type{$count} ,"\n",
			"REPORT SVR\t",$custom_report_server{$count} ,"\n",
			"REPORT DB\t",$custom_report_database{$count} ,"\n",
			"REPORT QUERY\t",$custom_report_query{$count}  if defined $DEBUG;

		my($title)=$custom_report_name{$count};
		print "**> Custom Report $title \n" ;
		$title=~s/_/ /g;

		if( $custom_report_type{$count} =~ /^cmd/i ) {
   		run_a_command(
				-rptname=>$custom_report_name{$count},
				-title=>$title,
	  			-cmd=>$custom_report_query{$count},
	  			-outfile=>$custom_report_name{$count}.".txt",
	  			-die=>undef);
			next;

		} elsif(  $custom_report_type{$count} =~ /^html/i ) {

   		run_a_command(
				-rptname=>$custom_report_name{$count},
				-title=>$title,
	  			-cmd=>$custom_report_query{$count},
	  			-outfile=>$custom_report_name{$count}.".html",
	  			-die=>undef);
			next;

		} elsif(  $custom_report_type{$count} =~ /^file/i ) {

			# ok use FTP libs to get the file to the right directory
			my($host)= $custom_report_server{$count};
			my($file)= $custom_report_query{$count};
     		my($login,$password,$method);
			if($host =~ /^null/i ) {
     			$host = hostname();
			} else {
     			($login,$password) = get_password(-name=>$host,-type=>"unix");
     			my(%args)=get_password_info(-type=>"unix",-name=>$host);
     			if( is_nt() ) {
					$method=$args{WIN32_COMM_METHOD} if is_nt();
				} else {
					$method=$args{UNIX_COMM_METHOD};
				}
				die "COMM_METHOD NOT DEFINED for SERVER $host\n" unless $method;
				if( $method eq "NONE" ) {	# hmm cant talk
					l_die( "host $host: METHOD=NONE - skipping\n" );
				}
			}

			use Net::myFTP;
			print "  Creating $method Connection To $host\n" if defined $DEBUG;
			my($ftp) = Net::myFTP->new($host,METHOD=>$method,Debug=>$DEBUG)
				or l_die("Failed To Create FTP Object $@");

			if( ! $ftp )  {
				print "Failed To Connect To $host\n";
				next;
			}

			my($r,$err)=$ftp->login($login,$password);
			if( ! $r ) {
				print "WARNING: Cant Connect to Server: $err\n";
				next;
			}

			if( ! $ftp->get($file,$OUTDIRECTORY."/".$custom_report_name{$count})){
				print("WARNING: Couldnt Copy File $file from $host: $! \n");
				next;
			}
			$ftp->close();
			$ftp->quit();

			next;

		} elsif(  $custom_report_type{$count} =~ /^sql/i ) {

			my(@d)=split(/\s+/,$custom_report_query{$count});
			# first ping it
			# my($rc) = db_syb_ping($d[0],$d[1],$d[2]);
			# if( $rc ne "0" ) {
      		# print "Cant connect to $d[0] as $d[1] : $rc\n";
				# @rc[0]="Cant connect to $d[0] as $d[1] : $rc\n";
				# @rc[1]="Custom Report Aborting\n";
  			#} els
			if( ! dbi_connect(-srv=>$d[0],-login=>$d[1],-password=>$d[2]) ) {
      		print "Cant connect to $d[0] as $d[1]\n";
				$rc[0]="Cant connect to $d[0] as $d[1]\n";
				$rc[1]="Custom Report Aborting\n";
   		} else {
				shift @d;
				shift @d;
				shift @d;

				my($q)=join(" ",@d);
				@rc=dbi_query_to_table(
					-db=>"master",
					-query=>$q,
					-print_hdr=>1,
					-table_options=>"BORDER=1",
					-TBLHDRCOLOR=>"BEIGE");

				dbi_disconnect();
			}

		} else {

			# not a command or html so its a server
     			my($login,$password) = get_password(-name=>$custom_report_server{$count},-type=>"unix");
	  			if( ! dbi_connect(-srv=>$custom_report_server{$count},-login=>$login,-password=>$password) ) {
	      		print "Cant connect to $custom_report_server{$count} as $login\n";
					$rc[0]="Cant connect to $custom_report_server{$count} as $login\n";
					$rc[1]="Custom Report Aborting\n";
   			} else {
				@rc=dbi_query_to_table(
					-db=>$custom_report_database{$count},
					-query=>$custom_report_query{$count},
					-print_hdr=>1,
					-table_options=>"BORDER=1",
					-TBLHDRCOLOR=>"BEIGE");

				dbi_disconnect();
			}
		}

		print "create_custom_report(",$custom_report_name{$count}.".html)\n";

	   	open( WR,"> ".$OUTDIRECTORY."/".$custom_report_name{$count}.".html" )
			or l_die("Cant Write to File $OUTDIRECTORY/".$custom_report_name{$count} ."\n");
	      print WR pagehead($title,$custom_report_name{$count}.".html","Custom Report # $count" );
			foreach (@rc) { chomp; print WR $_,"\n"; }
	      print WR pagefoot();
	      close(WR);
	}
}

sub read_custom_defined_reports
{
	print "read_custom_defined_reports() from $gem_root_dir/conf/console.dat\n";
	open(USERDEF,$gem_root_dir."/conf/console.dat")
		or open(USERDEF,"console.dat")
		or open(USERDEF,"../console.dat")
		or return;

	my(%used_names);
	my($count)=0;
	while ( <USERDEF> ) {
		next if /^\s*$/ or /^\s*#/;
		chomp;
		my(@x)=split(/\s+/,$_,4);
		if( $x[0] eq "IGNORE" ) {
			$cust_ignore_msgs{$x[1]." ".$x[2]}=$x[3];
			next;
		};
		if( defined $used_names{$x[0]} ) {
			print "Warning: Custom Report Previously Defined By Same Name\n";
			print "This Report Shall Be Ignored\n";
			next;
		};
		$used_names{$x[0]}=1;
		$custom_report_name{$count} = $x[0];
		if( $x[1] 	=~ /^cmd/i
			or $x[1] =~ /^file/i
			or $x[1] =~ /^html/i
			or $x[1] =~ /^sql/i ) {

			$custom_report_type{$count} = $x[1];
			$custom_report_server{$count} = "";
			$custom_report_database{$count} = "";

			if( $x[1] =~ /^file/i ) {
				$custom_report_server{$count} = $x[2];
				shift @x;
			}
			shift @x;
			shift @x;
			$custom_report_query{$count} = join(" ",@x);

			# ok... set up %next and %last
			#if( $tmplast != "" ) {
				#$next{$tmplast} = $x[0].".html" ;
				#$last{$x[0].".html"} = $tmplast;
			#}
			#$tmplast = $x[0].".html";

		} else {

			$custom_report_server{$count} = $x[1];
			if( $x[2] =~ /null/i ) {
				$custom_report_database{$count} = "";
			} else {
				$custom_report_database{$count} = $x[2];
			}
			$custom_report_query{$count} = $x[3];

		}
		$count++;
	}
	close USERDEF;
}

sub run_a_command
{
	my(@rc);
	my(%OPT)=@_;
	starttiming( "**> $OPT{-rptname}" );
	print "   Output=".$OPT{-outfile}."\n" if defined $OPT{-outfile};
	my($cmd)=$OPT{-cmd};
	my($cmd_no_pass)=$cmd;
	if( $cmd_no_pass =~ /-PASSWORD/ ) {
		$cmd_no_pass=~s/-+PASSWORD=[\w]+/-PASSWORD=xxx/;
	} else {
		$cmd_no_pass=~s/-P\w+/-Pxxx/;
	}
	chomp $cmd_no_pass;
	print "   Command=>$cmd_no_pass\n";
	if( $OPT{-outfile} ) {
		 if( $OPT{-outfile}=~/\.txt$/ ) {
	      $OPT{-outfile} =~ s/txt$/html/;
	      $OPT{-is_txt} = 1;
	   }
	}

	my($output_text)="";
	if( defined $OPT{-outfile} ) {
		$output_text .= pagehead($OPT{-title},$OPT{-outfile},"Built from $cmd_no_pass");
		if( defined $OPT{-is_txt} ) {
			$output_text .= "\n<PRE>";
		} else {
			$output_text .= "\n";
		}
	}

	$cmd.= " 2>&1 " if defined $OPT{-nostderr};
	open( OUT, $cmd." |" ) or l_die("Cant Run Command $OPT{-cmd} : $!\n");
	foreach (<OUT>) {
	   chomp;
		next if defined $OPT{-exclude} and $_ =~ /$OPT{-exclude}/;
		push @rc, $_;
	   if(defined $OPT{-outfile} ) {
			$output_text .= $_."\n";
	   } else {
			print " -- ".$_."\n";
	   };
	}
	close OUT;

	$output_text .= "</PRE>" if defined $OPT{-is_txt};
	$output_text .= pagefoot() if defined $OPT{-outfile};

	if( defined $OPT{-outfile} ) {
		unlink($OPT{-outfile}) if -r $OPT{-outfile};
	  	open( WR,"> ".$OPT{-outfile})
			      or l_die("Cant Write to File ".$OPT{-outfile}."\n");
		print WR $output_text;
		close(WR)  if defined $OPT{-outfile};
		chmod(0666,$OPT{-outfile});	# or die "Cant Chmod File $OPT{-outfile}\n";
	}

	if( ! defined $OPT{-die} or $OPT{-die}==0 ) {
	   warn("======= Warning ========\n| ($cmd_no_pass)\n| Subcommand Had A Bad Return Code ".($?/256)."\n| Command Output in ".$OPT{-outfile}."\n==============\n\n") if $? != 0;
	} else {
	   l_die("======= Fatal Error ========\n| ($cmd_no_pass)\n| Subcommand Had A Bad Return Code ".($?/256)."\n| Command Output in ".$OPT{-outfile}."\n==============\n\n") if $? != 0;
	}
	endtiming();
	return @rc;
}
