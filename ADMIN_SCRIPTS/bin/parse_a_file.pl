#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# Parse a file and reports on stats

use strict;
use Getopt::Std;

sub usage {
	my($msg)=@_;
	print "ERROR: $msg\n" if defined $msg;

	print "Usage: $0 -FFilename -Rrow_delimeter -Ccol_delimter -Kkey

key is column number and defaults to 1 (for output report)\n";
   exit(1);
}

use vars qw($opt_K $opt_F $opt_R $opt_C $opt_B);

usage() if $#ARGV<0;
usage("Bad Parameter List") unless getopts('F:R:C:B:');
usage("Must pass filename\n" )  unless defined $opt_F;

sub error_out {
	use MlpAlarm;
	my($msg)=join("",@_);
	$msg = "BATCH ".$opt_B.": ".$msg if $opt_B;

	MlpEvent(
		-monitor_program=> $opt_B || "parse_a_file.pl",
		-system=> $opt_B || "",
		-message_text=> $msg,
		-severity => "ERROR"
	);
	die $msg;
}
error_out( "File $opt_F Not Found" )		unless -e $opt_F;
error_out( "File $opt_F Not Readable") 	unless -r $opt_F;

$/=$opt_R if defined $opt_R;
$opt_C="THIS LINE CAN NOT EXIST IN THE FILE" unless defined $opt_C;
$opt_K=1 unless defined $opt_K;
$opt_K--;
error_out( "key ($opt_K) must be > 1\n") unless $opt_K >= 0;

my($max_num_cols)=0;
my($min_num_cols)=1000000000;
my($max_num_rows)=0;
my(@max_col_len)=(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my(@row_of_max_col)=();
my(@first_col_data);

open(FILE,$opt_F) or error_out( "Cant open $opt_F : $!\n");
foreach (<FILE>) {
	next if $_ =~ /^\s*$/;
	my(@d)=split /$opt_C/;
	$max_num_rows++;
	$min_num_cols = ($#d+1) if $min_num_cols>($#d+1);
	$max_num_cols = ($#d+1) if $max_num_cols<($#d+1);

	my($i)=1;
	foreach(@d) {
		my($len)=length;
		if( $max_col_len[$i]<$len ){
			# print "DBG: ITS A MAX: FIRST $d[0] LEN $len MAXLEN $max_col_len[$i] ROW $max_num_rows COL $i\n";
			$max_col_len[$i]=$len ;
			$row_of_max_col[$i] = $max_num_rows ;
			$first_col_data[$i] = $d[$opt_K] ;
		}
		$i++;
	}
}

close(FILE);

print "REPORT ON FILE $opt_F

          NUMBER OF ROWS   $max_num_rows
MAXIMUM COLUMNS IN A ROW   $max_num_cols
MINIMUM COLUMNS IN A ROW   $min_num_cols\n\n";

for( my $i=1; $i<=$max_num_cols; $i++) {
	print "COLUMN NUMBER $i : Max Length = ",$max_col_len[$i]," on row ",$row_of_max_col[$i],"\n";
	print "         FIRST FIELD OF ROW IS ",$first_col_data[$i],"\n";
}

__END__

=head1 NAME

parse_a_file.pl - Parse a file into parts

=head2 DESCRIPTION

Very useful for finding problems with your bcp files...

=head2 USAGE

Usage: parse_a_file.pl -FFilename -Rrow_delimeter -Ccol_delimter

=head2 NOTES


=cut

