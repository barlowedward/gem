#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;

#
# PREPROCESSING - MAKE SURE SYBASE LIBRARIES CAN LOAD
#
BEGIN {
        use File::Copy;
        my $str='Sybase::SybFunc';

        unless ( eval "use $str" ) {
                # Copy SybFunc it to your local system

                # Get Target Directory
                my($outdir)=$INC[0];

                # Get Source Directory
                #  - try lib subdirectory
                my $source;
                if( -d 'lib' and -e  'lib/Sybase.SybFunc.pm' ) {
                        $source='lib/Sybase.SybFunc.pm';
                } elsif( -d '../lib' and -e  '../lib/SybFunc.pm' ) {
                        $source='../lib/Sybase.SybFunc.pm';
                } elsif( -d 'C:/riskiqserver/lib' and -e  'C:/riskiqserver/lib/SybFunc.pm' ) {
                        $source='C:/riskiqserver/lib/SybFunc.pm';
                } elsif( ! -f '//nyiqfxrm06/distrib/Joyo/J1.0Build11/lib/SybFunc.pm' ) {
                        $source='//nyiqfxrm06/distrib/Joyo/J1.0Build11/lib/SybFunc.pm';
                } else {
                        die "Error. Cant find SybFunc.pm.  Seek Help.\n";
                }

                print "Installing SybFunc.pm to $outdir\n";
                copy($source,$outdir) or die "Cant Copy $source to $outdir: $!\n";
        }
}

#
# GENERIC PATCH INSTALLER.
#

# THE FOLLOWING IS A LIST OF VALID SAVE_CFG_ITEMS FILE ITEMS
#  - These items are saved
my(@ok_cfg_items)= qw(TITLE_BANNER VALUATION_HOSTNAME SYBASE_HOSTNAME SQL_SVR_LOGIN SQL_SVR_PASSWD DSQUERY BACKUP_SVR_NAME BACKUP_DIR SYBASE APP_ROOT_DIR TEMP DIR_ALIAS REPOSITORY_SERVER REPOSITORY_DB REPOSITORY_LOGIN REPOSITORY_PASSWD);

my( %SERVER_DIR ) = (
        'RR_NT' => '//nyiqfxrm01/RiskIQServer',
        'RR_NTCA' => '//nyiqfxrm04/RiskIQServer',
        'QA1NT' => '//nyiqfxrm03/RiskIQServer',
        'QA2NT' => '//nyiqfxrm02/RiskIQServer',
        'QA3NT' => '//nyiqfxrm06/xrm06c/RiskIQServer',
        'PLAY'  => '//nyls2w1115/RiskIQServer',
        'DB1'   => '//nyfsia0000/RiskIQServer',
        'LOCAL' => 'C:/RiskIqServer'
);

my( %SERVER_C_DIR ) = (
        'RR_NT' => '//nyiqfxrm01/iqfxrm01',
        'RR_NTCA' => '//nyiqfxrm04/xrm04c',
        'QA1NT' => '//nyiqfxrm03/xrm03c',
        'QA2NT' => '//nyiqfxrm02/xrm02c',
        'QA3NT' => '//nyiqfxrm06/xrm06c',
        'PLAY'  => '//nyls2w1115/1115c',
        'DB1'   => '//nyfsia0000/C',
        'LOCAL' => 'C:/'
);

my( %SERVER_DSQUERY ) = (
        'RR_NT' => 'SYBASE_RR_NT',
        'RR_NTCA' => '',
        'QA1NT' => 'SYBASE_NTQA1',
        'QA2NT' => 'SYBASE_NTQA2',
        'QA3NT' => 'SYBASE_NTQA3',
        'PLAY'  => 'NYLS2W1115',
        'DB1'   => 'NYFSIA0000',
        'LOCAL' => 'SYBASE_LOCAL'
);

my( %SERVER_LOGIN ) = (
        'RR_NT' => 'SYS_INSTALL',
        'RR_NTCA' => '',
        'QA1NT' => 'SYS_INSTALL',
        'QA2NT' => 'SYS_INSTALL',
        'QA3NT' => 'SYS_INSTALL',
        'PLAY'  => 'SYS_INSTALL',
        'DB1'  => 'SYS_INSTALL',
        'LOCAL' => 'SYS_INSTALL'
);

my( %SERVER_PASS ) = (
        'RR_NT' => 'PASSWORD',
        'RR_NTCA' => '',
        'QA1NT' => 'PASSWORD',
        'QA2NT' => 'PASSWORD',
        'QA3NT' => 'PASSWORD',
        'PLAY'  => 'PASSWORD',
        'DB1'  => 'PASSWORD',
        'LOCAL' => 'PASSWORD'
);

sub usage
{
        print @_;
        print "USAGE: setup.pl ENVIRONMENT\n";
        print "\nValid ENVIRONMENTs are: \n";
        foreach (keys %SERVER_DIR) { print "\t",$_,"\n"; }

        print "Additionally the ENVIRONMENT ALL can be passed\n";
        return "\n";
}

use File::Basename;
use File::Path;
use Cwd;

die usage() unless $#ARGV>=0;

# ===========================
# Find Appropriate Function Libraries and Then Source Them
# ===========================
my($drf);
$drf='./do_release_func.pl'                             if -e './do_release_func.pl';
$drf='./utilities/do_release_func.pl'  if -e './utilities/do_release_func.pl';
$drf=$SERVER_DIR{"LOCAL"}.'/utilities/do_release_func.pl'  if -e $SERVER_DIR{"LOCAL"}.'/utilities/do_release_func.pl';

die "do_release_func.pl not found in ., ./utilities, or ",$SERVER_DIR{"LOCAL"}."/utilities\n"   unless defined $drf;

require $drf;

my(%SAVE_CFG_ITEMS);
foreach (@ok_cfg_items) { $SAVE_CFG_ITEMS{$_}=""; }

my $version = basename(getcwd());
my $homedir = getcwd();

# ==========================
# Read Config File
# ==========================
my(%SETUP);
if( -e "setup.env" ) {
        open (ENV,"setup.env") or die "Cant Read setup.env $!\n";
        while (<ENV>) {
                next if /^#/ or /^\s*$/;
                my($k,$v)=split(/=/,$_);
                $SETUP{$k}=$v;
                chop;
        }
        close(ENV);
}

if( $INC[$#INC-1] !~ /site/ ) {
        die "ERROR: Perl seems to be confused according to my definition of what it should be doing.  I expect the INC array to have site libraries in it. INC is, however\n".join("\n",@INC)." and the directory i wish to install to is $INC[$#INC-1].  Please contact support\n";
}

if( $ARGV[0] eq "ALL" ) {
        shift @ARGV;
        foreach (keys %SERVER_DIR) {
                push @ARGV,$_ unless $_ eq "LOCAL";
        }
}

foreach my $Sys (@ARGV) {
        chdir $homedir or die "Cant cd $homedir\n";

        print "==========================================\n";
        printf( "| INSTALLING %-12.12s INTO %-8.8s  |\n",$version,$Sys );
        print "==========================================\n";

        # ================================
        # CHECKING VERSIONS
        # ================================
        my($oldversion)="";
        if(-e $SERVER_DIR{$Sys}."/VERSION") {
                open(VERSION,$SERVER_DIR{$Sys}."/VERSION")
                        or die "Cant read VERSION file:$!\n";
                while (<VERSION>) {
                        chop;
                        $oldversion=$_;
                }
                close VERSION;

                print "Old Version is at $oldversion\n";
                if( $SETUP{"needs_version"} !~ /^\s*$/ ) {
                        print "Checking Old Version against ".$SETUP{"needs_version"}."\n";
                        die "ERROR => CANT CONTINUE - THIS PATCH REQUIRES SOFTWARE AT VERSION: ".$SETUP{"needs_version"}."\nThe ".$SERVER_DIR{$Sys}."/VERSION file is at $oldversion\n" if $SETUP{"needs_version"} > $oldversion;
                }
        }

        die "ERROR: Environment $Sys is not defined\n"
                unless defined $SERVER_DIR{$Sys};

        # ================================
        print "Removing Old Files\n";
        # ================================
        if( -e 'rm_files' ) {
        open(RM,'rm_files') or die "Cant open rm_files\n";
        while ( <RM> ) {
                next if /^#/ or /^\s*$/;
                chop;
                next unless -e $SERVER_DIR{$Sys}."/".$_;
                print "Removing ".$SERVER_DIR{$Sys}."/".$_."\n";
                unlink $SERVER_DIR{$Sys}."/".$_ or die "Cant Remove File $!\n";
        }
        close RM;
        }

        my(@um_files,@xrm_files,@sim_files,@sec_files,@sod_files,@sys_files);

        # ===================================
        # Update Timestamp of Release
        # ===================================
        open(LOGFILE,">>".$SERVER_DIR{$Sys}."/setup.log")
                or die "Cant Write Install Log $SERVER_DIR{$Sys}/setup.log : $!\n";
        my $curtime=gmtime();
        print LOGFILE "Started Upgrade of $version At $curtime\n";
        print "Started Upgrade of $version At $curtime\n";
        close LOGFILE;

        # ===================================
        # Recursively Get Files !
        # ===================================
        foreach ( dirlist('.',1) ) {
                s/^\.//;
                if( /^\/DBinstall\// ) {
                        my($db)=basename(dirname($_));
                        if( $db eq 'xrm_db' ) {
                                push @xrm_files, ".".$_;
                        } elsif( $db eq 'um_stuff' ) {
                                push @um_files, ".".$_;
                        } elsif( $db eq 'xrm_security_db' ) {
                                push @sec_files, ".".$_;
                        } elsif( $db eq 'sybsystemprocs'  ) {
                                push @sys_files, ".".$_;
                        } elsif( $db eq 'xrm_sod_db'    ) {
                                push @sod_files, ".".$_;
                        } elsif( $db eq 'sim_db' ) {
                                push @sim_files, ".".$_;
                        } else {
                                print "DATABASE $db NOT IN ALLOWED LIST - IGNORING\n";
                        }
                }

                my($dir)=dirname($SERVER_DIR{$Sys}.$_);
                if( ! -d  $dir ) {
                        if($dir ne $SERVER_DIR{$Sys}) {
                                if( -d dirname($dir) ) {
                                        print "Creating $dir";
                                        mkdir( $dir,0777 ) or die "Cant make directory $dir\n";
                                } else {
                                        mkdir( $dir,0777 ) or die "Cant make director $dir\n";
                                        #mkpath($dir,1,0777) or die "Cant create directory $dir: $!.  If you are running from a remote system then this is acceptable and you should make the directories first.\n"
                                }
                        }
                }

                if( /\.pm$/ ) {
                        print "found library ".basename($_)."\n";
                        if( -d  $SERVER_C_DIR{$Sys}."/perl/site/5.00502/lib" ) {
                                print "COPYING LIBRARY to $SERVER_C_DIR{$Sys}/perl/5.00502/site/lib\n";

                                mycopy( ".".$_,$SERVER_C_DIR{$Sys}."/perl/site/5.00502$_" );
                        } else {
                                die "$SERVER_C_DIR{$Sys}/perl/site/5.00502/lib not found!\n";
                        }
                } else {
                        print "copying ".basename($_)."\n";
                        mycopy( ".".$_,$SERVER_DIR{$Sys}.$_ );
                }
        }

        # ===================================
        # Installing Database Objects
        # ===================================
        my($files);
        if( $#xrm_files>=0 ) {
                print "\n\ninstalling xrm_db\n";
                $files= join(" ",sort objsort @xrm_files);
                print "-> $files\n";
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'xrm_db',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'xrm_sod%db',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'xrm_acc%_db',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
        }

        if( $#sec_files>=0 ) {
                print "\n\ninstalling sec_db\n";
                $files= join(" ",sort objsort @sec_files);
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'xrm_security_db',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
        }

        if( $#um_files>=0 ) {
                print "\n\ninstalling um static data\n";
                $files= "./DBinstall/INSERT_SCRIPTS/um_stuff/um_login_function_save.sql ./DBinstall/INSERT_SCRIPTS/um_stuff/del_data.sql ./DBinstall/INSERT_SCRIPTS/um_stuff/l_um_function.ins ./DBinstall/INSERT_SCRIPTS/um_stuff/l_um_menu.ins ./DBinstall/INSERT_SCRIPTS/um_stuff/um_application.ins ./DBinstall/INSERT_SCRIPTS/um_stuff/um_application_function.ins ./DBinstall/INSERT_SCRIPTS/um_stuff/um_function_object.ins ./DBinstall/INSERT_SCRIPTS/um_stuff/um_login_function_res.sql";
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'xrm_security_db',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
        }

        if( $#sim_files>=0 ) {
                print "\n\ninstalling sim_db\n";
                $files= join(" ",sort objsort @sim_files);
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'sim_db',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
        }

        if( $#sys_files>=0 ) {
                print "\n\ninstalling sybsystemprocs\n";
                $files= join(" ",sort objsort @sys_files);
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'sybsystemprocs',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
        }

        if( $#sod_files>=0 ) {
                print "\n\ninstalling sod_db\n";
                $files= join(" ",sort objsort @sod_files);
                do_release(
                                        username   => $SERVER_LOGIN{$Sys},
                                        servername => $SERVER_DSQUERY{$Sys},
                                        password   => $SERVER_PASS{$Sys},
                                        dbname     => 'xrm_sod%_db',
                                        objects    => $files,
                                        batch_die  => 1 ) if defined $SERVER_DSQUERY{$Sys} and $SERVER_DSQUERY{$Sys} ne "";
        }

        # ===========================
        # Update Timestamp of Release
        # ===========================
        open(LOGFILE,">>".$SERVER_DIR{$Sys}."/setup.log") or die "Cant Write $SERVER_DIR{$Sys}/setup.log : $!\n";
        my $curtime=gmtime();
        print LOGFILE "Completed Upgrade of $version At $curtime\n";
        print "Completed Upgrade of $version At $curtime\n";
        close LOGFILE;

        # ===========================
        # Manage Version File
        # ===========================
        print "Writing VERSION file\n";
        unlink $SERVER_DIR{$Sys}."/VERSION" if -e $SERVER_DIR{$Sys}."/VERSION";
        open(VERSION,">".$SERVER_DIR{$Sys}."/VERSION")
                or die "Cant Write $SERVER_DIR{$Sys}/VERSION : $!\n";
        print VERSION $version."\n";
        close VERSION;

        # ===========================
        # Convert Config File
        # ===========================
        chdir $SERVER_DIR{$Sys} or die "Cant change directory $!\n";
        chdir 'web' or die "Cant change directory $!\n";
        convert_gui_config($Sys);
        if( defined $SETUP{"needs_configure"}
        and $SETUP{"needs_configure"} =~ /^\s*1\s*$/ ) {
                print "Running Web Interface Configure\n";
                require 'configure.pl';
        } else {
                print "Not Running Web Interface Configure\n";
        }
        #print $SETUP{"needs_configure"}."|\n";
}
exit();

sub dirlist
{
        my($dir,$recurse) = @_;

        opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
        my(@files)=readdir(DIR);
        closedir(DIR);

        my(@out);
        foreach (@files) {
                next if /^\./;
                if( -d $dir."/".$_ and $recurse ) {
                        push @out,dirlist($dir."/".$_,1);
                } else {
                        push @out,$dir."/".$_;
                }
        }

        return @out;
}

# sort rul/def/tbl/idx/prc/sql first

sub objsort {
        my($o1,$o2);
        $a=~/\.(\w+)$/;
        $o1=$1;
        $b=~/\.(\w+)$/;
        $o2=$1;
        my( %SORTKEYS )=(
                rul => 0,
                dropcst => 0,
                def => 1,
                tbl => 2,
                idx => 3,
                vie => 4,
                prc => 6,
                sql => 7,
                ins => 8
        );
        die "CANT  UNDERSTAND FILE TYPE $o1 - $a\n" unless defined $SORTKEYS{$o1};
        die "CAN'T UNDERSTAND FILE TYPE $o2 - $b\n" unless defined $SORTKEYS{$o2};
        return $SORTKEYS{$o1} - $SORTKEYS{$o2} if $SORTKEYS{$o1} != $SORTKEYS{$o2};
        $a cmp $b;
}

sub convert_gui_config
{
        my($env)=@_;
        my($filename)=$SERVER_DIR{$env}."/web/GUI_CONFIG.env";
        print "Converting $filename\n";

        open(SAVE_CFG_ITEMS_VBLSFILE,$filename) || die("Can't open file $filename: $!\n");
        while ( <SAVE_CFG_ITEMS_VBLSFILE> ) {
                        next if /^#/;
                        next if /^\s+$/;
                        chop;

                        # remove ctrl m's
                        my($i)=index($_,"
");

                        substr($_,$i,1)="" if $i>0;
                        next if /export/;
                        s/;//g;
                        s/"//g;
                        my($var,$val)=split("=",$_,2);

                        $SAVE_CFG_ITEMS{$var}=$val if defined $SAVE_CFG_ITEMS{$var};
        }
        close SAVE_CFG_ITEMS_VBLSFILE;

        # FIND FIRST FREE FILE
        my($i);
        if( -e "GUI_CONFIG.env" ) {
                $i=1;
                while( -e "GUI_CONFIG.$i" ) { $i++; }
                rename "GUI_CONFIG.env","GUI_CONFIG.$i"
                        or die "Cant rename file $!\n";
                print "SAVED CURRENT SAVE_CFG_ITEMS FILE IN GUI_CONFIG.$i <br>\n";
        }

        $SAVE_CFG_ITEMS{"VERSION"}=$version;

        open(WRCFG,">".$filename) || die("Can't open file $filename: $!\n");
        open(RDCFG,$filename.".sample") || die("Can't open file $filename.sample: $!\n");
        while ( <RDCFG> ) {
                chop;
                if( /=/ )  {
                        my($var,$val)=split("=",$_,2);
                        if(defined $SAVE_CFG_ITEMS{$var}) {
                                if( $SAVE_CFG_ITEMS{$var} =~ /\s/ ) {
                                        print "\tSUsing Saved ".$var."=\"".$SAVE_CFG_ITEMS{$var}."\"\n";
                                        print WRCFG $var."=\"".$SAVE_CFG_ITEMS{$var}."\"\n";
                                } else {
                                        print "\tUsing Saved ".$var."=".$SAVE_CFG_ITEMS{$var}."\n";
                                        print WRCFG $var."=".$SAVE_CFG_ITEMS{$var}."\n";
                                }
                        } else {
                                print WRCFG $_,"\n";
                        }
                } else {
                        print WRCFG $_,"\n";
                }
        }
        close RDCFG;
        close WRCFG;
}

# mycopy from to
#  - copy for nt = ignores permissions
sub mycopy
{
        my($from,$to)=@_;
        my($success)=1;
        copy( $from,$to )  or $success=0;
        if( $success==0 ) {
                my $target="C:/temp/".basename($to);
                die("ERROR - Cant copy $from and $to not exists\n")
                        unless -e $to;
                print "===> warning - copy failed trying rename first\n";
                print "===> rename($from,$target) \n";
                rename($to,$target)
                        or die "Cant Rename $from : $!\n";
                copy( $from,$to )
                        or die "Cant Copy $from : $!\n";
        }
}
__END__

=head1 NAME

setup.pl - install patch to an application as appropriate

=head2 DESCRIPTION

This is a customized package.  Installations must be customized for each
server / environment you wish to work with.

=head2 USAGE

setup.pl ENVIRONMENT

For a list of valid environments, run the command without any arguments.
Valid environments must be setup by editing this script.  See the CUSTOMIZATION
section.  You may run with the environment ALL which runs in all environments.

=head2 IMPLEMENTATION DETAILS

This program lives in the root directory of the patch release.    Database objects get installed based on the directory right above them.


=head2 CUSTOMIZATION

You must edit this script to define what the environments are.   Specifically, you must add your environment to the perl hashes to identify installation info.

=over 4

=item * %SERVER_DIR

Target directory for installation

=item * %SERVER_DSQUERY

DSQUERY of server for db objects

=item * %SERVER_LOGIN

LOGIN of server for db objects

=item * %SERVER_PASS

PASSWORD of server for db objects

=back

Use of environments allows you to install into remote directories and servers
transparently without specifying everything on the command line.

=head2 REQUIREMENTS

Requires do_release_func.pl and syb_func.pl to be installed either in the
current directory, the subdirectory utilities, or the LOCAL root.

=head2 TO DO

Should upgrade platinum  and gold whenever a release is made

Should do a better job checking for existing patches

Perhaps we should have a facility to see which patch upgrade we are at from
        the gui

Perhaps it should keep track of individual object upgrades in its own database
        On a per server/db/object/version basis.

=cut


