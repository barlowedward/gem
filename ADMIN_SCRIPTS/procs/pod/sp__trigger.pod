=head1 NAME

sp__trigger - Useful synopsis report of current database trigger schema

=head2 AUTHOR

Simon Walker, The SQL Workshop LTD.

=head2 DESCRIPTION

show all tables/triggers in current database

=head2 USAGE

sp__trigger [@tablename]

=head2 SEE ALSO

sp__helptrigger

=head2 SAMPLE OUTPUT

 1> exec sp__trigger

 table name         insert trigger     update trigger  delete trigger
 ------------------ ------------------ --------------- ---------------
 alerts             .................. ............... ...............
 audit_trail        .................. ............... ...............
 comn_database      .................. ............... ...............
 comn_dumpdevices   .................. ............... ...............
 comn_syscolumns    .................. ............... ...............
 comn_sysdevices    .................. ............... ...............
 comn_sysindexes    .................. ............... ...............
 comn_syslocks      lock_del_trigger   ............... ...............
 comn_syslogins     syslogin_insert_tr ............... ...............
 comn_sysobjects    .................. ............... ...............

