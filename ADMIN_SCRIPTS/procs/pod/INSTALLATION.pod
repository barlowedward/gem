=head1  INSTALLATION

The library is distributed in two ways.  The Generic Enterprise Manager
software incldues a copy and a full featured user interface to install
the procedures.  GEM also uses these procedures extensively - the console
creates numerous reports based on these procedures that can be viewed
using your web browser.  You can also download these procedures as a
standalone package.

The primary interface to install this library is to use the configure.pl program.
configure.pl is a perl script that requires DBI and
either DBD::Sybase (unix) or DBD::ODBC (windows).  You can also
use the older - unsupported .bat/.sh files that are shipped with the
distribution but let me repeat - they are not supported.  The .bat files
require two parameters SERVERNAME and PASSWORD (the installSQLSVR.bat
script only requires SERVERNEAME - it uses native authentication).

To run configure.pl type

  perl configure.pl -?
which should respond with

	Unknown option: ?
	usage: configure.pl --DEBUG --USER=sa --PASSWORD=pass --SERVER=svr --MINVERSION=ver --UPGRADE_TO_LATEST --ALLSERVERS|--SQLSERVERS|--SYBSERVERS [-FILE=File]
If it does not produce the above output on windows - and the cause is missing libraries (the above mentioned DBI and DBD::ODBC),
you can install them by running

  ppm install DBI
  ppm install DBD::ODBC

If the libaries are missing on unix, contact your unix administrator.

If the above command works (ie it displays the command syntax) - your perl has all necessary modules installed - you
can just run

   perl configure.pl
ignoring the command line arguments and it will ask you for SERVER/LOGIN/PASSWORD.  The login you use must have
sa_role (sybase) or sysadmin (sql server) role.

As system databases will be modified by these procedures, it is suggested that you dump that database before you load
the procedure.

If you are not able to get the perl installer working - you can install via the following unspported scripts:

 configure.sh - the original shell script installer
 installSQLSVR.bat
 installSYBASE12.bat
 installSYBASE15.bat
 installSYBASE_MDA.bat - must change to mda procedure directory

You are, of course, required to know the sa password to the server to install.
Well thats not quite true - sql server, if you enter an incorrect password,  will attempt to
use native windows authentication (so the install will work if you have admin role on the server).
Note that, on sybase, the default size for sybsystemprocs is a bit small - it is recommended you extend the
default size to use the full sysprocsdev device that gets allocated during installation.  You could always just
try an install and if you get out of space messages - to extend the space and reinstall.

