use strict;
use lib 'G:/dev/lib';
use Win32Scheduler;
use Data::Dumper;

my($t)=new Win32Scheduler(-name_prefix	=> 'gem_');
print "Scheduling ping_servers\n";
$t->schedule_a_job( 
	-name		=> 'ping_servers',
	-schedule	=> 'daily',
	-account	=> 'sqlaccount',
	-password	=> 'dev2000',
	-program	=> 'C:/gem/win32_batch_scripts/batch/ping_servers.bat',
	-directory	=> 'C:\\',
	-start_hour	=> 1,
	-start_minutes	=> 5
);

print "Scheduling ping_servers2\n";
$t->schedule_a_job( 
	-name		=> 'ping_servers2',
	-schedule	=> 'hourly',
	-account	=> 'sqlaccount',
	-password	=> 'dev2000',
	-program	=> 'C:/gem/win32_batch_scripts/batch/ping_servers.bat',
	-start_hour	=> 1,
	-start_minutes	=> 12
);
print "Re Scheduling ping_servers - to hourly\n";
$t->schedule_a_job( 
	-name		=> 'ping_servers',
	-schedule	=> 'hourly',
	-account	=> 'sqlaccount',
	-password	=> 'dev2000',
	-program	=> 'C:/gem/win32_batch_scripts/batch/ping_servers.bat',
	-directory	=> 'C:\\',
	-start_hour	=> 7,
	-start_minutes	=> 18,
	-end_hour	=> 13,
	-end_minutes	=> 18,
	-frequency	=> 13
);
#print "Re Scheduling ping_servers2\n";
#$t->schedule_a_job( 
	#-name		=> 'ping_servers2',
	#-schedule	=> 'weekly',
	#-account	=> 'sqlaccount',
	#-password	=> 'dev2000',
	#-program	=> 'C:/gem/win32_batch_scripts/batch/ping_servers.bat',
	#-start_hour	=> 10,
	#-start_minutes	=> 19
#);

my($rc)=$t->get_all_tasks();
print Dumper $rc;

