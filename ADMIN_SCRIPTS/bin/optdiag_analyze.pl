#/apps/sybmon/perl/bin/perl

use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use strict;
use Getopt::Long;
use Do_Time;
use DBIFunc;
use CommonHeader;
use CommonFunc;
use Sys::Hostname;
use Repository;
use File::Basename;

use vars qw( $USER $SERVER $SAVEFILE $RUNALL $PASSWORD $NOSTDOUT $HTML $opt_d $DATABASE $OPTDIAG $HTML $INFILE $REBUILD $NOEXEC $OUTFILE );

sub usage
{
	print @_;
	print basename($0)." -USER=SA_USER [-INFILE=file] [--NOSTDOUT] --OUTFILE=File --OPTDIAG=optdiag -DATABASE=db -SERVER=SERVER -PASSWORD=SA_PASS [-debug] [-REBUILD] [-NOEXEC]\n";

   return "\n";
}

$| =1; 				# unbuffered standard output

# change directory to the directory that contains THIS file
my($curdir)=cd_home();

die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"USER=s"			=> \$USER ,
		"PASSWORD=s"	=> \$PASSWORD ,
		"RUNALL" 		=> \$RUNALL ,
		"OPTDIAG=s"		=> \$OPTDIAG,
		"OUTFILE=s"		=> \$OUTFILE,
		"NOSTDOUT"		=> \$NOSTDOUT,
		"HTML" 			=> \$HTML ,
		"SAVEFILE"		=> \$SAVEFILE ,
		"INFILE=s" 		=> \$INFILE ,
		"REBUILD" 		=> \$REBUILD ,
		"NOEXEC" 		=> \$NOEXEC ,
		"DATABASE=s" 	=> \$DATABASE ,
		"DEBUG"      	=> \$opt_d );

die usage("Must pass server\n" )		unless defined $SERVER   or defined $INFILE or $RUNALL;
die usage("Must pass username\n" )	unless defined $USER     or defined $INFILE or $RUNALL;
die usage("Must pass password\n" )	unless defined $PASSWORD or defined $INFILE or $RUNALL;

open(OUTF,">$OUTFILE") if $OUTFILE;

sub myprint {
	my($flag)=shift;
	print @_ unless $NOSTDOUT and ! $flag;
	print OUTF @_ if $OUTFILE and $flag != 2;
}

my($NL)="\n";
$NL="<br>\n" if $HTML;
dbi_set_web_page(1) if $HTML;
myprint(1, "Optdiag analyzer v1.0",$NL );
myprint(1, "Run at ".localtime(time)." on host ".hostname().$NL );
myprint(1, "The following chart lists information necessary to identify Tables to Reorg and/or Rebuild",$NL );
myprint(1, "DPCR: data page cluster ratio",$NL);
myprint(1, "SPUT: space utilization",$NL);
myprint(1, "LGIO: large I/O",$NL);
myprint(1, "IPCR: index page cluster ratio",$NL);
myprint(1, "DRCR: data row cluster ratio",$NL);
myprint(1, $NL,"We request feedback on our rating algorithm... Fundamentally - im not sure what to report...",$NL);

if( $INFILE or ($SERVER and $DATABASE) ) {
	if( ! $USER or ! $PASSWORD ) {
		($USER,$PASSWORD)=get_password(-type=>"sybase",-name=>$SERVER);
	}
	run_a_server($SERVER,$USER,$PASSWORD,$DATABASE);
} elsif( $SERVER ) {
	myprint(1, "Processing Optdiag Results for $SERVER",$NL );
	if( ! $USER or ! $PASSWORD ) {
		($USER,$PASSWORD)=get_password(-type=>"sybase",-name=>$SERVER);
	}
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD);
	if( ! $rc ) {
		print "Cant connect to $SERVER as $USER",$NL;
		next;
	}
	my($typ,@db)=dbi_parse_opt_D("%",1,1);
	dbi_disconnect();
	foreach my $DATABASE (@db) { run_a_server($SERVER,$USER,$PASSWORD,$DATABASE); }
} elsif( defined $RUNALL ) {
	debug("Running on all servers",$NL);
	my(@serverlist)=get_password(-type=>"sybase",-name=>undef);
	foreach my $server (@serverlist) {
		myprint(1, "Processing Optdiag Results for $server ",$NL );
	   ($USER,$PASSWORD)=get_password(-type=>"sybase",-name=>$server);
	   my($rc)=dbi_connect(-srv=>$server,-login=>$USER,-password=>$PASSWORD);
		if( ! $rc ) {
			print "Cant connect to $server as $USER",$NL;
			next;
		}
		my($typ,@db)=dbi_parse_opt_D("%",1,1);
		dbi_disconnect();
		foreach my $DATABASE (@db) { run_a_server($server,$USER,$PASSWORD,$DATABASE); }
	        # run_a_server($server,$USER,$PASSWORD,$DATABASE);
	}
} else {
	die "Must pass server options\n";
}
close(OUTF) if $OUTFILE;
close(OUT) if $SAVEFILE;
exit(0);

sub run_a_server {
	my($SERVER,$USER,$PASSWORD,$DATABASE)=@_;
	myprint(1,"========================================".	$NL);
	myprint(1,"Working on server $SERVER DB=$DATABASE".	   $NL);
	myprint(1,"========================================".	$NL);

my($cmd_no_pass);
my(@INDATA);
if( defined $INFILE and ! $SAVEFILE) {
	myprint(1,"Reading Input File $INFILE",$NL);
	open(OPTD,$INFILE) or die "Cant read file $INFILE $!",$NL;
	while (<OPTD>)	 {
		push @INDATA,$_;
		if(/^Server name:/) {
			chomp;
			s/Server name://;
			s/^\s+//;
			s/\"//g;
			$SERVER=$_;
			myprint(1,"SETTING SERVER to $SERVER",$NL);
		}
		if(/^Specified database:/) {
			chomp;
			s/Specified database://;
			s/^\s+//;
			s/\"//g;
			$DATABASE=$_;
			myprint(1,"SETTING DATABASE to $DATABASE",$NL);
		}
	}
	close(OPTD);
} else {
	if( defined $SAVEFILE ) {
		open(OUT,">".$SAVEFILE) or die "Cant write file $SAVEFILE $!",$NL;
	}
	my($cmd)="optdiag statistics $DATABASE -U$USER -P$PASSWORD -S$SERVER ";
	$cmd_no_pass="optdiag statistics $DATABASE -U$USER -Pxxx -S$SERVER ";
	myprint(1,"Running $cmd_no_pass",$NL);
	open(OPTD,"$cmd |") or die "Cant run optdiag",$NL;
	while (<OPTD>)	 {
		print OUT $_ if $SAVEFILE;
		push @INDATA,$_;
	}
	close(OPTD);
}
myprint( 2,"Found $#INDATA rows of optdiag data",$NL );
debug( "Connecting to the database".$NL );
my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD);
die "Cant connect to $SERVER as $USER\n" if ! $rc;

debug("Connected to $SERVER as $USER".$NL);
my(%table_type);
my($sql)="select user_name(uid),name, case when sysstat2 & 8192 != 0 then 'allpages'
				when  sysstat2 & 16384 != 0 then 'datapages'
				when  sysstat2 & 32768 != 0 then 'datarows'
				else '?'
	end
	from sysobjects where type='U'";
foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
	my($usr,$tbl,$ttype)= dbi_decode_row($_);
	$table_type{$usr.".".$tbl}=$ttype;
	#$print "TABLE TYPE $usr $tbl = $ttype \n";
}
myprint( 2,"Fetch of table type information completed successfully.".$NL );
dbi_disconnect();

my($success);
my($tblname,$tblowner,$index);
my(%rows,%dpcr, %sput, %lgio, %ipcr, %drcr, %allkeys);
my($key);
my($optdiag_failed)="FALSE";
my(@messages);
myprint(1,"Parsing Input Data",$NL);
foreach (@INDATA)	 {
	chop;chomp;
	print "dbg: $_\n" if $opt_d;
	if( /Can\'t get localized message./ ) {
		myprint(1, "ERROR>>> FATAL ERROR: OPTDIAG LOCALIZATION IS INCORRECT!",$NL );
		myprint(1, "ERROR>>> \$SYBASE ($ENV{SYBASE})",$NL );
		myprint(1, "ERROR>>> \$LD_LIBRARY_PATH ($ENV{LD_LIBRARY_PATH})",$NL );
		myprint(1, "ERROR>>> OPTDIAG HAS FAILED!",$NL,$NL );
		myprint(1, $NL,"$cmd_no_pass returned:",$NL );
		foreach (@messages) { myprint(1, ">>>",$_,$NL); }
		myprint(1,">>>",$_,$NL);
		#exit(0);
		@messages=();
		$optdiag_failed="TRUE";
		last;
	}
	if( /Optdiag failed./ ) {
		myprint(1, "ERROR>>> OPTDIAG on $SERVER db=$DATABASE as $USER FAILED!",$NL );
		foreach (@messages) { myprint(1, "\t(error)",$_,$NL); }
		myprint(1,$_,$NL);
		@messages=();
		$optdiag_failed="TRUE";
		last;
	}
	if( $optdiag_failed eq "TRUE" ) {
		myprint(1, "\t(error)",$_,$NL );
		next;
	}
	push @messages,$_ unless $opt_d;
	chomp;
	s/^\s+//;
	s/\"//g;
	if(/^Table owner:/){
		s/Table owner:\s+//;
		($tblname,$tblowner,$index)=("",$_,"");
	} elsif( /^Table name:/){
		s/Table name:\s+//;
		$tblname=$_;
		$key=$tblowner."|".$tblname."|".$index;
		$allkeys{$key}=1;
	} elsif( /^Data page cluster ratio:/){
		s/Data page cluster ratio:\s+//;
		$dpcr{$key}=$_;
	} elsif( /^Index page cluster ratio:/){
		s/Index page cluster ratio:\s+//;
		$ipcr{$key}=$_;
	} elsif( /^Space utilization:/ ){
		s/Space utilization:\s+//;
		$sput{$key}=$_;
	} elsif( /^Data row cluster ratio:/ ){
		s/Data row cluster ratio:\s+//;
		$drcr{$key}=$_;
	} elsif( /^Large I\/O efficiency:/ ){
		s/Large I\/O efficiency:\s+//;
		$lgio{$key}=$_;
	} elsif( /^Data row count:/ ){
		s/Data row count:\s+//;
		$rows{$tblowner."|".$tblname}=int($_);
	} elsif( /^Statistics for table:/){
		s/Statistics for table:\s+//;
		$index="";
		$tblname = $_;
		$key=$tblowner."|".$tblname."|".$index;
		$allkeys{$key}=1;
	} elsif( /^Statistics for index:/){
		s/Statistics for index:\s+//;
		$index=$_;
		$key=$tblowner."|".$tblname."|".$index;
		$allkeys{$key}=1;
	} elsif( /^Optdiag succeeded./ ) {
		$success="TRUE";
	}
}

myprint(1,"Reporting On Input Data",$NL);
if( $optdiag_failed eq "TRUE" ) {
	myprint(0, "<h2>"  ) if $HTML;
	myprint(1, "\nOPTDIAG FAILED FOR SERVER $SERVER DATABASE $DATABASE\n" );
	myprint(0, "</h2>" ) if $HTML;
	return;
}

myprint(0, "<h2>"  ) if $HTML;
myprint(0, "\nSERVER $SERVER DATABASE $DATABASE\n" );
myprint(0, "</h2>" ) if $HTML;
my($rowsfound)=0;
my($header);
if( $HTML ) {
	$header="<TR BGCOLOR=BROWN><TH>Table</TH><TH>Index</TH><TH>Locking</TH><TH>Rows</TH><TH>DPCR</TH><TH>SPUT</TH><TH>LGIO</TH><TH>IPCR</TH><TH>DRCR</TH><TH>ADVICE</TH></TR>\n";
} else {
	$header=sprintf("%30s %40s %10s %10s %10s %10s %10s %10s %10s %10s\n","Table","Index","Locking","Rows","DPCR","SPUT","LGIO","IPCR","DRCR","ADVICE");
}

myprint(2, "Looping through keys\n") if $opt_d;
foreach ( sort keys %allkeys ) {
	next unless $dpcr{$_} or $sput{$_} or $lgio{$_} or $ipcr{$_} or $drcr{$_};
	my($own,$tbl,$ind)=split(/\|/,$_,3);
	$tbl=$own.".".$tbl if $own ne "dbo";
	next unless $rows{$own."|".$tbl} > 1000;

	if( $rowsfound==0 ) {	# print section header
		myprint(0, "<br><TABLE BORDER=1>" ) if $HTML;
	}
	if( $rowsfound % 15 == 0 ) {	# print section header
		myprint(2, "Printing header at row $rowsfound\n" ) if $opt_d;
		myprint(0, $header);
	}
	my($recommendation, $rowcolor)=("","");
	if( $table_type{$own.".".$tbl} eq "allpages" ) {
		if( $ind =~ /\(nonclustered\)/ ) {
			if( $ipcr{$_} < .5 ) {
				$recommendation.="IPCR<.5 Drop & Recreate NC Indx";
				$rowcolor="BGCOLOR=PINK";
			} elsif( $sput{$_} < .5 ) {
				$recommendation.="SPUT<.5 Reorg Rebuild Recommended";
				$rowcolor="BGCOLOR=PINK";
			} elsif( $lgio{$_} < .4 ) {
				$recommendation.="LGIO<.4 Recreate NonClustered Indx";
				$rowcolor="BGCOLOR=PINK";
			}
		} else {
			if( $sput{$_} > 1 ) {
				$recommendation.="SPUT>1 Need Update Statistics";
				$rowcolor="BGCOLOR=PINK";
			} elsif( $dpcr{$_} < .7 ) {
				$recommendation.="DPCR<.7 Reorg Recommended";
				$rowcolor="BGCOLOR=PINK";
			} elsif( $lgio{$_} < .5 and $ind =~ /cluster/ ) {
				$recommendation.="LGIO<.5 Recreate Clustered Indx";
				$rowcolor="BGCOLOR=PINK";
			}
		}
	} elsif( $table_type{$own.".".$tbl} eq "datapages"
	or       $table_type{$own.".".$tbl} eq "datarows" ) {
			if( $sput{$_} > 1 ) {
				$recommendation.="SPUT>1 Need Update Statistics";
				$rowcolor="BGCOLOR=PINK";
			} elsif( $sput{$_} < .5 ) {
				$recommendation.="SPUT<.5 Need Reorg Rebuild";
				$rowcolor="BGCOLOR=PINK";
			} elsif( $ipcr{$_} < .5 and $ipcr{$_}>0 ) {
				$recommendation.="IPCR<.5 Reorg Recommended";
				$rowcolor="BGCOLOR=PINK";
			} elsif( $lgio{$_} < .4 and $lgio{$_} > 0  ) {
				$recommendation.="LGIO<.4 Reorg Recommended";
				$rowcolor="BGCOLOR=PINK";
			}
	} else {
	}


	if( $HTML ) {
		myprint(0, "<TR $rowcolor><TD>$tbl</TD><TD>$ind</TD><TD>".
				$table_type{$own.".".$tbl}."</TD><TD>".
				$rows{$own."|".$tbl}."</TD><TD>".
				roundit($dpcr{$_})."</TD><TD>".
				roundit($sput{$_})."</TD><TD>".
				roundit($lgio{$_})."</TD><TD>".
				roundit($ipcr{$_})."</TD><TD>".
				roundit($drcr{$_})."</TD><TD>".
				$recommendation."</TD></TR>\n" );
	} else {
		myprint(0, sprintf("%30s %40s %10d %10f %10f %10f %10f %10f %10s %s\n",$tbl,$ind,
				$table_type{$own.".".$tbl},
				$rows{$own."|".$tbl},
				roundit($dpcr{$_}),roundit($sput{$_}),roundit($lgio{$_}),roundit($ipcr{$_}),roundit($drcr{$_})),
				$recommendation );
	}
	$rowsfound++;
}

if( $HTML and $rowsfound>0 ) {
	myprint(2, "Printing End Table Tag\n") if $opt_d;
	myprint(0, "</TABLE>\n" );
}

if( $rowsfound == 0 ) {
	myprint(1, "NO DATA FOUND\n" );
	myprint(1, "<br>" ) if $HTML;
}
	myprint(2,"Done Working on server $SERVER DB=$DATABASE".	   $NL);

}

sub roundit {
	my($val)=@_;
	return int($val*100)/100;
}

#return unless defined $REBUILD;
#myprint(1, "REBUILDING.....\n" );
#myprint(1, sprintf("%30s %30s %10s %10s\n","Table","Index","Rows","Ratio") );
#foreach ( sort keys %rows ) {
#	next unless $rows{$_} > 100;	# and $dpcr{$_} < .8;
#	my($own,$tbl,$ind)=split(/\|/,$_,3);
#	$tbl=$own.".".$tbl if $own ne "dbo";
#	$ind=~s/\(nonclustered\)//;
#	$ind=~s/\(clustered\)//;
#	$ind=~s/\s+//;
#	if( ! defined $table_type{$tbl}  and ! defined $table_type{"dbo.".$tbl} ) {
#		myprint(1, "skipping $tbl as its not datapages/datarows \n" );
#		next;
#	}
#	my($starttime)=time;
#	my $sql = "reorg rebuild $tbl $ind";
#	if( defined $NOEXEC ) {
#		myprint(1, "(noexec) $sql\n" );
#	} else {
#		debug( "query  is $sql\n" );
#		foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
#			my @dat=dbi_decode_row($_);
#			myprint(1, "Query Returned : ",join(" ",@dat),"\n" );
#		}
#	}
#	myprint(1, sprintf("%40s %10d %s\n",$tbl.".".$ind,$rows{$_},do_diff(time-$starttime)) );
#}

sub debug {
	myprint(1, @_ ) if defined $opt_d;
}

__END__

=head1 NAME

optdiag_analyze.pl - anlyze optdiag output and reorg rebuild if cluster ratio sucks

=head2 USAGE

optdiag_analyze.pl -USER=SA_USER [-FILE=file] [--NOSTDOUT] --OUTFILE=File --OPTDIAG=optdiag -DATABASE=db -SERVER=SERVER -PASSWORD=SA_PASS [-debug] [-REBUILD] [-NOEXEC]

=head2 DESCRIPTION

Analyzes optdiag output and produces a fragmentation report as per the algorithms described
at 2006 techwave.  Need to validate this by lock scheme.  Uses the following keys:

Data page cluster ratio:
Index page cluster ratio:
Data row cluster ratio:
Space utilization:
Large I/O efficiency:

=cut
