#!/apps/perl/linux/perl-5.8.2/bin/perl
#!/apps/perl/solaris/perl-5.8.1/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use DBIFunc;
use MlpAlarm;
use CommonFunc;
use Getopt::Long;
use CommonHeader;

$|=1;

use vars qw( $DAYS $EVDAYS $HBDAYS $DEBUG );

sub usage {
	print @_,"\n";
 	return "Usage: MlpAlarmCleanupDB.pl --DAYS=days --DEBUG --HBDAYS=days --EVDAYS=days\n";
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
    GetOptions(
		"DAYS=s"		=>	\$DAYS,
		"HBDAYS=s"	=>	\$HBDAYS,
		"DEBUG"	=>	\$DEBUG,
		"EVDAYS=s"	=>	\$EVDAYS
	);

$HBDAYS=$DAYS unless defined $HBDAYS;
$EVDAYS=$DAYS unless defined $EVDAYS;

die "Days must be passed\n" unless $EVDAYS;
die "Days must be passed\n" unless $EVDAYS>0;
die "Days must be passed\n" unless $HBDAYS;
die "Days must be passed\n" unless $HBDAYS>0;

print "Starting AlarmCleanupDB\n";
MlpBatchJobStart(-BATCH_ID=>"AlarmCleanupDB");

#print "Starting Config File Sync\n";
#MlpSyncConfigFiles(-debug=>$DEBUG);

# We do this so logs dont fill
print "Starting MlpCleanup Pass 1\n";
my(@rc) = MlpCleanup( 	-hbdays	=> ($HBDAYS+6), -evdays=>($EVDAYS+6), -days=>($EVDAYS+6), -debug=>$DEBUG );
foreach (@rc) { chomp; print $_,"\n"; }

print "Starting MlpCleanup Pass 2\n";
my(@rc) = MlpCleanup( 	-hbdays	=> ($HBDAYS+3), -evdays=>($EVDAYS+3), -days=>($EVDAYS+3), -debug=>$DEBUG );
foreach (@rc) { chomp; print $_,"\n"; }

print "Starting MlpCleanup Pass 3\n";
my(@rc) = MlpCleanup( 	-hbdays	=> $HBDAYS, -evdays=>$EVDAYS, -days=>$EVDAYS, -debug=>$DEBUG );
foreach (@rc) { chomp; print $_,"\n"; }

print "Cleaning Any Systems Containing Ip Addresses\n";
my(@rc2) = MlpCleanup( -cleanservers	=> 1 );
foreach (@rc2) { chomp; print "\t",$_,"\n"; }

#print "Rebuilding COntainers\n";
#my(@rc2) = MlpManageData( Admin	=> "Rebuild Containers" );
#foreach (@rc2) { chomp; print $_,"\n"; }

print "Marking Batch Done\n";
MlpBatchJobEnd();
__END__

=head1 NAME

MlpAlarmCleanupDB.pl - perform alarm system maintenance

=head2 DESCRIPTION

This batch should be used to perform necessary maintenance on your
alarm system.  You can pass in --DAYS or --HBDAYS and --EVDAYS. In
addition to cleaning up the database, it performs necessary maintenance.

This jobs should be scheduled daily.

=head2 USAGE

Usage: MlpAlarmCleanupDB.pl --DAYS=xxx --HBDAYS=days --EVDAYS=days

=cut
