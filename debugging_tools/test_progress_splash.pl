use lib '../lib';
use Tk;
BEGIN {
require Tk::ProgressSplash;

$splashscreen=Tk::ProgressSplash->Show(
	-splashtype=>'safe',"../doc/gem.gif",350,200,"Gem V1.0",0);

}

$splashscreen->Update(0.1);
$splashscreen->Update(1);
$splashscreen->Destroy;
MainLoop;
