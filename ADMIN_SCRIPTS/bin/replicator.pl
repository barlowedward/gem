#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# SIMPLE REPLICATOR
#  see bottom of the file for documentation in perldoc format.  To
#  view documentation, type perldoc filename
#
# Copyright (c) 2001
# All Rights Reserved

sub usage
{
   die @_ . "replicator.pl [-c configfile ] -dusSl

   -c cfg,cfg     configuration file name (default replicate.cfg)
   -D             define the triggers if they are not allready there
   -u             uninstall
   -S             starts the configuration
   -l             active configuration and statistics report
   -d             debug mode

   one of -d,s,S,or l must be specified

\n";
}

use strict;
use Getopt::Std;
use Sybase::DBlib;
use Sybase::SybFunc;

use vars qw($opt_c $opt_d $opt_u $opt_S $opt_D);
usage("") if $#ARGV<0;
usage("Bad Parameter List") unless getopts('c:duSdD');
$opt_c="replicator.cfg" unless -e $opt_c;
usage("Cant read config file $opt_c\n") unless -r $opt_c;

# read the configuration file
my(%cfg) = read_config( $opt_c );
foreach (keys %cfg) {
	print "   (debug only) => $_ $cfg{$_} \n";
}

if( defined $opt_D ) {
	configure();
	exit(0);
} elsif( defined $opt_u ) {
	print "*************************************************************\n";
   print "Are you sure you wish to uninstall? please check user defined
trigger (identfy these...) as they are not cleaned up.  \n";
	print "Hit any key to continue\n";
	print "*************************************************************\n";
	<STDIN>;
	configure();
	exit(0);
} elsif( defined $opt_S ) {
	replicate();
} else {
	usage( "Exiting - paramaters passed\n" );
}

#
# replicate from SRCREPL database in SRCSRV to DESTSERV/DESTDB combinations
#
my(%InsFmtStringByTable,%DelFmtStringByTable);
sub replicate {

   print "Replication started\n";

	print "connecting to the source server $cfg{SRCSRV}\n" if defined $opt_d;
	db_connect($cfg{SRCSRV},$cfg{SRCUSR},$cfg{SRCPASS})
        or die "Cant connect to $cfg{SRCSVR} as $cfg{SRCUSR}\n";

	connect_to_dest();

	my(%tbllist);
	foreach ( split( /,/, $cfg{TABLES} ) ) { $tbllist{$_}=1; }

	while (1) {
		#
		# Find Tables Out There To Replicate From
		# select count(*) from tables
		#
		my($query) = "
	select 	object_name(i.id),rowcnt(i.doampg)
	from 		sysindexes i
	where 	rowcnt(i.doampg)!=0
	";

		my(@tables_and_rows)=db_query( $cfg{SRCREPL}, $query );
		foreach ( @tables_and_rows ) {
			my($objectnm,$rcnt)=db_decode_row($_);
			next unless $objectnm =~ /^r_/;
			print "  table ",$objectnm , " has ", $rcnt, " rows\n";
		}
		print "\n" if $#tables_and_rows>=0;


		#
		# ok now loop through tables and process each... alphabetically works
		#
		foreach ( @tables_and_rows ) {
			my($objectnm,$rcnt)=db_decode_row($_);
			next unless $objectnm =~ /^r_/;
			print "processing ",$objectnm , " ( ", $rcnt, " rows )\n";

			# ensure that format string is set up
			if( ! defined $InsFmtStringByTable{$objectnm} ) {
				# get format string
				($InsFmtStringByTable{$objectnm}, $DelFmtStringByTable{$objectnm})
					= GetFormatString($objectnm);
				print "  format strings defined for $objectnm\n" if defined $opt_d;
			}

			# do the start tran on all servers simultaneously
			run_query_on_dest("BEGIN TRAN repl_trans" );
			foreach ( db_query( $cfg{SRCREPL}, "BEGIN TRAN main_trans" )) {
				die "Insert Failed: BEGIN TRAN main_trans";
			}

			# updated rows in the table to in progress
			print "  marking rows as transfering \n" if defined $opt_d;
			foreach ( db_query( $cfg{SRCREPL}, "update $objectnm set r_xfer='x'")){
				die "Error : $_ \n";
			}

			# select stuff from the table
			foreach ( db_query( $cfg{SRCREPL}, "select * from $objectnm where r_xfer='x' order by r_trig_time,r_trig_type" ,
						undef,undef,undef,
						undef,undef,undef,
						"NULLPLACEHOLDER" )) {

				s/\"/\\"/g;

				my(@dat)=db_decode_row($_);
				my($trigtype) = shift @dat;
				shift @dat;
				shift @dat;

				my($replquery);
				if( $trigtype eq "i" ) {
					print "Update Trigger Inserted Row\n";
					$replquery=sprintf($InsFmtStringByTable{$objectnm},@dat);
				}elsif( $trigtype eq "d" ) {
					print "Update Trigger Deleted Row\n";
					$replquery=sprintf($DelFmtStringByTable{$objectnm},@dat);
				}elsif( $trigtype eq "I" ) {
					print "Insert Trigger Row\n";
					$replquery=sprintf($InsFmtStringByTable{$objectnm},@dat);
				}elsif( $trigtype eq "D" ) {
					print "Delete Trigger Row\n";
					$replquery=sprintf($DelFmtStringByTable{$objectnm},@dat);
				} else {
					print "Undefined trigtype Trigger Row\n";
				}

				$replquery =~ s/\"*NULLPLACEHOLDER\"*/NULL/g;
				run_query_on_dest( $replquery );
			}

			# delete rows in the table
			foreach (db_query( $cfg{SRCREPL},"delete $objectnm where r_xfer='x'")){
				die "Error : $_ \n";
			}

			run_query_on_dest("COMMIT TRAN repl_trans" );
			foreach ( db_query( $cfg{SRCREPL}, "COMMIT TRAN main_trans" )) {
				die "Insert Failed: COMMIT TRAN main_trans";
			}
		}

		# if rows then update them
		print "Sleeping for $cfg{POLLTIME} seconds....\n";
		sleep($cfg{POLLTIME});
	}
}

sub GetFormatString {
	my($tbl)=@_;

	# BUILD STRING FOR INSERT
	#     helpcolumn returns ordered list of column names & types
	#     we want a string that looks like
   #     INSERT $tbl ( $field_name_list ) VALUES ( $format_str )

	my($ins_header) =  "INSERT $tbl (\n\t";
	my($del_string) =  "DELETE $tbl WHERE \n";
	$ins_header =~ s/r_//;
	$del_string =~ s/r_//;
	my($hdr_cnt)=0;		# used for indenting
	my($format_str)="";
	foreach ( db_query( $cfg{SRCREPL},
				"exec sp__helpcolumn \@objname=$tbl, \@dont_format='Y'",) ) {
		$hdr_cnt++;
		next unless $hdr_cnt>3;
      s/^\s//g;
      my(@rc)=db_decode_row($_);
		$rc[0]=~s/\s//g;

		$ins_header.=$rc[0].",\t";
		$ins_header.="\n\t" if $hdr_cnt%3==0;

		$del_string .= " $rc[0] = ";

      if(     $rc[1] !~ /int/
           and $rc[1] !~ /float/
           and $rc[1] !~ /decimal/
           and $rc[1] !~ /numeric/
           and $rc[1] !~ /money/
           and $rc[1] !~ /double/ ) {
			$format_str .= "\"%s\"";
			$del_string .= "\"%s\"";
		} else {
			$format_str .= "%s";
			$del_string .= "%s";
		}
		$del_string   .= " and \n";
      $format_str   .= ",\t" unless $hdr_cnt == 0;
		$format_str   .= "\n\t" if $hdr_cnt%3==0;
	};

	$ins_header    =~ s/,\t$//;
	$format_str 	=~ s/,\t$//;
	$del_string    =~ s/ and \n$//;
   return ( $ins_header."\n\t) VALUES (\n\t$format_str )\n",
				$del_string );
}

sub configure {
	print "Starting configuration process...\n";
	my(%revtable);		# table creation scripts for each table

	my(@tables) = split( /,/ , $cfg{TABLES} );
	die "Must have Defined Some TABLES to replicate in the config file"
		if $#tables==-1;

	print "Connecting to Source Server ...\n";
	db_connect($cfg{SRCSRV},$cfg{SRCUSR},$cfg{SRCPASS})
        or die "Cant connect to $cfg{SRCSVR} as $cfg{SRCUSR}\n";

	print "Checking for prior installs\n";
	my(@rc)=db_query($cfg{SRCREPL},"Select name from sysobjects where type='U' and name like 'r_%'");
	if( $#rc < 0 ) {
		print " ... new install\n";
	} else {
   	print "Dropping tables in replication db\n";
		foreach( @rc ) {
			my($query)="Drop Table ".$_."\n";
			foreach (db_query($cfg{SRCREPL},$query)) { die "SQL Failed: $_"; }
			print $query if defined $opt_d;
		}

		my(@rc)=db_query($cfg{SRCDB},"Select name from sysobjects where type='TR' and name like 'r_%'");
   	print "Dropping triggers in replication db\n";
		foreach( @rc ) {
			my($query)="Drop Trigger ".$_."\n";
			foreach (db_query($cfg{SRCDB},$query)) { die "SQL Failed: $_"; }
			print $query if defined $opt_d;
		}
	}
	die "
***************************************
*   System Uninstalled Successfully   *\n
***************************************\n" if defined $opt_u;

   print "Defining tables\n";
	my($query)=
		"select 	name from 	sysobjects
		where name in ('".join("\'\,\'",@tables)."')\n";
	my(@rc)=db_query($cfg{SRCDB},$query);

	die "Hmmm No Tables Found In $cfg{SRCDB} to Replicate\nTables: ".join(" ",@tables)." Should have been found\n" if $#rc == -1;
	if( $#rc != $#tables ) {
		my(%found);
		foreach (@tables) { $found{$_}="Required But Not Found"; }
		foreach (@rc) {
			if( $found{$_} eq "Required But Not Found" ) {
				undef $found{$_};
			} else {
				$found{$_} = "Hmmmm This Shouldnt Happen\n";
			}
		}
		die "There is a missmatch between the tables in the configuration file and in the server".join("/n",values(%found))."\n";
	}

	foreach  my $tb ( @rc ) {
		print " Table => $tb\n" if defined $opt_d;

		# get table text info
		db_use_database($cfg{SRCDB});
		my($tblstr)=db_get_sql_text( $tb, 1 );

		# modify table text for new table r_yyyyy
		$revtable{$tb} = $tblstr;
		$tblstr =~ s/$tb/r_$tb/;
		$tblstr =~ s/\n\(/\n\(\n r_trig_type\tchar\(1\),\n r_xfer\tchar\(1\),\n r_trig_time\ttimestamp,/;
		$tblstr =~ s/\ngo\n[\w\W\s]*/\n/;
		print $tblstr if defined $opt_d;
		foreach (db_query($cfg{SRCREPL},$tblstr)) { die "SQL Failed: $tblstr"; }
	}

   print "Defining triggers\n";
	my(@rc)=db_query($cfg{SRCDB},
		"select 	name,
					object_name(instrig),
					object_name(deltrig),
					object_name(updtrig)
		from sysobjects
		where ( instrig !=0 or deltrig !=0 or updtrig !=0 )
		and name in ('".join(/'/,@tables)."')");

	my(%ins,%upd,%del);
	foreach  ( @rc) {
		my($n,$i,$d,$u) = db_decode_row($_);
		$ins{$n} = $i if $i != 0;
		$upd{$n} = $u if $u != 0;
		$del{$n} = $d if $d != 0;
		print "found trig: $n $i $d $u\n";
	}

	foreach (@tables) {
      my($tnm)=substr("r_d_".$_,0,30);
      $query="create trigger $tnm on $_ for delete as
      begin
         insert
         into $cfg{SRCREPL}..r_$_
		   select \'D \',' ',NULL,* from deleted
      end\n";
	   print $query if defined $opt_d;
	   foreach (db_query($cfg{SRCDB},$query)) { die "SQL Failed: $_"; }

      $tnm=substr("r_i_".$_,0,30);
      $query="create trigger $tnm on $_ for insert as
      begin
         insert
         into $cfg{SRCREPL}..r_$_
		   select \'I \',' ',NULL,* from inserted
      end\n";
	   print "[ ",$query," ]\n" if defined $opt_d;
	   foreach (db_query($cfg{SRCDB},$query)) { die "SQL Failed: $_"; }

      $tnm=substr("r_u_".$_,0,30);
	   $query="create trigger $tnm on $_ for update as
      begin
         insert
         into $cfg{SRCREPL}..r_$_
		   select \'d\',' ',NULL,* from deleted

         insert
         into $cfg{SRCREPL}..r_$_
		   select \'i\',' ',NULL,* from inserted
      end\n";
	   print "[ ",$query," ]\n" if defined $opt_d;
	   foreach (db_query($cfg{SRCDB},$query)) { die "SQL Failed: $_"; }
	}
	print "Completed source side configuration...\n";
	db_close_sybase();

#	print "Started  destination side configuration...\n";
#	connect_to_dest();
#
#	foreach my $tbl (@tables) {
#		# check for existence of table
#		my($tblexists)=0;
#		foreach( db_query($cfg{DESTDB},"select * from sysobjects where name = \'".$tbl."\'") ) {
#			$tblexists=1;
#		}
#		if ($tblexists) {
#			print "Tbl $tbl exists in destination - not creating \n\tpls ensure that it is identical to the source table\n";
#			next;
#		}
#		print "Tbl $tbl does not exist in destination - creating it\n";
#		die "Hmmm Table Definition For Table $tbl Not Found\n"
#			unless defined $revtable{$tbl};
#		# if does not exist split revtable by "go"
#		my(@spl)=split( /\ngo\n/,$revtable{$tbl} );
#		foreach (@spl) {
#			next if /PRIMARY KEY/;
#			print "Executing [ $_ ] \n";
#			foreach( db_query($cfg{DESTDB},$_) ) {
#				die "Error Generated By Query $_\n";
#			}
#		}
#	}
#	print "Completed destination side configuration...\n";
#	db_close_sybase();
}

sub read_config {
	my($file)=@_;
	my(%cfg);
	print "reading configuration file $opt_c\n";
	open(CFG,$file) || die "Cant read file $file\n";
	while( <CFG> ) {
		next if /^#/ or /^\s+/;
		chop;
		s/\s//g;
		my($k,$v)=split /=/;
		$cfg{$k}=$v;
	}
	close(CFG);
	return %cfg;
}

#
# SQL FOR THE DESTINATION
#
my(@destsrv);
my(@d)      ;
my(%destdb);
my( @dbproc_dest );

sub connect_to_dest
{
	print "Connecting to Destination Server ...\n";

	&dbmsghandle ("message_handler_mine");
	&dberrhandle ("error_handler_mine");

	@destsrv 	 = split(/,/,$cfg{DESTSRV});
	my(@d)       = split(/,/,$cfg{DESTDB });
	my(@l)       = split(/,/,$cfg{DESTUSR });
	my(@p)       = split(/,/,$cfg{DESTPASS });

	foreach ( @destsrv ) {
		$destdb{$_} = shift @d;
		my($usr)    = shift @l;
		my($pass)   = shift @p;
		my($ss)=new Sybase::DBlib $usr, $pass, $_;
		push @dbproc_dest, $ss;
		die( "Can't connect to the $_ Sybase server as login $usr.\n")
			unless defined $ss;
		$ss->dbuse($destdb{$_});
	}
	1;
}

sub run_query_on_dest
{
	my($query)=@_;

	my(@srvlist)=@destsrv;
	foreach my $dq (@dbproc_dest) {
		my($s)=shift @srvlist;
		if( defined $opt_d ) {
			print "TARGET SERVER: $s\n";
			print "TARGET DATABASE: $destdb{$s}\n";
			print "TARGET QUERY:\n",$query;
		}
		$dq->dbcmd($query);
		my($rc) = $dq->dbsqlexec();
		die("ERROR: Query Failed ($rc)") unless defined $rc;
		while( $dq->dbresults != NO_MORE_RESULTS  ) {
			while ( my(@dat) = $dq->dbnextrow()) {
				die "Error - Srv $s Query $query Failed\n".join(" ",@dat);
			}
		}
	}
}

__END__

=head1 NAME

replicator.pl - simple sybase replicator

=head2 DESCRIPTION

Simple database replicator. Implemented by a trigger on source tables which
saves data into a separate replication database.  The replicator.pl program
sweeps data from this replication database to the destination server /
database, and should always be running during production hours.  A
configuration file contains the details of the replication, so multiple
replications can occur, run by different processes.
replicator.pl contains the code to define the replication
tables and triggers as well as performing the actual replication.

you probably should read the whole code line before implementing ... its not
complex.

=head2 HOW IT WORKS

The source tables have triggers on them for insert, update, and delete
which save data into a replication database in a table of the same name
with an r_ prepended. As an example, the mydb..mytable table would
have triggers that take changes and put them into the repldb..r_mytable
table from where replicator.pl would sweep them to targetdb..mytable.
The triggers supports user defined triggers on the source table.  When
replicator.pl -d is run, it defines triggers which start with r_, but
does not modify and user generated triggers (which should not start with
r_).  The user is responsible for modifying their own triggers to conform
to the replication spec.  Triggers in the source db with a name starting
with r_ are system managed.  The replication db must be dedicated to
the replication system and will contain tables identical to the source tables
with the addition of a char(1) r_trig_type column, a char(1) r_xfer column,
and a timestamp r_trig_time column (first 3 columns).

The replicator moves data from the replication db to the target db in a safe
manner using transactions.

=head2 USAGE

   ./replicator.pl [-c configfile ] -snl

   -c cfg,cfg     configuration file name (default replicate.cfg)
                  multiple configuration files allow multiple replications
   -D             define the triggers if they are not allready there
   -u             uninstall
   -S             starts the configuration
   -l             active configuration and statistics report
   -d             debug mode

   one of -d,S,or l must be specified

=head2 DEFINE OPTION

The -D option will create the tables and triggers necessary to
run the system.  Triggers are created in the source database and
tables are created in the replication database (using the
information found in the config file).

The -d option presumes that objects starting with r_ are system created
and can be managed internally.  It also presumes that if there are no
triggers on a table, it is safe to create one.

User triggers will need to be changed to add statements that manage the
r_tablename table in the replication database.

	select r_trig_type=' ',r_xfer=' ',r_trig_time=NULL,*
	into   orig_table
	from 	 yyy..orig_table
	where  1=2

You are welcome to put your own triggers in with your own replication
definition so long as you dont start the trigger name with r_.

=head2 SPECIAL COLUMN DEFINITION

The r_trig_type column contains the following values

   "I" - inserted
   "D" - deleted
   "d" - deleted rows in update trigger
   "i" - inserted rows in update trigger

The r_trig_time column is a timestamp (insert null into this column)

The r_xfer column should be 'x' for columns being transfered and ' ' otherwise.

=head2 CONFIG FILE

The configuration file is in replicator.cfg which has the following format:

[
  SRCSRV  = DSQUERY
  SRCUSR  = LOGIN
  SRCPASS = PASSWORD
  SRCDB   = DB
  SRCREPL = DB
  DESTSRV = DSQUERY,DSQUERY,DSQUERY
  DESTDB  = DB,DB,DB
  TABLES  = tbla,tblb,tblc
  POLLTIME= num_seconds
]

ORIG_DB is the original database and is only used by the -d create option.

=head2 TODO

Must create index on tables for optimal tuning (hmmmm how...)

Destination server EXPECTED to be consistent when startup.  This might
be a problem.

Sanity Checker?

=cut
