use Repository;
use Data::Dumper;

# open(IN, "G:/gem/conf/files.dat") or die "Cant open IN";
open(HDR,"G:/gem/conf/unix_logfiles.dat.sample") or die "Cant open HDR";
open(OUT,"> G:/gem/conf/unix_logfiles.dat") or die "Cant open OUT";

while(<HDR>) { chomp; print OUT $_,"\n" unless /^\s*$/; }
my($logdat)=get_logfiles();
my %logfiles =%{$logdat};

my(@sybsvrs) = get_password(-type=>'sybase');
my(%sybsvr);
foreach (@sybsvrs) { $sybsvr{$_}=1; }

my(@unixsvrs) = get_password(-type=>'unix');
my(%unixsvr);
foreach (@unixsvrs) { $unixsvr{$_}=1; }

my(%found);
my(%ALLATTRS);
foreach my $keyname ( sort keys %logfiles ) {
	if( $found{$keyname} ) {
		print "Skipping $_ - allready found\n"; next;
	}
	$found{$keyname}=1;
	my($host,$filename)=split(/:/,$keyname);
	my $ref=$logfiles{$keyname};
	# print "$host - $filename\n";	
	foreach ( keys %$ref ) { 
		$ALLATTRS{$_} = 1; 
		#print "k=$_ "; 
	}
	next if $$ref{File_Type} eq "Run File";
	$$ref{File_Type} =~ s/ File$//i;
	#print Dumper $ref;
	# print "\n";
	
	# figure out the server
	my($nm)=$$ref{Server_Name};
	$nm=~s/TLMSYB/SSDB/;
	my($typ)='DBServer';
	$typ='BackupServer' if $nm=~/BS$/ or $nm=~/BACK$/;
	$typ='MonServer' if $nm=~/MS$/ or $nm=~/MON$/;
	$typ='RepServer' if $nm=~/RS$/ or $nm=~/REP$/;
	my($f)=0;
	if( $sybsvr{$nm} ) {
		$f++;
	} else {
		$nm=~s/\_\w+$//;
		$f++ if $sybsvr{$nm};
	} 
	if( ! $f ) {
		$nm=~s/_MON//;
		$nm=~s/_BACK//;
		$nm=~s/_BS//;
	}
	next if $filename=~/\$/ or $nm=~/\$/;
	#next unless $unixsvr{$host};
	#next if $f;
	#print $f," ",$nm,"\n" unless $$ref{File_Type} eq "Interfaces" or $$ref{File_Type} eq "Environment";
	
	# <hostname>:<filename>:DB_SERVERNAME:DB_SERVERTYPE:LOGFILETYPE:CAN_OVERWRITE:FETCHFILE
	
#	 LOGFILETYPE:SERVERNAME:SERVERTYPE:<hostname>:<filename>:CAN_OVERWRITE:FETCHFILE
#
# CAN_OVERWRITE=YES|NO
# FETCHFILE=YES|NO
# LOGFILETYPE=Errorlog|Interfaces|Environment
# Hostname= name of unix host
# SERVERTYPE=BackupServer|DBServer|MonServer|RepServer
#
	printf OUT "%3s,%-11s,%-12s,%-20s,%-20s,%-s\n",
			$$ref{FETCHFILE},
			$$ref{File_Type},
			$typ,
			$nm,
			$host,
			$filename;
}

# foreach ( sort keys %ALLATTRS ) { print $_,"\n"; }    		
	
#close(IN);
close(OUT);
close(HDR);