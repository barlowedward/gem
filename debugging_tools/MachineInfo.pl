use strict;
use lib 'G:/dev/lib';

use Win32::MachineInfo;
use Repository;

my(@servers)=get_password(-type=>'win32servers');
foreach (@servers) {
    	print "SERVER=$_\n";
        chomp;
        my(%info);
	if (Win32::MachineInfo::GetMachineInfo($_, \%info)) {
		for my $key (sort keys %info) {
			if( ref $info{$key} eq "ARRAY" ) {
				print "\t$key=", join(",",@{$info{$key}}), "\n";
			} else {
				print "\t$key=", $info{$key}, "\n";
			}
		}
	} else {
		print "\tError: $^E\n";
    	}
}
