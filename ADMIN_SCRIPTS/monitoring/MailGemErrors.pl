#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Sys::Hostname;
use Repository;
use Do_Time;
use MlpAlarm;
use CommonFunc;

use vars qw( $BATCH_ID $MAILTO $DEBUG $MAX_NUM_ERR_LINES );

$MAX_NUM_ERR_LINES=100;
# Copyright (c) 2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
	print "MailGemErrors.pl [ -BATCH_ID=id ] [ --MAILTO=a,b,c ] [--MAX_NUM_ERR_LINES=xx]\n";
   return "\n";
}

$| =1;

# die usage("") if $#ARGV<0;

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"				=> \$BATCH_ID ,
		"MAILTO=s" 					=> \$MAILTO,
		"DEBUG"      				=> \$DEBUG,
		"MAX_NUM_ERR_LINES=s" 	=> \$MAX_NUM_ERR_LINES );

my $GemConfig=read_gemconfig( -debug=>$DEBUG, -nodie=>1, -READONLY=>1 );
$MAILTO= $GemConfig->{ADMIN_EMAIL} unless $MAILTO;
$BATCH_ID="MailGemErrors" unless $BATCH_ID;

my($DATADIRECTORY)=get_gem_root_dir();
die "DATA DIRECTORY DOES NOT EXIST\n" unless -d $DATADIRECTORY;

$DATADIRECTORY.="/data";
die "DATA DIRECTORY DOES NOT EXIST\n" unless -d $DATADIRECTORY;

print "MailGemErrors.pl\nRun At ".localtime()."\n\n";
print
"MAIL_HOST = $GemConfig->{MAIL_HOST}
MAIL_FROM = $GemConfig->{ALARM_MAIL}
BATCH_ID  = $BATCH_ID\n
MAIL_TO   = $MAILTO\n";
print "DATADIR   = $DATADIRECTORY\n";

my($msgtxt)='';
my($msghdr)="<H2>MailGemErrors.pl</H2>";

foreach my $directory ("BACKUP_LOGS","GEM_BATCHJOB_LOGS","batchjob_logs","cronlogs","html_output") {
	my($d)="$DATADIRECTORY/$directory";
	next unless -d $d;
	next unless -r $d;

	print "Searching $d\n";
	$msgtxt.="<h2>Directory $d</h2>\n";
	opendir(DIR,$d) || die("Can't open directory $d for reading\n");
	my(@dirlist) = grep(/\.err$/,readdir(DIR));
	closedir(DIR);
	my($numfiles)=0;
	foreach my $errfile ( @dirlist ) {
		my($filesize)= -s "$d/$errfile";
		if( ! $filesize ) {
			print "(skipped) File $d/$errfile<br>\n";
			next;
		}
		$numfiles++;
		print "File $d/$errfile\n";
		$msgtxt.="<h3>Error File $d/$errfile (size=$filesize age=".do_diff_file("$d/$errfile").")</h3>\n";
		if( ! -r "$d/$errfile" ) {
			$msgtxt.="\tCan Not Read $d/$errfile\n";
		} else {
			open(F,"$d/$errfile");
			$msgtxt.="<PRE>\n";
			my($filelineno)=0;
			while( <F> ) {
				chomp;$msgtxt.=$_."\n";
				last if defined $MAX_NUM_ERR_LINES and $filelineno++ > $MAX_NUM_ERR_LINES;
			}
			$msgtxt.="</PRE>\n";
			close(F);
		}
	}
	if( $numfiles==0 ) {
		$msgtxt.="\tNo error Files Found In Directory<br>\n";
	}
}

# print $msgtxt;

my($subject)= $BATCH_ID || 'MailGemErrors.pl Output';

send_mail_message($MAILTO, $GemConfig->{ALARM_MAIL},$GemConfig->{MAIL_HOST},$subject,$msghdr.$msgtxt);

