: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

# The above runs the first perl in your path

# dfstats.pl - run appropriate df and use logger on results
# Runs DF on platform send results to syslog
#  - this should work on any platform with any perl
#

use strict;
use Getopt::Long;
use Sys::Hostname;

my($VERSION)="1.0";
my($ERRTHRESH,$WARNTHRESH,$NOLOGGER,$EXCEPTFILE,$SLEEPTIME,$DEBUG,$SHOWSTDOUT);

# note the lables map lower case stuff returned from first line of df to
# the key within the output
my(%labelmap) = (
	filesystem	=> '',
	kbytes		=> 'Sz',
	'1k-blocks'	=> 'Sz',
	'size'	=> 'Sz',
	used			=> 'Usd',
	avail			=> 'Avl',
	available	=> 'Avl',
	'use%'			=> 'Pct',
	capacity		=> 'Pct',
	mounted		=> 'FS',
	on				=> '',
);

sub usage
{
	print "dfstats.pl --DEBUG --NOLOGGER --EXCEPTFILE=file --SLEEPTIME=secs WARNTHRESH=pct ERRTHRESH=pct --SHOWSTDOUT\n";
   return "\n";
}

$| =1;		# dont buffer standard output

die usage() unless GetOptions(
		"ERRTHRESH=s" 	=> \$ERRTHRESH,
		"WARNTHRESH=s"  => \$WARNTHRESH,
		"NOLOGGER" 	=> \$NOLOGGER ,
		"SHOWSTDOUT" 	=> \$SHOWSTDOUT ,
		"EXCEPTFILE=s" 	=> \$EXCEPTFILE ,
		"SLEEPTIME=s" 	=> \$SLEEPTIME ,
		"DEBUG"     	=> \$DEBUG );

print "dfstats.pl - version $VERSION\n" if defined $DEBUG;

#
# SET UP DEFAULTS
#
if( ! defined $WARNTHRESH ) {
	$WARNTHRESH=90;
	print "Setting Warning Threshold to default (90)\n" if defined $DEBUG
}
if( ! defined $ERRTHRESH ) {
	$ERRTHRESH =98;
	print "Setting Error Threshold to default (98)\n" if defined $DEBUG
}
die "ERRTHRESH < WARNTHRESH\n" if $ERRTHRESH<$WARNTHRESH;

# Get the OS Name
my($OS) = `uname -s`;
chomp $OS;

# READ THE EXCEPTIONS FILE
#  - ignore blanks and # lines
#  - scan for hostname in first column
#  - if host on line alone - ignore the host
#  - if args after that is override thresholds for errors
if ( defined $EXCEPTFILE and -r $EXCEPTFILE ) {
	my($curhost)=hostname();
	open( EXCPT,$EXCEPTFILE ) or die "Cant read $EXCEPTFILE\n";
	while(<EXCPT>) {
		next if /^\s*$/ or /^\s*#/;
		my(@d)=split;
		if( $d[0] eq $curhost ) {
			if ( $#d==0 ) {
				# hostname alone in on a line - ignore the host
				print "Found this host ($curhost) in exceptions file ($EXCEPTFILE) - ignoring\n" if defined $DEBUG;
				exit(0);
			} else {
				print "Multiple Fields In Exceptions File ($EXCEPTFILE) not handled";
				exit(0);
			}
		}
	}
	next if
	close(EXCPT);
}

#
# FIND THE APPROPRIATE DF COMMAND
#
my($dfcmd)="df -kl";
$dfcmd = "df -hlP" if $OS eq "Linux";
if( -x "/bin/df" ) {
	$dfcmd = "/bin/$dfcmd";
} elsif( -x "/usr/bin/df" ) {
	$dfcmd = "/usr/bin/$dfcmd";
}

print "disk space command is $dfcmd\n" if defined $DEBUG;

if( defined $SLEEPTIME ) {						 # test if you are up
	my($timethresh)=60*60;		# 1 hour
	$timethresh=2*$SLEEPTIME 	if $timethresh < 2*$SLEEPTIME;
	if( Am_I_Up(-filename=>"/tmp/dfstats.lock", -threshtime=>$timethresh) ) {
		print "ERROR - PROGRAM RUNNING - remove /tmp/dfstats.lock and rerun\n"
			if defined $DEBUG;
		exit(0);
	}
}

while (1) {
	open(DF,"$dfcmd |") or die "Cant Run $dfcmd";
	my($lineno)=1;
	my($pct_colid);
	my($fs_colid);
	my(%labels);
	while(<DF>){
		chomp;
		chomp;
		my(@dat)=split;
		if( $lineno==1 ) {
			# figure out your columns
			my($column)=0;
			foreach( @dat ) {
				$pct_colid=$column 	if /\%/ or /capacity/;
				$fs_colid=$column 	if /mounted/i;
				$labels{$column}=$labelmap{lc($_)};
				$column++;
			}
			print "Percent Column is number $pct_colid\n" if defined $DEBUG;
			print "File System Column is number $fs_colid\n" if defined $DEBUG;
			die   "Cant Find Percent Column\n" unless defined $pct_colid;
		} else {
			next if 	$dat[$#dat] =~ /^\/cdrom/ or
					$dat[$#dat] eq "mnttab" or
					$dat[$#dat] eq "/proc" or
					$dat[0] =~ /:/ or
					$dat[$#dat] eq "/tempdb" or
					$dat[0] !~ /\//;
			print "df returned $_ \n" if defined $DEBUG;
			$dat[$pct_colid] =~ s/\%//;

			# build the string
			my($str)="FS=".$dat[$fs_colid]." Pct=".$dat[$pct_colid]." (";
			my($column)=0;
			foreach( @dat ) {
				$str.=$labels{$column}."=".$dat[$column]." "
					if $labels{$column} ne ""
						and $column!=$fs_colid
						and $column!=$pct_colid;
				$column++;
			}
			$str=~ s/ $/)/;

			if( $dat[$pct_colid] >= $ERRTHRESH ) {
				log_it( -err=>"err",-msg=>$str);
			} elsif( $dat[$pct_colid] >= $WARNTHRESH ) {
				log_it( -err=>"warning",-msg=>$str);
			} else {
				log_it( -err=>"info",-msg=>$str);
			}
		};
		$lineno++;
	}
	close(DF);
	exit(0) unless defined $SLEEPTIME;
	sleep($SLEEPTIME);
	I_Am_Up(-filename=>"/tmp/dfstats.lock");
}

sub log_it
{
	my(%args)=@_;
	if( defined $DEBUG ) {
		print "State=".$args{-err}.": ".$args{-msg}."\n";
	}
	if( $args{-err} ne "info" and defined $SHOWSTDOUT ) {
		print "State=".uc($args{-err}).": ".$args{-msg}."\n";
	}
	if( ! defined $NOLOGGER ) {
		my($str)="logger -plocal1.$args{-err} -tdfstats.pl \"$args{-msg}\"\n";
		$str="/usr/bin/".$str if -x "/usr/bin/logger";
		print $str if $DEBUG;
		system($str);
	}
}

# Check for Lock File and Dont Restart unless it exists
# Args: -threshtime , -filename
sub Am_I_Up
{
	my(%args)=@_;
	my($filename,$threshtime);

	print "Am_I_Up() called\n" if defined $args{-debug};
	if( defined $args{-filename} ) {
		$filename = $args{-filename};
	} elsif( defined $args{-dir} ) {
		$filename= $args{-dir}."/".basename($0).".lck";
	} else {
		$filename=basename($0).".lck";
	}
	print "Am_I_Up() filename=$filename\n" if defined $args{-debug};

	my($rc);
	if( -e $filename ) {
		print "Am_I_Up() file exists\n" if defined $args{-debug};
		die "Lock File Is UnWritable ($filename)"
			unless -w $filename;

		if( $args{-threshtime} ) {
			$threshtime = $args{-threshtime};
		} else {
			$threshtime=1200;	# twenty minutes
		}

		my($pingsecs)= (-M $filename)*24*60*60;
		if( $pingsecs > $threshtime ) {
			$rc=0;
		} else {
			$rc=1;
			print "Am_I_Up() returning $rc (not touching file)\n" if defined $args{-debug};
			return $rc;
		}
	} else {
		print "Am_I_Up() file does not exists\n" if defined $args{-debug};
		$rc=0;
	}
	I_Am_Up(-filename=>"/tmp/dfstats.lock");
	return $rc;
}

sub I_Am_Up
{
	my(%args)=@_;
	my($filename,$threshtime);

	print "I_Am_Up() called\n" if defined $args{-debug};
	if( defined $args{-filename} ) {
		$filename = $args{-filename};
	} elsif( defined $args{-dir} ) {
		$filename= $args{-dir}."/".basename($0).".lck";
	} else {
		$filename=basename($0).".lck";
	}
	print "I_Am_Up() filename=$filename\n" if defined $args{-debug};

	my($rc);
	if( -e $filename ) {
		print "I_Am_Up() file exists\n" if defined $args{-debug};
		die "Lock File Is UnWritable ($filename)"
			unless -w $filename;

		if( $args{-threshtime} ) {
			$threshtime = $args{-threshtime};
		} else {
			$threshtime=1200;	# twenty minutes
		}

		my($pingsecs)= (-M $filename)*24*60*60;
		if( $pingsecs > $threshtime ) {
			$rc=0;
		} else {
			$rc=1;
		}
	} else {
		print "I_Am_Up() file does not exists\n" if defined $args{-debug};
		$rc=0;
	}

	print "I_Am_Up() creating $filename\n" if defined $args{-debug};
	open(OUT,">$filename") or die "Cant write $filename: $!";
	print OUT "Lock File Written\n";
	close(OUT);

	return $rc;
}

__END__

=head1 NAME

dfstats.pl - utility to check disk space

=head2 SYNOPSIS

Output to syslog via logger on a configurable channel.  Program is designed to w
ork with any perl anywhere.  It can run in a loop if you pass --SLEEPTIME and wi
ll single thread itself so that multiple instances will not run on a single syst
em in loop mode.

=head2 USAGE

        dfstats.pl --DEBUG --EXCEPTFILE=file --SLEEPTIME=secs --WARNTHRESH=pct --ERRTHRESH=pct -NOLOGGER --SHOWSTDOUT

=head2 ARGUMENTS
        --DEBUG 		diagnostic mode
	--EXCEPTFILE=file 	exception file
	--SLEEPTIME=secs 	run in loop - sleeping --SLEEPTIME seconds per loop
	--WARNTHRESH=pct 	warning percentage threshold
	--ERRTHRESH=pct 	error percentage threshold
	--NOLOGGER 		dont output to syslogd
	--SHOWSTDOUT 		show errors and warnings on standard output

=head2 DESCRIPTION

Works on Linux or Solaris (althought this can be easily expanded.  When run with no arguments it will produce no output.  Run with --DEBUG flag to see whats is going on.  This is a feature.  You may also run with --SHOWSTDOUT to see output. Put stuff in EXCEPTFILE to have it ignored.

=head2 OUTPUT

In monitor host will produce logger lines as

 Jul 21 13:18:32 kickstart1 dfstats.pl: FS=/ Pct=30 (Sz=985M Usd=273M Avl=662M)
 Jul 21 13:18:33 kickstart1 dfstats.pl: FS=/boot Pct=10 (Sz=193M Usd=17M Avl=167
M)
 Jul 21 13:18:33 kickstart1 dfstats.pl: FS=/export/home Pct=81 (Sz=6.5G Usd=4.9G
 Avl=1.2G)
 Jul 21 13:18:33 kickstart1 dfstats.pl: FS=/usr Pct=79 (Sz=5.3G Usd=4.0G Avl=1.1
G)
 Jul 21 13:18:33 kickstart1 dfstats.pl: FS=/var Pct=28 (Sz=2.0G Usd=508M Avl=1.4
G)

or

 Jul 21 13:20:03 kdb2 dfstats.pl: [ID 702911 local1.info] FS=/var Pct=26 (Sz=963543 Usd=229538 Avl=676193)
 Jul 21 13:20:03 kdb2 dfstats.pl: [ID 702911 local1.info] FS=/export/home Pct=8 ( Sz=30113974 Usd=2326863 Avl=27485972)


