#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
# use Do_Time;
use DBIFunc;
use CommonFunc;
use Sys::Hostname;
use Repository;
use File::Basename;
use MlpAlarm;

use vars qw( $USER $SERVER $RUNONALLSERVERS $PASSWORD $DEBUG $DOALL $PRODUCTION $NOPRODUCTION
	$ASNEEDEDONLY $REORG_ONLINE $MAXTIMEMINS $CONFIG_FILE $DB $HTML $MINDB $MINSVR
	$REORG_REBUILD $NOEXEC $OUTFILE $TIMEINSECS $NOTIMEOUT);

my($VERSION)=1.1;

sub usage
{
print "reorg.pl Version $VERSION\n\n";
print @_;
print "Reorganize/Rebuild Indexes on Sybase and Sql Server\n\n";
print basename($0)." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
or \n".
basename($0)." -DOALL=sybase
or \n".
basename($0)." -DOALL=sqlsvr

Additional Arguments
	--OUTFILE=file    [Output File]
	--ASNEEDEDONLY    [ only run if fragmented ]
	--MAXTIMEMINS		[ max time it can run - do not trust ]
	--HTML            [ output in html ]
	--PRODUCTION|NOPRODUCTION [ Further Restrict Servers set via --DOALL ]
	--DEBUG
	--REORG_ONLINE|REORG_REBUILD [ Default REORG_ONLINE ]
	--CONFIG_FILE     [ File tracking where you last left off ]
	      use -CONFIG_FILE=DEFAULT for default ( .../data/lockfiles/ )
	--NOEXEC\n";
return "\n";
}

$| =1; 				# unbuffered standard output

# change directory to the directory that contains THIS file
my($curdir)=cd_home();

die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"USER=s"				=> \$USER ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DATABASE=s" 		=> \$DB ,
		"DOALL=s" 			=> \$DOALL,
		"RUNONALLSERVERS"	=> \$RUNONALLSERVERS ,		# backwards compatibility
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"OUTFILE=s"			=> \$OUTFILE,
		"HTML"		 		=> \$HTML ,

		"REORG_COMPACT" 	=> \$REORG_ONLINE ,			# backwards compatibility
		"REORG_ONLINE" 	=> \$REORG_ONLINE ,
		"REORG_REBUILD" 	=> \$REORG_REBUILD ,
		"ASNEEDEDONLY" 	=> \$ASNEEDEDONLY ,

		"TIMEINSECS=i" 	=> \$TIMEINSECS ,
		"NOTIMEOUT=i" 	=> \$NOTIMEOUT ,
		"MAXTIMEMINS=i" 	=> \$MAXTIMEMINS ,
		"CONFIG_FILE=s"	=> \$CONFIG_FILE,
		"CONFIGFILE=s"		=> \$CONFIG_FILE,
		"MINDB=s"		=> \$MINDB,
		"MINSVR=s"		=> \$MINSVR,
		"NOEXEC" 			=> \$NOEXEC ,
		"DEBUG"      		=> \$DEBUG );

die usage("Must pass server\n" )		unless defined $SERVER or $RUNONALLSERVERS or $DOALL;

$DOALL='sybase' if $RUNONALLSERVERS and ! $DOALL;	# backwards compatibility

my($STARTTIME)=time;
$REORG_ONLINE=1 unless $REORG_ONLINE or $REORG_REBUILD;

$SIG{ALRM} = sub { 	die "CommandTimedout"; 	};

#
# THE WHOLE PURPOSE OF THIS BLOCK IS TO PERMIT AUTO TIMEOUTS
#
my($timeoutsecs) = $MAXTIMEMINS*60;
# $timeoutsecs=20 unless $MAXTIMEMINS;

if( $NOTIMEOUT or $timeoutsecs==0) {
	reorg_main();
} else {
	print "ALARMING ON $timeoutsecs\n";
	#print "DIAG: ALARM=$SIG{ALRM}\n";
	eval {
		alarm($timeoutsecs);
		reorg_main();
		alarm(0);
	};

	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			die "Reorg Command Timed Out at ",time," ",time-$STARTTIME,"\n";
		} else {
			print "Command Returned $@\n";
		}
	}
}

exit(0);

my( %DBTYPE,%DBVERSION,%DBPATCH );

sub reorg_main {

open(OUTF,">$OUTFILE") if $OUTFILE;
sub myprint {
	print @_;
	print OUTF @_ if $OUTFILE;
}

#print "DIAG: ALARM=$SIG{ALRM}\n";
my($NL)="\n";
$NL="<br>" if $HTML;
myprint( "Reorg Rebuild v",$VERSION,$NL );
myprint( "Run at ".localtime(time)." on host ".hostname().$NL );
$DB="%" unless $DB;

my($min_db, $min_svr)=($MINDB, $MINSVR);

# GET CONFIG_FILE
if( $CONFIG_FILE ) {
	if( $CONFIG_FILE eq "DEFAULT" ) {
		if( $DOALL ) {
			$CONFIG_FILE=get_gem_root_dir()."/data/lockfiles/".hostname().$DOALL.".reorg.dat" if $CONFIG_FILE eq "DEFAULT";
		} else {
			$CONFIG_FILE=get_gem_root_dir()."/data/lockfiles/".hostname().$SERVER.".reorg.dat" if $CONFIG_FILE eq "DEFAULT";
		}
	}
	my($b)=dirname($CONFIG_FILE);
	if( $b eq "." ) {
		$CONFIG_FILE= get_gem_root_dir()."/data/lockfiles/".$CONFIG_FILE;
	} else {
		print "Config Directory is $b\n";
		if( ! -w $b and ! -r $CONFIG_FILE ) {
			die "CONFIG FILE DIRECTORY $b does Not Exist\n" if $b and ! -d $b;
		}
	}
	if( -r $CONFIG_FILE ) {
		print "READING CONFIG FILE - $CONFIG_FILE\n";
		open(CFG,$CONFIG_FILE) or die "Cant open $CONFIG_FILE $!\n";
		while(<CFG>) {
			debug("Read $_\n");
			chop;
			my($server,$database,$doall,$runonall,$production,$noproduction,$svr,$db)	= split(/::/);

			if(( $server eq "" or $server eq $SERVER )
			and ( $database eq "" or $database eq $DB )
			and ( $doall eq "" or $doall eq $DOALL )
			and ( $runonall eq "" or $runonall eq $RUNONALLSERVERS )
			and ( $production eq "" or $production eq $PRODUCTION )
			and ( $noproduction eq "" or $noproduction eq $NOPRODUCTION )) {
				# row matches!
				print "FOUND MATCHING ROW\n";
				$min_svr=$svr;
				$min_db=$db;
			#	$min_table=$table;
				last;
			}
		}
		close($CONFIG_FILE);
	} else {
		print "NEW CONFIG FILE $CONFIG_FILE WILL BE WRITTEN LATER\n";
		open(CFG2,">".$CONFIG_FILE) or die "Cant write $CONFIG_FILE\n";
		close(CFG2);
	}
}

if( $SERVER ) {
	myprint( "[$SERVER] Server Specified On Command Line\n" );
	if( ! $USER or ! $PASSWORD ) {
		#($USER,$PASSWORD)=get_password(-type=>"sybase",-name=>$SERVER);
		if( defined $DOALL ) {
	      ($USER,$PASSWORD)=get_password(-type=>$DOALL, -name=>$SERVER);
	   } else {
	      ($USER,$PASSWORD)=get_password(-name=>$SERVER, -type=>'sybase')
	      	if ! defined $USER or ! defined $PASSWORD;
	      ($USER,$PASSWORD)=get_password(-name=>$SERVER, -type=>'sqlsvr')
	      	if ! defined $USER or ! defined $PASSWORD;
	   }
	}
	#print "DIAG: ALARM=$SIG{ALRM}\n";
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD);
	#print "DIAG: ALARM=$SIG{ALRM}\n";
	# dbi_set_debug(1) if $DEBUG;
	if( ! $rc ) {
		myprint( "Cant connect to $SERVER as $USER\n" );
	} else {
		myprint( "[$SERVER] Connected as $USER\n" );
		my($typ,@db)=dbi_parse_opt_D($DB,1,1);
		myprint("[$SERVER] Checking Database Version...\n");
		($DBTYPE{$SERVER},$DBVERSION{$SERVER},$DBPATCH{$SERVER})=dbi_db_version();
		myprint("[$SERVER] DBTYPE=$DBTYPE{$SERVER} VERSION=$DBVERSION{$SERVER} PATCHLEVEL=$DBPATCH{$SERVER}\n");
		dbi_disconnect();

		# rotate the array!
		my(@newdblist)=@db;
		if($min_db) {
			myprint( "ROTATING ARRAY ($min_db first) =>",join(" ",@db),"\n" );
			foreach (@db) {
				last if $min_db eq $_;
				push @newdblist, shift @newdblist;
			}
			myprint( "DB=>",join(" ",@newdblist),"\n" );
		}

		myprint("[$SERVER] No Databases Found\n") if $#newdblist<0;
		foreach my $DATABASE (@newdblist) {
			MlpLogOperationStart('reorg',$SERVER,$DATABASE,'');
			my($count)=run_a_server_db($SERVER,$USER,$PASSWORD,$DATABASE);
			MlpLogOperationEnd('reorg',$SERVER,$DATABASE,'',undef,"Num Tables=".$count);
		}
	}
} elsif( $RUNONALLSERVERS or $DOALL ) {
	debug("Running on all servers\n");
	my(@serverlist);

	die( usage("DOALL must be sqlsvr or sybase\n" )) if $DOALL ne "sqlsvr" and $DOALL ne "sybase";
 	if( $PRODUCTION ) {
 		push @serverlist, get_password(-type=>$DOALL,PRODUCTION=>1);
 	} elsif( $NOPRODUCTION ) {
 		push @serverlist, get_password(-type=>$DOALL,NOPRODUCTION=>1);
 	} else {
		push @serverlist, get_password(-type=>$DOALL);
	}

	# rotate the array!
	if( $min_svr ) {
		myprint( "ROTATING SERVERS ($min_svr first) =>",join(" ",@serverlist),"\n" );
		my(@newlist)=@serverlist;
		my($found)=0;
		foreach (@serverlist) {
			last if $min_svr eq $_;
			push @newlist, shift @newlist;
		}
		myprint( "SVR=>",join(" ",@newlist),"\n" );
		@serverlist=@newlist;
	}

	foreach my $server (@serverlist) {
		myprint( "[$server] Starting Server Processing At ".localtime(time)."\n" );
	   if( defined $DOALL ) {
	      ($USER,$PASSWORD)=get_password(-type=>$DOALL, -name=>$server);
	   } else {
	      ($USER,$PASSWORD)=get_password(-name=>$server, -type=>'sybase')
	      	if ! defined $USER or ! defined $PASSWORD;
	      ($USER,$PASSWORD)=get_password(-name=>$server, -type=>'sqlsvr')
	      	if ! defined $USER or ! defined $PASSWORD;
	   }

	   my($rc)=dbi_connect(-srv=>$server,-login=>$USER,-password=>$PASSWORD);
		if( ! $rc ) {
			print "Cant connect to $server as $USER\n";
			next;
		}

		my($typ,@db)=dbi_parse_opt_D($DB,1,1);
		debug("[$server] Checking Database Version...\n");
		($DBTYPE{$server},$DBVERSION{$server},$DBPATCH{$server})=dbi_db_version();

		myprint("[$server] DBTYPE=$DBTYPE{$server} VERSION=$DBVERSION{$server} PATCHLEVEL=$DBPATCH{$server}\n");
		dbi_disconnect();
		myprint("[$server] No Databases Found\n") if $#db<0;

		# rotate the array!
		my(@newdblist)=@db;
		if($min_db and $min_svr eq "$server" ) {
			myprint( "ROTATING ARRAY ($min_db first) =>",join(" ",@db),"\n" );
			foreach (@db) {
				last if $min_db eq $_;
				push @newdblist, shift @newdblist;
			}
			myprint( "DB=>",join(" ",@newdblist),"\n" );
		}

		foreach my $DATABASE (@newdblist) {
			MlpLogOperationStart('reorg',$server,$DATABASE,'');
			my($count)=run_a_server_db($server,$USER,$PASSWORD,$DATABASE);
			MlpLogOperationEnd('reorg',$server,$DATABASE,'',undef,"Num Tables=".$count);
		}
		myprint( "\n[$server] Completed Server Processing \n\n" );

	}
} else {
	die "Must pass --SERVER or --DOALL\n";
}
myprint( "Program completed successfully at ".localtime(time).".\n" );
close(OUTF) if $OUTFILE;
}

sub has_timed_out {
	return 0 unless $MAXTIMEMINS;
	if( time > $STARTTIME + $MAXTIMEMINS*60 ) {
		print "TIMEOUT DETECTED\n";
		return 1;
	}
	return 0;
}


sub run_a_server_db {
	my($SERVER,$USER,$PASSWORD,$DATABASE)=@_;
	my($count)=0;

	#print "DIAG: ALARM=$SIG{ALRM}\n";
	return if has_timed_out();

#	myprint("[$SERVER] Working on DB=$DATABASE\n");
	myprint( "<h2>"  ) if $HTML;
	myprint( "\n[$SERVER] Processing Database $DATABASE at ".localtime(time)."\n" );
	myprint( "</h2>" ) if $HTML;

	#log_restart($SERVER,$DATABASE);

	#
	# CONNECT TO THE DATABASE
	#
	debug( "Connecting to $SERVER\n" );
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD);
	if( ! $rc ) {
		myprint ("Cant connect to $SERVER as $USER\n");
		return;
	}
	debug("Connected\n");
	#print "DIAG: ALARM=$SIG{ALRM}\n";

	my(%oktbls);
	if( $DBTYPE{$SERVER} eq "SYBASE" ) {

		# 16384=allpages 32768=datarows
		my($sql)="select user_name(uid),o.name, forwrowcnt, delrowcnt, rowcnt, rslastoam, rslastpage, frlastoam, frlastpage
		from sysobjects o, systabstats ts
		where (sysstat2&16384 !=0 or sysstat2&32768 !=0) and type='U' and ts.id=o.id";


		debug("[$DATABASE] $sql\n");
		return if has_timed_out();
		foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
			my($usr,$tbl,@vals)= dbi_decode_row($_);
			if( $ASNEEDEDONLY ) {
				# forwarded rows + deleted > 10% of the total rows... its time
				next unless $vals[0] + $vals[1] > $vals[2]/10;
			}
			$oktbls{$usr.".".$tbl}=1;
			$count++;
		}
		debug("Read $count Tables\n");

		foreach my $table ( keys %oktbls ) {
			if( $REORG_REBUILD ) {
				my $sql = "reorg rebuild $table";
				log_reorg_start($SERVER,$DATABASE,$table,$sql);
				if( defined $NOEXEC ) {
					myprint( "(noexec) $sql\n" );
				} else {
					debug( "query  is $sql\n" );
					myprint( "-- ".localtime(time)." ".$sql."\n" );
					return if has_timed_out();
					foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
						my $str = join("",dbi_decode_row($_));
						myprint( "Query Returned : ",$str,"\n" ) if $str;
					}
				}
				log_reorg_end($SERVER,$DATABASE,$table,$sql);

			} elsif( $REORG_ONLINE ) {
				my $sql = "reorg compact $table";

				if( $MAXTIMEMINS ) {
					return if has_timed_out();
					my($minremaining)= $MAXTIMEMINS - int((time-$STARTTIME)/60);
					last if $minremaining<=0;
					$sql .= " with resume,time=$minremaining";
				}

				log_reorg_start($SERVER,$DATABASE,$table,$sql);
				if( defined $NOEXEC ) {
					myprint( "(noexec) $sql\n" );
				} else {
					debug( "query  is $sql\n" );
					myprint( "-- ".localtime(time)." ".$sql."\n" );
					return if has_timed_out();
					foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
						my $str = join("",dbi_decode_row($_));
						myprint( "Query Returned : ",$str,"\n" ) if $str;
					}
				}
				log_reorg_end($SERVER,$DATABASE,$table,$sql);
			}
		}
	} elsif( $DBPATCH{$SERVER} >= 9 ) {		#sql server 2005
		my($sql)="select db_id(\'$DATABASE\')";
		debug( $sql,"\n" );
		my($Dbid)=0;
		return if has_timed_out();
		foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
			($Dbid) = dbi_decode_row($_);
		}
		die "NO DBID FOR $DATABASE" unless $Dbid;
		debug("[$SERVER] Dbid=$Dbid\n");
		if( $ASNEEDEDONLY ) {
			myprint("[$SERVER] Fetching Index Statistics\n");
			$sql="select s.name,o.name,i.name,
			p.object_id,p.index_id,partition_number,avg_fragmentation_in_percent
from sys.dm_db_index_physical_stats($Dbid,null,null,null,'LIMITED') p
join sys.objects as o with (NOLOCK)  on p.object_id=o.object_id
join sys.schemas as s with (NOLOCK)  on s.schema_id=o.schema_id
join sys.indexes i with (NOLOCK)     on i.object_id = p.object_id and i.index_id=p.index_id
where p.index_id>0 and allow_row_locks = 1		-- no table lvl
and avg_fragmentation_in_percent>";
			$sql.=30;

		} else {
			myprint("[$SERVER] Fetching Indexes\n");
			$sql="select usrname=user_name(uid),objname=o.name,indname=i.name,
			i.id, i.indid from sysindexes i, sysobjects o
where o.id=i.id
and   type!='S'
and   indid!=255
and   indid!=0
and   i.id>100
and   lockflags != 2		-- no table lvl locks
and   rowcnt>1000";
			# $sql.=15;
		}

		$count=0;
		debug( $sql,"\n" );
		return if has_timed_out();

		my(@Todo);
		my(%donekeysbytodo);

		# die "* * * DIAG: $sql\n";

		foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
			my($usr,$tbl,$idx,@vals)= dbi_decode_row($_);
			print "$count) u=$usr t=$tbl i=$idx ",join(" ",@vals),"\n" if $ASNEEDEDONLY;
			warn "====BAD QUERY in $DATABASE ====\n$sql\n==== Returned $_\n" unless $tbl;
			warn "NULL TABLE " unless $tbl;
			next unless $tbl;
			push @Todo, $usr."~~".$tbl."~~".$idx;
			# $oktbls{$usr."~~".$tbl."~~".$idx}=1;
			$donekeysbytodo{$usr."~~".$tbl."~~".$idx}=$vals[0]."~~".$vals[1];
			$count++;
		}

		myprint("[$SERVER] Processing Reorgs ($count tables) $#Todo\n");
		my($count2)=0;
		# foreach ( keys %oktbls ) {
		foreach ( @Todo ) {

			print "$count2 DIAG: processing $_\n" if $DEBUG;
			my($usr,$tbl,$idx)=split(/~~/);
			my $sql = "ALTER INDEX $idx ON $usr.$tbl reorganize";
			print "* * * DIAG - PROCESSING user=$usr table=$tbl index=$idx\n" if $DEBUG;
			if( $MAXTIMEMINS ) {
				my($minremaining)= $MAXTIMEMINS - int((time-$STARTTIME)/60);
				last if $minremaining<=0;
			}
			log_reorg_start($SERVER,$DATABASE,$usr.".".$tbl.".".$idx,$sql);
			#log_reorg_start($SERVER,$DATABASE,$donekeysbytodo{$usr."~~".$tbl."~~".$idx},$sql);
			if( defined $NOEXEC ) {
				myprint( "$count2) (noexec) $sql\n" );
			} else {
				debug( "query  is $sql\n" );
				myprint( "-- ".localtime(time)." $count2) ".$sql."\n" );
				return if has_timed_out();
				foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
					my $str = join("",dbi_decode_row($_));
					myprint( "Query Returned : ",$str,"\n" ) if $str;
				}
			}
			log_reorg_end($SERVER,$DATABASE,$tbl.".".$idx,$sql);
			# log_reorg_end($SERVER,$DATABASE,$donekeysbytodo{$usr."~~".$tbl."~~".$idx},$sql);
			$count2++;
			print "DIAG: done processing $_\n" if $DEBUG;
		}
		# ALTER INDEX [PK__FUEGO_BINPROPS__604834B3] ON [dbo].[FUEGO_BINPROPS] REORGANIZE WITH ( LOB_COMPACTION = ON )
	} else {		# sql server 2000 or before
		my($Scanned,$moved,$removed)=(0,0,0);
		my($sql)="select user_name(uid),o.name,indid
from sysobjects o, sysindexes i
where indid!=255 and indid!=0 and i.id>100
and o.id=i.id and rowcnt>1000";			# 1000 rows minimum

		return if has_timed_out();
		foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
			my($usr,$tbl,$idx)= dbi_decode_row($_);
			next unless $idx and $tbl and $usr;
			$oktbls{$usr."~~".$tbl."~~".$idx}=1;
		}

		foreach ( keys %oktbls ) {
			my($usr,$tbl,$idx)=split(/~~/);
			#print "DIAG: ALARM=$SIG{ALRM}\n" if $DEBUG;
			my $sql = "dbcc indexdefrag($DATABASE,[$usr.$tbl],$idx)";
			if( $MAXTIMEMINS ) {
				my($minremaining)= $MAXTIMEMINS - int((time-$STARTTIME)/60);
				last if $minremaining<=0;
			}

			log_reorg_start($SERVER,$DATABASE,$tbl.".".$idx,$sql);
			if( defined $NOEXEC ) {
				myprint( "(noexec) $sql\n" );
			} else {
				debug( "query  is $sql\n" );
				myprint( "-- ".localtime(time)." ".$sql."\n" );
				return if has_timed_out();
				foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
					my @dat=dbi_decode_row($_);
					if( $#dat==2 ) {
						myprint( "Scanned $dat[0] Moved=$dat[1] Removed=$dat[2]\n" );
						$Scanned	+= $dat[0];
						$moved	+= $dat[1];
						$removed	+= $dat[2];
					} else {
						my $str = join("",@dat);
						myprint( "Query Returned : ",$str,"\n" ) if $str;
					}
				}
			}
			log_reorg_end($SERVER,$DATABASE,$tbl.".".$idx,$sql);
		}
		myprint( "Scanned $Scanned Moved=$moved Removed=$removed\n" );
	}
	debug( "server / database completed successfully.\n" );
	log_restart($SERVER,$DATABASE);
	return $count;
}

sub debug { myprint( @_ ) if defined $DEBUG; }

my(%DURATION);
sub log_reorg_start {
	my($server,$db,$table,$sql)=@_;
	$DURATION{join("|",@_)}= -1 * time;
	MlpLogOperationStart("reorg",$server,$db,$table);
	print "START $server.$db key=$table\n";
}

sub log_reorg_end {
	my($server,$db,$table,$sql)=@_;
	$DURATION{join("|",@_)} = time + $DURATION{join("|",@_)};
	print "END   $server.$db key=$table duration=",$DURATION{join("|",@_)},"\n";
	MlpLogOperationEnd("reorg",$server,$db,$table);
}

sub log_restart
{
	my($s,$d)=@_;

	return unless $CONFIG_FILE;
	debug( "REWRITING CONFIG FILE - $CONFIG_FILE\n" );
	open(CFG,$CONFIG_FILE) or die "Cant open $CONFIG_FILE $!\n";
	my(@output);
	my($found)=0;

	my($line)=$SERVER."::".$DB."::".$DOALL."::".$RUNONALLSERVERS."::".$PRODUCTION."::".$NOPRODUCTION."::".$s."::".$d."\n";
	debug( "NEW LINE $line" );
	while(<CFG>) {
		chop;	chomp;
		my($server,$database,$doall,$runonall,$production,$noproduction,$svr,$db,$table)	= split(/::/);

		if((  $server eq "" or $server eq $SERVER )
		and ( $database eq "" or $database eq $DB )
		and ( $doall eq "" or $doall eq $DOALL )
		and ( $runonall eq "" or $runonall eq $RUNONALLSERVERS )
		and ( $production eq "" or $production eq $PRODUCTION )
		and ( $noproduction eq "" or $noproduction eq $NOPRODUCTION )) {
			# row matches!
			$found=1;
			push @output,$line;
		} else {
			push @output,$_."\n";
		}
	}
	push @output,$line if $found==0;
	close(CFG);
	open(CFG2,">".$CONFIG_FILE) or die "Cant Write $CONFIG_FILE $!\n";
	print CFG2 @output;
	close(CFG2);
}


__END__

=head1 NAME

reorg.pl - database reorg manager

=head2 USAGE

reorg.pl Version 1.2

Reorganize/Rebuild Indexes on Sybase and Sql Server

 reorg.pl -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
 -or-
 reorg.pl -DOALL=sybase
 -or-
 reorg.pl -DOALL=sqlsvr

 Additional Arguments
   --OUTFILE=file    [Output File]
   --ASNEEDEDONLY    [ only run if fragmented ]
   --MAXTIMEMINS           [ max time it can run - do not trust ]
   --HTML            [ output in html ]
   --PRODUCTION|NOPRODUCTION [ Further Restrict Servers set via --DOALL ]
   --DEBUG
   --REORG_ONLINE|REORG_REBUILD [ Default REORG_ONLINE ]
   --CONFIG_FILE     [ File tracking where you last left off ]
   --NOEXEC

=head2 CONFIG_FILE

Uses Default GEM Config file via. --CONFIG_FILE=DEFAULT

 if --CONFIG_FILE=DEFAULT, the configuration file will be either
		get_gem_root_dir()."/data/lockfiles/".hostname().$DOALL.".reorg.dat"
		get_gem_root_dir()."/data/lockfiles/".hostname().$SERVER.".reorg.dat"
 if --CONFIG_FILE is set, the configuration file will be
		get_gem_root_dir()."/data/lockfiles/".$CONFIG_FILE;

The config file format is :: separated fields in the format:

server::database::doall::runonall::production::noproduction::svr::db

And will be updated each run

=head2 ADDITIONAL COMMAND LINE ARGS

You can run on all your servers with --RUNONALLSERVERS or you can specify a server with --SERVER/--USER/--PASSWORD.  If no --DATABASE is specified, it will run on all user databases.

This program has been tested on sql 2000, sql 2005, and sybase

On Sybase, the command run will be reorg REORG_ONLINE (--REORG_ONLINE) or reorg REORG_REBUILD (--REORG_REBUILD).

If you specify --ASNEEDEDONLY, it will only reorg tables that need to be reorged (highly fragmented)

You can also pass in --NOEXEC to print what will happen but not run the commands

Output can be directed to --OUTFILE and can be saved in HTML format if --HTML is passed.

The MAXTIMEMINS argument will specify the max time in minutes that the entire operation
can run (all servers all databases).  You can specify --CONFIG_FILE to store the order
of this run for the future - in which case the next run will pick up where the last one left
off.

=head2 TO DO

Must add Table to the config file... not sure why i removed it but needed for > 50gb databases. may
want cmd line option to control "tbl" behavior... -or- may do it based on size (30GB limit).
in vldb case may need to open/close the config file a bunch... or better... every 10 minutes...

The MAXTIMEMINS option is set up to check BEFORE & AFTER tables are reorged.  It is also set to check
using an alarm().  But the code at least on windows appears NOT to alert well, leading to the case where
when a huge table is being reorged, that reorg can runs well past the max time specified.

No destructive rebuilds on sql server

Run ALTER INDEX command with SET (ALLOW_PAGE_LOCK = ON)

May need from sysindexes where allow_page_locks=0

=cut


