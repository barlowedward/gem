
IF OBJECT_ID('tempdb..#am_dbfileio', 'U') IS NULL
BEGIN
    CREATE TABLE #am_dbfileio (collection_time datetime PRIMARY KEY, total_io_bytes numeric (28, 1));
END;

IF OBJECT_ID ('tempdb..#am_dbfilestats', 'U') IS NULL
BEGIN
    CREATE TABLE #am_dbfilestats (
        [collection_time] datetime,
        [Database] sysname,
        [File] nvarchar(1024),
        [Total MB Read] numeric (28,1),
        [Total MB Written] numeric (28,1),
        [Total I/O Count] bigint,
        [Total I/O Wait Time (ms)] bigint,
        [Size (MB)] bigint
    );
    CREATE CLUSTERED INDEX cidx ON #am_dbfilestats ([collection_time]);
END;
