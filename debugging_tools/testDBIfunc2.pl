#!/usr/local/bin/perl-5.6.1

use lib qw(/apps/sybmon/dist/lib /apps/sybmon/dist/ADMIN_SCRIPTS/lib /apps/sybmon/dist/plugins /apps/sybmon/dist/cpan);

use strict;
use DBI;
use MlpAlarm;

my($DEBUG);	# set to dbi trace level

my($server) 	= $MLPAlarm::server || "DEVNULL";
my($login) 	= $MLPAlarm::login || "gemalarms";
my($password) 	= $MLPAlarm::password || "gemalarms";
my($db) 	= $MLPAlarm::db || "gemalarms";
my($server_type)="Sybase";

print "Connecting to Alarm Server\n";

my($dbproc) = DBI->connect("dbi:$server_type:$server",$login,$password,{RaiseError=>0,PrintError=>0,AutoCommit=>1});
die "Cant Connect to Server $server as Login $login password $password db $db" unless defined $dbproc;
print "Connection Ok.\n";

$dbproc->{syb_err_handler}  = \&sybase_handler;
$dbproc->do("use $db");
DBI->trace($DEBUG) if defined $DEBUG;

my($query)="sp_who";

my($sth)=$dbproc->prepare($query);
if( ! defined $sth or defined $sth->err ) {
	die "ERROR: Prepare Failed";
}
my($rv)=$sth->execute();
if(! $rv or defined $sth->err) {
	die "ERROR: Execute Failed";
}

my($c)=1;
do {
	print "ResultType=",$sth->{syb_result_type},"\n";
	while( my($d)=$sth->fetch ) {
		next  if $sth->{syb_result_type} == 4043;
		next  if $sth->{syb_result_type} == 4043;
		#print "QUERY RETURNED ($d) \n";
		print join("\t",@$d),"\n";
		$c++;
		die "DONE - ONLY 5 ROWS PRINTED" if $c>5;
	} 
} while ( $sth->{syb_more_results} );

#do {
#		while ( my $aref = $sth->fetchrow_arrayref() ) {
#			last if ! defined $aref or $aref==0 or $#$aref<0 ;
#			my(@dat)=@$aref;
#			print "WARNING QUERY RETURNED: ",join(" ",@dat),"\n";
#		}
#} while( $dbproc->{"syb_more_results"} );

sub sybase_handler {
	my(@dat)=@_;
	print "ERROR:",join(" ",@dat),"\n";
}
