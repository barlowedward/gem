# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.
package servers;

@ISA = qw(Plugin);
use strict;
use Repository;
use File::Basename;
use Tk;
use CommonFunc;
use DBIFunc;
use Do_Time;

use vars qw($VERSION);
$VERSION = ".01";

my($std_page_name)="properties";
my($version,$sht_version);
my($connectionmanager);
my(@servrs,%server_typ);
my(%servertypes);
my($current_screen,$current_tab,%has_been_painted,%tabinfo);

my( %type_labels ) = (
	sybase			=> "Sybase ASE",
	unix				=> "Unix System",
	sqlsvr			=> "Sql Server",
	win32servers 	=> "NT Server",
	oracle  			=> "Oracle"
);

my( %toi ) = (
	"Sybase ASE"	=> "sybase",
	"Unix System"	=> "unix",
	"Sql Server"	=> "sqlsvr",
	"NT Server"	=> "win32servers",
	"Oracle"	=> "oracle"
);

my(@menuitems);

# right click options for sybase & sql server - at the server level
my(%sqlsyb_svr_options) = (
		"Reports"	=> 'Summary',
		"      -  Diagnose Server"		=> 'Audit',
		"      -  Configuration Options"	=> 'Configuration',
		"      -  Database List"		=> 'Database',
		"      -  Storage Report"		=> 'Storage',
		"      -  Space Usage"			=> 'Space',
		"      -  Login Report"			=> 'Logins',
	);

my(%sqlsyb_db_options) = (
		"DB Reports"				=> 'Summary',
		"      -  Diagnose Database"		=> 'Audit',
		"      -  Database Options"		=> 'Summary',
		"      -  User Report"			=> 'Users',
		"      -  Table Space"			=> 'Tables',
		"      -  Index Report"			=> 'Index',
		"      -  Database Audit"		=> 'Storage',
		"      -  Dependency Tree"		=> 'Depends',

	);



# 1 Button For Each Type Of Server
sub getExplorerButtons {
	my($package)=shift;
	$package->init();
	my(%labels);
	foreach ( keys %servertypes ) {
		$labels{$type_labels{$_}} = $_ if defined $type_labels{$_};
	}
	return( %labels );
}

sub init {
	my($package)=shift;
	if( ! defined $connectionmanager ) {
		my(%c)=$package->get_CONFIG();
		$connectionmanager=  $c{-connectionmanager};
	}

	foreach my $ty ( get_passtype() ) {
		next if $ty eq "documenter";
		$servertypes{$ty} = 1;
	}

	foreach ( keys %servertypes ) {
		push @menuitems,"Configuration/Register/".$type_labels{$_};
	}
	push @menuitems, "Sql Server/Stored Procedures";
	push @menuitems, "Sybase/Stored Procedures";
	push @menuitems, "Configuration/Sybase Stored Procedures";
	push @menuitems, "Configuration/Sql Server Stored Procedures";
}

sub getMenuItems {
	my($package)=shift;
	$package->init() if $#menuitems<0;
	return (@menuitems);
}

sub getPluginPanes { return $std_page_name; }

sub getTreeRtclk {
	my($package,$curTreeType,$treeitmptr)=@_;
	$package->debugmsg(  "[servers] getTreeRtclk(curtype=$curTreeType)\n" );
	return( undef ) unless defined $type_labels{$curTreeType};
	my(@rightclicks);

	foreach my $itm ( @$treeitmptr ) {
		my($dummy,$typ,$server,$db)=split(/\//,$itm);

		if ( $itm eq "/" or ! defined $server ) {
			my(@main_rclk)=(
				"Registration Summary",
				"Stored Procedure Library",
				"Files And Directories",
				"Survey $type_labels{$curTreeType}",
				"Cross Server Reports",
				"Register New $type_labels{$curTreeType} Server");

			push @rightclicks, \@main_rclk;
		} elsif( defined $db ) {
			my(@sv_rclk);
			if( $toi{$typ} eq "sqlsvr" or $toi{$typ} eq "sybase" ) {
				push @sv_rclk, (
                                	"Connection",
                                	"      - Disconnect from $server",
                                	"      - [Re]Connect to $server",
                                	"Import/Export Data",
                                	"Maintenance Plans",
                                	"      - Define Plan",
                                	"      - Execute Plan",
                                	"      - Backup Operations",

                                	"Shrink Database",
                                	"Detach Database",
                                	"Survey Server",
                                	"Properties",
                                );
				push @sv_rclk, keys %sqlsyb_db_options;
			} elsif( $toi{$typ} eq "oracle"  ) {
				push @sv_rclk, (
                                	"Connection",
                                	"      - Disconnect from $server",
                                	"      - [Re]Connect to $server",

                                	"Survey Server",
                                	"Properties",
                                );
			} elsif( $toi{$typ} eq "unix"  ) {
				push @sv_rclk, (
                                	"Connection",
                                	"      - Disconnect from $server",
                                	"      - [Re]Connect to $server",
                                	"Survey Server",
                                	"Properties",

                                );
			} elsif( $toi{$typ} eq "win32servers"  ) {
                                push @sv_rclk, (
                                	"Connection",
                                	"      - Disconnect from $server",
                                	"      - [Re]Connect to $server",
                                	"Survey Server",
                                	"Properties",
                                );
			}
			push @rightclicks, \@sv_rclk;
		} elsif( defined $server ) {
			my( @sv_rclk );
			if( $toi{$typ} eq "sqlsvr" or $toi{$typ} eq "sybase" ) {
				push @sv_rclk, (
                                	"Connection",
                                	"      - Disconnect from $server",
                                	"      - [Re]Connect to $server",
                                	"Import/Export Data",
                                	"Maintenance Plans",
                                	"      - Define Plan",
                                	"      - Execute Plan",
                                	"      - Backup Operations",

                                	"Register",
                                	"      -  New $type_labels{$curTreeType} Server",
                                	"      -  Edit $server Registration",
                                	"      -  Deregister $server",
                                	"Survey Server",
                                	"Properties",

                                );
				push @sv_rclk, keys %sqlsyb_svr_options;
			} elsif( $toi{$typ} eq "oracle"  ) {
				push @sv_rclk, (
                                	"Connection",
                                	"      - Disconnect from $server",
                                	"      - [Re]Connect to $server",

                                	"Register",
                                	"      -  New $type_labels{$curTreeType} Server",
                                	"      -  Edit $server Registration",
                                	"      -  Deregister $server",
                                	"Survey Server",
                                	"Properties",
                                );
			} elsif( $toi{$typ} eq "unix"  ) {
				push @sv_rclk, (
                                	"Connection",
                                	"      - Disconnect from $server",
                                	"      - [Re]Connect to $server",
                                	"Register",
                                	"      -  New $type_labels{$curTreeType} Server",
                                	"      -  Edit $server Registration",
                                	"      -  Deregister $server",
                                	"Survey Server",
                                	"Properties",
                                );
			} elsif( $toi{$typ} eq "win32servers"  ) {
				push @sv_rclk, (
                                	"Register",
                                	"      -  New $type_labels{$curTreeType} Server",
                                	"      -  Edit $server Registration",
                                	"      -  Deregister $server",
                                	"Survey Server",
                                	"Properties",
                                );
			}
			push @rightclicks, \@sv_rclk;
		}
	}
	return \@rightclicks;
}


# The argument here will be a server type as thats what is returned above
# you then draw a tree just with that is /$servertype/$server with
#    Delete and Edit Items on It and You draw a /$servertype with Add
#    item on it.
sub getTreeItems {
	my($package,$curTreeType)=@_;
	#$working_treetype = $curTreeType;
	$package->debugmsg(  "[servers] getTreeItems(curtype=$curTreeType)\n" );

	my(@treeitems);
	return( undef ) unless defined $servertypes{$curTreeType};

	my(%c) =  $package->get_CONFIG();
	my($connectionmanager)=  $c{-connectionmanager};

	# add /
	push @treeitems, "/".$type_labels{$curTreeType};
	foreach my $s (main::get_server_by_type(-type=>$curTreeType)) {
		push @treeitems, "/$type_labels{$curTreeType}/$s";

		my($x)=$package->global_data( -class=>$curTreeType,-name=>$s, -arg=>'Database_Info', -keys=>1);
		if( defined $x ) {
			foreach ( sort @$x ) {
				push @treeitems, "/$type_labels{$curTreeType}/$s/$_";
			}
		}
	}

	$package->write_CURRENT_CONFIG();
	return \@treeitems;
}

sub do_get_version_info {
	my($package)=shift;
	($version,$sht_version)=get_version(get_root_dir()."/procs/VERSION") unless defined $version;
	return ($version,$sht_version);
}

sub screen_proclib_summary {
	my($package)=@_;
	my($table);
	my($Frame)=$package->clear_tab($std_page_name);
	my($version,$sht_version)=$package->do_get_version_info();
	$package->statusmsg(  "[servers] screen_proclib_summary( version=$version )\n" );
	$package->init() if $#menuitems<0;

	$Frame->Label( -text=>"Stored Procedure Library Manager - Latest Version $version",
		-relief=>'ridge' , -bg=>'white' )->pack( -side=>'top', -fill=>'x' );

	#my $version_subframe = $Frame->Frame()->pack( -fill=>'x', -side=>'top');
    	#$version_subframe->Label(-text => "LATEST LIBRARY VERSION: ")->pack(-side=>'left');
	#$version_subframe->Label(-text => $version, -relief => 'sunken', -bg=>'white')->pack(-side=>'left');

 	@servrs	= main::get_server_by_type(-type=>"sybase");
 	foreach ( @servrs ) { $server_typ{$_}="sybase"; }
 	my @sql=main::get_server_by_type(-type=>'sqlsvr');
 	foreach ( @sql ) { $server_typ{$_}="sqlsvr"; }
 	push @servrs,@sql;

 	my $numrows = scalar @servrs;
	my($table)= $Frame->Table( -rows=>    $numrows+1,
 				   -columns=> 4,
 				   -scrollbars => 'osoe' );

	# PRINT BUTTON BAR
	my $navigator_subframe = $Frame->Frame()->pack( -fill=>'x', -side=>'top');
    	$package->make_button(-widget=>$navigator_subframe,
				-text=>"Refresh Server Version Info",
				-help=>"Poll All Servers And Refresh Version Info",
				-func=>sub {
					foreach my $s ( @servrs ) {$package->screen_upgrade_proclib( $s,$server_typ{$s},"Refresh" );    					}
   					$package->write_CURRENT_CONFIG();
   					$package->screen_proclib_summary();
				} )->pack( -side=>'left', -padx=>3, -pady=>2 );
	$package->make_button(-widget=>$navigator_subframe,
				-text=>"Update To $version Where Obsolete",
				-help=>"Update All Servers To Latest Version - Skipping Those Completed",
				-func=>sub {
					foreach my $s ( @servrs ) {$package->screen_upgrade_proclib( $s,$server_typ{$s},"Incremental" );    					}
   					$package->write_CURRENT_CONFIG();
   					$package->screen_proclib_summary();
   				} )->pack( -side=>'left', -padx=>3, -pady=>2 );
	$package->make_button(-widget=>$navigator_subframe,
				-text=>"Reinstall All Servers",
				-help=>"Update All Servers To Latest Version - Repeating Those Completed",
				-func=>sub {
					foreach my $s ( @servrs ) {$package->screen_upgrade_proclib( $s,$server_typ{$s},"Full" );    					}
   					$package->write_CURRENT_CONFIG();
   					$package->screen_proclib_summary();
   				} )->pack( -side=>'left', -padx=>3, -pady=>2 );

	$package->make_button(-widget=>$navigator_subframe,
				-text=>"Help",
				-help=>"Help",
				-func=>sub {$package->show_html("http://www.edbarlow.com/document/procs/index.htm");} )->pack( -side=>'left', -padx=>3, -pady=>2 );

	$table->put(0,0,$table->Label(-text => "SERVER", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,1,$table->Label(-text => "LIBRARY VERSION", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,2,$table->Label(-text => "DBMS VERSION", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,3,$table->Label(-text => "OPERATION", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,4,$table->Label(-text => "POLL SERVER", -relief => 'sunken', -bg=>'beige'));

 	my($row) = 0;
	foreach my $s ( sort @servrs ) {
 		$row++;
 		my($Server_Version) = $package->global_data(-class=>$server_typ{$s}, -name=>$s, -arg=>"Server_Version");
		my($Proc_Lib_Version)=$package->global_data(-class=>$server_typ{$s}, -name=>$s, -arg=>"Proc_Lib_Version");

		$Server_Version = "[No Info Available]"." "x43 unless defined $Server_Version;
		$Proc_Lib_Version = "[No Info Available]" unless defined $Proc_Lib_Version;
		$table->put($row,0,$table->Label(-text => $s, -relief => 'sunken', -bg=>'beige'));
   		$table->put($row,1,$table->Label(-text => $Proc_Lib_Version, -relief => 'sunken', -bg=>'white'));
   		$table->put($row,2,$table->Label(-text => $Server_Version, -relief => 'sunken', -bg=>'white'));
   		$table->put($row,3,
   			$package->make_button(
   				-widget=>$table,
	 			-help=>"Upgrade Procedure Library On Server $s",
	 			-text=>"Upgrade to $sht_version",
   				-command=> sub {
   					my($sv,$pv)=$package->screen_upgrade_proclib( $s,$server_typ{$s},"Full" );
   					$package->write_CURRENT_CONFIG();
   					$package->screen_proclib_summary();
   				}
 			)
 		);
 		$table->put($row,4,
   			$package->make_button(
   				-widget=>$table,
	 			-help=>"Fetch Library & DBMS Version For Server",
	 			-text=>"Poll $s",
   				-command=> sub {
   					$package->screen_upgrade_proclib( $s,$server_typ{$s},"Refresh" );
   					#my($sv,$pv)=$package->screen_upgrade_proclib( $s,$server_typ{$s},"Full" );
   					#$package->write_CURRENT_CONFIG();
   					$package->screen_proclib_summary();
   				}
 			)
 		);
	}
	$table->pack(-side=>'left',-expand=>1,-fill=>'both');
}

sub event_handler
{
	my($package)=shift;
	my(%args)=@_;

	return if $args{-eventtype} eq "tabchange" 	and $args{-curtab} ne $std_page_name;
	return if $args{-eventtype} eq "expbutton" 	and ! defined $type_labels{$args{-context}};

	return if ! defined $type_labels{$args{-context}}
		and ($args{-eventtype} eq "treebrowse"
			or $args{-eventtype} eq "treertclick"
			or $args{-eventtype} eq "imgbutton" );

	#return if $args{-eventtype} eq "treebrowse" and $args{-curtab} ne $std_page_name;

	my($dummy,$path_svrtype,$path_server,$path_db)=split(/\//,$args{-entrypath});

	if( $args{-eventtype} eq "menu" ) {
		my($found)="FALSE";
		foreach ( @menuitems ) { $found="TRUE" if $args{-menuitem} eq $_; }
		return unless $found eq "TRUE";

        	$package->statusmsg( "[servers] event_handler() called\n" );
		$package->statusmsg(  "[servers] menuclick(",join(",",@_),")\n" );
        	my($entryPath)=$args{-menuitem};
        	if( $entryPath eq "Sql Server/Stored Procedures"
        	or       $entryPath eq "Configuration/Sql Server Stored Procedures"  ){
        		$package->ignore_events(1);
        		if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$package->refresh_tree('sqlsvr') unless $args{-context} eq "sqlsvr";
			$package->statusmsg( "[servers] screen_proclib_summary()\n" );
			$package->screen_proclib_summary();
			$package->ignore_events(0);

        	} elsif( $entryPath eq "Sybase/Stored Procedures"
        	or       $entryPath eq "Configuration/Sybase Stored Procedures"  ){
        		$package->ignore_events(1);
        		if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$package->refresh_tree('sybase') unless $args{-context} eq "sqlsvr";
			$package->statusmsg( "[servers] screen_proclib_summary()\n" );
			$package->screen_proclib_summary();
			$package->ignore_events(0);

        	} elsif( $entryPath =~ "^Configuration/Register"  ){

        		if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$entryPath =~ s/^Configuration\/Register\///;
        		#ok jump to the appropriate server type
        		my($stype)=$toi{$entryPath};
        		my($nb)=$package->properties_by_type($stype);
        		$nb->raise("Summary");
        		$package->add_edit_server(undef,$stype,"add");

        	}

	} elsif( $args{-eventtype} eq "expbutton" ) {

		return if $args{-curtab} eq "monitoring" or $args{-curtab} eq "schema";
		$package->statusmsg( "[servers] event_handler() called for button press\n" );
		$package->ignore_events(1);

		if( $args{-curtab} ne $std_page_name ) {
        		$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
			$main::notebook_f->raise(ucfirst($std_page_name));
		}

#		if( ! defined $type_labels{$args{-context}} ) {
#			$package->statusmsg( "[servers] event_handler() setting context\n" );
#			$package->refresh_tree('sqlsvr');
#			$args{-context}='sqlsvr';	# the refresh_tree sets global variable but we are not
#							# triggering additional event so we do this.
#		}


        	$package->ignore_events(undef);
		my($nb)=$package->properties_by_type($args{-context});# if defined $type_labels{$args{-context}};
		$nb->raise("Summary");

	} elsif( $args{-eventtype} eq "tabchange" ) {

		$package->statusmsg( "[servers] event_handler() called for tab change\n" );
		$package->ignore_events(1);

		if( ! defined $type_labels{$args{-context}} ) {
			$package->statusmsg( "[servers] event_handler() setting context\n" );
			$package->refresh_tree('sqlsvr');
			$args{-context}='sqlsvr';	# the refresh_tree sets global variable but we are not
							# triggering additional event so we do this.
		}

        	$package->ignore_events(undef);

		# $package->properties_by_type($args{-context});# if defined $type_labels{$args{-context}};

		if( defined $path_db ) {
			my($frame)=$package->clear_tab($std_page_name);
			my($server_nb)=$package->properties_by_database($frame,$path_svrtype,$path_server,$path_db);
			$server_nb->raise("Summary");
		} elsif( defined $path_server ) {
			my($frame)=$package->clear_tab($std_page_name);
			my($nb)=$package->properties_by_server($frame,$path_server,$path_svrtype);
			$nb->raise("Summary");
    		} else {
			my($nb)=$package->properties_by_type($args{-context});
			$nb->raise("Summary");
		}

	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {

		return unless $args{-curtab} ne "welcome"
		and $args{-curtab} ne "sql"
		and $args{-curtab} ne "log"
		and $args{-curtab} ne "debug"
		and $args{-curtab} ne "plugin"
		and $args{-curtab} ne "backups";

		$package->statusmsg( "[servers] event_handler() called for tree browse\n" );
		if (	$args{-newtab} eq "welcome"
		and 	$args{-curtab} eq "welcome" ) {
			$package->ignore_events(1);
			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
			$main::notebook_f->raise(ucfirst($std_page_name));
			$package->ignore_events(undef);
		}

		# Ok someone clicked something
		if( defined $path_db ) {
			my($frame)=$package->clear_tab($std_page_name);
			my($server_nb)=$package->properties_by_database($frame,$path_svrtype,$path_server,$path_db);
			$server_nb->raise("Summary");
		} elsif( defined $path_server ) {
			my($frame)=$package->clear_tab($std_page_name);
			my($server_nb)=$package->properties_by_server($frame,$path_server,$path_svrtype);
			$server_nb->raise("Summary");
    		} else {
			my($nb)=$package->properties_by_type($args{-context});
			$nb->raise("Summary");
		}

	} elsif( $args{-eventtype} eq "treertclick" ) {

		my($entryPath, $clktext)=($args{-entrypath},$args{-clicktext});
            	$package->statusmsg( "[servers] event_handler(Path=$entryPath Txt=$clktext) right clicked\n" );

            	my($typ,$server,$db)=($path_svrtype,$path_server,$path_db);
            	my($working_treetype)=$toi{$typ} if defined $toi{$typ};

		if( $clktext eq "Stored Procedure Library" ) {
			my($server_nb)=$package->properties_by_type($args{-context});
			$package->ignore_events(1);
			if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$package->ignore_events(undef);
			$server_nb->raise('Procedure Library');
		} elsif( $clktext eq "Registration Summary" ) {
			my($server_nb)=$package->properties_by_type($args{-context});
			$package->ignore_events(1);
			if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$package->ignore_events(undef);
			$server_nb->raise('Summary');
		} elsif( $clktext eq "Cross Server Reports" ) {
			$package->unimplemented($clktext);
		} elsif(( $clktext =~ /^Register New/ and $clktext=~/Server$/ )
		or $clktext eq "Register"
		or $clktext =~ /^      -  New / and $clktext=~ / Server$/ ) {
			$package->messageBox(
					-title => "NOTE",
					-message => "Feature $clktext is under development\nI hope it works...",
					-type => "OK" );
			$package->add_edit_server($path_server,$args{-context},"add");
		} elsif( $clktext eq "Files And Directories" ) {
			my($server_nb)=$package->properties_by_type($args{-context});
			$package->ignore_events(1);
			if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$package->ignore_events(undef);
			$server_nb->raise('Files and Directories');
		} elsif( $clktext eq "Survey Server" ) {
#			my($server_nb)=$package->properties_by_type($args{-context});
#			$package->ignore_events(1);
#			if( $args{-curtab} ne $std_page_name ) {
#        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
#				$main::notebook_f->raise(ucfirst($std_page_name));
#			}
#			$package->ignore_events(undef);
#			$server_nb->raise('Survey');
#			$package->messageBox(
#					-title => "NOTE",
#					-message => "From This Screen You can Perform Surveys",
#					-type => "OK" );

			my(%pargs)= (
				-sybase_dirs=>		$main::server_info{$path_server.":".$args{-context}}{SYBASE}||"",
				-sybase_client_dirs=>	$main::server_info{$path_server.":".$args{-context}}{SYBASE_CLIENT}||"",
				-ignore_runfile=> 	$main::server_info{$path_server.":".$args{-context}}{IGNORE_RUNFILE}||""
					) if $args{-context} eq "unix";

			main::busy();
			my($c,$errmsg)=$connectionmanager->connect(-name=>$path_server, -type=>$args{-context});
			main::colorize_tree($server);
			my(@rc);
            		if( ! $c ) {
            			$package->messageBox(
					-title => "ERROR",
					-message => "Failed To Connect to $server: $errmsg",
					-type => "OK" );

            			$package->statusmsg("[servers] unable to connect to $server: $errmsg\n");
               		} else {
              			@rc=$main::gem_data->survey(
               				-connection=>$c,
					-type=>$args{-context},
					-name=>$path_server,
					#-login=>$main::server_info{$path_server.":".$args{-context}}{login},
					#-since=>time-24*7*3600,
					#-password=>$main::server_info{$path_server.":".$args{-context}}{password},
					-local_directory => $main::CURRENT_CONFIG{gem_root_directory}.'/data/system_information_data',
					%pargs );
			}
			main::unbusy();
			$package->messageBox(
					-title => "NOTE",
					-message => join("",@rc),
					-type => "OK" );

		} elsif( $clktext =~ /^Survey / ) {
			my($server_nb)=$package->properties_by_type($args{-context});
			$package->ignore_events(1);
			if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$package->ignore_events(undef);
			$server_nb->raise('Survey');
			$package->messageBox(
					-title => "NOTE",
					-message => "From This Screen You can $clktext",
					-type => "OK" );

# ****************** DONE FIRST SECTION ***********************************

        	} elsif( $clktext eq "Import/Export Data" ) {
        		my($frame)=$package->clear_tab($std_page_name);
			my($server_nb)=$package->properties_by_database($frame,$path_svrtype,$path_server,$path_db);
			$package->ignore_events(1);
			if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
			}
			$package->ignore_events(undef);
			$server_nb->raise("Operations");
			$package->unimplemented($clktext);
		} elsif( $clktext eq "Properties" ){
			if( defined $path_db ) {
				my($frame)=$package->clear_tab($std_page_name);
				my($server_nb)=$package->properties_by_database($frame,$path_svrtype,$path_server,$path_db);
				$server_nb->raise("Summary");
			} elsif( defined $path_server ) {
				my($frame)=$package->clear_tab($std_page_name);
				my($server_nb)=$package->properties_by_server($frame,$path_server,$path_svrtype);
				$server_nb->raise("Summary");
    			} else {
				my($nb)=$package->properties_by_type($args{-context});
				$nb->raise("Summary");
			}
		} elsif( defined $sqlsyb_db_options{$clktext} ) {

			# raise properties if its not raised
			if( $args{-curtab} ne $std_page_name ) {
				$package->ignore_events(1);
				$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
				$package->ignore_events(undef);
			}
			# now navigate to given server - and the audit subtab
			my($frame)=$package->clear_tab($std_page_name);
			my($nb)=$package->properties_by_server($frame,$path_server,$path_svrtype);
			$nb->raise($sqlsyb_svr_options{$clktext});
		} elsif( defined $sqlsyb_svr_options{$clktext} ) {
			# raise properties if its not raised
			if( $args{-curtab} ne $std_page_name ) {
				$package->ignore_events(1);
				$package->statusmsg( "[servers] event_handler() raising tab $std_page_name\n" );
				$main::notebook_f->raise(ucfirst($std_page_name));
				$package->ignore_events(undef);
			}
			# now navigate to given server - and the audit subtab
			my($frame)=$package->clear_tab($std_page_name);
			my($nb)=$package->properties_by_database($frame,$path_svrtype,$path_server,$path_db);
			$nb->raise($sqlsyb_svr_options{$clktext});
		} elsif( $clktext eq "Connection" ) {
			return;
		} elsif( $clktext =~ /^\s+- Disconnect from/ ) {
			$connectionmanager->disconnect(-name=>$server, -type=>$working_treetype);
			$package->ignore_events(1);
			$package->refresh_tree();
			$package->ignore_events(undef);
		} elsif( $clktext =~ /^\s+\- \[Re\]Connect to/ ) {
			my($c,$errmsg)=$connectionmanager->connect(-name=>$server, -type=>$working_treetype);
			main::colorize_tree($server);
            		if( ! $c ) {
            			$package->messageBox(
					-title => "ERROR",
					-message => "Failed To Connect to $server: $errmsg",
					-type => "OK" );

            			$package->statusmsg("[servers] unable to connect to $server: $errmsg\n");
               		} else {
               			$package->statusmsg("[servers] connected to $server\n");
               		}

               		$package->write_CURRENT_CONFIG();
               		#$package->refresh_tree();
		} elsif( $clktext eq "Maintenance Plans" ) {
			return;
		} elsif( $clktext eq "      - Define Plan" ) {
			$backup::current_server=$server;
			$backup::current_plan=undef;
			$main::notebook_f->raise("Backups");
		} elsif( $clktext eq "      - Execute Plan" ) {
			$backup::current_server=$server;
			$backup::current_plan=undef;
			$main::notebook_f->raise("Backups");
		} elsif( $clktext eq "      - Backup Operations" ) {
			$backup::current_server=$server;
			$backup::current_plan=undef;
			$main::notebook_f->raise("Backups");
		} elsif( $clktext eq "Shrink Database" ) {
			$package->unimplemented($clktext);
		} elsif( $clktext eq "Detach Database" ) {
			$package->unimplemented($clktext);
		#} elsif( $clktext eq "Database Options" ) {
		} elsif( $clktext =~ /^      -  Edit / and $clktext=~ / Registration$/ ) {
			$package->unimplemented($clktext);
		} elsif( $clktext =~ /^      -  Deregister / ) {
			$package->unimplemented($clktext);
			$package->do_delete_a_server($server,$working_treetype);
            		$server=undef;
		} elsif( $clktext =~ /^Register New / and $clktext=~ / Server$/ ) {
			$package->unimplemented($clktext);
		}

#               	if( $clktext =~ /^Register New/i ) {
#            	} elsif( $clktext =~ /^Show Summary/ ) {
#            		$server = undef;
#            	} elsif( $clktext =~ /^Show All/i ) {
#            		#$package->debugmsg( "Clk=$clktext\n" );
#            		$clktext=~s/Show All //;
#            		#print "Var=$clktext\n";
#            		$working_treetype=$toi{$clktext} if defined $toi{$clktext};
#            		#print "stype=$working_treetype\n";
#            		$server = undef;
#            	} elsif( $clktext =~ /^Edit/i ) {
#            		die "No Server Type" unless defined $working_treetype;
#
#            		#if( defined $multiserver_by_name{ $servername} ) {
#            		#	# ok figure out what type this server is...
#            		#	if( $multiserver_by_name{$servername} =~ /$working_treetype/ ) {
#            		#		$working_treetype=$working_treetype;
#            		#	} else {
#            		#		$working_treetype=$servertype_by_name{$servername};
#            		#		$package->messageBox(-message=>"Duplicate Server Type For $servername : $multiserver_by_name{ $servername}!!!", -title=>"WARNING");
#            		#		return;
#            		#	}
#            		#} else {
#            		#	$working_treetype=$servertype_by_name{$servername}
#            		#}
#
#            		#my(%dat) = get_password_info( -name=>$server, -type=>$working_treetype );
#               		#my($login,$pass,$ctyp) = get_password( -name=>$server, -type=>$working_treetype );
#               		#$package->add_edit_server( $server, $login, $pass, $ctyp,%dat );
#               		$package->add_edit_server( $server, $working_treetype, "edit" );
#            	} elsif( $clktext =~ /^Deregister/i ) {
#            		#$servername =~ s/^deregister\s+//i;
#            		$package->do_delete_a_server($server,$working_treetype);
#            		$server=undef;
#            	} elsif( $clktext =~ /^Disconnect/i ) {
#            		$connectionmanager->disconnect(-name=>$server, -type=>$working_treetype);
#            	} elsif( $clktext =~ /^\[Re\]Connect/i or $clktext =~ /^ReConnect/i ) {
#            		my($c,$errmsg)=$connectionmanager->connect(-name=>$server, -type=>$working_treetype, -refetchdb=>1);
#			main::colorize_tree($server);
#            		if( ! $c ) {
#            			$package->statusmsg("[servers] unable to connect to $server\n");
#               		} else {
#               			$package->statusmsg("[servers] connected to $server\n");
#               		}
#               		$package->write_CURRENT_CONFIG();
#               		$package->refresh_tree();
#            	} elsif( $clktext =~ /^Ping All/i ) {
#            		$package->do_ping_all();
#            	} else {
#            		return;
#            	}
#            	$package->show("show",$server,$working_treetype);
	} elsif( $args{-eventtype} eq "imgbutton" ) {
		#my($entryPath)=@_;
		$package->statusmsg( "[servers] bitmapclick(",$args{-menuitem}," unimplemented)\n" );
		#$package->browse("/sybase");
	}
}


#sub do_ping_all {
#	my($package)=shift;
#	my($working_treetype)=shift;
#	my(@srvrs) = main::get_server_by_type( -type=>$working_treetype );
#	foreach ( @srvrs ) {
#		my($rc)=$connectionmanager->ping(-name=>$_, -type=>$working_treetype);
#		$package->statusmsg( "[servers] ping of $_ - result = $rc\n" );
#	}
#	$package->write_CURRENT_CONFIG;
#	$package->properties_by_type($working_treetype);
#}

#sub draw_page_top {
#	my($navigator_subframe) = $frame->Frame()->pack( -fill=>'x', -side=>'top');
#	#my $frame=$frame->Frame()->pack(-side=>'top',-fill=>'x', -anchor=>'n');
#    	$package->make_button(-widget=>$navigator_subframe,
#    			-help=>'Ping All Servers of this type',
#    			-text=>"Ping All",
#    			-command=> sub { $package->do_ping_all(); }
#    			 )->pack( -side=>'left', -padx=>3, -pady=>8 );
#
#	$package->make_button(-widget=>$navigator_subframe,
#    			-help=>'Configure Stored Procedure Library',
#    			-text=>"Stored Procedures",
#    			-command=> sub { $package->screen_proclib_summary(); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
#
#    	my $b5 = $package->make_button(-widget=>$navigator_subframe,
#			-text=>"Register New Server",
#			-help=>"Register A Server To The Enterprise Manager",
#			-func=>sub { 	$package->add_edit_server(undef,$working_treetype,"add");	});
#
#	$b5->pack( -side=>'left',-pady=>2, -padx=>2 );
#
#	$servername = "Unknown" unless $servername;
#	my $b6 = $package->make_button(-widget=>$navigator_subframe,
#			-text=>"Deregister $servername",
#			-help=>"Delete Server $servername From The System",
#			-func=>sub { $package->do_delete_a_server($servername,$stype)} );
#	my $b7 = $package->make_button(-widget=>$navigator_subframe,
#			-text=>"Edit $servername Registration",
#			-help=>"Edit Server $servername",
#			-func=>sub {
#				#my(%dat) = get_password_info( -name=>$servername, -type=>$stype );
#   				#my($login,$pass,$conn)=get_password( -name=>$servername, -type=>$stype );
#   				#$package->add_edit_server( $servername, $login, $pass, $conn, %dat);
#   				$package->add_edit_server( $servername, $stype, "edit");
#			} );
#	my $b8 = $package->make_button(-widget=>$navigator_subframe,
#			-text=>"Ping $servername",
#			-help=>"Ping $servername Server",
#			-func=>sub {
#	 			my($c)=$connectionmanager->ping(-name=>$servername, -type=>$working_treetype);
#	 			if( $c ) {
#	 				$package->messageBox(-message=>"Ping of $servername Succeeded!!!", -title=>"ping of $servername");
#	 			} else {
#	 				$package->messageBox(-message=>"Ping of $servername Failed!!!", -title=>"ping of $servername");
#	 			}
#			} );
#
#	my $b9 = $package->make_button(-widget=>$navigator_subframe,
#			-text=>"Survey $servername",
#			-help=>"Survey $servername Server",
#			-func=>sub {
#	 			my($c)=$connectionmanager->ping(-name=>$servername, -type=>$working_treetype);
#	 			if( $c ) {
#	 				$package->messageBox(-message=>"Ping of $servername Succeeded!!!", -title=>"ping of $servername");
#	 			} else {
#	 				$package->messageBox(-message=>"Ping of $servername Failed!!!", -title=>"ping of $servername");
#	 			}
#			} );
#
#	$b6->pack( -side=>'left',-pady=>2, -padx=>2 );
#	$b7->pack( -side=>"left",-pady=>2, -padx=>2 );
#	$b8->pack( -side=>"left",-pady=>2, -padx=>2 );
#	$b9->pack( -side=>"left",-pady=>2, -padx=>2 );
#}

#sub do_delete_a_server {
#	my($package,$server,$stype)=@_;
#	$package->statusmsg( "[servers] do_delete_a_server($server,$stype)" );
#
# 	if( ! defined $stype ) {
# 		$package->messageBox( -title => 'Notice',-message => "Unknown Server Type For $server - Can Not Delete",-type => "OK" );
# 		return;
# 	}
#
# 	# confirm the delete
#	my($d) = $package->DialogBox(-title=>"Confirm $server De-Registration",-buttons=>["OK","Cancel"],-default_button=>'Cancel');
#	$d->add("Label",-text=>"Confirm De-Registration of Server \"$server\"")->pack();
#	my $button=$d->Show();
#	if( $button ne "OK" ) {
#		$package->statusmsg( "[servers] $server will not be deleted ($button)" );
#		return;
#	}
#
#	$package->messageBox( -title => 'Notice',-message => "Server $server Of Type $stype Deleted",-type => "OK" );
#	#delete $servertype_by_name{$server};
#	$package->statusmsg( "[servers] add_edit_item( -op=>del, -name=>$server, -type=>$stype )" );
#	add_edit_item( -op=>"del", -name=>$server, -type=>$stype, -debug=>1 );
#	# $package->show("show",undef,$working_treetype);
#	$package->refresh_tree() if $stype=$working_treetype;
#}

sub add_edit_server {
	my( $package, $server, $servertype, $op )=@_;

	$package->statusmsg( "[servers] add_edit_server($server, $servertype, $op)" );
	my(@keys)= get_passfile_info(-type=>$servertype);
	my(%k);

	my( %dat );
	unless ( $op eq "add" ) {
		my( $dathash ) = $main::server_info{$server.":$servertype"};
		if( ! defined $dathash ) {
			$package->statusmsg( "[servers]  WARNING: Server $server of type $servertype Has been Deleted\n" );
			return;
		}
		%dat = %$dathash;
	}

	foreach (@keys) {
		$k{$_}=1;
		$dat{$_}="" if $op eq "add" and $_ ne "server" and
			$_ ne "login" and $_ ne "password" and $_ ne "conntype";
	}

	my($login,$pass,$ctyp)=($dat{login},$dat{password},$dat{conn_type}) unless $op eq "add";
	my( $radio ) = $dat{SERVER_TYPE} || "PRODUCTION";

	# must do package->DialogBox cause package has the toplevel
	my($dlg_addsvr)= $package->DialogBox(
		-title 		=> "Enter ".main::get_label_by_type($servertype)." Server Information",
		-buttons 	=> [ "Ok", "Cancel" ] );

	my($dlg_addsvr_name,$dlg_addsvr_password,$dlg_addsvr_login,$dlg_addsvr_password, $rb_frm, $rb_ctyp);
	my($row)=1;
	if( $k{server} ) {
		$dlg_addsvr->add("Label",-text=>"Server Name")->grid(-column=>1, -row=>$row);
		$dlg_addsvr_name = $dlg_addsvr->add("Entry",-textvariable=>\$server,-width=>35)->grid(-column=>2,-row=>$row);
		$row++;
	}

	if( $k{login} ) {
		$dlg_addsvr->add("Label",-text=>"Administrator Login")->grid(-column=>1, -row=>$row);
		$dlg_addsvr_login = $dlg_addsvr->add("Entry",-textvariable=>\$login,-width=>35)->grid(-column=>2,-row=>$row);
		$row++;
	}

	if( $k{password} ) {
		$dlg_addsvr->add("Label",-text=>"Administrator Password")->grid(-column=>1, -row=>$row);
		$dlg_addsvr_password = $dlg_addsvr->add("Entry",-show=>'*',-textvariable=>\$pass,-width=>35)->grid(-column=>2,-row=>$row);
		$row++;
	}

	if( $k{-SERVER_TYPE} ) {
		$dlg_addsvr->add("Label",-text=>"Server Type")->grid(-column=>1, -row=>$row);
		$rb_frm=$dlg_addsvr->Frame()->grid(-column=>2,-row=>$row);
		$rb_frm->Radiobutton(-variable=>\$radio,-text=>'Development',-value=>'DEVELOPMENT')->pack( -fill=>'y', -side=>'left');
		$rb_frm->Radiobutton(-variable=>\$radio,-text=>'Production',-value=>'PRODUCTION')->pack( -fill=>'y', -side=>'left');
		$rb_frm->Radiobutton(-variable=>\$radio,-text=>'Critical',-value=>'Critical')->pack( -fill=>'y', -side=>'left');
		$rb_frm->Radiobutton(-variable=>\$radio,-text=>'QA',-value=>'QA')->pack( -fill=>'y', -side=>'left');
		$row++;
	}

	if( $servertype eq "sybase" ) {
		$dlg_addsvr->add("Label",-text=>"Connection Type")->grid(-column=>1, -row=>$row);
		$rb_ctyp=$dlg_addsvr->Frame()->grid(-column=>2,-row=>$row);
		$rb_ctyp->Radiobutton(-variable=>\$ctyp,-text=>'ODBC',-value=>'ODBC')->pack( -fill=>'y', -side=>'left');
		$rb_ctyp->Radiobutton(-variable=>\$ctyp,-text=>ucfirst($servertype),-value=>ucfirst($servertype))->pack( -fill=>'y', -side=>'left');
		$row++;
	}

	foreach ( keys %dat ) {
		next if $_ eq "-SERVER_TYPE" or $_ eq "SERVER_TYPE";
		my($txt)=$_;
		$txt=~s/^-//;
		$dlg_addsvr->add("Label",-text=>$_)->grid(-column=>1, -row=>$row);
		$dlg_addsvr->add("Entry",-textvariable=>\$dat{$_},-width=>35)->grid(-column=>2,-row=>$row);
		$row++;
	}

	my $button=$dlg_addsvr->Show();
	if( $button eq "Ok" ) {
		$package->statusmsg("[servers] add_edit_server - OK Button Pressed");

		#my($dlg_addsvr_name,$dlg_addsvr_password,$dlg_addsvr_login,$dlg_addsvr_password, $rb_frm, $rb_ctyp);

		return $package->messageBox( -title => 'Data Entry Error',
			-message => 'ERROR: name not entered',
			-type => "OK" ) if $k{server} and $server=~/^\s*$/;
		return $package->messageBox( -title => 'Data Entry Error',
			-message => 'ERROR: name may not contain blanks',
			-type => "OK" ) if $k{server} and $server=~/\s/;
		return $package->messageBox( -title => 'Data Entry Error',
			-message => 'ERROR: login not entered',
			-type => "OK" ) if $k{login} and $login=~/^\s*$/;
		return $package->messageBox( -title => 'Data Entry Error',
			-message => 'login may not contain blanks',
			-type => "OK" ) if $k{login} and $login=~/\s/;
		return $package->messageBox( -title => 'Data Entry Error',
			-message => 'ERROR: password not entered',
			-type => "OK" ) if $k{password} and $pass=~/^\s*$/;
		return $package->messageBox( -title => 'Data Entry Error',
			-message => 'password may not contain blanks',
			-type => "OK" ) if $k{password} and $pass=~/\s/;

		$package->statusmsg("[servers] add_edit_server - saving $server of type $servertype");

		main::do_add_a_server(main::get_label_by_type($servertype),$server,$login,$pass,$servertype,$ctyp);

		# if new you can not have existing
#		if( $op eq "add" ) {
#				$package->messageBox(
#					-title => 'Data Entry Error',
#					-message => 'Duplicate Server',
#					-type => "OK" );
#				return 0;
#		}
#		foreach ( keys %dat ) {
#			$package->statusmsg("[servers] DAT $_ => $dat{$_}");
#		}
#
#		add_edit_item( -op=>$op,
#			-name=>$server,
#			-type=>$servertype,
#			-login=>$login,
#			-conntype=>$ctyp,
#			-password=>$pass,
#			%dat,
#			-debug=>1 );
#
#		$package->statusmsg("[servers] $op Server $server to Repository completed");

		if( $op eq "add" ) {
			$package->statusmsg("[servers] refresh_tree() completed");
			$package->refresh_tree();
		}

	} elsif( $button eq "Cancel" ) {
		$package->statusmsg("[servers] CANCEL BUTTON PRESSED");
		return 0;
	} else {
		die "This Cant Happen\n";
	}
}

sub getBitmaps {
	my($package)=@_;
	my(@ids)= ('previous', 'next' ,'update','mark','markplus','markall');

   	my %bitmaps = ('previous' => '#define previous_width 16
   #define previous_height 16
   static unsigned char previous_bits[] = {
      0x08, 0x10, 0x08, 0x18, 0x08, 0x1c, 0x08, 0x1e, 0x08, 0x1f, 0x88, 0x1f,
      0xc8, 0x1f, 0xe8, 0x1f, 0xc8, 0x1f, 0x88, 0x1f, 0x08, 0x1f, 0x08, 0x1e,
      0x08, 0x1c, 0x08, 0x18, 0x08, 0x10, 0x00, 0x00};
   ',
             'next' => '#define next_width 16
   #define next_height 16
   static unsigned char next_bits[] = {
      0x08, 0x10, 0x18, 0x10, 0x38, 0x10, 0x78, 0x10, 0xf8, 0x10, 0xf8, 0x11,
      0xf8, 0x13, 0xf8, 0x17, 0xf8, 0x13, 0xf8, 0x11, 0xf8, 0x10, 0x78, 0x10,
      0x38, 0x10, 0x18, 0x10, 0x08, 0x10, 0x00, 0x00};
   ',
             'update' => '#define update_width 16
   #define update_height 16
   static unsigned char update_bits[] = {
      0x00, 0x00, 0x00, 0x00, 0xc0, 0x01, 0xf0, 0x07, 0x38, 0x0e, 0x1c, 0x1c,
      0x0c, 0x1c, 0x0e, 0x1c, 0x0e, 0x1c, 0x86, 0xff, 0x06, 0x7f, 0x06, 0x3e,
      0x0c, 0x1c, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00};
   ',
             'mark' => '#define /home/dharrel/mark.bmp_width 16
   #define /home/dharrel/mark.bmp_height 16
   static unsigned char /home/dharrel/mark.bmp_bits[] = {
      0x00, 0x00, 0x00, 0x70, 0x00, 0x3c, 0x00, 0x0e, 0x00, 0x07, 0x00, 0x03,
      0x80, 0x01, 0x9c, 0x01, 0xbe, 0x00, 0xf8, 0x00, 0xf0, 0x00, 0xe0, 0x00,
      0xc0, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00};
   ',
             'markplus' => '#define markplus_width 16
   #define markplus_height 16
   static unsigned char markplus_bits[] = {
      0x00, 0x00, 0x00, 0x38, 0x00, 0x1e, 0x00, 0x07, 0x80, 0x03, 0x80, 0x01,
      0xc0, 0x08, 0xce, 0x08, 0x5f, 0x08, 0x7c, 0x7f, 0x78, 0x08, 0x70, 0x08,
      0x60, 0x08, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00};
   ',
             'markall' => '#define markall_width 16
   #define markall_height 16
   static unsigned char markall_bits[] = {
      0x00, 0x00, 0x00, 0x70, 0x00, 0x3c, 0x00, 0x0e, 0x00, 0x07, 0x00, 0x03,
      0x80, 0x01, 0x9c, 0x01, 0xbe, 0x00, 0xf8, 0xa4, 0xf0, 0xaa, 0xe0, 0xae,
      0xc0, 0xaa, 0xc0, 0xaa, 0x00, 0x00, 0x00, 0x00};
   ',

   );

   my %bitmaptext = (
   	     'previous' => 'Previous',
             'next' 	=> 'Next',
             'update' 	=> 'Update',
             'mark' 	=> 'Mark   ',
             'markplus' => 'Mark Plus',
             'markall' 	=> 'Mark All'
   	);

   	my %baloontext = (
   	     'previous' => 'Previous Item',
             'next' 	=> 'Next Item',
             'update' 	=> 'Update Screen',
             'mark' 	=> 'Mark Them',
             'markplus' => 'Mark Plus Selected',
             'markall' 	=> 'Mark All Selected'
   	);

	my($i)=1;
	my(@items);
	foreach (@ids) {
		my(%itm);
		$itm{name} 	  = $_;
		$itm{order}	  = $i++ * 10;
		$itm{image} 	  = $bitmaps{$_};
		$itm{help_text}   = $bitmaptext{$_};
		$itm{baloon_text} = $baloontext{$_};
		$itm{plugin}	  = "servers";
		push @items, \%itm;
 	}
   	return @items;
}

sub screen_upgrade_proclib {
	my($package,$svr,$svrtype,$upgradetype)=@_;
	$package->statusmsg(  "[servers] screen_upgrade_proclib( srvr=$svr,typ=$svrtype,$upgradetype )\n" );
	my($c,$errmsg)=$connectionmanager->connect(-name=>$svr, -type=>$svrtype);
	main::colorize_tree($svr);
	if( ! $c ) {
		$package->messageBox(
				-title => "ERROR",
				-message => "Failed To Connect to $svr: $errmsg",
				-type => "OK" );

		$package->statusmsg(  "[servers] can not connect to $svr\n" );
		$package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Proc_Lib_Version", -value=>"[Can Not Connect]")
			unless $package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Proc_Lib_Version");
		$package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Server_Version", -value=>"[Can Not Connect]")
			unless $package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Server_Version");
		return(	"Can Not Connect","Can Not Connect" )
	}


   	$package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"version_poll_time", -value=>time);

	my($Server_Version) = $package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Server_Version");
	my($new_Server_Version) =join(" ",dbi_db_version(-connection=>$c));
	if( $Server_Version ne $new_Server_Version ) {
		$package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Server_Version", -value=>$new_Server_Version);
		$Server_Version = $new_Server_Version;
	}

	#my($Proc_Lib_Version)=$package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Proc_Lib_Version");
	my($Proc_Lib_Version) = $package->get_proc_version($c,$svr,$svrtype);

	my( $dathash ) = $main::server_info{$svr.":$svrtype"};
	if( ! defined $dathash ) {
		$package->statusmsg( "[servers]   WARNING: Server $svr of type $svrtype Has been Deleted\n" );
		return;
	}
	my( %dat ) = %$dathash;

	if( $upgradetype eq "Full" or $upgradetype eq "Incremental" ) {
		$package->statusmsg(  "[servers] PERFORMING UPGRADE TO SERVER (see background screen)\n" );
		my($cmd)=get_root_dir()."/procs/configure.pl";
		die "CAN NOT FIND COMMAND $cmd" unless -r $cmd;

		my($login,$password,$conn)=($dat{login},$dat{password},$dat{conn_type});

		$cmd.="	 --USER=$login --SERVER=$svr --TYPE=$conn";
		if( $upgradetype eq "Incremental" ) {
			my($version,$sht_version)=$package->do_get_version_info();
			$cmd.="  --MINVERSION=".$version;
		}

		$package->statusmsg(  "[servers] EXECUTING $^X $cmd --PASSWORD=xxx\n" );
		$cmd.=" --PASSWORD=$password";
		#require Tk::ProgressSplash;
		#my($splashscreen)=Tk::ProgressSplash->Show(-splashtype=>'fast',"gem.gif",350,200,"Stored Procedure Installer",0);
		open(DOIT,"$^X -I".get_root_dir()."/lib $cmd |");
		#my($cnt)=1;
		#my($numsteps)=$package->global_data(-class=>"package", -name=>"servers", -arg=>"numsteps");
		#$numsteps=100 unless $numsteps;
		my($errmsg)="";
		my($lineno)=0;
		while(<DOIT>) {
			$package->statusmsg(  "[installer] $svr: $_" );
			$errmsg.=$_ if $lineno<10;
			if( $_ =~ "^\* Cant connect to" ) {
				$package->messageBox(
					-title => "ERROR CONNECTING TO SERVER",
					-message => $errmsg,
					-type => "OK" );
			}
			$lineno++;
			#$splashscreen->Update( $cnt/$numsteps);
			#$cnt++ if /Installing/;
		}
		close(DOIT);
		#$package->global_data(-class=>"package", -name=>"servers", -arg=>"numsteps", -value=>$cnt)
		#	if $cnt>$numsteps;
		#$splashscreen->Destroy();
		$Proc_Lib_Version=$package->get_proc_version( $c, $svr,$svrtype);
	}

	$package->statusmsg( "[procs] server=$Server_Version  proclib=$Proc_Lib_Version\n" );
	return($Server_Version,$Proc_Lib_Version);
}

sub get_proc_version {
	my($package)=shift;
	my($c,$svr,$svrtype)=@_;
	my($rc);
	die "ConnectionManager not defined" unless defined $connectionmanager;
	foreach (  $connectionmanager->query(-query=>"sp_proclib_version",-connection=>$c,-db=>"master") ) {
		my($val)=dbi_decode_row($_);
		$val="[Not Installed]" if $val=~/42000: Could not find stored procedure/
					or $val=~/not found. Specify owner.object/;
		$rc=$val;
		$package->global_data(-class=>$svrtype, -name=>$svr, -arg=>"Proc_Lib_Version", -value=>$rc);
	}
	$package->statusmsg( "[procs] server=$svr proclib version is at $rc\n" );
	return $rc;
}

#sub create_a_page {
#	my($package,$notebook,$tabname,$servername,$servertype,$database,$query,$report_name,$in_each_db_query)=@_;
#
#	die "Incorrect # args passed to create_a_page()\n" unless defined $report_name;
#	$package->debugmsg( "[servers] create_a_page($tabname,$servername,$servertype,$database,$query)\n" );
#	my($rc)= $notebook->add($tabname,
#		-label=>$tabname,
#		-underline=>0,
#		-createcmd=>sub {$package->create_a_tab($tabname); },
#		-raisecmd =>sub {$package->raise_a_tab($tabname); }
#	);
#
#	my($textVariable,$textObject);
#	if( $query ne "" or $report_name ne "NO" ) {
#		$textVariable="Insert Results of $query on $servername/$servertype/$database";
#		$rc->Label( -textvariable=>\$textVariable,
#			-relief=>'ridge' , -bg=>'red' )->pack( -side=>'top', -fill=>'x' );
#
#		$textObject = $rc->Scrolled("ROText",
#			-relief => "sunken", -wrap=>'none',
#                	-bg=>'white',-font=>'code',
#                	-scrollbars => "osoe")->pack(-side   => "top",
#                                                -anchor => 'w',
#                                                -expand => 1,
#                                                 -fill   => 'both');
#
#        	$textObject->tagConfigure("t_hdr",   -foreground=>'black', -relief=>'raised', -background=>'beige');
#        	$textObject->tagConfigure("t_blue",  -foreground=>'blue');
#        	$textObject->tagConfigure("t_red",   -foreground=>'red');
#        	$textObject->tagConfigure("t_orange", -foreground=>'orange');
#        }
#
#	my(%this_tab_stuff);
#	$this_tab_stuff{tabname}		= $tabname;
#	$this_tab_stuff{servername}		= $servername;
#	$this_tab_stuff{servertype}		= $servertype;
#	$this_tab_stuff{database}		= $database || "master";
#	$this_tab_stuff{query}			= $query;
#	$this_tab_stuff{in_each_db_query} 	= $in_each_db_query;
#	$this_tab_stuff{report_name}		= $report_name;
#	$this_tab_stuff{window}			= $rc;
#	$this_tab_stuff{textVariable}		= \$textVariable;
#	$this_tab_stuff{textObject}		= \$textObject;
#
#	$tabinfo{$tabname}=\%this_tab_stuff;
#
#	return $rc;
#}

#sub show_that_hash {
#	my($package,$thehashref,$textObject,$title,$do_delete,$reportname)=@_;
#	$textObject->delete("1.0","end") if $do_delete;
#	$textObject->insert('end', $title, "t_hdr") if $title ne "";
#	my($font)="t_blue";
#	if( ! defined $thehashref ) {
#		$textObject->insert('end',"No Data Found For Report\n", $font);
#	} elsif( $reportname eq "Configuration"
#	or $reportname eq "Database Layout"
#	or $reportname eq "Disk/Storage Layout") {
#		# these reports are pure tables
#
#		my(@keys);
#		@keys=("category","comment","default","value") if $reportname eq "Configuration";
#		@keys=("key","Space_Allocated","LogSpaceByDb","DBoptions") if $reportname eq "Database Layout";
#		@keys=("key","size","alloc","free","phys","growth","maxsize") if $reportname eq "Disk/Storage Layout";
#
#		# GET THE LENGTHS
#		my(%maxsize_by_key);
#		foreach (@keys) {
#			$maxsize_by_key{$_} = length;
#		}
#		foreach my $item (keys %$thehashref) {
#			if( ! ref $$thehashref{$item} eq "HASH" ) {
#				die "DBG: ITS NOT A HASH REF $item / $$thehashref{$item}\n";
#			}
#			my(%href) = %{$$thehashref{$item}};
#			foreach(@keys) {
#				my($l)=length($href{$_});
#				$l=length($item) if $_ eq "key";
#				$maxsize_by_key{$_} = $l if $l>$maxsize_by_key{$_};
#			}
#		}
#		foreach( keys %maxsize_by_key ) { $maxsize_by_key{$_}++; }
#
#		# RUN THE REPORT
#		my($rowid)=0;
#		foreach my $item (sort keys %$thehashref) {
#			my(%href) = %{$$thehashref{$item}};
#			if( $rowid==0 ) {
#				foreach (@keys) {
#					my($fmt)="%-".$maxsize_by_key{$_}.".".$maxsize_by_key{$_}."s";
#					$textObject->insert('end',sprintf($fmt, ucfirst($_)), "t_orange");
#				}
#				$textObject->insert('end',"\n");
#			}
#			foreach (@keys) {
#				my($fmt)="%-".$maxsize_by_key{$_}.".".$maxsize_by_key{$_}."s";
#				if( $_ eq "key" ) {
#					$textObject->insert('end',sprintf($fmt, $item), "t_blue");
#				} else {
#					$textObject->insert('end',sprintf($fmt, $href{$_}), "t_blue");
#				}
#			}
#			$textObject->insert('end',"\n");
#			$rowid++;
#		}
#	#} elsif( $reportname ne "Disk/Storage Layout" ) {
#	#} elsif( $reportname ne "Database Layout" ) {
#	} else {
#		foreach (sort keys %$thehashref) {
#			next if /^Configuration/ and $reportname ne "Configuration";
#			next if /^DeviceSize/ and $reportname ne "Disk/Storage Layout";
#			next if /^Database_Info/ and $reportname ne "Database Layout";
#
#			if( ref $$thehashref{$_} eq "HASH" ) {
#				$textObject->insert('end',sprintf("%20.20s:\n", $_), $font);
#				my(%href) = %{$$thehashref{$_}};
#				foreach( keys %href ) {
#					if( ref $href{$_} ) {
#						$textObject->insert('end',sprintf("         %20.20s: ", $_), "t_orange");
#						my($str);
#						my(%href2)=%{$href{$_}};
#						foreach( keys %href2 ) {
#							$str.="$_=>$href2{$_} ";
#						}
#						$textObject->insert('end', $str.":\n", "t_blue");
#					} else {
#						$textObject->insert('end',sprintf("         %14.14s: %s\n", $_, $href{$_}), "t_orange");
#					}
#				}
#			} else {
#				my($val)=$$thehashref{$_};
#				$val=do_time(-fmt=>'yyyy/mm/dd hh:mm',-time=>$val)
#					if /_time$/;
#				$val=~s/\.0+$//;
#				$textObject->insert('end',sprintf("%20.20s: %s\n", $_, $val), $font);
#			}
#		}
#	}
#}

#sub draw_a_tab {
#	my($package,$tab)=@_;
#	die "draw_a_tab() WOAH.... undefined info for tab $tab\n" if ! defined $tabinfo{$tab};
#
#	my(%inforef)=%{$tabinfo{$tab}};
#	$package->statusmsg( "[servers] draw_a_tab($tab)\n" );
#	if( $inforef{query} eq "" ) {
#		if( $inforef{report_name} ne "NO" ) {
#			#if( $inforef{report_name} eq "YES" ) {
#				#foreach ( keys %inforef ) { print "INFO $_ = $inforef{$_}\n"; }
#				my($t);
#				if( $inforef{report_name} eq "Server Summary" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-name=>$inforef{servername});
#     				} elsif( $inforef{report_name} eq "Configuration" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-name=>$inforef{servername},
#     						-arg=>'Configuration');
#     				} elsif( $inforef{report_name} eq "Database Layout" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-name=>$inforef{servername},
#     						-arg=>'Database_Info');
#     				} elsif( $inforef{report_name} eq "Disk/Storage Layout" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-arg=>'DeviceSize',
#     						-name=>$inforef{servername});
#     				} else {
#     					foreach ( keys %inforef ) { $package->statusmsg( "[servers]  INFO $_ = $inforef{$_}\n" ); }
#     					die "No Report Found $inforef{report_name}\n";
#     				}
#
#
#				$package->show_that_hash($t,
#					${$inforef{textObject}},
#					"Running $inforef{report_name} Report For $inforef{servername} At ".localtime(time)."\n\n",
#					1,$inforef{report_name});
#				${$inforef{textVariable}}="Completed: Report $inforef{report_name} on ". $inforef{servername}."/".$inforef{servertype}."/".$inforef{database};
#				$package->statusmsg(  "[servers] Completed report $inforef{report_name} for server $inforef{servername}\n" );
#			#}
#		}
#		return;
#	}
#	my($textObject)		= ${$inforef{textObject}};
#	if( $inforef{servertype} ne "sqlsvr" and $inforef{servertype} ne "sybase" ) {
#		$textObject->insert('end',"Only sqlsvr and sybase currently supported\n", "t_hdr");
#		$textObject->insert('end',"No Data Found For Report\n", "t_hdr");
#		return;
#	}
#	${$inforef{textVariable}}="Fetching Results for ".$inforef{query}." on ". $inforef{servername}."/".$inforef{servertype}."/".$inforef{database};
#
#	#$inforef{window}->Label( -text=>"Insert Results of $inforef{query} on $inforef{servername}/$inforef{servertype}/$inforef{database}",
#	#	-relief=>'ridge' , -bg=>'red' )->pack( -side=>'top', -fill=>'x' );
#
#	# run the query
#	my($connectionmanager)=$$package{-connectionmanager};
#	#$package->statusmsg(  "[servers] CONNECTING TO $inforef{servername} TYPE as $inforef{servertype} \n" );
#	my($c,$errmsg)=$connectionmanager->connect(-name=>$inforef{servername}, -type=>$inforef{servertype} );
#	main::colorize_tree($inforef{servername});
#	if( ! $c ) {
#		${$inforef{textVariable}}="[servers] unable to connect to $inforef{servername}";
#		$package->statusmsg("[servers] unable to connect to $inforef{servername}\n");
#		$package->statusmsg( "[servers]  error: unable to connect to $inforef{servername}\n" );
#		return;
#	}
#
#	$package->statusmsg("[servers] running $inforef{query} on $inforef{servername}\n");
#	my($q_starttime)=time;
#	$textObject->delete("1.0","end");
#
#	foreach ( split(/\n/,$inforef{query}) ) {
#            	my(@rc) = $connectionmanager->query(
#            		-query=>$_,
#            		#-debug=>4,
#            		-connection=>$c,
#            		-db=>$inforef{database},
#            		-print_hdr=>1 );
#            	$package->statusmsg("[servers] query completed : ".($#rc+1)." rows returned\n");
#
#            	if( defined $inforef{in_each_db_query} ) {
#            		# db list
#            		foreach ( split( /\s+/, $package->global_data(	-class=>$inforef{servertype}, -name=>$inforef{servername}, -arg=>"Database_String") )) {
#            			print "RUNNING $inforef{in_each_db_query} IN DB $_!!!\n";
#            			push @rc, $connectionmanager->query(
#            				-query=>$inforef{in_each_db_query},
#            				-connection=>$c,
#            				-db=>$_	);
#            		}
#
#        	}
#        	my( @results ) = dbi_reformat_results(@rc);
#		my($font)="t_hdr";
#		foreach (@results) {
#			$_=uc($_) if $font eq "t_hdr";
#			$textObject->insert('end',$_."\n", $font);
#			$font="t_blue";
#		}
#		$textObject->insert('end',"\n", $font);
#	}
#
#	${$inforef{textVariable}}="Completed: Results for ".$inforef{query}." on ". $inforef{servername}."/".$inforef{servertype}."/".$inforef{database};
#	$package->statusmsg(  "[servers] Completed query on $inforef{servername}\n" );
#}

sub new_draw_a_tab {
	my($package,$tab)=@_;

	$package->statusmsg("[servers] drawing tab $tab srvr=$tabinfo{servername} type=$tabinfo{servertype} db=$tabinfo{database}\n" );
	die "draw_a_tab() WOAH.... undefined info for tab $tab\n" if ! defined $tabinfo{$tab};
	my(%info)=  %{$tabinfo{$tab}};
	${$info{label_variable}}="Report $tab: server=$tabinfo{servername} type=$tabinfo{servertype} db=$tabinfo{database}";
	my($gd)=$main::gem_data;

	my(%args);
	$args{-servertype} = $tabinfo{servertype};
	$args{-servername} = $tabinfo{servername} 	|| "";
	$args{-database	 } = $tabinfo{database}		|| "";

	$gd->run_reports(
		-type=>	      $tabinfo{screentype},
		-report =>    $tab,
		-textObject=> ${$info{results_obj}},
		-label_variable=> ${$info{label_variable}},
		%args
	);
	return;
#
#	my(%inforef)=%{$tabinfo{$tab}};
#	$package->statusmsg( "[servers] draw_a_tab($tab)\n" );
#	if( $inforef{query} eq "" ) {
#		if( $inforef{report_name} ne "NO" ) {
#			#if( $inforef{report_name} eq "YES" ) {
#				#foreach ( keys %inforef ) { print "INFO $_ = $inforef{$_}\n"; }
#				my($t);
#				if( $inforef{report_name} eq "Server Summary" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-name=>$inforef{servername});
#     				} elsif( $inforef{report_name} eq "Configuration" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-name=>$inforef{servername},
#     						-arg=>'Configuration');
#     				} elsif( $inforef{report_name} eq "Database Layout" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-name=>$inforef{servername},
#     						-arg=>'Database_Info');
#     				} elsif( $inforef{report_name} eq "Disk/Storage Layout" ) {
#     					$t = $package->global_data( -class=>$inforef{servertype},
#     						-arg=>'DeviceSize',
#     						-name=>$inforef{servername});
#     				} else {
#     					foreach ( keys %inforef ) { $package->statusmsg( "[servers]  INFO $_ = $inforef{$_}\n" ); }
#     					die "No Report Found $inforef{report_name}\n";
#     				}
#
#
#				$package->show_that_hash($t,
#					${$inforef{textObject}},
#					"Running $inforef{report_name} Report For $inforef{servername} At ".localtime(time)."\n\n",
#					1,$inforef{report_name});
#				${$inforef{textVariable}}="Completed: Report $inforef{report_name} on ". $inforef{servername}."/".$inforef{servertype}."/".$inforef{database};
#				$package->statusmsg(  "[servers] Completed report $inforef{report_name} for server $inforef{servername}\n" );
#			#}
#		}
#		return;
#	}
#	my($textObject)		= ${$inforef{textObject}};
#	if( $inforef{servertype} ne "sqlsvr" and $inforef{servertype} ne "sybase" ) {
#		$textObject->insert('end',"Only sqlsvr and sybase currently supported\n", "t_hdr");
#		$textObject->insert('end',"No Data Found For Report\n", "t_hdr");
#		return;
#	}
#	${$inforef{textVariable}}="Fetching Results for ".$inforef{query}." on ". $inforef{servername}."/".$inforef{servertype}."/".$inforef{database};
#
#	#$inforef{window}->Label( -text=>"Insert Results of $inforef{query} on $inforef{servername}/$inforef{servertype}/$inforef{database}",
#	#	-relief=>'ridge' , -bg=>'red' )->pack( -side=>'top', -fill=>'x' );
#
#	# run the query
#	my($connectionmanager)=$$package{-connectionmanager};
#	#$package->statusmsg(  "[servers] CONNECTING TO $inforef{servername} TYPE as $inforef{servertype} \n" );
#	my($c,$errmsg)=$connectionmanager->connect(-name=>$inforef{servername}, -type=>$inforef{servertype} );
#	main::colorize_tree($inforef{servername});
#	if( ! $c ) {
#		${$inforef{textVariable}}="[servers] unable to connect to $inforef{servername}";
#		$package->statusmsg("[servers] unable to connect to $inforef{servername}\n");
#		$package->statusmsg( "[servers]  error: unable to connect to $inforef{servername}\n" );
#		return;
#	}
#
#	$package->statusmsg("[servers] running $inforef{query} on $inforef{servername}\n");
#	my($q_starttime)=time;
#	$textObject->delete("1.0","end");
#
#	foreach ( split(/\n/,$inforef{query}) ) {
#            	my(@rc) = $connectionmanager->query(
#            		-query=>$_,
#            		#-debug=>4,
#            		-connection=>$c,
#            		-db=>$inforef{database},
#            		-print_hdr=>1 );
#            	$package->statusmsg("[servers] query completed : ".($#rc+1)." rows returned\n");
#
#            	if( defined $inforef{in_each_db_query} ) {
#            		# db list
#            		foreach ( split( /\s+/, $package->global_data(	-class=>$inforef{servertype}, -name=>$inforef{servername}, -arg=>"Database_String") )) {
#            			print "RUNNING $inforef{in_each_db_query} IN DB $_!!!\n";
#            			push @rc, $connectionmanager->query(
#            				-query=>$inforef{in_each_db_query},
#            				-connection=>$c,
#            				-db=>$_	);
#            		}
#
#        	}
#        	my( @results ) = dbi_reformat_results(@rc);
#		my($font)="t_hdr";
#		foreach (@results) {
#			$_=uc($_) if $font eq "t_hdr";
#			$textObject->insert('end',$_."\n", $font);
#			$font="t_blue";
#		}
#		$textObject->insert('end',"\n", $font);
#	}
#
#	${$inforef{textVariable}}="Completed: Results for ".$inforef{query}." on ". $inforef{servername}."/".$inforef{servertype}."/".$inforef{database};
#	$package->statusmsg(  "[servers] Completed query on $inforef{servername}\n" );
}

#sub create_a_tab {
#	my($package,$tab)=@_;
#
#	if( $has_been_painted{$tab} ) {
#		$package->statusmsg("[Servers] create_a_tab() tab $tab has been painted\n");
#		return;
#	}
#
#	$package->statusmsg( "[servers] create_a_tab($tab) cscrn=$current_screen, ctab=$current_tab)\n" );
#	$has_been_painted{$tab}=1;
#	$package->draw_a_tab($tab);
#
#	return 1;
#}
#
#sub raise_a_tab {
#	my($package,$tab)=@_;
#	if( $has_been_painted{$tab} ) {
#		$package->statusmsg("[Servers] raise_a_tab() (tab $tab allready painted)\n");
#		return;
#	}
#
#	$package->statusmsg( "[servers]   raise_a_tab() TAB $tab Has Been raised - $current_screen, $current_tab\n" );
#	$has_been_painted{$tab}=1;
#	$package->draw_a_tab($tab);
#	return 1;
#}

sub new_create_a_tab {
	my($package,$tab)=@_;

	if( $has_been_painted{$tab} ) {
		#$package->statusmsg("[Servers] create_a_tab() tab $tab has been painted\n");
		return 1;
	}
	$has_been_painted{$tab}=1;

	$package->statusmsg( "[servers] create_a_tab($tab) screen=$current_screen, tab=$current_tab)\n" );
	$package->new_draw_a_tab($tab);

	return 1;
}

sub properties_by_type {
	my($package,$type)=@_;
	$package->statusmsg( "[servers] properties_by_type(type=$type)\n" );
	$type=$toi{$type} if defined $toi{$type};

	my($frame)=$package->clear_tab($std_page_name);

	$frame->Label( -text=>$type_labels{$type}." Aggregate Screen",
		-relief=>'ridge' , -bg=>'white' )->pack( -side=>'top', -fill=>'x' );

	my($server_nb) 	  = $frame->NoteBook()->pack(-side=>'bottom',-fill=>'both',-expand=>1, -anchor=>'n');

	$current_screen="bytype";
	$current_tab="Summary";
	%has_been_painted=();
	%tabinfo=();
	#my($tab1) = $package->create_a_page($server_nb,'Registration', undef,$type,undef,"","NO");
     	#my($tab2) = $package->create_a_page($server_nb,'Procedure Library', 	undef,$type,undef,"","NO");
     	#my($tab3) = $package->create_a_page($server_nb,'Files and Directories',undef,$type,undef,"","NO");
     	#my($tab5) = $package->create_a_page($server_nb,'Surveys Summary', 		undef,$type,undef,"","NO");

	my($tab1)= $server_nb->add('Summary',-label=>'Registration',-underline=>0);
	my($tab2)= $server_nb->add('Procedure Library',-label=>'Procedure Library',-underline=>0);
	my($tab3)= $server_nb->add('Files and Directories',-label=>'Files and Directories',-underline=>0);
	my($tab5)= $server_nb->add('Survey',-label=>'Surveys Summary',-underline=>0);


	%main::procs_frame_sv=();	# clear the saved stuff or we may end up in trouble
	main::ServerTab_draw( 	$tab1, $type );
	main::configuretab_draw_mainarea(  $tab2,$type,"proc" );
	main::filetab_create( 	$tab3 );
	main::configuretab_draw_mainarea(  $tab5, $type,"survey" );

	return $server_nb;
}

sub properties_by_database
{
	my($package,$frame,$stype,$servername,$database)=@_;
	$package->statusmsg( "[servers] properties_by_database(server=$servername,type=$stype,db=$database)\n" );
	#my($frame)=$package->clear_tab($std_page_name);
	$stype=$toi{$stype} if defined $toi{$stype};

	$frame->Label( -text=>"Showing Properties for Server $servername Database $database",
		-relief=>'ridge' , -bg=>'white' )->pack( -side=>'top', -fill=>'x' );

	my %event = $package->get_last_event_info();
	my($server_nb) 	  = $frame->NoteBook()->pack(-side=>'bottom',-fill=>'both',-expand=>1, -anchor=>'n');
	$current_screen="bydatabase";
	$current_tab="Summary";
	%has_been_painted=();
	%tabinfo=();
	$tabinfo{servertype}=$stype;
	$tabinfo{servername}=$servername;
	$tabinfo{database}=$database;
	$tabinfo{screentype}="database";

	my($gd)=$main::gem_data;
        foreach my $tabname ( $gd->get_reports(-type=>"database") ) {
        	my($rc)= $server_nb->add($tabname,
        		-label=>ucfirst($tabname),
        		-underline=>0,
        		-createcmd=>sub {
        			#print "DBG: $tabname - create \n";
        			$package->new_create_a_tab($tabname);
        		}
        	);

        	my($textVariable,$textObject);
        	$textVariable="Insert Results of Report $tabname on $servername/$stype/$database";
        	$rc->Label( -textvariable=>\$textVariable,
        			-relief=>'ridge' , -bg=>'red' )->pack( -side=>'top', -fill=>'x' );

        	$textObject = $rc->Scrolled("ROText",
        			-relief => "sunken", -wrap=>'none',
                        	-bg=>'white',-font=>'code',
                        	-scrollbars => "osoe")->pack(-side   => "top",
                                                        -anchor => 'w',
                                                        -expand => 1,
                                                         -fill   => 'both');

                $textObject->tagConfigure("t_hdr",    -foreground=>'black', -relief=>'raised', -background=>'beige');
                $textObject->tagConfigure("t_blue",   -foreground=>'blue');
                $textObject->tagConfigure("t_red",    -foreground=>'red');
                $textObject->tagConfigure("t_orange", -foreground=>'orange');

        	my(%sv_dat);
                $sv_dat{label_variable}=\$textVariable;
	        $sv_dat{results_obj}=\$textObject;
	        $tabinfo{$tabname}=\%sv_dat;
        }

        my($subtab)= $server_nb->add("DDL",
        		-label=>"Objects",
        		-underline=>0
        	);
        my($subtab_nb) 	  = $subtab->NoteBook()->pack(-side=>'bottom',-fill=>'both',-expand=>1, -anchor=>'n');

	foreach my $tabname ( $gd->get_reports(-type=>"objreports") ) {
        	my($rc)= $subtab_nb->add($tabname,
        		-label=>ucfirst($tabname),
        		-underline=>0,
        		-createcmd=>sub {
        			#print "DBG: $tabname - create \n";
        			$package->new_create_a_tab($tabname);
        		}
        	);

        	my($textVariable,$textObject);
        	$textVariable="Insert Results of Report $tabname on $servername/$stype/$database";
        	$rc->Label( -textvariable=>\$textVariable,
        			-relief=>'ridge' , -bg=>'red' )->pack( -side=>'top', -fill=>'x' );

        	$textObject = $rc->Scrolled("ROText",
        			-relief => "sunken", -wrap=>'none',
                        	-bg=>'white',-font=>'code',
                        	-scrollbars => "osoe")->pack(-side   => "top",
                                                        -anchor => 'w',
                                                        -expand => 1,
                                                         -fill   => 'both');

                $textObject->tagConfigure("t_hdr",    -foreground=>'black', -relief=>'raised', -background=>'beige');
                $textObject->tagConfigure("t_blue",   -foreground=>'blue');
                $textObject->tagConfigure("t_red",    -foreground=>'red');
                $textObject->tagConfigure("t_orange", -foreground=>'orange');

        	my(%sv_dat);
                $sv_dat{label_variable}=\$textVariable;
	        $sv_dat{results_obj}=\$textObject;
	        $tabinfo{$tabname}=\%sv_dat;
        }

#	my($tab1) = $package->create_a_page($server_nb,'Summary', 	$servername,$stype,$database,"sp_helpdb \"$database\"","NO");
#     	my($tab2) = $package->create_a_page($server_nb,'Operations', 	$servername,$stype,$database,"","NO");
#     	my($tab4) = $package->create_a_page($server_nb,'Users',		$servername,$stype,$database,"sp__helpuser","NO");
#     	my($tab5) = $package->create_a_page($server_nb,'Tables', 	$servername,$stype,$database,"sp__helptable","NO");
#
#     	my($tab6) = $package->create_a_page($server_nb,'Index', 	$servername,$stype,$database,"sp__helpindex","NO");
#     	my($tab7) = $package->create_a_page($server_nb,'Procedures',	$servername,$stype,$database,"sp__helpproc","NO");
#     	my($tab8) = $package->create_a_page($server_nb,'Triggers', 	$servername,$stype,$database,"sp__trigger","NO");
#     	my($tab9) = $package->create_a_page($server_nb,'Rules', 	$servername,$stype,$database,"sp__helprule","NO");
#     	my($tab9) = $package->create_a_page($server_nb,'Type', 		$servername,$stype,$database,"sp__helptype","NO");
#     	my($tab9) = $package->create_a_page($server_nb,'Dflt', 		$servername,$stype,$database,"sp__helpdefault","NO");
#     	my($tab0) = $package->create_a_page($server_nb,'Permissions',	$servername,$stype,$database,"sp__helprotect","NO");
#     	my($tabB) = $package->create_a_page($server_nb,'Depends', 	$servername,$stype,$database,"sp__depends","NO");
#	my($tabB) = $package->create_a_page($server_nb,'Audit', 	$servername,$stype,$database,"sp__noindex\nsp__colconflict\nsp__colnull","NO");

	return $server_nb;
}

sub properties_by_server
{
	my($package,$frame,$servername,$stype)=@_;
	$package->statusmsg( "[servers] properties_by_server(server=$servername,type=$stype)\n" );
	$stype=$toi{$stype} if defined $toi{$stype};


	$frame->Label( -text=>"Showing Properties for Server $servername",
		-relief=>'ridge' , -bg=>'white' )->pack( -side=>'top', -fill=>'x' );

	my($server_nb) 	  = $frame->NoteBook()->pack(-side=>'bottom',-fill=>'both',-expand=>1, -anchor=>'n');
	$current_screen="byserver";
	$current_tab="Summary";
	%has_been_painted=();
	%tabinfo=();

#	my %event = $package->get_last_event_info();
	$tabinfo{servertype}=$stype;
	$tabinfo{servername}=$servername;
	$tabinfo{database}="";
	$tabinfo{screentype}="server";

	my($gd)=$main::gem_data;
        foreach my $tabname ( $gd->get_reports(-type=>"server") ) {
        	my($rc)= $server_nb->add($tabname,
        		-label=>ucfirst($tabname),
        		-underline=>0,
        		-createcmd=>sub {
        			$package->new_create_a_tab($tabname);
        		}
        	);

        	my($textVariable,$textObject);
        	$textVariable="Insert Results of Report $tabname on $servername/$stype";
        	$rc->Label( -textvariable=>\$textVariable,
        			-relief=>'ridge' , -bg=>'red' )->pack( -side=>'top', -fill=>'x' );

        	$textObject = $rc->Scrolled("ROText",
        			-relief => "sunken", -wrap=>'none',
                        	-bg=>'white',-font=>'code',
                        	-scrollbars => "osoe")->pack(-side   => "top",
                                                        -anchor => 'w',
                                                        -expand => 1,
                                                         -fill   => 'both');

                $textObject->tagConfigure("t_hdr",    -foreground=>'black', -relief=>'raised', -background=>'beige');
                $textObject->tagConfigure("t_blue",   -foreground=>'blue');
                $textObject->tagConfigure("t_red",    -foreground=>'red');
                $textObject->tagConfigure("t_orange", -foreground=>'orange');

        	my(%sv_dat);
                $sv_dat{label_variable}=\$textVariable;
	        $sv_dat{results_obj}=\$textObject;
	        $tabinfo{$tabname}=\%sv_dat;
        }

#	$package->create_a_page($server_nb,'Summary', 	$servername,$stype,undef,"","Server Summary");
#     	$package->create_a_page($server_nb,'Configuration',$servername,$stype,undef,"","Configuration");
#     	$package->create_a_page($server_nb,'Database',	$servername,$stype,undef,"","Database Layout");
#     	$package->create_a_page($server_nb,'Vdevs',	$servername,$stype,undef,"sp__vdevno","NO");
#     	$package->create_a_page($server_nb,'Storage',	$servername,$stype,undef,"","Disk/Storage Layout");
#     	$package->create_a_page($server_nb,'Audit', 	$servername,$stype,undef,
#     		"sp__auditsecurity \@srvname='".$servername."'","NO","sp__auditdb \@srvname='".$servername."'");
#     	$package->create_a_page($server_nb,'Space', 	$servername,$stype,undef,"sp__dbspace","NO","sp__dbspace");
#     	$package->create_a_page($server_nb,'Layout#1', 	$servername,$stype,undef,"sp__helpdbdev \@fmt=\"ND\", \@dont_format=\"Y\"","NO");
#     	$package->create_a_page($server_nb,'Layout#2', 	$servername,$stype,undef,"sp__helpdbdev \@fmt=\"P\", \@dont_format=\"Y\"","NO");
#     	$package->create_a_page($server_nb,'Logins', 	$servername,$stype,undef,"sp__helplogin \@dont_format='Y'","NO");
#     	#$package->create_a_page($server_nb,'Overview', 	$servername,$stype,undef,"sp__server","NO");
#     	$package->create_a_page($server_nb,'RevDb', 	$servername,$stype,undef,"sp__revdb","NO");
#    	$package->create_a_page($server_nb,'RevDev', 	$servername,$stype,undef,"sp__revdevice","NO");

	return $server_nb;

#	# this is the repository stuff
#	my( $dathash ) = $main::server_info{$servername.":".$stype};
#	if( ! defined $dathash ) {
#		print "WARNING: Server $servername of type $stype Has been Deleted\n";
#		return;
#	}
#	my(%dat) = %$dathash;
#	print "------------------- REPOSITORY -------------------\n";
#	foreach (sort keys %$dathash ) { print ">>> $_ => $$dathash{$_} \n"; 	}
#
#	# this is the normal stuff
#	my($t) = $package->global_data( -class=>$stype,-name=>$servername);
#	print "------------------- GLOBAL DATA -------------------\n";
#	if( ! defined $t ) {
#		print "Global Data Not Found\n";
#	} else {
#		foreach (sort keys %$t ) { print ">>> $_ => $$t{$_} \n"; }
#	}

	#my( @items ) = sort keys %dat;
	#my $numrows = scalar @items;
	#my($table)= $frame->Table( -rows=>$numrows+1,-columns=> 4,-scrollbars => 'se' );
	# Print Headers
 	# $table->put(0,0,"Key");
 	# $table->put(0,1,"Value");
 	# my($row)=1;
 	# foreach ( @items ) {
	#	my($v)=$dat{$_};
	#	next if /^password/i;
	#	$v=~s/\s+/\n/g;
	#			-widget=>$table,
	#			-text=>"Modify $_",
	#			-help=>"Modify Column $_ Details for $servername",
	#			-func=>sub { $package->show("modify_svrtype",$servername, $working_treetype); });
	#	$table->put($row,0,$_);
	#	$table->put($row,1,$v);
	#	$table->put($row,2,$b);
	#	$row++;
 	# }
 	# $table->pack(-side=>'left');#,-expand=>1,-fill=>'both');
}

1;

__END__

=head1 NAME

Servers.pm -  Servers Manager Plugin

=head2 DESCRIPTION

This plugin manages servers.  It is a system plugin and has a
ton of oddities.  It probably can be viewed as one of my messiest
pieces of code... yech... Apologies.  That is what happens when core functionality
is recoded 10 times
using different algorithms.  At the point im writing this - there
are more commented out codelines than there are runnable lines.

Ill fix it... so here goes... a spec... yay...

=head2 INTERFACE ELEMENTS

Explorer Buttons created in english per server type.  Selection of button will
put you to the Properties tab if you are on welcome tab.

Selection of an Explorer Button draws the Explorer Section which will give a list of
the servers of that type.  The idea is that browse shows you reports while rtclick
executes operations. The Heirarchy is /Type/ServerName/Database.

Menu Items Include:
	Configuration/Register/{ServerType}
	Configuration/Files
	Configuration/Systems
	SQL Server/Stored Procedures
	Sybase/Stored Procedures
	Configuration/Sybase Stored Procedures

Explorer Items
	/
		browse:
		rtclick:
	/Type
		browse:
	 		Table Of Servers

		rtclick:
			Survey All
			Connect All
			Files and Directories
			Monitor?
	/Type/Server/
		browse:
	 		Properties Notebook
	 			Summary Tab
				Device Layout
				Database Layout
				Server Logins

		rtclick:
			Diagnose
			Connect
			Disconnect
			Change Password
			Deregister
			Install/Upgrade Proc Library
			Object Explorer
 			Survey
			Files and Directories	(if unix)
			Monitor
			Maintenenace
				Plans Definition
				Execute Plan
				Backup a database
				Restore a database
				Update Statistics
				Create Audit Report
				Reorg Rebuild
			Survey
	 		Properties	[ see browse ]

	/Type/Server/Database/{Database}
		browse:
			Summary Tab
			files/usage
			ddl
			users
			Database Options
			Permissions
		rtclick:
			"Diagnose",
			"Import/Export Data",
			"Maintenance Plans",
			"Backup Operations",
			"Explorer DDL Objects",
			"Shrink Database",
			"Detach Database",
			"Database Options",
			"Properties"
			Maintenenace
				Plans Definition
				Execute Plan
				Backup a database
				Restore a database
				Update Statistics
				Create Audit Report
				Reorg Rebuild



These items call generic Screens as per the erver, file, and configure tabs that are
shared with the setup program.  These screens have only core components - the nav
buttons are not shown.

Operations You can Perform are:
	Connect To All Servers Of A Type
	Connect To One Server
	Survey One
	Survey By Type
	Install Procedures on One Server


