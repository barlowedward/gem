-- report_key=iosummary
-- report_type=PERFORMANCE
-- run_frequency=15Min
-- do_diff=Y
-- keep_hours=Y
-- keep_6=Y
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=This Report Lists Io Summaries
-- report_footer=
-- enabled=Y
-- performancetype=int

select @@SERVERNAME, db_name(database_id),	'','',
	sum(num_of_bytes_read),
	sum(num_of_bytes_written),
	sum(io_stall_read_ms )
	from sys.dm_io_virtual_file_stats( null ,null )
	group by db_name(database_id)


