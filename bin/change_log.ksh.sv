echo 
cwd=`pwd`
echo Change Log For $cwd
echo 
date=`date "+%Y%m%d.%T"`
date_readable=`date "+%D %T"`

user=$USER
[ -z "$user" ] && user=$USERNAME

ans1=.
ans2=.
ans3=.
ans4=.
ans5=.

echo "ENTER NOTES "
echo "  - End Data Entry by entering a single '.'"
echo "  - Thats a period followed by a newline, ie. '.<enter>'"
echo ""
read ans1
[ "$ans1" = "." ] && OK=1 
if [ -n "$OK" ] 
then
echo single period entered at first line - no data entry - aborting
exit
fi
[ -z "$OK" ] && read ans2
[ "$ans2" = "." ] && OK=1 
[ -z "$OK" ] && read ans3
[ "$ans3" = "." ] && OK=1 
[ -z "$OK" ] && read ans4
[ "$ans4" = "." ] && OK=1 
[ -z "$OK" ] && read ans5
[ "$ans5" = "." ] && OK=1 
[ -z "$OK" ] && read ans6

touch CHANGE_LOG
chmod 770 CHANGE_LOG
mv CHANGE_LOG CHANGE_LOG.sv
echo "$date_readable    *** New Change Log Entry"						> CHANGE_LOG
#echo "   date  $date"								>> CHANGE_LOG
echo "   by    $username"							>> CHANGE_LOG
echo "   cwd   $cwd"									>> CHANGE_LOG
[ "$ans1" != "." ] && echo "   note1 $ans1" 	>> CHANGE_LOG
[ "$ans2" != "." ] && echo "   note2 $ans2"	>> CHANGE_LOG
[ "$ans3" != "." ] && echo "   note3 $ans3"	>> CHANGE_LOG
[ "$ans4" != "." ] && echo "   note4 $ans4"	>> CHANGE_LOG
[ "$ans5" != "." ] && echo "   note5 $ans5"	>> CHANGE_LOG
cat 	CHANGE_LOG.sv >> CHANGE_LOG
rm 	CHANGE_LOG.sv

echo "*** SUCCESSFUL ENTRY TO CHANGE LOG"
echo "   date  date_readable"
echo "   by    $username"
echo "   cwd   $cwd"
