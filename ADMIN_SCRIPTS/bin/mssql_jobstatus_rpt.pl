#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use Do_Time;
use MlpAlarm;
use Getopt::Long;
use DBIFunc;
use CommonFunc;
use Repository;

use vars qw( $SYSTEMS $STDALARM $NOSTDOUT $DEBUG $HTML $OUTFILE);

my($VERSION)=1.0;

$|=1;

sub usage
{
	return "Usage: mssql_jobstatus_rpt.pl --HTML --NOSTDOUT --STDALARM --OUTFILE --DEBUG --SYSTEMS=system[,system] \n";
}

#die usage() if $#ARGV<0;
die "Bad Parameter List $!\n".usage() unless
   GetOptions(  "STDALARM"	=> \$STDALARM,
		"SYSTEMS=s"	=> \$SYSTEMS,
		"OUTFILE=s"	=> \$OUTFILE,
		"DEBUG"		=> \$DEBUG,
		"NOSTDOUT"	=> \$NOSTDOUT,
		"HTML"		=> \$HTML
		);

sub error_out {
   my($msg)=join("",@_);

   $msg = "BATCH MssqlJobStatusRpt: ".$msg;

   MlpBatchJobEnd(-EXIT_CODE=>"ERROR", -EXIT_NOTES=>$msg);

   MlpEvent(
      -monitor_program=> "MssqlJobStatusRpt",
      -system=> "",
      -message_text=> $msg,
      -severity => "ERROR"
   );
   die $msg;
}


cd_home();
my(@SRV_LIST);
if( $SYSTEMS eq "ALL" or ! defined $SYSTEMS ) {
	@SRV_LIST=get_password(-type=>'sqlsvr');
} else {
	@SRV_LIST=split(/,/,$SYSTEMS);
}
error_out( "No Systems Defined" ) if $#SRV_LIST<0;
MlpBatchJobStart(-BATCH_ID => "MssqlJobStatusRpt");
my($nl,$cell,$hcell)=("\n","","");
$nl	="<p>\n" 				if $HTML;
$cell	="</TD><TD>" 		if $HTML;
$hcell	="</TH><TH>" 	if $HTML;

print "MssqlJobStatusRpt v1.0: run at ".localtime(time).$nl;

unlink $OUTFILE 			if defined $OUTFILE and -w $OUTFILE;
if( defined $OUTFILE ) {
	open(OUT,">".$OUTFILE) 	or die "Cant write $OUTFILE: $!\n";
	print "OUTPUT file $OUTFILE\n";
	output( "MssqlJobStatusRpt v1.0: run at ".localtime(time).$nl );
} else {
	print "No Output File Passed - Output to standard output\n";
}

my(@ttt)=localtime(time);

my(%ok_times);
my($t)=do_time(-fmt=>"yyyymmdd");
$ok_times{ $t } =1;

if( $ttt[6]==0 or $ttt[6]==1 or $ttt[6]==6 ) {
	output( "Weekend Time Stamps Apply".$nl );
	$ok_times{ do_time(-fmt=>"yyyymmdd", -time=>(time-24*60*60)) } = 1;
	$ok_times{ do_time(-fmt=>"yyyymmdd", -time=>(time-2*24*60*60)) }=1;
}

sub output {
	if( $OUTFILE ) {
		print OUT @_;
		print ".";
		print @_ unless $NOSTDOUT;
	} else {
		print @_;
	}
}
output( "To remove rows from this report, disable the job on the appropriate server. $nl" );
output( "Run At ".(scalar localtime),$nl,$nl,"\n");
foreach (keys %ok_times) { print "OK Dates - $_\n"; }

my($login,$pass,$ctype,$srv);
my(@rc);
my(%server_start_row,%server_end_row);
output( "Fetching Job Details using sp_help_job\n" );
foreach $srv (sort @SRV_LIST) {
	($login,$pass,$ctype)=get_password(-type=>'sqlsvr',-name=>$srv);
	print "Working on server $srv - sp_help_job\n";
   my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
	if( ! $rcx ) {
   	output( "Cant connect to $srv as $login\n" );
		next;
	}

	my($query)="msdb..sp_help_job \@enabled=1";
	my($db)="msdb";
	$server_start_row{$srv}=$#rc+1;
   push @rc, dbi_query(-db=>$db,-query=>$query);	#,-debug=>$DEBUG);
	$server_end_row{$srv}=$#rc;
 	dbi_disconnect();
	print "Completed Work on Server $srv\n" if defined $DEBUG;
}

	#output( "Completed Work on All Servers\n" ) if defined $DEBUG;
 	my(@COLUMNS)=(2,19,20,22,23,21);

	my(@hdr) = ("Server","Job","Last Time","Next Time","Outcome");
	my(@outcomes)=("Failed","Succeeded","","Canceled","","Unknown");

	my(%serv_by_row);
	foreach my $srv ( keys %server_start_row ) {
		for ($server_start_row{$srv}...$server_end_row{$srv}) {
			$serv_by_row{$_}=$srv;
		}
	}
	my(@newrc);
	my($rowcount)=0;
   foreach (@rc) {
		my(@x)=dbi_decode_row($_);
		my(@newrow);
		# put the server in...

		# 0 = the servername
		push @newrow,$serv_by_row{$rowcount};
		print "---- Server $serv_by_row{$rowcount} \n" if defined $DEBUG;
		$rowcount++;
		foreach (@COLUMNS) { push @newrow,$x[$_]; }

		print "Input: ",join(",",@newrow),"\n" if defined $DEBUG;

		# 6 = outcome
		$newrow[6]=$outcomes[$newrow[6]];

		# 3 = unused
		$newrow[3]=int($newrow[3]/100);

		# 5 = unused
		$newrow[5]=int($newrow[5]/100);

		$newrow[3] = substr("    ",0,4-length($newrow[3])).$newrow[3];
		$newrow[5] = substr("    ",0,4-length($newrow[5])).$newrow[5];

		# 2 = last time
		my($date_ok) = 0;
		$date_ok=1 if defined $ok_times{$newrow[2]};
		$newrow[2]= sprintf("%d/%d/%d %d",
				substr($newrow[2],4,2) ,
				substr($newrow[2],6,2) ,
				substr($newrow[2],0,4) ,
				$newrow[3]);

		# 4 = next time
		if( $newrow[4] == 0 ) {
			$newrow[4]= "---";
		} else {
			$newrow[4]= sprintf("%d/%d/%d %d",
					substr($newrow[4],4,2) ,
					substr($newrow[4],6,2) ,
					substr($newrow[4],0,4) ,
					$newrow[5]);
		}

		print "Output: ",join(",",@newrow),"\n" if defined $DEBUG;
		push @newrc, dbi_encode_row(
			$newrow[0],
			$newrow[1],
			$newrow[2],
			$newrow[4],
			$newrow[6],
			$date_ok
			);
	}

	if( $HTML ) {
		output( "<TABLE BORDER=1><TR><TH>\n" );
		output( join($hcell,@hdr) );
		output( "</TH></TR>\n" );
		my(@normal_rows);

   	foreach (@newrc) {
			my(@x)=dbi_decode_row($_);
			my($date_ok)= pop @x;
			my($color)="";

			# mark red if ... not succeeded and if recent
			if( $x[4] ne "Succeeded" and $x[3] ne "---" ) {

				output( "<TR BGCOLOR=\"RED\" ><TD>" ) if $date_ok;
				output( "<TR BGCOLOR=\"PINK\" ><TD>" ) unless $date_ok;
				output( join("</TD>\n   <TD>",@x) );
				output( "</TD></TR>\n" );
			} else {
				push @normal_rows, "<TR><TD>". join("</TD>\n   <TD>",@x). "</TD></TR>\n";
			};
		}
		foreach (@normal_rows) { output $_; }
		output( "</TABLE>\n" );
	} else {
		unshift @newrc,dbi_encode_row(@hdr);
		if( defined $DEBUG) {
			foreach (@newrc) {
				print $_,"\n";
			}
		}
  		my(@finalrc)=dbi_reformat_results(@newrc);
  		foreach (@finalrc) {
			output( $_,"\n" );
		}
	}
print "Report Completed\n";
MlpBatchJobEnd();
exit(0);
__END__

=head1 NAME

mssql_jobstatus_rpt.pl - Print a job status report

=head2 DESCRIPTION

You may specify all systems from the pc config file or specifiy the systems
to use.

=head2 USAGE

Usage: mssql_jobstatus_rpt.pl --STDALARM --SYSTEMS=system[,system] --DOSHRINK

=head2 OPTIONS

 --SYSTEMS : comma separated list of systems.  Otherwise does em all

=cut

