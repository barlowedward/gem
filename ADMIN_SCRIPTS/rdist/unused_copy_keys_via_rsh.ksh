#!/bin/ksh

if [ -z "$1" ]
then
	echo "Usage $0 Hostname User"
	exit 1
fi

if [ -z "$2" ]
then
	echo "Usage $0 Hostname User"
	exit 1
fi

HOST=$1
USER=$2
set -x

# test connection
X=`rsh -l $USER $HOST echo "helloworld" 2>&1`
if [ "$X" == "permission denied" ]
then
	echo ERROR RSH RETURNED: $X
	exit
fi
rsh -l $USER $HOST mkdir .ssh 2>&1 | grep -v "Failed to make directory"

X=`rsh -l $USER $HOST pwd`
echo "DIRECTORY=$X"

echo "Setting Permissions to 777"
rsh -l $USER $HOST chmod 777 .ssh

echo "copying files"
cat authorized_keys2 | rsh -l $USER $HOST "cat > .ssh/authorized_keys2"
rsh -l $USER $HOST cp .ssh/authorized_keys2 .ssh/authorized_keys

echo "Setting Permissions back"
rsh -l $USER $HOST chmod 700 .ssh
rsh -l $USER $HOST chmod 644 .ssh/authorized_keys .ssh/authorized_keys2

echo "TESTING"
