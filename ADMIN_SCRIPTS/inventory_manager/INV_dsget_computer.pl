#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use Repository;
use DBIFunc;
use Sys::Hostname;
my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $BATCH_ID $DEBUG $RUNADFIND $NOEXEC $RUNADFINDONLY );

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID
   --RUNADFIND       [ run adfind ]
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --DEBUG
   --NOEXEC \n";
  return "\n";
}

$| =1;
#my($curdir)=dirname($0);
#chdir $curdir or die "Cant cd to $curdir: $!\n";

# die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"RUNADFIND"			=> \$RUNADFIND,
		"RUNADFINDONLY"	=> \$RUNADFINDONLY,
		"NOEXEC"				=> \$NOEXEC,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"			=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

#my($DSQUERY)='dsquery.exe';
#$DSQUERY="C:/windows/system32/dsquery.exe" if -x "C:/windows/system32/dsquery.exe";
#$DSQUERY="C:/winnt/system32/dsquery.exe" if -x "C:/winnt/system32/dsquery.exe";

#my($DSGET)='dsget.exe';
#$DSGET="C:/windows/system32/dsget.exe" if -x "C:/windows/system32/dsget.exe";
#$DSGET="C:/winnt/system32/dsget.exe" if -x "C:/winnt/system32/dsget.exe";
my($adoutfile);
if( $RUNADFIND or $RUNADFINDONLY ) {
	print "Running adfind to collect LDAP data for parsing\n";
	my($HOSTNAME)     = hostname();
	my($GEM_ROOT_DIR) = get_gem_root_dir();
	$adoutfile = "$GEM_ROOT_DIR/discovery/$HOSTNAME.adfind.txt";
	print "The Output file is $adoutfile\n";
	open(OUT,">".$adoutfile) or die "Cant Write $adoutfile $!\n";

	#my($cmd)="$GEM_ROOT_DIR/AdFind/adfind -b ou=AD_Servers,dc=AD,dc=MLP,dc=COM -f &(objectCategory=computer)";
	# &(objectClass=computer)";

	my($cmd)="$GEM_ROOT_DIR/AdFind/adfind -b ou=AD_Servers,dc=AD,dc=MLP,dc=COM -f \"(objectCategory=computer)\"";
	print "\n\nSTARTING OF AD INFO COMPLETED\n\n";
	print "Running Command: $cmd\n";
	open( CMD, "$cmd 2>&1 |" ) or die "Cant run Command $!\n";
	my($cnt)=0;
	while(<CMD>) {
		print OUT $_;
		print "." if $cnt++ % 25==0;
	}
	close(CMD);
	close(OUT);

	print "\n\nFETCH OF AD INFO COMPLETED\n\n";
	exit() if $RUNADFINDONLY;
}

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Credential Mgt' ) if $BATCH_ID;
my($connection)=MlpGetDbh();

my(@t);
my($x);
$x=localtime(time);
push @t,"Time Start: ".$x;
print "Loading The Inventory System From File\n";

my($rootdir)=get_gem_root_dir();
my($ddir)=$rootdir."/discovery";


# Get the Latest Discovery File
my($dfile)=$adoutfile;
if( ! $dfile ) {
	opendir(DIR,$ddir) or die("Cant open directory $ddir for reading : $!\n");
	my(@dirlist)=grep(!/^\./,readdir(DIR));
	closedir(DIR);
	foreach (@dirlist) {
		next unless /adfind.txt$/;
		if( $dfile ) {
			$dfile = $ddir."/".$_ if -M $dfile > -M $ddir."/".$_ ;
		} else {
			$dfile = $ddir."/".$_;
		}
	}
}
die "NO discovery File " unless $dfile;
print "Discovery File is $dfile\n";

my($t) = "ldap_computer";

my($q)="DELETE INV_system_attribute where system_type='ldap_computer'";
print $q,"\n" if $DEBUG or $NOEXEC;
MlpAlarm::_querynoresults($q,1) unless $NOEXEC;

$q="DELETE INV_system where system_type='ldap_computer'";
print $q,"\n" if $DEBUG or $NOEXEC;
MlpAlarm::_querynoresults($q,1) unless $NOEXEC;;

my(%FOUNDSYS);
my($currentdn);
my(%attributes);

open(DX,$dfile) or die "Cant Read $dfile $!\n";
while(<DX>) {

	next if /^Using server:/ or /^Directory:/;
	chomp;
	### print ">>> $_\n";

	if( /^\s*$/ ) {
		next unless $currentdn;
		my($n) = $attributes{name} || $attributes{cn};
		if( ! $n ) {
			use Data::Dumper;
			warn "Data Error: Skipping Dn as No name/cn found for dn = $currentdn\n";
			warn Dumper \%attributes;
			next;
		}
		print "Saving Attributes for $n\n";

		if( $FOUNDSYS{$n} ) {
			print "WARNING: $n IS DUPLICATED IN LDAP\n";
			%attributes=();
			next;
		}

		$FOUNDSYS{$n}=1;
		my($q)="insert INV_system (system_name,system_type ) values ("
					.dbi_quote(-connection=>$connection,-text=>$n).","
					.dbi_quote(-connection=>$connection,-text=>$t)." )";
		print $q if $DEBUG or $NOEXEC;
		MlpAlarm::_querynoresults($q,1) unless $NOEXEC;;

		$attributes{dn} = $currentdn;
		foreach my $k ( keys %attributes ) {
			my($v)=$attributes{$k};
			print "K=$k V=$v \n" if $DEBUG;
			next unless $v;
			my($q)="insert INV_system_attribute (attribute_name,system_name,system_type,attribute_value,mod_date,mod_by ) values ("
						.dbi_quote(-connection=>$connection,-text=>"ldap_".lc($k)).","
						.dbi_quote(-connection=>$connection,-text=>$n).","
						.dbi_quote(-connection=>$connection,-text=>$t).","
						.dbi_quote(-connection=>$connection,-text=>$v).",getdate(),'INV_dsget_computer.pl')";
			print $q if $DEBUG or $NOEXEC;
			MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
		}
		%attributes=();
		$currentdn='';
		next;
	} elsif( /^dn/ ) {
		$currentdn=$_;
	} elsif( /^>/ and /:/ ) {
		next if /^>objectClass:/ or /^>instanceType/ or /^>objectGUID/ or /^>userAccountControl/
			or /^>codePage/ or /^>primaryGroupID/ or /^>accountExpires/ or  /^>dS/ or /^>uSN/;
		my($k,$v)=split(/:/,$_,2);
		$v=~s/^\s*//;
		next if $v=~ /^\s*$/;
		$k=~s/^>//;
		### print "setting $k to $v\n";
		$attributes{$k} = $v;
	} elsif( /Objects returned/) {
		print "OK:",$_,"\n";
	} elsif( /joeware.net/) {		# skip copyright
	} else {
		warn "WOAH - RETURNED $_\n";
	}
}
close(DX);

$x=localtime(time);
push @t,"Done: ".$x;
print join("\n",@t),"\n";
print "Load Completed\n";
MlpBatchJobEnd() if $BATCH_ID;
exit(0);


## old trial codeline
#
#	my(@t);
#	my($x);
#
#	$x=localtime(time);
#	push @t,"Time Start: ".$x;
#	print "Loading The Inventory System From LDAP\n";
#
#	my(@samid);
#	my(%dn);
#	my(%rdn);
#
## if(0) {
#	my($cmd)="$DSQUERY computer -o samid -limit 0";
#	print "$cmd\n";
#	open(A,"$cmd |") or die "Cant run command $cmd\n$!\n";
#	while(<A>) {
#		chomp;
#		s/"//g;
#		push @samid,$_;
#	}
#	close(A);
#	$x=localtime(time);
#	push @t,"Done Samid Fetch: ".$x;
#
#	$cmd="$DSQUERY computer -o dn -limit 0";
#	print "$cmd\n";
#	open(C,"$cmd |") or die "Cant run command $cmd\n$!\n";
#	my($cnt)=0;
#	while(<C>) {
#		chomp;
#		s/"//g;
#		print "Setting DN for $samid[$cnt] to $_\n";
#		$dn{$samid[$cnt++]} = $_;
#	}
#	close(C);
#	$x=localtime(time);
#
#	push @t, "Done Dn Fetch: ".$x;
#
#	$cmd="$DSQUERY computer -o rdn -limit 0";
#	print "$cmd\n";
#	open(B,"$cmd |") or die "Cant run command $cmd\n$!\n";
#	my($cnt)=0;
#	while(<B>) {
#		chomp;
#		s/"//g;
#		print "Setting RDN for $samid[$cnt] to $_\n";
#		$rdn{$samid[$cnt++]} = $_;
#	}
#	close(B);
#	$x=localtime(time);
#	push @t,"Done Rdn Fetch: ".$x;
##}
#
##	my($cnt)=1;
##	foreach (@samid) {
##		print "$cnt) samid=$_ rdn=$rdn{$_} dn=$dn{$_}\n";
##		$cnt++;
##	}
#
##	use Data::Dumper;
##	print Dumper \%rdn;
##	print "\n";
##	print Dumper \%dn;
##	print "\n";
##	print "\n";
#
#	my($q)="DELETE INV_system_attribute where system_type='ldap_computer'";
#	print $q if $DEBUG;
#	MlpAlarm::_querynoresults($q,1);
#
#	$q="DELETE INV_system where system_type='ldap_computer'";
#	print $q if $DEBUG;
#	MlpAlarm::_querynoresults($q,1);
#
#	my(%attributes);
#	$cmd="\"$DSQUERY\" computer -o dn -limit 0 \| \"$DSGET\" computer -samid -sid -desc -loc -disabled -l";
#	print $cmd,"\n";
#	open(E,"$cmd |") or die "Cant run command $cmd\n$!\n";
#	my(%FOUNDSYS);
#	while(<E>) {
#		chomp;
#		next if /dsget succeeded/;
#		# print "DBGDBG: $_\n";
#		s/"//g;
#
#		if( /^\s*$/ ) {
#			print "Saving Attributes for $attributes{samid}\n";
#			my($n) = $attributes{rdn} = $rdn{$attributes{samid}};
#			$attributes{dn} = $dn{$attributes{samid}};
#			my($t) = "ldap_computer";
#
#			if( $FOUNDSYS{$n} ) {
#				print "WARNING: $n IS DUPLICATED IN LDAP\n";
#				%attributes=();
#				next;
#			}
#
#			$FOUNDSYS{$n}=1;
#			my($q)="insert INV_system (system_name,system_type ) values ("
#						.dbi_quote(-connection=>$connection,-text=>$n).","
#						.dbi_quote(-connection=>$connection,-text=>$t)." )";
#			print $q if $DEBUG;
#			MlpAlarm::_querynoresults($q,1);
#
#			foreach my $k ( keys %attributes ) {
#				my($v)=$attributes{$k};
#				print "K=$k V=$v \n" if $DEBUG;
#				next unless $v;
#				my($q)="insert INV_system_attribute (attribute_name,system_name,system_type,attribute_value,mod_date,mod_by ) values ("
#							.dbi_quote(-connection=>$connection,-text=>"LDAP_".$k).","
#							.dbi_quote(-connection=>$connection,-text=>$n).","
#							.dbi_quote(-connection=>$connection,-text=>$t).","
#							.dbi_quote(-connection=>$connection,-text=>$v).",getdate(),'INV_dsget_computer.pl')";
#				print $q if $DEBUG;
#				MlpAlarm::_querynoresults($q,1);
#			}
#			%attributes=();
#			next;
#		}
#
#		if( /:/ ) {
#			s/\s+$//;
#			my($k,$v)=split(/:/,$_);
#			$v=~s/^\s*//;
#			next if $v=~ /^\s*$/;
#			$attributes{$k} = $v;
#
#
#		} else {
#			warn "WOAH - RETURNED $_\n";
#		}
#	}
#	close(E);
#	$x=localtime(time);
#	push @t,"Done: ".$x;
#	print join("\n",@t),"\n";
#	print "Load Completed\n";
#	exit(0);
#}
#
#my($query)="select system_name,system_type from INV_system\n";
#print( "$query \n");
#foreach( dbi_query(-db=>"", -connection=>$connection, -query=>$query)) {
#	my($n,$t)=dbi_decode_row($_);
#	$n=~s/\s+$//;
#	$t=~s/\s+$//;
#	next unless $t =~ /unix/ or $t=~/win32servers/;
#
#	print "\n****************\n";
#	print "*  $n\n";
#	print "****************\n";
#
#	my($q)="$DSQUERY computer -o dn -name $n";
#	print $q,"\n" if $DEBUG;
#	my($dn);
#	open(X,$q." |") or die "Cant run $q $!\n";
#	while(<X>) {
#		chomp;
#		next if /^\s*$/;
#		$dn=$_;
#		print "Setting DN=$dn\n" if $DEBUG;
#	}
#	close(X);
#	if( ! defined $dn ) {
#		print "No dn found for $n - continuing\n";
#		next;
#	}
#
#	print "name=$n dn=$dn\n" if $DEBUG;
#
#	my($q)="delete INV_system_attribute where system_name = ".dbi_quote(-connection=>$connection,-text=>$n).
#					" and system_type = ".dbi_quote(-connection=>$connection,-text=>$t).
#					" and attribute_name like 'LDAP_%'";
#	# print "$q\n";
#	MlpAlarm::_querynoresults($q);
#
#	$q="$DSGET computer $dn -samid -sid -desc -loc -disabled -l";
#	print $q,"\n" if $DEBUG;
#	open(X,$q." |") or die "Cant run $q $!\n";
#	while(<X>) {
#		chomp;
#		next if /^\s*$/ or /dsget succeeded/;
#		if( /:/ ) {
#			s/\s+$//;
#			my($k,$v)=split(/:/,$_);
#			$v=~s/^\s*//;
#			next if $v=~ /^\s*$/;
#			$q="insert INV_system_attribute (attribute_name,system_name,system_type,attribute_value,mod_date,mod_by ) values ("
#						.dbi_quote(-connection=>$connection,-text=>"LDAP_".$k).","
#						.dbi_quote(-connection=>$connection,-text=>$n).","
#						.dbi_quote(-connection=>$connection,-text=>$t).","
#						.dbi_quote(-connection=>$connection,-text=>$v).",getdate(),'INV_dsget_computer.pl')";
#			MlpAlarm::_querynoresults($q);
#		} else {
#			warn "WOAH - $q RETURNED $_\n";
#		}
#	}
#	close(X);
#}
#
#MlpBatchJobEnd() if $BATCH_ID;
#exit(0);

__END__

=head1 NAME

INV_dsget.pl - load LDAP attributes of computers into INV_system_attribute

=head1 SYNOPSIS

INV_dsget_computer.pl --BATCH_ID=s --DEBUG

=head1 DESCRIPTION

Foreach 'unix' or 'win32servers' system_name from INV_system records

	update INV_system_attribute LDAP_* from

	   dsget computer $dn -samid -sid -desc -loc -disabled -l

=head1 EXAMPLE RUN

	****************
	*  ADCLUSTER172
	****************
	delete INV_system_attribute where system_name = 'ADCLUSTER172' and system_type = 'win32servers' and attribute_name like 'LDAP_%'
	insert INV_system_attribute values ('LDAP_desc','ADCLUSTER172','win32servers','(cabinet 1155-205.04) SQLPROD2 - QAI',getdate(),'INV_dsget_computer.pl')
	insert INV_system_attribute values ('LDAP_samid','ADCLUSTER172','win32servers','ADCLUSTER172$',getdate(),'INV_dsget_computer.pl')
	insert INV_system_attribute values ('LDAP_sid','ADCLUSTER172','win32servers','S-1-5-21-3444377139-1232238647-3825202667-14181',getdate(),'INV_dsget_computer.pl')
	insert INV_system_attribute values ('LDAP_disabled','ADCLUSTER172','win32servers','no',getdate(),'INV_dsget_computer.pl')

=cut
