#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use DBIFunc;
my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw($BATCH_ID $DEBUG);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID

   --BATCH_ID=id     [ if set - will log via alerts system ]
   --DEBUG \n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

# die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"		=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

my($connection)=MlpGetDbh();

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Credential Mgt') if $BATCH_ID;

my($query)="\tdelete INV_credential where credential_type='ypcat passwd'";
MlpAlarm::_querynoresults($query,1-$DEBUG);

$query="\tdelete INV_credential_attribute where credential_type='ypcat passwd'";
MlpAlarm::_querynoresults($query,1-$DEBUG);

#/* , is_deleted       , display_name     */
my($cred_sql) = "(credential_name, credential_type, credential_id, system_name, system_type, collection_date, collected_by )";

my($q)='ypcat passwd';
print "Executing ",$q,"\n";
open(X,$q." |") or die "Cant run $q $!\n";
while(<X>) {
	chomp;
	next if /^\s*$/;
	#my($login,$pass,uid,$gid,$desc,$home,$sh)=split(/:/);
	my(@vals)=split(/:/);
	my(%ATT_MAP)=(
		'password' 			=> 1,
		#'uid' 				=> 2,
		'gid' 				=> 3,
		'description' 		=> 4,
		'home directory'	=> 5,
		'dflt shell' 		=> 6
	);

	my($base)=			dbi_quote(-connection=>$connection,-text=>$vals[0]).",".			# login
							"'ypcat passwd',".
							dbi_quote(-connection=>$connection,-text=>$vals[2]).",".			# sid
							"null,null";									# system, systype

	# print "V0=$vals[0] BASE=$base\n";
	my($q)= "insert INV_credential $cred_sql values \n\t( $base ,\n\tgetdate(),'".$PROGRAM_NAME."')";
	MlpAlarm::_querynoresults($q,1-$DEBUG);

	foreach my $str (keys %ATT_MAP ) {
		my($col)=$ATT_MAP{$str};
		$vals[$col] = "N.A." if $vals[$col] =~ /^\s*$/;
		$q="insert INV_credential_attribute values \n\t( $base ,\n\t '".$str."',".
						 dbi_quote(-connection=>$connection,-text=>$vals[$col])	.")";
		MlpAlarm::_querynoresults($q,1-$DEBUG);
	}
	print "\n";
}

my($q)= "delete INV_group_member where group_type='ypcat group'";
MlpAlarm::_querynoresults($q,1-$DEBUG);

my($q)='ypcat group';
print "Executing ",$q,"\n";
open(X,$q." |") or die "Cant run $q $!\n";
while(<X>) {
	chomp;
	next if /^\s*$/;
	my($group,$pass,$gid,$users)=split(/:/);
	#print "GROUP=$group\n";

	my(%users);
	foreach ( split(/,/,$users) ) {
		s/\s+$//;
		next if $users{$_};
		$users{$_}=1;
		#print " => user $_\n";
		my($q)= "insert INV_group_member values \n\t( ".
						dbi_quote(-connection=>$connection,-text=>$group).",".
						"'ypcat group',".
						dbi_quote(-connection=>$connection,-text=>$gid).",".

						dbi_quote(-connection=>$connection,-text=>$_).",\n\t".
						"'ypcat passwd',null,".

						"null,null)";


	#	print $q;
		MlpAlarm::_querynoresults($q,1-$DEBUG);
		print "\n";
	}
}

MlpBatchJobEnd() if $BATCH_ID;
print "Successful Completion\n";
exit(0);

#
#	xxxxxx
#	my($q)="delete INV_principal where system_name = ".dbi_quote(-connection=>$connection,-text=>$n).
#					" and system_type = ".dbi_quote(-connection=>$connection,-text=>$t).
#					" and attribute_name like 'LDAP_%'";
#	# print "$q\n";
#	MlpAlarm::_querynoresults($q);
#
#	$q="dsget computer $dn -samid -sid -desc -loc -disabled -l";
#	print $q,"\n" if $DEBUG;
#	open(X,$q." |") or die "Cant run $q $!\n";
#	while(<X>) {
#		chomp;
#		next if /^\s*$/ or /dsget succeeded/;
#		if( /:/ ) {
#			s/\s+$//;
#			my($k,$v)=split(/:/,$_);
#			$v=~s/^\s*//;
#			next if $v=~ /^\s*$/;
#			$q="insert INV_system_attribute values ("
#						.dbi_quote(-connection=>$connection,-text=>"LDAP_".$k).","
#						.dbi_quote(-connection=>$connection,-text=>$n).","
#						.dbi_quote(-connection=>$connection,-text=>$t).","
#						.dbi_quote(-connection=>$connection,-text=>$v).",getdate(),'INV_dsget_computer.pl')";
#			MlpAlarm::_querynoresults($q);
#		} else {
#			warn "WOAH - $q RETURNED $_\n";
#		}
#	}
#	close(X);
#}
#
#exit(0);

__END__
=pod

=head1 NAME

INV_unix_ypcat_extractor - extract `ypcat password` and `ypcat group`
into inventory system

=head1 SYNOPSIS

INV_unix_ypcat_extractor.pl --BATCH_ID=s --DEBUG

=head1 DESCRIPTION

Clear out from INV_credential and INV_credential_attribute any
records with credential type = 'ypcat passwd'

Read the output of `ypcat passwd`

Insert into INV_credential:

  user name
  'ypcat passwd'
  user UID
  null
  null

Insert into INV_credential_attribute:

  user name
  'ypcat passwd'
  user UID
  null
  null
  attribute name
  attribute value

where the attribute name is among:

  password
  gid
  description
  home directory
  dflt shell

Clear out from INV_group_member any
records with group type = 'ypcat group'

Read the output of `ypcat group`

For each member of each group, insert into INV_group_member:

  group name
  'ypcat group'
  group GID
  user name
  'ypcat passwd'
  null
  null
  null

=cut
