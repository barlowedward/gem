=head1 NAME

sp__help  - Modified sp_help

=head2 AUTHOR

Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION

Pretty version of sp_help. sp_help scrolls off screen and is ugly. List
objects in current database (if @objname undefined) or list table columns
(if @objname defined). The proc is slow....

=head2 USAGE

sp_help [@objname]

if @objname is defined, will list information about specific object
if that object exists. If the object doesnt exist, it will try to print
all objects that contain the string fragment @object. EXAMPLE

sp__help "pmt_" Info about all objects with "pmt_" in the name

sp__help server Info about table server (with column listing)

=head2 SEE ALSO

sp__help calls the procedures sp__helpcolumn and sp__helpindex when an
object is passed as a parameter.

=head2 SAMPLE OUTPUT

    1> exec sp__help
     Name                 Owner                Object_type
     -------------------- -------------------- -----------------
     alerts               dbo                  user table
     audit_trail          dbo                  user table
     comn_database        dbo                  user table
     comn_dumpdevices     dbo                  user table
     comn_syscolumns      dbo                  user table

     comn_sysdevices      dbo                  user table
     comn_sysindexes      dbo                  user table
     comn_syslocks        dbo                  user table

    1> sp__help authors
     Name                 Owner                Object_type
     -------------------- -------------------- ----------------
     authors              dbo user             table

    table name    insert trigger  update trigger  delete trigger
    ------------- --------------- --------------- ---------------
    authors       authors_ins     ...........     ............

    Column_name   Type            Nulls Default_name    Rule_name
    ------------- --------------- ----- --------------- --------
    au_id         id 0
    au_lname      varchar(40)     0
    au_fname      varchar(40)     0
    phone         char(12)        0      phonedflt
    address       varchar(40)     1

    INDEX KEY c = clustered     u = unique
              a = allow dup row s = suspect

    Table Name           Index Name  c u a s List of Index Keys
    -------------------- ----------- - - - - ------------------
    authors              auidind     Y Y     au_id
