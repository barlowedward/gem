#!/apps/perl/linux/perl-5.8.2/bin/perl

# find all files on arg1 that are older than 1 month
sub usage {
	print @_;
	print "usage $0 --system=<sysname> --sharename=<c_drive_share>\n";
	exit(0);
}

use strict;
use Getopt::Long;
use File::Basename;

my($curdir)=dirname($0);
chdir $curdir or die "Cant chdir to $curdir: $!\n";

use vars qw( $system $share );
die usage("Bad Parameter List") unless
	GetOptions("system=s"=> \$system, "sharename=s"=>\$share);

my(@systems);
my(%disks_by_svr);
if( ! defined $system or ! defined $share ) {
	my($cfgfile)="win32_password.cfg";
	if( ! -e $cfgfile and -e "../$cfgfile" ) {
		$cfgfile = "../".$cfgfile;
	} elsif( ! -e $cfgfile ) {
		die "Cant find $cfgfile\n";
	}

	open(DISKS,$cfgfile) or die "Cant open $cfgfile : $! \n";
	foreach my $d ( <DISKS> ) {
		chomp;
		my(@x)=split(/\s+/,$d);
		my($sys)=shift @x;
      		push @systems, $sys;
      		$disks_by_svr{$sys} = \@x;
	}
	close(DISKS);
}

my(@cursys);
if( ! defined $system ) {
	push @cursys, @systems;
} else {
	push @cursys,$system;
}

SYSTEM: foreach my $s ( @cursys ) {
	if( ! defined $share )  {
		die "Cant find disks in password file"
			unless defined $disks_by_svr{$s};
		my(@disks) = @{$disks_by_svr{$s}};
		$share = $disks[0];
	}
	die usage("Must Pass boot share --share\n") unless defined $share;
	print "STARTING SERVER=$s\n";

	my($dir)="\\\\$s/$share/winnt/tasks";
	#opendir(DIR,$dir) || die "Cant Open Directory $dir - This should be your windows boot directory: $!\n";
	opendir(DIR,$dir) or next;
	my(@files)=grep(!/^\./,readdir(DIR));
	closedir(DIR);
	my($found)=0;
	foreach (sort @files) {
		next if /^desktop.ini$/ or /^SA.DAT$/;
		if( $found==0 ) {
			print "   SCHEDULED JOBS (from $dir)\n";
			$found++;
		}
		print "      ",$_,"\n";
	}
	if( $found==0 ) { print "   NO SCHEDULED JOBS ON $s\n"; }

	my($dir)="\\\\$s/$share/documents and settings";
	opendir(DIR,$dir) || next;
		#print "Cant Open Directory $dir : $!\nIgnoring This Server\n";
		#next;
	#};
	my(@files)=grep(!/^\./,readdir(DIR));
	closedir(DIR);
	print "   START PROGRAMS ON $s\n";
	foreach my $userdir (sort @files) {
		my($subdir) = $dir."/".$userdir."/Start Menu/Programs/Startup";
		next unless -d $subdir;
		opendir(SDIR,$subdir) || die "Cant Open Directory $subdir : $!\n";
		my($found)=0;
		my($str)= "Directory=$dir/$userdir\n";
		foreach ( grep(!/^\./,readdir(SDIR)) ) {
			print "      ",$str if $found==0;
			print "        ../Start Menu/Programs/Startup/$_\n";
			$found++;
		};
		closedir(SDIR);
	}
	print "COMPLETED SERVER=$s\n";
}
