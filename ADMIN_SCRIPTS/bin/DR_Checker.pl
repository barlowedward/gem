#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

my($DEVELOPER_MODE);

# copyright (c) 2008 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

use strict;
use File::Basename;
use Getopt::Long;
use Repository;
use DBI;
use MlpAlarm;
use Time::Local;
use CommonFunc;
use Inventory;
use DBIFunc;
use Do_Time;
use vars qw( $BATCHID $OUTFILE $SQLSVRONLY $SYBASEONLY $DEBUG $SHOWPATCHES $FAST $SERVER $TOGEMCONSOLE );

$|=1;

my($VERSION)="1.0";

# changedir to directory in which the executable exists
# my($curdir)=dirname($0);
# chdir $curdir or die "Cant chdir to $curdir : $! \n";

sub usage {
	print @_,"\n";
	return "Usage: DR_Checker.pl [--SYBASEONLY] [--SQLSVRONLY] --OUTFILE=file --BATCHID=name --DEBUG --SHOWPATCHES";
}

die usage("Bad Parameter List $!\n") unless
	GetOptions(
			"TOGEMCONSOLE"		=>\$TOGEMCONSOLE,
			"OUTFILE=s"			=>\$OUTFILE,
			"BATCHID=s"			=>\$BATCHID,
			"SYBASEONLY"		=>\$SYBASEONLY,
			"SHOWPATCHES"		=>\$SHOWPATCHES,
			"SERVER=s"			=>\$SERVER,
			"SQLSVRONLY"		=>\$SQLSVRONLY,
			"FAST"				=>\$FAST,
			"DEBUG"				=>\$DEBUG
	);

my($nl)="\n";

die "Cant pass TOGEMCONSOLE and OUTFILE" if $OUTFILE and $TOGEMCONSOLE;

if($OUTFILE) {
	my($rcc)=open(OUT,">$OUTFILE");
	if( ! $rcc ) { warn "WARNING: CANT WRITE $OUTFILE\n"; $OUTFILE=''; }
}

my($gem_root_dir)=get_gem_root_dir();
die "No Gem Root Dir" unless -d $gem_root_dir;
print "GEM ROOT=$gem_root_dir\n";

my $GemConfig=read_gemconfig( -debug=>$DEBUG);
my $ROLLOUT_DIRECTORY = $GemConfig->{MIMI_FILENAME_WIN32};
print "  ROLLOUT_DIRECTORY  is $ROLLOUT_DIRECTORY\n";
if( ! -d $ROLLOUT_DIRECTORY ) {
	die "\nERROR: $ROLLOUT_DIRECTORY is not a directory.  \nFrom gem.dat MIMI_FILENAME_WIN32\n";
}
if( ! -w $ROLLOUT_DIRECTORY ) {
	die "\nERROR: $ROLLOUT_DIRECTORY is not writable.  \nFrom gem.dat MIMI_FILENAME_WIN32\n";
}

my $CONSOLE_DIRECTORY = $gem_root_dir."/data/CONSOLE_REPORTS";
print "  CONSOLE_DIRECTORY  is $ROLLOUT_DIRECTORY\n";
if( ! -d $CONSOLE_DIRECTORY ) {
	die "\nERROR: $CONSOLE_DIRECTORY is not a directory.  \nFrom gem.dat MIMI_FILENAME_WIN32\n";
}
if( ! -w $CONSOLE_DIRECTORY ) {
	die "\nERROR: $CONSOLE_DIRECTORY is not writable.  \nFrom gem.dat MIMI_FILENAME_WIN32\n";
}

status_message("Running DR_Checker.pl v1.0 at ".localtime(time)."\n");
status_message( "<br><h2>Batch $BATCHID</h2>\n"	)				if $BATCHID;
status_message( "<br>Option: SYBASEONLY set \n" )					if $SYBASEONLY;
status_message( "<br>Option: SQLSVRONLY set \n" )					if $SQLSVRONLY;
status_message( "<br>Option: SERVER set to $SERVER\n" )					if $SERVER;
status_message( "<br>Option: OUTFILE = $OUTFILE \n" )			if $OUTFILE;
status_message( "<hr><br>\n" );

debug_message("Reading crosscheck.dat\n");
my(%logship_destinations);								# source_server:dest_server pairs
my(%logship_specified_ignoredb);                # source_server:dest_server:db key - value = 1/0
my(%logship_testok);										# ditto
my(%logship_notes);
my(%logship_type);
foreach ( get_conf_file_dat('crosscheck') ) {
	chomp;
	if( /^LOGSHIP\s/i or /^LOGSHIP_DBONLY/i ) {
		chomp;
		my($txt,$svr,$dest,$h1,@databases)=split;
		next if $SERVER and $svr ne $SERVER;
		$logship_destinations{$svr.":".$dest} = 1;
		foreach (@databases) { $logship_specified_ignoredb{$svr.":".$dest.":".$_} = 1 };

	} elsif( /^LOGSHIP_TYPE/i ) {
		chomp;
		my($txt,$svr,$dest,$typ)=split;
		next if $SERVER and $svr ne $SERVER;
		$logship_type{$svr.":".$dest} = $typ;

	} elsif( /^LOGSHIP_IGNORE/i ) {
		chomp;
		my($key,$srcsvr,$destsvr,@dbs)=split;
		next if $SERVER and $srcsvr ne $SERVER;
		foreach (@dbs) {
			#print "DBGDBG Ignoring $srcsvr => $destsvr db=$_\n";
			$logship_specified_ignoredb{$srcsvr.":".$destsvr.":".$_} = 0;
		};
	} elsif( /^LOGSHIP_TESTOK/i ) {
		chomp;
		my($key,$srcsvr,@testsok)=split;
		next if $SERVER and $srcsvr ne $SERVER;
		foreach (@testsok) {
			s/~/ /g;
			# print ":: SETTING logship_testok{".$srcsvr.":".$_."} \n";
			$logship_testok{$srcsvr.":".$_} = 1;
		};
	} elsif( /^LOGSHIP_NOTES/i ) {
		chomp;
		my($key,$srcsvr,$des,$tx)=split(/\s+/,$_,4);
		next if $SERVER and $srcsvr ne $SERVER;
		# print "NOTES FOUND FOR $srcsvr:$des == $tx\n";
		$logship_notes{$srcsvr.":".$des} = $tx;
	}
}

foreach( sort keys %logship_destinations ) {
	my($src,$dest)=split(/:/);
	if( $TOGEMCONSOLE ) {
		$OUTFILE = $CONSOLE_DIRECTORY;
		if( $logship_type{$src.":".$dest} =~ /sqlsvr/i ) {
			$OUTFILE .= "/console_sqlsvr/${src}_DrChecker.txt";
		} else {
			$OUTFILE .= "/console_sybase/${src}_DrChecker.txt";
		}
		print "Writing to $OUTFILE\n";
		my($rcc)=open(OUT,">$OUTFILE");
		if( ! $rcc ) { warn "WARNING: CANT WRITE $OUTFILE\n"; $OUTFILE=''; }
	}

	debug_message("Comparing $src to $dest\n");
	compare_2_servers($src,$dest);

	if( $TOGEMCONSOLE and $OUTFILE ) {
		close(OUT);
	}

	print "EXITING - DEVELOPER MODE" if $DEVELOPER_MODE;
	exit(0) if $DEVELOPER_MODE;
}

print_all_alerts();
status_message("Completed DR_Checker at ".localtime(time)."\n");
exit(0);

my(@errors_for_last);
my(@allerrors);
sub print_alerts {
	if( $#errors_for_last == -1 ) {
		print "\nNO ALERTS FOR THIS SERVER\n";
	} else {
		print "\nALERTS FOR THIS SERVER\n";
	}
	push @allerrors, @errors_for_last;
	foreach (@errors_for_last) { status_message( $_ ); }
	$#errors_for_last = -1;
}

sub print_all_alerts {

	print "***********************\n";
	print "* SUMMARY OF ALL ALERTS\n";
	print "***********************\n";

	foreach (@allerrors) { print $_; }
}

sub set_alert {
	my($alertlvl, $src, $subsys, $msg)=@_;
	$msg=~s/\s+$//;

	die "BAD ALERT LEVEL $alertlvl - must be OK or ERROR\n" unless $alertlvl eq "OK" or $alertlvl eq "ERROR";
	$alertlvl="APPRV" if $alertlvl eq "ERROR" and $logship_testok{$src.":".$subsys};
	# print ":: TESTING logship_testok{".$src.":".$subsys."} \n";
	status_message( sprintf("=> %5.5s %44.44s|%s\n", $alertlvl, $src.".".$subsys, $msg ));
	#status_message("=> \tNote this event is OK based on user override\n")
	# 	if $logship_testok{$src.":".$subsys};
	$alertlvl = "OK" if $logship_testok{$src.":".$subsys};
	if( $alertlvl eq "OK" ) {
		debug_message("[ $alertlvl ] '".$src.".".$subsys."' $msg\n");
	} else {
		debug_message("[ $alertlvl ] '".$src.".".$subsys."' $msg\n");
		push @errors_for_last, "[ $alertlvl ] '".$src.".".$subsys."' $msg\n";
	}

	if( $BATCHID ) {
		MlpHeartbeat(  -monitor_program => $BATCHID,	-system=> $src,-subsystem=> $subsys,
			-state=> $alertlvl, -message_text=> $msg );
	}
}
sub compare_2_servers {
	my($src,$dest)=@_;

	status_message( "\n===============================================\n" );
	status_message( "| PRIMARY=$src DESTINATION=$dest\n" );

	my($found_1)="FALSE";
	my(%directive_include,%directive_exclude,$found_1);
	foreach ( sort keys %logship_specified_ignoredb ) {
		next unless /^$src:$dest:(.+)/;
		if( $logship_specified_ignoredb{$_} == 1 ) {
			status_message( "|   INCLUDE: $1\n" );
			$directive_include{$1}=1;
			$found_1='TRUE';
		} else {
			status_message( "|   EXCLUDE: $1\n" );
			$directive_exclude{$1}=1;
		}
	}

	# print "CHCEKING NOTES FOUND FOR $src:$dest == ",$logship_notes{$src.":".$dest},"\n";
	status_message( "| <FONT COLOR=RED><b>******************\n| $src=>$dest NOTES ".$logship_notes{$src.":".$dest}."\n| ******************</b></FONT>\n" ) if $logship_notes{$src.":".$dest};

	foreach ( keys %logship_testok ) {
		status_message( "| TestOk=",$_,"\n" ) if $_ =~ /^${src}\:/;
	}
	status_message( "===============================================\n" );

	debug_message("Fetching Passwords\n");
	my($s_l,$s_p)=get_password(-type=>'sybase',-name=>$src);
	my($LS_SOURCE_DBTYPE)='sybase' if $s_l;
	my($t_l,$t_p)=get_password(-type=>'sybase',-name=>$dest);
	my($LS_TARGET_DBTYPE)='sybase' if $t_l;
	if( ! $s_l ) {
		($s_l,$s_p)=get_password(-type=>'sqlsvr',-name=>$src);
		($LS_SOURCE_DBTYPE)='sqlsvr' if $s_l;
	}
	if( ! $t_l ) {
		($t_l,$t_p)=get_password(-type=>'sqlsvr',-name=>$dest);
		($LS_TARGET_DBTYPE)='sqlsvr' if $t_l;
	}
	if( $SQLSVRONLY and ($LS_SOURCE_DBTYPE eq "sybase" or $LS_TARGET_DBTYPE eq "sybase") ) {
		status_message("SKIPPING: --SQLSERVER passed and this is sybase replication\n");
		return 0;
	}
	if( $SYBASEONLY and ($LS_SOURCE_DBTYPE eq "sqlsvr" or $LS_TARGET_DBTYPE eq "sqlsvr") ) {
		status_message("SKIPPING: --SYBASE passed and this is sqlsvr replication\n");
		return 0;
	}
	if( ! is_nt() and ($LS_SOURCE_DBTYPE eq "sqlsvr" or $LS_TARGET_DBTYPE eq "sqlsvr") ) {
		status_message("SKIPPING: You are On UNIX and this is sqlsvr replication\n");
		return 0;
	}

	if( ! $LS_SOURCE_DBTYPE ) {
		status_message("SKIPPING: No Source TYPE\n");
		return 0;
	}

	status_message("Checking Server Types\n");
	if( $LS_SOURCE_DBTYPE ne $LS_TARGET_DBTYPE ) {
		status_message("ALERT: Source Type=$LS_SOURCE_DBTYPE Target Type=$LS_TARGET_DBTYPE\n");
		set_alert('ERROR',$src,"DrTypeCheck","Log Shipping Server Type MISMATCH in crosscheck.dat Src=$LS_SOURCE_DBTYPE Tgt=$LS_TARGET_DBTYPE");
		return 0;
	} else {
		set_alert('OK',$src,"DrTypeCheck","Log Shipping Target Types Match (crosscheck.dat)");
	}

	debug_message("Connecting To Source $src\n");
	my($conn_s) = dbi_connect(-srv=>$src,-login=>$s_l,-password=>$s_p,-connection=>1 );
	unless( $conn_s ) {
		status_message("=> UNABLE TO CONNECT TO SERVER $src - SKIPPING \n" );
		set_alert('ERROR',$src,"ConnectS","Cant Connect To Primary Server $src");
		return 0;
	}else {
		set_alert('OK',$src,"ConnectS","Connection To Primary Server $src");
	}

   dbi_set_mode("INLINE");
	my($primary_typeF,$sverF,$patchF)=dbi_db_version(-connection=>$conn_s);

	debug_message("Connecting To Target $dest\n");
	my($conn_t) = dbi_connect(-srv=>$dest,-login=>$t_l,-password=>$t_p,-connection=>1 );
	unless( $conn_t ) {
		status_message("=> UNABLE TO CONNECT TO SERVER $dest - SKIPPING \n" );
		set_alert('ERROR',$src,"ConnectT","Cant Connect To Disaster Recovery Server $dest");
		return 0;
	} else {
		set_alert('OK',$src,"ConnectT","Connection To Disaster Recovery Server $dest");
	}
	status_message( "\n" );

   my($primary_typeT,$sverT,$patchT)=dbi_db_version(-connection=>$conn_t);

	status_message( "Version Check\n" );
	status_message( "\t$src is type $primary_typeF version $sverF\n" );
	status_message( "\t$dest is type $primary_typeT version $sverT\n" );
	my($Version_State)="OK";
	$Version_State="ERROR" if  $sverT<$sverF;
	my($message)="Logship Version: $src=$sverF $dest=$sverT";
	set_alert($Version_State,$src,"DrDBVersion",$message);
	status_message( " - $Version_State: Version Check\n" );
	status_message( "\n" );

	status_message( "\@\@Version Check\n" );
	my($v1,$v2);
	foreach  ( dbi_query(-db=>"master", -query=>'select @@VERSION', -connection=>$conn_s) ) {
		my(@x)=dbi_decode_row($_);
		$v1=$x[0];
		status_message( "\t$src Version $v1\n" );
	}
	foreach  ( dbi_query(-db=>"master", -query=>'select @@VERSION', -connection=>$conn_t) ) {
		my(@x)=dbi_decode_row($_);
		$v2=$x[0];
		status_message( "\t$dest Version $v2\n" );
	}
	if( $v1 eq $v2 ) {
		set_alert('OK',   $src,'Dr@@VERSION',"Versions Exactly Match");
		status_message( ' - OK: @@Version Check - Versions Exactly Match' );
	} else {
		set_alert('ERROR',$src,'Dr@@VERSION',"ERROR Versions Do NOT Exactly Match Disaster Recovery Server");
		status_message( ' - ERROR: @@Version Check - Versions Do NOT Exactly Match Disaster Recovery Server' );
	}
	status_message( "\n\n" );

	if( ! $FAST ) {
		status_message( "Hardware Check\n" );
		my(%srcatts) =get_systeminfo($src);
		my(%destatts)=get_systeminfo($dest);
		my(@a)=keys %srcatts;
		my(@b)=keys %destatts;
		# print "$#a RETRIEVED $#b \n";
		if( $#a > -1 and $#b> -1 ) {
			printf "%-25.25s %-30.30s %-30.30s\n","KEY", $src, $dest;
			foreach ( sort keys %srcatts ) {
				next unless /Memory/ or /System/ or /Version/ or /Name/;
				next if /NetWork/i or /System Directory/i or /Available Physical/i;
				printf "%-25.25s %-30.30s %-30.30s\n",$_,$srcatts{$_}, $destatts{$_};
				next if /Up Time/i or /Virtual Memory/ or /Host Name/i;
				if( $srcatts{$_} eq $destatts{$_} ) {
					# set_alert('OK',$src,'DrHwAtts '.$_,"ERROR $_ Does NOT Exactly Match Disaster Recovery Server");
				} else {
					set_alert('ERROR',$src,'DrHwAtts '.$_,"ERROR $_ Does NOT Exactly Match ($srcatts{$_},  $destatts{$_})");
				}
			}
			status_message( "\n" );
			if ( $SHOWPATCHES ) {
				status_message( "Hardware Patch Level Check\n" );
				printf "%40.40s %12s %12s\n","KEY", $src, $dest;
				foreach ( sort keys %srcatts ) {
					next unless /Hotfix/i;
					printf "%-40.40s %-12.12s %-12.12s\n",$_,$srcatts{$_}, $destatts{$_};
				}
				status_message( "\n" );
			} else {
				status_message( "Skipping Microsoft Patch Checks - use --SHOWPATCHES to see them\n\n" );
			}
		} else {
			status_message( "Skipping Hardware Checks - UNIX\n" );
			status_message( "\n" );
		}
	} else {
		status_message( "Skipping Hardware Check in FAST mode\n" );
		status_message( "\n" );
	}
	my($Version_State)="OK";
	my($Primary_Type,	@Primary_dblist)= dbi_parse_opt_D('%',1,1,undef,$conn_s);
	my($Dest_Type,		@Dest_dblist)	 = dbi_parse_opt_D('%',0,1,undef,$conn_t);

	#print "PRIMARY     $Primary_Type",join(" ",@Primary_dblist),"\n";
	#print "DESTINATION $Dest_Type",join(" ",@Dest_dblist),"\n";

	# SPACE CHECKS ONLY IMPORTANT ON SYBASE - SQL SERVER HAS AUTO EXPAND
	my(%spaceS,%logspaceS,%spaceT,%logspaceT);
	if( $Primary_Type eq "SYBASE" and $Dest_Type eq "SYBASE" ) {
		foreach  ( dbi_query(-db=>"master", -query=>"exec sp__helpdb \@dont_format='Y'", -connection=>$conn_s) ) {
			my(@x)=dbi_decode_row($_);
			$x[0] =~ s/\s+\(\d+\)*$//;
			$x[1] =~ s/\s//g;
			$x[2] =~ s/\s//g;
			$spaceS{$x[0]}=$x[1];
			$logspaceS{$x[0]}=$x[2];
			$logspaceS{$x[0]}=0 if $logspaceS{$x[0]} eq "N/A";
		}
		foreach  ( dbi_query(-db=>"master", -query=>"exec sp__helpdb \@dont_format='Y'", -connection=>$conn_t) ) {
			my(@x)=dbi_decode_row($_);
			$x[0] =~ s/\s+\(\d+\)*$//;
			$x[1] =~ s/\s//g;
			$x[2] =~ s/\s//g;
			$spaceT{$x[0]}=$x[1];
			$logspaceT{$x[0]}=$x[2];
			$logspaceT{$x[0]}=0 if $logspaceT{$x[0]} eq "N/A";
		}

		debug_message("\tSUMMARY of disk space on SRC=$src TGT=$dest\n");
		foreach ( sort keys %spaceS ) {
			debug_message( sprintf("\t==> %25s: SRC=%12s TARGET=%12s \n", $_, $spaceS{$_}."/".$logspaceS{$_}, $spaceT{$_}."/".$logspaceT{$_}) )
				unless $directive_exclude{$_}
				or $_ eq "master"
				or $_ eq "model"
				or $_ eq "tempdb"
				or $_ eq 'syblogins'
				or $_ eq 'pubs'
				or $_ eq 'pubs2'
				or $_ eq 'msdb'
				or $_ eq 'Northwind'
				or $_ eq 'sybsystemdb'
				or $_ eq 'sybsecurity'
				or $_ eq 'sybsystemprocs';
		}
	}

   if( ! $FAST ) {
		if( $Primary_Type eq "SYBASE" and $Dest_Type eq "SYBASE" ) {
			# srvname,srvnetname where srvname!=srvnetname and srvname!='SYB_BACKUP'
		} else {
			# ON SQL SERVER {
			#select srvname,srvproduct,providername,datasource,catalog,rpc,pub,sub,dist,dpub,rpcout from sysservers
			status_message( "Link Server Check (ORACLE LINKSERVERS TESTED ONLY FOR EXISTENCE)\n" );
			my(%primary_linkservers);

			my(@rc)=dbi_query(-db=>"master", -query=>"select srvname,srvproduct,providername from sysservers", -connection=>$conn_s);
			foreach  ( @rc ) {
				my(@x)=dbi_decode_row($_);
				if( $x[1] =~ /ORACLE/i or $x[2]=~/ORA$/i ) {	# oracle
					$primary_linkservers{$x[0]}="ORACLE" ;
					next;
				}
				$primary_linkservers{$x[0]}="WORKS" ;
				my(@rc2)=dbi_query(-db=>"master", -query=>"select * from $x[0].master.dbo.sysobjects", -connection=>$conn_s);
				if( $#rc2 < 5 ) {
					$primary_linkservers{$x[0]}="NONFUNCTIONAL" ;
				}
			}
			my(%dr_linkservers);
			my(@rc)=dbi_query(-db=>"master", -query=>"select srvname,srvproduct,providername  from sysservers", -connection=>$conn_s);
			foreach  ( @rc ) {
				my(@x)=dbi_decode_row($_);
				if( $x[1] =~ /ORACLE/i or $x[2]=~/ORA$/i ) {	# oracle
					$dr_linkservers{$x[0]}="ORACLE" ;
					next;
				}
				$dr_linkservers{$x[0]}="WORKS" ;
				my(@rc2)=dbi_query(-db=>"master", -query=>"select * from $x[0].master.dbo.sysobjects", -connection=>$conn_s);
				if( $#rc2 < 5 ) {
					$dr_linkservers{$x[0]}="NONFUNCTIONAL" ;
				}
			}
			foreach ( sort keys %primary_linkservers ) {
				if(  $primary_linkservers{$_} eq "ORACLE" ) {
					if( $dr_linkservers{$_} ) {
						set_alert('OK',$src,"DrLinkServer.".$_,"Oracle Linkserver Exists");
						status_message("\tOK   Oracle Linkserver $_ Exists On Both Sides\n");
					} else {
						set_alert('ERROR',$src,"DrLinkServer.".$_,"Oracle Linkserver Does Not Exist in DR");
						status_message( "\t==> ERROR Oracle $_ Linkserver Does Not Exist in DR\n");
					}
				} elsif( $primary_linkservers{$_} eq "WORKS" ) {
					if( $dr_linkservers{$_} eq "WORKS" ) {
						set_alert('OK',$src,"DrLinkServer.".$_,"Linkservers Ok");
						status_message("\tOK   Linkserver $_ Works On Both Sides\n");
					} elsif( $dr_linkservers{$_} eq "NONFUNCTIONAL" ) {
						set_alert('ERROR',$src,"DrLinkServer.".$_,"Disaster Recovery Linkserver NONFUNCTIONAL");
						status_message( "\t==> ERROR  Linkserver $_ Works In Production But Does NOT work on DR\n");
					} else {
						set_alert('ERROR',$src,"DrLinkServer.".$_,"Disaster Recovery Linkserver Does Not Exist");
						status_message( "\t==> ERROR  Linkserver $_ Works In Production But Does NOT exist on DR\n");
					}
				} else {
					set_alert('OK',$src,"DrLinkServer.".$_,"Linkservers Does Not Work In Production");
					status_message("\tOK   Linkserver $_ Does NOT work in PRODUCTION\n");
				}
			}
			status_message( "\n" );
		}
	}

	#
	# CHECK SYSLOGINS
	#
	status_message( "SysLogins Check\n" );
	my($query)='select name from master..syslogins';
	my(%login_state);
	my(%login_real);
	my(@rc)=dbi_query(-db=>"master", -query=>$query, -connection=>$conn_s);
	my($cntb,$cntp,$cnts) = (0,0,0);
	foreach  ( @rc ) {
		my(@x)=dbi_decode_row($_);
		next if $x[0] =~ /\SqlServer20/i or $x[0] =~ /^NT AUTH/i  or $x[0] =~ /^\#\#/i  or $x[0] =~ /^BUILTIN/i or $x[0] =~ /\$/;
		$login_real{lc($x[0])} = $x[0] unless $login_real{lc($x[0])};
		$login_state{lc($x[0])} = "ON PRIMARY";
		$cntp++;
	}
	my(@rc)=dbi_query(-db=>"master", -query=>$query, -connection=>$conn_t);
	foreach  ( @rc ) {
		my(@x)=dbi_decode_row($_);
		next if $x[0] =~ /\SqlServer20/i or $x[0] =~ /^NT AUTH/i or $x[0] =~ /^\#\#/i  or $x[0] =~ /^BUILTIN/i or $x[0] =~ /\$/;

		$login_real{lc($x[0])} = $x[0] unless $login_real{lc($x[0])};
		if( $login_state{ lc($x[0]) } ) {
			$login_state{lc($x[0])} = "ON BOTH";
			$cntb++;
		} else {
			$login_state{lc($x[0])} = "ON SECONDARY";
			$cnts++;
		}
	}
	if($cntb<$cntp) {
		foreach ( keys %login_state ) {
			next unless $login_state{$_} eq "ON PRIMARY";
			status_message("\t==> ERROR Login $login_real{$_} Only On $src (Primary) Not On $dest (DR)\n");
		}
		$cnts-=$cntb;
		$cntp-=$cntb;
		set_alert('ERROR',$src,"DrLoginCheck","NOT All $src (Primary) Logins On $dest (DR)");
		# status_message( "\t==> ERROR $cntb logins on both $cntp only on primary \n" );
	} else {
		set_alert('OK',$src,"DrLoginCheck","All $src (Primary) Logins On $dest (DR)");
		# status_message("\tOK   All Primary Logins On Secondary\n");
	}
	status_message("\n");

	#
	# CHECK SYSCONFIGURES
	#
	# the trick here is to do different queries based on db type...
	my($qq1,$qq2);
	my($qcol1,$qcol2);
	if( $primary_typeF eq "SQL SERVER" and $primary_typeT eq "SQL SERVER" ) {

		if( $patchF >= 9 and $patchT >= 9 ) {
			$qcol1=0;
			$qcol2=1;
			$qq1=$qq2="
			select name,
			convert(int, isnull(value, value_in_use)) as config_value
		from  sys.configurations
		where name like '%enabled' or name like '%parallelism' or  name like '%cmdshell'
		or    name like '%language' or name like '%memory%' or name like '%access'
		order by lower(name)";

		} elsif( $patchF >= 8 and $patchT >= 8 ) {
			$qcol1=0;
			$qcol2=1;
			$qq1=$qq2="
/* sql 2000 */
select name,
config_value = c.value
from master.dbo.spt_values, master.dbo.sysconfigures c, master.dbo.syscurconfigs
where type = 'C'
and number = c.config
and number = master.dbo.syscurconfigs.config
order by lower(name)";

		} else {
			status_message("Can No Test Configuration As Invalid Patch Levels\n");
		}

#		print "PV1 = $primary_typeF\n";
#		print "PV2 = $sverF\n";
#		print "PV3 = $patchF\n";
#
#		print "PT1 = $primary_typeT\n";
#		print "PT2 = $sverT\n";
#		print "PT3 = $patchT\n";
	} else {
		status_message("Comparing sp_configure values\n");
		$qcol1=0;
		$qcol2=3;
		$qq1=$qq2='exec sp_configure';
	}

	status_message( "SysConfigures Check\n" );
	my(%configure_dataP,%configure_dataD);
	my(@rc)=dbi_query(-db=>"master", -query=>$qq1, -connection=>$conn_s);
	my($cntb,$cntp,$cnts) = (0,0,0);
	foreach  ( @rc ) {
		my(@x)=dbi_decode_row($_);
		# next if $x[0] =~ /\SqlServer20/ or $x[0] =~ /^NT AUTH/;
		$configure_dataP{$x[$qcol1]} = $x[$qcol2];
		print "From T $x[$qcol1] val=$x[$qcol2]\n" if $DEVELOPER_MODE;
		$cntp++;
	}
	my(@rc)=dbi_query(-db=>"master", -query=>$qq2, -connection=>$conn_t);
	foreach  ( @rc ) {
		my(@x)=dbi_decode_row($_);
		# next if $x[0] =~ /\SqlServer20/ or $x[0] =~ /^NT AUTH/;
		$configure_dataD{$x[$qcol1]} = $x[$qcol2];
		print "From D $x[$qcol1] val=$x[$qcol2]\n" if $DEVELOPER_MODE;
	}
	my($okcnt,$badcnt)=(0,0);
	foreach ( sort keys %configure_dataP ) {
		# print "CHECKING VALUE $_\n";
		if( $configure_dataP{$_} eq $configure_dataD{$_} ) {
			set_alert('OK',$src,"DrConfig.".$_,"Config Values Match");
			$okcnt++;
		} else {
			set_alert('ERROR',$src,"DrConfig.".$_,"Config Missmatch Key=$_ $src (Primary)=$configure_dataP{$_} $dest (DR)=$configure_dataD{$_}");
			# status_message( "Config Missmatch Key=$_ $src (Primary)=$configure_dataP{$_} $dest (DR)=$configure_dataD{$_}\n" );
			$badcnt++;
		}
	}
	status_message(" - config checking shows $okcnt identical attributes and $badcnt different attributes\n");
	status_message("\n");

	status_message( "System Metadata Check\n" );
	my(%metadata_dataP,%metadata_dataD);
	my(@rc)=dbi_query(-db=>"master", -query=>"exec sp__metadata", -connection=>$conn_s);
	my($cntb,$cntp,$cnts) = (0,0,0);
	foreach  ( @rc ) {
		my(@x)=dbi_decode_row($_);
		# next if $x[0] =~ /\SqlServer20/ or $x[0] =~ /^NT AUTH/;
		$metadata_dataP{$x[0]} = $x[1];
		print "From T $x[0] val=$x[$qcol2]\n" if $DEVELOPER_MODE;
		$cntp++;
	}
	my(@rc)=dbi_query(-db=>"master", -query=>"exec sp__metadata", -connection=>$conn_t);
	foreach  ( @rc ) {
		my(@x)=dbi_decode_row($_);
		# next if $x[0] =~ /\SqlServer20/ or $x[0] =~ /^NT AUTH/;
		$metadata_dataD{$x[0]} = $x[1];
		print "From D $x[0] val=$x[$qcol2]\n" if $DEVELOPER_MODE;
	}
	my($okcnt,$badcnt)=(0,0);
	foreach ( sort keys %metadata_dataP ) {
		next if /ActiveSystemName/ or /ComputerNamePhysicalNetBIOS/ or /MachineName/
				or /ProcessID/ or /ServerName/ or /Reg_Setup_SQL/ or /ResourceLastUpdateDateTime/
				or /Reg_Parm_ErrorLog/ or /Reg_Parm_MasterDataFile/ or /Reg_Parm_MasterLogFile/
				or /Reg_MSSQL_BackupDirectory/ or /Real_MasterDataFile/ or /Real_MSDBLogFile/
				or /Real_MSDBDataFile/ or /Real_MasterLogFile/ or /NumLicenses/
				or /ClusterNode/
				or /IsClustered/
				or /IsIntegratedSecurityOnly/
				or /LicenseType/
				or /Reg_MSSQL_FullTextDefaultPath/
				or /SharedDrive/;

		if( $metadata_dataP{$_} eq $metadata_dataD{$_} ) {
			set_alert('OK',$src,"DrMetadata.".$_,"Metadata Values Match");
			$okcnt++;
		} else {
			set_alert('ERROR',$src,"DrMetadata.".$_,"Metadata Missmatch Key=$_ $src (Primary)=$metadata_dataP{$_} $dest (DR)=$metadata_dataD{$_}");
			# status_message( "Config Missmatch Key=$_ $src (Primary)=$metadata_dataP{$_} $dest (DR)=$metadata_dataD{$_}\n" );
			$badcnt++;
		}
	}
	status_message(" - metadata checking shows $okcnt identical attributes and $badcnt different attributes\n");
	status_message("\n");

	#
	# CHECK DATABASES
	#
	status_message( "Database Check\n" );
	my(%foundstate,$nummissing,%realname);
	$nummissing=0;
	debug_message("COMPARING DATABASES\n");

	foreach my $db (sort @Primary_dblist) {
		next if $directive_exclude{$db};
		$realname{ lc($db) } = $db;
		$foundstate{ lc($db) }="MISSING";
		if( $found_1 ) { # we have specified specific copy directories
			$foundstate{lc($db)}="NOSHIP" unless $directive_include{lc($db)};
			next;
		}
		$foundstate{lc($db)}="EXCLUDE" if $directive_exclude{lc($db)};
		if( $foundstate{lc($db)} eq "MISSING" ) {
			foreach ( @Dest_dblist ) { next if lc($db) ne lc($_); $foundstate{lc($db)}="MATCHED"; last; }
		}
		$nummissing++ if $foundstate{lc($db)} eq "MISSING";
	}

	debug_message("LIST OF DATABASES\n");
	my($missingdbs)='';
	my($spacedbs)='';
	foreach my $db (sort @Primary_dblist) {
		next if $directive_exclude{$db};
		if( $foundstate{lc($db)} eq "MISSING" ) {
			set_alert("ERROR",$src,"DrDbExists.".$db,"Database $db Not On Disaster Recovery Server");
			$missingdbs.="$db ";
			next;
		} else {
			set_alert("OK",$src,"DrDbExists.".$db,"Database $db Exists On Disaster Recovery Server");
		}

		next unless $Primary_Type eq "SYBASE" and $Dest_Type eq "SYBASE";

		# Test Database Space
		my($Space_State)="OK";
		my($ss)= $spaceS{$db} + $logspaceS{$db};
		my($st)= $spaceT{$db} + $logspaceT{$db};
		$Space_State="ERROR" if  $ss>$st;
		$spacedbs.="$db " if $ss>$st;

		my($message)="Size Db $db $src=".$ss."MB $dest=".$st."MB";
		set_alert($Space_State,$src,"DrSize.$db",$message);
	}

	if($missingdbs eq "" and $spacedbs eq "" ) {
		status_message( "\tOK: Databases all matched\n" );
	} elsif($missingdbs ne "" and $spacedbs ne "" ) {
		status_message( "\tERROR: MISSING DATABASES = $missingdbs\n" );
		status_message( "\tERROR: SPACE MISMATCH    = $spacedbs\n" );
	} elsif($missingdbs ne "" ) {
		status_message( "\tERROR: MISSING $missingdbs\n" );
	} else {
		status_message( "\tERROR: SPACE MISMATCH $spacedbs\n" );
	}

	if( $Primary_Type eq "SYBASE" and $Dest_Type eq "SYBASE" ) {
		status_message( "Starting Tempdb Check \n" );
		my($Tempdb_State)="OK";
		my($ss)= $spaceS{tempdb} + $logspaceS{tempdb};
		my($st)= $spaceT{tempdb} + $logspaceT{tempdb};
		$Tempdb_State="ERROR" if  $ss>$st;
		my($message)="Tempdb Size: $src=$ss Server $dest=$st";
		set_alert($Tempdb_State,$src,"DrTempdbCheck",$message);
		status_message( "\t$Tempdb_State: $message \n" );
	}

	print_alerts();

	status_message( "\n" );
	status_message("Audit Completed\n");
	dbi_disconnect(-connection=>$conn_t);
	dbi_disconnect(-connection=>$conn_s);
}

exit(0);
#	my($cmd)="$^X $curdir/copy_syslogins.pl -F$s -U$ -t$t -u$t_l ";
#	if( $SERVER_TYPE{$s} eq "SQLSVR" ) {
#		if( ! is_nt() ) {
#			status_message("=> SKIPPING SQL SERVER AS YOU ARE ON UNIX\n");
#			next;
#		}
#		$cmd.=" -M"
#	}
#	#status_message("exec: $cmd -p??? -P???\n" );
#	#$cmd .= "-P$s_p -p$t_p ";
#
##	open(CMD,"$cmd |") or die "Cant run Command $!\n";
##	while(<CMD>) {
##		chomp;
##		status_message(" => $_\n" );
##	}
##	close(CMD);

sub status_message {
	my(@msg)=@_;
	print OUT @msg if $OUTFILE;
	INV_sethtmlforms("INV_CONSOLE_HELP",@msg);
	print @msg;
}

sub debug_message {
	my(@msg)=@_;
	return unless $DEBUG;
	print OUT @msg if $OUTFILE;
	print @msg;
}

sub get_systeminfo {
	my($sys)=@_;
	my(%hash);
	open(CMD,"systeminfo /s $sys 2>&1 |") or die "Cant run systeminfo /s $sys $!\n";
	my($lastkey)='';
	my($failed)="FALSE";
	foreach (<CMD>) {
		next if /^\s*$/;		# empty strings
		chop;
		chomp;
		if( /The RPC server is unavailable/ ) {
			print "Unable to run systeminfo /s $sys\n";
			$failed='TRUE';
			next;
		}
		s/\s+$//;				# trailing white space
		if( /^\s/ ) {
			s/^\s*$//;
			my($key,$val)=split(/:/,$_,2);
			if( $lastkey =~ /hotfix/i ) {
				$val=~s/^\s+//;
				$hash{"Hotfix ".$val}="Installed";
			} else {
				$hash{$lastkey." ".$key}=$val;
			}
		} else {
			my($key,$val)=split(/:/,$_,2);
			$val=~s/^\s+//;
			$val=~s/\s+$//;
			$hash{$key}=$val unless $key =~ /^Hotfix/;				# ignore num patches installed
			$lastkey=$key;
		}
	}
	close(CMD);
	#use Data::Dumper;
	#print Dumper \%hash;
	return %hash;
}

1;

__END__

=head1 NAME

DR_Checker.pl - Check your dr database configuration

=head2 DESCRIPTION

Checks Your Log Shipping Servers For
   ... APPROPRIATE SET OF DATABASES (And Sizes if Sybase) & DB Flags
	... SYSLOGINS
	... TEMP SPACE (sybase)
	... SYSCONFIGURES
	... SQL Native Log ship status if needed

=head2 USAGE

Usage: DR_Checker.pl --OUTFILE=file --HTMLFILE=file --SYSTEM=system --USER=sa --PASSWORD=password --SRVNAME=ODBCID --OUTFILE=<backup|restore> --PRIMARY=<primary host if restore> --DEBUG --HTMLDETAIL=file --LISTEVENTS

#status_message( "\tCopying Syslogins\n" );
	#status_message( "\tComparing Temporary Space\n" );
	#status_message( "\tComparing Sysconfigures\n" );

=end

