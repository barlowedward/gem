#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);
# copyright (c) 2009 by Edward Barlow

use strict;
use Socket;
use Getopt::Long;
use Carp qw/confess/;
use CommonFunc;
use Repository;
use File::Basename;
use DBIFunc;
use Win32::DriveInfo;
use MlpAlarm;
use Sys::Hostname;
use File::Copy;
use Win32::TieRegistry;
use Do_Time;

use vars qw( $ROOT $discovery_ok_file $UPDATE_GEM $DEBUG $DOSQLPING $SILENT $BATCH_ID $USECACHE $DEVELOPER $INIT_GEM );

my(%ipcache);
my($hostname)=hostname();
my(@errors);
my($VERSION)=1.1;
my($CONFIG_FILE);
my(%ALIASES);
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";
my($starttime)=time;

############################################
# MAIN VARIABLES
############################################
my(%REPOSITORY_SQL_ADDS,%REPOSITORY_WIN32_ADDS);	# changes that can be written to config file if $UPDATE_GEM

my(%CONFIGFILE_INFO);		# Saved Info From Configfile
my(%GEM_UNREG_WIN32_SVRS);
my(%CLUSTERNODES,%IS_PRIMARY);			# key=win32 system, value= whats it a cluster of

# REGISTERED SERVERS FROM GEM CONFIG FILES
my(%GEM_REGISTERED_SYBASE,%GEM_REGISTERED_UNIX,%GEM_REGISTERED_ORACLE,%GEM_REGISTERED_SQLSVR,%GEM_REGISTERED_WIN32);

my(%MONITORED_MISSING_DSN);

# my(%GEM_SQLSVR_MATCHSTATE);		# LIST OF GEM SQL SERVERS - VALUE=DISCOVERED SERVER ITS PAIRED TO

my(%dsn_list);			#    - dsn_list(%svr) List Of ODBC Dsn's With Value=Type Of Driver

my(%unused_dsn_list);#    - values that are not mapped yet

my(%IPFAIL,%DISKFAIL);	  # set if the get of IP address or disk fails

#	  - GEM_REGISTERED_SQLSVR(%svr)     => exists if registered with gem

# from SqlPing3
#	  - IP(svr)        => ip
#	  - DISCOVERED_SVR_HOSTNAME(svr)  => hostname
#		q_connectstate	=> "FAILED","ODBC","NATIVE"
my(%q_connectstate,%q_mchn,%q_clustered,%q_product,%q_servername,%q_version,%q_ip,%NOTES,%q_netbiosname,%q_firstconnecttime);
my(%q_disks,%q_aliases);
############################################

sub usage	{print "Usage Error : SqlPortScan.pl\n
Current Argument List:
UPDATE_GEM   updates the gem confiugraiton files
USECACHE     dont reping servers - use cache from last run (faster)
SILENT       dont show progress prints
DEBUG        diagnostic mode
BATCH_ID     alert using this BATCH_ID key
DOSQLPING    run sqlping3cl.exe - this takes time - default is to use osql -L
";return "\n";}

$| =1; 				# unbuffered standard output
die "SqlPortScan.pl MUST run on windows - it detects SQL Servers & Their Windows Hosts\n" unless is_nt();
die usage("Bad Parameter List\n") unless GetOptions(
		"UPDATE_GEM"		=> \$UPDATE_GEM,
		"USECACHE"			=> \$USECACHE,
		"SILENT"				=> \$SILENT,
		"DEVELOPER"			=> \$DEVELOPER,
		"DEBUG"				=> \$DEBUG,
		"BATCH_ID=s"		=> \$BATCH_ID,
		"ROOT=s"				=> \$ROOT,
		"SUCCESSFILE=s"	=> \$discovery_ok_file,
		"DOSQLPING"			=> \$DOSQLPING);

set_gem_root_dir($ROOT) if -d $ROOT;		# 3/09 - handle fact that @inc not set during initial gem discover
die "--ROOT directory $ROOT does not exist \n" if $ROOT and ! -d $ROOT;
$ROOT=get_gem_root_dir() unless $ROOT;
die "ROOT directory $ROOT does not exist \n" unless -d $ROOT;

die "You obviously cant --USECACHE if ur running --DOSQLPING" if $DOSQLPING and $USECACHE;
die "Why would you want to update the repository if ur using cached data?\n" if $USECACHE and $UPDATE_GEM and ! $DEVELOPER;
my($SQLPING) = $ROOT."/ADMIN_SCRIPTS/bin/SQLPing3cl.exe"
	if -x $ROOT."/ADMIN_SCRIPTS/bin/SQLPing3cl.exe";

$SQLPING="$curdir/SQLPing3cl.exe" if ! defined $SQLPING and  -x "$curdir/SQLPing3cl.exe";
die "CANT find SQLPing3cl.exe - should be in $curdir\n" unless $SQLPING;

my($DISCOVERY_ROOT) =$ROOT."/discovery/".$hostname."_";
my($CONFIGFILE_ROOT)=$ROOT."/conf/A_";
$CONFIG_FILE= $DISCOVERY_ROOT."sqlportscan.dat";
die "File Exists But Is Unreadable - $CONFIG_FILE" if -e $CONFIG_FILE and ! -r $CONFIG_FILE;


my($file)=$CONFIGFILE_ROOT."CLUSTERNODES.dat";
die "CANT WRITE $file\n" if -e $file and ! -w $file;
my($file)=$CONFIGFILE_ROOT."ALIASES.dat";
die "CANT WRITE $file\n" if -e $file and ! -w $file;

heading( "SqlPortScan.pl version $VERSION" );
print "\nCopyright (c) 2009 By SQL Technologies\n\n";
print "This program creates a survey of your Sql Servers.\n";
print "No changes are made to your servers - this process is safe.\n\n";
print "SqlPortScan.pl uses native authentication and must run using an\n";
print "account with sql server connect permission (for any sql servers you want\n";
print "to monitor) and windows administrator rights (for windows monitoring).\n";
print "ODBC entries must be set up for all SQL Servers you wish to discover\n";

if( -r $CONFIG_FILE and $USECACHE ) {
	print "Config File $CONFIG_FILE exists - Reading it\n";
	read_portscan_configfile();
	read_configfiles();
} else {
	print "NOT READING $CONFIG_FILE\n   Use --USEDSAVECONFIG to reread prior discovery\n";
}
read_ip_data();
MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Gem Monitor') if $BATCH_ID;

#############  GET ODBC DATA SOURCES ####################
my($t)=do_diff(time-$starttime);
print "[$t] Getting ODBC Data Sources\n";
eval {  DBI->data_sources("ODBC");	};
if( $@ ) {
  die "Errors loading ODBC DSNs : $@\n";
} else {
  my(@dat)= grep(s/DBI:ODBC://i,DBI->data_sources("ODBC"));
  my($svrcount)=0;
  foreach (@dat) {
  		$dsn_list{$_} = GetODBCDriverName($_);
  		$unused_dsn_list{$_}=1;
  		$svrcount++;
  	}
  	print "   ... Found $svrcount ODBC Entries\n";
}
#
#############  READ SYBASE INTERFACES ####################
#
my(%SYB_INTERFACES_HOSTS,%SYB_INTERFACES_PORTS);		# *** THIS INFORMATION IS FROM THE INTERFACES FILE
my($t)=do_diff(time-$starttime);
print "\n[$t] Getting Sybase Interfaces Information\n";
my(@sybase)=dbi_get_interfaces(undef,undef,1);			# the 1 is for nodie
foreach (@sybase) {
	my($server, $host,$port)=( $_->[0], $_->[1], $_->[2] );
	$SYB_INTERFACES_HOSTS{$server} = $host;
	$SYB_INTERFACES_PORTS{$server} = $port;
	delete $unused_dsn_list{$server};			# delete dsn's for sybase servers from interfaces
	print "\tMarking dsn $server as sybase\n" if $dsn_list{$server} eq "unknown" and ! $SILENT;
	$dsn_list{$server} = "sybase" if $dsn_list{$server} eq "unknown";
}
###############################################################

#############  READ GEM CONFIG FILES ####################
my($t)=do_diff(time-$starttime);
print "[$t] Reading GEM Config Files\n";
foreach ( get_password(-type=>"sqlsvr") ) {
	$GEM_REGISTERED_SQLSVR{$_}=1;
	#  $GEM_SQLSVR_MATCHSTATE{$_}="UNMATCHED";
	delete $unused_dsn_list{$_};			# delete used sql server dsn's

	if( ! $dsn_list{$_} ) {
		$MONITORED_MISSING_DSN{$_}='sqlsvr';
		MlpHeartbeat(
			-state=>"ERROR",
			-debug=>$DEBUG,
			-monitor_program=>$BATCH_ID,
			-subsystem=>"DSN Check",
			-system=>$_,
			-message_text=>"GEM_018: Sql Server $_ Missing ODBC Dsn on ".$hostname) if $BATCH_ID;
		push @errors,"$_: Sql Server $_ Missing ODBC Dsn on ".$hostname;
	} elsif( $dsn_list{$_} ne "sqlsvr"  ) {	# appears the registry is confused
		push @errors, "$_: $_ (sqlsvr) uses odbc driver $dsn_list{$_} on $hostname";
		MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"BadIni",
				-system=>$_,
				-message_text=>"GEM_018:  $_ (sqlsvr) uses odbc driver $dsn_list{$_} on $hostname") if $BATCH_ID;
		undef $dsn_list{$_};
	} else {
		MlpHeartbeat(
			-state=>"OK",
			-debug=>$DEBUG,
			-monitor_program=>$BATCH_ID,
			-subsystem=>"DSN Check",
			-system=>$_,
			-message_text=>"GEM_018: Sql Server $_ Has An ODBC Dsn on ".$hostname) if $BATCH_ID;
	}
}
foreach ( get_password(-type=>"win32servers")) { $GEM_REGISTERED_WIN32{uc($_)}="REGISTERED"; }
foreach ( get_password(-type=>"unix") ) 		  { $GEM_REGISTERED_UNIX{$_}="REGISTERED";	}

foreach ( get_password(-type=>"sybase") ) {
	$GEM_REGISTERED_SYBASE{$_}="REGISTERED";
	delete $unused_dsn_list{$_};		# delete used sybase dsns

	if( ! $dsn_list{$_} ) {
		$MONITORED_MISSING_DSN{$_}='sybase';
		MlpHeartbeat(
			-state=>"ERROR",
			-debug=>$DEBUG,
			-monitor_program=>$BATCH_ID,
			-subsystem=>"DSN Check",
			-system=>$_,
			-message_text=>"GEM_018: Sybase Server $_ Missing ODBC Dsn on ".$hostname) if $BATCH_ID;
		push @errors,"$_: Sybase Server $_ Missing ODBC Dsn on ".$hostname;
	} elsif( $dsn_list{$_} ne "sybase"  ) {	# appears the registry is confused
		push @errors, "$_: $_ (sybase) uses odbc driver $dsn_list{$_} on $hostname";
		MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"BadIni",
				-system=>$_,
				-message_text=>"GEM_018:  $_ (sybase) uses odbc driver $dsn_list{$_} on $hostname") if $BATCH_ID;
		undef $dsn_list{$_};
	} else {
		MlpHeartbeat(
			-state=>"OK",
			-debug=>$DEBUG,
			-monitor_program=>$BATCH_ID,
			-subsystem=>"DSN Check",
			-system=>$_,
			-message_text=>"GEM_018: Sybase Server $_ Has An ODBC Dsn on ".$hostname) if $BATCH_ID;
	}
}

foreach ( get_password(-type=>"oracle") ) {
	$GEM_REGISTERED_ORACLE{$_}="REGISTERED";
	delete $unused_dsn_list{$_};		# delete monitored oracle dsns

	if( ! $dsn_list{$_} ) {
		$MONITORED_MISSING_DSN{$_}='oracle';
		MlpHeartbeat(
			-state=>"ERROR",
			-debug=>$DEBUG,
			-monitor_program=>$BATCH_ID,
			-subsystem=>"DSN Check",
			-system=>$_,
			-message_text=>"GEM_018: Oracle Server $_ Missing ODBC Dsn on ".$hostname) if $BATCH_ID;
		push @errors,"$_: Server $_ Missing ODBC Dsn on ".$hostname;
	} elsif( $dsn_list{$_} ne "oracle"  ) {	# appears the registry is confused
		push @errors, "$_: $_ (oracle) uses odbc driver $dsn_list{$_} on $hostname";
		MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"BadIni",
				-system=>$_,
				-message_text=>"GEM_018:  $_ (oracle) uses odbc driver $dsn_list{$_} on $hostname") if $BATCH_ID;
		undef $dsn_list{$_};
	} else {
		MlpHeartbeat(
			-state=>"OK",
			-debug=>$DEBUG,
			-monitor_program=>$BATCH_ID,
			-subsystem=>"DSN Check",
			-system=>$_,
			-message_text=>"GEM_018: Oracle Server $_ Has An ODBC Dsn on ".$hostname) if $BATCH_ID;
	}
}

my($t)=do_diff(time-$starttime);
print "[$t] GEM Config File Read Completed\n";

my(%DISCOVERED_SVR_IP,%DISCOVERED_SQLSVR_GEM_NAME);
my($tfile)=$DISCOVERY_ROOT."SqlPing3cl.out";
my($ofile)="SqlPing3cl.out";
if( $DOSQLPING ) {			#or ! -e $tfile ) {
	unlink $ofile if -r $ofile;
	my($t)=do_diff(time-$starttime);
	print "[$t] Executing $SQLPING -scantype stealth -output $ofile\n";
	open(OUTF,"$SQLPING -scantype stealth -output $ofile 2>&1 |");
	my($HASABORTED)=0;
	while(<OUTF>) {
		if( /Unhandled Exception: / ) { # carp
			print "\n*****************\n";
			print "* SQLPING HAS ENCOUNTERED A PROBLEM EXECUTING AND MUST CLOSE\n";
			print "*****************\n\n";
			$HASABORTED=1;
			last;
		}
		print "(SqlPing3cl.exe) ".$_;
	}
	close(OUTF);
	if( $HASABORTED ) {		# run osql -L ???
		print "SQLPING appears to have aborted - sigh - dont sweat it - we will discover stuff another way\n";
	}
	if( ! -e $ofile ) {
		die "FATAL ERROR: NO SqlPing output file found - $ofile\n";
	}
	copy($ofile, $tfile) or die "Cant copy $ofile, $tfile $!\n";

} else {
	print "NOTE : SQLPING WAS NOT RUN (pass --DOSQLPING to run)";
}

if( -e $tfile ) {
	open(O,$tfile) or die "Cant read $tfile $!";
	my($lineno)=0;
	while(<O>) {
		next if $lineno++==0;
		my($ip,$port,$server,$instance)=split(/,/);
		if( $port or $instance ) {
			warn "WARNING - Port=$port Instance=$instance => These are not handled by this script currently\n";
			next;
		}
		my($nm)=$server;
		$DISCOVERED_SQLSVR_GEM_NAME{$nm} = "";
		$DISCOVERED_SVR_IP{uc($nm)}=$ip if $ip =~/\d\.\d/;
		$ipcache{uc($nm)}=$ip if $ip =~/\d\.\d/;

		#my($pk_ip) = inet_aton($ip);
		#my($name,$aliases,$addrtype,$length,@addrs)=gethostbyaddr($pk_ip,AF_INET);
		#$name=~s/\..*$//;
		#$DISCOVERED_SVR_HOSTNAME{$nm}=$name;
		#print "SERVER=$server IP=$ip HOST=$name\n" if $DEBUG;
		print "SERVER=$server IP=$ip\n" if $DEBUG;
	}
	close(O);
}
# we are going to run this each time despite cache directives - its fast
open(OUTF2,"osql -L |");
while(<OUTF2>) {
	chop;
	next if /^\s*$/ or /^Servers:/ or /local/;
	s/^\s+//;
	s/\s+$//;
	next if /\\/;
	$DISCOVERED_SQLSVR_GEM_NAME{$_} = "";
	$DISCOVERED_SVR_IP{uc($_)} = GetIpAddress($_);
}
close(OUTF2);

# LOOP THROUGH THE REGISTERED GEM SERVERS & COLLECT INFO FOR THEM
warning( "Pinging Registered SQL Servers" );
foreach ( sort keys %GEM_REGISTERED_SQLSVR ) {
	SqlPing($_);
}

# COMPARISON ALGORITHM
heading( "Starting Results Comparison" );
foreach my $dsvr ( sort keys %DISCOVERED_SQLSVR_GEM_NAME ) {			# IS IT IN GEM?  Same IP or NOT???
	if( $GEM_REGISTERED_SQLSVR{$dsvr} ) {
		$q_ip{$dsvr} = $DISCOVERED_SVR_IP{uc($dsvr)} if ! $q_ip{$dsvr} and $DISCOVERED_SVR_IP{uc($dsvr)} =~ /\d/;
		$DISCOVERED_SVR_IP{uc($dsvr)} = $q_ip{$dsvr} if $q_ip{$dsvr} and ! $DISCOVERED_SVR_IP{uc($dsvr)};
		print "WARNING: matched $dsvr ip=$q_ip{$dsvr} != ip=$DISCOVERED_SVR_IP{$dsvr} \n"
			if $q_ip{$dsvr} ne $DISCOVERED_SVR_IP{uc($dsvr)}
			and $DISCOVERED_SVR_IP{uc($dsvr)} =~ /\d/
			and $q_ip{$dsvr} =~ /\d/;
		# $GEM_SQLSVR_MATCHSTATE{$dsvr} =  $dsvr;
		$DISCOVERED_SQLSVR_GEM_NAME{$dsvr} = $dsvr;
	} else {
		# LOOP THROUGH BIOS NAMES
		my($t)=do_diff(time-$starttime);
		foreach my $xsvr ( keys %GEM_REGISTERED_SQLSVR ) {
			if( uc($dsvr) eq uc($q_mchn{$xsvr})) {
				print "[$t]  => Matched $xsvr to $dsvr\n" unless $SILENT;
				# $GEM_SQLSVR_MATCHSTATE{$xsvr} =  $dsvr;
				$DISCOVERED_SQLSVR_GEM_NAME{$dsvr} = $xsvr;
				$q_ip{$dsvr} = $q_ip{$xsvr} if $q_ip{$xsvr}=~ /\d\.\d/;
				set_aliases( $dsvr, $DISCOVERED_SQLSVR_GEM_NAME{$dsvr}, $xsvr);
				last;
			} elsif( uc($dsvr) eq uc($q_netbiosname{$xsvr})) {
				print "[$t]  => Matched $xsvr to $dsvr\n" unless $SILENT;
				# $GEM_SQLSVR_MATCHSTATE{$xsvr} =  $dsvr;
				$DISCOVERED_SQLSVR_GEM_NAME{$dsvr} = $xsvr;
				$q_ip{$dsvr} = $q_ip{$xsvr} if $q_ip{$xsvr}=~ /\d\.\d/;
				set_aliases( $dsvr, $DISCOVERED_SQLSVR_GEM_NAME{$dsvr}, $xsvr);
				last;
			}
		}

		# can we not fit $dsvr - if so look at aliases
		#if( ! $GEM_SQLSVR_MATCHSTATE{$dsvr} ) {
		#	foreach (get_aliases($dsvr)) {
		#		next unless $GEM_REGISTERED_SQLSVR{$_};
		#		print " => Matched $dsvr to $_\n";
				#$GEM_SQLSVR_MATCHSTATE{$dsvr} = $_;
		#	}
		#}
	}
}

# FIND DUPLICATE GEM SERVERS
heading( "DUPLICATE MONITORED SERVERS (REMOVE 1 FROM CONFIGFILE)" );
my($dupcnt)=0;
foreach my $dsvr ( sort keys %GEM_REGISTERED_SQLSVR ) {
	# next if $GEM_SQLSVR_MATCHSTATE{$dsvr} ne "UNMATCHED";
	my($is_duplicate)='FALSE';
	foreach my $xsvr ( sort keys %GEM_REGISTERED_SQLSVR ) {
		next if $dsvr eq $xsvr;

		if( $q_mchn{$dsvr} eq $q_mchn{$xsvr}
		and $q_clustered{$dsvr} eq $q_clustered{$xsvr}
		and $q_product{$dsvr} eq $q_product{$xsvr}
		and $q_version{$dsvr} eq $q_version{$xsvr}
		and $q_ip{$dsvr} eq $q_ip{$xsvr}
		and $q_netbiosname{$dsvr} eq $q_netbiosname{$xsvr} ) {
			$dupcnt++;
			print "DUPLICATE SERVER - $dsvr eq $xsvr\n";
			set_aliases( $dsvr, $xsvr);		# probably unnecessary
			# $GEM_SQLSVR_MATCHSTATE{$dsvr} = $xsvr if $GEM_SQLSVR_MATCHSTATE{$dsvr} eq "UNMATCHED";
			# $GEM_SQLSVR_MATCHSTATE{$xsvr} = $dsvr if $GEM_SQLSVR_MATCHSTATE{$xsvr} eq "UNMATCHED";
			$is_duplicate='TRUE';
			MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"Duplicates",
				-system=>$dsvr,
				-message_text=>"GEM_018: It Appears $dsvr = $xsvr (Both Names Are Monitored)") if $BATCH_ID;
			push @errors, "$dsvr: It Appears $dsvr = $xsvr (Both Names Are Monitored)";
		}
	}
	if( $is_duplicate ne "FALSE" ) {
		foreach my $xsvr ( split(/\|/, $ALIASES{$dsvr} )) {
			next unless $xsvr eq $dsvr;
			if( $q_mchn{$dsvr} eq $q_mchn{$xsvr}
			and $q_clustered{$dsvr} eq $q_clustered{$xsvr}
			and $q_product{$dsvr} eq $q_product{$xsvr}
			and $q_version{$dsvr} eq $q_version{$xsvr}
			and $q_ip{$dsvr} eq $q_ip{$xsvr}
			and $q_netbiosname{$dsvr} eq $q_netbiosname{$xsvr} ) {
				$dupcnt++;
				print "DUPLICATE SERVER - $dsvr eq $xsvr\n";
				set_aliases( $dsvr, $xsvr);		# probably unnecessary
				# $GEM_SQLSVR_MATCHSTATE{$dsvr} = $xsvr if $GEM_SQLSVR_MATCHSTATE{$dsvr} eq "UNMATCHED";
				# $GEM_SQLSVR_MATCHSTATE{$xsvr} = $dsvr if $GEM_SQLSVR_MATCHSTATE{$xsvr} eq "UNMATCHED";
				$is_duplicate='TRUE';
				MlpHeartbeat(
					-state=>"ERROR",
					-debug=>$DEBUG,
					-monitor_program=>$BATCH_ID,
					-subsystem=>"Duplicates",
					-system=>$dsvr,
					-message_text=>"GEM_018: It Appears $dsvr = $xsvr (Both Names Are Monitored)") if $BATCH_ID;
				push @errors, "$dsvr: It Appears $dsvr = $xsvr (Both Names Are Monitored)";
			}
		}
	}

	if( $is_duplicate ne "TRUE" ) {
		MlpHeartbeat(
			-state=>"OK",
			-debug=>$DEBUG,
			-monitor_program=>$BATCH_ID,
			-subsystem=>"Duplicates",
			-system=>$dsvr,
			-message_text=>"$dsvr: No Duplicate Monitored Servers") if $BATCH_ID;
	}
}
print "NO DUPLICATE SERVERS FOUND\n" unless $dupcnt;

#heading( "COMPARISON MAPPINGS" ) if $DEBUG;
#printf "%20s %s\n","DISCOVERED","GEM MONITORED SERVERS" if $DEBUG;
#foreach my $dsvr ( sort keys %DISCOVERED_SQLSVR_GEM_NAME ) {
	#if( $DISCOVERED_SQLSVR_GEM_NAME{$dsvr} ) {
		#printf "%20s %s\n",$dsvr,$DISCOVERED_SQLSVR_GEM_NAME{$dsvr} if $DEBUG;
		#$GEM_SQLSVR_MATCHSTATE{$DISCOVERED_SQLSVR_GEM_NAME{$dsvr}} = "MATCHED"
		#	if $GEM_SQLSVR_MATCHSTATE{$DISCOVERED_SQLSVR_GEM_NAME{$dsvr}};
	#}
#}

#foreach my $dsvr ( sort keys %GEM_SQLSVR_MATCHSTATE ) {
#	print "UNMatched $dsvr \n" unless $GEM_SQLSVR_MATCHSTATE{$dsvr} eq "MATCHED";
#}

heading( "GEM MONITORED SERVERS MISSING ODBC DSN on $hostname" );
my($count)=0;
foreach ( sort keys %MONITORED_MISSING_DSN ) {
		$count++;
		printf "%15s (%6s)\n",$_,$MONITORED_MISSING_DSN{$_};
}
print "NO MISSING ODBC DSNS" unless $count;
print "\n";

warning( "TESTING NEWLY DISCOVERED SQL SERVERS" );
foreach ( sort keys %DISCOVERED_SQLSVR_GEM_NAME ) {
	if( ! $DISCOVERED_SQLSVR_GEM_NAME{$_} ) {
		# $dsn_list{$_} = "CAN BE USED" if $dsn_list{$_} eq "UNUSED";
		# warning1("Trying $_ ");
		SqlPing($_);
		delete $unused_dsn_list{$_};		# delete newly discovered dsn's
	}
}

warning( "TESTING UNREGISTERED ODBC DSNs" );
foreach my $dsn ( sort keys %unused_dsn_list ) {
	next if $SYB_INTERFACES_HOSTS{$dsn} or $dsn_list{$dsn} eq "sybase" or $dsn_list{$dsn} eq "oracle";
	if( $dsn_list{$dsn} eq "sqlsvr"  ) {
		my($found)=0;
		foreach ( get_aliases($dsn) ) { $found++ if $dsn_list{$_} ne "UNUSED"; }
		$dsn_list{$dsn}="ALIASED" if $found;
		if( $dsn =~ /Visio/i or $dsn =~ /dbase/i or $dsn=~/Excel Files/ or $dsn=~/MS Access/) {
			print " ==> skipping $dsn\n" unless $SILENT;
			$q_connectstate{$dsn} = "FAILED";
			next;
		}
		# warning1("Trying $dsn - please ignore any sql error messages you see here");
		SqlPing($dsn,1) unless $found;
	}
}

fix_aliases();			# remove clustered nodes
foreach my $cl (keys %CLUSTERNODES ) {
	next if $GEM_REGISTERED_WIN32{$cl};
	# print "DBG DBG - New UnregWinServer $cl [ clusternode of $CLUSTERNODES{$cl} ]\n";
	$GEM_UNREG_WIN32_SVRS{$cl}=1;
}


warning( "Collecting Win32 Information For REGISTERED servers" );
my($count)=0;
foreach( sort keys %GEM_REGISTERED_WIN32 ) {
	printf " => %-20s ",$_;
	$q_disks{$_} = Win32Ping($_);
	$q_ip{$_}    = GetIpAddress($_);
	print "\n" if ++$count % 3 == 0;
}

my($count)=0;
warning( "Collecting Win32 Information For Discovered UnREGISTERED servers & Clusternodes" );
my(@messages)=();
foreach my $usvr ( sort keys %GEM_UNREG_WIN32_SVRS ) {
	my($found)=0;
	foreach ( get_aliases($usvr) ) {
		if( $GEM_REGISTERED_WIN32{uc($_)} ) {
			delete $GEM_UNREG_WIN32_SVRS{$usvr};
			print "Ignoring unreg win32 $usvr -> alias of win32 $_\n";	# if $DEBUG;
			$found++;
			last;
		}
	}
	next if $found;
	printf " => %-20s",$usvr;
	$q_ip{$usvr} = GetIpAddress($usvr);
	print "\n" if ++$count % 3 == 0;
	if( ! $CLUSTERNODES{$usvr} ) {
		foreach ( keys %q_ip ) {
			next if $_ eq $usvr or $q_ip{$_} ne $q_ip{$usvr} or $CLUSTERNODES{$_};
			delete $GEM_UNREG_WIN32_SVRS{$usvr};
			push @messages, "\nIgnoring $usvr (same IP as $_)\n";	# if $DEBUG;
			set_aliases( $usvr, $_ );
			$found++;
			$q_disks{$usvr} = $q_disks{$_} unless $q_disks{$usvr};
			$q_disks{$usvr} = Win32Ping($usvr)  unless $q_disks{$usvr};
			last;
		}
		next if $found;
	}
	$q_disks{$usvr} = Win32Ping($usvr);
}
print @messages;

warning( "Starting Report Generation" );
print_monitored_server_report();
write_clustered_nodes_report();
print_new_server_report();

print_connectivity_report();
print_aliases_report();
print_odbc_dsn_noconnect_report();
print_windows_server_reports();

#heading( "DRAFT DISKS.dat" );
my($file)=$DISCOVERY_ROOT."disks.dat";
open(AL,">".$file) or die "Cant read $file $!";
foreach( sort keys %q_disks ) {
	my($flag)="[unregistered]" unless $GEM_REGISTERED_WIN32{$_};
	if( $DISKFAIL{$_} ) {
#		print $_, " FAILED $flag\n";
		print AL $_, " FAILED\n";
	} else {
#		print    $_,' ',$q_disks{$_}," $flag\n" if $q_disks{$_};
		print AL $_,' ',$q_disks{$_},"\n" if $q_disks{$_};
	}
}
close(AL);

heading( "ERRORMESSAGES" );
foreach( sort @errors ) {
	my($h,$m)=split(/:/);
	printf "%20s %s\n",$h,$m;
}

print "\n";
heading( "Saving Results" );
write_ip_dat();
write_portscan_configfile() if $CONFIG_FILE;
update_config_files();
my($t)=do_diff(time-$starttime);
print "[$t] Program completed successfully at ".localtime(time)."\n\n";
MlpBatchJobEnd() if $BATCH_ID;
heading( "Program Completed" );

if( $discovery_ok_file ) {
	open(X, "> $discovery_ok_file") or die "Cant Write $discovery_ok_file $!\n";
	print X localtime(time);
	close(X);
}

exit(0);

sub update_config_files {
	return unless $UPDATE_GEM;

	warning( "UPDATING CONFIG FILES\n" );

	use Data::Dumper;

	print "SAVING WIN32 CHANGES TO THE GEM REPOSITORY\n";
	add_edit_item( -type=>'win32servers', -op=>'update', -values=>\%REPOSITORY_WIN32_ADDS );
	print " -> changes applied\n";

	print "SAVING SQL Server CHANGES TO THE GEM REPOSITORY\n";
	add_edit_item( -type=>'sqlsvr', -op=>'update', -values=>\%REPOSITORY_SQL_ADDS );
	print " -> changes applied\n";
}

sub register_win32_svr{
	my($s)=@_;
}
sub register_sql_svr{
	my($s)=@_;
}

sub heading {
	my($str)=@_;
	my($t)=do_diff(time-$starttime);
	print "\n[".$t."] *****************************\n[".$t."] ".uc($str)."\n[".$t."] *****************************\n";
}

sub warning {	my($str)=@_;
	my($t)=do_diff(time-$starttime);
	print "\n[".$t."] >>> \n[".$t."] >>> ".uc($str)."\n[".$t."] >>>\n";
}

sub warning1 {	my($str)=@_;
	my($t)=do_diff(time-$starttime);
	print "[".$t."] >>> ".$str."\n";
}

sub write_ip_dat {

	# exclude cluster nodes
	my(%EXCLUDE);
	foreach ( keys %CLUSTERNODES ) { $EXCLUDE{ $CLUSTERNODES{$_} } = 1; }
	foreach ( keys %q_ip ) {
		next if $ipcache{uc($_)} or $EXCLUDE{$_};
		warn "q_ip($_) exists as $q_ip{$_}\n";
		$ipcache{uc($_)} = $q_ip{$_};
	};
	my($file)=$CONFIGFILE_ROOT."IP.dat";
	print "WRITING $file\n";
	open(AB,">".$file) or die "Cant write $file $!";
	print AB "#A_IP.dat\n";
	print AB "#*** THIS FILE IS NOT HAND EDITABLE ***\n";
	print AB "#*** CREATED BY SQLPORTSCAN.PL ***\n";
	#heading( "DRAFT A_IP.dat" );
	foreach( sort keys %ipcache ) {
		#if( $IPFAIL{$_} ) {
		#	print AB $_, " FAILED\n";
		#} else {
			print AB $_,' ',$ipcache{$_},"\n" if $ipcache{$_} and ! $EXCLUDE{$_};
		#}
	}
	close(AB);
}

sub read_ip_data {
	my($file)=$CONFIGFILE_ROOT."IP.dat";
	if( -r $file ) {
		open(XX,$file) or die "Cant read $file $!\n";
		while(<XX>) {
			next if /^#/ or /^\s*$/;
			my($svr,$ipadds,$flag)=split(/\s+/,$_);
			if( $ipadds eq "FAILED" ) {
				$IPFAIL{$svr}=1;
			} else {
				$q_ip{$svr} = $ipadds;
				$ipcache{ uc($svr) } = $ipadds;
			}
		}
		close(XX);
	}
}
sub read_configfiles {
	# DISKS.dat
	my($file)=$DISCOVERY_ROOT."disks.dat";
	if( -r $file ) {
		open(XX,$file) or die "Cant read $file $!\n";
		while(<XX>) {
			my($svr,$disks,$flag)=split(/\s+/,$_);
			if( $disks eq "FAILED" ) {
				$DISKFAIL{$svr}=1;
			} else {
				$q_disks{$svr} = $disks;
			}
		}
		close(XX);
	}

	$file=$CONFIGFILE_ROOT."CLUSTERNODES.dat";
	if( -r $file ) {
		open(XX,$file) or die "Cant read $file $!\n";
		while(<XX>) {
			next if /^#/;
			my($cl,$svr,$flag)=split(/\s+/,$_);
			if( defined $flag and $flag=~/PRIM/ ) {
				$IS_PRIMARY{$svr}="PRIMARY";
			}
			$CLUSTERNODES{$svr}=$cl;
		}
		close(XX);
	}

	$file=$CONFIGFILE_ROOT."ALIASES.dat";
	if( -r $file ) {
		open(AX,$file) or die "Cant read $file $!";
		while(<AX>) {
			next if /^#/;
			my(@aliases) = split(/\s+/,$_);
			set_aliases(@aliases);
		}
		close(AX);
	}
}

sub get_aliases {
	my($sys)=@_;
	return () unless $ALIASES{$sys};
	return (split(/\|/,$ALIASES{$sys}));
}

sub fix_aliases {		# remove clustered nodes from aliases
	foreach my $akey (keys %ALIASES) {
		my(@rc);
		foreach ( get_aliases($akey) ) {	push @rc, $_ unless $CLUSTERNODES{$_};	}
		if( $#rc <= 0 ) {
			delete $ALIASES{$akey};
		} else {
			$ALIASES{$akey} = join("|",@rc);
		}
	}
	foreach ( keys %CLUSTERNODES ) { delete $ALIASES{$_}; }
}

sub set_aliases {
	my(@sys)=@_;
	print "DBGDBG ==> set alias ( ",join(",",@sys)," )\n";
	my(%done);
	my(@list);
	foreach (@sys) {
		s/\s//g;
		next if /^\s*$/;
		next if $done{$_};
		next if $CLUSTERNODES{$_};
		$done{$_}=1;
		push @list,$_;
	}
	return if $#list<=0;

	foreach my $a ( @list ) {
		if( $ALIASES{$a} ) {
			my(@b)=split(/\|/,$ALIASES{$a});
			foreach (@b){ $done{$_}=1 unless /^\s*$/; }
		}
	}
	my($t)=do_diff(time-$starttime);
	print "[$t]   ==> set alias ( ",join(",",keys %done)," )\n" unless $SILENT;
	my($str)=join("|",keys %done);
	foreach my $a ( @list ) {
		print "  set_aliases $a = $str\n" if $DEBUG;
		$ALIASES{$a}=$str;
	}
}

sub set_win32 {
	my($svr)=@_;
	return unless $svr;
	confess( "DBG DBG - SET UNREG $svr" ) unless $svr;
	$GEM_UNREG_WIN32_SVRS{uc($svr)} = 1 unless $GEM_REGISTERED_WIN32{uc($svr)};
}


sub GetIpAddress {
	my($svr,$onetryonly)=@_;
	return $ipcache{$svr} if $ipcache{$svr};
	return undef if $svr =~ /\\/;

	$ipcache{uc($svr)}=$q_ip{$svr} if $q_ip{$svr}=~/\d\.\d/;
	return $q_ip{$svr} if $q_ip{$svr}=~/\d\.\d/;

	$ipcache{uc($svr)} = $DISCOVERED_SVR_IP{$svr} if $DISCOVERED_SVR_IP{$svr}=~/\d\.\d/;
	return $DISCOVERED_SVR_IP{$svr} if $DISCOVERED_SVR_IP{$svr}=~/\d\.\d/;

	return undef if $IPFAIL{$svr};

	foreach ( get_aliases($svr) ) {
		next if $_ eq $svr;
		my($ip)=GetIpAddress($_);
		print "  ==> GetIpAddress($svr) = $ip\n"  if $ip =~ /\d\.\d/ and $DEBUG;
		$ipcache{uc($svr)} = $ip if $ip =~ /\d\.\d/;
		return $ip if $ip =~ /\d\.\d/;
	}

	# ok now get the ip the hard way
	my(@address)=gethostbyname($svr) or confess "Cant resolve $svr $!\n";
	@address= map { inet_ntoa($_) } @address[4..$#address];
	print "  ==> GetIpAddress($svr) = ",join(" ",@address),"\n" if $DEBUG;
	$IPFAIL{$svr}=1 if $#address<0;
	print "DBG DBG: IP FAILED FOR $svr\n" if $#address<0;
	$ipcache{uc($svr)} =  join(/\|/,@address);
	return  join(/\|/,@address);
}

sub Win32Ping {
	my($svr,$onetryonly)=@_;
	return undef	unless $svr=~/\w/;
	return $q_disks{$svr} if $q_disks{$svr};

	print " => Fetching $svr Disks (not cached)\n" if $DEBUG;
	foreach ( get_aliases($svr) ) { return $q_disks{$_} if $q_disks{$_}; }
	return undef if $DISKFAIL{$svr};

	local $SIG{ALRM} = sub { die "CommandTimedout"; };
	my(@disks);
	eval {
		alarm(2);
   	foreach my $drive ( 'c$','d$','e$','f$' ) {
   		last if  $CLUSTERNODES{$svr} and $drive eq 'd$';   # clusternodes only on c$
			my($url)="\\\\".$svr."\\".$drive;
			my(@diskdat)=Win32::DriveInfo::DriveSpace($url);
			push @disks,$drive if defined $diskdat[5];
		}
   	alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			print "Command Timed Out\n";
			$DISKFAIL{$svr}=1;
		} else {
			print "Command Returned $@\n";
		}
	}
	$DISKFAIL{$svr}=1 if $#disks<0;
	return join(",",@disks);
}

sub SqlPing {
	my($svr,$onetryonly)=@_;
	return unless $svr=~/\w/;

	#if( $dsn_list{$svr} eq "OTHER" ) {
	#	print " ==> ignoring $svr (not sql server)\n";
	#	$q_connectstate{$svr} = "FAILED";
	#	return;
	#}

	my($t)=do_diff(time-$starttime);
	if( $CONFIGFILE_INFO{$svr} ) {
		my(@d)=split(/\|/,$CONFIGFILE_INFO{$svr});
		print "[$t] ==> SqlPing($svr) [cached] \n" unless $SILENT;
		$q_connectstate{$svr}=$d[0];
		# print "DBG DBG - SETTING MCHN $svr = $d[1] \n";
		$q_mchn{$svr}=$d[1];
		set_win32($q_netbiosname{$svr});
		$q_clustered{$svr}=$d[2];
		$q_product{$svr}=$d[3];
		$q_servername{$svr}=$d[4];
		$q_version{$svr}=$d[5];
		$q_ip{$svr}=$d[6];
		$q_netbiosname{$svr}=$d[7];
		set_win32($q_netbiosname{$svr});
		$q_firstconnecttime{$svr}=$d[8];
		$q_aliases{$svr}=$d[9];
		$q_disks{$svr}=$d[10];
		if( $q_clustered{$svr} ) {			# CLUSTERED NODE
			get_cluster_info_via_osql($svr);
		}

		set_aliases($svr, $q_netbiosname{$svr}, $q_mchn{$svr}, $q_servername{$svr}) unless $q_connectstate{$svr} eq "FAILED";
		return;
	}

	print "[$t] ==> SqlPing($svr)\n" unless $SILENT;

	die "SERVER $svr (",$q_mchn{$svr}," PINGED TWICE??? That shouldnt happen... Something is wierd...\n",Dumper \%q_mchn
		if $q_mchn{$svr};

	my($q)="select ServerProperty('MachineName'),ServerProperty('IsClustered'),ServerProperty('ProductLevel'),ServerProperty('ServerName'),ServerProperty('ProductVersion'),ServerProperty('ComputerNamePhysicalNetBIOS')";
	my($q2)="select convert(varchar(30),ServerProperty('MachineName')),convert(varchar(30),ServerProperty('IsClustered')),convert(varchar(30),ServerProperty('ProductLevel')),convert(varchar(30),ServerProperty('ServerName')),	convert(varchar(30),ServerProperty('ProductVersion')),convert(varchar(30),ServerProperty('ComputerNamePhysicalNetBIOS'))";

	my($q3)='DECLARE @ValueOut varchar(20)
	EXEC master..xp_regread @rootkey=\'HKEY_LOCAL_MACHINE\', @key=\'SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName\',@value_name=\'ComputerName\', @value=@ValueOut OUTPUT
SELECT @ValueOut';

	# CLUSTER NODES
	# my($q5)='select * from ::fn_servershareddrives()';
	my($login,$pass) = get_password(-name=>$svr, -type=>'sqlsvr');
	$login='sa' unless $login;
	$pass='XXX' unless $pass;
	my($rc) = dbi_connect(-srv=>$svr,-login=>$login,-password=>$pass,-quiet=>1);
	if( ! $rc ) {
		if( $onetryonly ) {
			print "     ==> Cant connect to $svr via ODBC - continuing\n" unless $SILENT;
			$q_connectstate{$svr} = "FAILED";
			return;
		}

		print "[$t] ==> Cant ODBC to $svr - Trying osql " unless $SILENT;
		$q_connectstate{$svr} = "FAILED";

		my($query) = "osql -E -n -S$svr -Q\"print \'LOGIN OK\'\"";
		print "\n => $query\n" if $DEBUG;
		open(Q,"$query |") or die "Cant run osql query\n";
		my($rc)='FAILED';
		while(<Q>) {
			chomp;
			print "RC=$_\n" if $DEBUG
						and ! /has occurred while establishing a connection/
						and ! /When connecting to SQL Server 2005/
						and ! /the fact that under the default settings SQL/
						and ! /^Server /
						and ! /Provider: Could not open a connection to SQL/
						and ! /^connections\./;
			$rc = $q_connectstate{$svr} = "NATIVE"  if /LOGIN OK/;
		}
		close(Q);

		if( $rc eq "NATIVE" ) {	# we got in via native auth!
			print " => osql Ok! Digging in!\n" unless $SILENT;
			$query = "osql -w2000 -s~ -E -n -S$svr -Q\"$q2\"";
			print $query,"\n" if $DEBUG;
			open(Q,"$query |") or die "Cant run osql query\n";
			my($row)=0;
			while(<Q>) {
				$row++;
				next if $row<=2;
				s/\s//g;
				chomp;

				my($m,$c,$pr,$se,$pv,$nb)=split(/~/);
				# print __LINE__," DBG DBG - SETTING MCHN $svr = $m \n";
				$q_mchn{$svr} = uc($m);
				set_win32(uc($m));
				$q_clustered{$svr} = $c;
				$q_product{$svr} = $pr;
				$q_servername{$svr} = $se;
				$q_version{$svr} = $pv;
				$nb=~s/\s//g;
				$q_netbiosname{$svr} = uc($nb);
				$q_netbiosname{$svr}=$q_mchn{$svr} unless $q_netbiosname{$svr} or $q_clustered{$svr};
				set_win32($q_netbiosname{$svr});

				set_aliases($svr, $q_netbiosname{$svr}, $q_mchn{$svr}, $q_servername{$svr});

				my($packedip) = inet_aton($m);		#'adsdr001.ad.mlp.com');
				my($unpackedip)= join(".",unpack('C4', $packedip));
				$q_ip{$svr} = $unpackedip;

				if( $c ) { # CLUSTERED!
					$query = "osql -w2000 -s~ -E -n -S$svr -Q\"exec master..xp_cmdshell 'ping -a 127.0.0.1'\"";
					print $query,"\n" if $DEBUG;
					open(I,"$query |") or die "Cant run osql $query\n";
					my($r1)=0;
					while(<I>)	 {
						$r1++;
						last if /Msg 15281/;
						next if $r1<=2;
						chomp;
						s/\s*~//g;
						s/\s*$//;
						print "PING: $_\n" if $DEBUG;
					}
					close(I);

					$query = "osql -w20000 -s~ -E -n -S$svr -Q\"$q3\"";
					print $query,"\n" if $DEBUG;
					my($r2)=0;
					open(J,"$query |") or die "Cant run osql $query\n";
					while(<J>) {
						$r2++;
						next if $r2<=2;
						chomp;
						#s/\s*~//g;
						print "Q3: $_\n" if $DEBUG;
						s/\s//g;
						$q_netbiosname{$svr}=uc($_);
						set_win32($q_netbiosname{$svr});
						last;
					}
					close(J);
				}
				last;
			}
			close(Q);
			if( $q_clustered{$svr} ) {			# CLUSTERED NODE
				get_cluster_info_via_osql($svr);
			}
		} else {
			print " => \'osql -E $svr\' Failed!\n" unless $SILENT;
		}
		return;
	}

	$q_connectstate{$svr} = "ODBC";
	my(@rc)=dbi_query(-db=>"master",-query=>$q);
	foreach (@rc) {
		my($m,$c,$pr,$se,$pv,$nb)=dbi_decode_row($_);

		if( ! $se ) {
			print " => $svr IS NOT A SQL SERVER!\n";
			$q_connectstate{$svr} = "FAILED";
			return;
		}

		# print __LINE__," DBG DBG - SETTING MCHN $svr = $m \n";
		$q_mchn{$svr} = uc($m);
		set_win32($q_mchn{$svr});
		$q_clustered{$svr} = $c;
		$q_product{$svr} = $pr;
		$q_servername{$svr} = $se;
		$q_version{$svr} = $pv;
		$nb=~s/\s//g;
		$q_netbiosname{$svr}=uc($nb);
		$q_netbiosname{$svr}=$q_mchn{$svr} unless $q_netbiosname{$svr} or $q_clustered{$svr};
		set_win32($q_netbiosname{$svr});

		set_aliases($svr, $q_netbiosname{$svr}, $q_mchn{$svr}, $q_servername{$svr});

		my($packedip) = inet_aton($m);		#'adsdr001.ad.mlp.com');
		my($unpackedip)= join(".",unpack('C4', $packedip));
		$q_ip{$svr} = $unpackedip;
	}

	if( ! $q_netbiosname{$svr} ) {
		my(@rc)=dbi_query(-db=>"master",-query=>$q3);
		print $q3,"\n" if $DEBUG;
		foreach (@rc) {
			my($k,$v)=dbi_decode_row($_);
			$v=~s/\s*//g;
			next unless $v;
			print "Netbiosname= $v\n" if $DEBUG;
			$q_netbiosname{$svr}=uc($v);
			set_win32($q_netbiosname{$svr});
		}
	}

	if( ! $q_netbiosname{$svr} ) {
		my(@rc)=dbi_query(-db=>"master",-query=>'exec master..xp_cmdshell \'ping -a 127.0.0.1\'');
		my($lineno)=0;
		foreach (@rc) {
			$lineno++;
			if( $lineno==2 ) {
				my($rc)=dbi_decode_row($_);
				print "PINGER: ",$rc,"\n" if $DEBUG;
				next unless $rc=~/\s*Pinging/i;
				$rc=~s/^\s*Pinging\s*//i;
				my($nh,$junk)=split(/\s/,$rc,2);
				$nh=~s/\..+//;
				next unless $nh;
				$nh=~s/\s//g;
				print "Netbiosname=$nh\n" if $DEBUG;
				$q_netbiosname{$svr}=uc($nh);
				set_win32($q_netbiosname{$svr});

				last;
			}
		}
	}

	if( $q_clustered{$svr} ) {			# CLUSTERED NODE
		get_cluster_info_via_osql($svr);
	}
	dbi_disconnect();

	return $rc;
}

sub get_cluster_info_via_osql {
	my($svr)=@_;
	my($t)=do_diff(time-$starttime);
	print "[$t]   ==> get_cluster_info_via_osql($svr)\n" unless $SILENT;
	#	FOR SOME REASON I CANT GET PERL CONNECTIVITY SO WE WILL USE OSQL FOR THIS
	my($q4)='select * from ::fn_virtualservernodes()';
	print " => using osql to fetch clustered nodes\n" if $DEBUG;
	my($query) = "osql -w2000 -s~ -E -n -S$svr -Q\"$q4\"";
	#print $query,"\n" if $DEBUG;
	open(Q,"$query |") or die "Cant run osql query\n";
	my($row)=0;
	my(@nodes)=();
	while(<Q>) {
		$row++;
		next if $row<=2;
		s/\s//g;
		chomp;
		next if /^\s*$/ or /rows\s*affected/  or /row\s*affected/;
		push @nodes,$_;
		$CLUSTERNODES{$_} = $svr;
		$IS_PRIMARY{$_}="PRIMARY" if $_ eq $q_netbiosname{$svr};
		my($t)=do_diff(time-$starttime);
		print "[$t]   ==> set clusternode $svr = $_ $IS_PRIMARY{$_}\n" unless $SILENT;
		set_win32($svr);
	}
	close(Q);

	#$q_aliases{$svr}=join(",",@nodes);
	#set_aliases($svr,@nodes);

	my($q5)='select * from ::fn_servershareddrives()';
	$query = "osql -w2000 -s~ -E -n -S$svr -Q\"$q5\"";
	#print $query,"\n" if $DEBUG;
	open(Q,"$query |") or die "Cant run osql query\n";
	($row)=0;
	my(@drives);
	while(<Q>) {
		$row++;
		next if $row<=2;
		s/\s//g;
		chomp;
		next if /^\s*$/ or /rows\s*affected/  or /row\s*affected/;;
		push @drives,lc($_)."\$";
		print "DRIVE = $_\n" if $DEBUG;
	}
	$q_disks{$svr}=join(",",@drives);
	my($t)=do_diff(time-$starttime);
	print "[$t]   ==> set drives $svr ( ",$q_disks{$svr}," )\n" unless $SILENT;

	close(Q);
}

sub GetODBCDriverName {
	my($svr)=@_;
	my($key)="HKEY_LOCAL_MACHINE\\SOFTWARE\\ODBC\\ODBC.INI\\ODBC Data Sources\\\\".$svr;
	my($rc) = $Registry->{$key};
	return "unknown" if ! $rc;
	return "sybase" if $rc =~ /sybase/i or $rc =~ /adaptive server/i;
	return "oracle" if $rc =~ /oracle/i;
	return "sqlsvr" if $rc =~ /SQL Server/i or $rc =~ /SQL Native/i;
	return "unknown" if $rc =~ /Microsoft Access/;
	warn "WARNING : Undefined ODBC Driver $rc\nThis is a code problem (and can be ignored) - contact GEM Support to add this driver to the list\n";
	return "unknown";
}

sub read_portscan_configfile {
	print "NOT " unless $USECACHE;
	my($t)=do_diff(time-$starttime);
	print "[$t] Reading PortScan Prior Data\n";
	return unless $USECACHE;
	open(CFG,$CONFIG_FILE) or die "Cant open $CONFIG_FILE $!\n";
	while(<CFG>) {
		chop;
		my($svr,$data)=split(/\|/,$_,2);
		$CONFIGFILE_INFO{$svr}=$data;
		print "=> read $svr\n" if $DEBUG;
	}
	close (CFG);
}

sub write_portscan_configfile {
	print "WRITING $CONFIG_FILE\n";
	open(FD,"> ".$CONFIG_FILE) or die "Cant open $CONFIG_FILE $!\n";
	foreach ( sort keys %q_connectstate ) {
		# print "DBG DBG = writing $_\n";
		$q_firstconnecttime{$_}=time unless $q_firstconnecttime{$_};
		print FD join("|",
		($_,
			$q_connectstate{$_},$q_mchn{$_},$q_clustered{$_},$q_product{$_},
			$q_servername{$_},$q_version{$_},$q_ip{$_},
			$q_netbiosname{$_},$q_firstconnecttime{$_},$q_aliases{$_},$q_disks{$_})),"\n";
	}
	close (FD);
}


sub print_connectivity_report {
	heading( "CONNECTIVITY OF REMAINING ODBC DSNs" );
	my($rcnt)=0;
	foreach ( sort keys %dsn_list ) {
		if( $dsn_list{$_} eq "unused" ) {
			next if $q_connectstate{$_} eq "FAILED";
			if( $rcnt==0 ) {
				printf "%16s %8s %2s %4s %3s %15s %15s %s\n", "SERVER", "CONN", "CL", "", "VER", "BIOS", "NOTES";
				printf "%16s %8s %2s %4s %3s %15s %15s %s\n", "NAME", "VIA", "", "", "", "NAME", "";
			}
			$rcnt++;
			#my($hasdsn)="YES" if $dsn_list{$_};
			my($ver)="?";
			$ver = "2000" if $q_version{$_}=~/^8/;
			$ver = "2005" if $q_version{$_}=~/^9/;
			$ver = "2008" if $q_version{$_}=~/^10/;
			$NOTES{$_}.="NEEDS PATCH" if $q_product{$_} ne "SP4" and $ver eq "2000";
			$NOTES{$_}.="NEEDS PATCH" if $q_product{$_} ne "SP3" and $ver eq "2005";
			printf "%16s %8s %2s %4s %3s %15s %15s %s\n", $_, $q_connectstate{$_}, $q_clustered{$_},
				$ver, $q_product{$_}, $q_netbiosname{$_}, $NOTES{$_};
		}
	}
	print "NO ISSUES\n" unless $rcnt;
}

sub print_new_server_report {
	heading( "NEW SQL SERVERS CAPABLE OF BECOMING MONITORED SERVERS" );
	my($has_printed_hdr)=0;
	foreach ( sort keys %DISCOVERED_SQLSVR_GEM_NAME ) {
		if( ! $DISCOVERED_SQLSVR_GEM_NAME{$_} ) {
			next if $q_connectstate{$_} eq "FAILED";
			if( ! $has_printed_hdr ) {
				printf "%16s %8s %2s %4s %3s %15s %15s %s\n", "SERVER", "CONN", "CL", "", "VER", "BIOS", "NOTES";
				printf "%16s %8s %2s %4s %3s %15s %15s %s\n", "NAME", "VIA", "", "", "", "NAME", "";
			}
			$has_printed_hdr++;
			my($ver)="?";
			$ver = "2000" if $q_version{$_}=~/^8/;
			$ver = "2005" if $q_version{$_}=~/^9/;
			$ver = "2008" if $q_version{$_}=~/^10/;
			$NOTES{$_}.="NEEDS PATCH" if $q_product{$_} ne "SP4" and $ver eq "2000";
			$NOTES{$_}.="NEEDS PATCH" if $q_product{$_} ne "SP3" and $ver eq "2005";
			printf "%16s %8s %2s %4s %3s %15s %15s %s\n", $_, $q_connectstate{$_}, $q_clustered{$_},$ver, $q_product{$_}, $q_netbiosname{$_}, $NOTES{$_};

			if( $UPDATE_GEM ) {
				$REPOSITORY_SQL_ADDS{$_."|X_IS_CLUSTERED"}=$q_clustered{$_};
				$REPOSITORY_SQL_ADDS{$_."|X_VERSION"}=$ver;
				$REPOSITORY_SQL_ADDS{$_."|X_SERVICEPACK"}=$q_product{$_};
				$REPOSITORY_SQL_ADDS{$_."|X_HOSTNAME"}=$q_netbiosname{$_};
				$REPOSITORY_SQL_ADDS{$_."|SERVER_TYPE"} = "PRODUCTION";
			}

			MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"IsRegistered",
				-system=>$_,
				-message_text=>"GEM_018: SQL Server $_ Is UnMonitored") if $BATCH_ID;
			push @errors, "$_: SQL Server $_ Is UnMonitored";
		} else {
			MlpHeartbeat(
				-state=>"OK",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"IsRegistered",
				-system=>$_,
				-message_text=>"GEM_018: SQL Server $_ is Registered") if $BATCH_ID;
		}
	}
	if( ! $has_printed_hdr ) {
		print "NO SERVERS FOUND\n";
	}
}

sub print_windows_server_reports {
	heading("UNREGISTERED WINDOWS SERVERS - NO ACCESS");
	foreach my $usvr ( sort keys %GEM_UNREG_WIN32_SVRS ) {
		die "DBG DBG empty $usvr \n" unless $usvr;
		if( ! $q_disks{$usvr} ) {
			printf "%20s [%15s] Cant Monitor As No Native Access\n",$usvr,$q_ip{$usvr};
			MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"WinCheck",
				-system=>$usvr,
				-message_text=>"GEM_018: Win32 Server $usvr Is UnMonitored") if $BATCH_ID;
			push @errors, "$usvr: Win32 Server $usvr Is UnMonitored";
		}
	}

	heading("UNREGISTERED WINDOWS SERVERS - READY TO MONITOR");
	foreach my $usvr ( sort keys %GEM_UNREG_WIN32_SVRS ) {
		die "DBG DBG empty $usvr \n" unless $usvr;
		if( $q_disks{$usvr} ) {
			printf "%20s [%15s] Monitor Ok - Disks=[%s]\n",$usvr,$q_ip{$usvr},$q_disks{$usvr};
			register_win32_svr($usvr);
			MlpHeartbeat(
				-state=>"OK",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"WinCheck",
				-system=>$usvr,
				-message_text=>"GEM_018: Win32 Server $usvr is Registered") if $BATCH_ID;
			if( $UPDATE_GEM ) {
				# print __LINE__," DBG DBG : SAVING $usvr WIN32\n";
				$REPOSITORY_WIN32_ADDS{$usvr."|SERVER_TYPE"}   		= "PRODUCTION";
				$REPOSITORY_WIN32_ADDS{$usvr."|DISKS"}  = $q_disks{$usvr};
				$REPOSITORY_WIN32_ADDS{$usvr."|X_IP"}   = $q_ip{$usvr};
			}
		}
	}

	heading("REGISTERED WINDOWS SERVERS - ACCESS OK");
	foreach( sort keys %GEM_REGISTERED_WIN32 ) {
		if( $q_disks{$_} ) {
			printf "%20s Ip=%-15s Disks=[%s]\n",$_,$q_ip{$_},$q_disks{$_};
			if( $UPDATE_GEM ) {
				# print __LINE__," DBG DBG : SAVING $_ WIN32\n";
				$REPOSITORY_WIN32_ADDS{$_."|DISKS"}  = $q_disks{$_};
				$REPOSITORY_WIN32_ADDS{$_."|X_IP"}   = $q_ip{$_};
			}
			MlpHeartbeat(
				-state=>"OK",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"WinCheck",
				-system=>$_,
				-message_text=>"GEM_018: Win32 Server $_ is Registered") if $BATCH_ID;
		}
	}
	print "\n";

	heading("REGISTERED WINDOWS SERVERS - NO ACCESS");
	foreach( sort keys %GEM_REGISTERED_WIN32 ) {
		if( ! $q_disks{$_} ) {
			printf "%20s Ip=%-15s Cant Monitor As No Native Access\n",$_,$q_ip{$_};
			MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"WinCheck",
				-system=>$_,
				-message_text=>"GEM_018: Registered Win32 $_ -> NO Access from $hostname") if $BATCH_ID;
			push @errors, "$_: Registered Win32 $_ -> NO Access from $hostname";
		}
	}
	print "\n";
}

sub print_aliases_report {
	warning( "UPDATING A_ALIASES.dat" );
	my($file)=$CONFIGFILE_ROOT."ALIASES.dat";
	die "CANT WRITE $file\n" if -e $file and ! -w $file;

	my($rc)=open(AX,">".$file);
	if( ! $rc ) {
		print "********************\n";
		print "ERROR: Cant write $file $!\n";
		print "********************\n";
		return;
	}

	print AX "#A_ALIASES.dat\n";
	print AX "#*** THIS FILE IS NOT HAND EDITABLE ***\n";
	print AX "#*** CREATED BY SQLPORTSCAN.PL ***\n";

	foreach my $s ( sort keys %ALIASES ) {
		next unless $s =~ /\w/;
		next unless $ALIASES{$s};
		my(@aliases) = get_aliases($s);
		print AX $s," ";
		foreach ( @aliases ) { print AX "$_ " unless $_ eq $s; }
		print AX "\n";
	}
	close(AX);
}

sub 	print_odbc_dsn_noconnect_report {
	heading( "UNABLE TO CONNECT DISCOVERED SQL SERVERS" );
	($count)=0;
	foreach ( sort keys %DISCOVERED_SQLSVR_GEM_NAME ) {
			next unless $q_connectstate{$_} eq "FAILED" or $CLUSTERNODES{$_};
			if( $dsn_list{$_} ) {
				printf "%15s (%6s) ", $_, $dsn_list{$_};
			} else {
				printf "%15s  %6s  ", $_, '';
			}
			print "\n" if ++$count % 3 == 0;
	}
	print "\n";
	print "\n";
}

sub write_clustered_nodes_report {

	my($file)=$CONFIGFILE_ROOT."CLUSTERNODES.dat";
	die "CANT WRITE $file\n" if -e $file and ! -w $file;
	heading( "CLUSTERED NODES REPORT ($file)" );
	my($rc)=open(AX,">".$file);
	if( ! $rc ) {
		print "********************\n";
		print "ERROR: Cant write $file $!\n";
		print "********************\n";
		return;
	}
	print AX "#A_CLUSTERNODES.dat\n";
	print AX "#*** THIS FILE IS NOT HAND EDITABLE ***\n";
	print AX "#*** CREATED BY SQLPORTSCAN.PL ***\n";

	printf "%-20s %-20s %s\n", "ACTIVE NODE", "DB SERVER", "IS_PRIMARY?";
	my($lastsvr)='dummy';
	foreach my $cl ( sort keys %CLUSTERNODES ) {
		if( $lastsvr ne $CLUSTERNODES{$cl} ) {		# add a newline for ease of viewing
			print "\n" if $lastsvr ne 'dummy';
			$lastsvr = $CLUSTERNODES{$cl};
		}
		printf "%-20s %-20s %s\n",   $CLUSTERNODES{$cl},$cl,$IS_PRIMARY{$cl};
		printf AX "%s %s %s\n",      $CLUSTERNODES{$cl},$cl,$IS_PRIMARY{$cl};

	}
	close(AX);
	print "\n";
}

sub print_monitored_server_report {
	# LOOP THROUGH ALL SERVERS AND PRINT A REPORT
	heading( "MONITORED SQL SERVERS" );
	printf "%16s %8s %2s %4s %3s %15s %15s %s\n", "SERVER", "CONN", "CL", "", "VER", "BIOS", "NOTES";
	printf "%16s %8s %2s %4s %3s %15s %15s %s\n", "NAME", "VIA", "", "", "", "NAME", "";
	foreach ( sort keys %GEM_REGISTERED_SQLSVR ) {
		my($ver)="?";
		$ver = "2000" if $q_version{$_}=~/^8/;
		$ver = "2005" if $q_version{$_}=~/^9/;
		$ver = "2008" if $q_version{$_}=~/^10/;

		if( $q_product{$_} ne "SP4" and $ver eq "2000" ) {
			$NOTES{$_}.="NEEDS PATCH";
			MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"Patch Check",
				-system=>$_,
				-message_text=>"GEM_018: Sql 2000 Server Needs Patch (current=$q_product{$_})") if $BATCH_ID;
			push @errors, "$_: Sql 2000 Server Needs Patch (current=$q_product{$_})";
		} elsif( $q_product{$_} ne "SP3" and $ver eq "2005" ) {
			$NOTES{$_}.="NEEDS PATCH";
			MlpHeartbeat(
				-state=>"ERROR",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"Patch Check",
				-system=>$_,
				-message_text=>"GEM_018: Sql 2005 Server Needs Patch (current=$q_product{$_})") if $BATCH_ID;
			push @errors, "$_: Sql 2005 Server Needs Patch (current=$q_product{$_})";
		}	else {
			MlpHeartbeat(
				-state=>"OK",
				-debug=>$DEBUG,
				-monitor_program=>$BATCH_ID,
				-subsystem=>"Patch Check",
				-system=>$_,
				-message_text=>"GEM_018: Patch Level OK") if $BATCH_ID;
		}

		printf "%16s %8s %2s %4s %3s %15s %15s %s\n", $_, $q_connectstate{$_}, $q_clustered{$_},
				$ver, $q_product{$_}, $q_netbiosname{$_}, $NOTES{$_};

		if( $UPDATE_GEM ) {
			$REPOSITORY_SQL_ADDS{$_."|X_IS_CLUSTERED"}=$q_clustered{$_};
			$REPOSITORY_SQL_ADDS{$_."|X_VERSION"}=$ver;
			$REPOSITORY_SQL_ADDS{$_."|X_SERVICEPACK"}=$q_product{$_};
			$REPOSITORY_SQL_ADDS{$_."|X_HOSTNAME"}=$q_netbiosname{$_};
			$REPOSITORY_SQL_ADDS{$_."|SERVER_TYPE"} = "PRODUCTION";
		}
	}
}

__END__

=head1 NAME

SqlPortScan.pl - sql port scan

=head2 SYNOPSIS

This program uses a variety of methods to discover your sql server configuration.  No guessing is done for passwords,
native authentication only.

=head2 USAGE

[G:/dev/ADMIN_SCRIPTS/bin] perl SqlPortScan.pl --?
Usage Error : SqlPortScan.pl

Current Argument List:
        UPDATE_GEM   updates the gem confiugraiton files
        USECACHE     dont reping servers - use cache from last run (faster)
        SILENT       dont show progress prints
        DEBUG        diagnostic mode
        BATCH_ID     alert using this BATCH_ID key
        DOSQLPING    run sqlping3cl.exe - this takes time  - default is to use osql -L

=head2 CONFIG FILES

The discovery directive will end up holding files named

 a) ${HOSTNAME}_SqlPing3cl.out		- output of SqlPing3cl.exe
 b) ${HOSTNAME}_sqlportscan.out		- cached data from internal pings

This program writes the following system managed configuration files

	Win32Cluster.dat
	Win32Aliases.dat

=head2 SQLPING3 NOTES

This utility uses SqlPing3cl.exe which is shipped with GEM.  A standalone version
can be found at http://www.sqlsecurity.com/Tools/FreeTools/tabid/65/Default.aspx

Copied from http://www.sqlsecurity.com/Tools/FreeTools/tabid/65/Default.aspx

Here is the alpha release of the command-line version of SQLPing3. Please provide any feedback here at the download area for any errors or comments you have concerning this version. Keep in mind that this alpha release only contains the high-level switches. The ability to disable or alter the scan options will come later once the application is stabilized. For now the command-line switches are as follows:

SQLPing3cl - SQLPing3 Command Line version - alpha release

Syntax: SqlPing3cl.exe -scantype [range,list,stealth] -StartIP [IP] -EndIP [IP]
-IPList [FileName] -UserList [FileName] -PassList [FileName] -Output [FileName]

Currently -scantype is the only required parameter. IPs will default to 127.0.0.1 if nothing is provided. The default output file is output.txt.

=cut
