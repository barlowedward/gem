create procedure clear_possible_alarms
as
begin
	update Heartbeat set internal_state='C' where internal_state='P'
	update Event     set internal_state='C' where internal_state='P'

	-- delete Blackout where enddate<getdate()
end
go
