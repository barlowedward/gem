create trigger INV_credential_notes_upd_trg on INV_credential_notes for update
as
begin
	insert INV_credential_notes_audit
	select 'INS',suser_name(),getdate(),* from inserted
	-- where suser_name() != 'gemalarms'

	insert INV_credential_notes_audit
	select 'DEL',suser_name(),getdate(),* from deleted
	-- where suser_name() != 'gemalarms'


/* -- is_ldap_user	= c.is_ldap_user, */
update INV_principal_map
set  	is_unuseable 		= c.is_unuseable,
		is_serviceaccount = c.is_serviceaccount,
	   is_error    		= c.is_error,
	   is_operator 		= c.is_operator,
	   is_ldap_group		= c.is_ldap_group,
	   is_shared			= c.is_shared,
	   user_notes 	= isnull(m.user_notes,c.user_notes)
from 	INV_principal_map m, inserted c
where 	m.principal_name = c.credential_name
and   	m.principal_type = c.credential_type

update INV_principal_map
set  	is_unuseable 		= c.is_unuseable,
		is_serviceaccount = c.is_serviceaccount,
	   is_error    		= c.is_error,
	   is_operator 		= c.is_operator,
	   is_ldap_group		= c.is_ldap_group,
	   is_shared			= c.is_shared,
	   user_notes 	= isnull(m.user_notes,c.user_notes)
from 	INV_principal_map m, inserted c
where 	m.credential_name = c.credential_name
and   	m.credential_type = c.credential_type

if update(is_ldap_user)
begin
	update INV_principal_map
	set   	is_ldap_user  = 'Y',
		principal_type='ldap user',
		principal_name= c.ldap_user
	from 	INV_principal_map m, inserted c
	where m.principal_name = c.credential_name
	and   m.principal_type = c.credential_type
	and   c.is_ldap_user='Y'
	and   c.ldap_user is not null

	update INV_principal_map
	set   	is_ldap_user  = c.is_ldap_user,
		principal_type= m.credential_type,
		principal_name= m.credential_name
	from 	INV_principal_map m, inserted c
	where m.principal_name = c.credential_name
	and   m.principal_type = c.credential_type
	and   c.is_ldap_user='N'
	and   m.is_ldap_user='Y'

	/*
	update INV_principal_map
	set   	is_ldap_user  = c.is_ldap_user,
				principal_type='ldap user',
				principal_name=isnull(c.ldap_user,'unmapped by user')
	from 	INV_principal_map m, inserted c
	where m.credential_name = c.credential_name
	and   m.credential_type = c.credential_type
	*/
end

/* if update(is_ldap_group) */

end
go
