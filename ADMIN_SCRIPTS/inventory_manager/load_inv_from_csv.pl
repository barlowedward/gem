#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Long;
use MlpAlarm;
use Inventory;
use Text::CSV;

use vars qw( $FROMFILE $LOAD_SYSTEM_NAME_FIELD $LOAD_SYSTEM_TYPE $LOAD_SYSTEM_TYPE_FIELD $SYTYPFLD $BATCHID $DEBUG );

my($VERSION)=1.0;

$|=1;

sub usage { 	return "usage: load_inv_from_csv.pl -FROMFILE=file -LOAD_SYSTEM_NAME_FIELD=nm --SYTYPFLD=nm --BATCHID=key\n";  }

die usage() if $#ARGV<0;
die "Bad Parameter List $!\n".usage() unless GetOptions(  "SYTYPFLD=s"	=> \$SYTYPFLD,
	"FROMFILE=s" 	=> \$FROMFILE,
	"LOAD_SYSTEM_NAME_FIELD=s" => \$LOAD_SYSTEM_NAME_FIELD,
	"LOAD_SYSTEM_TYPE=s" 		=> \$LOAD_SYSTEM_TYPE,
	"LOAD_SYSTEM_TYPE_FIELD=s" 	=> \$LOAD_SYSTEM_TYPE_FIELD,
	"BATCHID=s"		=> \$BATCHID,
	"DEBUG"			=>	\$DEBUG);

die "NO FROMFILE" 	unless $FROMFILE;
die "NO LOAD_SYSTEM_NAME_FIELD" 	unless $LOAD_SYSTEM_NAME_FIELD;
die "NO LOAD_SYSTEM_TYPE" 	unless $LOAD_SYSTEM_TYPE or $LOAD_SYSTEM_TYPE_FIELD;

$LOAD_SYSTEM_NAME_FIELD = lc($LOAD_SYSTEM_NAME_FIELD);
$LOAD_SYSTEM_TYPE_FIELD = lc($LOAD_SYSTEM_TYPE_FIELD);

my($t)=localtime();
print "load_inv_from_csv.pl\n";
print " -- run at $t\n";
print " -- fromfile=$FROMFILE\n";
print " -- sysnamefld=$LOAD_SYSTEM_NAME_FIELD\n";
print " -- systypefld=$LOAD_SYSTEM_TYPE_FIELD\n";
print "\n";
my $csv = Text::CSV->new();

open(FF,$FROMFILE) or die "Cant read $FROMFILE\n";
my(@inlines);
while(<FF>) {
	chomp;chomp;chomp;	# soooo hungry...
	s/\r//g;
	next if /^,,,,,/;		# probably a blank line
	push @inlines,$_;
}
close(FF);

# print "Splitting - ",join("\n",split(/,/,$inlines[0])),"\n";
my(%i_have_deleted_types);

my($CUR_LOAD_SYSTEM_TYPE)=$LOAD_SYSTEM_TYPE; # the system type - either from $LOAD_SYSTEM_TYPE or the field $LOAD_SYSTEM_TYPE_FIELD

my($lineno)=0;
my(@hdr);
my($unknownnm)=1;
my(%lcnamesused);
$lcnamesused{''}=1;
$lcnamesused{$CUR_LOAD_SYSTEM_TYPE.":".'null'}=1;
my(@warnings);
my($inserted_system_cnt)=0;
my($dupname_system_cnt)=0;
my($lastname)='xxxxabc';		# cant happen - to prevent dup name warnings from row repeats
foreach(@inlines) {
	print "READ LINE $_" if $DEBUG;
	my $status  = $csv->parse($_);         # parse a CSV string into fields
	print "STATUS of the parse=$status\n" if $DEBUG;
	my @columns = $csv->fields();          # get the parsed fields
	print "NUMCOL=",$#columns," COL=",join(" ",@columns),"\n" if $DEBUG;
	if( $lineno++ == 0 ) {
		#print "READ LINE $_\n";
		foreach ( @columns ) { push @hdr, lc($_); }
		#print "HDR=",join(" ",@hdr),"\n";
		# @hdr = @columns;
		my($fnd)=0;

		foreach (@hdr) {
			# print "Comparing $_ to ",lc($LOAD_SYSTEM_NAME_FIELD),"\n";
			next unless $_ eq lc($LOAD_SYSTEM_NAME_FIELD);
			$fnd++;
		}
		die __LINE__," CANT FIND $LOAD_SYSTEM_NAME_FIELD NAME FIELD ON LINE 1\n FILE=$FROMFILE\n".join("\t",@hdr)."\n" if $fnd==0;
		die __LINE__," MULTIPLE $LOAD_SYSTEM_NAME_FIELD NAME FIELD ON LINE 1\n FILE=$FROMFILE\n".join("\t",@hdr)."\n" if $fnd>1;

		if( $LOAD_SYSTEM_TYPE_FIELD ) {
			my($fnd)=0;
			foreach (@hdr) {
				next unless $_ eq $LOAD_SYSTEM_TYPE_FIELD;
				$fnd++;
			}
			die __LINE__," CANT FIND $LOAD_SYSTEM_TYPE_FIELD TYPE FIELD ON LINE 1\n FILE=$FROMFILE\n".join("\t",@hdr)."\n" if $fnd==0;
			die __LINE__," MULTIPLE $LOAD_SYSTEM_TYPE_FIELD TYPE FIELD ON LINE 1\n FILE=$FROMFILE\n". join("\t",@hdr)."\n" if $fnd>1;
		}
		next;
	}
	my($id)=0;
	my(%dathash);
	my($sysname);
	foreach( @columns ) {
		s/\s+$//;
		if( $hdr[$id] eq $LOAD_SYSTEM_NAME_FIELD ) {
			$sysname=$_;
		} elsif( $hdr[$id] eq $LOAD_SYSTEM_TYPE_FIELD ) {
			if( $LOAD_SYSTEM_TYPE ) {
				$CUR_LOAD_SYSTEM_TYPE= lc($_)."_".$LOAD_SYSTEM_TYPE;
			} else {
				$CUR_LOAD_SYSTEM_TYPE=$_;
			}
		} else {
			$dathash{$hdr[$id]} = $_ if $_ ne "";
		}
		$id++;
		# print $hdr[$id++]," => $_\n";
	}
	next if $CUR_LOAD_SYSTEM_TYPE =~ /^\s*\_${LOAD_SYSTEM_TYPE}/; 	# it was a blank

	# ignore blank or "NULL" names
	if( $sysname =~ /^\s*$/ or $sysname=~ /^null$/i ) {
		my($m)= "Empty Name \'$sysname\' on line $lineno\n";
		print $m;
		push @warnings,$m;
		next;
	}

	if( $CUR_LOAD_SYSTEM_TYPE =~ /^\s*$/ or $CUR_LOAD_SYSTEM_TYPE=~ /^null$/i  ) {
		my($m)= "No System Type on line $lineno\n";
		print $m;
		push @warnings,$m;
		next;
	}

	die "NO LOAD_SYSTEM_TYPE " unless defined $CUR_LOAD_SYSTEM_TYPE;

	# use Data::Dumper;
	# print Dumper \%dathash;
	if( $lcnamesused{lc($CUR_LOAD_SYSTEM_TYPE.":".$sysname)} ) {
		my($m)= "Duplicate Name: \'$sysname\' on line $lineno";
		my($sysnameold)=$sysname;
		# create a new name!
		my($ccc)=0;
		while(1) {
			$ccc++;
			next if $lcnamesused{ lc($CUR_LOAD_SYSTEM_TYPE.":".$sysname." [$ccc]") };
			$sysname .= " [$ccc]";
			$dupname_system_cnt++;
			last;
		}
		$m.=" (renamed to $sysname)\n";
		print $m;
		push @warnings,$m if $lastname ne $sysnameold;
		$lastname=$sysnameold;
	} else {
		$lastname=$sysname;
	}

	if( ! defined $i_have_deleted_types{$CUR_LOAD_SYSTEM_TYPE} ) {
		_querynoresults("delete INV_system where system_type='".$CUR_LOAD_SYSTEM_TYPE."'");
		_querynoresults("delete INV_system_attribute where system_type='".$CUR_LOAD_SYSTEM_TYPE."'");
		$i_have_deleted_types{$CUR_LOAD_SYSTEM_TYPE} = 1;
	}

	printf "InvUpdSystem( -system_name=%14s  -system_type=%s)\n",$sysname,$CUR_LOAD_SYSTEM_TYPE;
	$lcnamesused{lc($CUR_LOAD_SYSTEM_TYPE.":".$sysname)} = $sysname;
	$inserted_system_cnt++;
	InvUpdSystem(
			-system_name 			=> 	$sysname,
			-system_type			=> 	$CUR_LOAD_SYSTEM_TYPE,
			-clearattributes		=>		1,	# UNNEEDED (well actually not - i got dups without it...
			-attribute_category	=> 	$CUR_LOAD_SYSTEM_TYPE,
			-attributes 			=> 	\%dathash );
}

print "\nProcess Completed\n";
print "\t$dupname_system_cnt duplicates ignored\n";
print "\t$inserted_system_cnt systems inserted\n";

foreach (@warnings) { print "*** Warning *** ".$_; }
print "Exiting...\n";
exit(0);
