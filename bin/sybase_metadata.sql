set nocount on
go
create table #SERVERPROPERTIES ( Property varchar(30),	Value nvarchar(80) )
go
Insert #SERVERPROPERTIES values ('BOOTTIME',		convert(nvarchar(80),@@BOOTTIME))
go
Insert #SERVERPROPERTIES values ('ERRORLOG',		convert(nvarchar(80),@@ERRORLOG))
go
Insert #SERVERPROPERTIES values ('PAGESIZE',		convert(nvarchar(80),@@maxpagesize))
go
Insert #SERVERPROPERTIES values ('VERSION_AS_INTEGER',		convert(nvarchar(80),@@VERSION_AS_INTEGER))
go
Insert #SERVERPROPERTIES values ('VERSION',		convert(nvarchar(80),@@VERSION))

Insert #SERVERPROPERTIES 
select 'HOSTNAME', substring(address_info, 1, charindex(" ", address_info)-1) 
	from master..syslisteners where address_info not like '%loopback%' and address_info not like '%127.0.0.1%'

Insert #SERVERPROPERTIES 
select 'PORT', substring(address_info, charindex(" ", address_info)+1, 10) 
	from master..syslisteners where address_info not like '%loopback%' and address_info not like '%127.0.0.1%'

Insert #SERVERPROPERTIES 
select 'NETADDRESS',convert(nvarchar(80),address_info) 
from master..syslisteners 
where net_type='tcp'
and address_info not like '%loopback%' and address_info not like '%127.0.0.1%'

Insert #SERVERPROPERTIES 
select distinct "CONFIG:"+upper(x.comment), CharValue=c.value2
from     master..sysconfigures x,
        master..syscurconfigs c
where    c.config=x.config
and   c.config in (418, 402, 396, 307, 293,  259, 127, 126, 122, 117, 114, 356, 378, 381, 393, 123, 124, 104, 170, 113 )

Insert #SERVERPROPERTIES 
select 'SERVERNAME',isnull(s.srvname, @@servername)   
from master..sysservers s, master..sysservers s2  
where s2.srvclass = 0 and s.srvnetname = s2.srvnetname and s.srvname != 'loopback'

Insert #SERVERPROPERTIES 
select 'BACKUPSERVER',srvnetname from master..sysservers where srvname = 'SYB_BACKUP'

select * from #SERVERPROPERTIES order by Property

drop table #SERVERPROPERTIES
go
