#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# grab perf data and reload it

use strict;
use Getopt::Std;
use File::Basename;
use DBIFunc;
use MlpAlarm;

use vars qw($opt_d $opt_I $opt_O);
die usage("Bad Parameter List") unless getopts('dI:O:');

die "Must pass input key with -I"  unless defined $opt_I;
die "Must pass output key with -O" unless defined $opt_O;
die "-O cant be teh same as -I" if $opt_I eq $opt_O;

my($curdir)=dirname($0);

print "Deleting Old Stats $opt_O\n";
my($q)="delete PER_lastintstatistics where perf_key='".$opt_O."'";
_querynoresults($q,1);
my($q)="delete PER_intstatistics where perf_key='".$opt_O."'";
_querynoresults($q,1);

my($query)="select * from PER_intstatistics where perf_key='".$opt_I."' order by perf_time asc";
my(@rc)=_querywithresults($query,1);
my($i)=0;my($j)=0;my($k)=0;
my($t0)=time;
foreach (@rc) {
	my($sname,$stype,$k2,$k3,$k4,$pk,$ptime,$weight,$v1,$v2,$v3,$v4,$v5,$v6)=dbi_decode_row($_);

	my($q2)="exec PER_saveintstatrate '".$sname."','".$stype."',\n\t'".$k2."','".$k3."','".$k4."',\n\t'".$opt_O."','".$ptime."',\n\t".$weight.",".$v1;
	$q2.=",".$v2 if $v2;
	$q2.=",".$v3 if $v3;
	$q2.=",".$v4 if $v4;
	$q2.=",".$v5 if $v5;
	$q2.=",".$v6 if $v6;
	$q2.="\n";

	_querynoresults($q2,1);
	$j++;
   next if $k++<20;
	$k=0;
	print ".";
	next if $i++ < 50;
	$i=0;
	print "   ",time-$t0," secs\n $j ] ";
	$t0=time;

