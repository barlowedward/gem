
-- For chart
IF OBJECT_ID('tempdb..#am_request_count', 'U') IS NULL
BEGIN
    CREATE TABLE #am_request_count (collection_time datetime PRIMARY KEY, request_count numeric (28, 1));
END;

IF OBJECT_ID ('tempdb..#am_fingerprint_stats_snapshots') IS NULL
BEGIN
    CREATE TABLE #am_fingerprint_stats_snapshots (
        collection_time datetime,
        query_fingerprint binary(64),
        plan_fingerprint binary(64),
        plan_count int,
        creation_time datetime,
        last_execution_time datetime,
        execution_count bigint,
        total_worker_time_ms bigint,
        min_worker_time_ms bigint,
        max_worker_time_ms bigint,
        total_physical_reads bigint,
        min_physical_reads bigint,
        max_physical_reads bigint,
        total_logical_writes bigint,
        min_logical_writes bigint,
        max_logical_writes bigint,
        total_logical_reads bigint,
        min_logical_reads bigint,
        max_logical_reads bigint,
        total_clr_time bigint,
        min_clr_time bigint,
        max_clr_time bigint,
        total_elapsed_time_ms bigint,
        min_elapsed_time_ms bigint,
        max_elapsed_time_ms bigint,
        total_completed_execution_time_ms bigint,
        -- A specific example of a plan with this plan fingerprint
        sample_sql_handle binary(64),
        sample_plan_handle binary(64),
        sample_statement_start_offset int,
        sample_statement_end_offset int,
        -- This fingerprint's rank by each of our perf metrics
        plan_count_rank int,
        cpu_rank int,
        physical_reads_rank int,
        logical_reads_rank int,
        logical_writes_rank int,
        max_duration_rank int,
        execution_count_rank int,
        batch_text nvarchar(max),
        dbname nvarchar(128)
    );
    CREATE CLUSTERED INDEX cidx ON #am_fingerprint_stats_snapshots (collection_time, query_fingerprint, plan_fingerprint)
END;
