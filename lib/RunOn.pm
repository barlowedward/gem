# copyright (c) 2003-2011 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

package RunOn;

use vars qw($VERSION @ISA @EXPORT);
use Exporter;
use strict;

$VERSION= 1.0;

@ISA      = (	'Exporter'	);
@EXPORT   = ( 	'mysql_is_ok_on_unix','sybase_is_ok_on_unix','sqlsvr_is_ok_on_unix','oracle_is_ok_on_unix','gemsys_is_ok_on_unix', 
				 	'mysql_is_ok_on_win32','sybase_is_ok_on_win32','sqlsvr_is_ok_on_win32','oracle_is_ok_on_win32','gemsys_is_ok_on_win32' );

# return MSWin32 or Unix
sub mysql_is_ok_on_unix 	{ 	return 1; 	}
sub sybase_is_ok_on_unix 	{ 	return 1; 	}
sub sqlsvr_is_ok_on_unix 	{  return 0;	}
sub oracle_is_ok_on_unix 	{  return 0; }
sub gemsys_is_ok_on_unix 	{	return 1; 	}

sub mysql_is_ok_on_win32 	{ 	return 0; 	}
sub sybase_is_ok_on_win32 	{ 	return 0; 	}
sub sqlsvr_is_ok_on_win32 	{  return 1;	}
sub oracle_is_ok_on_win32 	{  return 1; }
sub gemsys_is_ok_on_win32 	{	return 0; 	}

1;

__END__

=head1 NAME

RunOn.pm - Common Function Library

=head2 DESCRIPTION

Cant this monitoring host run the code

=head2 SUMMARY

 is_mysql_is_ok_on_unix()   - returns 1/0 if this monitoring server can run mysql code
 is_sybase_is_ok_on_unix()  - returns 1/0 if this monitoring server can run sybase code
 is_sqlsvr_is_ok_on_unix()  - returns 1/0 if this monitoring server can run sqlsvr code
 is_oracle_is_ok_on_unix()  - returns 1/0 if this monitoring server can run oracle code

 is_windows_is_ok_on_unix() - returns 1/0 if this monitoring server can run windows code
 is_unix_is_ok_on_unix()    - returns 1/0 if this monitoring server can run unix code
