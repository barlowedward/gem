#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- \n".
basename($0)." -DOALL=sybase
-or- \n".
basename($0)." -DOALL=sqlsvr

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile \n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL ,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;
	sub myformat {
		my(@x)=@_;
		my($str)=join("||",@x);
		$str=~ s/\s*\|\|\s*/\|\|/g;
		$str =~ s/\s+$//;
		return $str;
	}

	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	if( $DBTYPE eq "SQL SERVER" ) {
		my($q)="exec sp_configure 'show advanced options',1";
		print "--> $q\n";
		foreach( dbi_query(-db=>"master",-query=>$q)){
			print myformat(dbi_decode_row($_)),"\n";
		}
		$q="reconfigure";
		print "--> $q\n";
		foreach( dbi_query(-db=>"master",-query=>$q)){
			print myformat(dbi_decode_row($_)),"\n";
		}
		$q="exec sp_configure";
		print "--> $q\n";
		foreach( dbi_query(-db=>"master",-query=>$q)){
			print myformat(dbi_decode_row($_)),"\n";
		}
		$q="exec sp_configure 'show advanced options',0";
		print "--> $q\n";
		foreach( dbi_query(-db=>"master",-query=>$q)){
			print myformat(dbi_decode_row($_)),"\n";
		}
		$q="reconfigure";
		print "--> $q\n";
		foreach( dbi_query(-db=>"master",-query=>"reconfigure")){
			print myformat(dbi_decode_row($_)),"\n";
		}
	} else {
		my($q)="exec sp_configure";
		print "--> $q\n";
		foreach( dbi_query(-db=>"master",-query=>$q)){
			print myformat(dbi_decode_row($_)),"\n";
		}
	}

#	MlpAlarm::_querynoresults("delete INV_system_audit_result \n\twhere system_name = ".dbi_quote(-text=>$cursvr)." and system_type = ".dbi_quote(-text=>$typ) );
#
#			my($q)= "insert INV_system_audit_result values (\n\t".
#									dbi_quote(-text=>$cursvr)	.",".				# system
#									dbi_quote(-text=>$typ)		.",".				# systyp
#									dbi_quote(-text=>$PROGRAM_NAME)	.",".		# monitor
#									dbi_quote(-text=>$vals[3])	.",".				# database
#									dbi_quote(-text=>'?')	.",".					# errorlvl
#									dbi_quote(-text=>$vals[2])	.",\n\t".		# msgid
#									dbi_quote(-text=>$vals[6])	.					# msgtxt
#									",\n\tgetdate(),".								# moddt
#									dbi_quote(-text=>$PROGRAM_NAME)	.")";		# modby
#			#print $q;
#			MlpAlarm::_querynoresults($q);
#	}
}

__END__

=head1 NAME

configuration_tracker.pl

=head1 SYNOPSIS

Track system configuration table changes, specifically sp_configure, syslogins, and database size/log (sybase only)

=head1 TABLES

create table INV_configuration_tracker (
	server_name		varchar(30),				-- the server_name
	server_type		varchar(30),				-- sybase -or- sqlsvr

   config_type		varchar(30),				-- config -or- login -or- size
   config_name		varchar(255),				-- id of the config
   													-- login name
   												   -- dataspace -or- logspace
   config_value	varchar(30)
)

create table INV_configuration_history (

	change_time		datetime,					-- THE TIME THIS OCCURRED
	change_message varchar(255),				-- THE MESSAGE TO DISPLAY

	server_name		varchar(30),				-- the server_name
	server_type		varchar(30),				-- sybase -or- sqlsvr

   config_type		varchar(30),				-- config -or- login -or- size
   config_name		varchar(255),				-- id of the config
   													-- login name
   												   -- dataspace -or- logspace
   config_value	varchar(30)
)

=head2 NOTES

If no History record it will put 'initial audit' message

tracker table records all current values, history is the change history

=head1 REPORTS

1/1/08	initial	config
3/1/08	add 		login		bsmith	1/1/2009
5/1/08	change	config	memory	20004556


