#!/usr/local/bin/perl-5.8.2

BEGIN { $ENV{'LANG'} = 'C'; };

use strict;
use DBI;
use Text::CSV;
use Getopt::Long;
use Carp;

use vars qw( $DEBUG $SERVER $DATABASE $QUERYFILE $OUTFILE $LOGIN $PASSWORD);

usage("Bad Parameter List $!\n") unless
   GetOptions(
   	"SERVER=s"			=> \$SERVER,
		"QUERYFILE=s"	=> \$QUERYFILE,
		"DATABASE=s"		=> \$DATABASE,
		"OUTFILE=s"			=> \$OUTFILE,
		"LOGIN=s"			=> \$LOGIN,
		"PASSWORD=s"		=> \$PASSWORD,
		"DEBUG"				=> \$DEBUG
	);

sub usage {	
	use File::Basename;
	die @_,"\n INCORRECT USAGE OF PROGRAM 
	perl ".basename($0)." -LOGIN=USERNAME -SERVER=SERVERNAME -PASSWORD=PASSWORD -DATABASE=DATABASE -QUERYFILE=FILE --OUTFILE=FILE\n";
}
usage("MUST PASS CREDENTIALS") unless $LOGIN and $PASSWORD and $SERVER and $DATABASE;	
usage("MUST PASS OUTFILE") unless $OUTFILE;
usage("MUST PASS QUERYFILE") unless $QUERYFILE;
usage("BAD FILE QUERYFILE - $QUERYFILE") unless -r $QUERYFILE;

my($QUERY)='';

open(OUT,">".$OUTFILE) or die "Cant write $OUTFILE: $!\n";

open(Q,$QUERYFILE) or die "Cant read $QUERYFILE: $!\n";
while(<Q>) {
	chomp;chomp;
	$QUERY.=$_."\n";
}
	
my($Driver)="Sybase";
$Driver	=	"ODBC" if $^O =~ /MSWin32/;
DBI->trace(8) if $DEBUG;

my $dsn = "dbi:$Driver:$SERVER";
# $dsn = "DBI:mysql:host=$SERVER" if $Driver	eq "mysql";

print "Using $dsn\n" if $DEBUG;
my $dbh = DBI->connect( $dsn, $LOGIN, $PASSWORD) or die $DBI::errstr;
		
my $sth;
my $dat;
$dbh->do("use $DATABASE") if $DATABASE;
unless ($sth = $dbh -> prepare ($QUERY)) { 
	print("SQL command $QUERY prepare failed ");
	$dbh->disconnect;
	exit;
}

# excute the query
unless ( $sth -> execute ) { 
	print("SQL command $QUERY execute failed ");
	$dbh->disconnect;
	exit;
}
	 
my $csv = Text::CSV->new( { binary => 1 }) or die "Cant use CSV::Text\n";

while ( $dat = $sth -> fetchrow_arrayref ) {
	my(@out);
	foreach(@$dat) { s/\n/\<nl\>/g; push @out,$_; }
	print "SUCCESS $SERVER ".join("|",@out),"\n";
	print ".";	
	my($rc)=$csv->combine(@out); 
	#die "rc not 1! \n" unless $rc==1;
	print $csv->string(),"\n";
	print OUT $csv->string(),"\n";
}
close(OUT);

$sth ->finish;
$dbh->disconnect;
exit;

__END__

=head1 NAME

QueryToCsv.pl

=head2 SYNOPSIS

Runs a query and puts output to a csv file

=head2 USAGE

  perl QueryToCsv.pl -LOGIN=USERNAME -SERVER=SERVERNAME -PASSWORD=PASSWORD -DATABASE=DATABASE -QUERYFILE=FILE --OUTFILE=FILE
  