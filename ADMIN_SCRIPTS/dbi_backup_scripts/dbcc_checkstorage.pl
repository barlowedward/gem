#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# use subs qw(logdie infof info warning alert);
use strict;
use Getopt::Std;
use Logger;
use DBIFunc;
use File::Basename;
use CommonFunc;
use MlpAlarm;
use vars qw($opt_U $opt_p $opt_S $opt_P $opt_D $opt_d $opt_h $opt_e $opt_l $opt_M $opt_o %CONFIG);

sub usage
{
   print @_;
   print "dbcc_db.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST

   -p            print all output from dbcc
   -T Type     	 Sybase or Odbc
   -D DB_LIST    pipe separated list with wildcards of databases
   -d            debug mode
   -h            html output
   -o outputdir  for raw output (file is server.db.dbcc.datestamp)
   -e errorlog   optional
   -l sessionlog optional

   Runs dbcc checkstorage on the list of databases
\n";
   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
die(usage("Bad Parameter List")) unless getopts('U:D:S:P:o:dhl:e:M:T:p');
%CONFIG=read_configfile( -srv=>$opt_S );

############################### DEBUGGING #########################
$CONFIG{DO_DBCC}='y';
###################################################################

$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);

logger_init(-logfile         => $opt_l,
            -errfile         => $opt_e,
            -database        => $opt_D,
            -debug           => $opt_d,
            -system          => undef,
            -subsystem       => undef,
            -command_run     => $CONFIG{COMMAND_RUN},
            -mail_host       => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to         => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

use Sys::Hostname;
info( "dbcc_checkstorage.pl : run at ".localtime(time)."\n" );
info( "run on hostname=",hostname(),"\n" );

my($start_time)=time;

logdie usage("Must pass server name\n")   unless defined $opt_S;
logdie usage("Must pass sa login ID\n" )  unless defined $opt_U;
logdie usage("Must pass sa password\n" )  unless defined $opt_P;
logdie usage("Output directory does not exist\n")
  if defined $opt_o and ! -d $opt_o;

if( is_nt() ) {
  $opt_M="ODBC" unless defined $opt_M;
} else {
  $opt_M="Sybase" unless defined $opt_M;
}

logdie usage("Type must be Sybase|ODBC\n")
  unless $opt_M eq "Sybase" or $opt_M eq "ODBC";

#
# CONNECT TO DB
#
if( $opt_M eq "ODBC" ) {
  dbi_msg_ok(1000);
# dbi_msg_exclude("
}

info "Connecting to sybase",$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S, -login=>$opt_U, -password=>$opt_P,
            -type=>$opt_M, -debug=>$opt_d)
  or logdie "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");

my($server_type)="SQLSERVER";
foreach( dbi_query(-db=>"master", -query=>"
select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
   ($server_type)=dbi_decode_row($_);
}
info("Server Type=$server_type\n");

# GET DB INFO
my($whereclause)="";

if( defined $opt_D ) {
  	foreach ( split(/[\,\|]/,$opt_D)) {
 		chomp;
 	 	next if /^\s*$/;
 		if( $whereclause eq "" ) {
			$whereclause="and ( name like \'$_\' or name = \'$_\'";
		} else {
			$whereclause .= " or name like \'$_\' or name = \'$_\'";
		}
	}
	$whereclause.=")" if $whereclause ne "";
}

my( $status1bits, $status2bits );
if( $server_type eq "SQLSERVER" ) {
   $status1bits = 32+64+128+256+512+1024+4096+32768;
   $status2bits = 0x0000;
} else {
# 1120= 1000 (single usr) + 100 (suspect) + 20 (for load)
#   30= 20 (offline until recovery) + 10 (offline)
   $status1bits = 0x1120;
   $status2bits = 0x0030;
}

my(@databaselist)=dbi_query(-db=>"master",-query=>"
select name from sysdatabases
 where status & $status1bits = 0
   and status2& $status2bits = 0 $whereclause
");

if( $#databaselist<0 ) {
   warning "No Databases Found On Server\n";
   warning "Query Was select name from sysdatabases where status & $status1bits = 0 and status2&$status2bits=0 $whereclause";
   exit(0);
}

my(@errormsg);

foreach (@databaselist) {
   my($db)=dbi_decode_row($_);
   info "Working on db $db",$NL if defined $opt_d;
   my($st)=time;

   MlpLogOperationStart('dbcc',$opt_S,$db,'');
   run_a_dbcc($db);
   MlpLogOperationEnd('dbcc',$opt_S,$db,'');

   my($s)=time-$st;
   my $minutes=int($s/60);
   $s-=60*$minutes;
   info "Dbcc of $db completed in $minutes minutes and $s secs.\n";
   info $NL,$NL if $#databaselist>=0;
}

dbi_disconnect();

if( $#errormsg>=0 ) {
   bad_email("error:".join("\nerror:",@errormsg)."\n");
}

my($s)=time-$start_time;
my $minutes=int($s/60);
$s-=60*$minutes;
info "Dbcc program completed in $minutes minutes and $s secs.\n";

exit(0);

sub run_a_dbcc
{
   my($db)=@_;
   my($tot_numerr)=0;

   info("<h2>") if defined $opt_h;
   info("Checking Database $db in Server $opt_S\n");
   info("</h2>") if defined $opt_h;

   logdie("Cant Use Database $db\n") unless dbi_use_database($db);

   info("<h3>") if defined $opt_h;
   info("Checkstorage $db\n");
   info("</h3>")if defined $opt_h;

   my($logfile)=$opt_o;
   $logfile  =~ s|\/$|| if defined $logfile;
   $logfile  .=  "/".$opt_S.".".$db.".dbcc.".today() if defined $logfile;

   my($errfile)=$logfile.".errors" if defined $logfile;

   info "Raw Output Saved To $logfile\n" if defined $logfile and $opt_d;
   if( defined $logfile ) {
      open(DBCCOUT,">".$logfile) or die "Cant open $logfile for writing : $!\n";
      open(DBCCERR,">".$errfile) or die "Cant open $errfile for writing : $!\n";
   }

   print DBCCOUT "Running dbcc checkstorage($db): \n" if defined $logfile;
   print DBCCERR "Running dbcc checkstorage($db): \n" if defined $logfile;

   my @rc= dbi_query(-db=>"master",-query=>"dbcc checkstorage($db)",
                     -no_results=>1, -debug=>$opt_d);
   info( "checkstorage($db) returned ".($#rc+1)." rows\n" )
      if defined $opt_d or defined $opt_p;

# Check if there are errors running checkstorage.

   my($count,$numerr,$in_error)=(0,0,0);
   my(@dat);

   foreach ( @rc ) {
     my($msg)=dbi_decode_row($_);
     info("checkstorage: ".$msg."\n") if defined $opt_d or defined $opt_p;
     print DBCCOUT "checkstorage: ".$msg."\n" if defined $logfile;
     push @dat,$msg;
     $in_error=1 if $msg =~ /^\s*9975/;
   }

   if ($in_error) {
     $numerr++;
     foreach ( @dat ) {
       push @errormsg,$_ unless $_=~/^\s*$/;
       print DBCCERR "checkstorage: ".$_."\n" if defined $logfile;
     }
     return;
   }

# Running dbcc checkverify to fix severity codes in dbcc_faults table.

   print DBCCOUT "Running dbcc checkverify($db): \n" if defined $logfile;
   print DBCCERR "Running dbcc checkverify($db): \n" if defined $logfile;

   my @rc= dbi_query(-db=>"master",-query=>"dbcc checkverify($db)",
                     -no_results=>1, -debug=>$opt_d);
   info( "checkverify($db) returned ".($#rc+1)." rows\n" )
      if defined $opt_d or defined $opt_p;

# Check if there are errors running checkstorage.

   my(@dat);

   foreach ( @rc ) {
     my($msg)=dbi_decode_row($_);
     info("checkverify: ".$msg."\n") if defined $opt_d or defined $opt_p;
     print DBCCOUT "checkverify: ".$msg."\n" if defined $logfile;
     push @dat,$msg;
     $in_error=1 if $msg =~ /^\s*12924/;
   }

   if ($in_error) {
     $numerr++;
     foreach ( @dat ) {
       push @errormsg,$_ unless $_=~/^\s*$/;
       print DBCCERR "checkverify: ".$_."\n" if defined $logfile;
     }
     return;
   }

# Now verify if there are real faults.

   print DBCCOUT "Verifying dbcc results for $db: \n" if defined $logfile;
   print DBCCERR "Verifying dbcc results for $db: \n" if defined $logfile;

   my @rc= dbi_query(-db=>"dbccdb",-query=>"
select distinct
       object_name(f.id, f.dbid), f.status, f.type_code, rtrim(t.description)
  from dbcc_faults f, dbcc_types t, dbcc_operation_log l
 where l.start >= (select max(start) from dbcc_operation_log
                    where dbid = db_id('$db'))
   and l.dbid = db_id('$db')
   and l.dbid = f.dbid
   and l.opid = f.opid
   and f.type_code = t.type_code
   and f.status != 0
 order by 1,2", -debug=>$opt_d);

   info( "checkverify($db) returned ".($#rc+1)." rows\n" )
      if defined $opt_d or defined $opt_p;

   if ($#rc<1){

# No faults for this operation.

      print DBCCOUT "Checkstorage: No faults found for $db: \n"
         if defined $logfile;
      print DBCCERR "Checkstorage: No faults found for $db: \n"
         if defined $logfile;
      return;
   }

   my ($count,$hardfault,$softfault,$transient)=(0,0,0,0);
   foreach ( @rc ) {
      my(@dat)=dbi_decode_row($_);
      info "checkstorage: ".join(" ",@dat)."\n"
        if defined $opt_d or defined $opt_p;

      my $line="Table=$dat[0]; Status=$dat[1]; Type=$dat[2]: $dat[3]";
      if ($dat[1] == 1 or $dat[1] == 3 or $dat[1] == 5 or $dat[1] == 7) {

# Not VERY severe hard fault.
         $hardfault++;
         $numerr++;
         push @errormsg, $line;
         print DBCCERR "checkstorage: ".$line."\n" if defined $logfile;
      } elsif ($dat[1] == 9 or
               $dat[1] == 11 or $dat[1] == 17 or $dat[1] == 19) {

# VERY severe hard fault.
         $hardfault++;
         $numerr++;
         push @errormsg, $line;
         print DBCCERR "checkstorage: ".$line."\n" if defined $logfile;
      } elsif ($dat[1] == 16 or $dat[1] == 18) {

# Severe soft fault.
         $softfault++;
         $numerr++;
         push @errormsg, $line;
         print DBCCERR "checkstorage: ".$line."\n" if defined $logfile;
      } else {

# Transient soft fault (2).
         $transient++;
         $numerr++;
         push @errormsg, $line;
         print DBCCERR "checkstorage: ".$line."\n" if defined $logfile;
      }
   }

   info("Checkstorage completed successfully with no errors","\n")
      if $numerr == 0;

   if( defined $logfile ) {
      close(DBCCOUT);
      close(DBCCERR);
      chmod 0666,$logfile;
      chmod 0666,$errfile;
      unlink $errfile if -e $errfile and $numerr==0;
   }
}

__END__

=head1 NAME

dbcc_db.pl - utility to dbcc databases

=head2 AUTHOR

By Ed Barlow

=head2 USAGE

dbcc_db.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST

	-p 			print all output from dbcc
	-T Type       Sybase or Odbc
   -D DB_LIST    pipe separated list with wildcards of databases
   -d	    debug mode
   -h	    html output
	-o outputdir  for raw output (file is server.db.dbcc.datestamp)
   -e errorlog   optional
   -l sessionlog optional

   Runs standard dbcc on the list of databases

=head2 DESCRIPTION

Runs dbcc checkdb, checkalloc, and checkcatalog on your databases. Only
prints output if there is a problem.

=cut

