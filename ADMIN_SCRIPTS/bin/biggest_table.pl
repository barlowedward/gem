#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Std;
use CommonFunc;
use DBIFunc;
use Sys::Hostname;

# Copyright (c) 2003 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

$|=1;

sub usage
{
        print @_;
        print "biggest_table.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST -nrows

   -s - sort by space used instead of rowcount
   -n rows - number of tables
   -f - first object to work on

   -d - debug mode
   -r - runs reorg rebuild
   -c - runs reorg compact
   -t min - maximum time to use for the whole thing
   -x - no header messages

   Runs the reorgs starting biggest first. If you specify -t it will do resume as well.  \n";
   return "\n";
}

$| =1;

use vars qw( $opt_d $opt_h $opt_U $opt_S $opt_P $opt_f $opt_D $opt_r $opt_c $opt_r $opt_c $opt_t $opt_s $opt_n $opt_x);
die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('hf:U:D:S:P:rct:sn:dx');

my($end_time) = time + $opt_t * 60 if defined $opt_t;

cd_home();

die usage("Must pass sa password\n" ) unless defined $opt_P;
die usage("Must pass server\n" )	 unless defined $opt_S;
die usage("Must pass username\n" ) unless defined $opt_U;

if( ! $opt_x ) {
	print "<h2>" if $opt_h;
	print "biggest_table.pl - version 1.0\n";
	print "</h2>\n" if $opt_h;
	print "Run on server ".hostname()." at ".localtime(time)."\n\n";
	print "Connecting to Server $opt_S\n" if defined $opt_d;
	print "<p>" if $opt_h;
}

dbi_connect(	-srv	=>	$opt_S,
		-login	=>	$opt_U,
		-password=>	$opt_P,
		# -type	=>	"Sybase",
		-debug	=>	undef)
	or die "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");

my($TYPE,@databaselist) = dbi_parse_opt_D($opt_D,1,1);

if( $#databaselist < 0 ) {
	print "NO DATABASES FOUND TO PROCESS\n";
	exit(0);
}
print "Number of Databases To Work With: ".($#databaselist+1)."\n";
print "<p>" if $opt_h;

my(%tblrows,%tblspace);
my($widesttable,$numtables,$rowptr,$spcptr)=get_tbl_sizes();
%tblrows  = %$rowptr;
%tblspace = %$spcptr;

#if( defined $opt_d ) {
#	print "dbg: PRINTING ALL TABLES ($numtables total)\n";
#	foreach ( sort mysort keys %tblrows ) {
#		print "dbg:",$_," rows=",$tblrows{$_},"\n";
#	}
#}

my(@tblstodo);

my($i)=1;
my($minprintrow)=0;
$minprintrow= $numtables-$opt_n+1 if defined $opt_n;

if( $opt_h ) {
	printf "<TABLE BORDER=1>\n<TR BGCOLOR=BEIGE><TH>Database</TH><TH>Table</TH><TH>Rows</TH><TH>Size</TH></TR>\n"
		unless defined $opt_c or defined $opt_r;
} else {
	printf "    %".$widesttable."s %-10s %s\n","Table","Rows","KB"
		unless defined $opt_c or defined $opt_r;
}
print "minprintrow=$minprintrow\n" if defined $opt_d;

my(@rows);
foreach ( sort mysort keys %tblrows ) {
	$i++;
	next if $i<= $minprintrow;
	if( $opt_h ) {
		my($db,$tbl)=split(/\./,$_,2);
		$tbl=~s/^\.+//;
		my($sp)=int ( $tblspace{$_} / 1000 );
		if( $sp > 10000 ) {
			$sp= int( $sp/1000 );
			$sp.=" GB";
		} else {
			$sp.=" MB";
		}
		my($rows)=$tblrows{$_};
		if( $rows>10000000 ) {
			$rows=int($rows/1000000);
			$rows.="M";
		} elsif( $rows>10000 ) {
			$rows=int($rows/1000);
			$rows.="K";
		}
		push @rows, "<TR><TD>".$db."</TD><TD>".$tbl.
				"</TD>\n\t<TD>".$rows.
				"</TD>\n\t<TD>".$sp."</TD></TR>\n"
			unless ( defined $opt_c or defined $opt_r ) and ! defined $opt_d;
	} else {
		push @rows,sprintf( "%3d %".$widesttable."s %-10d %s\n",
				($i-$minprintrow),$_,$tblrows{$_},$tblspace{$_})
					unless ( defined $opt_c or defined $opt_r ) and ! defined $opt_d;
	}
	push @tblstodo,$_;
}

foreach ( reverse @rows ) { print $_; }
print "</TABLE>" if $opt_h;


# do a reorg...
if ( defined $opt_c or defined $opt_r ) {
	print "dbg: TBLS TO DO = $#tblstodo \n" if defined $opt_d;
	my( $found_first ) = 0;
	my($fdb,$ftbl);
	if( defined $opt_f ) {
		if( $opt_f =~ /\.\./ ) {
			($fdb,$ftbl) = split(/\.\./,$opt_f);
			print "Searching For Tbl $ftbl (in $fdb)\n";
		} else {
			$ftbl=$opt_f;
			print "Searching For Tbl $ftbl (no db)\n";
		}
	} else {
		$found_first=1;
	}

	foreach my $obj ( reverse @tblstodo ) {
		my($db,$tbl) = split(/\.\./,$obj);
		my($cmd);

		# start at opt_f if it is passed
		if( $found_first==0 ) {
			# print "dbg: comparing $ftbl to $tbl\n" if defined $opt_d;
			next unless $ftbl eq $tbl;
			# print "dbg: comparing $fdb to $db\n" if defined $opt_d;
			next unless ! defined $fdb or $fdb eq $db;
			print "(dbg) Found First Table\n" if defined $opt_d;
			$found_first=1;
		}
		my($time_remaining)= ($end_time - time ) / 60;
		$time_remaining=1 if int($time_remaining)==0;
		if( defined $opt_c ) {
			$cmd = "reorg compact $tbl";
			$cmd .= " with resume, time=".int($time_remaining) if defined $opt_t;
		} else {
			$cmd = "reorg rebuild $tbl";
			$cmd .= " with resume, time=".int($time_remaining) if defined $opt_t;
		}

		print "($db) ",$cmd;
		dbi_set_mode("INLINE");
		my($i)=0;
		foreach ( dbi_query(-db=>$db, -query=>$cmd, -die_on_error=>undef)){
			my($d)=join("",dbi_decode_row($_));
			chomp $d;
			print "\n",$d unless $d=~/11916/;	# processed
			print "." if $d=~/11916/ and $i++ % 10 == 0;
		}
		print "\n";

		if( time > $end_time ) {
			print "Timer Has Expired - Last object $db..$tbl\n";
			last;
		}
	}

	die "ERROR: Object $opt_f Was Not Found\n" unless $found_first;

	print "Fetching Rowcounts Again\n";
	my(%atblrows,%atblspace);
	my($dummy,$dummy2,$arowptr,$aspcptr)=get_tbl_sizes();
	%atblrows  = %$arowptr;
	%atblspace = %$aspcptr;

	my($i)=1;
	printf "    %".$widesttable."s %10s %10s %s\n","Table","BeforeKB","AfterKB" ,"Diff";
	foreach ( @tblstodo ) {
		printf "%3d %".$widesttable."s %10s %10s %s\n",$i++,$_,$tblspace{$_},$atblspace{$_},$tblspace{$_}-$atblspace{$_};
	}
}
exit(0);

sub mysort {
	if( defined $opt_s ) {
		$tblspace{$a} <=> $tblspace{$b};
	} else {
		$tblrows{$a} <=> $tblrows{$b};
	}
}

sub get_tbl_sizes {
	my(%trows,%tspace);
	my($widesttable)=0;
	my($numtables)=0;
	foreach my $db ( @databaselist ) {
		next if    $db eq "tempdb"
			or $db eq "master"
			or $db eq "model"
			or $db eq "sybsystemprocs"
			or $db eq "sybsystemdb"
			or $db eq "sybsecurity"
			or $db eq "sybsyntax";
		print "   Reading database ($db)\n" if defined $opt_d;
		foreach ( dbi_query(-db=>$db, -query=>"sp__helptable \@dont_format=\"y\"")){
			my(@x)=dbi_decode_row($_);
			next if $x[0] =~ /syslogs/;
			my($t)=$db."..".$x[0];
			$widesttable=length $t if $widesttable<length $t;
			$trows{$t} = $x[1];
			$tspace{$t} = $x[2];
			$numtables++;
		}
	}
	print "dbg: returning widesttable=$widesttable,numtables=$numtables \n" if defined $opt_d;
	return($widesttable, $numtables, \%trows, \%tspace);
}
