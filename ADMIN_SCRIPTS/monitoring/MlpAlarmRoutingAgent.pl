#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use MlpAlarm;
use CommonFunc;
use Do_Time;
use Getopt::Long;
use File::Basename;
use Repository;
use MIME::Lite;
use Sys::Hostname;
use DBIFunc;
use vars qw( $DEBUG $NOSEND $SLEEPTIME $FROM $RESTART $ROUTING $TEST $MAXTIME $TIMEOUT );
my($LOGOPEN);

my(%severities) = (
	"EMERGENCY" 	=> 5,
	"CRITICAL" 	=> 4,
	"ALERT" 	=> 4,
	"FATALS"	=> 4,
	"ERROR" 	=> 3,
	"ERRORS"	=> 3,
	"WARNING" 	=> 2,
	"WARNINGS" 	=> 2,
	"INFORMATION" 	=> 1,
	"OK" 		=> 1,
);

my(%short_state) = (
	"EMERGENCY" 	=> "!",
	"CRITICAL" 	=> "!",
	"ALERT" 	=> "!",
	"FATALS"	=> "!",
	"ERROR" 	=> "E",
	"ERRORS"	=> "E",
	"WARNING" 	=> "W",
	"WARNINGS" 	=> "W",
	"INFORMATION" 	=> "I",
	"OK" 		=> "I",
);

sub usage {
	print @_;
	print "Usage: $0 --SLEEPTIME=secs --FROM=from --DEBUG --ROUTING --TEST
	 --TIMEOUT=time
   --SLEEPTIME=SECONDS
   --MAXTIME=minutes\n";
	return "";
}

$| = 1;

die usage("Bad Parameter List\n") unless GetOptions(
		"FROM=s"			=> \$FROM,
		"ROUTING"		=> \$ROUTING,
		"RESTART"		=> \$RESTART,
		"NOSEND"			=> \$NOSEND,
		"TEST"			=> \$TEST,
		"DEBUG" 		   => \$DEBUG,
		"MAXTIME=s" 	=> \$MAXTIME,
		"SLEEPTIME=i"	=> \$SLEEPTIME
);

$MAXTIME  = time+$MAXTIME*60 if $MAXTIME;
my($curdate)=do_time(-fmt=>'yyyymmdd_hhmiss');
my($gem_root_dir)=get_gem_root_dir();
die "No Gem Root Dir" unless -d $gem_root_dir;

my($LOGFILE)=$gem_root_dir."/data/logs/AlarmRoutingAgent_".hostname()."_".$curdate.".log";

my($timethresh)=60;		# minute
if( defined $SLEEPTIME ) {						 # test if you are up
	#$timethresh=2*$SLEEPTIME 	if $timethresh < 2*$SLEEPTIME;
	#$timethresh=2*$MAXTIME  	if $timethresh < 2*$MAXTIME;
	if( Am_I_Up( -threshtime=>$timethresh) and ! defined $RESTART ) {
		if( $MAXTIME and $SLEEPTIME ) {
			sleep($SLEEPTIME);
			if( Am_I_Up( -threshtime=>$timethresh) and ! defined $RESTART ) {
				print "(debug mode) ERROR - PROGRAM RUNNING - remove lock file and rerun\n" if defined $DEBUG;
				exit(0);
			}
		} else {
			print "(debug mode) ERROR - PROGRAM RUNNING - remove lock file and rerun\n" if defined $DEBUG;
			exit(0);
		}
	}
}


my($cc)=read_gemconfig(-filename=>'gem.dat',-debug=>1,READONLY=>1);
print "ALARM_MAIL=",$cc->{ALARM_MAIL},"\n";

#use Data::Dumper;
#print Dumper \%cc;
#print "\n";

#print "\t\treading ignore.dat\n";
#my($file)=$gem_root_dir."/conf/ignore.dat";
#die "NO CONFIGURATION FILE $file\n" 				unless -e $file;
#die "UNREADABLE CONFIGURATION FILE $file\n" 		unless -r $file;
#
my(@ignorepat);	# array of hashes
#open(CCF,$file) or die "Cant open $file $!\n";
#while (<CCF>) {
#	next if /^\s*#/ or /^\s*$/;
#	next unless /^NOMAIL/;
#	chomp;
#	s/#.+$//;
#	s/\s+$//;
#	s/^NOMAIL://;
#	my(@x) = split(/:/,$_);
#
#	push @ignorepat, \@x;
#}
#close(CCF);

if( ! defined $ROUTING and $LOGFILE ) {
	open(LOG,">$LOGFILE") or die "Cant write to $LOGFILE\n";
	$LOGOPEN="TRUE";
}

pr_message( 1,  "MlpAlarmRoutingAgent.pl v1.0\n" );
pr_message( 1,  "Run At ".localtime(time)."\n" );
pr_message( 1,  "ARGUMENTS: DEBUG=$DEBUG\n\tSLEEPTIME=$SLEEPTIME\n\tFROM=$FROM\n\tRESTART=$RESTART\n\tROUTING=$ROUTING\n\tMAXTIME=$MAXTIME\n\tTIMEOUT=$TIMEOUT\n" );

MlpBatchJobStart(-BATCH_ID=>'AlarmRoutingAgent',-AGENT_TYPE=>'Gem Monitor');

pr_message( 1,  "Running  MlpRunScreen(-screen_name=>Operator) \n" );# if defined $DEBUG;
my($operator_info) = MlpRunScreen(-screen_name=>"INV_OPERATOR_SETUP");
#use Data::Dumper;
#print Dumper $operator_info;
pr_message( 1,  "Running  MlpRunScreen(-screen_name=>Operator Schedule) \n");# if defined $DEBUG;
my($operator_sched)= MlpRunScreen(-screen_name=>"INV_OPERATOR_SCHEDULE");
my($shdr)=shift @$operator_sched;
#print Dumper $operator_sched;
pr_message( 1,  "Running  MlpRunScreen(-screen_name=>Alarm Routing) \n");# if defined $DEBUG;
my($alarm_routes)=MlpRunScreen(-screen_name=>"INV_ALARM_ROUTING");
# use Data::Dumper; print Dumper $alarm_routes; die "DONE";
my($rhdr)=shift @$alarm_routes;

pr_message( 1,  "Running  MlpRunScreen(-screen_name=>Containers) \n");# if defined $DEBUG;
my($container_defn)=MlpRunScreen(-screen_name=>"Containers");
my($chdr)=shift @$container_defn;

pr_message( 1,  "Fetching Operator Information\n" );# if defined $DEBUG;
my(%op_mail_byname);
# Get pager schema
my($hdr)=shift @$operator_info;
foreach my $dat (@$operator_info) {
	my(@row)=@$dat;
	# print "Operator $row[2] $row[0]\n" if $row[1] eq "YES";
	$op_mail_byname{$row[0]} = $row[2] if $row[1] eq "YES";
}

if( defined $ROUTING ) {

	my($s,$l,$p,$d)=MlpGetVars();
	print "ROUTING MODE - printing routes for alarms\n";
	print "\tAlarm Diag Server=$s Login=$l Db=$d\n";

	print "\n\n\tOPERATORS\n";
#	print join("\t",@$hdr),"\n";
#	foreach(@$operator_info) {
#		my(@row)=@$_;
#		print "\t",join("\t",@row),"\n";
#	}
	printf "\t%12s %12s %12s\n",@$hdr;
	foreach(@$operator_info) {
		printf "\t%12s %12s %12s \n",@$_;
	}

	print "\n\n\tSCHEDULES\n";
	printf "\t%12s %12s %12s %12s %12s %12s %12s %12s %12s\n",@$shdr;
	foreach(@$operator_sched) {
		printf "\t%12s %12s %12s %12s %12s %12s %12s %12s %12s\n",@$_;
	}

	print "\n\n\tROUTING\n";
	#print "\t",join("\t",@$rhdr),"\n";
	#foreach(@$alarm_routes) { print "\t",join("\t",@$_),"\n"; }
	printf "\t%12s %12s %20s %12s %16s %16s %12s \n",@$rhdr;
	foreach(@$alarm_routes) {
		printf "\t%12s %12s %20s %12s %16s %16s %12s \n",@$_;
	}

	# print "\n\nCONTAINERS\n";
	# print join("\t",@$chdr),"\n";
	# foreach(@$container_defn) { print join("\t",@$_),"\n"; }

	print "\n\nCompleted Show Routing Information\n";
	#MlpBatchJobEnd();
	#exit(0);
} else {
	print "Routing Info is not being printed as --ROUTING not passed\n";
}

pr_message( 1,  "Synopsis of what is going to happen:\n" );
pr_message( 1,  "  * fetch static routing data\n" );
pr_message( 1,  "  * get_possible_alarms() update Heartbeat internal_state from I to P\n" );
pr_message( 1,  "  * route alarms\n" );
pr_message( 1,  "  * clear_possible_alrms() updates Heartbeat internal_state to C if P\n\n" );
pr_message( 1,  "Log file=$LOGFILE\n" );

while(1) {
	last if $MAXTIME and time>$MAXTIME;
	I_Am_Up(-threshtime=>$timethresh)		if defined $SLEEPTIME;

	#
	# GENERATE PAGES FOR THE MONITOR
	#
	my(@t)   = localtime(time);
	my($dow) = $t[6];
	my($hour)= $t[2];

	pr_message( 1,  "\nFetching Alarms From Database at ",scalar(localtime(time)),"\n" );

	my(%args);
	$args{-test}=1 if $DEBUG or $TEST;
	pr_message( $DEBUG, "Running MlpGetAlarms\n" );# if defined $DEBUG;
	my(@alarms)=MlpGetAlarms(%args);

	pr_message( 1,  " -- Found ".($#alarms+1)." Alarms\n" );

	if( $^O !~ /Win32/ and $#alarms>=0 ) {	# unix syslogging
		use Sys::Syslog;
		openlog("DBA_Monitor","nofatal","local0");
	}

	foreach my $row ( @alarms ) { do_a_message(@$row,$dow,$hour); }

	if( $^O !~ /Win32/ and $#alarms>=0 ) {	# unix
		closelog();
	}

	pr_message( $DEBUG, "Running MlpClearAlarms\n" );# if defined $DEBUG;
	pr_message( 1,  "Running MlpClearAlarms\n" );
	MlpClearAlarms();

	last unless $SLEEPTIME;
	print " -- Sleeping for $SLEEPTIME seconds\n";
	last if $MAXTIME and time>$MAXTIME;
	sleep($SLEEPTIME);
	# print "Running MlpBatchRunning\n";# if defined $DEBUG;
	# MlpBatchJobStep(-step=>"Running at ".localtime(time));
	last if $MAXTIME and time>$MAXTIME;
}
print "Completed Routing\n";
MlpBatchJobEnd();
exit(0);

my(%ALLREADY_SENT);
my($mimi_url);
sub do_a_message {
	my($program,$system,$subsystem,$state,$msg,$container,$mon_time,$dow,$hour)=@_;

	if( $subsystem =~ /Job:/ or lc($program)=~ /bkp/
	or lc($program)=~ /backup/ or lc($program)=~ /logship/
	or lc($program)=~ /tran/ ) {
		print "Ignoring Message - Program=$program System=$system Sub=$subsystem\n";
		return;
	}

	$container = undef if $container=~/^\s*$/;

	pr_message( 1,   "Processing Message (program=$program,system=$system,subsys=$subsystem,\n\tstate=$state,msg=$msg,container=$container,time=$mon_time,doweek=$dow,hour=$hour)\n" )
		if defined $DEBUG;

	# Handle Ignore Directives
	my($msgok)="YES";
	foreach ( @ignorepat ) {
		$msgok="NO" if defined $_->[0] and $_->[0] eq $program;
		$msgok="NO" if defined $_->[1] and $_->[1] eq $system;
		$msgok="NO" if defined $_->[2] and $_->[2] eq $subsystem;
	}
	if( $msgok eq "NO" ) {
		pr_message(1,"Ignoring Message from $program/$system/$subsystem Based On Ignore.dat Directives\n");
		return;
	}

	my(%message_sent_list);
	pr_message( 1,   "Processing Message (program=$program,system=$system,subsys=$subsystem,\n\tstate=$state,msg=$msg,container=$container,time=$mon_time,doweek=$dow,hour=$hour)\n" )
		if defined $DEBUG;
	pr_message( 1,  "#
#Processing Message (program=$program,system=$system,
#\tsubsystem=$subsystem,state=$state,
#\tmessage=".join(/\n#\tmessage=/,split(/\n/,$msg))."
#\tcontainer=$container,time=$mon_time,doweek=$dow,hour=$hour)
#\n" );

	if( $^O !~ /Win32/ ) {	# unix
		my($m)="system=$system ";
		$m .= "prg=$program " if $program;
		$m .= "sub=$subsystem " if $subsystem;
		$m .= join(/ - /,split(/\n/,$msg));
		$m =~ s/GEM_\d+://;
		$m =~ s/\s\s+/ /g;
		if( $state eq "CRITICAL" or $state eq "EMERGENCY"  or $state eq "FATALS" or $state eq "ALERT" ) {
			pr_message( 1, "# saving syslog error Message\n" );
			syslog("err",$m);
		} elsif(  $state eq "ERROR" or $state eq "ERRORS"   ) {
			pr_message( 1, "# saving syslog Notice Message\n" );
			syslog("notice",$m);
		# } elsif(  $state eq "WARNING" or $state eq "WARNINGS") {
			# pr_message( 1, "# saving syslog Notice Message\n" );
			# syslog("notice",$m);
		} else {
			print "Not syslogging message";
		}
	}

	my(%page,%email,%netsend);
	pr_message( 1,   " -- $#$alarm_routes operators/routes detected\n" ) if defined $DEBUG;
	my($lastmsg)="";
	my(%sent_allready);
	my(@send_count);
	foreach my $dat (@$alarm_routes) {
		#my($usr,$prog,$contr,$sys,$ssys,$minsev,$maxsev,$usenetsend,$useemail,$usenetsend,$useblackberry)=@$dat;
		my($usr,$prog,$contr,$sys,$minsev,$maxsev)=@$dat;
		my($ssys);

		# FORMAT OF ABOVE REPORT CHANGED
		my($usenetsend, $useblackberry,$useemail,$usepager)=(0,'YES',1,0);

		next if defined $message_sent_list{$usr};

		pr_message( 1,   " -- testing $usr (" );
		pr_message( 1,   "prog=$prog," ) if $prog !~ /^\s*$/;
		pr_message( 1,   "ctr=$contr," ) if $contr !~ /^\s*$/;
		pr_message( 1,   "sys=$sys," ) 	if $sys !~ /^\s*$/ ;
		pr_message( 1,   "ssys=$ssys," ) if $ssys !~ /^\s*$/;
		pr_message( 1,   "sev=$minsev/$maxsev)\n" );

		if( $prog ne "All Programs" and $prog ne $program ) {
			pr_message( 1,   "   -- skipping route - $prog != $program\n" );
			next;
		}

		# the container test is a bit odd... $contr contains the container of the possible error
		# so this test is right?
#		if( defined $contr or defined $container ) {
			#pr_message( $DEBUG,  "   -- DBG DBG : contr is defined as $contr\n" ) if defined $contr;
			#pr_message( $DEBUG,  "   -- DBG DBG : container is defined as $container\n" ) if defined $container;
#			if( ! defined $contr or ! defined $container or $contr ne $container ) {
#				pr_message( $DEBUG,  "   -- (not alarming) container check (contr=$contr, container=$container)\n" );
#				next;
#			}
#		}

		if( defined $sys   and $sys ne $system ) {
			pr_message( $DEBUG,  "   -- (not alarming) system check\n" );
			next;
		}

		if( defined $ssys  and $ssys ne $subsystem ) {
			pr_message( $DEBUG,  "   -- (not alarming) sub system check ($ssys!=$subsystem)\n" );# if defined $DEBUG;
			next;
		}

		if( ! defined $op_mail_byname{$usr} ) {
			pr_message( $DEBUG,  "   -- (not alarming) active operator check\n" );# if defined $DEBUG;
			next;
		}

		$minsev=uc($minsev);
		$maxsev=uc($maxsev);
		if( $minsev ne "ALL" and $maxsev ne "ALL") {
			if( ! defined $severities{$state} ) {
				pr_message( 1,  "   -- No Severity For $state Check\n" );
				next;
			}
			if( $severities{$minsev} > $severities{$state} or
				$severities{$maxsev} < $severities{$state} ) {
				pr_message( $DEBUG,  "   -- (not alarming) Severity Check ($state not between $minsev and $maxsev)\n" );
				#print "SevMin=$severities{$minsev} SevState=$severities{$state} Smax=$severities{$maxsev}\n" if defined $DEBUG;
				next;
			}
		}

		# check if operator time active
		my($start,$end)=(-1,24);
		foreach my $d (@$operator_sched) {
			my(@row)=@$d;
			next unless $usr eq $row[0];
			if( $dow == 0 ) { # sunday
				$start=$row[5];
				$end=$row[6];
			} elsif( $dow == 6 ) {
				$start=$row[3];
				$end=$row[4];
			} else {
				$start=$row[1];
				$end=$row[2];
			}
		}

		$end=24 if defined $start and ! defined $end;
		$start=0 if ! defined $start and defined $end;
		if( ! defined $start or ! defined $end or $hour<$start or $hour>$end){
			pr_message( 1,  "   -- Failed Operator Time Check start=$start end=$end curtime=$hour\n" );
			next;
		}
		#pr_message( 1,  "Operator Time Check - Start=$start End=$end Time=$hour\n";
		pr_message( 1,  "###########################################################\nNOTIFYING OPERATOR=$usr pager=$usepager mail=$useemail netsend=$usenetsend\n###########################################################\n" );

		# at this point we are going to send it

		$message_sent_list{$usr}=1;

		my($msg2)=$msg;
		$msg2=~s/^system\s*//;
		$msg2=~s/^$subsystem\s*//;
		$msg2=~s/\(.+\)//;
		my($SUBJECT);
		if( $useblackberry eq "YES" ) {
			$SUBJECT=$short_state{$state}."."."$system $subsystem $msg2";
		} else {
			$SUBJECT="($state) $system $subsystem $msg2";
		#?#	$SUBJECT=~s/\(.\)// if $SUBJECT =~ /\(/;
		}

		# remove everything after two arrows
		$SUBJECT=~s/\>\>.+$//;

		my($MESSAGE)="system=$system subsystem=$subsystem state=$state\n$msg\n";

		if( 	defined	$ALLREADY_SENT{$op_mail_byname{$usr}.":".$MESSAGE}
			or defined 	$ALLREADY_SENT{$usr.":".$MESSAGE} ) {
				pr_message( 1,  "   -- DUPLICATE MESSAGE - THIS MESSAGE HAS ALLREADY BEEN SENT\n" );
				next;
			}

		$ALLREADY_SENT{$op_mail_byname{$usr}.":".$MESSAGE}=1;
		$ALLREADY_SENT{$usr.":".$MESSAGE}=1;

		#http://mlpdev2/mimi/mimi2.pl?Operation=setblackout&Server=IMAGSYBSTAGE1&Admin=Production&Value=1&BATCH_SUBMIT=1
		# add &debug=1 if you care
		if( ! defined $mimi_url ) {
			# my(%CONFIG)=read_configfile(-filename=>'gem.dat');
			my($CONFIG)=read_gemconfig(-filename=>'gem.dat',-debug=>1,READONLY=>1);
			$mimi_url=$CONFIG->{MIMI_URL};
			if( $FROM ) {
				print "FROM Email Address Is $FROM\n";
			} else {
				print "Setting FROM Email Address To $CONFIG->{ALARM_MAIL}\n";
				$FROM=$CONFIG->{ALARM_MAIL};
			}
			$mimi_url="UNKNOWN" unless defined $mimi_url and $mimi_url!~/^\s*$/;
		}

		push @send_count, $op_mail_byname{$usr};

		## BLACKOUT REMOVED - PEOPLE WERE SELECTING IT
		#$MESSAGE.="<br>\n<A HREF=\"".$mimi_url."?Operation=setblackout&Server=".$system."&Admin=Production&Value=1&BATCH_SUBMIT=1\">Blackout $system For 1 Day</A><br>\n<A HREF=\"".$mimi_url."?Operation=setblackout7days&Server=".$system."&Admin=Production&Value=1&BATCH_SUBMIT=1\">Blackout $system For 7 Days</A>\n\n"
		#	if $mimi_url ne "UNKNOWN";
		MlpSaveNotification(
			-monitor_program	=> 	$program,
			-system			=>	$system,
			-subsystem		=>	$subsystem,
			-severity		=>	$state,
			-monitor_time	=> 	$mon_time,
			-user_name		=>	$op_mail_byname{$usr},
			-notify_type	=>	"Mail",
			-header			=>	$SUBJECT,
			-message			=>	$MESSAGE ) if( ! $DEBUG and ! $TEST );

		if( defined $useemail ) {
			if( $lastmsg ne $MESSAGE ) {
				pr_message( 1,   "\n================ MESSAGE ===========\n");
				pr_message( 1,   "| ".join("\n\|",split(/\n/,$MESSAGE)) );
				pr_message( 1,   "\n====================================\n" );
				$lastmsg=$MESSAGE;
			}
			if( $DEBUG or $TEST ) {
				pr_message( 1,  "(Not Sending Test/Debug Mode) ");
			} else {
				pr_message( 1,  "Sending " );
			}

			pr_message( 1,  "Message To $usr ( $op_mail_byname{$usr} )\n" );
			pr_message( 1,   " From=$FROM\n"	   );
			pr_message( 1,   " Subject=$SUBJECT\n" )	if $DEBUG;

			if( ! $DEBUG and ! $TEST ) {
				$MESSAGE.="\n<br>\nMessage Generated By $program\nSent By MlpAlarmRoutingAgent.pl at ".localtime(time)."\nFrom Host ".hostname()."\n";
				#$MESSAGE.="\n<br>DIAGNOSTIC: $usr,$prog,$contr,$sys,$ssys,$minsev,$maxsev,$usepager,$useemail,$usenetsend,$useblackberry)\n";
				if( ! $NOSEND ) {
					my($msg)=MIME::Lite->new(
						To	=> $op_mail_byname{$usr},
						From	=> $FROM,
						Subject	=> $SUBJECT,
						Type=>'multipart/related' );
					$msg->attach(Type=>'text/html', Data=>$MESSAGE);
					$msg->send() || die "You DONT have MAIL!!! $!";
				} else {
					print "=========================\n";
					print "NOSEND: SENDING MAIL TO $op_mail_byname{$usr}\n";
					print "NOSEND: SUBJECT: $SUBJECT\n";
					print "NOSEND: MESSAGE: $MESSAGE\n";
					print "=========================\n";
				}
			}
		}
		pr_message( 1,   "Route Test Completed - next\n" );# if defined $DEBUG;
	}
	pr_message( 1,   "#\n#FINISHED SENDING ".join(" ",@send_count)," MESSAGES ON THE ABOVE ALERT!\n#\n" );

	#	if( defined $PAGETO ) {
	#		die "No Beep Program in /apps/bin/beep" unless -x "/apps/bin/beep";
	#		foreach my $target ( split(/,/,$PAGETO) ) {
	#			my($pager_id)=$pagers{lc($target)};
	#			die "ERROR: Invalid Pager - No Data For $target\n"
	#				unless $pager_id;
	#
	#			system("/apps/bin/beep $pager_id \'$MESSAGE\'");
	#		}
	#	}
}

sub pr_message {
	my($dostdout)=shift;
	print LOG @_ if $LOGOPEN;
	print @_ if $dostdout;
}

__END__

=head1 NAME

MlpAlarmRoutingAgent.pl - monitoring system alarm router

=head2 DESCRIPTION

This is the alarm router.  It sends alarms to operators based on routing data.

=head2 USAGE

Usage: $0 --SLEEPTIME=secs --MAXTIME=minutes --FROM=from --DEBUG

If sleeptime is defined, will check to see if allready running by looking for a lock file.

	--ROUTING displays the routing table
	--SLEEPTIME is the number of seconds to sleep between runs
	--FROM		email from
	--MAXTIME  max minutes this program is to stay alive (it quits then)
	--RESTART
	--TEST		work as normal but dont actually alarm
	--DEBUG"      	wont alarm

=head2 NOTES

The subject is based on the message.  Everything after >> will be ignored in the subject line.

=cut

