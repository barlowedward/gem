-- report_key=DM_DB_MISSING_INDEX
-- report_type=HTML
-- run_frequency=Daily
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_query=
-- include_server_query=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=This report details tables with (potentially) missing index columns
-- report_footer=
-- enabled=Y

select distinct
       db_name(z.database_id) as [database name]
      ,object_name(z.object_id, z.database_id) as [object name]
      ,equality_columns, inequality_columns, included_columns
      ,x.user_seeks + x.user_scans as [user access]
      ,x.avg_user_impact as [average impact]
  from sys.dm_db_missing_index_group_stats x
      ,sys.dm_db_missing_index_details z
 where x.group_handle = y.index_group_handle
   and z.index_handle = y.index_handle
   and z.database_id not in (db_id(N'master'), db_id(N'model'), db_id(N'distribution'), db_id(N'msdb'))
 order by [database name], [average impact] desc, [object name]

