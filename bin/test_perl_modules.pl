: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

# CONFIGURATION UTILITY FOR EDS SYBASE TOOLS
# copyright (c) 1997-2008 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

use strict;
my($num_errors)=0;

print "***************************************\n";
print "* Verbose GEM Perl Module Tester v1.0 *\n";
print "***************************************\n";
print "\n";
print ("This module tests your Perl library setup\n");
print ("   Your Perl version is $]\n");

#
# check min version
#
if( $] < 5.008001 and defined $ENV{PROCESSOR_LEVEL} ) {
	if( defined $^V and $^V!~/\s*$/) {
		print "ERROR: You must have perl 5.8.1 installed under Win32.  \nYou are running $^V \n";
		exit(1);
	}
	my($x)=$];
	$x=~s/0+/\./g;
	$x=~s/\.\.+/\./g;
	print "ERROR: You must have perl 5.8.1 installed.  \nYou are running $x ($])\n";
	exit(1);
}

my(%badpackage)=();
sub testit {
	my($pkg_arryref, $msgbad, $msgok)=@_;
	my $err;
	my $numbad=0;
	foreach my $str (@$pkg_arryref) {
		if ( eval "require $str" ) {
			$str->import();
		} else {
			my $err = $@;
			chomp $err;
			$numbad++;
			$badpackage{$str}=$err;
		}
	}

	if ($numbad ) {
		print $msgbad;
		foreach ( keys %badpackage) {
			print ("ERROR: Package Did Not Load: $_ \n");
			$num_errors++;
			if( defined $ENV{PROCESSOR_LEVEL} ) {
				print ("\tplease run: ppm install DBI\n")	if $_ eq "DBI";
				print ("\tplease run: ppm install DBD-ODBC\n") if $_ eq "DBD::ODBC";

				if(  $]<5.008 ) {
					print ("\tplease run: ppm install Win32-TaskScheduler --location=http://taskscheduler.sourceforge.net/perl/\n")
						if $_ eq "Win32::TaskScheduler";
				} else {
					print ("\tplease run: ppm install http://taskscheduler.sourceforge.net/perl58/Win32-TaskScheduler.ppd\n")
						if $_ eq "Win32::TaskScheduler";
				}
			}

			foreach ( split( /\n/,$badpackage{$_} )) {
				next if /^Can't locate /;
				print ("\t=> $_\n");
			}
		}
		print ("ERROR: The package(s) listed above MUST be installed before you may proceed.\n\n");
		$num_errors++;
	} else {
		print ( $msgok );
	}
}

my @std_packages=qw(CGI Getopt::Std Carp File::Copy File::Basename 
		Sys::Hostname Data::Dumper File::Find File::Path LWP::Simple
		Socket Tk URI::Escape Log::Dispatch Log::Dispatch::Screen
		Log::Dispatch::File Fcntl Getopt::Long );
		
my @tk_packages=qw(Tk::HList Tk::Tree Tk::DialogBox Tk::ItemStyle Tk::NoteBook Tk::BrowseEntry Tk::Listbox
			Tk::ErrorDialog Tk::Menu Tk::ROText Tk::Scrollbar Tk::Pixmap Tk::Label Tk::Balloon );

my @dbi_packages=qw(DBI);
if( defined $ENV{PROCESSOR_LEVEL}) {
	push @dbi_packages,"DBD::ODBC";
	push @dbi_packages,"Win32::TaskScheduler";
	push @dbi_packages,"Win32::API"; 
	push @dbi_packages,"Win32::DriveInfo"; 
	push @dbi_packages,"Win32::TaskScheduler"; 
	push @dbi_packages,"RunCommand"; 
	push @dbi_packages,"File::MultiTail";
} else {
	push @dbi_packages,"DBD::Sybase";
}

print ("   => Testing Perl Builtin Package Integrity\n      ");
testit( \@std_packages,
	"\nERROR: At least one pre-requisite required package was not found!\nThese are required parts of perl and must be available\nContact your system administrator\n",
	"   OK: All Required Builtin Perl Packages Found!!\n" );

print ("   => Testing Perl Database Connectivity Package Integrity\n      ");
testit( \@dbi_packages,
	"\nERROR: Database Tests FAILED\n",
	"   OK: All Required Perl/DBI Packages Found!!\n" );

if( ! defined $badpackage{DBI}) {
	if( $DBI::VERSION < 1.0 ) {
		print ("   ERROR - DBI Version ($DBI::VERSION) Must be at 1.0 or later");
	} else {
		print ("         OK: DBI Version is ($DBI::VERSION)\n");
	}
}

if( defined $ENV{PROCESSOR_LEVEL}) {
	print ( "   DBD::ODBC Version ($DBD::ODBC::VERSION) Should be at 1.0 or later" )
		if $DBD::ODBC::VERSION < 1.0;
	print ("         OK: DBD::ODBC Version is ($DBD::ODBC::VERSION)\n");
} else {
   if( $DBD::Sybase::VERSION < 1.0 ) {
	   print ("   ***************************************\n");
	   print ("   * WARNING - DBD::Sybase Version ($DBD::Sybase::VERSION) Should Probably be at 1.0 or later\n");
	   print ("   * This will not stop your install but a more stable version is better\n");
	   print ("   ***************************************\n");
   } else {
	   print ("         OK: DBD::Sybase Version is ($DBD::Sybase::VERSION)\n");
   };
}

print ("   => Testing Perl/Tk Package Integrity\n   ");
testit( \@tk_packages,
	"\nERROR: At least one Tk Package did not load found!\nThese are usually shipped with Tk - Is your version recent?\nYour Perl Tk Version is $Tk::VERSION",
	"   OK: All Required Perl/Tk Packages Found!!\n" );
if( $Tk::VERSION < 800.000 ) {
	   print ("   ***************************************\n");
	   print ("   * ERROR - Tk Version ($Tk::VERSION) MUST be 8.24 or later\n");
	   print ("   ***************************************\n");
} elsif( $Tk::VERSION < 800.024 ) {
	   print ("   ***************************************\n");
	   print ("   * WARNING - Tk Version ($Tk::VERSION) Should Probably be at 8.24 or later\n");
	   print ("   * This will not stop your install but a more stable version is better\n");
	   print ("   ***************************************\n");
} else {
	   print ("      OK: Tk Version is ($Tk::VERSION)\n" );
}
print "\n";	

print "**********************************\n";
print "* Completed Perl Module Tester *\n";
print "**********************************\n\n";

if( $num_errors > 0 ) {
	print "ERROR: Some Modules are Missing\n";
} else {
	print "SUCCESSFUL: All Modules are OK\n";
}