#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use Inventory;
use DBIFunc;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw($BATCH_ID $DEBUG);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." --BATCH_ID
k
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --DEBUG \n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

# die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"		=> \$BATCH_ID ,
		"DEBUG"      		=> \$DEBUG );

my($connection)=MlpGetDbh();
MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -AGENT_TYPE=>'Credential Mgt') if $BATCH_ID;
InvYpcatHosts();
MlpBatchJobEnd() if $BATCH_ID;
print "Successful Completion\n";
exit(0);

__END__
=pod

=head1 NAME

INV_load_ypcat_hosts - extract `ypcat hosts` into inventory system

=head1 SYNOPSIS

INV_load_ypcat_hosts.pl --BATCH_ID=s --DEBUG

=cut
