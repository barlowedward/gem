
select cntrycitybldgnum,floor,row,cabinet,cnt=count(*)
into #machines
from racks..machines
group by cntrycitybldgnum,floor,row,cabinet
order by cabinet,cntrycitybldgnum,floor,row

delete #machines
from #machines b, barlow_place_map m
where b.cntrycitybldgnum 	= m.dcm_placeid
and b.floor 				= m.dcm_cabfloor
and b.row		= m.dcm_cabrow
and b.cabinet 		= m.dcm_cabcab

select plcntry, plcity, plbuild, m.* from #machines m,racks..places p
where p.plid = m.cntrycitybldgnum
order by p.plid, floor, row

drop table #machines

-- select * from barlow_place_map where dcm_placeid=19 and dcm_floor='04
