#
# standard plugin header
#
use strict;
use Getopt::Long;
use vars qw( $DEBUG $CURDIR );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions( "DEBUG"	=> \$DEBUG, "CURDIR" => \$CURDIR );
my(@errors);
#
# end standard header
#

use File::Basename;
use CommonFunc;
use Repository;
my($curdir)=basename(dirname($0));

my(@MONITOR_SERVERS);

print "*****************************************************************\n";
print "* test_required_commands.pl: Testing System Commands       *\n";
print "*****************************************************************\n\n";
	
my($GemConfig) = read_gemconfig(-isconfigured=>1);
if( ! defined $GemConfig ) {
	print "ERROR: CANT FIND gem.dat ???";
	print "THIS IS A FATAL ERROR - THE SAMPLE FILES SHOULD HAVE BEEN COPIED VIA test_copy_configfiles.pl AT THIS POINT\n";
	print "ERROR: Can not proceed! exit()\n";
	exit(0);
}

print "Reading Configuration File gem.dat\n";
if( $GemConfig->{INSTALLSTATE} eq "NOT_INSTALLED" ) {
	print "This is an initial install\n";
}

#
# FIND MONITORING STATIONS
#
foreach ( keys %$GemConfig ) {
	if( /MONITOR_SERVER\s*=/ ) {
		my($a,$b)=split(/\s*=\s*/);
		$b=~s/\s//g;
		print "Found Existing Monitor Server = $b\n";
		push @MONITOR_SERVERS,$b;			
	}
}

if( $#MONITOR_SERVERS >= 0 ) {
	print "\tMONITOR_SERVERS: ".join(" ",@MONITOR_SERVERS),"\n";
} else {
	print "\tNo MONITOR_SERVERS Found: possible initial install\n"
	 	if $GemConfig->{INSTALLSTATE} ne "NOT_INSTALLED";
}

use Sys::Hostname;
my($hostname)=hostname();

# test out the required external command
my($state)="OK";
print "\nTesting commands that should be in your path\n\n";
my($num_errors)=0;
foreach my $cmd ('ping -?','osql -?','isql -v','sqlplus -H','mysql -V','dsget.exe -?','scp -?','ssh -?') {
	next if $cmd eq "osql -?" and !  $ENV{PROCESSOR_LEVEL};	
	next if $cmd eq "dsget.exe -?" and !  $ENV{PROCESSOR_LEVEL};	
	next if $cmd eq "dsquery.exe -?" and !  $ENV{PROCESSOR_LEVEL};	
	next if $cmd eq "ssh -?" and $ENV{PROCESSOR_LEVEL};	
	next if $cmd eq "scp -?" and $ENV{PROCESSOR_LEVEL};		
	
	print "Running $cmd\n" if $DEBUG;
	my(@rc)=run_cmd($cmd);
	my($ok)=1;
	foreach(@rc){
		chomp;
		if( /is not recognized as an internal/  or /not found/ ) { $ok=0; last;	}
		print "returned -> $_\n" if $DEBUG;
	}
	print "$#rc rows returned for $cmd!\n" if $DEBUG;
	$ok=0 unless $#rc>5 or $cmd =~ /ping/ or $cmd =~ /mysql/ or $cmd=~/scp/ or $cmd=~/ssh/;	# 5 row minimum
	$ok=0 if $cmd=~/mysql/ and $#rc!=0;									# 1 line from mysql	
	
	if( ! $ok ) {
		if( $cmd eq "osql -?" and $GemConfig->{USE_SQLSERVER} eq "N" ) {
			#print "Skipping osql test as USE_SQLSERVER=$GemConfig->{USE_SQLSERVER}\n";
			next;
		}	
		if( $cmd eq "sqlplus -H" and $GemConfig->{USE_ORACLE} eq "N" ) {
			#print "Skipping sqlplus test as USE_ORACLE=$GemConfig->{USE_ORACLE}\n";
			next;
		}		
		if( $cmd eq "mysql -V" and $ENV{PROCESSOR_LEVEL} ) {
			#print "Skipping sqlplus test as USE_ORACLE=$GemConfig->{USE_ORACLE}\n";
			next;
		}		
		if( $cmd eq "isql -v" and $GemConfig->{USE_SYBASE_ASE} eq "N" ) {
			#print "Skipping isql test as USE_SYBASE_ASE=$GemConfig->{USE_SYBASE_ASE}\n";
			next;
		}	
		if( $cmd eq "mysql -V" and $GemConfig->{USE_MYSQL} eq "N" ) {
			#print "Skipping mysql test as USE_MYSQL=$GemConfig->{USE_MYSQL}\n";
			next;
		}	
		if( $cmd eq "dsget.exe -?" and $GemConfig->{USE_LDAP} eq "N" ) {
			next;
		}		
	}
		
	if( ! $ok ) {
		print "   ***************************************\n";
		print "   * WARNING:  $cmd appears to not be runnable/in your path\n";
		
		if( $cmd eq "osql -?" ) {
			print "   * WARNING:  osql is required for sql server access\n"; 
			print "   * WARNING:  this system does NOT have osql in its path\n";
			$GemConfig->{$hostname."_USE_SQLSERVER"}  = 'N';			
		} elsif( $cmd eq "ping -?" ) {
			print "ERROR:  ping is required for system monitoring\n";
			print "ERROR:  please ensure that ping is in your path\n";
			exit(1);			
		} elsif( $cmd eq "sqlplus -H" ) {
			print "   * WARNING:  sqlplus is required for oracle monitoring\n";
			print "   * WARNING:  this system does NOT have sqlplus in its path\n";
			$GemConfig->{$hostname."_USE_ORACLE"} = 'N';			
		} elsif( $cmd eq "isql -v" ) {
			print "   * WARNING:  isql is required for sybase access\n";
			print "   * WARNING:  this system does NOT have isql in its path\n";
			$GemConfig->{$hostname."_USE_SYBASE_ASE"} = 'N';
		} elsif( $cmd eq "mysql -V" ) {
			print "   * WARNING:  mysql is required for mysql access\n";
			print "   * WARNING:  this system does NOT have mysql in its path\n";
			$GemConfig->{$hostname."_USE_MYSQL"} = 'N';
		} elsif( $cmd eq "dsget.exe -?" ) {
			print "   * WARNING:  dsget.exe is required for ldap access\n";
			print "   * WARNING:  this system does NOT have dsget in its path\n";
			$GemConfig->{$hostname."_USE_LDAP"} = 'N';
		} elsif( $cmd eq "ssh -?" ) {
			print "   * ERROR:  ssh is required for unix communication\n";
			print "   * ERROR:  this system does NOT have ssh in its path\n";			
		} elsif( $cmd eq "scp -?" ) {
			print "   * ERROR:  scp is required for unix communication\n";
			print "   * ERROR:  this system does NOT have scp in its path\n";
		} else {
			die "INTERNAL ERROR = cmd should not be $cmd";
		}

		foreach ( @rc) { 
			chomp; 
			print "   * ".$_."\n" unless /is not recognized as an internal/  or /not found/ or /operable program or batch/; 
		}
		
		print "   ***************************************\n";
		$num_errors++;
	} else {
		print "   $cmd succeeded\n";
		my(@paths);
		if( $ENV{PATH} =~ /;/ ) {
			# windows
			@paths=split(/;/,$ENV{PATH});
		} else {
			@paths=split(/:/,$ENV{PATH});
		}
		# my(@x)=split(/[;:]/,$ENV{PATH});
		my($c)=split(/\s/,$cmd);
	#	print "DBG: Searching for $c\n";
		my($found)="";
		foreach ( @paths ) { 
			# print "Testing $_ for $c\n";
			next unless -x "$_/$c" or -x "$_/$c.exe";
			print "      Found $c in $_\n";
			$found=$_;
			$found=~s/\\/\//g;
			if( -x "$_/$c" ) {
				$found.="/$c";
			} else {
				$found.="/$c.exe";
			}
			last;
		}
		
		print "   $c ran but was not found in the path?" if $found eq "";
		print "      path = $ENV{PATH} \n" if $found eq "";
		print "      $c => $found\n" if $found ne "";
		
		$found = "Y" if $found eq "";
		
		if( $cmd eq "osql -?" ) {
			$GemConfig->{$hostname."_CMD_OSQL"}  = $found;			
		} elsif( $cmd eq "ping -?" ) {
			$GemConfig->{$hostname."_CMD_PING"} = $found;			
		} elsif( $cmd eq "ssh -?" ) {
			$GemConfig->{$hostname."_CMD_SSH"} = $found;			
		} elsif( $cmd eq "scp -?" ) {
			$GemConfig->{$hostname."_CMD_SCP"} = $found;			
		} elsif( $cmd eq "sqlplus -H" ) {
			$GemConfig->{$hostname."_CMD_SQLPLUS"} = $found;			
		} elsif( $cmd eq "isql -v" ) {
			$GemConfig->{$hostname."_CMD_ISQL"} = $found;
		} elsif( $cmd eq "mysql -V" ) {
			$GemConfig->{$hostname."_CMD_MYSQL"} = $found;
		} elsif( $cmd eq "dsget.exe -?" ) {
			$GemConfig->{$hostname."_CMD_DSGET"} = $found;
		} else {
			die "INTERNAL ERROR = cmd should not be $cmd";
		}
	}	
	print "\n";
}

close_gemconfig();
print "SUCCESSFUL: found all required commands\n" if $num_errors==0;
exit($num_errors);

sub run_cmd {
	my($ocmd)=@_;
	my($cmd)=$ocmd;
	$cmd.=" 2>&1 |";
	my($k)=open(XCMD,$cmd);
	if( ! $k ) { return "ERROR: Cant run $ocmd : $!\n";  }
	my(@rc);
	foreach (<XCMD>) {
		chomp;
		push @rc,$_;				
	}
	close(XCMD);
	return @rc;
}

##print   "$^X $includes $dir/bin/test_required_commands.pl \n";
##open( XXX, "$^X $includes $dir/bin/test_required_commands.pl |" )
##	or die "Cant Run $^X $includes $dir/bin/test_required_commands.pl\n";
#my(@warnings);
#my( $is_sqlsvr_ok, $is_sybase_ok, $is_oracle_ok, $is_initial_install ) = ('?','?','?','?');
#my(@MONITOR_SERVERS);
#my($ok)="";	
#my(@deferredmsgs);
## foreach( <XXX> ) {	
#foreach( @{$rowresults} ) {	
#	push @warnings, $_ if /WARNING:/;	
#	if( /ERROR:/ ) {
#		$ok="FALSE";
#	} elsif ( /^USE_/ ) {
#		if( /^USE_SYBASE_ASE=/ ) { s/^USE_SYBASE_ASE=//; chomp; chomp; $is_sybase_ok = $_; push @deferredmsgs, "   * Turning OFF Sybase Monitoring\n" if $_ eq 'N';}
#		if( /^USE_ORACLE=/ ) { s/^USE_ORACLE=//; chomp; chomp; 			$is_oracle_ok = $_; push @deferredmsgs, "   * Turning OFF Oracle Monitoring\n" if $_ eq 'N';}
#		if( /^USE_SQLSERVER=/ )  { s/^USE_SQLSERVER=//;  chomp; chomp; $is_sqlsvr_ok = $_; push @deferredmsgs, "   * Turning OFF SqlSvr Monitoring\n" if $_ eq 'N';}		
#	} elsif( /^SUCCESSFUL:/ ) {		
#		$ok="TRUE";	
#		print "> ",$_;
#	} elsif( /No MONITOR_SERVERS Found:/i ) {
#		$is_initial_install='Y';
#	} elsif( /Existing Install - Reading/ ) {	
#		$is_initial_install='N';		
#	} elsif( /^MONITOR_SERVERS:/ ) {	
#		print "> ",$_;
#		$is_initial_install='N';
#		@MONITOR_SERVERS=split;		
#	} else {
#		print "> ",$_ unless /^Completed Testing/;
#	}	
#}
#close(XXX);
#
## *STEP - set $is_existing_monitor_station = TRUE|FALSE
#my($is_existing_monitor_station)='FALSE';
#foreach (@MONITOR_SERVERS) {  $is_existing_monitor_station='TRUE' if $hostname eq $_; }
## print "Is Existing Monitor Station = $is_existing_monitor_station \n" if $DEBUG;
#
## *STEP - IF $is_initial_install & $ok : SUITABLE
##       - IF $is_initial_install & ! ok : warn and ask
#if( $is_initial_install eq "Y" ) {
#	system("cls 2> /dev/null")   if $^O =~ /MSWin32/;
#	system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;
#	print "************************\n";
#  	print "* INITIAL INSTALLATION *\n";
#  	print "************************\n";
#  	if( $ok eq "TRUE" ) {
#  		print "This System is Suitable As A Monitoring Station\n";
#  		$REGISTER_THIS_AS_MONSTATION='TRUE';  		
#  	} else {
#  		print "
#   ******************************
#   * configure.pl has encountered a missing prerequesite.
#   *\n";
#		print join("",@warnings);
#		print "   *\n";
#		print "   * Missing client software turns OFF GEM features. Review the above WARNINGs 
#   * to ensure they make sense.  If not, abort the install, install the client
#   * software and rerun configure.pl.
#   *\n";
#   print @deferredmsgs;
#		print "   ******************************
#		
#Use Control-C To abort this application
#Would you still like to register $hostname as a monitoring station (y/n)\n";
#	
#	my($a);
#	$a=<STDIN>;
#	while ( $a !~ /n/i and $a !~ /y/i ) {
#		print "Woah - must enter y or n... \n";
#		$a=<STDIN>;
#	}
#	$REGISTER_THIS_AS_MONSTATION='TRUE' if $a =~ /y/i;	
#  	}
#} elsif( $is_initial_install eq "N" ) {
#	system("cls 2> /dev/null")   if $^O =~ /MSWin32/;
#	system("clear 2> /dev/null") unless  $^O =~ /MSWin32/;
#	print "*************************\n";
#  	print "* EXISTING INSTALLATION *\n";
#  	print "*************************\n";
#  	
#	if( $ok eq "TRUE" ) {
#		if( $is_existing_monitor_station eq 'TRUE' ) {
#			print "This system IS a GEM Monitoring Station\n";
#		} else {
#  			print "This System is Suitable As A Monitoring Station\n";
#  			print "Would you like to register this system as a NEW monitoring station (y/n)\n";
#  			
#			$a=<STDIN>;
#			while ( $a !~ /n/i and $a !~ /y/i ) {
#				print "Woah - must enter y or n... \n";
#				$a=<STDIN>;
#			}	
#			print "Continuing...\n";	
#			$REGISTER_THIS_AS_MONSTATION='TRUE' if $a =~ /y/i;
#  		}
#  	} else {
#  		if( $is_existing_monitor_station eq 'TRUE' ) {
#	  		print "
#   ******************************
#   * configure.pl has encountered a missing prerequesite.
#   *
#   * check that all required commands are in your path\n";
#			print "   *\n";
#			print join("",@warnings);
#			print "   *\n";
#			print "   * Missing commands will turn OFF features \n";
#			print "   *\n";
#			print "   * Use Control-C To abort this application
#   * If everything is ok, continue by hitting the enter key.
#   ******************************
#	
#Hit Enter To Continue, CTRL-C to Exit\n";
#		
#		$a=<STDIN>;
#		print "Continuing...\n";	
#			} else {
#  			print "
#		   ******************************
#		   * configure.pl has encountered a missing prerequesite.
#		   *
#		   * check that all required commands are in your path\n";
#			print "   *\n";
#			print join("",@warnings);
#			print "   *\n";
#			print "   * THIS SYSTEM IS NOT SUITABLE AS A GEM MONITOR STATION\n";
#			print "   *\n";
#			print "   * Use Control-C To abort this application
#		   * If everything is ok, continue by hitting the enter key.
#		   ******************************
#			
#			Despite the above errors, would you still like to register this system as a NEW monitoring station (y/n)\n";
#			
#			$a=<STDIN>;
#			while ( $a !~ /n/i and $a !~ /y/i ) {
#				print "Woah - must enter y or n... \n";
#				$a=<STDIN>;
#			}				
#			print "Continuing...\n";	
#			$REGISTER_THIS_AS_MONSTATION='TRUE' if $a =~ /y/i;	  		
#  		}		
#  	}
#} else {
#	die "ERROR - THIS SHOULD NOT HAPPEN\nis_initial_install = $is_initial_install\nAbnormal Program Termination\n";
#}
#
#
#if( $REGISTER_THIS_AS_MONSTATION eq 'TRUE' ) {
#	print "\n***********************************************************\n";
#  	print "* $hostname WILL be installed as a GEM Monitoring Station *\n";
#  	print "***********************************************************\n";
#} else {
#	print "\nSERVER is NOT suitable as a monitor station\nREGISTER_THIS_AS_MONSTATION=$REGISTER_THIS_AS_MONSTATION\n";
#}
#
