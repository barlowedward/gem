#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2006 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Std;
use Repository;
use File::Basename;
use File::Find;
use CommonHeader;
use Do_Time;

# cd to the right directory
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

$| = 1;

sub usage
{
	print @_;
	print "space_errors.pl -i sourcedir -A -H

   -d debug mode
   -H use html
\n";
   return "\n";
}

use vars qw($opt_i $opt_A $opt_H $opt_d);

die usage("Bad Parameter List\n") unless getopts('i:AHd');
if( ! defined $opt_i ) {
	my($gr_dir)=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gr_dir;
	$opt_i = "$gr_dir/data/system_information_data";
	die "No Input File $opt_i" unless -d $opt_i;
}

$opt_d=1 if defined $opt_d;

die usage("Must Pass Output Directory\n") unless defined $opt_i;
die usage("Output Directory $opt_i does not exist\n") unless -d $opt_i;
$opt_i=~s/\s*$//;
$opt_i=~s/\/$//;

my($cur_day)=do_time(-fmt=>'yyyymmdd');

#my($first_day,$last_day)=(21000101,0);
my %SRVR_DB_LIST;			# server\sdb
my %DB_SPACE_RESULTS;	# server\sdb\sday
my %DB_SPACE_PERCENT;	# server\sdb\sday
my %LOG_SPACE_PERCENT;	# server\sdb\sday

if( defined $opt_H ) {
	print "<h1>List of Databases with data or log over 80% Full</h1>\n";
	print "Run at ".localtime(time)." by space_errors.pl version 1.1<br>\n";
	print "Files Found in $opt_i\n";
	print "<TABLE BORDER=1>\n";
} else {
	print "List of Databases with data or log over 80% Full\n";
	print "Run at ".localtime(time)." by space_errors.pl version 1.1\n";
	print "Files Found in $opt_i\n";
}

my($starttime)=time;
print "Reading directory $opt_i\n";
find(\&wanted,$opt_i);
print "	 --> file parse completed in ",fmt_time(time,$starttime),"\n";
$starttime=time;

foreach ( sort keys %SRVR_DB_LIST ) {
	my($srvr,$db)=split;
	my($dbpct) =	$DB_SPACE_PERCENT{$_};
	next unless defined $dbpct;	# no data for today
	my($logpct)=	$LOG_SPACE_PERCENT{$_};
	my($dbusd) =	$DB_SPACE_RESULTS{$_};
	next unless $dbpct>80	or $logpct>80;
	my $color="BGCOLOR=PINK" if $dbpct>90;
	if( defined $opt_H ) {
		print "<TR $color><TD $color>",$srvr,
			"</TD><TD $color>",
			$db,
			"</TD><TD $color>",
			$dbpct."%",
			"</TD><TD $color>",
			$dbusd."MB",
			"</TD></TR>\n";
	} else {
		printf( "%20s %20s %10s%% %10s\n",
			$srvr,
			$db,
			$dbpct,
			$dbusd."MB");
	}
}
print "</TABLE>\n" if defined $opt_H;
print "	 --> report write completed in ",fmt_time(time,$starttime),"\n";

exit(0);


sub fmt_time
{
	my($endtime,$starttime)=@_;
	my $minutes=int(($endtime-$starttime)/60);
	return "$minutes min ".($endtime-$starttime-60*$minutes)." sec.";
}
sub wanted
{
	return unless /^dbspace/;
	my($dummy,$server,$db)=split(/\./,$_);
	# get
	print "reading $_\n" if defined $opt_d;
	if( ! -r $_ ) {
		print "Skipping File [".$File::Find::name."]\n";
		print "<br>" if $opt_H;
		return;
	}
	my($rc) = open(FIL,$_);
	if( ! $rc ) {
		warn "Non Fatal Error - Cant open file [".$File::Find::name."] $!\n";
		return;
	}
	seek(FIL,-200,2);	# start 100 bytes from end of file
	<FIL>;
	while ( <FIL> ) {
		chomp;
		my($day,$dbase,$sp_all,$sp_usd,$sp_pct,$log_all,$log_usd,$log_pct)=split;

		#$first_day = $day if $first_day > $day;
		#$last_day  = $day if $last_day  < $day;
		next unless $day eq $cur_day;
		if ( ! defined $log_pct or $log_pct =~ /^\s*$/ ) {
			print "(no log pct) $_ \n" if defined $opt_d;
			$log_pct=0;
		};
		$SRVR_DB_LIST{$server." ".$db}=1;
		$DB_SPACE_RESULTS{$server." ".$db}  = $sp_usd;
		$DB_SPACE_PERCENT{$server." ".$db}  = $sp_pct;
		$LOG_SPACE_PERCENT{$server." ".$db} = $log_pct;
	}
	close FIL;
}

__END__

=head1 NAME

space_errors.pl - report on space

=head2 USAGE

space_errors.pl -i sourcedir  -A -N

   -d debug mode
   -N dont use html

=cut
