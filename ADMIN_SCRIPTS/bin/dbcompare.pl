#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
use Cwd;
my($curdir)=cwd();
my($fullcmd)=$^X." ".$curdir."/dbcompare.pl ".join(" ",@ARGV);

select (STDOUT); $| = 1;                # make output unbuffered

sub usage
{
        print @_,"\n";
        print "dbcompare.pl [-axh]  -FFile -Ssrv -Uusr -Ppass -Ddb -ffile -ssrv -uusr -ppass -ddb

      Compare 2 Databases DDL.  Can be from File (use -F) or DB (-SUPD)
      If both -SUPD and -F are supplied, file is written.
      If you ignore one server (ie the lower case side), the values are copied from the other.  This
        saves typing

         -g no group permissions
         -t dont show text of proc differences
         -o object name (for debugging purposes)
         -h htmlize output
         -a tables only
         -i ignore identical output
         -x debug mode \n";

        return "\n";
}

my(%obj_desc)=(
        "U "=>"Table",
        "P "=>"Procedure",
        "S "=>"System Proc",
        "V "=>"View",
        "TR"=>"Trig",
        "D "=>"Dflt",
        "XP"=>"XP",
        "R "=>"Rule",
);

use vars qw($opt_a $opt_t $opt_p $opt_i $opt_h $opt_D $opt_U $opt_S $opt_P $opt_d $opt_s $opt_u $opt_g $opt_x $opt_c $opt_o $opt_F $opt_f);
die usage("Bad Parameter List") unless getopts('atxU:D:S:P:d:u:p:s:chipgo:f:F:');

use strict;
use Getopt::Std;
use DBIFunc;

my($NL)="\n";
$NL="<br>\n"            if defined $opt_h;

die usage("")   unless defined $opt_U
                                                        or defined $opt_P
                                                        or defined $opt_S
                                                        or defined $opt_D
                                                        or defined $opt_F;
die usage("")   unless defined $opt_u
                                                        or defined $opt_p
                                                        or defined $opt_s
                                                        or defined $opt_d
                                                        or defined $opt_f;

$opt_d = $opt_D if defined $opt_D and ! defined $opt_d;
$opt_D = $opt_d if defined $opt_d and ! defined $opt_D;

$opt_s = $opt_S if defined $opt_S and ! defined $opt_s and ! defined $opt_f;
$opt_S = $opt_s if defined $opt_s and ! defined $opt_S and ! defined $opt_S;

die usage("must provide -S or -F option")
        unless defined $opt_F or defined $opt_S;
die usage("must provide -s or -f option")
        unless defined $opt_f or defined $opt_s;

my($label_s,$label_S)= ($opt_s,$opt_S);
if( defined $opt_s and defined $opt_S and $opt_S eq $opt_s ) {
        #die usage("ERROR NO DATABASES PASSED")
                #unless defined $opt_d and defined $opt_D;
        $label_s=$opt_d;
        $label_S=$opt_D;
}
$label_s=$opt_f unless defined $opt_s;
$label_S=$opt_F unless defined $opt_S;

$opt_p = $opt_P if defined $opt_P and ! defined $opt_p ;
$opt_P = $opt_p if defined $opt_p and ! defined $opt_P ;

$opt_u = $opt_U if defined $opt_U and ! defined $opt_u ;
$opt_U = $opt_u if defined $opt_u and ! defined $opt_U ;

if( defined $opt_S ) {
        die usage("Bad User Info -U\n")         unless defined $opt_U;
        die usage("Bad Srv Info -S\n")  unless defined $opt_S;
        die usage("Bad Pass Info -P\n")         unless defined $opt_P;
}

if( defined $opt_s ) {
        die usage("Bad User Info -u\n")         unless defined $opt_u;
        die usage("Bad Srv Info -s\n")  unless defined $opt_s;
        die usage("Bad Pass Info -p\n")         unless defined $opt_p;
}

print "<H1>" if defined $opt_h;
print "Server Comparison Utilitity\n\n";
print "</H1><p>" if defined $opt_h;

print "The following output is a comparison of two databases.  This comparison is made based on data retrieved from the Sybase catalogs regarding database objects and their definition.  When comparing procedures, rules, defaults, and triggers, the actual text (from syscomments) is compared. The comparison is fairly accurate but users of this utility should understand how this program computes what is displayed.  The comparison done here extracts the text, strips out all comments, removes whitespace, and then removes, and then removes similar strings at the start and end of procedure. The displayed text is what is the remainder.$NL  \n\n";

if( defined $opt_h ) {
        print "<H2>RUN INFORMATION</H2>\n";
        print "<FONT COLOR=BLUE>SERVER 1: $opt_s  $opt_d </FONT><br>\n"
                if defined $opt_s;
        print "<FONT COLOR=GREEN>SERVER 2: $opt_S  $opt_D </FONT><br>\n"
                if defined $opt_S;
        print "<FONT COLOR=BLUE>SERVER 1: FILE $opt_f </FONT><br>\n"
                unless defined $opt_s;
        print "<FONT COLOR=GREEN>SERVER 2: FILE $opt_F </FONT><br>\n"
                unless defined $opt_S;

        print "Group Permissions: No<br>\n" if defined $opt_g;
        print "Group Permissions: Yes<br>\n" unless defined $opt_g;

        print "Show Identical: No<br>\n" if defined $opt_i;
        print "Show Identical: Yes<br>\n" unless defined $opt_i;

        print "Tables only<br>\n" if defined $opt_a;
}

if( !defined $opt_f and ! defined $opt_F
                and (! defined $opt_d or ! defined $opt_D )) {

                print "GETTING DATABASE LIST\n" if defined $opt_x;

                my(%DB);
                dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
                        or die "Cant connect to $opt_S as $opt_U\n";


                my($x)=dbi_query(-db=>"master",-query=>"exec sp__proclib_version");
                my @ver=dbi_decode_row($x);
                die "Error.  Extended stored procedure library must be at v6.1 or later.  Server $opt_S is at $ver[0].\n" if $ver[0] < 6.1;

                foreach (dbi_query(-db=>"master"
                	,-query=>"select name from master..sysdatabases  where status & 0x1120 = 0 and status2&0x0030=0")) {
                		my($d)=dbi_decode_row($_);
                        	$DB{$d}=0
                                if $d ne "master"
                                and $d ne "model"
                                and $d ne "tempdb"
                                and $d ne "sybsystemprocs"
                                and $d ne "sybsecurity"
                                and $d ne "sybsyntax"
                                and $d ne "model";
                }

                dbi_disconnect();

                dbi_connect(-srv=>$opt_s,-login=>$opt_u,-password=>$opt_p)
                        or die "Cant connect to $opt_s as $opt_u\n";

                my($x)=dbi_query(-db=>"master",-query=>"exec sp__proclib_version");
                my @ver=dbi_decode_row($x);

                die "Error.  Extended stored procedure library must be at v6.1 or later.  Server $opt_s is at $ver[0].\n" if $ver[0] < 6.1;


                foreach (dbi_query(-db=>"master",-query=>"select name from master..sysdatabases  where status & 0x1120 = 0 and status2&0x0030=0")) {
              		my($d)=dbi_decode_row($_);
                        $DB{$d}=1
                                if $DB{$d}==0
                                and $d ne "master"
                                and $d ne "model"
                                and $d ne "tempdb"
                                and $d ne "sybsystemprocs"
                                and $d ne "sybsecurity"
                                and $d ne "sybsyntax"
                                and $d ne "model";
                }

                dbi_disconnect();

                foreach (keys %DB) {
                        next  unless $DB{$_} == 1;
                        print "<h2>PROCESSING $_</h2>\n";
                        my $string=$fullcmd." -D$_ -d$_ 2>&1 |";
                        open( SYS2STD,$string ) || s_quit("Unable To Run $string");
                        my($found)=0;
                        while ( <SYS2STD> ) {
                                $found=1 if /OBJECT/;
                                print $_ if $found;
                        }
                        close(SYS2STD);

                        print "<hr>" if defined $opt_h;
                        print "\n\n";
                }

                exit;
}

if( defined $opt_h ) {
        print "<H2>OUTPUT INFORMATION</H2>\n";
        print "<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=1 BORDER=1>";
        print "<TR><TH>OBJECT</TH><TH>SRV 1</TH><TH>SRV 2</TH><TH>DESCRIPTION</TH></TR>\n";
} else {
        printf("%30s %6.6s %6.6s %s\n",
                        "OBJECT",
                        "SRV #1",
                        "SRV #2",
                        "DESCRIPTION" );
}

dbi_set_web_page(1)      if defined $opt_h;

my($sql_sysobj) = "select name,type,object_name(deltrig),object_name(instrig),object_name(updtrig),convert(char(11),crdate,100) from sysobjects where uid=1 and type != 'S'";
$sql_sysobj .= " and type = \"U\"" if defined $opt_a;
my($sql_syscol) = "exec sp__helpcolumn \@dont_format=\"Y\"";
my($sql_sysprot) = "exec sp__helprotect \@groups_only='Y'";
my($sql_index) = "exec sp__helpindex \@dont_format='Y', \@no_print='A'";

my(%sysobjects_S,%syscolumns_S,%syscomments_S,%sysprotects_S,%index_S);
if( defined $opt_S ) {
        dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
                or die "Cant connect to $opt_S as $opt_U\n";

        printf( "TIME=%-10d STARTING RETRIEVES IN $label_S $NL",time-$^T)
                if defined $opt_x;

        my(@keys)=(0);
        my(@val)=(1,2,3,4,5);
        %sysobjects_S = dbx_query_to_hash($opt_D,$sql_sysobj," ",\@keys,\@val);

        @keys=(6,0);
        @val=(1,2,3,7);
        %syscolumns_S = dbx_query_to_hash($opt_D,$sql_syscol," ",\@keys,\@val);
        %syscomments_S= get_syscomments($opt_D);

        @keys=(0);
        @val=();
        %sysprotects_S = dbx_query_to_hash($opt_D,$sql_sysprot," ",\@keys,\@val)
                unless defined $opt_g;

        @keys=(0);
        @val=(1,2,3,4,5,6);
        %index_S = dbx_query_to_hash($opt_D,$sql_index," ",\@keys,\@val);

        dbi_disconnect();

        # NOW WRITE FILE
        if( defined $opt_F ) {
                print "Writing File $opt_F\n";
                open(WRFILE,">$opt_F") or die "Cant Write $opt_F";
                foreach (keys %sysobjects_S)  {
                        print WRFILE "O|$_|".$sysobjects_S{$_}."\n";
                }
                foreach (keys %syscolumns_S)  {
                        print WRFILE "COL|$_|".$syscolumns_S{$_}."\n";
                }
                my($val);
                foreach (keys %syscomments_S) {
                        $val=$syscomments_S{$_};
                        $val=~s/\n/\?\?/g;
                        print WRFILE "COM|$_|".$val."\n";
                }
                foreach (keys %sysprotects_S) {
                        $val=$sysprotects_S{$_};
                        $val=~s/\n/\?\?/g;
                        print WRFILE "P|$_|".$val."\n";
                }
                foreach (keys %index_S)       {
                        $val=$index_S{$_};
                        $val=~s/\n/\?\?/g;
                        print WRFILE "I|$_|".$val."\n";
                }
                close WRFILE;
        }
} else {
        # read from FILE
        print "Reading File $opt_F\n";
        die "Cant read file $opt_F" unless -r $opt_F;
        open(RDFILE,"$opt_F") or die "Cant Read File $opt_F";
        my $cnt=0;
        while (<RDFILE>) {
                my($typ,$realkey,$val)=split(/\|/,$_,3);
                $val=~s/\?\?/\n/g;
                if( $typ eq 'O' ) {
                        $sysobjects_S{$realkey}  = $val;
                } elsif( $typ eq 'COL' ) {
                        $syscolumns_S{$realkey}  = $val;
                } elsif( $typ eq 'COM' ) {
                        $syscomments_S{$realkey} = $val;
                } elsif( $typ eq 'P' ) {
                        $sysprotects_S{$realkey} = $val;
                } elsif( $typ eq 'I' ) {
                        $index_S{$realkey}       = $val;
                } else {
                        print "Ignoring Line $cnt of $opt_F - Invalid Key Type $NL";
                }
                $cnt++;
        }
        print "Read $cnt Items From DB\n";
        close RDFILE;
}

sub dbx_query_to_hash
{
	my($db,$query,$keysep,$k,$v)=@_;
	DBIFunc::debugmsg("Running Cmd To Hash Keys=@$k Vals=@$v");

	my(@keys)=@$k;
	my(@values)=@$v;
	my(%results_hash);
	foreach my $row ( dbi_query(-db=>$db,-query=>$query) ) {
		my @rc=dbi_decode_row($row);
		my $key = "";
		foreach (@keys) {
			$key.=$keysep unless $key eq "";
			$key.=$rc[$_];
		}
		my $val = "";
		foreach (@values) {
			$val.="~~" unless $val eq "";
			$val.=$rc[$_];
			#push @val, $rc[$_];
		}
		DBIFunc::debugmsg("Res $key => $val");
		$results_hash{$key} = $val;
	}
	return(%results_hash);
}

my(%sysobjects_s,%syscolumns_s,%syscomments_s,%sysprotects_s,%index_s);
if( defined $opt_s ) {
        dbi_connect(-srv=>$opt_s,-login=>$opt_u,-password=>$opt_p)
                or die "Cant connect to $opt_s as $opt_u\n";

        printf( "TIME=%-10d STARTING RETRIEVES IN $label_s $NL",time-$^T)
                if defined $opt_x;

        my @keys=(0);
        my @val=(1,2,3,4,5);
        %sysobjects_s = dbx_query_to_hash($opt_d,$sql_sysobj," ",\@keys,\@val);
        %syscomments_s= get_syscomments($opt_d);

        @keys=(6,0);
        @val=(1,2,3,7);
        %syscolumns_s = dbx_query_to_hash($opt_d,$sql_syscol," ",\@keys,\@val);

        @keys=(0);
        @val=();
        %sysprotects_s = dbx_query_to_hash($opt_d,$sql_sysprot," ",\@keys,\@val)
                unless defined $opt_g;

        @keys=(0);
        @val=(1,2,3,4,5,6);
        %index_s = dbx_query_to_hash($opt_d,$sql_index," ",\@keys,\@val);

        dbi_disconnect();

        # NOW WRITE FILE
        if( defined $opt_f ) {
                print "Writing File $opt_f\n";
                open(WRFILE,">$opt_f") or die "Cant Write $opt_f";
                foreach (keys %sysobjects_s)  {
                        print WRFILE "O|$_|".$sysobjects_s{$_}."\n";
                }
                foreach (keys %syscolumns_s)  {
                        print WRFILE "COL|$_|".$syscolumns_s{$_}."\n";
                }
                my($val);
                foreach (keys %syscomments_s) {
                        $val=$syscomments_s{$_};
                        $val=~s/\n/\?\?/g;
                        print WRFILE "COM|$_|".$val."\n";
                }
                foreach (keys %sysprotects_s) {
                        $val=$sysprotects_s{$_};
                        $val=~s/\n/\?\?/g;
                        print WRFILE "P|$_|".$val."\n";
                }
                foreach (keys %index_s)       {
                        $val=$index_s{$_};
                        $val=~s/\n/\?\?/g;
                        print WRFILE "I|$_|".$val."\n";
                }
                close WRFILE;
        }
} else {
        # read from FILE
        print "Reading File $opt_f\n";
        die "Cant read file $opt_f" unless -r $opt_f;
        open(RDFILE,"$opt_f") or die "Cant Read $opt_f";
        my $cnt=0;
        while (<RDFILE>) {
                my($typ,$realkey,$val)=split(/\|/,$_,3);
                $val=~s/\?\?/\n/g;
                if( $typ eq 'O' ) {
                        $sysobjects_s{$realkey}  = $val;
                } elsif( $typ eq 'COL' ) {
                        $syscolumns_s{$realkey}  = $val;
                } elsif( $typ eq 'COM' ) {
                        $syscomments_s{$realkey} = $val;
                } elsif( $typ eq 'P' ) {
                        $sysprotects_s{$realkey} = $val;
                } elsif( $typ eq 'I' ) {
                        $index_s{$realkey}       = $val;
                } else {
                        print "Ignoring Line $cnt of $opt_f - Invalid Key Type $NL";
                }
                $cnt++;
        }
        print "Read $cnt Items From DB\n";
        close RDFILE;
}

printf( "TIME=%-10d STARTING ANALYSIS $NL",time-$^T)
        if defined $opt_x;

# Get complete object list for the database
# - IGNORE ANY INTERNAL OBJECTS
my(@OBJECTS) = grep ( $_ !~ /\d\d\d\d\d\d$/, keys %sysobjects_S);
foreach (keys %sysobjects_s) {
        push @OBJECTS,$_ unless defined $sysobjects_S{$_} or $_ =~ /\d\d\d\d\d\d$/;
}

printf( "TIME=%-10d FOUND OBJECTS $NL",time-$^T)
        if defined $opt_x;

# ===================================
# Get differences in columns and mark
# ===================================
my($ncs,$ncS,$ncd)=(0,0,0);
foreach (keys %syscolumns_s) {
        next unless defined $syscolumns_S{$_};
        $syscolumns_s{$_} =~ s/\s//g;
        $syscolumns_S{$_} =~ s/\s//g;
        if( $syscolumns_s{$_} eq $syscolumns_S{$_} ) {
                $ncd++;
                delete $syscolumns_s{$_};
                delete $syscolumns_S{$_};
        }
}

my(%coldifs);
foreach (keys %syscolumns_s) {
        $ncs++;
        my( $tbl,$col ) = split;

        # FIND WHAT IS DIFFERENT - OUTPUT IS type ident null num
        if( ! defined $syscolumns_S{$_} ) {
                $coldifs{$tbl} .= ">>>".$col." Only in $label_s\n" unless defined $opt_h;
                $coldifs{$tbl} .= $NL."<b>$col</b>: Only in $label_s" if defined $opt_h;
        } else {
                my(@d_s)=split("~~",$syscolumns_s{$_});
                my(@d_S)=split("~~",$syscolumns_S{$_});
                my($str)="Column $col:";
                $str = "<b>$str</b>" if defined $opt_h;
                $str .= " Type ($label_s => $d_s[0] $label_S => $d_S[0])" if $d_s[0] ne $d_S[0];
                $str .= " Ident ($label_s => $d_s[1] $label_S => $d_S[1])" if $d_s[1] ne $d_S[1];
                $str .= " Null ($label_s => $d_s[2] $label_S => $d_S[2])" if $d_s[2] ne $d_S[2];
                $str .= " ColId ($label_s => $d_s[3] $label_S => $d_S[3])" if $d_s[3] ne $d_S[3];

                $coldifs{$tbl} .= ">>>".$str.$NL unless defined $opt_h;
                $coldifs{$tbl} .= $NL.$str         if  defined $opt_h;
        }
}

foreach (keys %syscolumns_S) {
        $ncS++;
        my( $tbl,$col ) = split;
        if( ! defined $syscolumns_s{$_} ) {
                $coldifs{$tbl} .= ">>>".$col." Only in $label_S\n" unless defined $opt_h;
                $coldifs{$tbl} .= $NL."<b>$col</b>: Only in $label_S" if defined $opt_h;
        }
}
printf( "TIME=%-10d COLUMNS %s DELETED %s IN %s, %s IN %s $NL",time-$^T,$ncd,$ncs,$label_s,$ncS,$label_S) if defined $opt_x;

# ===================================
# Get protection differences
# ===================================
my($nps,$npS,$npd)=(0,0,0);
foreach (keys %sysprotects_s) {
        if( defined $sysprotects_S{$_} ) {
                $npd++;
                delete $sysprotects_s{$_};
                delete $sysprotects_S{$_};
        }
}

my(%protdifs);
foreach (keys %sysprotects_s) {
        my($pr)=$_;
        $nps++;
        s/on\s+([\w\_]+)\s//;
        if( defined $opt_h ) {
                $protdifs{$1} .= $NL.$label_s.": ".$pr;
        } else {
                $protdifs{$1} .= "<<<"."$label_s: $pr\n";
        }
        print "PROTS $pr  => $1 $NL" if $1 eq $opt_o;
}

foreach (keys %sysprotects_S) {
        my($pr)=$_;
        $npS++;
        s/on\s+([\w\_]+)\s//;
        if( defined $opt_h ) {
                $protdifs{$1} .= $NL.$label_S.": ".$pr;
        } else {
                $protdifs{$1} .= "<<<"."$label_S: $pr\n";
        }
        print "PROTS $pr  => $1 $NL" if $1 eq $opt_o;
}

printf( "TIME=%-10d PROTECTIONS %s DELETED %s IN %s, %s IN %s $NL",time-$^T,$npd,$nps,$label_s,$ncS,$label_S) if defined $opt_x;

# ===================================
# GET INDEX DIFS
# ===================================
foreach (keys %index_s) {
        if( $index_s{$_} eq $index_S{$_} ) {
                delete $index_s{$_};
                delete $index_S{$_};
        }
}

my(%indxdifs);
foreach (keys %index_s) {
        my( $nm,$indnm ) = split /\./;
        if( !defined $indnm) { "$label_s INDEX NAME IS $_\n"; next; }
        my( @ind ) = split("~~", $index_s{$_} );
        my($defn) = $ind[5];
        $defn=$ind[4] if $defn =~ /^\s*Y*\s*$/;
        $defn=$ind[6] if $defn =~ /^\s*Y*\s*$/;

        if( defined $opt_h ) {
                $indxdifs{$nm} .= $NL."$label_s: Index=$indnm ";
                $indxdifs{$nm} .= "Unique " if $ind[1] =~ /Y/;
                $indxdifs{$nm} .= "Clustered " if $ind[0] =~ /Y/;
                $indxdifs{$nm} .= $defn;
        } else {
                $indxdifs{$nm} .= "<<< Index Name=$indnm Clustered=$ind[0] Unique=$ind[1] Defn=$defn \n";
        }
}

foreach (keys %index_S) {
        my( $nm,$indnm ) = split /\./;
        if( !defined $indnm) { "$label_S INDEX NAME IS $_\n"; next; }
        my( @ind ) = split("~~", $index_S{$_} );
        my($defn) = $ind[5];
        $defn=$ind[4] if $defn =~ /^\s*Y*\s*$/;
        $defn=$ind[6] if $defn =~ /^\s*Y*\s*$/;
        if( defined $opt_h ) {
                $indxdifs{$nm} .= $NL."$label_S: Index=$indnm ";
                $indxdifs{$nm} .= "Unique " if $ind[1] =~ /Y/;
                $indxdifs{$nm} .= "Clustered " if $ind[0] =~ /Y/;
                $indxdifs{$nm} .= $defn;
        } else {
                $indxdifs{$nm} .= "<<< Index Name=$indnm Clustered=$ind[0] Unique=$ind[1] Defn=$defn\n";
        }
}

# DEBUG SECTION
#if( defined $opt_x and defined $opt_h ) {
#if( 1 ) {
#
#       print "DEBUG: PRINTING INDEXES IN $opt_S not in $opt_s</h1>";
#       foreach (sort keys %index_S) {
#               next if defined $index_s{$_};
#               print "DBG>>> key ",$_," value ",$index_S{$_},$NL;
#       }
#
#       print "<h1>DEBUG: PRINTING INDEXES IN $opt_s not in $opt_S</h1>";
#       foreach (sort keys %index_s) {
#               next if defined $index_S{$_};
#               print "DBG>>> key ",$_," value ",$index_s{$_},$NL;
#       }
#
#       print "<h1>DEBUG: DIFFERENT INDEXES</h1>";
#       foreach (sort keys %index_s) {
#               next unless defined $index_S{$_};
#               print "DBG>>> key ",$_," value ",$index_s{$_},$NL;
#               print "DBG<<< key ",$_," value ",$index_S{$_},$NL;
#
#       }
#
#       print "<h1>DEBUG: PRINTING INDEXDIFS</h1>";
#       foreach (keys %indxdifs ) {
#               print "<Font Color=RED>",$indxdifs{$_},"</FONT>";
#       }
#        DONE DEBUG
#}

printf( "TIME=%-10d INDEXES $NL",time-$^T) if defined $opt_x;

# output format
#    name  srv1_type srv2_type data_1 data_2
#   where data1 is proc size or num cols diff
my($num_diffs)=0;
foreach my $objnm (sort @OBJECTS) {

        printf( "TIME=%-10d OBJECT %s $NL",time-$^T,$objnm) if defined $opt_x;
        print "WORKING ON $objnm\n" if $opt_o eq $objnm;

        if( ! defined $sysobjects_S{$objnm} ) {

                $num_diffs++;
                if( defined $opt_h ) {
                        print errorrow(
                                $objnm,
                                $label_s,
                                "",
                                get_type($objnm,'s')." ONLY IN $label_s"
                        );
                } else {
                        printf("%30s %6.6s        ONLY IN $label_s\n",
                                $objnm,
                                get_type($objnm,'s'));
                }

        } elsif( ! defined $sysobjects_s{$objnm} ) {

                $num_diffs++;
                if( defined $opt_h ) {
                        print errorrow(
                                $objnm,
                                "",
                                $label_S,
                                get_type($objnm,'S')." ONLY IN $label_S"
                        );
                } else {
                        printf("%30s %6.6s        ONLY IN $label_S\n",
                                $objnm,
                                get_type($objnm,'S'));
                }

        } elsif( substr($sysobjects_S{$objnm},0,2) ne substr($sysobjects_S{$objnm},0,2) ) {

                $num_diffs++;
                if( defined $opt_h ) {
                        print errorrow(
                                $objnm,
                                get_type($objnm,'s'),
                                get_type($objnm,'S'),
                                "OBJECT TYPES DIFFERENT"
                        );
                } else {
                        printf("%30s %6.6s %6.6s OBJECT TYPES DIFFERENT\n",
                                $objnm,
                                get_type($objnm,'S'),
                                get_type($objnm,'s'));
                }

        } else {                                                                        # ok same type in both servers

                my($desc1)="";
                print "OBJECT $opt_o IN BOTH SERVERS ... TESTING\n" if $objnm eq $opt_o;

                my $ddl_no_space_S = $syscomments_S{$objnm};
                my $ddl_no_space_s = $syscomments_s{$objnm};

                $ddl_no_space_S =~ s/[\x0\a\e\s]//g;
                $ddl_no_space_s =~ s/[\x0\a\e\s]//g;

                my @obinfo_s = split("~~",$sysobjects_s{$objnm});
                my @obinfo_S = split("~~",$sysobjects_S{$objnm});

                my(@words_S, @words_s);

                # Trigger different

                if( $obinfo_S[1] ne $obinfo_s[1]
                or  $obinfo_S[2] ne $obinfo_s[2]
                or  $obinfo_S[3] ne $obinfo_s[3] ) {
                        $num_diffs++;
                        $desc1.="TRIGGERS ";
                }

                # DDL different
                if( $ddl_no_space_S ne $ddl_no_space_s ) {
                my $lc_ddl_S= lc $ddl_no_space_S;
                my $lc_ddl_s= lc $ddl_no_space_s;
                if( $lc_ddl_S ne $lc_ddl_s ) {

                        # Remove Comments
                        $syscomments_s{$objnm} =~ s/(\/\*[\w\W]+?\*\/)|--(.*?)\n//g;
                        $syscomments_S{$objnm} =~ s/(\/\*[\w\W]+?\*\/)|--(.*?)\n//g;

                        #  OK get comments and pare em down
                        @words_S = split /\s+/,$syscomments_S{$objnm} ;
                        @words_s = split /\s+/,$syscomments_s{$objnm} ;

                        #
                        # THE COMPARISON ROUTINE!
                        #
                        # Remove Stuff At Start and End
                        while ( $words_s[0] eq $words_S[0]
                                or $words_s[0]=~/^\s*$/
                                or $words_S[0]=~/^\s*$/ ) {

                                last if $#words_s<0 or $#words_S<0;
                                if($words_s[0] eq $words_S[0] ) {
                                        shift @words_s;
                                        shift @words_S;
                                } else {
                                        shift @words_s if $words_s[0]=~/^\s*$/;
                                        shift @words_S if $words_S[0]=~/^\s*$/;
                                }

                        }

                        while ( $words_s[$#words_s] eq $words_S[$#words_S]
                                or $words_s[0]=~/^\s*$/
                                or $words_S[0]=~/^\s*$/ ) {

                                last if $#words_s<0 or $#words_S<0;
                                if($words_s[$#words_s] eq $words_S[$#words_S] ) {
                                        pop @words_s;
                                        pop @words_S;
                                } else {
                                        pop @words_s if $words_s[$#words_s]=~/^\s*$/;
                                        pop @words_S if $words_S[$#words_S]=~/^\s*$/;
                                }

                        }

                        if( $#words_s>=0 or $#words_S>=0 ) {
                                $num_diffs++;
                                $desc1.="& " if $desc1 ne "";
                                $desc1.="DDL ";
                                print "DDL FOR $opt_o\n\t$syscomments_S{$objnm}\n\t$syscomments_S{$objnm}\n" if $objnm eq $opt_o;
                        }
                }
                }

                if( defined $indxdifs{$objnm} ) {
                        $num_diffs++;
                        $desc1.="& " if $desc1 ne "";
                        $desc1.="INDEX ";
                        print "INDEX FOR ",$opt_o,$NL,$indxdifs{$objnm},"\n"
                                if $objnm eq $opt_o;
                }

                # Prot  different
                if( defined $protdifs{$objnm} ) {
                        $num_diffs++;
                        $desc1.="& " if $desc1 ne "";
                        $desc1.="PROTECTIONS ";
                        print "PROTS FOR ",$opt_o,$NL,$protdifs{$objnm},"\n"
                                if $objnm eq $opt_o;
                }

                # Cols  different
                if( defined $coldifs{$objnm} ) {
                        $num_diffs++;
                        $desc1.="& " if $desc1 ne "";
                        $desc1.="COLUMNS ";
                        print "COLUMNS FOR $opt_o\n\t$coldifs{$objnm}\n" if $objnm eq $opt_o;
                }

                $desc1.= "DIFFERENT" unless $desc1 eq "";

                if( defined $opt_h ) {

                        if( $desc1 eq "" ) {
                                print row(
                                        $objnm,
                                        get_type($objnm,'s'),
                                        get_type($objnm,'S'),
                                        "IDENTICAL"
                                ) unless defined $opt_i;
                        } else {

                                $desc1 .= $protdifs{$objnm} if defined $protdifs{$objnm};
                                $desc1 .= $coldifs{$objnm}  if defined $coldifs{$objnm};
                                $desc1 .= $indxdifs{$objnm}  if defined $indxdifs{$objnm};

                                print errorrow(
                                        $objnm,
                                        "",
                                        get_type($objnm,'S'),
                                        $desc1
                                );

                                print errorrow("","",$label_s, "<FONT SIZE=1 COLOR=BLACK>Date ".$obinfo_s[4]."</FONT>: ".join(" ",@words_s)) if $#words_s>=0 and ! defined $opt_t;
                                print errorrow("","",$label_S, "<FONT SIZE=1 COLOR=BLACK>Date ".$obinfo_S[4]."</FONT>: ".join(" ",@words_S)) if $#words_S>=0 and ! defined $opt_t;

                        }

                } else {
                        next if $desc1 eq "" and defined $opt_i;
                        $desc1 = "IDENTICAL"      if $desc1 eq "";

                        printf("%30s %6.6s %6.6s %s\n",
                                $objnm,
                                get_type($objnm,'S'),
                                get_type($objnm,'s'),
                                $desc1);

                        print $protdifs{$objnm} if defined $protdifs{$objnm};
                        print $indxdifs{$objnm}  if defined $indxdifs{$objnm};

                        if( $ddl_no_space_S ne $ddl_no_space_s ) {

                                print "$objnm : ",$label_s,": ",join(" ",@words_s),"\n" if $#words_s <15 and ! defined $opt_t;
                                print "$objnm : ",$label_S,": ",join(" ",@words_S),"\n" if $#words_S <15 and ! defined $opt_t;

                        }
                }
        }
        printf( "TIME=%-10d DONE OBJECT %s $NL",time-$^T,$objnm) if defined $opt_x;
        die "DONE WITH $objnm\n" if $opt_o eq $objnm;
}
print "</TABLE>" if defined $opt_h;
print "\nNUMBER OF DIFFERENCES FOUND=$num_diffs\n$NL";


printf( "TIME=%-10d ANALYSIS COMPLETED SUCCESSFULLY $NL",time-$^T)
        if defined $opt_x;

sub get_syscomments
{
        my($db)= @_;
        my %syscomments;
        my $sql="select o.name,text
                        from dbo.syscomments c, dbo.sysobjects o where o.id=c.id and o.uid=1";
        $sql .= " and type=\"U\"" if defined $opt_a;
        foreach (dbi_query(-db=>$opt_D,-query=>$sql)) {
                my( $object, $text) = dbi_decode_row($_);
                next if $object =~ /\d\d\d\d\d\d$/;
                $syscomments{$object}.=$text;
        }
        return %syscomments;
}

sub get_type
{
        my($obj,$srv)=@_;
   if( $srv eq "S" ) {
                $obj_desc{substr($sysobjects_S{$obj},0,2)};
        } else {
                $obj_desc{substr($sysobjects_s{$obj},0,2)};
        }
}

sub rowhead
{
        # if no data, you probably want to print a row, so just put an empty
        # line onto the row
        return "<TR><TH>".
                        "<FONT COLOR=BLACK>".
                        $_[0].
                        "</FONT>".
                        "</TH><TH>".
                        "<FONT COLOR=BLUE>".
                        $_[1].
                        "</FONT>".
                        "</TH><TH>".
                        "<FONT COLOR=GREEN>".
                        $_[2].
                        "</FONT>".
                        "</TH><TH>".
                        "<FONT COLOR=BLACK>".
                        $_[3].
                        "</FONT>".
                        "</TH></TR>\n";
}

sub row
{
        return "<TR><TD VALIGN=TOP>".
                        "<FONT COLOR=BLACK>".
                        $_[0].
                        " </FONT>".
                        "</TD><TD VALIGN=TOP>".
                        " <FONT COLOR=BLUE>".
                        $_[1].
                        " </FONT>".
                        "</TD><TD VALIGN=TOP>".
                        "<FONT COLOR=GREEN>".
                        $_[2].
                        " </FONT>".
                        "</TD><TD VALIGN=TOP>".
                        "<FONT COLOR=BLACK>".
                        $_[3].
                        " </FONT>".
                        "</TD></TR>\n";
}

sub errorrow
{
        my $cellcolor="BLACK";
        my @cells = @_;

        if($cells[2] eq "") {
                $cells[2] = $cells[1];
                $cells[1] = "";
        };

        $cellcolor="BLUE" if $cells[2] eq $label_s;
        $cellcolor="GREEN" if $cells[2] eq $label_S;

        my $str= "<TR><TD BGCOLOR=PINK VALIGN=TOP>".
                        "<FONT COLOR=BLACK>".
                        $cells[0].
                        "&nbsp;</FONT>".
                        "</TD>";
        $str .= "<TD BGCOLOR=PINK VALIGN=TOP>".
                        "<FONT COLOR=BLUE>".
                        $cells[1].
                        "&nbsp;</FONT>".
                        "</TD><TD BGCOLOR=PINK VALIGN=TOP>".
                        "<FONT COLOR=GREEN>".
                        $cells[2].
                        "&nbsp;</FONT>".
                        "</TD>" if $cells[1] ne "";
        $str .= "<TD COLSPAN=2 ALIGN=RIGHT BGCOLOR=PINK VALIGN=TOP>".
                        "<FONT COLOR=$cellcolor>".
                        $cells[2].
                        "&nbsp;</FONT>".
                        "</TD>" if $cells[1] eq "";
        $str .= "<TD BGCOLOR=PINK VALIGN=TOP>".
                        "<FONT COLOR=$cellcolor>".
                        $cells[3].
                        "&nbsp;</FONT>".
                        "</TD></TR>\n";
        return $str;
}

__END__

=head1 NAME

dbcompare.pl - database comparison utility

=head2 DESCRIPTION

Compares two databases.  Checks the following things:

=over 4

=item * object existence

=item * trigger names on tables

=item * Column definitions on table

=item * Object text if the object not a table

=back

=head2 USAGE

dbcompare.pl [-xh] -FFile -Ssrv -Uusr -Ppass -Ddb -ffile -ssrv -uusr -ppass -ddb

	Compare 2 Databases DDL.  Can be from File (use -F) or DB (-SUPD)
	If both -SUPD and -F are supplied, file is written.
	If you ignore one server (ie the lower case side), the values are copied from the other.  This saves typing

	-g no group permissions
	-o object name
	-h htmlize output
	-i ignore identical output
	-x debug mode

=head2 EXAMPLE

I want to compare 2 databases (db1 and db1_bak) on one server:

perl dbcompare.pl -Usa -SSVR1 -Psrv1pass -Ddb1 -ddb1_bak -i

=head2 NOTES

If a server is passed, it will connect to that server and use server data.
If a file name is also passed, that data is written to the file.  If only a file
name is passed, the file is read and used as the data source.  This allows
you to compare stuff at remote sites to what is supposed to be in the database.

Works only on DBO Objects.  Might have some bugs with non dbo object data
(but i dont think so).

=cut
