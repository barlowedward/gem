#
# MlpAlarm - Master Alarm Router Partners
#
# copyright (c) 2005-2008 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

package  MlpAlarm;

# For documentation on this file - type perldoc MlpAlarm.pm
#
# This file best viewed at tabstop=3
#
use   vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use   strict;

# the following two must be set for diag messages
my($DEVELOPER_IPADDR)="10.88.50.131";
my($DEVELOPER_IPADDR2)="10.5.131.9";

my($is_development);

my($DBGDBG);
my(%mlpbatchjob_args);

# INSTALLATION INSTRUCTIONS
#    - edit the setup variables section below for the name and login/password for your database
#    - modify the BEGIN Block  to point to your sybase
#
# The next line contains the public login for the repository - this is a public account and it
# only has permissions on the appropriate tables as per the setup instructions for the database.
# Database installation notes are distributed in the ADMIN_SCRIPTS/gemalarms directory.
#
my($server,$login,$password,$db)=("CTSYBMON","gemalarms","gemalarms","gemalarms");
my($serverP,$loginP,$passwordP,$dbP)=("SYBPERF","gemalarms","gemalarms","gemalarms");

# THE DONT_USE_ALARMS VARIABLE SHOULD BE SET TO 1 IF YOU DO NOT WISH TO USE THE ALARM SYSTEM
my($DONT_USE_ALARMS);

#
# you should also add any necessary environment variables in the begin block.
#
BEGIN {
	if( -d "/export/home/sybase" and ! defined $ENV{SYBASE} ) {
		$ENV{SYBASE}="/export/home/sybase";
	} elsif( -d "/apps/sybase" and ! defined $ENV{SYBASE} ) {
		$ENV{SYBASE}="/apps/sybase";
	}
}
#################### END SETUP VARIABLES ################################

my(%mlp_batch_args,$job_started,$job_curstep,$job_curstepname);

use   Carp qw(confess croak);
use  	Do_Time;
use	CommonFunc;
use 	DBIFunc;
use 	File::Basename;
use 	Sys::Hostname;
my($this_hostname);

require  Exporter;
@ISA     = qw(Exporter);
@EXPORT  = qw(MlpEvent MlpTestInstall MlpHeartbeat MlpGetCategory MlpReportBackupJobRunInfo
	MlpGetScreens MlpRunScreen MlpGetContainer MlpGetDbh MlpGetReportCode MlpGetProgram MlpGetSeverity
	MlpManageData MlpGetOperator MlpEventUpdate MlpCleanup MlpSaveBackupJobRunInfo MlpGetAlarms MlpClearAlarms MlpBatchStep
	MlpSetBackupState MlpGetBackupState MlpSetPL MlpClearPL MlpSetVars MlpGetVars MlpSyncConfigFiles MlpLoadRepository
	MlpMonitorStart MlpBatchStart MlpAgentStart MlpBatchRunning MlpBatchDone MlpAgentDone MlpBatchErr MlpBatchJobStep
	MlpReportArgs MlpSaveNotification MlpOldBackupState _querynoresults _querywithresults MlpBatchJobErr
	MlpGetLookupInfo MlpIsPageable MlpLogOperationStart MlpLogOperationEnd MlpSetAuditResult MlpClearAuditResult
	MlpGetBusiness_System MlpGetPrincipal MlpGetSystem MlpDeleteSystem MlpDeleteSystemAttributes MlpUpdateSystem
	MlpGetPermission MlpBatchJobStart MlpBatchJobEnd GetLogonUser
	MlpCertifyUserStatus MlpCertifyDepartment set_developer MlpGetDepartment MlpLongRunningQuery
	);

$VERSION = '1.0';

#############################
# GLOBAL VARIABLES          #
#############################
my($i_am_initialized)="FALSE";
my($quiet);
my($monitor_program,$system,$debug);
my($server_type);
my(%condense_args);

##############################################################################
# METHOD ARGUMENTS                                                           #
# Set to 1 if Required, optional if 0.  key is func:value	                 #
# all method arguments must be in this array which is used for validation    #
##############################################################################

my(%OK_OPTS)= (
	"MlpEvent:-debug"			=> 0,
	"MlpEvent:-monitor_program"		=> 1,
	"MlpEvent:-system"			=> 1,
	"MlpEvent:-subsystem"			=> 0,
	"MlpEvent:-event_time"			=> 0,
	"MlpEvent:-severity"			=> 1,
	"MlpEvent:-event_id"			=> 0,
	"MlpEvent:-message_text"	  	=> 1,
	"MlpEvent:-message_value"		=> 0,
	"MlpEvent:-document_url"		=> 0,
	"MlpEvent:-condense"		=> 0,
	"MlpEvent:-nosave"		=> 0,
	"MlpEvent:-quiet"		=> 0,

"MlpLongRunningQuery:-server_name"	=> 1,
"MlpLongRunningQuery:-server_type"	=> 1,
"MlpLongRunningQuery:-query_start_time"	=> 1,
"MlpLongRunningQuery:-spid"	=> 1,
"MlpLongRunningQuery:-database_name"	=> 1,
"MlpLongRunningQuery:-login"	=> 1,
"MlpLongRunningQuery:-query"	=> 1,
"MlpLongRunningQuery:-io_used"	=> 1,
"MlpLongRunningQuery:-cpu_used"	=> 1,
"MlpLongRunningQuery:-client_hostname"	=> 1,
"MlpLongRunningQuery:-application_name"	=> 1,

	"MlpGetPermission:-samid"      => 1,
	"MlpGetPermission:-canaccess"  => 0,

	"MlpClearAuditResult:-system_name"      => 1,
   "MlpClearAuditResult:-system_type"      => 1,
   "MlpClearAuditResult:-monitor_program"  => 1,

	"MlpSetAuditResult:-system_name"      => 1,
   "MlpSetAuditResult:-system_type"      => 1,
   "MlpSetAuditResult:-monitor_program"  => 1,
   "MlpSetAuditResult:-database_name"    => 0,
   "MlpSetAuditResult:-error_level"      => 0,
   "MlpSetAuditResult:-message_id"       => 0,
   "MlpSetAuditResult:-message_text"     => 0,
   "MlpSetAuditResult:-mod_date"         => 0,
   "MlpSetAuditResult:-mod_by"           => 1,

	"MlpCertifyUserStatus:-credential"   => 1,
	"MlpCertifyUserStatus:-username"     => 1,
	"MlpCertifyUserStatus:-system"       => 1,
	"MlpCertifyUserStatus:-type"         => 1,
	"MlpCertifyUserStatus:-notes"        => 1,
	"MlpCertifyUserStatus:-status"       => 1,
	"MlpCertifyUserStatus:-yourname"     => 1,

	"MlpCertifyDepartment:-displayname"  => 1,
	"MlpCertifyDepartment:-ldap_group"   => 1,
	"MlpCertifyDepartment:-notes"        => 1,
	"MlpCertifyDepartment:-status"       => 1,

	#"MlpCertifyNow:-login"        => 1,
	#"MlpCertifyNow:-system"       => 1,
	#"MlpCertifyNow:-type"         => 1,

	"MlpBatchJobStart:-BATCH_ID"      => 1,
  	"MlpBatchJobStart:-AGENT_TYPE"     => 0,
  	"MlpBatchJobStart:-SUBKEY"  => 0,

  	"MlpBatchJobStep:-step" =>0,
  	"MlpBatchJobStep:-step_subkey" =>0,

	"MlpBatchJobEnd:-EXIT_CODE"      => 0,
	"MlpBatchJobEnd:-EXIT_NOTES"    	=> 0 ,

	"MlpEventUpdate:-debug"			=> 0,
	"MlpEventUpdate:-monitor_program"		=> 1,
	"MlpEventUpdate:-system"			=> 1,
	"MlpEventUpdate:-subsystem"			=> 0,
	"MlpEventUpdate:-event_time"			=> 1,
	"MlpEventUpdate:-severity"			=> 0,
	"MlpEventUpdate:-event_id"			=> 0,
	"MlpEventUpdate:-message_text"	  	=> 1,
	"MlpEventUpdate:-message_value"		=> 0,
	"MlpEventUpdate:-document_url"		=> 0,
	"MlpEventUpdate:-quiet"			=> 0,

	"MlpHeartbeat:-debug"			=> 0,
	"MlpHeartbeat:-monitor_program"		=> 1,
	"MlpHeartbeat:-heartbeat"		=> 0,
	"MlpHeartbeat:-system"			=> 1,
	"MlpHeartbeat:-subsystem"		=> 0,
	"MlpHeartbeat:-state"			=> 1,
	"MlpHeartbeat:-message_text"		=> 0,
	"MlpHeartbeat:-document_url"		=> 0,
	"MlpHeartbeat:-batchjob"		=> 0,
	"MlpHeartbeat:-event_time"		=> 0,
	"MlpHeartbeat:-quiet"			=> 0,
	"MlpHeartbeat:-reset_status"		=> 0,

	"MlpGetAlarms:-test"			=> 0,
	"MlpGetCategory:-user"			=> 0,
	"MlpGetCategory:-debug"			=> 0,

	"MlpGetBackupState:-system"		=> 0,
	"MlpGetBackupState:-production"	=> 0,
	"MlpGetBackupState:-orderby"		=> 0,
	"MlpGetBackupState:-debug"			=> 0,

	"MlpSetBackupState:-system"		=> 1,
	"MlpSetBackupState:-dbname"		=> 1,
	"MlpSetBackupState:-debug"		=> 0,
	"MlpSetBackupState:-last_fulldump_time"		=> 0,
  "MlpSetBackupState:-last_fulldump_file"		=> 0,
  "MlpSetBackupState:-last_fulldump_lsn"		=> 0,
  "MlpSetBackupState:-last_trandump_time"		=> 0,
  "MlpSetBackupState:-last_trandump_file"		=> 0,
  "MlpSetBackupState:-last_trandump_lsn"		=> 0,
  "MlpSetBackupState:-last_truncation_time"	=> 0,
  "MlpSetBackupState:-last_fullload_time"		=> 0,
  "MlpSetBackupState:-last_fullload_file"		=> 0,
  "MlpSetBackupState:-delete"		=> 0,
  "MlpSetBackupState:-last_fullload_lsn"		=> 0,
  "MlpSetBackupState:-last_tranload_time"		=> 0,
  "MlpSetBackupState:-last_tranload_file"		=> 0,
  "MlpSetBackupState:-last_tranload_lsn"		=> 0,
  "MlpSetBackupState:-last_tranload_filetime"	=> 0,
  "MlpSetBackupState:-is_tran_truncated"	=> 0,
  "MlpSetBackupState:-is_db_usable"	=> 0,
  "MlpSetBackupState:-db_server_name"	=> 0,
  "MlpSetBackupState:-file_time"	=> 0,
  "MlpSetBackupState:-is_readonly"	=> 0,
  "MlpSetBackupState:-is_select_into"	=> 0,
  "MlpSetBackupState:-is_singleuser"	=> 0,
  "MlpSetBackupState:-is_backupable"	=> 0,

   "MlpGetScreens:-user"			=> 0,
	"MlpGetScreens:-category"		=> 0,
	"MlpGetScreens:-debug"			=> 0,

	"MlpReportArgs:-reportname"	=> 1,
	"MlpReportArgs:-debug"			=> 0,

	"MlpRunScreen:-screen_name"		=> 1,
	"MlpRunScreen:-useheartbeattime"	=> 0,
	"MlpRunScreen:-container"		=> 0,
	"MlpRunScreen:-production"		=> 0,
	"MlpRunScreen:-severity"		=> 0,
	"MlpRunScreen:-system"			=> 0,
	"MlpRunScreen:-likefilter"		=> 0,
	"MlpRunScreen:-filter_num_days"		=> 0,
	"MlpRunScreen:-credentialfilter"		=> 0,
	"MlpRunScreen:-showdeleted"		=> 0,
	"MlpRunScreen:-subsystem"		=> 0,
	"MlpRunScreen:-program"			=> 0,
	"MlpRunScreen:-showok"			=> 0,
	#"MlpRunScreen:-SHOWMAPPED"			=> 0,
	"MlpRunScreen:-html"					=> 0,
	"MlpRunScreen:-systype"				=> 0,
	"MlpRunScreen:-maxtimehours"		=> 0,
	"MlpRunScreen:-LOGON_USER"			=> 0,
	"MlpRunScreen:-poweruser"			=> 0,
	"MlpRunScreen:-OTHER_ARG"			=> 0,
	"MlpRunScreen:-OTHER_ARG2"			=> 0,
	"MlpRunScreen:-matchstatus"			=> 0,

	"MlpRunScreen:-ldap_user"		=> 0,
	"MlpRunScreen:-ldap_group"		=> 0,
	"MlpRunScreen:-debug"				=> 0,
	"MlpRunScreen:-NL"					=> 0,
	"MlpRunScreen:-userpermission"	=> 0,

	"MlpGetContainer:-name"			=> 0,
	"MlpGetContainer:-type"			=> 0,
	"MlpGetContainer:-user"			=> 0,
	"MlpGetContainer:-debug"		=> 0,
	"MlpGetContainer:-screen_name"		=> 0,

	"MlpGetReportCode:-screen_name"		=> 1,
	"MlpGetReportCode:-user"		=> 0,

	"MlpCleanup:-days"			=> 0,
	"MlpCleanup:-debug"			=> 0,
	"MlpCleanup:-hbdays"			=> 0,
	"MlpCleanup:-evdays"			=> 0,
	"MlpCleanup:-cleanservers"	=> 0,

 	"MlpSaveNotification:-monitor_program"	=> 1,
	"MlpSaveNotification:-system"		=> 1,
	"MlpSaveNotification:-subsystem"	=> 0,
	"MlpSaveNotification:-severity"		=> 1,
	"MlpSaveNotification:-monitor_time"	=> 0,
	"MlpSaveNotification:-user_name" 	=> 1,
	"MlpSaveNotification:-notify_type" 	=> 1,
	"MlpSaveNotification:-header" 		=> 1,
	"MlpSaveNotification:-message" 		=> 1,

	"MlpMonitorStart:-monitor_program"=>	1,
	"MlpMonitorStart:-system"			=>	1,

	"MlpBatchStart:-monitor_program"=>	0,
	"MlpBatchStart:-name"			=>	0,
	"MlpBatchStart:-debug"			=>	0,
	"MlpBatchStart:-system"			=>	0,
	"MlpBatchStart:-subsystem"		=>	0,
	"MlpBatchStart:-message"		=>	0,
	"MlpBatchStart:-batchjob"		=>	0,
	"MlpBatchStart:-monitorjob"	=>	0,
	"MlpBatchStart:-reset_status"	=>	0,
	"MlpBatchStart:-stepnum"		=>	0,

	"MlpAgentStart:-monitor_program"	=>	1,
	"MlpAgentStart:-debug"			=>	0,
	"MlpAgentStart:-system"			=>	0,
	"MlpAgentStart:-subsystem"		=>	0,
	"MlpAgentStart:-message"		=>	0,

	"MlpBatchStep:-stepname"=>	1,
	"MlpBatchStep:-stepnum"=>	1,
	"MlpBatchStep:-message"=>	0,

	"MlpBatchRunning:-monitor_program"	=>	1,
	"MlpBatchRunning:-name"				=>	0,
	"MlpBatchRunning:-debug"			=>	0,
	"MlpBatchRunning:-system"				=>	1,
	"MlpBatchRunning:-subsystem"		=>	0,
	"MlpBatchRunning:-message"			=>	0,
	"MlpBatchRunning:-batchjob"			=>	0,
	"MlpBatchRunning:-stepnum"=>	0,
	"MlpBatchRunning:-monitorjob"=>	0,

	"MlpBatchDone:-monitor_program"		=>	1,
	"MlpBatchDone:-name"			=>	0,
	"MlpBatchDone:-batchjob"		=>	0,
	"MlpBatchDone:-state"			=>	0,
	"MlpBatchDone:-system"			=>	1,
	"MlpBatchDone:-debug"			=>	0,
	"MlpBatchDone:-subsystem"		=>	0,
	"MlpBatchDone:-message"			=>	0,
	"MlpBatchDone:-stepnum"			=>	0,
	"MlpBatchDone:-monitorjob"			=>	0,

	"MlpBatchErr:-monitor_program"=>	1,
	"MlpBatchErr:-name"				=>	0,
	"MlpBatchErr:-state"				=>	0,
	"MlpBatchErr:-system"			=>	1,
	"MlpBatchErr:-batchjob"			=>	0,
	"MlpBatchErr:-subsystem"		=>	0,
	"MlpBatchErr:-message"			=>	0,
	"MlpBatchErr:-stepnum"			=>	0,
	"MlpBatchErr:-monitorjob"			=>	0,
	"MlpBatchErr:-quiet"				=>	0,	
	
	"MlpDeleteSystemAttributes:-system_name" => 1,      
	"MlpDeleteSystemAttributes:-system_type" => 1,        
	"MlpDeleteSystemAttributes:-attribute_category" => 0,
	"MlpDeleteSystemAttributes:-debug" => 0,
	"MlpDeleteSystemAttributes:-noexec" => 0,

	"MlpUpdateSystem:-system_name" => 1,      
	"MlpUpdateSystem:-system_type" => 1,        
	"MlpUpdateSystem:-attribute_category" => 1,
	"MlpUpdateSystem:-attributes" => 1,
	"MlpUpdateSystem:-debug" => 0,
	"MlpUpdateSystem:-noexec" => 0,

	"MlpDeleteSystem:-system_name" => 1,      
	"MlpDeleteSystem:-system_type" => 1,        
	"MlpDeleteSystem:-debug" => 0,
	"MlpDeleteSystem:-noexec" => 0,

	"MlpGetSystem:-system_name" => 0,      
	"MlpGetSystem:-system_type" => 0,        
	"MlpGetSystem:-type" => 0,        
	"MlpGetSystem:-where" => 0,        
	"MlpGetSystem:-getattributes" => 0,        
	"MlpGetSystem:-where" => 0,   	
	"MlpGetSystem:-attribute_category" => 0,
	"MlpGetSystem:-debug" => 0,
	"MlpGetSystem:-noexec" => 0,
);

my($connection,$connectionP);

sub MlpSetVars {
	my(%args)=@_;
	$server		= $args{-server};
	$login		= $args{-login};
	$password	= $args{-password};
	$db			= $args{-db};
}
sub MlpGetVars {	return( $server, $login, $password , $db ); }

sub initializeP {
	my(%args)=@_;
	initialize(%args,-Performance=>1);
}

sub initialize {
	my(%args)=@_;

	if( $DONT_USE_ALARMS ) {
		if( ! $quiet ) {
			print "-------------------------------------------------\n";
			print "| PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED |\n";
			print "-------------------------------------------------\n";
		}
		$i_am_initialized = "FAILED";
		$quiet=1;
		return 0;
	}
	print "Initializing Repository Connection\n" if $args{-debug};

	if( $args{-debug} ) {
		print "DBG DBG: INIT( ";
		foreach( keys %args ) {
			print " ".$_."=>".$args{$_}." " unless $_ eq "-NL";
		}
		print ")\n",$args{-NL};
	}

	if( $server eq "" ) {
		# i dont know if this will work... but it might
		if( defined $main::CONFIG{ALARM_SVR} ) {
			print "Yes!!! i think this fixes the bug in configure timing!!!\n";
			print "the following variables were found:\n";
			print "SERVER\t$main::CONFIG{ALARM_SVR}\n";
			print "ALARM_DATABASE\t$main::CONFIG{ALARM_DATABASE}\n";
			print "ALARM_LOGIN\t$main::CONFIG{ALARM_LOGIN}\n";
			print "ALARM_PASSWORD\t$main::CONFIG{ALARM_PASSWORD}\n";
			print "ALARM_MAIL\t$main::CONFIG{ALARM_MAIL}\n";
			print "Contact Ed Barlow asap and tell him the good news!\n";
			confess("TRACE");
		}

		if( ! defined $quiet ) {
			print "******************************************************\n";
			print "* Monitoring Error: GEM MONITORING NOT INITIALIZED!!! \n";
			print "* Please 'Save Configuration Changes' in the configuration utility.\n";
			print "* This may indicate that you have not run upgrade.pl after downloading code\n";
			print "* This is a fatal error - aborting.\n";
			print "* If this is a legitimate error please contact GEM Support\n";
			print "* FILE=",__FILE__,"\n";
			print "******************************************************\n";
			print STDERR "Monitoring Error: GEM IS NOT INITIALIZED!!! \nIf You Are Running From The Configuration Utility, you need to save changes.\nIm sorry to exit here but GEM needs to be initialized.\nIf this is a legitimate error please contact GEM Support\n";
			print "******************************************************\n";
			confess("TRACE");
			exit(1);
		}
		$i_am_initialized = "FAILED";
		return 0;
	}

	if( defined $args{-monitor_program} ) {
		$monitor_program=$args{-monitor_program};
	} else {
		$monitor_program=basename($0);
	}

	$quiet=$args{-quiet} unless $quiet;

	$this_hostname=hostname();

	if( defined $args{-system} ) {
		$system=$args{-system};
	} else {
		$system=hostname();
	}

	if( is_nt() ) {
		$server_type="ODBC";
	} else {
		$server_type="Sybase";
	}

	# $subsystem=$args{-subsystem} if defined $args{-subsystem};
	$debug=$args{-debug} if $args{-debug};
	print "dbi_connect(-srv=>$server, -login=>$login, -password=>$password,
			-type=>$server_type, -debug=>$args{-debug},
	  		-nosybaseenvcheck =>1,	-connection=>1, 	-die_on_error=>0)\n",$args{-NL}	if $args{-debug};
	
	my($rc) = dbi_connect(-srv=>$server, -login=>$login, -password=>$password,   -nosybaseenvcheck =>1,
		-type=>$server_type, -debug=>$args{-debug}, -connection=>1, -die_on_error=>0);
	if( ! $rc ) {
		print "dbi_connect() Failed\n",$args{-NL} if $args{-debug};
		if( ! defined $quiet ) {
			print STDERR "******************************************************<br>\n";
			print STDERR "* Monitoring Error: Cant Connect To Server -  Marking Connection Failed<br>\n";
			print STDERR "* Unable to connect to $server_type server $server as login $login password $password from Host=".hostname()."<br>\n";
			print STDERR "* PERL=$^X <br>\n";
			print STDERR "* SYBASE=$ENV{SYBASE}<br>\n";
			print STDERR "* srv=$server<br>\n";
			print STDERR "* login=$login<br>\n";
			print STDERR "* password=$password<br>\n";
			print STDERR "* type=$server_type<br>\n";
			print STDERR "* FILE=",__FILE__,"\n";
			print STDERR "* error number=".$DBI::err."<br>\n";
			print STDERR "* errstr=".$DBI::errstr."<br>\n";
			print STDERR "******************************************************<br>\n";
			#print STDERR "Monitoring Error: Cant Connect To Server -  Marking Connection Failed\nUnable to connect to $server_type server $server as login $login\nHost=".hostname()."\nPERL=$^X \nSYBASE=$ENV{SYBASE}\nerror number=".$DBI::err."\nerrstr=".$DBI::errstr,"\n";
		}
		$i_am_initialized = "FAILED";
		return 0;
	};
	$connection=$rc;
	
	if( $serverP ne $server or $db ne $dbP ) {
		print "dbi_connect(-srv=>$serverP, -login=>$loginP, -password=>$passwordP,
				-type=>$server_typeP, -debug=>$args{-debug},
		  		-nosybaseenvcheck =>1,	-connection=>1, 	-die_on_error=>0)\n",$args{-NL}	if $args{-debug};
		my($rc) = dbi_connect(-srv=>$server, -login=>$login, -password=>$password,   -nosybaseenvcheck =>1,
			-type=>$server_type, -debug=>$args{-debug}, -connection=>1, -die_on_error=>0);
		if( ! $rc ) {
			print "dbi_connect() Failed\n",$args{-NL} if $args{-debug};
			if( ! defined $quiet ) {
				print STDERR "******************************************************<br>\n";
				print STDERR "* Monitoring Error: Cant Connect To Server -  Marking Connection Failed<br>\n";
				print STDERR "* Unable to connect to $server_type server $server as login $login password $password from Host=".hostname()."<br>\n";
				print STDERR "* PERL=$^X <br>\n";
				print STDERR "* SYBASE=$ENV{SYBASE}<br>\n";
				print STDERR "* srv=$server<br>\n";
				print STDERR "* login=$login<br>\n";
				print STDERR "* password=$password<br>\n";
				print STDERR "* type=$server_type<br>\n";
				print STDERR "* FILE=",__FILE__,"\n";
				print STDERR "* error number=".$DBI::err."<br>\n";
				print STDERR "* errstr=".$DBI::errstr."<br>\n";
				print STDERR "******************************************************<br>\n";
				#print STDERR "Monitoring Error: Cant Connect To Server -  Marking Connection Failed\nUnable to connect to $server_type server $server as login $login\nHost=".hostname()."\nPERL=$^X \nSYBASE=$ENV{SYBASE}\nerror number=".$DBI::err."\nerrstr=".$DBI::errstr,"\n";
			}
			$i_am_initialized = "FAILED";
			return 0;
		};
		$connectionP=$rc;
		print "Connected as $loginP / $passwordP - Trying to use $db\n",$args{-NL} if $args{-debug};
		$rc=dbi_use_database($db,$connectionP);
		print "Use Db Failed to Use $db\n" if $rc == 0;
		return 0 if $rc == 0;
	} else {
		$connectionP=$connection;
	}	
	dbi_set_mode("INLINE");

	print "Connected as $login / $password - Trying to use $db\n",$args{-NL} if $args{-debug};
	$rc=dbi_use_database($db,$connection);
	print "Use Db Failed to Use $db\n" if $rc == 0;
	return 0 if $rc == 0;

	print "looks good... Alarm System Initialization Successful\n",$args{-NL} if $args{-debug};
	$i_am_initialized="TRUE";
	return 1;
}

sub validate_params
{
	my($func,%OPT)=@_;

	# check for bad keys
	foreach (keys %OPT) {

		next if defined $OK_OPTS{$func.":".$_};
		next if /-debug/;

		foreach my $x (keys %OPT) {
			print "validate error: arg $x => $OPT{$x} $OPT{-NL} \n";
		}

		# did you mix up name=>x with -name=>x
		croak( "ERROR: Incorrect $func Arguments - Change $_ argument to -".$_."\n" )
			if defined $OK_OPTS{$func.":-".$_};

		croak( "ERROR: Function $func Does Not Accept Parameter Named $_\n" );
	}

	# check for missing required keys
	foreach (keys %OK_OPTS) {
		next unless /^$func:/;
		next unless $OK_OPTS{$_} == 1;
		$_=~s/$func://;
		next if defined $OPT{$_};
		my($n) = "Passed Args=|";
		foreach ( keys %OPT ) { $n.=$_."|"; }

		confess( "ERROR: Function $func Requires Parameter Named |$_|\n|$n|\n" );
	}
	return %OPT;
}

sub MlpHeartbeat {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	my($query_suffix)="";
	if( defined $args1{-heartbeat} ) {
		# OK HEARTBEATS ARE SPECIAL CASES!
	   $query_suffix.=",\@reviewed_by =".  dbi_quote(-text=>'HEARTBEAT', -connection=>$connection);
	   $query_suffix.=",\@reviewed_time =".  dbi_quote(-text=>do_time(-fmt=>'mon dd yyyy hh:mi:ss',-time=>time), -connection=>$connection);
	   $query_suffix.=",\@reviewed_until=".  dbi_quote(-text=>do_time(-fmt=>'mon dd yyyy hh:mi:ss',-time=>(time+$args1{-heartbeat}*60)), -connection=>$connection);
	   $args1{-state} = "ERROR" unless defined $args1{-state};
	   #die $query_suffix;
	}

	if( defined $args1{-severity} and ! defined $args1{-state} ) {
		$args1{-state}=$args1{-severity};
		undef $args1{-severity};
	};

	$args1{-debug}=$debug unless  defined $args1{-debug};
	$args1{-state}=uc($args1{-state});
	$args1{-state}="OK" if defined $args1{-state} eq "INFORMATION" or defined $args1{-state} eq "DEBUG";
	#$args1{-state}="OK" if $args1{-monitor_program} eq "HEARTBEAT";

	my(%args)= validate_params("MlpHeartbeat",%args1);
	if(   $args{-state} ne "EMERGENCY" and
		   $args{-state} ne "CRITICAL" and
		   $args{-state} ne "STARTED" and
		   $args{-state} ne "RUNNING" and
		   $args{-state} ne "NOT_RUN" and
		   $args{-state} ne "COMPLETED" and
		   $args{-state} ne "ALERT" and
		   $args{-state} ne "ERROR" and
		   $args{-state} ne "WARNING" and
		   $args{-state} ne "OK" ) {
		if( defined $args1{-batchjob} ) {
			warn "MlpHeartbeat Error : Invalid state {$args{-state}}\nValid states are: STARTED COMPLETED RUNNING NOT_RUN EMERGENCY CRITICAL ALERT ERROR WARNING OK INFORMATION DEBUG";
		} else {
			warn "MlpHeartbeat Error : Invalid state {$args{-state}}\nValid states are: EMERGENCY CRITICAL ALERT ERROR WARNING OK INFORMATION DEBUG";
		}
		return;
	}

	$args{-message_text} =~ s/\0/ /g if defined $args{-message_text};

	my($query);
	# handle -reset_status
	if( $args{-reset_status} ) {
		$query="UPDATE Heartbeat set state='NOT_RUN',message_text='".$args{-message_text}."' ".
   	" where monitor_program =".  dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection).
   	" and system =".  dbi_quote(-text=>$args{-system}, -connection=>$connection);
   	$query.=" and subsystem =".  dbi_quote(-text=>$args{-subsystem}, -connection=>$connection) if defined $args{-subsystem};
		print $query if $args{-debug};
		foreach ( dbi_query(-db=>$db,-no_results=>1,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			my($txt)=join(" ",dbi_decode_row($_));
			next if $txt=~/^\s*$/;
			chomp $txt;
			print "Heartbeat(-reset_status) ($query):",$txt,"\n"
				unless defined $quiet;
		}
	}

	print "MlpHeartbeat(): " if $args{-debug};
	$query= "exec Heartbeat_proc
   \@monitor_program =".  dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection).",
   \@system =".  dbi_quote(-text=>$args{-system}, -connection=>$connection).",
   \@subsystem =".  dbi_quote(-text=>$args{-subsystem}, -connection=>$connection).",
   \@state =".  dbi_quote(-text=>$args{-state}, -connection=>$connection).",
   \@message_text =".  dbi_quote(-text=>$args{-message_text}, -connection=>$connection).",
   \@batchjob =".  dbi_quote(-text=>$args{-batchjob}, -connection=>$connection).",
   \@document_url =".  dbi_quote(-text=>$args{-document_url}, -connection=>$connection)."\n";
	$query.=",\@heartbeat_time=".dbi_quote(-text=>$args{-event_time}, -connection=>$connection)."\n"
			if defined $args{-event_time};
	$query.=$query_suffix if $query_suffix;
	print $query if $args{-debug};

	my($ok)="TRUE";
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		$ok="FALSE";
		my($txt)=join(" ",dbi_decode_row($_));
		next if $txt=~/^\s*$/;
		chomp $txt;
		print "Hearbeat Save ($query):",$txt,"\n"
			unless defined $quiet;
	}

	if($ok eq "FALSE") {
		my($out)="Save Of Heartbeat Failed\n";
		$out.= " err=".$DBI::err."\n" 			if defined $DBI::err;
		$out.= " errstr=".$DBI::errstr."\n" 	if defined $DBI::errstr;
		chomp $out;
		chomp $out;
		chomp $out;
		print $out,"\n"
			unless defined $quiet;
	}

	if( $args{-debug} ) {
		print "Saved: ",$args{-state}," ",$args{-message_text},"\n"
	}
}

sub MlpReportArgs {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Report Args: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	my(%args)= validate_params("MlpReportArgs",%args1);
	my(%results);

	# standard Always On Args
	if( $args{-reportname} !~ /^INV_/ and $args{-reportname} !~ /^GEM_/ and $args{-reportname} !~ /^DCM_/ ) {
		$results{submit}					=	"Run Report";
		$results{NONAVIGATION}			=	1;
		$results{radio_screen}			=	"All Reports";
		$results{shrinkfont}				=	"shrink font" 		unless $args{-reportname}=~/^GemRpt/;
		$results{filter_display}		=	"Details";
		$results{UseHeartbeatTime}		=	"1";

		# Args we exptect to be changed (these are default values);
		$results{filter_severity}		=	"Warnings";
		$results{filter_time}			=	"5 Days";
		$results{filter_container}		=	"All";
	}
	#$results{-debug}					=	"1";

	if( $args{-reportname} eq "genreport" ) {
		my(%data,%keys,%reports);
		my($report_query)= "select reportname,keyname,value from ReportArgs";
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection,
				-die_on_error=>0, -debug=>$args{-debug} )) {
			my(@x) = dbi_decode_row($_);
			$reports{$x[0]} =1;
			$keys{$x[1]} =1;
			if( $data{$x[0].":".$x[1]} ) {
				$data{$x[0].":".$x[1]} .= ",".$x[2];
			} else {
				$data{$x[0].":".$x[1]} = $x[2];
			}
		}

		my($outstr)="<TABLE BORDER=1><TR><TH>Reportname</TH>";
		foreach my $k ( sort keys %keys ) {
			$outstr.= "<TH>$k</TH>\n";
		}
		$outstr.="</TR>";
		foreach my $r ( sort keys %reports ) {
			$outstr.= "<TR><TD>".$r."</TD>";
			foreach my $k ( sort keys %keys ) {
				$outstr.= "\t<TD>". $data{$r.":".$k} ."&nbsp;</TD>\n";
			}
			$outstr.= "</TR>\n";
		}
		$outstr.="</TABLE>";
		return $outstr;
	}

	my($report_query)= "select reportname,keyname,value from ReportArgs where reportname='".$args{-reportname}."'";
	my($found)="FALSE";
	foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection,	-die_on_error=>0, -debug=>$args{-debug})){
		my(@x) = dbi_decode_row($_);
		$found="TRUE";
		if( $x[1] eq "ProdChkBox" ) {
				$results{$x[1]} = $x[2] if $x[2] eq "YES";
		} else {
				$results{$x[1]} = $x[2];
		}
	}

	die "ERROR: $server/$db Can Not Find STATIC Information For Report $args{-reportname}
- invalid report - No Data In ReportArgs Table 
$report_query
- Line ".__LINE__." File ".__FILE__."\n"
		if $found eq "FALSE";

	if( $args{-debug} ) {		foreach (keys %results) {	print " results($_)=$results{$_} \n";		}	}

	return %results;
}

sub MlpClearPL {
	my(%args)=@_;

	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	my($query)= "DELETE ProgramLookup";
	foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		print "Returned: ",join(" ",dbi_decode_row($_)),"\n";
	}
}

sub MlpSetPL {
	my(%args)=@_;
	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}
	my($query)= "insert ProgramLookup values ('".
				$args{monitor_program}.
				"','".
				$args{use_system}.
				"','".
				$args{job_type}.
				"','".
				$args{rec_frequency_str}.
				"'".
				" )";
	foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		print "Returned: ",join(" ",dbi_decode_row($_)),"\n";
	}
}

# UPDATE heartbeat - used for repeated messages
sub MlpEventUpdate {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Event: Connection Was Marked Failed\n" 	unless defined $quiet;
		return 0;
	};
	$args1{-debug}=$debug unless  defined $args1{-debug};

	my(%args)= validate_params("MlpEventUpdate",%args1);

	$args{-message_text} =~ s/\0/ /g;

	my($query)="UPDATE Event
		set message_text=".
			dbi_quote(-text=>$args{-message_text}, -connection=>$connection).
		" where monitor_program=".
			dbi_quote(-text=>$args{-monitor_program},-connection=>$connection).
   	" and system=".
		dbi_quote(-text=>$args{-system}, -connection=>$connection).
   	" and event_time=".
		dbi_quote(-text=>$args{-event_time}, -connection=>$connection);
		#$query.= " and batchjob is null";
		$query.= " and subsystem=".  dbi_quote(-text=>$args{-subsystem}, -connection=>$connection) if defined $args{-subsystem};

	print "DBG: QUERY=$query\n" if $args{-debug};
	my($ok)="TRUE";
	foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0, -no_results=>1 )) {
		$ok="FALSE";
		print join(" ",dbi_decode_row($_)),"\n"
			unless defined $quiet;
	}
	print "WARNING: Query Failed: $query" if $ok eq "FALSE"
			and ! defined $quiet;
}

sub MlpTestInstall
{
	my(%args1)=@_;
	print "MlpTestInstall(): Running In Debug Mode ($i_am_initialized)\n" if defined $args1{-debug};

	return "Alarm System Has Not Been Configured"
		if $server eq "" or $login eq "" or $password eq "" or $db eq "";

	my($rc)=initialize(%args1) unless $i_am_initialized eq "TRUE";
	if( $rc==0 ) {
		return "Connect to $server may have failed\n" if ! defined $connection;
		return "Connect to $server ok but use $db may have failed\n";
	}

	if($i_am_initialized eq "FAILED" ) {
		return "Initialization Failed";
	}

	my(%objects) = (
Container		=> 'Missing',
Container_full		=> 'Missing',
Screen			=> 'Missing',
Severity		=> 'Missing',
Operator		=> 'Missing',
Heartbeat		=> 'Missing',
Event			=> 'Missing',
HeartbeatThresh		=> 'Missing',
clear_possible_alarms	=> 'Missing',
IgnoreList		=> 'Missing',
ProductionServers	=> 'Missing',
Heartbeat_proc		=> 'Missing',
Event_proc		=> 'Missing',
build_container		=> 'Missing',
get_alarm_version	=> 'Missing',
);
	foreach ( dbi_query(-db=>$db, -query=>"select name,type from sysobjects where type in ('U','P','TR')",
		-connection=>$connection, -die_on_error=>0 )) {
		my(@d)=dbi_decode_row($_);
		if( $d[0] =~ /_BAK$/ ) {
			return "A prior instalation of the Alarm System Appears to have failed. The table $d[0] is a backup of system data.  Data from this table should be inserted into the base table and this table should then be removed\n";
		}
		$objects{$d[0]} = "OK";
	}
	my($req_version)=2.1;

	if( ! defined $objects{Heartbeat} ) 		{ return "Alarm System Has Not Been Installed\n"; }
	if( ! defined $objects{get_alarm_version} ) 	{
		return "Alarm System Is not at v$req_version or later. Please Resinstall\n";
	}

	$rc="";
	foreach (keys %objects) { $rc.="Missing Object $_\n" if $objects{$_} ne "OK"; }
	return $rc if $rc ne "";

	my($version);
	foreach ( dbi_query(-db=>$db, -query=>"exec get_alarm_version",
		-connection=>$connection, -die_on_error=>0 )) {
		my(@d)=dbi_decode_row($_);
		$version=$d[0];
	}

	return "Alarm System Is At $version, not at v$req_version or later. Please Resinstall\n"
		unless $version>=$req_version;

	return "";
}

sub MlpEvent
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Event: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	# $args1{-system}=$system
			# unless  defined $args1{-system};
	# $args1{-subsystem}=$subsystem
			# unless  defined $args1{-subsystem};
	# $args1{-monitor_program}=$monitor_program
			# unless  defined $args1{-monitor_program};
	$args1{-debug}=$debug
			unless  defined $args1{-debug};

	my(%args)= validate_params("MlpEvent",%args1);
	$args{-severity} = "INFORMATION" if $args{-severity} eq "OK";

	if( defined $args{-condense} ) {
		if( defined $args{-event_time} and
		$condense_args{-system} eq $args{-system} and
		$condense_args{-monitor_program} eq $args{-monitor_program} and
		$condense_args{-subsystem} eq $args{-subsystem} and
		$condense_args{-severity} eq $args{-severity} and
		$condense_args{-message_text} eq $args{-message_text}  ) {
			$condense_args{-count}=1 unless defined $condense_args{-count};
			$condense_args{-count}++;

			MlpEventUpdate(
				#-event_time => $args{-event_time},
				-system=>	$args{-system} ,
				-monitor_program=>	$args{-monitor_program} ,
				-subsystem=>	$args{-subsystem} ,
				-event_time=>	$condense_args{-event_time} ,
				-message_text=> "[message repeated $condense_args{-count} times] ".$args{-message_text}  );
			return;
		}
		foreach ( keys %args )  { $condense_args{$_} = $args{$_}; }
	}

	warn "Warning : Invalid severity  $args{-severity}\n"
		unless $args{-severity} eq "EMERGENCY" or
		       $args{-severity} eq "CRITICAL" or
		       $args{-severity} eq "ALERT" or
		       $args{-severity} eq "RUNNING" or
		       $args{-severity} eq "NOT_RUN" or
		       $args{-severity} eq "COMPLETED" or
		       $args{-severity} eq "STARTED" or
		       $args{-severity} eq "ERROR" or
		       $args{-severity} eq "WARNING" or
		       $args{-severity} eq "INFORMATION" or
		       $args{-severity} eq "DEBUG";

	my($query)="exec Event_proc ".
   		"\@monitor_program=".
		dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection).",\n\t\t".
   		"\@monitor_time=";

	if( defined $args{-monitor_time}) {
		$query.=  dbi_quote(-text=>$args{-monitor_time}, -connection=>$connection).",\n\t\t";
	} else {
		$query.=  "null,\n\t\t";
	}

	my($txt)=substr($args{-message_text},0,130);
	if( $args{-nosave} ) { 	print "NOSAVE: txt=$txt\n"; }
	$txt=~s/\0/ /g;
	$txt=~s/\'//g;
	#$txt=~s/[\s\W]+$//;
	$txt=~s/\s+$//;
	#$txt="\'". $txt."\'";
	if( $args{-nosave} ) { 	print "NOSAVE: txt=$txt\n"; }

	$txt=dbi_quote(-text=>$txt, -connection=>$connection);
	if( $args{-nosave} ) { 	print "NOSAVE: txt=$txt\n"; }

	$query.=
   		"\@system=".
		dbi_quote(-text=>$args{-system}, -connection=>$connection).",\n\t\t".
   		"\@subsystem=".
		dbi_quote(-text=>$args{-subsystem}, -connection=>$connection).",\n\t\t".
   		"\@event_time=";

	if( defined $args{-event_time} ) {
		$query.=  dbi_quote(-text=>$args{-event_time}, -connection=>$connection).",\n\t\t";
	} else {
		$query.=  "null,\n\t\t";
	}

	$query.="\@severity=".
		dbi_quote(-text=>$args{-severity}, -connection=>$connection).",\n\t\t".
   	#	"\@event_id=".$args{-event_id}.",\n".
		#dbi_quote(-text=>$args{-event_id}, -connection=>$connection).",\n".
   		"\@message_text=".  $txt.",\n\t\t";
	#dbi_quote(-text=>$txt, -connection=>$connection).",\n\t\t";

	$query.= 		"\@event_id=".$args{-event_id}.",\n" if $args{-event_id};


	if( $args{-message_value} and $args{-message_value}=~/^\d+$/) {
		$query.= "\@message_value=".$args{-message_value}.",\n\t\t";
	} else {
		$query.= "\@message_value=null,\n\t\t";
	}
   	#"\@document_url=".
	#dbi_quote(-text=>$args{-document_url}, -connection=>$connection).",\n\t\t".
   	$query.="\@internal_state=".  "'I'\n";

   	#print $query;

	my($ok)="TRUE";
	if( $args{-nosave} ) {
		print "NOSAVE: db=$db query=$query\n";
	} else {
		print $query if $args{-debug};
		# , -debug=>$args{-debug}
		foreach ( dbi_query(-db=>$db, -query=>$query, -connection=>$connection, -die_on_error=>0, -no_results=>1 )) {
			$ok="FALSE";
			print join(" ",dbi_decode_row($_)),"\n"
				unless defined $quiet;
		}
	}

#	 THIS WHOLE BLOCK IS DEBUG TEXT
#	if( $ok eq "FALSE" ) {
#		open(DBG,">tmp.out");
#		print DBG $query;
#		print DBG "\n";
#
#	my($txt)=substr($args{-message_text},0,130);
#	$txt=~s/\0/ /;
#	#$txt=~s/\'//g;
#	#$txt=~s/[\s\W]+$//;
#	#$txt="\'". $txt."\'";
#	#$txt=dbi_quote(-text=>$txt, -connection=>$connection);
#
#		my(@d)=split("",$txt);
#		my($i)=0;
#		foreach (@d) {
#			print DBG $i,": $d[$i] => ",ord($d[$i]),"\n";
#			$i++;
#			last if $i>20;
#		}
#		print DBG $txt."\n";
#		close(DBG);
#		die "DONE";
#	}

	print "WARNING: Query Failed:\n$query" if $ok eq "FALSE"
			and ! defined $quiet;

	print "Saved: ",$args{-severity}," ",$args{-message_text},"\n" if $args{-debug};
}

sub MlpGetCategory
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetCategory",%args1);
	my($query)= "SELECT distinct category from Screen\n";
	$query.=" where owner = \'".$args{-user}."\'" if defined $args{-user};
	print $query if $args{-debug};

	my(@rc);
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub MlpGetScreens
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetScreens",%args1);
	my($query)= "SELECT distinct screen_name from Screen where 1=1\n";
	$query.=" and owner = \'".$args{-user}."\'" if defined $args{-user};
	$query.=" and owner is null " unless defined $args{-user};
	$query.=" and category = \'".$args{-category}."\'"
		if defined $args{-category};

	print $query if $args{-debug};
	my(@rc);
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub _warn {
	my( $html, $str )=@_;
	return $str unless $html;
	return "<font color=red>$str</font>";
}

# Run A Screen Returning Array of Arrays
# return $array_of_arrays and $!

sub _debugcolor {
	my($is_html)=shift;
	return("<PRE><FONT COLOR=red SIZE=-2>@_</FONT></PRE>") if $is_html;
	return(@_);
}
sub MlpRunScreen
{
	my(%args1)=@_;
	my($credentialwhere)='';
	$credentialwhere.=" and ( p.credential_name like '".$args1{-credentialfilter}."' or p.principal_name like '".$args1{-credentialfilter}."')" if $args1{-credentialfilter} !~ /^\s*$/;
	$credentialwhere.=" and p.is_deleted='N' "
		if $args1{-showdeleted} eq "NO" and  $args1{-screen_name} ne "INV_SEE_UNUSEABLE";

	# foreach ( keys %args ) { print $_ . " => $args{$_} <br>" unless $_ eq "-NL"; }

	#foreach ( keys %args1 ) { print __LINE__, " ".$_." ".$args1{$_}.$args1{-NL}."\n"; }
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};


	my(%args)= validate_params("MlpRunScreen",%args1);
	$args{-debug}='Y' if is_developer();

	my($NL) = $args{-NL} || "\n";
	if( $args{-poweruser} eq "Y" ) {
		my($argstr)='<TABLE WIDTH=100% BORDER=1><TR><TD COLSPAN=3 BGCOLOR=PEACH>MlpRunScreen() LINE='.__LINE__.
			" in MlpAlarm.pm SQL PRINT FOR SHOW DIAGNOSTICS";
		$argstr.="</TD></TR><TR>";
		my($argstrlong)='';
		my($cnt)=0;
		foreach ( sort keys %args ) {
			next if /^-NL$/;
			next unless $args{$_};
			if( length($args{$_}) > 30 ) {
				$argstrlong.= "<TR><TD COLSPAN=3>$_ => <FONT COLOR=RED>$args{$_}</FONT> </TD></TR>";
			} else {
				$argstr.= "<TD>Arg $_ => <FONT COLOR=RED>$args{$_} </FONT></TD>";
				$cnt++;
				$argstr .= "</TR><TR>" if $cnt%3 == 0;
			}
		}
		$argstr .= "</TR>".$argstrlong."</TABLE>";
		print "<FONT SIZE=-1 COLOR=BLUE>",$argstr,"</FONT>";
	}

	my(@outarray);	# array of arrays to return

	# INSTRUCTIONS FOR THIS SECTION
	#  return stuff in an array of pointers to arrays
	#  each element of the array is a row
	#  the first row is the header
	#  the header can have the following keywords in it
	#     *_LU - lookup, and in last row Both(=Add+Delete),Add,Delete,Update
	if( $args{-screen_name} eq "Delete By Monitor_Type") {
		my(@x)=qw(Monitor Type Batch Time Delete);
		push @outarray,\@x;
		my($report_query)=
"select monitor_program,convert(varchar,max(monitor_time),0),batchjob,'Heartbeat'
from Heartbeat where internal_state!='D' /* and isnull(batchjob,'x') !=\'AGENT\' */
group by monitor_program, batchjob
union
select monitor_program, convert(varchar,max(monitor_time),0),'','Event'
from Event where internal_state!='D'
group by monitor_program
order by monitor_program";

		my($d_monitor_program,$d_monitor_time,$d_batch,$d_system);
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			($d_monitor_program,$d_monitor_time,$d_batch,$d_system)= dbi_decode_row($_);
			my(@x)=($d_monitor_program,$d_system,$d_batch,$d_monitor_time );
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Delete By Monitor") {
		my(@x)=qw(Monitor Delete);
		push @outarray,\@x;
		my($report_query)=
"select distinct monitor_program from Heartbeat where internal_state!='D' and isnull(batchjob,'x') !=\'AGENT\' group by monitor_program
union
select monitor_program from Event where internal_state!='D' group by monitor_program
order by monitor_program";

		my($d_monitor_program);
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			($d_monitor_program)= dbi_decode_row($_);
			my(@x)=($d_monitor_program );
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Containers" ) {
		my(@x)=qw(Container Type System);
		push @outarray,\@x;
		my($report_query)=
		"select 	name,
			Type=case
				when type='c' then 'Container'
				when type='s' then 'Server'
				when type='p' then 'Program'
				else 'unknown'
			end,
			element
from Container order by name,type";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "System->Container" ) {
		my(@x)=qw(Container System_LU Both);
		push @outarray,\@x;
		my($report_query)=
		"select 	name,
			-- Type=case
				-- when type='c' then 'Container'
				-- when type='s' then 'Server'
				-- else 'unknown'
			-- end,
			element
from ContainerOverride order by name,type";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Production" ) {
		my(@x)=qw(Server Chkbox_Is_Production Chkbox_Ignore Chkbox_Blackout Delete);
		push @outarray,\@x;

#		my($report_query)="
#select p.system,p.is_production,
#	ignore=case 	when i.system is not null then 1 	else 0	end,
#	blackout=case	when b.system is not null then 1 	else 0	end
#from ProductionServers p, IgnoreList i, Blackout b
#where p.system *= i.system
#and   i.monitor_program is null
#and   i.subsystem is null
#and   p.system*=b.system
#order by p.system";

		my($report_query)="
select distinct p.system,p.is_production
	, ignore=case 	when i.system is not null then 1 	else 0	end
	 ,blackout=case	when b.system is not null then 1 	else 0	end
from ProductionServers p
	left outer join IgnoreList i on p.system = i.system
	and i.monitor_program is null
	and   i.subsystem is null
    left outer join Blackout b on p.system=b.system
order by p.system";

		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Program->Container" ) {
		my(@x)=qw(Program_LU Container_LU Both);
		push @outarray,\@x;
		my($report_query)= "select 	monitor_program, container from ContainerMap order by monitor_program";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Delete By Row" ) {
		my(@x)=qw(Monitor System Type Rows Time Delete);
		push @outarray,\@x;
		my($report_query)=
"select monitor_program,system,dtype='Heartbeat',count(*),max(monitor_time) from Heartbeat where internal_state!='D'
	and isnull(batchjob,'x') !=\'AGENT\' group by monitor_program,system
union
select monitor_program,system,dtype='Event',count(*),max(monitor_time)     from Event where internal_state!='D' group by monitor_program,system
order by monitor_program,system,dtype";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Alarm History" ) {
		my(@x)=qw(Notify_User Notify_Time Alarm_Type Alarm_Subject Alarm_Message);
		push @outarray,\@x;
		my($report_query)=
		"select 	user_name,convert(varchar,notify_time,0),notify_type,subject,message
			from SentNotification order by notify_time desc";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Alarm Routing" ) {
		my(@x)=qw( User_LU Program_LU INV_Business_System_LU System Min_Severity_LU Max_Severity_LU Both);
		push @outarray,\@x;
		 #use_pager,use_email,use_netsend,use_blackberry
		my($report_query)=	"select user_name,monitor_program,container,system,min_severity,max_severity
		from Notification order by user_name";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Filter Repeated Messages" ) {
		my(@x)=qw( User Program INV_Business_System_LU System EmergencyMin FatalMin ErrorMin WarningMin Update);
		push @outarray,\@x;
		my($report_query)=
		"select user_name,monitor_program,container,system,
			emergency_minutes, fatal_minutes, error_minutes, warning_minutes
		from Notification order by user_name";
		print _debugcolor( $args1{-html},$report_query),"\n";
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Operator" ) {
		my(@x)=qw(User Enabled_LU Txtbox40_Email Both);
		push @outarray,\@x;
		my($report_query)=
		"select 	user_name, enabled, email
			from Operator order by user_name";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Report Setup" ) {
		my(@x)=qw(Report_Name Min_Severity_LU Time_LU Container_LU Production_YN_LU RptType_LU Show_Program_YN_LU Program SysName Subsystem Title Both);
		push @outarray,\@x;
		my($report_query)= "select reportname, keyname, value	from ReportArgs order by reportname";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		my(%time,%container,%severity,%production,%showprogram,%reporttitle,%progname,%radio_screen,%system,%subsystem);
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			if( $x[1] eq "filter_severity" ) {				$severity{$x[0]} = $x[2];
			} elsif( $x[1] eq "filter_time" ) {				$time{$x[0]} = $x[2];
			} elsif( $x[1] eq "filter_container" ) {		$container{$x[0]} = $x[2];
			} elsif( $x[1] eq "ProdChkBox" ) {				$production{$x[0]} = $x[2];
			} elsif( $x[1] eq "radio_screen" ) {			$radio_screen{$x[0]} = $x[2];
			} elsif( $x[1] eq "progname" ) {
				if( $progname{$x[0]} ) {
					$progname{$x[0]} .= $x[2];
				} else {
					$progname{$x[0]} = $x[2];
				}
			} elsif( $x[1] eq "system" ) {					$system{$x[0]} = $x[2];
			} elsif( $x[1] eq "subsystem" ) {				$subsystem{$x[0]} = $x[2];
			} elsif( $x[1] eq "ShowProgram" ) {				$showprogram{$x[0]} = $x[2];
			} elsif( $x[1] eq "ReportTitle" ) {				$reporttitle{$x[0]} = $x[2];
			}
		}
		foreach ( sort keys %time ) {
			my(@x);
			#push @x,$_;
			push @x,"<A HREF=?REPORTNAME=$_>$_</A>";
			push @x,$severity{$_};
			push @x,$time{$_};
			push @x,$container{$_};
			push @x,$production{$_};
			push @x,$radio_screen{$_};
			push @x,$showprogram{$_};
			push @x,$progname{$_};
			push @x,$system{$_};
			push @x,$subsystem{$_};
			push @x,$reporttitle{$_};
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Ignore List" ) {
		my(@x)=qw(Program_LU System Subsystem Both);
		push @outarray,\@x;
		my($report_query)=
		"select 	monitor_program,system,subsystem
			from IgnoreList where monitor_program is not null order by monitor_program,system,subsystem";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Blackout Report" ) {
		my(@x)=qw(List Program_LU System Subsystem Enddate);
		push @outarray,\@x;
		my($report_query)=
		"select System=system,IgnoreType='IgnoreList',Program=monitor_program,SubSystem=subsystem,EndDate=null from IgnoreList
			union
			select system,'Blackout',null,null,enddate from Blackout
			order by system";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Operator Schedule" ) {
		my(@x)=qw(User_LU WD_Start_LU WD_End_LU Sat_Start_LU Sat_End_LU Sun_Start_LU Sun_End_LU Hol_Start_LU Hol_End_LU Update);
		push @outarray,\@x;
		my($report_query)=
		"select 	user_name,
   			isnull(weekday_start_time,0)   , isnull(weekday_end_time,24)     ,
			isnull(saturday_start_time,0)  , isnull(saturday_end_time,24)    ,
			isnull(sunday_start_time,0) ,    isnull(sunday_end_time,24)      ,
   			isnull(holiday_start_time,0)   , isnull(holiday_end_time,24)
			from Operator order by user_name";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Agents" ) {

		my(@x)=qw(MonitorProgram System Sched_Host Frequency Last_Run Text Minutes);
		push @outarray,\@x;

my($report_query)="select h.monitor_program, h.system, h.subsystem, rec_frequency_str,
	monitor_time=convert(varchar,monitor_time,0), message_text
	--timedif=max(monitor_time)
	--timedif=datediff(mi,max(monitor_time),getdate())
into #tmp
from ProgramLookup p,Heartbeat h
where p.monitor_program = h.monitor_program and batchjob='AGENT'

select monitor_program,timedif=datediff(mi,max(monitor_time) ,getdate())
into #times from Heartbeat
group by monitor_program

insert #tmp
select monitor_program, '', '', rec_frequency_str, 	'Never', 'No Heartbeats Found / Is this job scheduled?'
from ProgramLookup
where monitor_program not in
(
	select monitor_program from Heartbeat where batchjob='AGENT'
)

select h.monitor_program, h.system, h.subsystem, rec_frequency_str,
	monitor_time, message_text, isnull(timedif,-1),'' from #tmp h
	left outer join #times on h.monitor_program = #times.monitor_program";

$report_query.="
where ( (rec_frequency_str = 'frequently' and timedif >2*15)
or  	(rec_frequency_str = 'hourly' and timedif >2*60)
or  	(rec_frequency_str = 'daily' and timedif >2*24*60)
or  	( timedif >2*7*24*60) -- weekly
or	timedif = -1 or timedif is null
)" if $args{-severity} eq "Errors"
		or $args{-severity} eq "Fatals"
		or $args{-severity} eq "Emergency";

$report_query.=" order by timedif desc, monitor_program
drop table  #tmp
drop table  #times";

		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;

	} elsif( $args{-screen_name} eq "INV_CHANGE_TRACKER" ) {
		my(@x)=qw(System System_Type Change_Time Message);
		push @outarray,\@x;
		my(@ss)=SetupTmpTable(%args);
		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN>".join(" ",@ss)."</FONT>}".$NL if $args{-debug};

		my($report_query)="select s.system_name,s.system_type,convert(varchar,change_time,107),change_message
				from INV_configuration_history s, #ServerSubset ss
	where s.system_name=ss.system_name
	and   s.system_type=ss.system_type
	and   change_message not like 'initial%'
   order by change_time desc, system_name";
		print _debugcolor($args{-html},$report_query,"\n") if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_MANAGE_BUSSYSTEM" ) {
		my(@x)=qw(HYPERLINK Business_System_Name Both);
		push @outarray,\@x;
		my($report_query)=
		"select business_system_name from INV_business_system where is_view='N' and is_systemview='N' order by business_system_name";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			unshift @x, '?REPORTNAME=INV_BUSINESS_SYSTEM_MEMBERS&NOARGFETCH=1&filter_container='.$x[0];
			push @outarray, \@x;
		}
		if( $#outarray<=0 ) {		# NONE - startup?
			my(@x)=('DUMMY RECORD - DO NOT DELETE');
			unshift @x, '?REPORTNAME=INV_BUSINESS_SYSTEM_MEMBERS&NOARGFETCH=1&filter_container='.$x[0];
			push @outarray, \@x;
		}
		return \@outarray;
#	} elsif( $args{-screen_name} eq "INV_DEFINE_ALIASES" ) {
#		my(@x)=qw(System_Name System_Type Alias_System_Name Both);
#		push @outarray,\@x;
#		my($report_query)=
#		"select system_name, system_type, alias_name
#			from INV_aliases order by system_name, system_type";
#		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			push @outarray, \@x;
#		}
#		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_BUSINESS_SYSTEM_MEMBERS" ) {
		my(@x)=qw(INV_Business_System_LU INV_Database_LU INV_Hostname_LU Both);
		push @outarray,\@x;

		my($report_query)="select business_system_name,
	case when system_type in ('sybase','oracle','sqlsvr')     then system_name else null end as 'Database',
	case when system_type not in ('sybase','oracle','sqlsvr') then system_name else null end as 'Hostname'
   from INV_system_heirarchy ";
		$report_query .= " where business_system_name='".$args{-container}."'" if $args{-container};
		#"select business_system_name, system_name+'.'+system_type
		#	from INV_system_heirarchy order by business_system_name, system_name";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	#} elsif( $args{-screen_name} eq "INV_DEFINE_SYSGROUP" ) {
	#	my(@x)=qw( INV_LDAP_GROUP_LU INV_PERMISSION_LU Both);
	#	push @outarray,\@x;
	#	return \@outarray;
	} elsif( $args{-screen_name} eq "INV_DEFINE_SYSUSERS" 	 ) {
		
#		Both
#	Update
#	Delete
#	Add
#	OkDelete
#	Ok
		my(@x)=qw( INV_LDAP_USER_LU INV_PERMISSION_LU Both);
		push @outarray,\@x;
		my($report_query)=
		"select lower(samid), permission from INV_systemuser where permission not like 'Dflt~%' order by samid,permission";
		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
#	} elsif( $args{-screen_name} eq "INV_NEW_SYSTEM_TRACKER" ) {
#		my(@x)=qw(System_Name INV_SYSTEM_TYPE_LU Txtbox30_Description Txtbox30_Hostname Popup_INV_Waiting_On_LU Both );
#
#		push @outarray,\@x;
#		# Server Chkbox_Is_Production Chkbox_Ignore Chkbox_Blackout Delete
#
#		my($report_query)="
#	select system_name, system_type, description, hostname, waiting_on
#	from 	INV_proposed_system
#	order by system_name";
#
#		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			push @outarray, \@x;
#		}
#		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_DEFINE_SYSTEMS" ) {
		my(@ss)=SetupTmpTable(%args);
		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN>".join(" ",@ss)."</FONT>}".$NL if $args{-debug};
		my(@x)=qw(HYPERLINK System_Name INV_SYSTEM_TYPE_LU Chkbox_is_infra_monitored_LU Popup_INV_SERVER_TYPE_LU Chkbox_Is_Blackedout_LU Txtbox30_Description );
		push @outarray,\@x;
		#'?REPORTNAME=INV_DATABASE_METADATA&evl_databasenm='+system_name+'&evl_show_server=YES',

		my($report_query)="
	select case when s1.system_type in ('sybase','oracle','sqlsvr','mysql')
			 then '?REPORTNAME=INV_DATABASE_METADATA&evl_databasenm='+s1.system_name+'&evl_show_server=YES'
			 when s1.system_type in ('unix','win32servers')
			 then '?REPORTNAME=INV_COMPUTER_METADATA&evl_databasenm='+s1.system_name+'&evl_show_server=YES'
			 else ''
	       end as COLOR,
		    s1.system_name, s1.system_type, is_infra_monitored, production_state, is_blackedout, description
	from 	INV_system s1, #ServerSubset s2
	where s1.system_name=s2.system_name
	and   s1.system_type=s2.system_type
	order by s1.system_name";

		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
#	} elsif( $args{-screen_name} eq "INV_OUTAGE_TRACKER" ) {
#		my(@ss)=SetupTmpTable(%args);
#		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN><PRE>".
#			join(" ",@ss).
#			"</PRE></FONT>}".$NL if $args{-debug};	;
#		my(@x)=qw(System_Name INV_SYSTEM_TYPE_LU Chkbox_is_infra_monitored_LU Popup_INV_SERVER_TYPE_LU Chkbox_Is_Blackedout_LU Txtbox30_Description Both );
#		push @outarray,\@x;
#
#		my($report_query)="
#	select s1.system_name, s1.system_type, is_infra_monitored, production_state, is_blackedout, description
#	from 	INV_system s1, #ServerSubset s2
#	where s1.system_name=s2.system_name
#	and   s1.system_type=s2.system_type
#	order by s1.system_name";
#
#		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			push @outarray, \@x;
#		}
#		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_GROUP_CERT_HIST" ) {	# AUDIT CERTIFICATION REPORT
		my(@x)=qw(Date Approver Department Name Status Comment  );
		push @outarray,\@x;

		my($report_query)="select
			convert(varchar,approved_time,0), approved_by,
			department,display_name, status,comment+'&nbsp;'
			from INV_cert_department
			order by approved_time desc,department,display_name desc";
		push @outarray,_querywithresults($report_query,1);
		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_CRED_CERT_HIST" ) {	# AUDIT CERTIFICATION REPORT
		my(@x)=qw(Date Approver System Name Status Comment  );
		push @outarray,\@x;

		my($report_query)="select
			convert(varchar,approved_time,0), approved_by,
			system_name,display_name,status,comment+'&nbsp;'
			from INV_cert_credential
			order by approved_time desc,system_name";
		push @outarray,_querywithresults($report_query,1);
		return \@outarray;

#	} elsif( $args{-screen_name} eq "INV_AUD_CERT_STATE" ) {	# AUDIT CERTIFICATION REPORT
#
#		my(@x)=qw(COLOR Approver Certification_Type Certification_Target Last_Certification_Time How_Often_Required Permission );
#		push @outarray,\@x;
#
#	my($report_query)="
#	select distinct
#   case when permission = 'APPROVER' then 'CYAN'
#        when permission = 'DASHBOARD POWERUSER' then 'LIGHTBLUE'
#        else 'PINK'
#        end as COLOR,
#   display_name, r.certification_type, r.certification_target, convert(varchar,c.certification_time,107) , how_often_required, permission
#from INV_required_certification r, INV_certification c, INV_systemuser u, INV_credential x
#where r.samid*=c.approver_samid
#and   r.samid = u.samid
#and   r.certification_type*=c.certification_type
#and   x.credential_type = 'ldap user'
#and   x.credential_name = r.samid
#and   r.certification_target*=c.certification_target
#and   u.permission in ('DASHBOARD POWERUSER','APPROVER')
#
#	order by u.permission,display_name,r.samid,r.certification_type, r.certification_target";
#
#		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			push @outarray, \@x;
#		}
#		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_DEFINE_AUD_CERT" ) {
		my(@x)=qw( COLOR INV_CERTIFIER_LU INV_FREQUENCY_LU INV_Database_LU INV_Hostname_LU INV_LDAP_GROUP_LU Both );
		push @outarray,\@x;

	my($report_query)="
	select
		case when u.permission = 'APPROVER' then 'CYAN'
        when u.permission = 'DASHBOARD POWERUSER' then 'LIGHTBLUE'
        else 'PINK'
        end as COLOR,
		lower(r.samid), how_often_required,
		case when certification_type = 'DATABASE' then certification_target else null end as 'Database',
		case when certification_type = 'HOSTNAME' then certification_target else null end as 'Hostname',
		case when certification_type = 'GROUP'  	then certification_target else null end as 'LDAP Group'
	from 	INV_required_certification r, INV_system_permission_view u
	where   lower(u.samid)=lower(r.samid) and permission in ('APPROVER','DASHBOARD POWERUSER')
	order by u.permission,r.samid";

		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		
		if( $#outarray <= 0 ) {
			my($c);
			$report_query='select count(*) from INV_required_certification';
			foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
				($c) = dbi_decode_row($_);
			}
			if( $c =~ /^\d+$/ ) {
				if( $c == 0 ) {
					print "WARNING: No rows found in INV_required_certification<br>";
				#} else {
				#	print "$c rows found in INV_required_certification<br>";
				}
			} 
			$c = undef;
			$report_query='select count(*) from INV_system_permission_view where permission in (\'APPROVER\',\'DASHBOARD POWERUSER\')';
			foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
				($c) = dbi_decode_row($_);
			}
			if( $c =~ /^\d+$/ ) {
				if( $c == 0 ) {
					print "WARNING: No APPROVERS or DASHBOARD POWERUSERS found in INV_system_permission_view<br>";
				#} else {
				#	print "$c APPROVERS or DASHBOARD POWERUSERS found in INV_system_permission_view<br>";
				}
			} 
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_GLOBAL_SHARED_ACCOUNT" ) {
		my(@x)=qw(INV_Shared_Account_LU INV_Credential_Type_LU INV_LDAP_USER_LU INV_LDAP_GROUP_LU Both );
		push @outarray,\@x;

		my($report_query)="select credential_name, credential_type, ldap_user, ldap_group from INV_credential_notes
		where ( ldap_user is not null or ldap_group is not null )";

		#my($report_query)="select group_name, system_type,
		#case when credential_type = 'ldap user'  then credential_name else null end as 'LDAP User',
		#case when credential_type = 'ldap group' then credential_name else null end as 'LDAP Group'
	   #from INV_group_member where group_type='GLOBALSHARED' and system_name is null";

		print _debugcolor( $args1{-html},$report_query),"\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;

	# THREE FOR ONE DEAL - NONLDAP (INV_SEE_BATCHACCOUNTS), IS_ERROR=Y
	} elsif( $args{-screen_name} eq "INV_GROUP_SUMMARY" ){
		my(@rc)=("COLOR","HYPERLINK","Group","Status","Count");
		push @outarray,\@rc;
		my($oldsystem) = 'NOTPOSSIBLE';
		foreach my $v ( get_group_cert_summary(1) ) {
			if( $oldsystem ne ${$v}[2] ) {
				$oldsystem = ${$v}[2];
			 	my(@vv) = ('NEXTFORM','Group '.${$v}[2]);
			 	push @outarray,\@vv;
			}
			push @outarray,$v;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_DATABASE_SUMMARY" ){
		my(@rc)=("COLOR","HYPERLINK","System","Approval","Matched","Count");
		push @outarray,\@rc;
		my($oldsystem) = 'NOTPOSSIBLE';

		if( $args{-container} ) {
			push @outarray,get_credential_summary(1,undef," and APPROVALSTATUS = '".$args{-container}."'");
			return \@outarray;
		} else {
			foreach my $v ( get_credential_summary(1) ) {
				if( $oldsystem ne ${$v}[2] ) {
					$oldsystem = ${$v}[2];
				 	my(@vv) = ('NEXTFORM','Server '.${$v}[2]);
				 	push @outarray,\@vv;
				}
				push @outarray,$v;
			}
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_CREDENTIAL_MAPPING" ) {
		# return "This Report Requires Selection of a Database" unless $args{-system} ;
		if( ! $args{-system} ) {	# no database was sent

			my(@rc)=("COLOR","HYPERLINK","System","Approval","Matched","Count");
			push @outarray,\@rc;
			my($oldsystem) = 'NOTPOSSIBLE';
			my($morewhere)=''; #="and APPROVALSTATUS = '".$args{-container}."'";

			if( $args{-container} eq "All Accounts" ) {

			} elsif( $args{-container} eq "Service Accounts" ) {
				return "Line ".__LINE__." Not Implemented";
			#	$argswhere = "and p.is_serviceaccount='Y'";
			} elsif( $args{-container} eq "Unmapped Accounts" ) {
				return "Line ".__LINE__." Not Implemented";
			#	$argswhere = "and   p.is_deleted='N' and is_unuseable='N' and is_error='N'	and   principal_type!='ldap user' and p.is_error='N' and p.is_serviceaccount='N'";
			} elsif( $args{-container} eq "Mapped Accounts" ) {
				$morewhere= "and APPROVALSTATUS != 'UNMAPPED'";
			} elsif( $args{-container} eq	"Unuseable Accounts" ) {
				return "Line ".__LINE__." Not Implemented";
			#	$argswhere = "and ( is_unuseable='Y' or is_deleted='Y')";
			} elsif( $args{-container} eq "Error Accounts" ) {
				return "Line ".__LINE__." Not Implemented";
			#	$argswhere = "and is_error='Y'";
			} elsif( $args{-container} eq "LDAP User" ) {
				return "Line ".__LINE__." Not Implemented";
			#	$argswhere = "and principal_type='ldap user'";
			} elsif( $args{-container} eq "LDAP Group" ) {
				return "Line ".__LINE__." Not Implemented";
			#	return "LDAP GROUP Unimplemented";
			} else {
				return "Line ".__LINE__." INV_CREDENTIAL_MAPPING Not Implemented without -container set";
			#	return "ERROR: Bad Account Status Passed - $args{-container}";
			}

			foreach my $v ( get_credential_summary(1,undef,$morewhere ) ) {
				push @outarray,$v;
			}

			return \@outarray;
		}
		#	my($showall_servers,$showserver,$showstate)=@_;
		return "This Report Requires Selection of a Account Status" unless $args{-container};

		my($argswhere)='';
		if( $args{-container} eq "All Accounts" ) {
		} elsif( $args{-container} eq "Service Accounts" ) {
			$argswhere = "and p.is_serviceaccount='Y'";
		} elsif( $args{-container} eq "Unmapped Accounts" ) {
			$argswhere = "and   p.is_deleted='N' and is_unuseable='N' and is_error='N'	and   principal_type!='ldap user' and p.is_error='N' and p.is_serviceaccount='N'";
		} elsif( $args{-container} eq "Mapped Accounts" ) {
			$argswhere = "and ( is_unuseable='Y' or is_error='Y' or principal_type='ldap user' or p.is_serviceaccount='Y' )";
		} elsif( $args{-container} eq	"Unuseable Accounts" ) {
			$argswhere = "and ( is_unuseable='Y' or is_deleted='Y')";
		} elsif( $args{-container} eq "Error Accounts" ) {
			$argswhere = "and is_error='Y'";
		} elsif( $args{-container} eq "LDAP User" ) {
			$argswhere = "and principal_type='ldap user'";
		} elsif( $args{-container} eq "LDAP Group" ) {
			return "LDAP GROUP Unimplemented";
		} else {
			return "ERROR: Bad Account Status Passed - $args{-container}";
		}
		if( $args{-container} =~ /Accounts$/ ) {
			print "<h2>$args{-container} Report For $args{-system}</h2>";
		}else{
			print "<h2>$args{-container} Credentials Report For $args{-system}</h2>";
		}

		my(@x);
		@x=qw(COLOR 	HYPERLINKBLANK 	Credential_Name 		Principal_Type
				Principal_Name Chkbox_Is_Unuseable_Login Chkbox_Is_Service_Login Chkbox_Is_Error Popup_INV_LDAP_USER_LU Txtbox20_Notes Sysadmin);
		push @outarray,\@x;

		my($equality)='=';
		my(@ss)=SetupTmpTable(%args);
		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN>".join(" ",@ss)."</FONT>}".$NL if is_developer();

		my($report_query)="
select distinct
      case
            when is_error='Y'    then 'PINK'
            when ( is_unuseable='Y' or is_deleted='Y' )    then 'YELLOW'
            when ( is_operrole = 'Y' or is_sysadmin = 'Y' or is_securityadmin = 'Y' or is_operator='Y' )
                then 'LIGHTGREEN'
            when principal_type != 'ldap user' then 'LIGHTBLUE'
            else ''
         end as COLOR,
      case when principal_type != 'ldap user' and is_deleted='N' and principal_type != 'app user'
                then '?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT&SHOWUSER='+credential_name+'&SHOWSYSTYP='+p.credential_type
           when principal_type = 'app user'
                then '?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT&SHOWUSER='+principal_name+'&SHOWSYSTYP=app%20user'
           else ''
      end as HYPERLINKBLANK,
    p.credential_name,   p.principal_type,
    principal_name, is_unuseable,
    is_serviceaccount, is_error,
    case when principal_type='ldap user' then principal_name
         else '[Unmapped]'
         end as MappedLdapGroup,
    user_notes,
    case when ( is_operrole = 'Y' or is_sysadmin = 'Y' or is_securityadmin = 'Y' or is_operator='Y' )
           then 'YES'   -- Sysadmin
           else 'NO'
           end as IS_SYSADMIN
from INV_principal_map p ,#ServerSubset s2
where p.system_name=s2.system_name
and 	p.system_type in ('sqlsvr','oracle','sybase')
and   p.system_type=s2.system_type 	$credentialwhere $argswhere ";


		print "<FONT COLOR=GREEN><PRE>".$report_query."</FONT></PRE>" if is_developer();
		# print "<PRE>", _debugcolor($args1{-html},$report_query) ,"</PRE>\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			#print join(" ",@x);
			push @outarray,\@x;
		}
#	use Data::Dumper;
#	print "<PRE>",Dumper \@outarray,"</PRE>";

		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_USER_ACCESS" ) {
		return "Must pass ldap user for the User Access Report" unless $args{-ldap_user};
		print "<H1>Access Report For ".$args{-ldap_user}."</H1>";
		my($report_query)="
select
		system_name, system_type, credential_name,
		display_name, is_deleted,
		case when ( is_operrole = 'Y' or is_sysadmin = 'Y' or is_securityadmin = 'Y' or is_operator='Y' )
            then 'Y'   -- Sysadmin
            else 'N'
      end as IS_SYSADMIN,
		is_readonly, is_unuseable, is_serviceaccount, is_error
from 	 INV_principal_map d
where	 principal_name='".$args{-ldap_user}."'
order by system_name, system_type, display_name";


		# my(@x)=qw(COLOR HYPERLINK System Credential Name Status  );
		my(@x)=qw(System Type Credential Name Del Sa Ro Unuse Svc Err );
		push @outarray,\@x;
		push @outarray,_querywithresults($report_query,1);
		return \@outarray;
#	} elsif( $args{-screen_name} eq "INV_SEE_UNMAPPED"
#		or $args{-screen_name} eq "INV_SEE_MAPPED"
#		or $args{-screen_name} eq "INV_SEE_BATCHACCOUNTS"
#		or $args{-screen_name} eq "INV_SEE_UNUSEABLE"
#		or $args{-screen_name} eq "INV_SEE_ALL" ) {
#
#		if(  ! $args{-production} and ! $args{-systype} and ! $args{-container} and  ! $args{-system}
#		and  ! $args{-likefilter}  and ! $args{-credentialfilter}  ) {
#			# return "This Report Requires Selection of One or More of the Filters";
#			my(@rc)=("COLOR","HYPERLINK","System","Approval","Count");
#			push @outarray,\@rc;
#			my($oldsystem) = 'NOTPOSSIBLE';
#			foreach my $v ( get_credential_summary() ) {
#				if( $oldsystem ne ${$v}[2] ) {
#					$oldsystem = ${$v}[2];
#				 	my(@vv) = ('NEXTFORM','Server '.${$v}[2]);
#				 	push @outarray,\@vv;
#				}
#				push @outarray,$v;
#			}
#			return \@outarray;
#		}
#
#		my(@x);
#		@x=qw(COLOR HYPERLINKBLANK Account_Name Credential_Type System Mapped_To Mapped_Type Chkbox_UNUSEABLE_Login Chkbox_Service_Login Chkbox_Operator_Login Chkbox_LDAP_Group Chkbox_LDAP_User Chkbox_Shared_Login Chkbox_Is_Error Txtbox40_Notes);
#		push @outarray,\@x;
#
#		my($equality)='=';
#		my(@ss)=SetupTmpTable(%args);
#		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN><PRE>".
#			join(" ",@ss).
#			"</PRE></FONT>}".$NL if $args{-debug};
#		my($report_query)="
#select distinct
#			case 	when is_error='Y' 	then 'PINK'
#				when (is_unuseable='Y' or is_deleted='Y') 	then 'YELLOW'
#				when ( is_serviceaccount='Y' or is_operator='Y' ) then 'LIGHTGREEN'
#				when principal_type != 'ldap user' then 'LIGHTBLUE'
#				else ''
#			end as COLOR,
#		case when principal_type != 'ldap user' and is_deleted='N' and principal_type != 'app user'
#  	   	 	 then '?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT&SHOWUSER='+credential_name+'&SHOWSYSTYP='+p.credential_type
#			  when principal_type = 'app user'
#  	   	 	 then '?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT&SHOWUSER='+principal_name+'&SHOWSYSTYP=app%20user'
#			  else ''
#		end as HYPERLINKBLANK,
#	p.credential_name,p.credential_type,p.system_name,principal_notes,
#	is_deleted, is_serviceaccount, is_operator,
#	is_ldap_group,is_ldap_user,is_shared,
#	principal_notes,is_error,principal_name,principal_type
#from INV_principal_map p ,#ServerSubset s2
#where p.system_name=s2.system_name
#and   p.system_type=s2.system_type 	$credentialwhere
#";
#
#if( $args{-screen_name} eq "INV_SEE_UNMAPPED" ) {		# Unmapped
#	$report_query .= "and ( p.is_deleted ='N' and p.is_unuseable='N'
#		and p.is_serviceaccount ='N' and p.is_operator ='N' and p.is_error ='N' )
#	and p.principal_type != 'ldap user'";
#} elsif( $args{-screen_name} eq "INV_SEE_MAPPED" ) {	# Mapped
#	$report_query.=" and p.principal_type = 'ldap user' "
#
#} elsif( $args{-screen_name} eq "INV_SEE_BATCHACCOUNTS" ) {
#	$report_query.=" and ( p.is_serviceaccount ='Y' or p.is_operator ='Y')";
#} elsif( $args{-screen_name} eq "INV_SEE_UNUSEABLE" ) {
#	$report_query.="  and ( p.is_deleted ='Y' or p.is_unuseable='Y' ) ";
#}
#
#		print "<PRE>", $report_query,"</PRE>\n" if $args{-debug};
#		my(%sysname,%crenotes,%flags);
#		my($i)=0;
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			my($color)=shift @x;
#			my($hyperlink)=shift @x;
#			last if $i>10;
#
#			my($rowkey)=$x[0]."~".$x[1];
#			$rowkey .=  "~".$x[12] if $x[13] eq "app user";
#
#		#	print 'ROW '.$x[0].'<br>';
#		#	next if $args{-SHOWMAPPED} eq 'N' and ($x[4] or $x[5] or $x[6] or $x[7] or $x[8] or $x[9]);
#			$sysname{$rowkey} .= $x[2]."<br>\n";
#			$crenotes{$rowkey} = $x[10] unless $x[10]=~/^\s*$/;
#			$crenotes{$rowkey} = $x[3] 	unless $crenotes{$rowkey} or $x[3]=~/^\s*$/;
#			$flags{$rowkey."~is_error"} 		= $x[11] || 'N';
#			$flags{$rowkey."~is_unuseable"} 	= $x[4] || 'N';
#			$flags{$rowkey."~is_serviceaccount"} 	= $x[5] || 'N';
#			$flags{$rowkey."~is_operator"} 	= $x[6] || 'N';
#			$flags{$rowkey."~is_ldap_group"} 	= $x[7] || 'N';
#			$flags{$rowkey."~is_ldap_user"} 	= $x[8] || 'N';
#			$flags{$rowkey."~is_shared"} 		= $x[9] || 'N';
#			$flags{$rowkey."~hyperlink"} 		= $hyperlink;
#			$flags{$rowkey."~color"} 			= $color;
#			$flags{$rowkey."~principal_name"} 		.= $x[12] ."<br>\n";
#			$flags{$rowkey."~principal_type"} 		.= $x[13] ."<br>\n";
#		}
#		foreach ( sort keys %sysname ) {
#			#my($k)=$_;
#			my(@x);
#			$x[14] = $crenotes{$_} || '[Enter Credential Notes]';
#			$x[4] = $sysname{$_};
#			$x[4] =~ s/<br>\n$//;
#
#			$x[5] =  $flags{$_."~principal_name"};
#			$x[5] =~ s/<br>\n$//;
#			$x[6] = $flags{$_."~principal_type"};
#			$x[6] =~ s/<br>\n$//;
#
#			$x[7] = $flags{$_."~is_unuseable"};
#			$x[8] =$flags{$_."~is_serviceaccount"};
#			$x[9] =$flags{$_."~is_operator"};
#			$x[10] =$flags{$_."~is_ldap_group"};
#			$x[11] =$flags{$_."~is_ldap_user"};
#			$x[12] =$flags{$_."~is_shared"};
#			$x[13] =$flags{$_."~is_error"};
#
#			($x[2],$x[3])=split(/~/,$_);
#			$x[1] = $flags{$_."~hyperlink"} ;
#			$x[0] = $flags{$_."~color"} ;
#			push @outarray, \@x;
#		}
#		return \@outarray;
#	} elsif( $args{-screen_name} eq "INV_PRINCIPAL_MAP"   ) {
#
#		if(  ! $args{-production} and ! $args{-systype} and ! $args{-container} and  ! $args{-system}
#		and  ! $args{-likefilter}  and ! $args{-credentialfilter}  ) {
#			# return "This Report Requires Selection of One or More of the Filters";
#
#			#my(@rc)=("COLOR","HYPERLINKBLANK","System","Approval","Credential Type","Count");
#			#push @outarray,\@rc;
#			#push @outarray,get_credential_summary();
#			#return \@outarray;
#
#			my(@rc)=("COLOR","HYPERLINK","System","Approval","Count");
#			push @outarray,\@rc;
#			my($oldsystem) = 'NOTPOSSIBLE';
#			foreach my $v ( get_credential_summary() ) {
#				if( $oldsystem ne ${$v}[2] ) {
#					$oldsystem = ${$v}[2];
#				 	my(@vv) = ('NEXTFORM','Server '.${$v}[2]);
#				 	push @outarray,\@vv;
#				}
#				push @outarray,$v;
#			}
#			return \@outarray;
#		}
#
#		my(@ss)=SetupTmpTable(%args);
#		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN><PRE>".
#			join(" ",@ss).
#			"</PRE></FONT>}".$NL if $args{-debug};
#		my(@x)=qw(COLOR HYPERLINKBLANK System_Name System_Type Credential_Name Credential_Type Principal_Name Principal_Type Is_Deleted  );
#		push @outarray,\@x;
#		my($report_query)="
#		select
#			case 	when is_error='Y' 	then 'PINK'
#				when (is_unuseable='Y' or is_deleted='Y') 	then 'YELLOW'
#				when ( is_serviceaccount='Y' or is_operator='Y' ) then 'LIGHTGREEN'
#				when principal_type != 'ldap user' then 'LIGHTBLUE'
#				else ''
#			end as COLOR,
#		case when principal_type != 'ldap user' and is_deleted='N' and principal_type != 'app user'
#  	   	 	 then '?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT&SHOWUSER='+credential_name+'&SHOWSYSTYP='+p.credential_type
#			  when principal_type = 'app user'
#  	   	 	 then '?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT&SHOWUSER='+principal_name+'&SHOWSYSTYP=app%20user'
#			  when credential_name!=principal_name and principal_type='ldap user'
#  	   	 	 then '?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT&SHOWUSER='+principal_name+'&SHOWSYSTYP='+principal_type+'&ShowAll=Show%20All%20Groups/Users'
#			  else ''
#		end as HYPERLINKBLANK,
#
#		p.system_name,p.system_type,credential_name=substring(credential_name,1,30),credential_type,
#		principal_name,
#		principal_type,is_deleted
#	from 	INV_principal_map p, #ServerSubset s
#	where s.system_name=p.system_name
#	$credentialwhere
#	and   s.system_type=p.system_type
#	order by p.system_name,p.system_type,credential_name,credential_type";
#
#		print "<PRE>",$report_query,"</PRE>\n" if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			push @outarray, \@x;
#		}
#		return \@outarray;
#	} elsif( $args{-screen_name} eq "INV_SEE_ERROR" ) {
#		my(@ss)=SetupTmpTable(%args);
#		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN><PRE>".
#			join(" ",@ss).
#			"</PRE></FONT>}".$NL if $args{-debug};
#
#		my(@x)=qw(COLOR System_Name System_Type Credential_Name Credential_Type Principal_Name Principal_Type Is_Deleted Deleted_Reason Description );
#		push @outarray,\@x;
#
#		my($report_query)="
#	select distinct
#		case 	when is_error='Y' 	then 'PINK'
#				when (is_unuseable='Y' or is_deleted='Y') 	then 'YELLOW'
#				when ( is_serviceaccount='Y' or is_operator='Y' ) then 'LIGHTGREEN'
#				when principal_type != 'ldap user' then 'LIGHTBLUE'
#				else ''
#			end as COLOR,
#	p.system_name,p.system_type,p.credential_name,p.credential_type,
#	principal_name,principal_type,is_deleted,delete_reason,credential_notes
#	from 	INV_principal_map p, #ServerSubset s
#	where s.system_name=p.system_name
#	and   s.system_type=p.system_type 	$credentialwhere
#	-- and   n.credential_name=p.credential_name
#	-- and   n.credential_type=p.credential_type
#	and   p.is_error='Y'
#	-- and   (p.is_error='Y' or n.is_error = 'Y' or p.is_deleted='Y' or p.is_unuseable='Y' )
#	order by p.system_name,p.system_type,credential_name,credential_type";
#
#		print "<PRE>",$report_query,"</PRE>\n" if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			push @outarray, \@x;
#		}
#		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_LDAP_LOOKUP" ) {
		#foreach ( keys %args ) { print $_, " => ",$args{$_},"<br>" unless $_ eq "-NL"; }

		if( $args{-ldap_user} and $args{-ldap_group} ) {
			return "ERROR: can not pass both ldap user and ldap group";
		}

		if( ! $args{-ldap_user} and ! $args{-ldap_group} ) {
			return "You must pass either ldap user or ldap group";
		}

		my(@x)=qw(NAME KEY VALUE);
		push @outarray,\@x;
		my($report_query);
		if( $args{-ldap_user} ) {
			$report_query="select credential_name, attribute_key, attribute_value from INV_credential_attribute where credential_type='ldap user' and credential_name='".$args{-ldap_user}."' and attribute_value!='' ";
		}
		if( $args{-ldap_group} ) {
			$report_query="select credential_name, attribute_key, attribute_value from INV_credential_attribute where credential_type='ldap group' and credential_name='".$args{-ldap_group}."' and attribute_value!='' ";
		}

		print "<PRE>",$report_query,"</PRE>\n" if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}

		if( $args{-ldap_user} ) {
			my(@xxx)=('&nbsp;',' ',' ');
			push @outarray, \@xxx;
			$report_query="
select m.group_type,m.group_name
from INV_group_member m, INV_credential c
where m.credential_type='ldap user' and m.credential_name='".$args{-ldap_user}."'
and   c.credential_name = m.group_name
and   c.credential_type = m.group_type
and   display_name is not null
order by group_name";

			print "<PRE>",$report_query,"</PRE>\n" if $args{-debug};
			foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
				my(@x) = dbi_decode_row($_);
				unshift @x,"MemberOf";
				push @outarray, \@x;
			}
		}

		if( $args{-ldap_group} ) {
			my(@xxx)=('&nbsp;',' ',' ');
			push @outarray, \@xxx;

			$report_query="select 'SamId='+g.credential_name,' Name='+display_name
from INV_group_member g, INV_credential c
where group_type='ldap group'
and   c.credential_name = g.credential_name
and   c.credential_type = g.credential_type
and   group_name='".$args{-ldap_group}."'
order by display_name";

			print "<PRE>",$report_query,"</PRE>\n" if $args{-debug};
			foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
				my(@x) = dbi_decode_row($_);
				unshift @x,"Member";
				push @outarray, \@x;
			}
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_ALERTABLE_E_RPT" or $args{-screen_name} eq "INV_ALERTABLE_H_RPT"  ) {
		#$args{-debug} = 1;
		my($report_query);
		if( $args{-screen_name} eq "INV_ALERTABLE_E_RPT" ) {
			$report_query="get_possible_alarms 'E'";
		} else {
			$report_query="get_possible_alarms 'H'";
		}

   #	COLOR
   	print "<FONT COLOR=BLUE><TABLE BORDER=1><TR><TH>DBG (FINAL QUERY)</TH></TR>
			<TR><TD><FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT></TD></TR></TABLE></FONT>".$NL if is_developer();

		my(@rc)=dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 );
		my(@x)=qw( Deleted_Reason MonitorProgram System Subsystem state message_text container monitor_time reviewed_by reviewed_until);
		push @outarray,\@x;

		foreach ( @rc ) {
			my(@x) = dbi_decode_row($_);
			print join(" ",@x) if $#x==0;
			push @outarray, \@x;
		}
		return \@outarray;

	} elsif( $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT"  ) {
		# $args{-debug}=1;
		my($whereclause);
		if( $args{-severity} eq "Errors" ) {
			$whereclause=" where status_value in ('FAILED','NEVER_RUN') ";
		}
		my($report_query)=
		"select
         case
            when status_value='OK'       then 'BEIGE'
            when status_value='RUNNING'  then 'LIGHTGREEN'
            when status_value='FAILED'   then 'PINK'
            else 'RED'
         end as COLOR,
 agent_type      	,
 monitor_program 	,
 subkey            ,
 status_value      ,
 last_start_time   ,
 last_end_time     ,
 minutes=datediff( mi, last_start_time,last_end_time),
 last_exit_code    ,
 last_exit_notes
 from  INV_batchjob_definition
 $whereclause
 order by agent_type,monitor_program,subkey
         ";

		print "DBG (FINAL QUERY): {<FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT>}".$NL  if is_developer();;
		my(@rc)=dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 );
		my(@x)=qw(COLOR Agent_Type Program Key Status Last_Start Last_End Min ExitCode Exit_Notes);
		push @outarray,\@x;

		foreach ( @rc ) {
			my(@x) = dbi_decode_row($_);
			print join(" ",@x) if $#x==0;
			push @outarray, \@x;
		}
		return \@outarray;

	} elsif( $args{-screen_name} eq "INV_HEARTBEAT_RPT"
		or    $args{-screen_name} eq "INV_CONSOLE_RPT"
		or    $args{-screen_name} eq "INV_BACKUP_RPT"		) {
		my(@ss)=SetupTmpTable(%args);
		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN>".join(" ",@ss)."</FONT>}".$NL
			if $args{-debug};
		my($max_row_age_secs)=60*60;
		my($timewhere);

		my($whereclause);
		if( $args{-screen_name} eq "INV_BACKUP_RPT" ) {
$whereclause .= " and ( subsystem like 'Job:%'
or lower(monitor_program) like '%bkp%'
or lower(monitor_program) like '%backup%'
or lower(monitor_program) like '%logship%'
or lower(monitor_program) like '%tran%' )";
		} elsif( $args{-screen_name} eq "INV_CONSOLE_RPT" ) {
$whereclause .= "
and monitor_program in ('SybaseDbDebug','cisco_logmon.pl','ConsoleUnixNightly',
'ConsoleUnixWeekly','ConsoleWin32Nightly','Console_Daily','ConsoleDaily',
'ConsoleWin32Weekly','MssqlDailyCheck','SybDailyCheck','Win32DRChecker',
'UnixDRChecker','LoadAllTranlogs','MssqlBackupCrosscheck')
";
		} else {			# INV_HEARTBEAT_RPT			
$whereclause .= " and subsystem not like 'Job:%'
and lower(monitor_program) not like '%bkp%'
and lower(monitor_program) not like '%backup%'
and lower(monitor_program) not like '%logship%'
and lower(monitor_program) not like '%tran%' ";

$whereclause .= "
and monitor_program not in ('SybaseDbDebug','cisco_logmon.pl','ConsoleUnixNightly',
'ConsoleUnixWeekly','ConsoleWin32Nightly','Console_Daily','ConsoleDaily',
'ConsoleWin32Weekly','MssqlDailyCheck','SybDailyCheck','Win32DRChecker',
'UnixDRChecker','LoadAllTranlogs','MssqlBackupCrosscheck')
";
		}

		if( $args{-program} ) {
			$whereclause .= " and monitor_program='".$args{-program}."'";
		}

	#	$whereclause .= " and internal_state!='D'\n" unless $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT";
	#	$whereclause .= " and batchjob = 'AGENT'\n"		if $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT";
	#	$whereclause .= " and batchjob is null \n" 		if $args{-screen_name}  eq "INV_HEARTBEAT_RPT";

		#if( defined $args{-severity} and $args{-severity} ne "All" ) {
		if( $args{-severity} ne "All" ) {
			if( $args{-severity} =~ /Warning/i ) {
				$whereclause .= " and ( state in ('RUNNING','WARNING','ERROR','ALERT','CRITICAL','EMERGENCY')";
				$whereclause .= " or datediff(hh,last_ok_time,getdate())>4"
					if $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT";# or $args{-screen_name} =~ /Heartbeats/i;
				$whereclause .= " )\n"
			} elsif( $args{-severity} =~ /Emergency/i ) {
				$whereclause .= " and ( state = 'EMERGENCY'";
				$whereclause .= " or datediff(hh,last_ok_time,getdate())>4" if $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT";
				$whereclause .= " )\n"
			} elsif( $args{-severity} =~ /Error/i ) {
				$whereclause .= " and ( state in ('ERROR','ALERT','CRITICAL','EMERGENCY')";
				$whereclause .= " or datediff(hh,last_ok_time,getdate())>4"
					if $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT";# or $args{-screen_name} =~ /Heartbeats/i;
				$whereclause .= " )\n"
			} elsif( $args{-severity} =~ /Fatal/i ) {
				$whereclause .= " and ( state in ('ALERT','CRITICAL','EMERGENCY')";
				$whereclause .= " or datediff(hh,last_ok_time,getdate())>4"
					if $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT";# or $args{-screen_name} =~ /Heartbeats/i;
				$whereclause .= " )\n"
			} elsif( $args{-severity} =~ /Error/i ) {
				$whereclause .= " and ( state in ('ERROR','ALERT','CRITICAL','EMERGENCY')";
				$whereclause .= " or datediff(hh,last_ok_time,getdate())>4"
					if $args{-screen_name} eq "INV_GEM_BATCH_JOB_RPT";# or $args{-screen_name} =~ /Heartbeats/i;
				$whereclause .= " )\n"
			} elsif( ! defined $args{-severity} )  {
				die "OOPS  severity is undefined???\n";
			} else {
				die "OOPS  what kind of severity is $args{-severity} ???\n";
			}
		}

		my($report_query)=
		"select distinct
         case
            when datediff( hh,monitor_time,getdate())>24   then 'LIGHTBLUE'
            when state='ERROR'       then 'PINK'
            when state='WARNING'    then 'YELLOW'
            when state='ALERT'    then 'PINK'
            when state='COMPLETED'    then 'LIGHTGREEN'
            when state='OK'    then 'LIGHTGREEN'
            when state='CRITICAL'    then 'ORANGE'
            when state='RUNNING'    then 'YELLOW'
            when state='STARTED'    then 'YELLOW'
            else ''
         end as COLOR,
         system+'<BR>'+state, monitor_program+'<BR>'+subsystem,
         convert(varchar(5), monitor_time, 8),
         convert(varchar(5), last_ok_time, 8),
         message_text
      from Heartbeat e, #ServerSubset s
      where e.system=s.system_name
      and   ( reviewed_time is null or reviewed_until < getdate() )
      $timewhere
      $whereclause
      order by system,subsystem ";

		print "DBG (FINAL QUERY): {<FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT>}".$NL  if is_developer();;
		my(@rc)=dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 );
		my(@x)=qw(COLOR System<BR>State Program<BR>Subsystem LastTime LastOk Text OkDelete);
		push @outarray,\@x;

		foreach ( @rc ) {
			my(@x) = dbi_decode_row($_);
			if( $#x==0 ) {
				print join(" ",@x);
				last;
			}
			push @outarray, \@x;
		}

		# SECOND QUERY - NON SYSTEMS! - but only if nothing defined in the dropdowns!
		if( ! $args{-container}
		# and ! $args{-program}
		and ! $args{-system}
		and ! $args{-production}
		and ! $args{-systype}
		and ! $args{-likefilter} ) {

			my($report_query)=
			"select distinct
	         case
	            when datediff( hh,monitor_time,getdate())>24   then 'LIGHTBLUE'
	            when state='ERROR'       then 'PINK'
	            when state='WARNING'    then 'YELLOW'
	            when state='ALERT'    then 'PINK'
	            when state='COMPLETED'    then 'LIGHTGREEN'
	            when state='OK'    then 'LIGHTGREEN'
	            when state='CRITICAL'    then 'ORANGE'
	            when state='RUNNING'    then 'YELLOW'
	            when state='STARTED'    then 'YELLOW'
	            else ''
	         end as COLOR,
	         system+'<BR>'+state, monitor_program+'<BR>'+subsystem,
	         convert(varchar(5), monitor_time, 8),
	         convert(varchar(5), last_ok_time, 8),
	         message_text
	      from Heartbeat e
	      where e.system not in ( select system_name from INV_system )
	      and   ( reviewed_time is null or reviewed_until < getdate() )
	      $timewhere
	      $whereclause
	      order by system,subsystem ";

			print "DBG (FINAL QUERY PART 2): {<FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT>}".$NL  if is_developer();
			my(@rc)=dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 );
			foreach ( @rc ) {
				my(@x) = dbi_decode_row($_);
				print join(" ",@x) if $#x==0;
				push @outarray, \@x;
			}
		}

		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_EVENT_RPT" ) {
		# foreach ( sort keys %args1 ) { print __LINE__, " ".$_." ".$args1{$_}.$args1{-NL}."\n" unless /-NL/; }
		#$args{-debug}=1;
		my(@ss)=SetupTmpTable(%args);
		print "DBG (SERVER SUBSET): {<FONT COLOR=GREEN>".join(" ",@ss)."</FONT>}".$NL if $args{-debug};

		my($max_row_age_secs)=60*60;
		my($timewhere);
		$args{-maxtimehours}="1 Day" unless $args{-maxtimehours};
		if( $args{-maxtimehours} =~ /Hours/ ) {
			$args{-maxtimehours} =~ s/\s+Hours\s*//;
			$max_row_age_secs *= $args{-maxtimehours};
		} elsif(  $args{-maxtimehours} =~ /Since/ ) {
			$args{-maxtimehours} =~ s/Days*//;
			my(@t)=localtime();
			#print "HOURS=$t[2] MINUTES=$t[1] <br>";
			if($t[2] > 16) {
				$t[2]-=16;
			} else {
				$t[2]+=8;
			}
			$max_row_age_secs *= $t[2];
			$max_row_age_secs += $t[1]*60;
		} elsif(  $args{-maxtimehours} =~ /Days*/ ) {
			$args{-maxtimehours} =~ s/\s*Days*\s*//;
			$max_row_age_secs *= $args{-maxtimehours} * 24;
		}
		$timewhere = " and event_time>\'".do_time(-time=>(time - $max_row_age_secs), -fmt=>"mm/dd/yyyy hh:mi:ss")."\'";

		my($severitywhere)='';
		if( $args{-severity} ){
			$args{-severity} =~ s/\s//g;
			if( $args{-severity} eq "Emergency" ) {
				$severitywhere = " and severity in ( 'EMERGENCY') ";
			} elsif( $args{-severity} eq "Fatals" ) {
				$severitywhere = " and severity in ('CRITICAL','EMERGENCY') ";
			} elsif( $args{-severity} eq "Errors" ) {
				$severitywhere = " and severity in ('ERROR','ALERT','CRITICAL','EMERGENCY') ";
			} elsif( $args{-severity} eq "Warnings" ) {
				$severitywhere = " and severity in ('WARNING','ERROR','ALERT','CRITICAL','EMERGENCY') ";
			} elsif( $args{-severity} eq "All" ) {
			} else {
				print "ERROR SEVERITY $args{-severity} NOT FOUND";
			}
		}

		$severitywhere.="\n and monitor_program='".$args{-program}."' \n" if $args{-program};

# handle -severity
		my($report_query)=
		"select distinct
			case
				when severity='ERROR' 		then 'PINK'
				when severity='WARNING' 	then 'YELLOW'
				when severity='INFORMATION' 	then 'PINK'
				when severity='EMERGENCY' 	then 'ORANGE'
				when severity='CRITICAL' 	then 'ORANGE'
				when severity='RUNNING' 	then 'YELLOW'
				when severity='STARTED' 	then 'YELLOW'
				else ''
			end as COLOR,
		system+'<br>'+severity, monitor_program+'<br>'+subsystem, monitor_time, message_text
		from Event e, #ServerSubset s
		where e.system=s.system_name
		-- and   e.system_type=s.system_type
		and	reviewed_time is null
		$timewhere
		$severitywhere
		order by event_time desc";

		my(@x)=qw( COLOR System Source Event_Time Text );
		push @outarray,\@x;
		print "DBG (FINAL QUERY): {<FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT>}".$NL  if is_developer();
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			# print join("|",@x),"<br>";
			print join(" ",@x) if $#x==0;
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "INV_DEPARTMENT_ACCESS"
		or $args{-screen_name} eq "INV_DEPARTMENT_ACCESS_FULL" ) {
		return "ERROR: must pass a Ldap Group to the Department Access Report" unless $args{-ldap_group};
		print "<H1>Group Certification For ".$args{-ldap_group}."</H1>";
		my(@systems_of_interest)=qw(PLATINUM IMAGSYB1 ADSRV159 MXPRODDB MXDRDB SSDB);
		my(@hdr)=qw( User Radio_Approval_Status );
		@hdr=qw(User Department Approval) if $args{-screen_name} eq "INV_DEPARTMENT_ACCESS_FULL";
		push @hdr, @systems_of_interest;
		push @outarray,\@hdr;

		my($otherwhere,$equality)=('','');
		if( $args{-OTHER_ARG} eq "Removed Accounts" ) {
			$otherwhere="and d.status='Remove'";
			$equality  = "=";
		} elsif( $args{-OTHER_ARG} eq "Approved Accounts" ) {
			$otherwhere="and d.status='Approve'";
			$equality  = "=";
		} elsif( $args{-OTHER_ARG} eq 'Unknown Status') {
			$otherwhere=
"-- get the list of approved and removed dudes
 and   p.display_name not in ( select display_name from INV_cert_department where department='".
 $args{-ldap_group}.
 "' and status in ('Approve','Remove') )   ";
			$equality  = "*=";
		} else {
		#} elsif( $args{-OTHER_ARG} eq 'All Accounts') {
			$otherwhere="";
			$equality  = "*=";
		#} else {
		#	return "ERROR: Status=$args{-OTHER_ARG}";
		}

		my($addfield)= "	 , department = ( select attribute_value from INV_credential_attribute a
            where p.principal_name = a.credential_name
            and   p.principal_type = 'ldap user'
            and   a.attribute_key='dept' )  " if $args{-screen_name} eq "INV_DEPARTMENT_ACCESS_FULL";

		# -- is_ok_admin=( select 1 from INV_systemuser where permission like '%ADMIN' and samid=p.principal_name )

		my($report_query)="
   select distinct credential_name
      into \#MEMBERS
      from INV_group_member
      where group_name="._quote($args{-ldap_group})."
      and   credential_type='ldap user'

  select distinct
      case  when is_error='Y'                  then '<FONT COLOR=RED>INVESTIGATE</FONT>'       -- TO Investigate
            when ( is_operrole = 'Y' or is_sysadmin = 'Y' or is_securityadmin = 'Y' or is_operator='Y' )
                                               then 'ADMIN'       -- Sysadmin
            else 'YES'
         end as STATUS,
      system_name,
      -- p.credential_name,
      -- p.principal_name,
      p.display_name,
      d.status
      $addfield
   from   INV_principal_map p, \#MEMBERS m, INV_cert_department d
   where p.principal_name = m.credential_name
   and   p.principal_type= 'ldap user'
   and   p.display_name $equality d.display_name
   and   d.department ="._quote($args{-ldap_group})."
   and   p.system_name in ('".join("','",@systems_of_interest)."')
   and   is_unuseable!='Y'
   $otherwhere
   and 	is_deleted!='Y' and system_type in ('oracle','sqlsvr','sybase')
   and   p.display_name is not null
   order by p.display_name, p.system_name, system_type";

		print "<FONT COLOR=BLUE><TABLE BORDER=1><TR><TH>DBG (FINAL QUERY)</TH></TR>
			<TR><TD><FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT></TD></TR></TABLE></FONT>".$NL  if is_developer();
		my( %SYSTEM_DISP_STATUS, %DISPLAYNAMES, %APPSTATUS, %DEPT );
		foreach ( _querywithresults($report_query, 1) ) {
			my($status,$system_name,$display_name,$appstatus,$dept) = dbi_decode_row($_);
			$display_name=~s/\(.*$//;
			$DISPLAYNAMES{$display_name} = 1;
			$APPSTATUS{$display_name} = $appstatus || "Unknown";
			$SYSTEM_DISP_STATUS{$display_name."~".$system_name} = $status;
			$DEPT{$display_name} = $dept;
		}
		foreach my $display_name ( sort keys %DISPLAYNAMES ) {
			my(@x)=($display_name);
			push @x, $DEPT{$display_name} if $args{-screen_name} eq "INV_DEPARTMENT_ACCESS_FULL";
			push @x,$APPSTATUS{$display_name};
			foreach (@systems_of_interest) {
				push @x, $SYSTEM_DISP_STATUS{$display_name."~".$_} || "&nbsp;";
			}
			push @outarray,\@x;
		}

		#use Data::Dumper;
		#print "<PRE>",Dumper \@outarray,"</PRE>";

		return \@outarray;

	} elsif( $args{-screen_name} eq "INV_DATABASE_ACCESS"
		or    $args{-screen_name} eq "INV_DATABASE_ACCESS_RPT"
		or    $args{-screen_name} eq "INV_SYSTEM_ACCESS_RPT"  ) {

		my(@hdr)=qw( COLOR Credential User Department Radio_Approval_Status Txtbox30_Notes Approved_By );
		@hdr=qw( COLOR Credential User Department Status Notes ) if $args{-screen_name} =~ /\_RPT$/;
		push @outarray,\@hdr;

		my($sys)=$args{-system};
		return "ERROR: NO SYSTEM PASSED" unless $sys;

		print "<H1>",$args{-OTHER_ARG}." ".$args{-credentialfilter}." Accounts on Database ".$args{-system}."</H1>";
		my($where)='';
		$where.=" and APPROVALSTATUS = '".$args{-OTHER_ARG}."'"
			if $args{-OTHER_ARG} and  $args{-OTHER_ARG} ne "ALL";
		$where.=" and MATCHSTATUS = '".$args{-matchstatus}."'"
			if $args{-matchstatus} and $args{-matchstatus} ne "ALL";

		#print "XXX HERE WE ARE";
		if( $args{-OTHER_ARG2} and $args{-OTHER_ARG2} ne "All" ) {
			if( $args{-OTHER_ARG2} eq "Other" ) {
				my(@rc)=MlpGetDepartment();
				foreach (@rc) {
					$where .= " and department not like '".$_."%'";
				}
			} else {
				$where .= " and department like '".$args{-OTHER_ARG2}."%'";
			}
		}

	   my($report_query)="select APPROVALCOLOR,
	   	system_type,credential_name,principal_name,display_name,
	      APPROVALSTATUS2 as STATUS,
	   	comment,	approved_by, convert(varchar,approved_time,107), 	department
	   from INV_cert_credential_view
	   where system_name='".$sys."' and system_type in ('sqlsvr','oracle','sybase')
	   -- and is_unuseable!='Y' and is_deleted!='Y'
	   $where
	   order by system_type,APPROVALCOLOR,department,display_name";

		print "DBG (FINAL QUERY): {<FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT>}".$NL  if is_developer();
		my($sv_color)='NOT POSSIBLE';
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my($color,$system_type,$cred,$princ,$display,$nus,$nuc,$nab,$nat,$dept) = dbi_decode_row($_);
			$nus='Unknown' unless $nus;
			# print "Row = ST=$system_type PR=$princ DEPT=$dept<br>\n";
			if( $args{-OTHER_ARG} eq "Removed Accounts" ) {
				next if $nus ne "REMOVE";
			} elsif( $args{-OTHER_ARG} eq "Approved Accounts" ) {
				next if $nus ne "APPROVED";
			} elsif( $args{-OTHER_ARG} eq 'Unknown Status') {
				next if $nus ne "UNAPPROVED" and $nus ne "UNMAPPED";
			}

			if(    $nus eq "APPROVED" ) {		$nus = 'Approve'; }
			elsif( $nus eq "REMOVE" ){ 		$nus = 'Remove'; }
			elsif( $nus eq "UNAPPROVED" ){ 	$nus = 'Unknown'; }
			elsif( $nus eq "UNMAPPED" ){ 		$nus = 'Unknown'; }
			$display=~s/\(.*$//;

			my(@z)=($color, $cred, ($display || $princ), $dept,  $nus, $nuc, $nab." ".$nat );
			pop @z if $args{-screen_name} =~ /\_RPT$/;
			if( $sv_color ne $color ) {
				my($subtable_title);
				$subtable_title='Regular Users';
				$subtable_title='Needs To Be Mapped'	 			if $color eq "LIGHTBLUE";
				$subtable_title="Unuseable, Locked, or Disabled" if $color eq "YELLOW";
				$subtable_title="IT Administrator" 					if $color eq "ORANGE";
				$subtable_title="SysAdmin Permission" 				if $color eq "LIGHTGREEN";
				$subtable_title="To Investigate" 					if $color eq "PINK";
				$subtable_title="Service Account"					if $color eq '#ffccff';

				if( $args{-screen_name}  =~ /\_RPT$/ ) {
					if( $system_type eq "win32servers" ) {
						$subtable_title = "Computer ".$sys."&nbsp;".$subtable_title;
					} else {
						$subtable_title = "Database ".$sys."&nbsp;".$subtable_title;
					}
				} else {
					if( $system_type eq "win32servers" ) {
						$subtable_title = "Computer ".$sys."&nbsp;&nbsp;CERTIFY&nbsp;&nbsp;".$subtable_title;
					} else {
						$subtable_title = "Database ".$sys."&nbsp;&nbsp;CERTIFY&nbsp;&nbsp;".$subtable_title;
					}
				}

				my(@v) = ('NEXTFORM',$subtable_title);
				push @outarray,\@v;
				$sv_color = $color;
			}
			push @outarray,\@z;
		}
		return \@outarray;

	#} elsif( defined $args{-screen_name} and ($args{-screen_name}=~/^INV/ or $args{-screen_name}=~/^GEM/)) {
	} elsif( defined $args{-screen_name} and $args{-screen_name}=~/^INV/ ) {
		print "<br><B>SCREEN $args{-screen_name} NOT FOUND</B> at line ".__LINE__." of ".__FILE__."<br>";
		foreach ( keys %args ) { print $_ . " => $args{$_} <br>" unless $_ eq "-NL"; }
		return undef;
	}

	my($container_name)=$args{-container} if defined $args{-container};

	if( ! defined $args{-system} and ! defined $args{-program} ) {
		if( ! defined $container_name or $container_name eq "" ) {
	   		print _warn($args{-html},"Line ",__LINE__," Error - No Container Passed".$NL);
	   		$! = _warn($args{-html},"Line ",__LINE__,"Error - No Container Passed".$NL);
	   		foreach (keys %args) { print "MlpRunScreen: $_ => $args{$_}<br>\n" unless $_ eq "-NL"; }
	   		return undef;
		}
	}

	if( ! defined $args{-maxtimehours}	or $args{-maxtimehours} eq "" ) {
   		print _warn($args{-html},"Error - No Time Passed".$NL);
   		$! = _warn($args{-html},"Error - No Time Passed".$NL);
   		return undef;
	}

#	if( ! defined $screen_defn	or $screen_defn eq ""
#   	or  ! defined $container_name 	or  $container_name eq "" ) {
#   		$! = _warn($args{-html},"Error - Bad Screen Definition ($screen_defn) or Container ($container_name)".$NL);
#   		return undef;
#	}

	my($screen_defn)=$args{-screen_name};
	print "DBG: Screen=$screen_defn Container=$container_name".$NL if $args{-debug};

	#################################
   #  BUILD THE QUERY              #
	#################################
	my($report_query)="select * from ";
	my($order_by)="";
	my($whereclause);

	if( defined $args{-production} and $args{-production} ne "0" ) {
		$whereclause .= " ,ProductionServers p";
	}

	if( defined $args{-system} ) {
		$whereclause.="\n where t.system='".$args{-system}."'";
	} elsif( defined $args{-program} ) {
		if( $args{-program} eq "BACKUPS") {
			$whereclause.="where ( lower(monitor_program) like '%bkp%'
									or lower(monitor_program) like '%backup%'
									or lower(monitor_program) like '%logship%'
									or lower(monitor_program) like '%tran%' )";

		} elsif( $args{-program} =~ /,/ ) {
			$whereclause.="\n where monitor_program in (";
			foreach ( split(/,/,$args{-program}) ) {
				$whereclause .= "'".$args{-program}."',";
			}
			$whereclause =~ s/,$/\)/;	# remove final ,
		} else {
			$whereclause.="\n where monitor_program='".$args{-program}."'";
		}
 	} elsif( $container_name eq "All" ) {
 		$whereclause.=" where\n 1=1 ";
	} else {
		$whereclause.=" ,Container_full f\n where f.name='".$container_name."' and ((t.system=f.element and f.type='s') or (f.type='p' and t.monitor_program=f.element))\n ";
	}

	$whereclause .= " and subsystem like 'Job:%'"
		if $args{-screen_name} eq "Backups";

	$whereclause .= " and subsystem = '".$args{-subsystem}."' " if $args{-subsystem};
	#print "DBG: Whereclause=>$whereclause".$NL if $args{-debug};

	#####################
	# Get Report Type Code and Max Age for rows
	#####################
	my($report_code)=MlpGetReportCode(-screen_name=>$args{-screen_name});
	my($max_row_age_secs)=60*60;
	$max_row_age_secs *= $args{-maxtimehours} if $args{-maxtimehours};

	#print "DBG: ReportTypeCode=$report_code MaxRowAge=$max_row_age_secs seconds.".$NL if $args{-debug};
	my($state_sev_col);
	if( $report_code eq "e" ) {
		if( ! defined $args{-showok} ) {
			#$whereclause.=" and ( reviewed_time is null or datediff(dd,reviewed_time,getdate())>=1 )\n ";
			$whereclause.=" and reviewed_time is null\n ";
		}
		$state_sev_col='severity';
		$order_by=" order by event_time desc";
		$report_query.= "Event t $whereclause and event_time>\'".do_time(-time=>(time - $max_row_age_secs), -fmt=>"mm/dd/yyyy hh:mi:ss")."\'";
	} elsif( $report_code eq "h" ) {
		if( ! defined $args{-showok} ) {
			#$whereclause.=" and ( reviewed_time is null or datediff(dd,reviewed_time,getdate())>=1 )\n ";
			$whereclause.=" and ( reviewed_time is null or reviewed_until < getdate() )\n ";
		}
		$state_sev_col='state';
			$report_query="select monitor_program,t.system,subsystem,state,
		monitor_time=datediff(ss,monitor_time,getdate()),
		lastok=datediff(ss,last_ok_time,getdate()),
		message_text,document_url,reviewed_by,reviewed_time,
		datediff(mi,reviewed_until,getdate()),
		reviewed_severity
	from Heartbeat t $whereclause";
			$report_query .= " and monitor_time>\'".do_time(-time=>(time - $max_row_age_secs), -fmt=>"mm/dd/yyyy hh:mi:ss")."\'" if defined $args{-useheartbeattime} and $args{-useheartbeattime}==1;
			$order_by=" order by t.system,subsystem";
	} elsif( $report_code eq "p" ) {
		die "DEPRICATED CODE SECTION - SEEK HELP\n";
#		$state_sev_col='state';
#		$order_by="";
#		if( ! defined $max_row_age_secs or $max_row_age_secs =~ /^\s*$/ ) {
#			$report_query.="Performance $whereclause";
#		} else {
#			$report_query.= "Performance t $whereclause and monitor_time>\'".do_time(-time=>(time - $max_row_age_secs), -fmt=>"mm/dd/yyyy hh:mi:ss")."\'";
#		}
	} else {
		$! = _warn($args{-html},"Invalid Source Table For $screen_defn - $report_code must be e,h,p".$NL);
		return undef;
	}

	if( ! defined $report_code ) {
		$! = _warn($args{-html}, "Report Internal errror.  No Report in ScreenDefn for $screen_defn" );
		return undef;
	}

	$report_query .= " and internal_state!='D'"
		unless $args{-screen_name} eq "Agents";
	$report_query .= " and batchjob is not null"
		if $args{-screen_name} =~ /Batch/i;
	#$report_query .= " and monitor_program = 'Monitor'"
	$report_query .= " and batchjob = 'AGENT'"
		if $args{-screen_name} eq "Agents";
	#$report_query .= " and batchjob is null and monitor_program != 'Monitor'"
	$report_query .= " and batchjob is null "
		if $args{-screen_name} =~ /eartbeat/i and defined $container_name;

	if( defined $args{-severity} and $args{-severity} ne "All" ) {
		# $report_query .= " and last_ok_time is not null and batchjob is null and monitor_program != 'Monitor'"
		if( $args{-severity} =~ /Warning/i ) {
			$report_query .= " and ( $state_sev_col in ('WARNING','ERROR','ALERT','CRITICAL','EMERGENCY')";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( $args{-severity} =~ /Emergency/i ) {
			$report_query .= " and ( $state_sev_col = 'EMERGENCY'";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( $args{-severity} =~ /Error/i ) {
			$report_query .= " and ( $state_sev_col in ('ERROR','ALERT','CRITICAL','EMERGENCY')";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( $args{-severity} =~ /Fatal/i ) {
			$report_query .= " and ( $state_sev_col in ('ALERT','CRITICAL','EMERGENCY')";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( ! defined $args{-severity} ) {
			die "OOPS  severity is undefined???\n";
		} else {
			die "OOPS  what kind of severity is $args{-severity} ???\n";
		}
	}
	if( defined $args{-production} and $args{-production} ne "0" ) {
		$report_query .= " and t.system = p.system and is_production=1";
	}

	$report_query.=$order_by;
	print "DBG (FINAL QUERY): {<FONT COLOR=GREEN><PRE>".$report_query."</PRE></FONT>}".$NL if is_developer();

	if( $report_code eq "h" ) {
		my(@rc)=dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 );
		my(@x)=qw(State System Subsystem Program LastTime LastOk Text OkDelete);
		push @outarray,\@x;
		foreach ( @rc ) {
			my($d_monitor_program,$d_system,$d_subsystem,$d_state,$d_monitor_time,
				$d_lastok,$d_message_text,$d_document_url,$d_reviewed_by,
				$d_reviewed_time,$d_reviewed_until,$d_reviewed_severity)=dbi_decode_row($_);

			$d_state="(OK)-".$d_state if $d_reviewed_by ne "" and $d_reviewed_until<0;
			# $d_state="(OK)-".$d_reviewed_until if $d_reviewed_by ne "";
			# $d_monitor_time=do_diff($d_monitor_time);
			if( $d_monitor_time  =~ /^\s*$/ ) {
				$d_monitor_time="";
			} elsif( $d_monitor_time > 24*60*60 ) {
				$d_monitor_time=do_diff($d_monitor_time);
			} else {
				$d_monitor_time=do_time(-fmt=>"hh:mi", -time=>(time-$d_monitor_time));
			}
			if( $d_lastok =~ /^\s*$/ ) {
				$d_lastok="Never";
			} elsif( $d_lastok > 24*60*60 ) {
				$d_lastok=do_diff($d_lastok);
			} else {
				$d_lastok=do_time(-fmt=>"hh:mi", -time=>(time-$d_lastok));
			}

			my(@x)= ( $d_state,$d_system,$d_subsystem,$d_monitor_program,$d_monitor_time,
				$d_lastok,$d_message_text );
			push @outarray, \@x;
		}
	} elsif( $report_code eq "e" ) {
		$report_query =~ s/and state in/and severity in/;
		my(@x4)=qw(Source EventTime System Subsystem Severity Text);
		push @outarray,\@x4;
		my($d_monitor_program,$d_monitor_time,$d_system,$d_subsystem,$d_event_time,$d_severity,
				$d_event_id, $message_text, $message_value, $document_url,
				$reviewed_by, $reviewed_time, $internal_state );
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			($d_monitor_program,$d_monitor_time,$d_system,$d_subsystem,$d_event_time,$d_severity,
				$d_event_id, $message_text, $message_value, $document_url,
				$reviewed_by, $reviewed_time, $internal_state )= dbi_decode_row($_);
			my(@x2)=($d_monitor_program,$d_event_time,$d_system,$d_subsystem,$d_severity,$message_text );
			push @outarray, \@x2;
		}
	} else {
		my(@x3)=qw(N.A. N.A. N.A. N.A. N.A. N.A.);
		push @outarray,\@x3;
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x2)= dbi_decode_row($_);
			push @outarray, \@x2;
		}
	}
	print "DBG MlpRunScreen Completed: Returning $#outarray Rows".$NL if $args{-debug};
	return \@outarray;
}

sub get_group_cert_summary {
	my($showall)=@_;
	my($LOGON_USER)=GetLogonUser();

my($report_query)="
select 	distinct certification_target
	into 		#GROUPS
	from 		INV_required_certification
	where 	certification_type='GROUP'";
$report_query.="
and 		lower(samid)=lower('".$LOGON_USER."')

-- if you got no rows - see everyone! perhaps ur an auditor or admin!
if \@\@rowcount=0
insert 	#GROUPS
select 	distinct certification_target
from 		INV_required_certification
where 	certification_type='GROUP'" unless $showall;

$report_query.="
select distinct group_name,credential_name,display_name=convert(varchar(60),'')
   into #MEMBERS
   from INV_group_member, #GROUPS
      where group_name=certification_target
      and   group_type='ldap group'
      and   credential_type='ldap user'

update #MEMBERS
set display_name= p.display_name
from #MEMBERS m, INV_principal_map p
where m.credential_name = p.principal_name
and   p.principal_type='ldap user'

delete #MEMBERS where display_name is null

select
	case 		when status = 'Approve' 		then 'LIGHTGREEN'
				when status = 'Remove'  		then 'LIGHTGREEN'
				else 	 'PINK'
			end as COLOR,
	case		when status = 'Approve' then
			'?CANCERTIFY=Y&REPORTNAME=INV_DEPARTMENT_ACCESS&filter_approve=Approved%20Accounts&filter_ldap_group='+group_name
			when status = 'Remove'  then
			'?CANCERTIFY=Y&REPORTNAME=INV_DEPARTMENT_ACCESS&filter_approve=Removed%20Accounts&filter_ldap_group='+group_name
			else
			'?CANCERTIFY=Y&REPORTNAME=INV_DEPARTMENT_ACCESS&filter_approve=Unknown%20Status&filter_ldap_group='+group_name
			end as HYPERLINK,
	group_name,status,count(*)
from 	#MEMBERS m, INV_cert_department c
where 	m.group_name*=c.department
and 	m.display_name*=c.display_name
and   m.display_name!=''
group by group_name,status
order by group_name,status";

print "<FONT COLOR=GREEN><PRE>".$report_query."</FONT></PRE>" if is_developer();
return _querywithresults($report_query,1);
}

sub get_credential_summary {
	#my($showall)=@_;
	my($showall_servers,$showserver,$morewhere)=@_;

	my($LOGON_USER)=GetLogonUser();

	die "cant" if $showall_servers and $showserver;

	my($report_query)="
select 	distinct certification_target
into 		#tmp
from 		INV_required_certification
where 	certification_type='DATABASE'

";

$report_query="	select 	'".$showserver."'
	into #tmp" if $showserver;

$report_query.="
and 		lower(samid)=lower('".$LOGON_USER."')

-- if you got no rows - see everyone! perhaps ur an auditor or admin!
if \@\@rowcount=0
insert 	#tmp
select 	distinct certification_target
from 		INV_required_certification
where 	certification_type='DATABASE'" unless $showall_servers;

#-- Unknown Status
$report_query.="select distinct
  APPROVALCOLOR as COLOR,
  case when  APPROVALSTATUS = 'APPROVED'   then '?permission=APPROVER&CANCERTIFY=Y&REPORTNAME=INV_DATABASE_ACCESS&filter_approve='+APPROVALSTATUS+'&filter_system='+system_name
       when  APPROVALSTATUS = 'REMOVE'     then '?permission=APPROVER&CANCERTIFY=Y&REPORTNAME=INV_DATABASE_ACCESS&filter_approve='+APPROVALSTATUS+'&filter_system='+system_name
       when  APPROVALSTATUS = 'UNUSEABLE'  then '?REPORTNAME=INV_CREDENTIAL_MAPPING&filter_container=Unuseable%20Accounts&NOARGFETCH=1&filter_system='+system_name
       when  APPROVALSTATUS = 'UNAPPROVED' then '?permission=APPROVER&CANCERTIFY=Y&REPORTNAME=INV_DATABASE_ACCESS&filter_approve='+APPROVALSTATUS+'&filter_system='+system_name+'&filter_matchstatus='+MATCHSTATUS
       when  APPROVALSTATUS = 'UNMAPPED'   then '?REPORTNAME=INV_CREDENTIAL_MAPPING&filter_container=Unmapped%20Accounts&NOARGFETCH=1&filter_system='+system_name
       else '?permission=APPROVER&CANCERTIFY=Y&REPORTNAME=INV_DATABASE_ACCESS&filter_system='+system_name
  end as HYPERLINK,
  system_name,
  APPROVALSTATUS,
  MATCHSTATUS,
  count(*)
from    INV_cert_credential_view d, #tmp
where   certification_target = d.system_name  $morewhere
and d.system_type in ('sqlsvr','oracle','sybase')
-- and is_unuseable!='Y' and is_deleted!='Y'
-- and    APPROVALSTATUS != 'UNUSEABLE'
group by system_name,APPROVALSTATUS,MATCHSTATUS
order by system_name,APPROVALSTATUS,MATCHSTATUS

drop table #tmp
";

print "<FONT COLOR=GREEN><PRE>".$report_query."</FONT></PRE>" if is_developer();
return _querywithresults($report_query,1);
}

sub MlpGetReportCode
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	my(%args)= validate_params("MlpGetReportCode",%args1);

	my($rc)='h';
	$rc="e" if $args{-screen_name} =~ /event/i;

	return $rc;
}

sub MlpGetAlarms {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	my($query);
#	if( defined $args1{-test} ) {
		# this query will get all alarms WITHOUT regard to internal_state
#	$query="select h.monitor_program,h.system,h.subsystem,h.state,h.message_text,n.container,h.monitor_time
#		from Heartbeat h, Notification n
#		where n.monitor_program = h.monitor_program
#		and   isnull(n.system,isnull(h.system,'XXX')) = isnull(h.system,'XXX')
#		and   (n.container is null
#			or  h.system in ( select element from Container_full where name=n.container))";
#		$query="exec get_possible_alarms \@test_mode='Y'";
#	} else {
		$query="exec get_possible_alarms";
#	}

	my(@rc);
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		my(@r) = dbi_decode_row($_);
		print "DBG DBG RC=",join(",",@r),"\n";

		push @rc,\@r;
	}
	return @rc;
}

sub MlpClearAlarms {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(@rc);
	my($query)="exec clear_possible_alarms";
	dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
}

sub MlpSaveNotification
{
	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}

    my(%args)= validate_params("MlpSaveNotification",@_);
    $args{-EmergencyMin} 	= 0 unless $args{-EmergencyMin};
    $args{-FatalMin} 	= 60 unless $args{-FatalMin};
    $args{-ErrorMin} 	= 120 unless $args{-ErrorMin};
    $args{-WarnMin}	= 300 unless $args{-WarnMin};

    my($query)="insert SentNotification
    values ('".
      	$args{-monitor_program}. "','".
        	$args{-system}. "','".
        	$args{-subsystem}. "','".
        	$args{-monitor_time}. "','".
			$args{-severity}. "','".
      	$args{-user_name} 	. "',getdate(),'".
      	$args{-notify_type} 	. "','".
      	$args{-header} 		. "','".
      	dbi_quote(-text=>$args{-message}) 		. "'".
      	" )";

	#print "DBG DBG :",__FILE__," :",__LINE__," :",$query,"\n";
   dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
   #print "DBG DBG :",__FILE__," :",__LINE__," : DONE\n";
}

sub MlpGetSeverity {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(@severityvalues)=('Emergency','Fatals','Errors','Warnings','All');
	return @severityvalues;

	# my(@rc);
	# my($query)="SELECT severity from Severity order by value";
	# foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		# push @rc, dbi_decode_row($_);
	# }
	# return @rc;
}

sub MlpGetOperator {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	if( defined $args1{-showall} ) {
		my($query)="selectuser_name,user_type,enabled,
			pager_email,email,netsend,
			last_pager,last_email,last_netsend,
			weekday_start_time,weekday_end_time,saturday_start_time,
			saturday_end_time,sunday_start_time,sunday_end_time,
			holiday_start_time,holiday_end_time
 		from Operator";
		return dbi_query( -db=>$db,
				-query=>$query,
				-connection=>$connection,
				-die_on_error=>0);
	}

	my(@rc);
	my($query2)="select distinct user_name from Operator";
	foreach ( dbi_query(-db=>$db,-query=>$query2, -connection=>$connection, -die_on_error=>0)) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub GetLogonUser {
	my($LOGON_USER)=$ENV{LOGON_USER};
	$LOGON_USER=~s/^ad[\\\/]//i;
	return $LOGON_USER;
}

sub MlpCertifyDepartment {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my($LOGON_USER)=GetLogonUser();

	# foreach ( keys %args1 ) { print "MlpCertifyDepartment(",$_,")=",$args1{$_},"<br>"; }
	#die "barlow coding this right now MlpCertifyDepartment";

		#print "<PRE>";
		#use Data::Dumper;
		#print Dumper \%args1;
		#print "</PRE>";

	my($update_credential)="Update INV_cert_credential".
		" set	status="._quote($args1{-status}).",".
		" approved_by="._quote($LOGON_USER).",".
      " approved_time  = getdate()".
		" where display_name="._quote($args1{-displayname}).
		" and   status != "._quote($args1{-status})
		unless $args1{-status} eq "Unknown";

	my($query)="
	if exists (
    select 1 from INV_cert_department
    where     department   = "._quote($args1{-ldap_group})."
      and   display_name   = "._quote($args1{-displayname})."
    )
    begin
       update  INV_cert_department
          set approved_by   = "._quote($LOGON_USER).",
             approved_time  = getdate(),
              status        = "._quote($args1{-status}).",
              comment       = "._quote($args1{-notes})."
      where     department   = "._quote($args1{-ldap_group})."
      and   display_name   = "._quote($args1{-displayname})."
    end
    else
    begin
      insert INV_cert_department
      ( department, display_name, status, comment, approved_by , approved_time )
      values
      ( ".
      _quote($args1{-ldap_group}).", ".
      _quote($args1{-displayname}).", ".
      _quote($args1{-status}).",".
      _quote($args1{-notes}).", ".
      _quote($LOGON_USER).", getdate() )
   end

   $update_credential
   ";
	print "<FONT COLOR=GREEN><PRE>".$query."</FONT></PRE>" if $args1{-poweruser} eq "Y";
	_querynoresults($query,1);

}
sub MlpCertifyUserStatus {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

#	# CAN YOU CERTIFY!
#	#print "CHECKING YOUR CERTIFICATION RIGHTS<br>\n";
#	my($q)="Select permission from INV_systemuser where samid='".
#			$args1{-login}.
#			"' and ( permission='APPROVER' or permission='DASHBOARD POWERUSER')";
#	my($OK)='FALSE';
#	foreach ( _querywithresults($q,1)) {
#		($OK)=dbi_decode_row($_);
#	}
#	if( $OK eq "FALSE" ) {
#		print "ERROR: CAN NOT CERTIFY - YOU ARE NOT A REGISTERED APPROVER OR DASHBOARD POWERUSER.";
#		foreach ( _querywithresults("select permission from INV_systemuser where samid='".$args1{-login}."'",1) ) {
#			($OK)=dbi_decode_row($_);
#			print 'YOU HAVE PERMISSION AS A '.$OK."<br>";
#		}
#
#		return;
#	}

	my($LOGON_USER)=GetLogonUser();
	# foreach ( keys %args1 ) { print "MlpCertifyUserStatus(",$_,")=",$args1{$_},"<br>"; }

	my($query)="
   declare \@nm varchar(30)
   select \@nm=principal_name
   from INV_principal_map
   where system_name        = "._quote($args1{-filter_system})."
   and  ( display_name      = "._quote($args1{User})." or principal_name      = "._quote($args1{User}).")
   and   credential_name    = "._quote($args1{Credential})."

	if exists (
    select 1 from INV_cert_credential
    where     system_name   = "._quote($args1{-filter_system})."
      and   principal_name  = \@nm
      and   display_name    = "._quote($args1{User})."
      and   credential_name = "._quote($args1{Credential})."
    )
    begin
       update  INV_cert_credential
          set approved_by   = "._quote($LOGON_USER).",
             approved_time  = getdate(),
              status        = "._quote($args1{Radio_Approval_Status}).",
              comment       = "._quote($args1{Notes})."
      where system_name     =  "._quote($args1{-filter_system})."
      and   principal_name  = \@nm
      and   display_name    = "._quote($args1{User})."
      and   credential_name = "._quote($args1{Credential})."
    end
    else
    begin
      insert INV_cert_credential
      ( system_name, credential_name, display_name, principal_name,
         status, comment, approved_by , approved_time )
      values
      ( "._quote($args1{-filter_system}).", ".
      _quote($args1{Credential}).", ".
      _quote($args1{User}).", \@nm,".
      _quote($args1{Radio_Approval_Status}).", ".
      _quote($args1{Notes}).", ".
      _quote($LOGON_USER).", getdate() )
   end  ";
	print "<FONT COLOR=GREEN><PRE>".$query."</FONT></PRE>" if $args1{-debug};
	_querynoresults($query,1);
}

#sub MlpCertifyNow {
#	my(%args1)=@_;
#	initialize(%args1) unless $i_am_initialized eq "TRUE";
#	if($i_am_initialized eq "FAILED" ) {
#		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
#		return 0;
#	};
#	# CAN YOU CERTIFY!
#	#print "CHECKING YOUR CERTIFICATION RIGHTS<br>\n";
#	my($q)="Select permission from INV_systemuser where samid='".
#			$args1{-login}.
#			"' and ( permission='APPROVER' or permission='DASHBOARD POWERUSER')";
#	my($OK)='FALSE';
#	foreach ( dbi_query(-db=>$db,-query=>$q, -connection=>$connection, -die_on_error=>0)) {
#		($OK)=dbi_decode_row($_);
#	#	print 'YOU HAVE PERMISSION AS A '.$OK."<br>";
#	}
#	if( $OK eq "FALSE" ) {
#		print "ERROR: CAN NOT CERTIFY - YOU ARE NOT A REGISTERED APPROVER OR DASHBOARD POWERUSER.";
#		return;
#	}
#
#	# GET WHAT YOU ARE CERTIFYING
#	my($CERTTYPE)='DATABASE' if $args1{-system} ne "";
#	   $CERTTYPE ='GROUP'    if $args1{-ldap_group} ne "";
#
#	if( !  $CERTTYPE ) {
#		#my($argsstr)='';
#		#foreach ( keys %args1 ) { $argsstr.=" ".$_."=>".$args1{$_}.", "; }
#		#$argsstr=~ s/, $//;
#		# die "MlpCertifyNow($argsstr) - FATAL ERROR - BAD ARGUMENTS ";
#	   confess "MlpCertifyNow() - FATAL ERROR - BAD ARGUMENTS ";
#	}
#	my($CERTTARGET) = $args1{-system} if $args1{-system} ne "";
#	   $CERTTARGET  = $args1{-ldap_group} if $args1{-ldap_group} ne "";
#
#
#
#$q="BEGIN TRANSACTION
#
#   declare \@x datetime
#   select \@x=getdate()
#   ";
#
#$q.="insert INV_principal_map_history select \@x,* from INV_principal_map where principal_type='ldap group' and principal_name='
#
#if \@\@error !=0
#begin
#  rollback tran
#  --exit
#end
#".
#$args1{-ldap_group}.
#"'\n" if $args1{-ldap_group} ne "";
#
#$q.="insert INV_principal_map_history select \@x,* from INV_principal_map where system_name='".
#$args1{-system}.
#"'
#
#if \@\@error !=0
#begin
#  rollback tran
#  --exit
#end
#" if $args1{-system} ne "";
#
#$q.="insert INV_certification ( approver_samid, certification_type, certification_target, certification_time, notes)
#	 values ( '".$args1{-login}."','".$CERTTYPE."','".$CERTTARGET."',\@x,'".$args1{-certification_notes}."')
#
#if \@\@error !=0
#begin
#  rollback tran
#  --exit
#end
#
#	 COMMIT TRANSACTION ";
#	print "<PRE>",$q,"</PRE>\n" if $args1{-debug};
#	_querynoresults($q,1);
#
#	print "<p><TABLE BORDER=1 BGCOLOR=WHITE><TR class='menuheader'><TD ALIGN=LEFT><FONT face=sans-serif size=+1 color=red>SUCCESSFUL CERTIFICATION OF $CERTTYPE $CERTTARGET</FONT></TD>\n</TR></TABLE>\n";
#
#	#print __FILE__,' ',__LINE__," NOT CERTIFYING $CERTTYPE";
#	#foreach ( keys %args1 ) { print $_ ," - ",$args1{$_},"\n"; }
#	print "<h2>Your Last 10 Certifications</H2>\n";
#	my($report_query)="
#select top 10 'Certification Type'=c.certification_type, Target=c.certification_target, Date=c.certification_time, Notes=notes
#from 	INV_certification c
#where approver_samid = '".$args1{-login}."'
#	order by c.certification_time desc";
#	print $report_query,"\n" if $args1{-debug};
#	print dbi_query_to_table( -query=>$report_query, -db=>$db, -connection=>$connection,
#		 -print_hdr=>1, -table_options=>'', -die_on_error=>0 );
#
#	print "<h2>Your Certification Requirements</H2>\n";
#	$report_query="
#select distinct  'Certification Type'=r.certification_type, Target=r.certification_target, Date=c.certification_time , Frequency=how_often_required
#from INV_required_certification r, INV_certification c
#where r.samid*=c.approver_samid
#and   r.certification_type*=c.certification_type
#and   r.certification_target*=c.certification_target
#and	samid = '".$args1{-login}."'
#	order by r.certification_type, r.certification_target, c.certification_time desc";
#
#	print $report_query,"\n" if $args1{-debug};
#	print dbi_query_to_table( -query=>$report_query, -db=>$db, -connection=>$connection,
#		 -print_hdr=>1, -table_options=>'', -die_on_error=>0 );
#}

sub MlpGetPermission {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	$args1{-samid}=~s/^[a-z][a-z][\\\/]//i;

#print __LINE__," fetching permssions for ".$args1{-samid}."<br>";
	my($query)="
select distinct permission from INV_system_permission_view 
where samid=lower('".$args1{-samid}."') or '".$args1{-samid}."'='BATCHREPORTS'";

#select distinct permission 
#from 	INV_systemgroup g, INV_group_member m 
#where g.samid=m.group_name
#and   lower(m.credential_name ) = lower('".$args1{-samid}."') 
#and   group_type='ldap group'
#union
#select permission 
#from INV_systemuser 
#where lower(samid)=lower('".$args1{-samid}."') or '".$args1{-samid}."'='BATCHREPORTS'";
#	print "<PRE>",$query,"</PRE>";
	my(@rc);
	my($has_user_login)='N';

	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		 my(@x)=dbi_decode_row($_);
		 push @rc,$x[0];
		 $has_user_login='Y'; #  if $x[0]=~/^DASHBOARD/ or $x[0]=~/APPROVER/ or $x[0]=~/AUDITOR/
		 	# or $x[0]=~/POWERUSER/ or $x[0]=~/ADMIN/ or $x[0]=~/MANAGER$/;
	}
#print "Can->",$args1{-canaccess}," H-",$has_user_login,"<br>";
	return $has_user_login if $args1{-canaccess};
	return @rc;
}

# MlpDeleteSystem
#   -system_name      
#   -system_type        
sub MlpDeleteSystem {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my($query) = "delete INV_system where system_name = '".$args1{-system_name}."' and system_type='". $args1{-system_type}."'";
	print $query if $args1{-debug} or $args1{-noexec};	
	_querynoresults($query,1) unless  $args1{-noexec};
	
	$query = "delete INV_system_attributes where system_name = '".$args1{-system_name}."' and system_type='". $args1{-system_type}."'";
	print $query if $args1{-debug} or $args1{-noexec};	
	_querynoresults($query,1) unless  $args1{-noexec};	
}
	
# MlpDeleteSystemAttributes
#   -system_name      
#   -system_type        
#   -attribute_category
sub MlpDeleteSystemAttributes {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	
	my($query) = "delete INV_system_attributes where system_name = '".$args1{-system_name}."' and system_type='". $args1{-system_type}."'";
	$query .= " and attribute_category='".$args1{-attribute_category}."'" if $args1{-attribute_category};
	print $query if $args1{-debug} or $args1{-noexec};	
	_querynoresults($query,1) unless  $args1{-noexec};	
}

# MlpUpdateSystem
#   -system_name      
#   -system_type        
#   -attribute_category
#   -attributes			hashref
sub MlpUpdateSystem {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	
	my($query) = "if not exists ( select * from INV_system 
	where system_name = '".$args1{-system_name}."' and system_type='". $args1{-system_type}."' )
	insert INV_system ( system_name,system_type ) values ('".$args1{-system_name}."','".$args1{-system_type}."') ";
	print $query if $args1{-debug} or $args1{-noexec};	
	_querynoresults($query,1) unless  $args1{-noexec};	
	
	my(%denormalized)=(
		primary_dnsalias => 1,
		description1 => 1,
		description2 => 1,
		description3 => 1,
		description_ldap => 1,
		is_infra_monitored => 1,
		is_dba_monitored => 1,
		production_state => 1,
		blackedout_until => 1,
		hostname => 1,
		host_type => 1,
		host_hardwaretype => 1,
		dr_system_name => 1,
		dr_system_type => 1
	);
	
	MlpDeleteSystemAttributes(-system_name=>$args1{-system_name},-system_type=>$args1{-system_type});
	my($updateclause) = '';
	foreach ( keys %{$args1{-attributes}} ) {
		if( $denormalized{$_} ) {
			$updateclause.= "$_ = '".$args1{-attributes}->{$_}."',";
		}
		$query="INSERT INV_system_attribute 
			(attribute_name,system_name,system_type,database_name,attribute_value,attribute_category,mod_date,mod_by)
		      values 	(
		      '".$_."','".
		      $args1{-system_name}."','".
		      $args1{-system_type}."',null,'".
		      $args1{-attributes}->{$_}."','".
		      $args1{attribute_category}."',".getdate().",'bob')";
		       
		print $query if $args1{-debug} or $args1{-noexec};	
		_querynoresults($query,1) unless  $args1{-noexec};	
	}
	if( $updateclause ) {	
		$updateclause = "update INV_system set ".$updateclause.
		" where system_name = '".$args1{-system_name}."' and system_type='". $args1{-system_type}."'";
		print $updateclause if $args1{-debug} or $args1{-noexec};	
		_querynoresults($updateclause,1) unless  $args1{-noexec};	
	}		
}

# MlpGetSystem
#
#   -type               - Database | Host | ApproveRqdDatabase
#   -where              - if set then this is an added where clause restricting the systems being returned
#   -getattributes      - returns attributes
#   -attribute_category - modifies attributes if those are requested
#   -debug              - prints the queries to standard output
#   -system_name        - restrict list to systems of this name (wildcards ok)
#   -system_type        - restrict list to systems of given system_type
#
#   if -getattributes passed then
#       returns hashref of key= (names if -type or name.type if ! -type ) and values=attributes hash
#   else
#       returns array of names if -type or name.type if ! -type
#
# example #1 � my(@rc)=MlpGetSystem()  
#            - @rc is an array of hostname.hosttype
# example #2 � my(@rc)=MlpGetSystem(-type=>�Host�)  
#            - @rc is an array of hostname
# example #3 -  my($rc)=MlpGetSystem(-type=>�Host�, -attribute_category=>�ldapextract�, -getattributes=>1)  
#            - $rc is an hashref with keys of hostname and values which are hashrefs containing attributes

sub MlpGetSystem {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	my(@rc);
	my($where)='';
	if( $args1{-type} ) {
		if( $args1{-type} eq "Database" ) {
			$where = 'where s.system_type in ("sybase","oracle","sqlsvr","mysql")';
		} elsif( $args1{-type} eq "Host" ) {
			$where = 'where s.system_type in ("win32servers","unix")';
		} elsif( $args1{-type} eq "ApproveRqdDatabase" ) {
		} else {
			die "CANT USE TYPE $args1{-type}";
		}
	}
	if( $args1{-where} ) {
		if( $where eq '' ) {
			$where = "where $args1{-where} ";
		} else {
			$where .= " and $args1{-where} ";
		}
	}
	if( $args1{-system_name} ) {
		if( $where eq '' ) {
			$where = "where s.system_name like $args1{-system_name} ";
		} else {
			$where .= " and s.system_name like $args1{-system_name} ";
		}
	}
	if( $args1{-system_type} ) {
		if( $where eq '' ) {
			$where = "where s.system_type = $args1{-system_type} ";
		} else {
			$where .= " and s.system_type = $args1{-system_type} ";
		}
	}

	if( ! $args1{-getattributes} ) {
		my($query)="select system_name,system_type from INV_system s $where order by system_name,system_type";
		$query = "select distinct certification_target from INV_required_certification where certification_type='DATABASE'"
			if $args1{-type} eq "ApproveRqdDatabase";
		print $query if $args1{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
			 my(@x)=dbi_decode_row($_);
			 if( $args1{-type} or $args1{-system_type}) {
			 	push @rc,$x[0];
			} else {
				push @rc,$x[0].".".$x[1];
			}
		}
		return @rc;
	} else {
		if( $args1{-attribute_category} ) {
			if( $where eq '' ) {
				$where = "where a.attribute_category = $args1{-attribute_category} ";
			} else {
				$where .= " and a.attribute_category = $args1{-attribute_category} ";
			}
		}

		my($query)="select s.system_name,s.system_type, attribute_name, attribute_value
						from INV_system s, INV_system_attribute a
						$where
						order by system_name,system_type";
		print $query if $args1{-debug};
		my(%rchash);
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
			 my(@x)=dbi_decode_row($_);
			 my($key);
			 if( $args1{-type} or $args1{-system_type} ) {
			 	$key = $x[0];
			} else {
				$key = $x[0].".".$x[1];
			}
			if( defined $rchash{$key} ) {
				my($hashptr)=$rchash{$key};
				$hashptr->{$x[3]} = $x[4];
			} else {
				my(%hash);
				$hash{$x[3]} = $x[4];
				$rchash{$key} = \%hash;
			}
		}
		return \%rchash;
	}
}

sub MlpGetBusiness_System {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	# my($where)="where business_system_type is null";
	# $where = "where isnull(business_system_type,'".$args1{-type}."') = '".$args1{-type}."'" if $args1{-type} !~ /^\s*$/;
	my($where) = "where is_view='N' and is_systemview='N'" ; # if $args{-type}='normal';
	my(@rc);
	my($query)="select business_system_name from INV_business_system $where order by business_system_name";
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub MlpGetPrincipal {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	my($second_field)=",Dname=isnull(display_name,credential_name)" if $args1{-getlabelhash};

	my($display_mod) = " and display_name is not null" if $args1{-displayable} eq 'Y';
	my(@rc);
	my($query);
	if( $args1{-type} eq 'ldap group' ) {
		$query="select distinct credential_name,
		Dname=case when upper(display_name) like 'DISABLED -%'
						then ltrim(substring(display_name,11,100))
				   else display_name
		      end
		from INV_credential where credential_type='ldap group'
		$display_mod
order by upper(credential_name)";
	} elsif( $args1{-type} eq 'certifier' ) {
		$query="select distinct lower(samid) from INV_system_permission_view
		where permission='APPROVER' or permission='DASHBOARD POWERUSER' order by samid";
	} elsif( $args1{-type} eq 'globalshared' ) {
		$query = "
select distinct credential_name from INV_principal_map
where credential_type!='ldap user' and credential_type!='ldap group' and is_deleted='N' and principal_type!='ldap user'
union select credential_name=principal_name from INV_principal_map where principal_type='app user'
order by credential_name ";
	} elsif( $args1{-type} eq 'allcredentials' ) {
		#$query = "select distinct credential_name $second_field from INV_credential";

		$query="select distinct credential_name ,
					Dname=case when upper(display_name) like 'DISABLED -%'
						then ltrim(substring(display_name,11,100))
					when display_name=''
						then credential_name
				   else display_name
		      end
		 from INV_credential ";
		 #where credential_type='ldap user'
		 #and 	display_name!=''";

		if( $args1{-getlabelhash} ) {
			$query .= " order by Dname";
		} else {
			$query .= " order by credential_name";
		}
	} else {
		$query="select distinct credential_name ,
		Dname=case when upper(display_name) like 'DISABLED -%'
						then ltrim(substring(display_name,11,100))
				   else display_name
		      end
		 from INV_credential
		 where credential_type='ldap user'
		 and 	display_name!=''";

		#$query="select distinct credential_name $second_field
		# from INV_credential
		# where credential_type='ldap user'
		# and 	display_name!=''
		# ";
		#-- and is_deleted='N'

		if( $args1{-getlabelhash} ) {
			$query .= " order by Dname";
		} else {
			$query .= " order by credential_name";
		}

		#$query="select distinct credential_name from INV_credential where credential_type='ldap user' and
#and attribute_key='disabled' and attribute_value='no'
#order by credential_name";
	}

	print "<PRE>", $query,"</PRE><br>" if $args1{-debug};
	if(  $args1{-getlabelhash} ) { # return a hash
		my(%hsh);
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
			my(@rc2) = dbi_decode_row($_);
			push @rc, $rc2[0];
			# $rc2[1] =~ s/^disabled[\s\-]+//i;
			if( $args1{-maxlength} ) { 
				$rc2[1] = substr($rc2[1],0,$args1{-maxlength});
			} 
			$hsh{ $rc2[0] } = $rc2[1];
			
		}
		return(\@rc,\%hsh);
	} else {
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
			my(@rc2)= dbi_decode_row($_);
			push @rc,$rc2[0];
		}
		return @rc;
	}
}

sub MlpGetProgram {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(@rc);
	my($query)="select distinct monitor_program from Heartbeat where batchjob is null
union
select monitor_program  from Event
order by monitor_program";
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}
sub MlpGetDepartment {
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	#my($query)="  select distinct attribute_value
	#from INV_credential_attribute where attribute_key='dept' and credential_type='ldap user'
	#    and credential_name in (
	#    select distinct credential_name from INV_credential_attribute where attribute_key='disabled' and credential_type='ldap user' and attribute_value='no'
	#    )
	#    order by attribute_value";
	#my(@rc);
	#foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
	#	push @rc, dbi_decode_row($_);
	#}
#	my(@rc)=qw(All Accounting Admin Audit Catapult Compliance
#Corporate Credit Decade
#Executive HR IAE IT Legal Millennium Operations Risk Tax
#Technology Trading Windows WorldQuant Other);

my(@rc)=(
'Accounting',
'Administration',
'Business Development',
'Catapult',
'Compliance',
'Corporate Initiatives',
'Decade',
'Executives',
'Fixed Income',
'Fixed Income & Risk Technology',
'Green Arrow',
'Human Resources',
'Information Technology',
'LN - Operations',
'Legal',
'Marketing & Investor Relations',
'Middle Office',
'Millennium',
'Operations',
'Risk Management',
'Senior Management',
'Stock Loan',
'Tax',
'Trading',
'Other'
);

	return @rc;
}

sub MlpGetContainer
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetContainer",%args1);

	$args{-type} = "s" unless defined $args{-type};

	# if screen_name passed then you must get the container name for that screen
	if( $args{-screen_name} ) {
		my($query)="SELECT container_name from Screen where screen_name=\'".$args{-screen_name}."\'";
		$query.=" and owner = \'".$args{-user}."\'" if defined $args{-user};
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
			($args{-name}) = dbi_decode_row($_);
		}
	}

	my(@rc);
	push @rc,"All";
	if( $args{-type} eq "s" ) {
		my($qry2)= "SELECT distinct element from Container_full where 1=1 \n";
		$qry2.=" and owner = \'".$args{-user}."\'" if defined $args{-user};
		$qry2.=" and name = \'".$args{-name}."\'" if defined $args{-name};
		print $qry2 if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$qry2, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			my($el,$ty)=dbi_decode_row($_);
			# if( $ty eq "s" ) {
				push @rc,$el;
			# } else {
				# push @rc,MlpGetContainer(-name=>$el,-type=>"s",-user=>$args{-user});
			# }
		}
		return sort @rc;
	} elsif( $args{-type} eq "c" ) {
		my($qry2)= "SELECT distinct name from Container where 1=1\n";
		$qry2.=" and owner = \'".$args{-user}."\'" if defined $args{-user};
		$qry2.=" and name = \'".$args{-name}."\'" if defined $args{-name};
		print $qry2 if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$qry2, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			push @rc,dbi_decode_row($_);
		}
		return sort @rc;
	} else {
		die "Invalid Type Arg Passed - must be c or s";
	}
}

sub MlpGetDbh
{
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	return $connection;
	# return dbi_getdbh();
}

sub set_developer { 	($is_development)=@_; }

sub is_developer {
	my($username) = @_;
	return 0 unless $is_development;
	if( $ENV{REMOTE_ADDR} eq $DEVELOPER_IPADDR  or  $ENV{REMOTE_ADDR} eq $DEVELOPER_IPADDR2 ) {
		print "<FONT SIZE=-2>Called From The Developers Workstation</FONT>\n";
		return 1;
	} else {
		return 0;
	}
}

sub MlpManageData
{
	my(%args)=@_;
	my(@rc);
	my($query);
	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my($testquery);		# if defined then if it returns results we have an issue
	if( $args{-canunderstandsql} eq 'Y' or is_developer() ) {
		$args{-canunderstandsql}='Y';
		my($str)="<FONT COLOR=GREEN>MlpManageData( ";
		foreach( sort keys %args ) {			$str.= "$_=>$args{$_}, " unless $_ eq "-NL"; 		}
		$str=~s/, $/)/;
		print $str."</FONT><br>\n";
	}

	my($username) = $ENV{REMOTE_ADDR} || $ENV{LOGNAME} || $ENV{USERNAME};
#	if( $username eq $DEVELOPER_IPADDR ) {
#		push @rc, "<FONT SIZE=-2>MlpManageData - Called From The Developers Workstation</FONT>\n";
#		foreach ( sort keys %args ) {
#			push @rc, "<FONT SIZE=-2>MlpManageData: $_ => $args{$_} </FONT>\n";
#		}
#	}

	# DELETE BUTTON - Operation='del' OkId=number, Admin=screen
	#		MlpManageData(-debug=>$debug,OkId=>$del_id,Operation=>"del",	%keys)
	# ADD BUTTON    - Operation='add' Admin = screen
	#		MlpManageData( Operation=>"add",Admin=>param("sv_admin"),	-debug=>$debug,%keys)
   # AUTO SET OPERATION
	#		MlpManageData(	Admin=>$x,%hash,Operation=>"Autoset",SetField=$var,Value=>$new )
	#
	if( $args{Operation} eq "Autoset" and defined $args{SetField} ) {
		#print "AUTOSET: $args{SetField} <br>" if $args{-canunderstandsql} eq 'Y';
		my($table,%updatable_by_label,%keys_by_label);
#		if( $args{Admin} eq 'INV_SEE_UNMAPPED'
#			or $args{Admin} eq 'INV_SEE_MAPPED'
#			or $args{Admin} eq 'INV_SEE_ALL'
#			or $args{Admin} eq "INV_SEE_BATCHACCOUNTS"  ) {
#			$table='INV_credential_notes';
#			%keys_by_label= (
#				"Account_Name"				=> 'credential_name',
#				"Credential_Type"			=> 'credential_type',
#				#"System"						=> 'system_name',
#			);
#			%updatable_by_label= (
#				"UNUSEABLE_Login"	=> 'is_unuseable',
#				"Service_Login"	=> 'is_serviceaccount',
#				"Is_Service_Login"	=> 'is_serviceaccount',
#				"Operator_Login"	=> 'is_operator',
#				"LDAP_Group"		=> 'is_ldap_group',
#				"LDAP_User"			=> 'is_ldap_user',
#				"Is_Error"			=> 'is_error',
#				"Shared_Login"		=> 'is_shared',
#				"Notes"				=> 'user_notes' );
#
#			print "RETURNING - $args{SetField} is not updatable <br>" if $args{-canunderstandsql} eq 'Y' and ! $updatable_by_label{$args{SetField}};
#			return if ! $updatable_by_label{$args{SetField}};
#			$args{Value}='N' if $args{Value} eq "" and $updatable_by_label{$args{SetField}} =~ /^is_/;
#		} els
		my($extraset)='';
		if(  $args{Admin} eq 'INV_CREDENTIAL_MAPPING' ) {
			# or   $args{Admin} eq 'INV_MAPPED_CREDENTIALS' ) {

		#use Data::Dumper;
		#print __LINE__," DBGDBG","<PRE>",Dumper \%args,"<PRE><br>";

			$extraset=", is_ldap_user='Y'" if $args{SetField} eq "INV_LDAP_USER_LU";

			$table='INV_credential_notes';
			%keys_by_label= (
				"Credential_Name"			=> 'credential_name',
				"Credential_Type"			=> 'credential_type',
				#"Credential_Name"			=> 'credential_name',
				#"Credential_Type"			=> 'credential_type',
				#"Principal_Name"			=> 'credential_name',
				#"Principal_Type"			=> 'credential_type',
				#"System"						=> 'system_name',
			);

			%updatable_by_label= (
				"INV_LDAP_USER_LU"	=> 'ldap_user',
				"Is_Error"				=> 'is_error',
				"Is_Service_Login"	=> 'is_serviceaccount',
				"Is_Unuseable_Login" => 'is_unuseable',
			 	"Notes"					=> 'user_notes' );
#
#				"UNUSEABLE_Login"	=> 'is_unuseable',
#				"Service_Login"	=> 'is_serviceaccount',
#				"Is_Service_Login"	=> 'is_serviceaccount',
#				"Operator_Login"	=> 'is_operator',
#				"LDAP_Group"		=> 'is_ldap_group',
#				"LDAP_User"			=> 'is_ldap_user',
#				"Is_Error"			=> 'is_error',
#				"Shared_Login"		=> 'is_shared',
#				"Notes"				=> 'user_notes' );

			print "RETURNING - $args{SetField} is not updatable <br>"
				if $args{-canunderstandsql} eq 'Y' and ! $updatable_by_label{$args{SetField}};
			return if ! $updatable_by_label{$args{SetField}};
			$args{Value}='N' if $args{Value} eq "" and $updatable_by_label{$args{SetField}} =~ /^is_/;
		}elsif( $args{Admin} eq 'INV_DEFINE_SYSTEMS' ) {
			$table='INV_system';
			%keys_by_label= (
				"System_Name"				=> 'system_name'	,
				"INV_SYSTEM_TYPE_LU"		=> 'system_type'	,
			);
			%updatable_by_label= (
				"Description"				=> 'description',
				"INV_SERVER_TYPE_LU"		=> 'production_state',
				"Is_Blackedout_LU"		=> 'is_blackedout',
				"is_infra_monitored_LU"			=> 'is_infra_monitored'
				 );

			return if ! $updatable_by_label{$args{SetField}};
			$args{Value}='N' if $args{Value} eq "" and $updatable_by_label{$args{SetField}} =~ /^is_/;
		} elsif( $args{Admin} eq "INV_NEW_SYSTEM_TRACKER" ) {
			$table = 'INV_proposed_system';
			%updatable_by_label= (
			"System_Name"				=> 'system_name',
			"SYSTEM_TYPE"				=> 'system_type',
			"Txtbox30_Description"	=> 'description',
			"Txtbox30_hostname"		=> 'hostname',
			#"Txtbox8_Scheduled_Date"=> 'scheduled_delivery',
			#"Txtbox8_Actual_Date"	=> 'actual_delivery',
			"Waiting_On"				=> 'waiting_on' );

			return if ! $updatable_by_label{$args{SetField}};
		} else {
			print "ERROR - TABLE $args{Admin} NOT MAPPED AT LINE ",__LINE__," Of ",__FILE__,"<br>";
			foreach ( keys %args ) { print $_ . " => $args{$_} <br>" unless $_ eq "-NL"; }
			exit(0);
		}
		if( ! defined $updatable_by_label{$args{SetField}} ) {
			print "ERROR - FIELD $args{SetField} NOT MAPPED For $args{Admin} AT LINE ",__LINE__," Of ",__FILE__;
			exit(0);
		}

		my($whereclause)='where ';
		my($keyclause)='';
		my($valueclause)='';
		my($keyvalues)='';
		#print __LINE__," DBGDBG<br>";

		foreach ( keys %args ) {
		#	print __LINE__," DBGDBG $_ $args{$_}<br>";
			if( $keys_by_label{$_} ) {
				$whereclause .= $keys_by_label{$_}." = ".dbi_quote(-text=>$args{$_}, -connection=>$connection)."\nand ";
				$keyclause   .= $keys_by_label{$_}.",";
				$keyvalues .=   dbi_quote(-text=>$args{$_}, -connection=>$connection).",";
			} elsif( $updatable_by_label{$_} ) {
				$valueclause .= dbi_quote(-text=>$args{$_}, -connection=>$connection).",";
			}

		}
		if( $whereclause eq "where " ) {
			print "ERROR: NO KEYS DEFINED\n";
			push @rc, "ERROR: NO KEYS DEFINED ";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}
		$whereclause	=~s/\nand $//;
		$keyclause		=~s/,$//;
		$valueclause	=~s/,$//;
		$keyvalues		=~s/,$//;

		$query="if not exists ( select * from $table \n$whereclause )\n insert $table ( $keyclause ) \nvalues\n ( $keyvalues )\n\n";

		# ok special case here... if updating a database system... then update the hardware the same way...
		#print "<PRE><B>$query</B></PRE>";
		#push @rc, "Running:",$query;

		#foreach ( dbi_query(-db=>$db,-query=>$query, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
	#		push @rc,"ERROR:".dbi_decode_row($_);
#			push @rc,"ERROR: $_ $DBI::err $DBI::errstr";
#		}

		$query .= "update $table set ";
		$query.= $updatable_by_label{$args{SetField}}. " = ".dbi_quote(-text=>$args{Value}, -connection=>$connection). $extraset. "\n".$whereclause;

	#	die "DBGDBG TEST DONE $query" if  $args{Admin} eq 'INV_CREDENTIAL_MAPPING';

	#if( $args{-canunderstandsql} eq 'Y' ) {
	#	print "<p><pre><FONT COLOR=GREEN>QUERY=$query</font></pre><br>\n" ;
	#	print "<p><FONT COLOR=GREEN>whereclause=$whereclause</font><br>\n";
	#	print "<p><FONT COLOR=GREEN>keyclause=$keyclause</font><br>\n";
	#	print "<p><FONT COLOR=GREEN>valueclause=$valueclause</font><br>\n";
	#}

	# OkId and ! Admin and Operation=del = delete row
	# OkId and ! Admin and Operation!=del = approve for 1 day
	} elsif( defined $args{OkId}
	and (! defined $args{Admin} or $args{Admin}=~/^\s*$/)) {
			if( ! defined $args{Program} ) {
				push @rc, "ERROR: Program Not Set - No Calling Screen ";
				if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
				}
				return @rc;
			}

			if( $args{Operation} eq "del" ) {
				$query="Delete Heartbeat
					where monitor_program='".$args{Program}."'
			and system =    '".$args{System}."'
			and isnull(subsystem,'')=  '".$args{Subsystem}."'
			and state=      '".$args{State}."'";
			} elsif( $args{Operation} eq "delserver" ) {
				$query="Delete Event where system = '".$args{System}."'";
				push @rc,$query;
				foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
					push @rc,"ERROR:".dbi_decode_row($_);
					push @rc,"$_ $DBI::err $DBI::errstr";
				}
				$query="Delete Heartbeat where system = '".$args{System}."'";

			} else {			# ITS OK!
				$args{Hours}=24 unless $args{Hours};
				if( $args{System} eq '' ) {
					$query="Update Heartbeat Set
						reviewed_by	=	'".$username."',
						reviewed_time	=	getdate(),
						reviewed_until	=	dateadd(hh,$args{Hours},getdate()),
						reviewed_severity= null
					where monitor_program='".$args{Program}."'";
				} else {
						$query="Update Heartbeat Set
					reviewed_by	=	'".$username."',
					reviewed_time	=	getdate(),
					reviewed_until	=	dateadd(hh,$args{Hours},getdate()),
					reviewed_severity= null
					where monitor_program='".$args{Program}."'
					and system =    '".$args{System}."'
					and isnull(subsystem,'')=  '".$args{Subsystem}."'
					and state=      '".$args{State}."'";
				}
			}
			# print $query;
	} elsif( $args{Admin} eq "Delete By Monitor") {
		if( $args{Operation} eq "del" ) {
			$query="Delete Event where monitor_program='".$args{Monitor}."'";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}
			$query="Delete Heartbeat where monitor_program='".$args{Monitor}."'";
			#push @rc,$query;
		# } else {
			# push @rc,"FILE=".__FILE__." Line=".__LINE__."DBG DBG NOTE - OPERATION=$args{Operation}";
		}
	} elsif( $args{Admin} eq "Delete By Monitor_Type") {
		if( $args{Operation} eq "del" ) {
			$query="Delete $args{Type} where monitor_program='".$args{Monitor}."'";
		}
	} elsif( $args{Admin} eq "Production" ) {
			# because of the way the initial query worked, we are ensured
			# that the server is in ProductionServers, but are not ensured
			# that a row exists in IgnoreList
			if( $args{Operation} eq "setignore" ) {
				if( $args{Value} == 1 ) {
					push @rc,"Placing Server $args{Server} On Ignore List\n";
					$query="insert IgnoreList values(null,'".$args{Server}."',null)";
				} else {
					push @rc,"Removing Server $args{Server} From Ignore List\n";
					$query="DELETE IgnoreList where monitor_program is null and subsystem is null and system='".$args{Server}."'";
				}
			} elsif( $args{Operation} eq "setproduction" ) {
				$query="Insert ServerChanges values( \'".$args{Server}."\',getdate(),\'is_production\',\'".$args{Value}."\')";
				foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
					push @rc,"ERROR:".dbi_decode_row($_);
					push @rc,"$_ $DBI::err $DBI::errstr";
				}
				if( $args{Value} == 1 ) {
					push @rc,"Server $args{Server} Is Now Considered PRODUCTION\n";
					$query="UPDATE ProductionServers set is_production=1 where system='".$args{Server}."'";
									} else {
					push @rc,"Server $args{Server} Is No Longer Considered PRODUCTION\n";
					$query="UPDATE ProductionServers set is_production=0 where system='".$args{Server}."'";
				}
			} elsif( $args{Operation} eq "setblackout7days" ) {
				$args{Days}=7 unless $args{Days};
				if( $args{Value} == 1 ) {
					push @rc,"Server $args{Server} Is Now Blacked Out\n";
					$query="Insert Blackout select '".$args{Server}."', dateadd(dd,$args{Days},getdate())\n";
					$query.="Update Heartbeat Set
			reviewed_by	=	'".$username."',
			reviewed_time	=	getdate(),
			reviewed_until	=	dateadd(dd,$args{Days},getdate()),
			reviewed_severity= null
			where system='".$args{Server}."'\n";
				} else {
					push @rc,"Server $args{Server} Is No Longer Blacked Out\n";
					$query="Delete Blackout where system='".$args{Server}."'\n";

				}
			} elsif( $args{Operation} eq "setblackout" ) {
				if( $args{Value} == 1 ) {
					my($hours)=0;
					$hours+=$args{Hours} 	if $args{Hours};
					$hours+=24*$args{Days} 	if $args{Days};
					push @rc,"Server $args{Server} Is Now Blacked Out\n";
					$query="Delete Blackout where system='".$args{Server}."'\n ";
					$query.="Insert Blackout select '".$args{Server}."', dateadd(hh,$hours,getdate())\n";
					$query.="Update Heartbeat Set
			reviewed_by	=	'".$username."',
			reviewed_time	=	getdate(),
			reviewed_until	=	dateadd(hh,$hours,getdate()),
			reviewed_severity= 	null
			where system='".$args{Server}."'\n";
				} else {
					push @rc,"Server $args{Server} Is No Longer Blacked Out\n";
					$query="Delete Blackout where system='".$args{Server}."'\n";
				}
			} elsif( $args{Operation} eq "del" ) {
				push @rc,"Server $args{Server} Deleted\n";
				$query.="Delete Blackout where system='".$args{Server}."'\n";
				$query.="Delete ProductionServers where system='".$args{Server}."'\n";
				$query.="Delete IgnoreList where system='".$args{Server}."'\n";
				$query.="Delete Heartbeat where system='".$args{Server}."'\n";
				$query.="Delete Event where system='".$args{Server}."'\n";
			} else {
					push @rc,"ERROR: Invalid Args To Production Screen Handler";
					foreach (keys %args) { push @rc,"Key=$_ val=$args{$_}"; }
					return @rc;
			}
	} elsif( $args{Admin} eq "Rebuild Containers" ) {
			$query="exec build_container";
	} elsif( $args{Admin} eq "System->Container" ) {
		if( $args{Container} =~ /^\s*$/ or $args{System_LU} =~ /^\s*$/ ) {
			push @rc,"ERROR: Not All Required Args Defined";
			return @rc;
		}

		if( $args{Operation} eq "del" ) {
			$query="Delete ContainerOverride where element='".
					$args{System_LU}."' and name='".
					$args{Container}."'";
		} else {
			$query="if exists ( select * from Container where name='".$args{System_LU}."' )
	Insert ContainerOverride Values ('".
					$args{Container}."','c','".
					$args{System_LU}."',null)
else
	Insert ContainerOverride Values ('".
					$args{Container}."','s','".
					$args{System_LU}."',null)";
		}
		push @rc, $query;
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
			push @rc,"ERROR:".dbi_decode_row($_);
			push @rc,"$_ $DBI::err $DBI::errstr";
		}
		$query="exec build_container";
	} elsif( $args{Admin} eq "Program->Container" ) {
		if( $args{Program_LU} =~ /^\s*$/ or $args{Container_LU} =~ /^\s*$/ ) {
			push @rc,"ERROR: Not All Required Args Defined";
			return @rc;
		}

		if( $args{Operation} eq "del" ) {
			$query="Delete ContainerMap where monitor_program='".
					$args{Program_LU}."' and container='".
					$args{Container_LU}."'";
		} else {
			$query="Insert ContainerMap Values ('".
					$args{Program_LU}."','".
					$args{Container_LU}."')";
		}
		push @rc, $query;
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
			push @rc,"ERROR:".dbi_decode_row($_);
			push @rc,"$_ $DBI::err $DBI::errstr";
		}
		$query="exec build_container";
	} elsif( $args{Admin} eq "Delete By Row" ) {
		if( $args{Monitor} =~ /^\s*$/
				or $args{System} =~ /^\s*$/
				or $args{Type} =~ /^\s*$/ ) {
			push @rc,"ERROR: Not All Required Args Defined";
			return @rc;
		}

		$query="Delete $args{Type} where monitor_program='".
				$args{Monitor}."' and system='".
				$args{System}."'";
	} elsif( $args{Admin} eq "Alarm History" ) {
				push @rc,"ERROR: I havent Got Around To Coding This Yet";
				return @rc;
	} elsif( $args{Admin} eq "Report Setup" ) {
		if( $args{Operation} eq "del" ) {
			$query="DELETE ReportArgs where reportname='".  $args{Report_Name}."'";
		} elsif( $args{Operation} eq "add" ) {
			$query="Delete ReportArgs where reportname='". $args{Report_Name}."'";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
				push @rc,"ERROR:".join(" ",dbi_decode_row($_));
			}

			sub add_reportargs {
				my($connection,$reportname,$keyname,$value)=@_;
				my($q)="insert ReportArgs ( reportname,keyname,value ) values ('".
					$reportname."','".$keyname."','".$value."')";
				my(@x);
				push @x,$q;
				foreach ( dbi_query(-db=>$db,-query=>$q, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
					push @x,"ERROR:".join(" ",dbi_decode_row($_));
				}
				return @x;
			}
			push @rc,add_reportargs($connection,$args{Report_Name},'progname',$args{Program}) if $args{Program};
			push @rc,add_reportargs($connection,$args{Report_Name},'filter_severity',$args{Min_Severity_LU});
			push @rc,add_reportargs($connection,$args{Report_Name},'ProdChkBox',$args{Production_YN_LU});
			push @rc,add_reportargs($connection,$args{Report_Name},'filter_time',$args{Time_LU});
			push @rc,add_reportargs($connection,$args{Report_Name},'ShowProgram',$args{Show_Program_YN_LU});
			push @rc,add_reportargs($connection,$args{Report_Name},'ReportTitle',$args{Title});
			push @rc,add_reportargs($connection,$args{Report_Name},'filter_container',$args{Container_LU});
			push @rc,add_reportargs($connection,$args{Report_Name},'radio_screen',$args{RptType_LU});
			push @rc,add_reportargs($connection,$args{Report_Name},'system',$args{SysName}) if $args{SysName};
			push @rc,add_reportargs($connection,$args{Report_Name},'subsystem',$args{Subsystem}) if $args{Subsystem};

			$query=undef;
		}
	} elsif( $args{Admin} eq "Ignore List" ) {
		if( $args{Operation} eq "del" ) {
			$query="Delete IgnoreList where monitor_program";
			if( $args{Program_LU} =~ /^\s*$/ ) {
				$query .= " is null";
			} else {
				$query .= "='".$args{Program_LU}."'";
			}
			if( $args{System} =~ /^\s*$/ ) {
				$query.=" and system is null";
			} else {
				$query.=" and system = '".$args{System}."'";
			}
			if( $args{Subsystem} =~ /^\s*$/ ) {
				$query.=" and subsystem is null";
			} else {
				$query.=" and subsystem = '".$args{Subsystem}."'";
			}
		} else {
			$query="insert IgnoreList ( monitor_program,system,subsystem
				) values ('".  $args{Program_LU}."',";

			if( $args{System} =~ /^\s*$/ ) {
				$query.="null,";
			} else {
				$query.="'".$args{System}."',";
			}

			if( $args{Subsystem} =~ /^\s*$/ ) {
				$query.="null)";
			} else {
				$query.="'".$args{Subsystem}."')";
			}
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}
			$query="DELETE Heartbeat from IgnoreList i, Heartbeat h where i.monitor_program = h.monitor_program and isnull(i.system,h.system)=h.system and isnull(i.subsystem,h.subsystem)=h.subsystem";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}
			$query="DELETE Event from IgnoreList i, Event h where i.monitor_program = h.monitor_program and isnull(i.system,h.system)=h.system and isnull(i.subsystem,h.subsystem)=h.subsystem";
		}
	} elsif( $args{Admin} eq "Alarm Routing" ) {
		my($del_query)="Delete Notification Where user_name = '".
							$args{User_LU}."' and	monitor_program = '".$args{Program_LU}."' ";

		if( ! defined $args{INV_Business_System_LU} or $args{INV_Business_System_LU} eq "" ) {
			$del_query.= " and container is null";
		} else {
			$del_query.= " and container = '".$args{INV_Business_System_LU}."'";
		}
		if( ! defined $args{System} or $args{System} eq "" ) {
			$del_query.= " and system is null";
		} else {
			$del_query.= " and system = '".$args{System}."'";
		}

		if( $args{Operation} eq "del" ) {
			$query=$del_query." and	min_severity = '".$args{Min_Severity_LU}."' and	max_severity = '".$args{Max_Severity_LU}."'";
		} else {
			my($cnt)=$#rc;
			push @rc,"ERROR: Missing Argument User"
					if $args{User_LU}					=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Program"
					if $args{Program_LU}			=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Min Severity"
					if $args{Min_Severity_LU}	=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Max Severity"
					if $args{Max_Severity_LU}	=~ /^\s*$/;
			return @rc if $#rc ne $cnt;
			my($sys)="null";

			my($contr)="null";
			$contr="'".$args{INV_Business_System_LU}."'" if defined $args{INV_Business_System_LU} and $args{INV_Business_System_LU}!~/^\s*$/;
			$sys="'".$args{System}."'" if defined $args{System} and $args{System}!~/^\s*$/;
			push @rc,$del_query if  $args{-debug};
			foreach ( dbi_query(-db=>$db,-query=>$del_query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}

			$query="insert Notification ( user_name,monitor_program,system,subsystem,min_severity,
				max_severity,use_pager,use_email,use_netsend,use_blackberry,container,
				emergency_minutes,fatal_minutes,error_minutes,warning_minutes
				) values ('".
					$args{User_LU}."','".
					$args{Program_LU}."',".
					$sys.", null,'".
					$args{Min_Severity_LU}."','".
					$args{Max_Severity_LU}."','".
					"NO"."','".
					"YES"."','".
					"NO"."','".
					"YES"."',".
					$contr.",0,60,120,300)";
		}
	} elsif( $args{Admin} eq "Filter Repeated Messages" ) {
		$args{-EmergencyMin} = 0 unless $args{-EmergencyMin} 	=~ /^\d+$/;
	   $args{-FatalMin} 	= 60 	 unless $args{-FatalMin} 	=~ /^\d+$/;
    	$args{-ErrorMin} 	= 120  unless $args{-ErrorMin} 	=~ /^\d+$/;
    	$args{-WarnMin}	= 300  unless $args{-WarnMin} 		=~ /^\d+$/;

		$query="Update Notification SET
			emergency_minutes	= $args{-EmergencyMin},
			fatal_minutes 		= $args{-FatalMin},
			error_minutes 	   = $args{-ErrorMin},
			warning_minutes 	= $args{-WarnMin}
		Where user_name = '".$args{User}."'
		and   monitor_program    = '".$args{Program}."'
		and   isnull(container,'')    = '".$args{INV_Business_System_LU}."'
		and   isnull(system,'')    = '".$args{System}."'";

	} elsif( $args{Admin} eq "Operator" ) {
		if( $args{Operation} eq "del" ) {
			$query="Delete Notification where user_name='".$args{User}."'";
			push @rc,$query if  $args{-debug};
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}
			$query="Delete Operator where user_name='".$args{User}."'";
		} else {
			if( 	$args{User}					=~ /^\s*$/ or
					$args{Enabled_LU}			=~ /^\s*$/ ) {
				push @rc,"ERROR: Not All Required Args Defined";
				return @rc;
			}
			# $args{Pager} =~ s/\@/\\\@/;
			# $args{Email} =~ s/\@/\\\@/;
			# $args{Netsend} =~ s/\@/\\\@/;
			$query="insert Operator ( user_name,user_type,enabled,pager_email,email,netsend ) values ('".
					$args{User}."','".
					"Admin"."','".
					$args{Enabled_LU}."','".
					$args{Pager}."','".
					$args{Email}."','".
					$args{Netsend}."')";
		}

	} elsif( $args{Admin} eq "Operator Schedule" ) {
		$args{WD_End_LU} 	= 0 unless defined $args{WD_End_LU}  or defined $args{WD_Start_LU};
		$args{Sat_End_LU}  	= 0 unless defined $args{Sat_End_LU} or defined $args{Sat_Start_LU};
		$args{Sun_End_LU}  	= 0 unless defined $args{Sun_End_LU} or defined $args{Sun_Start_LU};
		$args{Hol_End_LU}  	= 0 unless defined $args{Hol_End_LU} or defined $args{Hol_Start_LU};

		$args{WD_Start_LU} 	= 0 unless defined $args{WD_Start_LU};
		$args{Sat_Start_LU} 	= 0 unless defined $args{Sat_Start_LU};
		$args{Sun_Start_LU}     = 0 unless defined $args{Sun_Start_LU};
		$args{Hol_Start_LU}     = 0 unless defined $args{Hol_Start_LU};

		$args{WD_End_LU} 	= 24 unless defined $args{WD_End_LU};
		$args{Sat_End_LU}  	= 24 unless defined $args{Sat_End_LU};
		$args{Sun_End_LU}  	= 24 unless defined $args{Sun_End_LU};
		$args{Hol_End_LU}  	= 24 unless defined $args{Hol_End_LU};

		if( $args{WD_Start_LU} == $args{WD_End_LU} ){
			$args{WD_Start_LU} 	= "null" ;
			$args{WD_End_LU} 	= "null";
		}

		if( $args{Sat_Start_LU} == $args{Sat_End_LU} ){
			$args{Sat_Start_LU} 	= "null";
			$args{Sat_End_LU}  	= "null";
		}

		if( $args{Sun_Start_LU} == $args{Sun_End_LU} ){
			$args{Sun_Start_LU}     = "null";
			$args{Sun_End_LU}  	= "null";
		}

		if( $args{Hol_Start_LU} == $args{Hol_End_LU} ){
			$args{Hol_End_LU}  	= "null";
			$args{Hol_Start_LU}     = "null";
		}

		$query="Update Operator SET
			weekday_start_time 	= $args{WD_Start_LU},
			weekday_end_time 	= $args{WD_End_LU},
			saturday_start_time 	= $args{Sat_Start_LU},
			saturday_end_time 	= $args{Sat_End_LU},
			sunday_start_time 	= $args{Sun_Start_LU},
			sunday_end_time 	= $args{Sun_End_LU},
			holiday_start_time	= $args{Hol_Start_LU},
			holiday_end_time 	= $args{Hol_End_LU}
		Where user_name = '".$args{User_LU}."'";

		if( $args{Operation} eq "del" ) {
			$query="Delete INV_business_system where business_system_name='".$args{Business_System_Name}."'";
		} else {
			if( 	$args{Business_System_Name} =~ /^\s*$/ ) {
				push @rc,"ERROR: Not All Required Args Defined - You Must Pick A Business System_Name";
				return @rc;
			}
			my($isv)= substr($args{Is_System_View_YN_LU},0,1);
			my($iv) = substr($args{Is_View_YN_LU},0,1);
			$query="Insert INV_business_system ( business_system_name,is_view,is_systemview,description ) values ('".
					$args{Business_System_Name}."','".
					$isv."','".
					$iv."','".
					$args{Description}."')";
		}
	} elsif( $args{Admin} eq "INV_BUSINESS_SYSTEM_MEMBERS" ) {
		if( $args{INV_Business_System_LU} =~ /^\s*$/ or ($args{INV_Hostname_LU} =~ /^\s*$/ and $args{INV_Database_LU} =~ /^\s*$/ )) {
			push @rc,"ERROR: Not All Required Args Defined - You Must Pick A Business System Name & A Database or Hostname";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}

		if( $args{INV_Hostname_LU} and $args{INV_Database_LU} ) {
			my($xstr)= "ERROR: Can Not Define Both A Hostname And Database";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { $xstr.="<br>Arg $_ => $args{$_} "; }
			}
			push @rc,$xstr;
			return @rc;
		}

		$query='';
		if( $args{INV_Hostname_LU} ) {
			if( $args{Operation} eq "del" ) {
				$query.="\nDelete INV_system_heirarchy
						  where business_system_name='".$args{INV_Business_System_LU}."'
						  and	system_name = '".$args{INV_Hostname_LU}."'
						  and	system_type not in ('sybase','oracle','sqlsvr')";
			} else {
				$query.="\nInsert INV_system_heirarchy ( business_system_name,system_name,system_type )
					select '".$args{INV_Business_System_LU}."',system_name, system_type
					from INV_system
					where system_name = '".$args{INV_Hostname_LU}."'
					and	system_type not in ('sybase','oracle','sqlsvr')";
			}
		}

		if( $args{INV_Database_LU} ) {
			if( $args{Operation} eq "del" ) {
				$query.="\nDelete INV_system_heirarchy
						  where business_system_name='".$args{INV_Business_System_LU}."'
						  and	system_name = '".$args{INV_Database_LU}."'
						  and	system_type in ('sybase','oracle','sqlsvr')";
			} else {
				$query.="\nInsert INV_system_heirarchy ( business_system_name,system_name,system_type )
					select '".$args{INV_Business_System_LU}."',system_name, system_type
					from INV_system
					where system_name = '".$args{INV_Database_LU}."'
					and	system_type in ('sybase','oracle','sqlsvr')";
			}
		}

		$query .= "\n\nexec INV_reset_system_sp\n";

	} elsif( $args{Admin} eq "INV_MANAGE_BUSSYSTEM" ) {
		if( ! $args{Business_System_Name} ) {
			my($xstr)= "ERROR: Must Provide Business System Name";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { $xstr.="<br>Arg $_ => $args{$_} "; }
			}
			push @rc,$xstr;
			return @rc;
		}

		if( $args{Operation} eq "add" ) {
			$testquery = 'select * from INV_business_system where business_system_name='.
						dbi_quote(-text=>$args{Business_System_Name}, -connection=>$connection);
			$query = "insert INV_business_system ( business_system_name,is_view,is_systemview )
			 values ( '".$args{Business_System_Name}."','N','N' )";
		} elsif( $args{Operation} eq "del" ) {
			$query = "delete INV_system_heirarchy where business_system_name = '".$args{Business_System_Name}."'";
			print $query,"\n" if $args{-debug};
			_querynoresults($query,1);
			$query = "delete INV_business_system  where business_system_name = '".$args{Business_System_Name}."'";
		} else {
			my($xstr)= "ERROR: Undefined Operation On Report $args{Admin} At Line ".__LINE__." of file ".__FILE__;
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { $xstr.="<br>Arg $_ => $args{$_}"; }
			}
			push @rc,$xstr;
			return @rc;
		}
	} elsif( $args{Admin} eq "INV_DEFINE_SYSTEMS" ) {
		if( ! $args{System_Name} or ! $args{INV_SYSTEM_TYPE_LU} or ! $args{INV_SERVER_TYPE_LU} ) {
			push @rc,"ERROR: Missing Required Arguments";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}

		if( $args{Operation} eq "add" ) {
#			foreach ( keys %args ) { push @rc, "Key=$_ Arg=$args{$_} "; }

			my($im)=substr($args{is_infra_monitored_LU} || "Y",0,1);
			my($ib)=substr($args{Is_Blackedout_LU} || "Y",0,1);

			$query = "insert INV_system
				( system_name, system_type, description, is_infra_monitored, production_state, is_blackedout )
			values ( ".
				dbi_quote(-text=>$args{System_Name}, 			-connection=>$connection).",".
				dbi_quote(-text=>$args{INV_SYSTEM_TYPE_LU}, 	-connection=>$connection).",".
				dbi_quote(-text=>$args{Txtbox30_Description},-connection=>$connection).",".
				dbi_quote(-text=>$im,						 		-connection=>$connection).",".
				dbi_quote(-text=>$args{INV_SERVER_TYPE_LU}, 	-connection=>$connection).",".
				dbi_quote(-text=>$ib, 								-connection=>$connection).")";
		} elsif( $args{Operation} eq "del" ) {
			$query = "delete INV_system where system_name = '".$args{System_Name}."' and system_type='". $args{INV_SYSTEM_TYPE_LU}."'";
		} else {
			push @rc, "ERROR: Undefined Operation On Report $args{Admin} At Line ".__LINE__." of file ".__FILE__;
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}

	} elsif( $args{Admin} eq "INV_DEFINE_SYSUSERS" ) {
		my($v)='N';
		$v='Y' if $args{Chkbox_Has_Access_LU} eq "on";
		if( ! $args{INV_LDAP_USER_LU} ) {
			push @rc,"ERROR: Must Select Person";
			#if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			#}
			return @rc;
		}
		if( ! $args{INV_PERMISSION_LU} ) {
			push @rc,"ERROR: Must Select Permission";
			#if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			#}
			return @rc;
		}

		if( $args{Operation} eq "add" ) {
			$testquery = 'select * from INV_systemuser where lower(samid)=lower('.
						dbi_quote(-text=>$args{INV_LDAP_USER_LU}, -connection=>$connection).") and permission=".
						dbi_quote(-text=>$args{INV_PERMISSION_LU}, -connection=>$connection);
			$query = "insert INV_systemuser ( samid, permission ) values ( '".$args{INV_LDAP_USER_LU}."','". $args{INV_PERMISSION_LU}."' )";
		} elsif( $args{Operation} eq "del" ) {
			$query = "delete INV_systemuser where samid = '".$args{INV_LDAP_USER_LU}."'	and  permission like '". $args{INV_PERMISSION_LU}."'";
		} else {
			push @rc, "ERROR: Undefined Operation On Report $args{Admin} At Line ".__LINE__." of file ".__FILE__;
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}
	} elsif( $args{Admin} eq "INV_NEW_SYSTEM_TRACKER" ) {
		my($systype)= $args{SYSTEM_TYPE} ||  $args{INV_SYSTEM_TYPE_LU} ;
		# foreach ( keys %args ) { print "param ",$_," ",$args{$_},"<br>"; }
		if( ! $args{System_Name} or ! $systype ) {
			push @rc,"ERROR - Not all required Values Saved";
			return @rc;
		}

		if( $args{Operation} eq "add" ) {
			$query = "delete INV_proposed_system where system_name = '".$args{System_Name}."'	and  system_type='". $systype."'\n";
			$query .= "insert INV_proposed_system ( system_name,system_type,description,hostname,waiting_on )
			values ( ".
			dbi_quote(-text=>$args{System_Name}, -connection=>$connection).",".
			dbi_quote(-text=>$systype, -connection=>$connection).",".
			dbi_quote(-text=>$args{Txtbox30_Description}, -connection=>$connection).",".
			dbi_quote(-text=>$args{Txtbox30_hostname}, -connection=>$connection).",".
			#dbi_quote(-text=>$args{Txtbox8_Scheduled_Date}, -connection=>$connection).",".
			#dbi_quote(-text=>$args{Txtbox8_Actual_Date}, -connection=>$connection).",".
			dbi_quote(-text=>$args{Waiting_On}, -connection=>$connection)
			." )";
		} elsif( $args{Operation} eq "del" ) {
			$query = "delete INV_proposed_system where system_name = '".$args{System_Name}."' and  system_type='". $systype."'";
		} else {
			push @rc, "ERROR: Undefined Operation On Report $args{Admin} At Line ".__LINE__." of file ".__FILE__;
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}

#	} elsif( $args{Admin} eq "INV_DEFINE_SYSGROUP" ) {
#
#		if( ! $args{INV_LDAP_GROUP_LU} or ! $args{INV_PERMISSION_LU} ) {
#			push @rc,"ERROR: Missing Required Arguments";
#			if( $args{-canunderstandsql} eq 'Y' ) {
#					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
#			}
#			return @rc;
#		}
#
#		$query = "
#select g.credential_name
#into 	#INV_DEFINE_SYSGROUP
#from 	INV_group_member g, INV_credential_attribute a
#where group_name='".$args{INV_LDAP_GROUP_LU}."'
#and 	group_type='ldap group'
#and 	a.credential_name=g.credential_name and a.credential_type=g.credential_type
#and 	attribute_key='disabled' and attribute_value='no'
#
#delete INV_systemuser
#from   INV_systemuser u, #INV_DEFINE_SYSGROUP g
#where  u.samid=g.credential_name
#and    permission = '".$args{INV_PERMISSION_LU}."'
#
#insert INV_systemuser ( samid, permission )
#select credential_name,'".$args{INV_PERMISSION_LU}."'
#from   #INV_DEFINE_SYSGROUP
#
#drop table #INV_DEFINE_SYSGROUP";

	} elsif( $args{Admin} eq "INV_GLOBAL_SHARED_ACCOUNT" or $args{Admin} eq "INV_LOCAL_SHARED_ACCOUNT" ) {
		if( ! $args{INV_Shared_Account_LU} ) {
			push @rc,"ERROR: Must Define Account Name";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}
		my(@system_types);
		if(  ! $args{INV_Credential_Type_LU}) {
			$query="select distinct credential_type from INV_principal_map where credential_name='".$args{INV_Shared_Account_LU} ."'";
			foreach ( _querywithresults($query,1) ) {
				push @system_types,dbi_decode_row($_);
			}

			if( $#system_types< 0 ) {
				$query="select distinct principal_type from INV_principal_map where principal_name='".$args{INV_Shared_Account_LU} ."'";
				foreach ( _querywithresults($query,1) ) {
					push @system_types,dbi_decode_row($_);
				}
			}

			if( $#system_types< 0 ) {
				push @rc,"NOTE : NO CREDENTIAL OR PRINCIPAL FOUND BY THE NAME OF $args{INV_Shared_Account_LU}";
				push @rc, $query;
				return @rc;
			} else {
				push @rc,"NOTE : NO CREDENTIAL TYPE PASSED. INSERTING FOR ".	join(" ",@system_types);
			}
		}	else {
			push @system_types,$args{INV_Credential_Type_LU};
		}
		if( ! $args{INV_LDAP_USER_LU} and ! $args{INV_LDAP_GROUP_LU} ) {
			push @rc,"ERROR: Not All Required Args Defined - Must Define User or Group";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}
		if( $args{INV_LDAP_USER_LU} and $args{INV_LDAP_GROUP_LU} ) {
			push @rc,"ERROR: Can Not Define BOTH  User and Group";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}

		$query='';
		foreach my $systyp ( @system_types ) {

			if( $args{Operation} eq "add" ) {
				$query .= "IF NOT EXISTS (
				select 1
				from INV_credential_notes
				where credential_name = '".$args{INV_Shared_Account_LU} ."'
				and credential_type   = '".$systyp ."' )
				insert INV_credential_notes ( credential_name, credential_type )
				values ('".$args{INV_Shared_Account_LU} ."','".$systyp."' )
				";
			}

			if( $args{INV_LDAP_GROUP_LU} ) {
				if( $args{Operation} eq "del" ) {
					$query.="UPDATE INV_credential_notes set is_ldap_group='N', ldap_group=null
					where credential_name = '".$args{INV_Shared_Account_LU} ."'
					and ldap_group   = '".$args{INV_LDAP_GROUP_LU}."'
					and credential_type   = '".$systyp."'";
				} else {
					$query.="UPDATE INV_credential_notes set is_ldap_group='Y',
					 ldap_group='".$args{INV_LDAP_GROUP_LU}."'
					where credential_name = '".$args{INV_Shared_Account_LU} ."'
					and credential_type   = '".$systyp."'";
				}
			} elsif( $args{INV_LDAP_USER_LU} ) {
				if( $args{Operation} eq "del" ) {
					$query.="UPDATE INV_credential_notes set is_ldap_user='N', ldap_user=null
					where credential_name = '".$args{INV_Shared_Account_LU} ."'
					and ldap_user   = '".$args{INV_LDAP_USER_LU}."'
					and credential_type   = '".$systyp."'";
				} else {
					$query.="UPDATE INV_credential_notes set is_ldap_user='Y',
					 ldap_user='".$args{INV_LDAP_USER_LU}."'
					where credential_name = '".$args{INV_Shared_Account_LU} ."'
					and credential_type   = '".$systyp."'";
				}
			} else {
				return "ERROR - NO LDAP USER OR LDAP GROUP DEFINED\n";
			}
		}
	} elsif( $args{Admin} eq "INV_DEFINE_AUD_CERT" ) {
		if( ! $args{INV_CERTIFIER_LU} ) {
			push @rc,"ERROR: Must Define Who Will Certify";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}

		my($cnt)=0;
		$cnt++ if $args{INV_Database_LU};
		$cnt++ if $args{INV_Hostname_LU};
		$cnt++ if $args{INV_LDAP_GROUP_LU};
		if( $cnt>1 ) {
			push @rc,"ERROR: Must Define Only 1 Thing To Certify Per Click";
			if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
			return @rc;
		}

		if( $args{Operation} eq "del" ) {
			if( $args{INV_Database_LU} ) {
		 		$query="DELETE INV_required_certification where samid='".$args{INV_CERTIFIER_LU}.
		 		"' and certification_type='DATABASE' and certification_target='".$args{INV_Database_LU}."'";
			} elsif( $args{INV_Hostname_LU} ){
				$query="DELETE INV_required_certification where samid='".$args{INV_CERTIFIER_LU}.
				"' and certification_type='HOSTNAME' and certification_target='".$args{INV_Hostname_LU}."'";
			} elsif( $args{INV_LDAP_GROUP_LU} ){
				$query="DELETE INV_required_certification where samid='".$args{INV_CERTIFIER_LU}.
				"' and certification_type='GROUP'    and certification_target='".$args{INV_LDAP_GROUP_LU}."'";
			} else {
				push @rc,"ERROR: Must Define What To Certify";
				if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
				}
				return @rc;
			}
		} else {
			if( ! $args{INV_FREQUENCY_LU} ) {
				push @rc,"ERROR: Must Define Frequency";
				if( $args{-canunderstandsql} eq 'Y' ) {
					if( $args{-canunderstandsql} eq 'Y' ) {
						foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
					}
				}
				return @rc;
			}
			$testquery = "SELECT * from INV_required_certification where samid='".$args{INV_CERTIFIER_LU}."' and ";

		 	if( $args{INV_Database_LU} ) {
		 		$testquery .= "certification_type='DATABASE' and certification_target='".$args{INV_Database_LU}."'";
		 		$query="INSERT INV_required_certification ( samid, how_often_required, certification_type, certification_target)
		 		values ( '".$args{INV_CERTIFIER_LU}."','".$args{INV_FREQUENCY_LU}. "','DATABASE','".$args{INV_Database_LU}."')";
			} elsif( $args{INV_Hostname_LU} ){
				$testquery .= "certification_type='HOSTNAME' and certification_target='".$args{INV_Hostname_LU}."'";
		 		$query="INSERT INV_required_certification ( samid, how_often_required,certification_type, certification_target)
		 		values ( '".$args{INV_CERTIFIER_LU}."','".$args{INV_FREQUENCY_LU}. "','HOSTNAME','".$args{INV_Hostname_LU}."')";
			} elsif( $args{INV_LDAP_GROUP_LU} ){
				$testquery .= "certification_type='GROUP' and certification_target='".$args{INV_LDAP_GROUP_LU}."'";
		 		$query="INSERT INV_required_certification ( samid,how_often_required,certification_type, certification_target)
		 		values ( '".$args{INV_CERTIFIER_LU}."','".$args{INV_FREQUENCY_LU}. "','GROUP','".$args{INV_LDAP_GROUP_LU}."')";
			} else {
				push @rc,"ERROR: Must Define What To Certify";
				if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
				}
				return @rc;
			}
		}
	} elsif( $args{Admin} =~ /^INV/ ) {
		push @rc, "ERROR: Uncoded Report $args{Admin} At Line ".__LINE__." of file ".__FILE__;
		if( $args{-canunderstandsql} eq 'Y' ) {
					foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
			}
		return @rc;
	} else {
		push @rc, "ERROR: Unknown Calling Screen ($args{Admin}) At Line ".__LINE__." of file ".__FILE__;
		if( $args{-canunderstandsql} eq 'Y' ) {
			foreach ( keys %args ) { push @rc,"Arg $_ => $args{$_}"; }
		}
		return @rc;
	}

	if( defined $testquery ) {
		push @rc, "<FONT COLOR=GREEN>Line ".__LINE__." TestQuery:".$testquery."</FONT>" if $args{-canunderstandsql} eq 'Y';
		foreach ( dbi_query(-db=>$db,-query=>$testquery, -connection=>$connection, -die_on_error=>0 )) {
			push @rc, "ERROR: Duplicate Data Insert Attempt - the data you want to modify exists";
			return @rc;
		}
	}

	if( defined $query ) {
		push @rc, "<FONT COLOR=GREEN>Line ".__LINE__." ".$query."</FONT>" if $args{-canunderstandsql} eq 'Y';
		#-no_results=>1,
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
			my($x)=join(" ",dbi_decode_row($_));
			push @rc,"ERROR: $x $DBI::err $DBI::errstr";
		}
		return @rc;
		push @rc,"ERROR: $_ $DBI::err $DBI::errstr" if defined $DBI::err;
	}

	if( $args{-debug} ) {
		print "(-debug) MlpManageData() completed at=".localtime()."\n";
	}
	return @rc;
}

sub MlpReportBackupJobRunInfo {
	my($columns,$whereclause,$latest_data_only)=@_;

	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Run MlpReportBackupJobRunInfo: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};

	my($a1)=("");	# FOR LATEST ONLY
	if( $latest_data_only ) {
			my($q2)="select JN=JobName,JID=JobBatchId,TM=max(JobStartTime)
					into #tmpBJH
					from BackupJobHistory group by JobName,JobBatchId";
			foreach ( dbi_query(-db=>$db,-query=>$q2,-no_results=>1,-connection=>$connection,-die_on_error=>0 )) {
				warn "WARNING: ".$q2 . "\nRETURNED: ". join(" ",dbi_decode_row($_));
			}
			$a1 = ", #tmpBJH";
			if( $whereclause ) {
				$whereclause .= " and JobName=JN and JobBatchId=JID and JobStartTime=TM";
			} else {
				$whereclause = " where JobName=JN and JobBatchId=JID and JobStartTime=TM";
			}
	}

	$whereclause = "" unless $whereclause;
	$columns="JobName,JobArguments,JobExitNotes,JobBatchId,JobServerName,JobStartTime,JobEndTime,Duration=JobDuration_secs,JobSuccessCode"
		unless $columns;
	my($q3)="Select $columns
											from BackupJobHistory $a1
											$whereclause
											order by JobName, JobBatchId, JobStartTime";
	#print "DBG DBG RUNNING $q3\n";
	my(@rc)= dbi_query_to_table(
				-table_options	=>	'border=1',
				-print_hdr		=> 1,
				-HDRREPEATROWS => 15,
				-connection=>$connection,
				-die_on_error=>0 ,
				-db				=>	$db,
				-query			=> $q3
	);

	if( $latest_data_only ) {
		my($q2)="drop table #tmpBJH";
		#print "DBG DBG RUNNING $q2\n";
		foreach ( dbi_query(-db=>$db,-query=>$q2,-no_results=>1,-connection=>$connection,-die_on_error=>0 )) {
			die $q2 . "\nRETURNED: ". join(" ",dbi_decode_row($_));
		}
	}
	return @rc;
}

sub MlpSaveBackupJobRunInfo {
	return 0  if $i_am_initialized eq "FAILED";

	my($JobName,$JobArguments,$JobExitNotes,$JobBatchId,$JobServerName,$JobStartTime,$JobEndTime,$JobDuration_secs,$JobSuccessCode,
		$TRAN_DUMPS_secs,$COMPRESS_secs,$PURGE_secs,$DBCC_secs,$CLEAR_TRAN_LOGS_secs,
		$FULL_DB_DUMP_secs,$REBUILD_REORG_secs,$UPDATE_STATISTICS_secs,$AUDIT_secs,$RECONNECT_secs,
		$REBUILD_INDEXES_secs,$BCP_secs,$CROSS_CHECK_secs ) = @_;
	chomp $JobExitNotes;
	#print "DBG DBG ARGS",join("!",@_),"\n";

	my( $q2 )= "insert BackupJobHistory values (	'".
		$JobName."',\n".
		dbi_quote(-text=>$JobArguments, -connection=>$connection).",\n".
		dbi_quote(-text=>$JobExitNotes, -connection=>$connection).",\n'".
		$JobBatchId."',\n'".
		$JobServerName."',\n'".
		$JobStartTime."',\n'".
		$JobEndTime."',\n".
		$JobDuration_secs.",\n'".
		$JobSuccessCode."',".
		"$TRAN_DUMPS_secs,
		$PURGE_secs,
		$DBCC_secs,
		$CLEAR_TRAN_LOGS_secs,
		$FULL_DB_DUMP_secs,
		$REBUILD_REORG_secs,
		$UPDATE_STATISTICS_secs,
		$AUDIT_secs,
		$RECONNECT_secs,
		$COMPRESS_secs,
		$REBUILD_INDEXES_secs,
		$BCP_secs,
		$CROSS_CHECK_secs ) ";

	#print "DBG DBG QUERY=",$q2,"\n";
	foreach ( dbi_query(-db=>$db,-query=>$q2, -no_results=>1,
						-connection=>$connection, -die_on_error=>0 )) {
		warn "WARNING: ".$q2 . "\nRETURNED: ". join(" ",dbi_decode_row($_));
	}
}

sub MlpCleanup {
	my(%args1)=@_;
	print "Calling MlpCleanup()\n";
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpCleanup",%args1);

	if( $args{-cleanservers} ) {
		# CLOBBER IP ADDRESSES
		my(@rc);
		#my($query)= "SELECT system from ProductionServers where system not like '%[A-Za-z]%' and system like '%[0-9]%'\n";
		#print $query;# if $args{-debug};
		#foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		#	push @rc, "Deleting System ".join("",dbi_decode_row($_));
		#}

		#($query)= "DELETE ProductionServers where system not like '%[A-Za-z]%' and system like '%[0-9]%'\n";
		#print $query;# if $args{-debug};
		#foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
		#	push @rc, dbi_decode_row($_);
		#}
		my($query)= "DELETE Event where system not like '%[A-Za-z]%' and system like '%[0-9]%'\n";
		print $query;# if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
			push @rc, dbi_decode_row($_);
		}
		($query)= "DELETE Heartbeat where system not like '%[A-Za-z]%' and system like '%[0-9]%'\n";
		print $query;# if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
			push @rc, dbi_decode_row($_);
		}

#		($query)= "select system from ProductionServers where is_production=0
#  and system not in ( select distinct servername from ServerChanges )
#  and system not in ( select distinct servername from ServerInfo )
#  and system not in ( select distinct system from Event )
#  and system not in ( select distinct system from Heartbeat )
#  and system not in ( select distinct system from Blackout )
#  and system not in ( select distinct system from IgnoreList where system is not null)
#  \n";
#		print $query;# if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
#			push @rc, "Deleting System ".join("",dbi_decode_row($_));
#		}
#
#		($query)= "delete ProductionServers where is_production=0
#  and system not in ( select distinct servername from ServerChanges )
#  and system not in ( select distinct servername from ServerInfo )
#  and system not in ( select distinct system from Event )
#  and system not in ( select distinct system from Heartbeat )
#  and system not in ( select distinct system from Blackout )
#  and system not in ( select distinct system from IgnoreList where system is not null)
#  and system not like '%.pl' \n";
#		print $query;# if $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
#			push @rc, "Deleting System ".join("",dbi_decode_row($_));
#		}
#
#		return @rc;
	}

	$args{-evdays} = $args{-days} unless defined $args{-evdays};
	$args{-hbdays} = $args{-days} unless defined $args{-hbdays};

	my(@rc);
		
	if( $args{-evdays} ) {
		my($query)= "DELETE Event where datediff(dd,event_time,getdate()) > $args{-evdays}\n";
		print $query;# if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
			push @rc, dbi_decode_row($_);
		}
	}
	if( $args{-hbdays} ) {
	my($query)= "DELETE Heartbeat where datediff(dd,monitor_time,getdate()) > $args{-hbdays} and monitor_program not in (select monitor_program from HeartbeatThresh)\n";
		print $query;	# if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
			push @rc, dbi_decode_row($_);
		}

		$query= "DELETE Heartbeat where datediff(dd,monitor_time,getdate()) > 2*$args{-hbdays}\n";
		print $query;	# if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
			push @rc, dbi_decode_row($_);
		}
	}

	#$query= "DELETE AgentHistory where datediff(dd,start_time,getdate())> $args{-days}\n";
	#print $query;	# if $args{-debug};
	#foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
	#	push @rc, dbi_decode_row($_);
	#}

	my($query)="UPDATE Heartbeat
	set state=\'ERROR\', message_text=\'(No Recent Heartbeat) \'+ message_text
	from HeartbeatThresh t, Heartbeat h
	where t.monitor_program=h.monitor_program
	and   h.state in (\'OK\',\'COMPLETED\',\'RUNNING\')
	and   ( t.system is null or t.system=h.system)
	and   ( t.subsystem is null or t.subsystem=h.subsystem)
	and datediff(mi,h.last_ok_time,getdate())>threshold_min\n";

	print $query;	# if $args{-debug};
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug},-no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}

	if( $args{-days} ) {
		my($query)= "DELETE BackupJobHistory where datediff(dd,JobStartTime,getdate())>$args{-days}\n";
		print $query;	# if $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
			push @rc, dbi_decode_row($_);
		}
	}
	return @rc;
}

sub MlpBatchStep
{
	my(%args)=@_;
	my(%x);

	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}
	warn "Should Not Call MlpBatchStep()\n";
	%args= validate_params("MlpBatchStep",%args);
	$job_curstep 		= $args{-stepnum};
	$job_curstepname 	= $args{-stepname};

	foreach (keys %args ) {
		if( $_ eq "-stepname" ) {
			$x{-subsystem}=$args{$_};
		} else {
			$x{$_}=$args{$_};
		}
	}
	foreach (keys %mlp_batch_args ) {
		#print "BATCH ARGS $_ => $mlp_batch_args{$_}\n";
		$x{$_}=$mlp_batch_args{$_} unless defined $x{$_};
	}
	$x{-message}="Job=$x{-system} Step=$job_curstep Name=$job_curstepname Has Been Started" unless defined $x{-message};

	MlpHeartbeat(		-monitor_program=>$x{-monitor_program},
				-system=>$x{-system},
				-subsystem=>$x{-subsystem},
				-state=>"RUNNING",
				-debug=>$x{-debug},
				-batchjob=>$x{-batchjob},
				-message_text=>$x{-message});
}

sub _querywithresults {
	my($query, $dont_show_query, $dbnm)=@_;
	initialize() unless $i_am_initialized eq "TRUE";
	$dbnm = $db unless $dbnm;

	print $query,"\n" unless $dont_show_query;
	my(@rc)= dbi_query(
		-db=>$dbnm,
		-query=>$query,
		#-debug=>$Margs{-debug},
		-connection=>$connection,
		-die_on_error=>0 );
	return @rc;
}

sub _querywithresultsP {
	my($query, $dont_show_query, $dbnm)=@_;
	initializeP() unless $i_am_initializedP eq "TRUE";
	$dbnm = $dbP unless $dbnm;

	print $query,"\n" unless $dont_show_query;
	my(@rc)= dbi_query(
		-db=>$dbnm,
		-query=>$query,
		#-debug=>$Margs{-debug},
		-connection=>$connectionP,
		-die_on_error=>0 );
	return @rc;
}

sub MlpGetLookupInfo {
	my($lookup)=@_;
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my($query) = "select value from INV_constant where name='".$lookup."_LU' order by value";
	my(@rc);
	foreach ( _querywithresults($query,1) ) {
		my($x)=dbi_decode_row($_);
		$x=~s/\s+$//g;
		$x=~s/^\s+//g;
		push @rc,$x;
	}
	#print "DBGDBG",join("|",@rc),"<br>";
	return @rc;
}

sub _quote {
	my($txt)=@_;
	return dbi_quote(-text=>$txt, -connection=>$connection);
}

sub _querynoresultsP {
	my($query, $dont_show_query)=@_;
	initializeP() unless $i_am_initializedP eq "TRUE";

	print $query,"\n" unless $dont_show_query;
	foreach ( dbi_query(
		-db=>$dbP,
		-query=>$query,
		#-debug=>$Margs{-debug},
		-no_results => 1,
		-connection=>$connectionP,
		-die_on_error=>0 )
	) {
		my($rc)=join("\n--> ",dbi_decode_row($_));
		my($msg)="ERROR - query should not have returned data\n --> query=$query\n--> ".$rc."\n";
		MlpBatchJobEnd(-EXIT_CODE => "FAILED", -EXIT_NOTES =>$msg) 	unless $query=~/INV_batchjob_/;
		die  $msg;
	}
}

sub _querynoresults {
	my($query, $dont_show_query)=@_;
	initialize() unless $i_am_initialized eq "TRUE";

	print $query,"\n" unless $dont_show_query;
	foreach ( dbi_query(
		-db=>$db,
		-query=>$query,
		#-debug=>$Margs{-debug},
		-no_results => 1,
		-connection=>$connection,
		-die_on_error=>0 )
	) {
		my($rc)=join("\n--> ",dbi_decode_row($_));
		my($msg)="ERROR - query should not have returned data\n --> query=$query\n--> ".$rc."\n";
		MlpBatchJobEnd(-EXIT_CODE => "FAILED", -EXIT_NOTES =>$msg) 	unless $query=~/INV_batchjob_/;
		die  $msg;
	}
}

sub MlpClearAuditResult {
	my(%args)=@_;
	initialize() unless $i_am_initialized eq "TRUE";
	my($query)='delete system_audit_result where system_name='.
			dbi_quote(-text=>$args{-system_name}, -connection=>$connection).
			' and system_type='+dbi_quote(-text=>$args{-system_type}, -connection=>$connection).
			' and monitor_program='+dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection);
	_querynoresults( $query,1 );

}


sub MlpSetAuditResult {
	my(%args)=@_;
	initialize() unless $i_am_initialized eq "TRUE";
	if( $args{-moddate} ) {
		$args{-moddate}="'".$args{-moddate}."'";
	} else {
		$args{-moddate}="getdate()"
	}
	my($query)="insert system_audit_result (system_name, system_type, monitor_program,
		database_name,  	error_level,  message_id,
		message_text,  	mod_date, 		mod_by )
   values ( '".$args{-system_name}."','".$args{-system_type}."','".$args{-monitor_program}."','".
  		$args{ -database_name}."','".$args{-error_level}."','".$args{-message_id}."','".
   	$args{-message_text}."',".$args{-mod_date}.",'".$args{-mod_by}."' )";
  	_querynoresults( $query,1 );
}

sub MlpLoadRepository
{
	my(%Margs)=@_;
	print "Calling MlpLoadRepository to sync inventory repostiory\n";
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Run : Connection Was Marked Failed!\n(this should not abort your program)\n"	unless defined $quiet;
		return 0;
	};
	use Repository;

	# this is used for systems we cant determine the production status of...
	my(%CLUSTERNODES);
#	my($q)="delete INV_aliases where created_by='MlpLoadRepository'";
#	_querynoresults( $q );

#	my($file)=get_gem_root_dir()."/conf/A_CLUSTERNODES.dat";
#	die "CANT READ $file\n" if -e $file and ! -r $file;
#	open(AX,$file) or die "Cant Read $file $!\n";
#	while( <AX> ) {
#		next if /^#/;
#		my($cluname, $hostname, $isprimary)=split;
#		$CLUSTERNODES{$hostname}=$cluname;
#		my($q)="insert INV_aliases values ('".$cluname."','win32servers','".$hostname."','MlpLoadRepository')";
#		_querynoresults( $q );
#		$q="insert INV_aliases values ('".$hostname."','win32servers','".$cluname."','MlpLoadRepository')";
#		_querynoresults( $q );
#		$q="insert INV_aliases values ('".$cluname."','sqlsvr','".$hostname."','MlpLoadRepository')";
#		_querynoresults( $q );
#		$q="insert INV_aliases values ('".$hostname."','sqlsvr','".$cluname."','MlpLoadRepository')";
#		_querynoresults( $q );
#	}
#	close(AX);

	my(@d) = _querywithresults( "SELECT * FROM INV_policy_defn where policy_name in ('sqlsvr_production','sybase_production','sqlsvr_development','sybase_development','sqlsvr_dr','sybase_dr') ",1 );
	my($FOUND_POLICIES)='FALSE';
	foreach ( @d ) { $FOUND_POLICIES='TRUE'; }
	
	if( $FOUND_POLICIES eq "TRUE" ) {
		print "*************************************************************************\n";
		print "* MlpLoadRepository(): SKIPPING DEFAULT POLICIES (allready loaded)      *\n";
		print "*************************************************************************\n\n";
		print "To force a reload of the default audit policies, delete INV_policy_defn\n";
	} else {
		print "*************************************************************************\n";
		print "* MlpLoadRepository(): LOADING DEFAULT POLICIES (this is only run once) *\n";
		print "*************************************************************************\n\n";
		my(@auditdat)=get_conf_file_dat("AuditMessage");
		my($curtype)=1;
		my(%warnings);
		_querynoresults( "select * into #INV_policy_defn from INV_policy_defn",1 );
		_querynoresults( "DELETE INV_policy_defn where policy_name in ('sqlsvr_production','sybase_production',
	      'sqlsvr_development','sybase_development','sqlsvr_dr','sybase_dr') ",1 );
		foreach (@auditdat) {
				# two types of lines...
				chomp;
				$curtype=2 if $curtype==1 and /,/;
				if( $curtype==1 ) {
					s/\#.+$//;
					s/\s+//g;
					$warnings{$_}=1;
				} else {
					my($msgid,$value)=split(/,/,$_,2);
					my($err)=",'ERROR')";
					$err=",'OK')" if $warnings{$msgid};
					_querynoresults( "Insert INV_policy_defn Values('sqlsvr_production',$msgid,\n\t".$connection->quote($value).$err ,1 );
					_querynoresults( "Insert INV_policy_defn Values('sybase_production',$msgid,\n\t".$connection->quote($value).$err ,1 );
	
					_querynoresults( "Insert INV_policy_defn Values('sqlsvr_development',$msgid,\n\t".$connection->quote($value).$err ,1 );
					_querynoresults( "Insert INV_policy_defn Values('sybase_development',$msgid,\n\t".$connection->quote($value).$err ,1 );
	
					_querynoresults( "Insert INV_policy_defn Values('sqlsvr_dr',$msgid,\n\t".$connection->quote($value).$err ,1 );
					_querynoresults( "Insert INV_policy_defn Values('sybase_dr',$msgid,\n\t".$connection->quote($value).$err ,1 );
				}
		}
		_querynoresults( "UPDATE INV_policy_defn set error_level=t.error_level
		from INV_policy_defn p, #INV_policy_defn t
		where t.policy_name=p.policy_name 
		and t.message_id=p.message_id
		and p.policy_name in ('sqlsvr_production','sybase_production','sqlsvr_development',
	         'sybase_development','sqlsvr_dr','sybase_dr')" ,1 );
   }

#	# ok lets read the configuration file
#	_querynoresults( "Select * into #INV_system from INV_system where 1=2");
#	_querynoresults( "Select * into #INV_system_attribute from INV_system_attribute where 1=2");
#
#	foreach my $system_type ( get_passtype() ) {
#		next if $system_type eq "documenter";
#		my(@system_names)=get_password(-type=>$system_type);
#		foreach my $system_name (@system_names) {
#			_querynoresults( "Insert #INV_system \n\tValues('".$system_name."','".$system_type."',null,null,'Y','?','N')" );
#			my($l,$p)=get_password(-type=>$system_type,-name=>$system_name);
#			_querynoresults( "Insert #INV_system_attribute\n\tValues('LOGIN','".$system_name."','".$system_type."','".$l."','FILEREPOSITORY',\n\tgetdate(),'MlpLoadRepository')"  )  if $l and $system_type ne "sqlsvr";
#			_querynoresults( "Insert #INV_system_attribute\n\tValues('PASSWORD','".$system_name."','".$system_type."','".$p."','FILEREPOSITORY',\n\tgetdate(),'MlpLoadRepository')" ) if $p and $system_type ne "sqlsvr";
#			my(%args)=get_password_info(-type=>$system_type,-name=>$system_name);
#			foreach ( keys %args ) {
#				if( $args{NOT_FOUND} ) {
#					warn "NO attributes found for $system_name ($system_type)\n";
#					next;
#				}
#				_querynoresults( "Insert #INV_system_attribute\n\t\tValues('".uc($_)."','".$system_name."','".$system_type."','".$args{$_}."','FILEREPOSITORY',\n\tgetdate(),'MlpLoadRepository')" );
#			}
#   	}
#   }

#   print "**************************\n";
#	print "* PROCESSING DELETES     *\n";
#	print "**************************\n";
#
#   _querynoresults( "\tselect system_name,system_type into #INV_deletes from INV_system");
#   _querynoresults( "\tdelete #INV_deletes from #INV_deletes d, #INV_system s \twhere d.system_name=s.system_name and d.system_type=s.system_type");
#   _querynoresults( "\tupdate INV_system set is_blackedout='Y'
#\tfrom INV_system s, #INV_deletes d\twhere d.system_name=s.system_name and d.system_type=s.system_type");
#
#   # DELETE NEW ROWS THAT ARE IDENTICAL
#   _querynoresults("delete #INV_system_attribute from #INV_system_attribute a, INV_system_attribute b
#\twhere a.attribute_name=b.attribute_name and a.system_type=b.system_type
#\tand a.system_name=b.system_name and a.attribute_value=b.attribute_value");
#
#   _querynoresults("delete #INV_system from #INV_system a, INV_system b
#\twhere a.system_type=b.system_type and a.system_name=b.system_name");
#
#   # DELETE OLD ROWS WHERE DIFFERENT VALUES TO ALLOW KEYS TO WORK
#   _querynoresults("delete INV_system_attribute from #INV_system_attribute a, INV_system_attribute b
#\twhere a.attribute_name=b.attribute_name and a.system_type=b.system_type and a.system_name=b.system_name");
#
#   print "**************************\n";
#	print "* INSERTING NEW SYSTEM   *\n";
#	print "**************************\n";
#
#   _querynoresults("insert INV_system select * from #INV_system");
#   _querynoresults("insert INV_system_attribute select * from #INV_system_attribute");
#
#   _querynoresults("update INV_system set production_state=attribute_value
#	from INV_system s, INV_system_attribute a
#	where  a.system_name=s.system_name	and	 a.system_type=s.system_type
#	and	 a.attribute_name='SERVER_TYPE'");
#
#	_querynoresults("-- update win32servers production state to the production state of their database\n
#update 	INV_system set production_state = b.production_state
#from 	INV_system a, INV_system b
#where 	a.system_type='win32servers'
#and		b.system_type='sqlsvr'
#and		a.system_name=b.system_name
#and		a.production_state='?'");
#
#	_querynoresults("-- if the database has not the same name as the server (ie. cluster) use X_HOSTNAME\n
#update 	INV_system set production_state = b.production_state
#from 	INV_system a, INV_system b, INV_system_attribute c
#where 	b.system_type='sqlsvr'
#and		c.attribute_name='X_HOSTNAME'
#and		b.system_name=c.system_name
#and		b.system_type=c.system_type
#and		c.attribute_value !=  null
#and		c.attribute_value != 'NULL'
#and      b.system_name != c.attribute_value
#and      a.system_name = c.attribute_value
#and		a.production_state='?'");
#
## ok get the list of systems with production_state='?' and maybe CLUSTERNODES has it...
#my(@rc)=_querywithresults("SELECT system_name,system_type from INV_system where production_state='?'");
#foreach (@rc) {
#	my($sysname,$systype)=dbi_decode_row($_);
#	next unless  defined $CLUSTERNODES{$sysname};
#	#print "SYSTEM=$sysname  TYPE=$systype UNDEFINED - $CLUSTERNODES{$sysname}\n";
#
#_querynoresults("update INV_system
#set    production_state=b.production_state
#from   INV_system a, INV_system b
#where  a.system_name='".$sysname."'
#and    a.production_state='?'
#and    a.system_type='".$systype."'
#and    b.system_name='".$CLUSTERNODES{$sysname}."'
#and    b.production_state != '?'
#and    b.system_type='sqlsvr'");
#
#_querynoresults("update INV_system
#set    production_state=b.production_state
#from   INV_system a, INV_system b
#where  a.system_name='".$sysname."'
#and    a.production_state='?'
#and    a.system_type='".$systype."'
#and    b.system_name='".$CLUSTERNODES{$sysname}."'
#and    b.production_state != '?'
#and    b.system_type='win32servers'");
#}
#
#   #_querynoresults("update INV_system set is_production='N', is_infra_monitored='Y' where is_blackedout='N'");
#   #_querynoresults("update INV_system set is_production='Y' from INV_system a, INV_system_attribute b
#   #where a.system_type=b.system_type and a.system_name=b.system_name
#   #and   b.attribute_value='PRODUCTION' and b.attribute_name='SERVER_TYPE'");
#
#	_querynoresults("update INV_system set is_infra_monitored='N' from INV_system a, INV_system_attribute b
#   where a.system_type=b.system_type and a.system_name=b.system_name
#   and   b.attribute_value='Y' and b.attribute_name='NOT_MONITORED'");
#
	# CHECK THAT ALL NEW ATTRIBUTES ARE OK!
#	my($query)=
#"\tselect distinct attribute_name,system_type into #Chk from INV_system_attribute
#	delete #Chk from #Chk,  INV_attribute_dictionary b where #Chk.attribute_name = b.attribute_name and #Chk.system_type=b.system_type
#	select * from #Chk
#";
#	foreach ( dbi_query(
#		-db=>$db,
#		-query=>$query,
#		-connection=>$connection,
#		-die_on_error=>0 )
#	) {
#		my($a,$st)=dbi_decode_row($_);
#		print "WARNING: Attribute $st.$a not in INV_attribute_dictionary\n";
#	}

#	print "**************************\n";
#	print "* DROP TEMPORARY TABLES  *\n";
#	print "**************************\n";
#	_querynoresults("drop table #INV_system");
#	_querynoresults("drop table #INV_system_attribute");

	print "**************************\n";
	print "* MlpLoadRepository(): LOAD DEFAULT SYSTEM_POLICY  *\n";
	print "**************************\n";

	_querynoresults( "Select * into #INV_system_policy from INV_system_policy where 1=2",1  );

	_querynoresults( "INSERT #INV_system_policy select system_name,system_type,'sqlsvr_production'
	from INV_system where production_state='PRODUCTION'  and system_type='sqlsvr' " ,1 );

	_querynoresults( "INSERT #INV_system_policy select system_name,system_type,'sqlsvr_development'
	from INV_system where production_state!='PRODUCTION' and system_type='sqlsvr' " ,1 );

	_querynoresults( "INSERT #INV_system_policy select system_name,system_type,'sybase_production'
	from INV_system where production_state='PRODUCTION'  and system_type='sybase' " ,1 );

	_querynoresults( "INSERT #INV_system_policy select system_name,system_type,'sybase_development'
	from INV_system where production_state!='PRODUCTION' and system_type='sybase' " ,1 );

	_querynoresults( "delete #INV_system_policy from #INV_system_policy p1, INV_system_policy p2
	where p1.system_name=p2.system_name and p1.system_type=p2.system_type" ,1 );

	_querynoresults( "insert INV_system_policy select * from #INV_system_policy" ,1 );


#	print "**********************************************\n";
#	print "* LOADING CONSTANTS AND POSSIBLE ATTRIBUTES  *\n";
#	print "**********************************************\n";
#	# find the file
#	my($confdir)=get_gem_root_dir();
#	die "$confdir does not exist\n" unless -d $confdir;
#	$confdir.="/ADMIN_SCRIPTS";
#	die "$confdir does not exist\n" unless -d $confdir;
#	$confdir.="/gemalarms";
#	die "$confdir does not exist\n" unless -d $confdir;
#	$confdir.="/INV_attribute_dictionary.dat";
#	die "FILE $confdir does not exist\n" unless -e $confdir;
#	die "FILE $confdir is not readable \n" unless -r $confdir;
#
#	open(F, $confdir) or die "cant open $confdir\n";
#	my($curbatch)="";
#	while(<F>) {
#		if( /^go/ ) {
#			print "---> executing:\n";
#			_querynoresults($curbatch) if $curbatch ne '';
#			$curbatch='';
#		} else {
#			$curbatch.=$_;
#		}
#	}
#	close(F);

	# for sql servers - hostname is by definition an alias
#	$q="select system_name,attribute_value from INV_system_attribute
#	where attribute_name like '%HOSTNAME' and system_name!=attribute_value and system_type='sqlsvr'
#	and attribute_value is not null and attribute_value!='NULL'";
#	@rc=_querywithresults($q);
#	foreach (@rc) {
#		my($sysname,$val)=dbi_decode_row($_);
#		next if $CLUSTERNODES{$sysname} eq $val or $CLUSTERNODES{$val} eq $sysname;
#		my($q)="insert INV_aliases values ('".$sysname."','sqlsvr','".$val."','MlpLoadRepository')";
#		_querynoresults( $q );
#		$q="insert INV_aliases values ('".$val."','sqlsvr','".$sysname."','MlpLoadRepository')";
#		_querynoresults( $q );
#	}

	print "**************************\n";
	print "* MlpLoadRepository(): Config Load Completed  *\n";
	print "**************************\n";
	return 1;
}

my($opsstarttime);
sub MlpLogOperationStart {
	my($op,$server,$db,$table)=@_;			# if no table then its the whole db that starts

	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}

	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Run : Connection Was Marked Failed!\n(this should not abort your program)\n"	unless defined $quiet;
		return 0;
	};

	$opsstarttime=time unless $opsstarttime;
	$table = '' if ! defined $table;
	my($query)="insert BackupJobStep select '".$op."','".$server."','".$db."','".$table."',\n\tgetdate(),null,".$opsstarttime.",'".$this_hostname."',null,null";
		print $query,"\n" if $DBGDBG;
	_querynoresults($query,1);
}

sub SetupTmpTable {
		my(%args)=@_;

		my($setupquery)="select system_name,system_type,is_ok='N' into #ServerSubset from INV_system where 1=1 \n";
		if( $args{-production} ) {
			$setupquery .= " and production_state='PRODUCTION'\n";
		}
		if( $args{-systype} ) {
			$setupquery .= " and system_type='".$args{-systype}."'\n";
		}
		$setupquery.="\n";
		my($prefix)='';
		if( $args{-system} and $args{-system}=~/\./ ) {
			my($syname,$sytyp)=split(/\./,$args{-system});
			$setupquery .= "update #ServerSubset set is_ok='Y'
 where system_name = '".$syname."'
 and   system_type = '".$sytyp."'";
		} elsif( $args{-system} ) {
			$setupquery .= "update #ServerSubset set is_ok='Y'
 where system_name = '".$args{-system}."'";
		} elsif( $args{-container} ) {
			$setupquery .= "update #ServerSubset set is_ok='Y' from #ServerSubset t, INV_system_heirarchy h
 where h.business_system_name='".$args{-container}."'
 and   h.system_name = t.system_name
 and   h.system_type = t.system_type";
			$prefix="h.";
		} else {
			$setupquery .= "update #ServerSubset set is_ok='Y' where 1=1 ";
		}
		if($args{-likefilter}) {
			$setupquery.= " and ".$prefix."system_name like '".$args{-likefilter}."'"
		}
		$setupquery.="\n\n";
		$setupquery.="delete #ServerSubset where is_ok='N'\n\n";
		$setupquery.="select distinct system_name from #ServerSubset \n\n";
		print _debugcolor($args{-html},$setupquery,"\n") if $args{-debug};
		my(@outarray);
		my($cnt)=1;
		foreach ( dbi_query(-db=>$db,-query=>$setupquery, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, $x[0];
			$cnt++;
		}
		if( $cnt==1 ) {
			print "<TABLE BORDER=1 WIDTH=100% BGCOLOR=WHITE><TR><TD ALIGN=LEFT>";
			print "<FONT face=sans-serif size=+1 color=red>";
			print "<h2>NO SERVERS FOUND MATCHING THE SPECIFIED CRITERION</h2>";
			print "The following sql query was used to set up the server for this report - it returned no rows";
			print "<PRE>",$setupquery,"</PRE></FONT></TD>\n</TR></TABLE><p>\n";
		}

		# print "$cnt servers match criterion<br>\n";
		# findit();
		return @outarray;
}

#sub findit {
#	my($setupquery)="select * from #ServerSubset";
#	print "<PRE>",$setupquery,"</PRE>";
#	my(@outarray);
#	my($cnt)=1;
#	print "CONNECTION=$connection\n";
#	foreach ( dbi_query(-db=>$db,-query=>$setupquery, -connection=>$connection, -die_on_error=>0 )) {
#		my(@x) = dbi_decode_row($_);
#		push @outarray, $x[0];
#		print $x[0],"<br>\n";
#		$cnt++;
#	}
#	print "$cnt servers match criterion<br>\n";
#}

sub MlpLogOperationEnd {
	my($op,$server,$db,$table,$batch_id,$notes)=@_;			# if no table then its the whole db that starts

	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}

	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Run : Connection Was Marked Failed!\n(this should not abort your program)\n"	unless defined $quiet;
		return 0;
	};

	$table = '' if ! defined $table;

	warn "Coding Error - opsstarttime not set in program - MlpLogOperationStart not called\n"
		unless $opsstarttime;
	my($query)="update BackupJobStep set endtime=getdate() ";
	$query .= ", batch_id='".$batch_id."'" if $batch_id;
	$query .= ", notes='".$notes."'" if defined $notes;
	$query .= "
	where endtime is null and operation='".$op."'
	and system='".$server."'
	and dbname='".$db."'
	and tblname='".$table."'
	and internalkey=$opsstarttime";

	print $query,"\n" if $DBGDBG;
	_querynoresults($query,1);
}

# sync the config files with the server
sub MlpSyncConfigFiles
{
	print "DEPRICATED FUNCTION MlpSyncConfigFiles() CALLED\n";
	return;
	 
	my(%Margs)=@_;
	print "Calling MlpSyncConfigFiles to sync configuration files with the database\n";
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Run : Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	# extract the table
	use Repository;
	my($config_directory)=get_gem_root_dir()."/conf";

	my($query)="select servername, chvalue from ServerChanges where chkey=\'is_production\' order by tstamp\n";
	print $query if $Margs{-debug};
	my(%is_prod_changes);
	foreach ( dbi_query( 	-db=>$db,
		-query=>$query,
		-debug=>$Margs{-debug},
		-connection=>$connection,
		-die_on_error=>0 )
	) {
		my(@dat)=dbi_decode_row($_);
		$is_prod_changes{$dat[0]} = $dat[1];
		print "Setting $dat[1] to |".$dat[0]."| \n";
	}

	my($Ofound)=0;
	if( $Margs{-operator} ) {
		$query="select user_name from Operator where user_name='GEMADMIN'";
		print $query if $Margs{-debug};
		foreach ( dbi_query( 	-db=>$db,
			-query=>$query,
			-debug=>$Margs{-debug},
			-connection=>$connection,
			-die_on_error=>1 )) {
				$Ofound++;
		}

		if( $Ofound==0 ) {
			$query="insert Operator ( user_name,user_type,enabled,pager_email,email,netsend )
					values ('GEMADMIN','Admin','YES','','".$Margs{-operator}."','')";
			print $query if $Margs{-debug};
			foreach ( dbi_query( 	-db=>$db,
				-query			=>$query,
				-debug			=>$Margs{-debug},
				-connection		=>$connection,
				-die_on_error	=>1 )
			) {
					warn "CANT INSERT OPERATOR: $query ERROR:".dbi_decode_row($_).	"\n";
			}

			foreach( "SYBASE","WIN32","UNIX","ORACLE","SQL_SERVER" ) {
				foreach ( MlpManageData(
					-debug				=>	$Margs{-debug},
					System				=>	"",
					Program_LU			=>	"All Programs",
					Use_Netsend_LU		=>	"NO",
					Use_Blackberry_LU => "NO",
					User_LU				=>	"GEMADMIN",
					Min_Severity_LU	=>"Errors",
					Subsystem			=>"",
					Max_Severity_LU	=>'Emergency',
					Use_Email_LU		=>"YES",
					Container			=>$_,
					Admin					=>"Alarm Routing",
					Use_Pager_LU		=>"NO",
					Operation			=>"add" ) ) {
						print ">>",$_,"\n";
				}
			}
		}
	}

	# ok lets read the configuration file
	foreach my $filetype ( get_passtype() ) {
		next if $filetype eq "documenter";

		print "Working on $filetype\n";
		my(%comm_method,%win32_comm_method, %server_type, %hostname, %type, %backupservername, %is_production);
		#my($query)="select servername, comm_method, win32_comm_method, server_type, hostname, type,
		#	backupservername, is_production
       #  	from ServerInfo i, ProductionServers p
        # 	where p.system =* i.servername
         #	and filetype=\'".$filetype."\'\n";
      my($query)="select servername, comm_method, win32_comm_method, server_type, hostname, type,
			backupservername, is_production
         	from ServerInfo i left outer join ProductionServers p on p.system = i.servername
         	where filetype=\'".$filetype."\'\n";

		print $query if $Margs{-debug};
		my($count)=0;
		foreach ( dbi_query( -db=>$db, -query=>$query, -debug=>$Margs{-debug}, -connection=>$connection, -die_on_error=>0 )) {
				my(@dat)=dbi_decode_row($_);

				if( $#dat < 4 ) {
					die "ERROR: $query Failed With ".join("\t",@dat)."\n";
				}

				$dat[1]=~s/\s//g;
				$dat[2]=~s/\s//g;
				$dat[3]=~s/\s//g;
				$dat[4]=~s/\s//g;
				$dat[5]=~s/\s//g;
				$dat[6]=~s/\s//g;
				$count++;

				$comm_method{$dat[0]} 		= $dat[1];
				$win32_comm_method{$dat[0]}= $dat[2];
				$server_type{$dat[0]} 		= $dat[3];
				$hostname{$dat[0]} 			= $dat[4];
				$type{$dat[0]} 				= $dat[5];
				$backupservername{$dat[0]} = $dat[6];
				$is_production{$dat[0]} 	= $dat[7];
		}

		print "$count servers found of type $filetype\n";
		my(@hosts)=get_password(-type=>$filetype);
		my(%file_overrides);
		my($file_changes)=0;
		foreach my $host (@hosts) {
			#die "HOST ($filetype) UNDEFINED FROM ",join("|",@hosts),"\n" unless $host;
			my(%args)=get_password_info(-type=>$filetype,-name=>$host);
			my(%newargs);
			$newargs{comm_method}="";
			$newargs{win32_comm_method}="";
			$newargs{server_type}="";
			$newargs{hostname}="";
			$newargs{type}="";
			$newargs{backupservername}="";
			foreach (keys %args) { 	$newargs{lc($_)} = $args{$_}; 	}
			$newargs{server_type}="DEVELOPMENT" unless $newargs{server_type};
			$newargs{comm_method} = $newargs{unix_comm_method};

			my($query);
			my($CONTAINER);
			if( $filetype eq "sybase" ) {
				$CONTAINER="SYBASE";
			} elsif($filetype eq "win32servers") {
				$CONTAINER="WIN32";
			} elsif($filetype eq "unix") {
				$CONTAINER="UNIX";
			} elsif($filetype eq "oracle") {
				$CONTAINER="ORACLE";
			} elsif($filetype eq "sqlsvr") {
				$CONTAINER="SQL_SERVER";
			}

			$query="if not exists (select 1 from ContainerOverride where name=\'".$CONTAINER."' and type='s' and element=\'".$host."\' ) Insert ContainerOverride Values ('".$CONTAINER."','s','".$host."',null)\n";
			foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
					die join( "~",dbi_decode_row($_) );
		   }

			next if $filetype eq "win32servers";

			if( $server_type{$host} ) {
				#print "Existing host $host!\n";
				my($changes_found)=0;
				# compare the results
				if( $comm_method{$host} ne $newargs{comm_method} ) {
					$changes_found++;
					print "comm_method: You have updated {".$comm_method{$host}."} to {".$newargs{comm_method}."}\n";
				}
				if( $win32_comm_method{$host} ne $newargs{win32_comm_method} ) {
					$changes_found++;
					print "win32_comm_method: You have updated {".$win32_comm_method{$host}."} to {".$newargs{win32_comm_method}."}\n";
				}

				if( defined $is_prod_changes{$host} ) {
					# print "DBG DBG is_prod_changes $host = $is_prod_changes{$host} \n";
					if( $is_prod_changes{$host} ) {
						$file_overrides{$host."|SERVER_TYPE"}="PRODUCTION";
						$file_changes++;
						$changes_found++ if $newargs{server_type} ne "PRODUCTION";
						$newargs{server_type} = "PRODUCTION"
					} else {
						$file_overrides{$host."|SERVER_TYPE"}="DEVELOPMENT";
						$file_changes++;
						$changes_found++ if $newargs{server_type} ne "DEVELOPMENT";
						$newargs{server_type} = "DEVELOPMENT";
					}
					# print "DBG DBG $host was set via web page to value $is_prod_changes{$host}\n";
				} elsif( $server_type{$host} ne $newargs{server_type} ) {
					$changes_found++;
					print "server_type: Updating Database For $host ".$server_type{$host}." to ".$newargs{server_type}."\n"
					 	if $Margs{-debug};

					my($p)=0;
					$p=1 if $newargs{server_type} eq "PRODUCTION";
					$query="UPDATE ProductionServers set is_production=$p where system=\'".$host."\'\n";
					foreach ( dbi_query(-db=>$db,-query=>$query,
						-connection=>$connection, -die_on_error=>0 )) {
						die join( dbi_decode_row($_) );
	   				}
				} elsif( $is_production{$host} eq "1" ) {
					if( $newargs{server_type} ne "PRODUCTION" ) {
						print "Setting Host $host To Be Development\n"  if $args{-debug};
						$query="UPDATE ProductionServers set is_production=0	where system=\'".$host."\'\n";
						print $query;
						foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},
							-connection=>$connection, -die_on_error=>0 )) {
							die join( "~",dbi_decode_row($_) );
		   				}

		   			#$query="select * from ProductionServers where system=\'".$host."\'\n";
						#print  $query if $Margs{-debug};
						#foreach ( dbi_query(-db=>$db,-query=>$query,-connection=>$connection, -die_on_error=>0 )) {
						#	print "DBG DBG",join( "~",dbi_decode_row($_) )."\n";
	   				#}
	   			}
	   		} elsif( $is_production{$host} eq "0" ) {
					if( $newargs{server_type} eq "PRODUCTION" ) {
						print "Setting Host $host in Database to be Production\n"  if $Margs{-debug};
						$query="UPDATE ProductionServers set is_production=1 where system=\'".$host."\'\n";
						print $query;
						foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},
							-connection=>$connection, -die_on_error=>0 )) {
							die join( "~",dbi_decode_row($_) );
		   				}
		   			#$query="select * from ProductionServers\n
						#	where system=\'".$host."\'\n";
						#print $query  if $args{-debug};
						#foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>0,
						#	-connection=>$connection, -die_on_error=>0 )) {
						#	print "DBG DBG",join( "~",dbi_decode_row($_) )."\n";
		   			#	}
	   				}
				} elsif( $is_production{$host} eq "" ) {
					# oops not found - so set it
					my($p)=0;
					$p=1 if $server_type{$host} eq "PRODUCTION";

					$query="insert ProductionServers values ('".$host."',$p)\n";
					print $query;
					foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},
						-connection=>$connection, -die_on_error=>0 )) {
						die join( "~",dbi_decode_row($_) );
	   				}
				}

				if( $hostname{$host} ne $newargs{hostname} ) {
					$changes_found++;
					print "hostname: You have updated ".$hostname{$host}." to ".$newargs{hostname}."\n";
				}
				if( $type{$host}  ne $newargs{type} ) {
					$changes_found++;
					print "host: You have updated ".$type{$host}." to ".$newargs{type}."\n";
				}
				if( $backupservername{$host}  ne $newargs{backupservername} ) {
					$changes_found++;
					print "backupservername: You have updated ".$backupservername{$host}." to ".$newargs{backupservername}."\n";
				}

				next unless $changes_found;
				print "Server Values In Config Files Have Been Changed\n";

				# UPDATE the server
				$query="UPDATE ServerInfo set
			comm_method 		= \'".$newargs{comm_method}."\',
			win32_comm_method 	= \'".$newargs{win32_comm_method}."\',
			server_type 		= \'".$newargs{server_type}."\',
			hostname 		= \'".$newargs{hostname}."\',
			type 			= \'".$newargs{type}."\',
			backupservername 	= \'".$newargs{backupservername}."\'
				where filetype = \'".$filetype."\'
				and servername=\'".$host."\'\n";

			} else {
				print "New host $host\n";
				$query="insert ServerInfo values (
			\'".	hostname()."\',
			\'".	$filetype."\',
			\'".	$config_directory."\',
			\'".	$host."\',
			\'".$newargs{comm_method}."\',
			\'".$newargs{win32_comm_method}."\',
			\'".$newargs{server_type}."\',
			\'".$newargs{hostname}."\',
			\'".$newargs{type}."\',
			\'".$newargs{backupservername}."\')\n";

			}
			# print $query;

			foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},	-connection=>$connection, -die_on_error=>0 )) {
				my($str)="FATAL ERROR : SERVER=$host\nQUERY=$query\nRESULTS: ".join( dbi_decode_row($_) );
				die $str;
	   	}
	   	add_edit_item(-type=>$filetype, -values=>\%file_overrides, -op=>"update") if $file_changes;
   	}
   }

   $query="DELETE ServerChanges where chkey=\'is_production\'";
	print $query if $Margs{-debug};
	foreach ( dbi_query( 	-db=>$db,
		-query=>$query,
		-debug=>$Margs{-debug},
		-connection=>$connection,
		-die_on_error=>1 )
	) {
			warn "ERROR running $query:  ".dbi_decode_row($_);
	}

	$query="exec build_container\n";
	print $query if $Margs{-debug};
	foreach ( dbi_query( 	-db=>$db,
		-query=>$query,
		-debug=>$Margs{-debug},
		-connection=>$connection,
		-die_on_error=>1 )
	) {
			warn "ERROR running $query:  ".dbi_decode_row($_);
	}

	print "Sync of Config Files Completed\n";
	return 1;
  # exit(0);
}

# MlpBatchJobStart(-BATCH_ID => $opt_b, -AGENT_TYPE  =>  'Backup Job',  -SUBKEY  => $opt_J." ".$tran_log_text );
#
#	"MlpBatchJobStart:-BATCH_ID"      => 1,
#  "MlpBatchJobStart:-AGENT_TYPE     => 1,
#  "MlpBatchJobStart:-SUBKEY"  => 1,
#
sub MlpBatchJobStart {
	my(%args)=@_;

	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}
	if( $mlpbatchjob_args{-AGENT_TYPE} ) {
		#print "Code Warning - DUPLICATE CALL TO MlpBatchJobStart( agent_type= $mlpbatchjob_args{-AGENT_TYPE} ) continuing";		# allready run!
		return;
	}
	if( ! $args{-BATCH_ID} ) {
		use Carp;
		confess "WOAH - No -BATCH_ID SENT TO MlpBatchJobStart()";
		return;
	}

	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	%args= validate_params("MlpBatchJobStart",%args);

	if( $DBGDBG or $args{-debug} ) {
		print __LINE__,"MlpBatchJobEnd: ";
		foreach ( keys %mlpbatchjob_args ) { print "M",$_,"=",$mlpbatchjob_args{$_}," "; }
		foreach ( keys %args ) { print "A",$_,"=",$args{$_}," "; }
		print "\n";
	}


	$args{-AGENT_TYPE} = 'Gem Monitor' unless $args{-AGENT_TYPE};

	foreach ( keys %args ) { $mlpbatchjob_args{$_} = $args{$_}; }
	$mlpbatchjob_args{-internalkey}=time;
	$mlpbatchjob_args{-currentstep}=0;

	die "MlpBatchJobStart() AGENT TYPE INVALID - must be 'User Batch Job','Gem Monitor','Backup Job','Credential Mgt' \n"
		unless $args{-AGENT_TYPE} eq 'User Batch Job'
		or $args{-AGENT_TYPE} eq 'Gem Monitor'
		or $args{-AGENT_TYPE} eq 'Backup Job'
		or $args{-AGENT_TYPE} eq 'Credential Mgt';

	if( $DBGDBG or $args{-debug} ) {
		print __LINE__,"MlpBatchJobEnd: ";
		foreach ( keys %mlpbatchjob_args ) { print "M",$_,"=",$mlpbatchjob_args{$_}," "; }
		foreach ( keys %args ) { print "A",$_,"=",$args{$_}," "; }
		print "\n";
	}

	# FINAL VERSION OF AGENT STUFF
	my($rowexists)=0;
	foreach ( _querywithresults("SELECT 1 from INV_batchjob_definition ".
"where agent_type='".$mlpbatchjob_args{-AGENT_TYPE}.
"' and monitor_program='".$mlpbatchjob_args{-BATCH_ID}.
"' and subkey='".$mlpbatchjob_args{-SUBKEY}."'\n",1) ) {
				( $rowexists ) = dbi_decode_row($_);
	}

	my($query);
	if( $rowexists eq '0' ) {
		$query="INSERT INV_batchjob_definition ( agent_type, monitor_program, subkey, last_start_time,
		last_end_time, last_exit_code, last_exit_notes, spid, internalkey, status_value ) \n".
		"select '".$mlpbatchjob_args{-AGENT_TYPE}."', '".
		$mlpbatchjob_args{-BATCH_ID}."',\n '".
		$mlpbatchjob_args{-SUBKEY}."',getdate(),null,null,null,\n\@\@spid,$mlpbatchjob_args{-internalkey},'RUNNING'\n";
	} elsif( $rowexists eq '1' ) {
		$query="UPDATE INV_batchjob_definition\n\tset last_start_time=getdate(), last_end_time=null,
		last_exit_code=null, last_exit_notes=null,  status_value='RUNNING',\n\t spid=\@\@spid,
		internalkey=$mlpbatchjob_args{-internalkey}\n".
		"where agent_type='".$mlpbatchjob_args{-AGENT_TYPE}.
		"' and monitor_program='".$mlpbatchjob_args{-BATCH_ID}.
		"' and subkey='".$mlpbatchjob_args{-SUBKEY}."'\n";
	} else {
		die "Query Returned $rowexists";
	}
	print $query,"\n" if $DBGDBG or $args{-debug};
	_querynoresults($query,1);

# var for exit
#	job_exit_notes job_end_time job_success_code job_duration
# var for update points
#	job_system_name,job_system_type

	my($COMMAND_RUN)=$0." ".join(" ",@ARGV);
	my($USER)=GetLogonUser() || $ENV{USER} || $ENV{USERNAME};;
	$query="INSERT INV_batchjob_history(\n\tagent_type,monitor_program,subkey,spid,\n\tinternalkey, start_time, status_value,".
	"command_run,\n\tjob_perl,hostname,env_sybase,suser_name,\n\tos_username\n\t ".
	") values (\n\t".
	dbi_quote(-text=>$mlpbatchjob_args{-AGENT_TYPE}, -connection=>$connection).",".
	dbi_quote(-text=>$mlpbatchjob_args{-BATCH_ID}, -connection=>$connection).",".
	dbi_quote(-text=>$mlpbatchjob_args{-SUBKEY}, -connection=>$connection).",\@\@spid,\n\t".
	"$mlpbatchjob_args{-internalkey},getdate(),'RUNNING',".
	dbi_quote(-text=>$COMMAND_RUN, -connection=>$connection).",\n\t".
	dbi_quote(-text=>$^X, -connection=>$connection).",".
	dbi_quote(-text=>$this_hostname, -connection=>$connection).",\n\t".
	dbi_quote(-text=>$ENV{SYBASE}, -connection=>$connection).",suser_name(),".
	dbi_quote(-text=>$USER, -connection=>$connection).
	")\n";
	# dbi_quote(-text=>'', -connection=>$connection).
	# "$mlpbatchjob_args{-internalkey},getdate(),\@\@spid,\n\t".

	print $query,"\n" if $DBGDBG or $args{-debug};
	_querynoresults($query,1);
}

sub MlpLongRunningQuery
{
	my(%args)=@_;
	initialize(%args) unless $i_am_initialized eq "TRUE";
	my($query)="

if exists ( select * from INV_long_running_ops
            where server_name=".         _quote($args{-server_name})."
            and   server_type=".         _quote($args{-server_type})."
            and   query_start_time=".   _quote($args{-query_start_time})."
            and   spid=".               $args{-spid}." )
      update      INV_long_running_ops
      set         io_used      =   $args{-io_used},
                  cpu_used     =   $args{-cpu_used},
                  cur_time     =   getdate()
            where server_name=".         _quote($args{-server_name})."
            and   server_type=".         _quote($args{-server_type})."
            and   query_start_time=".   _quote($args{-query_start_time})."
            and   spid=".               $args{-spid}."
else
      insert       INV_long_running_ops
      values (    ".
         _quote($args{-server_name}).",".
         _quote($args{-server_type}).",".
         _quote($args{-query_start_time}).",".
         $args{-spid}.      ",".
         _quote($args{-database_name}).",".
         _quote($args{-login}).",".
         _quote($args{-query}).",".
         $args{-io_used}.   ",".
         $args{-cpu_used}.   ",".
         _quote($args{-client_hostname}).",".
         _quote($args{-application_name}).",getdate())\n";

 	_querynoresults( $query ,1 );
}


#	MlpBatchJobStep(-step=>Step, -step_subkey=>SubKey )
sub MlpBatchJobStep {
	my(%args)=@_;
	$args{-step}='Running' unless $args{-step};
	if( $DBGDBG or $args{-debug} ) {
		print "MlpBatchJobStep: ";
		foreach ( keys %mlpbatchjob_args ) { print "M",$_,"=",$mlpbatchjob_args{$_}," "; }
		foreach ( keys %args ) { print "A",$_,"=",$args{$_}," "; }
		print "\n";
	}

	return unless $mlpbatchjob_args{-BATCH_ID};

	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}

	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	%args= validate_params("MlpBatchJobStep",%args);


	if( ! $mlpbatchjob_args{CurrentStepNumber} ) {
			# NEW STEP
			$mlpbatchjob_args{CurrentStepNumber} = 1;
	} else {
		my($query)="UPDATE INV_batchjob_step set end_time=getdate()".
		"  where agent_type='".		$mlpbatchjob_args{-AGENT_TYPE}.
		"' and monitor_program='".	$mlpbatchjob_args{-BATCH_ID}.
		"' and isnull(subkey,'')='".				$mlpbatchjob_args{-SUBKEY}.
		"' and internalkey = $mlpbatchjob_args{-internalkey} ".
		" and  end_time is null and stepnumber=".$mlpbatchjob_args{CurrentStepNumber}."\n";

		print $query if $DBGDBG or $args{-debug};
		_querynoresults($query,1);

		$mlpbatchjob_args{CurrentStepNumber}++;
	}

	my($query)="INSERT INV_batchjob_step ( agent_type, monitor_program,
		subkey, end_time, internalkey, stepname, stepnumber, step_subkey, start_time ) \n".
		"select '".$mlpbatchjob_args{-AGENT_TYPE}."', '".
		$mlpbatchjob_args{-BATCH_ID}."',\n '".
		$mlpbatchjob_args{-SUBKEY}."',null, $mlpbatchjob_args{-internalkey},".
		dbi_quote(-text=>$args{-step}, -connection=>$connection).",".
		$mlpbatchjob_args{CurrentStepNumber}.",".
		dbi_quote(-text=>$args{-step_subkey}, -connection=>$connection).",getdate()\n";

	print $query if $DBGDBG or $args{-debug};
	_querynoresults($query,1);

}

# MlpBatchJobErr( $msg );
sub MlpBatchJobErr
{
	my($msg)=@_;
	return MlpBatchJobEnd( -EXIT_CODE=>'ERROR',  -EXIT_NOTES=>$msg);
}

# MlpBatchJobEnd(-EXIT_CODE=>"ERROR", -EXIT_NOTES=>$msg);
#
#	"MlpBatchJobEnd:-EXIT_CODE"      => 1,
#  "MlpBatchJobEnd:-EXIT_NOTES     	=> 1
#
sub MlpBatchJobEnd {
	my(%args)=@_;

	if( $DBGDBG or $args{-debug} ) {
		print "MlpBatchJobEnd: ";
		foreach ( keys %mlpbatchjob_args ) { print "M",$_,"=",$mlpbatchjob_args{$_}," "; }
		foreach ( keys %args ) { print "A",$_,"=",$args{$_}," "; }
		print "\n";
	}

	return unless $mlpbatchjob_args{-BATCH_ID};
	$args{-EXIT_CODE} = "OK" unless $args{-EXIT_CODE};
	$args{-EXIT_CODE} = "FAILED" if $args{-EXIT_CODE} eq "ERROR";
	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}
	my($query)="UPDATE INV_batchjob_definition set status_value='".$args{-EXIT_CODE}."', last_end_time=getdate(), ".
	" last_exit_code='".			$args{-EXIT_CODE}."', last_exit_notes=".dbi_quote(-text=>$args{-EXIT_NOTES}, -connection=>$connection).
	"  where agent_type='".		$mlpbatchjob_args{-AGENT_TYPE}.
	"' and monitor_program='".	$mlpbatchjob_args{-BATCH_ID}.
	"' and isnull(subkey,'')='".				$mlpbatchjob_args{-SUBKEY}.
	"' and internalkey = $mlpbatchjob_args{-internalkey} \n".
	"  and spid=\@\@spid\n";

	print $query if $DBGDBG or $args{-debug};
	_querynoresults($query,1);

	$query="UPDATE INV_batchjob_history set status_value='".$args{-EXIT_CODE}."',end_time=getdate(), ".
	" exit_code='".$args{-EXIT_CODE}."', exit_notes=".dbi_quote(-text=>$args{-EXIT_NOTES}, -connection=>$connection).
	" where agent_type='".$mlpbatchjob_args{-AGENT_TYPE}.
	"' and monitor_program='".$mlpbatchjob_args{-BATCH_ID}.
	"' and isnull(subkey,'')='".$mlpbatchjob_args{-SUBKEY}.
	"' and internalkey = $mlpbatchjob_args{-internalkey}".
	"  and spid=\@\@spid\n";
	print $query if $DBGDBG or $args{-debug};
	_querynoresults($query,1);

	# update the end time for the last step if you are using steps
	if( $mlpbatchjob_args{CurrentStepNumber} ) {
		my($query)="UPDATE INV_batchjob_step set end_time=getdate()".
		"  where agent_type='".		$mlpbatchjob_args{-AGENT_TYPE}.
		"' and monitor_program='".	$mlpbatchjob_args{-BATCH_ID}.
		"' and isnull(subkey,'')='".				$mlpbatchjob_args{-SUBKEY}.
		"' and internalkey = $mlpbatchjob_args{-internalkey} ".
		" and  end_time is null and stepnumber=".$mlpbatchjob_args{CurrentStepNumber}."\n";

		print $query if $DBGDBG or $args{-debug};
		_querynoresults($query,1);
	}
	%mlpbatchjob_args = ();
}

sub MlpAgentStart
{
	my(%args)=@_;

	MlpBatchJobStart(-BATCH_ID=>$args{-monitor_program},-AGENT_TYPE=>'Gem Monitor',-SUBKEY=> $args{-system} )
		if $args{-monitor_program};
	warn "Should Not Call MlpAgentStart()\n";
	return;
}

sub MlpAgentDone {
	my(%args)=@_;
	if( $DBGDBG or $args{-debug} ) {
		print "MlpAgentDone";
		foreach ( keys %mlpbatchjob_args ) { print $_," ",$mlpbatchjob_args{$_},"\n"; }
	}
	warn "Should Not Call MlpAgentDone()\n";
	MlpBatchJobEnd(-EXIT_NOTES =>$args{-message}) if $mlpbatchjob_args{BATCH_ID};
}
sub MlpMonitorStart
{
	my(%args)=@_;
	$args{-monitorjob}=1;
	warn "Should Not Call MlpMonitorStart()\n";
	MlpBatchStart(%args);
}
sub MlpBatchStart
{
	my(%args)=@_;
	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	warn "Warning Should Not Call MlpBatchStart()\n";
	MlpBatchJobStart(-BATCH_ID=>$args{-batchjob},-AGENT_TYPE=>'Gem Monitor',-SUBKEY=> $args{-system} )
				if $args{-batchjob};
	return;
}

sub MlpBatchDone
{
	my(%args)=@_;

	if( $DONT_USE_ALARMS ) {
		print "PLEASE NOTE THAT THE ALARM SYSTEM IS DISABLED\n" unless $quiet;
		$quiet=1;
		return 0;
	}

	$args{-state}="OK" unless $args{-state};
	MlpBatchJobEnd(-EXIT_CODE => $args{-state}, -EXIT_NOTES =>$args{-message}) if $args{-monitor_program};
	warn "Warning: Should Not Call MlpBatchDone()\n";
}

sub MlpGetBackupState
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetBackupState",%args1);
	my($query)= 'SELECT
			source_system,
			dbname,
			convert(varchar,last_fulldump_time,110)+\' \'+convert(varchar,last_fulldump_time,108),
			last_fulldump_file,
			last_fulldump_lsn,
			convert(varchar,last_trandump_time,110)+\' \'+convert(varchar,last_trandump_time,108),
			last_trandump_file,
			last_trandump_lsn,
			last_truncation_time,
			convert(varchar,last_fullload_time,110)+\' \'+convert(varchar,last_fullload_time,108),
			last_fullload_file,
			last_fullload_lsn,
			convert(varchar,last_tranload_time,110)+\' \'+convert(varchar,last_tranload_time,108),
			last_tranload_file,
			last_tranload_lsn,
			last_tranload_filetime,
			is_tran_truncated,
			is_db_usable,
			is_readonly,
			is_select_into,is_singleuser,is_backupable,
			db_server_name  from BackupState ';

	if( defined $args{-production} and $args{-production} ne "0" ) {
		$query .= " ,ProductionServers p";
	}

	$query.=" where  is_deleted is null ";
	$query.=" and source_system = \'".$args{-system}."\'" if defined $args{-system};
	if( defined $args{-production} and $args{-production} ne "0" ) {
		$query .= " and BackupState.source_system = p.system and is_production=1";
	}

	$query.= " ".$args{-orderby} if defined $args{-orderby};
	print $query."\n" if $args{-debug};

	my(@rc);
	@rc = dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 );
	return @rc;
}

sub MlpOldBackupState
{
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my($query)="	UPDATE BackupState set is_deleted='Y' where datediff(dd,row_touch_time,getdate())>21 ";
	print $query,"\n";
   my(@rc)=dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
   foreach (@rc) { print "ERROR>>>",dbi_decode_row($_); }
	return @rc;
}

sub MlpSetBackupState
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpSetBackupState",%args1);
	if( $args{-debug} ) {
		print "MlpSetBackupState\n";
		foreach ( keys %args) { print "\t$_ => $args{$_}\n" if defined $args{$_}; }
	}

	return if $args{-dbname} eq "tempdb" or $args{-dbname} eq "model";

	if( $args{-delete} ) {
		my($query)="	UPDATE BackupState set is_deleted='Y' where source_system=".$connection->quote($args{-system}).
						" and dbname=".$connection->quote($args{-dbname});
		print $query,"\n";
	   my(@rc)=dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
	   foreach (@rc) { print "ERROR>>>",dbi_decode_row($_); }
		return @rc;
	}

    my($query)="Exec Backup_proc ".
    	  $connection->quote($args{-system}).",".
        $connection->quote($args{-dbname}).",".
    	  $connection->quote($args{-last_fulldump_time}).",".
        $connection->quote($args{-last_fulldump_file}).",".
        $connection->quote($args{-last_fulldump_lsn}).",".

        $connection->quote($args{-last_trandump_time}).",".
        $connection->quote($args{-last_trandump_file}).",".
        $connection->quote($args{-last_trandump_lsn}).",".

        $connection->quote($args{-last_truncation_time}).",".

        $connection->quote($args{-last_fullload_time}).",".
        $connection->quote($args{-last_fullload_file}).",".
        $connection->quote($args{-last_fullload_lsn}).",".

        $connection->quote($args{-last_tranload_time}).",".
        $connection->quote($args{-last_tranload_file}).",".
        $connection->quote($args{-last_tranload_lsn}).",".
        $connection->quote($args{-last_tranload_filetime}).",".
        $connection->quote($args{-is_tran_truncated}).",".
		  $connection->quote($args{-is_db_usable}).",".

		  $connection->quote($args{-is_readonly}).",".
		  $connection->quote($args{-is_select_into}).",".
		  $connection->quote($args{-is_singleuser}).",".
		  $connection->quote($args{-is_backupable}).",".

		  $connection->quote($args{-db_server_name}).",".
		  $connection->quote($args{-file_time}) ;

		print $query,"\n" if $args{-debug};
    	my(@rc)=dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
   	foreach (@rc) { print "ERROR>>>",dbi_decode_row($_); }

   	if( ! $args{-delete} ) {
			my($query)="	UPDATE BackupState set is_deleted='N' where source_system=".$connection->quote($args{-system}).
							" and dbname=".$connection->quote($args{-dbname});
			print $query,"\n" if $args{-debug};
		   my(@rc)=dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
		   foreach (@rc) { print "ERROR>>>",dbi_decode_row($_); }
		}
}

sub MlpBatchRunning
{
	my(%args)=@_;
	MlpBatchJobStep( -step=>$args{-message} );
	return;
}

sub MlpBatchErr
{
	my(%args)=@_;
	warn "Should Not Call MlpBatchErr()\n";
	return MlpBatchJobErr( $args{-message} );
}

sub MlpIsPageable {
	my($svr,$check_prod)=@_;
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my($query)="select * from Blackout where system='".$svr."'";
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		return 0;
	}
	return 1 unless $check_prod;
	$query="select * from ProductionServers where system='".$svr."' and is_production=1";
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		return 1;
	}
	return 0;
}

1;

__END__

=head1 NAME

MlpAlarm.pm - Alarming And Monitoring

=head2 DESCRIPTION

This perl module provides a generic mechanism to manage alarms and monitoring. The tool is distributed as a perl module with several associated programs (a web based GUI, an alarm router, and some monitoring programs).   The library contains several simple functions to monitor your systems and some back end functions used by the reporting user interface.  Alarm data is stored in a database (Sybase or SQL Server).

=head2 SUMMARY

MlpAlarm provides a simple mechanism to manage alarming and reporting.  Functions use a by name interface with consistent parameters for simplicity and ease of use.  It has the following features

The system supports the following types of "data"

=over 4

=item * Heartbeat Data

Heartbeats are frequent up/down type messages regarding state.  Examples of programs that generate heartbeats include ping and a database disk space monitor which alarms based on space used.  Heartbeats are saved with a -severity argument which can be EMERGENCY, CRITICAL, ERROR, WARNING, or OK.  Because we expect heartbeats frequently, it is considered a problem if a heartbeat is not generated in a timely manner. Heartbeats are stored using the MlpHeartbeat() function. The system does not keep track of historical Heartbeat information.

=item * Batch Job Data

Batch jobs typically run infrequently but regularly.  They can thus be treated as Heartbeats with a low frequency.  The system identifies batch jobs by the -batchjob argment to MlpHeartbeat.  Because some batch jobs have a frequency as low as once per month, it is not considered a warning when a Heartbeat message is not generated in a timely manner.

Because the system does not care how long it is between batch job heartbeats, heartbeat messages for batch jobs may include three states in adition to the normal Heartbeat states.   These are "RUNNING", "NOT RUN", and "COMPLETED".  Batch jobs, when started, should set their state to RUNNING, and when finished should set their state to COMPLETED (which is the same as OK) or one of the error states.

There are several convenience functions starting with MlpBatch...() that can be used to store Batch Job data.  These routines are simple wrappers on MlpHeartbeat().

=item * Event Data

Events are incidents on our systems for which we care about the time they occurred.  Event logs for your servers are an example.  Because we care what time they occurred, The system keeps a history of all the events you have saved and a cleanup program must be run to purge this data.  Events are stored using the MlpEvent() function.

=back

Fundamentally, from the perspective of a user of this module, the system consists of 3 functions named MlpHeartbeat and MlpEvent.  These functions can be considered Black Boxes to transmit, store, and route your messages appropriately.

=head2 THE GORY DETAILS

=head2 System/Subsystem Values

Everything monitored is keyed by monitoring_program, system and subsystem.  A Systems is a hardware or software component.  Subsystems refer to individual parts of those systems (disks, logs etc).   If you are monitoring batch jobs, the system refers to a logical group of batch jobs, while the subsystem refers to the step.

=head2 Containers

When you view the data, you view it by Containers, which are groups of systems.  The user interface supports a mechanism to create and manage your "Containers".  An example of containers are PRODUCTION, DATABASES, or UNIX.  This allows different users to see  only the subset of systems they are interested in.

=head2 State/Severity Values

Key to the system is the state and severity levels.  Message routing will be based on a combination of the key (monitoring_program/system/subsystem) and this value.

The following are legitimate heartbeat states

=over 4

=item * EMERGENCY = system down / critical failure

=item * CRITICAL = serious problem.

=item * ERROR = non fatal error possibly requiring administrator attention

=item * WARNING = non fatal warning.

=item * INFORMATION = a simple message.  Synonym for OK.

=item * DEBUG = messages only of interest to developers

=back


Additionally, batch jobs may also be

=over 4

=item * STARTED = the job has started

=item * COMPLETED = the job has completed normally

=item * NOT_RUN = the job has not started (default state)

=back


If a batch job aborts/fails, the status should not be COMPLETED (it should be one of the other states from above).  Note that you can have the batch job submit many RUNNING heartbeats, with different message texts, to identifying the exact position in the batch.

=head1 Monitoring API

=head2 MlpHeartbeat()

Heartbeat message.  A heartbeat message may require attention but the system does not keep heartbeat history.  Ping is an example of a heartbeat message.

Requried Arguments: -state, -monitor_program -system

 -state=>[STARTED, COMPLETED, RUNNING, EMERGENCY, CRITICAL, ERROR, WARNING, INFORMATION, DEBUG]

Optional Arguments: -debug -subsystem -message_text -document_url -batchjob

 -batchjob =>	this is a batch job - so value is not going to be refreshed frequently
 -event_time => valid sybase date time format - usually can be ignored

=head2 MlpEvent()

MlpEvent saves Events, which are monitioring messages where history may be of interest.  Application errorlogs are examples of events.

Required arguments;

 -message_text=>[text string]
 -severity=>[EMERGENCY,CRITICAL,ERROR,WARNING,INFORMATION,DEBUG]
 -system=>[system being monitored]
 -monitor_program=>[program name.  Default is basename($0)]
 -severity

Optional arguments:

 -subsystem=>[subsystem being monitored]
 -debug
 -event_id=> 0
 -message_value=> 0
 -message_text
 -document_url=> 0
 -event_time=>[timestamp of event in sybase format.  Default to now]
 -save_syslog= 0|1	- save event using syslogd if on unix
 -save_dbms = 0|1

=head2 Batch Job Convenience Functions

Batch jobs in the system are treated as a special case of Heartbeats.  You *could* write your manager using a heartbeat fore each batch, or you can use these convenience functions.

4 functions are provided as simple wrappers to the MlpHeartbeat function for use by batch jobs.  These functions are MlpBatchStart, MlpBatchRunning, MlpBatchDone, and MlpBatchErr.   Funcamentally, when your batch or monitoring job starts, you call MlpBatchStart and you finish with a call to MlpBatchDone or MlpBatchErr.  If your job runs for a while (or continuously), you can use MlpBatchRunning to indicate that the batch is still alive.

=over 4

=item * MlpBatchStart()

This function identifies that a program has started.  The program should be passed either -name or -system (they are synonyms) and the -batchjob flag.   If you are using multiple occurances of this program to check multiple systems, you should identify the system monitored with the -subsystem parameter. If you are running a batchjob, you should also pass a name for the batch job with -monitor_program.  Finally you can pass in an optional -message.

Additionally, if this is a -batchjob, you may specify -step to indicate which step this subsystem is numbered and -reset_status to clear times.

example
	MlpBatchStart(-monitor_program=>"NightlyCopy", -name="IMAGSYB1", -batchjob=>1);

=item * MlpBatchStep

This function takes a required -stepname and -stepnum and optional -message. The
step name and number are saved so the next call to MlpBatchDone/MlpBatchErr will know where you are at.  The -stepname is considered a -subsystem and a heartbeat is saved with a state of RUNNING

=item * MlpBatchJobStep()

This function just saves a heartbeat in the system so you can see that your batch or monitoring job is working.

example
	MlpBatchJobStep()
	MlpBatchRunning(-message=>"at step number 3")

=item * MlpBatchDone()

The batch has completed.  Required -monitor_program and -system.  Optional -name -debug -subsystem -message -stepnum.

=item * MlpBatchErr()

These two functions indicate the completion of your batch or monitoring job.  They also use the same arguments as above, although you probably also want to pass a -message if the job is in error.

example
	MlpBatchDone()
	MlpBatchErr(-message=>"Cant Connect To The Server")

=back

=head1 Monitoring Reporting

The reporting functions are pretty for developer internal use and are simply listed here for completeness:

=over 3

=item * MlpGetCategory()

Args:  -user => username, -debug

Get report category code

=item * MlpGetScreens()

Args:  -user=>username, -category=name, -debug

Get screens viewable by a user

=item * MlpGetReportCode()

Args:  -user=>username, -screen_name=name

Get Report type code

=item * MlpRunScreen()

Args: -screen_name=name, [-container=name], -system, -program, -debug

MlpRunScreen() returns an array of pointer to arrays.  You can
override your predefined containers or just use the normally set up ones.  You can pass -NL to define newline characters for debug messages

=item * MlpGetContainer()

Args:  -name=name, [-type=(s)ystem|(c)container], -user=>username, -debug=>[1|0], -screen_name=>name

Fetch a list of systems and containers

=item * MlpGetDbh()

Returns the DBprocess handle currently in use (shouldnt be needed)

=item * MlpGetProgram()

Gets a list of monitoring program information

=item * MlpGetSeverity()

Retrieves a list of severities

=item * MlpManageData()

Administrative interface to the system

=item * MlpGetOperator()

Returns operator information

=back

=head1 Monitoring Coding

=head2 Example 1.  Write Event Log

The following example writes an event message.  Note that the monitor program name will be determined from the current program name, and the system the event is for will be determined by hostname():

 #!C:\Perl\bin\perl.exe
 use lib 'G:/ADMIN_SCRIPTS/lib';
 use strict;
 use MlpAlarm;
 MlpEvent( -severity=>"INFORMATION",
           -message_text=>"Test Message",
           -system=>"test");


=head2 Example 2. A Simple Monitor

The following example monitors something...

 #!C:\Perl\bin\perl.exe
 use lib 'G:/ADMIN_SCRIPTS/lib';
 use MlpAlarm;
 MlpBatchStart( -system=>"TaQ",-subsystem=>'Nyse',-monitorjob => 1 );
 if( ! connect($SERVER) ) {
	MlpBatchErr( -message=>"Can Not Connect To DB $SERVER: $text" );
	MlpHeartbeat( 	-system	=> 'TaQ', -subsystem => 'Nyse',
				-state=>"ERROR", -monitor_program=>"TaqMonitor",
				-message_text=>"Can Not Connect to Taq/Nyse");
	die "ERROR - Can Not Connect to Data Source TaQ";
 }
 while( 1 ) {
	my($rc,$text)=monitor_taq($SERVER);
	if( $rc  eq  "ERROR" ) {
		MlpHeartbeat( -state=>"ERROR", -debug=>$opt_d, -system=>"TaQ", -subsystem=>"Nyse",
			-monitor_program=>"TaqMonitor", -message_text=>$text);
	} else {
		MlpHeartbeat( -state=>"OK", -debug=>$opt_d, -system=>"TaQ", -subsystem=>"Nyse",
			-monitor_program=>"TaqMonitor", -message_text=>"Server Ok: $text");
	}
	sleep(300);
   MlpBatchRunning(-message=>'still working')
 }
 MlpBatchDone(-message=>'completed normally');

=head2 Example 3. A Complex Multistep Batch

Let us assume that we have a batch job called Nightly that is composed of 6 named steps.  We would like to see the state of this batch in the appropriate order and would like to reset the state of the subbatches when we start.  This way we can see progress.  So... we should *always* see 6 substeps plus an overall step when we look a the Heartbeat table.  When the batch starts, everything gets marked "not run".  When steps get run their status gets updated.

The way we do this is to use our convenience functions. Technically, the use master heartbeat differes from that of the steps as it has no -subsystem while the steps do.  But they are connected - they share the same other information.  The Master row state will be set to RUNNING at the start.  This row will have its timestamp updated when any MlpBatchStep row is run.  It will also have its message updated to be the last message saved by any of the -step functons.  Note that this means that failed substeps where the program continues will result in a progression like (state=RUNNING msg="Running Step 1 : Init") to (state=RUNNING msg="Step 1 Failed") to (state=RUNNING msg="Running Step 2 : AlphaSetup") in short order.  You must track final state and write that with MlpBatchDone/MlpBatchErr as normal.

 # start up our program, clear the states of all the jobs
 my($finalstate,$finalmsg)=("OK","Job Complted Successfully";

 # start our batch named Nightly and reset all associated rows
 MlpBatchStart( -system=>"Nightly",-batchjob => 1, -reset_status=>1 );
 $rc=connect();
 if( $rc eq "FAIL" ) { 		# Fail the whole batch...
 	MlpBatchErr( -message=>"Can not Connect");
 	die "Done - Error Can Not Connect";
 }

 # Step 1 - changed to RUNNING and then to COMPLETED or ERROR
 MlpBatchStep( -stepname=>"Init",-stepnum=>1);
 $rc=init();
 if( $rc eq "FAIL" ) {
 	MlpBatchErr( -message=>"Step 1 Failed $!", -stepnum=>1 );
 	($finalstate,$finalmsg)=("ERROR","Step 1 Failed $!");
 } else {
 	MlpBatchDone( -message=>"Step 1 Completed Normally", -stepnum=>1 );
 }

 # Step 2
 if( $finalstate eq "OK" ) {
   MlpBatchStep( -stepname=>"AlphaSetup", -stepnum=>2 );
   $rc=AlphaSetup();
   if( $rc eq "FAIL" ) {
 	  MlpBatchErr( -message=>"Step 2 Failed $!", -stepnum=>2 );
 	  ($finalstate,$finalmsg)=("ERROR","Step 2 Failed $!");
   } else {
 	  MlpBatchDone( -message=>"Step 2 Completed Normally", -stepnum=>2 );
   }
 }

 # Step 3 - A long Running step
 # changed to RUNNING and then the timestamp is updated every 100 second
 # loop and finally to COMPLETED or ERROR
 if( $finalstate eq "OK" ) {
   MlpBatchStep( -stepname=>"AlphaRun", -stepnum=>3 );
   while($i<100) {
 	  sleep(100);
 	  $rc = itterate();
 	  last if $rc eq 'FAIL";
 	  MlpBatchRunning(-message=>"step 3: completed $i/100", -stepnum=>3);
 	  $i++;
   }
 	if( $rc eq "FAIL" ) {
 		MlpBatchErr( -message=>"Step 3 Failed at part $i: $!", -stepnum=>3 );
 		($finalstate,$finalmsg)=("ERROR","Step 2 Failed at part $i $!");
 	} else {
 		MlpBatchDone( -message=>"Step 3 Completed Normally", -stepnum=>3 );
 	}
 }

 if( $finalstate eq "ERROR" ) {
 	MlpBatchErr( -message=>$finalmsg );
 } else {
 	MlpBatchDone( -message=>$finalmsg );
 }

=head1 Monitoring Notes

The following are paths and perl versions that are known to work with this lib.

=over 4

=item * NT

With G: mapped to /samba/sybmon

	#!C:/perl/bin/perl.ex
	use lib 'G:/ADMIN_SCRIPTS/lib

=item * Solaris

	#!/usr/local/bin/perl-5.6.1
	use lib '/apps/sybmon/ADMIN_SCRIPTS/lib';

=item * Linux

	#!/usr/local/bin/perl-5.8.2
	use lib "/apps/sybmon/ADMIN_SCRIPTS/lib";

=back

=head2 FEATURE HEARTBEAT

The MlpHeartbeat() function has been modified to provide true heartbeats.  Simply pass in an appropriate set of
core information (monitor_program,system,text...) and the argument -heartbeat=minutes and a record will be placed
that has been "REVIEWED" for getdate()+minutes.  If no new "heartbeat shows up", an alarm will be sent whenever
it expires!

=head2 MONITORJOB / BATCHJOB

In addition to Heartbeats and Events, the system will handle batch jobs and agents.  Agents are the
programs that collect Heartbeats and Events.  Agents are stored with by setting -monitor_program=>1
in the functions.  This saves heartbeat data with batchjob=AGENT.  Batchjobs are considered all
data with batchjob!=AGENT but not null. The functions MlpBatchStart, MlpBatchDone, MlpBatchErr,
and MlpBatchRunning and MlpBatchStep.

=head2 SYNC

MlpSyncConfigFiles()

  - Called by MlpAlarmCleanupDB.pl

	my($config_directory)=get_gem_root_dir()."/conf";
	my($query)="select servername, chvalue from ServerChanges where chkey=\'is_production\' order by tstamp\n";
	my(%is_prod_changes) run_query()


	if( $Margs{-operator} ) {
		$query="select user_name from Operator where user_name='GEMADMIN'";
		if no operators
			$query="insert Operator ( user_name,user_type,enabled,pager_email,email,netsend )
					values ('GEMADMIN','Admin','YES','','".$Margs{-operator}."','')";

			foreach( "SYBASE","WIN32","UNIX","ORACLE","SQL_SERVER" ) {
				foreach ( MlpManageData(
					-debug				=>	$Margs{-debug},
					System				=>	"",
					Program_LU			=>	"All Programs",
					Use_Netsend_LU		=>	"NO",
					Use_Blackberry_LU => "NO",
					User_LU				=>	"GEMADMIN",
					Min_Severity_LU	=>"Errors",
					Subsystem			=>"",
					Max_Severity_LU	=>'Emergency',
					Use_Email_LU		=>"YES",
					Container			=>$_,
					Admin					=>"Alarm Routing",
					Use_Pager_LU		=>"NO",
					Operation			=>"add" ) ) {

	foreach my $filetype ( get_passtype() ) {
      my($query)="select servername, comm_method, win32_comm_method, server_type, hostname, type,
			backupservername, is_production
         	from ServerInfo i left outer join ProductionServers p on p.system = i.servername
         	where filetype=\'".$filetype."\'\n";

		foreach ( dbi_query( -db=>$db, -query=>$query, -debug=>$Margs{-debug}, -connection=>$connection, -die_on_error=>0 )) {
				$comm_method{$dat[0]} 		= $dat[1];
				$win32_comm_method{$dat[0]}= $dat[2];
				$server_type{$dat[0]} 		= $dat[3];
				$hostname{$dat[0]} 			= $dat[4];
				$type{$dat[0]} 				= $dat[5];
				$backupservername{$dat[0]} = $dat[6];
				$is_production{$dat[0]} 	= $dat[7];
		}

		foreach my $host (get_password(-type=>$filetype)) {
			my(%args)=get_password_info(-type=>$filetype,-name=>$host);
			my(%newargs);
			$newargs{comm_method}="";
			$newargs{win32_comm_method}="";
			$newargs{server_type}="";
			$newargs{hostname}="";
			$newargs{type}="";
			$newargs{backupservername}="";
			foreach (keys %args) { 	$newargs{lc($_)} = $args{$_}; 	}
			$newargs{server_type}="DEVELOPMENT" unless $newargs{server_type};
			$newargs{comm_method} = $newargs{unix_comm_method};

			my($query);
			my($CONTAINER);
			if( $filetype eq "sybase" ) {
				$CONTAINER="SYBASE";
			} elsif($filetype eq "win32servers") {
				$CONTAINER="WIN32";
			} elsif($filetype eq "unix") {
				$CONTAINER="UNIX";
			} elsif($filetype eq "oracle") {
				$CONTAINER="ORACLE";
			} elsif($filetype eq "sqlsvr") {
				$CONTAINER="SQL_SERVER";
			}

			$query="if not exists (select 1 from ContainerOverride where name=\'".$CONTAINER."' and type='s' and element=\'".$host."\' ) Insert ContainerOverride Values ('".$CONTAINER."','s','".$host."',null)\n";
			foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
					die join( "~",dbi_decode_row($_) );
		   }

			next if $filetype eq "win32servers";

			if( $server_type{$host} ) {
				my($changes_found)=0;
				# compare the results
				if( $comm_method{$host} ne $newargs{comm_method} ) {
					$changes_found++;
					print "comm_method: You have updated {".$comm_method{$host}."} to {".$newargs{comm_method}."}\n";
				}
				if( $win32_comm_method{$host} ne $newargs{win32_comm_method} ) {
					$changes_found++;
					print "win32_comm_method: You have updated {".$win32_comm_method{$host}."} to {".$newargs{win32_comm_method}."}\n";
				}

				if( defined $is_prod_changes{$host} ) {
					# print "DBG DBG is_prod_changes $host = $is_prod_changes{$host} \n";
					if( $is_prod_changes{$host} ) {
						$file_overrides{$host."|SERVER_TYPE"}="PRODUCTION";
						$file_changes++;
						$changes_found++ if $newargs{server_type} ne "PRODUCTION";
						$newargs{server_type} = "PRODUCTION"
					} else {
						$file_overrides{$host."|SERVER_TYPE"}="DEVELOPMENT";
						$file_changes++;
						$changes_found++ if $newargs{server_type} ne "DEVELOPMENT";
						$newargs{server_type} = "DEVELOPMENT";
					}
					# print "DBG DBG $host was set via web page to value $is_prod_changes{$host}\n";
				} elsif( $server_type{$host} ne $newargs{server_type} ) {
					$changes_found++;
					print "server_type: Updating Database For $host ".$server_type{$host}." to ".$newargs{server_type}."\n"
					 	if $Margs{-debug};

					my($p)=0;
					$p=1 if $newargs{server_type} eq "PRODUCTION";
					$query="UPDATE ProductionServers set is_production=$p where system=\'".$host."\'\n";
					foreach ( dbi_query(-db=>$db,-query=>$query,
						-connection=>$connection, -die_on_error=>0 )) {
						die join( dbi_decode_row($_) );
	   				}
				} elsif( $is_production{$host} eq "1" ) {
					if( $newargs{server_type} ne "PRODUCTION" ) {
						print "Setting Host $host To Be Development\n"  if $args{-debug};
						$query="UPDATE ProductionServers set is_production=0	where system=\'".$host."\'\n";
						print $query;
						foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},
							-connection=>$connection, -die_on_error=>0 )) {
							die join( "~",dbi_decode_row($_) );
		   				}

		   			#$query="select * from ProductionServers where system=\'".$host."\'\n";
						#print  $query if $Margs{-debug};
						#foreach ( dbi_query(-db=>$db,-query=>$query,-connection=>$connection, -die_on_error=>0 )) {
						#	print "DBG DBG",join( "~",dbi_decode_row($_) )."\n";
	   				#}
	   			}
	   		} elsif( $is_production{$host} eq "0" ) {
					if( $newargs{server_type} eq "PRODUCTION" ) {
						print "Setting Host $host in Database to be Production\n"  if $Margs{-debug};
						$query="UPDATE ProductionServers set is_production=1 where system=\'".$host."\'\n";
						print $query;
						foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},
							-connection=>$connection, -die_on_error=>0 )) {
							die join( "~",dbi_decode_row($_) );
		   				}
		   			#$query="select * from ProductionServers\n
						#	where system=\'".$host."\'\n";
						#print $query  if $args{-debug};
						#foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>0,
						#	-connection=>$connection, -die_on_error=>0 )) {
						#	print "DBG DBG",join( "~",dbi_decode_row($_) )."\n";
		   			#	}
	   				}
				} elsif( $is_production{$host} eq "" ) {
					# oops not found - so set it
					my($p)=0;
					$p=1 if $server_type{$host} eq "PRODUCTION";

					$query="insert ProductionServers values ('".$host."',$p)\n";
					print $query;
					foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},
						-connection=>$connection, -die_on_error=>0 )) {
						die join( "~",dbi_decode_row($_) );
	   				}
				}

				if( $hostname{$host} ne $newargs{hostname} ) {
					$changes_found++;
					print "hostname: You have updated ".$hostname{$host}." to ".$newargs{hostname}."\n";
				}
				if( $type{$host}  ne $newargs{type} ) {
					$changes_found++;
					print "host: You have updated ".$type{$host}." to ".$newargs{type}."\n";
				}
				if( $backupservername{$host}  ne $newargs{backupservername} ) {
					$changes_found++;
					print "backupservername: You have updated ".$backupservername{$host}." to ".$newargs{backupservername}."\n";
				}

			# UPDATE the server
			$query="UPDATE ServerInfo set
				comm_method 		= \'".$newargs{comm_method}."\',
				win32_comm_method 	= \'".$newargs{win32_comm_method}."\',
				server_type 		= \'".$newargs{server_type}."\',
				hostname 		= \'".$newargs{hostname}."\',
				type 			= \'".$newargs{type}."\',
				backupservername 	= \'".$newargs{backupservername}."\'
				where filetype = \'".$filetype."\'
				and servername=\'".$host."\'\n";


			foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$Margs{-debug},	-connection=>$connection, -die_on_error=>0 )) {
				my($str)="FATAL ERROR : SERVER=$host\nQUERY=$query\nRESULTS: ".join( dbi_decode_row($_) );
				die $str;
	   	}
	   	add_edit_item(-type=>$filetype, -values=>\%file_overrides, -op=>"update") if $file_changes;
   	}
   }

   $query="DELETE ServerChanges where chkey=\'is_production\'";
	$query="exec build_container\n";

=cut

# MlpGetSystem
#   -type       - Database | Host | ApproveRqdDatabase
#   -where      - if set then this is an added where clause for the system
#   -getattributes - returns attributes
#   -attribute_category - modifies attributes if those are requested
#
#   if -getattributes passed then
#       returns hashref of key= (names if -type or name.type if ! -type ) and values=attributes hash
#   else
#       returns array of names if -type or name.type if ! -type
