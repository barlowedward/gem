#!/apps/perl/linux/perl-5.8.2/bin/perl

#
# Based On Generic Log File Filteration Script
#
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use     	strict;
use     	File::Basename;
use     	LogFunc;
use 		Do_Time;
use 		Getopt::Long;
use 		MlpAlarm;
use 		Sys::Hostname;
use 		Repository;
use 		CommonHeader;

my($VERSION)=1.0;
my($PROGNAME)  ="cisco_logmon.pl";
my($HB_MONITOR)="CiscoLogmon";
my($HB_SYSTEM) ="cisco";
#my(%date_lookup)= (
#	"Jan"=>1, "Feb"=>2, "Mar"=>3, "Apr"=>4,
#	"May"=>5, "Jun"=>6, "Jul"=>7, "Aug"=>8,
#	"Sep"=>9, "Oct"=>10, "Nov"=>11, "Dec"=>12);

use vars qw( $DEBUG $NOSAVEPATFILE $PATFILE $INFILE $HTML $TODAYONLY $OUTFILE $NOLOG  );

$|=1;
#my($current_state)="";
my(%catsum,%catmsg);

sub usage
{
	return "Usage: $PROGNAME --NOSAVEPATFILE --TODAYONLY --HTML --OUTFILE=file --DEBUG --NOLOG [--PATFILE=file] --INFILE=file\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(	"INFILE=s"	=> \$INFILE,
		"PATFILE=s"	=> \$PATFILE,
		"TODAYONLY"	=> \$TODAYONLY,
		"NOSAVEPATFILE" => \$NOSAVEPATFILE,
		"NOLOG" 	=> \$NOLOG,
		"HTML" 		=> \$HTML,
		"DEBUG"		=> \$DEBUG );

die usage("Must pass INFILE\n" ) unless defined $INFILE;

my($nl)="\n";
$nl="<br>\n" if $HTML;
print $PROGNAME," version $VERSION",$nl;
print "Run on host ",hostname()," at ".localtime(time).$nl;
MlpBatchJobStart(-BATCH_ID=>$HB_MONITOR ) unless defined $NOLOG;

$PATFILE=get_gem_root_dir()."/data/patern_files/$HB_MONITOR.pat" unless $PATFILE;

my($tmpfile);
if( defined $OUTFILE ) {
	$tmpfile=get_tempfile($OUTFILE);
	print "Temporary Output File : $tmpfile\n";
	print "Results will be copied to $OUTFILE\n";
	die "ERROR: Cant write to $OUTFILE\n" if -e $OUTFILE and ! -w $OUTFILE;
	open(OUTFILE,">$tmpfile" ) or error_out( "Cant write $tmpfile \n" );
}

# ============================
# get start patern
# ============================
my($byte_count,$start_patern)=(0,"");
if( defined $PATFILE and -e $PATFILE ) {
	pr_output( "  Patern File: ".$PATFILE."$nl" );
	($byte_count,$start_patern) = log_rd_patern_file(file=>$PATFILE);
	my($patfiletime)= -M $PATFILE;
	$patfiletime *= 24;
	my($patfilestr) = "";
	if( $patfiletime >= 1 ) {
		$patfilestr .= int($patfiletime)." hours ";
		$patfiletime -= int($patfiletime);
	}
	$patfiletime *= 60;
	$patfilestr.= " ".int($patfiletime)." minutes";
	pr_output( "  Report Filter : Only Include Messages In The Past ",$patfilestr,$nl );
	pr_output( "  PATERN FILE byte count : $byte_count   ",$nl ) if defined $DEBUG;
	pr_output( "  PATERN FILE startpat   : $start_patern ",$nl ) if defined $DEBUG;
} elsif( defined $PATFILE )  {
	pr_output( "  Patern File: ".$PATFILE." (NOT FOUND)",$nl );
}

if( defined $TODAYONLY ) {
	pr_output( "  Time Restriction: Today and Yesterday",$nl );
	log_grep(search_patern=>undef,today=>1,yesterday=>1,date_fmt=>"mon dd");
} else {
	pr_output( "  Time Restriction: none",$nl );
	log_grep(search_patern=>undef,today=>undef,yesterday=>undef,date_fmt=>"mon dd");
}

if( defined $DEBUG ) {
	pr_output( "  Debug Mode=SET",$nl );
	log_debug();
}

my($max_lines)=100000;
$max_lines = 10000 if defined $TODAYONLY;
pr_output( "** PROCESSING FILE **",$nl ) if defined $DEBUG;
my($n_read,$n_returned,$found_pat,$returned_pat,$byte_cnt,@output_r)=
	log_process_file(
		file		=> $INFILE,
		search_patern	=> $start_patern,
		byte_offset	=> $byte_count,
		cat_nogrep	=> 1,
		max_lines	=> $max_lines);

pr_output(    $nl."[ Read $n_read rows from $INFILE.  Filtered to $n_returned ]$nl");
pr_output(    "[ DBG: found_pat=$found_pat (0=false 1=true)]$nl") 	if defined $DEBUG;
pr_output(    "[ DBG: next_run_returned_pat=$returned_pat ]$nl") 	if defined $DEBUG;
pr_output(    "[ DBG: next_run_byte_cnt=$byte_cnt ]$nl" ) 		if defined $DEBUG;
if( ! defined $NOSAVEPATFILE or defined $DEBUG ) {
	log_wr_patern_file(patern=>$returned_pat, bytes=>$byte_cnt, file=>$PATFILE );
	pr_output( "WRITING PATERN FILE byte count = $byte_cnt   ",$nl ) if defined $DEBUG;
	pr_output( "WRITING PATERN FILE patern   = $returned_pat ",$nl ) if defined $DEBUG;
} else {
	pr_output( "NOT WRITING PATERN FILE - NOSAVE/DEBUG MODE",$nl );
	pr_output( "[ PATERN FILE ] byte count = $byte_cnt   ",$nl );
	pr_output( "[ PATERN FILE ] patern   = $returned_pat ",$nl );
}

pr_output( "<b>TRUNCATING OUTPUT TO $max_lines LINES</b>$nl" )
	if $#output_r>=($max_lines-1);


# AT THIS POINT YOU HAVE @output_r which contains your rows
pr_output( "FOUND $#output_r ROWS",$nl );

foreach (@output_r) {
	chomp;
	manage_message($_);
}

pr_output($nl."Program Completed... ",$nl);

pr_output( "\nMessage Category Report\n" );
foreach (sort keys %catsum) {
	pr_output( sprintf("Category %12s : %s messages\n", $_,$catsum{$_})  );
}

if( $OUTFILE ) {
	pr_output($nl."Renaming temporary file $tmpfile to $OUTFILE.",$nl);
	close(OUTFILE);
	my($rc)=rename($tmpfile,$OUTFILE);
	if( ! $rc ) {
		warn "ERROR: cant rename $tmpfile to $OUTFILE : $!",$nl;
	} else {
		print "Renamed file to $OUTFILE",$nl;
		unlink $tmpfile if -r $tmpfile;
	}
}

MlpBatchJobEnd() unless defined $NOLOG;
chmod( 0777, $OUTFILE ) if -w $OUTFILE;
exit(0);

my(%state_by_host_sys);
sub manage_message {
	my($m) = @_;
	next if /^\(dbg\)/;
	$_ = $m;

	pr_output( $_,"\n" ) if defined $DEBUG;

	return 0 if /INVALID_RP_JOIN:/
		or /PORTTOSTP:/
		or /PORTFROMSTP:/
		or /CONFIG_RESOLVE_FAILURE:/
		or /SYS_LCPERR3:/
		or /DUPLEXMISMATCH:/
		or /EXCESSCOLL:/
		or /FAILNOTFOUND:/
		or /BADCKSUM:/
		or /CRYPTO-4-IKMP_BAD_MESSAGE:/
		or /RESTART:/
		or /NBRCHANGE:/
		or /IKMP_NO_SA:/
		or /IKMP_MODE_FAILURE:/
		or /COLDSTART:/
		or /Cisco Internetwork Operating/
		or /RELEASE SOFTWARE/
		or /Copyright (c) /
		or /last message repeated /
		or /CONFIG_I:/
		or /INVALID_RP_REG:/
		or /LATECOLL:/
		or /CONNECT:/
		or /COUNTERS:/
		or /SUPERVISOR_ERR:/
		or /RECVD_PKT_NOT_IPSEC:/
		or /PORT_AUTHORIZED:/
		or /: terminating/
		or /P2_WARN:/
		or /TOPOTRAP:/
		or /PORTLISTEN:/
		or /AUTHFAIL:/
		or /ERRDISPORTENABLED:/
		or /LOOPPAK:/
		or /MISMATCH:/
		or /MSG:/
		or /NDEDISABLED:/
		or /NOTRUNK:/
		or /PORTINTFINSYNC:/
		or /RSHPORTATTEMPT:/
		or /STATUS:/
		or /SYNCEVENT:/
		or /MSGAGEEXPIRY:/
		or /LAYER2DOWN:/
		or /LAYER2UP:/
		or /OVERFLOW:/
		or /LINKTRAP:/
		or /PORT_COLL:/
		or /PORT_COLLDIS:/
		or /MOD_OK:/
		or /CONNECT:/
		or /\/var\/log\/cisco.log/;

	my(@data)=split;
	my($year)=$data[4];
	# create a date string
	my($datestring);
	# ok if $year is correct...
	if( $year !~ /^20\d\d$/ ) {
		# print "UNKNOWN YEAR AT $_\n";
		my(@xltime)=localtime(time);
		$year= 1900+$xltime[5];
	}
	$datestring=$data[0]." ".$data[1]." ".$year." ".$data[2];

	# check the datestring
	die "*******************\nInvalid Date $datestring\n*************************\nLine=$_"
		if $datestring !~ /\w\w\w\s+\d+\s+\d\d\d\d\s\d\d:\d\d:\d\d/;

	my($host)=$data[3];
	if( $host =~ /\[*(\d+\.\d+\.\d+\.\d+)\.\d+\.\d+\]*/ ) {
		$host=$1;
		# die "HOST=$host\n";
	}
	my($category);
	/\%([A-Z\_]+):/;
	$category=$1;
	if( ! defined $category ) {
		/([A-Z]+):/;
		$category=$1;
	}

	return 0 if ! defined $category or $category =~ /^\s*$/;

	$catsum{$category}=0 unless defined $catsum{$category};
	$catsum{$category}++;
	$catmsg{$category}=$_;

	my($subs,$action,$key,$state);
	my($message);

# 	my($date)=$date_lookup{$data[0]}."/".$data[1]."/".$data[4]." ".$data[2];
#	if( /LAYER2DOWN:/ ) {
#		/Interface (.+) changed/;
#		$subs=$1;
#		$action="Down";
#		$key="LAYER2DOWN:";
#	} elsif( /LAYER2UP:/ ) {
#		#/Interface (\w+,\s\w+\s\d+),/;
#		/Interface (.+) changed/;
#		$subs=$1;
#		$action="Up";
#		$key="LAYER2UP:";
#	} elsif( /PORTLISTEN:/ ) {
#		$key="PORTLISTEN:";
#		print "MSG=$_\n";

	if( /UPDOWN:/ ) {
		/Interface (.+),/;
		$subs=$1;
		$state="WARNING";
		$state="ERROR" if /down$/;
		$state="OK"    if /up$/;
		$key="(UPDOWN)";
		$message="$key host=$host subsystem=$subs state=$state\n";
	} elsif( /STATECHANGE:/ ) {
		/STATECHANGE: (.+) state (.+)$/;
		$subs=$1;
		$action=$2;
		$subs =~ s/Standby:\s+\d+:\s+//;
		$state="ERROR";
		$state="OK" if $action=~/\sSpeak$/
			or $action=~/\sActive$/
			or $action=~/\sInit$/;

		$key="(STATECHANGE)";
		$message="$key host=$host subsystem=$subs state=$state\n";
	} elsif( /ALARM:/ or /INFO:/) {
		$state="OK";
		$message=$_;
		pr_output( "ALARM/INFO EVENT : NO SPECIFIC HANDLER IMPLEMENTED\n\thost=$host subsystem=$subs state=$state\n\tMessage=$message\n" );
		MlpEvent(
			-monitor_program	=> $PROGNAME,
			-system			=> $host,
			-subsystem		=> $subs,
			-event_time		=> $datestring,
			-message_text		=> $message,
			-severity		=> $state ) unless $NOLOG;
		return 1;
	} else {
		if( /NONTRUNKPORTON/
		or  /TRUNKPORTON/ ) {
			$state="OK";
			/:(\w+\s+\d\/\d)\s/;
			$subs=$1;
		} elsif( /SUP_IMGSYNCFINISH:/ or /MOD_REMOVE:/ ) {
			$state="OK";
			$subs=undef;
		} else {
			$state="WARNING";
			$subs=undef;
		}
		$message=$_."\n";
		pr_output( "MISC EVENT : DATE=$datestring STATE=$state SYSTEM=$host SUBSYS=$subs MSG=$message\n" );
		MlpEvent(
			-monitor_program	=>	$PROGNAME,
			-system			=> $host,
			-subsystem		=> $subs,
			-message_text		=> $message,
			-event_time		=> $datestring,
			-severity		=> $state ) unless $NOLOG;
		return 1;
	}
	pr_output( "Heartbeat : STATE=$state SYSTEM=$host SUBSYS=$subs MSG=$message\n" );
	MlpHeartbeat(
		-monitor_program	=>	$PROGNAME,
		-system			=> $host,
		-event_time		=> $datestring,
		-subsystem		=> $subs,
		-message_text		=> $message,
		-state			=> $state ) unless $NOLOG;
	return 1;
}

sub pr_output {
	if( defined $OUTFILE ) {
		print OUTFILE @_;
		print @_ if defined $DEBUG;
	} else {
		print @_;
	}
}

__END__

=head1 NAME

cisco_logmon.pl - Read Cisco Logs And Alarm Appropriately

=head2 DESCRIPTION

This perl module parses and reads the cisco logs.

=head2 FILTER RULES

There are a number of paterns that are excluded.  The main interest of this program is whether lines are up or down.  Consequently, we search on the following mesages:

=over 4

=item * UPDOWN:

=item * STATECHANGE:

=item * ALARM: or INFO:

=item * RANDOM Less Used Stuff

=back

=cut
