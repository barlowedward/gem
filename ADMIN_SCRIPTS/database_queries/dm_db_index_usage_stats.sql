-- report_key=index_usage_stats
-- report_type=PERFORMANCE
-- run_frequency=daily
-- do_diff=Y
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=Y
-- run_query_in_sys_db=N
-- report_header=
-- report_footer=
-- enabled=Y
-- to_do=add rowcount > 10000 rows and filter on some proxy for usage
-- performancetype=int
-- performancekey=index_usage_stats

/*
select @@SERVERNAME,db_name(x.database_id) as [db name]
      ,object_name(x.object_id) as [object name]
      -- ,x.object_id,
      i.name as [index name],
      x.index_id
      ,(user_seeks + user_scans + user_lookups + user_updates) as [usage count]
      ,user_seeks, user_scans, user_lookups, user_updates

  from sys.dm_db_index_usage_stats x, sys.indexes i
where objectproperty(x.object_id, 'IsUserTable') = 1
   and x.database_id not in (	db_id(N'master'), 			db_id(N'model'),
   									db_id(N'distribution'), 	db_id(N'msdb'),
   									db_id('tempdb'), 				db_id('distribution'),
   									db_id('ReportServer'), 		db_id('ReportServerTempDB'))
   and i.object_id = x.object_id
   and i.index_id = x.index_id
   AND		objectproperty(s.object_id,'IsUserTable') = 1
order by [db name], [usage count]
*/
/*
select @@SERVERNAME,db_name(x.database_id) as [db name]
      ,object_name(x.object_id) as [object name],
      i.name as [index name],
      x.index_id
      ,(user_seeks + user_scans + user_lookups + user_updates) as [usage count]
      ,user_seeks, user_scans, user_lookups, user_updates
 from sys.dm_db_index_usage_stats x, sys.indexes i
where objectproperty(x.object_id, 'IsUserTable') = 1
   and x.database_id not in (db_id(N'master'), db_id(N'model'), db_id(N'distribution'), db_id(N'msdb'))
   and i.object_id = x.object_id
   and i.index_id = x.index_id
 --  AND		objectproperty(i.object_id,'IsUserTable') = 1
order by [db name], [usage count]
*/

select @@SERVERNAME,db_name(x.database_id) as [db name]
      ,object_name(x.object_id,x.database_id) as [object name],
      i.name as [index name],
      x.index_id
      ,(user_seeks + user_scans + user_lookups + user_updates) as [usage count]
      ,user_seeks, user_scans, user_lookups, user_updates
from  sys.dm_db_index_usage_stats x, sys.indexes i
where x.database_id=db_id()
 and  x.index_id=i.index_id
 and x.database_id not in (	db_id(N'master'), 			db_id(N'model'),
   									db_id(N'distribution'), 	db_id(N'msdb'),
   									db_id('tempdb'), 				db_id('distribution'),
   									db_id('ReportServer'), 		db_id('ReportServerTempDB'))
 and  x.object_id = i.object_id
 AND		objectproperty(i.object_id,'IsUserTable') = 1
 and    (user_seeks + user_scans + user_lookups + user_updates) >100
  -- order by [db name], [usage count]
