create procedure INV_UpdDict4MisngAtts
as
begin
	select distinct system_type,compute_priority=convert(int,0),compute_into_type=convert(varchar(30),'')
	into #PRI
	from INV_attribute_dictionary

	update #PRI set compute_priority=a.compute_priority, compute_into_type=a.compute_into_type
		from  #PRI p, INV_attribute_dictionary a
		where  p.system_type = a.system_type
		and    a.compute_priority is not null
		and    a.compute_into_type is not null

	-- Get rid of stuff we dont care about
	Delete #PRI where compute_priority =0 or compute_into_type = ''

	-- create #TMP a list of all FEEDS
	select distinct system_type,attribute_name
	into #TMP from INV_system_attribute
	where system_type not in ('win32servers','sybase','sqlsvr','oracle','mysql','unix')

	-- Clobber #TMP where exists in the dict
	delete 	#TMP
	from 	#TMP t, INV_attribute_dictionary a
	where 	t.system_type		=	a.system_type
	and  	a.attribute_name 	=	t.attribute_name

	-- put these things new attributes the dictionary
	insert INV_attribute_dictionary ( system_type,attribute_name,attribute_type,master_display_key,detail_display_key )
	select t.system_type,t.attribute_name,'STRING',0,0
	from #TMP t

	-- copy values you saved earlier for the feed
	update 	INV_attribute_dictionary
	set 		compute_priority  = p.compute_priority,
				compute_into_type = p.compute_into_type,
				compute_into_name = t.attribute_name
	from 		INV_attribute_dictionary t, #PRI p
	where 	t.system_type=p.system_type
	and   	t.compute_priority 	is null
	and		t.compute_into_type 	is null
	and 		t.compute_into_name 	is null

	update 	INV_attribute_dictionary
	set	 	compute_priority  = isnull(compute_priority,0),
				compute_into_type = isnull(compute_into_type,'EXISTS'),
				compute_into_name = isnull(compute_into_name,attribute_name)
	from 		INV_attribute_dictionary t
	where    t.compute_priority 	is null
	or			t.compute_into_type 	is null
	or 		t.compute_into_name 	is null

	-- ok get any base type attributes - this could be from compute into types...
	select 	distinct attribute_name, system_type, attribute_type='STRING', master_display_key=0, detail_display_key=0
	into    	#SAtts
	from 		INV_system_attribute
	where 	system_type in ('win32servers','sybase','sqlsvr','oracle','mysql','unix')

	update   #SAtts set detail_display_key = d.detail_display_key
	from 		#SAtts a, INV_attribute_dictionary d
	where    a.attribute_name = d.compute_into_name

	update   #SAtts set master_display_key = d.master_display_key
	from 		#SAtts a, INV_attribute_dictionary d
	where    a.attribute_name = d.compute_into_name

	delete 	#SAtts
	from 		#SAtts t, INV_attribute_dictionary a
	where 	t.system_type		=	a.system_type
	and  		a.attribute_name 	=	t.attribute_name

	insert 	INV_attribute_dictionary ( system_type,attribute_name, attribute_type, master_display_key, detail_display_key, compute_priority, compute_into_type, compute_into_name )
	select 	system_type,attribute_name, attribute_type, master_display_key, detail_display_key, null,null, null
	from 		#SAtts
end
go
grant execute on INV_UpdDict4MisngAtts to public
go
