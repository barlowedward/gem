# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package MlpReports;

@ISA      = (	'Exporter'	);
@EXPORT   = ( 	'paint', 'get_report_list' );

use strict;
use MlpAlarm;
use Tk;
use Tk::ErrorDialog;
use Do_Time;
use Tk::Table;
use CommonFunc;
use Data::Dumper;
use Carp;

my(@cell_array,@cell_colors);
my(%MlpOperationsReportList)=(
			"Servers Definition"					=> "Production",
			"Ignore Server & Program" 			=> "Ignore List",
			"Rebuild Server Groups"				=> "Rebuild Containers",
			"Systems by Groups"					=> "Containers",
			"System to Group Mapping"			=> "System->Container",
			"Program to Group Mapping"			=> "Program->Container",
			"Delete by Monitor"					=> "Delete By Monitor",
			"Delete by Monitor & Type"			=> "Delete By Monitor_Type",
			"Delete by Monitor/System/Type"	=> "Delete By Row",
			"Report Setup"							=> "Report Setup",
			"Alarm History"						=> "Alarm History",
			"Alarm Routing"						=> "Alarm Routing",
			"Operator Definition"				=> "Operator",
			"Operator Schedules"					=> "Operator Schedule",
			"Filter Repeated Messages"			=> "Filter Repeated Messages"
			);
	
	my(%colormap)=(
			t_row_header	=>   	'black beige',
			t_black			=> 	'black white',
			t_out_of_date	=>  	'blue white',
			t_red				=>   	'blue red',
			t_normal			=>		'black white',
			t_grey			=>  	'blue grey',
			t_orange			=>		'blue orange' );
			
my($internal_debug)=1;
my(%add_row_variables);	# these are the variables for add row / special

	
# REQUIRED ARGS
#		-toplevel
#		-module_frame
sub new {
	my( $class,%args) = @_;
	my 	$package = {};
	bless	$package, $class;
	
	foreach ( keys %args ) { $package->{$_} = $args{$_}; }	
	$package->{tableObject}=undef;	
	$package->{timevalues}=\("2 Hours","4 Hours","8 Hours","Since 4PM","1 Day","3 Days", "5 Days");
	die "Must Pass -module_frame" unless $args{-module_frame};
	$package->{-module_frame} = $args{-module_frame};	
	$package->{colormap}		=\%colormap;	
	@cell_array=\();	# an array of arrays
	@cell_colors=\();	# ditto
	#XX#$package->{maxcolnum}=0;
		
	return $package;	
}

sub init {
	my($package)=@_;
	my(@groups)			=	MlpGetContainer( -type=>'c');
	my(@systems) 		= grep(!/^\s*$/,MlpGetContainer(-screen_name=>'All'));
	unshift @systems,	"[ All Systems ]";	
	$package->{Initialized}		= 1;
	$package->{MlpGroups}		=\@groups;
	$package->{MlpSystems}		=\@systems;
	$package->{NumMlpSystems}	=$#systems;

}

sub get_report_list { return keys %MlpOperationsReportList; }

sub tableclear {
	my($package)=shift;
	
	@cell_array=\();
	@cell_colors=\();
	undef %add_row_variables;
	main::_statusmsg( "[tblmgr] Clearing Table!\n" );
	my($DBGTM1)=time;
	$package->{tableObject}->destroy() if defined $package->{tableObject} and Tk::Exists($package->{tableObject});
	#XX# $package->{tableObject} = $package->{-module_frame}->Table(
	#XX# 				#-rows=>$#cell_array, -columns=>$package->{maxcolnum},
	#XX# 				-scrollbars=>"osoe", -fixedrows=>1 );
	$package->{tableObject} = $package->{-module_frame}->Scrolled('Frame',-scrollbars=>"osoe");
	#XX#$package->{maxcolnum}=0;
   main::_statusmsg( "[tblmgr]   Table Cleared in ",time-$DBGTM1, " Seconds \n");   
}

sub tableput {	
	my($package,$row,$col,$itm,$color)=@_;
	if( ! defined $cell_array[$row] ) {			# create data row
		my(@newrow, @newcolor);
		$cell_array[$row]	=\@newrow;
		$cell_colors[$row]=\@newcolor;
	}
	
	#XX#$package->{maxcolnum}=$col if $package->{maxcolnum}<$col;
	
	if( defined $cell_array[$row]->[$col] ) {
		$cell_array[$row]->[$col]	.= "\n".$itm;
	} else {
		$cell_array[$row]->[$col]=$itm;
		$cell_colors[$row]->[$col]=$color;
	}
}

sub tableshow {
	my($package)=shift;	
	#XX# $package->{tableObject}->configure( -rows=>$#cell_array, -columns=>($package->{maxcolnum}+1));
	my($rownum)=0;
	foreach my $row ( @cell_array ) {
		my($colnum)=0;
		foreach ( @$row ) {
			my($fg,$bg)=split(/\s/,$package->{colormap}{$cell_colors[$rownum]->[$colnum]});			
			if( ref $_ ) {
				#XX# $package->{tableObject}->put($rownum,$colnum++,$_);
				$_->grid(-row=>$rownum, -column=>$colnum++, -sticky=>"ew");
			} else {
				#XX# $package->{tableObject}->put($rownum,$colnum++,
				#XX# 	$package->{tableObject}->Label(-text=>$_, #-font=>$font_normal,
				#XX# 		-justify=>'left', -relief=>'sunken', -anchor=>'w',
				#XX# 		-background=>$bg, -foreground=>$fg));
				$package->{tableObject}->Label(-text=>$_, 
				 		-justify=>'left', -relief=>'sunken', -anchor=>'w',
				 		-background=>$bg, -foreground=>$fg)->grid(-row=>$rownum, -column=>$colnum++, -sticky=>"ew");
			}
		}
		$rownum++;
	}

	$package->{tableObject}->configure( -scrollbars=>'osoe' );
	$package->{tableObject}->update();
	$package->{tableObject}->pack(-side => "top", -anchor => 'nw', -expand => 1, -fill   => 'both');
	$package->{tableObject}->update();
}

sub process_button {
	my($package)=shift;
	my(%args)=@_;	
	main::_statusmsg( "[tblmgr] process_button( -buttonid=>$args{-buttonid},-rowid=>$args{-rowid})" );
	print Dumper \%args;
	if( defined $internal_debug ) {
		foreach ( keys %add_row_variables ) {
			main::_statusmsg( "[DEBUG] ... add_row_variables key $_ val=$add_row_variables{$_}\n" );
		}
	}
	
	if( $args{-buttonid} == 1 ) {
		if( defined $internal_debug ) {
			main::_statusmsg( "[DEBUG] Updating A Row\n" );			
		}
		main::_statusmsg( "[DEBUG]",join("\n",MlpManageData(
						Admin=>$MlpOperationsReportList{$package->{MlpPageType}},
						-debug=>$internal_debug, 
						Operation=>"add", %add_row_variables)),"\n" );
	} elsif( $args{-buttonid} == 2 ) {
		if( defined $internal_debug ) {
			main::_statusmsg( "[DEBUG] Adding New Item\n" );			
		}

		main::_statusmsg( "[DEBUG]",join("\n",MlpManageData( 
							Admin=>$MlpOperationsReportList{$package->{MlpPageType}},
							-debug=>$internal_debug, 
							Operation=>"add", %add_row_variables)),"\n" );
	} elsif( $args{-buttonid} == 3 ) {
		main::_statusmsg( "[DEBUG] Approving A Row\n" ) if defined $internal_debug;
		my($i)=0;
		my(%args_to_pass);
		foreach (@{$args{-data}}) {
			main::_statusmsg( "   DBG: ${$args{-hdr}}[$i]  ... ${$args{-data}}[$i] \n" )
				if defined $internal_debug;

			# woo hoo... i just wrote the following line of perl and it is completely indecipherable...
			# ive hit that point...
			$args_to_pass{${$args{-hdr}}[$i]} = ${$args{-data}}[$i];
			$i++;
		}
		main::_statusmsg( "[DEBUG]",join("\n",MlpManageData( 
						-debug=>$internal_debug, 
						OkId=>1, 
						Operation=>"Ok", 
						%args_to_pass)),"\n" );

	} elsif( $args{-buttonid} == 4 ) {
		main::_statusmsg( "[DEBUG] Deleting A Row\n\t".join(",",@{$args{-data}}),"\n" ) if defined $internal_debug;
		my($i)=0;
		my(%args_to_pass);
		foreach (@{$args{-data}}) {
			$args_to_pass{${$args{-hdr}}[$i]} = ${$args{-data}}[$i];
			$i++;
		}
		main::_statusmsg( "[DEBUG] ",join("\n",
				MlpManageData( 						
						Admin=>$MlpOperationsReportList{$package->{MlpPageType}},
						OkId=>1, 
						-debug=>$internal_debug, 
						Operation=>"del", 
						%args_to_pass)),"\n" );
	} else {
		die "Program Error - Button Id Out Of Range\n";
	}
	$package->paint( $package->{MlpPageType} );
}

sub paint {
	my($package,$screen)=@_;
	$package->init() unless $package->{Initialized}	== 1;

	my($STARTPAINTTM)=time;
	main::_statusmsg( "[tblmgr] ==================== redraw screen $screen started ====================\n" );
	main::busy();														# set cursor to an hourglass	
	
	$package->{MlpPageType} = $screen;
	
	$package->tableclear();					# clear/rebuild object
					
	if( $package->{MlpPageType} eq "Rebuild Server Groups" ){	
		MlpManageData( -debug=>$internal_debug, Admin=>"Rebuild Containers");		
		main::unbusy();	# set it to a normal cursor
		$package->{-toplevel}->messageBox( -type=>'OK',	-message => "Containers Rebuilt" );
		return;
	}

	my($DBGTM1)=time;
	main::_statusmsg("[tblmgr] Fetching Data For Page '",$package->{MlpPageType},"'\n" );
	my($reportname) = $MlpOperationsReportList{$package->{MlpPageType}};		
	
	my($rc)=MlpRunScreen( -screen_name=>$reportname, -html=>0, -debug=>$internal_debug );
	main::_statusmsg( "[tblmgr]   Table Data Fetched ($#$rc Rows) in ",time-$DBGTM1, " Seconds \n");   

	if( ! defined $rc ) {
		$package->{-toplevel}->messageBox( -type=>'OK', -message => "Screen ($package->{MlpPageType}) returned undefined" );
		main::_statusmsg( "[tblmgr] Screen ($package->{MlpPageType}) returned undefined\n" );
		main::unbusy();	# set it to a normal cursor
		return;
	}

	my( @data_rows )= @$rc;
	my( $hdr_row )=shift(@data_rows);
	#print "DBG DBG: HEADER RETURNED is ",join(" ",@$hdr_row),"\n";
	my($operation)= $$hdr_row[$#$hdr_row];
	#print "DBG DBG: Operation=$operation\n";
	if( $operation eq "Both" or $operation eq "Update" or $operation eq "Delete" or $operation eq "Add" ){
		pop 	@$hdr_row;
		push 	@$hdr_row,"Operation";
	}

	$DBGTM1=time;	
	main::_debugmsg( "[tblmgr] Page=[$package->{MlpPageType}] Header is ".join(",",@$hdr_row) );

	my(%chkbox_mask);
	my($count)=0;

	#
	# Print The Header Row
	#
	main::_statusmsg( "[tblmgr] Painting Data page=$package->{MlpPageType}\n" );
	my($i)=0;
	my(@textcol);
	main::_statusmsg( "[tblmgr]   Formatting Header Row at 0 secs\n" );	
	my($found_chkbox)=0;	
	foreach ( @$hdr_row ) {
		my($x)=$_;				 # to make a local copy so if we chg it its ok
		if( /^Chkbox_/ ) {
			$x=~s/^Chkbox_//;
			$chkbox_mask{$count}=$x;
			$found_chkbox=1;
			#$operation="Chkbox";
		}
		$x=~s/_YN_LU$//;
		$x=~s/_LU$//;
		$x=~s/\_/ /g;
		$x=~s/Min$/\nMin/g;
		$x=~s/^Use Pager/Pager/g;
		$x=~s/^Use Email/Email/g;
		$x=~s/^Use Netsend/Netsend/g;
		push @textcol,$i if $x=~/Text$/ or $x=~/Alarm Subject/ or $x=~/Alarm Message/;

		if( $x eq "OkDelete" ) {
			$package->tableput(0,$count++, "", "t_row_header");
			$package->tableput(0,$count++, "", "t_row_header");
		} else {
			# for heartbeats
			$package->tableput(0,$count, $x, "t_row_header");				
			$count++ unless ((($package->{MlpPageType} eq "Heartbeats" or $package->{MlpPageType} eq "Batch Jobs")
						and ($i==0 or $i==2 or $i==4))
					or ($package->{MlpPageType} eq "Events" and ( $i==0 or $i==2)));
		}
		$i++;
	}	

	#
	# Reword any columns that are text to be more normal by adding line feeds
	#
	main::_statusmsg( "[tblmgr]   Rewording at ".(time-$DBGTM1)."secs\n" );
	foreach my $tc (@textcol) {
		my($maxtextlen);
		foreach my $row ( @data_rows ) {
			$$row[$tc] = reword( $$row[$tc],40,45);
			$$row[$tc] =~ s/\s+$//;
		}
	}

	#
	# if the last item on the header row is "Operation" then include add line if needed
	#
	my($colcount)=0;
	my($has_header_row)=0;
	if(  $operation eq "Both" or $operation eq "Update" or $operation eq "Add" ){
		$has_header_row=1;
		main::_statusmsg( "[tblmgr]   Special Header ($operation) at ".(time-$DBGTM1)."secs\n" );
		foreach my $hdr_itm ( @$hdr_row ) {
			#print " DBG DBG: Processing Header Item [$hdr_itm]\n";			
			if( $hdr_itm eq 'Operation' ) {
				if( $operation eq 'Update' ) {
					#my($w) = $package->make_button(-widget=>$package->{tableObject},-activebackground=>'beige',
					#	-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
					#	-help=>'Update this item',
		    		#	-text=>"Update Row", -width=>11,
		    		#	-command=> sub  { $package->process_button(-buttonid=>1,-rowid=>"Special" ); } );
		    			
		    		my($w)=$package->{tableObject}->Button(
		    							-activebackground=>'beige',
										-pady=>-1,
										-highlightthickness=>0,
										-borderwidth=>1,
										-text=>"Update Row", 
										-width=>11,
		    							-command=> sub  { $package->process_button(-buttonid=>1,-rowid=>"Special" ); }
		    							);
					
	
					$package->tableput(1,$colcount, $w, "t_row_header");
				} else {
					#my($w) = $package->make_button(-widget=>$package->{tableObject},-activebackground=>'beige',
					#	-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
					#	-help=>'Add This item',
		    		#	-text=>"Add Row", -width=>7,
		    		#	-command=> sub  { $package->process_button(-buttonid=>2,-rowid=>"Special" ); } );
					my($w)=$package->{tableObject}->Button(
		    							-activebackground=>'beige',
										-pady=>-1,
										-highlightthickness=>0,
										-borderwidth=>1,
										-text=>"Add Row", 
										-width=>7,
		    							-command=> sub  { $package->process_button(-buttonid=>2,-rowid=>"Special" ); }
		    							);
					$package->tableput(1,$colcount, $w, "t_row_header");
				}
			} elsif( $hdr_itm=~/_LU$/ 
			and ($hdr_itm=~/System_LU/ or $package->{NumMlpSystems}<30 )) {				
				my(@x,$xptr);			
				if( $hdr_itm=~/Start/ or $hdr_itm=~/End/ ) {
					for (0..24) { push @x, $_; }					
				} elsif( $hdr_itm=~/User_LU/ ) {
					@x=MlpGetOperator();									
				} elsif( $hdr_itm=~/Program_LU/ ) {
					@x=MlpGetProgram();
					unshift @x, "All Programs" if $reportname eq "Alarm Routing";					
				} elsif( $hdr_itm=~/System_LU/ ) {
					$xptr = $package->{MlpSystems};					
				} elsif( $hdr_itm=~/Severity_LU/ ) {
					@x=MlpGetSeverity();					
				} elsif( $hdr_itm=~/^Use/ or $hdr_itm=~/YN_LU/ ) {
					@x = ("YES","NO");					
				} elsif( $hdr_itm=~/^Container_LU/ ) {
					$xptr = $package->{MlpGroups};
				} elsif( $hdr_itm=~/^Time_LU/ ) {
					$xptr = $package->{timevalues};					
				} elsif( $hdr_itm=~/^Enabled/ ) {
					@x = ("YES","NO");					
				} else {
					@x = ("Invalid Lookup $hdr_itm","Please Seek Help");					
				}
				#print "DBG DBG : ",join(" ",@x),"\n";
				$xptr = \@x unless $xptr;
				
				#print "DBG DBG; adding add_row_variables for $hdr_itm \n";
				my($w)=$package->{tableObject}->Optionmenu(
						-background=>'yellow',
						-options=>$xptr,
						-variable=>\$add_row_variables{$hdr_itm} );

				$package->tableput(1,$colcount, $w, "t_black");
			} else {
				#print "DBG DBG; adding add_row_variables for $hdr_itm \n";
				my($length)=10;
				$length = 2 if $hdr_itm=~/Use_/;
				my($w) = $package->{tableObject}->Entry(
					-background=>'yellow',
					-textvariable=>\$add_row_variables{$hdr_itm},
					-width=>$length );
				$package->tableput(1,$colcount, $w, "t_row_header");
			}
			$colcount++;
		}
	}

	#
	# Save the actual data in a format suitable for printing in the next step
	#
	main::_statusmsg( "[tblmgr]   Starting Formatting Data at ".(time-$DBGTM1)."secs - operation=$operation\n" );
	my($rowcount)= $has_header_row;
	foreach my $row ( @data_rows ) {
		$rowcount++;
		if( $rowcount > 2000 ) {
			main::_statusmsg( "[tblmgr]   Exiting - More than 2000 Rows Returned... Not processing remainder.\n" );
			main::_statusmsg( "[tblmgr]   Please use the web script these if its necessary.\n" );
			last;
		}

		#
		# SET THE COLOR
		#
		my($color)	=	"t_black";		
		
		if( 	$operation eq "Both"
		or 	$operation eq "Ok"
		or 	$operation eq "OkDelete"
		#or		$operation eq "Chkbox"
		or	$found_chkbox==1		
		or 	$operation eq "Delete" ) {
			# button name del_butncnt
			# hidden row values are sv_butncnt_key = value
			my($i)=0;
			my($colcount)=0;
			foreach my $cell ( @$row ) {	
				if( defined $chkbox_mask{$i} ) {
					my($col)=$chkbox_mask{$i};
					my($w)=$package->{tableObject}->Frame( -relief=>'sunken', -background=>'grey' );
					my($g)=$w->Checkbutton(
						-text=>"",	
						-variable=>\$cell,-background=>'grey',
						-command => sub {
							if( $col eq "Ignore" ) {
								main::_statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug,
									Admin=>$MlpOperationsReportList{$package->{MlpPageType}},
									Server => $$row[0],
									Value => $cell,
									-debug=>$internal_debug,
									Operation=>"setignore")),"\n");
							} elsif( $col eq "Is_Production" ) {
								main::_statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug,
									Admin=>$MlpOperationsReportList{$package->{MlpPageType}},
									Server => $$row[0],
									Value => $cell,
									-debug=>$internal_debug,
									Operation=>"setproduction")),"\n");
							} elsif( $col eq "Blackout" ) {
								main::_statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug,
									Admin=>$MlpOperationsReportList{$package->{MlpPageType}},
									Server => $$row[0],
									Value => $cell,
									-debug=>$internal_debug,
									Operation=>"setblackout")),"\n");
							} else {
								die "Unknown Column Name $col\n";
							}
						}
						)->pack();
					$package->tableput($rowcount,$colcount++,$w, $color);
					
				} else {
					$package->tableput($rowcount,$colcount,$cell, $color);

					$colcount++ unless ((($package->{MlpPageType} eq "Heartbeats" or $package->{MlpPageType} eq "Batch Jobs")
						and ($i==0 or $i==2 or $i==4))
					or ($package->{MlpPageType} eq "Events" and ( $i==0 or $i==2)));
				}
				$i++;
			}

			my($rid)=$rowcount - $has_header_row;			
			
			if( $operation eq "OkDelete" ) {
#				my($w) = $package->make_button(-widget=>$package->{tableObject},-activebackground=>'beige',
#						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
#    						-help=>'Mark this row as reviewed-ok for 1 day',
#			    			-text=>"Review/OK", -width=>11,
#			    			-command=> sub  { $package->process_button(-buttonid=>3,-rowid=>$rid,
#			    						-hdr=>$hdr_row,
#			    						-data=>$data_rows[$rid-1] ); } );
			    my($w)=$package->{tableObject}->Button(
		    							-activebackground=>'beige',
										-pady=>-1,
										-highlightthickness=>0,
										-borderwidth=>1,
										-text=>"Review/OK", 
										-width=>11,
		    							-command=> sub  { $package->process_button(-buttonid=>3,-rowid=>$rid,-hdr=>$hdr_row,
			    																-data=>$data_rows[$rid-1] ); }
		    							);
					
				$package->tableput($rowcount,$colcount++,$w, $color);
#				my($w2) = $package->make_button(-widget=>$package->{tableObject},-activebackground=>'beige',
#						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
#    						-help=>'Permanantly Delete This Row',
#			    			-text=>"Delete", -width=>7,
#			    			-command=> sub  { $package->process_button(-buttonid=>4,-rowid=>$rid,
#			    					-hdr=>$hdr_row,
#			    					-data=>$data_rows[$rid-1] ); } );
			    my($w2)=$package->{tableObject}->Button(
		    							-activebackground=>'beige',
										-pady=>-1,
										-highlightthickness=>0,
										-borderwidth=>1,
										-text=>"Delete", 
										-width=>7,
		    							-command=> sub  { $package->process_button(-buttonid=>4,-rowid=>$rid,-hdr=>$hdr_row,
			    																-data=>$data_rows[$rid-1] ); }
		    							);
			
			   $package->tableput($rowcount,$colcount++,$w2, $color); 					
    			
				
			} elsif( $operation eq "Ok" ) {
#				my($w) = $package->make_button(-widget=>$package->{tableObject},-activebackground=>'beige',
#						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
#    						-help=>'Ok This Item',
#			    			-text=>"Ok", -width=>2,
#			    			-command=> sub  { $package->process_button(-buttonid=>3,-rowid=>$rid,
#			    					-hdr=>$hdr_row,
#			    					-data=>$data_rows[$rid-1] ); } );
#			    					
	 		    my($w)=$package->{tableObject}->Button(
		    							-activebackground=>'beige',
										-pady=>-1,
										-highlightthickness=>0,
										-borderwidth=>1,
										-text=>"OK", 
										-width=>11,
		    							-command=> sub  { $package->process_button(-buttonid=>3,-rowid=>$rid,-hdr=>$hdr_row,
			    																-data=>$data_rows[$rid-1] ); }
		    							);
	 					
				$package->tableput($rowcount,$colcount++,$w, $color);
			} elsif( $operation ne "Chkbox") {
#				my($w) = $package->make_button(-widget=>$package->{tableObject},-activebackground=>'beige',
#						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
#    						-help=>'Delete this item',
#			    			-text=>"Delete", -width=>7,
#			    			-command=> sub  { $package->process_button(-buttonid=>4,-rowid=>$rid,
#			    					-hdr=>$hdr_row,
#			    					-data=>$data_rows[$rid-1] ); } );
			    my($w)=$package->{tableObject}->Button(
		    							-activebackground=>'beige',
										-pady=>-1,
										-highlightthickness=>0,
										-borderwidth=>1,
										-text=>"Delete", 
										-width=>7,
		    							-command=> sub  { $package->process_button(-buttonid=>4,-rowid=>$rid,-hdr=>$hdr_row,
			    																-data=>$data_rows[$rid-1] ); }
		    							);
	
				$package->tableput($rowcount,$colcount++,$w, $color);				
			};
		} else {
			my($colcount)=0;
			my($i)=0;
			foreach my $x (@$row) {
				$package->tableput($rowcount,$colcount,$x, $color);
				$colcount++ unless ((($package->{MlpPageType} eq "Heartbeats" or $package->{MlpPageType} eq "Batch Jobs")
						and ($i==0 or $i==2 or $i==4))
					or ($package->{MlpPageType} eq "Events" and ( $i==0 or $i==2)));
				$i++;
			}
		}
	}
	main::_statusmsg( "[tblmgr]   Table Defined at ",time-$DBGTM1, " Seconds \n");   

#
# Show the data
#
	$package->tableshow($package->{tableObject});
	main::_statusmsg( "[tblmgr]   Table Displayed at ",time-$DBGTM1, " Seconds \n");   
	
#
# We are Done
#
	main::_statusmsg( "[tblmgr] Refresh Completed: $rowcount Rows Have Been Displayed at ",time-$DBGTM1, " Seconds \n");  
	main::unbusy(1);	# set it to a normal cursor
	main::_statusmsg( "[tblmgr] Ready at ".localtime(time)." (totaltime=",(time-$STARTPAINTTM)," secs) : $rowcount rows ====================\n" );
}

#sub make_button {
#	my($package)=shift;
#	
#	my(%args)=@_;
#	my($widget)=$args{-widget};
#	my(%newarg);
#	foreach ( keys %args ) {
#		next if /^-widget/ or /^-help/;
#		$newarg{$_} = $args{$_};
#	}	
#	my($ButtonObj)=$widget->Button(%newarg);
#	main::dohelp( $ButtonObj, $args{-help} ) if $args{-help};
#	return $ButtonObj;
#}

1;