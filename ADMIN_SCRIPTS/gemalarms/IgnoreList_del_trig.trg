if exists ( select * from sysobjects where name='IgnoreList_del_trig' )
drop trigger IgnoreList_del_trig
go
create trigger IgnoreList_del_trig on IgnoreList for insert as
begin
declare @mp varchar(20),@sy varchar(30),@ss varchar(30)

if @@rowcount = 1
begin
	select @mp=monitor_program, @sy=system, @ss=subsystem
	from inserted

	update 	Heartbeat
	set 		internal_state='D'
	where 	isnull(Heartbeat.monitor_program ,'')
					= isnull(@mp, isnull(Heartbeat.monitor_program,''))
	and   	isnull(Heartbeat.system ,'')
					= isnull(@sy, isnull(Heartbeat.system,''))
	and   	isnull(Heartbeat.subsystem,'')
					= isnull(@ss, isnull(Heartbeat.subsystem,''))

	update Event
	set internal_state='D'
	where 	isnull(Event.monitor_program ,'')
					= isnull(@mp, isnull(Event.monitor_program,''))
	and   	isnull(Event.system ,'')
					= isnull(@sy, isnull(Event.system,''))
	and   	isnull(Event.subsystem,'')
					= isnull(@ss, isnull(Event.subsystem,''))
end
else
begin
	print 'ERROR CAN NOT DO MULTI ROW UPDATES'
	rollback transaction
end
/*
	update Heartbeat
	set internal_state='D'
	from Heartbeat h,inserted i
	where isnull(h.monitor_program,'NULL')=isnull(i.monitor_program,'NULL')
	and isnull(h.system,'NULL')=isnull(i.system,'NULL')
	and isnull(h.subsystem,'NULL')=isnull(i.monitor_program,'NULL')

	update Event
	set internal_state='D'
	from Heartbeat h,inserted i
	where isnull(h.monitor_program,'NULL')=isnull(i.monitor_program,'NULL')
	and isnull(h.system,'NULL')=isnull(i.system,'NULL')
	and isnull(h.subsystem,'NULL')=isnull(i.monitor_program,'NULL')
end
*/
end
go
