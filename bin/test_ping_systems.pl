use strict;
use Repository;

use Getopt::Long;
use vars qw( $DEBUG $TYPE $SYSTEM );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless
   GetOptions( "DEBUG"		=> \$DEBUG, 
   				"TYPE=s"		=> \$TYPE,
   				"SYSTEM=s"	=>	\$SYSTEM );

$| = 1;

my($dir)=get_gem_root_dir();
chdir $dir 	or die "Cant cd to $dir: $!\n";

my(@perl_includes);
foreach (@INC) { push @perl_includes, $_ if /$dir/;  }
my($cmd)="$^X -I".join(" -I",@perl_includes)." $dir/ADMIN_SCRIPTS/bin/ping_systems.pl  --NOLOCK";
if( $SYSTEM ) {
	$cmd.=" --SYSTEM=$SYSTEM ";
} else {
	$cmd.=" --DOWNANDDIRTY ";
}
$cmd.=" --DEBUG" if $DEBUG;
$cmd.=" --TYPE=$TYPE" if $TYPE;
$cmd.=" 2>&1\n";
print $cmd if $DEBUG;

my(@warnings);
open( OUT, $cmd." |" ) or die "Cant Run Command $cmd : $!\n";
foreach (<OUT>)  {
	chomp;
	if( /hosts are/ ) { 
		print $_,"\n"; 
		next; 
	} else { 
		print $_,"\n" if $DEBUG; 
	}
	next if /^Running/ or /^Pings Started/;
	push @warnings, $_  unless /\.\.\. alive\!/ or /\*\*\* redoing/ or /^Sleeping/;
}

close(OUT);
foreach (@warnings) { print "ERROR: $_\n"; }
if( $#warnings<0 ) {  print "SUCCESSFUL: Connectivity OK\n"; }
exit( $#warnings+1)