#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Std;
use File::Basename;
use CommonFunc;

use vars qw( %CONFIG $opt_J $opt_d);

#Copyright (c) 2005-2008 by SQL Technologies.
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
   	print $0.' -Jjob [-d]';
	return "\n";
}

$| =1;


die usage("Bad Parameter List\n") unless getopts('J:d');

if( defined $opt_J ) {
	%CONFIG=read_configfile( -job=>$opt_J, -debug=>$opt_d );
	print "==== CONFIG REPORT FOR JOB $opt_J ====\n";
	foreach (sort keys %CONFIG) { printf "%25s => %s\n",$_ ,$CONFIG{$_}; }
} else {
	%CONFIG=read_configfile();
	my(@keys)=get_job_list(%CONFIG);
	print "CURRENTLY DEFINED PLANS ARE : \n";
	print join(" ",sort @keys),"\n";
}

__END__

=head1 NAME

show_configvars.pl - print out the job configuration variables as they would be used

=head2 USAGE

show_configvars.pl

=head2 SYNOPSIS

This is a diagnostic utility

show_configvars.pl

CURRENTLY DEFINED PLANS ARE :

SYB1 PLATINUM MMMDBDB_COPY XXX YYY ZZZ

or

show_configvars.pl -JJOB

show variables for the plan

 debug: CONFIG{AUDIT_PURGE_DAYS}=30
 debug: CONFIG{BASE_BACKUP_DIR}=/apps/sybmon/dev/data/BACKUP_LOGS
 debug: CONFIG{BCP_COMMAND}=
 debug: CONFIG{BCP_TABLES}=
 debug: CONFIG{CODE_LOCATION_ALT1}=
 debug: CONFIG{CODE_LOCATION_ALT2}=
 debug: CONFIG{COMPRESS}=/usr/local/bin/gzip
 debug: CONFIG{SYBASE_COMPRESSION_LEVEL}=0
 debug: CONFIG{COMPRESS_LATEST}=n
 debug: CONFIG{DBCC_IGNORE_DB}=
 debug: CONFIG{DO_AUDIT}=y
 debug: CONFIG{DO_BCP}=n
 debug: CONFIG{DO_CLEARLOGSBEFOREDUMP}=y
 debug: CONFIG{DO_EXTERNAL_COMPRESS}=y
 debug: CONFIG{DO_DBCC}=y
 debug: CONFIG{DO_DUMP}=y
 etc


=cut
