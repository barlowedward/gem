delete Screen
go
-- this is ok... container map matters container doesnt
-- delete Container
go
insert Screen values ('Events (4 Hr)', '4HourEvents','Event',null,'All')
insert Screen values ('Events (1 Dy)', '1DayEvents','Event',null,'All')
insert Screen values ('Events (3 Dy)', '3DayEvents','Event',null,'All')
insert Screen values ('Batch', 'BatchHeartbeat','HeartBeat',null,'All')
insert Screen values ('Bad Heartbeat', 'BadHeartbeat','HeartBeat',null,'All')
insert Screen values ('Monitor Agents', 'MonitorAgent','HeartBeat',null,'All')
insert Screen values ('Heartbeat',     'Heartbeat','HeartBeat',null,'All')
go
--insert Container
--select distinct 'All','s',system,null from    Event
--union
--select distinct 'All','s',system,null from    Heartbeat
go
delete HeartbeatThresh where monitor_program='load_all_tranlogs' and system is null
delete HeartbeatThresh
   where monitor_program='CheckForBlocks' and system='IMAGSYB1'
delete HeartbeatThresh
   where monitor_program='CheckUsers' and system='IMAGSYB1'
go
insert HeartbeatThresh values ('load_all_tranlogs',null,null,180)
insert HeartbeatThresh values ('CheckForBlocks','IMAGSYB1',null,120)
insert HeartbeatThresh values ('CheckUsers','IMAGSYB1',null,120)
go

delete Severity
go
insert Severity values ( 'EMERGENCY',0 )
insert Severity values ( 'CRITICAL', 1)
insert Severity values ( 'ALERT', 2)
insert Severity values ( 'ERROR', 3)
insert Severity values ( 'WARNING', 4)
insert Severity values ( 'INFORMATION', 5)
insert Severity values ( 'STARTED', 5)
insert Severity values ( 'COMPLETED', 5)
insert Severity values ( 'RUNNING', 5)
insert Severity values ( 'DEBUG', 6)
insert Severity values ( 'OK', 5)
go

delete ReportArgs where reportname = 'GemRpt_Backups'
go
insert ReportArgs values ('GemRpt_Backups','filter_severity','All')
insert ReportArgs values ('GemRpt_Backups','filter_container','All')
insert ReportArgs values ('GemRpt_Backups','filter_time','1 Day')
insert ReportArgs values ('GemRpt_Backups','ShowProgram','NO')
insert ReportArgs values ('GemRpt_Backups','ReportTitle','All Backup Messages')
insert ReportArgs values ('GemRpt_Backups','progname','BACKUPS')
go

Delete Event_Filter where monitor_program='Sybase_ASE_Log'
Delete Event_Filter where monitor_program='Win32Eventlog' and subsystem='MSSQLSERVER'
Delete Event_Filter where monitor_program='Win32Eventlog' and subsystem='ApplicationError'
go
Insert Event_Filter values ('Sybase_ASE_Log',null,null,'y',60)
Insert Event_Filter values ('Win32Eventlog',null,'MSSQLSERVER','y',60)
Insert Event_Filter values ('Win32Eventlog',null,'ApplicationError','y',60)
go
