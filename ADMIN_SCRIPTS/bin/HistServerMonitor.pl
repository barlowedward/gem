#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use MlpAlarm;
use Getopt::Long;
use CommonFunc;
use Repository;
use DBIFunc;
use Do_Time;
use Time::Local;
$|=1;

my($dbgoffsetmin) = 127;	# for debugging with -d - the bucket ur debugging
my($dbgoffsetmax) = 127;	# for debugging with -d - the bucket ur debugging
#my($hist_svr_connection);

use vars qw( $MON_SERVER $CONFIGFILE $BATCHID $DEBUG $LOGIN $OUTDIR $PASSWORD $NOLOG $DATADIR $START_MONITORING $RUNREPORTS $RUN_AVERAGES $HIST_SERVER);

my($VERSION)=2.0;

BEGIN { 	$ENV{SYBASE}='/apps/sybase'; }
$|=1;

sub usage
{
	return "Usage: HistServerMonitor.pl --NOLOG --DATADIR=directory -LOGIN=LOGIN -PASSWORD=pass --START_MONITORING -REPORTTODAY --RUN_AVERAGES --CONFIGFILE=file --MON_SERVER=system[,system] --DEBUG\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
		"CONFIGFILE=s"	=> \$CONFIGFILE,
		"NOLOG"		=> \$NOLOG,
		"HIST_SERVER=s"	=> \$HIST_SERVER,
		"LOGIN=s"	=> \$LOGIN,
		"PASSWORD=s"	=> \$PASSWORD,
		"OUTDIR=s"	=> \$OUTDIR,
		"DEBUG"		=> \$DEBUG,
		"START_MONITORING"	=> \$START_MONITORING,
		"RUNREPORTS"	=> \$RUNREPORTS,
		"RUN_AVERAGES"	=> \$RUN_AVERAGES,
		"BATCHID=s"	=> \$BATCHID,
		"DATADIR=s"	=> \$DATADIR,
		"MON_SERVER=s"	=> \$MON_SERVER );

$dbgoffsetmin = -1 unless $DEBUG;
$dbgoffsetmax = -1 unless $DEBUG;
my($BUCKET_MINUTES)=10;
if( ! defined $DATADIR ) {
	die "NO Data Directory" unless -d get_gem_root_dir()."/data";
	$DATADIR=get_gem_root_dir()."/data";
	md($DATADIR);
}

my($cur_mon_server)="";

sub error_out {
   my($msg)=@_;
   MlpEvent(
      -monitor_program=> $BATCHID || "HistServerMonitor",
      -system=>  $cur_mon_server,
      -message_text=> $msg,
      -severity => "ERROR"
   );
   die $msg;
}

sub warn_out {
   my($msg)=@_;
   MlpEvent(
      -monitor_program=> $BATCHID || "HistServerMonitor",
      -system=>  $cur_mon_server,
      -message_text=> $msg,
      -severity => "ERROR"
   );
   warn $msg;
}

sub md {
	my($dirtomake)=@_;
	return if -d $dirtomake;
	print "Creating directory $dirtomake\n";
	mkdir( $dirtomake, 0755 ) or error_out( "Cant mkdir $dirtomake : $!\n" );
	die "ERROR: cant make directory $dirtomake\n" unless -d $dirtomake;
}

if( defined $START_MONITORING ) {
	# ok if we start monitoring... we need to pass HIST_SERVER, MON_SERVER, LOGIN, PASSWORD
	error_out( "Can Not Start Monitoring - missing --MON_SERVER argument\n" ) 	unless $MON_SERVER;
	error_out( "Can Not Start Monitoring - missing --HIST_SERVER argument\n" ) 	unless $HIST_SERVER;
	error_out( "Can Not Start Monitoring - missing --LOGIN argument\n" ) 		unless $LOGIN;
	error_out( "Can Not Start Monitoring - missing --PASSWORD argument\n" ) 	unless $PASSWORD;

	MlpMonitorStart(-monitor_program=>$BATCHID || "HistServerMonitor");
	print "Connecting to $HIST_SERVER as $LOGIN\n";

	my($type) = "ODBC"   if defined $ENV{PROCESSOR_LEVEL};
	$type = "Sybase" unless defined $type;
	print "TYPE is $type\n";

	DBI->trace(1);
	my($rc) = DBI->connect("dbi:$type:$HIST_SERVER",$LOGIN,$PASSWORD,{RaiseError=>0,PrintError=>0,AutoCommit=>1
		,syb_chained_txn=>0
		});
	die "DONE";
#	if( ! defined $rc ) {
#		print( "Can't connect to dbi:$type:$HIST_SERVER as $LOGIN\n");
#		return 0;
	#}
	#my($rc)=dbi_connect( -connection=>$rc);

	my($rc)=dbi_connect( -srv=>$HIST_SERVER,-login=>$LOGIN,-password=>$PASSWORD,-type=>$type,-debug=>1);
        if( ! $rc ) {
        	error_out( "Cant connect to $HIST_SERVER as $LOGIN\n" );
        	next;
        } else {
        	print "Successful connection to historical server\n";
        }
        dbi_set_debug(1);

      	my(@rc)=dbi_query(-query=>"hs_status activity",-db=>"",-debug=>1);
      	my($dbprocess)=dbi_getdbh();
	$dbprocess->finish() if $dbprocess->{syb_more_results};


	foreach $cur_mon_server ( split(/,/,$MON_SERVER) ) {
           	#($LOGIN,$PASSWORD)=get_password(-type=>"sybase", -name=>$cur_mon_server)
           	#	unless defined $LOGIN and defined $PASSWORD;

		my($DAYU, $DAYS, $TIME);
        	$DAYU = do_time(-fmt=>"yyyy_mm_dd");
        	$DAYS  = do_time(-fmt=>"yyyy/mm/dd");
        	$TIME  = do_time(-fmt=>"hh:mi:ss");

        	md( $DATADIR."/RAW_DATA_FROM_HIST_SVR" );
        	md( $DATADIR."/RAW_DATA_FROM_HIST_SVR/".$cur_mon_server );
        	md( $DATADIR."/RAW_DATA_FROM_HIST_SVR/".$cur_mon_server."/".$DAYU );

#		print "Connecting to monitor server $cur_mon_server\n";
#           	my($connection)=dbi_connect(-srv=>$cur_mon_server,-login=>$LOGIN,-password=>$PASSWORD,
#           		-type=>'ODBC',-debug=>1,-connection=>1);
#        	if( ! $connection ) {
#               		error_out("Cant connect to $cur_mon_server as $LOGIN\n");
#        		next;
#        	}
#
#        	print "executing sms_status server\n";
#           	my(@rc)=dbi_query(-query=>"sms_status server",-db=>"",-debug=>1,-connection=>$connection);
#        	my($mon_servername)="FALSE";
#        	foreach ( @rc ) {
#        		my(@x)=dbi_decode_row($_);
#        		$mon_servername=$x[0];
#        	}
#
#        	print "Database Server for monitor server $cur_mon_server is $mon_servername\n";
#        	print "Disconnecting from monitor server\n\n";
#        	dbi_disconnect(-connection=>$connection);

		print "Showing Sessions and killing dups\n";
		show_sessions($cur_mon_server,$LOGIN, 0);

        	print "\nConnected successfully to $cur_mon_server\n\tat ".localtime(time)."\n";
        	run_startup_queries($cur_mon_server,$DAYS, $DATADIR."/RAW_DATA_FROM_HIST_SVR/".$cur_mon_server."/".$DAYU );
        	print "Completed at ".localtime(time)."\n\n";
        	show_sessions("","",1);
        	#dbi_disconnect();
        }
       	MlpBatchDone();
       	exit(0);
} elsif( $RUN_AVERAGES ) {
	print "Started Averages Compilation at ".localtime(time)."\n";
	print "DATA DIRECTORY is $DATADIR\n";
	opendir(DIR,"$DATADIR/RAW_DATA_FROM_HIST_SVR") ||
		die("Can't open directory $DATADIR/RAW_DATA_FROM_HIST_SVR for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);


	#averageit( 	-DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/IMAGSYB1_MON",
        #		-FILEENDING=>"lock_perf_sum",
        #		-GREP_STRING=>",1,1,",
        #		-debug=>1,
        #		-OUTFILE=>"IMAGSYB1_MON.excl_tbl_lock_avg.dat" );


	foreach my $MON_SERVER (@dirlist) {
		next unless -d "$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER";
		print "Working on $MON_SERVER\n";
        	averageit(
        		-DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"server_perf_sum",
        		-OUTFILE=>"$MON_SERVER.server_perf_sum.dat" );
        	averageit(
        		-DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"cache_perf_sum",
        		-OUTFILE=>"$MON_SERVER.cache_perf_sum.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"device_io",
        		-OUTFILE=>"$MON_SERVER.device_io.dat" );
        	averageit(
        		-DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"device_io",
        		-PEAK_ONLY=>1,
        		-OUTFILE=>"$MON_SERVER.peak_device_io.dat" );
        	averageit(
        		-DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"lock_perf_sum",
        		-GREP_STRING=>",1,1,",
        		-OUTFILE=>"$MON_SERVER.excl_tbl_lock_avg.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"lock_perf_sum",
        		-GREP_STRING=>",6,1,",
        		-OUTFILE=>"$MON_SERVER.shrd_page_lock_avg.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"lock_perf_sum",
        		-GREP_STRING=>",7,1",
        		-OUTFILE=>"$MON_SERVER.upd_page_lock_avg.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"process_perf_sum",
        		-GREP_STRING=>",6,",
        		-OUTFILE=>"$MON_SERVER.process_perf_sum.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"network_activit",
        		-OUTFILE=>"$MON_SERVER.network_activity.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"procedure_cache_stats",
        		-OUTFILE=>"$MON_SERVER.procedure_cache_stats.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"page_cache_stats",
        		-OUTFILE=>"$MON_SERVER.page_cache_stats.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"transaction_activity",
        		-OUTFILE=>"$MON_SERVER.transaction_activity.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"engine_activit",
        		-OUTFILE=>"$MON_SERVER.engine_activity.dat" );
        	averageit( -DIR=>"$DATADIR/RAW_DATA_FROM_HIST_SVR/$MON_SERVER",
        		-FILEENDING=>"data_cache_sum",
        		-OUTFILE=>"$MON_SERVER.data_cache_sum.dat" );
	}
	print "Completed Averages Compilation at ".localtime(time)."\n";
} else {
	die "ERROR : not running averages and not running other\n";
}

exit(0);

##############################################################################################
sub show_sessions {
	my($cur_mon_server,$LOGIN, $show)=@_;
       	print "executing: hs_list sessions,active\n";
       	#die "NO connection\n" unless defined $hist_svr_connection;
       	my(@rc)=dbi_query(-query=>"hs_list \"sessions\",\"active\"", -db=>"", -debug=>1);	#,-connection=>$hist_svr_connection);
       	foreach ( @rc ) {
       		my($spid,$status,$owner, $sqlsvr, $monsvr, $starttm, $end, $dirtm, $sample,$err)=dbi_decode_row($_);
       		$monsvr=~s/\s+$//;
       		$owner=~s/\s+$//;

       		if( defined $show and $show==1) {
       			printf( "spid=%4d db=%20s monsvr=%20s \n\tstarttime=%20s endtime=%20s\n\tdatadirectory=%s\n",
       			$spid,$sqlsvr,$monsvr,$starttm,$end,$dirtm );
       			next;
       		}
       		if( $cur_mon_server ne "" and $LOGIN ne "") {
	       		print "comparing $monsvr eq $cur_mon_server and $owner eq $LOGIN \n" if $DEBUG;
       			next unless $monsvr eq $cur_mon_server;
       			next unless $owner eq $LOGIN;
       		}

       		# hey we have an active session
       		print "Terminating active session $spid : login=$LOGIN monsvr=$monsvr database=$sqlsvr\n";
       		my(@rc2)=dbi_query(-query=>"hs_terminate_recording $spid",-db=>"");	#,-connection=>$hist_svr_connection);
       		foreach (@rc2) {
       			my(@x)=dbi_decode_row($_);
       			print "Terminate Returns: ",join(" ",@x),"\n";
       		}
       	}
}

#if( defined $CONFIGFILE  ) {
#	open(CONFOUT,">".$CONFIGFILE) or error_out( "Cant Write $CONFIGFILE $!" );
#	foreach( keys %sqid_by_svr ) {
#		print CONFOUT $_," ",$sqid_by_svr{$_},"\n";
#	}
#	close(CONFOUT);
#}


sub run_startup_queries {
	my($MON_SVR_NAME,$DAYS,$DIR)=@_;
	my($batchsql)="";
	my($batchid)=0;
	#die "NO connection\n" unless defined $hist_svr_connection;
	my(@rc)=dbi_query(	#-connection=>$hist_svr_connection,
		-query=>"hs_create_recording_session $MON_SVR_NAME, 300, \"$DIR\",NULL,\"$DAYS 23:59\",public,continue,sybase_script",
		-db=>"");
	foreach (@rc) {
		my(@d)=dbi_decode_row($_);
		print "ERROR: ",join(" ",@d),"\n";
	}

	while(<DATA>) {
		chomp;chomp;
		if(/^go/) {
			$batchid++;
			print "Running Batch Number $batchid\n";
			print "$batchsql\n" if defined $DEBUG;

			#if ( 0 ) {
			my(@rc) = dbi_query( -query=>$batchsql, -db=>"" ) ;
			foreach (@rc) {
				my(@d)=dbi_decode_row($_);
				my($rowdat)=join(" ",@d);
				next if $rowdat =~ /because it does not exist in the system catalog/;
				next if $rowdat =~ /because it doesn't exist in the system catalog/;
				print "Batch $batchsql\nError $rowdat\n\n";
			}
			#}
			$batchsql="";
		} else {
			$batchsql.=$_."\n";
		}
	}
	print "Startup Queries Completed\n";
}

# takes multiple rows and reaggregate based on time
# excludes weekends
# CAN BE PASSED DIRECTORY TO AGGREGATE ACROSS FILES or FILE TO AGGREGATE THAT
#
# ALGORITHM
#   - Read In Command Line Args
#   - Find List of Files To Aggregate into @allfiles
#   - foreach (@allfiles) {
# 			save if day30 or day90
#			read file
#				get offset from field 1
#				shift row
#				if -p {
#					if( $IN[0]=$lasttime ) {
#						@SAVE += @row
#					} else {
#						if $OUT[$bucket]
#								$OUT[$bucket] = max( @SAVE, @row)
#						else 	$OUT[$bucket] = @SAVE
#						$lasttime=$IN[0]
#					}
#				} else { $OUT[$bucket] += ( 1, @row) }
#     }
# 		save normal

#sub usage {
#	print @_;
#	print "Usage: AverageIt.pl -DDIR|FILE -FILEENDING=>FileEnding -BUCKET_MINUTES=>increment -dOFFSET -p<col> -fstr
#
#		-D  the directory to get RAW data from for the MON server
#       this directory will have a bunch of date subdirectories
#       you can find this in ./RAW_DATA_FROM_HIST_SVR/$MON_SVR
#		 if -D points to a file, ONLY that file will be used
#   		-FILEENDING=> = file ending for the input file
#   		-BUCKET_MINUTES=> = minute increments to lump data together in.
#       	-BUCKET_MINUTES=>10 would create 10 minute buckets for your output data
#
#   		-OUTFILE=><file> = output file.  Will be saved into AVGDATA, AVGDATA30, & AVGDATA90
#			subdirectories if -Ddir and into curdir if -Dfile
#   		-S<days> = number of days to include go back
#		-d = debug flag - MUST PASS IN OFFSET TO DEBUG FOR
#   		-p = assume that <col> is a text name - get peak for a time across that col
#   		-f = ignore data lines that do not have matching string
#
#Notes.  Files must be in standard date<sp>time,dat1,dat2,dat3... format \n";
#	return 0;
#}


sub averageit {
	my(%args)=@_;
        die usage("-p must be > 0\n") if defined $args{-PEAK_ONLY} and $args{-PEAK_ONLY}<=0;

        # ok list the files that you find
        die "Must pass directory or file using -DIR\n"
        	unless defined $args{-DIR} and -e $args{-DIR};
        die "Must pass extension using -FILEENDING\n"
        	if ! defined $args{-FILEENDING} and -d $args{-DIR};
        die "Must pass output file name with -OUTFILE\n"
        	unless defined $args{-OUTFILE};

        #die "-d option must have an int offset associated" unless int($args{-DEBUG}) ne "";
        #$dbgoffset=$args{-DEBUG} if defined $args{-DEBUG};

        die "$DATADIR/AVGDATA subdirectory does not exist" unless -d "$DATADIR/AVGDATA";
        die "$DATADIR/AVGDATA30 subdirectory does not exist" unless -d "$DATADIR/AVGDATA30";
        die "$DATADIR/AVGDATA90 subdirectory does not exist" unless -d "$DATADIR/AVGDATA90";

        # remove old files
        my($day30,$day90)=("","");
        my(@allfiles);
        if( -d $args{-DIR} ) {
        	unlink "$DATADIR/AVGDATA/".$args{-OUTFILE}   if -r "$DATADIR/AVGDATA/".$args{-OUTFILE};
        	unlink "$DATADIR/AVGDATA30/".$args{-OUTFILE} if -r "$DATADIR/AVGDATA30/".$args{-OUTFILE};
        	unlink "$DATADIR/AVGDATA90/".$args{-OUTFILE} if -r "$DATADIR/AVGDATA90/".$args{-OUTFILE};

        	#
        	# READ IN DIRECTORIES
        	#
        	opendir( DIR, $args{-DIR} ) or die "Cant open directory $args{-DIR} : $! \n";
        	my(@by_date_dirs) = readdir( DIR );
        	closedir( DIR ) ;

        	#
        	# eliminate weekend dates from the data
        	#
        	my(@alldirs);
        	my(@wday)= qw(Sun Mon Tue Wed Thu Fri Sat);
        	foreach (reverse sort @by_date_dirs ) {
        		next unless -d $args{-DIR}."/".$_;
        		next unless /(\d\d\d\d)\_(\d\d)\_(\d\d)$/ ;

        		# print "yy=$1 mm=$2 dd=$3 ";
        		my($t) = timelocal(0,0,0,$3,($2-1),$1);
        		# print scalar localtime($t)," ";
        		my($thisday)=@wday[(localtime($t))[6]];
        		# print "DBG: Ignoring $_ As It is $thisday \n"
        			# if $thisday eq "Sun" or $thisday eq "Sat";
        		next if $thisday eq "Sun" or $thisday eq "Sat";

        		push @alldirs, $_;
        	}

        	die "NO DATA FOUND" if $#alldirs < 0;

        	#
        	# GET FILES
        	#
        	my($daycount)=0;
        	foreach my $dir (@alldirs) {
        		$daycount++;
        		my($num)=$#allfiles;
        		opendir( DIR2, $args{-DIR}."/".$dir )
        			or die "Cant open directory $args{-DIR}/$dir : $! \n";
        		push @allfiles, grep(/$args{-FILEENDING}/, map("$args{-DIR}/$dir/$_",readdir(DIR2)));
        		closedir( DIR2 );
        		if( $num != $#allfiles ) {		# added a file!!!
        			# 20 days for the 30 day graph (exclude weekends) etc...
        			$day30 = $dir if $daycount>=20 and $day30 eq "";
        			$day90 = $dir if $daycount>=60 and $day90 eq "";
        		}
        	}
        } else {
        	unlink $args{-OUTFILE}   if -r $args{-OUTFILE};
        	push @allfiles, $args{-DIR};
        }

        # # OK NOW WE HAVE OUR LIST OF FILES
        # if( defined $args{-DIR} ) {
        #	print "FILE LIST DAY 30=$day30 DAY90=$day90 \n" if defined $args{-DIR};
        #	my($i)=0;
        #	foreach my $file (@allfiles) {
        #		print "$i $file \n";
        #		$i++;
        #	}
        #}

        my(@OUTPUTDATA);				# stored in buckets basedon -BUCKET_MINUTES=>
        foreach my $file (@allfiles) {

        	if( $day30 ne "" and $file=~ /$day30/ ) {
        		print "   Saving 30 Day Data - $day30\n" if defined $DEBUG;
        		printoutput($args{-DIR},"$DATADIR/AVGDATA30/$args{-OUTFILE}",		\@OUTPUTDATA);
        	}

        	if( $day90 ne "" and $file=~ /$day90/ ) {
        		print "   Saving 90 Day Data - $day90\n" if defined $DEBUG;
        		printoutput($args{-DIR},"$DATADIR/AVGDATA90/$args{-OUTFILE}",		\@OUTPUTDATA);
        	}

        	print "\nPROCESSING: ",$file," !!!\n" if defined $DEBUG;

        	my($last_timestamp)="";		# time for above point
        	my($last_offset)= -1;
        	my($last_file)="";
        	my(@last_data);

#print "GREP STRING IS $args{-GREP_STRING}\n";
        	open ( INFILE, $file ) or die "Cant Open $file : $!\n";
        	while( <INFILE>  ) {
        		chomp;
        		next if defined $args{-GREP_STRING} and ! /$args{-GREP_STRING}/;
        		my(@dat) = split /,/;
        		my($hr)= substr( $dat[0],11,2 );
        		my($mi)= substr( $dat[0],14,2);
        		my($bucket)= int((($hr*60 + $mi) * $BUCKET_MINUTES)/60);
print "Row bucket = $bucket for $hr hours/$mi min\n" if $bucket>=$dbgoffsetmin and $bucket<=$dbgoffsetmax;
print "Skipping row as no offset!!!\n" if $bucket eq "";
        		next if $bucket eq "";

        		print "READ:",join(" ",@dat),"\n" if $bucket>=$dbgoffsetmin and $bucket<=$dbgoffsetmax;
        		shift @dat;					# lets get rid of the date time field

        		if( defined $args{-PEAK_ONLY} ) {
        			$dat[ $args{-PEAK_ONLY} - 1 ] = -1;

        #    foreach(FILE)
        #      $lasttime="";
        #      foreach(ROW)
        #			if( $hr.$mi=$lasttime ) {
        #				@LASTDATA += @dat
        #			} else {
        #				if $lasttime ne ""
        #					if defined $OUT[$last_offset]
        #							$OUT[$last_offset] = max( @LASTDATA, @dat)
        #					else 	$OUT[$last_offset] = @LASTDATA
        #           @LASTDATA=@dat
        #				$lasttime=$hr.$mi
        #				$lastoffset=$bucket;
        #			}
        #	    if $lasttime ne ""
        #		 	if defined $OUT[$last_offset]
        #					$OUT[$last_offset] = max( @LASTDATA, @dat)
        #		 	else 	$OUT[$last_offset] = @LASTDATA
        #

        			if( $last_timestamp eq $hr.$mi ) {
        				print "SAME TIMESTAMP AS LAST ROW\n" if $bucket>=$dbgoffsetmin and $bucket<=$dbgoffsetmax;
        				# do a col by col addition into @last_data
        				my($i)=0;
        				foreach (@dat) { $last_data[$i] += $dat[$i]; $i++; }
        			} else {
        				# new timestamp so save the data
        				if( $last_timestamp eq "" ) {
        					# if here then timestamp changed and no last
        					print "NEW TIMESTAMP - FIRST ROW\n" if $bucket>=$dbgoffsetmin and $bucket<=$dbgoffsetmax;
        				} elsif( defined $OUTPUTDATA[$last_offset] ) {
        					print "TIMESTAMP CHANGE - COMBINING (last=$last_timestamp now=",$hr,$mi,")\n" if $last_offset>=$dbgoffsetmin and $last_offset<=$dbgoffsetmax;
        					# A prior record exists for offset - use max values
        					my($c)= $OUTPUTDATA[$last_offset];
        					my($i)=0;
        					#foreach (@last_data) { @$c[$i+1] = $_ if @$c[$i+1]<$_;	$i++; }
        					foreach (@last_data) { @$c[$i+1] += $_;	$i++; }
        					@$c[0]++;
        				} else {
        					# if here then timestamp changed and we have a last
        					print "TIMESTAMP CHANGE (last=$last_timestamp now=",$hr,$mi,")\n" if $last_offset>=$dbgoffsetmin and $last_offset<=$dbgoffsetmax;
        					# No prior record exists for offset - copy values
        					my(@xdat);
        					foreach (@last_data) { push @xdat , $_; }
        					unshift(@xdat,1);
        					$OUTPUTDATA[$last_offset] = \@xdat;
        				}
        				# COPY STUFF OVER SINCE ITS A NEW ROW!!!
        				my($i)=0;
        				foreach (@dat) { $last_data[$i] = $dat[$i]; $i++; }
        				print "FINAL(offset=$last_offset)=",join(" ",@{$OUTPUTDATA[$last_offset]}),"\n"
        					if defined $OUTPUTDATA[$last_offset]
        					and $last_offset>=$dbgoffsetmin and $last_offset<=$dbgoffsetmax;
        				$last_timestamp=$hr.$mi;
        				$last_offset=$bucket;
        			}
        			print " LASTD($bucket): ",join(" ",@last_data),"\n" if $bucket>=$dbgoffsetmin and $bucket<=$dbgoffsetmax;
        		} else {
        			# SAVE @dat INTO @OUTPUTDATA - FIRST COL IS NUM POINTS
        			if( defined $OUTPUTDATA[$bucket] ) {
        				my($c)= $OUTPUTDATA[$bucket];
        				print " +HIST:  ",join(" ",@$c),"\n" if $bucket>=$dbgoffsetmin and $bucket<=$dbgoffsetmax;
        				@$c[0]++;
        				my($i)=1; foreach (@dat) { @$c[$i]+=$_; $i++; }
        				$OUTPUTDATA[$bucket] = $c;
        				print " =FINAL: ",join(" ",@{$OUTPUTDATA[$bucket]}),"\n"
        					if $bucket>=$dbgoffsetmin and $bucket<=$dbgoffsetmax;
        			} else {
        				unshift(@dat,1);
        				$OUTPUTDATA[$bucket] = \@dat;
        			}
        		}
        	}

        	if( defined $args{-PEAK_ONLY} and $last_timestamp ne "" ) {	 # now lets save last row
        		if( defined $OUTPUTDATA[$last_offset] ) {
        			print "TIMESTAMP CHANGE - COMBINING\n" if $last_offset>=$dbgoffsetmin and $last_offset<=$dbgoffsetmax;
        			# A prior record exists for offset - use max values
        			my($c)= $OUTPUTDATA[$last_offset];
        			my($i)=0;
        			foreach (@last_data) { @$c[$i] = $_ if @$c[$i]<$_;	$i++; }
        			@$c[0]++;
        		} else {
        			# if here then timestamp changed and we have a last
        			print "TIMESTAMP CHANGE \n" if $last_offset>=$dbgoffsetmin and $last_offset<=$dbgoffsetmax;
        			# No prior record exists for offset - copy values
        			my(@xdat);
        			foreach (@last_data) { push @xdat , $_; }
        			unshift(@xdat,1);
        			$OUTPUTDATA[$last_offset] = \@xdat;
        		}
        		print "EOF FINAL(offset=$last_offset)=",join(" ",@{$OUTPUTDATA[$last_offset]}),"\n"
        					if defined $OUTPUTDATA[$last_offset]
        					and $last_offset>=$dbgoffsetmin and $last_offset<=$dbgoffsetmax;
        		$last_offset = -1;
        	}

        	close(INFILE);
        	$last_file=$file;
        }
        print "Saving All Days Data\n" if defined $DEBUG;
        if( -d $args{-DIR} ) {
        	printoutput($args{-DIR},"$DATADIR/AVGDATA/$args{-OUTFILE}",			\@OUTPUTDATA);
        } else {
        	printoutput($args{-DIR},"$args{-OUTFILE}",\@OUTPUTDATA);
        }
}

sub printoutput
{
	my($dir,$fname,$a)=@_;
	my(@array)=@$a;

#	if( defined $DEBUG ) {
#		print "------ FINAL DEBUG OUTPUT --------\n";
#		my($c) = $array[$dbgoffset];
#		if( defined $c ) {
#		my(@x)= @$c;
#		my($hr)=int($dbgoffset/$BUCKET_MINUTES);
#		my($mi)=60*($dbgoffset-$hr*$BUCKET_MINUTES)/$BUCKET_MINUTES;
#		$mi="0".$mi if $mi < 10;
#		$hr="0".$hr if $hr < 10;
#		print  "$hr:$mi ",$dbgoffset," ",join(" ",@x) ,"\n";
#		print  "$hr:$mi ",$dbgoffset," ";
#		foreach (@x) { print int((100*$_)/$x[0])/100, " "; }
#		print  "\n";
#		print  "------ END FINAL DEBUG OUTPUT --------\n";
#		} else {
#			print "NO debug Output Found\n";
#		}
#	}

	open( OUT, ">".$fname ) or die "cant write $fname : $! \n";
	# manage/print the output
	my($i)= -1;
	foreach (@array) {
		$i++;
		next unless defined $_;

		my($hr)=int($i/$BUCKET_MINUTES);
		my($mi)=60*($i-$hr*$BUCKET_MINUTES)/$BUCKET_MINUTES;
			# my($bucket)= int((($hr*60 + $mi) * $BUCKET_MINUTES)/60);
			# $hr = $i / $BUCKET_MINUTES - $mi/60
		$mi="0".$mi if $mi < 10;
		$hr="0".$hr if $hr < 10;
		# print "$i [$hr:$mi]=> ",join(" ",@$_)," \n";
		#### FIXUP COMMENT ### print OUT "\[$i\]" if defined $dir;
		print OUT "$hr:$mi";
		my($c) = $_;
		my(@x) = @$c;
		my($numpts)=shift @x;
		#### FIXUP COMMENT ### print OUT "\[numpts=",$numpts,"\]" if defined $dir;
		# if( defined $args{-PEAK_ONLY} ) {
			# foreach (@x) { print OUT ",",$_; }
		# } else {
			foreach (@x) { print OUT ",",int(10*$_/$numpts)/10; }
		# }
		print OUT "\n";
	}
	close(OUT);
}
__DATA__

hs_create_view server_perf_sum,
"Lock Count",			"Rate for Sample",
"CPU Busy Percent",		"Value for Sample",
"Transactions",			"Rate for Sample",
"Deadlock Count",		"Value for Sample"
go

hs_create_view cache_perf_sum,
"Page Hit Percent", 		"Value for Sample",
"Procedure Hit Percent", 	"Value for Sample"
go

hs_create_view device_io,
"Device Name",		"Value for Sample",
"Device I/O", 		"Value for Sample",
"Device Reads",		"Value for Sample",
"Device Writes",	"Value for Sample",
"Device I/O", 		"Rate for Sample",
"Device Reads",		"Rate for Sample",
"Device Writes",	"Rate for Sample"
go

hs_create_view lock_perf_sum,
"Lock Type",			"Value for Sample",
"Lock Results Summarized",	"Value for Sample",
"Lock Count",			"Value for Sample"
go
hs_create_view process_activity,
"LOGIN Name","Value for Sample",
"Process ID","Value for Sample",
"Kernel Process ID","Value for Sample",
"Connect Time","Value for Session",
"Page I/O","Value for Session",
"CPU Time","Value for Session",
"Current Process State","Value for Sample"
go
hs_create_view process_perf_sum,
"Process State",		"Value for Sample",
"Process State Count", 		"Value for Sample"
go

hs_create_view network_activity,
"Net Default Packet Size",	"Value for Sample",
"Net Max Packet Size",		"Value for Sample",
"Net Packet Size Sent",		"Value for Sample",
"Net Packet Size Received",	"Value for Sample",
"Net Packets Sent",		"Value for Sample",
"Net Packets Received",		"Value for Sample",
"Net Packets Sent",		"Rate for Sample",
"Net Packets Received",		"Rate for Sample",
"Net Bytes Sent",		"Value for Sample",
"Net Bytes Received",		"Value for Sample",
"Net Bytes Sent",		"Rate for Sample",
"Net Bytes Received",		"Rate for Sample"
go

hs_create_view procedure_cache_stats,
"Procedure Hit Percent",	"Value for Sample",
"Procedure Logical Reads",	"Value for Sample",
"Procedure Logical Reads",	"Rate for Sample",
"Procedure Physical Reads",	"Value for Sample",
"Procedure Physical Reads",	"Rate for Sample"
go

hs_create_view page_cache_stats,
"Page Hit Percent",		"Value for Sample",
"Logical Page Reads",		"Value for Sample",
"Logical Page Reads",		"Rate for Sample",
"Physical Page Reads",		"Value for Sample",
"Physical Page Reads",		"Rate for Sample",
"Page Writes",			"Value for Sample",
"Page Writes",			"Rate for Sample"
go


hs_create_view transaction_activity,
"Transactions",			"Value for Sample",
"Rows Deleted",			"Value for Sample",
"Rows Inserted",		"Value for Sample",
"Rows Updated",			"Value for Sample",
"Rows Updated Directly",	"Value for Sample",
"Transactions",			"Rate for Sample",
"Rows Deleted",			"Rate for Sample",
"Rows Inserted",		"Rate for Sample",
"Rows Updated",			"Rate for Sample",
"Rows Updated Directly",	"Rate for Sample"
go

hs_create_view engine_activity,
"Engine Number",		"Value for Sample",
"CPU Busy Percent",		"Value for Sample",
"Logical Page Reads",		"Value for Sample",
"Physical Page Reads",		"Value for Sample",
"Page Writes",			"Value for Sample"
go

hs_create_view data_cache_sum,
"Cache Name", "Value for Sample",
"Cache Hit Pct", "Value for Sample"
go
hs_initiate_recording
go

__END__

=head1 NAME

HistServerMonitor.pl - read logmaintplan table in mssql and save as heartbeats

=head2 DESCRIPTION

Reads the logmaintplan table in mssql - which stores sql server job states
and stores the results as heartbeats.

=cut

