-- report_key=SYSPERFINFO
-- run_frequency=5Min
-- do_diff=Y
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_query=
-- include_server_query=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=
-- report_footer=
-- enabled=Y

SELECT @@SERVERNAME,db_name(database_id),'PerSec',cntr_value
FROM sys.sysperfinfo
WHERE counter_name = 'Batch Requests/sec' COLLATE Latin1_General_BIN;
