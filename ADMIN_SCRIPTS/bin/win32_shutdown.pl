#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Win32;

print "Rebooting System: ",Win32::NodeName()," in 5 seconds\n";
Win32::InitiateSystemShutdown(undef,"Rebooting from Perl",5,0,1)
        or die "Cant Shutdown: ",Win32::FormatMessage(Win32::GetLastError)

__END__

=head1 NAME

win32_shutdown.pl - shutdown windows system

=head2 DESCRIPTION

Shutdown an windows system. Only works if you have administrator priviliges.

=cut
