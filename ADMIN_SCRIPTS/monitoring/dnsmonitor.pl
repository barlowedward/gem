#!/apps/perl/linux/perl-5.8.2/bin/perl
#!/apps/perl/solaris/perl-5.8.1/bin/perl
#
# Script to monitor dns
#
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Net::DNS;
use Getopt::Long;
use MlpAlarm;
use Sys::Hostname;
use CommonFunc;
use Repository;

use vars qw( $DEBUG $DOALARM $OUTFILE );

my %DNSSERVER = (
       'dns1'	=> '204.212.175.5',
	'dns2'	=> '204.212.175.5',
	'dns3'	=> '204.212.175.5',
	'proxy1' => '208.34.187.94',
	'proxy2' => '208.34.187.94',
	'mlpdns' => '208.34.187.94'
	);

sub usage
{
	return "Usage: dnsmonitor.pl --DEBUG --DOALARM --OUTFILE=file";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(	"DEBUG"	=> \$DEBUG, "DOALARM"	=> \$DOALARM, "OUTFILE=s"=>\$OUTFILE  );

open(OUT,"> $OUTFILE") if $OUTFILE;
$|=1;

my($cwd)=cd_home();
# MlpBatchJobStart(-BATCH_ID=>"DnsMonitor");
MlpBatchJobStart(-BATCH_ID=>"DnsMonitor",-AGENT_TYPE=>'Gem Monitor');
my($dir)=get_gem_root_dir()."/conf";

sub output {
	print @_;
	print OUT @_ if $OUTFILE;
}

output( "dnsmonitor.pl : run at ".localtime(time)."\n" );

die "ERROR: $dir does not exist" 		unless -d $dir;
die "dns.dat file does not exist in $dir" 	unless -e $dir."/dns.dat";
die "dns.dat file is not readable in $dir" 	unless -r $dir."/dns.dat";

output( "Reading nameservers from $dir/dns.dat\n" );
my(@nameservers);
open(DD,"$dir/dns.dat") or die "Cant read $dir/dns.dat $!\n";
while(<DD>) {
	chomp;
	next if /^\s*#/ or /^\s*$/;
	push @nameservers,$_;
}
close(DD);

my($hostname)=hostname();

foreach my $DNS (  @nameservers ) {
	output( "Testing $DNS ..." );
	my $res = new Net::DNS::Resolver;
	$res->nameservers($DNS);
	my($status) = $res->search($hostname);

	my($alarmstring,$alarmstate);
	if ( $status ) {
		$alarmstring = "OK: DNS $DNS is OK";
		$alarmstate  = "OK";
	} else {
		output( "PASS 1: DNS $DNS is not OK - sleep(5) and retry..." ) if $DEBUG;
		sleep 5;
		my($status2) = $res->search($hostname);
		unless (  $status2 ) {
			$alarmstring = "ERROR: DNS $DNS is not OK";
			$alarmstate  = "CRITICAL";
		} else{
			$alarmstring = "WARNING: DNS $DNS is OK but failed one ping";
			$alarmstate  = "WARNING";
		}
	}
	output( " $alarmstring\n" );
	MlpHeartbeat(  -monitor_program => "DnsMonitor",
		# -debug => 1,
		-system=> $DNS,
		-subsystem=> "",
		-state=> $alarmstate,
		-message_text=> $alarmstring ) if $DOALARM;
}

MlpBatchJobEnd();
exit(0)
