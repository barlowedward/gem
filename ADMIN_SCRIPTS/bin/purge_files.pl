#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

sub usage {
	print @_;
	print "usage $0 --hours=<hours> --days=<days> --dir=<dir> [--del] [--done] [--reportfile=file] [--donothing] --ctlfile=<file>
	--compress=progname
	--host=<host>			UNIX ONLY (purge_old_unix_files.pl)
	--versions=<num>
	--zerosize           only remove 0 sized files
	[--reportfile=file]
	--systemname=sys		override system name for alerts
	[-html]\n";
	exit(0);
}

use strict;
use Getopt::Long;
use Net::myFTP;
use Repository;
use MlpAlarm;
use Do_Time;
use File::Basename;

use vars qw( $days $hours $dir $del $versions $html $host $zerosize $systemname $done $compress $ctlfile $donothing $reportfile $minfilesize $BATCH_ID $DEBUG $file $versions $host $filepat );

die usage("") if $#ARGV<0;
$days=0;
$hours=0;
$|=1;

my(@CONFIGURATION_ERRORS);		# A list of errors found in the directives
my(%purge_start_time,%purge_end_time);
my(%purge_num_deleted, %purge_num_compressed);

die usage("Bad Parameter List\n") unless GetOptions(
	"debug"			=>	\$DEBUG,
	"days=i"			=> \$days,
	"dir=s" 			=> \$dir,
	"host=s"=> \$host,"hostname=s"=> \$host,
	"file=s" 		=> \$file,
	"del" 			=> \$del,
	"zerosize" 		=> \$zerosize,
	"filepat=s" 	=> \$filepat,
	"versions=i" 	=> \$versions,
	"hours=i"		=>	\$hours,
	"minfilesize=i"=>	\$minfilesize,
	"reportfile=s"	=>	\$reportfile,
	"done"			=>	\$done,
	"compress=s"	=>	\$compress,
	"systemname=s"	=>	\$systemname,
	"ctlfile=s"		=>	\$ctlfile,
	"donothing"		=>	\$donothing ,
	"html"			=>	\$html,
	"BATCH_ID=s"	=> \$BATCH_ID);

print "purge_files.pl - version 1.0\n";
MlpBatchJobStart( -BATCH_ID=>$BATCH_ID )  if defined $BATCH_ID;

$systemname=$BATCH_ID unless $systemname;

my(%FTPCONFIG);
$FTPCONFIG{Debug}=1 if $DEBUG;
$FTPCONFIG{Timeout}=90000;

if( defined $ctlfile) {
	die "control file $ctlfile does not exist\n" 	unless -e $ctlfile;
	die "control file $ctlfile is not readable\n" unless -r $ctlfile;
   print "Reading control file $ctlfile\n";

	my(@dat);
	open(X,"$ctlfile") or die "Cant open $ctlfile $!\n";
	while(<X>) {
		next if /^#/ or /^\s*$/;
		chomp;chomp;chomp;
		s/^\s+//;
		s/\s+$//;
		push @dat,$_;
	}
	close(X);

	# randomize the controlfile
	# basically we do this because otherwise a large directory can block purges on later
	# files.  admitedly this is a shitty algorithm.  Its mostly to deal with the fact that
	# my purges overrun the two hour window...
	print "Randomizing Controlfile Data\n";
	my $cnt = rand $#dat;
	$cnt--;
	print "Cnt is $cnt dat is $#dat\n" if $DEBUG;
	#print "\@dat starts ",join("\n\t",@dat),"\n"  if $DEBUG;
	while ( $cnt-- >= 0 ) {		push @dat, shift @dat;	}
	print "Randomized Controlfile :\n",join("\n\t",@dat),"\n"  if $DEBUG;

	foreach (@dat) {
		my(@valpairs)=split;
		my($hours,$days,$dir,$del,$done,$compress,$versions,$file,$minfilesize,$filepat,$host);
		$hours=$days=0;
		foreach my $kv (@valpairs) {
			#print "=> $kv\n" if $DEBUG;
			if( $kv=~/^\s*\-+days=/i ) {
				$kv=~s/^\s*\-+days=//gi;
				$days=$kv;
			} elsif( $kv=~/^\s*\-+hours=/i ) {
				$kv=~s/^\s*\-+hours=//gi;
				$hours=$kv;
			} elsif( $kv=~/^\s*\-+versions=/i ) {
				$kv=~s/^\s*\-+versions=//gi;
				$versions=$kv;
			} elsif( $kv=~/^\s*\-+minfilesize=/i ) {
				$kv=~s/^\s*\-+minfilesize=//gi;
				$minfilesize=$kv;
			} elsif( $kv=~/^\s*\-+dir=/i ) {
				$kv=~s/^\s*\-+dir=//gi;
				$dir=$kv;
			} elsif( $kv=~/^\s*\-+del/i ) {
				$kv=~s/^\s*\-+del//gi;
				$del=1;
			} elsif( $kv=~/^\s*\-+filepat=/i ) {
				$kv=~s/^\s*\-+filepat=//gi;
				$filepat=$kv;
			} elsif( $kv=~/^\s*\-+hostname=/i ) {
				$kv=~s/^\s*\-+hostname=//gi;
				$host=$kv;
			} elsif( $kv=~/^\s*\-+host=/i ) {
				$kv=~s/^\s*\-+host=//gi;
				$host=$kv;
			} elsif( $kv=~/^\s*\-+file=/i ) {
				$kv=~s/^\s*\-+file=//gi;
				$file=$kv;
			} elsif( $kv=~/^\s*\-+done/i ) {
				$kv=~s/^\s*\-+done//gi;
				$done=1;
			} elsif( $kv=~/^\s*\-+zerosize/i ) {
				$kv=~s/^\s*\-+zerosize//gi;
				$zerosize=1;
			} elsif( $kv=~/^\s*\-+compress=/i ) {
				$kv=~s/^\s*\-+compress=//gi;
				$compress=$kv;
			} elsif( $kv=~/^\s*\-+systemname=/i ) {
				$kv=~s/^\s*\-+systemname=//gi;
				$systemname=$kv;
			} else {
				die "UNKNOWN KV PAIR $kv\n";
			}
			#print "$hours,$days,$dir,$del,$done,$compress,$versions,$file,$minfilesize,$filepat,$host\n" if $DEBUG;
		}
		print "purge_it($days+$hours/24,$dir,$del,$done,$compress,$versions,$file,$minfilesize,$filepat,$host,$zerosize)\n" if $DEBUG;
		purge_it($days+$hours/24,$dir,$del,$done,$compress,$versions,$file,$minfilesize,$filepat,$host,$zerosize);
		print "\n" unless $donothing;
	}
} else {
	purge_it($days+$hours/24,$dir,$del,$done,$compress,$versions,$file,$minfilesize,$filepat,$host,$zerosize);
}
#
# PRINT A REPORT
#
my($rptstr)="";
if( $html ) {
#	if( $#CONFIGURATION_ERRORS > -1 ) {
#		$rptstr.="<h2>INVALID DIRECTORIES IN $ctlfile<\h2>\n";
#		foreach (@CONFIGURATION_ERRORS) { $rptstr.= $_."<br>\n"; }
#		$rptstr.= "<p>\n";
#	}

	$rptstr.= "<h2>OUTPUT STATISTICS FOR WIN32 PURGES<\h2>\n";
	$rptstr.= "<TABLE>\n";
	$rptstr.= sprintf( "<TR><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><\TR>\n","Duration",'#Cmp','#Del',"Directory/File" );
	foreach (keys %purge_start_time ) {
		$rptstr .= sprintf( "<TR><TD>%s</TD><TD>%d</TD><TD>%d</TD><TD>%s</TD><\TR>\n",
								do_diff($purge_end_time{$_}-$purge_start_time{$_}),
								$purge_num_compressed{$_},
								$purge_num_deleted{$_},
								$_);
	}
	$rptstr.= "<\TABLE>\n";
} else {
#	if( $#CONFIGURATION_ERRORS > -1 ) {
#		$rptstr.="INVALID DIRECTORIES IN $ctlfile\n";
#		foreach (@CONFIGURATION_ERRORS) { $rptstr .= $_."\n"; }
#		$rptstr .= "\n";
#	}
	$rptstr.= "PURGE COMPLETED : OUTPUT STATISTICS FOLLOW\n";
	$rptstr.= sprintf( "%8s %4s %4s %s\n","Duration",'#Cmp','#Del',"Directory/File" );

	#foreach (keys %purge_start_time ) { print "ST $purge_start_time{$_}  $_\n"; }
	#foreach (keys %purge_end_time )   { print "EN $purge_end_time{$_}  $_\n"; }

	foreach (keys %purge_start_time ) {
		$rptstr .= sprintf( "%8s %4d %4d %s\n",
							do_diff($purge_end_time{$_}-$purge_start_time{$_}),
							$purge_num_compressed{$_},
							$purge_num_deleted{$_},
							$_);
	}
}

if( $reportfile ) {
	open(RPT,"> $reportfile" ) or die "Cant open $reportfile\n";
	print RPT $rptstr;
	close(RPT);
}
print $rptstr;

if( $#CONFIGURATION_ERRORS > -1 ) {
	print "\n";
	print STDERR "INVALID DIRECTIVES IN CONTROLFILE $ctlfile\n";
	print STDERR "Please edit this file to remove the invalid directories\n";
	foreach (@CONFIGURATION_ERRORS) { print STDERR $_; }
}
MlpBatchJobEnd( ) if defined $BATCH_ID;
exit(0);

my(%directory_cache);

##########################################
# purge_it()
#	$dir			-- mandatory directory to look in
#  $del			-- if set, delete files that match spec
#              -- if not set, check that files < $days
#  $compress	-- compress the files you find
#  $file			-- can not contain digits, any files matching exactly pass (no digits, .gz, .done)
#					-- cant pass -del and -file
#  $days
#  $done				-- only work on .done files (probably requires -del)
#  $versions		-- max num versions of file you desire to keep (requires -del)
#  $minfilesize	-- minimum file size (requires -file)
#  $filepat       -- exact patern the file must match
#
# foreach ( @files = files in --DIR (no subdirectories )) {
#     if --FILE  @files = grep(/--FILE/,@files) no numbers
#    	if 	--DEL and --DAYS		 	delete files older than $DAYS
#		elsif --DEL and --VERSIONS  	delete files > $VERSIONS
#		elsif $DAYS							check file exists < $DAYS
#		elsif $VERSIONS					error
#		else check existence of file < $DAYS
#
sub purge_it {
	my($days,$dir,$del,$done,$compress,$versions,$file,$minfilesize,$filepat,$hostname,$zerosize)=@_;

	$dir =~ s.\\.\/.g;
	my($purge_key);
	if( $file ) {
		$purge_key=$dir."/".$file;
	} else {
		$purge_key=$dir;
	}
	$purge_start_time{$purge_key}=time;

	print "####### Processing Directive #######\n" if $ctlfile;
   print "#   Run Time: ",(scalar localtime(time))."\n";
   print "#   Directory=$dir\n";
   print "#   File=$file\n" if $file;
   my($pdays)=$days*100;
	$pdays=int($pdays)/100;
	print "#   Days=$pdays\n" if $pdays;
   print "#   Delete=$del\n" if $del;
   print "#   Zerosize=$zerosize\n" if $zerosize;
   print "#   filepat=$filepat\n" if $filepat;
   print "#   Done=$done\n" if $done;
   print "#   hostname=$hostname\n" if $hostname;
   print "#   Compress=$compress\n" if $compress;
   print "#   Ver=$versions\n" if $versions;
   print "####################################\n" if $ctlfile;
   print "\n";

 	die "Must Pass -dir\n" 					unless defined $dir;
	die "CAN NOT PASS --versions but not --del\n" if $versions and ! $del;
	die "Must Pass -days or -versions\n" unless defined $days or $versions;
   die "Can Not Pass --del and --file\n" if $del and $file;
   die "Must Pass -file for --minfilesize\n" if $minfilesize and ! $file;
   die "Must Pass -del for --zerosize\n" if $zerosize and ! $del;

	if( ! -d $dir and ! $hostname ) {
		warn " unable to find $dir\n";
		print " unable to find $dir\n";
		push @CONFIGURATION_ERRORS, "### Warning: Configuration Error ($dir) is not a directory\n" ;
   	$purge_end_time{$purge_key}=time;
		return;
	};
	if( ! -r $dir and ! $hostname ) {
		warn " unable to read $dir\n";
		print " unable to read $dir\n";
		push @CONFIGURATION_ERRORS, "### Warning: Configuration Error ($dir) is not readable\n" ;
   	$purge_end_time{$purge_key}=time;
		return;
	};

	my($ftp);
   my(@files,%FILE_TIMES,%FILE_SIZES);
   if( defined $directory_cache{$hostname.":".$dir} ) {
   	@files=@{$directory_cache{$hostname.":".$dir}};
   	print "   ",($#files+1)," Files found in cached directory\n";
   } elsif( $hostname ) {
		my($unix_login,$unix_password)=get_password(-name=>$hostname,-type=>"unix");
		if( ! $unix_login ) {
			warn "No Login Found for $hostname\n";
			print "No Login Found for $hostname\n";
			push @CONFIGURATION_ERRORS, "### No Login Found for $hostname\n";
	   	$purge_end_time{$purge_key}=time;
			return;
		}
		if( ! $unix_password ) {
			warn "### No Password Found for $hostname\n";
			print "### No Password Found for $hostname\n";
			push @CONFIGURATION_ERRORS, "### No Password Found for $hostname\n";
	   	$purge_end_time{$purge_key}=time;
			return;
		}

		my(%svrargs)=get_password_info(-type=>"unix",-name=>$hostname);
		my($method)=$svrargs{UNIX_COMM_METHOD};

		print "connecting() via $method to $hostname\n";
		$ftp= Net::myFTP->new($hostname,%FTPCONFIG,METHOD=>$method)
	                or die "Failed To Create FTP to $hostname Object $@";
	  	if( ! $ftp ) {
			warn "### Unable to connect to $hostname\n";
			print "### Unable to connect to $hostname\n";
			push @CONFIGURATION_ERRORS, "### Unable to connect to $hostname\n";
	   	$purge_end_time{$purge_key}=time;
			return;
		}

		if( $unix_login ne "null" and $unix_password ne "null" ) {
			print "Connecting as login=$unix_login\n";
			my($rc,$err)=$ftp->login($unix_login,$unix_password);
			die( "Cant FTP To host ($hostname) As $unix_login : $err \n") unless $rc;
		}

		$ftp->ascii();

		my($file_count)=0;
		print "ftp\-\>cwd($dir) \n" if $DEBUG;
		if( ! $ftp->cwd($dir) ) {
			warn "Cant cwd to $dir\n";
			print "Cant cwd to $dir\n";
			$purge_end_time{$purge_key}=time;
			return;
		}
		print "Directory $dir appears to exist\n" if $DEBUG;

		print "ftp\-\>fspec(.) \n" if $DEBUG;
		foreach ( $ftp->fspec(".") ) {
			my($filenm,$filetime,$filesize,$filetype)=@$_;
			next if $filenm =~ /^\./;
			next unless $filetype eq "File";
			# next if $filepat and $filenm!~/$filepat/;
			$file_count++;
			$FILE_TIMES{$filenm}=(time-$filetime)/(60*60*24);  # convert to days
			$FILE_SIZES{$filenm}=$filesize;
			push @files,$filenm;
			print "$file_count) $filenm time=$FILE_TIMES{$filenm} size=$filesize\n" if $DEBUG;
		}
	  	print "   ",($#files+1)," Files found in directory\n";
   } else {
   	opendir(DIR,$dir) || die "Cant Open Directory $dir : $!\n";
   	my(@filessv) = grep((!/^\./ and ! -d $dir."/".$_ ),readdir(DIR));
   	closedir(DIR);
   	$directory_cache{$hostname.":".$dir} = \@filessv;
   	@files=@filessv;
   	print "   ",($#files+1)," Files found in directory\n";
   }

	# die "DONE" if $hostname;

	my($fileage)=100000000;	# if $file, this is the youngest file in existence
	if( $file ) {
		die "--FILE=$file CAN NOT CONTAIN DIGITS" if $file=~/\d/;
		my(@newfile);
		foreach my $f (@files) {
   		my($f_nonum)=$f;
   		$f_nonum=~s/\d//g;
   		$f_nonum=~s/.gz$//;
   		$f_nonum=~s/.done$//;
   		next if $f_nonum ne $file;
   		push @newfile,$f;
   		my($s);
   		if( $hostname ) {
   			$s= $FILE_TIMES{$f};
   		} else {
   			$s= -M $dir."/".$f;
   		}
   		$fileage = $s if $s < $fileage;
   	}
   	@files = @newfile;
   	print "   ",($#files+1)," Files match --FILE\n";
   }

   if( $zerosize ) {
   	my(@newfile);
		foreach (@files) {
			my($s);
			if( $hostname ) {
   			$s= $FILE_SIZES{$_};
   		} else {
   			$s= -s $dir."/".$_;
   		}
			push @newfile,$_ if $s == 0;
   	}
   	@files = @newfile;
   	print "   ",($#files+1)," Files match --done\n";
   }

   if( $done ) {
   	my(@newfile);
		foreach (@files) {
			push @newfile,$_ if /\.done$/ or /\.done\./;
   	}
   	@files = @newfile;
   	print "   ",($#files+1)," Files match --done\n";
   }

   if( $filepat ) {
   	my(@newfile);
		foreach (@files) {
			push @newfile,$_ if /$filepat/;
   	}
   	@files = @newfile;
   	print "   ",($#files+1)," Files match --filepat\n";
   }

   if( $days ) {
   	my(@newfile);
		foreach my $f (@files) {
			my($s);
   		if( $hostname ) {
   			$s= $FILE_TIMES{$f};
   		} else {
   			$s= -M $dir."/".$f;
   		}
   		if( $del ) {
   			if( $days < $s ) {
   				print "PASSED: $f : $days<$s\n" if $DEBUG;
   				push @newfile,$f;
   			} else {
   				print "IGNORED: $f : $days>$s\n" if $DEBUG;
   			}
   		} else {
   			if( $days > $s ) {
   				print "PASSED: $f : $days>$s\n" if $DEBUG;
   				push @newfile,$f;
   			} else {
   				print "IGNORED: $f : $days<$s\n" if $DEBUG;
   			}
   		}
   	}
   	@files = @newfile;
   	print "   ",($#files+1)," Files match --days\n";
   }

   if( $versions ) {
   	my(@newfile);
		my(%foundkeys);	# num files found with root (nonumbs)
   	foreach my $f (reverse sort @files) {
   		my($f_nonum)=$f;
   		$f_nonum=~s/\d\d+//g;
   		$f_nonum=~s/.gz$//;
   		$f_nonum=~s/.done$//;
   		$foundkeys{$f_nonum} = 0 unless $foundkeys{$f_nonum};
   		$foundkeys{$f_nonum}++;
   		if( $foundkeys{$f_nonum} <= $versions ) {
   			print "KEEPING  $f\n";
   			next;
   		}
   		push @newfile,$f;
   	}
   	@files = @newfile;
   	print "   ",($#files+1)," Files match --versions\n";
   }

   if( $del or $compress ) {
   	print "\n";
   	print "Processing Deletes on ",($#files+1), " files.\n" if $del and $#files>=0;
   	print "Processing Compress on ",($#files+1)," files.\n" if $compress and $#files>=0;
	  	foreach my $f (@files) {
	  		next if -d $dir."/".$f;
		   if( defined $compress ) {
		   	next if $f=~/\.gz$/ or $f=~/\.Z/;
				print " (compressing) $f\n";
				# if .gz file exists then remove it
	   		if( $compress=~/gzip/ and -e "$dir/$f.gz" ) {
	   			print "(not) " if defined $donothing;
	   			print "   removeing $dir/$f.gz \n";
	   			if( ! defined $donothing ) {
	  					unlink( "$dir/$f.gz" )  or warn "Warning: Cant Unlink $dir/$f.gz : $!\n";
	  				}
	   		}
	   		print "   $compress $dir/$f \n";
	   		open(CMD,"$compress \"$dir/$f\" |")	or die "Cant Run $compress $dir/$f: $!\n";
	   		$purge_num_compressed{$dir}++;
	   		foreach (<CMD>) {
	   			chop;
	   			print "   returned: $_\n";
	   		}
	   		close(CMD);
		   } elsif( defined $del ) {
		   	print "(not) " if defined $donothing;
				print "REMOVING $f\n";
				$purge_num_deleted{$dir}++;
				if( ! defined $donothing ) {
					if( $hostname ) {
						$ftp->delete("$dir/$f") or warn("WARNING: ($hostname) Cant Delete $dir/$f : $!\n");
					} else {
						unlink( $dir."/".$f ) or warn "Warning: Cant Unlink $dir/$f : $!\n";
	  				}
	  			}
	  		}
		}
	} else {
		print "Processing Checks (--del or --compress not specified)...\n";
		if( $file ) {
  			my($bfile)=basename($file);
  			if( $#files < 0 ) {
  				print "ERROR $file not found\n";
  				set_alert($bfile,"ERROR","FILE $file not found in $dir");
			} elsif( $minfilesize ) {	# files must be of at least the given size
		   	my(@newfile);
		   	my($ok)="OK";
				foreach my $f (@files) {
					my($s)= -s $dir."/".$f;
					my($s);
		   		if( $hostname ) {
		   			$s= $FILE_SIZES{$f};
		   		} else {
		   			$s= -s $dir."/".$f;
		   		}
			   	next if $s > $minfilesize;
			   	set_alert($bfile,"ERROR",	"FILE $dir/$file is $s bytes" );
			   	$ok="ERROR";
			   	last;
		   	}
	   	} elsif( $fileage > $days ) {
				print "ERROR: $file $fileage days old\n";
				$fileage= int( $fileage*100)/100;
				set_alert($bfile,"ERROR","FILE $file in $dir is $fileage days old \n");
   		} else {
	   		$fileage= int( $fileage*100)/100;
	   		print "OK $file\n";
	   		set_alert($bfile,"OK","FILE $file was found in $dir");
	   	}
	   }
  	}

   $purge_end_time{$purge_key}=time;
   my($d)=do_diff($purge_end_time{$purge_key}-$purge_start_time{$purge_key});
   print "Directory $dir Completed ($d seconds)\n\n";
   $ftp->close() if $hostname;
}

sub set_alert {
	my($subsys,$state,$msg) = @_;
	MlpHeartbeat(
						-monitor_program	=>	$BATCH_ID,
						-subsystem			=>	$subsys,
						-system				=>	$systemname,
						-state				=>	$state,
						-message_text		=>	$msg	) if $BATCH_ID;
	print "[$state] $systemname:$subsys:$msg\n";
}

__END__

=head1 NAME

purge_files.pl - purge old unix and windows files

=head2 DESCRIPTION

A core dba task is directories space management.  This excellent utility provides you exactly that.
It is smart enough to do it right - parsing your files so you control exactly what the program does.   The program either works on full spec windows path names (//server/share/dir/file - note forward slashes as backslashes have special meaning in perl) or, on unix, where you can pass in a --hostname
=head2 USAGE

To CHECK file existence

purge_files.pl --hours=<hours> --days=<days> --dir=<dir>
				 [--done] [--donothing] [--report=file] --compress=progname --ctlfile=<file>

To PURGE older files

purge_files.pl --hours=<hours> --days=<days> --dir=<dir> --del
				 [--done] [--donothing] [--report=file] --compress=progname --ctlfile=<file>

or

purge_files.pl --dir=<dir> --del --version=<num>
				 [--done] [--donothing] [--report=file] --compress=progname --ctlfile=<file>

=head2 ARGUMENTS

--ctlfile=file : A control file can be used to automate multiple systems.  See conf/win32_cleanup.dat.sample.
The controlfile basically contains the arguments to this program

	# DR01 Cleanup
	#
	--days=2 --dir=//ad001/e$/logshipbuffer --del --done
	--days=10 --dir=//ad001/e$/logshipbuffer --del

--hours or --days : controls the time for the comparison

--del or --donothing : what do you want to do - delete the files or dont

--done : only work on files with a .done or .done.gz or .done.Z extension.  These files are created by
our log shipping system AFTER they have been applied successfully to the log shipping target.

--days switches meanings based on the --del flag.  If --del is set, then days means delete everything OLDER than the argument,
while if --del is not set, it means you are checking for files NEWER than --days.

-compress=<comprssprogram> compresses the files

-versions=num : keep only num versions of the file.  Because backup files are normally datestamped, this
option removes all sequences of 2+ numbers from the filename

=head2 FILECHECKONLY

if the FILE flag is passed, the program will look for specific files in the directory and alert if older than --DAYS

=head2 POPULATION

you can use console/find_win32_files_for_monitoring.pl to create the controlfile
=cut
