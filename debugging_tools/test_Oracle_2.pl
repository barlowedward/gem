: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use strict;
use Carp;
use Cwd;
use CommonFunc;
use Repository;
use File::Basename;
use File::Copy;
use File::Find;
use Do_Time;
use ConnectionManager;

my($status_bar_ready)="FALSE";
our(%CONFIG,%CURRENT_CONFIG,%server_info,$DEBUG,$statusmsgbar,$mf,$connectionmanager,@perl_includes);
my $_statusmsg_idle = "Ready";
my($print_stdout)="TRUE";

$|=1;

print "Starting Up Test Script....\n";
$CURRENT_CONFIG{gem_root_directory}		= get_gem_root_dir();
$CURRENT_CONFIG{gem_configuration_directory}   = $CURRENT_CONFIG{gem_root_directory}."/conf";
require $CURRENT_CONFIG{gem_root_directory}."/lib/GemFunctions.pl";

print "Reading Gem Function Library\n";
read_all_configfiles("FULL");

my $GOD_MODE;			# well really developer mode - TODO: probably need to encrypt
my $NUM_STATUS_LINES=5; 	# number of lines on status line - constant
my($Bbase);

#die usage("Bad Parameter List $!\n") unless GetOptions( "DEBUG"	=>\$DEBUG );
#print "Creating MainWindow\n" if defined $DEBUG;
#$CURRENT_CONFIG{mainWindow} = MainWindow->new(-title=>"G.E.M. Configuration Utility : $CURRENT_CONFIG{VERSION}");
#my $top   = $CURRENT_CONFIG{mainWindow}->Frame()->pack(qw/-side top -fill x/);
#$mf    	  = $CURRENT_CONFIG{mainWindow}->NoteBook();
#my $bot   = $CURRENT_CONFIG{mainWindow}->Frame()->pack(qw/-side bottom -fill x/);


#$CURRENT_CONFIG{mainWindow}->fontCreate('header', 	-family=>'Ariel', -size=>'12', -weight=>'bold');
#$CURRENT_CONFIG{mainWindow}->fontCreate('large', 	-family=>'Ariel', -size=>'12' );
#$CURRENT_CONFIG{mainWindow}->fontCreate('bold', 	-family=>'Ariel', -size=>'10', -weight=>'bold');
#$CURRENT_CONFIG{mainWindow}->fontCreate('code', 	-family=>'Courier', -size=>'10');
#$CURRENT_CONFIG{mainWindow}->fontCreate('norm', 	-family=>'Ariel', -size=>'10');
#$CURRENT_CONFIG{mainWindow}->fontCreate('small', 	-family=>'Ariel', -size=>'9');
#$CURRENT_CONFIG{mainWindow}->fontCreate('smbold', 	-family=>'Ariel', -size=>'9', -weight=>'bold');
#$CURRENT_CONFIG{mainWindow}->fontCreate('tiny', 	-family=>'Ariel', -size=>'6');
#$top->Label(-font=>"header",-text=>=>"Generic Enterprise Manager $CURRENT_CONFIG{VERSION} Copyright (c) 1995-2004 By SQL Technologies", -relief=>"groove", -bg=>'beige')->pack(-side => "top", -pady=>2, -fill=>'x');
#$statusmsgbar= $bot->Label(-height=>$NUM_STATUS_LINES,-justify=>'left',-text=>"")->pack(-side=>'left');
#set_status_label( undef, "G.E.M. Configuration Utility");

print "Initializing ConnectionManager\n";
$connectionmanager=new ConnectionManager(
	#-debug		=>	$DEBUG,
	-debug		=>	1,
	# -sqltext	=>	\$sql_text,
	-statusmsg	=>	\&_statusmsg,
	-debugmsg	=> 	\&_debugmsg,
	-global_data	=>	\&global_data
);

$status_bar_ready="TRUE";
my($c)=$connectionmanager->connect( -name=>'SCPROD', -type=>'oracle' );
my(@rc)=$connectionmanager->query( -connection=>$c, -query=>'select * from v$version', -db=>"");
foreach (@rc) { print join(" ",$connectionmanager->decode_row($_)),"\n"; }

my(@rc)=$connectionmanager->query( -connection=>$c, -query=>'select * from v$option', -db=>"");
foreach (@rc) { print join(" ",$connectionmanager->decode_row($_)),"\n"; }

my(@rc)=$connectionmanager->query( -connection=>$c, -query=>'select * from v$controlfile', -db=>"");
foreach (@rc) { print join(" ",$connectionmanager->decode_row($_)),"\n"; }
#MainLoop;

sub myprint {	print @_;}
sub _statusmsg { print @_; }
#read_all_configfiles("FULL");
#my($cm)=new ConnectionManager( -statusmsg=>\&myprint, -debug=>1 );


