#
# TEMPLATE IS MY STANDARD MODULE FOR GEM TEMPLATE
#
use strict;
use lib qw(lib);
use Getopt::Long;
use CommonFunc;

my($PROGRAM_DESCRIPTION)="Crontab Creator";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions( "DEBUG"	=> \$DEBUG );

print "[create_crontab] $0 - v",$VERSION,"\n";
print "[create_crontab] Reading gem.dat\n";
my($GemConfig)=read_gemconfig(-debug=>$DEBUG);
# START BODY

use Repository;
use JobScheduler;
use Sys::Hostname;

my($GEM_ROOT_DIR)  = get_gem_root_dir();
my($job_scheduler) = fetch_jobscheduler();			# ALARM SCHEDULE DATA

build_crontab();

print "[create_crontab] Saving Scheduler Information\n[create_crontab] ";
save_jobscheduler($job_scheduler);

# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "[create_crontab] SUCCESSFUL: $PROGRAM_DESCRIPTION\n";
close_gemconfig();
exit(0);


sub build_crontab {

	my($monitoring_details,$backup_details,$maintenance_details)=("","","");
	__statusmsg("[create_crontab] building crontab.txt\n");

	my($logdir)="$GemConfig->{UNIX_CODE_LOCATION}/data/GEM_BATCHJOB_LOGS";
	my($outfile)="$GemConfig->{UNIX_CODE_LOCATION}/crontab.txt";
	unlink($outfile) if -w $outfile;
	open(OUTF,"> $outfile") or "Die Cant Write $outfile $!\n";
	

#
# SURVEY SCRIPTS
#
my($maddress)="--MAILTO=\"".$GemConfig->{ADMIN_EMAIL}."\"";

print OUTF "#
#############################################
# START GEM SCHEDULED JOBS
#############################################
";

print OUTF	 "#
# GENERIC ENTERPRISE MANAGER - crontab file and install instructions
#
# This file can be used as a crontab file.  
# \n";

print OUTF 	"# This file was created at ".localtime(time)."\n";

#_debugmsg( "# CREATING CRONTAB TEXT FILE
##
##############################################
## Notes On GEM Batch Jobs
##  - UNIX Driver scripts exist in $GemConfig->{UNIX_CODE_LOCATION}/unix_batch_scripts/
##  - Organized by Subdirectory
##        plan_batch and plan_interactive for backup plan jobs
##        batch and interactive for GEM jobs
##  - the batch and interactive directories are identical except that jobs in the batch
##    directory redirect standard output and standard error.  You should schedule the
##    jobs from the batch directories, and use the scripts in the interactive
##    directory when you run by hand / debugging.
##############################################\n" );
#_debugmsg( sprintf("# %33s %11s %11s %11s %11s %11s %11s\n","Name","OkTypes","JobType","Freq","RunType","SchedOn","Enabled" ));
#foreach my $job_name (	sort keys %$job_scheduler ) {
#	_debugmsg( sprintf  "# %33s %11s %11s %11s %11s %11s %11s\n",
#			$job_name,
#			$$job_scheduler{$job_name}->{ok_types},
#			$$job_scheduler{$job_name}->{job_type},
#			$$job_scheduler{$job_name}->{frequency},
#			$$job_scheduler{$job_name}->{run_type},
#			$$job_scheduler{$job_name}->{scheduled_on},
#			$$job_scheduler{$job_name}->{enabled} );
#}
#_debugmsg("#\n");

#	foreach my $job_type ( "backup","monitoring","reporting" ) {

#	print 	"#
#############################################
# SURVEY AND REPORTING SECTION
#
# IMPORTANT - THE JOBS IN THIS SECTION MUST NOT OVERLAP - YOU WILL NEED TO RESCHEDULE
# THEM BY HAND TO INSURE THIS FACT (at this point).  The hot spot is gem.dbm - one
# disadvantage of an xml database is that there is no implicit file locking and that is
# on the to-do list.
#
# The Survey scripts need to be run daily
#  survey_windows  - only will run on windows
#  survey_sqlserver - only will run on windows
#  survey_oracle
#  survey_sybase
#  survey_unix - works both windows and unix#
#
# BuildConsoleXXX jobs build the documentation tree (important) not that XXX
#   indicates the level of the rebuild (weekly>nightly>hourly).
#############################################\n"
#		if $job_type eq "reporting";
#
#		print "#
#############################################
# DATABASE BACKUP JOBS (ie the plan jobs)
#   - The defaults start times for backup jobs will be totally UNACCEPTABLE
#   - Batch Job syntax should be obvious.
#         backup.pl is the driver.  It takes a plan/job name passed with -J
#         and adding -t implies transaction log dump instead of full (default)
#   - The driver script Backup_<JOBNAME>.ksh translates to backup.pl -J<JOBNAME>
#   - The driver script Trandump_<JOBNAME>.ksh translates to backup.pl -t -J<JOBNAME>
#############################################\n" if $job_type eq "backup";

#		print "#
#############################################
# MONITORING SECTION
#############################################
#\n" if $job_type eq "monitoring";

	my($num_ignored)=0;
	my($num_scheduled)=0;
	foreach my $job_name (	sort keys %$job_scheduler ) {
		
     		my($job_dat) = $$job_scheduler{ $job_name };        		
		 	if( $$job_scheduler{$job_name}->{run_type} ne "Unix"
			    or $$job_scheduler{$job_name}->{enabled} eq "NO" ) {
				  	if( $$job_scheduler{$job_name}->{run_type} ne "Unix" ) {
				     	print "Ignoring $job_name runtype=$$job_scheduler{$job_name}->{run_type}\n" if $DEBUG;
				     	$num_ignored++;
				   } else {
				     	print "Ignoring $job_name enabled=$$job_scheduler{$job_name}->{enabled}\n" if $DEBUG;
				     	$num_ignored++;
				   }
				  	next;
			}
		
			# SKIP AS NEEDED
			if( $GemConfig->{INSTALLTYPE} eq "WINDOWS" and $job_name=~/Win32/ ) {
				print "Ignoring $job_name - Win32 Job\n" if $DEBUG;
				next;
			}
			if( $GemConfig->{USE_SYBASE_ASE} eq "N" 	and ($job_name=~/Sybase/ or $job_name=~/^GemRpt_Syb/ or $job_name=~/^Syb/) ) {
				print "Ignoring $job_name - USE_SYBASE=N\n" if $DEBUG;
				next;
			}
			if( $GemConfig->{USE_SQLSERVER} eq "N"	and $job_name=~/Mssql/ ) {
				print "Ignoring $job_name - USE_SQLSERVER=N\n" if $DEBUG;
				next;
			}
			if( $GemConfig->{USE_ORACLE} eq "N"	 	and $job_name=~/Oracle/ ) {
				print "Ignoring $job_name - USE_ORACLE=N\n" if $DEBUG;
				next;
			}		
			$num_scheduled++;
			
			$$job_scheduler{$job_name}->{scheduled_on}=hostname();
			print "Schedule $job_name\n" if $DEBUG;
			
     		# print "# DBG: WORKING ON job_type $$job_dat{job_type}\n";
     		# print "# DBG: WORKING ON ok_types $$job_dat{ok_types}\n";

        	#	if( $$job_dat{job_type} ne "monitoring"
        	#	and $$job_dat{job_type} ne "reporting"
        	#	and $$job_dat{job_type} ne "backup" ) {
        	#		print "#\n#\n# Unknown Job Type $$job_dat{job_type} for $job_name\n";
        	#		next;
        	#	}
        	#	next unless $$job_dat{job_type} eq $job_type;

	       # 	next if $job_type eq "backup" and $job_name =~ /FixLogship/;
			#	next if $job_type eq "backup" and $job_name =~ /LoadTranlogs/;
			#	next if $job_type eq "backup" and  $$job_dat{ok_types} eq "nt";
			#	next if $$job_dat{ok_types} eq "nt";
	
				# print "# DBG: WORKING ON Job $job_name - Frequency=",$$job_dat{frequency},"\n";
				
        		my($str)="";
        		$str="# [NOT ENABLED]" if $$job_dat{enabled} ne "YES";
        		#$str.="# [NT JOB ONLY]" if $$job_dat{ok_types} eq "nt";

        		if( defined $$job_scheduler{$job_name}->{scheduled_on}
        		and $$job_scheduler{$job_name}->{scheduled_on} !~ /^\s*$/
        		and $$job_scheduler{$job_name}->{scheduled_on} ne hostname() ) {
        			$str="# [SCHEDULED ON ".$$job_scheduler{$job_name}->{scheduled_on}." ]"
        		}

        		if( $$job_dat{frequency} eq "hourly" ) {
        			$str.=int(rand(59))." * * * *  ";
        		} elsif( $$job_dat{frequency} eq "weekly" ) {
        			$str.=int(rand(59))." ".int(rand(4))." * * 0  ";
        		} elsif( $$job_dat{frequency} eq "never" ) {
        			print "Ignoring $job_name - Frequency $$job_dat{frequency}\n" if $DEBUG;
					$str.="#" unless $$job_dat{enabled} ne "YES";
        			$str.="0 0 * * 0-6  ";
        		} elsif( $$job_dat{frequency} eq "daily" ) {
        			$str.=int(rand(59))." ".int(rand(4))." * * *  ";
        		} elsif( $$job_dat{frequency} =~ /^daily\-(\d+)am/i ) {
        			$str.=int(rand(59))." ".$1." * * *  ";
        		} elsif( $$job_dat{frequency} =~ /^daily\-(\d+)pm/i ) {
        			my($hr)=$1;
        			$hr+=12;
        			$str.=int(rand(59))." ".$hr." * * *  ";
        		} elsif( $$job_dat{frequency} eq "saturday-am" ) {
        			my($hr)=int(rand(15));
        			my($mi)=int(rand(59));
        			$str.=$mi." $hr * * 6 ";
        		} elsif( $$job_dat{frequency} eq "sunday-am" ) {
        			my($hr)=int(rand(15));
        			my($mi)=int(rand(59));
        			$str.=$mi." $hr * * 0 ";
        		} elsif( $$job_dat{frequency} eq "Every15Min" ) {
        			my($tm)=int(rand(15));
        			$str.=$tm.",".($tm+15).",".($tm+30).",".($tm+45)." * * * 0-6  ";
        		} elsif( $$job_dat{frequency} eq "Every10Min" ) {
        			my($tm)=int(rand(10));
        			$str.=$tm.",".($tm+10).",".($tm+20).",".($tm+30).",".($tm+40).",".($tm+50)." * * * 0-6  ";
        		} else {
        			warn "Ignoring $job_name - Unknown Frequency $$job_dat{frequency}\n";
					$str.="#[".$$job_dat{frequency}."] ";
        		}

				$$job_scheduler{$job_name}->{scheduled_on} = hostname();
				
        		if( $$job_dat{job_type} eq "backup" ) {
        			printf OUTF "%-24s %s",$str,$GemConfig->{UNIX_CODE_LOCATION}."/unix_batch_scripts/plan_batch/$job_name.ksh\n";
        		} else {
        			printf OUTF "%-24s %s",$str,$GemConfig->{UNIX_CODE_LOCATION}."/unix_batch_scripts/batch/$job_name.ksh\n";
        		}
        	}
#	}
print OUTF "#
#############################################
# DONE GEM SCHEDULED JOBS
#############################################
";

	print "[create_crontab] Jobcount: $num_scheduled scheduled - $num_ignored ignored\n";
	print "[create_crontab] Crontab File $outfile Created\n";
	
	close(OUTF);
}