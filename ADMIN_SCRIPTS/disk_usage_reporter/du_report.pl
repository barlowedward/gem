#!/apps/perl/linux/perl-5.8.2/bin/perl

use strict;
use Getopt::Long;
use Time::Local;
use CGI qw(:standard);

#
# Read The Code Directory And Create Reports
#

sub usage {
	print @_;
	print "usage: $0 --INDIR=directory --HTMLFILE=file --ROWS=maxrows\n";
	return "\n";
}

my($INDIR,$DEBUG,$HTMLFILE,$ROWS);

die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless GetOptions( "INDIR=s"=>\$INDIR, "DEBUG"=>\$DEBUG, "HTMLFILE=s"=>\$HTMLFILE, "ROWS=s"=>\$ROWS  );
die usage("Must Pass Directory With --INDIR\n") unless defined $INDIR;
die usage("Must Pass Html Output File With --HTMLFILE\n") unless defined $HTMLFILE;

$ROWS=20 unless defined $ROWS;

opendir(DIR,$INDIR) or die("Can't open directory $INDIR : $!\n");
my(@dirlist) = grep((!/^\./ and /\./),readdir(DIR));
closedir(DIR);

open(HTML,">".$HTMLFILE) or die "Cant Write $HTMLFILE $!\n";

# This is the output
my(@roots);
my(%yday_latest,%today_file,%yest_file,%week_file,%month_file);

# These are Temp Files
my(%file_by_root_yday);
my(%file_roots);
foreach ( sort @dirlist ) {
	my($root,$datestr)=split(/\./,$_,2);

	push @roots,$root unless defined $file_roots{$root};
	$file_roots{$root}=1;

	my($dt)=substr($datestr,0,8);
	my($tm)=substr($datestr,8,2);

	# this lamer is my best way to take file time and convert it to an int days
	#  - would normally use -M but we can not assume that the mod time is right for the file
	my($filetime)=timelocal(0,0,$tm,substr($dt,6,2),substr($dt,4,2),substr($dt,0,4));
	my(@x)=localtime($filetime);
	my($yday)= $x[7];
	$file_by_root_yday{$root."-".$yday} = $_;

	print "Root=$root Date=$dt Time=$tm Day=$yday\n" if defined $DEBUG;

	# rely on files coming in in sorted order
	$today_file{$root} = $_;
	$yday_latest{$root} = $yday;
}

foreach my $r ( @roots ) {
	my($cnt)=1;
	while($cnt<20) {
		my($d)= $yday_latest{$r} - $cnt;
		if( defined $file_by_root_yday{$r."-".$d} ) {
			$yest_file{$r}= $file_by_root_yday{$r."-".$d};
			last;
		}
		$cnt++;
	}

	$cnt=6;
	while($cnt<30) {
		my($d)= $yday_latest{$r} - $cnt;
		if( defined $file_by_root_yday{$r."-".$d} ) {
			$week_file{$r}= $file_by_root_yday{$r."-".$d};
			last;
		}
		$cnt++;
	}

	$cnt=30;
	while($cnt<50) {
		my($d)= $yday_latest{$r} - $cnt;
		if( defined $file_by_root_yday{$r."-".$d} ) {
			$month_file{$r}= $file_by_root_yday{$r."-".$d};
			last;
		}
		$cnt++;
	}
}

my(%today_dat,%yest_dat,%week_dat,%month_dat);
foreach my $r ( @roots ) {
	print "Root=$r Today=$today_file{$r} Yest=$yest_file{$r} Week=$week_file{$r} Month=$month_file{$r}\n"
		if defined $DEBUG;
	%today_dat=( %today_dat, read_file($today_file{$r}));
	%yest_dat=( %yest_dat,   read_file($yest_file{$r}));
	%week_dat=( %week_dat,   read_file($week_file{$r}));
	%month_dat=( %month_dat, read_file($month_file{$r}));
}

print HTML "<BODY BGCOLOR=WHITE>\n";
print HTML h2("Disk Usage Report v1.0");
print HTML "Report Run at ",scalar(localtime(time)),p,p;
print "Report Run at ",scalar(localtime(time)),"\n";
show_biggest(-title=>"Top $ROWS Biggest Directories",   -firstdat=>\%yest_dat,  -todaydat=>\%today_dat, -rows=>$ROWS);
compare_two_arrays(-title=>"Top $ROWS Biggest 1 Day Change",   -firstdat=>\%yest_dat,  -todaydat=>\%today_dat, -rows=>$ROWS);
compare_two_arrays(-title=>"Top $ROWS Biggest 1 Week Change",  -firstdat=>\%week_dat,  -todaydat=>\%today_dat, -rows=>$ROWS);
compare_two_arrays(-title=>"Top $ROWS Biggest 1 Month Change", -firstdat=>\%month_dat, -todaydat=>\%today_dat, -rows=>$ROWS);
print HTML "</BODY>\n";

print "REPORTING COMPLETED\n";
close(HTML);
exit(0);

sub read_file {
	my($file)=@_;
	return unless defined $file;
	my(%indat);
	open(IN,$INDIR."/".$file) or die "can not read $file $!\n";
	foreach ( <IN> ) {
		chop;
		my($fs,$size)=split;
		$size=int($size/1024);
		$indat{$fs}=$size;
	}
	close(IN);
	return %indat;
}

sub compare_two_arrays {
	my(%args)=@_;
	my(%indat, %in2dat, @flist);

	%indat =%{$args{-firstdat}};
	%in2dat=%{$args{-todaydat}};

	foreach ( keys %indat) { push @flist,$_ if defined $in2dat{$_}; }

	print "-----------------------------------------\n";
	print $args{-title},"\n";
	print "-----------------------------------------\n";
	if( $#flist < 0 ) { print "No data exists for this report\n\n"; return; }

	my(@x) = sort { $indat{$a}-$in2dat{$a} <=> $indat{$b}-$in2dat{$b} } @flist;

	my($cnt)=1;
	print HTML "<TABLE BORDER=1>";
	print HTML "<TR BGCOLOR=YELLOW><TH COLSPAN=5 BGCOLOR=YELLOW>",$args{-title},"</TH></TR>","\n";
	printf "%3.3s  %14.14s  %14.14s  %14.14s  %14.14s\n","Cnt","File","Prior","Today","Increase";
	print HTML "<TR><TH>Cnt</TH><TH>File</TH><TH>Prior</TH><TH>Today</TH><TH>Increase</TH></TR>\n";
	foreach ( @x ) {
		printf "%3d) %14.14s %14sM %14sM %14sM\n",
				$cnt,
				$_,
				do_comma($indat{$_}),
				do_comma($in2dat{$_}),
				do_comma($in2dat{$_}-$indat{$_});
		print HTML
				"<TR><TD>",$cnt,"</TD>",
				"<TD>",$_,"</TD>",
				"<TD ALIGN=RIGHT>",do_comma($indat{$_}),"Mb</TD>",
				"<TD ALIGN=RIGHT>",do_comma($in2dat{$_}),"Mb</TD>",
				"<TD ALIGN=RIGHT>",do_comma($in2dat{$_}-$indat{$_}),"Mb</TD></TR>\n";
		$cnt++;
		last if $cnt> $args{-rows};
	}
	print HTML "</TABLE><p><p>\n";
	print "\n";
}


sub show_biggest {
	my(%args)=@_;
	my(%indat, %in2dat, @flist);

	%indat =%{$args{-firstdat}};
	%in2dat=%{$args{-todaydat}};

	foreach ( keys %in2dat) { push @flist,$_; }
	my(@x) = sort { $in2dat{$b} <=> $in2dat{$a} } @flist;

	print "-----------------------------------------\n";
	print $args{-title},"\n";
	print "-----------------------------------------\n";
	if( $#flist < 0 ) { print "No data exists for this report\n\n"; return; }

	my($cnt)=1;
	print HTML "<TABLE BORDER=1>";
	print HTML "<TR BGCOLOR=YELLOW><TH COLSPAN=5 BGCOLOR=YELLOW>",$args{-title},"</TH></TR>","\n";
	printf "%3.3s  %14.14s %14.14s  %14.14s  %14.14s\n","Cnt","File","Today","Increase";
	print HTML "<TR><TH>Cnt</TH><TH>File</TH><TH>Today</TH><TH>Increase</TH></TR>\n";
	foreach ( @x ) {
		my($diff)="N.A.";
		$diff=$in2dat{$_}-$indat{$_} if defined $indat{$_};
		printf "%3d) %14.14s %14sM %14sM\n",$cnt,$_,do_comma($in2dat{$_}),do_comma($diff);
		print HTML "<TR><TD>",$cnt,"</TD><TD>",$_,"</TD><TD ALIGN=RIGHT>",
				do_comma($in2dat{$_}),
				"Mb</TD><TD ALIGN=RIGHT>",
				do_comma($diff),
				"Mb</TD></TR>\n";
		$cnt++;
		last if $cnt> $args{-rows};
	}
	print HTML "</TABLE><p><p>\n";
	print "\n";
}

sub do_comma {
	my($x)=@_;
	my($isneg);
	if( $x < 0 ) {
		$isneg=1;
		$x *= -1;
	}
	my($len)=length $x;
	# print "x is $x len=$len \n";
	if( $len>9 )  {
		substr($x,$len-9,0)="," ;
		substr($x,$len-5,0)="," ;
		substr($x,$len-1,0)="," ;
		# print "x>6 is $x len=$len \n";
	} elsif( $len>6 )  {
		substr($x,$len-6,0)="," ;
		substr($x,$len-2,0)="," ;
		# print "x>6 is $x len=$len \n";
		# print "x>6 is $x len=$len \n";
	} elsif( $len>3 )  {
		substr($x,$len-3,0)="," ;
		# print "x>3 is $x len=$len \n";
	}
	# print "Returning $x\n";
	return "-".$x if defined $isneg;
	return $x;
}
