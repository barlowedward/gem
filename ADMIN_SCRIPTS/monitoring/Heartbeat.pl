#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use MlpAlarm;
use CommonFunc;
use Getopt::Long;

use vars qw( $MONITOR_PROGRAM $STATE $HEARTBEAT $HEARTBEAT_HOURS $SEVERITY $SYSTEM $SUBSYSTEM $BATCHJOB
	 $EVENT_ID $DEBUG $EVENT_TIME $MESSAGE_TEXT $DOCUMENT_URL $FILE);

$|=1;

sub usage {
	print @_,"\n";
 	return "Usage: Heartbeat.pl --MONITOR_PROGRAM=file --STATE=val --SYSTEM=system --SUBSYSTEM=subsystem --DEBUG=1 --MESSAGE_TEXT=text --DOCUMENT_URL=url

   --MONITOR_PROGRAM= Unique Name Of the Program Name
   --STATE= Severity Of the Event
        * EMERGENCY = system down / critical failure
        * CRITICAL = serious problem.
        * ALERT = non fatal error needing attention
        * ERROR = non fatal error possibly requiring administrator attention
        * WARNING = non fatal warning.
        * OK = a simple message.  Synonym for INFORMATION.
        * DEBUG = messages only of interest to developers
   --FILE=filename [if file exists with contents, contents are ERROR msg else OK]
   --SYSTEM=system
   --HEARTBEAT=minutes     ( the HEARTBEAT Special Case )
   --HEARTBEAT_HOURS=hours ( the HEARTBEAT Special Case )
   --SUBSYSTEM=subsystem
   --DEBUG=1 	- print diagnostics
   --MESSAGE_TEXT=string tehxt
   --BATCHJOB=1
   --DOCUMENT_URL=attachment
";
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
    GetOptions(
      "MONITOR_PROGRAM=s"	=>	\$MONITOR_PROGRAM,
		"STATE=s"				=>	\$STATE,
		"SEVERITY=s"			=>	\$SEVERITY,
		"SYSTEM=s"				=>	\$SYSTEM,
		"SUBSYSTEM=s"			=>	\$SUBSYSTEM,
		"FILE=s"					=>	\$FILE,
		"DEBUG"					=>	\$DEBUG,
		"HEARTBEAT=i"			=>	\$HEARTBEAT,
		"HEARTBEAT_HOURS=i"			=>	\$HEARTBEAT_HOURS,
		"MESSAGE_TEXT=s"		=>	\$MESSAGE_TEXT,
		"BATCHJOB=s"			=>	\$BATCHJOB,
		"DOCUMENT_URL=s"		=>	\$DOCUMENT_URL );

# use of SEVERITY is simply a convenience in case you forget
$STATE=$SEVERITY if defined $SEVERITY and ! defined $STATE;
$HEARTBEAT=$HEARTBEAT_HOURS*60 if $HEARTBEAT_HOURS;
my(%opt);

if( $FILE ) {
	if( ! -e $FILE ) {
		$MESSAGE_TEXT="FILE $FILE DOES NOT EXIST";
		$STATE="OK";
	} elsif( ! -r $FILE ) {
		$MESSAGE_TEXT="FILE $FILE IS NOT READABLE";
		$STATE="ERROR";
	} else {
		my($msg)="";
		open(F,$FILE) or die "Cant Read File $FILE\n";
		while(<F>) { $msg.=$_ unless /^\s*$/; }
		close(F);
		if( $msg eq "" ) {
			$MESSAGE_TEXT="FILE $FILE CONTAINS NO ERRORS";
			$STATE="OK";
		} else {
			$msg=~s/\n/\<br\>\n/g;
			$MESSAGE_TEXT="FILE $FILE - CONTENTS:<br>\n$msg";
			$STATE="ERROR";
		}
	}
}
if( ! defined $MONITOR_PROGRAM and ! defined $SUBSYSTEM and ! defined $SYSTEM ) {
	die usage("MUST PASS --MONITOR_PROGRAM, --SYSTEM, or --SUBSYSTEM to Heartbeat.pl");
}

if( $HEARTBEAT ) {
	$MONITOR_PROGRAM = "HEARTBEAT" unless $MONITOR_PROGRAM;
	$SYSTEM="" 		unless $SYSTEM;
	$SUBSYSTEM=""  unless $SUBSYSTEM;
	$STATE="ERROR" unless $STATE;
	$MESSAGE_TEXT = "A HEARTBEAT has not occurred for at least $HEARTBEAT Minutes" if ! $MESSAGE_TEXT;
	$opt{-heartbeat}=$HEARTBEAT;
}

die usage("MISSING ARGUMENT --MONITOR_PROGRAM TO Heartbeat.pl") unless defined $MONITOR_PROGRAM ;
die usage("MISSING ARGUMENT --SUBSYSTEM TO Heartbeat.pl") unless defined $SUBSYSTEM ;
die usage("MISSING ARGUMENT --STATE TO Heartbeat.pl") unless defined $STATE;

MlpHeartbeat( 	-monitor_program	=>	$MONITOR_PROGRAM,
	  	-system			=>	$SYSTEM,
	  	-subsystem		=>	$SUBSYSTEM,
		-debug			=>	$DEBUG,
	  	-state			=>	$STATE,
	  	-message_text		=>	$MESSAGE_TEXT,
	  	-batchjob		=>	$BATCHJOB,
		-document_url 		=>	$DOCUMENT_URL,
		%opt
);

__END__

=head1 NAME

Heartbeat.pl - save a heartbeat

=head2 SYNOPSIS

A simple front end utility to the GEM Alarm system heartbeats.  This program can be used in two modes.  Mode 1 is
as a regular GEM Heartbeat - which represents a system/state message where you do not care about history.  Examples
of this is system up/down (you dont care whether it was up yesterday - just that it is up/down now).  The other
use is as a timer.  In this "HEARTBEAT" case, you identify an alarm time - and the heartbeat is saved as an error
heartbeat that has been "REVIEWED" until a specific time in the future.  In other words, the system stores (for example)
a CRITICAL message that "BATCH JOB AAA HAS NOT RUN IN 2 HOURS" and then says that this message is "OK" until 2 hours in
the future.  In two hours, the alarm routing agent and any alarm viewers will start seeing that message - which is hidden
until them as it is marked as reviewed.  To prevent your getting a page - run the same program within 2 hours,
which will push the "REVIEWED UNTIL TIME" out for another 2 hours - which is the point - alarm unless another heartbeat
comes in within a specified time.

=head2 USAGE

Usage: MlpHeartbeat.pl --MONITOR_PROGRAM=file --STATE=val --SYSTEM=system --SUBSYSTEM=subsystem --DEBUG=1 --MESSAGE_TEXT=text --DOCUMENT_URL=url

   --MONITOR_PROGRAM= Unique Name Of the Program Name
   --STATE= Severity Of the Event
        * EMERGENCY = system down / critical failure
        * CRITICAL = serious problem.
        * ALERT = non fatal error needing attention
        * ERROR = non fatal error possibly requiring administrator attention
        * WARNING = non fatal warning.
        * OK = a simple message.  Synonym for INFORMATION.
        * DEBUG = messages only of interest to developers
   --SYSTEM=system
   --HEARTBEAT=minutes ( the HEARTBEAT Special Case )
   --SUBSYSTEM=subsystem
   --DEBUG=1    - print diagnostics
   --MESSAGE_TEXT=string tehxt
   --BATCHJOB=1
   --DOCUMENT_URL=attachment

=head2 EXAMPLE1

 # In this example, we save a normal heartbeat
 #  - these messages appear in the alarm system immediately
 Heartbeat.pl --SYSTEM=DEF --MONITOR_PROGRAM=XYZ   --STATE=OK --MESSAGE_TEXT="SYSTEM DEF PASSED THE XYZ TEST"
 Heartbeat.pl --SYSTEM=DEF --MONITOR_PROGRAM=XYZ   --STATE=ERROR --MESSAGE_TEXT="SYSTEM DEF FAILED THE XYZ TEST"

 # In this example, we save a HEARTBEAT - an error that is reviewed until <currenttime>+$HEARTBEAT
 #  - these messages appear in the alarm system after 60 minutes (unless time is extended by rerunning
 #  - for the same key value (monitor_program,system,subsystem)
 Heartbeat.pl --SYSTEM=ABC	--HEARTBEAT=60
	translates into
		--MONITOR_PROGRAM=HEARTBEAT
		--SYSTEN=ABC
		--STATE=ERROR
		--SUBSYSTEM=""
		--MESSAGE_TEXT="A HEARTBEAT has not occurred for at least $HEARTBEAT Minutes"
		--heartbeat=60

 # This is similar to the above but with a custom message
 Heartbeat.pl --SYSTEM=ABC	--HEARTBEAT=10  --MESSAGE_TEXT="ERROR ABC HAS NOT RUN IN 10 MINUTES - SEEK HELP"


=head2 Notes

If you want to be alarmed for a regular heartbeat - best choose a system that is production
