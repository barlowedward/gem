#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

#
# MONITOR SIZE IN DATABASE OVER TIME
#

sub usage
{
        print @_;
        print "size_monitor.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST -ccnt -ld -tsec
                -c count - times to repeat - if no count then loops forever
                -t sleep time in seconds between run
                -l show log space   (defaults to -ld)
                -d show data space
                -s space (data and log)
                -q query
        ";

        return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_d $opt_U $opt_S $opt_P $opt_D $opt_c $opt_l $opt_d $opt_t $opt_s $opt_q);

fix_getopts();
die usage("Bad Parameter List") unless getopts('dU:D:S:P:D:c:ldsq:t:');

if( defined $opt_s ) { $opt_l=1; $opt_d=1; }

use Sybase::SybFunc;

print "QUERY is $opt_q" if defined $opt_q;

$opt_D="%"                                                                                      unless defined $opt_D;
die usage("Must pass sa password\n" )           unless defined $opt_P;
die usage("Must pass server\n")                                 unless defined $opt_S;
die usage("Must pass sa username\n" )           unless defined $opt_U;

#
# CONNECT TO DB
#
db_connect($opt_S,$opt_U,$opt_P)
        or die "Cant connect to $opt_S as $opt_U\n";

# GET DB INFO
my @databaselist;
if( defined $opt_l or defined $opt_d )  {
        @databaselist=db_query("master","select name from sysdatabases where name like \"$opt_D\"  and status & 0x1120 = 0 and status2&0x0030=0");
} else {
        @databaselist=("master");
}

################ MAIN ########################

my($lquery)="select reserved_pgs(i.id, doampg) from sysindexes i where i.id = 8\n";

my($squery)="select sum(reserved_pgs(id, doampg) + reserved_pgs(id, ioampg)) from sysindexes where id != 8";

my(@rc,$space);
if( defined $opt_l or defined $opt_d ) {
        foreach (@databaselist)  {
                my(@rc)=db_query($_,"select sum(size) from master.dbo.sysusages u where u.dbid = db_id() and   u.segmap != 4");
                die "Multiple Rows Returned\n" if( $#rc>0 );
                $space=$rc[0];

                print "Database Allocation: $_ Size=$space (",$space/512,"MB)";

                @rc=db_query($_,"select sum(size) from master.dbo.sysusages u where u.dbid = db_id() and   u.segmap = 4");
                die "Multiple Rows Returned\n" if( $#rc>0 );
                $space=$rc[0];
                if( $space eq "" ) {
                        print " Log=NULL \n";
                } else {
                        print " Log=$space (",$space/512,"MB)\n";
                }
        }
}

$opt_c=10000000 unless defined $opt_c;
my($count)=0;
while ($count++ < $opt_c) {
        if( ! defined $opt_d and ! defined $opt_l ) {
                my($row_cnt)=0;
                foreach( db_query("master",$opt_q,1,1)) {
                        $row_cnt++;
                        next if $row_cnt == 1 and $count%10!=1;
                        print join(" ",db_decode_row($_)),"\n";
                }
                sleep($opt_t) if defined $opt_t;
                next;
        }

        if($count%10==1 ) {
                foreach (@databaselist)  {
                        print "----------------------|";
                }
                print "\n";
                foreach (@databaselist)  {
                        printf("%22s|",$_);
                }
                print "\n";
                foreach (@databaselist)  {
                        print "----------------------|";
                }
                print "\n";
        }
        my(@logspace,@dbspace);
        foreach (@databaselist)  {
                @logspace=db_query($_,$lquery) if defined $opt_l;
                @dbspace =db_query($_,$squery) if defined $opt_d;
                $logspace[0] = int( $logspace[0] / 51.2);
                $dbspace[0] = int( $dbspace[0] / 51.2);
                $logspace[0] /= 10;
                $dbspace[0] /= 10;
                printf("Db: %4sMB Log: %4sMB|",$dbspace[0],$logspace[0] )
                        if defined $opt_l and defined $opt_d;
                printf("Log: %15sMB|",$dbspace[0],$logspace[0] )
                        if defined $opt_l and ! defined $opt_d;
                printf("Db: %16sMB|",$dbspace[0],$logspace[0] )
                        if ! defined $opt_l and defined $opt_d;
        }
        print "\n";
        sleep($opt_t) if defined $opt_t;
}

db_close_sybase();

sub fix_getopts
{
        my(@tmpargv);
        my($cnt)=-1;
        foreach (@ARGV) {
                if( /^-/ ) {
                        $cnt++;
                        $tmpargv[$cnt] = $_;
                } else {
                        $tmpargv[$cnt] .= " ".$_;
                }
        }
        @ARGV=@tmpargv;
}

__END__

=head1 NAME

size_monitor.pl - monitor database (size or performance)

=head2 USAGE

        size_monitor.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST -ccnt -ld -tsec
                -c count - times to repeat - if no count then loops forever
                -t sleep time in seconds between run
                -l show log space   (defaults to -ld)
                -d show data space
                -s space (data and log)
                -q quick statistics

=head2 DESCRIPTION

Monitors a db size or summary statistics.

=head2 SAMPLE OUTPUT

  size_monitor.pl -d -USYS_INSTALL -PPASSWORD -SSYBASE_LOCAL -Dxrm%db -c3

  Database Allocation: xrm_db Size=20480 (40MB) Log=5120 (10MB)
  Database Allocation: xrm_security_db Size=10240 (20MB) Log=2560 (5MB)
  Database Allocation: xrm_sod01_db Size=20480 (40MB) Log=5120 (10MB)
  ----------------------|----------------------|----------------------|
                  xrm_db|       xrm_security_db|          xrm_sod01_db|
  ----------------------|----------------------|----------------------|
  Db:             35.8MB|Db:              5.2MB|Db:             28.9MB|
  Db:             35.8MB|Db:              5.2MB|Db:             28.9MB|
  Db:             35.8MB|Db:              5.2MB|Db:             28.9MB|

=cut


