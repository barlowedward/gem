-- report_key=DM_DB_INDEX_PHYSICAL_STATS
-- report_type=HTML_AGGREGATE
-- run_frequency=Weekly
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=Y
-- run_query_in_sys_db=N
-- report_header=This report lists tables in the server with over 1000 pages
-- report_footer=
-- enabled=Y

SELECT
	@@SERVERNAME as servername,
	db_name() as [dbname],
    OBJECT_NAME(i.object_id) AS TableName,
	i.name AS TableIndexName,
	index_level,
	str(phystat.avg_fragmentation_in_percent,5,2) as avg_frgmnt_pct,
	-- phystat.avg_page_space_used_in_percent,	/* limited only */
	phystat.fragment_count,
	str(phystat.avg_fragment_size_in_pages,5,2) as avg_frgmnt_sz_pages,
	i.index_id,
	index_type_desc,
	str(page_count,10,0) as page_count
FROM
sys.dm_db_index_physical_stats( DB_ID(), NULL, NULL, NULL, NULL) phystat
	inner JOIN sys.indexes i
ON i.object_id = phystat.object_id
AND i.index_id = phystat.index_id
-- WHERE phystat.avg_fragmentation_in_percent > 10
WHERE page_count>1000
order by servername,dbname,TableName
