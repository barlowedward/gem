
CREATE PROC #am_generate_waitstats AS
BEGIN

-- Setup query starts a tran -- make sure it wasn't orphaned
WHILE (@@TRANCOUNT > 0) COMMIT TRAN;

-- Get the most recent snapshot ID in the history table ('previous snapshot')
DECLARE @previous_snapshot_id bigint;
DECLARE @previous_collection_time datetime;
SELECT @previous_snapshot_id = ISNULL (MAX (snapshot_id), 0) FROM #am_wait_stats_snapshots;
SELECT TOP 1 @previous_collection_time = ISNULL (collection_time, GETUTCDATE()) FROM #am_wait_stats_snapshots
WHERE snapshot_id = @previous_snapshot_id;

-- The snapshot we're about to capture ('current snapshot')
DECLARE @current_snapshot_id bigint;
DECLARE @current_collection_time datetime;
SET @current_collection_time = GETUTCDATE();
SET @current_snapshot_id = @previous_snapshot_id + 1;

-- The snapshots table holds the two most recent snapshots. Delete the older of the two to
-- make room for a new snapshot.
DELETE FROM #am_wait_stats_snapshots WHERE snapshot_id < @previous_snapshot_id;
DELETE FROM #am_resource_mon_snap WHERE current_snapshot_id < @previous_snapshot_id;

DECLARE @interval_sec numeric (20, 4);
SET @interval_sec =
    CASE
        -- Avoid int overflow that DATEDIFF (ms, ...) can cause in the event of a huge gap between intervals
        WHEN DATEDIFF (second, @previous_collection_time, @current_collection_time) >= 10
            THEN DATEDIFF (second, @previous_collection_time, @current_collection_time)
        -- Avoid divide-by-zero
        WHEN DATEDIFF (millisecond, @previous_collection_time, @current_collection_time) = 0.0
            THEN 0.0001
        ELSE DATEDIFF (millisecond, @previous_collection_time, @current_collection_time) / 1000.0
    END;

-- This query captures in-progress and completed (cumulative) wait time for each wait type
INSERT INTO #am_wait_stats_snapshots
SELECT
    @previous_snapshot_id + 1 AS snapshot_id,
    @current_collection_time AS collection_time,
    wait_type,
    SUM (waiting_tasks_count) AS waiting_tasks_count,
    SUM (signal_wait_time_ms) AS signal_wait_time_ms,
    SUM (wait_time_ms) AS wait_time_ms,
    SUM (raw_wait_time_ms) AS raw_wait_time_ms
FROM
(
    -- global server wait stats (completed waits only)
    SELECT
        wait_type,
        waiting_tasks_count,
        (wait_time_ms - signal_wait_time_ms) AS wait_time_ms,
        signal_wait_time_ms,
        wait_time_ms AS raw_wait_time_ms
    FROM sys.dm_os_wait_stats
    WHERE waiting_tasks_count > 0 OR wait_time_ms > 0 OR signal_wait_time_ms > 0
    UNION ALL
    -- threads in an in-progress wait (not yet completed waits)
    SELECT
        wait_type,
        1 AS waiting_tasks_count,
        wait_duration_ms AS wait_time_ms,
        0 AS signal_wait_time_ms,
        wait_duration_ms AS raw_wait_time_ms
    FROM sys.dm_os_waiting_tasks
    -- Very brief waits quickly will roll into dm_os_wait_stats; we only need to
    -- query dm_os_waiting_tasks to handle longer-lived waits.
    WHERE wait_duration_ms > 1000
) AS merged_wait_stats
GROUP BY merged_wait_stats.wait_type;

/*
Now join the current snapshot to the prior snapshot to calculate the wait time for
the just-completed time interval.

The previous_snapshot derived table represents cumulative wait stats at the beginning
of the just-completed time interval, while current_snapshot is wait stats at the end of
the interval. By subtracting the start cumulative wait time from the end cumulative
wait time, we can calculate the wait time that accumulated during this time interval.

The query uses a CTE to expose the interval-specific stats that are the result of the
comparison of the current snapshot to the previous snapshot.

Notes on the calculation of the [weighted_average_wait_time_per_sec] column:
-------------------------
We use a weighted average formula to provide the user with a more stable indicator of
the recent waittime. This is primarily useful if the user has selected a rapid refresh
rate.  With a sample interval of 1 second, for example, it could be hard to assess the
overall bottleneck on the system if short-term fluxuations in waittime distribution
were creating constant changes in the 'worst' wait category for the just-completed 1
second sample interval.

The goal of this is to make the average of (approximately) the past 30-60 seconds
dominate the value.  The longer the sample interval, the greater the weight that is
given to the just-completed sample interval. With a 1-second sample interval, the
just-completed sample is given a weight of about 10%.  With a 5 second sample
interval, it is given a weight of about 50%.  Sample intervals longer than 5 seconds
quickly approach 100% (a 60 second interval is weighted as ~80%, and a 10 minute
interval at ~95%).

The formula for the weight W of the current interval is below (T is the duration of
the just-completed time interval, in seconds):

    W  =  (1 - (T / (T^1.4 + 0.1)))

You can see the curve of the weight function by running this TSQL:
    DECLARE @interval_sec numeric(20, 2)
    DECLARE @current_interval_weight numeric (20, 2)
    SET @interval_sec = 1
    PRINT CONVERT (char(20), 'Interval (sec)') + 'Interval Weight'
    PRINT CONVERT (char(20), '==============') + '==============='
    WHILE (@interval_sec <= 6000)
    BEGIN
        SET @current_interval_weight = 1 - (@interval_sec / (POWER (@interval_sec, 1.4) + 0.1))
        PRINT CONVERT (char(20), @interval_sec) + CONVERT (char(20), @current_interval_weight)
            + '  ' + REPLICATE ('-', @current_interval_weight * 100)
        SET @interval_sec = CASE WHEN @interval_sec = 1 THEN 5 ELSE @interval_sec + 5 END
    END

The wait time for the just-completed interval is combined with the previous weighted
average in the following way, where P is the previous sample's weighted average wait
time, C is the current interval's wait time, and W is @current_interval_weight (the %
weight given to the current waittime, using the prior formula):

    ((1-W) * P) + (W * C)

*/

DECLARE @current_interval_weight numeric (10,6);
-- SET @current_interval_weight = @interval_sec / 60;
SET @current_interval_weight = 1 - (@interval_sec / (POWER (@interval_sec, 1.4) + 0.1))
IF @current_interval_weight > 1 SET @current_interval_weight = 1;

WITH interval_waitstats AS
(
    -- First get resource wait stats for this interval.
    SELECT
        #am_wait_types.category_name,
        current_snapshot.wait_type,
        -- All wait stats will be reset to zero by a service cycle, which will cause
        -- (snapshot2waittime-snapshot1waittime) calculations to produce an incorrect
        -- negative wait time for the interval.  Detect this and avoid calculating
        -- negative wait time/wait count/signal time deltas.
        CASE
            WHEN (current_snapshot.waiting_tasks_count - previous_snapshot.waiting_tasks_count) < 0 THEN current_snapshot.waiting_tasks_count
            ELSE (current_snapshot.waiting_tasks_count - previous_snapshot.waiting_tasks_count)
        END AS interval_waiting_tasks_count,
        -- Use [raw_wait_time_ms] for this check because [wait_time_ms] is a calculated
        -- value and tiny differences in sample time for signal wait time vs. wait time
        -- can cause its calculated value to be slightly negative even when stats weren't
        -- reset.
        CASE
            WHEN (current_snapshot.raw_wait_time_ms - previous_snapshot.raw_wait_time_ms) < 0 THEN current_snapshot.wait_time_ms
            ELSE (current_snapshot.wait_time_ms - previous_snapshot.wait_time_ms)
        END AS interval_resource_wait_time,
        CASE
            WHEN (current_snapshot.signal_wait_time_ms - previous_snapshot.signal_wait_time_ms) < 0 THEN current_snapshot.signal_wait_time_ms
            ELSE (current_snapshot.signal_wait_time_ms - previous_snapshot.signal_wait_time_ms)
        END AS interval_resource_signal_time,
        current_snapshot.wait_time_ms AS resource_wait_time_cumulative
    FROM (
            SELECT * FROM #am_wait_stats_snapshots WHERE snapshot_id = @previous_snapshot_id
        ) AS previous_snapshot
    INNER JOIN (
            SELECT * FROM #am_wait_stats_snapshots WHERE snapshot_id = @current_snapshot_id
        ) AS current_snapshot
        ON previous_snapshot.wait_type = current_snapshot.wait_type
    INNER JOIN #am_wait_types ON #am_wait_types.wait_type = current_snapshot.wait_type
    WHERE #am_wait_types.ignore != 1 AND #am_wait_types.category_name != 'Idle'
)
INSERT INTO #am_resource_mon_snap
SELECT
    @previous_snapshot_id AS previous_snapshot_id,
    @current_snapshot_id AS current_snapshot_id,
    @previous_collection_time AS previous_collection_time,
    @current_collection_time AS current_collection_time,
    @interval_sec AS interval_sec,
    interval_waitstats.category_name,
    interval_waitstats.wait_type,
    interval_waitstats.interval_waiting_tasks_count,
    -- Tiny differences in sample time for signal wait time vs. wait time can cause our
    -- calculated wait time to be slightly negative
    CASE
        WHEN interval_waitstats.interval_resource_wait_time < 0 THEN 0
        ELSE interval_waitstats.interval_resource_wait_time
    END AS interval_resource_wait_time,
    interval_waitstats.interval_resource_signal_time,
    CONVERT (bigint, interval_waitstats.interval_resource_wait_time / @interval_sec) AS interval_wait_time_per_sec,
    CONVERT (numeric (10, 2), interval_waitstats.interval_resource_wait_time / @interval_sec / 1000) AS interval_avg_waiter_count,
    interval_waitstats.resource_wait_time_cumulative,
    CASE
        WHEN previous_interval_waitstats.weighted_average_wait_time_per_sec IS NULL
        THEN CONVERT (bigint, interval_waitstats.interval_resource_wait_time / @interval_sec)
        ELSE
            -- weighted average formula -- see comment above for explanation
            ((1-@current_interval_weight) * previous_interval_waitstats.weighted_average_wait_time_per_sec)
                + (@current_interval_weight * CONVERT (bigint, interval_waitstats.interval_resource_wait_time / @interval_sec))
    END AS weighted_average_wait_time_per_sec
FROM interval_waitstats
LEFT OUTER JOIN (
    SELECT * FROM #am_resource_mon_snap
    WHERE current_snapshot_id = @previous_snapshot_id
) AS previous_interval_waitstats ON interval_waitstats.wait_type = previous_interval_waitstats.wait_type;

-- We can delete the data for the previous snapshot now
DELETE FROM #am_resource_mon_snap WHERE current_snapshot_id < @current_snapshot_id;
END
