#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2011 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# BEGIN { $ENV{SYBASE}="/export/home/sybase"; }

use strict;
use Getopt::Long;
use Repository;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use Data::Dumper;
use CommonFunc;
use CommonHeader;

# cd to the right directory
my($curdir)=dirname($0);
my($STARTTIME)=time;
chdir $curdir or die "Cant cd to $curdir: $!\n";

$| = 1;

use vars qw($opt_o $PRODUCTION $opt_S $NOLOCK $opt_R $opt_D $opt_A $opt_d $opt_T $opt_B $opt_t $dohelptable $opt_UNUSED);

sub usage
{
	print @_;
	print "space_monitor.pl -outdir=outdir

MASTER SPACE USAGE MONITOR FOR SYBASE & SQL SERVER DATABASES

-BATCHID=batchid		batchid for alarming (if -ALARM)
-DEBUG 					debug mode
-NOLOCK					dont use a lockfile
--PRODUCTION			only run on production servers
-SERVER=Server
-DATABASE=Database
-TYPE=sqlsvr|sybase|both	database type
-DOHELPTABLE 			monitor table space
-WRITEHISTORY			Rewrite history files
-ALARM					Alarm appropriately)

With --DOHELPTABLE saves sp_helptable in files named helptable.SERVER.DB.DATE.TIME
With --WRITEHISTORY saves space to history files

This directory output is useful for the program plot_tablespace.pl \n";
   return "\n";
}

my(@ALERTS);

GetOptions(
	"outdir=s"		=>	\$opt_o,
	"server=s"		=>	\$opt_S,
	"rewrite"		=>	\$opt_R,
	"PRODUCTION"	=>	\$PRODUCTION,
	"WRITEHISTORY"	=>	\$opt_R,
	"nogemxmlwrite"=> \$opt_UNUSED,		# depricated
	"database=s"	=>	\$opt_D,
	"alarm"			=>	\$opt_A,
	"NOLOCK"			=>	\$NOLOCK,
	"debug" 			=> \$opt_d,
	"type=s"			=>	\$opt_T,
	"batch_id=s"	=>	\$opt_B,
	"batchid=s"		=>	\$opt_B,
	"dohelptable"	=>	\$dohelptable,
	)	or die usage( "syntax error\n" );

my($gr_dir)=get_gem_root_dir();
die "No Gem Root Dir" unless -d $gr_dir;
if( ! defined $opt_o ) {
	$opt_o = "$gr_dir/data/system_information_data";
	die "No Input File $opt_o" unless -d $opt_o;
	die "Directory $opt_o not writable" unless -w $opt_o;
}
die usage("Output Directory $opt_o does not exist\n") unless -d $opt_o;
die usage("-T must be sybase/sqlsvr/both\n") if defined  $opt_T
	and $opt_T ne "sybase" and $opt_T ne "sqlsvr" and $opt_T ne "both";

if( $opt_B ) {
	if( ! $NOLOCK ) {
		print "NOLOCK IS NOT DEFINED!\n" if $opt_D;
		if( Am_I_Up(-debug=>$opt_D,	-prefix=>$opt_B) ) {
			my($fname,$secs,$thresh)=GetLockfile(-prefix=>$opt_B);
			print "Cant Start space_monitor.pl - it is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nLock File=$fname secs=$secs thresh=$thresh secs\n";
			exit(0);
		}
	} else {
		print "NOLOCK IS DEFINED!\n" if $opt_D;
	}
}

# read override file if one exists
print "space_monitor.pl
	outdir			=>	$opt_o,
	PRODUCTION		=>	$PRODUCTION
	WRITEHISTORY	=>	$opt_R
	Alarm				=>	$opt_A
	batch_id			=>	$opt_B
	dohelptable		=>	$dohelptable\n";

print "Reading Override File\n" if $opt_d;
my(%overrides);
if( -r $gr_dir."/conf/threshold_overrides.dat" ) {
	print "Reading $gr_dir/conf/threshold_overrides.dat\n";
	open (TR, $gr_dir."/conf/threshold_overrides.dat" ) or die "Cant open threshold_overrides.dat\n";
	while (<TR>) {
		next if /^#/ or /^\s*$/;
		next unless /,\s*SybSpaceMonitor\s*,/;
		s/\s+$//;
		chomp;
		$overrides{$_}=1;
	}
	close(TR);
}

my(%helptable_to_collect);

if( $opt_R ) {
	my(@cfgdat)=get_conf_file_dat("space_monitor");
	foreach(@cfgdat) {
		next unless /space_monitor.pl/;
		my($x,$junk,$svr,$db)=split;
		die "WOAH: arg 2 is not 'tablespace' in space_monitor.dat" unless $junk eq "tablespace";
		$helptable_to_collect{$svr."/".$db}=1;
		print "Collecting sp__helptable Info for $svr $db (from space_monitor.dat)\n";
	}
}

dbi_set_web_page(0);
dbi_set_debug($opt_d);

MlpBatchJobStart(-BATCH_ID=>$opt_B) if $opt_B;

if( ! defined $opt_T or $opt_T eq "sybase" or $opt_T eq "both" ) {
	my(@servers);
	if( $PRODUCTION ) {
		@servers=get_password(-type=>"sybase",-PRODUCTION=>1);
	} else {
		@servers=get_password(-type=>"sybase");
	}
	print "Found ",($#servers+1)," Sybase Servers\n";
	foreach my $server (@servers) {
		if( defined $opt_S ) {
			#print "Ignoring $server (".length($server).") as it does not match $opt_S (".length($opt_S).")\n"
			#	if defined $opt_d and $server ne $opt_S;
			next if $server ne $opt_S;
		}
		process_a_server($server,"sybase");
	}
}

if( ! defined $opt_T or $opt_T eq "sqlsvr" or $opt_T eq "both" ) {
	if( is_nt() ) {
		my(@servers);
		if( $PRODUCTION ) {
			@servers=get_password(-type=>"sqlsvr",-PRODUCTION=>1);
		} else {
			@servers=get_password(-type=>"sqlsvr");
		}

	#	my(@servers)=get_password(-type=>"sqlsvr");
		print "Found ",($#servers+1)," Sql Servers\n";
		foreach my $server (@servers) {
			print "Ignoring $server as it does not match $opt_S\n"
				if defined $opt_d and defined $opt_S and $server ne $opt_S;
			next if defined $opt_S and $server ne $opt_S;
			process_a_server($server,"sqlsvr");
		}
	} else {
		print "Ignoring sql servers - this is not an windows box\n"
			if defined $opt_T;
	}
}

foreach ( @ALERTS ) {
	print "ALERT: $_\n";
}
if( $#ALERTS<0 ) {
	print "NO ALERTS FOUND\n";
}
MlpBatchJobEnd()if $opt_B;
print "Successful Completion at ".localtime(time)."\n";
I_Am_Down(-prefix=>$opt_B) if $opt_B and ! $NOLOCK;
exit(0);

sub process_a_server
{
	my($server,$type)=@_;
   print "Processing server $opt_S of type $type\n" if defined $opt_d;
	my($login,$password)=get_password(-type=>$type,-name=>$server);
	I_Am_Up(-prefix=>$opt_B)  if $opt_B and ! $NOLOCK;

	my($conntype);
	if( is_nt() or $type eq "sqlsvr" ) {
   		$conntype="ODBC";
	} else {
   		$conntype="Sybase";
	}

	print "[".localtime(time)."] $server...\t";
	if( ! dbi_connect(-srv=>$server,-login=>$login,-password=>$password, -type=>$conntype, -debug=>$opt_d) ) {
      		print "Cant connect to $conntype $server as $login - continuing\n";
      		return;
   	}
	print "Connected...\t";

  	my($server_type,@dbs)=dbi_parse_opt_D($opt_D,1);
  	dbi_msg_exclude(1131) if $type eq "sybase";	# ignore violations on oam pages
	my($c)=$#dbs;
	$c++;
	print $c," Databases\n";

	mkdir("$opt_o/$server",0777) unless -d "$opt_o/$server";

	foreach ( keys %helptable_to_collect ) {
		next unless /$server/;
		print "(override from space_monitor.dat) Collecting Table Space Info for $_\n";
	}

	foreach my $db (@dbs) {
		next if $db eq "model" or $db eq "tempdb";

		my($file);
		# YOU CAN ONLY run sp_helptable when REWRITE option is set
		if( $opt_R and ( $dohelptable or $helptable_to_collect{$server."/".$db} )) {
			$file=$opt_o."/$server/helptable.$server.$db.".today();
			if( ! -e $file ) {
				print " -- Saving Table Space For Database $db to $file\n";
				my($rc)=open(WR,">".$file);
				if( ! $rc ) {
					print "WARNING: Cant Write To File $file : $!";
					warn  "WARNING: Cant Write To File $file : $!";
					next;
				}
				foreach (dbi_query(-db=>$db,-query=>"exec sp__helptable \@dont_format=\"y\"")){
					print WR join("\t",dbi_decode_row($_)),"\n";
				}
				close(WR);
				chmod 0666, $file;
			}
		}

		###############################
		# GET DB SPACE
		###############################
		my($today_line);
		if( $type eq "sqlsvr" ) {
			print "Working on SQL SERVER\n" if $opt_d;
			my($q)="select (sum(size*10)/128)/10.,status&0x40 from sysfiles group by status&0x40";
			my($dbsp,$logsp)=(0,0);
			foreach ( dbi_query(-db=>$db,-query=>$q)){
				my($mb,$islog)=dbi_decode_row($_);
				$mb=~s/0+$//;
				$mb=~s/\.$//;
				if( $islog==0 ) {
					$dbsp+=$mb;
				}else{
					$logsp+=$mb;
				}
			}
			my($q)="exec PER_saveintstat '".
							$server  ."','sqlsvr',\n\t'".
							$db  ."',\n\t'','','space_monitor',null,\n\t".
							int($dbsp).",".
							int($logsp);
			_querynoresults( $q, 1 );
			$today_line = today()."\t$db\t".  $dbsp ."\t". $logsp 	."\n";
		} else {
			foreach ( dbi_query(-db=>$db,-query=>"exec sp__dbspace \@dont_format=\"y\"")){
				my(@dat)=dbi_decode_row($_);
				next if $#dat<4;
				print "Appending to $file\n" if $opt_d;
	  			print "$db: ",today()." ".join(" ",@dat),"\n" if $opt_d;

	  			$today_line = today()."\t".join("\t",@dat),"\n";
				print $server,"\t",today()."\t".join("\t",@dat),"\n" if defined $opt_D;

				my($dbpct)=$dat[3];
				my($log_pct)=$dat[6];
				$dbpct=~s/\s//;
				$log_pct=~s/\s//;
				$log_pct=0 if ! defined $log_pct or $log_pct =~ /^\s*$/;

        		my(@thresholds)=(90,92,96);
        		my($found_override)=0;
        		my($is_db_specific_err,$is_db_specific_warn,$is_db_specific_crit);
        		foreach ( keys %overrides ) {
        			next unless /^$server/;
        			print "Server Override=$_\n" if $opt_d;
        			my($s,$mp,$lvl,$thresh,$subsys)=split(/,/,$_,5);
        			if( defined $subsys and $subsys !~ /^\s*$/ and $subsys ne $db ) {
        				print "DBG DBG Skipping based on $db != $subsys\n" if $opt_d;
        				next;
        			}
        			$s=~s/\s//g;
        			if( $s ne $server ) {# handle case where server=ABC but file=ABC_DR
        				print "DBG DBG Server $server != $s\n" if $opt_d;
        				next;
        			}

        			print "DBG DBG Threshold Found lvl=$lvl t=$thresh s=$subsys\n" if $opt_d;
        			$lvl=~s/\s//g;
        			$thresh=~s/\s//g;
        			$found_override=1;
        			if( $lvl eq "WARNING" ) {
        				if( $is_db_specific_err  and ! $subsys ) {
        					print "Skipping Non DB Specific Override As Db Specific One Exists\n";
        					next;
        				}
        				$is_db_specific_err  = "TRUE" if $subsys;
        				$thresholds[0]=$thresh;
        				print "Found Warning Threshold of $thresh for $server $db\n" if $opt_d;
        			} elsif( $lvl eq "ERROR") {
        				if( $is_db_specific_warn and ! $subsys ) {
        					print "Skipping Non DB Specific Override As Db Specific One Exists\n";
        					next;
        				}
        				$is_db_specific_warn = "TRUE" if $subsys;
        				$thresholds[1]=$thresh;
        				print "Found Error Threshold of $thresh for $server $db\n" if $opt_d;
        			} elsif( $lvl eq "CRITICAL") {
        				if( $is_db_specific_crit and ! $subsys ) {
        					print "Skipping Non DB Specific Override As Db Specific One Exists\n";
        					next;
        				}
        				$is_db_specific_crit = "TRUE" if $subsys;
        				$thresholds[2]=$thresh;
        				print "Found Critical Threshold of $thresh for $server $db\n" if $opt_d;
        			} else {
        				error_out( "ERROR>>> UNABLE TO PARSE LEVEL $lvl - must be WARNING or CRITICAL or ERROR:\n line= $_\n");
        			}
        		}
	        	# correct if odd
	        	$thresholds[1]=$thresholds[0]+1 if $thresholds[0] >= $thresholds[1];
	        	$thresholds[2]=$thresholds[1]+1 if $thresholds[1] >= $thresholds[2];
	        	print "[ Thresholds For $server db=$db Overriden To ",join("/",@thresholds)," ]\n" if $found_override;

				my($state)="OK";
				$state="WARNING" 	if $dbpct>$thresholds[0];
				$state="ERROR" 		if $dbpct>$thresholds[1] 	or $log_pct>50;
				$state="CRITICAL" 	if $dbpct>$thresholds[2] 	or $log_pct>99;

				my($msg)= "";
				$msg = "GEM_002: "  if $state ne "OK";
				$msg .= "Data Space at ".$dbpct. "% ";
				$msg .= "Log space at ".$log_pct."%" if $log_pct>50;
				#$msg .= "\n(override using threshold_overrides.dat)" if $state ne "OK";

				printf " -- [time=%-3s min] %-20s state=%-8s %s\n", int((time-$STARTTIME)/60), $db, $state, $msg;

				if( defined $opt_A ) {
					print "Heartbeat($server,$db,$state,$msg)\n"	if defined $opt_d;
					MlpHeartbeat(
						-monitor_program=>$opt_B,
						-system=>$server,
						-subsystem=>"Space: ".$db,
						# -debug=>$opt_d,
						-state=>$state,
						-message_text=>$msg
					) if $opt_B;
				}
				push @ALERTS, "$state $server $db $msg\n" unless $state eq "OK";
			}
		}
		#################################

		if( $opt_R ) {
			$file=$opt_o."/$server/dbspace.$server.$db";
			my(@fileinfo);
			my $today_str =today();
			my($rc)=open(WR2,">".$file.".tmp");
			if( ! $rc ) {
				print "Cant Write To File $file.tmp : $!\n";
				warn  "Cant Write To File $file.tmp : $!\n";
				next;
			}

			if( -r $file ) {
				print "Purging file $file\n" if $opt_d;
				chmod 0666, $file;
				open(RD,$file) or error_out( "Cant Read $file\n" );
				while(<RD>) {
					# clobber space if not correct
					chomp;
					chomp;
					my($day,$dbase,$sp_all,$sp_usd,$sp_pct,$log_all,$log_usd,$log_pct)=split;
					if( $day!~/^\d\d\d\d\d\d\d\d$/ ) {
						warn "Cleaning Invalid Line In $file\n Line=$_ \n";
						next;
					}
					print WR2 $_,"\n" unless /^$today_str/;
				}
				close(RD);
			} else {
				print "No existing file $file\n";
			}
			print WR2 $today_line if $today_line;
			close(WR2);
			my($rc)=rename($file.".tmp",$file);
			if( ! $rc ) {
				warn "Non Fatal Error: Cant Rename $file.tmp to $file\n";
				unlink $file.".tmp";
			}
			chmod 0666, $file;
		}
	}
	dbi_disconnect();
	print "[".localtime(time)."] $server completed\n";

}

sub today
{
	my(@t)=localtime(time);
	$t[4]++;
	$t[5]+=1900;
	substr($t[3],0,0)="0" if length($t[3])==1;
	substr($t[4],0,0)="0" if length($t[4])==1;
	substr($t[5],0,0)="0" if length($t[5])==1;
	return $t[5].$t[4].$t[3];
}

sub error_out {
	my($msg)=join("",@_);
	$msg = "FATAL ERROR IN ".$opt_B.": ".$msg if $opt_B;
	$msg = "FATAL ERROR in space_monitor.pl: ".$msg unless $opt_B;
	MlpBatchJobErr($msg) if $opt_B;
	die($msg);
}
__END__

=head1 NAME

space_monitor.pl - space monitor for sybase and sql server

=head2 USED BY

SybSpaceMonitor
SpaceMonitorSqlsvr

=head2 DESCRIPTION

This utility monitors database space

It also monitors space history into ascii files.  This utility will save table space information the first time it
is run every day. These files are named helptable.SERVER.DB.DATE.TIME.
It will also save database level space information so that can be tracked.
Data is saved in the gem/data directory in ascii file format.

You may need to run two batches of this job - one for sqlserver (only
works from windows) and one for sybase (works both windows and sql).

=head2 USAGE

space_monitor.pl -outdir=outdir

MONITOR SPACE USAGE ON YOUR SYBASE/SQL SERVER DATABASES

-BATCHID=batchid
-DEBUG 			(debug)
-SERVER=Server
-DATABASE=Database
-TYPE=(DB Type - sqlsvr / sybase / both)
-DOHELPTABLE 		( table space )
-REWRITE		(Rewrite)
-ALARM			(Alarm appropriately)

With --DOHELPTABLE saves sp_helptable into output directory in files named
	helptable.SERVER.DB.DATE.TIME

This directory output is useful for the program plot_tablespace.pl
This program works with space_report.pl

=head2 OVERRIDES

The program uses threshold_overrides.dat for override information.

=cut

