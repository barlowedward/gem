# GEM

GEM Database Enterprise Manager

**********************************
*  GEM README                    *
**********************************

This is the readme file located in the GEM root directory.  The file 
you SHOULD be looking at is doc/index.html (all documentation is in
the doc subdirectory).   Browse there with your web browser now.

The GEM documentation in the doc subdirectory can also be found
online at http://www.edbarlow.com/gem/index.html.  The main site for 
our software is http://www.edbarlow.com.

**********************************
*  GEM QUICK OVERVIEW            *
**********************************

We understand that you are pressed for time have no time for complicated
installations.  Every attempt has been made to make your installation a
pleasure.  If you have problems with our installation process, please
contact us with advice on how to make the install easier.  

The installation instructions shipped with the GEM documentation give step 
by step instructions for installing and supporting GEM.  It is is strongly 
recommended that you follow these instructions.  These instructions are
clear and concise. They can be found on the above mentioned web page 
documentation.  If you encounter with installation problems, do not 
hesitate to contact us.  

GEM Contains internal diagnostics that you should be aware of.  Firstly,
most GEM commands can be run with --DEBUG (or -d for older style programs).
This will print diagnostics information when the program is run.  Additionally,
the GEM User Interface Programs (all the programs in the GEM Root) will 
create a timestamped output file in the logs subdirectory.  Please provide
a copy of the relevant log file when seeking technical support.  

The following is a summary checklist for your install.  We mentioned where
you can find full documentation - but continue reading here if you want
an out of date and really really short version of the procedure... 

PREREQUESITES (ie what you need)

Definition:    A hybrid environment is when you have both unix and win32
   servers to monitor

*  One monitoring server for your unix servers, one for your win32 servers.
      In a hybrid environment you will have 2 monitoring servers
*  If you have Hybrid you need a samba share that works on both 	  
*	A Sql Server (all win32 environment) or Sybase (for hybrid/unix envs) 
      database named gemalarms sized at  200-300 MB, turn select into on, 
	   might as well truncate logs. Create non-dbo non-privilidged login/user 
      gemalarms with a password of gemalarms.
*	perl 5.8.1 or later
        http://www.activestate.com/store/languages/register.plex?id=ActivePerl
*	DBI and DBD modules for your perl
		on windows, go to dos prompt and run 
			ppm install DBI
			ppm install DBD::ODBC
		unix users should install DBI and DBD::Sybase per standard perl module 
         procedures 

TEST YOUR PREREQUESITES

*	perl troubleshoot.pl

START THE INSTALLATION

*	perl configure.pl

If you get stuck, please read the html documentation!!!

Go through the screens left to right filling in yellowed fields.  Expect 
that when you run configure you will 

   *) need sa passwords for your servers and the sybase password for your
        unix systems (any account that can read the sybase error logs works)
   *) install our stored procedure library on all your servers
   *) on unix, you can choose ping/ftp/ssh/rsh on a server by server basis to
        handle your monitoring.
   *) decide whether you want to use our backup scripts - we currently recommend
      configuring our backup scripts by hand (ie. read the doc and hand edit the
      configure.cfg file - its easy) because the user interface to that file in
      the main GEM configuration is still under development.  As of 4/2006 we 
      recommend hand editing that file.
        
* Schedule Your Crontab

* View Your Console

* Install our web scripts into a script enabled directory on your web server.  
  This provides our GEM anywhere feature which includes a nice cgi-script 
  version of our event monitor (i personally prefer it to the standalone 
  version of the event monitor).

SUPPORT IS AVAILABLE at gemsupport@hotmail.com.  For installation 
  troubleshooting, please attach a copy of the logs/configure.log log file.   
  Final pricing and right to use of GEM have NOT been determined (4/2006) - 
  the package *will* be a commercial package - but we am giving significant 
  discounts for beta testers who provide feedback.  Again no determination 
  on if or how this project will charge has been made.

Thank You

Ed Barlow
October 2005-10


PS. If you like gem, spread the word!
ppm install http://www.bribes.org/perl/ppm/Win32-PerfMon.ppd
