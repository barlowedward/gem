use strict;

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Win32::IPConfig;
use Repository;
use Getopt::Long;
use CommonFunc;

use vars qw( $SYSTEMS $DEBUG);
$|=1;

sub usage
{
	return "Usage: win32_ipconfig_report.pl --DEBUG\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
   		# "SYSRPT"	=> \$SYSRPT,
		# "SWRPT"		=> \$SWRPT,
		"DEBUG"		=> \$DEBUG );


my(@servers)=get_password(-type=>'win32servers');

	print "<h1>GEM Microsoft IP Configuration Report</h1>";
	print "generated by win32_ipconfig_report.pl at ".localtime(time)."<br><hr>\n";
	print "<h2>Listing Of Servers And THeir Basic IP Configuration</h2>";
	print "<TABLE BORDER=1>\n";
	print "<TR BGCOLOR=BEIGE>
		<TH>System</TH>
		<TH>Domain</TH>
		<TH>Search List</TH>
		<TH>Node Type</TH>
		<TH>Is Router?</TH>
		<TH>Wins Enabled?</TH>
		<TH>LMHOSTS?</TH>
		<TH>DNS?</TH>
		</TR>\n";

	my(@sv);
	push @sv,"<TR BGCOLOR=BEIGE><TH>System</TH>\n\t<TH>Adapter</TH>\n\t<TH>DHCP?</TH>\n\t<TH>Domain</TH>\n\t<TH>IP Address</TH>\n\t<TH>Subnet Mask</TH>\n\t<TH>Gateway</TH>\n\t<TH>DNS</TH>\n\t<TH>Wins</TH></TR>";

	foreach my $srv (@servers) {
		my $ipconfig = Win32::IPConfig->new($srv);
		if( ! $ipconfig ) {
	        	print "<TR BGCOLOR=PINK><TD VALIGN=TOP>$srv</TD><TD COLSPAN=7>Unable to connect to $srv</TD></TR>";
	        	#warn "Unable to connect to $srv\n";
	        	next;
	        }

		print "<TR><TD VALIGN=TOP>",
		 	$ipconfig->get_hostname,
		 	"</TD>\n\t<TD VALIGN=TOP>",
    			$ipconfig->get_domain,
    			"</TD><TD VALIGN=TOP>".
    			join(" ",$ipconfig->get_searchlist),
    			"</TD>\n\t<TD VALIGN=TOP>",
    			$ipconfig->get_nodetype,
    			"</TD>\n\t<TD VALIGN=TOP>",
    			$ipconfig->is_router ? "Yes" : "No",
    			"</TD>\n\t<TD VALIGN=TOP>",
        		$ipconfig->is_wins_proxy ? "Yes" : "No",
        		"</TD>\n\t<TD VALIGN=TOP>",
    		 	$ipconfig->is_lmhosts_enabled ? "Yes" : "No",
    		 	"</TD>\n\t<TD VALIGN=TOP>",
			$ipconfig->is_dns_enabled_for_netbt ? "Yes" : "No",
			"</TD></TR>\n";

		  for my $adapter ($ipconfig->get_configured_adapters) {
    		  	push @sv, join( "","<TR><TD VALIGN=TOP>",
			 	$ipconfig->get_hostname,
		 		"</TD>\n\t<TD VALIGN=TOP>",
        			$adapter->get_name,
        			"</TD>\n\t<TD VALIGN=TOP>",
            			$adapter->is_dhcp_enabled ? "Yes" : "No",
            			"</TD>\n\t<TD VALIGN=TOP>",
        			$adapter->get_domain  || "&nbsp;",
        			"</TD>\n\t<TD VALIGN=TOP>",
        			join("<br>\n",$adapter->get_ipaddresses) || "&nbsp;",
        			"</TD>\n\t<TD VALIGN=TOP>",
        			join("<br>\n",$adapter->get_subnet_masks) || "&nbsp;",
        			"</TD>\n\t<TD VALIGN=TOP>",
        			join("<br>\n",$adapter->get_gateways) || "&nbsp;",
        			"</TD>\n\t<TD VALIGN=TOP>",
        			join("<br>\n",$adapter->get_dns) || "&nbsp;",
        			"</TD>\n\t<TD VALIGN=TOP>",
        			join("<br>\n",$adapter->get_wins) || "&nbsp;",
        			"</TD>\n\t</TR>" );
		}
	}
	print "</TABLE>";

	print "<h2>Listing Of All Adapters On The Servers (both enabled and disabled adapters)</h2>";
	print "<TABLE BORDER=1>";
	print join("\n",@sv);
	print "</TABLE>";

1;

__END__

=head1 NAME

win32_ipconfig_report.pl - Create Ip Configuration Report For Windows

=head2 USAGE

	win32_ipconfig_report.pl

=cut
