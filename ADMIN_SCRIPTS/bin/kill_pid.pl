#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
use MlpAlarm;
use CommonFunc;
use Logger;
use File::Basename;

use vars qw( $PID $TYPE $USER $SERVER $PASSWORD $OUTFILE $opt_d);

BEGIN {
   if( ! defined $ENV{SYBASE} or ! -d $ENV{SYBASE} ) {
      if( -d "/export/home/sybase" ) {
         $ENV{SYBASE}="/export/home/sybase";
      } else {
         $ENV{SYBASE}="/apps/sybase";
      }
   }
}

# Copyright (c) 1997-2004 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
	print "kill_pid.pl -USER=SA_USER -SERVER=SERVER -PASSWORD=SA_PASS [-debug] -TYPE=Sybase|ODBC ";

   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"PID=s"	=> \$PID,
		"USER=s"	=> \$USER ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"TYPE=s" 	=> \$TYPE ,
		"DEBUG"      	=> \$opt_d );

my($log_msgs_to_stdout);
$log_msgs_to_stdout=1 if defined $opt_d;

logger_init( -debug=> $opt_d, -command_run => $0  );

logdie( usage("Must pass server\n" ))		unless defined $SERVER;
logdie( usage("Must pass pid\n" ))		unless defined $PID;
logdie( usage("Must pass username\n" ))	unless defined $USER;
logdie( usage("Must pass password\n" ))	unless defined $PASSWORD;

unlink($OUTFILE) if defined $OUTFILE;

#
# CONNECT
#
debug( "Connecting\n" );
$TYPE="Sybase" unless defined $TYPE;
my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD,-type=>$TYPE);
if( ! $rc ) {
	logdie("Cant connect to $SERVER as $USER\n");
}

dbi_set_mode("INLINE");
my(@pids)=split(",",$PID);

my($count)=0;
while ($count<200) {
	foreach my $pid (@pids) {
		my $sql = "kill $pid";
		print $sql,"\n";
		foreach( dbi_query(-db=>"master",-query=>$sql)) {
			my(@x)=dbi_decode_row($_);
			print join(" ",@x),"\n";
		}
	}
	sleep 1;
	$count++;
}

dbi_disconnect();
exit(0);

sub debug { print @_ if defined $opt_d; }

__END__

=head1 NAME

kill_pid.pl - kills a pid

=head2 USAGE

	kill_pid.pl -USA_USER -SSERVER -PSA_PASS

=cut

