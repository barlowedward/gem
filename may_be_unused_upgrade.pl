: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

use strict;
use File::Basename;
use Cwd;

BEGIN {
	if( $] < 5.008001 ) {
		if( defined $^V and $^V!~/\s*$/) {
			die "Error : You must have perl 5.8.1 or later installed.  You are running $^V \n";
		}
		my($x)=$];
		$x=~s/0+/\./g;
		$x=~s/\.\.+/\./g;
		die "Error : You must have perl 5.8.1 or later installed.  You are running $x ($])\n";
	}
	if( ! defined $ENV{DISPLAY} and ! defined $ENV{PROCESSOR_LEVEL} ) {
		die "Error : DISPLAY environment variable not set!\n";
	}
}

END {		# We do this next thing to allow running right from the
		# command line with a doubleclick on windows.
                if( defined $ENV{PROCESSOR_LEVEL} ) {
                        print "<Hit Enter Key To Continue>";
                        <STDIN>;
                }
}

use lib "lib";
use CommonFunc;

my  $curdir=dirname($0);
chdir($curdir) or die "Cant cd to $curdir: $!\n";

print "upgrade.pl version 1.1

This script performs all upgrade operations...

It is designed for use AFTER you have extracted the GEM software
over a working GEM installation.  Eventually, it will download and
extract the latest version of the code as well as just doing the
post processing.\n\n";

my($VERSION,$sht_version)	=	get_version();
print "Current Version - $VERSION ($sht_version)\n";

die "NOW HOW CAN WE NOT HAVE A CONF DIRECTORY???" unless -d "conf";

sub readfile {
	my($filename)=@_;
	my(%C);
	open(CFGFILE,$filename) or die("Cant Read $filename: $!\n");	
	my($txt)="";
	while (<CFGFILE>) {
		chomp;		
		s/\s+$//;
		if( /^\s*$/ or  /^\s*#/ ) { $txt.=$_."\n"; next; }	# comment
		if( /=/ ) {
			$txt.=$_."\n";
			my($key,$value)=split(/=/,$_,2);
			$C{$key} = $value;			
		} else {
			warn "Broken Directive: $_\n";
		}
	}
	close CFGFILE;
	
	#foreach (sort keys %C ) { print "DBG DBG: $_ VAL: $C{$_} \n"; } 
	return($txt,%C);
}
	
if( -r "conf/configure.cfg" ) {
	print "Updating Configuration File configure.cfg\n";
	print "Reading conf/configure.cfg.sample\n";
	my($samptxt,%SAMPLECFG)=readfile("conf/configure.cfg.sample");
	
	print "Reading conf/configure.cfg\n";	
	my($realtxt,%REALCFG)=readfile("conf/configure.cfg");
	#foreach (sort keys %REALCFG ) { print "KEY: $_ VAL: $REALCFG{$_} \n"; }
	my($txt)="";
	my($needswrite)=0;
	foreach ( sort keys %SAMPLECFG) { 
		if( defined $REALCFG{$_} ) {
			#printf  "> %20s S=%20s R=%20s %s\n", $_,$SAMPLECFG{$_},$REALCFG{$_},"Y" ; 
		} else {
			print "Adding Directive: $_=\n";
			#printf  "> %20s S=%20s R=%20s %s\n", $_,$SAMPLECFG{$_},$REALCFG{$_},"N" ; 
			$needswrite++;
			$realtxt.=$_."=\n";
		}
	}
	if( $needswrite ) {
		print "CHANGES FOUND! UPDATING conf/configure.cfg\n";
		print "renaming configure.cfg to configure.cfg.UPGRADE\n";
		unlink( "conf/configure.cfg.UPGRADE" ) if -r "conf/configure.cfg.UPGRADE";
		rename("conf/configure.cfg","conf/configure.cfg.UPGRADE" ) or die "Cant rename configure.cfg to configure.cfg.UPGRADE";
		die("HMM CONFIGURE.CFG STILL EXISTS\n") if -r "conf/configure.cfg";
		open(OUT,"> conf/configure.cfg") or die "cant write conf/configure.cfg $!";	
		print OUT $realtxt;
		close OUT;
		print "Saved new version of configure.cfg\n";
	}
	#foreach ( sort keys %REALCFG) { print "\CONFIG: $_ $REALCFG{$_}\n"; }
} else {
	print "Skipping configure.cfg - does not exist\n";
}

my($dir)=cwd();
my($includes)=" -I$dir/lib ";		#-I$dir/plugins ";
$includes=" -I$dir/Win32_perl_lib_5_8 ".$includes	if defined $ENV{PROCESSOR_LEVEL};
$includes .= $ENV{perlargs}." " if defined $ENV{perlargs};

print "Generic Enterprise Manager Setup Driver\n\n\tMessages will be logged to configure.log\n\n";
print  "$^X $includes $dir/bin/configure.pl --LOGFILE=logs/upgrade_part1.log --UPGRADEALARMS ".join(" ",@ARGV)."\n\n";
system("$^X $includes $dir/bin/configure.pl --LOGFILE=logs/upgrade_part1.log --UPGRADEALARMS ".join(" ",@ARGV));
print  "$^X $includes $dir/bin/configure.pl --LOGFILE=logs/upgrade_part2.log --UPGRADE ".join(" ",@ARGV)."\n\n";
system("$^X $includes $dir/bin/configure.pl --LOGFILE=logs/upgrade_part2.log --UPGRADE ".join(" ",@ARGV));

__END__

=head1 NAME

configure.pl - GEM Configuration Tool

=head2 USAGE

<p><font color=RED>perl configure.pl</FONT></p>

=head2 DESCRIPTION

configure.pl is the GEM configuration utility.  The primary output of this program are the configuration
files stored in the conf directory.  These files can also be hand edited.  Additionally, configure.pl will
do important installation tasks like modify all the programs in the GEM to have appropriate library paths
and hash bangs (for unix).  It also has an interface to the windows task scheduler (only if you are running
on unix) and can create a crontab file if you are on unix.

GEM is a server adminstration tool, and it is expected
that you have administrator access to your databases.  You will not need administrator (root) permissions on the systems themselves,
just on the databases.  The only reason you need sa is that you will be installing some stored procedures into each server.  Configure.pl creates a file configure.log which can be sent to tech support if you have issues with your installation.

If you cant register with the store on step two (probably a firewall issue), dont sweat it
You must "Save Configuration Changes" to write out your configuration files.  Some operations (adding stored procedures to servers)
are, however permanent once completed.
Register some servers on the Servers tab.  You can ignore servers of types you dont own (unix, oracle...).

The files tab is confusing.  This tab will be expanded to include any log files you wish to have incorporated into the integrated
alarm system - but for now it only relates to Sybase Error Logs on unix/linux. For Sybase logs, you only need to identify the $SYBASE
directories of concern.  The survey process searches for important sybase files (interfaces files, sybase logs, and Run_* files) and
relates servers to log files by parsing the Run_* files (ie. it associates ABC.log with server ABC by parsing the RUN_ABC file).

=head2 HOW TO USE configure.pl

In General, work your way through the tabs from left to right.

=over 3

=item * Welcome Tab / License Acceptance

This tab provides basic information on GEM.  You must select "I AGREE WITH THE LICENSE" to continue.


=item * Register Tab

This tab allows on line registration of GEM.  The information we collect will never be shared with outside parties, so you
might as well make it accurate.  Select the appropriate environment for your install - All windows, all Unix, or Hybrid.

In environments where you have both Windows and Unix servers, running a Hybrid installation installed on a samba share
is preferred.  I use special techniques so that the same scripts can work on both
unix and windows environments.  This allows you to run similar tools using the same
configuration files from both enviornments.

=item * Paths Tab

This tab is pretty simple if you have an All Windows or All Unix environment.  It is more complex, however, if you
have a mixed mode enviornment.  GEM needs some information to map the two environments.  Specifically it needs to know
what perl to run in each environment (ie.  what is the perl path on the Windows and the Unix server that will be running
your Batch Jobs).  It also needs to know directory mappings.  You should put the full path name to top level of the
GEM directory on both Unix and Windows.  On windows you should use a fully qualified path (ie.  //hostname/drive/gem).  You
can also put in 2 alternate paths to this same directory.  If, for example, //hostname/drive was mapped to G: or H: depending
on what workstation you are on, put G:/gem and H:/gem for alternate code locations.  GEM can only run if the program being
run can find the GEM Home directory in one of these 4 directory paths because it uses these paths to dynamically define the perl
include library paths.

=item * Mail Tab

What is your SMTP mail server name.  I believe this is only required on Windows.

=item * Servers Tab

Set em up here.  If you just getting started with GEM, we recommend installing just a few servers.
Currently, GEM requires sa passwords for your databases.  Server Login access on unix can, however, be a normal
account (the sybase or oracle account is preferred). Windows Login access is done using NT authentication so you dont
need to do anything - you just need to schedule the batch jobs as an account that has appropriate permissions.
Sadly, native windows NT authenticaion is not currently possible for database access using Perl/DBI.

The windows accounts you run as should be able to, however, fetch windows eventlogs and do directory size
operations on any shares you identify that you want monitored (ie. C$ D$ E$).

=item * Files Tab

The files tab is a bit complex.  Here is the deal.  GEM will perform surveys of your systems to identify what type of
system it is and what files to collect and parse.  It needs to know where to start.  This is mainly needed for unix
systems.  At a first cut, you need to tell the system what the Sybase Server and Sybase Open Client directories are
on your systems.  Surveys will look through  these directories and identify startup and configuration files.  These
will be parsed to identify server types and log files.

So you will click Add for each of your unix servers, and identify the Sybase Server and Sybase Open Client Directories.

Now the tricky part.  You might have UNUSED run files in your tree.  These will probably have unused error log files
attached.  You dont need to do this - so once the system has been discovered, and you identify these files in the
console error reports, you will come back to this screen and select Ignore this RUN_ File.  You can also identify
Operating system log files, Oracle ORAHOMEs, Sql Server Plan Log Directories, and  Application Logs.  I would wait on
those.  Just identify your sybase directories for now.

The output of this tab (once saved) is the special syntax in the unix_passwords.dat configuration file.  This syntax looks like

   SERVERNAME LOGIN PASSWORD
        VARIABLE=VALUE

where the variables are described in the header of the configuration file.

This tab shows discovered files in blue.  To ignore a file that has been discovered, just click the ignore button next to it.

=item * Backups Tab

=item * Html Tab

The CONSOLE is normally stored in the data/CONSOLE_REPORTS directory of the GEM install.  This might not be where you
want to publish it though.  You can use this tab to copy the Console to another directory on the same server or to another
server.  Copies to another hostname will be done with ftp.  This is done for security reasons.  If you do not want
to have your users be able to get anywhere near the gem tree, this gives you an option.

=item * Connections Tab

The connections tab allows you to test connectivity and run surveys.  BE SURE TO SELECT THE SYBASE AND SQL SERVER
TABS AND USE THIS TAB TO INSTALL YOUR SYSTEM STORED PROCEDURE LIBRARY.  This is where to do it.  The "Install Procedure
Library" tab at the top of the page installs into all servers of the given type - first checking that they are not
at the latest release level.  The Install Library button will install into an individual server.

Thes tabs also display the latest installed release level of the stored procedure library.

=item * Alarms Tab

The alarms subsystem must be installed - objects created etc.   The only true requirement for this system is
that the database be named gemalarms.  I also recommend that you create a login gemalarms with a password gemalarms
for the alarming.  It does not really matter what the login and password are however.  Remember (as per the above
prerequesite instructions), you need to have select into turned on and you need to have the login created prior to
Installing the Alarm Database.  Additionally, you need to have registered the server you are installing on.  If you
dont, the system wont know the sa password to use for the install.   The Alarm Mail Address can be made up - it will
be the "From: " address for your alarm mails.  It would be confusing if you used your own mail address.  I suggest something
like alarm@mycompany.com.

=item * Install Software Tab

The install software button runs all the other operations.

=item * Post Install Tasks Tab

These operations are designed to simplify post installation tasks as noted in the post installation tasks document.

=item * Scheduler Tab

GEM integrates with cron and with the windows task scheduler.  THis screen allows you to define where you want
your jobs to run, and it will then schedule jobs appropriately.  As jobs are created during the configuration
process, the Repaint buttons at the page top will refresh the screen data.  This screen also allows you to create
a crontab file to help you schedule jobs on unix.

=back

=head1 NAME

A Few Notes

=over 3

=item * If You add a SQL Server, it adds the corresponding Windows Server

=item * If you have no unix servers, the Files tab will be blank

=item * Leave the backups tab alone for your first pass

=item * The configure.log file is useful to see output from prior installations

=back

=head1 NAME

SET UP YOUR SCHEDULED JOBS

Installation to the windows scheduler is automated from the configuration utility.
The scheduled jobs are stored in .../gem/win32_batch_scripts and .../unix_batch_scripts and you can read the setup_instructions.txt file
that is created by the user interface for more details.

=cut

