create table #ty ( ptype varchar(30), stype varchar(30) )

insert #ty values ( 'unix','unixinv')
insert #ty values ( 'win32servers','ldap computer')
insert #ty values ( 'sqlsvr','sqlsvr_discovery')
insert #ty values ( 'sybase','sybase_discovery')
insert #ty values ( 'oracle','oracle_discovery')
insert #ty values ( 'mysql','mysql_discovery')

-- select * from #ty

select *
from INV_attribute_dictionary d1, INV_attribute_dictionary d2, #ty
where d1.system_type=#ty.ptype
and   d2.system_type=#ty.stype
and   d1.attribute_name = d2.compute_into_name
and(
	d1.attribute_type != d2.attribute_type
or d1.master_display_key != d2.master_display_key
or d1.detail_display_key != d2.detail_display_key
or d1.display_text != d2.display_text
)

update INV_attribute_dictionary
set
	attribute_type = d2.attribute_type,
	master_display_key = d2.master_display_key,
	detail_display_key = d2.detail_display_key,
	display_text = d2.display_text
from INV_attribute_dictionary d1, INV_attribute_dictionary d2, #ty
where d1.system_type=#ty.ptype
and   d2.system_type=#ty.stype
and   d1.attribute_name = d2.compute_into_name
and(
	d1.attribute_type != d2.attribute_type
or d1.master_display_key != d2.master_display_key
or d1.detail_display_key != d2.detail_display_key
or d1.display_text != d2.display_text
)
