#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Std;
use File::Find ();
use File::Basename;

$| = 1;	# as always

my($curdir)=dirname($0);
#chdir $curdir or die "Cant cd to $curdir: $!\n";

sub usage
{
	print @_;
	print "core_dump_checker.pl -o outdir  -l file_for_bad_links -s size_file

If -l is passed, bad links will also be output to the appropriate file
If -s is passed, files > 1MB will be put in that file

Finds core dump files and makes an html report of them.  Basically does
a find on all files on your system.  Reads (outdir)/core_dump_checker.hist
for files it has found on prior runs.  \n";
   return "\n";
}

use vars qw($opt_o $opt_l $opt_s);
die usage("Bad Parameter List\n") unless getopts('o:l:s:');
if( ! defined $opt_o ) {
	use Repository;
	my($gr_dir)=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gr_dir;
	$opt_o = "$gr_dir/data/CONSOLE_REPORTS";
	die "No Input File $opt_o" unless -d $opt_o;
}

die usage("Must Pass Output Directory\n") unless defined $opt_o;
die usage("Output Directory $opt_o does not exist\n") unless -d $opt_o;
$opt_o=~s/\s*$//;
$opt_o=~s/\/$//;

# for the convenience of &wanted calls, including -eval statements:
use vars qw/*name *dir *prune/;
*name   = *File::Find::name;
*dir    = *File::Find::dir;
*prune  = *File::Find::prune;

my($core_count)=0;
my(%FILE_BY_TIME,%SOURCE_BY_TIME,@bad_links,%file_size);

# Traverse desired filesystems
# dont read /proc,/dev, /nas (NFS Mounted) or /net (NFS Mounted)
opendir(DIR,"/") || die("Can't open directory / for reading\n");
my(@dirlist) = grep(!/^\.|^proc$|^dev$|^nas$|^net$/,readdir(DIR));
closedir(DIR);

foreach (@dirlist) {
	next unless -d "/".$_ and -x "/".$_ and ! -l "/".$_;
	print ".";
	File::Find::find(\&wanted, "/".$_);
}
print "Found $core_count Files Existing On System\n";

my(%historical_data);

# read history file
if( -e $opt_o."/core_dump_checker.hist" ) {
	open(HIST,$opt_o."/core_dump_checker.hist")
		or die "Cant Read $opt_o/core_dump_checker.hist : $!\n";
	foreach (<HIST>) {
		chop;
		my($tm,$file,$source)=split;
# print "DBG FOUND $tm $file\n";
		next if defined $FILE_BY_TIME{$tm};
		$core_count++;
		$FILE_BY_TIME{$tm}=$file;
		$SOURCE_BY_TIME{$tm}=$source if defined $source and $source !~ /^\s*$/;
		$historical_data{$tm}="<TR><TD>".$file."</TD><TD><PRE>".scalar(localtime($tm))."</PRE></TD><TD>".$SOURCE_BY_TIME{$tm}."</TD></TR>\n";
	}
	close HIST;
}
print "After Historical File $core_count Files Found\n";

if( $core_count <= 0 ) {
	print h2("No Core Files Found");
} else {
	unlink "$opt_o/core_dump_checker.hist";
	open(HIST,"> $opt_o/core_dump_checker.hist")
		or die "Cant Write $opt_o/core_dump_checker.hist : $!\n";
	foreach ( sort keys %FILE_BY_TIME ) {
		print HIST $_," ",$FILE_BY_TIME{$_}," ",$SOURCE_BY_TIME{$_},"\n";
	}
	close HIST;

	print "<H2>CORE DUMPS</H2><FONT SIZE=-1><TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n<TR><TH BGCOLOR=#FFCCFF>TIME</TH><TH BGCOLOR=#FFCCFF>File</TH><TH BGCOLOR=#FFCCFF>Program That Crashed</TH>\n";
	foreach (sort numerically keys %historical_data) {
		print $historical_data{$_};
	}
	print "</TABLE>\n";
	print "$core_count Core Files Found\n\n";

	my(%COUNT_BY_HOUR,%COUNT_BY_DOW );
	for( my $i = 0;$i<24;$i++) {
		if(length($i)==1) {
			$COUNT_BY_HOUR{"0".$i}=0;
		} else {
			$COUNT_BY_HOUR{$i}=0;
		}
	}
	for( my $i = 0;$i<7;$i++)  { $COUNT_BY_DOW{$i}=0; }
	foreach (sort keys %FILE_BY_TIME ) {
		$COUNT_BY_HOUR{get_time(-time=>$_,-fmt=>"hh")}++;
		$COUNT_BY_DOW{ get_time(-time=>$_,-fmt=>"dw")}++;
	}

	print "<h2>COUNT BY HOUR</h2><table border=1><tr><th>HOUR</th><th>NUMBER OF DUMPS</th></tr>\n";
	foreach (sort keys %COUNT_BY_HOUR) {
		print "<TR><TD>",$_,"</TD><TD>",$COUNT_BY_HOUR{$_},"</TD></TR>\n";
	}
	print "</table>\n";

	print "<h2>COUNT BY DAY OF WEEK</h2><table border=1><tr><th>DAY OF WEEK</th><th>NUMBER OF DUMPS</th></tr>\n";
	foreach (sort keys %COUNT_BY_DOW) {
		print "<TR><TD>",$_,"</TD><TD>",$COUNT_BY_DOW{$_},"</TD></TR>\n";
	}
	print "</table>\n";
}

if( defined $opt_l ) {
	open(LI,">".$opt_l) or die "Cant Write $opt_l : $!";
	print LI "Report created by core_dump_checker.pl at".localtime(time)."\n";
	print LI "----------------------------\n";
	print LI "| Bad Symbolic Links Found |\n";
	print LI "----------------------------\n\n";
	print LI join("\n",sort @bad_links);
	close(LI);
}

if( defined $opt_s ) {
	open(LI,">".$opt_s) or die "Cant Write $opt_s : $!";
	print LI "Report created by core_dump_checker.pl at".localtime(time)."\n";
	print LI "-------------------\n";
	print LI "| Files Over 2 MB |\n";
	print LI "-------------------\n\n";
	foreach ( sort size_numerically keys %file_size ) {
		printf( LI "%-12.12s %s\n",$file_size{$_},$_);
	}
	close(LI);
}

exit(0);

sub size_numerically {
	$file_size{$b} <=> $file_size{$a};
}

sub numerically {
	$b <=> $a;
}

sub wanted {
	# print $name,"\n";
	# print "prune $name\n" if -d $name and ! -x $name;
	$prune = 1 if -d and ! -x;
   $prune = 1 if -l;
	$prune = 1 if ! -r;

	if( -r and /^core$/s ) {
 		my($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = lstat($_);
		$FILE_BY_TIME{$mtime} =  $name;
		my($fileinfo)=`/usr/bin/file $name`;
		$fileinfo =~ /from \'([\w\W]+)\'$/;
		$SOURCE_BY_TIME{$mtime}=$1;
		$historical_data{$mtime}="<TR><TD>".$name."</TD><TD><PRE>".scalar(localtime($mtime))."</PRE></TD><TD>".$SOURCE_BY_TIME{$mtime}."</TD></TR>\n";
		$core_count++;
	}

	# find bogus links
   $opt_l && -l && !-e && push @bad_links,$name;

	# -f normal file - find large files
	my($sz) = -s;
	$opt_s && -f && $sz>2000000 && ($file_size{$name}=$sz);
}

sub get_time
{
   my(%OPT)=@_;
	croak("Must Pass -fmt argument to get_time")
		unless defined $OPT{-fmt};

	my($now)=$OPT{-time};
   $now=time unless defined $now;

   my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($now);

   $mon++;
   $year+=1900;
   substr($mon,0,0)='0' 	if length($mon)==1;
   substr($mday,0,0)='0' 	if length($mday)==1;
   substr($hour,0,0)='0' 	if length($hour)==1;
   substr($min,0,0)='0' 	if length($min)==1;
   substr($sec,0,0)='0' 	if length($sec)==1;

	my($rc)=$OPT{-fmt};
	$rc =~ s/yyyy/$year/g;
	substr($year,0,2)="";
	$rc =~ s/yy/$year/g;
	$rc =~ s/mm/$mon/g;
	$rc =~ s/dd/$mday/g;
	$rc =~ s/hh/$hour/g;
	$rc =~ s/dw/$wday/g;
	$rc =~ s/mi/$min/g;
	$rc =~ s/ss/$sec/g;

   return $rc;
}

__END__

=head1 NAME

core_dump_checker.pl - utility to find core dumps on unix systems

=head2 USAGE

core_dump_checker.pl -o outdir

=cut
