-- report_key=IO_USAGE
-- run_frequency=15min
-- do_diff=Y
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=
-- report_footer=
-- enabled=Y
-- performancetype=int
-- performancekey=io_usage

select distinct @@SERVERNAME as server_name,
      case when db_name(dbid) is null then 'Adhoc Queries' else  db_name(dbid) end as db_name,
		'','',sum(total_logical_reads), sum(total_logical_writes) as  total_io
from sys.dm_exec_query_stats s1
cross apply sys.dm_exec_sql_text(sql_handle) as  s2
group by db_name(dbid)
