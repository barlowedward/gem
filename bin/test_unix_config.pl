#!C:\Perl\bin\perl.exe

# Copyright (c) 1997-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
use Sys::Hostname;
use CommonFunc;
use Net::myFTP;
use Net::myRSH;
use strict;
use Repository;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

use Getopt::Long;
use vars qw( $DEBUG );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions(  "DEBUG"		=> \$DEBUG);

my $curhostname = hostname();
print "test_unix_config.pl : version 2.0 \nchecking your unix connectivity\n";
print "run on $curhostname at ".localtime(time)."\n";

my($dir)=get_gem_root_dir();
if( ! -e "$dir/conf/unix_passwords.dat" ) {
	print "ERROR: Can not read $dir/conf/unix_passwords.dat";
	exit(1);
}
if( ! -r "$dir/conf/unix_passwords.dat" ) {
	print "OK: System Not Set Up - No Unix Information Found";
	exit(0);
}

my(@hosts)=get_password(-type=>"unix");
my(@warnings);
my(%state,%stype);
foreach my $host (@hosts) {
	print "Working on $host\n";
   my($login,$password)=get_password(-type=>"unix",-name=>$host);
   my(%args)=get_password_info(-type=>"unix",-name=>$host);   	
	if( $host eq $curhostname ) {
		print "\tOk - You are running on this host\n";
		next;
	};
	$stype{$host}=$args{SERVER_TYPE};
	$state{$host}="WARNING";
	
	my($method)=$args{UNIX_COMM_METHOD};
	$method=$args{WIN32_COMM_METHOD} if is_nt();
	if( $method eq "NONE" ) {
		print "\tIgnoring - METHOD=NONE\n";
		next;
	}
	$method="SSH" if ( ! defined $method or $method eq "" ) and ! is_nt();

	if( ! defined $args{SERVER_TYPE} ) {
		push @warnings, sprintf("host %20s : %s",$host,"No SERVER_TYPE argument found for server\n");
	} elsif( $args{SERVER_TYPE} ne "PRODUCTION"
	and 	$args{SERVER_TYPE} ne "CRITICAL"
	and 	$args{SERVER_TYPE} ne "DEVELOPMENT"
	and 	$args{SERVER_TYPE} ne "DR"
	and 	$args{SERVER_TYPE} ne "QA" ) {
		push @warnings, sprintf("host %20s : %s",$host,"type=$args{SERVER_TYPE} => MUST BE PRODUCTION/DEVELOPMENT/QA/DR\n");
	}

	my(@sybase_dirs)=split(/\s/,$args{SYBASE});
	my(@more_sybase_dirs)=split(/\s/,$args{SYBASE_CLIENT})
		if defined $args{SYBASE_CLIENT} and $args{SYBASE_CLIENT} !~ /^\s*$/;
	push @sybase_dirs,@more_sybase_dirs;

	if( $method ne "FTP" ) {
		print "\tTesting $method api to $host\n";
		my($dat)=do_rsh(-login=>$login, -hostname=>$host, -method=>$method,
				-command=>"echo helloworld",
				-debug=>$DEBUG);
		if( $dat =~ /^failure/ ) {
			push @warnings, "rsh to $host - ".$dat;
			print " $dat\n";
			next;
		} elsif( $dat eq "helloworld" ) {
			print " test ok\n" if $DEBUG;
		} else {
			die "HOW ODD - THE COMMAND RETURNED $dat\n";
		}
	}

	print "\tTesting $method Connectivity (login=$login) ";	#XXX if $DEBUG;
   my($ftp) = Net::myFTP->new($host,Debug=>undef, Timeout=>5, METHOD=>$method);
   if( ! $ftp )  {
   		my($c)=sprintf("host %20s : %s",$host," $method Failed To FTP Connect $@\n");
   		push @warnings, $c;
   		print "\n\t",$c unless $DEBUG;
   		print "test failed...\n" if $DEBUG;
   		next;
	}
	my($rc,$err)=$ftp->login($login,$password);
	if( ! $rc ) {
		my($c)=sprintf("host %20s : %s",$host," $method Failed To Log In as $login $@ $err\n");
   		push @warnings, $c;
   		print "\n\t",$c unless $DEBUG;
			print "test failed...\n" if $DEBUG;
   		next;
	}
	print "...OK\n";
	$ftp->ascii();

	print "\tTesting Important Directories\n" if $DEBUG;
	my($isbad)=0;
	foreach my $file_dir (@sybase_dirs) {
		next if $file_dir =~ /^\s*$/;
		print "\tTesting $file_dir  "; #XXX if $DEBUG;
		if( ! $ftp->cwd($file_dir) ) {
			my($c)=sprintf("host %20s : %s",$host,"Cant Chdir To $file_dir $@\n");
			push @warnings, $c;
			print "\n\t\t",$c unless $DEBUG;
			$isbad++;
			print "test failed...\n" if $DEBUG;
			next;
		}
   	my(@files)=$ftp->ls();
   	if( $#files <= 0 ) {
			my($c)= sprintf("host %20s : %s",$host,"No Files Found In $file_dir\n");
			push @warnings, $c;
			print "\n\t\t",$c unless $DEBUG;
			$isbad++;
			print "test failed...\n" if $DEBUG;
   		next;
		}
		print " ...OK\n";
	}
	$ftp->close();

	if( $isbad==0 ) {
		print "\tMarking Host $host As OK\n" if $DEBUG;
		$state{$host}="OK";	
	}else {
		print "\tMarking Host $host As Having Errors\n" if $DEBUG;		
	}	
}

#alarm( $opt_T ) if defined $opt_T;
#BEGIN {
#	$SIG{ALRM} = sub { die "ERROR: PROGRAM TIMED OUT\n"; }
#}


print "\n";

if( $#warnings < 0 ) {
	print "SUCCESSFUL: No errors found\n";
	exit(0);
} else {
	print "Listing Errors from encountered in Unix Test Batch\n";
	foreach (@warnings) {	chomp;print "ERROR: ",$_,"\n"; }
	exit(1);
}

#print "...................................\n";
#foreach (sort keys %state) {
#	$state{$_} = "WARNING" unless defined $state{$_};
#	printf "%10s %30.30s %20s\n",$state{$_},$_,$stype{$_};
#
#}
