# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

#
# A PLUGIN TO SHOW/MANAGE PLUGIN INFORMATION
#
package plugins;
@ISA = qw(Plugin);

use strict;
use File::Basename;

use vars qw($VERSION);
$VERSION = ".01";
my($std_page_name)="plugin";

use Tk::Table;
my($main_plugin_nb_frame);
my($details_subframe);

my( @rtclick ) = (	"Download Plugin Catalog",
			"Show All Plugins"	 );

my(@menuitems)=("Configuration/Plugins/Show Installed",
		"Configuration/Plugins/Show Catalog" 	);

sub getExplorerButtons {   my(%x); $x{Plugins}="Plugins";return %x; }
sub getMenuItems    {	   return (@menuitems); }
sub getPluginPanes {	return $std_page_name;}

sub init {
	my($package)=@_;

	$main_plugin_nb_frame=$package->clear_tab($std_page_name);
	my($button_bar_frm) = $main_plugin_nb_frame->Frame()->pack( -fill=>'x', -side=>'top');
    	$package->make_button(-widget=>$button_bar_frm,
    			-help=>'Download Plugin Catalog From The Web',
    			-text=>"Download Plugin Catalog",
    			-command=> sub { $package->show("Download Plugin Catalog",undef);
    				} )->pack( -side=>'left', -padx=>3, -pady=>8 );

    	 $package->make_button(-widget=>$button_bar_frm,
    			-help=>'Show Installed Plugins',
    			-text=>"Show Installed Plugins",
    			-command=> sub { $package->show("Show Installed Plugins",undef); }
    				)->pack( -side=>'left', -padx=>3, -pady=>8 );

    	 $package->make_button(-widget=>$button_bar_frm,
    			-help=>'Show Available Plugins',
    			-text=>"Show Available Plugins",
    			-command=> sub { $package->show("Show Plugins You Have Not Installed",undef); }
    				)->pack( -side=>'left', -padx=>3, -pady=>8 );

}

sub event_handler
{
	my($package)=shift;
	my(%args)=@_;

	# if your context is Plugins and you tab out of Plugins - Force a redraw
	if( $args{-context} eq "Plugins" and $args{-eventtype} eq "tabchange" and $args{-curtab} ne $std_page_name ){
		$package->refresh_tree("sqlsvr");
		return;
	}

	return if $args{-eventtype} eq "treertclick" and $args{-context} ne "Plugins";
	return if $args{-eventtype} eq "treebrowse"  and $args{-context} ne "Plugins";

	return unless $args{-curtab} eq $std_page_name or $args{-eventtype} eq "menu"
		or $args{-eventtype} eq "expbutton";

	my(@path_dat)=split(/\//,$args{-entrypath});

	if( $args{-eventtype} eq "menu" ) {
		if( $args{-menuitem} eq "Configuration/Plugins/Show Installed" ) {
			$package->statusmsg( "[plugins] event_handler() called\n" );
			$package->statusmsg( "[plugins] menuclick($args{-menuitem})\n" );
			$main::notebook_f->raise("Plugin") unless $args{-curtab} eq $std_page_name;
			$package->show("Show Installed Plugins",undef);
		} elsif(  $args{-menuitem} eq "Configuration/Plugins/Show Catalog" ) {
			$package->statusmsg( "[plugins] event_handler() called\n" );
			$package->statusmsg( "[plugins] menuclick($args{-menuitem})\n" );
			$main::notebook_f->raise("Plugin") unless $args{-curtab} eq $std_page_name;
			$package->show("Show All Plugins",undef);
		}
	} elsif( $args{-eventtype} eq "tabchange" ) {
		return if $args{-curtab} ne $std_page_name;
		$package->statusmsg( "[plugins] event_handler() called\n" );
		$package->refresh_tree("Plugins");
		$package->show("Show Installed Plugins") unless defined $details_subframe;
	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {
		return if $args{-curtab} ne $std_page_name;
		$package->statusmsg( "[plugins] event_handler() called\n" );

		my($entryPath)=$args{-entrypath};
		#$package->statusmsg(  "[plugins] browse($entryPath)\n" );

		# ok we have a variety of things we could have browsed from
		if( $entryPath eq "/Plugins" ) {
			$package->show("Show All Plugins");
		} elsif( $entryPath eq "/" ) {
			$package->show("Show All Plugins");
		} elsif( $entryPath eq "/Plugins/Installed" ) {
			$package->show("Show Installed Plugins");
		} elsif( $entryPath eq "/Plugins/Not Installed" ) {
			$package->show("Show Plugins You Have Not Installed");
		} elsif( $entryPath eq "/Download Plugin Catalog" ) {
			$package->show("Download Plugin Catalog");
		} elsif( $entryPath eq "/Show All Plugins" ) {
			$package->show("Show All Plugins");
		} elsif( $entryPath =~ "^/Plugins/Installed" ) {
			#my($plugin)=basename($entryPath);
			#$package->show("Show A Plugin",$plugin);
			$package->show("Show A Plugin",$path_dat[3]);
   		} elsif( $entryPath =~ "^/Plugins/Not Installed" ) {
			#my($plugin)=basename($entryPath);
   			#$package->show("Show A Plugin",$plugin);
   			$package->show("Show A Plugin",$path_dat[3]);
   		} else {
   			$package->statusmsg( "[plugins] Something not efficient - $entryPath we just ran treebrowse on unknown path in plugins\n" );
   		}

	} elsif( $args{-eventtype} eq "treertclick" ) {
		if( $args{-clicktext} eq "Properties" ) {
   			$package->statusmsg( "[plugins] event_handler() called\n" );
			$package->show("Show A Plugin",$path_dat[3]);
		} elsif( $args{-clicktext} eq "Download Plugin Catalog" ) {
			$package->statusmsg( "[plugins] event_handler() called\n" );
			$package->show("Download Plugin Catalog");
		} elsif( $args{-clicktext} eq "Show All Plugins" ) {
			$package->statusmsg( "[plugins] event_handler() called\n" );
			$package->show("Show All Plugins");
		} elsif( $args{-clicktext} =~ /^Install/ and  $args{-clicktext} =~ /Plugin$/ ) {
			$package->statusmsg( "[plugins] event_handler() called\n" );
			$package->unimplemented("Cant Install Plugins Yet\n" );
		} elsif( $args{-clicktext} =~ /^UnInstall/ and $args{-clicktext} =~ /Plugin$/ ) {
			$package->statusmsg( "[plugins] event_handler() called\n" );
			$package->unimplemented("Cant UnInstall Plugins Yet\n" );
		}

	} elsif( $args{-eventtype} eq "expbutton" ) {
		#$package->refresh_tree("Plugins")	# kind of redundant to do this
		if( $args{-curtab} ne $std_page_name and $args{-context} eq "Plugins" ) {
			$package->statusmsg( "[plugins] event_handler() called\n" );
			$main::notebook_f->raise("Plugin");
		}

	} elsif( $args{-eventtype} eq "imgbutton" ) {
	}
}

sub getTreeRtclk {
	my($package,$curTreeType,$treeitmptr)=@_;
	$package->debugmsg(  "[monitor.pm] getTreeRtclk(curtype=$curTreeType)\n" );
	return(undef) unless $curTreeType eq "Plugins";
	my(@rightclicks);
	foreach my $itm ( @$treeitmptr ) {
		if ( $itm eq "/" ) {
			push @rightclicks, \@rtclick;
		} else {
			my(@d)=split(/\//,$itm);
			# print "PROCESSING $#d/$d[0]/$d[1]/$d[2]/$d[3]/\n";
			if( $#d==1 ) {
				push @rightclicks, \@rtclick;
			} elsif( $#d==2 ) {
				if( $d[2] eq "Installed" ) {
					push @rightclicks, \@rtclick;
				} elsif( $d[2] eq "Not Installed" ) {
					push @rightclicks, \@rtclick;
				} else {
					die "THis Cant HAPPEN $d[2]!\n";
				}
			} elsif( $#d==3 ) {
				if( $d[2] eq "Installed" ) {
					my( @regular_opts )= (
						"Install $d[3] Plugin",
						"UnInstall $d[3] Plugin",
						"Properties" );
					push @rightclicks,\@regular_opts;
				} elsif( $d[2] eq "Not Installed" ) {
					my( @regular_opts )= (
						"Install $d[3] Plugin",
						"Properties");
					push @rightclicks,\@regular_opts;
				} else {
					die "THis Cant HAPPEN $d[2]!\n";
				}
			}
		}
	}
	return \@rightclicks;
}

sub getTreeItems {
	my($package)=shift;
	my $curTreeType=shift;

	$package->debugmsg(  "[plugins] getTreeItems(curtype=$curTreeType)\n" );
	return(undef) unless $curTreeType eq "Plugins";

	my(@treeitems);

	push @treeitems, "/Plugins";
	my(@itmrtclk);
	foreach ( @rtclick ) { 	push @treeitems, "/".$_; }

	# /Plugins/Installed
	push @treeitems, "/Plugins/Installed";
	my(@itmrtclk);
	foreach ( @rtclick ) { 	push @itmrtclk, $_; 	}

	my($x)=$$package{-package_imports};
	foreach (sort keys %$x){
		push @treeitems, "/Plugins/Installed/$_";
	}

	my($pm)=$$package{-pluginmanager};
	my(@notinstalled)=$pm->not_installed_plugins();

	if( $#notinstalled>=0 ) {
		push @treeitems, "/Plugins/Not Installed";
		foreach ( @notinstalled ) { push @treeitems, "/Plugins/Not Installed/$_"; }
	}
	return \@treeitems;
}



# show(action,pluginname) - the meat of this module
sub show {
   	my($package, $action, $pluginname)=@_;
   	$package->statusmsg(  "[plugins] show( Action=$action, Plugin=$pluginname )\n" );

   	if( 	    $action ne "Download Plugin Catalog"
   		and $action ne "Show Installed Plugins"
   		and $action ne "Show Plugins You Have Not Installed"
   		and $action ne "Show All Plugins"
   		and $action ne "Show A Plugin"  	) {
   			$package->messageBox(-message=>"Programming Error - invalid action $action", -title=>"ERROR");
			return;
   	}

   	if( $action eq "Download Plugin Catalog" ) {
   		$$package{-pluginmanager}->fetch_catalog_from_web();
   		$package->show("Show All Plugins", $pluginname);
   		return;
	}

	$details_subframe->destroy() if Tk::Exists($details_subframe);
	$details_subframe = $main_plugin_nb_frame->Frame()->pack(
				-fill=>'both', -expand=>1, -side=>'bottom', -anchor=>'n');

	# ok was name passed
	if( defined $pluginname ) {
		my($label_frm) = $details_subframe->Frame()->pack( -fill=>'x', -side=>'top', -anchor=>'n');

		$label_frm->Label( -text=>"Showing Details for plugin: $pluginname",
			-relief=>'ridge', -bg=>'white' )->pack(-fill=>'x', -anchor=>'n', -side=>'top');
		my($plugin_frm) = $details_subframe->Frame()->pack(
				-fill=>'x', -expand=>1, -side=>'top', -anchor=>'n');

		my($pm) = $$package{-pluginmanager};
		my(%dat)= $pm->plugin_data($pluginname);
		my($row)=4;

		$plugin_frm->Label(-justify=>'right',-text=>"")->grid(
			-sticky=>'e',-row=>$row,   -column=>1,-padx=>2,-pady=>2);
		$plugin_frm->Label(-justify=>'left',-text=>"")->grid(
			-sticky=>'w',-row=>$row++, -column=>2,-padx=>2,-pady=>2);

   		foreach my $d ( sort keys %dat ) {
   			#print "$d --> $dat{$d} \n";
   			my($lbl)=ucfirst($d);
   			$lbl =~ s/_/ /g;

   			if( ref $dat{$d} ) {
   				if( ref $dat{$d} ne "HASH" ) {
   					use Data::Dumper;
   					print "KEY=$d\n";
   					print Dumper %dat;
   					print "ERROR------------\n";
   				}
   				my( %d2 ) = %{$dat{$d}};
   				my($str)="";
   				foreach ( keys %d2 ) {
   					next unless defined $d2{$_} and $d2{$_} !~ /^\s*$/;
   					$str.=$_." : ".$d2{$_}."\n";
   				}
   				chomp $str;

   				$plugin_frm->Label(-justify=>'right',-text=>$lbl." : ")->grid(
					-sticky=>'e',-row=>$row,   -column=>1,-padx=>2,-pady=>2);
				$plugin_frm->Label(-justify=>'left',-text=>$str)->grid(
					-sticky=>'w',-row=>$row++, -column=>2,-padx=>2,-pady=>2);


   			} else {
   				$plugin_frm->Label(-justify=>'right',-text=>$lbl." : ")->grid(
					-sticky=>'e',-row=>$row,   -column=>1,-padx=>2,-pady=>2);
				$plugin_frm->Label(-justify=>'left',-text=>$dat{$d})->grid(
					-sticky=>'w',-row=>$row++, -column=>2,-padx=>2,-pady=>2);

			}
		}
		$package->statusmsg(  "[plugins] Done Showing Details for plugin $pluginname" );
		return;
	}

	$details_subframe->Label( -text=>$action, -relief=>'ridge', -bg=>'white' )->pack(-fill=>'x');
	my($pm)=$$package{-pluginmanager};

	my($catalog)=$pm->get_catalog();
	my(%plugin_data)=$pm->plugin_data();
	#my(@plgins2) = sort keys %plugin_data;
	#print "DBG : ",join(" ",@plgins2),"\n";
	my(@plgins) = sort keys %$catalog;

	#print "DBG : ",join(" ",@plgins),"\n";
	my($tbl_rows)=($#plgins+2);
	$tbl_rows=10 if $tbl_rows>10;

	my($table)= $details_subframe->Table(
					-rows=>   $tbl_rows,
 				   	-columns=> 5,
 				   	-scrollbars => 'osoe' );

 	$table->put(0,0,$table->Label(-justify=>'left',-text => "Plugin Name", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,1,$table->Label(-justify=>'left',-text => "Installed\nVersion", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,2,$table->Label(-justify=>'left',-text => "Catalog\nVersion", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,3,$table->Label(-justify=>'left',-text => "Description", -relief => 'sunken', -bg=>'beige'));
 	#$table->put(0,4,$table->Label(-text => "Plugin Date", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,4,$table->Label(-justify=>'left',-text => "State", -relief => 'sunken', -bg=>'beige'));
 	$table->put(0,5,$table->Label(-justify=>'left',-text => "Action", -relief => 'sunken', -bg=>'beige'));

	my($row)=0;

	foreach my $nm ( @plgins ) {
		#print "Working on Plugin $nm\n";
		my(%installed) 		= %{$plugin_data{$nm}} if defined $plugin_data{$nm};
		my(%catalog_data)	= %{$$catalog{$nm}};
		my($is_installed)=1 if defined $plugin_data{$nm} and $installed{file_name} ne "N.A.";
		next if $action eq "Show Installed Plugins"		 and ! defined $is_installed;
		next if $action eq "Show Plugins You Have Not Installed" and   defined $is_installed;
		$row++;


		# print " ... $nm / $plugin_data{$nm} \n" ;
		my($state)="";
		my($bgcolor)="white";
		if( ! defined $is_installed ) {
			$state="Not Installed";
			$bgcolor='yellow';
			$table->put($row,1,$table->Label(-text =>"n.a.", -relief => 'sunken', -bg=>$bgcolor));
		} else {
			if( $catalog_data{version} > $installed{version} ) {
				$state="Out Of Date";
				$bgcolor='orange';
			} else {
				$state="Installed";
			}
			$table->put($row,1,$table->Label(-bg=>$bgcolor,-text =>$installed{version}, -relief => 'sunken'));
		}
 		$table->put($row,0,$table->Label(-justify=>'left',-text =>$nm, -relief => 'sunken',
 			-bg=>'beige'));
		$table->put($row,2,$table->Label(-bg=>$bgcolor,-justify=>'left',
			-text =>$catalog_data{version}, -relief => 'sunken'));
 		$table->put($row,3,$table->Label(
			-bg=>$bgcolor,
			-justify=>'left',
			-text =>$catalog_data{short_description},
			-relief => 'sunken'
			));

		#$table->put($row,3,$table->Label(-text =>$installed{file_date}, -relief => 'sunken', -bg=>'white'));
		$table->put($row,4,$table->Label(-bg=>$bgcolor,-justify=>'left',
			-text =>$state, -relief => 'sunken'));

		my($obj)=$package->make_button(
			-widget=>$table,
    			-help=>"Download Latest $nm Plugin and Upgrade",
    			-text=>"Install",
    			-command=> sub {
    				$$package{-pluginmanager}->fetch_plugin_from_web($nm);
    				$package->show($action, $pluginname); }
    			);

		$table->put($row,5,$obj);

		#foreach ( keys %installed ) {
		#	print " ... $nm key=$_  val=$installed{$_}\n";
		#}
	};

      	$table->pack(-side=>'left',-expand=>1,-fill=>'both');
      	$package->statusmsg(  "[plugins] Done Painting Plugins Data\n" );
}

1;

__END__

=head1 NAME

Plugin.pm - Plugin Manager

=head2 DESCRIPTION

This plugin manages plugins.  It is a system plugin and is responsible
for displaying available plugins and permitting downloading of them.

=head2 FUNCTIONALITY

	Show Installed Plugins
	Show Plugin Catalog

=head2 EXPLORER TREE
	/	0+1
	Plugins		1
		Installed	1
			Plugins	2
		Not Installed	1
			Plugins	2
	Download Plugin Catalog	1
	Show All Plugins	1

