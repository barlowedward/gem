#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use MlpAlarm;

#
# Script to monitor feedhandlers
#
my $Control="/home/wombat/bin/control.cfg";
my $ControlNas="/home/wombat/bin/control.cfg.nasdaq.linux";
my $StatusCmd="/home/wombat/bin/feedstatus";
# cd_home() or die "Cant Cd home";
#if( is_up() ) {
	#print "Progam is up - Halting.  Remove the .lck file extension in the program\n";
#}
	#is_up() if $CONFIRM;
# Read Configuration Information
my(%server_by_feed);
my($DEBUG);

if( 0 ) {
	#$server_by_feed{liquidity} = "openbook1";
	$server_by_feed{bfx_demo} = "directfeed8";
	$DEBUG=1;
} else {
	open(CONF,"$Control") || die "Could not open $Control file\n";
	while(<CONF>) {
		chomp;
		my($feed,$servers,$interface)=split(/:/,$_);
		$server_by_feed{$feed} = $servers;
	}
	close(CONF);

	open(CONF2,"$ControlNas") || die "Could not open $ControlNas file\n";
	while(<CONF2>) {
		chomp;
		my($feed,$servers,$interface)=split(/:/,$_);
		$server_by_feed{$feed.".linux"} = $servers;
	}
	close(CONF2);
	$DEBUG=0;
}

foreach my $FeedName (  keys %server_by_feed ) {
	my @Serverlist = split(/\,/,$server_by_feed{$FeedName});
	foreach my $Server ( @Serverlist ) {
		my($rc)=StatusInfo($FeedName,$Server,$DEBUG);
		my(%FeedStatus)=%$rc;

		if( defined $FeedStatus{primary} and defined $FeedStatus{secondary} ) {
			print "Both Defined\n" if $DEBUG;
			$FeedStatus{primary}->{'linestatus'} = 'down' if length($FeedStatus{primary}->{'linestatus'}) < 2;
			$FeedStatus{primary}->{'status'} = 'Down'     if length($FeedStatus{primary}->{'status'}) < 2;
			$FeedStatus{secondary}->{'linestatus'} = 'down' if length($FeedStatus{secondary}->{'linestatus'}) < 2;
			$FeedStatus{secondary}->{'status'} = 'Down'     if length($FeedStatus{secondary}->{'status'}) < 2;
		} elsif( defined $FeedStatus{primary} ) {
			print "Primary Defined\n" if $DEBUG;
			$FeedStatus{primary}->{'linestatus'} = 'down' if length($FeedStatus{primary}->{'linestatus'}) < 2;
			$FeedStatus{primary}->{'status'} = 'Down'     if length($FeedStatus{primary}->{'status'}) < 2;
		} elsif( defined $FeedStatus{secondary} ) {
			print "Secondary Defined\n" if $DEBUG;
			$FeedStatus{secondary}->{'linestatus'} = 'down' if length($FeedStatus{secondary}->{'linestatus'}) < 2;
			$FeedStatus{secondary}->{'status'} = 'Down'     if length($FeedStatus{secondary}->{'status'}) < 2;
		} else {
			print "WARNING: No data monitor program failed\n";
			$FeedStatus{secondary}->{'linestatus'} = 'unknown';
			$FeedStatus{secondary}->{'status'} = 'Unknown';
		}

		my $message="$Server Feed=$FeedName";
		my $alarmstate;

		# my $message = "$FeedName $Server $FeedStatus{primary}->{'status'} $FeedStatus{primary}->{'linestatus'}";

		foreach ( sort keys %FeedStatus ) {
			my($s)= $FeedStatus{$_}->{status};
			if(  $s eq "BadParse" ) {
				$message .= " $_ return is unparsable \n";
			} else {
				$message .= " $_ state=".  $FeedStatus{$_}->{status}.  " line=".$FeedStatus{$_}->{linestatus}."\n";
			}

			if( defined $alarmstate ) {
				if( $s eq "Down" or $s eq "Unknown" ) {
					$alarmstate = "ERROR";
				} elsif( $s eq "BadParse" ) {
					$alarmstate = "WARNING";
				} elsif( $s ne "Normal" and $s ne "Sleep" ) {
					die "Unknown status $s (coding problem)\n";
				}
			} else {
				if( $s eq "Normal" or $s eq "Sleep" ) {
					$alarmstate = "OK";
				} elsif( $s eq "Down" or $s eq "Unknown" ) {
					$alarmstate = "ERROR";
				} elsif( $s eq "BadParse" ) {
					$alarmstate = "WARNING";
				} else {
					die "Unknown status $s (coding problem)\n";
				}
			}

			# print "status is blank\n" if $FeedStatus{$_}->{status} =~ /^\s*$/;
			# print "linestatus is blank\n" if $FeedStatus{$_}->{linestatus} =~ /^\s*$/;
			# print "status is undef\n" unless defined $FeedStatus{$_}->{status};
			# print "linestatus is undef\n" unless defined $FeedStatus{$_}->{linestatus};
		}
		print "==>Heartbeat ($alarmstate) $message\n";
		MlpHeartbeat ( -system=>$Server, -subsystem=>$FeedName,
							-monitor_program=>"WADMIN",
							-state=>$alarmstate,
							-message_text=>$message );
	}
}

exit;

sub StatusInfo {
	my($Feed,$FeedServer,$Debug) = @_;
	print "Running $StatusCmd $Feed $FeedServer\n";
	my(%rc);
	open(STATUS,"$StatusCmd $Feed $FeedServer | ") || die "Could not run program $StatusCmd\n";
	while(<STATUS>) {
		next if /rvd/;
		next if /WombatSessionRv/;
		next if /Replies/;
		chomp;
		print "Read: ",$_,"\n" if $Debug;
		my($Info,$feed,$feedhost,$ft,$status,$linestatus,$mdrvcount,$linecount,$version) = split(/\s+/,$_);

		print "PARSE INFO=",$Info,"\nPARSE FEED=",$feed,"\nPARSE FEEDHOST=",$feedhost,"\nPARSE FT=",$ft,"\nPARSE STATUS=",$status,"\nPARSE LINESTATUS=",$linestatus,"\nPARSE MDRVCOUNT=",$mdrvcount,"\nPARSE LINECOUNT=",$linecount,"\nPARSE VERSION=",$version,"\n" if $Debug;
		$feed = (split(/_/,$feed,2))[0] unless $feed =~ /xpress/ or $feedhost=~/superdirect/;

		# convert cta and nasdaq feed
		$feed = 'nlevel2' if $feed eq 'nqds';
		$feed = 'utp' if $feed eq 'uqdf' || $feed eq 'utdf';
		$feed = 'cta' if $feed eq 'cts' || $feed eq 'cqs';
		#
		$ft =~ s/\(\d\d\)//;
		if ( "$feed" eq "$Feed" ) {
			$linestatus = 'up' if $Feed eq 'openbook' && $linestatus eq 'part';
			$linestatus = 'up' if $linestatus =~ /^\d/;
			$linestatus = 'up' if $linestatus =~ /unk/;
			$linestatus = 'up' if $Feed eq 'ssl2mdrv';
			$status = 'Normal' if $Feed eq 'ssl2mdrv';
			#
			$linestatus = 'down' if length($linestatus) < 2;
			$status = 'Down' if length($status) < 2;
			$ft = 'primary' if length($ft) < 2;
			#
print "Saving Ft=$ft Status=$status Linestatus=$linestatus\n" if $Debug;
			$rc{$ft}->{'status'} = $status;
			$rc{$ft}->{'linestatus'} = $linestatus;
			$rc{$ft}->{'mdrvcount'} = $mdrvcount;
			$rc{$ft}->{'linecount'} = $linecount;
		} else { # no response from feed handler
			print "-- WARNING: $feed is not equal $Feed\n";
			print "-- INPUT LINE: $_ \n";
			$ft = "primary";
			$rc{$ft}->{'status'} = 'BadParse' unless defined $rc{$ft};
			$rc{$ft}->{'linestatus'} = 'BadParse' unless defined $rc{$ft};
		}
	}
	close(STATUS);
	if( $Debug ) {
		foreach (keys %rc ) {
			print "DEBUG: $_:",
				" Status=",$rc{$_}->{'status'} ,
				" LineStatus=",$rc{$_}->{'linestatus'} ,
				" Mdrvcount=",$rc{$_}->{'mdrvcount'} ,
				" Linecount=",$rc{$_}->{'linecount'} ,"\n";
		}
	}
	return \%rc;
}
