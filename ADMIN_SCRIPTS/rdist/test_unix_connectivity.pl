#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Basename;
use Sys::Hostname;
use Getopt::Long;
use Repository;
use Net::myRSH;
use Net::myFTP;
use File::Path;
use MlpAlarm;
use CommonFunc;

use vars qw($SYSTEM $SOURCE_FILE $ACTION $TARGET_FILE $NOEXEC $COMMAND $DEBUG $RUNMETHOD $TARGET_DIR);
use vars qw($TIMEOUT $COLUMNATE $COMMANDOVERRIDE $SILENTMODE $HTML $FIELDS_TO_SHOW $COMMAND_2 $COMMAND_3);
use vars qw($ADDNEWLINE $BATCHID $REPORTFILE $TESTMODERPT $TESTMODEUPDATE );

sub usage {
	my($msg)=@_;
   print $msg,"\n";
   print "putget.pl  GENERIC UNIX FILE MANAGER\n";

   print '
   --DEBUG              --NOEXEC
   --COLUMNATE          --FIELDS_TO_SHOW=a,b [ comma separated ]
   --SILENTMODE         --HTML	--BATCHID=AlarmBatchId

   --SYSTEM=sys         [ optional - comma separated list ]
   --RUNMETHOD=ssh|ftp  [ override default behaviors of servers ]
   --TIMEOUT=SECS       [ timeout for subcommands ]
   --COMMANDOVERRIDE    [ use config file directive to override command ]
   --ADDNEWLINE         [ add a new line between server output ]

   --SOURCE_FILE=file     --TARGET_FILE=file        --TARGET_DIR=dir
   --COMMAND=cmd          --COMMAND_2               --COMMAND_3
   --REPORTFILE=file      --TESTMODERPT=file
   --TESTMODEUPDATE update configfiles with best comm method (TESTMODE ONLY)

   --ACTION=REPORT		[ include $REPORTFILE;
        foreach(data/system_information_data/$host/$TARGET_FILE)
              run process_file($filename);
        Run report_file() ]

   --ACTION=REMOVE      [ cd $TARGET_DIR; remove $TARGET_FILE ]
   --ACTION=RENAME      [ cd $TARGET_DIR; rename($SOURCE_FILE,$TARGET_FILE); ]
   --ACTION=LS          [ cd $TARGET_DIR; ls $TARGET_FILE or ls ]
   --ACTION=STORE       [ cp $SOURCE_FILE
        ..data/system_information_data/$host/$TARGET_FILE ]
   --ACTION=GET         [ cp $SOURCE_FILE to [$TARGET_DIR/]$TARGET_FILE ]
   --ACTION=PUT         [ rcp $SOURCE_FILE $TARGET_DIR/$TARGET_FILE ]
   --ACTION=RUN         [ exec $COMMAND on remote system ]';

   return "\n";
}

die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless GetOptions(
         "SOURCE_FILE=s"  		=> \$SOURCE_FILE	,
         "ACTION=s" 				=> \$ACTION	,
         "DEBUG"  				=> \$DEBUG	,
         "TESTMODERPT=s" 		=> \$TESTMODERPT,
         "TESTMODEUPDATE"		=> \$TESTMODEUPDATE,

         "HTML" 	 				=> \$HTML	,
         "COMMANDOVERRIDE" 	=> \$COMMANDOVERRIDE	,
         "ADDNEWLINE" 			=> \$ADDNEWLINE	,
         "BATCHID=s" 			=> \$BATCHID	,
         "REPORTFILE=s" 		=> \$REPORTFILE	,
         "RUNMETHOD=s" 			=> \$RUNMETHOD	,
         "SYSTEM=s"  			=> \$SYSTEM	,
         "COMMAND=s"  			=> \$COMMAND	,
         "TARGET_FILE=s"  		=> \$TARGET_FILE ,
         "TARGET_DIR=s"  		=> \$TARGET_DIR ,
         "NOEXEC"					=> \$NOEXEC,
        	"TIMEOUT=s"  			=> \$TIMEOUT	,
      	"COLUMNATE"  			=> \$COLUMNATE	,
      	"FIELDS_TO_SHOW=s"  	=> \$FIELDS_TO_SHOW	,
      	"COMMAND_2=s"  		=> \$COMMAND_2	,
      	"COMMAND_3=s"  		=> \$COMMAND_3
);

die "Cant pass any other arguments if --TESTMODE\n" if $ACTION or $COMMAND;
$TIMEOUT=10 unless $TIMEOUT;
$ACTION="RUN";
$COMMAND="echo 'test_completed'";

die "CAN NOT FIND REPORTFILE=$REPORTFILE" if $REPORTFILE and ! -r $REPORTFILE;
require $REPORTFILE if -r $REPORTFILE;
die "WOAH: REPORTFILE WAS NOT INCLUDED\n" if $ACTION eq "REPORT" and ! $REPORTFILE;

my($NL)="\n";
$NL="<br>\n" if $HTML;
my($FS)="";
my($FB)="";
my($FE)="";
$FB="<FONT COLOR=BLUE>" if $HTML;
$FS="<FONT COLOR=RED>" 	if $HTML;
$FE="</FONT>" if $HTML;

my $curhostname = hostname();

my(@hosts) = get_password(-type=>"unix");
@hosts=split(/,/,$SYSTEM) if $SYSTEM;

$BATCHID="test_unix_connectivity.pl" unless $BATCHID;

print "$BATCHID run at ".localtime(time)." on host ".$curhostname.$NL;
print "Testmode has been set to test connectivity",$NL if $TESTMODE;
if( $SYSTEM ) {
	print "Running on: ".join(" ",@hosts),$NL;
} else {
	my($c)=$#hosts;
	$c++;
	print "Running on $c Hosts",$NL;
}
print "RUNNING IN TEST CONNECTIVITY MODE",$NL;
print "This command will run $FB ssh user\@system $FE",$NL;
print "Rsh will be tested if ssh fails",$NL;

print "TARGET_FILE=$TARGET_FILE TARGET_DIR=$TARGET_DIR \n"
	if ( $TARGET_FILE or $TARGET_DIR ) and ! $REPORTFILE;
print "REPORT FILE =$REPORTFILE\n" if $REPORTFILE;
print "Server List: $SYSTEM",$NL if defined $SYSTEM;
print "Command run in DEBUG mode",$NL if $DEBUG;
print "\n";

my(@hosts) = get_password(-type=>"unix");
@hosts=split(/,/,$SYSTEM) if $SYSTEM;

# BUILD COLUMN MAP
my $hlen=0;
my(@show_column);
if( $ACTION eq "RUN" ) {
	$show_column[100]=1;
	if( defined $FIELDS_TO_SHOW ) {
		$FIELDS_TO_SHOW =~ s/\s//g;
		foreach ( split(/,/,$FIELDS_TO_SHOW)) { $show_column[$_]=1; }
	} else {
		my($i)=0;
		while ($i<100) { $show_column[$i]=1; $i++; }
	}
}

# OUTPUT VARIABLES
#
my( %OUTPUT );							# by host
my( $file_count_for_host )=1;

# THE FOLLOWING VARIABLES MOSTLY RELATE TO TESTMODE
# THE _OK VARS ARE yes | no while the reason are a textual reason
my( %FTP_OK, %SSH_OK, %RSH_OK );
my( %FTP_REASON, %SSH_REASON, %RSH_REASON );
my( %SSH_COMMAND, %RSH_COMMAND );
my( %LOGIN_USED, %DEFAULT_METHOD );

my(@warnings);
foreach my $host (sort @hosts) {
	my($login,$password)=get_password(-type=>"unix",-name=>$host);
	$LOGIN_USED{$host}=$login;
	if( ! $login ) {
		$FTP_OK{$host} = $RSH_OK{$host} = $SSH_OK{$host} = "NO";
		$FTP_REASON{$host} = $RSH_REASON{$host} = $SSH_REASON{$host} = $OUTPUT{$host} = "Unable to get login\n";
		next;
	}
	if( ! $password ) {
		$FTP_OK{$host} = $RSH_OK{$host} = $SSH_OK{$host} = "NO";
		$FTP_REASON{$host} = $RSH_REASON{$host} = $SSH_REASON{$host} = $OUTPUT{$host} = "Unable to get password\n";
		next;
	}
	my(%args)=get_password_info(-type=>"unix",-name=>$host);
	my($method);
	if( is_nt() ) {
		$method=$args{WIN32_COMM_METHOD};
	} else {
		$method=$args{UNIX_COMM_METHOD};
	}
	$DEFAULT_METHOD{$host}=$method;

	print "Working on host $host (default method=$method)",$NL if $DEBUG;

	$method=uc($RUNMETHOD) if $RUNMETHOD;							# FROM THE COMMAND LINE
	$method="FTP" if ! defined $method or $method eq "";

	local $SIG{ALRM} = sub { die "CommandTimedoutPG"; };

	$method="FTP";
	$TARGET_DIR="/";

	test_a_host ( $host,$login,$password,$method,$ACTION,$COMMAND,$DEBUG_DOHOST);
$FTP_OK{$host} = "NO";
$OUTPUT{$host} = $FTP_REASON{$host} = "FTP Command Timed Out";

sub test_a_host {
	my($host,$login,$password,$method,$ACTION,$COMMAND,$DEBUG_DOHOST)=@_;
	my($IS_OK,$REASON);
	print "[$host] TESTING SERVER $host - METHOD=$method",$NL;

	$! = "";
	eval {
		alarm($TIMEOUT) if $TIMEOUT;
		do_host( $host, $login, $password, "FTP", "LS", undef,$DEBUG );
		alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedoutPG/ ) {
			print "[$host] $method Command Timed Out to $host".$NL if $DEBUG;
			$IS_OK = "NO";
			$REASON= "Command Timed Out";
		} else {
			if( $@ !~ /^\s*$/ ) {
				print "[$host] $method Command Returned $@ from $host".$NL if $DEBUG;
				$REASON = $FS."$method Command Returned $@ from $host".$FE;
			} else {
				die "DBG DBG ODDITY $@";
			}
			alarm(0);
		}
	}
	alarm(0);
	if( ! $IS_OK and $OUTPUT{$host} =~ /\s\/*dev\s/ ) {
		$IS_OK = "YES";
		print "[$host] method $method ***OK***",$NL;
	} else {
		$IS_OK = "NO";
		$REASON = join("",split(/\n/,$OUTPUT{$host})) unless $FTP_REASON{$host};
		print "[$host $method] FTP OUTPUT is ".join("",split(/\n/,$OUTPUT{$host}))."\n";
	}
}

	#
	# IF TESTMODE RUN SSH NEXT
	#
   $method="SSH" if $TESTMODE;
	my $cmd = $COMMAND;
   $cmd = $args{$COMMANDOVERRIDE} if defined $COMMANDOVERRIDE
				and defined $args{$COMMANDOVERRIDE}
				and $args{$COMMANDOVERRIDE} !~ /^\s*$/;
	eval {
		alarm($TIMEOUT) if $TIMEOUT;
		do_host( $host, $login, $password, $method, $ACTION, $COMMAND,$DEBUG );
		alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedoutPG/ ) {
			if( $TESTMODE ) {
				$SSH_OK{$host} = "NO";
				$OUTPUT{$host} = $SSH_REASON{$host} = "$method Command Timed Out";
			} else {
				print "$method Command Timed Out to $host".$NL if $DEBUG;
				push @warnings, "$method Command Timed Out to $host";
				$OUTPUT{$host} = $FS."$method Command Timed Out to $host".$FE;
			}
		} else {
			if( $@ !~ /^\s*$/ ) {
				print "$method Command Returned $@ from $host".$NL if $DEBUG;
				push @warnings, "$method Command Returned (".$@.") from $host";
				$OUTPUT{$host} = $FS."$method Command Returned $@ from $host".$FE;
			} else {
				print "DBG DBG ODDITY $@";
			}
			alarm(0);
		}
	}
	alarm(0);

	#
	# IF TESTMODE THEN DO RSH! LAST
	#
	next unless $TESTMODE;

	# ONLY NEED TO SAVE TESTMODE RESULTS HERE (After the next) - IF ITS NOT TESTMODE WE DONT USE THEM
	if( $OUTPUT{$host} eq "test_completed" ) {
		$SSH_OK{$host} = "YES";
		print "[$host $method] $host SSH ***OK***",$NL;
		$OUTPUT{$host}="";
	} else {
		$SSH_OK{$host} = "NO";
		$SSH_REASON{$host} = join("",split(/\n/,$OUTPUT{$host})) unless $SSH_REASON{$host};
		$SSH_COMMAND{$host} = rsh_lastcommand();
		print "[$host $method] COMMAND = ",rsh_lastcommand(),"\n";
		print "[$host $method] SSH OUTPUT is ".join("",split(/\n/,$OUTPUT{$host}))."\n";
	}

	$method="RSH";
	$OUTPUT{$host}="";

   eval {
		alarm($TIMEOUT) if $TIMEOUT;
		do_host( $host, $login, $password, $method, $ACTION, $COMMAND,$DEBUG );
		alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedoutPG/ ) {
			$RSH_OK{$host} = "NO";
			$OUTPUT{$host} = $RSH_REASON{$host} = "$method Command Timed Out";
			print "$method Command Timed Out to $host".$NL if $DEBUG;
		} else {
			if( $@ !~ /^\s*$/ ) {
				$RSH_OK{$host} = "NO";
				print "$method Command Returned $@ from $host".$NL if $DEBUG;
				$RSH_REASON{$host} = $FS."$method Command Returned $@".$FE;
			} else {
				print "DBG DBG ODDITY $@";
			}
			alarm(0);
		}
	}
	alarm(0);

	# SAVE TESTMODE RESULTS FOR THE RSH RUN
	if( $OUTPUT{$host} eq "test_completed" ) {
		$RSH_OK{$host} = "YES";
		print "[$host $method] $host RSH ***OK***",$NL;
		$OUTPUT{$host}="";
	} else {
		$RSH_OK{$host} = "NO";
		$RSH_REASON{$host} = join("",split(/\n/,$OUTPUT{$host})) unless $RSH_REASON{$host};
		print "[$host $method] RSH OUTPUT is ".join("",split(/\n/,$OUTPUT{$host}))."\n";
		$RSH_COMMAND{$host} = rsh_lastcommand();
		print "[$host $method] COMMAND = ",rsh_lastcommand(),"\n";
	}
	print "[$host] FTP=$Ok RSH=$RSH_OK{$host} SSH=$SSH_OK{$host}\n" unless $SILENTMODE;
}


if( $TESTMODE ) {
	my(@alerts);
	my(%recommendedupgrades);
	my(%recommendeddowngrades);
	my($rptstr)="";
	if( $HTML ) {
		my($HEADER_COLOR)="brown";
		$rptstr .=  "<TABLE BORDER=1>";
		$rptstr .=  "<TR BGCOLOR=$HEADER_COLOR><TH COLSPAN=7>CONNECTIVITY TEST RESULTS</TH></TR>\n";
		$rptstr .=  "<TR BGCOLOR=$HEADER_COLOR><TH>HOSTNAME</TH><TH>FTP OK</TH><TH>SSH OK</TH><TH>RSH OK</TH><TH>DFLT METHOD</TH><TH>LOGIN</TH><TH>ERRORS</TH></TR>\n";
		foreach my $host (@hosts) {
			my($CURRENT_COLOR)=("beige");
			$SSH_OK{$host}=$FS."NO".$FE if $HTML and $SSH_OK{$host} eq "NO";
			$RSH_OK{$host}=$FS."NO".$FE if $HTML and $RSH_OK{$host} eq "NO";
			$FTP_OK{$host}=$FS."NO".$FE if $HTML and $FTP_OK{$host} eq "NO";
			my($d_color,$e_color);
			$d_color="<FONT COLOR=RED><b>" if $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} ne "YES";
			$d_color="<FONT COLOR=RED><b>" if $DEFAULT_METHOD{$host} eq "RSH" and $RSH_OK{$host} ne "YES";
			$d_color="<FONT COLOR=RED><b>" if $DEFAULT_METHOD{$host} eq "FTP" and $FTP_OK{$host} ne "YES";
			$e_color="</b></FONT" if $d_color;
			$CURRENT_COLOR="PINK" if $d_color;
			$rptstr .= sprintf( "<TR BGCOLOR=$CURRENT_COLOR><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>",
					$FB.$host.$FE,
					$FTP_OK{$host},
					$SSH_OK{$host},
					$RSH_OK{$host},
					$d_color.$DEFAULT_METHOD{$host}.$e_color,
					$LOGIN_USED{$host});
			$rptstr .=  $FS."\tFTP_REASON=$FTP_REASON{$host} ".$FE.$NL if $FTP_REASON{$host} and $FTP_OK{$host} ne "YES";
			$rptstr .=  $FS."\tSSH_COMMAND=$SSH_COMMAND{$host} ".$FE.$NL if $SSH_COMMAND{$host} and $SSH_OK{$host} ne "YES";
			$rptstr .=  $FS."\tSSH_REASON=$SSH_REASON{$host} ".$FE.$NL if $SSH_REASON{$host} and $SSH_OK{$host} ne "YES";
			$rptstr .=  $FS."\tRSH_COMMAND=$RSH_COMMAND{$host} ".$FE.$NL if $RSH_COMMAND{$host} and $RSH_OK{$host} ne "YES";
			$rptstr .=  $FS."\tRSH_REASON=$RSH_REASON{$host} ".$FE.$NL if $RSH_REASON{$host} and $RSH_OK{$host} ne "YES";
			$rptstr =~ s/<br>\n$//;
			$rptstr =~ s/\n$//;
			$rptstr .=  "&nbsp;</TD></TR>\n";

			my($upgrdstr);

			$recommendedupgrades{$host}="FSH" if $DEFAULT_METHOD{$host} eq "NONE" and $FTP_OK{$host} eq "YES";
			$recommendedupgrades{$host}="RSH" if $DEFAULT_METHOD{$host} eq "NONE" and $RSH_OK{$host} eq "YES";
			$recommendedupgrades{$host}="RSH" if $DEFAULT_METHOD{$host} eq "FTP" and $RSH_OK{$host} eq "YES";
			$recommendedupgrades{$host}="SSH" if $DEFAULT_METHOD{$host} ne "SSH" and $SSH_OK{$host} eq "YES";

			$recommendeddowngrades{$host}="FTP" if $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} eq "NO" and $FTP_OK{$host} eq "YES";
			$recommendeddowngrades{$host}="FTP" if $DEFAULT_METHOD{$host} eq "RSH" and $RSH_OK{$host} eq "NO" and $FTP_OK{$host} eq "YES";
			$recommendeddowngrades{$host}="RSH" if $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} eq "NO" and $RSH_OK{$host} eq "YES";

			$rptstr .= $host. " should be upgraded to ".$recommendedupgrades{$host}.$NL if $recommendedupgrades{$host};
			$rptstr .= $host. " should be down graded to ".$recommendeddowngrades{$host}.$NL if $recommendeddowngrades{$host};

			my($username)=$ENV{USER} || $ENV{USERNAME};
			$username="USER=".$username if $username;

			if( $d_color ) {
				my($reason);
				$reason = $SSH_REASON{$host}." ".$SSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "SSH";
				$reason = $RSH_REASON{$host}." ".$RSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "RSH";
				$reason = $FTP_REASON{$host} if $DEFAULT_METHOD{$host} eq "FTP";

				$rptstr .=  $FS."DEFAULT CONNECTION TO $host FAILED - $reason".$FE.$NL;
				push @alerts, $FS."ALERT: DEFAULT CONNECTION $DEFAULT_METHOD{$host} TO $host FAILED - $reason".$FE.$NL;
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "ERROR",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host FAILED from $curhostname $username - $reason" )
								if $BATCHID ne "test_unix_connectivity.pl";
			} else {
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "OK",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host SUCCEEDED from $curhostname $username" )
								if $BATCHID ne "test_unix_connectivity.pl";
   		}
	#		foreach (split(/\n/,$OUTPUT{$host})) {
	#			next if /^\s*$/;
	#			printf "%".$hlen."s ",$host unless $HTML;
	#			print $_,$NL;
	#		}
		}
		$rptstr .=  "</TABLE>";

	} else {
		$rptstr .=  "--- PARSABLE TEST MODE OUTPUT ---".$NL unless $TESTMODERPT;
		foreach my $host (@hosts) {
			$rptstr .= sprintf( "HOST %-16s FTP_OK=%-3.3s SSH_OK=%-3.3s RSH_OK=%-3.3s METHOD=%s LOGIN=%s",
				$host, $FTP_OK{$host}, $SSH_OK{$host}, $RSH_OK{$host}, $DEFAULT_METHOD{$host}, $LOGIN_USED{$host});
			$rptstr .=  $NL."\tFTP_REASON=$FTP_REASON{$host} " if $FTP_REASON{$host} and $FTP_OK{$host} ne "YES";
			$rptstr .=  $NL."\tSSH_REASON=$SSH_REASON{$host} " if $SSH_REASON{$host} and $SSH_OK{$host} ne "YES";
			$rptstr .=  $NL."\tRSH_REASON=$RSH_REASON{$host} " if $RSH_REASON{$host} and $RSH_OK{$host} ne "YES";

			if (( $DEFAULT_METHOD{$host} eq "SSH" and $SSH_OK{$host} ne "YES" )
			or (  $DEFAULT_METHOD{$host} eq "RSH" and $RSH_OK{$host} ne "YES" )
			or (  $DEFAULT_METHOD{$host} eq "FTP" and $FTP_OK{$host} ne "YES" )){
				my($reason);
				$reason = $SSH_REASON{$host}." ".$SSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "SSH";
				$reason = $RSH_REASON{$host}." ".$RSH_COMMAND{$host} if $DEFAULT_METHOD{$host} eq "RSH";
				$reason = $FTP_REASON{$host} if $DEFAULT_METHOD{$host} eq "FTP";

				$rptstr .=  $NL."DEFAULT CONNECTION TO $host FAILED - $reason";
				push @alerts, "ALERT: DEFAULT CONNECTION $DEFAULT_METHOD{$host} TO $host FAILED - $reason".$NL;
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "ERROR",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host FAILED from $curhostname - $reason" )
								if $BATCHID ne "test_unix_connectivity.pl";
			} else {
				MlpHeartbeat(
							-monitor_program => $BATCHID,
							-system=> $host,
							-subsystem=> "GEM Host Connect",
							-state=> "OK",
							-message_text=> "GEM_012: $DEFAULT_METHOD{$host} TO $host SUCCEEDED from $curhostname" )
								if $BATCHID ne "test_unix_connectivity.pl";
   		}
			$rptstr .=  $NL;
		}
		$rptstr .=  "--- DONE PARSABLE TEST MODE OUTPUT ---".$NL unless $TESTMODERPT;
	}
	if( $TESTMODERPT ) {
		open(RPT,"> $TESTMODERPT") or die "Cant write to $TESTMODERPT $! \n";
		foreach ( sort keys 	%recommendeddowngrades ) {
			print RPT $_. " should be downgraded to ".$recommendeddowngrades{$_}.$NL;
		}
		foreach ( sort keys 	%recommendedupgrades ) {
			print RPT $_. " should be upgraded to ".$recommendedupgrades{$_}.$NL;
		}

		print RPT $rptstr;
		#print RPT @alerts;
		close RPT;
	} else {
		print $rptstr;
		foreach ( sort keys 	%recommendeddowngrades ) {
			print RPT $_. " should be downgraded to ".$recommendeddowngrades{$_}.$NL;
		}
		foreach ( sort keys 	%recommendedupgrades ) {
			print RPT $_. " should be upgraded to ".$recommendedupgrades{$_}.$NL;
		}
		print @alerts;
	}
	if( $TESTMODEUPDATE ) {
		#we can update everything in the config file!
		print "UPDATING CONFIGURATION FILES AS TESTMODEUPDATE SET\n";
		my(%hashkeys);
		foreach ( sort keys 	%recommendeddowngrades ) {
			$hashkeys{$_."|UNIX_COMM_METHOD"}=$recommendeddowngrades{$_};
		}
		foreach ( sort keys 	%recommendedupgrades ) {
			$hashkeys{$_."|UNIX_COMM_METHOD"}=$recommendedupgrades{$_};
		}
		add_edit_item(-type=>'unix',-op=>'update',-values=>\%hashkeys);	#,-debug=>1);
		print "Completed\n";
	}
	exit(0);
}


sub do_host {
	my($host,$login,$password,$method,$ACTION,$COMMAND,$DEBUG_DOHOST)=@_;
	$hlen=length $host if length $host > $hlen;
	$OUTPUT{$host} = "";
	print __LINE__," do_host(Host=$host Action=$ACTION Login=$login Method=$method Cmd=$COMMAND)".$NL if $DEBUG_DOHOST;

	my $cmd;

	$TIMEOUT=120 unless $TIMEOUT;
	if( $method eq "FTP" ) {
			$OUTPUT{$host} = $FS."host $host: method=FTP - skipping".$FE;
			push @warnings, "host $host: method=FTP - skipping";
			return;
	}

	if( $method eq "NONE" ) {
			$OUTPUT{$host} = $FS."host $host: method=NONE - skipping".$FE;
			push @warnings, "host $host: method=NONE - skipping";
			return;
	}

	die usage("Must Pass COMMAND if ACTION=RUN") 	unless $COMMAND;
	do_rsh_method($method);
	my($dat)= do_rsh(-hostname=>$host, -command=>$COMMAND, -debug=>$DEBUG_DOHOST, -noexec=>$NOEXEC,
			-login=>$login, -password=>$password);
	print __LINE__," do_rsh(Host=$host Login=$login Cmd=$COMMAND) returned $dat".$NL if $DEBUG_DOHOST;

	if( $dat=~/^failure/ ) {
		if ( $dat =~ /connection timed out/ ) {
			# not much you can do here
			print "host $host connection timed out".$NL;
			$OUTPUT{$host} =$FS."host $host connection timed out".$FE;
			return;
		} elsif ( $dat =~ /not found/ ) {
			print "host $host: command $COMMAND returned $dat".$NL;
			$OUTPUT{$host} =$FS."host $host: command $COMMAND returned $dat".$FE;
			return;
		}
	}
	$OUTPUT{$host} = $dat;
}

sub red {
	if( $HTML ) {
		return "<FONT COLOR=RED>".join("",@_)."</FONT>";
	} else {
		return join("",@_);
	}
}

__END__

=head1 NAME

pugtet_file.pl - unix file manager

=head2 SYNOPSIS

Allows you to do a bunch of things on all your unix systems.  This should supersceed all the other files in
the rdist directory.

=head2 EXAMPLE

 perl test_unix_connectivity.pl --DEBUG -ACTION=RUN -COMMAND=id

will Run the unix command "id" on all hosts that are in your unix_passwords.dat file and
have COMM_METHOD = SSH or RSH

   perl test_unix_connectivity.pl -ACTION=PUT --SOURCE_FILE=authorized_keys2 --TARGET_FILE=authorized_keys --TARGET_DIR=.ssh
    perl test_unix_connectivity.pl -ACTION=RUN -COMMAND="chmod 700 .ssh"

Example to collect all .rhosts files from systems home directories for future use (stored in data/system_information_data)

   perl test_unix_connectivity.pl -ACTION=STORE --SOURCE_FILE=.rhosts --TARGET_FILE=rhosts --RUNMETHOD=rsh

=head2 TEST PLAN

Test plan for full backwards compatibility

a)    put a file onto all systems   - using each of the methods

      perl test_unix_connectivity.pl -ACTION=RENAME --RUNMETHOD=ssh --SOURCE_FILE=delete_me --TARGET_FILE=delete_me

   see that the file is there

      perl test_unix_connectivity.pl -ACTION=LS --RUNMETHOD=ssh --TARGET_FILE=delete_me

pwd
cwd /
pwd home
file list

	perl test_unix_connectivity.pl -ACTION=LS --RUNMETHOD=ssh --TARGET_FILE=delete_me

fspec list

rename file

      perl test_unix_connectivity.pl -ACTION=RENAME --RUNMETHOD=ssh --SOURCE_FILE=delete_me --TARGET_FILE=delete_me2

remove file
   mkdir
   rmdi

perl test_unix_connectivity.pl --TARGET_DIR=/export/home --ACTION=LS --DEBUG

=head2 HOW TO TEST CONNECTIVITY

This program is also a connectivity tester.  Run

test_unix_connectivity.pl --TESTMODE --TESTMODRPT=file.html --HTML
or
test_unix_connectivity.pl --TESTMODE --TESTMODRPT=file.html --HTML --TESTMODEUPDATE
  to actually update the config files

