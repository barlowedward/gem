#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use      strict;
use      Carp;

die "Must have save directory available\n" unless -d "save";
die "save directory not writable\n" unless -w "save";
use File::Copy;

foreach (@ARGV) {
	print "Copying File $_\n";
	my($date)=fmt_time(-fmt=>"yymmdd");
	print "copy $_,save/$_.$date\n";
	copy($_,"save/".$_.".".$date) or die "cant copy $_ to save/$_.$date\n";
}

sub fmt_time
{
   my(%OPT)=@_;
	croak("Must Pass -fmt argument to wpg_time")
		unless defined $OPT{-fmt};

	my($now)=$OPT{-time};
   $now=time unless defined $now;

   my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($now);

   $mon++;
   $year+=1900;
   substr($mon,0,0)='0' 	if length($mon)==1;
   substr($mday,0,0)='0' 	if length($mday)==1;
   substr($hour,0,0)='0' 	if length($hour)==1;
   substr($min,0,0)='0' 	if length($min)==1;
   substr($sec,0,0)='0' 	if length($sec)==1;

	my($rc)=$OPT{-fmt};
	$rc =~ s/yyyy/$year/g;
	substr($year,0,2)="";
	$rc =~ s/yy/$year/g;
	$rc =~ s/mm/$mon/g;
	$rc =~ s/dd/$mday/g;
	$rc =~ s/hh/$hour/g;
	$rc =~ s/mi/$min/g;
	$rc =~ s/dw/$wday/g;
	$rc =~ s/ss/$sec/g;

   return $rc;
}

__END__

=head1 NAME

readate.pl - archiver - tacks dates on to files

=head2 DESCRIPTION

copies all files passed on command line to save/filename.yymmdd

=head2 USAGE

        ./redate.pl file1 file2 file3 file4

=cut
