#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#GENERIC REPOSITORY RUN

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use Do_Time;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- (native authentication) \n".
$PROGRAM_NAME." -DATABASE=db  -SERVER=SERVER
-or- \n".
basename($0)." -DOALL=sybase
-or- \n".
basename($0)." -DOALL=sqlsvr

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"			=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

$DOALL="sqlsvr" unless $DOALL or $SERVER; 	# just in this case

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;
my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL ,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);
print_timings();
exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

my(%time_query,%time_processing);

sub print_timings {
	foreach ( sort keys %time_query ) {
		printf "%20s %8s %8s\n", $_,$time_query{$_},$time_processing{$_};
	}
	printf "%20s %6s minutes\n", 'TOTAL:',int((time-$STARTTIME)/60);
}
###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;
	my($t1)=time;

	#
	# Get Database Type & Level
	#
	my( %V_BACKUP_TIME, %V_BACKUP_FILE, %V_BACKUP_LSN, %V_TRANDUMP_TIME, %V_TRANDUMP_FILE, %V_INTERNAL_SYSTEM_NAME,
		%V_TRANDUMP_LSN, %V_RESTORE_TIME, %V_RESTORE_FILE, %V_RESTORE_LSN, %V_TRANLOAD_TIME, %V_TRANLOAD_SVR, %V_RESTORE_SVR,
		%V_TRANLOAD_FILE, %V_TRANLOAD_FILETIME, %V_TRANLOAD_LSN, %V_LOGTRUNCATED, $primary_svr, $secondary_svr );

	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	if( $DBTYPE eq "SQL SERVER" ) {
		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";
		my( %dblist );

#		my($q)=" if exists ( select name from master..sysobjects where name='sp_help_log_shipping_monitor' )
#begin
#	exec sp_help_log_shipping_monitor
#end";
#		print "(noexec)" if $NOEXEC and $DEBUG;
#		print $q,"\n" if $DEBUG;
#		my(@lastresults) = dbi_query( -db=>"master", -query=>$q );
#		foreach (@lastresults) {
#			my($status,$is_primary,$server,$dbname,$time_since_last_backup,$lst_backup_file,
#			$backup_threshold,$is_backup_alert_enabled,$tiem_sinc_last_copy,$last_copied_file,
#			$time_since_last_restore,$last_restored_file,$last_restored_latency,$restore_threshold,$is_restore_alert_enabled,
#			$junk) = dbi_decode_row($_);
#
#			if( $status == 0 or $status == 1 ) {
#				$dblist{$dbname}=1;
#				if( $is_primary ) {
#					$primary_svr = $server unless $primary_svr or ! $server;
#					next unless $lst_backup_file;
#					$V_TRANDUMP_TIME{$dbname} = time - $time_since_last_backup*60;
#					$V_TRANDUMP_FILE{$dbname} = $lst_backup_file;
#				} else {
#					$secondary_svr = $server unless $secondary_svr or ! $server;
#					next unless $last_restored_file;
#					$V_TRANLOAD_TIME{$dbname} = time - $time_since_last_restore*60;
#					$V_TRANLOAD_FILE{$dbname} = $last_restored_file;
#				}
#			} else {
#				print "PROC ERROR: status=$status\n",join(" ",dbi_decode_row($_)),"\n";;
#			}
#		}

# 	get backup info
#
#		-- Backup of data
#SELECT database_name,
#    CONVERT( SmallDateTime , MAX(backup_finish_date)) as Last_Backup,
#    DATEDIFF(d, MAX(backup_finish_date), Getdate()) as Days_Since_Last
#FROM msdb.dbo.backupset
#WHERE type = 'D'
#GROUP BY database_name
#ORDER BY 3 DESC
#Go
#-- Find the backup of Transaction Log files
#SELECT database_name,
#    CONVERT( SmallDateTime , MAX(backup_finish_date)) as Last_Backup,
#    DATEDIFF(d, MAX(backup_finish_date), Getdate()) as Days_Since_Last
#FROM msdb.dbo.backupset
#WHERE type = 'L'
#GROUP BY database_name
#ORDER BY 3 DESC
#
#		$q = "
#SELECT
#   msdb..backupset.type,
#   msdb.dbo.backupset.database_name,
#   msdb.dbo.backupset.backup_start_date,
#   msdb.dbo.backupset.backup_finish_date,
#   msdb.dbo.backupset.backup_size,
#   msdb.dbo.backupmediafamily.physical_device_name,
#   msdb.dbo.backupset.checkpoint_lsn
#FROM   msdb.dbo.backupmediafamily
#   INNER JOIN msdb.dbo.backupset ON msdb.dbo.backupmediafamily.media_set_id = msdb.dbo.backupset.media_set_id
#WHERE  (CONVERT(datetime, msdb.dbo.backupset.backup_start_date, 102) >= GETDATE() - 7)
#-- and    msdb..backupset.type = 'D'
#ORDER BY
#   msdb.dbo.backupset.database_name,
#   msdb.dbo.backupset.backup_finish_date";

# Latest Query To Get Backup Info
my($q)="
SELECT database_name, type,
    MAX(backup_finish_date) as Last_Backup,
    media_set_id = convert(int,null),
    server_name, checkpoint_lsn=convert(numeric(25,0),null)
into #LatestBackups
FROM msdb.dbo.backupset
GROUP BY database_name, type, server_name

update #LatestBackups
set		media_set_id= b.media_set_id, checkpoint_lsn=b.checkpoint_lsn
from	#LatestBackups m, msdb.dbo.backupset b
where	m.database_name=b.database_name
and		m.Last_Backup = b.backup_finish_date
and		m.type = b.type

select SERVERPROPERTY('Servername'),b.database_name,type,Last_Backup, physical_device_name, server_name, checkpoint_lsn
from #LatestBackups b,  msdb.dbo.backupmediafamily f
where b.media_set_id=f.media_set_id
and db_id(b.database_name) is not null
and type in ('D','L')
order by 2,3,4 desc

drop table #LatestBackups
";

   	my(@lastresults) = dbi_query( -db=>"master", -query=>$q );
   	$time_query{$cursvr} = time-$t1;
   	my($typold)='?';
   	my($dbold)='?';
		foreach (@lastresults) {
			my($sname, $db,$type,$bsd,$pdn,$src_sname,$lsn) = dbi_decode_row($_);
			$V_INTERNAL_SYSTEM_NAME{$db}=$sname;
			# create a database list for this sever
			$dblist{$db}=1;

			# second / older backup file - maybe an earlier load from another system
			# we only care about the latest
			next if $type eq $typold and $db eq $dbold;
			$typold = $type;
			$dbold  = $db;

			if( $sname eq $src_sname ) {	# its a backup
				if( $type =~ /^L/i ) {		# logdump
					die "WOAH"  if $V_TRANDUMP_TIME{$db};
					$V_TRANDUMP_TIME{$db} = $bsd;
					$V_TRANDUMP_FILE{$db} = $pdn;
					$V_TRANDUMP_LSN{$db}  = $lsn;
				}elsif( $type =~ /^D/i ) {	# dbdump
					die "WOAH"  if $V_BACKUP_TIME{$db};
					$V_BACKUP_TIME{$db} = $bsd;
					$V_BACKUP_FILE{$db} = $pdn;
					$V_BACKUP_LSN{$db}  = $lsn;
				} else {
					print "BKP ERROR: type=$type\n",join(" ",dbi_decode_row($_)),"\n";;
				}
			} else {	# its a restore
				if( $type =~ /^L/i ) {
					next if $V_TRANDUMP_TIME{$db};
					$V_TRANLOAD_TIME{$db} = $bsd;
					$V_TRANLOAD_FILE{$db} = $pdn;
					$V_TRANLOAD_LSN{$db}  = $lsn;
					$V_TRANLOAD_SVR{$db}  = $src_sname;
				}elsif( $type =~ /^D/i ) {
					next if $V_BACKUP_TIME{$db};		# what if you loaded from more than 1 system
					$V_RESTORE_TIME{$db} = $bsd;
					$V_RESTORE_FILE{$db} = $pdn;
					$V_RESTORE_LSN{$db}  = $lsn;
					$V_RESTORE_SVR{$db}   = $src_sname;
				} else {
					print "RESTORE ERROR: type=$type\n",join(" ",dbi_decode_row($_)),"\n";;
				}
			}
		}


#$q="SELECT
# CASE WHEN rsh.restore_type = 'D' THEN 'Database'
#  WHEN rsh.restore_type = 'F' THEN 'File'
#  WHEN rsh.restore_type = 'G' THEN 'Filegroup'
#  WHEN rsh.restore_type = 'I' THEN 'Differential'
#  WHEN rsh.restore_type = 'L' THEN 'Log'
#  WHEN rsh.restore_type = 'V' THEN 'Verifyonly'
#  WHEN rsh.restore_type = 'R' THEN 'Revert'
#  ELSE rsh.restore_type
# END AS [Restore Type],
# rsh.destination_database_name AS [Database],
# rsh.restore_date AS [Restore Date],
# bmf.physical_device_name AS [Restored From],
# checkpoint_lsn
#FROM msdb.dbo.restorehistory rsh
# INNER JOIN msdb.dbo.backupset bs ON rsh.backup_set_id = bs.backup_set_id
# INNER JOIN msdb.dbo.restorefile rf ON rsh.restore_history_id = rf.restore_history_id
# INNER JOIN msdb.dbo.backupmediafamily bmf ON bmf.media_set_id = bs.media_set_id
#WHERE rsh.restore_date >= DATEADD(dd, -30, GETDATE()) --want to search for previous days
#ORDER BY rsh.restore_history_id DESC";
#
#		@lastresults = dbi_query( -db=>"master", -query=>$q );
#		foreach (@lastresults) {
#			my($type,$db,$rd,$file,$lsn) = dbi_decode_row($_);
#			$dblist{$db}=1;
#			if( $type =~ /^Log/i ) {
#				next if $V_TRANDUMP_TIME{$db};
#				$V_TRANLOAD_TIME{$db} = $rd;
#				$V_TRANLOAD_FILE{$db} = $file;
#				$V_TRANLOAD_LSN{$db}  = $lsn;
#			}elsif( $type =~ /^Database/i or $type =~ /^Differential/ or $type =~ /^File/ ) {
#				next if $V_BACKUP_TIME{$db};
#				$V_RESTORE_TIME{$db} = $rd;
#				$V_RESTORE_FILE{$db} = $file;
#				$V_RESTORE_LSN{$db}  = $lsn;
#			} else {
#				print "RESTORE ERROR: type=$type\n",join(" ",dbi_decode_row($_)),"\n";;
#			}
#		}

#		use Data::Dumper;
#		print $primary_svr,"\n";
#		print Dumper \%V_TRANDUMP_TIME;
#		print Dumper \%V_TRANDUMP_FILE;
#		print $secondary_svr,"\n";
#		print Dumper \%V_TRANLOAD_TIME;
#		print Dumper \%V_TRANLOAD_FILE;

		foreach my $db ( sort keys %dblist ) {
			next if $db eq "model" or $db eq "tempdb";
			print "Saving BackupState for $cursvr / $db \n";

 print "   -last_tranload_int_sysnm     => $V_TRANLOAD_SVR{$db} \n"    if $V_TRANLOAD_SVR{$db};
 print "   -last_fullload_int_sysnm     => $V_RESTORE_SVR{$db} \n"    if $V_RESTORE_SVR{$db};
 print "   -internal_system_name     => $V_INTERNAL_SYSTEM_NAME{$db} \n"    if $V_INTERNAL_SYSTEM_NAME{$db};
			print "   -last_fulldump_time     => $V_BACKUP_TIME{$db} \n"    if $V_BACKUP_TIME{$db};
         print "   -last_fulldump_file     => $V_BACKUP_FILE{$db} \n"    if $V_BACKUP_FILE{$db};
 			print "   -last_fulldump_time     => $V_BACKUP_TIME{$db} \n"    if $V_BACKUP_TIME{$db};
         print "   -last_fulldump_file     => $V_BACKUP_FILE{$db} \n"    if $V_BACKUP_FILE{$db};
         print "   -last_fulldump_lsn      => $V_BACKUP_LSN{$db} \n"       if $V_BACKUP_LSN{$db};
         print "   -last_trandump_time     => $V_TRANDUMP_TIME{$db} \n"    if $V_TRANDUMP_TIME{$db};
         print "   -last_trandump_file     => $V_TRANDUMP_FILE{$db} \n"    if $V_TRANDUMP_FILE{$db};
         print "   -last_trandump_lsn      => $V_TRANDUMP_LSN{$db} \n"    if $V_TRANDUMP_LSN{$db};
         print "   -last_fullload_time     => $V_RESTORE_TIME{$db} \n"    if $V_RESTORE_TIME{$db};
         print "   -last_fullload_file     => $V_RESTORE_FILE{$db} \n"    if $V_RESTORE_FILE{$db};
         print "   -last_fullload_lsn      => $V_RESTORE_LSN{$db} \n"    if $V_RESTORE_LSN{$db};
         print "   -last_tranload_time     => $V_TRANLOAD_TIME{$db} \n"    if $V_TRANLOAD_TIME{$db};
         print "   -last_tranload_file     => $V_TRANLOAD_FILE{$db} \n"    if $V_TRANLOAD_FILE{$db};
         print "   -last_tranload_lsn      => $V_TRANLOAD_LSN{$db} \n"    if $V_TRANLOAD_LSN{$db};
         print "   -last_tranload_filetime => $V_TRANLOAD_FILETIME{$db} \n" if $V_TRANLOAD_FILETIME{$db};
         print "   -last_truncation_time   => $V_LOGTRUNCATED{$db} \n"    if $V_LOGTRUNCATED{$db};

			MlpSetBackupState(
				-system					=>	$cursvr,
				-internal_system_name=> $V_INTERNAL_SYSTEM_NAME{$db},
				-dbname					=>	$db,
				-debug					=> $DEBUG,
				-last_fulldump_time 	=> $V_BACKUP_TIME{$db},
				-last_fulldump_file 	=> $V_BACKUP_FILE{$db},
				-last_fulldump_lsn 	=> $V_BACKUP_LSN{$db},

				-last_trandump_time 	=> $V_TRANDUMP_TIME{$db},
				-last_trandump_file 	=> $V_TRANDUMP_FILE{$db},
				-last_trandump_lsn 	=> $V_TRANDUMP_LSN{$db},

				-last_fullload_time 	=> $V_RESTORE_TIME{$db},
				-last_fullload_file 	=> $V_RESTORE_FILE{$db},
				-last_fullload_lsn 	=> $V_RESTORE_LSN{$db},

				-last_tranload_time 	=> $V_TRANLOAD_TIME{$db},
				-last_tranload_file 	=> $V_TRANLOAD_FILE{$db},
				-last_tranload_lsn 	=> $V_TRANLOAD_LSN{$db},
				-last_tranload_filetime => $V_TRANLOAD_FILETIME{$db},
				-last_fullload_int_sysnm => $V_RESTORE_SVR{$db},
				-last_tranload_int_sysnm => $V_TRANLOAD_SVR{$db},
				-last_truncation_time => $V_LOGTRUNCATED{$db},
				# -file_time => do_time( -fmt => "yyyy/mm/dd hh:mi:ss")
			);
		}

		$time_processing{$cursvr} = time-$t1-$time_query{$cursvr};
		# printf "%20s %8s %8s\n", $cursvr,$time_query{$cursvr},$time_processing{$cursvr};

		return;
	} else {
		die "THis program can only run on sql servers\n";
		# print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";

		# get current locked/expired status from the repository
		#my($q)="select credential_name, attribute_key, attribute_value
		#	print "(noexec)" if $NOEXEC and $DEBUG;
		#	print $q,"\n" if $DEBUG;
		#	MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
		#  my(@lastresults)=MlpAlarm::_querywithresults($q,1)  unless $NOEXEC
		#  foreach (@lastresults) {
		#		my(@vals) = dbi_decode_row($_);

	}
}


__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

