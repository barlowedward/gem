# rewritten by ed barlow based on the below code
#
#-----------------------------------------------------------------------------------#
#  Show_Service.pl        displays information about services on a remote machine   #
#                         by Thomas Berger: http://members.aol.com/bergert          #
#                                                                                   #
#  Ver 1.00  23-Jan-2004  first release                                             #
#  Ver 1.01  09-Jul-2004  fixed check for "\\" in server name                       #
#                         added startup = 5 (check for no registry key)             #
#                         added state   = 8 (undefined)                             #
#                         added ServiceType; have yet to find what it means         #
#                                                                                   #
#-----------------------------------------------------------------------------------#

use Win32::Service;
use Win32::Registry;
use Repository;

my($HTML)=1;

my ($key, %service);
my %status;

%state = (
            0 => 'unknown',
            1 => 'stopped',
            2 => 'starting',
            3 => 'stopping',
            4 => 'running',
            5 => 'resuming',
            6 => 'pausing',
            7 => 'paused',
            8 => 'undefined',  # used only by Show_Service.pl
            );
%startup = (
            0 => 'unknown',
            1 => 'kernel driver',
            2 => 'automatic startup',
            3 => 'manual startup',
            4 => 'startup disabled',
            5 => 'no registry key',   # used only by Show_Service.pl
            );
%type = (
            0   => 'unknown',
            16  => 'unknown',
            32  => 'unknown',
            272 => 'unknown',
            288 => 'unknown',
            );


#if (@ARGV != 1) {
#   $server = Win32::NodeName();
#}
#else {
#   $server = $ARGV[0];
#   if (substr($server,0,2) eq "\\\\") {
#      # strip off backslash
#      $server = substr($server,2,);
#   }
#}

print "<h1>" if $HTML;
print "Win32 Service Report - showservice.pl \n";
print "</h1>" if $HTML;
print "Run at ".localtime(time)."\n";
print "<p>" if $HTML;
my($COLOR1,$COLOR2, $CURRENT_COLOR)=("beige","#FFCCCC","beige");
my($HEADER_COLOR)="brown";

my(%servicekeys);
my(%currentstate, %currentstartup, %servicetype, %rowcolor);  # key = host:key
my(@serverlist)=get_password(-type=>'win32servers');
foreach my $server ( @serverlist ) {
	print "connecting to machine [$server]\n" unless $HTML;

	undef %services;
	#
	# connect to service manager and enumerate services (long name, short name)
	#
	Win32::Service::GetServices('\\\\'.$server,\%services);

	foreach $key (sort keys %services) {
		next unless $key =~ /^ORA/i or $key =~/SQL/i or $key=~/Sybase/i or $key=~/Backup/i;
		#
	   # connect to service manager and get service status
	   #
	   Win32::Service::GetStatus( '',$services{$key}, \%status);
	   if (!defined($status{CurrentState})) {
	      $status{CurrentState} = 8;
	   }
	   if (!defined($status{ServiceType})) {
	      $status{ServiceType} = 0;
	   }

	    #
	   # connect to registry and get service startup mode
	   #
	   $HKEY_LOCAL_MACHINE->Win32::Registry::Connect($server,$Remote_Registry_HKLM);
	   if (!$Remote_Registry_HKLM) {
	      print "Unable to Connect to HKLM on $server\n";
	      next;
	   }
	   $Remote_Registry_HKLM->Open('System\\CurrentControlSet\\Services\\'.$services{$key},$Reg_Service);
	   if (!$Reg_Service) {
	     #die "Unable to Connect to HKLM...Services\\$services{$key} on $server\n";
	     $value = 5;
	   }
	   else {
	      $Reg_Service->QueryValueEx('Start',$type,$value);
	      $Reg_Service->Close;
	   }
	   $Remote_Registry_HKLM->Close;

		my($shortsvcname)=lc($key);
		$shortsvcname="Sybase" if $shortsvcname=~/^sybase/;
		$shortsvcname="Oracle" if $shortsvcname=~/^oracle/;
		$shortsvcname="Sql Server" if $shortsvcname=~/^sqlserveragent$/ or  $shortsvcname=~/^mssqlserver/ or $shortsvcname=~/^sql server /;

		if( $shortsvcname ne lc($key) ) {
			$servicetype{$key}=$shortsvcname;
		}

		$servicekeys{$key}=$services{$key};
		$realsvcname{$server.":".$key} = $key;
		$currentstate{$server.":".$key} = $state{$status{CurrentState}};
		$currentstartup{$server.":".$key} =$startup{$value};
		$rowcolor{$server.":".$key} = "BGCOLOR=PINK" if $state{$status{CurrentState}} eq "running" and $startup{$value} eq "startup disabled";
		$rowcolor{$server.":".$key} = "BGCOLOR=LIGHTBLUE" if $state{$status{CurrentState}} eq "running" and $startup{$value} eq "manual startup";

	   # print "$server:   $key ($services{$key}), $state{$status{CurrentState}}, $startup{$value}, $status{ServiceType}\n";
	}
}

# PRINT THE TYPED SERVICES
print "<TABLE BORDER=1>\n" if $HTML;
my($header)="<TR BGCOLOR=$HEADER_COLOR><TH COLSPAN=6>DATABASE SERVICES ON YOUR WINDOWS SERVERS</TH></TR>\n";
my($headerx)="<TR bgcolor=$HEADER_COLOR><TH>HOSTNAME</TH><TH>SERVICE TYPE</TH><TH>SERVICE</TH><TH>DESCRIPTION</TH><TH>CURRENT STATE</TH><TH>STARTUP</TH></TR>\n";

foreach my $svctype ( "Sybase","Oracle","Sql Server") {
	if( $CURRENT_COLOR eq $COLOR1 ) {
      $CURRENT_COLOR = $COLOR2;
   } else {
      $CURRENT_COLOR = $COLOR1;
   }
	if( $HTML) {
		print $header if $header;
		undef $header;
	}
	print $headerx;

	foreach my $k ( sort keys %servicekeys ) {
		next unless $servicetype{$k} eq $svctype;
		print "$k (".$servicekeys{$k}.")\n" unless $HTML;
		foreach my $s ( sort @serverlist ) {
			next unless $currentstate{$s.":".$k};
			$rowcolor{$s.":".$k}	= "BGCOLOR=$CURRENT_COLOR" unless $rowcolor{$s.":".$k};
			if( ! $HTML ) {
				print $s," ",$currentstate{$s.":".$k}," ",$currentstartup{$s.":".$k},"\n";
			} else {
				print "<TR ".$rowcolor{$s.":".$k}."><TD>",$s,"</TD><TD>", $svctype, "</TD><TD>",$k,"</TD><TD>",$servicekeys{$k},"</TD><TD>",$currentstate{$s.":".$k},"</TD><TD>",
						$currentstartup{$s.":".$k},"<\TD><\TR>\n" if $HTML;
			}
		}
		print "\n" unless $HTML;
	}
}
print "</TABLE><p><p>\n" if $HTML;

# PRINT UNTYPED SERVICES
print "<TABLE BORDER=1>\n" if $HTML;
my($header2)="<TR BGCOLOR=$HEADER_COLOR><TH COLSPAN=6>NON DATABASE SERVICES ON YOUR WINDOWS SERVERS</TH></TR>\n";
$header3="<TR bgcolor=$HEADER_COLOR><TH>HOSTNAME</TH><TH>SERVICE</TH><TH>DESCRIPTION</TH><TH>CURRENT STATE</TH><TH>STARTUP</TH></TR>\n";

foreach my $k ( sort keys %servicekeys ) {
	next if defined $servicetype{$k};
	print "$k (".$servicekeys{$k}.")\n" unless $HTML;
	if( $CURRENT_COLOR eq $COLOR1 ) {
      $CURRENT_COLOR = $COLOR2;
   } else {
      $CURRENT_COLOR = $COLOR1;
   }
   if( $HTML) {
		print $header2 if $header2;
		undef $header2;
	}

	print $header3;
	foreach my $s ( sort @serverlist ) {
		next unless $currentstate{$s.":".$k};
		$rowcolor{$s.":".$k}	= "BGCOLOR=$CURRENT_COLOR" unless $rowcolor{$s.":".$k};
		if( ! $HTML ) {
			print $s," ",$currentstate{$s.":".$k}," ",$currentstartup{$s.":".$k},"\n";
		} else {
			print "<TR ".$rowcolor{$s.":".$k}."><TD>",$s,"</TD><TD>", $k,"</TD><TD>",$servicekeys{$k},"</TD><TD>",$currentstate{$s.":".$k},"</TD><TD>",
				$currentstartup{$s.":".$k},"<\TD><\TR>\n";
		}
	}
	print "\n" unless $HTML;
}
print "</TABLE>\n" if $HTM
