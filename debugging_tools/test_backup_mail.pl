#!/usr/local/bin/perl-5.6.1

use strict;
use Sys::Hostname;
use MIME::Lite;
use File::Basename;
use CommonFunc;

$|=1;

my(%CONFIG)=read_configfile();

print "FROM MAIL: $CONFIG{MAIL_FROM} \n";
print "TO MAIL:   $CONFIG{MAIL_TO} \n";

print "Testing Smtp Mail Setup\n";
die "MAILHOST NOT DEFINED IN CONFIGFILE" unless defined $CONFIG{MAIL_HOST};
print "Your SMTP Mail Server Is $CONFIG{MAIL_HOST} \n";

die "MAIL TO NOT DEFINED IN CONFIGFILE" unless defined $CONFIG{MAIL_TO};
print "Sending Test Mail Message\n";
print "  MAILTO=$CONFIG{MAIL_TO} \n";

my($fromuser);
if( $CONFIG{MAIL_FROM} ){
	$fromuser=$CONFIG{MAIL_FROM};
} else {
 	$fromuser=$ENV{USERNAME} || $ENV{USER} || $ENV{LOGNAME};
	$fromuser.= "\@".hostname();
	$fromuser.= ".".$ENV{USERDNSDOMAIN} if defined $ENV{USERDNSDOMAIN};
}
print "  FROM USER=",$fromuser,"\n";
my($txt)="Test Mail From program=".basename($0)." on host=".hostname();
print "  Creating Mail\n";
my $msg= MIME::Lite->new(
	From => $fromuser,
	To   => $CONFIG{MAIL_TO}, # "ebarlow\@mlp.com",
	Subject => $txt,
	Data    => $txt."\n" );

print "  Sending Mail via $CONFIG{MAIL_HOST} \n";
$msg->send("smtp",$CONFIG{MAIL_HOST},Timeout=>60);;
print "  Done\n";
