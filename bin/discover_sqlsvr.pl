#!/apps/perl/linux/perl-5.8.2/bin/perl

# copyright (c) 2009-2011 by Edward Barlow

use strict;
use Socket;
use Getopt::Long;
use DBIFunc;
use DBI_discovery;
use Carp qw/confess/;
use Repository;
use Win32::ODBC;

# the plan
# get osql -l
# ping the osql -l servers with osql -E 
# get dsn info - specifically mapping of dsn to server
# get servers - and map via dsns
#
# report gem servers w/o dsn

use vars qw( $DEBUG $DIAGNOSE);
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" 
	unless  GetOptions(  "DEBUG" => \$DEBUG , "DIAGNOSE"	=> \$DIAGNOSE	);

# we are going to run this each time despite cache directives - its fast
open(OUTF2,"osql -L |");
my(%ALLSERVER_STATUS);
while(<OUTF2>) {
	chop;
	next if /^\s*$/ or /^Servers:/ or /local/;
	s/^\s+//;
	s/\s+$//;
	next if /\\/;
	$ALLSERVER_STATUS{$_}="OSQL";
}
close(OUTF2);

my(%DBI_HASHREF);						# a hash of available DSN's
my(@list_of_odbc_dsns);
%DBI_HASHREF=fetch_dbi_data_sources();	# key=$DRIVER:$DSN
foreach ( keys %DBI_HASHREF ) {
	my($typ,$nm)=split(":",$_,2);
	push @list_of_odbc_dsns,$nm;
}

my(%Realtype_By_DbName);
my(%Ip_By_DbName);
my(%InternalName_By_DbName);
my(%Port_By_DbName);
my(%Hostname_By_DbName);
my(%Description_By_DbName);

fetch_win32_odbc();

exit(0);

my(%ipcache);	# ip by svr
sub GetIpAddress {
	my($svr)=@_;
	return $ipcache{$svr} if $ipcache{$svr};
	my(@address)=gethostbyname($svr);# or confess "Cant resolve ip for $svr $!\n";
	# print "DBGDBG $#address FOR imagsyb2\n" if $svr eq "imagsyb2";
	return "" if $#address < 0;
	@address= map { inet_ntoa($_) } @address[4..$#address];
	print "  ==> GetIpAddress($svr) = ",join(" ",@address),"\n" if $DEBUG;
	print "FAILED FOR $svr\n" if $#address<0;
	$ipcache{$svr} = join(/\|/,@address);
	return  join(/\|/,@address);
}

sub fetch_win32_odbc {
	# load_pkg("Win32::ODBC");

	my(%DriverList) = Win32::ODBC::Drivers();
	if( $DIAGNOSE ) {
		print "The following drivers are installed:\n";
		foreach my $Driver ( keys( %DriverList ) ) {
			print "$Driver\n";
			print "\tAttributes:\n";
			map { print "\t\t$_\n" } ( split( /;/, $DriverList{$Driver} ) );
		}
	}

	my( %DataSources ) = Win32::ODBC::DataSources();
	_debugmsg( "WIN32 DSN's available from this user account:\n" );
	foreach my $DSN ( sort (keys( %DataSources )) )
	{
	  next unless $DataSources{$DSN}=~/sybase/i
	  			or $DataSources{$DSN}=~/adaptive server/i
	  			or $DataSources{$DSN}=~/mysql/i
	  			or $DataSources{$DSN}=~/oracle/i
	  			or $DataSources{$DSN}=~/sql server/i
	  			or $DataSources{$DSN}=~/sql native/i;
	  next if $DSN=~/localserver/i;

	  my($DSNKEY)=$DSN;
	  $DSNKEY=~s/\..+$//;

	  if( defined $DBI_HASHREF{ "ODBC:".$DSNKEY } ) {
	  		$DBI_HASHREF{ "ODBC:".$DSNKEY } = "OK";
	  } else {
	  		$DBI_HASHREF{ "ODBC:".$DSNKEY } = "ODBCONLY";
	  }
	  my %Config = Win32::ODBC::GetDSN( $DSN );

	  _debugmsg( sprintf( "%-20s %s \n",$DSNKEY,$DataSources{$DSN} ));
	  $Realtype_By_DbName{$DSNKEY} 		= get_realtype_from_drivertype($DataSources{$DSN});
	  $InternalName_By_DbName{$DSNKEY} = $DSNKEY if $DataSources{$DSN} =~ /sql server/i;
	  foreach my $Attrib ( keys( %Config ) )	  {
	  		_debugmsg( "    $Attrib => '$Config{$Attrib}'\n" );
	      $Description_By_DbName{$DSNKEY} = $Config{$Attrib} if $Attrib =~ /description/i;
	      if( $DataSources{$DSN} =~ /sybase/i or $DataSources{$DSN} =~ /adaptive/i) {
	      	if( $Attrib eq "NetworkAddress" ) {
	      		( 	$Hostname_By_DbName{$DSNKEY}, $Port_By_DbName{$DSNKEY} ) = split(/,/,$Config{$Attrib});
	      	}
	      }
	      if( $Attrib eq "ServerName" and $DataSources{$DSN} =~ /oracle/i ) {
	      	$InternalName_By_DbName{$DSNKEY} = $Config{$Attrib};
	      }
	      if( $DataSources{$DSN} =~ /sql server/i ) {
	    		$InternalName_By_DbName{$DSNKEY} = $Config{$Attrib} if $Attrib =~ /server/i;
	      }
	  }
	  # set_registered_hostname_for_host($InternalName_By_DbName{$DSNKEY}) if $InternalName_By_DbName{$DSNKEY};
	  _diagmsg( "\n" );
	}

	# was ignore_unsupported_odbc_servers_on_win32();
	if( $^O eq 'MSWin32' ) {
		header( "THE FOLLOWING NONSUPPORTED ODBC DSNs WILL BE IGNORED" );
		foreach ( get_dsn_list() ) {
			next if $DBI_HASHREF{"ODBC:".$_} eq "OK";
			delete $DBI_HASHREF{"ODBC:".$_};
			_statusmsg( "Ignoring NONSUPPORTED ODBC DSN $_\n" );
		}
	}
}

sub error_out { die @_; }
sub _statusmsg { print "        ",@_; }
sub header {
	my($str)=join("",@_);
	$str =~ s/\s*$//;
	print "\n--------------------------------------------------------------------------\n| ",
		$str,
		"\n","--------------------------------------------------------------------------\n";
}

sub _debugmsg { print( "[debug] ",@_) if $DEBUG; }
sub _diagmsg  { print( "[diag]  ",@_) if $DIAGNOSE; }

sub get_realtype_from_drivertype {
	my($k)=@_;
	return "sqlsvr" if $k eq "SQL Server"	or  $k eq "SQL Native Client";
	return "mysql"  if $k =~ /mysql/i;
	return "oracle" if $k =~ /oracle/i;
	return "sybase" if $k =~ /sybase/i;
	return undef;
}