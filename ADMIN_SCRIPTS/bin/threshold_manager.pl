#!/apps/perl/linux/perl-5.8.2/bin/perl
#
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use MlpAlarm;
use CommonFunc;
use CommonHeader;
use Do_Time;
use DBIFunc;
use Repository;
use Sys::Hostname;

my($VERSION)=4;

my $query_cnt = 1;
use vars qw( $DEBUG $SERVER $TIME_THRESH $ACTION $HTML $THRESHFILE $OUTFILE $DATABASE $BATCHID $NOEXEC);
sub usage {
	my($msg)=@_;
	print $msg."\nUsage: $0 --ACTION=INSTALL|UNINSTALL|MONITOR|REPORT|CHECK|CLEAR
	[--SERVER=xxx] [--DEBUG] [--OUTFILE=file] --HTML --THRESHFILE=file --BATCHID=id [--NOEXEC]

	if --ACTION=INSTALL will install to all your servers
	if --ACTION=CHECK will check installation
	if --ACTION=CLEAR will clear data for --SERVER/--DATABASE
	if --ACTION=REPORT will create a report of current values
		- will print HTML output if --HTML is passed
	\n";
	exit(1);
}

usage("Bad Parameter List $!\n") unless
   GetOptions(
   	"ACTION=s"			=> \$ACTION,
		"HTML"				=> \$HTML,
		"TIME_THRESH=s"	=> \$TIME_THRESH,
		"THRESHFILE=s"		=> \$THRESHFILE,
		"BATCHID=s"			=> \$BATCHID,
		"SERVER=s"			=> \$SERVER,
		"DATABASE=s"		=> \$DATABASE,
		"OUTFILE=s"			=> \$OUTFILE,
		"NOEXEC"				=> \$NOEXEC,
		"DEBUG"				=> \$DEBUG
	);

usage( "Must Pass --ACTION\n") unless defined $ACTION;
$TIME_THRESH=7*24 unless $TIME_THRESH;

usage( "ACTION=INSTALL|UNINSTALL|MONITOR|REPORT|CHECK|CLEAR" )
	unless $ACTION eq "INSTALL"
		or  $ACTION eq "UNINSTALL"
		or  $ACTION eq "MONITOR"
		or  $ACTION eq "CHECK"
		or  $ACTION eq "CLEAR"
		or  $ACTION eq "REPORT";

my($NL)="\n";
$NL="<br>\n" if $HTML;
dbi_set_web_page(1) if $HTML;

if( $OUTFILE ) {
	open(OUT,">$OUTFILE") or die "Cant write $OUTFILE\n";
}

output("threshold_manager.pl --ACTION=$ACTION run at ".localtime(time)." on host ".hostname().$NL);
output("BatchName=$BATCHID",$NL) if $BATCHID;
if( $ACTION eq "REPORT" ) {
	output( "<TABLE BGCOLOR=WHITE><TR><TH>\n<center>Threshold Manager Instructions</center>" );
	output( "The sybase threshold manager is a warning system for out of space conditions on sybase.  It does this by installing sybase threshold stored procedures that monitor peak usage for the segments in your database.   This is an effective method of monitoring your sybase log and data space.<p>" );
	output( "This system tracks peak usage (in pages).  If you add space to any of your databases, or add a new server, you must run\n" );
	output( "<i>" ) if $HTML;
	output( "threshold_manager.pl --ACTION=INSTALL [--SERVER=svr] \n" );
	output( "</i>" ) if $HTML;
	output( "to clear historical data or your percentages will be messed up.<p>") ;
	output( "</TR></TH></TABLE>\n" );
}

$|=1;

MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor') if $BATCHID and $ACTION eq "REPORT";

# ALL THE LEGAL ACTIONS ARE LISTED HERE
my(%prior_install_dat,%this_install_dat);
if( $ACTION eq "REPORT" or $ACTION eq "CHECK" or $ACTION eq "INSTALL"
	or $ACTION eq "UNINSTALL" or $ACTION eq "CLEAR") {

	my($warnth,$alarmth,$critth);
	if( $THRESHFILE and $ACTION eq "REPORT" ) {
		($warnth,$alarmth,$critth)=read_threshfile($THRESHFILE,"ThresholdManager");
		# delete all existing thresholds!
		MlpManageData(Admin=>"Delete By Monitor", Operation=>"del", Monitor=>'ThresholdManager');
	}

	%prior_install_dat=get_prior_config_dat() if $ACTION eq "INSTALL";
	# foreach ( keys %prior_install_dat ) {		print "DBG DBG $_ \n";	}

	my(@s);
	if( defined $SERVER ) {
		push @s,split(/,/,$SERVER);
	} else {
		push @s,get_password( -type=>'sybase');
	}
	dbi_msg_exclude("This threshold is too close to one");	# we are just going to ignore thresholds too close
	dbi_msg_exclude("DBCC execution completed");
	dbi_msg_exclude("Dropping threshold for segment");
	dbi_msg_exclude("Adding threshold for segment");
	dbi_msg_exclude("Cannot add rows to sysdepends for the cur");
	dbi_msg_exclude("2528");
	dbi_msg_exclude("17872");

	my($curday)=do_time(-fmt=>'yyyymmdd');

	foreach my $server ( @s ) {
		my($USER,$PASSWORD)=get_password(-name=>$server, -type=>'sybase');
		my($rc)=dbi_connect(-srv=>$server,-login=>$USER,-password=>$PASSWORD, -quiet=>1);
		if( ! $rc ) {
			output( "*** Cant Connect To $server as $USER from ".hostname().$NL );
			next;
		}
		output("Connected To $server",$NL) if $DEBUG;

		if( $ACTION eq "REPORT" ) {
			my($q)="select * from sysobjects where name like 'gem_threshold_hist%'";
			my(@rc)= dbi_query( -db=>'sybsystemprocs', -query=>$q );
			my($tbl);
			foreach ( @rc ) {($tbl)=dbi_decode_row($_);}
			if( ! $tbl ) {
				output( "*** Threshold Manager Is *NOT* installed on $server",$NL );
				next;
			}
			output( "*** Clearing Old Data From $server (sybsystemprocs..$tbl)",$NL );
			my($q)="delete $tbl where datediff(hh,cur_time,getdate())>8";
			output(" => $q",$NL);
			dbi_query( -db=>'sybsystemprocs', -query=>$q, -no_results=>1 ) unless $NOEXEC;

			output( "*** Starting Reports on $server",$NL );
			my($q)="
select dbname,segment_name,percent=max(thresh_percent)
into #tmp
from gem_threshold_hist".$VERSION."
group by dbname,segment_name
order by dbname,segment_name

select distinct t.*,day=convert(char(10),max(h.cur_time),112),tm=convert(char(10),max(h.cur_time),108)
from #tmp t,gem_threshold_hist".$VERSION." h
where h.dbname=t.dbname and h.segment_name=t.segment_name and t.percent=h.thresh_percent
group by t.dbname,t.segment_name,t.percent
having h.dbname=t.dbname and h.segment_name=t.segment_name and t.percent=h.thresh_percent

drop table #tmp
";
			my(@rc);
			if( $HTML ) {
				@rc= dbi_query( -db=>'sybsystemprocs', -query=>$q,-print_hdr=>1 );
				if( $#rc>=0 ) {
					output( "<PRE><FONT COLOR=RED>" );
					output( join("\n",dbi_reformat_results( @rc )),"\n" );
					output( "</PRE></FONT>\n");
				}
			} else {
				@rc= dbi_query( -db=>'sybsystemprocs', -query=>$q,-print_hdr=>1 );
				output( join("\n",dbi_reformat_results( @rc )),"\n" ) if $#rc>=0;
			}

			foreach (@rc) {
				my($db,$seg,$pct,$dt,$tm)=dbi_decode_row($_);
				next if $db eq "dbname" and $seg eq "segment_name"; # header
				my($state)="OK";
				$$alarmth{$server.":logsegment"}=70 unless $$alarmth{$server.":logsegment"};
				$$alarmth{$server.":default"}=90 unless $$alarmth{$server.":default"};

				if( $db eq "tempdb" ) {
					$state="ERROR"       if $pct>50;
					$state="CRITICAL"    if $pct>80;
				} else {
					$state="WARNING"  if $$warnth{$server.":$seg"}
								and $pct>$$warnth{$server.":$seg"};
					$state="ERROR"    if $$alarmth{$server.":$seg"}
								and $pct>$$alarmth{$server.":$seg"};
					$state="CRITICAL" if $$critth{$server.":$seg"}
								and $pct>$$critth{$server.":$seg"};
				}
				output("Saving Alarm $state for $server/$db/$seg ".
						$$warnth{$server.":$seg"}."/".
						$$alarmth{$server.":$seg"}."/".
						$$critth{$server.":$seg"}.$NL) if $DEBUG;

				if( ! $BATCHID ) {
					output("(NoAlert as NoBatchid) $state $server $db seg=$seg usg=$pct dt=$dt $tm".$NL );
				} else {
					if( $curday eq $dt ) {
					MlpHeartbeat(
						-state=>$state,
			 			-monitor_program=>$BATCHID,
						-system=>$server,
						-subsystem=>$db." ".$seg,
						-message_text=>"GEM_008: Thresholds show peak usage=$pct at $dt $tm");
					} else {
						output("(NoAlert as NotToday) $state $server $db seg=$seg usg=$pct dt=$dt $tm".$NL );
					}
				}
			}

			next;

		} elsif( $ACTION eq "CLEAR" ) {
			my($q)="select * from sysobjects where name like 'gem_threshold_hist%'";
			my(@rc)= dbi_query( -db=>'sybsystemprocs', -query=>$q );
			my($tbl);
			foreach ( @rc ) {($tbl)=dbi_decode_row($_);}
			if( ! $tbl ) {
				output( "*** Threshold Manager Is *NOT* installed on $server",$NL );
				next;
			}
			output( "*** Clearing Old Data From $server (sybsystemprocs..$tbl)",$NL );
			my($q)="delete $tbl ";
			$q.=" where dbname='".$DATABASE."'" if $DATABASE;
			output(" => $q",$NL);
			dbi_query( -db=>'sybsystemprocs', -query=>$q, -no_results=>1 ) unless $NOEXEC;
			next;
		} elsif( $ACTION eq "CHECK" ) {
			output( "*** Starting Checks on $server",$NL );
		} elsif( $ACTION eq "UNINSTALL" ) {
			output( "*** Starting Uninstall on $server",$NL );
		} elsif( $ACTION eq "CLEAR" ) {
			output( "*** Clearing Data From $server",$NL );
		} else {		# ACTION=INSTALL IF YOU GET HERE!
			output( "*** Starting Installation on $server",$NL );
		}

		# IF HERE ITS NOT ACTION=REPORT
		my($exclude)=1 if $ACTION eq "INSTALL";
		my($SERVERTYPE,@databaselist)=dbi_parse_opt_D('%',1,$exclude);
		push @databaselist, "master" if  $ACTION eq "INSTALL";
		push @databaselist, "tempdb" if  $ACTION eq "INSTALL";
		my($DBTYPE,$DBVER,$DBPATCH)=dbi_db_version();
		output("DB TYPE=$DBTYPE VER=$DBVER PAT=$DBPATCH\n");

		output( "Checking For Default Threshold Proc",$NL );
		my($q)='if exists ( select * from sysobjects where name = "sp_thresholdaction" )
				select "it exists" else select "it doesnt exist"'."\n";
		output( "$server: ",$q ) if $DEBUG;
		my($threshold_action_exists)="OOPS";
		my(@rc) = dbi_query(-db=>'sybsystemprocs',	-query=>$q );
		foreach ( @rc ) {
			my($msg)=dbi_decode_row($_);
			if( $msg eq "it exists" ) {
				output( "Found sp_thresholdaction",$NL );
				$threshold_action_exists=1;
			} elsif( $msg eq "it doesnt exist" ) {
				output( "Did not find sp_thresholdaction",$NL );
				$threshold_action_exists=0;
			} else {
				MlpBatchJobErr("WOAH: query $q returned ".dbi_decode_row($_));
				die "WOAH: that query returned : ",dbi_decode_row($_),"\n";
			}
		}
		my($q)='if exists ( select * from sysobjects where name = "sp_thresholdaction2" )
				select "it exists" else select "it doesnt exist"'."\n";
		output( "$server: ",$q ) if $DEBUG;
		my($managed_thresholdaction)="OOPS";
		my(@rc) = dbi_query(-db=>'sybsystemprocs',	-query=>$q );
		foreach ( @rc ) {
			my($msg)=dbi_decode_row($_);
			if( $msg eq "it exists" ) {
				output( "sp_thresholdaction appears to have been build by threshold_manager",$NL );
				$managed_thresholdaction=1;
			} elsif( $msg eq "it doesnt exist" ) {
				output( "sp_thresholdaction appears to have been build by you",$NL );
				$managed_thresholdaction=0;
			} else {
				MlpBatchJobErr("WOAH: query $q returned ".dbi_decode_row($_));
				die "WOAH: that query returned : ",dbi_decode_row($_),"\n";
			}
		}

		my(%logpages);

		#$q='select db_name(dbid),sum(u.size) from master.dbo.sysusages u where u.segmap & 4 = 4 group by dbid'."\n";
		$q='select db_name(dbid),sum(u.size) from master.dbo.sysusages u where u.segmap = 4 group by dbid'."\n";
		output( "$server: ",$q ) if $DEBUG;
		foreach ( dbi_query(-db=>'master',-query=>$q ) ) {
			my($db,$sz)=dbi_decode_row($_);
			$logpages{$db}=$sz;
		}

		my(%datapages);
		#$q='select db_name(dbid),sum(u.size) from master.dbo.sysusages u where u.segmap & 3 = 3 group by dbid'."\n";
		$q='select db_name(dbid),sum(u.size) from master.dbo.sysusages u where u.segmap != 4 group by dbid'."\n";
		output( "$server: ",$q ) if $DEBUG;
		foreach ( dbi_query(-db=>'master',-query=>$q ) ) {
			my($db,$sz)=dbi_decode_row($_);
			$datapages{$db}=$sz;
		}

#		$q='select db_name(dbid),sum(u.size) from master.dbo.sysusages u where u.segmap & 7 = 7 group by dbid'."\n";
#		output( "$server: ",$q ) if $DEBUG;
#		foreach ( dbi_query(-db=>'master',-query=>$q ) ) {
#			my($db,$sz)=dbi_decode_row($_);
#			$datapages{$db}=0 unless $datapages{$db};
#			$logpages{$db}=0 unless $logpages{$db};
#			$datapages{$db}+=$sz;
#			$logpages{$db}+=$sz;
#
#			#output( "Skipping $db as data and log on same device",$NL )
#			#	if $logpages{$db} and $ACTION ne "CHECK";
#			#delete $logpages{$db};
#		}


#foreach ( @databaselist ) {	print "DBG: $_ ",($datapages{$_}/128)," ",($logpages{$_}/128),"\n";}
#foreach ( keys %datapages ) {	print "DBG: $_ $datapages{$_}\n";}
#foreach ( keys %logpages ) {	print "DBG: $_ $logpages{$_}\n";}

		output( $NL );
		my($dohdr)=1;
		my(@results);
		my(@more_queries);
		foreach my $db (@databaselist) {
			# next unless defined $logpages{$db} or $ACTION eq "CHECK";
			push @more_queries,"$db;-- Working on database $db\n";
			my(@rc)=dbi_query(-db=>$db,-query=>'sp_helpthreshold',-print_hdr=>$dohdr );
			if( $dohdr) {
				my(@y)=dbi_decode_row(shift @rc);
				unshift @y,"Database";
				push    @y,"LogPages";
				push @results, dbi_encode_row(@y);
				$dohdr=undef;
			}
			foreach (@rc) {
				my($seg,$freespace,$status,$proc)=dbi_decode_row($_);
				if( $status==1 ) { $status="Yes"; } else { $status="No"; }
				push @results, dbi_encode_row($db,$seg,$freespace,$status,$proc,$logpages{$db});
				push @more_queries, "$db;exec sp_dropthreshold \"$db\", \"$seg\", $freespace"."\n"
					if $proc =~ /sp_gemthresholdaction/;

			}
			my($freespace);
			if( $ACTION eq "INSTALL" ) {
				push @more_queries,"$db;-- Space for $db is $datapages{$db} data and $logpages{$db} log\n";
				if( $logpages{$db} > 100000 ) {
					foreach my $i (30,40,50,60,70,80,85,90,93,95,97,99) {
						$freespace= $logpages{$db} - int($i * $logpages{$db} / 100);
						push @more_queries, "$db;exec sp_addthreshold \"$db\", \"logsegment\", $freespace,\"sp_gemthresholdaction_".$i."\"\n";
					}
				} elsif( $logpages{$db} ) {
					foreach my $i (50,80,90,95) {
						$freespace= $logpages{$db} - int($i * $logpages{$db} / 100);
						push @more_queries, "$db;exec sp_addthreshold \"$db\", \"logsegment\", $freespace,\"sp_gemthresholdaction_".$i."\"\n";
					}
				}
				#if( $datapages{$db} > 100000 ) {
					foreach my $i (80,85,90,95) {
						$freespace= $datapages{$db} - int($i * $datapages{$db} / 100);
						push @more_queries, "$db;exec sp_addthreshold \"$db\", \"default\", $freespace,\"sp_gemthresholdaction_".$i."\"\n";
					}

				if( $db eq "tempdb" ) {
					foreach my $i (30,50,60,70) {
						$freespace= $datapages{$db} - int($i * $datapages{$db} / 100);
						push @more_queries, "$db;exec sp_addthreshold \"$db\", \"default\", $freespace,\"sp_gemthresholdaction_".$i."\"\n";
					}
				}
				#} else {
				#	push @more_queries, "$db;-- Skipping Data As $datapages{$db} < 100000 pages";
				#}
			}
		}

		if( $ACTION eq "CHECK" ) {
			output( join("\n",dbi_reformat_results( @results )),"\n" );
			output ( "*** FINISHED WORKING ON $server\n" );
			next;
		}

		#
		# WE HAVE ALL THE INFO - NOW REBUILD OR UNINSTALL
		#
		my($q);
		my($IGNORE_TABLE_REBUILD);
		$q="select * from sysobjects where name like 'gem_threshold_hist%'";
		my(@rc)= dbi_query( -db=>'sybsystemprocs', -query=>$q );
		foreach ( @rc ) {
			my($tbl)=dbi_decode_row($_);
			$tbl=~s/gem_threshold_hist//;
			if( $tbl eq $VERSION ) {
				output( "SERVER is at latest version!!! Ignoring table rebuild\n" );
				$IGNORE_TABLE_REBUILD=1;

				foreach my $db (@databaselist) {
					next unless defined $logpages{$db} or $ACTION eq "CHECK";
					my($str)=$server.":".$db.":".$datapages{$db}.":".$logpages{$db};
					$this_install_dat{$str}=1;

					# WE ONLY DELETE OLD DATA IF THE SIZE OF THE DBASE HAS CHANGED!!!
					output("$server: not deleting $db data as database size has NOT changed\n") if $DEBUG;
					next if defined $prior_install_dat{$str};
					print "DBG DBG |".$str."| WAS NOT FOUND\n";

					$q="delete gem_threshold_hist".$tbl." where dbname='".$db."'\n";
					output( "$server: ",$q );
					dbi_query( -db=>'sybsystemprocs', -query=>$q ) unless $NOEXEC;
				}
			} else {
				output( "SERVER is at an older version!!! Upgrading...\n" );
				$q="drop table gem_threshold_hist".$tbl."\n";
				output( "$server: ",$q );
				dbi_query( -db=>'sybsystemprocs', -query=>$q ) unless $NOEXEC;
			}
		}

		if( ! defined $IGNORE_TABLE_REBUILD ) {
			$q="create table gem_threshold_hist".$VERSION." (
					thresh_percent		char(3),
					dbname		varchar(30),
					segment_name	varchar(30),
					space_left	int,
					is_last_chance	int,
					cur_time	datetime,

					oldest_tran_start datetime,
					tran_name       varchar(67) null,
					cmd		varchar(16) null,
					ipaddr		varchar(15) null,
					login_name	varchar(30) null,
					is_alerted	char(1)='Y'
					)\n";
			output( "$server: ",$q );
			@rc = ();
			@rc = dbi_query(-db=>'sybsystemprocs',	-query=>$q ) unless $NOEXEC;
			foreach ( @rc ) { output( "RC>>>".dbi_decode_row($_),"\n" ); }
		}

		#print "Dropping Threshold Procs\n";
		foreach my $pct (30,40,50,60,70,80,90,85,95,'lct') {
			$q='if exists ( select * from sysobjects where name = "sp_gemthresholdaction_'.$pct.'" ) drop procedure sp_gemthresholdaction_'.$pct."\n";
			if( $DEBUG ) {
				output( "$server: ",$q );
			} else {
				output( "$server: drop procedure sp_gemthresholdaction_".$pct."\n" );
			}
			@rc = ();
			@rc = dbi_query(-db=>'sybsystemprocs',	-query=>$q ) unless $NOEXEC;
			foreach ( @rc ) { output( "RC>>>".dbi_decode_row($_),"\n"); }
		}

		if( $ACTION eq "INSTALL" ) {

			foreach my $pct (30,40,50,60,70,80,90,85,95,'lct') {
				$q='create procedure sp_gemthresholdaction_'.$pct.'
	@dbname varchar(30),
	@segmentname varchar(30),
	@space_left int,
	@status int
as
	insert gem_threshold_hist'.$VERSION.'
	select "'.$pct.'", @dbname, @segmentname, @space_left, @status, getdate(),
		starttime, h.name, p.cmd, ';

			if( $DBVER < 12.5 ) {
				$q.="null,";
			} else {
				$q.="ipaddr,";
			}
		$q.=' suser_name(p.suid)
	from master..syslogshold h, master..sysprocesses p
	where h.spid=p.spid and  db_id(@dbname)=h.dbid
';
				output( "$server: ",$q ) if $DEBUG;
				output( "$server: create procedure sp_gemthresholdaction_".$pct."\n" ) unless $DEBUG;
				@rc = ();
				@rc = dbi_query(-db=>'sybsystemprocs',	-query=>$q ) unless $NOEXEC;
				foreach ( @rc ) { output( "RC>>>".dbi_decode_row($_),"\n" ); }
			}

			# ok if managed_thresholdaction=1 then we can overwrite the default proc otherwise not
			if( $threshold_action_exists == 0 ) {
				$q='create procedure sp_thresholdaction
	@dbname varchar(30),
	@segmentname varchar(30),
	@space_left int,
	@status int
as
	exec sp_gemthresholdaction_lct @dbname,@segmentname,@space_left,@status
';
				output( "$server: ",$q) if $DEBUG;
				output( "$server: create procedure sp_gemthresholdaction_lct\n" ) unless $DEBUG;
				@rc = ();
				@rc = dbi_query(-db=>'sybsystemprocs',	-query=>$q ) unless $NOEXEC;
				foreach ( @rc ) { output( "RC>>>".dbi_decode_row($_),"\n"); }

				# this proc is unused... its just a reference so that we know that the thresholdaction
				# proc is under our control
				$q='create procedure sp_thresholdaction2
	@dbname varchar(30),
	@segmentname varchar(30),
	@space_left int,
	@status int
as
	exec sp_gemthresholdaction_lct @dbname,@segmentname,@space_left,@status
';
				if( $DEBUG ) {
					output( "$server: ",$q);
				} else {
						output( "$server: create procedure sp_thresholdaction2\n");
				}
				@rc = ();
				@rc = dbi_query(-db=>'sybsystemprocs',	-query=>$q ) unless $NOEXEC;
				foreach ( @rc ) { output( "RC>>>".dbi_decode_row($_),"\n"); }
			}
		}

		foreach my $a (@more_queries) {
			chomp $a;
			my($d,$q)=split(/;/,$a,2);
			output( "$server:$d:",$q,"\n");
			next if $q =~ /^--/;
			@rc = ();
			@rc = dbi_query(-db=>$d,-query=>$q ) unless $NOEXEC;
			foreach ( @rc ) {
				my(@msg)=dbi_decode_row($_);
				output( join(" ",@msg),$NL);
			}
		}
	}
	save_prior_config_dat() if $ACTION eq "INSTALL";
	output ( "*** FINISHED threshold_manager.pl\n" );
} else {
	MlpBatchJobErr("UNIMPLEMENTED");
	die "UNIMPLEMENTED";
}

MlpBatchJobEnd() if $ACTION eq "REPORT";
close(OUT) if $OUTFILE;
exit(0);

sub output {
	print @_;
	print OUT @_ if $OUTFILE;
}

# this func is in more than one configfile - should be moved to a librar
sub pr_query {
	my($a)=join("\n",@_)."\n";
	my(@v)=split(/\n/,$a);
	output( $NL);
	foreach (@v) {
		next if /^\s*$/;
		output( "[".$query_cnt."] ".$_.$NL );
	}
	output( $NL);

	$query_cnt++;
}

sub save_prior_config_dat {
	my($gem_root_dir)=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gem_root_dir;

	open( BBX, "> $gem_root_dir/data/threshold_manager.dat" )
		or die "Cant Write $gem_root_dir/data/threshold_manager.dat\n";
	print BBX "#Config FIle For threshold_manager.pl\n";
	print BBX "#Do Not Modify - THis is Programatically Generated\n";
	print BBX "#Format Server:Db:DataPages:LogPages\n";
	foreach ( sort keys %this_install_dat ) {
		print BBX $_,"\n";
	}
	close(BBX);
}

sub get_prior_config_dat {
	my($gem_root_dir)=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gem_root_dir;

	my(%outdat);
	return() unless -e "$gem_root_dir/data/threshold_manager.dat";
	open( aaX, "$gem_root_dir/data/threshold_manager.dat" )
		or die "Cant Read $gem_root_dir/data/threshold_manager.dat\n";
	while(<aaX>) {
		next if /^#/;
		s/^\s+//;
		s/\s+$//;
		chomp;
		$outdat{$_}=1;
	}
	close(aaX);
	return %outdat;
}


__END__

=head1 NAME

ThresholdManager.pl - Manage Sybase Thresholds

=head2 DESCRIPTION

This program is a warning system for out of space conditions on sybase.  It does this by installing
sybase threshold stored procedures named sp_gemthresholdaction_<number> that allow you to monitor
the peak usage percentage of each of the segments in your database.   This is an effective method
of monitoring your sybase log and data space.

=head2 USAGE

Usage: ThresholdManager.pl --ACTION=INSTALL|UNINSTALL|REPORT|CHECK
	[--SERVER=xxx] [--DEBUG] [--OUTFILE=file]

If server is not provided it will loop through all servers

ACTIONS
	if --ACTION=INSTALL will install into the server
	if --ACTION=UNINSTALL will remove created thresholds from the server
	if --ACTION=CHECK will check your installation
	if --ACTION=REPORT will create a report
		- will print data for --REPORT_HOURS hours
		- will only show rows > --TIME_THRESH hours (default 24x7 hours)
		- will print HTML output if --HTML is passed


=head2 NOTES

ONLY USE THIS UTILITY IF YOU DO NOT ACTIVELY USE DATA SEGMENTS... CURRENTLY SEGMENTS ARE NOT
CHECKED FOR AND WE ONLY DEAL WITH logsegment and default.  The reports will be off if you use
segments.

Basically what this tool does is create a bunch of thresholds in your databases that are very
simple one liners, saving info into a table in sybsystemprocs.  The goal here is so that you
can see when the logsegment is getting full.  It also allows you to see if the default segment
is getting full - which might be useful in the case where data is rapidly growing and shrinking
during the course of the day and you cant rely on the once per day space monitoring.  Fundamentally
this is a light weight method of doing that... active space monitoring is a big performance hog
and runs the risk of system table locks - while this does not.

So you an INSTALL the stuff easily into all your sybase servers and then rely on the console to
report peak usages and to make recommendations on how to grow your log files.

After you extend your databases, rerun the -ACTION=INSTALL procedure to reset your thresholds.

Tempdb is 50% error and 80% critical always

Only alerts if the value was from today - dont care that much about historical stuff
=end
