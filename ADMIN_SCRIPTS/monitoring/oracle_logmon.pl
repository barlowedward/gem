#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
require 5.002;
use     strict;
use     File::Basename;
use     LogFunc;
use 	Do_Time;
use 	Getopt::Long;
use 	MlpAlarm;

my($VERSION)=1.0;

use vars qw( $DEBUG $NOSAVEPATFILE $SERVER $PATFILE $INFILE $NOLOG );

$|=1;
my($message_num)=0;
my($current_state)="";

sub usage
{
	return "Usage: oracle_logmon.pl --NOSAVEPATFILE --DEBUG --NOLOG --SERVER=server --PATFILE=file --INFILE=file \n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(
		"INFILE=s"		=> \$INFILE,
		"PATFILE=s"		=> \$PATFILE,
		"SERVER=s"		=> \$SERVER,
		"NOSAVEPATFILE" 	=> \$NOSAVEPATFILE,
		"NOLOG" 		=> \$NOLOG,
		"DEBUG"			=> \$DEBUG );

die usage("Must pass INFILE\n" ) unless defined $INFILE;

my($srvr)=uc(basename($INFILE));
$srvr=~s/\.\w+$//;
$srvr=~s/^ALERT_//i;
$srvr = $SERVER if $SERVER;

# ============================
# get start patern
# ============================
my($start_offset,$start_patern)=(0,"");
$PATFILE=$INFILE.".pat" unless $PATFILE;
if( -r $PATFILE ) {
	die "Cant Find $PATFILE" unless -e $PATFILE;
	print "READING PATERN FILE $PATFILE\n";
	open(PAT,$PATFILE) or die "Cant Read $PATFILE $!\n";
	while(<PAT>) {
		chop;
   		($start_offset,$current_state,$start_patern) = split(/\s/,$_,3);
	}
	close(PAT);
}
print "Byte Offset=$start_offset\nPatern=$start_patern\n";

open(INF,"$INFILE")  or die "Cant Read $INFILE : $! \n";

# move forward to the patern
if( defined $start_patern and defined $start_offset ) {
		# print "DEBUG SEEKING $start_offset\n";
		seek(INF,$start_offset,0);
		my $tline = <INF>;
		if( index( $tline , $start_patern )==0 ) {
			print "PATERN FOUND AT OFFSET\n";
		} else {
			# if( defined $DEBUG ) {
				print "(dbg) PATERN NOT FOUND IN FILE - RESETTING FILE\n";
				print "(dbg) File At Byte Offset = $tline\n";
				print "(dbg) PATERN=$start_patern\n";
			# }
			seek INF,0,0 ;
		}
}  else {
	my $filenm=<INF>;
	print "No Patern Found In File";
}

# read the log file
print "-------------------- READING INPUT FILE -----------------------\n";
my($current_timestamp);
my($lastline,$curpos,$nextpos)=("",0,0);
my @curmsg;
while( <INF> ) {
   	$lastline = $_;
	$curpos   = $nextpos;
	s/\s+$//;
	next if /^$/;
	# blocks of lines start with a date on its own line
	if( length == 24 and /200\d$/ and $current_timestamp ne $_ ) {
		manage_message($current_timestamp,@curmsg) unless $#curmsg==-1;
		$#curmsg = -1;
		$current_timestamp=$_;
	} else {
		push @curmsg, $_ unless length==24 and /200\d$/;
		# print length," ",$_,"\n";
	}
	$nextpos   = tell INF;
}
close(INF);
print "-------------------- DONE READING INPUT FILE -----------------------\n";

if( $lastline ne "" ) {
	print "File Size=$curpos LastLn=$lastline\n";
	if( defined($PATFILE) and ! defined $NOSAVEPATFILE ) {
		# die "Cant Find $PATFILE" unless -e $PATFILE;
		print "Writing Patern File $PATFILE\n";
		open(PAT,">".$PATFILE) or die "Cant Write $PATFILE $!\n";
		print PAT $curpos," ",$current_state," ",$lastline;
		close(PAT);
	}
} else {
	print "Done... nothing to read\n";
}
exit(0);

my(%recent_msgs);		# list of messages to ignore because they occurred within the past minute
sub manage_message {
	my($ct, @cm) = @_;
	$message_num++;

	my($ARCHIVER_STATE)="DOWN";
	print "\n\nDBG: ($message_num) Data for $ct ($current_state)\nDBG: ",join("\nDBG: ",@cm),"\n" if $DEBUG;

	# skip some messages
	my($do_print) = "FALSE";
	my($skip_block)="FALSE";
	my($rowcount) = -1;
	my($current_msg)="";
	foreach ( @cm ) {
		$rowcount++;
		if( 	/^ARC[\dH]+: Archival started/ or
			/^Creating archive destination/ or
			/^\s+Log actively being archived by another process/ or
			/^ARC[\dH]+: Completed archiving\s+log/ ){
			change_state("UP",$ct);
			next;
		}
		if ( 	/^Archiving is disabled/ or
			/^Shutting down archive processes/  or
		     	/^ARC[\dH]+: Archiving is disabled/ or
			/^ARCH shutting down/ ) {
			change_state("DOWN",$ct);
			next;
		}

		if( /^ARC[\dH]+: Unable to archive\s+log/ ) {
			my($active);
			foreach my $r (@cm) {
				if( $r=~/Log actively being archived by another process/ ) {
					$active=1;last;
				}
			}
			next if $active;
			change_state("DOWN",$ct);
		}

		# $skip_block="FALSE" if $skip_block eq "TRUE" and ! /^\s\s/;
		# $skip_block="TRUE" if /System parameters with non-default values:/;
		$skip_block="TRUE" if /^Starting ORACLE instance/;

		next if $skip_block eq "TRUE";

		if( ! /^  Current log# \d+ seq#/
		and ! /^\s+/
		and ! /^ARC[\dH]+: Beginning to archive log/
		and ! /^Thread \d+ advanced to log sequence/
		and ! /^Archiving is enabled/
		and ! /^ARC[\dH]+: Shutdown aborted \(current state is/
		and ! /^Archive process shutdown avoided/
		and ! /^ORACLE V\d/
		and ! /^ARC\d+\: Archival stopped/
		and ! /^ARC\d+\: Thread not mounted/
		and ! /^to \'/
		and ! /^vsnsql=f vsnxtr/
		and !/^ARC[\dH]+: STARTING ARCH PROCESSES/
		and ! /^ARC[\dH]+: Evaluating archive/
		and ! /started with pid=\d/
		and ! /^alter database/i
		and ! /^Completed: /
		and ! /\/\* OracleOEM \*\//i
		and ! /alter system set /i
		and ! /MAXLOGFILES 32/i
		and (! /alter tablespace / and ! /begin backup$/)
		and (! /alter tablespace / and ! /end backup$/)
		and ! /: Becoming the heartbeat ARCH/
		and ! /Thread \d+ opened at log sequence \d+/

		and ! /^ORA-01555 caused by SQL statement below (SCN: 0x0000.088b601d):/
		and ! /^select * from /
		and ! /^ALTER SYSTEM SET log_archive_dest_state_\d='/
		# and ! /^ARCH: Possible network disconnect with primary database/
		and ! /^Clearing standby activation ID/
		and ! /^Media Recovery Log/
		and ! /^Media Recovery Waiting/
		and ! /^Media Recovery Cancelled/
		and ! /^Media Recovery user cancelled/
		and ! /^Managed Standby Recovery Cancelled/
		and ! /^ORA-\d+ signalled during: ALTER DATABASE RECOVER/
		and ! /^Datafile \d+: \'/
		and ! /^ALTER DATABASE RECOVER CANCEL/
		and ! /^Starting datafile \d+ recovery in/
		and ! /^The primary database controlfile was created using the/
		and ! /^There is space for up to \d+ standby redo logfiles/
		and ! /^Use the following SQL commands on the standby database to create/
		and ! /standby redo logfiles that match the primary database:/
		and ! /ORA-00312/
		and ! /^Windows \d+ Version \d/ ) {
			if( $current_msg ne $_ ) {
				my($dt)=$ct;
				$dt =~ s/^\w\w\w\s//;
				my($dt2)=$ct;
				$dt2=~s/:\d\d 20//g;
				next if $recent_msgs{$dt2.":".$_};
				$recent_msgs{$dt2.":".$_} = 1;
				my($severity) = "WARNING";
				$severity="ERROR" if /^ORA-\d\d\d\d\d+:/ or /^OSD-\d\d\d\d\d+:/ or /^O\/S-Error:/;
				$severity="CRITICAL" if /^ORA-000060/ or /^Shutting down instance/ or /^Database mounted/
						or /^Database Characterset is /;
				print $message_num,") $ct - ARCHIVER=$current_state - $severity\n..... $_\n";
				MlpEvent(
					-monitor_program 	=> "OracleLogmon",
					-event_time			=>  $dt,
					-system				=> $srvr,
					-subsystem			=> "Error Logs",
					-message_text		=> $_,
					-severity			=> $severity ) unless $NOLOG;
				$current_msg=$_;
			}
		}
	}

	# print $message_num,") $ct: ",join("\n",@cm),"\n" if $do_print eq "TRUE";
}

sub change_state {
	my($state,$time)=@_;
	if( $state ne $current_state ) {
		print "ARCH - STATE CHANGED TO $state at $time\n";
		$current_state=$state;
		if( $state eq "UP" ) {
			MlpHeartbeat(
				-monitor_program =>"OracleLogmon",
				-system		=>$srvr,
				-subsystem	=>"Error Logs",
				-message_text	=>"Archiving UP",
				-state		=>"OK" ) unless $NOLOG;
		} else {
			MlpHeartbeat(
				-monitor_program =>"OracleLogmon",
				-system		=>$srvr,
				-subsystem	=>"Error Logs",
				-message_text	=>"Archiving DOWN",
				-state		=>"WARNING" ) unless $NOLOG;
		}
	}
}

