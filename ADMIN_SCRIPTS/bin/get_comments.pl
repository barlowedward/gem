#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

die "Usage: get_comments.pl FILE\n" unless $#ARGV>=0;

open(FILE,$ARGV[0]) or die "cant open ".$ARGV[0]."\n";
undef $/;
my $file=<FILE>;  # slurp...
# while ( $file =~ m/(\/\*[\w\W]+?\*\/)|--(.*?)\n/g ) {
while ( $file =~ m/(\/\*[\w\W]+?\*\/)|--(.*?)\n|\#(.*?)\n/g ) {
        print "=>",$1,$2,$3,"\n";
}

close FILE;

__END__

=head1 NAME

get_comments.pl - utility to extract comments from file

=head2 DESCRIPTION

Extracts comments of the style /* */, --, and #

=cut
