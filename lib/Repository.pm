# copyright (c) 1999-2011 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

package Repository;

use Exporter;
use Carp;

$VERSION= 1.0;
@ISA      = ('Exporter');
@EXPORT = ( 'get_password','get_password_info','read_threshfile',
	'set_gem_root_dir','get_passtype','get_conf_dir','get_gem_root_dir',
	'get_root_dir','get_passfile_info', 'add_edit_item',
	'get_conf_file_dat',	'get_console_info', 'get_filename',
	'get_logfiles' );

##################################################

use strict;

my( %filemap ) = (
	sybase 			=> "sybase_passwords.dat",
	unix				=> "unix_passwords.dat",
	mysql				=> "mysql_passwords.dat",
	sqlsvr 	   	=> "sqlsvr_passwords.dat",
	oracle 	   	=> "oracle_passwords.dat",
	win32servers 	=> "win32_passwords.dat",
	documenter 		=> "console.dat",
	monitor_server => "monitorserver.dat",
	logfiles			=> "unix_logfiles.dat",
	shadow_sybase 			=> "shadow/sybase_passwords.dat",
	shadow_unix				=> "shadow/unix_passwords.dat",
	shadow_mysql			=> "shadow/mysql_passwords.dat",
	shadow_sqlsvr 	   	=> "shadow/sqlsvr_passwords.dat",
	shadow_oracle 	   	=> "shadow/oracle_passwords.dat",
	shadow_win32servers 	=> "shadow/win32_passwords.dat",

);

my(%login);			# key type.name
my(%password);		# key type.name
my(%conntype); 	# key type.name
my(%server_data);	# key type.name.key
my(%file_read);	# key filename
my(%server_type); # key servername
# my(%logfile_data,$logfile_was_read); 

#sub get_installation_state
#{	
#	my($dir)=get_conf_dir();	
#	return "UNINSTALLED" unless -r $dir."/InstallState";
#	open(IS,$dir."/InstallState") or die "Cant read $dir/InstallState";
#	my(%vals);
#	while(<IS>) {
#		chomp;
#		my($k,$v)=split(/\s+/);
#		$vals{$k} = $v;
#	}	
#	close(IS);		
#	return wantarray ? ($vals{STATE}, %vals) : $vals{STATE};
#}

#sub set_install_major_step
#{
#	my($state)=@_;	
#	# REGISTERED INSTALLED
#	my($cs, %vals) = get_installation_state();
#	$vals{$state} = time;
#	
#	$cs = $state if ( $cs eq "UNINSTALLED" or $cs eq "REGISTERED" ) or $state eq "INSTALLED";
#	my($dir)=get_conf_dir();
#	
#	open(IS2, "> ".$dir."/InstallState") or die "Cant write $dir/InstallState";
#	print IS2 "STATE\t",$cs,"\n";
#	foreach ( keys %vals ) {
#		print IS2 "$_ $vals{$_}\n" unless /^STATE/;
#	}	
#	close(IS2);
#}

sub get_root_dir
{
	my($dir)=get_gem_root_dir();
	return "$dir/ADMIN_SCRIPTS";
}

my($root_dir);
# the following function is used to set the root directory
# mostly useful in the case where you have not configured gem
sub set_gem_root_dir {
	my($newroot)=@_;
	$root_dir=$newroot;
}
sub get_gem_root_dir {
	my($finding_cfg_files)=@_;
	return $root_dir if defined $root_dir;

	my(%WHY_NOT);
	foreach my $d (@INC) {
		$WHY_NOT{$d}="Not a lib"  unless $d =~ /lib$/i;
		#print "Not a lib $d\n" unless $d =~ /lib$/i;
		next unless $d =~ /lib$/i;
		$WHY_NOT{$d}="Not a directory"  unless -d $d;
		next unless -d $d;

		# ok its a lib directory ... lets see if there are conf and admin
		# directories as its peers

		my($rd)=$d;
		$rd =~ s/.lib$//;
		if( -d "$rd/conf" and -d "$rd/debugging_tools" and -d "$rd/bin" ) {
			use Cwd;
			use File::Basename;
			my($startdir)=dirname($0);
			my($curdir) = cwd();

			die "WOAH... gem root dir is ($rd). seek help.  Your include path should not have relative paths in it.\n".
				"Module Repository.pm - function=get_gem_root_dir()\n".
				"Program Direstory=$startdir  Current Directory=$curdir\n" 
					if $rd =~ /^\./ and $0 !~ /configure.pl$/;
			$root_dir=$rd;
			return $rd;
		} elsif( $rd eq "lib" and -r "lib/MlpAlarm.pm" and -r "lib/CommonFunc.pm" ) {
			use Cwd;
			return cwd();
		} elsif( ! -d "$rd/debugging_tools" ) {
			$WHY_NOT{$d}="No SubDirectory $rd/debugging_tools";
		} elsif( ! -d "$rd/bin" ) {
			$WHY_NOT{$d}="No SubDirectory $rd/bin";
		} elsif( ! -d "$rd/conf" ) {
			$WHY_NOT{$d}="No SubDirectory $rd/conf";
		}
	}

	if( $finding_cfg_files) {	# exception case - not using gem looking for cfg files
		if( -r "./gem.dat" or -r "./sybase_passwords.dat"  or -r "./sqlsvr_password.dat" ) {
			$root_dir=".";			
			return "." ;
		}
		if( -r "../gem.dat" or -r "../sybase_passwords.dat"  or -r "../sqlsvr_password.dat" ) {
			$root_dir="..";			
			return ".." ;
		}
		if( -r "../conf/gem.dat" or -r "../conf/sybase_passwords.dat" or -r "../conf/sqlsvr_password.dat" ) {
			$root_dir="../conf";			
			return "../conf" ;
		}
	}

	# figure out why not above

#		next unless /ADMIN_SCRIPTS\/lib$/i;
#		my($dir)=$_;
#		$dir=~s/\/lib$//i;
#		$dir=~s/\/ADMIN_SCRIPTS$//i;
#		next # die "Gem Root Directory $dir - Does Not Exist\n"
#			unless -d $dir;
#		return $dir;

	# CAN NOT FIND APPROPRIATE THING IN THE INCLUDE PATH...

	warn "get_gem_root_dir() failed\n";
	warn "Can Not Locate Gem Root Directory based on your \@INC Perl Path.\n";
	warn "GEM searches your include path for a directory with conf, debugging_tools, and bin subdirectories.\n";
	warn "Your PERL Include path is \n";
	foreach (@INC) { printf STDERR "%-30s => %s\n",$_,$WHY_NOT{$_}; }
	return undef;
}

sub get_conf_dir {
	my($dir)=get_gem_root_dir(1);
	return $dir if $dir eq "." or $dir eq ".." or $dir eq "../conf";
	return "$dir/conf";
}

sub get_filename {
	my($typ)=@_;
	return $filemap{$typ};
}
sub get_passtype
{
	my(@t);
	foreach (keys %filemap) {
		push @t,$_ unless $_ eq "monitor_server" 
						or $_ eq "documenter" 
						or $_ eq "logfiles"
						or $_ =~ /^shadow_/;
	} 
	return @t;
	#return keys %filemap;
}

sub get_password
{
	my(%args)=@_;
	
	$args{-type} = $server_type{$args{-name}}
		if defined $args{-name} and ! defined $args{-type};
	die "Must Pass -type to get_password"
			unless defined $args{-type};
	unless( defined $filemap{$args{-type}} ) { $args{-type}=~s/\s//g; }
	die "Invalid type ".$args{-type}."\nValid Types Are: ".join(" ",keys(%filemap))
			unless defined $filemap{$args{-type}};
	print "get_password( type=>$args{-type} name=>$args{-name} )\n" if defined $args{-debug};
	read_passfile( -type=>$args{-type}, -debug=>$args{-debug} )
			unless defined $file_read{$args{-type}};

	if( $args{-type} eq "monitor_server" ) {		
		if( defined $args{-name} ) {
			my($srv)=$server_data{$args{-name}} || $server_data{"MONITOR.".$args{-name}};
			return undef unless $srv;
			chomp $srv;
			print "GETTING PASSWORD FOR SYBASE SERVER $srv\n" if $args{-debug};
			return get_password(-type=>"sybase",-name=>$srv);
		} else {
			my(%mon_servers);
			foreach ( keys %server_data ) {
				#print "  k=",$_," v=",$server_data{$_},"\n" if $args{-debug};
				next unless /MONITOR\./ or /HISTORICAL\./ or /^\s*$/ or /=/;
				$mon_servers{ $_ } = $server_data{$_};
			}		
			return %mon_servers;			
		}
	}
	
	if( defined $args{-name} ) {
		foreach (sort keys %login) { print "debug:",$_," ",$login{$_}," ",$password{$_},"\n" if $args{-debug} and /$args{-name}/; }
		#print "returning login/pass/type","\n" if defined $args{-debug};
		$conntype{$args{-type}.".".$args{-name}} = ucfirst($args{-type})
			if defined $conntype{$args{-type}.".".$args{-name}}
			and $conntype{$args{-type}.".".$args{-name}} eq "ODBC" and
			! defined $ENV{PROCESSOR_LEVEL};
		$conntype{$args{-type}.".".$args{-name}} = "ODBC"
			unless defined $conntype{$args{-type}.".".$args{-name}};

		print "returning(login=".  $login{$args{-type}.".".$args{-name}}," pass=",
					$password{$args{-type}.".".$args{-name}}," ctype=",
					$conntype{$args{-type}.".".$args{-name}}," )\n" if $args{-debug};
		return(  $login{$args{-type}.".".$args{-name}},
					$password{$args{-type}.".".$args{-name}},
					$conntype{$args{-type}.".".$args{-name}} );
	} else {
		# fix up some values
		$args{NOPRODUCTION}=1 if $args{-NOPRODUCTION};
		$args{PRODUCTION}=1   if $args{-PRODUCTION};
		
		# return array of values
		my(@rc);		
		foreach ( keys %login ) {
			next unless /^$args{-type}/;
			next if $args{NOPRODUCTION} and $server_data{$_.".SERVER_TYPE"}  eq "PRODUCTION";
			next if $args{NOPRODUCTION} and $server_data{$_.".SERVER_TYPE"}  eq "QA";
			next if $args{PRODUCTION} 
					and $server_data{$_.".SERVER_TYPE"}	ne "PRODUCTION" 
					and $server_data{$_.".SERVER_TYPE"} ne "DR";					
			next if $args{MONITORED} and $server_data{$_.".NOT_MONITORED"} and $server_data{$_.".NOT_MONITORED"} eq "1";
			s/^$args{-type}\.//;
			push @rc, $_;
		}
		print "returning ",join(" ",@rc),"\n" if defined $args{-debug};
		return sort @rc;
	}
}

# returns a array config_options
#sub get_passfile_info
#{
#	my(%args)=@_;
#	if( ! defined $filemap{$args{-type}} ) {
#		print "get_passfile_info() \n";
#		foreach ( keys %args ) {
#			print " key=$_ val=$args{$_} \n";
#		}
#		die "Invalid type ".$args{-type}." Passed to get_passfile_info in Repostiory.pm";
#	}
#	if( $args{-type} eq "sybase" ){
#		return( qw(server login password conntype -SERVER_TYPE -backupservername -NOT_MONITORED) );
#	} elsif( $args{-type} eq "oracle" ){
#		return( qw(server login password conntype -SERVER_TYPE) );
#	} elsif( $args{-type} eq "mysql" ){
#		return( qw(server login password conntype -SERVER_TYPE) );
#	} elsif( $args{-type} eq "unix"   ){
#		return( qw(server login password -SYBASE -SYBASE_CLIENT -IGNORE_RUNFILE -UNIX_COMM_METHOD -WIN32_COMM_METHOD -SERVER_TYPE) );
#	} elsif( $args{-type} eq "sqlsvr" ){
#		return( qw(server login password -hostname -SERVER_TYPE -NOT_MONITORED) );
#	} elsif( $args{-type} eq "win32servers" ){
#		return( qw(server DISKS) );
#	} elsif( $args{-type} eq "documenter" ){
#		return( qw(console.dat) );
#	}
#}

sub read_passfile
{
	my(%args)=@_;
	my($type)= $args{-type};
	
	my($filenm) = $filemap{$type};
	print "read_passfile( ".join(" ",%args)." )\n" if $args{-debug};
	$filenm = get_conf_dir()."/".$filenm unless -r $filenm;
	die "Cant find file $filenm" unless -r $filenm;
	print "  filename=$filenm\n" if $args{-debug};

	my(@dat);
	my($server);
	if( $type eq "sybase" or $type eq "unix" or $type eq "sqlsvr" or $type eq "oracle" or $type eq "mysql" 
	 or $type eq "shadow_sybase" or $type eq "shadow_unix" or $type eq "shadow_sqlsvr" 
	 or $type eq "shadow_oracle" or $type eq "shadow_mysql" ) {
		print "Reading Password File $filenm\n" if defined $args{-debug};
		open(CONFIGFILE,$filenm)
			|| croak("Can't open file: $filenm: $!");
		while ( <CONFIGFILE> ) {
			next if /^\s*$/ or /^\s*#/ ;
			s/\s+$//;
			chomp;
			#print "debug: Read Line $_\n" if defined $args{-debug};
			if( /^\s/ ) {
				s/^\s+//;
				my($var,$val) = split("=",$_,2);
				if( $var eq "RSH_OK" ) {	# RSH_OK depricated
					if( $val eq "Y" ) {
						$server_data{$type.".".$server.".UNIX_COMM_METHOD"} = "RSH";
					} else {
						$server_data{$type.".".$server.".UNIX_COMM_METHOD"} = "FTP";
					}
				} else {
					#print "RESETTING $type.$server.$var = $val \n";
					$server_data{$type.".".$server.".".$var} = $val;
				}
			} else {
				my(@d)=split;
				$server=$d[0];
				$login{$type.".".$server}	 	=	$d[1];
				$password{$type.".".$server}	=	$d[2];
				$conntype{$type.".".$server}	=	$d[3];
				$server_type{$server} = $type;
				if( $type eq "unix" ) {
					$server_data{$type.".".$server.".UNIX_COMM_METHOD"}  = "FTP";	# defaults
					$server_data{$type.".".$server.".WIN32_COMM_METHOD"} = "FTP";	# defaults					
				}
			}
		}
		close(CONFIGFILE);
#	} elsif( $type eq "logfiles" ) {
#		open(CONFIGFILE,$filenm) 			|| croak("Can't open file: $filenm: $!");
#		my($ky)="";
#		while ( <CONFIGFILE> ) {
#			next if /^\s*$/ or /^\s*#/ ;
#			s/\s+$//;
#			chomp;
#			if( /^\s/ ) {
#				s/^\s+//;
#				my($var,$val) = split("=",$_,2);
#				$logfile_data{$ky}->{$var} = $val;				
#			} else {
#				my(%x);
#				$ky=$_;
#				$logfile_data{$ky}=\%x;
#			}
#		}
#		close(CONFIGFILE);		
#	} elsif( $type eq "logfiles" ) {
#		# ENABLED:LOGFILETYPE:SERVERTYPE:SERVERNAME:<hostname>:<filename>
#		open(CONFIGFILE,$filenm) 			|| croak("Can't open file: $filenm: $!");
#		my($ky)="";
#		while ( <CONFIGFILE> ) {
#			next if /^\s*$/ or /^\s*#/ ;
#			s/\s//g;
#			chomp;
#			my($ENABLED,$FILETYPE,$SERVERTYPE,$SERVERNAME,$HOSTNAME,$FNAME)=split(/,/,$_);
#			my(%hash);			
#			$hash{SURVEYS_CAN_OVERWRITE}=$ENABLED;
#			$hash{FETCHFILE}=$ENABLED;
#			$hash{File_Type}=$FILETYPE;
#			$hash{Hostname}=$HOSTNAME;
#			$hash{Local_Filename}=$HOSTNAME."/".basename($FNAME);
#			$hash{Server_Name}=$SERVERNAME;
#			$hash{Server_Type}=$SERVERTYPE;	
#			$logfile_data{ $HOSTNAME.":".$FNAME } = \%hash;
#		}
#		close(CONFIGFILE);		
	} elsif( $type eq "documenter" ) {
		my(%INFO)=read_custom_defined($filenm);
		foreach my $k ( keys %INFO ) {
			$login{$type.".".$k}	 	=	"";
			# print "$k = $INFO{$k} \n";
			my(%xxx)= %{$INFO{$k}};
			foreach my $j ( keys %xxx ) {
				$server_data{$type.".".$k.".".$j} = $xxx{$j};
			}
		}
	} elsif( $type eq "monitor_server" ) {
		open(CONFIGFILE,$filenm) || croak("Can't open file: $filenm: $!");
		while ( <CONFIGFILE> ) {
			chomp;
			chomp;
			next if /^\s*$/ or /^\s*#/ ;
			my(@d)=split;			
			if( $#d == 0 ) {
				$server_data{$d[0]} = 1;			
			} else {
				$server_data{$d[0].".".$d[1]} = $d[2];			
			}
		}
		close(CONFIGFILE);
	} elsif( $type eq "win32servers" or $type eq "shadow_win32servers"  ) {
		open(CONFIGFILE,$filenm)
			|| croak("Can't open file: $filenm: $!");
		while ( <CONFIGFILE> ) {
			chomp;
			chomp;
			next if /^\s*$/ or /^\s*#/ ;
			if( /^\s/ ) {
				s/^\s+//;
				my($var,$val) = split("=",$_,2);
				$server_data{$type.".".$server.".".$var}	 	=	$val;
			} else {
				my(@d)=split;
				$server=shift @d;
				$login{$type.".".$server}	 	=	"";
				$server_data{$type.".".$server.".DISKS"} = join(" ",@d);
			}
		}
		close(CONFIGFILE);
	} else {
		die "Invalid Password File Type=$type\n";
	}
	$file_read{$type}=1;
}

# -type
# -name
sub get_password_info
{
	my(%args)=@_;
	croak "Must pass -name to get_password_info" unless defined $args{-name};
	$args{-type} = $server_type{$args{-name}} unless defined $args{-type};
	croak "Must pass -type to get_password_info()" unless defined $args{-type};

	read_passfile( -type=>$args{-type}, -debug=>$args{-debug} )
			unless defined $file_read{$args{-type}};

	my($key)=$args{-type}."\\.".$args{-name}."\\.";
	my(%INFO);
	my($found)=0;
	foreach ( keys %server_data ) {
		next unless /^$key/;
		my($v)=$_;		
		s/^$key//;
		$INFO{$_} = $server_data{$v};
		$found++;
	}
	$INFO{NOT_FOUND}=1 unless $found;
	if( ! $found and $args{-debug} ) {		
		print "Warning: No info Found for $key\n";
		foreach ( keys %server_data ) { print "ok key=$_ \n"; }
	}
	return %INFO;
}

#sub get_password_info_raw { 
#	read_passfile( -type=>$args{-type}, -debug=>$args{-debug} )
#			unless defined $file_read{$args{-type}};
#	return %server_data; 
#}

sub read_custom_defined
{
	my($filenm)=@_;

	# the output hashes
	my(%custom_report_name);	# key=number (from $count)
	my(%custom_ignore_msgs );	# ignore messages
	my(%custom_report_type, %custom_report_server, %custom_report_query, %custom_report_database);
	my(%next,%last);
	my(%used_names);				# used names

	my($count)=0;
	my($tmplast)="";

	open(USERDEF,$filenm) or return;
	while ( <USERDEF> ) {
		next if /^\s*$/ or /^\s*#/;
		chomp;
		chomp;
		my(@fields)=split(/\s+/,$_,4);

		if( $fields[0] eq "IGNORE" ) {
			$custom_ignore_msgs{$fields[1]." ".$fields[2]}=$fields[3];
			next;
		};

		if( defined $used_names{$fields[0]} ) {
			print "Warning: Custom Report Previously Defined By Same Name\n";
			print "This Report Shall Be Ignored\n";
			next;
		} else {
			$used_names{$fields[0]}=1;
		}

		$custom_report_name{$count} = $fields[0];
		if( $fields[1] 	=~ /^cmd/i
			or $fields[1] =~ /^file/i
			or $fields[1] =~ /^html/i
			or $fields[1] =~ /^sql/i ) {

			$custom_report_type{$count} = $fields[1];
			$custom_report_server{$count} = "";
			$custom_report_database{$count} = "";

			if( $fields[1] =~ /^file/i ) {
				$custom_report_server{$count} = $fields[2];
				shift @fields;
			}
			shift @fields;
			shift @fields;
			$custom_report_query{$count} = join(" ",@fields);

			# ok... set up %next and %last
			if( $tmplast != "" ) {
				$next{$tmplast} = $fields[0].".html" ;
				$last{$fields[0].".html"} = $tmplast;
			}
			$tmplast = $fields[0].".html";

		} else {
			# key number 1 is server

			$custom_report_server{$count} = $fields[1];
			if( $fields[2] =~ /null/i ) {
				$custom_report_database{$count} = "";
			} else {
				$custom_report_database{$count} = $fields[2];
			}
			$custom_report_query{$count} = $fields[3];

		}
		$count++;
	}
	close USERDEF;

	my(%info);
	$info{custom_report_name} = \%custom_report_name;
	$info{custom_ignore_msgs } = \%custom_ignore_msgs;
	$info{custom_report_type} = \%custom_report_type;
	$info{custom_report_server} = \%custom_report_server;
	$info{custom_report_query} = \%custom_report_query;
	$info{custom_report_database} = \%custom_report_database;
	$info{next} = \%next;
	$info{last} = \%last;
	return(%info);
}

# add_edit_item() - is the api to modify & edit the configfiles
# 
# required arguments: -op -type -name -values (unless op=del) = hashptr
# optional arguments: -login -password -debug
#
# -type => type of the configfile
#       => allowed values; sybase, unix, sqlsvr, oracle, win32servers, documenter, monitor_server, logfiles			
#
# if -op = add will add or update as needed
# if -op = update then will update exiting keys based on -value
#
# if -name
#	-name and -del will delete server - NOTE -name can be a string or arrayptr in which it will delete a bunch
#	-name and -conntype will set conntype
#
# -values => a hashptr that contains key value pairs
#         => unneeded if op = -del
#         => otherwise keys are [system/server]|[key] and values are the values to put in that key
# To Delete a key - run with op=update and set value={emptystring}
# EXAMPLE
#
# # update 2 SERVER_TYPES in the unix password file 
# my(%x);
# $x{host1|SERVER_TYPE}=PRODUCTION;
# $x{host2|SERVER_TYPE}=DEVELOPMENT;
# add_edit_item(-op=>update, -type=>unix, -values=>\%x);
#
# # update disks on windows server mywin32
# $x{$host."|DISKS"}="c$ d$ e$"
# add_edit_item(-op=>update, -type=>win32servers, -values=>\%x);
#
sub add_edit_item {
	my(%args)=@_;
	my($type)	= $args{-type};
	my($filenm)	= $filemap{$type};

	if( defined $args{-debug} ) {
		print "Repository.pm add_edit_item() called in Debug Mode\n";
		use Data::Dumper;
		print Dumper \%args;
	}

	$filenm = get_conf_dir()."/".$filenm unless -r $filenm;
	die "Cant find file $filenm" unless -r $filenm;

	read_passfile( -type=>$type, -debug=>$args{-debug} ) 	unless defined $file_read{$type};

	my(@dat);
	my($server);
	if( $type eq "sybase" or $type eq "unix" or $type eq "sqlsvr" or $type eq "oracle" 
			or $type eq "mysql" or $type eq "win32servers" ) {
				
		print "Reading Password File $filenm\n" if defined $args{-debug};
		open(CONFIGFILE,$filenm) || croak("Can't open file: $filenm: $!");
		my(@x)= <CONFIGFILE>;
		close(CONFIGFILE);
		if( $args{-op} eq "update" ) {
		
			my($hash_override)=$args{-values};	# key= server.val
			die "Must pass -values" unless $hash_override;			
			
			# if( defined $args{-debug} ) {	foreach ( keys %$hash_override ) {	print "add_edit_item(key=$_ val=$$hash_override{$_})\n";}}
			
			print "Writing Password File $filenm\n";# if defined $args{-debug};
			open(WRITEIT,">".$filenm)	|| croak("Can't write file: $filenm: $!");
			my($cursvrname)="";
			my(%written_items);
			foreach my $line (@x) {
				$line =~ s/\s+$//;					# remove trailing spaces
				chomp $line; #yum
								
	 			if( $line =~ /^\s*\#/ ) {			# that would be a comment 
	 				print WRITEIT $line,"\n";
	 			} elsif( $line !~ /^\s/ ) { 		# NEW SERVER LINE	 		
	 				
	 
	 				$line =~ s/^\s+//;				# remove leading spaces
	 				my($s,$l,$p,$c)=split(/\s+/,$line);
	 				$cursvrname=$s;
					
										# credential retrieval
	 									# can come from -login/-password
	 									# can come from sqlserver
	 									# can come from LOGIN or PASSWORD values
	 									
	 				my($login) = $args{-login} || $$hash_override{$cursvrname."|LOGIN"} || $l;
	 				$login = 'sa' 		if ! $login and $type eq "sqlsvr";
	 				$login = 'sybase' if ! $login and $type eq "unix";
	 				# print "DBG DBG: $type LOGIN IS $login\n";
	 				
	 				my($password) = $args{-password} || $$hash_override{$cursvrname."|PASSWORD"} || $$hash_override{$cursvrname."|PASSWD"} ||$p;
	 				$password = 'xxx' if ! $password and $type eq "sqlsvr";
	 				$password = 'xxx' if ! $password and $type eq "unix";
	 				
	 				#
	 				# SET SOME DEFAULTS
	 				#	 				
	 				#$$hash_override{$cursvrname."|UNIX_COMM_METHOD"} = "SSH" 
	 				#	if $type eq "unix" and ! $$hash_override{$cursvrname."|UNIX_COMM_METHOD"};
	 				#$$hash_override{$cursvrname."|WIN32_COMM_METHOD"} = "NONE" 
	 				#	if $type eq "unix" and ! $$hash_override{$cursvrname."|WIN32_COMM_METHOD"};
	 				#$$hash_override{ $cursvrname."|SERVER_TYPE" } = "PRODUCTION" 
	 				#	unless defined $$hash_override{$cursvrname."|SERVER_TYPE"};
	 				
	 				$written_items{$cursvrname."|LOGIN"}=1; 
	 				$written_items{$cursvrname."|PASSWORD"}=1; 
	 				$written_items{$cursvrname."|PASSWD"}=1; 
	 				
	 				print WRITEIT $cursvrname."\t".$login."\t".$password."\t".$c."\n";
	 				
#	 				xxx
#	my($key)=$args{-type}."\\.".$args{-name}."\\.";
#	my(%INFO);
#	my($found)=0;
#	foreach ( keys %server_data ) {
#		next unless /^$key/;
#		my($v)=$_;		
#		s/^$key//;
#		$INFO{$_} = $server_data{$v};
#		$found++;
#	}
#	$INFO{NOT_FOUND}=1 unless $found;
#	if( ! $found and $args{-debug} ) {		
#		print "Warning: No info Found for $key\n";
#		foreach ( keys %server_data ) { print "ok key=$_ \n"; }
#	}
#	return %INFO;
#	 				xxx
	 				# write any new keys from the hash
	 				foreach my $vkey ( sort keys %$hash_override ) {
						next if $written_items{$vkey};	
						my($k,$v)=split(/\|/, $vkey,2);
						next if $k ne $cursvrname;
						# new key - its not written so write it!
						$written_items{$vkey} = 1;
						if( defined $$hash_override{$vkey} ) {
							print "WRITING\t$v=".$$hash_override{$vkey},"\n" if $args{-debug};	
	 						print WRITEIT  "\t$v=".$$hash_override{$vkey},"\n";	
		 				} else {
		 					print "NOT WRITING\t$v=".$$hash_override{$vkey},"\n" if $args{-debug};	
		 				}
	 					
					}			
	 						
	 			} elsif( $line =~ /\=/ ) {			# Existing Key/Value pair line	 				
	 				my($k,$v)=split(/=/,$line,2);
	 				$k=~s/^\s+//;
	 				my($key)=$cursvrname."|".$k;	 				
	 				if( ! $written_items{$key} ) {
		 				$written_items{$key} = 1;	 					
		 				if( defined $$hash_override{$key} ) {
		 					print WRITEIT  "\t$k=".$$hash_override{$key},"\n";
		 					print  "\t$k=".$$hash_override{$key},"\n" if $args{-debug};
		 				} else {
		 					print WRITEIT $line,"\n";	
		 				}
		 			}
	 			}	 			
	 		}
	 		
	 		# APPEND ANY UNWRITTEN STUFF TO THE BOTTOM	 			
	 		$cursvrname='notpossible';
			foreach my $vkey ( sort keys %$hash_override ) {
				next if $written_items{$vkey};	
				# print __LINE__,"processing VKEY=$vkey\n"; 			
				my($k,$i)=split(/\|/, $vkey,2);
				if( $k ne $cursvrname ) {			# new server					
					
					$cursvrname = $k;

					my($login) = $args{-login} || $$hash_override{$cursvrname."|LOGIN"};
	 				$login = 'sa' if ! $login and $type eq "sqlsvr";
	 				$written_items{$cursvrname."|LOGIN"}=1; 
	 				# print __LINE__," marking $cursvrname|LOGIN ($login,$type)\n"; 			
	 				
	 				my($password) = $args{-password} 
	 					|| $$hash_override{$cursvrname."|PASSWORD"}  
	 					|| $$hash_override{$cursvrname."|PASSWD"};
	 				$password = 'xxx' if ! $password and $type eq "sqlsvr";
	 				$written_items{$cursvrname."|PASSWORD"}=1; 
	 				$written_items{$cursvrname."|PASSWD"}=1; 
	 				# print __LINE__," marking $cursvrname|PASSWORD ($login,$type)\n"; 			
	 				# print __LINE__," marking $cursvrname|PASSWD ($login,$type)\n"; 			
	 				
	 				# print "DIAG: NEW SERVER ADD $k $login $password - vkey=$vkey\n"; 	 					 			
					print WRITEIT $k."\t".$login."\t".$password."\n";
					next if $i eq "PASSWD" or $i eq "LOGIN" or $i eq "PASSWORD";
				} 
					
				# print "DIAG: $vkey NEW VALUE $i = $$hash_override{$vkey} \n";
				print WRITEIT "\t",uc($i),"=",$$hash_override{$vkey},"\n" 
					unless $$hash_override{$vkey} =~ /^-/ or $$hash_override{$vkey} =~ /^\s*$/;
			}
	 		
	 		close(WRITEIT);
	 		print "... Finished modification of $filenm \n"	if defined $args{-debug};
	 	
	 	} elsif( $args{-op} eq "add" ) {
	 		if( $type eq "sqlsvr" ) { 	# native auth defaults
				$args{-login}='sa' 		unless $args{-login};
				$args{-password}='xxx' 	unless $args{-password};
			}
			# get list of servers
			my(%svrhash);
			foreach ( get_password(-type=>$args{-type}) ) {	$svrhash{$_}=1; }
			
			print "Appending Password File $filenm\n" if defined $args{-debug};
			open(WRITEIT,">>".$filenm) || croak("Can't write file: $filenm: $!");
			
			my($newservernm)='notpossible';
			foreach my $vkey ( sort keys %{$args{-values}} ) {
				my($k,$i)=split(/\|/, $vkey);
				die "CANT ADD SERVER $k - IT EXISTS " if $svrhash{$k};
				if( $k ne $newservernm ) {
					print WRITEIT $k."\t".$args{-login}."\t".$args{-password}."\n";			
					$newservernm = $k;
				}
				print WRITEIT "\t",uc($i),"=",$args{$vkey},"\n" unless $args{$vkey} =~ /^-/;
			}
			close(WRITEIT);
			
		} elsif($args{-op} eq "del" ) {
			print "Writing Password File $filenm\n" if defined $args{-debug};
			die "NO -name Passed " unless $args{-name};
			my(%serverstodelete);
			if( ref $args{-name} ) {
				foreach ( @{$args{-name}} ) {
					$serverstodelete{$_}=1;
				}
			} else {
				$serverstodelete{	$args{-name}}=1;
			}
			if( $args{-debug} ) { print Dumper \%serverstodelete; }
			
			open(WRITEIT,">".$filenm) || croak("Can't write file: $filenm: $!");
			my($currentserver)="impossible x";
			print "... Searching for $args{-name} \n" if defined $args{-debug};
			foreach (@x) {
				chomp; chomp;
				if( /^\w/ ) {
					my($svr,@dat)=split(/\s+/,$_);
					$currentserver = $svr;					
				}
				print "deleting line $_\n" if $args{-debug} and $serverstodelete{$currentserver};
				next if $serverstodelete{$currentserver};
				print WRITEIT $_,"\n";
			}			
			close(WRITEIT);
			print "... Finished modification of $filenm \n"	if defined $args{-debug};
		} else {
			warn "CAN NOT PROCESS - no -op argument $args{-op}\n";
		}
	} elsif( $type eq "documenter" ) {
		my(%INFO)=read_custom_defined($filenm);
		foreach my $k ( keys %INFO ) {
			$login{$type.".".$k}	 	=	"";
			# print "$k = $INFO{$k} \n";
			my(%xxx)= %{$INFO{$k}};
			foreach my $j ( keys %xxx ) {
				$server_data{$type.".".$k.".".$j} = $xxx{$j};
			}
		}		
#	} elsif( $type eq "win32servers" ) {
#		die "Cant write win32servers - must debug this code but it should be the same as the other password files now\n";
#		print "Opening $filenm\n" if defined $args{-debug};
#		open(CONFIGFILE,$filenm) || croak("Can't open file: $filenm: $!");
#		my(@x)= <CONFIGFILE>;
#		close(CONFIGFILE);
#		
#		# CAN TAKE -name & -DISKS or the more normal -values=\%x where %x is formatted $x{$host."|DISKS"}="c$ d$ e$"
#		my($hash_override)=$args{-values};
#		my(%updated_server_diskinfo);
#		if( $hash_override ) {
#			foreach ( keys %$hash_override ) {
#				my($k,$i)=split(/\|/);
#				$updated_server_diskinfo{$k}=$$hash_override{$_} if $i eq "DISKS";
#			}
#		} else {
#			$updated_server_diskinfo{$args{-name}}=$args{DISKS};
#		}
#		
#		my(@outfile_lines);
#		foreach ( @x ) {
#			s/\s+$//;
#			chomp;
#			if( /^\s*$/ or /^\s*#/ ){
#				push @outfile_lines,$_;
#				next;
#			}
#			my(@d)=split;
#			my($server)=shift @d;
#			next if $updated_server_diskinfo{$server};			
#			push @outfile_lines,$_;
#		}
#		
#		unless( $args{-op} eq "del" ) {
#			foreach my $server (keys %updated_server_diskinfo ) {
#				$login{$type.".".$server}	 	=	"";
#				$server_data{$type.".".$server.".DISKS"} = $updated_server_diskinfo{$server};
#				push @outfile_lines,$server." ".$updated_server_diskinfo{$server};
#			}
#		}
#		
#		#if( $args{-name}) {}
#		#	print WRITEIT $args{-name}." ".$args{DISKS}."\n" unless $args{-op} eq "del";
#		#	$login{$type.".".$args{-name}} =	"";
#		#	$server_data{$type.".".$args{-name}.".DISKS"} = $args{DISKS};
#		#}
#		print "Writing Password File $filenm\n"; # if defined $args{-debug};
#		open(WRITEIT,">".$filenm) || croak("Can't write file: $filenm: $!");
#		foreach(@outfile_lines) {
#			next if /^\s*$/;
#		#	print $_,"\n";
#			print WRITEIT $_,"\n";
#		}
#		close(WRITEIT);
	} else {
		die "Invalid Password File Type=$type\n";
	}

	print "... Marking $type information unread - it will be refetched.\n" if defined $args{-debug};
	undef $file_read{$type};
}

sub read_threshfile {
	my($THRESHFILE,$program)=@_;

	if( ! $THRESHFILE ) {
		$THRESHFILE = get_conf_dir()."/threshold_overrides.dat"
			if -r get_conf_dir()."/threshold_overrides.dat";
		return unless $THRESHFILE;
	}

	my(%warnthresh, %alarmthresh, %critthresh);
	
	if( ! -r $THRESHFILE and -r "$THRESHFILE.sample" ) {
		use File::Copy;
		output("Copying Sample File To $THRESHFILE\n");
		copy "$THRESHFILE.sample",$THRESHFILE;
	}
		
        if( -r $THRESHFILE ) {
      		#output("Reading $THRESHFILE\n");
         	open (TR, $THRESHFILE ) or die "Cant open $THRESHFILE\n";
                while (<TR>) {
                   chomp;
                   chomp;
                    #SERVERNM,ThresholdMonitor,CRITICAL,99,dbname
                   next if /^#/ or /^\s*$/;
                   next unless /,$program,/;
                   
                   my($svr,$prog,$state,$pct,$subsys)=split(/,/);
                   $svr=~s/\s//g;
                   $state=~s/\s//g;
                   undef $subsys if $subsys =~ /^\s*$/;
                   #output("   Setting $state threshold for system $svr to $pct \n");
                   if( ! $subsys ) {
	                $warnthresh{$svr}=$pct 	if $state=~/WARNING/i;
                   	$alarmthresh{$svr}=$pct 	if $state=~/ERROR/i;
                   	$critthresh{$svr}=$pct 	if $state=~/CRITICAL/i;
                   } else {
                     $warnthresh{$svr.":".$subsys}=$pct 	if $state=~/WARNING/i;
                   	$alarmthresh{$svr.":".$subsys}=$pct 	if $state=~/ERROR/i;
                   	$critthresh{$svr.":".$subsys}=$pct 	if $state=~/CRITICAL/i;
               	  }
                }
         	close(TR);
      }
      return(\%warnthresh, \%alarmthresh, \%critthresh);	
}

#sub write_conf_file_dat {
#	my($file, @dat)=@_;
#	my($gem_root_dir)=get_gem_root_dir();
#	die "No Gem Root Dir" unless -d $gem_root_dir;
#	die "No $gem_root_dir/conf/$file.dat File Found\n" unless -e "$gem_root_dir/conf/$file.dat";
#	die "conf/$file.dat File Is Not Readable\n" unless -r "$gem_root_dir/conf/$file.dat";
#	die "conf/$file.dat File Is Not Writable\n" unless -w "$gem_root_dir/conf/$file.dat";
#	open( aaR, "$gem_root_dir/conf/$file.dat.sample" ) 
#		or die "Cant Read $gem_root_dir/conf/$file.dat.sample\n";
#	open( aaW, "> $gem_root_dir/conf/$file.dat" ) 
#		or die "Cant Write $gem_root_dir/conf/$file.dat\n";
#	while(<aaX>) {
#		print aaW $_ if /^#/;
#	}		
#	
#	foreach (@dat) { chomp; print aaW $_,"\n"; }	
#	
#	close(aaR);
#	close(aaW);
#}

sub get_conf_file_dat {
	my($file)=@_;
	my($gem_root_dir)=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gem_root_dir;
	die "No $gem_root_dir/conf/$file.dat File Found\n" unless -e "$gem_root_dir/conf/$file.dat";
	die "conf/$file.dat File Is Not Readable\n" unless -r "$gem_root_dir/conf/$file.dat";
	open( aaX, "$gem_root_dir/conf/$file.dat" ) 
		or die "Cant Read $gem_root_dir/conf/$file.dat\n";
	my(@rc2);
	while(<aaX>) {
		next if /^#/ or /^\s*$/;
		s/\s+$//;		
		chomp;		
		push @rc2, $_;
	}		
	close(aaX);
	return @rc2;
}

# returns a pointer to an array of file items
# this function replaces
# 	my($dat)=$gem_data->get_set_data(-class=>'unix',-name=>$unixhost,-arg=>'discovered_files');
# with
# 	my($dat)=get_logfiles(-hostname=>$unixhost);
# $dat is null if no data found
sub get_logfiles {
	my(%args)=@_;
	die "NO  -type passed to get_logfiles()" unless defined $args{-filetype};
	die "BAD -type" unless $args{-filetype};
	my(@dbs)=get_password(-type=>$args{-filetype});
	my(%rc);	
	foreach (@dbs) {
		my(%info)=get_password_info(-name=>$_, -type=>'sybase');
		if( $info{BACKUPSERVERLOG} ) {
			my(%hash);		# much of this is for backwards compatibility with nothing that i know of
			$hash{SURVEYS_CAN_OVERWRITE}="YES";
			$hash{FETCHFILE}="YES";
			$hash{File_Type}="ASE_BACKUP_SERVER";
			$hash{Hostname}=$info{HOSTNAME};
			$hash{Local_Filename}=$info{HOSTNAME}."/".basename($info{BACKUPSERVERLOG});
			$hash{Server_Name}=$_;
			$hash{Server_Type}="DBServer";	
			$rc{ $info{HOSTNAME}. ":" . $info{BACKUPSERVERLOG} } = \%hash;
		}
		if( $info{ERRORLOG} ) {
			my(%hash);		# much of this is for backwards compatibility with nothing that i know of
			$hash{SURVEYS_CAN_OVERWRITE}="YES";
			$hash{FETCHFILE}="YES";
			$hash{File_Type}="ASE_SERVER_LOG";
			$hash{Hostname}=$info{HOSTNAME};
			$hash{Local_Filename}=$info{HOSTNAME}."/".basename($info{ERRORLOG});
			$hash{Server_Name}=$_;
			$hash{Server_Type}="DBServer";	
			$rc{ $info{HOSTNAME}. ":" . $info{ERRORLOG} } = \%hash;
		}
		
		close(CONFIGFILE);	
	}		
	return \%rc;
}
	
#	if( ! $logfile_was_read ) {
#		read_passfile(-type=>'logfiles');
#		$logfile_was_read="READ";
#	}
#	my(%args)=@_;			# if passed 
#	my(%rc);	
#	foreach my $hst_file ( keys %logfile_data ) {
#		if( defined $args{-hostname} ) {
#			my($sys,$file)=split(/:/,$hst_file);
#			next if $args{-hostname} and $sys      ne $args{-hostname};
#			next if $args{-key}      and $hst_file ne $args{-key};
#		} 
#		$rc{$hst_file} = $logfile_data{$hst_file};		
#	}
#	return \%rc;
#}

#sub set_logfiles {
#	my(%args)=@_;	
#	die "Barlow Must Rewrite set_logfiles()\n";
#	if( ! $logfile_was_read ) { read_passfile(-type=>'logfiles');	}		
#	$logfile_was_read="UPDATED";
#	
#	if( $args{-ALLFILES} ) {
#		my(%inhash)=%{$args{-ALLFILES}};
#		foreach my $filekey ( keys %inhash ) {
#			my(%rowdata)=%{$inhash{$filekey}};						
#			$logfile_data{$rowdata{Hostname}.":".$filekey} = \%rowdata;
#			print "DBG DBG set_logfiles( key=$filekey host=$rowdata{Hostname}) \n";	
#			foreach ( keys %rowdata ) { print " DBG DBG set_logfiles $_ => $rowdata{$_} \n"; }
#		}
#		return;
#	}
#	my($str)="DBG DBG set_logfiles(";foreach (keys %args) { $str.="$_=>$args{$_},"; } chop $str; print $str,")\n";	
#	
#	croak "Must pass -name and -filename args to set_logfiles"	unless $args{-name} and $args{-filename};
#		
#	if( $args{-DELETE} ) { 
#		undef $logfile_data{$args{-name}.":".$args{-filename}}; 
#		return; 
#	};
#	
#	my(%x);
#	foreach ( keys %args ) {
#		next if $_ eq "-name";
#		next if $_ eq "-filename";
#		$x{$_} = $args{$_};
#	}
#	$logfile_data{$args{-name}.":".$args{-filename}} = \%x;
#}

#sub save_logfiles {
#	my(%args)=@_;
#	# my($str)="DBG DBG save_logfiles(";foreach (keys %args) { $str.="$_=>$args{$_},"; } chop $str; print $str,")\n";;
#	# print "DBG DBG: $logfile_was_read\n";
#	return unless $logfile_was_read eq "UPDATED";
#
#	my($filenm) = $filemap{logfiles};
#	$filenm = get_conf_dir()."/".$filenm unless -r $filenm;
#	die "Cant find file $filenm" unless -r $filenm;
#	print "  filename=$filenm\n" if $args{-debug};	
##print "DBG DBG WRITING $filenm\n";
#	open(CONFIGFILE,">".$filenm) 			|| croak("Can't write file: $filenm: $!");
#
#	print CONFIGFILE "# files.dat
##
## files.dat lists the for your systems.  This is the list of files you wish 
## to routinely copy locally from your remote servers.  The FORMAT is 
## similar to the other GEM configuration files. 
## 
## <hostname>:<filename>
## \tkey=value
##
## The file has special operational directives of which you need to be aware
##
## a) CAN SURVEYS OVERWRITE THE VALUES
##    SURVEYS_CAN_OVERWRITE=YES
## 	  surveys overwriting the data only if the line is EXACTLY as per above
##    so you comment it out or set it to NO to preclude that
##
## b) SHOULD THE FILE BE FETCHED
##    FETCHFILE=YES
## 	  the file will be fetched only if the line is EXACTLY as per above
##    so you comment it out or set it to NO to preclude that
#";
#	my($dat)=get_logfiles();
#	next if ! defined $dat;
#	my(%dathash)=%$dat;
#	foreach my $kk ( sort keys %dathash ) {
#		print CONFIGFILE "$kk\n";
#		print CONFIGFILE "\tSURVEYS_CAN_OVERWRITE=YES\n" unless $dathash{$kk}->{SURVEYS_CAN_OVERWRITE};
#	   print CONFIGFILE "\tFETCHFILE=YES\n" unless $dathash{$kk}->{FETCHFILE};
#		foreach ( sort keys %{$dathash{$kk}} ) {
#			print CONFIGFILE "\t$_=$dathash{$kk}->{$_}\n";
#		}	
#	}
#	close(CONFIGFILE);
#}

sub get_console_info {
	my($file)=@_;
	my(%outhash);
	my(@rc)=get_conf_file_dat($file);
	my($jobname)="";
	foreach (@rc) {
		if(/^\s/) {
			s/^\s+//;
			die "NO : in LINE $_\n" unless /:/;
			my($k,$v)=split(/:/,$_,2);
			$v=~s/^\s+//;
			$outhash{$jobname}->{$k}=$v;
		} else {
			if( /^FILE=/ ) {
				s/^FILE=//;
				$jobname=$_;
				my(%x);
				$outhash{$jobname}=\%x;
			} else {
				die "CANT PARSE LINE $_\n";
			}
		}
	}
	
	# use Data::Dumper;
	# print Dumper \%outhash;
	return %outhash;
}

1;

__END__

=head1 NAME

Repository.pm - Customizable Server/Password Repository

=head2 SYNOPSIS

	# Get Password For Particular Server
	($login,$password)=get_password(-type=>"sybase", -name=>"SYBPROD")

	# Work on all Sybase Servers
	use Repository;
	@servers=get_password(-type=>xxx,-name=>undef)
	foreach (@servers) {
		($login,$password)=get_password(-type=>xxx, -name=>$_)
		...
	}

	# Get Unix Servers
	use Repository;
	@hosts=get_password(-type=>"unix", -name=>undef)

	# Get information on particular server
	%info_about_servers=get_password_info($server)

=head2 DESCRIPTION

This basic module should be extended to allow encryption.  For now it
allows you to get passwords from a file for your application through a standard
interface that can be enhanced to meet your security concerns.

Two files are required for this module to work.  The first file is a
configuration file that identifies the password information.  This file
must exist in the current directory (checked first),  or in one of the
directories in your perl library include path.  The file name is
sybase_password.cfg by default.

The password file is user definable but needs to follow the following rules:

	lines starting with # are ignored
	lines starting with any white space are informational and can be
		returned with the get_password_info function.

Normally, the password file should be named password.cfg in the current
directory and have the format

SERVER<spaces>PASSWORD<spaces>LOGIN
<spaces>KEY=VALUE
<spaces>KEY=VALUE
SERVER<spaces>PASSWORD<spaces>LOGIN
<spaces>KEY=VALUE
<spaces>KEY=VALUE

If your password file is of a different format, or you are using a differently
named password file, you need to define a configuration file (default
sybase_password.cfg).  This file should be of the format:

	PASSFILE=passwords
	SERVER_COL=0
	LOGIN_COL=1
	PASS_COL=2

get_password_info returns lines that start with spaces

=head2 LOG FILES

get_logfiles()

	reads info from files.dat 
	returns a pointer to an array of hashes of file items 
	this function replaces
		my($dat)=$gem_data->get_set_data(-class=>'unix',-name=>$unixhost,-arg=>'discovered_files');
	with
		my($dat)=get_logfiles(-hostname=>$unixhost);
		foreach (@$dat) { print "HOST:FILENAME = $_; }
	takes -hostname - in which case it only returns data for the hostname

=HEAD3 Example 

use of get_logfiles to find all interfaces files on a host... From interfaces_file_rpt.pl

my($dat)=get_logfiles(-hostname=>$unixhost);
next unless defined $dat;
foreach ( keys %$dat ) {
	if( ref $$dat{$_} ) {
		if( $$dat{$_}{File_Type} eq "Interfaces File" ) {
			print " --> key => $_\n" if $DEBUG;
			my($h,$f)=split(/:/,$_);
			$$dat{$_}{name}=$f;
			push @INTERFACES_FILES,$$dat{$_};
		}
	} else {
		if( $$dat{File_Type} eq "Interfaces File" ) {
			print " --> key => $_ SINGLETON\n" if $DEBUG;
			print "DBG: SINGLETON FOR Host $unixhost\n" if defined $DEBUG and ! defined $PORTLIST;
			push @INTERFACES_FILES,$dat;
			last;
		}
	}
}
	

=head2 EDITING CONFIG FILES

add_edit_item() - is the api to modify & edit the configfiles
 
 required arguments: -op -type -name -values (unless op=del) = hashptr
 optional arguments: -login -password -debug

 -type => type of the configfile
       => allowed values; sybase, unix, sqlsvr, oracle, win32servers, documenter, monitor_server, logfiles			

 if -op = add will add or update as needed
 if -op = update then will ad or update exiting keys based on -value
 			To Delete a key - run with op=update and set value={emptystring}
 			This will not delete any servers

 if -name
	-name and -del will delete server 
	-name and -conntype will set conntype

 -values => a hashptr that contains key value pairs
         => unneeded if op = -del
         => otherwise keys are [system/server]|[key] and values are the values to put in that key

 EXAMPLE = update 2 SERVER_TYPES in the unix password file 
 
 my(%x);
 $x{host1|SERVER_TYPE}=PRODUCTION;
 $x{host2|SERVER_TYPE}=DEVELOPMENT;
 add_edit_item(-op=>update, -type=>unix, -values=>\%x);

 EXAMPLE = update disks on windows server mywin32
 $x{$host."|DISKS"}="c$ d$ e$"
 add_edit_item(-op=>update, -type=>win32s

=head2 MONITOR SERVERS

Sybase Monitor servers behave slightly differently.  These servers are stored in monitorserver.dat and represent
a map of monitor server -> sybase server.  When you run get_password(-type=>monitor_server) you get a hash with
keys of MONITOR.$MON_SERVER or HISTORICAL.$HIST_SERVER and values of the ASE Sybase SQL Server associated with the
monitor server. When you run get_password(-type=>monitor_server,-name=XXX_MON) you recieve the login and password information
for the sybase server associated with that monitor server...


=cut


