#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use     	strict;
use     	File::Basename;
use     	LogFunc;
use 		Do_Time;
use 		Getopt::Long;
use 		MlpAlarm;
use 		Sys::Hostname;
use 		Repository;
use 		CommonHeader;

my($VERSION)=1.0;
my($PROGNAME)  ="iq_logmon.pl";
my($HB_MONITOR)="IQLogmon";
my($FILE_DATE_FMT)="mm/dd";

my(@ddd)=localtime(time);
my($YEAR)=1900+$ddd[5];
use vars qw( $DEBUG $NOSAVEPATFILE $PATFILE $INFILE $HTML $TODAYONLY $OUTFILE $SYSTEM $NOLOG  );

$|=1;
my(%catsum,%catmsg);
sub usage
{
	return "Usage: $PROGNAME --NOSAVEPATFILE --TODAYONLY --HTML --OUTFILE=file --DEBUG --NOLOG [--PATFILE=file] --SYSTEM=host --INFILE=file\n";
}

die "Bad Parameter List $!\n".usage() unless
   GetOptions(	"INFILE=s"	=> \$INFILE,
		"PATFILE=s"	=> \$PATFILE,
		"SYSTEM=s"	=> \$SYSTEM,
		"TODAYONLY"	=> \$TODAYONLY,
		"NOSAVEPATFILE" => \$NOSAVEPATFILE,
		"NOLOG" 	=> \$NOLOG,
		"HTML" 		=> \$HTML,
		"DEBUG"		=> \$DEBUG );

die usage("Must pass INFILE\n" ) unless defined $INFILE;

my($nl)="\n";
$nl="<br>\n" if $HTML;
print $PROGNAME," version $VERSION",$nl;
print "Run on host ",hostname()," at ".localtime(time).$nl;
MlpBatchJobStart(-BATCH_ID=>$HB_MONITOR ) unless defined $NOLOG;
$PATFILE=get_gem_root_dir()."/data/patern_files/$HB_MONITOR.pat" unless $PATFILE;

my($tmpfile);
if( defined $OUTFILE ) {
	$tmpfile=get_tempfile($OUTFILE);
	print "Temporary Output File : $tmpfile\n";
	print "Results will be copied to $OUTFILE\n";
	die "ERROR: Cant write to $OUTFILE\n" if -e $OUTFILE and ! -w $OUTFILE;
	open(OUTFILE,">$tmpfile" ) or error_out( "Cant write $tmpfile \n" );
}

# ============================
# get start patern
# ============================
my($byte_count,$start_patern)=(0,"");
if( defined $PATFILE and -e $PATFILE ) {
	pr_output( "  Patern File: ".$PATFILE."$nl" );
	($byte_count,$start_patern) = log_rd_patern_file(file=>$PATFILE);
	my($patfiletime)= -M $PATFILE;
	$patfiletime *= 24;
	my($patfilestr) = "";
	if( $patfiletime >= 1 ) {
		$patfilestr .= int($patfiletime)." hours ";
		$patfiletime -= int($patfiletime);
	}
	$patfiletime *= 60;
	$patfilestr.= " ".int($patfiletime)." minutes";
	pr_output( "  Report Filter : Only Include Messages In The Past ",$patfilestr,$nl );
	pr_output( "  PATERN FILE byte count : $byte_count   ",$nl ) if defined $DEBUG;
	pr_output( "  PATERN FILE startpat   : $start_patern ",$nl ) if defined $DEBUG;
} elsif( defined $PATFILE )  {
	pr_output( "  Patern File: ".$PATFILE." (NOT FOUND)",$nl );
}

if( defined $TODAYONLY ) {
	pr_output( "  Time Restriction: Today and Yesterday",$nl );
	log_grep(search_patern=>undef,today=>1,yesterday=>1,date_fmt=>$FILE_DATE_FMT);
} else {
	pr_output( "  Time Restriction: none",$nl );
	log_grep(search_patern=>undef,today=>undef,yesterday=>undef,date_fmt=>$FILE_DATE_FMT);
}

if( defined $DEBUG ) {
	pr_output( "  Debug Mode=SET",$nl );
	log_debug();
}

my($max_lines)=100000000;
$max_lines = 100000 if defined $TODAYONLY;
pr_output( "** PROCESSING FILE **",$nl ) if defined $DEBUG;
my($n_read,$n_returned,$found_pat,$returned_pat,$byte_cnt,@output_r)=
	log_process_file(
		file		=> $INFILE,
		search_patern	=> $start_patern,
		byte_offset	=> $byte_count,
		cat_nogrep	=> 1,
		max_lines	=> $max_lines);

pr_output(    $nl."[ Read $n_read rows from $INFILE.  Filtered to $n_returned ]$nl");
pr_output(    "[ DBG: found_pat=$found_pat (0=false 1=true)]$nl") 	if defined $DEBUG;
pr_output(    "[ DBG: next_run_returned_pat=$returned_pat ]$nl") 	if defined $DEBUG;
pr_output(    "[ DBG: next_run_byte_cnt=$byte_cnt ]$nl" ) 		if defined $DEBUG;
if( ! defined $NOSAVEPATFILE or defined $DEBUG ) {
	log_wr_patern_file(patern=>$returned_pat, bytes=>$byte_cnt, file=>$PATFILE );
	pr_output( "WRITING PATERN FILE byte count = $byte_cnt   ",$nl ) if defined $DEBUG;
	pr_output( "WRITING PATERN FILE patern   = $returned_pat ",$nl ) if defined $DEBUG;
} else {
	pr_output( "NOT WRITING PATERN FILE - NOSAVE/DEBUG MODE",$nl );
	pr_output( "[ PATERN FILE ] byte count = $byte_cnt   ",$nl );
	pr_output( "[ PATERN FILE ] patern   = $returned_pat ",$nl );
}

pr_output( "<b>TRUNCATING OUTPUT TO $max_lines LINES</b>$nl" )
	if $#output_r>=($max_lines-1);

# AT THIS POINT YOU HAVE @output_r which contains your rows
pr_output( "FOUND .($#output_r+1)." ROWS",$nl );

# GLUE LINES TOGETHER
my(@glued);
my($lastmsg)="";
foreach (@output_r) {
	chomp;
	if( /^\s/ ) {
		s/^\s+/ /;
		s/\s+$//;
		$lastmsg.=$_;
	} else {
		push @glued,$lastmsg if $lastmsg;
		$lastmsg=$_;
	}
}
push @glued,$lastmsg if $lastmsg;
pr_output( "RE-ASSEMBLED INTO .($#glued+1)." ROWS",$nl );
my($processed_rows)=0;
foreach (@glued) {
	chomp;
	manage_message($_);
}
pr_output( "ALERTED FOR $processed_rows ROWS",$nl );
pr_output($nl."Program Completed... ",$nl);

pr_output( "\nMessage Category Report\n" );
foreach (sort keys %catsum) {
	pr_output( sprintf("Category %12s : %s messages\n", $_,$catsum{$_})  );
}

if( $OUTFILE ) {
	pr_output($nl."Renaming temporary file $tmpfile to $OUTFILE.",$nl);
	close(OUTFILE);
	my($rc)=rename($tmpfile,$OUTFILE);
	if( ! $rc ) {
		warn "ERROR: cant rename $tmpfile to $OUTFILE : $!",$nl;
	} else {
		print "Renamed file to $OUTFILE",$nl;
		unlink $tmpfile if -r $tmpfile;
	}
}

MlpBatchJobEnd() unless defined $NOLOG;
chmod( 0777, $OUTFILE ) if -w $OUTFILE;
exit(0);

my(%state_by_host_sys);
sub manage_message {
	my($m) = @_;
	$_ = $m;
	return if /^-- \(/
		or /^I./ and ! /0000000000/
		or /^Delete of/ and /rows completed for table:/
		or /0000000000 Disconnect:/
		or /0000000000 Cancellation request received/
		or /^-- \(.+\.cxx\s\d+\)\s*$/
		or /0000000005/
		or /\s0000000000 DB:/
		or /\s0000000000\s+Chk/
		or /\s0000000000\s+PostChk/
		or /\s0000000000\s+Collation/
		or /\s0000000000\s+Mem:\s/
		or /\s0000000000\s+RcvyCmpl\s*$/
		or /\s0000000000\s+---IOX: Could not open script file/
		or /\s0000000000\s+IQ cmd line srv opts:/;
	return unless /.\.\s/;
	pr_output( $m,"\n" );
	s/\s\d\d\d\d\d\d\d\d\d+\s//;
	my($date)= substr( $_, 3, 5)."/$YEAR ".substr($_,9,8);
	$processed_rows++;
	MlpEvent(
					-monitor_program =>$HB_MONITOR,
					-event_time		=>  $date,
					-system			=> $SYSTEM,
					-subsystem		=> "Error Log",
					-message_text	=> $_,
					-severity		=> "ERROR" ) unless $NOLOG;
	return 1;
}

sub pr_output {
	if( defined $OUTFILE ) {
		print OUTFILE @_;
		print @_ if defined $DEBUG;
	} else {
		print @_;
	}
}

__END__

=head1 NAME

cisco_logmon.pl - Read Cisco Logs And Alarm Appropriately

=head2 DESCRIPTION

This perl module parses and reads the cisco logs.

=head2 FILTER RULES

There are a number of paterns that are excluded.  The main interest of this program is whether lines are up or down.  Consequently, we search on the following mesages:

=over 4

=item * UPDOWN:

=item * STATECHANGE:

=item * ALARM: or INFO:

=item * RANDOM Less Used Stuff

=back

=cut
