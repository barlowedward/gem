#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use File::Basename;
use Sys::Hostname;
use Repository;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

print "Do you wish to copy the authorized keys file to all remote systems?\n";
print "Hit enter to continue - ctrl C to abort\n";
<STDIN>;

my $curhostname = hostname();
my(@hosts)=get_password(-type=>"unix");
my($count)=1;
foreach my $host (@hosts) {
   my($login,$password)=get_password(-type=>"unix",-name=>$host);
	next if $host eq $curhostname;
	my $cmd="update_remote_ssh_keys_one_system.ksh $host authorized_keys";
	print $cmd,"\n";
	system($cmd);
	$count++;
}
