# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package backup;

@ISA = qw(Plugin);
use strict;

use vars qw($VERSION);
$VERSION = ".01";

# save some frames
my($current_main_frame);	# the backups tab
my($working_frame);		# everything in current_main_frame but the save_button_frame
my($save_button);		# the save button

my($std_page_name)="backups";
my(@menuitems) = (
	"Sybase/Maintenance Plans/Define Maintenance Plans",
	"Sybase/Maintenance Plans/Execute Maintenance Plans",
	"Sybase/Maintenance Plans/Execute Plan Operations",
	"Sql Server/Maintenance Plans/Define Maintenance Plans",
	"Sql Server/Maintenance Plans/Execute Maintenance Plans",
	"Sql Server/Maintenance Plans/Execute Plan Operations",
	"Configuration/Define Maintenance Plans" );

my($current_plan);
my($current_server);
my($current_screen)="Define";	# Define, Help, Operations, Logs, Execute, filetype

my($filename);
my($current_filetype)="sessionlog";
my($current_nb,$current_nb_tab);
my($current_tree);
my($rotext);
my($show_all_files)=0;
my(%fetch_history);
my(@servers);
my(@serverlist,%filedata);

sub getPluginPanes { 	return "backups"; }
sub getExplorerButtons {my(%x);	$x{"Maint. Plans"}="Plans";	return %x;}
sub getMenuItems {	return @menuitems;}

sub event_handler
{
	my($package)=shift;
	my(%args)=@_;

	# if your context is backups and you tab out of Plugins - Force a redraw
	# errr... i dont know what this means
	#if( $args{-context} eq "Plans" and $args{-eventtype} eq "tabchange" and $args{-curtab} ne $std_page_name ){
	#	$package->refresh_tree("sqlsvr");
	#	return;
	#}

	return unless $args{-curtab} eq $std_page_name
		or $args{-context}   eq "Plans"
		or $args{-eventtype} eq "menu"
		or $args{-eventtype} eq "expbutton";

	if( $args{-eventtype} eq "menu" ) {
		my($found)="FALSE";
		foreach ( @menuitems ) { $found="TRUE" if $args{-menuitem} eq $_; }
		return unless $found eq "TRUE";

		$package->statusmsg( "[backup] menuclick($args{-menuitem})\n" );
		if( $args{-curtab} ne $std_page_name ) {
        			$package->statusmsg( "[backup] event_handler() raising tab $std_page_name\n" );
        			$package->ignore_events(1);
				$main::notebook_f->raise(ucfirst($std_page_name));
				$package->ignore_events(undef);
		}
		if( $args{-menuitem}=~/Define Maintenance Plans/ ) {
			$package->paint_body("Define");
		}elsif( $args{-menuitem}=~/Execute Maintenance Plans/ ) {
			$package->paint_body("Execute");
		}elsif( $args{-menuitem}=~/Execute Plan Operations/ ) {
			$package->paint_body("Operations");
		} else {
			die "This Cant Happen Unless The Code Line is Screwed Up\n";
		}

	} elsif( $args{-eventtype} eq "tabchange" ) {
		return if $args{-curtab} ne "backups";
		$package->statusmsg( "[backup] event_handler() called\n" );
		$package->statusmsg( "[backup] Backups Tab Has Been Raised\n" );
		$package->refresh_tree("Plans") if $args{-context} ne "Plans";
		$package->paint_body();
	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {
		return if $args{-context} ne "Plans";
		$package->statusmsg( "[backup] treebrowse(path=$args{-entryPath})\n" );
		if( $args{-curtab} ne $std_page_name ) {
        		$package->statusmsg( "[backup] event_handler() raising tab $std_page_name\n" );
			$main::notebook_f->raise(ucfirst($std_page_name));
		}

		my(@path_dat)=split(/\//,$args{-entrypath});
		$current_server	= $path_dat[1];
		$current_plan	= $path_dat[2];
		$current_plan =~ s/^Plan = //;
		$current_server =~ s/^Server = //;
		$package->paint_body();

	} elsif( $args{-eventtype} eq "treertclick" ) {
		#return if $args{-curtab} ne "backups";
		#$package->statusmsg( "[backup] event_handler() called\n" );
		#my($entryPath, $clktext)=($args{-entrypath},$args{-clicktext});
		#$package->statusmsg(  "[backup] rtclick(Path=$entryPath Txt=$clktext)\n" );
		#$package->browse($entryPath);

		return if $args{-context} ne "Plans";
		$package->statusmsg( "[backup] treebrowse(path=$args{-entryPath})\n" );
		if( $args{-curtab} ne $std_page_name ) {
        		$package->statusmsg( "[backup] event_handler() raising tab $std_page_name\n" );
			$main::notebook_f->raise(ucfirst($std_page_name));
		}

		my(@path_dat)=split(/\//,$args{-entrypath});
		$current_server	= $path_dat[1];
		$current_plan	= $path_dat[2];
		$current_plan =~ s/^Plan = //;
		$current_server =~ s/^Server = //;
		$package->paint_body();

		if( $args{-clicktext} eq "Backup System Help" ){
			$package->paint_body("Help");
		} elsif( $args{-clicktext} =~ /^Backup History for/ ){
			$package->paint_body("Logs");
		} elsif( $args{-clicktext} eq /^Create New Plan For/ ){
			$package->unimplemented("Create Plan Unimplemeted\nPlease use G.E.M. Configuration manger.");
		} elsif( $args{-clicktext} eq /^Delete/ ){
			$package->unimplemented("Delete Plan Unimplemented\nPlease use G.E.M. Configuration manger.")
		} elsif( $args{-clicktext} eq /Properties$/ ){
			$package->paint_body("Define");
		}

	} elsif( $args{-eventtype} eq "expbutton" ) {
		# GO TO THE BACKUPS TREE TAB
		if( $args{-context} eq "Plans" and $args{-curtab} ne $std_page_name ) {
			$package->statusmsg( "[backup] event_handler() called\n" );
			$package->statusmsg( "[backup] raising Backups tab\n" );
			$main::notebook_f->raise("Backups");
		}
	} elsif( $args{-eventtype} eq "imgbutton" ) {
	}
}

sub getTreeItems {
	my($package,$curTreeType)=@_;
	$current_tree = $curTreeType;
	return unless $curTreeType eq "Plans";

	# ok get a list of the backup jobs
	my(@rc);
	my(%found);
#	foreach ( sort @{$main::XMLDATA{Backup_Plan_Aryptr}} ) {
	foreach ( sort plan_get() ) {
		next if  /^\s*$/;
		my($dsquery)=$main::CONFIG{$_."_SERVER_NAME"};
		next unless defined $dsquery and $dsquery !~/^\s*$/;
		push @rc, "/Server = ".$dsquery unless $found{$dsquery};
		$found{$dsquery}=1;
		push @rc, "/Server = ".$dsquery."/Plan = ".$_;

		if( ! defined $current_plan ) {
			if( defined $current_server ) {
				$current_plan	=$_ if $current_server eq $dsquery;
			} else {
				$current_plan	=$_;
				$current_server	=$dsquery;
			}
		}

	}
	return \@rc;
}

sub getTreeRtclk {
	my($package,$curTreeType,$treeitmptr)=@_;
	$package->debugmsg(  "[backup] getTreeRtclk(curtype=$curTreeType)\n" );
	return(undef) unless $curTreeType eq "Plans";
	my(@rightclicks);
	foreach my $itm ( @$treeitmptr ) {
		my(@rtclick)=("Backup System Help");
		if ( $itm eq "/" ) {
			# push @rightclicks, \@rtclick;
		} else {
			my(@d)=split(/\//,$itm);
			if( $#d==1 ) {
			} elsif( $#d==2 ) {
				unshift @rtclick, "Backup History for $d[1]";
				unshift @rtclick, "Create New Plan For $d[1]";
			} elsif( $#d==3 ) {
				unshift @rtclick, "Delete $d[2] on $d[1]";
				unshift @rtclick, "Create New Plan For $d[1]";
				unshift @rtclick, "$d[2] Properties";

			}
		}
		push @rightclicks, \@rtclick;
	}
	return \@rightclicks;
}


sub paint_body {
	my($package,$screen)=@_;

	$screen=$current_screen unless defined $screen;
	$package->statusmsg( "[backup] paint_body($screen,plan=$current_plan,svr=$current_server)\n" );
	if( $screen eq "Define" ) {
		$package->screen_define_plan();
	} elsif( $screen eq "Help" ) {
		$package->screen_help();
	} elsif( $screen eq "Logs" or $screen eq "filetype" ) {
		$package->screen_logfiles($screen);
	} elsif( $screen eq "Execute" ) {
		$package->screen_exec_plan();
	} elsif( $screen eq "Operations" ) {
		$package->unimplemented("Operations");
	} else {
		die "paint_body($screen) cant happen\n";
	}
	$current_screen = $screen;
}

sub screen_define_plan {
	my($package)=@_;
	$package->statusmsg( "[backup] screen_define_plan(plan=$current_plan,svr=$current_server)\n" );
	if( $current_screen eq "Define" and defined $current_nb_tab and Tk::Exists($current_nb_tab)) {
		$package->statusmsg( "[backup] refreshing plan tab but not recreating\n" );
		main::update_planinfo($current_plan) if defined $current_plan;
		return;
	}

	$save_button->packForget();
	$current_nb_tab=undef;
	$current_nb_tab=$current_nb->raised() if defined $current_nb;
	$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);
	$working_frame  = $current_main_frame->LabFrame(
		-label=>"Current Plan = $current_plan Current Server = $current_server",
		-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
	$current_plan="/" unless defined $current_plan;
	$current_nb=main::plantab_paint_notebook($current_plan,$working_frame);
	if( defined $current_nb_tab) {
		$current_nb->raise($current_nb_tab);
	}
	$save_button->pack( -side=>'bottom', -padx=>3, -pady=>8 );
	$package->refresh_tree("Plans") if $current_tree ne "Plans";

}
sub screen_help {
	my($package)=@_;
	$package->statusmsg( "[backup] screen_help(plan=$current_plan,svr=$current_server)\n" );

	$save_button->packForget();
	$current_nb=undef;
	$package->show_html("http://www.edbarlow.com/document/sydump_readme.htm");

}
sub screen_exec_plan {
	my($package)=@_;
	$package->statusmsg( "[backup] screen_exec_plan(plan=$current_plan,svr=$current_server)\n" );
	$save_button->packForget();
}

sub screen_logfiles  {
	my($package,$screen)=@_;
	$package->statusmsg( "[backup] screen_logfiles(plan=$current_plan,svr=$current_server)\n" );

	$save_button->packForget();

	$current_nb=undef;
	$package->refresh_tree("Plans") if $current_tree ne "Plans";

	$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);
	$working_frame=$current_main_frame->Frame()->pack( -fill=>'both', -side=>'top', -expand=>1);

	my($log_frame)  = $working_frame->LabFrame(
		-label=>"Current Plan = $current_plan Current Server = $current_server",
		-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);

	my($log_btns) = $log_frame->Frame()->pack( -fill=>'x', -side=>'top');

	$package->make_button(-widget=>$log_btns,
		-help=>'Errors',
		-text=>"Errors",
		-command=> sub {
			$current_filetype = "errors";
			$package->paint_body("filetype"); } )->pack( -side=>'left', -padx=>3);
	$package->make_button(-widget=>$log_btns,
		-help=>'Session Logs',
		-text=>"Session Logs",
		-command=> sub {
			$current_filetype = "sessionlog";
			$package->paint_body("filetype");
			 } )->pack( -side=>'left' , -padx=>3);
	$package->make_button(-widget=>$log_btns,
		-help=>'DBCC',
		-text=>"DBCC",
		-command=> sub {
			$current_filetype = "dbcc";
			$package->paint_body("filetype") } )->pack( -side=>'left', -padx=>3 );
	$package->make_button(-widget=>$log_btns,
		-help=>'Audits',
		-text=>"Audits",
		-command=> sub {
			$current_filetype = "audits";
			$package->paint_body("filetype");
		} )->pack( -side=>'left', -padx=>3 );
	$package->make_button(-widget=>$log_btns,
		-help=>'Refetch Plan Files',
		-text=>"Refetch Plan Files",
		-command=> sub {
	#		@servers=undef;
	#		%audits=undef;
	#		%dbcc=undef;
	#		%errors=undef;
	#		%sessionlog=undef;
			$package->paint_body("Execute");
		 } )->pack( -side=>'left', -padx=>3 );

	$log_btns->Checkbutton(
		-variable=>\$show_all_files,
		-text=>'Show All Files',
		-command=>sub{ $package->paint_body("filetype"); }
	)->pack( -side=>'left', -padx=>3 );

	return if $screen ne "filetype"; # or ! defined $current_server or ! defined $current_plan;

	# prefetch if Server/Type not fetched

	if( $current_filetype eq "audits"
	or $current_filetype  eq "dbcc"
	or $current_filetype  eq "errors"
	or $current_filetype  eq "sessionlog" ) {
		$package->do_dir( $current_filetype );
		my($srchfrm)=$log_frame->Frame()->pack( -fill=>'x', -side=>'top', -padx=>5, -pady=>5);

		$filename=undef;
		my(@flist);
		foreach ( sort keys %{$filedata{$current_filetype}} ) {
			my(@d)=split(/\//,$_,3);
			if( $show_all_files == 0 and defined $current_server ) { next if $d[0] ne $current_server; }
			$filename=$_ unless defined $filename;
			push @flist,$_;
			#$be->insert("end",$_);
			#$be->insert("end",$d[2]);
		}

		unshift @flist," "x64;
		my($be)=$srchfrm->BrowseEntry(-variable=>\$filename, -choices=>\@flist,
			-listwidth => 64, -width=>64,
			#-autolistwidth=>1,
			-browsecmd=> sub { $package->search() } )->pack(-side=>'left');
		$be->bind("<Return>", sub { $package->search() } );

		$be->focus();


		#foreach ( keys %{$filedata{$current_filetype}} ) {
		#	my($itemptr)= ${$filedata{$current_filetype}}{$_};
		#	my(@d)=split(/\//,$_,3);
		#	print $d[0]," ",$d[1]," ",$d[2]," ",$$itemptr{size}," ",$$itemptr{time},"\n";
		#}

		$package->make_button(-widget=>$srchfrm,
   			-help=>'Fetch This Log File',
			-text=>"Fetch",
			-command=> sub { $package->search(); })->pack(-side=>'left');

		$rotext=$log_frame->Scrolled('ROText',
			-relief => "sunken",
			-wrap=>'none',
         		-bg=>'white',
         		-font=>'code',
         		-scrollbars => "osoe")->pack(-expand=>1,-fill=>'both');
	}
		$fetch_history{$current_server}=1;
}

sub search {
	my($package)=@_;
	$package->statusmsg( "Searching for $filename\n" );
	$rotext->delete("1.0",'end');
	if( -r "$main::CONFIG{BASE_BACKUP_DIR}/$filename" ) {
		$rotext->insert('end',"Contents of file $main::CONFIG{BASE_BACKUP_DIR}/$filename\n");
		$rotext->insert('end',"=========================================================\n");
		open(F,$main::CONFIG{BASE_BACKUP_DIR}."/".$filename);

		foreach( <F> ) {
			$rotext->insert('end',$_);
		}
		close(F);
	} else {
		$rotext->insert('end',"File $main::CONFIG{BASE_BACKUP_DIR}/$filename Not Readable");
	}
}

sub do_dir {
	my($package,$filetype)=@_;
	my($d)=$main::CONFIG{BASE_BACKUP_DIR};
	my(%rc);

	# have servers been fetched before?
	if( ! defined $fetch_history{current_server} ) {
		opendir(DIR,$d ) or die("Cant Open Directory $d For Listing\n");
		@serverlist=grep( !/^\./, readdir(DIR) );
		closedir(DIR);
		$fetch_history{current_server} = 1;
	}

	return if defined $fetch_history{$filetype};
	$fetch_history{$filetype}=1;

	foreach my $servername ( @serverlist ) {
		$package->statusmsg( "Getting $filetype for $servername\n" );
		if( -d "$d/$servername/$filetype" ){
			opendir(DIR,$d."/$servername/$filetype")
				or die("Cant Open Directory $d/$servername/$filetype For Listing\n");
			my(@xlist)=grep( !/^\./, readdir(DIR) );
			closedir(DIR);

			foreach my $fname (@xlist) {
				my(%fileinfo);
				$fileinfo{type}=$filetype;
				$fileinfo{server}=$servername;
				$fileinfo{time}= -M "$d/$servername/$filetype/$fname";
				$fileinfo{size}= -s "$d/$servername/$filetype/$fname";
				#$$hashptr{"$d/$servername/$filetype/$fname"} = \%fileinfo;
				$rc{"$servername/$filetype/$fname"}=\%fileinfo;
			}
		}
	}
	$package->statusmsg( "Base Backup Directory $filetype Fetching Completed\n" );

	$filedata{$filetype}=\%rc;
}

sub init {
	my($package)=@_;
	$current_main_frame=$package->clear_tab('backups');

	$current_main_frame->Label(
		-text=>"Maintenance / Backup Plan Manager",-relief=>'ridge' , -bg=>'white'
		)->pack( -side=>'top', -fill=>'x', -anchor=>'n');

	my($butfrm)=$current_main_frame->Frame()->pack( -fill=>'x', -side=>'top', -anchor=>'n');
	$package->make_button(-widget=>$butfrm,
    			-help=>'Show Recent Backup Job History',
    			-text=>"Recent Backup History",
    			-command=> sub {
					$package->paint_body("Logs");
			} )->pack( -side=>'left', -padx=>3, -pady=>8 );

	$package->make_button(-widget=>$butfrm,
    			-help=>'Show Backup Job Plans',
    			-text=>"Job Plan Definition",
    			-command=> sub {
    				$package->paint_body("Define");
    			 } )->pack( -side=>'left', -padx=>3, -pady=>8 );

    	$package->make_button(-widget=>$butfrm,
    			-help=>'Delete Plan',
    			-text=>"Delete Plan",
    			-command=> sub {
    				$package->unimplemented("Delete") } )->pack( -side=>'left', -padx=>3, -pady=>8 );
    	$package->make_button(-widget=>$butfrm,
    			-help=>'Create Plan',
    			-text=>"Create Plan",
    			-command=> sub {
    				$package->unimplemented("Create") } )->pack( -side=>'left', -padx=>3, -pady=>8 );

    	$package->make_button(-widget=>$butfrm,
				-text=>"Backup System Help",
				-help=>"Help",
				-func=>sub {	$package->paint_body("Help");	}
				)->pack( -side=>'left', -padx=>3, -pady=>2 );

	my($save_button_frame)=$current_main_frame->Frame()->pack( -fill=>'x', -side=>'bottom');
	$save_button = $package->make_button(-widget=>$save_button_frame,
    			-help=>'Save Plan Changes',
    			-text=>"Save Plan Changes",
    			-command=> sub {
    				$package->unimplemented("Save Plan Changes");
    		} );
}

1;
