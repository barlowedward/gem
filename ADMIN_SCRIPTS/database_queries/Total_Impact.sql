-- report_key=Total_Impact
-- report_type=HTML_AGGREGATE
-- run_frequency=daily
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_on_server_csv=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=This Report Lists Queries By Their Total Impact
-- report_footer=
-- enabled=N
-- outfile=Total_Impact

SELECT TOP 100
   ProcedureName    = substring(t.text,0,250),
   ExecutionCount   = s.execution_count,
   AvgExecutionTime = isnull( s.total_elapsed_time / s.execution_count, 0 ),
   AvgWorkerTime    = s.total_worker_time / s.execution_count,
   TotalWorkerTime  = s.total_worker_time,
   MaxLogicalReads  = s.max_logical_reads,
   MaxLogicalWrites = s.max_logical_writes,
   CreationDateTime = convert(varchar,s.creation_time,107),
   CallsPerSecond   = isnull( s.execution_count / datediff( second, s.creation_time, getdate()), 0 )
FROM sys.dm_exec_query_stats s
   CROSS APPLY sys.dm_exec_sql_text( s.sql_handle )  t
ORDER BY
   s.total_elapsed_time DESC
