-- BackupState
--
-- BackupState contains one row per database in your environment
--    contains the latest dump and load times for the database
--    contains the state of database options (readonly etc)
--    there is some confusion between monitored_system_name and from_internal_system_name that needs documentation
create table BackupState (
	monitored_system_name				varchar(30),

	dbname						varchar(30),

	last_fulldump_time		datetime			null,
	last_fulldump_file		varchar(127)	null,
	last_fulldump_lsn			varchar(20)		null,
	last_trandump_time		datetime	null,
	last_trandump_file		varchar(127)	null,
	last_trandump_lsn			varchar(20)		null,
	last_truncation_time		datetime			null,
	last_fullload_time		datetime			null,
	last_fullload_file		varchar(127)	null,
	last_fullload_lsn			varchar(20)		null,
	last_tranload_time		datetime			null,
	last_tranload_file		varchar(127)	null,
	last_tranload_lsn			varchar(20)		null,
	last_tranload_filetime	varchar(20)	   null,
   is_tran_truncated       char(1)        null,
   is_db_usable            char(1)        null,
   is_readonly             char(1)        null,
   is_select_into          char(1)        null,
	is_singleuser           char(1)			null,
	is_backupable           char(1)        null,
   from_internal_system_name				varchar(30)    null,

	file_time	datetime			null,
	row_touch_time	datetime			null,
	is_deleted	char(1)	null
)
go

create unique clustered index BSIND2 on BackupState ( monitored_system_name, dbname)
go

--------------------------------------------------------------------------------

create procedure Backup_proc (
	@monitored_system_name				varchar(30),
	@dbname						varchar(30),

	@last_fulldump_time		datetime			=null,
	@last_fulldump_file		varchar(127)	=null,
	@last_fulldump_lsn		varchar(20)		=null,

	@last_trandump_time		datetime			=null,
	@last_trandump_file		varchar(127)	=null,
	@last_trandump_lsn		varchar(20)		=null,

	@last_truncation_time	datetime			=null,

	@last_fullload_time		datetime			=null,
	@last_fullload_file		varchar(127)	=null,
	@last_fullload_lsn		varchar(20)		=null,

	@last_tranload_time		datetime			=null,
	@last_tranload_file		varchar(127)	=null,
	@last_tranload_lsn		varchar(20)		=null,
	@last_tranload_filetime	varchar(20)		=null,

	@is_tran_truncated		char(1)			=null,
	@is_db_usable				char(1)			=null,

	@is_readonly				char(1)			=null,
	@is_select_into			char(1)			=null,
	@is_singleuser				char(1)			=null,
	@is_backupable				char(1)			=null,
	@from_internal_system_name	varchar(20)		=null,
	@file_time					datetime			=null
)
as
	... 
--------------------------------------------------------------------------------
DEPRICATED

#bin/BkpChk_Mssql_Logs.pl
#	makefile("Mssql_BkpChk_Hourly","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -HOURS=24 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChk.txt --BATCHID=__BATCHID__\n",
#		undef, "hourly","monitoring");
#	makefile("Mssql_BkpChk_Morning","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -HOURS=24 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChk.txt --BATCHID=__BATCHID__\n",
#		undef, "daily-4am","monitoring");
#	makefile("Mssql_BkpChk_Evening","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -HOURS=24 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChk.txt --BATCHID=__BATCHID__\n",
#		undef, "daily-8pm","monitoring");
#	makefile("Mssql_BkpChk_Weekly","YES","NO",
#		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Logs.pl -DAYS=7 --OUTFILE=__CODELOCATION__\\\\data\\html_output\\MssqlBkpChkWeekly.txt --BATCHID=__BATCHID__\n",
#		undef, "saturday-am","monitoring");
	
--------------------------------------------------------------------------------
bin/BkpChk_Mssql_Proc.pl
	makefile("Mssql_BkpChk_ByProc","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Mssql_Proc.pl --DOALL=sqlsvr --BATCHID=__BATCHID__\n",
		undef, "Every15min","monitoring");
	
--------------------------------------------------------------------------------
bin/BkpChk_Report.pl
	makefile("Mssql_BkpChk_Report","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Report.pl --SQLSVRONLY --ERROR_REPORT=__CODELOCATION__\\\\data\\html_output\\Mssql_BkpChk_Errors.html --ALL_REPORT=__CODELOCATION__\\\\data\\html_output\\Mssql_BkpChk_Report.html --HTML --BATCHID=__BATCHID__ --TRANHOURS=2\n",
		undef, "hourly","monitoring");
	makefile("Sybase_BkpCheck_Report","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_Report.pl --SYBASEONLY --ERROR_REPORT=__CODELOCATION__\\\\data\\html_output\\Sybase_BkpCheck_Errors.html --ALL_REPORT=__CODELOCATION__\\\\data\\html_output\\Sybase_BkpCheck_Report.html --HTML --BATCHID=__BATCHID__ --TRANHOURS=2\n",
		undef, "hourly","monitoring");

--------------------------------------------------------------------------------
bin/BkpChk_UpdateStaticInfo.pl
	makefile("Win32_BackupStateStaticInfo","YES","NO",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_UpdateStaticInfo.pl --TYPE=sqlsvr --BATCH_ID=__BATCHID__\n",
		undef, "daily","reporting");		
	makefile("Unix_BackupStateStaticInfo","NO","YES",
		"__PERL__  __CODELOCATION__\\ADMIN_SCRIPTS\\bin\\BkpChk_UpdateStaticInfo.pl  --TYPE=sybase --BATCH_ID=__BATCHID__\n",		
		undef, "daily","reporting");				
		
--------------------------------------------------------------------------------
console/process_log_files.pl
gem_development/maintplan_wizard.pl

/apps/sybmon/gem/bin/create_batch_scripts.pl
--------------------------------------------------------------------------------
/apps/sybmon/gem/lib/MlpAlarm.pm
	sub MlpGetBackupState
			if( defined $args{-production} and $args{-production} ne "0" ) {
			$query .= " , GEM_servers p";
			}
		
			$query.="where isnull( is_deleted,'N')='N'\n";
			$query.="   and monitored_system_name = \'".$args{-system}."\'\n" if defined $args{-system};
			if( defined $args{-production} and $args{-production} ne "0" ) {
				$query .= "   and BackupState.monitored_system_name = p.system_name and production_sate='PRODUCTION'\n";
			}
		
			$query.= " ".$args{-orderby} if defined $args{-orderby};
			print $query."\n" if $args{-debug};
		
			my(@rc);
			@rc = dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$gemalarms_connection, -die_on_error=>0 );

		"MlpGetBackupState:-system"		=> 0,
		"MlpGetBackupState:-production"	=> 0,
		"MlpGetBackupState:-orderby"		=> 0,
		"MlpGetBackupState:-debug"			=> 0,

	sub MlpOldBackupState
			my($query)="	UPDATE BackupState set is_deleted='Y' where datediff(dd,row_touch_time,getdate())>21 ";

	sub MlpSetBackupState	
		"MlpSetBackupState:-system"		=> 1,
		"MlpSetBackupState:-dbname"		=> 1,
		"MlpSetBackupState:-debug"		=> 0,
		"MlpSetBackupState:-last_fulldump_time"		=> 0,
		"MlpSetBackupState:-last_fulldump_file"		=> 0,
		"MlpSetBackupState:-last_fulldump_lsn"		=> 0,
		"MlpSetBackupState:-last_trandump_time"		=> 0,
		"MlpSetBackupState:-last_trandump_file"		=> 0,
		"MlpSetBackupState:-last_trandump_lsn"		=> 0,
		"MlpSetBackupState:-last_truncation_time"	=> 0,
		"MlpSetBackupState:-last_fullload_time"		=> 0,
		"MlpSetBackupState:-last_fullload_file"		=> 0,
		"MlpSetBackupState:-delete"		=> 0,
		"MlpSetBackupState:-last_fullload_lsn"		=> 0,
		"MlpSetBackupState:-last_tranload_time"		=> 0,
		"MlpSetBackupState:-last_tranload_file"		=> 0,
		"MlpSetBackupState:-last_tranload_lsn"		=> 0,
		"MlpSetBackupState:-last_tranload_filetime"	=> 0,
		"MlpSetBackupState:-is_tran_truncated"	=> 0,
		"MlpSetBackupState:-is_db_usable"	=> 0,
		"MlpSetBackupState:-from_internal_system_name"	=> 0,
		"MlpSetBackupState:-file_time"	=> 0,
		"MlpSetBackupState:-is_readonly"	=> 0,
		"MlpSetBackupState:-is_select_into"	=> 0,
		"MlpSetBackupState:-is_singleuser"	=> 0,
		"MlpSetBackupState:-is_backupable"	=> 0,
		
