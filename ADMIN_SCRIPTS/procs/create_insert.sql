/* Procedure created 2010 by Yury Davydov */

/************************************************************************\
|* Procedure Name:      sp__create_insert
|*
|* Description: generates insert statement for the selected table
|*
|* Usage: sp__create_insert @table_name, @insert_identity
|*
|* Where: @table_name --> [db-name.][owner.]table_name
|*        @insert_identity --> if 'Y' generates set identity_insert ...
|*
|* Modification History:
|*
\************************************************************************/

:r database
go
:r dumpdb
go

if exists (select *
	   from   sysobjects
	   where  type = "P"
	   and    name = "sp__create_insert")
begin
    drop proc sp__create_insert
end
go

create procedure sp__create_insert
  @table_name       varchar(320)
 ,@insert_identity  varchar(4) = NULL
as
begin
  set nocount on

  declare @db_name    varchar(30)
         ,@user_name  varchar(30)
         ,@i          int
         ,@colid      smallint
         ,@id         int
         ,@msg        varchar(255)

  create table #table_def
    (colid     smallint
    ,name      varchar(255)
    ,type      varchar(30)
    ,length    smallint     null
    ,prec      smallint     null
    ,scale     smallint     null
    ,status    tinyint
    ,format    varchar(60))

  create table #text
    (text  varchar(300)
    ,id    numeric(9)   identity)

-- Check if we are in the current database.

  if (@table_name like '%.%.%')
  begin
    select @i = charindex('.', @table_name)
    select @db_name = substring(@table_name, 1, @i -1)
    select @table_name = substring(@table_name, @i +1, datalength(@table_name) - @i)

    if (@db_name != db_name())
    begin
    	select @msg = 'You should be in the '+@db_name+' database to execute this procedure.'
      return 32001
    end
  end

  if (@table_name like '%.%')
  begin
    select @i = charindex('.', @table_name)
    select @user_name = substring(@table_name, 1, @i -1)
    select @table_name = rtrim(ltrim(substring(@table_name, @i +1, datalength(@table_name) - @i)))
  end

  if (rtrim(ltrim(@user_name)) is null) select @user_name = 'dbo'

  if ((select count(*) from sysobjects
        where type = 'U' and uid = user_id(@user_name) and name = @table_name) = 0)
  begin
    select @msg = 'No tables found that satisfy the criteria: '+@table_name+'.'
    return 32002
  end

  select @id = id from sysobjects
   where type = 'U' and uid = user_id(@user_name) and name = @table_name

-- A bit of data massaging

  select @insert_identity = lower(substring(rtrim(ltrim(isnull(@insert_identity, 'No'))), 1, 1))

-- Create column definitions.

  delete #table_def
  insert #table_def
  select distinct c.colid, c.name, x.name, c.length, c.prec, c.scale, c.status, '--- '+x.name
    from syscolumns c, systypes x, systypes t
   where c.id = @id and c.usertype = x.usertype and x.type = t.type
     and (t.usertype <= 80 or t.usertype = 80)
   group by c.colid, c.name, x.name, c.length, c.prec, c.scale, c.status
           ,t.usertype, c.usertype, x.usertype, c.id, x.type, t.type
  having t.usertype = min(t.usertype) and c.id = @id and c.usertype = x.usertype
     and x.type = t.type and (t.usertype <= 80 or t.usertype = 80)
   order by c.colid

  if exists (select * from #table_def where type = 'timestamp')
  begin
    insert #text(text) select '--- Warning: Table has timestamp colum: '+name
      from #table_def where type = 'timestamp'
    insert #text(text) select '---          This column will not be specified.'
      from #table_def where type = 'timestamp'
    delete #table_def where type = 'timestamp'
    insert #text(text) values(' ')
  end

  if (@insert_identity = 'y' and exists (select * from #table_def where status & 128 = 128))
  begin
    insert #text(text) select '--- IDENTITY column value must be specified for: '+name
      from #table_def where status & 128 = 128
    insert #text(text) select 'set identity_insert '+@user_name+'.'+@table_name+' on'
    insert #text(text) values(' ')
    select @insert_identity = 'Z'
  end
  else
  begin
    insert #text(text) select '--- Warning: Table has identity colum: '+name
      from #table_def where status & 128 = 128
    insert #text(text) select '---          This column will not be specified.'
      from #table_def where status & 128 = 128
    delete #table_def where status & 128 = 128
    insert #text(text) values(' ')
  end

  insert #text(text) select 'insert '+@user_name+'.'+@table_name

  select @colid = min(colid) from #table_def
  update #table_def set name = '('+name where colid = @colid

  update #table_def set name = ','+name where colid != @colid

  select @colid = max(colid) from #table_def
  update #table_def set name = name+')' where colid = @colid

-- Generate format comment.

  update #table_def set format = format+'('+convert(varchar, length)+')'
   where type in ('binary', 'char', 'nchar', 'nvarchar', 'unichar', 'univarchar', 'varchar', 'varbinary')

  update #table_def
     set format = format+'('+convert(varchar, prec)+','+convert(varchar, scale)+')'
   where type in ('decimal', 'numeric')

  select @i = max(datalength(format)) from #table_def
  update #table_def
     set format = format+space(@i - datalength(format) +1)+' identity'
        ,@i = @i +9
   where status & 128 = 128

  update #table_def
     set format = format+space(@i - datalength(format) +1)+'     NULL'
   where status & 8 = 8

  update #table_def
     set format = format+space(@i - datalength(format) +1)+' NOT NULL'
   where status & 8 = 0

  select @i = max(datalength(name)) from #table_def
  insert #text(text)
  select name+'   '+space(@i - datalength(name) +1)+format from #table_def order by colid
--  insert #text(text) select name+'   '+format from #table_def order by colid

  if (@insert_identity = 'Z')
  begin
    insert #text(text) values(' ')
    insert #text(text)
    select 'set identity_insert '+@user_name+'.'+@table_name+' off'
  end

  select text from #text order by id
end
go

grant execute on sp__create_insert to public
go

