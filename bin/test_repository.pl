use strict;
use Repository;

use Getopt::Long;
use vars qw( $DEBUG );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless
   GetOptions(  "DEBUG"		=> \$DEBUG);

# TEST THE DIRECTORY TREE

my($dir)=get_gem_root_dir();
print "Gem Root Dir = $dir\n";
error_out("$dir is not a directory") unless -d $dir;
error_out("$dir is not readable") unless -r $dir;
error_out("$dir is not writable") unless -w $dir;

$dir=get_conf_dir();
print "Gem Conf Dir = $dir\n";
error_out("$dir is not a directory") unless -d $dir;
error_out("$dir is not readable") unless -r $dir;
error_out("$dir is not writable") unless -w $dir;

$dir=get_root_dir();
print "Gem Programs Root = $dir\n";
error_out("$dir is not a directory") unless -d $dir;
error_out("$dir is not readable") unless -r $dir;
error_out("$dir is not writable") unless -w $dir;

my(@types)=get_passtype();
my(@errors);
foreach my $typ (@types) {
	next if $typ eq "documenter";
	my(@svrs)=get_password(-type=>$typ);
	#my(@kys) =get_passfile_info(-type=>$typ);	
				
	printf( "Found %4s registered servers of type $typ \n",($#svrs+1) );
	foreach my $s ( @svrs ) {
		my($l,$p,$t)=get_password(-type=>$typ,-name=>$s);
		if( ! defined $l or $l eq "" ) {
			push @errors, "\tsvr=$s of type $typ has no login/password\n"
				unless $typ eq "documenter" or $typ eq "win32servers";
		}
		my(%hsh)=get_password_info(-type=>$typ,-name=>$s);
		push @errors, "Server $s of type $typ has no SERVER_TYPE\n" unless $hsh{SERVER_TYPE} or $typ eq "win32servers";
		push @errors, "Server $s of type $typ has bad SERVER_TYPE\n" 
			if $hsh{SERVER_TYPE} ne "PRODUCTION"
			 and $hsh{SERVER_TYPE} ne "DR"
			 and $hsh{SERVER_TYPE} ne "QA"
			 and $hsh{SERVER_TYPE} ne "DEVELOPMENT"
			 and $typ ne "win32servers";
		
#		foreach (@kys) {	
#			s/^-//;
#			next if /^SYBASE/ or /^IGNORE/;			
#			if( /login/ ) {
#				push @errors, "\tsvr=$s of type $typ has no value for login\n" unless $l;
#			} elsif( /password/ ) {
#				push @errors, "\tsvr=$s of type $typ has no value for password\n" unless $p;
#			} elsif( /conntype/ or /server/ ) {		
#			} else {
#				push @errors, "\tsvr=$s of type $typ has no value for key=$_\n" unless $hsh{$_};
#			}
#		}		
	}
}

sub error_out {
	foreach (@_) { chomp;print "ERROR: $_\n"; }
	print @_;
	exit(1);
}

if( $#errors<0 ) {
	print "SUCCESSFUL: No Errors\n";
	exit(0);
}

foreach (@errors) {
	s/^\s+//;
	print "ERROR: $_";
}
exit( $#errors+1 );