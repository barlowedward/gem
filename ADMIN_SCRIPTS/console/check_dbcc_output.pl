#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2001 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use File::Basename;
use CommonFunc;
use GemData;
use Repository;
use CommonHeader;
use vars qw($opt_D $opt_J $opt_d);
use Getopt::Std;

die "Usage $0 -DNUM_DAYS\n" unless getopts('D:d');

$| = 1;	# as always

my($curdir)=dirname($0);
#chdir $curdir or die "Cant cd to $curdir: $!\n";

print "check_dbcc_output.pl version 1.0<br>\n";
print "Finding errors for the past $opt_D days<br>\n" if defined $opt_D;

my(%CONFIG)=read_configfile();
use Sys::Hostname;
my($h)=hostname();
sub _statusmsg {    print "[msg] ",join("",@_); }
sub _debugmsg {     print "[dbg]  ",join("",@_); }
my($gem_data) = new GemData( -printfunc=>\&_statusmsg, -debugfunc=>\&_debugmsg, -hostname=>$h );

#my($gem_data)=new GemData;

my($dir)=$CONFIG{BASE_BACKUP_DIR};
die "BASE BACKUP DIRECTORY ($dir) DOES NOT EXIST\n" unless -d $dir;

print "Checking files in $dir/.../dbcc<br>\n";

opendir(DIR,$dir)
	or die("Can't open directory $dir for reading : $!\n");
my(@dirlist)= grep((!/^\./ and -d "$dir/$_" and -d "$dir/$_/dbcc"),readdir(DIR));
closedir(DIR);

my(%DAYS_FOUND);	# has of days found
my(%RESULTS_FOUND);
my(%SERVERDB_FOUND);
my($num_files)=0;
foreach (@dirlist) {
	#print $_,"\n";
	my($dir)=$CONFIG{BASE_BACKUP_DIR}."/$_/dbcc";
	opendir(DIR,$dir) or die("Can't open directory $dir for reading : $!\n");
	my(@dbcc_files)= grep((!/^\./ and /\.dbcc\./),readdir(DIR));
	closedir(DIR);
	foreach (@dbcc_files) {
		#print "\t",$_,"\n";

		# only show specific number of days
		if( defined $opt_D and -M $dir."/".$_ > $opt_D ) {
			print "DBG: ignoring $_ -> age=",(-M $dir."/".$_),"\n" if defined $opt_d;
			next;
		}
		print "DBG: found file $_ \n" if defined $opt_d;
		$num_files++;

		s/dbcc\.\d\d\d\d//;		# remove year

		my($has_errorfile)=0;
		if( /\.errors$/ ) {
			s/\d+\.errors$//;
			$has_errorfile++;
		} else {
			s/\d+$//;
		}
		my($server,$db,$day)=split(/\./,$_);
		#print "server=$server db=$db day=$day\n";

		$DAYS_FOUND{$day}=1;
		$RESULTS_FOUND{$server." ".$db." ".$day}=$has_errorfile;
		$SERVERDB_FOUND{$server." ".$db}=1;
	}
}
print "Found ".($#dirlist+1)." Directories and $num_files files to Search<br>\n";


if( defined $opt_d ) {
	foreach (sort keys %RESULTS_FOUND) { print $_,"\n"; }
}

# header
printf("%31.31s","");
print "<TABLE BORDER=1>\n<TR><TH BGCOLOR=#FFCCFF>SERVER</TH><TH BGCOLOR=#FFCCFF>DATABASE</TH>\n";
foreach ( reverse sort keys %DAYS_FOUND ) {
	print "\t<TH BGCOLOR=#FFCCFF>",$_,"</TH>\n";
}
print "</TR>\n";

my($s)= $gem_data->db_param(-name=>"sybase_servers");
#foreach my $srvr ( sort split(/\s+/,$s) ) {
#	print "Server=$srvr\n";
#	foreach my $db ( sort split(/\s+/, $gem_data->get_set_data(-class=>'sybase',-name=>$srvr,-arg=>'Database_Info'))){
#		print "Server=$db\n";
foreach( sort keys %SERVERDB_FOUND ) {
		my($srvr, $db ) = split;

		next if $db eq "tempdb";
		print "<TR><TD>$srvr</TD><TD>$db</TD>\n";
		foreach ( reverse sort keys %DAYS_FOUND ) {
			if( defined $RESULTS_FOUND{$srvr." ".$db." ".$_} ) {
				if( $RESULTS_FOUND{$srvr." ".$db." ".$_} == 1 ) {
					print "\t<TD BGCOLOR=BLACK><FONT COLOR=WHITE>ERROR</FONT></TD>\n";
				} else {
					print "\t<TD>ok!</TD>\n";
				}
			} else {
					print "\t<TD>&nbsp; </TD>\n";
			}
		}
		print "</TR>\n";
#	}
}
print "</TABLE>\n";
