#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "adduser.pl [-hdx] -USA_USER -SSERVER -PSA_PASS -DDB_LIST
                -uNEW_USER -pNEW_PASS -gNEW_GROUP [ -rDEF_DB -fFULLNAME ]

                DB_LIST may include wildcards
                -h html output
                -d debug mode
                -x remove login only

This program allows you to add or drop a login.  If you pass -x, then the login specified
with -u will be dropped.  The group defaults to public if not passed.  You may ignore the
-U and -P options if you have set up password file\n";
        return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_h $opt_d $opt_U $opt_S $opt_P $opt_x $opt_D $opt_u $opt_p $opt_g $opt_r $opt_f);
die usage("") if $#ARGV<0;
die usage("Bad Parameter List") unless getopts('dxU:D:S:P:d:u:p:g:r:f:h');

# cd to the right directory
use File::Basename;

$opt_r="master" if ! defined $opt_r;

use DBIFunc;

# IF ONLY HAVE opt_S get login & password from password file
if( defined $opt_S and ! defined $opt_U ) {
   use Repository;
   print "Fetching password from $opt_S\n";
   ($opt_U,$opt_P)=get_password(-type=>"sybase",-name=>$opt_S);
   ($opt_U,$opt_P)=get_password(-type=>"sqlsvr",-name=>$opt_S) unless $opt_U;
}

$opt_D="%"
        unless defined $opt_D;
die usage("Must pass sa password\n" )
        unless defined $opt_P;
die usage("Must pass server\n")
        unless defined $opt_S;
die usage("Must pass sa username\n" )
        unless defined $opt_U;
die usage("No -u or -p parmameter passed\n") if ! defined $opt_x and (!defined $opt_u or ! defined $opt_p );
$opt_g="public" unless defined $opt_g;

die "Cant Manage sa" if $opt_u eq "sa";

#
# CONNECT TO DB
#
print 'Connecting to sybase' if defined $opt_d;
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
        or die "Cant connect to $opt_S as $opt_U\n";

dbi_set_web_page(undef) unless defined $opt_h;

# GET DB INFO
my(@rc)=dbi_query(-db=>"master",
	-query=>"select name from sysdatabases where name like \"$opt_D\" and status & 0x1120 = 0 and status2&0x0030=0");
my(@databaselist);
foreach (@rc) { my(@a)=dbi_decode_row($_); push @databaselist,$a[0]; }
print "Found DB: ",join(" ",@databaselist),"\n" if defined $opt_d;

my($db, @procs,@tbls,@views);

# DOES LOGIN EXIST
@rc =  dbi_query(-db=>"master",-query=>"select name from syslogins where name=\"$opt_u\"");

my(@login_exists);
foreach (@rc) { my(@a)=dbi_decode_row($_); push @login_exists,$a[0]; }
print "Checking Login\n" if defined $opt_d;

my(@rc);

# DROP EXISTING LOGIN
if( $#login_exists>=0 ) {
        print "Login Exists\n" if defined $opt_d;
        foreach $db (@databaselist) {
                print "Checking Database $db\n" if defined $opt_d;
                die "Cant Use Database $db\n" unless dbi_use_database($db);
                @rc=dbi_query(-db=>$db,-query=>"select name from sysusers where name=\"$opt_u\"");
                if( $#rc >= 0 ) {
                        print "\tFound User in $db\n" if defined $opt_d;
                        printf( "%30s:\t",$db );
                        my(@rc)=dbi_query(-db=>$db,-query=>"exec sp_dropuser $opt_u");
                        print "\n";
                        foreach (@rc) { my(@a)=dbi_decode_row($_); print "MSG>",$a[0],"\n"; }
                }
                @rc=dbi_query(-db=>$db,-query=>"select * from sysalternates where suid=suser_id(\"$opt_u\")");
                if( $#rc >= 0 ) {
                        print "\tFound Alias in $db\n" if defined $opt_d;
                        printf( "%30s:\t",$db );
                        my(@rc)=dbi_query(-db=>$db,-query=>"exec sp_dropalias $opt_u");
                        print "\n";
                        foreach (@rc) { my(@a)=dbi_decode_row($_); print "MSG>",$a[0],"\n"; }
                }
        }

        # only drop the login if cleaning up
        if( defined $opt_x ) {
                printf( "%30s:\t","master" );
                @rc=dbi_query(-db=>"master",-query=>"exec sp_droplogin \"$opt_u\"");
                print "\n";
                foreach (@rc) { my(@a)=dbi_decode_row($_); print "MSG>",$a[0],"\n"; }
        }
} else {
        print "Login Does Not Exist\n" if defined $opt_d or defined $opt_x;
        exit if defined $opt_x;

        # Only add login if it does not exist
        printf( "%30s:\t","master" );
        foreach (dbi_query(-db=>"master",-query=>"exec sp_addlogin \"$opt_u\",\"$opt_p\",\"$opt_r\",null,\"$opt_f\"")) {
                 #print "MSG> $_\n"
                 my(@a)=dbi_decode_row($_);
                 print "MSG>",$a[0],"\n";
        };
}

exit if defined $opt_x;

foreach $db (@databaselist) {
        print "Checking Database $db\n" if defined $opt_d;
        die "Cant Use Database $db\n" unless dbi_use_database($db);

        # CHECK IF GROUP EXISTS
        my(@rc)=dbi_query(-db=>$db,-query=>"select name from sysusers where uid=gid and name=\"$opt_g\"");

        if( $#rc < 0 ) {
                printf( "%30s:\tGroup Does Not Exist - Adding To Public\n",$db );
                printf( "%30s:\t",$db );
                my(@rc)=dbi_query(-db=>$db,-query=>"exec sp_adduser \"$opt_u\",\"$opt_u\",\"public\"");
                print "\n";
        } else {
                printf( "%30s:\t",$db );
                my(@rc)=dbi_query(-db=>$db,-query=>"exec sp_adduser \"$opt_u\",\"$opt_u\",\"$opt_g\"");
                print "\n";
        }
}

dbi_disconnect();

__END__

=head1 NAME

adduser.pl - utility to add users and logins

=head2 DESCRIPTION

This perl script creates / drops logins & users.  It works in a set of
databases (ie. you can pass in a wildcard for the db - but you probably dont
want to pass in % or the user will be added to tempdb etc... It creates
the login only if necessary.  The user is added to
public if the group is not passed.   It deletes the login only if B<-x>
is passed - so you could invoke the script multiple times with multiple db%
lists to get a useful setup.

=head2 USAGE

        ./adduser.pl [-dx] -USA_USER -SSERVER -PSA_PASS -DDB_LIST
            -uNEW_USER -pNEW_PASS -gNEW_GROUP [ -rDEF_DB -fFULLNAME ]

        DB_LIST may include wildcards
            -d debug mode
            -x remove stuff only

You may pass a sybase wildcard to DB_LIST like xrm% and it will only work in those databases

You may ignore B<-U> and B<-P> options if you have set up password file

=head2 NOTES

It will print ugly messages if both the NEW_GROUP and the public group are not available.

=head2 BUGS

It will fail if trying to remove login that has objects or that is actual
db creator (not just owner).  You must run sp_changedbowner to get it to work.

=cut
