#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

#GENERIC REPOSITORY RUN

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);
use Sys::Hostname;
my($hostname)=hostname();

use vars qw( $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

local $SIG{ALRM} = sub { die "CommandTimedout"; };

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME."

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
}

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
	#	"USER=s"				=> \$USER ,
	#	"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		# "DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"BATCHID=s"			=> \$BATCH_ID ,
	#	"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($STARTTIME)=time;
my(@errors);
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> "unix",
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	NOSTDOUT			=> 1,
	returnonlycmdline => 1,
	server_command => \&server_command
);

if( $#errors<0 ) {
	print "Program Succeeded - no errors\n";
} else {
	print "\nERRORS OCCURRED\n",join("\n",@errors),"\n";
}

exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my( $host, $svrtype, $login, $passwd )=@_;

	print "==> Testing System $host ($host)\n";

	my($serverstate)='OK';

	eval {
		alarm(30);
		my($cmd)="ssh $login\@$host echo helloworld";
		print "$cmd\n" if $DEBUG;
		open( XX, $cmd ." 2>&1 |" ) or die "Cant Run $cmd $!\n";
		my($found)=0;
		my($text)='';
		while(<XX>) {
			chomp;chomp;
			next if $_ eq "helloworld";
			$found++;
			$text.=$_."\n";
			print "*** ",$_,"\n";
		}
		close(XX);

		if( $found==0 ) {
			print "SSH Test Succeeded\n";
		} else {
			print "SSH Test Failed\n";
			$serverstate="ERROR";
		}

		alarm(0);
	};
	alarm(0);

	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			print "Command Timed Out\n";
			$serverstate="ERROR";
		} else {
			print "Command Returned $@\n";
		}
	}

	if( $serverstate eq 'OK' ) {
		MlpHeartbeat(
				-monitor_program 	=> $BATCH_ID,
				-system				=> $host,
				-subsystem			=> "ssh connectivity",
				-state				=> "OK",
				-message_text		=> "GEM_012: ssh $login\@$host succeeded from $hostname as user=$ENV{USER}" )
							if $BATCH_ID;
	} else {
			my($msg)="GEM_012: \'ssh $login\@$host echo helloworld\' failed from $hostname as user=$ENV{USER}";
			push @errors,$msg;
			print "no --BATCHID: NOT " unless $BATCH_ID;
			print "Alerting Host $host As Connection Error = Key $BATCH_ID\n";
			MlpHeartbeat(
				-monitor_program 	=> $BATCH_ID,
				-system				=> $host,
				-subsystem			=> "ssh connectivity",
				-state				=> "ERROR",
				-message_text		=> $msg )
								if $BATCH_ID;
	}
}


__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

