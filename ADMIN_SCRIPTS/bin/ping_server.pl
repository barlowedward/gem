#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
use Sys::Hostname;
use CommonFunc;
use Repository;
use MlpAlarm;

use vars qw( $SERVER $TYPE $NOLOG $DEBUG $BATCH_ID $NOLOCK );

# Copyright (c) 2001-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
	print "ping_server.pl [ -SERVER=SERVER ] -NOLOCK -NOLOG --DEBUG --TYPE=SYBASE|SQLSVR\n";
   return "\n";
}

$| =1;

die usage("") unless GetOptions(
	"SERVER=s"		=>	\$SERVER,
	"NOLOG"			=>	\$NOLOG,
	"TYPE=s"			=>	\$TYPE,
	"NOLOCK"			=> \$NOLOCK,
	"BATCH_ID=s"	=> \$BATCH_ID,
	DEBUG				=>\$DEBUG);

$BATCH_ID="PingServer" unless defined $BATCH_ID;

if( ! $NOLOCK ) {
	print "NOLOCK IS NOT DEFINED!\n" if $DEBUG;
	if( Am_I_Up(-debug=>$DEBUG,-prefix=>$BATCH_ID,-threshtime=>120) ) {
		my($fname,$secs,$thresh)=GetLockfile(-prefix=>$BATCH_ID);
		print "Cant Start ping_SERVER.pl - it is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nLock File=$fname secs=$secs thresh=$thresh secs\n";
		exit(0);
	}
} else {
	print "NOLOCK IS DEFINED!\n" if $DEBUG;
}
$TYPE=uc($TYPE);


my(@serverlist);
my(@sybaselist)=get_password(-type=>"sybase") if ! defined $TYPE or $TYPE eq "SYBASE";
my(@sqllist)   =get_password(-type=>"sqlsvr") if is_nt() and (! defined $TYPE or $TYPE eq "SQLSVR");
my(@oralist)   =get_password(-type=>"oracle") if is_nt() and (! defined $TYPE or $TYPE eq "ORACLE");
my(%servertype);
foreach( @sybaselist)  { $servertype{$_}="sybase"; }
foreach( @sqllist)  {    $servertype{$_}="sqlsvr"; }
foreach( @oralist)  {    $servertype{$_}="oracle"; }

print "ping_server.pl: run at ". localtime(time). " on host ". hostname(). "\n";
# print "ping_server.pl run at ".localtime(time)."\n";
print "BatchId=$BATCH_ID\n";
print "Found ",($#sybaselist+1)," Sybase Servers\n" 	if ! defined $TYPE or $TYPE eq "SYBASE";
print "Found ",($#sqllist+1)," SQL Servers\n" 			if ! defined $TYPE or $TYPE eq "SQLSVR";;
print "Found ",($#oralist+1)," Oracle Servers\n" 		if ! defined $TYPE or $TYPE eq "ORACLE";;

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-AGENT_TYPE=>'Gem Monitor') unless $NOLOG;
if( defined $SERVER) {
	push @serverlist,$SERVER;
	my(@servers)=split(/,/,$SERVER);
} else {
	push @serverlist, @sybaselist;
	push @serverlist, @sqllist;
	push @serverlist, @oralist;
}

use DBI_discovery;
my(%HASH)=fetch_dbi_data_sources();
my(%servers_with_odbc_setup);
if( is_nt()) {
	foreach ( keys %HASH ) {
		s/odbc://i;
		$servers_with_odbc_setup{$_}=1;
	}
}

#if( is_nt()) {
#    eval {  DBI->data_sources("ODBC");	};
#    if( $@ ) {
#	     	print "ERROR: can not load DBD::ODBC : $@\n";
#        	exit(1);
#    } else {
#     	my(@dat)= grep(s/DBI:ODBC://i,DBI->data_sources("ODBC"));
#     	foreach (@dat) {
#     		$servers_with_odbc_setup{$_}=1;
#     	}
#   }
#}

my($lengthof)=0;
foreach (@serverlist) { $lengthof=length if length>$lengthof; }
sub empty_sub {}
dbi_set_option(-print=>\&empty_sub);
my($numbad)=0;
foreach my $server (@serverlist) {
	my($login,$passwd,$conntype)=get_password(-name=>$server,-type=>$servertype{$server});
	die usage("No username\n" )	  	unless defined $login;
	die usage("No password\n" )	  	unless defined $passwd;
	printf("%".$lengthof."s ", $server);

	I_Am_Up(-prefix=>$BATCH_ID) unless $NOLOCK;
	if( is_nt() and ! $servers_with_odbc_setup{$server} ) {
			$numbad++;
			print " failed (no odbc dsn)\n";
			MlpHeartbeat( -monitor_program=>$BATCH_ID,
					-system=>$server,
					-subsystem=>"ping",
					-message_text=>"GEM_016: Database Ping from ".hostname()." to $server Failed - NO ODBC DSN",
					-state=>'ERROR' ) unless $NOLOG;
	} else {
		my($rc) = dbi_connect(-srv=>$server,-login=>$login,-password=>$passwd);
		dbi_disconnect();
		if( $rc eq "1" ) {
			print " ok...\n";
			MlpHeartbeat( -monitor_program=>$BATCH_ID,
					-system=>$server,
					-subsystem=>"ping",
					-message_text=>"database ping succeeded",
					-state=>'OK' ) unless $NOLOG;
		} else {
			$numbad++;
			print " failed\n";
			MlpHeartbeat( -monitor_program=>$BATCH_ID,
					-system=>$server,
					-subsystem=>"ping",
					-message_text=>"GEM_017: Database Ping from ".hostname()." to $server Failed",
					-state=>'CRITICAL' ) unless $NOLOG;
		}
	}
}
my($cnt)=$#serverlist +1 - $numbad;
print  $cnt." servers are reachable\n";
print "$numbad servers not reachable\n";
I_Am_Down(-prefix=>$BATCH_ID);
MlpBatchJobEnd() unless $NOLOG;
exit(0);

__END__

=head1 NAME

ping_server.pl - utility to ping databases

=head2 USAGE

	ping_server.pl -SERVER=list -NOLOG
	ping_server.pl

=head2 SYNOPSIS

pings all servers in your password files.  Returns number of servers
that cant be connected to.  Prints status of your servers.  Has no
embedded error handling.

IF -SERVER is passed, you can specify a comma separated list of servers

=cut

