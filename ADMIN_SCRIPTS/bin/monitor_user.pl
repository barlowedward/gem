#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use Getopt::Long;
use strict;
use DBIFunc;
use CommonFunc;

use vars qw( $USER $SERVER $PASSWORD $CONTINUOUS $REINIT $LOGFILE $MONUSER $MONSPID $DEBUG $SLEEPTIME $ITERATIONS );
die ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"USER=s"	=> \$USER,
		"PASSWORD=s" 	=> \$PASSWORD,
		"MONSPID=s"	=> \$MONSPID,
		"CONTINUOUS"	=> \$CONTINUOUS,
		"REINIT"	=> \$REINIT,
		"SLEEPTIME=s"	=> \$SLEEPTIME,
		"ITERATIONS=s"	=> \$ITERATIONS,
		"LOGFILE=s"	=> \$LOGFILE,
		"MONUSER=s"	=> \$MONUSER,
		"DEBUG"      	=> \$DEBUG );

sub usage
{
  return "@_
	USAGE: monitor_user.pl -USER=USER -PASSWORD=PASSWORD -SERVER=SERVER
		-DEBUG debug mode
		-MONSPID= spid to monitor
		-MONUSER= user to monitor
		-LOGFILE=file
		-REINIT
		-SLEEPTIME=
		-ITERATIONS= or  -CONTINUOUS=
";
}

$|=1;

if( !defined($SERVER)) { die usage("Error: Must Pass Server\n"); }
if( !defined($USER)) { die usage("Error: Must Pass User\n"); }
if( !defined($PASSWORD)) { die usage("Error: Must Pass Password\n"); }
#if( !defined $MONUSER and ! defined $MONSPID) { die usage("Error: Must Pass -MONUSER or -MONSPID\n"); }
$SLEEPTIME=30 unless $SLEEPTIME;
$ITERATIONS=1000000 if $CONTINUOUS;
$ITERATIONS=3 unless $ITERATIONS;

if( $LOGFILE ) {
	open( LOG, ">".$LOGFILE ) or die "Cant write $LOGFILE";
}

sub output {
	print  @_ ;
	print LOG @_ if $LOGFILE;
}

output(  "monitor_user.pl: MDA Table User/Process Monitor V1.0\n" );
output(  "Connecting to sybase server $SERVER \n" ) if defined $DEBUG;
dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD)	or die "Cant connect to $SERVER as $USER\n";

output(  "\nINITIALIZING\n" );

my($qsetup);
if( $MONUSER ) {
	$qsetup="select spid=SPID, Login, Application, SecondsConnected into #tmp from monProcess where Login='".$MONUSER."'";
} elsif( $MONSPID ) {
	$qsetup="select spid=SPID, Login, Application, SecondsConnected into #tmp from monProcess where SPID=".$MONSPID;
} else {
	$qsetup="select spid=SPID, Login, Application, SecondsConnected into #tmp from monProcess ";
}

foreach( dbi_query(-db=>"master", -query=>$qsetup ) ) {
	die "ERROR: Query Returned: ",join(" ",dbi_decode_row($_));
}

my(%MP);
foreach( dbi_query(-db=>"master", -query=>"select spid, Login, Application, SecondsConnected from #tmp " ) ) {
	my(@x)=dbi_decode_row($_);
	$MP{$x[0]}=$_;
	output(  "STARTING MONITORING of spid $x[0] (login=$x[1], app=$x[2], SecCon=$x[3])\n" );
}

output(  "\nStarting Monitoring\n" );

my($q1)="select SPID,PlanText from monSysPlanText, #tmp where  SPID=spid order by SPID,PlanID,BatchID,SequenceNumber ";

my($q2a)="select m.SPID, p.SecondsConnected /* StartTime=convert(char(8),StartTime,8)*/,
	CPUTime,MemUsageKB, PhyReads=PhysicalReads,LoReads=LogicalReads,/* PhysicalWrites */ p.Login, Conn=p.SecondsConnected,
	PhysicalWrites,Tables=TableAccesses,Indexes=IndexAccesses
	from master..monProcessActivity m, master..monProcess p, #tmp where m.SPID=spid and m.SPID=p.SPID and p.SPID=spid";

my($q2)="select m.SPID, StartTime=convert(char(8),StartTime,8),CpuTime,MemUsageKB, PhysicalReads,LogicalReads, p.Login, p.SecondsConnected
	from master..monProcessStatement m, master..monProcess p, #tmp where m.SPID=spid and m.SPID=p.SPID and p.SPID=spid";

my($q3)="select SPID,Batch=BatchID,SQLText from monSysSQLText m,#tmp t
	where m.SPID=t.spid
	order by SPID,BatchID,SequenceInBatch";

my($q4)="select SPID,Num=LineNumber,SQLText
	from monProcessSQLText m,#tmp t
	where m.SPID=t.spid
	order by SPID,LineNumber,SequenceInLine";

my($q5)="select p.SPID,SecondsWaiting,'Wait Reason'=i.Description
	from    master..monProcess p, master..monWaitEventInfo i,#tmp t
	where p.WaitEventID = i.WaitEventID
	and     i.WaitEventID!=250
	and p.SPID=t.spid";

my($q6)="select s.SPID, 'Event'=substring(i.Description,1,40),WaitTime,Waits
        from master..monWaitEventInfo i, master..monProcessWaits s,#tmp t
        where s.WaitEventID= i.WaitEventID
        and     WaitTime>1000
        and s.SPID=t.spid";

my($q7)="select SPID,ID=convert(char(2),ContextID),
        StackType=convert(varchar(19),ObjectType),
        StackObject=convert(varchar(50),DBName+'.'+OwnerName+'.'+ObjectName)
from master..monProcessProcedures,#tmp t
where SPID=spid order by ContextID";

my($q8)="select distinct SPID, PReads=PhysicalReads,LReads=LogicalReads,APFreads=PhysicalAPFReads,
	ObjectType,'Open Object'=convert(varchar(40),DBName+'.'+ObjectName)
from     master..monProcessObject,#tmp
where  SPID=spid";

my($q8)="select distinct SPID, ObjectType,'Open Object'=convert(varchar(40),DBName+'.'+ObjectName)
from     master..monProcessObject,#tmp
where  SPID=spid";

my($q8)="select distinct SPID, ObjectName,ObjectType
from     master..monProcessProcedures,#tmp
where  SPID=spid";


my(%Stime, %Cpu, %Mem, %PR, %LR , %Login, %ConnTime );
my($ltime);
while ($ITERATIONS--) {
	$ltime=time;
	if( $REINIT ) {
		foreach( dbi_query(-db=>"master", -query=>"drop table #tmp" ) ) {
			die "ERROR: Query Returned: ",join(" ",dbi_decode_row($_));
		}
		foreach( dbi_query(-db=>"master", -query=>$qsetup ) ) {
			die "ERROR: Query Returned: ",join(" ",dbi_decode_row($_));
		}
	}

	$ltime=time;
	my(@rc)=dbi_query(-db=>"master", -query=>$q2a, -print_hdr=>1 );
	output(  (time-$ltime)," ",localtime(time)." ===================================================\n" );
	output(  join("\n",dbi_reformat_results(@rc)) );
	output(  "\n\n" );
	shift @rc;

	if( ! $REINIT ) {
		foreach ( @rc ) {
			my(@x)=dbi_decode_row($_);

			if( ! defined $Cpu{$x[0]} ) {
				$Stime{$x[0]}=$x[1];
				$Cpu{$x[0]}=$x[2];
				$Mem{$x[0]}=$x[3];
				$PR{$x[0]}=$x[4];
				$LR{$x[0]}=$x[5];
				$Login{$x[0]}=$x[6];
				$ConnTime{$x[0]}=$x[7];
			}

			#output( f( "SPID=%4s Cpu=%5s Mem=%5s PhyReads=%5s LogReads=%5s login=%s\n",
			#		$x[0], $x[2]-$Cpu{$x[0]}, $x[3]-$Mem{$x[0]}, $x[4]-$PR{$x[0]}, $x[5]-$LR{$x[0]}, $x[6] )
			#		if $Login{$x[0]} eq $x[6] and $ConnTime{$x[0]} eq $x[7];

			if( $Login{$x[0]} ne $x[6] or $ConnTime{$x[0]} > $x[7] ) {
				output(  "No Longer Monitoring Spid $x[0]\n" );
				output(  "L=$Login{$x[0]} ne $x[6] or C=$ConnTime{$x[0]} < $x[7] \n" );
				dbi_query(-db=>"master", -query=>"delete #tmp where spid=".$x[0]);
			}
			$ConnTime{$x[0]}=$x[7];
		}
	}
	$ltime=time;
	my(@rc)=dbi_query(-db=>"master", -query=>$q1, -print_hdr=>1 );
	output(  (time-$ltime)," PLAN TEXT\n",join("\n",dbi_reformat_results(@rc)), "\n\n" )
		if $#rc>=0;

#	if( $#rc<0 ) {
#		output(  " [ No New Showplans Run ]\n" ) unless $CONTINUOUS;
#		sleep($SLEEPTIME);
#		next;
#	}
#
#	output(  "PLAN TEXT\n" );
#	output(  join("\n",dbi_reformat_results(@rc)) );
#	output(  "\n\n" );

#	foreach( @rc ) {
#		my(@x)=dbi_decode_row($_);
#		soutput( f("%4d %s\n",$x[0],$x[1]);
#	}
#	output(  "\n\n";

	$ltime=time;
	@rc=dbi_query(-db=>"master", -query=>$q3, -print_hdr=>1 );
	output( (time-$ltime),"SYSSQLTEXT\n",join("\n",dbi_reformat_results(@rc)), "\n\n" )
		if $#rc>=0;

#	foreach( @rc ) {
#		my(@x)=dbi_decode_row($_);
#		output(  join("\t",@x),"\n";
#		#soutput( f("%4d %s\n",$x[0],$x[1]);
#	}

	$ltime=time;
	@rc=dbi_query(-db=>"master", -query=>$q4, -print_hdr=>1 );
	output( (time-$ltime),"PROCESS SQLTEXT\n",join("\n",dbi_reformat_results(@rc)), "\n\n" )
		if $#rc>=0;

	$ltime=time;
	@rc=dbi_query(-db=>"master", -query=>$q5, -print_hdr=>1 );
	output( (time-$ltime)," BLOCKS & WAITS\n",join("\n",dbi_reformat_results(@rc)), "\n\n" )
		if $#rc>=0;

#	$ltime=time;
#	@rc=dbi_query(-db=>"master", -query=>$q6, -print_hdr=>1 );
#	output( (time-$ltime)," PROCESS WAITS\n",join("\n",dbi_reformat_results(@rc)), "\n\n" )
#		if $#rc>=0;

	$ltime=time;
	@rc=dbi_query(-db=>"master", -query=>$q7, -print_hdr=>1 );
	output( (time-$ltime)," SYSTEM STACK\n",join("\n",dbi_reformat_results(@rc)), "\n\n" )
		if $#rc>=0;;

	$ltime=time;
	@rc=dbi_query(-db=>"master", -query=>$q8, -print_hdr=>1 );
	output( (time-$ltime)," OPEN OBJECTS\n",join("\n",dbi_reformat_results(@rc)), "\n\n" )
		if $#rc>=0;;

	sleep($SLEEPTIME);
}

foreach( dbi_query(-db=>"tempdb", -query=>"drop table #tmp" ) ) {
	die "ERROR: Query Returned: ",join(" ",dbi_decode_row($_));
}

output(  "Program Completed.\n" );
close(LOG);
exit(0);

sub get_pinfo
{
	foreach ( dbi_query(-db=>"tempdb", -query=>"select MONSPID, Login, Application, SecondsConnected from monProcess" ) ) {
		my(@rc)=dbi_decode_row($_);
		output(  "MONITORING spid : ",$rc[0],"\n" );
	}
}
__END__

=head1 NAME

monitor_user.pl - utility to debug servers

=head2 DESCRIPTION

Check out your server!  Should audit as much as it can!

=head2 USAGE

	monitor_user.pl [-dh] -UUSER -PPASSWORD -SSERVER

=head2 DETAILS

	* Checks stored procedure library
	* run sp__auditsecurity
	* check wierd select_into/trunc option definition on databases
	* checks sysprocesses for blocked, log suspend, infected, sleeping io,
		or stopped status
	* foreach database
	->  space used (data&log) from sp__qspace
	->  sp__auditdb
	->  database options

  database options

  configuration (default memory, allow updates, connections, updates)
	 -> not implemented
=cut
