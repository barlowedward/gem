#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

my(%MAX_VERSION_SET) = (
"Microsoft SQL Server 2000" =>  8.2055,
"Microsoft SQL Server 2005" => 9.5000,
"Microsoft SQL Server 2008" => 10.4000,
"Microsoft SQL Server 2008 R2" => 10.1777
);

my(%VERSION_INFO) = (
'10.50.1777.00'	=> 'RTM CU #7 (2507770) Apr 18, 2011', 
'10.50.1765.00'	=>'RTM CU #6 (2489376) Feb 21, 2011',
'10.50.1753.00'	=>'RTM CU #5 (2438347) Dec 20, 2010',
'10.50.1746.00'	=>'RTM CU #4 (2345451) Oct 18, 2010',
'10.50.1734.00'	=>'RTM CU #3 (2261464) Aug 16, 2010',
'10.50.1720.00'	=>'RTM CU #2 (2072493) Jun 21, 2010', 
'10.50.1702.00'	=>'RTM CU #1 (981355) May 18, 2010', 
'10.50.1600.1'	=>'RTM � May 10, 2010', 
'9.00.5266'	=>'SP4 CU #3 (2507769) Mar 22, 2011', 
'9.00.5259'	=>'SP4 CU #2 (2489409) Feb 21, 2010', 
'9.00.5254'	=>'SP4 CU #1 (2464079) Dec 23, 2010', 
'9.00.5000'	=>'SP4 RTW (PCU4) Dec 16, 2010', 
'9.00.4325'	=>'SP3 CU #15 (2507766) Mar 22 , 2011', 
'9.00.4317'	=>'SP3 CU #14 (2489375) Feb 21, 2011', 
'9.00.4315'	=>'SP3 CU #13 (2438344) Dec 20, 2010', 
'9.00.4311'	=>'SP3 CU #12 (2345449) Oct 18, 2010SP3', 
'9.00.4309'	=>'SP3 CU #11 (2258854) Aug 16, 2010', 
'9.00.4305'	=>'SP3 CU #10 (983329) Jun 21, 2010', 
'9.00.4294'	=>'SP3 CU #9 (980176) Apr 19, 2010', 
'9.00.4285'	=>'SP3 CU #8 (978915) Feb 16, 2010', 
'9.00.4273'	=>'SP3 CU #7 (976951) Dec 21, 2009', 
'9.00.4266'	=>'SP3 CU #6 (974648) Oct 19, 2009', 
'9.00.4230'	=>'SP3 CU #5 (972511) Aug 17, 2009', 
'9.00.4226'	=>'SP3 CU #4 (970279) June 15, 2009', 
'9.00.4220'	=>'SP3 CU #3 (967909) Apr 20, 2009', 
'9.00.4211'	=>'SP3 CU #2 (961930) Feb 16, 2009', 
'9.00.4207'	=>'SP3 CU #1 (959195) Dec 19, 2008', 
'9.00.4035'	=>'SP3 RTW (955706) Dec 15, 2008',
'8.00.2283'	=>'Post-SP4 hotfix for MS09-004 (971524)', 
'8.00.2282'	=>'MS09-004: KB959420 October 29,2008', 
'8.00.2273'	=>'MS08-040 - KB 948111 July 8, 2008',  
'8.00.2040'	=>'Post-SP4 AWE fix (899761)',
'8.00.2039'	=>'SQL Server 2000 SP4'
);

# Copyright (c) 2009-2011 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;
use Repository;
use GemReport;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- (native authentication) \n".
$PROGRAM_NAME." -DATABASE=db  -SERVER=SERVER
-or- \n".
basename($0)." 

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

# die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		# "NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		# "DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my(%MAX_VERSION_FOUND);
my($STARTTIME)=time;
my(%VERSION_COUNT);
report_init(-file_name=>'Mssql.Version', -addbreaks=>1 ) ;
report_print(0,"<h1>Sql Server Version Report</h1>");
my($ltime);
$ltime=localtime(time);
report_print(0,"Run At $ltime\n");
report_table(1,"border=1");
report_print(0, "Server","Mneumonic","Is Production","Patch","Notes" );
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> 1,
	DOALL				=> "sqlsvr",
	NO_CONNECTION_ALERTS => 1,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);
report_table(0);

report_print(1, "\n" );
report_print(0,"<h1>");
report_print(1, "SUMMARY RESULTS\n" );
report_print(0,"</h1>");

report_table(1,"border=1");
report_print(0, "Version","Max Version Found","Standard?" );
foreach ( sort keys %MAX_VERSION_FOUND ) {
	if( $MAX_VERSION_SET{$_} > $MAX_VERSION_FOUND{$_} ) {
		report_color('RED');
		report_print(0, $_,$MAX_VERSION_FOUND{$_},$MAX_VERSION_SET{$_} );	
		# report_print(1, " -- THE VERSION LEVEL IN mssql_checkversion.pl IS NOT THE LATEST! $MAX_VERSION_SET{$_} should be $MAX_VERSION_FOUND{$_}\n" );
		report_color('');
		MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> 'VerCheck',
					-state=> "ERROR",-subsystem=>'',
					-message_text=> "THE VERSION LEVEL IN mssql_checkversion.pl IS NOT THE LATEST" ) if $BATCH_ID;
	} else {
		report_print(0, $_,$MAX_VERSION_FOUND{$_},$MAX_VERSION_SET{$_} );	
		MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> 'VerCheck',
					-state=> "OK",-subsystem=>'',
					-message_text=> "THE VERSION LEVEL IN mssql_checkversion.pl IS OK" ) if $BATCH_ID;
	}
}
report_table(0);

report_print(1,"\n");

report_table(1,"border=1");
report_print(0, "Version","Patch Level","Num Servers");
foreach ( sort keys %VERSION_COUNT ) {
	my($v,$p)=split(/\|/,$_,2);
	report_print(1, $v,$p,$VERSION_COUNT{$_}." Servers\n" );	
}
report_table(0);
report_close();

exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init { 
	
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;

	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	
	# must be 4 digits to the right!
	my($release,$subrelease)=split(/\./,$PATCHLEVEL,2);
	if( $subrelease == 0  ) {
		$PATCHLEVEL = $release.".0000";
	} elsif( $subrelease < 10 ) {
		$PATCHLEVEL = $release.".000".$subrelease;
	} elsif( $subrelease < 100 ) {
		$PATCHLEVEL = $release.".00".$subrelease;
	} elsif( $subrelease < 1000 ) {		
		$PATCHLEVEL = $release.".0".$subrelease;
	}
	$VERSION_COUNT{$DBVERSION."|".$PATCHLEVEL}++;
	if( $DBTYPE eq "SQL SERVER" ) {
		#print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";
		$MAX_VERSION_FOUND{$DBVERSION} =  $PATCHLEVEL
		 	if ! $MAX_VERSION_FOUND{$DBVERSION} or  $MAX_VERSION_FOUND{$DBVERSION} < $PATCHLEVEL;
		my %info = get_password_info(-name=>$cursvr, -type=>'sqlsvr');
			
		if( ! $MAX_VERSION_SET{$DBVERSION} ) {
			my(@alert)= ("$cursvr", $info{MNEUMONIC}, $info{SERVER_TYPE}, "No max version set for $DBVERSION","Please edit mssql_checkversion.pl to set the approrpriate max version at the top of the page\n");
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> 'SvrVerCheck',
					-subsystem=>$cursvr,
					-state=> "ERROR",
					-message_text=> join(" ",@alert)  ) if $BATCH_ID;
			report_color('RED');
			report_print(1, @alert );
			report_color('');
		} elsif( $PATCHLEVEL < $MAX_VERSION_SET{$DBVERSION} ) {
			my(@alert)= ("$cursvr", $info{MNEUMONIC}, $info{SERVER_TYPE},"At patch level $PATCHLEVEL","Should be at $MAX_VERSION_SET{$DBVERSION}\n");
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> 'SvrVerCheck',
					-subsystem=>$cursvr,
					-state=> "ERROR",
					-message_text=> join(" ",@alert) ) if $BATCH_ID;
			report_color('RED');
			report_print(1, @alert );
			report_color('');
		} else {
			my(@alert)= ("$cursvr", $info{MNEUMONIC}, $info{SERVER_TYPE},"OK","At $PATCHLEVEL\n");
			MlpHeartbeat(
					-monitor_program => $BATCH_ID,
					-system=> 'SvrVerCheck',
					-subsystem=>$cursvr,
					-state=> "OK",
					-message_text=> join(" ",@alert)  ) if $BATCH_ID;
			report_print(0, @alert );
		}
		
		return;
	} else {
		die "This program is for sql server only\n";
#		print "SERVER $cursvr OF TYPE $DBTYPE VER $DBVERSION PAT $PATCHLEVEL\n";

		#my %info = get_password_info(-name=>$cursvr, -type=>'sqlsvr');
		#if( $info{SERVER_TYPE} ne "PRODUCTION" and $info{SERVER_TYPE} ne "CRITICAL"   ) {
		#	print "Skipping Server $srv As It is ",$info{SERVER_TYPE},"\n" if $DEBUG;
		#	next;
		#}
		

		# get current locked/expired status from the repository
		#my($q)="select credential_name, attribute_key, attribute_value
		#	print "(noexec)" if $NOEXEC and $DEBUG;
		#	print $q,"\n" if $DEBUG;
		#	MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
		#  my(@lastresults)=MlpAlarm::_querywithresults($q,1)  unless $NOEXEC
		#  foreach (@lastresults) {
		#		my(@vals) = dbi_decode_row($_);
	}
}

__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

