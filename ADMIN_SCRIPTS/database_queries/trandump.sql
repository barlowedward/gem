-- report_key=TRANDUMPHISTORY
-- report_type=HTML_AGGREGATE
-- run_frequency=daily
-- do_diff=N
-- keep_hours=N
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=This Report Lists 2 Days Of Database Tran Log Backup Information
-- report_footer=
-- enabled=Y
-- outfile=Trandump_History

SELECT 
	 database_name,
    backup_finish_date,
    datediff(ss,backup_start_date,backup_finish_date) as [Time in Seconds],
    backup_size
FROM msdb.dbo.backupset
where type='L'
and datediff(dd,backup_finish_date,getdate()) < 2
order by database_name, backup_finish_date desc
