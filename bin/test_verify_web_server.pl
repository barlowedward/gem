#
# TEMPLATE IS MY STANDARD MODULE FOR GEM TEMPLATE
#
use strict;
use Getopt::Long;
use CommonFunc;

my($PROGRAM_DESCRIPTION)="Verify Web Server";
my($VERSION)=1.0;
my(@errors);
use vars qw( $DEBUG );

die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless GetOptions( "DEBUG"	=> \$DEBUG );

print "$0 - v",$VERSION,"\n";
print "   Reading gem.dat\n";
my($GemConfig)=read_gemconfig(-debug=>$DEBUG);
# START BODY

print "ROLLOUT_DIRECTORY      = $GemConfig->{MIMI_FILENAME_WIN32} \n";
print "URL                    = $GemConfig->{MIMI_URL_WIN32} \n";
print "MIMI_CGIBIN_PERL_WIN32 = $GemConfig->{MIMI_CGIBIN_PERL_WIN32} \n";
print "MIMI_LOGIN_WIN32       = $GemConfig->{MIMI_LOGIN_WIN32} \n";

# END BODY
# standard footer
if( $#errors>= 0 ){
	foreach (@errors) { print "ERROR: $_\n"; }
	exit( $#errors+1);
}
print "\nSUCCESSFUL: $PROGRAM_DESCRIPTION\n\n";
close_gemconfig();
exit(0);