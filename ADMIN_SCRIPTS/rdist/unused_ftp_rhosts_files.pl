#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
use Sys::Hostname;
use Net::myFTP;
use strict;
use Repository;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die "Must have file named rhosts to put to the world!!!\n"
	unless -e "rhosts";

my $curhostname = hostname();
my(@hosts)=get_password(-type=>"unix");
foreach my $host (@hosts) {

   my($login,$password)=get_password(-type=>"unix",-name=>$host);
	next if $host eq $curhostname;

	my(%args)=get_password_info(-type=>"unix",-name=>$host);
	my($method);
	if( is_nt() ) {
		$method=$args{WIN32_COMM_METHOD} if is_nt();
	} else {
		$method=$args{UNIX_COMM_METHOD};
	}
	die "COMM_METHOD NOT DEFINED for SERVER $host\n" unless $method;

	if( $method eq "NONE" ) {	# hmm cant talk
		print "host $host: METHOD=NONE - skipping\n";
		return
	}

	print "  Creating FTP Connection To $host\n";
   my($ftp) = Net::myFTP->new($host,METHOD=>$method,Debug=>undef)
         or die "Failed To Create FTP Object $@";
   if( ! $ftp )  {
      warn "Failed To Connect To $host\n";
      next;
   }
   my($rc,$err)=	$ftp->login($login,$password);
	if( ! $rc )  {
      warn "Failed To Connect To $host : $err\n";
      next;
   }
   $ftp->ascii();

   if( ! $ftp->put("rhosts",".rhosts") ) {
      warn("WARNING: Couldnt Copy File rhosts to server: $!\n");
   }
}
