/*
select SERVER=server_name,DB=convert(varchar,database_name),machine_name,
backup_start_date,backup_finish_date,backup_size, duration=datediff(mi,backup_start_date,backup_finish_date),
physical_device_name
from msdb..backupset b, msdb..backupmediafamily f
where b.media_set_id = f.media_set_id
and   b.type='D'
and   datediff(dd,backup_start_date,getdate())<7
order by server_name,machine_name,database_name,backup_start_date
*/

SELECT database_name,
    MAX(backup_finish_date) as Last_Backup,
    media_set_id = convert(int,null),
    server_name,
    checkpoint_lsn=convert(numeric(25,0),null),
    backup_size=convert(numeric(20,0),null),
    backup_start_date=convert(datetime,null)
into #LatestBackups
FROM msdb.dbo.backupset
where type='D'
GROUP BY database_name, type, server_name

insert #LatestBackups (database_name,server_name)
select name,convert(varchar,SERVERPROPERTY('Servername') )
from master..sysdatabases where name not in ( select database_name from #LatestBackups )

update #LatestBackups
set		media_set_id= b.media_set_id, checkpoint_lsn=b.checkpoint_lsn,
		backup_start_date = b.backup_start_date,
		backup_size = b.backup_size
from	#LatestBackups m, msdb.dbo.backupset b
where	m.database_name=b.database_name
and		m.Last_Backup = b.backup_finish_date

select SERVERPROPERTY('Servername') as 'Server',b.database_name as 'Database',
	Last_Backup as 'Backup Time',
	datediff(dd,Last_Backup,getdate()) as 'Backup Age (Days)',
	physical_device_name,
	convert(int,backup_size/(1024*1024)) as 'Size In MB',
	backup_minutes=datediff(mi,backup_start_date,Last_Backup)
from #LatestBackups b
left outer join  msdb.dbo.backupmediafamily f on  b.media_set_id=f.media_set_id
where db_id(b.database_name) is not null
and SERVERPROPERTY('Servername') = server_name
order by 2,3

drop table #LatestBackups
