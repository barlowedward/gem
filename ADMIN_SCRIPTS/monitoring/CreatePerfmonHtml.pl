#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# creates perfmon html
print "CreatePerfmonHtml.pl v1.0\n";
print " This file modifies PERFMON_ALL_WIN32.htm\n";
print " This file uses a generic ODBC connection named PERFMON\n";
print " This file uses a generic database  named Win32_Perfmon\n";


# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;

die "Cant read PERFMON_ALL_WIN32.htm" unless -r "PERFMON_ALL_WIN32.htm";
die "Cant write PERFMON_ALL_WIN32.htm" unless -w "PERFMON_ALL_WIN32.htm";

my(@inlines);
open(IN,"PERFMON_ALL_WIN32.htm") or die "Cant open PERFMON_ALL_WIN32.htm\n";
foreach (<IN>) {
	push @inlines,$_;
}
close(IN);
my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." \n

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
}

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

open(OUT,"> PERFMON_ALL_WIN32.htm.out") or die "Cant open PERFMON_ALL_WIN32.htm\n";
my($curcounter)=1;
foreach (@inlines) {
	if( /Counter00001/ ) {
		print "Found Counter Line\n";
		last;
	}
	print OUT $_;	
}

my($STARTTIME)=time;
repository_parse_and_run(
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> "win32servers" ,
	BATCH_ID			=> $BATCH_ID ,	
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	returnonlycmdline => 1,
	NOSTDOUT			=> 1,
	server_command => \&server_command
);

my($canprint)=0;
foreach (@inlines) {
	chomp;chomp;
	$canprint++ if /Counter00001/;	
	next if $canprint==0 or /Counter00/ ;
	if( /CounterCount/ ) {
		$curcounter--;
		print OUT "\t<PARAM NAME=\"CounterCount\" VALUE=\"".$curcounter."\">\n";
		next;
	}	
	print OUT $_,"\n";
}
close(OUT);

exit(0);

sub init {}
sub server_command {
	my($cursvr)=@_;

	
	print "Cursvr=$cursvr\n";	
	if( $cursvr=~/^LN/ or $cursvr=~/^DR/ or $cursvr=~/CLUSTER/ or $cursvr=~/ADSDR/ or $cursvr=~/^PA/ or $cursvr=~/^SI/
		or $cursvr=~/004$/ or $cursvr=~/244$/ or $cursvr=~/013$/ or $cursvr=~/034$/ or $cursvr=~/074$/ or $cursvr=~/077$/
		or $cursvr=~/088$/ or $cursvr=~/149$/ or $cursvr=~/170$/ or $cursvr=~/193$/ or $cursvr=~/188$/ or $cursvr=~/033/
		or $cursvr=~/070$/ or $cursvr=~/067$/ or $cursvr=~/115$/ or $cursvr=~/121$/ or $cursvr=~/195$/ 	) {
		print "... skipped\n";
		next;
	}
	
	my($l)= 5 - length $curcounter;
	my($a)= substr("00000",0,$l).$curcounter;
	
	#print $a,"\n";
	#printf "%5.5s\n", $a;
	#printf "%5s\n", $a;
	#printf "%.5s\n", $a;
	print OUT "\t",'<PARAM NAME="Counter';
	printf OUT "%5s",$a;
	print OUT '.Path" VALUE="\\\\'.$cursvr.'\PhysicalDisk(*)\*">'."\n";
	$curcounter++;
	
	my($l)= 5 - length $curcounter;
	my($a)= substr("00000",0,$l).$curcounter;
	
	print OUT "\t",'<PARAM NAME="Counter';
	printf OUT "%5s",$a;
	print OUT '.Path" VALUE="\\\\'.$cursvr.'\Processor(*)\*">'."\n";
	$curcounter++;
	
}


__END__

=head1 NAME

RepositoryRun.pl

=head1 SYNOPSIS

