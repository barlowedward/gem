#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
use strict;
use Repository;
use File::Copy;
use Getopt::Std;
use vars qw($opt_g $opt_I $opt_R $opt_O $opt_D $opt_S $opt_h $opt_d $opt_c $opt_o);

$|=1;

my($base_col_width)="40";
my($progname)='depends_get.pl';
sub usage {
        die "@_
Syntax:  $progname -D Database -S Server -I Input_File -O object -hdc
	-S : Server - optional to help find Input_File
   -O : Object to search for - may contain wild card
   -g : gif subdirectory url path (relative.. ie /gifs or gifs)
   -h : html output
   -c : insert/update/delete only
   -o : output file
   -d : debug
   -R : Produce A Full File Report
";
}

sub pr_output {
	if( $opt_o )  {
		#print "DBG OUTPUT FILE> ",@_;
		print OUTPUT @_;
	} else {
		print @_;
	}
}
sub pr_error {
	pr_output("<FONT COLOR=RED>");
	pr_output(@_);
	pr_output("</FONT>");
}


usage("Bad Parameter List")     								unless getopts('I:F:O:D:dhg:cS:R');

die usage("Must Pass Object With -O")           		unless defined $opt_O or $opt_R;
die usage("Must Pass Database With -D")         		unless defined $opt_D or $opt_R;
die usage("Must Pass Intput File Or Server")         	unless defined $opt_I or $opt_S or $opt_R;

# print "DBG: Setting Change Mode to 1 - This should be removed in toolkit - EMB\n";
# $opt_c = 1;

$opt_g = "http://www.edbarlow.com/gifs/" unless $opt_g;
$opt_g .= "/" unless $opt_g =~ /\/$/;

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

my(%bitmap) = (
        'P'     => 'diamond.gif'        ,
        'U'     => 'red_swirl.gif',
        'V'     => 'star.gif'           ,
        'T'     => 'swirl.gif'          ,
        'S'     => 'redarrow.gif'       ,
        'TR'    => 'bullet.gif' ,
        'XP'    => 'yelarrow.gif' ,
        'X'    => 'yelarrow.gif' ,

        'P '     => 'diamond.gif'        ,
        'U '     => 'red_swirl.gif',
        'V '     => 'star.gif'           ,
        'T '     => 'swirl.gif'          ,
        'S '     => 'redarrow.gif'       ,
        'TR'    => 'bullet.gif' ,
        'XP'    => 'yelarrow.gif' ,
        'X '    => 'yelarrow.gif' ,
);

my(%objtypmap) = (
        'P'     => 'Procedure'        ,
        'U'     => 'Table    ',
        'V'     => 'View     '           ,
        'T'     => '????     '          ,
        'S'     => 'System   '       ,
        'TR'    => 'Trigger  ' ,
        'XP'    => 'XProc    ' ,
        'X'    => 'XProc    ' ,

		  'P '     => 'Procedure'        ,
        'U '     => 'Table    ',
        'V '     => 'View     '           ,
        'T '     => '????     '          ,
        'S '     => 'System   '       ,
        'TR'    => 'Trigger  ' ,
        'XP'    => 'XProc    ' ,
        'X '    => 'XProc    ' ,
);

my(%uses);
my(%obj_type);
my(%db_by_obj,%obj_by_obj);
my(%anchorlist);
my($curanchor)=0;


my($SOURCEDIR)=get_gem_root_dir()."/data/depends/";
if( $opt_R and ! $opt_S ) {
	# LOOP THROUGH ALL THE POSSIBLE FILES
	die unless -d $SOURCEDIR;;
	opendir(DIR,$SOURCEDIR);
	my(@dirlist)=grep(/^depends\./,readdir(DIR));
	closedir(DIR);
	foreach (@dirlist) {
		next if /.html/;
		s/^depends\.//;
		$opt_S = $_;
		$opt_I = $SOURCEDIR."depends.$opt_S";
		$opt_o = $SOURCEDIR."depends.".$opt_S.".html";
		do_one_dependsfile();
	}

} else {
	if( defined $opt_S and ! defined $opt_I ) {
		$opt_I= $SOURCEDIR."depends.$opt_S";
		$opt_o = $SOURCEDIR."depends.".$opt_S.".html";
		die "Cant find $SOURCEDIR/depends.$opt_S" unless -r $opt_I;
	}
	do_one_dependsfile();
}
exit();

sub do_one_dependsfile {
	%uses=();
	%obj_type=();
	%anchorlist=();
	$curanchor=0;
	%db_by_obj=();
	%obj_by_obj=();

	if( $opt_o ) {
		print "SAVING OUTPUT FOR $opt_S in $opt_o\n";
		open(OUTPUT,">$opt_o") or die "Cant write $opt_o\n";
	}
	die usage("Must Pass Input File With -I")       unless defined $opt_I;
	die usage("Input File Does Not Exist")          unless -r $opt_I;
	open (FILE, $opt_I)        || die "Could not open $opt_I: $!\n";
	my(@inlines)=<FILE>;
	close FILE;

	pr_output( "<h2>" ) if defined $opt_h;
	pr_output( "Dependency Formatter v1.5 : Run At ".localtime(time)."\n" ) ;
	pr_output( "</h2>") if defined $opt_h;

	# FIND LIST OF OBJECTS
	foreach ( @inlines ) {
		chop;chomp;chomp;
		# LINE FORMAT obj|type|verb:obj|verb:obj...
		#pr_output( "LINE=$_\n")

		my(@words) = split(/\|/,$_,3);
		chomp $words[0];
		chomp $words[1];
		chomp $words[2];
		$words[1] =~ s/\s//g;
		#if( $#words<1 ) {
		#	pr_output( "LINE=$_\n", "Skipping Line as only $#words Words Detected\n") if $opt_d;
		#	next;
		#}
		if( length($words[1])<=2 and defined $bitmap{$words[1]} ) {
			my(@x)=split(/\./,$words[0]);
			if( $#x != 2 ) {
				pr_output( "LINE=$_",$NL, $#x," ODD PARSE : ",$words[0],$NL);
				next;
			}

			if( $#x>3 ) {
				print( "LINE=$_",$NL, "$words[0] Parses Into $#x Parts\n") if $opt_d;
				next;
			}
			$db_by_obj{$words[0]}=$x[0];
			$obj_by_obj{$words[0]}=$x[2];
			$obj_type{$words[0]}=$words[1];

			if( $words[1] ne "S" ) {
				$curanchor++;
				$anchorlist{$words[0]}=$curanchor;
				# print "LINE ",__LINE__," anchorlist{ $words[0] } = $curanchor\n";
			}
			$words[2] =~ s/\s+$//;
			chomp $words[2];
			#X# print "DBG DBG: uses $words[0] is set to $words[2]\n";
			$uses{$words[0]}=$words[2];
		} elsif( $words[1] eq 'D'
				or $words[1] eq 'R'
				or $words[1] =~ /^\s*$/
				or $words[1]=~/^select/
				or $words[1] eq 'C'
				or $words[1] eq 'F'
				or $words[1] eq 'L'
				or $words[1] eq 'K'
				or $words[1] eq 'IF'
				or $words[1] eq 'FN'
				or $words[1] eq 'IN'
				or $words[1] eq 'PK'
				or $words[1] eq 'RF'
				or $words[1] eq 'TF'
				or $words[1] eq 'UQ'
				) {
		} elsif( length($words[1])<=2  ) {
			pr_output( "LINE=$_",$NL, "WARNING : object type '",$words[1],"' Has No Bitmap Defined",$NL);
		} else {
			print( "LINE=$_",$NL, "FAILURE TO PARSE: $_\nNUM WORDS=",$#words," OBJ TYPE=|",$words[1],"| LEN=",length($words[1]),$NL) if $opt_d;
		}
	}

	#	/^([^|]+)\|(..)\|(.+)$/;
	#	my($key,$otype,$extra)=($1,$2,$3);
	#	if( ! $key or ! $otype ) {
	#		pr_output( "FAILURE TO PARSE: $_\n");
	#		next;
	#	}
	#	pr_output( "KEY is $1 TYPE=$otype\n");
	#	$obj_type{$key}=$otype;
	#	$uses{$key}=$extra;

	#	if( /^[\w\d\_]+\.\.[\w\d\_]+\.[\w\d\_]+\|[\w\_]+\s*\|/ ) {
	#		# COLUMN FROM PERHPAS A VIEW
	#		pr_output( "FOUND TYPE 1\n");
	#		s/^([\w\d\_]+\.\.[\w\d\_]+)\.[\w\d\_]+\|([\w\_]+)\s*\|//;
	#		$key  =$1;
	#		$obj_type{$key}=$2;
	#  	} else {
	#		if( !  /^[\w\d\_]+\.\.[\w\d\_]+\|[\w\_]+\s*\|/ ) {
	#			if( !  /^[\w\d\_]+\.\./ ) {
	#				die "FAILURE TO PARSE 1: $_\n";
	#			}
	#			if( !  /^[\w\d\_]+\.\.[\w\d\_]+/ ) {
	#				die "FAILURE TO PARSE 3: $_\n";
	#			}
	#			if( !  /^[\w\d\_]+\.\.[\w\d\_]+\|[\w\_]+\s*\|/ ) {
	#						die "FAILURE TO PARSE 4: $_\n";
	#			}
	#			die "FAILURE TO PARSE 2: $_\n";
	#			next;
	#		};
	#		s/^([\w\d\_]+\.\.[\w\d\_]+)\|([\w\_]+)\s*\|//;
	#		$key  =$1;
	#		$obj_type{$key}=$2;
	#	}

	#	unless( defined $obj_type{$key} and $obj_type{$key} ne "" ) {   # perhaps format obj|type
	#		pr_output( "IGNORING: $_\n");
	#		next;
	#	}

	#	pr_output( "PARSED OBJ=$key / TYPE=$obj_type{$key}\n");
		#my($key)  =$1;
		#$obj_type{$key}=$2;
	 # 	$uses{$key}=$_;

	if( $opt_R ) {
		foreach (sort keys %obj_type) {
			if( $opt_D ) {
				next unless $db_by_obj{$_} eq $opt_D;
				printf " ==> DB: %-20s OBJ: %s\n",$db_by_obj{$_},$obj_by_obj{$_};
				do_one_database_object( $db_by_obj{$_}, $obj_by_obj{$_} );
			} else {
				printf " ==> DB: %-20s OBJ: %s\n",$db_by_obj{$_},$obj_by_obj{$_};
				do_one_database_object( $db_by_obj{$_}, $obj_by_obj{$_} );
			}
		}
	} else {
		do_one_database_object($opt_D,$opt_O);
	}
	if( $opt_o ) {
		close(OUTPUT);
	}
}

sub do_one_database_object {
	my($DATABASE,$OBJNAME)=@_;
	return if $OBJNAME=~/\$/;
	my($searchstr)  =       $DATABASE."\.\.".$OBJNAME;
	my($prstring)=$searchstr;
	$prstring=~s/\\w/\(word\)/g;
	$prstring=~s/\\s/\(space\)/g;

	my(@objlist);
	foreach (keys %uses) {
	        next unless /^$searchstr$/;
	        #X# print "DBG DBG: push uses $_ onto objlist stack\n";
	        push @objlist,$_;
	}

	if( $#objlist<0 ) {
		print "ERROR no objects matching $searchstr found...\n" ;
		return;
	}

	my($rowheader)="";
	if( $opt_h ) {
	  	if( defined $anchorlist{$DATABASE."..".$OBJNAME} ) {
			$rowheader.= "<A NAME=link".$anchorlist{$DATABASE."..".$OBJNAME}.">" ;
			$rowheader.="<h2>DB=$DATABASE OBJECT=$OBJNAME</h2>";
			$rowheader.="</A>\n";
		} else {
			$rowheader.= "UNKNOWN ANCHOR FOR $DATABASE."..".$OBJNAME\n";
			$rowheader.="<h2>DB=$DATABASE OBJECT=$OBJNAME</h2>";
		}
	}

	my($tbl_started)="NO";
	foreach my $obj ( sort @objlist ) {
	   next if defined $opt_c and $obj_type{$obj} ne "U";
	   next unless $obj_type{$obj};
	   next if $obj=~/^tempdb../;
	   $obj=~s/\s+$//;
	   chomp $obj;

		if( defined $opt_h and $tbl_started eq "NO"	and $obj_type{$obj} ne "S" ) {
			pr_output( $rowheader."\n"."<TABLE WIDTH=100%>\n");
			$tbl_started = "YES";
	   }
		show_output($obj,0,"");
	}
	pr_output( "\n</TABLE>\n") if $tbl_started eq "YES";
}

############################################################################
# Recursively Print Output
sub show_output
{
    my($obj,$offset,$verb)=@_;
    #X# print __LINE__," OFFSET=$offset DBG DBG: OBJ=$obj\n";
    my($object_type)=$obj_type{$obj};
    my($otyp_name)=$objtypmap{$object_type} if defined $object_type;
    my($otyp_bitm)=$bitmap{$object_type} if defined $object_type;

    if( ! defined $object_type ) {
    	 # print "WARNING: $obj has no defined object_type\n";
    } else {
	    print "SKIPPING SYSTEM OBJECT $obj AT LEVEL #1",$NL if $object_type eq "S" and $offset==0 and $obj !~ /\.\.sys/i;
	    return if $object_type eq "S" and $offset==0;	# skip system tables at the top level
	 }

    pr_output( "DBG: object=$obj offset=$offset verb=$verb",$NL) if defined $opt_d;
    return if $verb =~ /by\s*$/ and $offset>1;
    return if $offset>7;
    # return if defined $opt_c and $verb =~ /select/;
    # return if defined $opt_c
				# and $object_type ne "TR" and $object_type ne "U";
		#X# print __LINE__," OFFSET=$offset DBG DBG: OBJ=$obj\n";
    if( defined $opt_h ) {
        pr_output( "<TR><TD>\n");
        for (1..$offset) { pr_output( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"); }
        pr_output( "=>&nbsp;");
    } else {
        pr_output( $object_type);
        pr_output( " ") if length($object_type)==1;
        for (1..$offset) { pr_output( "  ");}
    }

    if( defined $opt_h ) {
       if( defined $otyp_bitm ) {
           pr_output( "<IMG SRC=".$opt_g.$otyp_bitm." ALT='obj type=".$otyp_name."'>\n");
       } else {
           pr_output( "UNKNOWN TYPE=$object_type") if $object_type;
       }
    }
	#X# print __LINE__," OFFSET=$offset DBG DBG: OBJ=$obj\n";
    pr_output( $verb," ") if $verb ne "";
    if( $opt_h and defined $anchorlist{$obj} and $offset!=0 ) {
    	   pr_output( "<A HREF=\"#link".$anchorlist{$obj}."\">".$obj."</A>");
	 } elsif( $opt_h and defined $anchorlist{$obj} and $offset=0 ) {
	 		# dont hyperlink top level objects... its redundant
	 		pr_output( $obj);
	 } elsif( $opt_h and ! defined $anchorlist{$obj} ) {
	 		#X# print __LINE__," OFFSET=$offset DBG DBG: OBJ=$obj\n";
	 		print "NO ANCHOR FOUND FOR $obj\n" unless $obj=~/\.\.sys/ or $obj=~/\.\.spt_values/;
    	   pr_output( $obj);
	 } else {
	    pr_output( $obj);
	 }
    if( $opt_h ) { pr_output( "&nbsp;[ ",$otyp_name," ]\n") if $otyp_name; }
    pr_output( "\n</TD>" ) if defined $opt_h;
    pr_output( "\n");
    pr_output( "</TR>\n") if defined $opt_h;

    # DO YOU WISH TO CONTINUE DOWN TREE?
    return if ! defined $opt_c and $verb =~ /by\s*$/ and $offset>=1;
    return if $verb =~ /select from/ and $offset>=1;
    return if $verb =~ /select by/ and $offset>=1;
    return if $verb =~ /\(recursive\)$/;
    #X# print __LINE__," OFFSET=$offset DBG DBG: OBJ=$obj\n";


    # Now Loop Through Dependencies
    if( $uses{$obj} ) {
    	 #X# print "DBG DBG: uses is $uses{$obj}\n";
	    foreach ( sort split /\|/,$uses{$obj} ) {
	       my($verb,$nextobj)=split /:/;
	       $verb .= " (recursive)" if $nextobj eq $obj and $verb !~ /by\s*$/;
	       # next if defined $opt_c
					# and $obj_type{$nextobj} ne "TR"
					# and $obj_type{$nextobj} ne "U";
			 #X# print __LINE__," OFFSET=$offset DBG DBG: OBJ=$obj\n";
	       show_output($nextobj,$offset+1,$verb);
	    }
	 }
	 #X# print __LINE__," OFFSET=$offset DBG DBG: OBJ=$obj\n";

}

__END__

=head1 NAME

depends_get.pl - parse dependencies for an object and print output

=head2 SEE

See depends_analyze.pl

=cut
