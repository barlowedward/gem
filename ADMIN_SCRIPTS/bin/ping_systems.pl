#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

BEGIN {
	$ENV{PATH}.=":/usr/sbin" 		if -x "/usr/sbin/ping";
	$ENV{PATH}.=":/usr/bin" 		if -x "/usr/bin/ping";
	$ENV{PATH}.=":/usr/local/bin" if -x "/usr/local/bin/ping";
}

#use subs qw(debug logdie infof info warning alert);
use strict;
use Repository;
use Net::Ping::External qw(ping);
use Sys::Hostname;
use CommonFunc;
use Getopt::Long;
use MlpAlarm;

# Copyright (c) 2001-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed
sub usage {
	print "Usage $0
   --LOCAL          do all of given type . if you are win32, do all win32. if unix do all unix.
   --TYPE           win32|unix
   --NOLOCK         dont use a lock file
   --BATCHID        batch to monitor as - default to PingSystems
   --DEBUG          diagnostic mode
   --IPADDRESS      monitor single ip address (uses --SYSTEM for name)
   --SYSTEM         monitor a single system
   --DOWNANDDIRTY   no logging or alarming
   --LOGALARM
   --TIMEOUT=time
   --ITERATIONS=num (loop num times slepeing SLEEPTIME seconds betwen loops)
   --SLEEPTIME=SECONDS
   --MAXTIME=minutes
   ";
	return 0;
}

$| = 1;
use vars qw($BATCHID $LOCAL $TYPE $NOLOCK $SYSTEM $DEBUG $IPADDRESS $DOWNANDDIRTY $MAXTIME $SLEEPTIME $TIMEOUT $ITERATIONS $LOGALARM);
die usage("Bad Parameter List\n") unless GetOptions(
		"LOCAL"				=> \$LOCAL,
		"TYPE=s"				=> \$TYPE,
		"TIMEOUT=s"			=> \$TIMEOUT,
		"NOLOCK"				=> \$NOLOCK,
		"DOWNANDDIRTY"		=> \$DOWNANDDIRTY,
		"BATCHID=s"			=> \$BATCHID,
		"DEBUG"				=> \$DEBUG,
		"IPADDRESS=s"		=> \$IPADDRESS,
		"SYSTEM=s" 			=> \$SYSTEM ,
		"MAXTIME=s" 		=> \$MAXTIME ,

		"LOGALARM"			=> \$LOGALARM,
		"SLEEPTIME=i"		=> \$SLEEPTIME,
		"ITERATIONS=i"		=> \$ITERATIONS		);

if( $DOWNANDDIRTY ) {
	$TIMEOUT=1;
}

$BATCHID="PingSystems" unless $BATCHID;
$MAXTIME  = time+$MAXTIME*60 if $MAXTIME;
if( ! $NOLOCK ) {
	print "NOLOCK IS NOT DEFINED!\n" if $DEBUG;
	if( Am_I_Up(-debug=>$DEBUG,-prefix=>$BATCHID) ) {
		my($fname,$secs,$thresh)=GetLockfile(-prefix=>$BATCHID);
		print "Cant Start ping_systems.pl - it is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nFile=$fname secs=$secs thresh=$thresh secs\n";
		exit(0);
	}
} else {
	print "NOLOCK IS DEFINED!\n" if $DEBUG;
}

print "ping_systems.pl: created at ". localtime(time). " on host ". hostname(). "\n";
my(@systemlist);
if( $SYSTEM ) {
	push  @systemlist,$SYSTEM;
} elsif( defined $TYPE ) {
	if( $TYPE eq "win32" ) {
		push @systemlist,get_password(-type=>"win32servers");
	} elsif( $TYPE eq "unix" ) {
		push @systemlist,get_password(-type=>"unix");
	} else {
		die "ERROR - TYPE must be win32 or unix - it is set to $TYPE\n";
	}
} elsif( defined $LOCAL ) {
	if( is_nt() ) {
		push @systemlist,get_password(-type=>"win32servers");
	} else {
		push @systemlist,get_password(-type=>"unix");
	}
} else {
	@systemlist=get_password(-type=>"win32servers");
	push @systemlist,get_password(-type=>"unix");
}

#my($i)=1;foreach (@systemlist) { print "DBG DBG: ",$i++,": ",$sys,"\n"; }

MlpBatchJobStart( -BATCH_ID=>$BATCHID )	if $BATCHID;

print "Running $0\nPings Started at " .scalar(localtime) ."\n";
my($ping_interval)=300;
my($lastpingtime)=time;

while(1) {
	my($numalive,$numdead)=(0,0);
	my(@redolist);
	my(%redotimes);
	my($alive);
	foreach my $sys (@systemlist) {
		last if $MAXTIME and time>$MAXTIME;

		chomp;
		if( $sys =~ /\s/ ) {
			warn "ERROR SYSTEM CONTAINS WHITE SPACE??  ($sys)\n";
			next;
		}
		printf "Pinging ... %20s ... ",$sys;
		I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;
		my($timeout)=5;
		$timeout=$TIMEOUT if $TIMEOUT;
		if( $SYSTEM and $IPADDRESS ) {
			#print "DBG DBG - $IPADDRESS \n";
			$alive = ping(host=>$IPADDRESS, timeout=>$timeout);
		} else {
			#print "DBG DBG - gethostbyname($sys) \n";
			my($ip)=gethostbyname($sys);
			if( $ip ) {
				$alive = ping(host=>$sys, timeout=>$timeout);
			} else {
				printf "%20s not found!\n",$sys;
				$numdead++;
				MlpHeartbeat(
					-monitor_program=>$BATCHID,
					-system=>$sys,
					-subsystem=>"ping",
					-debug=>$DEBUG,
					-message_text=>"$BATCHID Ping from ".hostname()." to $sys Failed: Hostname Not Found",
					-state=>'CRITICAL' ) if $BATCHID;
				next;
			}
		}

		if( $alive ) {
			print "alive!\n";
			$numalive++;
			MlpHeartbeat(
				-monitor_program=>$BATCHID,
				-system=>$sys,
				-debug=>$DEBUG,
				-subsystem=>"ping",
				-message_text=>"ping succeeded",
				-state=>'OK' )	if $BATCHID;

		} else {
			push @redolist, $sys;
			$redotimes{$sys}=time;
			print "*** redoing $sys ***\n";
	#		$numdead++;
	#		MlpHeartbeat(
	#			-monitor_program=>$BATCHID,
	#			-system=>$sys,
	#			-subsystem=>"ping",
	#			-debug=>$DEBUG,
	#			-message_text=>"Ping from ".hostname()." to $sys Failed",
	#			-state=>'CRITICAL' ) if $BATCHID;
		}
	}

	last if $MAXTIME and time>$MAXTIME;

	if( $#redolist >= 0 ) {
		print "Sleeping 10 seconds\n";
		sleep(10);
	}

	# we now reping the ones that failed because network storms or whatever
	# sometimes make it so that ping fails and we dont really want to page you
	# for false positives.
	foreach my $sys (@redolist) {
		print "Re Pinging ... $sys... ";
		I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;
		my($timeout)=5;
		$timeout=$TIMEOUT if $TIMEOUT;
		my $alive = ping(host=>$sys, timeout=>$timeout);
		if( $alive ) {
			print "alive!\n";
			$numalive++;
			MlpHeartbeat(
				-monitor_program=>$BATCHID,
				-system=>$sys,
				-debug=>$DEBUG,
				-subsystem=>"ping",
				-message_text=>"ping succeeded",
				-state=>'OK' )	if $BATCHID;

		} else {
			print "*** dead! ***\n";
			$numdead++;
			MlpHeartbeat(
				-monitor_program=>$BATCHID,
				-system=>$sys,
				-subsystem=>"ping",
				-debug=>$DEBUG,
				-message_text=>"$BATCHID Ping from ".hostname()." to $sys Failed at ".localtime($redotimes{$sys})." and ".localtime(),
				-state=>'CRITICAL' ) if $BATCHID;

		}
	}
	last if $MAXTIME and time>$MAXTIME;

	print $numalive," hosts are alive!\n";
	print $numdead," hosts are dead!\n";

	last if ! defined $SLEEPTIME or $SLEEPTIME<=0;
	last if $ITERATIONS-- <= 0;
	I_Am_Up(-prefix=>$BATCHID) unless $NOLOCK;

	print "sleeping $SLEEPTIME seconds...";
	print " $ITERATIONS iterations remaining " if defined $ITERATIONS;
	print "\n";

	if( time - $lastpingtime > $ping_interval ) {
		$lastpingtime=time;
		MlpBatchJobStep(-step=>"RUNNING") if defined $BATCHID;
	}

	sleep($SLEEPTIME);
	last if $MAXTIME and time>$MAXTIME;

}
I_Am_Down(-prefix=>$BATCHID);
MlpBatchJobEnd()	if $BATCHID;
