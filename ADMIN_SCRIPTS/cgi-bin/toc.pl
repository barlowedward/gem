use strict;
use lib qw(.);
use lib qw(lib);
# my($starttime)=time;

use MlpAlarm;
use DBIFunc;
use CGI qw( :standard );
use CGI::Carp qw(fatalsToBrowser);

#my(@timings);
#sub do_timings {
#	my($line,$lbl)=@_;
#	push @timings, $line.":".time().":".$lbl;
#}
#
#sub show_timings {
#	my($lasttime);
#	foreach ( @timings ) {
#		my($l,$t,$lbl)=split(/:/,$_,3);
#		$lasttime=$starttime unless $lasttime;
#		print "   ",$l," ($lbl) => ",($t-$lasttime),br,"\n";
#		$lasttime = $t;
#	}
#	print "   Total = ",(time-$starttime),br,"\n";
#}

my($PAGEID)=param('pageid');
print header, start_html( -style=>'styles/main.css', -style=>'styles/Customer.css'),"<FONT SIZE=-1>";

my($LOGON_USER)=GetLogonUser();
errorout("FATAL ERROR - LOGON_USER IS UNDEFINED") unless $LOGON_USER;

# do_timings(__LINE__,"Permissions") if $show_timings;
my($rc)=MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>1);
if( $rc ne "Y" ) {
	my($str)=GetLogonUser().": ACCESS DENIED".br."Please Contact Support".br;
	my(@rc)=MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0);
	foreach( @rc ) {
		$str.="Ok: $_".br;
	}
	$str.="You have NO permissions" if $#rc<0;
	errorout($str);
}

if( ! defined $PAGEID ) {
	my($show_credentials,$show_amreview,$show_inv,$show_alerts,$show_admin,$show_audit,$show_report,$show_help,$show_development,$show_datacenter,$show_feeds)=get_dashboard_perms();

	# ADMINISTRATORS CAN SEE ALTERNATE PAGES
	print br,"<TABLE WIDTH=100% BORDER=1>";
	print "<TR class='menuheader'><TD><b>PAGES NAVIGATION</TD></TR>\n";
	print "<TR><TD><A TARGET=_top HREF=index_credentials.htm>Credential Management</A></TD></TR>\n" 	if $show_credentials;
	print "<TR><TD><A TARGET=_top HREF=index_inventory.htm>Systems Inventory</A></TD></TR>\n" 			if $show_inv;
	print "<TR><TD><A TARGET=_top HREF=index_inventory_feeds.htm>Inventory Feeds</A></TD></TR>\n" 		if $show_feeds;
	print "<TR><TD><A TARGET=_top HREF=index_inventory_dcm.htm>Data Center Management</A></TD></TR>\n" if $show_datacenter;
	print "<TR><TD><A TARGET=_top HREF=index_alerts.htm>Alerts</A></TD></TR>\n" 								if $show_alerts;
	print "<TR><TD><A TARGET=_top HREF=index_audit.htm>Server Audits</A></TD></TR>\n" 						if $show_audit;
	print "<TR><TD><A TARGET=_top HREF=index_amreview.htm>AM Review</A></TD></TR>\n" 						if $show_amreview;
	print "<TR><TD><A TARGET=_top HREF=index_reports.htm>Server Reports</A></TD></TR>\n" 					if $show_report;
	print "<TR><TD><A TARGET=_top HREF=index_admin.htm>Dashboard Administration</A></TD></TR>\n" 		if $show_development;
	print "<TR><TD><A TARGET=_top HREF=index_help.htm>Help</A></TD></TR>\n" 									if $show_help;
	print "</TABLE>";
	print end_form,end_html;
	exit(0);
}

my(%dashboard_permissions);
if( param('permission') ) {
		$dashboard_permissions{param('permission')}=1;
} else {
	foreach( MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0) ) {
		$dashboard_permissions{$_}=1;
	}
}

my($perm_url)="permission=".param('permission')."&" if param('permission');
$perm_url=~s/ /%20/g;

# do_timings(__LINE__,"Workflow") if $show_timings;
generate_workflow() if $PAGEID==0;

# do_timings(__LINE__,"Parsing Data") if $show_timings;
my($current_header,$current_header_printed);
my(%PAGETITLES);
my($page_print_string)='';
while(<DATA>) {
	chomp;
	next if /^\s*$/ or /^#/;
	if( /^PAGE(\d+)=/ ) {
		my($pagenum)=$1;
		s/^PAGE\d+=//;
		$PAGETITLES{$pagenum} = $_;
	} elsif( /^=/ ) { 		# header
		s/^=//;
		$current_header=$_;
		$current_header_printed=0;
	} else {				# data row
		my($page,$perm,$url,$label)=split(/;/,$_,4);
		next if $page ne $PAGEID;

		# print "<FONT COLOR=RED>perm = $perm label=$label</FONT>\n".br;
		if( $dashboard_permissions{'DASHBOARD ADMINISTRATOR'}
		or    ( $perm ne "" and $dashboard_permissions{'IT MANAGER'} )

		or    ( $perm=~/A/ and $dashboard_permissions{'APPROVER'} )
		or    ( $perm=~/X/ and $dashboard_permissions{'AUDITOR'} )
		or    ( $perm=~/S/ and $dashboard_permissions{'DASHBOARD POWERUSER'} )
		or    ( $perm=~/P/ and $dashboard_permissions{'DASHBOARD POWERUSER'} )

		or    ( $perm=~/W/ and $dashboard_permissions{'ALERTS PG'} )
		or    ( $perm=~/D/ and $dashboard_permissions{'ALERTS PG'} )
		or    ( $perm=~/D/ and $dashboard_permissions{'DASHBOARD PG'} )

		or    ( $perm=~/Z/ and $dashboard_permissions{'AUDIT PG'} )
		or    ( $perm=~/U/ and $dashboard_permissions{'INVENTORY PG'} )
		or    ( $perm=~/D/ and $dashboard_permissions{'INVENTORY PG'} )
		or    ( $perm=~/\*/  )
		or    ( $perm=~/Z/ and $dashboard_permissions{'DASHBOARD ADMINISTRATOR'} )
		) {
			if( $current_header_printed == 0 ) {
				$current_header_printed=1;
				$page_print_string.= "<BR><B>$current_header</B><BR>\n";
			}

			if( $url eq "printenv.pl" ) {
				$page_print_string.= "<A TARGET=basefrm HREF=$url><font size=-1>$label</font></A><BR>\n";
			} elsif( $url eq "AMREVIEWREPORTS" ) {
				my($report_query)="select report_name,report_file from GEM_amreview where req_review_frequency is not null order by report_name ";
				$page_print_string.="<p>";

				my($dirhr)="html_output";				
				opendir(DIR,$dirhr) || die("Can't open directory ".$dirhr." for reading\n");
				my(@dirlist) = grep(!/^\./,readdir(DIR));
				my(%dirhash);
				foreach ( @dirlist ) { $dirhash{$_} = 1; }
				closedir(DIR);

				# names - framesettop = top_XXX, report=XXX, approval=app_XXX
				foreach ( _querywithresults($report_query,1) ) {
					my($rpt,$file) = dbi_decode_row($_);
					$page_print_string.= "<A TARGET=basefrm HREF=$dirhr/top_".$file."><font size=-1>$rpt</font></A><BR>\n";
					if( ! $dirhash{"top_".$file} ) {
						# print "PAINTING top_$file\n",br;
						open(WR,">$dirhr/top_".$file) or die "Cant Write $dirhr/top_$file\n";
						print WR "<HTML>
<FRAMESET ROWS=\"43,*\" BORDER=NO SCROLLING=NO>
<FRAME SRC=app_${file} NAME=approval SCROLLING=NO>
	<FRAME SRC=$file NAME=approvalbot>
</FRAMESET>
</HTML>
";
						close(WR);
					}
					if( ! $dirhash{"app_".$file} ) {
						# print "PAINTING app_$file\n",br;
						open(WRX,">$dirhr/app_".$file) or die "Cant Write $dirhr/app_$file\n";
						print WRX '<input type="checkbox" name="app'.$file.'" value="Y" />&nbsp;<font color=BLUE><b>APPROVE FILE '.$file.'</b></font><br>';
						close(WRX);
					}
				}
				

			} elsif( $url =~ /^directory/ ) {
				$url=~s/^directory//;
				$page_print_string.= "<A TARGET=basefrm HREF=".$url."/index.htm>$label</A><BR>\n";
			} elsif( $current_header eq "DIAGNOSTICS" ) {
				$page_print_string.= "<A HREF=$url><font size=-1>$label</font></A><BR>\n";
			} else {
				$page_print_string.= "&nbsp;&nbsp;&nbsp;- " if $label =~ /^   -/;
				$label=~s/^   -//;
				$url=~s/reportwriter.pl\?/reportwriter.pl\?$perm_url/ if $perm_url;
				$page_print_string.= "<A TARGET=basefrm HREF=$url>$label</A><BR>\n";
			}
		}
	}
}

# do_timings(__LINE__,"Footer") if $show_timings;
if( $PAGEID!=0 ) {
	print "<TABLE WIDTH=100% BORDER=1>";
	print "<TR class='menuheader'><TD><b>$PAGETITLES{$PAGEID}</TD></TR>\n";
	print "</TABLE>";
}

print $page_print_string;

print end_form,end_html;
#do_timings(__LINE__,"Done") if $show_timings;
#show_timings() if $show_timings;
# print "TIMING=",time-$starttime,br;
exit(0);

#sub errorbox
#{
#	my($msg)=join('',@_);
#	return "<TABLE BORDER=1 WIDTH=100% BGCOLOR=WHITE><TR class='menuheader'><TD ALIGN=LEFT><FONT face=sans-serif size=+1 color=red>@_</FONT></TD>\n</TR></TABLE>\n";
#}

sub errorout
{
	my($msg)=@_;
	print "<FONT COLOR=RED>".b($msg)."</FONT>";
	print end_form,end_html;
	exit(0);
}

sub generate_workflow {

	return unless  param('permission') eq "APPROVER" or $dashboard_permissions{'APPROVER'};

	my($cancertify)="CANCERTIFY=Y\&";

	my($l) = GetLogonUser();
	my($lname)=$l;
	$lname = "IT MANAGER"  if $dashboard_permissions{'IT MANAGER'};

	my(@str);
	my($report_query)="select 	distinct certification_target from INV_required_certification where certification_type='DATABASE'  order by certification_target ";
	foreach ( _querywithresults($report_query,1) ) {
		my($sysname) = dbi_decode_row($_);
		push @str, $sysname;

	}

	my(@str2);
	my($report_query)="select distinct certification_target from INV_required_certification where certification_type='GROUP' order by certification_target ";
	foreach ( _querywithresults($report_query,1) ) {
		my(@x) = dbi_decode_row($_);
		push @str2, $x[0];
	};

	if( $#str>=0 or $#str2>=0) {
		print "<TABLE BORDER=1 WIDTH=100% BGCOLOR=#f0e9d9>
		<TR class='menuheader'><TH COLSPAN=2>SAS 70 Required Certifications</TH></TR>
		<TR><TD ALIGN=LEFT VALIGN=TOP>";
		my($lastsvr);
		foreach ( sort @str ) {
			my($a,$b,$c)=split(/;/,$_,3);
			if( $lastsvr ne $a ) {
				print '<A TARGET=basefrm HREF=reportwriter.pl?permission=APPROVER&CANCERTIFY=Y&REPORTNAME=INV_DATABASE_ACCESS&filter_system='.$a.">Database $a Certification</A>",br;
				$lastsvr=$a;
			}
		}
		print br if $#str2>=0;
		foreach ( sort @str2 ) {
			print "<A TARGET=basefrm HREF=reportwriter.pl?".$perm_url.$cancertify."REPORTNAME=INV_DEPARTMENT_ACCESS&filter_ldap_group=".$_.">Group ".$_." Certification</A>\n",br;
		}

		print br,"</TD></TR></TABLE>";
	}
}

sub get_dashboard_perms {
	my($show_credentials,$show_systems,$show_alerts,$show_administration,$show_audit,$show_report,$show_help,$show_development,$show_datacenter,$show_feeds)=(0,0,0,0,0,0,0,0,0,0);
	foreach( MlpGetPermission(-samid=>GetLogonUser(), -canaccess=>0) ) {
		next if /^Dflt/;
		#print $_,"<br>" if $DIAG;
		# $perms{$_}=1;
		$show_report=1 if /REPORTS PG/;

		if( /DASHBOARD ADMINISTRATOR/ or /IT MANAGER/ ) {
			($show_credentials,$show_systems,$show_alerts,$show_administration,$show_audit,$show_help,$show_development)=(1,1,1,1,1,1,1);
		}
		if( /APPROVER/ or /AUDITOR/ or /DASHBOARD POWERUSER/ or /DASHBOARD POWERUSER/ or /CREDENTIALS PG/ ) {
			$show_credentials++;
		}
		$show_systems++ 			if /INVENTORY PG/ or /AUDITOR/;
		$show_administration++ 	if /DASHBOARDADMIN PG/;
		$show_audit++ 				if /AUDIT PG/ or /AUDITOR/;
		$show_feeds++ 				if /FEEDS PG/;
		$show_datacenter++ 		if /DATACENTER PG/;
		$show_alerts++ 			if /ALERTS PG/ or /DASHBOARD POWERUSER/;
	}
	return($show_credentials,$show_systems,$show_alerts,$show_administration,$show_audit,$show_report,$show_help,$show_development,$show_datacenter,$show_feeds);
}

# ALGORITHM FOR SHOWING AN ITEM
#
#		if( $dashboard_permissions{'DASHBOARD ADMINISTRATOR'}
#		or    ( $perm=~/A/ and $dashboard_permissions{'APPROVER'} )
#		or    ( $perm=~/X/ and $dashboard_permissions{'AUDITOR'} )
#		or    ( $perm=~/S/ and $dashboard_permissions{'DASHBOARD POWERUSER'} )
#		or    ( $perm=~/P/ and $dashboard_permissions{'DASHBOARD POWERUSER'} )

#		or    ( $perm=~/U/ and $dashboard_permissions{'INVENTORY PG'} )
#		or    ( $perm=~/W/ and $dashboard_permissions{'ALERTS PG'} )
#		or    ( $perm=~/D/ and $dashboard_permissions{'ALERTS PG'} )
#		or    ( $perm=~/M/ and $dashboard_permissions{'IT MANAGER'} )
# ) {
#
#if( $dashboard_permissions{'DASHBOARD ADMINISTRATOR'}
#		or    ( $perm ne "" and $dashboard_permissions{'DEMO ALL FEATURES'} )
#
#		or    ( $perm=~/A/ and $dashboard_permissions{'APPROVER'} )
#		or    ( $perm=~/X/ and $dashboard_permissions{'AUDITOR'} )
#		or    ( $perm=~/S/ and $dashboard_permissions{'DASHBOARD POWERUSER'} )
#		or    ( $perm=~/P/ and $dashboard_permissions{'DASHBOARD POWERUSER'} )
#
#		or    ( $perm=~/W/ and $dashboard_permissions{'ALERTS PG'} )
#		or    ( $perm=~/D/ and $dashboard_permissions{'ALERTS PG'} )
#
#		or    ( $perm=~/U/ and $dashboard_permissions{'INVENTORY PG'} )
#		or    ( $perm=~/I/ and $dashboard_permissions{'ADMIN INVENTORY PG'} )
#
#		or    ( $perm=~/M/ and $dashboard_permissions{'IT MANAGER'} )
#		or    ( $perm=~/Z/ and $dashboard_permissions{'DASHBOARD ADMINISTRATOR'} )
#
#
# The PERMISSIONS SHOULD MATCH THE LOOKUP FROM CONSTANT.DAT
#

#=DEVELOPMENT
#MDUW;reportwriter.pl?REPORTNAME=INV_OUTAGE_TRACKER;*Outage Tracker
#MDUW;reportwriter.pl?REPORTNAME=INV_NEW_SYSTEM_TRACKER;*System Tracker
#MDUW;reportwriter.pl?REPORTNAME=INV_ASTERDATA_TEST1;*ASTERDATA TEST1
#MDUW;reportwriter.pl?REPORTNAME=INV_ASTERDATA_TEST2;*ASTERDATA TEST2
#D;reportwriter.pl?REPORTNAME=INV_DBA_TASKS;DBA Task List
__DATA__

PAGE0=Credential Management
PAGE1=Systems Inventory
PAGE2=Alerts, Heartbeats, Events
PAGE3=Dashboard Administration
PAGE4=Audit Reports
PAGE5=Feed Reports
PAGE6=Dcm Reports
PAGE7=Development
PAGE8=Server Reports
PAGE9=AM Review
PAGE10=Help

=CURRENT BACKUP STATE
9;*;rwnew.pl?REPORTNAME=GEM_BACKUP_STATE;Backup State
9;*;rwnew.pl?REPORTNAME=GEM_BACKUP_TRAN_STATE;TranDump State
9;*;rwnew.pl?REPORTNAME=GEM_RESTORE_STATE;Restore State
9;*;rwnew.pl?REPORTNAME=GEM_RESTORE_TRAN_STATE;TranRestore State

=AM REVIEW PROCESS
9;*;rwnew.pl?REPORTNAME=GEM_AMREVIEW_MANAGE;Manage AM Review Reports
9;*;AMREVIEWREPORTS;Manage AM Review Reports

=DATABASE REPORTS
8;MWDP*;directory console_sqlsvr;Sql Server Reports
7;MWDP;directory console_sybase;Sybase Reports
7;MWDP;directory console_oracle;Oracle Reports
7;MWDP;directory console_mysql;Mysql Reports

=HOST REPORTS
8;MWDP*;directory console_win;Windows Reports
7;MWDP;directory console_unix;Unix Reports

=BATCH JOB MONITORING
7;MWDP;directory cronlogs;Cronlogs
7;MWDP;directory html_output;html_output
7;MWDP;directory depends;depends
7;MWDP;directory backup_logs;backup_logs
7;MWDP;directory gem_batchjob_logs;gem_batchjob_logs

=ALERTS AND MONITORING
2;MWDP;reportwriter.pl?REPORTNAME=INV_HEARTBEAT_RPT;Heartbeat Monitor
#2;MWDP;reportwriter.pl?REPORTNAME=INV_HEARTBEAT_RPT2;Heartbeat Monitor2
2;MWDP;reportwriter.pl?REPORTNAME=INV_EVENT_RPT;Event Monitor
2;MWDP;reportwriter.pl?REPORTNAME=INV_GEM_BATCH_JOB_RPT;Batch Job Monitor
2;MWDP;reportwriter.pl?REPORTNAME=INV_BACKUP_RPT;Backup Job Monitor
2;MWDP;reportwriter.pl?REPORTNAME=INV_CONSOLE_RPT;DR Configuration Monitor
2;MWDP;reportwriter.pl?REPORTNAME=INV_CONSOLE_HELP;-- DR Configuration Help
2;MWDP;reportwriter.pl?REPORTNAME=INV_OTHER_HB_RPT;Other Heartbeats Monitor

#MWDP;mimi.pl?radio_screen=Heartbeats&ProdChkBox=Production%20Only;Production Heartbeat Monitor
#MWDP;mimi.pl?radio_screen=Events&ProdChkBox=Production%20Only;Production Event Logs
#D;mimi.pl?radio_screen=Agents&filter_severity=All;Monitoring Jobs
#D;mimi.pl?radio_screen=Batch%20Jobs&filter_severity=All;Batch Jobs
#D;mimi.pl?radio_screen=Backups&filter_severity=All;Backup Jobs
# 0;MWDP;reportwriter.pl?REPORTNAME=INV_CHANGE_TRACKER;Configuration Change Tracker

=CHANGE TRACKING
4;MWDP;reportwriter.pl?REPORTNAME=INV_CHANGE_TRACKER;Configuration Change Tracker

=PERFORMANCE AUDITS
4;MWDP;reportwriter.pl?REPORTNAME=INV_LONGRUNNING;Long Running Operations

=SQL SERVER CONFIGURATION AUDIT
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDRES&AUDTYP=sqlsvr;Sql Server Audit Results
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDPOL&AUDTYP=sqlsvr;Sql Server Audit Policies
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDADDDELPOL&AUDTYP=sqlsvr;Add/Delete A Sql Server Policy
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDMAP&AUDTYP=sqlsvr;Sql Server Policy Map

=SYBASE CONFIGURATION AUDIT
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDRES&AUDTYP=sybase;Sybase Audit Results
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDPOL&AUDTYP=sybase;Sybase Audit Policies
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDADDDELPOL&AUDTYP=sybase;Add/Delete A Sybase Policy
4;MWDP;rwnew.pl?REPORTNAME=INV_AUDMAP&AUDTYP=sybase;Sybase Server Policy Map

=SYSTEM ACCESS REPORTS
0;ADUW;reportwriter.pl?REPORTNAME=INV_LDAP_LOOKUP;Ldap User/Group Information
0;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_USER_ACCESS;Access Report For A User
0;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_DEPARTMENT_ACCESS_FULL;Access Report For A Department
0;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_SYSTEM_ACCESS_RPT;Access Report For A Computer
0;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_DATABASE_ACCESS_RPT;Access Report For A Database System

8;ADUW;reportwriter.pl?REPORTNAME=INV_LDAP_LOOKUP;Ldap User/Group Information
8;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_USER_ACCESS;Access Report For A User
8;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_DEPARTMENT_ACCESS_FULL;Access Report For A Department
8;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_SYSTEM_ACCESS_RPT;Access Report For A Computer
8;MDUWAXSP;reportwriter.pl?REPORTNAME=INV_DATABASE_ACCESS_RPT;Access Report For A Database System

=SAS70 CERTIFICATIONS
0;AXS;reportwriter.pl?REPORTNAME=INV_DATABASE_SUMMARY;Database Certification Summary
0;AXS;reportwriter.pl?REPORTNAME=INV_DATABASE_SUMMARY&filter_container=UNAPPROVED&NOARGFETCH=1;* UnApproved Credentials
0;AXS;reportwriter.pl?REPORTNAME=INV_DATABASE_SUMMARY&filter_container=UNMAPPED&NOARGFETCH=1;* Unmapped Credentials
0;AXS;reportwriter.pl?REPORTNAME=INV_DATABASE_SUMMARY&filter_container=REMOVE&NOARGFETCH=1;* To Investigate / Remove
0;AXS;reportwriter.pl?REPORTNAME=INV_DATABASE_SUMMARY&filter_container=SYSADMIN&NOARGFETCH=1;* System Admin
0;AXS;reportwriter.pl?REPORTNAME=INV_DATABASE_SUMMARY&filter_container=SERVICEACCOUNT&NOARGFETCH=1;* Service Account

=SAS70 CREDENTIAL MAPPING
0;AXS;reportwriter.pl?REPORTNAME=INV_CREDENTIAL_MAPPING&filter_container=Mapped%20Accounts&NOARGFETCH=1;Define Credentials
#AXS;reportwriter.pl?REPORTNAME=INV_CREDENTIAL_MAPPING&filter_container=Unmapped%20Accounts&NOARGFETCH=1;Unmapped Credentials
#AXS;reportwriter.pl?REPORTNAME=INV_CREDENTIAL_MAPPING&filter_container=Error%20Accounts&NOARGFETCH=1;To Investigate / Remove
0;AXS;reportwriter.pl?REPORTNAME=INV_GLOBAL_SHARED_ACCOUNT;Map Credentials To Users

=SAS70 CREDENTIAL CERTIFICATIONS
#XS;reportwriter.pl?REPORTNAME=INV_PRINCIPAL_MAP;Credential Assignment
#XS;reportwriter.pl?REPORTNAME=INV_SEE_ALL;   - All Credentials
#XS;reportwriter.pl?REPORTNAME=INV_SEE_BATCHACCOUNTS;   - Service or SysAdmin
#XS;reportwriter.pl?REPORTNAME=INV_CREDENTIAL_MAPPING;   - Needs To Be Mapped
#XS;reportwriter.pl?REPORTNAME=INV_MAPPED_CREDENTIALS;   - Mapped Credentials
#XS;reportwriter.pl?REPORTNAME=INV_SEE_UNUSEABLE;   - Unuseable/Locked/Disabled
#XS;reportwriter.pl?REPORTNAME=INV_LOCAL_SHARED_ACCOUNT;   - System Shared Account Mapping
#XS;reportwriter.pl?REPORTNAME=INV_LDAP_LOOKUP;Ldap Lookup

=SAS70 GROUP CERTIFICATIONS
0;AXS;reportwriter.pl?REPORTNAME=INV_GROUP_SUMMARY;Group Certification Summary
0;AXS;reportwriter.pl?REPORTNAME=INV_GROUP_CERT_HIST;Group Certification History

#=REPORT SUBSCRIPTIONS
#D;reportwriter.pl?REPORTNAME=Report%20Setup;*Setup A Custom Report
#D;reportwriter.pl?REPORTNAME=Report%20Setup;*Subscribe To A Report

=DASHBOARD ADMINISTRATION
3;DIM;reportwriter.pl?REPORTNAME=INV_DEFINE_SYSUSERS2;Dashboard User Permissions
#3;X;reportwriter.pl?REPORTNAME=INV_DEFINE_SYSUSERS;Dashboard User Permissions
3;DIM;reportwriter.pl?REPORTNAME=INV_DEFINE_SYSGROUP;Dashboard Group Permissions

=SAS70 CERTIFICATIONS
3;DIM;reportwriter.pl?REPORTNAME=INV_DEFINE_AUD_CERT;Define Required Certifications
3;DIM;reportwriter.pl?REPORTNAME=INV_CERT_JOB_STATE;Certifications Batch Job State
3;DIM;reportwriter.pl?REPORTNAME=INV_DATABASE_SUMMARY;Database Certification Summary
3;DIM;reportwriter.pl?REPORTNAME=INV_CRED_CERT_HIST;Database Certification History

=SAS70 CERTIFICATION DEFINITIONS
4;DIM;reportwriter.pl?REPORTNAME=INV_DEFINE_AUD_CERT;Define Required Certifications
4;DIM;reportwriter.pl?REPORTNAME=INV_CERT_JOB_STATE;Certifications Batch Job State

=Batch Job State
3;DIM;reportwriter.pl?REPORTNAME=INV_CERT_JOB_STATE;Certifications Batch Job State
3;DIM;reportwriter.pl?REPORTNAME=INV_GEM_JOB_STATE;Gem/Monitoring Batch Job State
3;DIM;reportwriter.pl?REPORTNAME=INV_GEM_CHECKSERVER_STATE;Checkserver Batch Job State
3;DIM;reportwriter.pl?REPORTNAME=INV_BACKUP_JOB_STATE;Backup Batch Job State
3;DIM;reportwriter.pl?REPORTNAME=INV_FAILED_JOB_STATE;Failed Job State

=DASHBOARD EXAMPLE CODE
10;MZ;reportwriter.pl?REPORTNAME=INV_EXAMPLE_1;Dashboard Example Code 1
10;MZ;reportwriter.pl?REPORTNAME=INV_EXAMPLE_2;Dashboard Example Code 2
10;MZ;reportwriter.pl?REPORTNAME=INV_EXAMPLE_3;Dashboard Example Code 3

=HEARTBEAT/EVENT DATA MGMT
2;MWD;reportwriter.pl?REPORTNAME=GEM_Blackout;Blacked Out Servers
2;MWD;reportwriter.pl?REPORTNAME=GEM_Ignorelist2;Ignore List
2;MWD;reportwriter.pl?REPORTNAME=INV_DEL_BY_MONITOR =Delete%20By%20Monitor;Delete Data By Monitor
2;MWD;reportwriter.pl?REPORTNAME=INV_DEL_BY_MONTYPE Delete%20By%20Monitor_Type;Delete Data By Monitor/Type
2;MWD;reportwriter.pl?REPORTNAME=INV_DEL_BY_ROW   Delete%20By%20Row;Delete Data By Monitor/System/Type
#D;mimi.pl?NONAVIGATION=1&admin=Alarm%20History;View Page/Email Alert History

=OPERATOR AND ALERT SETUP
2;MWD;reportwriter.pl?REPORTNAME=INV_OPERATOR_SETUP;Setup Operator
2;MWD;reportwriter.pl?REPORTNAME=INV_OPERATOR_SCHEDULE;Setup Operator Schedule
2;MWD;reportwriter.pl?REPORTNAME=INV_ALARM_ROUTING_RPT;Alarm Routing To Operators
2;MWD;reportwriter.pl?REPORTNAME=INV_FILTER_REPEATS;Filter Repeated Messages
2;MWDP;reportwriter.pl?REPORTNAME=INV_ALARM_HISTORY;History of Alarm Mails
2;MWDP;reportwriter.pl?REPORTNAME=INV_ALERTABLE_E_RPT;Event Processing
2;MWDP;reportwriter.pl?REPORTNAME=INV_ALERTABLE_H_RPT;Heartbeat Processing

# =DIAGNOSTICS
#XSP;printenv.pl;printenv.pl

#0;X;toc.pl?permission=DASHBOARD%20ADMINISTRATOR;Become DASHBOARD ADMINISTRATOR
#0;X;toc.pl?permission=APPROVER;Become APPROVER
#0;X;toc.pl?permission=AUDITOR;Become AUDITOR
#0;X;toc.pl?permission=DATA%20SCRUBBER;Become DASHBOARD POWERUSER
#0;X;toc.pl?permission=SYSTEM%20POWERUSER;Become DASHBOARD POWERUSER

#0;X;toc.pl?permission=SHOW%20AUDIT%20PG;Show AUDIT PG
#0;X;toc.pl?permission=UNIX%20SYSADMIN;Become SHOW INVENTORY PG
#0;X;toc.pl?permission=WINDOWS%20SYSADMIN;Become SHOW ALERTS PG
#;toc.pl?permission=WINDOWS%20SYSADMIN;Become ADMIN INVENTORY PG
#0;X;toc.pl?permission=DBA%20ADMIN;Become ADMIN ALERTS PG
#0;X;toc.pl?permission=IT%20MANAGER;Become IT MANAGER
#0;X;toc.pl?permission=DEMO%20ALL%20FEATURES;Become DEMO ALL FEATURE

=SYSTEMS BILLING INFORMATION
1;MUI;reportwriter.pl?REPORTNAME=INV_CORES_REPORT;Cores Report
1;MUI;reportwriter.pl?REPORTNAME=INV_NOCONTACT_REPORT;Systems Missing Data
1;MUI;reportwriter.pl?REPORTNAME=INV_CONTACT_MANAGER;Manage Contacts
1;MUI;reportwriter.pl?REPORTNAME=INV_CORES_DATES;Manage Dates

=SYSTEMS INVENTORY REPORTS
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&feed_type=sybase;Sybase Report
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&feed_type=oracle;Oracle Report
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&feed_type=mysql;Mysql Report
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&feed_type=sqlsvr;SqlSvr Report

7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&feed_type=win32servers;Win32 Report
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&feed_type=unix;Unix Report
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&canned_report=coresbyloc&runcanned=1; - Cpu/Cores By Location
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&canned_report=coresbyrelease&runcanned=1; - Cpu/Cores By Release
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&canned_report=coresbyos&runcanned=1; - Cpu/Cores By OS Version
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&canned_report=coresbycputype&runcanned=1; - Cpu/Cores By Cpu Type
7;MUI;reportwriter.pl?REPORTNAME=INV_SYSTEM_REPORT&feed_type=dnsalias;Cluster Alias Report

=SYSTEMS INVENTORY METADATA
1;MUI;reportwriter.pl?REPORTNAME=INV_COMPUTER_METADATA;Computer Metadata
1;MUI;reportwriter.pl?REPORTNAME=INV_DATABASE_METADATA;Database Metadata
5;MUI;reportwriter.pl?REPORTNAME=INV_COMPUTER_METADATA;Computer Metadata
5;MUI;reportwriter.pl?REPORTNAME=INV_DATABASE_METADATA;Database Metadata
6;MUI;reportwriter.pl?REPORTNAME=INV_COMPUTER_METADATA;Computer Metadata
6;MUI;reportwriter.pl?REPORTNAME=INV_DATABASE_METADATA;Database Metadata

=DCM WASP COMPARE
6;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_REPORT;Reconcilliation Report
#1;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_MAP;Wasp To Dcm Map
#1;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_UNMAPPABLE_WASP;Unmappable Wasp
#1;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_UNMAPPABLE_DCM;Unmappable DCM Data

6;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_NODCMTAG;Wasp Assets wo/Matching DCM AssetTag
6;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_NOWASPTAG;Dcm Assets wo/Matching WASP AssetTag
6;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_WASPLOCINC;Wasp Location Incompete
6;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_DCMLOCINC;Dcm Location Incomplete
6;MUI;reportwriter.pl?REPORTNAME=DCM_WASP_LOCMISMATCH;Wasp/Dcm Location Missmatch

=BUSINESS SYSTEMS
1;MUI;reportwriter.pl?REPORTNAME=INV_MANAGE_BUSSYSTEM;Create or Delete
1;MUI;reportwriter.pl?REPORTNAME=INV_MAP_BUSSYSTEM;Map Systems

=FEED REPORTS
1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=DCM;DCM Feed Report
6;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=DCM;DCM Feed Report
1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=WASP;WASP Feed Report
6;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=WASP;WASP Feed Report
1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=unixinv;Unix Feed Report
1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=ldap_computer;Active Directory Feed Report
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=sybase_discovery;-- Sybase Discovery Feed
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=sqlsvr_discovery;-- SqlSvr Discovery Feed
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=oracle_discovery;-- Oracle Discovery Feed
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=mysql_discovery;-- Mysql Discovery Feed

1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=sqlsvr_spreadsheet;SqlServer Master Spreadsheet
1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=mysql_spreadsheet;Mysql Master Spreadsheet
1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=sybase_spreadsheet;Sybase Master Spreadsheet
1;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=oracle_spreadsheet;Oracle Master Spreadsheet

=BASE TYPE REPORTS
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=unix;-- Test SqlServer Master
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=win32server;-- Test SqlServer Master
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=sqlsvr;-- Test SqlServer Master
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=mysql;-- Test Mysql Master
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=sybase;-- Test Sybase Master
7;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_REPORT&feed_type=oracle;-- Test Oracle Master

=FEEDS
5;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_PRIORITY;Feed Priority
5;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_FEED_DATA;Feed Attributes
5;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_MONITORING;DBA & Other Monitoring
5;MUI;reportwriter.pl?REPORTNAME=INV_ADMIN_RUN_PROMOTION;Run Feed Promotion

=ADMIN CREDENTIALS
3;DIM;reportwriter.pl?REPORTNAME=INV_ADMIN_DATASVR_CREDENTIALS;Database Credentials

=GEM REPORTS
7;DIM;reportwriter.pl?REPORTNAME=GEM_CONFIG_TASK_REPORT;Gem Configuration Tas
