#!/apps/perl/linux/perl-5.8.2/bin/perl

my (@A) = (1,2,3,4);
print "\n.",join('.', @A), ".\n";
my (@B) = (5,6,7,8);
print "\n.",join('.', @B), ".\n";
foreach (@A,@B) {
print "\n...$_...";
}
print "\n\n";
exit;

print "\n\n$ARGV[0],$ARGV[1],$ARGV[2]\n\n";

use vars qw( $X);

print "\n.........NO X ............\n" unless $X;

my ($line)="RiskReport;sim_pnl_scenario;where sim_id in (SELECT sim_id FROM summary_report WHERE position_date >= '10/11/2010')\n";
chomp $line;

my ($database, $table, $query) = split(";", $line, 3);
print "\n1.$database\n2.$table\n3.$query\n\n";

$line="RiskReport;trader_secdesc_limit\n";
chomp $line;
($database, $table, $query) = split(";", $line, 3);
print "\n1.$database\n2.$table\n3.$query\n\n";

sub xyz {
  print "\n\n",join("\n",@_),"\n\n";
  my (%args) = @_;
  foreach my $key (keys %args) {
    print "$key...",$args{$key},"\n";
  }
  print "\n\n";
}

&xyz (a=>'AAA',b=>1234,c=>'ZZZZ');
exit;

