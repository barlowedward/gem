-- report_key=db_file_io
-- report_type=PERFORMANCE
-- run_frequency=Hourly
-- do_diff=Y
-- keep_hours=Y
-- keep_6=N
-- server_type=sqlsvr
-- exclude_server_csv=
-- run_on_server_csv=
-- server_min_version=9
-- server_max_version=
-- run_query_each_db=N
-- run_query_in_sys_db=N
-- report_header=This Report Lists Unused Indexes
-- report_footer=
-- enabled=Y
-- performancetype=int
-- performancekey=db_file_io

select @@SERVERNAME,db_name(database_id),	file_id,'',
	num_of_bytes_read,
	num_of_bytes_written,
	io_stall_read_ms
	from sys.dm_io_virtual_file_stats( null ,null )
	where db_name(database_id) not in ('master','model','msdb','distribution')
