create trigger INV_credential_notes_ins_trg on INV_credential_notes for insert
as
begin
	insert INV_credential_notes_audit
	select 'INS',suser_name(),getdate(),* from inserted
	-- where suser_name() != 'gemalarms'

	insert INV_credential_notes_audit
	select 'DEL',suser_name(),getdate(),* from deleted
	-- 	where suser_name() != 'gemalarms'
end
go
