#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 2006-2008 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# CREATE INSERT SCRIPT FROM DATA IN A SERVER

use strict;
use Getopt::Long;
use File::Basename;
use DBIFunc;
sub usage
{
	my($str)=@_;
   return $str."USAGE: file_loader.pl \<Args\>
   Generic Multipurpose File Loader - For Details run perldoc file_loader.pl
   CONNECTION: -SERVER=srv -USER-usr -PASSWORD=pass -DATABASE=db
   --USEBCP --SHOWSQL
   --LOGFILE=file --REJECTFILE=rfile
   [--PROC=proc|--TABLE=table]
   --FILE=file -DEFFILE=def --SKIPTAIL=rows --SKIPHEAD=rows [-NOEXEC] [-DEBUG]\n";
}

use vars qw( $FIXEDLENGTH $LABEL $SERVER $USER $SHOWSQL $DODELETE $USEBCP $SKIPTAIL $SKIPHEAD $NODETAILS
				 $PASSWORD $LOGFILE $REJECTFILE $DB $TABLE $PROC $FILE $DEFFILE $NOEXEC $DEBUG $DEBUG2);

die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"USER=s"			=> \$USER ,
		"DODELETE=s"	=> \$DODELETE ,
		"LOGFILE=s"		=> \$LOGFILE ,
		"SKIPHEAD=s"	=> \$SKIPHEAD ,
		"SKIPTAIL=s"	=> \$SKIPTAIL ,
		"FIXEDLENGTH=n"=> \$FIXEDLENGTH ,
		"REJECTFILE=s"	=> \$REJECTFILE ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"DATABASE=s" 	=> \$DB ,
		"PROC=s" 		=> \$PROC ,
		"LABEL=s" 		=> \$LABEL ,
		"TABLE=s"	 	=> \$TABLE ,
		"FILE=s"			=> \$FILE,
		"DEFFILE=s" 	=> \$DEFFILE ,
		"NODETAILS" 	=> \$NODETAILS ,
		"SHOWSQL"	 	=> \$SHOWSQL ,
		"USEBCP" 		=> \$USEBCP ,
		"NOEXEC" 		=> \$NOEXEC ,
		"DEBUG"      	=> \$DEBUG ,
		"DEBUG2"      	=> \$DEBUG2
		);

print "\n";
print "------------\n";
print "SERVER     = $SERVER\n";
print "USER       = $USER\n";
print "DODELETE   = $DODELETE\n";
print "FIXEDLENGTH= $FIXEDLENGTH\n" if $FIXEDLENGTH;
print "LOGFILE    = $LOGFILE\n";
print "SKIPHEAD   = $SKIPHEAD\n";
print "SKIPTAIL   = $SKIPTAIL\n";
print "REJECTFILE = $REJECTFILE\n";
print "PASSWORD   = $PASSWORD\n";
print "DATABASE   = $DB\n";
print "PROC       = $PROC\n" if $PROC;
print "TABLE      = $TABLE\n" if $TABLE;
print "FILE       = $FILE\n";
print "DEFFILE    = $DEFFILE\n";
print "NODETAILS  = $NODETAILS\n";
print "NOEXEC     = $NOEXEC\n";
print "DEBUG      = $DEBUG\n";
print "------------\n";
print "\n";

die usage("Must pass server\n"	)	unless defined $SERVER;
die usage("Must pass username\n")	unless defined $USER;
die usage("Must pass DB\n"	)			unless defined $DB;
die usage("Must pass password\n")	unless defined $PASSWORD;
die usage("Must pass DEFFILE\n"	)	unless defined $DEFFILE;
die usage("Must pass FILE\n"	)		unless defined $FILE;

$LABEL=basename($FILE) unless $LABEL;

my($num_ok_rows)=0;
my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD);
#### dbi_set_debug(1) if $DEBUG;
if( ! $rc ) {
	print "Cant connect to $SERVER as $USER\n";
} elsif( $SHOWSQL ) {
	print "Connected to $SERVER\n";
}
dbi_set_mode("INLINE");

output( "Successful login to $SERVER\n" );
my(%LTH,%TYP,%STCOL,@ORDER);
open(DEF,$DEFFILE) or die "Cant Read Definition File $DEFFILE $!\n";
while(<DEF>) {
	next if /^#/ or /^\s*$/;
	chomp;
	# output( "." );
	my($colid,$startcol,$lth,$type)=split;
	push @ORDER,$colid;
	$LTH{$colid}=$lth;
	$STCOL{$colid}=$startcol -1;
	$TYP{$colid}=$type;
}
close(DEF);
output( "\n" );

#foreach (keys %TYP) { print "K=$_\n"; }
#exit(0);

if( $LOGFILE ) {
	open(LOGFILE,">> ".$LOGFILE) or die "Cant write $LOGFILE\n";
}

# 20070417 LS - changed "if -w" to "if -e" and moved the open

my($num_rejects)=0;
if( $REJECTFILE ) {
	unlink $REJECTFILE if -e $REJECTFILE;
}

if( $DODELETE ) {
	my($q)="delete $DODELETE where FILENAME='".$LABEL."'";
	print $q,"\n" if $SHOWSQL;
	print LOGFILE $q,"\n" if $LOGFILE;
	foreach( dbi_query( -db=>$DB, -query=>$q )) {
			my @dat=dbi_decode_row($_);
			die @dat;
		}
}

$SKIPHEAD=0 unless $SKIPHEAD;
$SKIPTAIL=0 unless $SKIPTAIL;
my(@dat);


open(FIL,$FILE) or die "Cant Read Data File $FILE $!\n";
if( $USEBCP ) {
	output( "CREATING BCP FILE $FILE.bcp\n" );
	open(OUTFIL,">".$FILE.".bcp") or die "Cant Write $FILE.bcp $!\n";
}
if( $FIXEDLENGTH ) {
	my($record);
	#my($rowid)=0;
	print "READING FILE\n";
	until( eof(FIL) ) {
		read(FIL,$record,$FIXEDLENGTH) == $FIXEDLENGTH
			or die "ERROR: Short Read\n";
		#print $record,"\n\n";
		push @dat, $record;
		# $rowid++;
		# die if $rowid>3;
	}
} else {
	while (<FIL>) {
		chomp;
		push @dat, $_;
	}
}
close(FIL);

# die "DONE" if $FIXEDLENGTH;
shift @dat while $SKIPHEAD-- > 0;
pop   @dat while $SKIPTAIL-- > 0;

print "We expect to load ",($#dat+1)," rows from $FILE\n";
my $fmt;
my $rowid=0;
my $separator=',';
$separator='~~' if $USEBCP;

foreach( @dat ) {
	my($row)=$_;
	$rowid++;
	print "=========\nROW=$row\n=========\n" if $DEBUG or $DEBUG2;
	next if /^#/ or /^\s*$/;
	next if (substr($row, 0, 34) eq     "THERE WERE NO TRADES PROCESSED FOR");
	chomp;
	my($query)="exec $PROC " if $PROC;
	$query="insert $TABLE values (" if $TABLE;
	my($itm)=1;
	foreach (@ORDER) {
		my($val)= substr( $row, $STCOL{$_}, $LTH{$_} );
		printf "\t%d) %-15s st=%-3d lth=%-3d val=%s\n",$itm++,$_,$STCOL{$_},$LTH{$_},$val if $DEBUG2;
		if( $STCOL{$_} <= -1) {
			if( $USEBCP ) {
				print $LTH{$_},$separator if $DEBUG2;
				print OUTFIL $LTH{$_},$separator;
			} else {
				$query.= $LTH{$_}.",";
			}
			next;
		}
		if ($TYP{$_} eq "c" || $TYP{$_} eq "cu") {
                  $val =~ s/\s+$//;
                  if ($TYP{$_} eq "cu") { $val = uc($val);  }

                  if ($USEBCP) {
                    $val=~s/^\s+//;
                    print $val,$separator if $DEBUG2;
                    print OUTFIL $val,$separator;
                  } else {
                    $query.= slashquote($val).",";
                  }
                } elsif ($TYP{$_} eq "yyyymmdd") {
                  if( $val =~ /^\s*$/ )
                  {
                    if( $USEBCP )
                    {
                      print $separator if $DEBUG2;
                      print OUTFIL $separator;
                    }
                    else
                    {
                      $query.="null,";
                    }
                  }
                  else
                  {
                    if( $USEBCP )
                    {
                      print substr($val,0,4)."/".substr($val,4,2)."/".substr($val,6,2).$separator if $DEBUG2;
                      print OUTFIL substr($val,0,4)."/".substr($val,4,2)."/".substr($val,6,2).$separator;
                    }
                    else
                    {
                      $query.= "'".substr($val,0,4)."/".substr($val,4,2)."/".substr($val,6,2)."'".",";
                    }
                  }
                } elsif ($TYP{$_} eq "mmddyyyy") {
                  if( $val =~ /^\s*$/ )
                  {
                    if( $USEBCP )
                    {
                      print $separator if $DEBUG2;
                      print OUTFIL $separator;
                    }
                    else
                    {
                      $query.="null,";
                    }
                  }
                  else
                  {
                    my $yyyy_mm_dd = substr($val,4,4)."/".substr($val,0,2)."/".substr($val,2,2);
                    if( $USEBCP )
                    {
                      print $yyyy_mm_dd.$separator if $DEBUG2;
                      print OUTFIL $yyyy_mm_dd.$separator;
                    }
                    else
                    {
                      $query.= "'".$yyyy_mm_dd."'".",";
                    }
                  }
                }
                elsif ($TYP{$_} eq "mmddyy")		{
        if( $val =~ /^\s*$/ ) {
		  	  	if( $USEBCP ) {
		  	  		print $separator if $DEBUG2;
					print OUTFIL $separator;
				} else {
			 		$query.="null,";
			 	}
			} else {
		  		my $ymd;
			   $val =~ s/^\s*//;
			   if ($val eq "") {$ymd = "";}
			   else
			   {
			    my $yyyy = substr($val, 4, 2) + 1900;
			    if ($yyyy < 1970) {$yyyy += 100};
			    $ymd.= $yyyy."/".substr($val,0,2)
					."/".substr($val,2,2);
			   }
			   if( $USEBCP ) {
					print $ymd.$separator if $DEBUG2;
					print OUTFIL $ymd.$separator;
				} else {
			 		$query.= "'".$ymd."'".",";
			 	}
			}
		}
		elsif( $TYP{$_} =~ /^n\d+/ ) {
			my($c)=$TYP{$_};
			$c=~s/^n//;
			$c= length($val)-$c;
			$val = substr($val,0,$c).".".substr($val,$c);
			$val=~s/^0+//;
			$val="0".$val if $val=~/^\./;

			if( $USEBCP ) {
				$val=~s/^\s+//;
				print $val,$separator if $DEBUG2;
				print OUTFIL $val.$separator;
			} else {
				$query.= $val.",";
			}

			print "\tNEW VAL=$val\n" if $DEBUG2;
		}
		elsif ($TYP{$_} eq "c-")		# c-
		{
		  $val =~ s/\s+$//;
		  $val =~ s/-//g;

		  $fmt = sprintf("%%-%ds", $LTH{$_});
		  $val = sprintf($fmt, $val);

			if( $USEBCP ) {
				$val=~s/^\s+//;
				print $val,$separator if $DEBUG2;
				print OUTFIL $val.$separator;
			} else {
				$query.= slashquote($val).",";
			}
		}
		elsif ($TYP{$_} eq "yyyy-mm-dd")	# yyyy-mm-dd
		{
			if( $USEBCP ) {
				print substr($val,0,4)."/".substr($val,5,2)."/".substr($val,8,2).$separator if $DEBUG2;
				print OUTFIL substr($val,0,4)."/".substr($val,5,2)."/".substr($val,8,2).$separator;
			} else {
				my $ymd = substr($val,0,4)
				     ."/".substr($val,5,2)
				     ."/".substr($val,8,2);
				if ($ymd eq '    /  /  ')
				{
				  $ymd = 'null';
				}
				else
				{
				  $ymd = slashquote($ymd);
				}
				$query.= $ymd.",";
			}
		}
		elsif ($TYP{$_} eq "n")		# n
		{
		  $val =~ s/^\s*//;
		  $val =~ s/\s*$//;
		  $val =~ s/^0+//;
		  $val = "0".$val if (($val=~/^\./) || ($val eq ""));
		  $fmt = sprintf("%%%ds", $LTH{$_});
		  $val = sprintf($fmt, $val);

		  if( $USEBCP ) {
		  		$val=~s/^\s+//;
		  	 	print $val,$separator if $DEBUG2;
				print OUTFIL $val.$separator;
			} else {
				$query.= $val.",";
			}
		} else {
			die "ERROR - Bad Type $TYP{$_} FOR key $_\n";
		}
	}
	if( $USEBCP ) {
		print $LABEL."\n" if $DEBUG2;
		print OUTFIL $LABEL."\n";
		die "ONE LINE WRITTEN" if $DEBUG2;
		next;
	}

	$query.="'".$LABEL."'";
	$query.=")" if $TABLE;
	$query=~s/$/\n/;

	output( "(noexec)" ) if $NOEXEC;
	output( $query ) unless $NODETAILS and ! $NOEXEC and ! $DEBUG;
	output( "+" ) if $rowid%100==0;

	next if $NOEXEC;
	$num_ok_rows++;
	print LOGFILE $query,"\n" if $LOGFILE;
	print $query,"\n" if $SHOWSQL;
	foreach( dbi_query(-db=>$DB,-query=>$query))
	{
	  my @dat=dbi_decode_row($_);
	  output( "Returned : ",join(" ",@dat),"\n" );

	  $num_rejects++;
	  next unless $REJECTFILE;
	  # 20070417 LS moved the open here
	  if (! fileno REJECT)
	  {
	    open(REJECT,"> ".$REJECTFILE)
	    or die "Cant write $REJECTFILE\n";
	  }
	  print REJECT "============================\n";
	  print REJECT $row,"\n";
	  print REJECT "Query=",$query;
	  print REJECT "Reason=",join("",@dat),"\n";
	}
}

print "\n";
if( $num_rejects ) {
  print "REJECT FILE is $REJECTFILE ($num_ok_rows inserted - $num_rejects rows)\n";
} else {	# 20060417 LS suppress close & unlink
# close(REJECT) if $REJECTFILE;
# unlink $REJECTFILE  if $REJECTFILE;
 print "Successful Completion ($num_ok_rows inserted - no rejects) \n";
}

sub output {
	print @_;
	print LOGFILE @_ if $LOGFILE;
}

sub slashquote
{
  my $val = $_[0];
  my $quo = $val;

  $quo =~ s/'/''/g;

# print "$val\n";
# print "$quo\n\n";

  return "'$quo'";
}

__END__

=head1 NAME

file_loader.pl - generic utility to load a file

=head2 DESCRIPTION

This program actually reads a fixed format file and runs a stored procedure on each
row.  The stored procedure can, if you want, insert into a table.

=head2 USAGE

 USAGE: file_loader.pl --LOGFILE=file --REJECTFILE=rfile --PROC=proc -SERVER=srv -USER-usr -PASSWORD=pass -DATABASE=db -FILE=file -DEFFILE=def --SKIPTAIL=rows --SKIPHEAD=rows [-NOEXEC] [-DEBUG]

=head2 MANDATORY ARGUMENTS

 SERVER   : TARGET SERVER INFORMAITON
 USER     : TARGET SERVER INFORMAITON
 PASSWORD : TARGET SERVER INFORMAITON
 DATABASE : TARGET SERVER INFORMAITON
 FILE     : Input File
 PROC     : Stored procedure to call on each row of the file
 DEFFILE  : Definition File : $colname,$startcol,$lth,$type
            type = c: character field
            type = cu: character field, go to upper case
	    type = c-: character field, "-" gets removed
	    type = yyyymmdd: date field in the form yyyymmdd
	    type = mmddyyyy: date field in the form mmddyyyy
	    type = yyyy-mm-dd: date field in the form yyyy-mm-dd
	    type = mmddyy: date field in the form mmddyy
	    type = nD where D is a number of 1 or more digits:
	           number field written with implied decimal point
      		  before the last D-many digits
	    type = n: plain number field (possibly with decimal point)
	           blank field is mapped to 0

=head2 OPTIONAL ARGUMENTS

 FIXEDLENGTH: fixed length records (argument is the length)
 DEBUG      : Call the program in diagnostic mode
 DODELETE   : run query before you load the
              delete $DODELETE where FILENAME='".basename($FILE)."
              or if $LABEL is defined
              delete $DODELETE where FILENAME'$LABEL
 SKIPHEAD   : skip this number of rows from the head of the file
 SKIPTAIL   : skip this number of rows from the end of the file
 REJECTFILE : File name for rows that dont load correctly
 NODETAILS  : Dont print the query that you run
 NOEXEC     : No Exec Mode - Just print the queries

=head2 ALGORITHM

 Logs In To Target Db
 opens $DEFFILE and reads each non comment (^# or blank) line
    - format ColumnId, StartColumn, Length, Type
    - push @ORDER,ColumnId
 delete $DODELETE where FILENAME='".basename($FILE)."'";
 reads FILE into an array
    - skips head or tail if SKIPHEAD or SKIPTAIL
 foreach row in the array
	my($query)="exec $PROC ";
	foreach (@ORDER) {
		$val = substring of data as appropriate
      process based on type
         type='c'
         type='yyyymmdd'
         etc based on perldoc
   	run query

=head2 EXAMPLE

  C:\Perl\bin\perl.exe G:/gem/ADMIN_SCRIPTS/bin/file_loader.pl --DODELETE=NA_table
	--PROC=load_NA_sp --SKIPHEAD=0 --SKIPTAIL=0
	--SERVER=MYDB --PASSWORD=mypass --USER=mylogin --DATABASE=mydb
	--DEFFILE=code/loader/StdNA.def --FILE=X:/FILENAME.TXT --REJECTFILE=data/loader.REJ
	--NODETAILS --LOGFILE=data/Loader.log

=head2 SAMPLE DEFFILE

	RecordType      1   1  c
	AcctNum         2   8  c
	Name            22  30 c
	SSN             52  9  c
	AcctName        61  80 c
	FName           141  20 c
	LName           161 30 c
	CreateDate      191 8  yyyymmdd
	YYYYMMDD        199 8  c
	PrimarySSN      207 9  c
	NA1             222 80 c
	NA2             302 80 c
	City            382 26 c
	State           408 4  c
	Zip             412 10 c
	EmplId          422 10 c

=cut


