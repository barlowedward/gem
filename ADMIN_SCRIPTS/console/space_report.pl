#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copynight (c) 1997-2008 By Edward Barlow All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# Modified by B.Muthaiah on 04.06.2006 :
#	1. Aligned the values to the right for easy readability
#	2. Removed the rounding in total row - int() removed
#


use strict;
use Getopt::Std;
use Repository;
use File::Basename;
use File::Find;
use Sys::Hostname;
use Do_Time;
use Time::Local;
use CommonHeader;

my(@days_in_month)=(0,31,59,90,120,151,181,212,243,273,304,334,365);

# cd to the right directory
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

$| = 1;

sub usage
{
	print @_;
	print "space_report.pl -i sourcedir -d
\n";
   return "\n";
}

use vars qw($opt_i $opt_d);
my($starttime)=time;

die usage("Bad Parameter List\n") unless getopts('di:');

if( ! defined $opt_i ) {
	use Repository;
	my($gr_dir)=get_gem_root_dir();
	die "No Gem Root Dir" unless -d $gr_dir;
	$opt_i = "$gr_dir/data/system_information_data";
	die "No Input File $opt_i" unless -d $opt_i;
}

die usage("Must Pass Output Directory\n") unless defined $opt_i;
die usage("Output Directory $opt_i does not exist\n") unless -d $opt_i;
$opt_i=~s/\s*$//;
$opt_i=~s/\/$//;

my($tdy,$offset)=today();
my($ydy)=$tdy-1;
#print "OFFSET = $offset \n";

my($first_day,$last_day)=(21000101,0);
my %SRVR_DB_LIST;			# server\db keys
my %SRVR_DB_LIST_PCT;	# server\db but only if there is a pct used
my %DB_SPACE_RESULTS;	# server\db\week_no
my %DB_SPACE_PERCENT;	# server\db\week_no
my @other_errors;
# print "LINE ",__LINE__," FIRST=$first_day LAST=$last_day\n";
my($curday)=do_time(-fmt=>'yyyymmdd');
print "Searching $opt_i\n" if $opt_d;
find(\&wanted,$opt_i);
# print "LINE ",__LINE__," FIRST=$first_day LAST=$last_day\n";
print "Done Searching (".(time-$starttime)." secs\n" if $opt_d;

$starttime=time;
# print "LINE ",__LINE__," FIRST=$first_day LAST=$last_day\n";
if($first_day==21000101 and $last_day==0) {
	print "<h1>Space History Report</h1>\n";
	print "Searching directory $opt_i for files\n";
	print "No Data Points Found - Have you collected any data?\n" ;
	exit(0);
}

print "<h1>Space History Report</h1>\n";
print "created by space_report.pl at ".localtime(time)."<br>\n";
print "First Day of Monitoring= $first_day <br>\n";
print "Last  Day of Monitoring= $last_day <p>\n";
print "The data for this report has been collected and stored in flat files.  This includes a daily summary of space usage on a database and on a table level.   This report summarizes this data, which is found in the $opt_i directory on ",hostname()," and typically will be of the format dbspace.SERVER.DATABASE. If there are incorrect rows in this table, you should remove and edit the files by hand.\n<p>";

print "<h1>Miscellaneous Sizing Errors</h1>\n";
print "<TABLE BORDER=1><TR BGCOLOR=#FFCCFF><TH>SERVER</TH><TH>DATABASE</TH><TH>ERROR</TH><TH>FILENAME</TH><TH>DAY</TH></TR>";
print join("\n",@other_errors);
print "</TABLE><p>";

print "<h1>List of Databases over 80% Full</h1>\n";
print "<TABLE BORDER=1>\n";
print "<TR BGCOLOR=#FFCCFF><TH>Server</TH><TH>Database</TH><TH>Percent</TH><TH>MegaBytes</TH></TR>\n";
foreach ( sort keys %SRVR_DB_LIST_PCT ) {
	next unless $DB_SPACE_PERCENT{$_." ".to_week($tdy)}>=80
			  or  $DB_SPACE_PERCENT{$_." ".to_week($ydy)}>=80;
	my($color)="BGCOLOR=LIGHTBLUE" if $DB_SPACE_PERCENT{$_." ".to_week($last_day)}>80;
	$color    ="BGCOLOR=PINK"  if $DB_SPACE_PERCENT{$_." ".to_week($last_day)}>90;

# Next Line Modified by B.Muthaiah on 04.06.2006 - aligned the values to the Right :

	print "<TR $color><TD $color>",join("</TD><TD>",split(/\s+/,$_)),
			"</TD><TD $color align=right>",
			$DB_SPACE_PERCENT{$_." ".to_week($last_day)},
			"</TD><TD $color align=right>",
			$DB_SPACE_RESULTS{$_." ".to_week($last_day)},
			"</TD></TR>\n";
}
print "</TABLE>\n";

print "<h1>Space Growth In MB (max space for week shown)</h1>\n";
show_results(0);
print "<h1>Space Growth In Percent Used (max space for week shown)</h1>\n";
show_results(1);
print "Done  (".(time-$starttime)." secs\n" if $opt_d;
exit(0);

sub show_results
{
	my($flag)=@_;
	print "<TABLE BORDER=1>";
	my($header_row)="<TR><TH BGCOLOR=#FFCCFF>SERVER</TH><TH BGCOLOR=#FFCCFF>DB</TH>\n";
	my($num_cols)=2;

	my $day=$last_day;
	#print "DBG: DAY=$day\n";
	#print "DBG: first_day=$first_day\n";
	while( $day >= $first_day) {
		my($first_dow) = first_of_week(to_week($day));
		#print "DBG: FIRST_DOW=$first_dow\n";
		$first_dow =~ s/^\d\d\d\d//;
		#print "DBG: FIRST_DOW=$first_dow\n";
		$header_row.= "<TH BGCOLOR=#FFCCFF>".$first_dow."</TD>\n";
		$num_cols++;
		#print "DBG: BEFORE DATEADD - $day\n";
		$day=dateadd($day,-7);
		#print "DBG: AFTER DATEADD - $day\n";
	}
	$header_row.= "</TR>\n";
	print $header_row;
	my(@keys);
	if( $flag == 0 ) {
		@keys = sort keys %SRVR_DB_LIST;
	} else {
		@keys = sort keys %SRVR_DB_LIST_PCT;
	}
	my(%SERVER_TOTALS);
	my($last_server)="xxx";
	foreach ( @keys ) {
		my($srv,$db)=split;

		# remember to duplicate this next block for the last row in the table
		if( $flag==0 ) {
		if( $srv ne $last_server and $last_server ne "xxx" ) {
			print "<TR BGCOLOR=YELLOW><TD>$last_server</TD><TD>[Server Total]</TD>";
			my $day2=$last_day;
			while( $day2>=$first_day) {
				my($key) = to_week($day2);

# Next Line Modified by B.Muthaiah on 04.06.2006 - removed the rounding of the Total - int() removed :

				print "<TD align=right>",$SERVER_TOTALS{$key},"</TD>\n";
				$day2=dateadd($day2,-7);
			}
			print "</TR>\n";
			undef %SERVER_TOTALS;

			print "<TR>";
			for (1..$num_cols) { print "\t<TD>&nbsp;</TD>\n"; }
			print "</TR>";
			print $header_row;

		}
		}

		$last_server=$srv;
		print "<TR><TD>$srv</TD><TD>$db</TD>\n";
		my $day=$last_day;
		while( $day>=$first_day) {
			my($datapt)=$DB_SPACE_RESULTS{$_." ".to_week($day)} if $flag==0;
			$datapt=$DB_SPACE_PERCENT{$_." ".to_week($day)} if $flag==1;
			if( defined $datapt and $datapt=~/\d/ ) {
				my($bgcolor)="BGCOLOR=LIGHTBLUE" if $DB_SPACE_PERCENT{$_." ".to_week($day)}>80;
				$bgcolor    ="BGCOLOR=PINK"  if $DB_SPACE_PERCENT{$_." ".to_week($day)}>90;
				if( $flag==0 ) {
					if( defined $SERVER_TOTALS{to_week($day)} ) {
						$SERVER_TOTALS{to_week($day)}+=$datapt;
					} else {
						$SERVER_TOTALS{to_week($day)}=$datapt;
					}
				}
				print "<TD $bgcolor align=right>",$datapt,"</TD>\n";
			} else {
				print "<TD>&nbsp;</TD>\n";
			}
			$day=dateadd($day,-7);
		}
		print "</TR>\n";
	}
	# duplicate of above
	if( $flag==0 ) {
		#if( $srv ne $last_server and $last_server ne "xxx" ) {
			print "<TR BGCOLOR=YELLOW><TD>$last_server</TD><TD>[Server Total]</TD>";
			my $day2=$last_day;
			while( $day2>=$first_day) {
				my($key) = to_week($day2);
				print "<TD align=right>",($SERVER_TOTALS{$key}),"</TD>\n";
				$day2=dateadd($day2,-7);
			}
			print "</TR>\n";

			print "<TR>";
			for (1..$num_cols) { print "\t<TD>&nbsp;</TD>\n"; }
			print "</TR>";
			print $header_row;

		#	undef %SERVER_TOTALS;
		#}
	}
	print "</TABLE>\n";
}

sub dateadd
{
	my($dayno,$interval)=@_;
	$dayno =~ m/^(\d\d\d\d)(\d\d)(\d\d)$/;
	my($y,$m,$d)= ($1,$2,$3);
	$m--;
	$y-=1900;
	my($curt)=timelocal(0,0,0,$d,$m,$y)+($interval*24*60*60);

   my(@t)=localtime($curt);
   $t[4]++;
   $t[5]+=1900;
   substr($t[3],0,0)="0" if length($t[3])==1;
   substr($t[4],0,0)="0" if length($t[4])==1;
   substr($t[5],0,0)="0" if length($t[5])==1;
   return $t[5].$t[4].$t[3];
}

sub first_of_week
{
	my($wkno)=@_;
	my($d,$m,$y)=(0,0,2000);
	if( $wkno>52 ) {
		$y=int($wkno/52);
		$wkno-=(52*$y);
		$y+=2000;
	}

	$wkno--;
	$d = $wkno*7+1;
	while($m<12) {
		$m++;
		next if $d > $days_in_month[$m];
#print "DBG:  Found day $d in month $m\n";
		$d -= $days_in_month[$m-1];
#print "DBG:  day now $d\n";
		last;
	}

   substr($d,0,0)="0" if length($d)==1;
   substr($m,0,0)="0" if length($m)==1;

	return $y.$m.$d;
}

sub to_week
{
	my($dayno)=@_;
	$dayno =~ m/^(\d\d\d\d)(\d\d)(\d\d)$/;
	my($y,$m,$d)= ($1,$2,$3);
	$m--;
	$y-=2000;
	return 52*$y + int(($d+$days_in_month[$m])/7);
}

sub wanted
{
	return unless /^dbspace/;
	return if /.tmp$/;
	my($dummy,$server,$db)=split(/\./,$_);
	# get
	print "Opening File $_\n" if $opt_d;
	my($filenm)=$_;
	my($rc)=open(FIL,$_);
	if( ! $rc ) {
		print "WARNING: Cant open $_\n";
		return;
	}
	#print "LINE ",__LINE__," FIRST=$first_day LAST=$last_day FILE=$_\n";
	while ( <FIL> ) {
		chomp;
		s/\s+$//;
		#print "read: $_\n";
		# might not have all fields if sql server
		my($day,$dbase,$sp_all,$sp_usd,$sp_pct,$log_all,$log_usd,$log_pct)=split;
		#print "FFOUND - data=$sp_all / log=$sp_usd ... pct=$log_pct\n";
		next unless defined $dbase and defined $sp_all and defined $sp_usd;
		if( $day!~/^\d\d\d\d\d\d\d\d$/ ) {
			warn "Invalid Line In File $File::Find::name (day $day bad format)\nLine=$_ \n";
			return;
		}
		$first_day = $day if $first_day > $day;
		$last_day  = $day if $last_day  < $day;

		$SRVR_DB_LIST{$server." ".$db}=1;

		if( ! defined $sp_pct and ! defined $log_all and ! defined $log_usd and ! defined $log_pct ) {
			#print "NADA DEFINED\n";
			# from a sql server  format day/name/data/log
			if( defined $DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)} ) {
				$DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)}=$sp_all
					if $DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)} >= $sp_all;
			} else {
				$DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)}=$sp_all;
			}
			#print "Original Line=$_\n";
			push @other_errors, "<TR><TD>$server</TD><TD>$db</TD><TD><b>Log Space  > Db Space </b>.  Log=$sp_usd Db=$sp_all </TD><TD>$filenm </TD><TD>day $day</TD></TR>\n" if $sp_usd>$sp_all and $sp_usd>10 and $curday-$day==0;
			push @other_errors, "<TR><TD>$server</TD><TD>$db</TD><TD><b>Huge Tran Log</b> $sp_usd  </TD><TD>$filenm </TD><TD>day $day</TD></TR>\n"
				if $sp_usd<=$sp_all and $sp_usd>1000 and $curday-$day==0;
			next;
		}

		$SRVR_DB_LIST_PCT{$server." ".$db}=1;

		# round day back to prior sunday
		if( defined $DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)} ) {
			$DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)}=$sp_usd
				if $DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)} >= $sp_usd;
			$DB_SPACE_PERCENT{$server." ".$db." ".to_week($day)}=$sp_pct
				if $DB_SPACE_PERCENT{$server." ".$db." ".to_week($day)} >= $sp_pct;
		} else {
			$DB_SPACE_RESULTS{$server." ".$db." ".to_week($day)}=$sp_usd;
			$DB_SPACE_PERCENT{$server." ".$db." ".to_week($day)}=$sp_pct;
		}
	}
	#print "LINE ",__LINE__," FIRST=$first_day LAST=$last_day FILE=$_\n";

	close FIL;
}

sub today
{
   my(@t)=localtime(time);
   $t[4]++;
   $t[5]+=1900;
   substr($t[3],0,0)="0" if length($t[3])==1;
   substr($t[4],0,0)="0" if length($t[4])==1;
   substr($t[5],0,0)="0" if length($t[5])==1;
   return ($t[5].$t[4].$t[3],$t[6]);
}

__END__

=head1 NAME

space_report.pl - report on space

=head2 USAGE

space_report.pl -i outdir -d

=head2 NOTES

looks through the system_information_data tree and gets files starting with "dbspace_".  These files contain space used information - one line per day.  This data is parsed and put into a reasonable report.

The files are created by space_monitor.pl
=cut

