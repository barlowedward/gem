#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# copyright (c) 1997-2008 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

use strict;
use File::Basename;
use Getopt::Long;
use Repository;
use DBI;
use MlpAlarm;
use Time::Local;
use CommonFunc;
use DBIFunc;
use Do_Time;
use vars qw( $BATCHID $OUTDIR $SERVER $ALL_REPORT $NOALARM $TRANHOURS $HTML $SQLSVRONLY $SYBASEONLY $DEBUG $ERROR_REPORT $NOSTDOUT);

my(%ignoredb);	# set to 2 for dbs that need to be dumped, 1 for those that dont.
$ignoredb{master}				=2;
$ignoredb{msdb}				=2;
$ignoredb{tempdb}				=1;
$ignoredb{text_db}			=1;
$ignoredb{sybsyntax}			=1;
$ignoredb{pubs}				=1;
$ignoredb{Northwind}			=1;
$ignoredb{sybsystemprocs}	=1;
$ignoredb{sybsystemdb}		=1;
$ignoredb{model}		=1;
$ignoredb{ReportServerTempDB } =1;

$|=1;
my($VERSION)="1.0";
my($c)="</TD>\n\t<TD>";

# changedir to directory in which the executable exists
my($curdir)=dirname($0);
chdir $curdir or die "Cant chdir to $curdir : $! \n";

sub usage {
	print @_,"\n";
	return "Usage: BkpChk_Report.pl [--NOALARM] [--SYBASEONLY] [--SQLSVRONLY] --ALL_REPORT=file --ERROR_REPORT=file --NOSTDOUT --TRANHOURS=hours --BATCHID=name --DEBUG";
}

sub error_out {
   my($msg)=join("",@_);
   $msg = "BATCH ".$BATCHID.": ".$msg if $BATCHID;
   MlpEvent(
      -monitor_program=> "BackupRpt",
      -system=> $BATCHID,
      -message_text=> $msg,
      -severity => "ERROR"
   );
   die $msg;
}


die usage("Bad Parameter List $!\n") unless
	GetOptions(
			"ALL_REPORT=s"			=>\$ALL_REPORT,
			"BATCHID=s"				=>\$BATCHID,
			"TRANHOURS=s"			=>\$TRANHOURS,
			"SYBASEONLY"			=>\$SYBASEONLY,
			"SQLSVRONLY"			=>\$SQLSVRONLY,
			"ERROR_REPORT=s"		=>\$ERROR_REPORT,
			"HTML"					=>\$HTML,
			"NOALARM"				=>\$NOALARM,
			"SERVER=s"				=>\$SERVER,
			"NOSTDOUT"				=>\$NOSTDOUT,
			"DEBUG"					=>\$DEBUG
			 );

my($nl)="\n";

#dbi_set_web_page(1) if $HTML;
status_message("Running BkpChk_Report.pl v1.0 at ".localtime(time)."\n");
status_message("... ERROR_REPORT in $ERROR_REPORT\n") if $ERROR_REPORT;
status_message("... FULL_REPORT  in $ALL_REPORT\n")   if $ALL_REPORT;
status_message("Generating Report Header...\n");

my(@report_header_rows,@error_report_rows,@all_report_rows);
#if( $HTML ) {
	push @report_header_rows, "<br><h2>Batch $BATCHID</h2>"			if $BATCHID;
	push @report_header_rows, "Created by BkpChk_Report.pl at ".localtime(time);
#} else {
#	push @report_header_rows, "Batch $BATCHID $nl  started at ".localtime(time) if $BATCHID;
#}
push @report_header_rows, "<br>Option: SYBASEONLY set " 					if $SYBASEONLY;
push @report_header_rows, "<br>Option: SQLSVRONLY set " 					if $SQLSVRONLY;
push @report_header_rows, "<br>Option: ERROR_REPORT = $ERROR_REPORT " if $ERROR_REPORT;
push @report_header_rows, "<br>Option: FULL_REPORT = $ALL_REPORT " if $ALL_REPORT;
push @report_header_rows, "<hr><br>";

status_message("Reading crosscheck.dat\n");
my(@crosscheckdat)=get_conf_file_dat('crosscheck');
status_message("Defining Thresholds...\n");

my(%crosscheck_dat_thresholds);
my(%server_aliases);
my(@crosscheck_logship,%crosscheck_logship_ignore);
my(%logship_destinations);
my(%logship_dbonly);
foreach (@crosscheckdat) {
		chomp;
		if( /^BACKUP_ALARM_THRESHOLD/i ){
			my(@x)=split;
			if( $#x == 1 ) { # svr,hrs1
				$crosscheck_dat_thresholds{DFLT}=$x[1];
				status_message("Default Threshold Set to $x[1]\n");
			} elsif( $#x == 2 ) {
				$crosscheck_dat_thresholds{$x[1]}=$x[2];

			} elsif( $#x == 3 ) {
				$crosscheck_dat_thresholds{$x[1]."/".$x[2]}=$x[3];
			}
		} elsif( /^SERVER_ALIAS/i ) {
			my($key,$pserver,@d)=split;
			foreach(@d) {$server_aliases{$_}=$pserver;}
		} elsif( /^LOGSHIP_IGNORE/i ) {
			chomp;
			my($key,$srcsvr,$destsvr,@dbs)=split;
			foreach( @dbs ) {
				$crosscheck_logship_ignore{$srcsvr."/".$_}=1;
				$crosscheck_logship_ignore{$destsvr."/".$_}=1;
			}
		} elsif( /^LOGSHIP\s/i or /^LOGSHIP_DBONLY/i ) {
			chomp;
			push @crosscheck_logship,$_;
			my($txt,$svr,$dest,$h1,@databases)=split;
			$logship_dbonly{$svr.":".$dest}=1 if /^LOGSHIP_DBONLY/;
			status_message("  ... Ignoring Logship Destination $dest Thresholds\n");
			$logship_destinations{$dest}=1;
		}
}

foreach my $k ( sort keys %crosscheck_dat_thresholds ) {
	next if $k eq "DFLT" or $k =~ /\// or $crosscheck_dat_thresholds{$k} != -1;
	my($message)="Ignoring Server $k";
	foreach my $r ( keys %crosscheck_dat_thresholds ) {
		next if $r !~ /^$k\//;
		$message.=" $r=$crosscheck_dat_thresholds{$r} ";
	}
	status_message("  ... ".$message."\n");
}


my($Lsec,$Lmin,$Lhour,$Lmday,$Lmon,$Lyear,$Lwday,$Lyday,$Lisdst) =  localtime(time);
# $Lwday = 0 Sunday and 3 Wednesday...

# HOUR_OFFSET IS THE
my($HOUR_OFFSET)=$Lwday * 24 + $Lhour;
foreach ( keys %crosscheck_dat_thresholds ) { # SKIP WEEKENDS
	# WE HAVE AN ARRAY HERE OF HOURS TO WATCH
	# THRESH+=48 if Friday Midnight < currenttime - THRESH < Sun Midnight
	#            if -24 + $Lwday*24 + $Lhour<  THRESH_hours < 24 + $Lwday*24 + $Lhour
	next if $crosscheck_dat_thresholds{$_}<0;
	#print "THRESH FOR $_ is $crosscheck_dat_thresholds{$_}\n";
	print "[ Weekend Reset ] $_ Threshold set to $HOUR_OFFSET+30 (was ",$crosscheck_dat_thresholds{$_},")\n"
											if $DEBUG
												and $HOUR_OFFSET - $crosscheck_dat_thresholds{$_} < 24
												and $HOUR_OFFSET - $crosscheck_dat_thresholds{$_} > -24;
	$crosscheck_dat_thresholds{$_}= $HOUR_OFFSET+30 if
												    $HOUR_OFFSET - $crosscheck_dat_thresholds{$_} < 24
												and $HOUR_OFFSET - $crosscheck_dat_thresholds{$_} > -24;
}
#die "DONE" if $DEBUG;

status_message("Reading Password Files\n");
my(%SERVER_TYPE,%SERVER_IS_PRODUCTION);
foreach ( get_password(-type=>'sybase') ) {
	$SERVER_TYPE{$_}="SYBASE";
	my(%I)=get_password_info(-type=>'sybase',-name=>$_);
	if( $I{NOT_FOUND} ) {
		$SERVER_TYPE{$_}="NOT FOUND";
	} elsif( $I{SERVER_TYPE} eq "PRODUCTION" ) {
		$SERVER_IS_PRODUCTION{$_}=1;
	} elsif( $I{SERVER_TYPE} ) {
		$SERVER_IS_PRODUCTION{$_}=0;
	} else {
		$SERVER_IS_PRODUCTION{$_}=0;
		warn "DBG DBG: SERVER $_ HAS NO TYPE???\n";
	}
}

foreach ( get_password(-type=>'sqlsvr') ) {
	$SERVER_TYPE{$_}="SQLSVR";
	my(%I)=get_password_info(-type=>'sqlsvr',-name=>$_);
	if( $I{NOT_FOUND} ) {
		$SERVER_TYPE{$_}="NOT FOUND";
	} elsif( $I{SERVER_TYPE} eq "PRODUCTION" ) {
		$SERVER_IS_PRODUCTION{$_}=1;
	} elsif( $I{SERVER_TYPE} ) {
		$SERVER_IS_PRODUCTION{$_}=0;
	} else {
		$SERVER_IS_PRODUCTION{$_}=0;
		print "DBG DBG: SERVER $_ HAS NO TYPE???";
	}
}

my(%config_warning_msgs);
my(%DB_STATE,%TRAN_STATE);
my(%last_fulldump_time,%last_fulldump_file,%last_fulldump_lsn,%last_trandump_time
	,%last_trandump_file,%last_trandump_lsn,%last_truncation_time,%last_fullload_time
	,%last_fullload_file,%last_fullload_lsn,%last_tranload_time,%last_tranload_file
	,%last_tranload_lsn,%last_tranload_filetime,%is_tran_truncated,%is_db_usable,%is_readonly,%is_select_into,%bk_server_name
	,%is_singleuser, %is_backupable	 );

status_message("Running MlpGetBackupState()\n");
my(@rc)=MlpGetBackupState(-debug=>$DEBUG);
my($found_out_of_date);
my(%repeats);
foreach ( @rc ) {
	my(@d)=dbi_decode_row($_);
	my($bkserver)=shift @d;
	$bkserver=~s/\s//g;

	my($db)=shift @d;
	$db=~s/\s//;

	my($srv)=pop @d;
	$srv=~s/\s//g;

	$found_out_of_date=1 if $srv eq "";
	$srv=$bkserver if $srv eq "";
	next if defined $SERVER and $srv ne $SERVER and $bkserver ne $SERVER;

	if( defined $server_aliases{$srv} ) {
		status_message("  ... SERVER_ALIAS directive found for $srv of $server_aliases{$srv}\n") unless $repeats{$srv};
		$repeats{$srv}=1;
		$srv=$server_aliases{$srv};
	}

	if( ! $SERVER_TYPE{$srv} ) {
		$config_warning_msgs{ "SERVER $srv IS NOT IN SYBASE OR SQLSVR PASSWORD FILE (ignoring)\n" } =1;
		next;
	} elsif( ! defined $SERVER_IS_PRODUCTION{$srv} ) {
		$config_warning_msgs{ "SERVER $srv HAS NO SERVER_TYPE DIRECTIVE IN PASSWORD FILE (ignoring)\n" } =1;
		next;
	}

	if( $SYBASEONLY and $SERVER_TYPE{$srv} ne "SYBASE" ) {
		$config_warning_msgs{ "SKIPPING SERVER $srv IS NOT SYBASE\n" };
		next;
	}

	if( $SQLSVRONLY and $SERVER_TYPE{$srv} ne "SQLSVR" ) {
		$config_warning_msgs{ "SKIPPING SERVER $srv IS NOT SQL SERVER\n" };
		next;
	}

	my($key)=$srv."/".$db;
	$DB_STATE{$key}		= "UNKNOWN";
	$TRAN_STATE{$key}		= "UNKNOWN";

	sub mymax {
		my($v1,$v2)=@_;
		return $v2 unless defined $v1;
		return $v2 if $v2>$v1;
		return $v1;
	}

	$last_fulldump_time{$key}	= mymax( $last_fulldump_time{$key}, cvt_date(shift @d) );
	$last_fulldump_file{$key}	= mymax( $last_fulldump_file{$key},shift @d);
	$last_fulldump_lsn{$key}	= mymax( $last_fulldump_lsn{$key},shift @d);
	$last_trandump_time{$key}	= mymax( $last_trandump_time{$key}, cvt_date(shift @d) );
	$last_trandump_file{$key}	= mymax( $last_trandump_file{$key},shift @d);
	$last_trandump_lsn{$key}	= mymax( $last_trandump_lsn{$key},shift @d);
	$last_truncation_time{$key}= mymax( $last_truncation_time{$key},shift @d);
	$last_fullload_time{$key}	= mymax( $last_fullload_time{$key}, cvt_date(shift @d) );
	$last_fullload_file{$key}	= mymax( $last_fullload_file{$key},shift @d);
	$last_fullload_lsn{$key}	= mymax( $last_fullload_lsn{$key},shift @d);
	$last_tranload_time{$key}	= mymax( $last_tranload_time{$key}, cvt_date(shift @d) );
	$last_tranload_file{$key}	= mymax( $last_tranload_file{$key},shift @d);
	$last_tranload_lsn{$key}	= mymax( $last_tranload_lsn{$key},shift @d);
	$last_tranload_filetime{$key}	=mymax( $last_tranload_filetime{$key},shift @d);
	$is_tran_truncated{$key}	= mymax( $is_tran_truncated{$key},shift @d);
	$is_tran_truncated{$key}	= 0 unless $is_tran_truncated{$key}==1;
	$is_db_usable{$key}			= mymax( $is_db_usable{$key},shift @d);
	$is_db_usable{$key}			= 1 unless $is_db_usable{$key}==0;

	$is_readonly{$key}			= mymax( $is_readonly{$key},shift @d);
	$is_readonly{$key}			= 0 unless $is_readonly{$key}==1;

	$is_select_into{$key}		= mymax( $is_select_into{$key},shift @d);
	$is_select_into{$key}		= 0 unless $is_select_into{$key}==1;

	$is_singleuser{$key}		= mymax( $is_singleuser{$key},shift @d);
	$is_singleuser{$key}		= 0 unless $is_singleuser{$key}==1;

	$is_backupable{$key}		= mymax( $is_backupable{$key},shift @d);
	$is_backupable{$key}		= 0 unless $is_backupable{$key}==1;

	$bk_server_name{$key}		=$bkserver;
	$is_db_usable{$key}=1 if ! defined $is_db_usable{$key};
	print "KEY=$key\n\tFullDump=".$last_fulldump_time{$key}."=".localtime($last_fulldump_time{$key}).
			"\n\tTranLoad=".$last_tranload_time{$key}."=".localtime($last_tranload_time{$key})."\n" if $DEBUG;
}

my(%dump_error_messages);
my(%logship_errors);
my(%logship_warnings);

#
# THE FOLLOWING SECTION USES crosscheck_dat_thresholds DIRECTIVES TO FIND OUT IF YOU HAVE BACKED UP UR SERVERS
#
sub get_threshold {
	my($key,$svr,$db)=@_;
	if( $crosscheck_dat_thresholds{$key} ) {
		return $crosscheck_dat_thresholds{$key};
	} elsif($crosscheck_dat_thresholds{$svr} ) {
		return $crosscheck_dat_thresholds{$svr};
	} elsif($crosscheck_dat_thresholds{DFLT} ) {
		return $crosscheck_dat_thresholds{DFLT};
	} else {
		die "NO DFLT crosscheck.dat data LINE";
	}
}

my(%ROW_DATA, %STATE_DATA);
status_message("\nComparing Backup Times To Alarm Thresholds...\n\n");
foreach my $keyx ( sort keys %DB_STATE ) {
	my($srv,$db)=split(/\//,$keyx);
	next if $ignoredb{$db}==1;					# but of course master & msdb must be dumped
	next if $db =~ /dr$/i;						# no need to check dr databases
	next if  $db=~/^junk/i  or $db=~/^sqltrace/i or $db=~/^test/i or $db=~/^deleteme/i or $db=~/restore$/i
			or $db=~/tempdb/i or $db=~/^adventureworks/i or $db=~/^pubs/i or $db=~/^sybmgmtdb/
			or $db=~/^mon_db/ or $db=~/_test$/i ;

	my($state)="OK";
	my($thresh) = get_threshold($keyx,$srv,$db);
	print "SVR=$srv DB=$db Thresh=$thresh (hours) Tm=$last_fulldump_time{$keyx}\n" if $DEBUG;
	if( ! $logship_destinations{$srv} ) {		# assume logship dests are always such
		if( $thresh>=0 ) {
			$thresh = time-$thresh*3600;
			if( ! $last_fulldump_time{$keyx} ) {
				$state = "NoBackup";
				$dump_error_messages{$keyx}="GEM_003: DB Has Never Been Backed Up" if $is_db_usable{$keyx} and ! $is_readonly{$keyx} and ! $is_singleuser{$keyx};
			} elsif( $thresh < $last_fulldump_time{$keyx} ) {
				# BACKED UP RECENTLY
				#$dump_error_messages{$keyx}="Production DB OK TIME=".time." DUMPTM=".$last_fulldump_time{$keyx}." THRESH=$thresh D=".(time-$thresh);
			} elsif( $is_db_usable{$keyx} and ! $is_readonly{$keyx} and ! $is_singleuser{$keyx} ) {
				$state = "NoRecentBackup";
				$dump_error_messages{$keyx}="GEM_004: DB Has Not Been Backed Up Since ".localtime($last_fulldump_time{$keyx})
			} else {
				print "SVR=$srv DB=$db Is OK!\n" if $DEBUG;
			}
		}
	}
	print "\tline=",__LINE__," state=",$state," Err=",$dump_error_messages{$keyx},"\n" if $DEBUG;

	if( $thresh>=0 ) {
		if( ! $is_db_usable{$keyx}  ) {
			#} elsif( $is_db_usable{$_} == 0 and  $last_tranload_time{$_}>0 and $last_tranload_time{$_}<time - 2*$TRANHOURS*60*60
			#and ( $last_fullload_time{$_}==0 or $last_fullload_time{$_}<time-24*60*60) ) {
			#$state="OldTrnLoad";
			$state = "UnusableDb" unless $logship_destinations{$srv};
		}elsif(  $is_singleuser{$keyx} ) {
			$state = "SingleUser";
		}elsif(  $is_readonly{$keyx} ) {
			$state = "ReadOnlyDb";
		}elsif( $last_fulldump_time{$keyx} == 0 and $last_trandump_time{$keyx}==0 ) {
			$state="NoBackup";
		} elsif( $last_fulldump_time{$keyx}>0	and $last_fulldump_time{$keyx} < time - 10*24*60*60		# 10 days
			and ( $last_trandump_time{$keyx}==0 or $last_trandump_time{$keyx}<time - 10*24*60*60 )
			and ( $last_tranload_time{$keyx}==0 or $last_tranload_time{$keyx}<time - 2*24*60*60 )
			and ( $last_fullload_time{$keyx}==0 or $last_fullload_time{$keyx}<time - 2*24*60*60 )
			) {
			$state="OldBackup";
		} elsif( $is_tran_truncated{$keyx}==0 and ( $last_trandump_time{$keyx}==0 or $last_trandump_time{$keyx}<time - $TRANHOURS*60*60 )) {
			$state="OldTrnDump" unless $ignoredb{$db};
		}
	}
	print "\tline=",__LINE__," state=",$state," Err=",$dump_error_messages{$keyx},"\n" if $DEBUG;

	# Here is the heirarchy
	#
	# Backup Report
	# STATE		COLOR  CONDITIONS
	# NoBackup	RED	usable & !full and !tran
	# OldBackup     RED     usable & full & full>2 days & tran>2 days
	# OldBackup     RED     usable & full & full>2 days & tran>2 days
	# OldTran       PINK    usable & !tran_truncated & tran & tran>full and tran>2 Hours
	# OldTLoad      PURPLE    !usable & tranload & tranload<4 Hours & fullload<24 hours
	# NoRecent      PEACH   last_operation > 1 day

	my($row)=#"<TR BGCOLOR=$rowcolor>\n\t<TD>".
				#$state.	"</TD>\n\t<TD>".
				$srv.	"</TD>\n\t<TD>".
				$db. 	"</TD>\n\t<TD>";
	if( $last_fulldump_time{$keyx} ) {
		$row.=	do_time(-time=>$last_fulldump_time{$keyx},-fmt=>"mm/dd hh:mi").$c.to_dhm(time-$last_fulldump_time{$keyx}).$c;
	} else {
		$row.=	"&nbsp;".$c."&nbsp;".$c;
	}
	if( $last_trandump_time{$keyx} ) {
		$row.=	do_time(-time=>$last_trandump_time{$keyx},-fmt=>"mm/dd hh:mi").$c.to_dhm(time-$last_trandump_time{$keyx}).$c;
	} else {
		$row.=	"&nbsp;".$c;
		$row.=	"&nbsp;".$c;
	}
	if( $last_fullload_time{$keyx} ) {
		$row.=	do_time(-time=>$last_fullload_time{$keyx},-fmt=>"mm/dd/yyyy hh:mi").$c.
			to_dhm(time-$last_fullload_time{$keyx}).$c;
	} else {
		$row.=	"&nbsp;".$c;
		$row.=	"&nbsp;".$c;
	}
	if( $last_tranload_time{$keyx} ) {
		$row.=	do_time(-time=>$last_tranload_time{$keyx},-fmt=>"mm/dd hh:mi").$c.
			to_dhm(time-$last_tranload_time{$keyx}).$c;
	} else {
		$row.=	"&nbsp;".$c;
		$row.=	"&nbsp;".$c;
	}
	$row.=	"</TD></TR>\n";

	$ROW_DATA{$keyx}   = $row;
	# status_message("DBG DBG $row\n");
	$STATE_DATA{$keyx} = $state;
}

status_message("\nPrinting Configuraton Errors...\n");
foreach ( sort keys %config_warning_msgs ) {
	status_message( "   ".$_ );
}

#
# THE FOLLOWING SECTION USES LOGSHIP DIRECTIVES TO FIND OUT IF YOU HAVE SHIPPED
#
#
status_message("\n");
foreach (@crosscheck_logship) {
		my($txt,$svr,$dest,$h1,@databases)=split;
		status_message("\nChecking Logshipping/Dbshipping From $svr to $dest\n");

#		foreach ( sort keys %last_trandump_time ) {
#			if( /^$svr/ ) {
#				my($s,$d)=split(/\//,$_);
#				next if $ignoredb{$d};
#				my($fileagehrs) = (time-$last_trandump_time{$_}) / 3600;
#				$fileagehrs *= 100;
#				$fileagehrs = int($fileagehrs);
#				$fileagehrs /= 100;
#
#				my($key)=$_;
#				$key=~s/$svr/$dest/;
#				if( ! $last_tranload_time{$key} ) {
#					$logship_errors{$_}=sprintf("Has Not Been Loaded Into %s\n",$key);
#					#printf "%20s : Has Not Been Loaded Into %s\n",$_,$key;
#					next;
#				}
#				if( $h1 < $fileagehrs ) {
#					$logship_errors{$_}=sprintf( "LAST TRANDUMP AT %s TRANDUMP IS %s HOURS OLD",scalar localtime($last_trandump_time{$_}),$fileagehrs );
#				}
#			}
#		}

		my(%source_dbs);
		if( $#databases>=0 ) {
			foreach ( @databases ) { $source_dbs{$_}=1; }
		} else {
			foreach ( sort keys %DB_STATE ) {
				my($s,$d)=split(/\//,$_);
				next if $ignoredb{$d};
				if( $s eq $dest ) {
					$source_dbs{$d}=1;
				} elsif( $s eq $svr ) {
					$source_dbs{$d}=1;
				}
			}
		}

		foreach ( sort keys %source_dbs ) {
		  next if $crosscheck_logship_ignore{$svr."/".$_} and $crosscheck_logship_ignore{$dest."/".$_};
		  my($smsg)=sprintf("$svr => $dest  db=%-20s dump=",$_);
		  my($srckey)="$svr/$_";
		  my($destkey)="$dest/$_";

		  if( ! $DB_STATE{$destkey} ) {
		  	   my($l)=lc($destkey);
		  	  	foreach ( keys %DB_STATE ) {
		  	  		next unless lc($_) eq $l;
		  	  		status_message("Case Missmatch - Using $_ instead of $destkey\n");
		  	  		$destkey=$_;
		  	  		last;
		  		}
		  }
	 	  my($srctm) = time - $last_trandump_time{$srckey} if $last_trandump_time{$srckey};
	 	  my($desttm)= time - $last_tranload_time{$destkey} if $last_tranload_time{$destkey};
	 	  my($thresh) = get_threshold($srckey,$svr,$_);
	 	  next if $thresh<0;

	 	  if( defined $srctm ) {
	 	  		$smsg .= sprintf("%-9s load=" , do_diff($srctm) );
		  } else {
		  		if( $DB_STATE{$destkey}
					 			and $DB_STATE{$srckey}
					 			and ! $is_select_into{$srckey}
					 			and ! $is_singleuser{$srckey}
					 			and $is_db_usable{$srckey}
					 			and ! $is_readonly{$srckey}
					 			and ! $logship_dbonly{$svr.":".$dest} ) {
					$STATE_DATA{$srckey} = "NoTranDump";
					$logship_errors{$srckey}=sprintf( "SRC DB $_ HAS NO TRAN DUMP TIME")
						unless $crosscheck_logship_ignore{$srckey};
				}
		 		$smsg .= "N.A.      load=";
		  }

	 	  if( defined $desttm ) {
	 	  		$smsg .= sprintf("%-9s ",do_diff($desttm));
	 	  		if( $h1 < $desttm/3600 ) {
					if( $DB_STATE{$destkey}
					 			and $DB_STATE{$srckey}
					 			and ! $is_select_into{$srckey}
					 			and $is_db_usable{$srckey}
					 			and ! $is_readonly{$srckey}
					 			and ! $is_singleuser{$srckey}
					 			and ! $logship_dbonly{$svr.":".$dest} ) {
					 	##print "DBG DBG: dest=$dest _=$_ desttm=$desttm a=$last_tranload_time{$_} b=$last_tranload_time{$destkey} c=$last_tranload_time{$srckey}\n";
						$STATE_DATA{$destkey} = "OldTrnLoad";
		 	  			$logship_errors{$srckey}=sprintf( "LAST TRANLOAD INTO $dest AT %s; TRANLOAD IS %s HOURS OLD. ",
							scalar localtime($last_tranload_time{$destkey}),do_diff($desttm) );
					}
			 		#	if $DB_STATE{$destkey}
				 	#		and $DB_STATE{$srckey}
				 	#		and ! $is_select_into{$srckey}
				 	#		and $is_db_usable{$srckey}
				 	#		and ! $is_readonly{$srckey};
	 	  		}
		  } else {
				if( $DB_STATE{$destkey}
		 			and $DB_STATE{$srckey}
		 			and ! $is_select_into{$srckey}
		 			and $is_db_usable{$srckey}
		 			and ! $is_readonly{$srckey}
		 			and ! $is_singleuser{$srckey}
		 			and ! $logship_dbonly{$svr.":".$dest} ){
			 			$STATE_DATA{$destkey} = "NoTrnLoad";
		 	 			$logship_errors{$srckey}=sprintf( "DEST DB $_ HAS NO TRAN LOAD TIME on $destkey")
							 unless $crosscheck_logship_ignore{$destkey};
	 	 		}
		 		$smsg .=  "N.A.      ";
		  }

		  $smsg .= sprintf(" us=%2s ro=%2s si=%2s su=%2s",$is_db_usable{$srckey},$is_readonly{$srckey},$is_select_into{$srckey},$is_singleuser{$srckey});
		   if( ! $DB_STATE{$srckey} ) {
		     $STATE_DATA{$destkey} = "NoSrcDb";
		  	  $logship_warnings{$srckey} ="GEM_001: NO DB $_ ON SOURCE SERVER $svr"
							 unless $crosscheck_logship_ignore{$destkey};
		   } elsif( ! $DB_STATE{$destkey} ) {
		  		$STATE_DATA{$srckey} = "NoShipToDb";
		  	   $logship_errors{$srckey}="GEM-005: NO DB $_ ON DESTINATION SERVER $dest"
							 unless $crosscheck_logship_ignore{$srckey};
		  	} elsif( $is_select_into{$srckey} and ! $logship_dbonly{$svr.":".$dest}) {
				$STATE_DATA{$destkey} = "SrcDbSelectInto";
		  	   $logship_errors{$srckey}= "GEM-006: SOURCE DB $_ HAS SELECT/INTO SET. "
							 unless $crosscheck_logship_ignore{$srckey};
			} elsif( ! $is_db_usable{$srckey} ) {
				$STATE_DATA{$destkey} = "SrcDbUnusable";
		  	   $logship_warnings{$srckey}= "GEM-007: SOURCE DB $_ IS NOT USABLE"
							 unless $crosscheck_logship_ignore{$srckey};
		  	} elsif( $is_singleuser{$srckey}  and ! $logship_dbonly{$svr.":".$dest}) {
				$logship_warnings{$srckey}= "GEM-007: SOURCE DB $_ IS SINGLEUSER"
							 unless $crosscheck_logship_ignore{$srckey};
  		   } elsif( $is_readonly{$srckey}  and ! $logship_dbonly{$svr.":".$dest}) {
				$logship_warnings{$srckey}= "GEM-007: SOURCE DB $_ IS READONLY"
							 unless $crosscheck_logship_ignore{$srckey};
  		   }

		   status_message($smsg."\n");
		   status_message("   ERROR  : ".$dump_error_messages{$srckey}."\n")   if $dump_error_messages{$srckey};
		   status_message("   ERROR  : ".$dump_error_messages{$destkey}."\n")   if $dump_error_messages{$destkey};
		   status_message("   LOGSHIP ERROR  : ".$logship_errors{$srckey}."\n")   if $logship_errors{$srckey};
		   status_message("   LOGSHIP WARNING: ".$logship_warnings{$srckey}."\n") if $logship_warnings{$srckey};
		}
}

my($bid)=$BATCHID || "BkpChkReport"; 	#Use This? as the report can be called in multiple context
$bid=~s/Weekly$//i;										# weekly runs ar no different than normal ones
MlpManageData(Operation=>"del",Admin=>"Delete By Monitor",Monitor=>$bid);

status_message("\nTHE FOLLOWING ARE WARNINGS (SAVED IN THE ALARM DB)\n\n");
foreach ( sort keys %logship_warnings) {
	my($s,$d)=split(/\//,$_);
	status_message( sprintf("WARNING %-35s: %s\n",$s." ".$d,$logship_warnings{$_}) );
	MlpHeartbeat(  -monitor_program => $bid,
			# -debug => 1,
			-system=> $s,
			-subsystem=> $d." Backup Checker",
			-state=> "WARNING",
			-message_text=> $logship_warnings{$_}."\nModify conf/crosscheck.dat To Adjust" ) unless $NOALARM;
}
status_message("\n");

status_message("\nTHE FOLLOWING ERRORS WILL BE SAVED IN THE ALARM DB\n\n");
status_message("You can control these messags using the configuration file crosscheck.dat\n");
foreach ( sort keys %logship_errors) {
	my($s,$d)=split(/\//,$_);
	status_message( sprintf("ERROR %-37s: %s\n",$s." ".$d,$logship_errors{$_}) );
	MlpHeartbeat(  -monitor_program => $bid,
			# -debug => 1,
			-system=> $s,
			-subsystem=> $d." Backup Checker",
			-state=> "ERROR",
			-message_text=> $logship_errors{$_}."\nModify conf/crosscheck.dat To Adjust" ) unless $NOALARM;
}
foreach ( sort keys %dump_error_messages) {
	my($s,$d)=split(/\//,$_);
	status_message( sprintf("ERROR %-37s: %s\n",$s." ".$d,$dump_error_messages{$_}) );

	MlpHeartbeat(  -monitor_program => $bid,
			# -debug => 1,
			-system=> $s,
			-subsystem=> $d." Backup Checker",
			-state=> "ERROR",
			-message_text=> $dump_error_messages{$_}."\nModify conf/crosscheck.dat To Adjust" ) unless $NOALARM;
}

status_message("\n");

if( $NOALARM ) {
	status_message( "Exiting as --NOALARM is set\n" );
	exit(0);
}

if( ! $ALL_REPORT and ! $ERROR_REPORT ) {
	status_message( "No Reports Found To Create\n" );
	exit(0);
}


status_message("Creating Report Rows\n");
my(%printed_all_header,%printed_err_header);
foreach my $rowkey ( sort keys %ROW_DATA ) {
	my($srv,$db)=split(/\//,$rowkey);

	my($rowcolor)="PURPLE";
	$rowcolor="BEIGE"  		if $STATE_DATA{$rowkey} eq "OK"
									or $STATE_DATA{$rowkey} eq "UnusableDb"
									or $STATE_DATA{$rowkey} eq "NoSrcDb"
	                        or $STATE_DATA{$rowkey} eq "SrcDbUnusable"
									or $STATE_DATA{$rowkey} eq "SrcDbReadonly"
									or $STATE_DATA{$rowkey} eq "SingleUser"
								   or $STATE_DATA{$rowkey} eq "ReadOnlyDb";

	$rowcolor="RED" 			if $STATE_DATA{$rowkey} eq "NoBackup"
									or $STATE_DATA{$rowkey} eq "NoRecentBackup"
									or $STATE_DATA{$rowkey} eq "OldBackup";

	$rowcolor="PINK"  		if $STATE_DATA{$rowkey} eq "OldTrnDump"
									or $STATE_DATA{$rowkey} eq "NoTranDump"
									or $STATE_DATA{$rowkey} eq "NoShipToDb"
									or $STATE_DATA{$rowkey} eq "SrcDbSelectInto";

	$rowcolor="Orange" 		if $STATE_DATA{$rowkey} eq "OldTrnLoad"
									or $STATE_DATA{$rowkey} eq "NoTrnLoad";

#status_message("<PRE>DBG DBG2: k=$rowkey v=$ROW_DATA{$_}\n</PRE>");
	my($rowhdr)="<TR BGCOLOR=$rowcolor>\n\t<TD>".$STATE_DATA{$rowkey}.	"</TD>\n\t<TD>";
	if( ! defined $printed_all_header{$srv} ) {
		$printed_all_header{$srv}=1;
		push @all_report_rows, "<TR><TD COLSPAN=11>&nbsp;</TD></TR>\n" unless $#all_report_rows<0;
		push @all_report_rows, get_header("SERVER=$srv");
	}
	push @all_report_rows,$rowhdr.$ROW_DATA{$rowkey};

	if( $STATE_DATA{$rowkey} ne "OK" ) {
		if( ! defined $printed_err_header{$srv} ) {
			$printed_err_header{$srv}=1;
			push @error_report_rows, "<TR><TD COLSPAN=11>&nbsp;</TD></TR>\n" unless $#error_report_rows<0;
			push @error_report_rows, get_header("SERVER=$srv");
		}
		push @error_report_rows,$rowhdr.$ROW_DATA{$rowkey};
	}
}

status_message( "Writing Saved Data To Report $ALL_REPORT\n" ) if $ALL_REPORT;
status_message( "Writing Saved Data To Report $ERROR_REPORT\n" ) if $ERROR_REPORT;
my($tmpfile);
die "ALL_REPORT and ERROR_REPORT ARE IDENTICAL"
	if $ALL_REPORT and $ERROR_REPORT and $ALL_REPORT eq $ERROR_REPORT;
if( $ALL_REPORT ) {
	unlink $ALL_REPORT if -r $ALL_REPORT;
	open(OUTF,">$ALL_REPORT") or error_out( "Cant Write $ALL_REPORT: $!" );
}
if( $ERROR_REPORT ) {
	unlink $ERROR_REPORT if -r $ALL_REPORT;
	open(ERRF,">$ERROR_REPORT") or error_out( "Cant Write $ERROR_REPORT: $!" );
}

foreach (@report_header_rows) { output_to_both_reports( $_.$nl ); }

# PRINT THE COLOR KEY
output_to_both_reports("<Table><TR><TD>");
output_to_both_reports("</TD></TD ALIGN=RIGHT>");
output_to_both_reports("<TABLE BORDER=1><TR><TH COLSPAN=2 BGCOLOR=#FFCCFF>Color Key For Results</TH></TR>");
output_to_both_reports("<TR BGCOLOR=BEIGE><TD>OK</TD><TD>Appears Ok - Minor Issues</TD></TR>");
output_to_both_reports("<TR BGCOLOR=RED><TD>Backup Errors</TD><TD>No backups, No Recent Backups</TD></TR>");
output_to_both_reports("<TR BGCOLOR=PINK><TD>Warnings</TD><TD>Old Tran Dumps, Shipping Missconfiguration</TD></TR>");
output_to_both_reports("<TR BGCOLOR=ORANGE><TD>Log Shipping Errors</TD><TD>No/Old Tran Loads</TD></TR>");
output_to_both_reports("</TABLE>");
output_to_both_reports("</TD></TR></TABLE><br><br>");

output_to_both_reports( "<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=1>" );
if( $ERROR_REPORT ) 	{	foreach (@error_report_rows) 	{ print ERRF $_."\n"; }}
if( $ALL_REPORT ) 	{	foreach (@all_report_rows) 	{ print OUTF $_."\n";	}}
output_to_both_reports( "</TABLE>\n" );

output_to_both_reports( "Program Completed at ".localtime(time)."\n" );
output_to_both_reports( "OUTPUT MESSAGES were written to $ALL_REPORT\n" )   if $ALL_REPORT;
output_to_both_reports( "ERROR  MESSAGES were written to $ERROR_REPORT\n" ) if $ERROR_REPORT;

close(OUTF) if defined $ALL_REPORT;
close(ERRF) if defined $ERROR_REPORT;
exit(0);

sub get_header {
	my($title)=@_;
	return
		"<TR> <TH BGCOLOR=#FFCCFF Colspan=11>$title</TH></TR>".
		"<TR>	<TH BGCOLOR=#FFCCFF>State</TH>
				<TH BGCOLOR=#FFCCFF>Server</TH>
				<TH BGCOLOR=#FFCCFF>Database</TH>
				<TH BGCOLOR=#FFCCFF Colspan=2>Last Dump</TH>
				<TH BGCOLOR=#FFCCFF Colspan=2>Last Tran Dump</TH>
				<TH BGCOLOR=#FFCCFF Colspan=2>Last Load</TH>
				<TH BGCOLOR=#FFCCFF Colspan=2>Last Tran Load</TH></TR>\n";
}

sub status_message { my(@msg)=@_;	print @msg; }

sub output_to_both_reports {
	my(@msg)=@_;
	print OUTF @msg if  $ALL_REPORT;
	print ERRF @msg if  $ERROR_REPORT;
}

# pass in seconds
sub to_dhm {
	my($t)= @_;
	return "" unless defined $t;
	return "" if $t>100000000;
	if( $t>1000000 ) {
		return "(".int($t/(24*3600))." Days)";
	}
	my $d = int($t/(24*3600));
	$t -= $d*(24*3600);
	my $h = int($t/3600);
	$t -= $h*3600;
	my $m = int($t/60);
	$t -= $m*60;
	return "(".$d."D ".$h."H ".$m."M)";
}

sub cvt_date {
	my($dt)=@_;
	# convert dates... which come in mm-dd-yyyy hh:mi:ss format
	return 0 if ! $dt or $dt eq " ";
	#print "{$dt}";
	#print "defined" if defined $dt;
	$dt=~ m/(\d\d)-(\d\d)-(\d\d\d\d) (\d\d):(\d\d):(\d\d)/;
	my($y)=$3-1900;
	my($m)=$1-1;
	# print "converted $dt to year=$y mday=$2 mon=$m hr=$4 min=$5 sec=$6\n" if $DEBUG;
	my($newdt)=timelocal($6,$5,$4,$2,$m,$y);
	#print "$dt converts to ".localtime($newdt),"\n" if $DEBUG;

	return $newdt;
}

1;

__END__

=head1 NAME

BkpChk_Report.pl - SQL Server Backup Report

=head2 DESCRIPTION

Scans the event log and creates a backup and restore history for a server.
There are two types of reports here.  The first is the Backup Report, which
simply prints backup details for a server.  The second is a Restore report
which is more cmoplicated and shows restore mappings to the primary server

=head2 USAGE

Usage: BkpChk_Report.pl --ALL_REPORT=file --HTMLFILE=file --SYSTEM=system --TRANHOURS=hours --USER=sa --PASSWORD=password --SRVNAME=ODBCID --NOSTDOUT --REPORT=<backup|restore> --PRIMARY=<primary host if restore> --DEBUG --HTMLDETAIL=file --LISTEVENTS

=head2 EXAMPLE

To see whats going on in a server

   BkpChk_Report.pl --SYSTEM=S1 --HOURS=96

To put results in a file

   BkpChk_Report.pl --SYSTEM=S1 --HOURS=96 --NOSTDOUT --ALL_REPORT=my_report.txt

Since the ascii report sucks - an heml file can be generated by

   BkpChk_Report.pl --SYSTEM=S1 --HOURS=96 --NOSTDOUT
      --HTMLFILE=my_report.html

If you are using log shipping, a new report opens up.  Say your server is
BACKUP1 which does log loads from PRIMARY1

  BkpChk_Report.pl --SYSTEM=BACKUP1 --PRIMARY=PRIMARY1 --HOURS=96
  --NOSTDOUT --HTMLFILE=main.html --REPORT=restore --HTMLDETAIL=details.html

The above creates a linked pair of reports - with a summary in HTMLFILE and
details of the shipping and loading in HTMLDETAIL.  Note that some times
your report will be out of sync... the Behind time explains this - its possible
that the lsn's dont match up intentionally.

=head1 BACKUP CROSSCHECK PROCEDURES

a) once per week we run BkpChk_UpdateStaticInfo.pl to update BackupState table static information which includes a list of db's and servers
that are on them as well as dboptions etc... document_all with report 23 or 24 will run this.

b) For Windows, we run MssqlBkpChk.ksh (which calls BkpChk_Mssql_Logs.pl) once per hour.
This batch parses event logs and saves results in BackupState.
The job then runs BkpChk_Report.pl to generate latest reports

BkpChk_Mssql_Logs.pl -HOURS=3 --BATCHID=MssqlBkpChk
	--OUTFILE=//samba666/sybmon/dev/data/html_output/MssqlBkpChk.txt  $*
	 > //samba666/sybmon/dev/data/GEM_BATCHJOB_LOGS/Win_MssqlBkpChk.log
	2> //samba666/sybmon/dev/data/GEM_BATCHJOB_LOGS/Win_MssqlBkpChk.err


	-> Calls BkpChk_Report.pl --SQLSVRONLY -NOSTDOUT
		--ERROR_REPORT=//samba666/sybmon/dev/data/html_output/MssqlBkpChkErrors.html
		--ALL_REPORT=//samba666/sybmon/dev/data/html_output/MssqlBkpChk.html
		--HTML
		--BATCHID=HourlyBackupReport
		--TRANHOURS=2


=end

