# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package template;

@ISA = qw(Plugin);
use strict;

use vars qw($VERSION);
$VERSION = ".01";

################ FIXED CONSTANTS #######################
my($RFRM_Notebook_Tabname)="template";

# %MENUITEM_HASH should be a list of MENUITEM_HASH to add - in the format separated by a slash
# only put the complete paths to the items which you want to see
# or it will generate a Cant locate object method error
my(%MENUITEM_HASH) = ( 
	"PLUGINMENU/Item 1"				=>		"MenuItem 1 Action",
	"PLUGINMENU/Item 2"				=>		"MenuItem 2 Action",
	"PLUGINMENU/Item 3/SubItem1"	=>		"MenuItem 3.1 Action",
	"PLUGINMENU/Item 3/SubItem2"	=>		"MenuItem 3.2 Action");

# CONTEXT_HASH is a hash of buttons labels (keys) to 'contexts' (values)
my(%CONTEXT_HASH) = (
	"PAGE1" => "template_PAGE1",
	"PAGE2" => "template_PAGE2",
);

# RFRM_BUTTONS is a hash of button labels to actions
my(%RFRM_BUTTONS)=(
	"Button 1"			=>	"Button1 Action",
	"Button 2"			=>	"Button2 Action",
	"Button 3"			=>	"Button3 Action",
	"Plugin Help"		=>	"Help Action",
);		

my($RFRM_Title)="Right Frame Title";

################# SUBROUTINES ########################
my($current_main_frame);		# the template tab
my($working_frame);				# the dynamic frame that contains results
my(%CONTEXT_HASH_INV);			# reversed CONTEXT_HASH
my($current_UserAction)="";
my($current_TreeType);			# This will end up being one of the values of CONTEXT_HASH
my($current_TreeItem)="";		# full item path for the selected item of the left side tree
my($save_button);					# the save button
######################################################

sub getTreeItems {
	my($package,$current_Context)=@_;
	$current_TreeType = $current_Context;	
	$package->statusmsg( "[$RFRM_Notebook_Tabname] getTreeItems($current_Context)\n" );
	
	if( ! $CONTEXT_HASH_INV{$current_Context} ) {
		$package->statusmsg( "[$RFRM_Notebook_Tabname] Invalid Tree Type $current_Context = Not Drawing Left Side\n" );
		return ;
	}

	if( defined $$package{-modulename} ) {
		my(@rc);
		if( $current_Context =~ /1$/ ) {	# in this example we have 2 types of tree menus - ending in 1 or 2
			push @rc, "/page1";
			for ( 1..7 ) { push @rc, "/page1/$_";		}	
		} else {
			push @rc, "/page2";
			for ( 1..7 ) { push @rc, "/page2/$_";		}	
		}
		return \@rc;
	}
	return undef;
}

sub event_handler
{
	my($package)=shift;
	my(%args)=@_;

	# if your context is template and you tab out of Plugins - Force a redraw
	# errr... i dont know what this means
	#if( $args{-context} eq "Plans" and $args{-eventtype} eq "tabchange" and $args{-curtab} ne $RFRM_Notebook_Tabname ){
	#	$package->refresh_tree("sqlsvr");
	#	return;
	#}

	return unless $args{-curtab} eq $RFRM_Notebook_Tabname
		or $CONTEXT_HASH_INV{$args{-context}}
		or $args{-eventtype} eq "menu"
		or $args{-eventtype} eq "expbutton";

	my($string)="[$RFRM_Notebook_Tabname] event_handler: Tab=$args{-curtab} \tEVENTTYPE=$args{-eventtype}";
	foreach ( sort keys %args ) { $string.=sprintf("\n\t%12.12s => %s",$_,$args{$_}) unless /-eventtype/; }	
	$package->statusmsg($string);	
	
	if( $args{-eventtype} eq "menu" ) {
		my($found)="FALSE";
		foreach ( keys %MENUITEM_HASH ) { $found="TRUE" if $args{-menuitem} eq $_; }
		return unless $found eq "TRUE";

		$package->statusmsg( "[$RFRM_Notebook_Tabname] menuclick($args{-menuitem})\n" );
		if( $args{-curtab} ne $RFRM_Notebook_Tabname ) {
    			$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() raising tab $RFRM_Notebook_Tabname\n" );
     			$package->ignore_events(1);
				$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
				$package->ignore_events(undef);
		}
		
		$package->handle_plugin_action($MENUITEM_HASH{$args{-menuitem}});
	} elsif( $args{-eventtype} eq "tabchange" ) {
		return if $args{-curtab} ne $RFRM_Notebook_Tabname;
		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() called on change tab to $args{-curtab}\n" );
#		$package->refresh_tree("Plans") if $args{-context} ne "Plans";		
#		$package->handle_plugin_action("TABCHANGE");
	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {
		return unless $CONTEXT_HASH_INV{$args{-context}};
		$package->statusmsg( "[$RFRM_Notebook_Tabname] treebrowse(path=$args{-entryPath})\n" );
		if( $args{-curtab} ne $RFRM_Notebook_Tabname ) {
     		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() raising tab $RFRM_Notebook_Tabname\n" );
			$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
		}
		$current_TreeItem=$args{-entrypath};
		$package->handle_plugin_action('ACTION_SHOW_FILE');
	} elsif( $args{-eventtype} eq "treertclick" ) {
		return if $args{-curtab} ne $RFRM_Notebook_Tabname;
		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() called\n" );
		my($entryPath, $clktext)=($args{-entrypath},$args{-clicktext});
		$package->statusmsg(  "[$RFRM_Notebook_Tabname] rtclick(Path=$entryPath Txt=$clktext)\n" );
		$package->browse($entryPath);
		return unless $CONTEXT_HASH_INV{$args{-context}};
		$package->statusmsg( "[$RFRM_Notebook_Tabname] treebrowse(path=$args{-entryPath})\n" );
		if( $args{-curtab} ne $RFRM_Notebook_Tabname ) {
     		$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() raising tab\n" );
			$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));
		}

		if( $args{-clicktext} eq "Item Help" ){
			$package->handle_plugin_action("Help");
		} elsif( $args{-clicktext} =~ /^Right_Click_Num_1/ ){
			$package->handle_plugin_action("Logs");
		} elsif( $args{-clicktext} eq /^Right_Click_Num_2/ ){
			$package->unimplemented("Right Click Num 2 Unimplemeted");
		} elsif( $args{-clicktext} eq /^Right_Click_Num_A1/ ){
			$package->unimplemented("Right Click A1 Unimplemented.");
		} elsif( $args{-clicktext} eq /^Right_Click_Num_A2/ ){
			$package->unimplemented("Right Click A2 Unimplemented.");
		} elsif( $args{-clicktext} eq /^Delete/ ){
			$package->unimplemented("Delete Unimplemented.")
		} elsif( $args{-clicktext} eq /Properties$/ ){
			$package->handle_plugin_action("Define");
		} else {
			$package->unimplemented( "[$RFRM_Notebook_Tabname] non-programmed response to tree right click\n" );
			$package->handle_plugin_action($args{-clicktext});
		}
	} elsif( $args{-eventtype} eq "expbutton" ) {

		if( $CONTEXT_HASH_INV{$args{-context}} and $args{-curtab} ne $RFRM_Notebook_Tabname ) {
			$package->statusmsg( "[$RFRM_Notebook_Tabname] event_handler() called\n" );
			$package->statusmsg( "[$RFRM_Notebook_Tabname] raising ".ucfirst($package->getPluginPanes())." tab\n" );
			$main::notebook_f->raise(ucfirst($RFRM_Notebook_Tabname));				
		}
	} elsif( $args{-eventtype} eq "imgbutton" ) {
	}
}

sub getTreeRtclk {
	my($package,$current_Context,$treeitmptr)=@_;
	$package->debugmsg(  "[$RFRM_Notebook_Tabname] getTreeRtclk(curtype=$current_Context)\n" );
	return(undef) unless $CONTEXT_HASH_INV{$current_Context};
	
	my(@rightclicks);
	foreach my $itm ( @$treeitmptr ) {
		my(@rtclick)=("Item Help");
		if ( $itm eq "/" ) {
			# push @rightclicks, \@rtclick;
		} else {
			my(@d)=split(/\//,$itm);
			if( $#d==1 ) {
			} elsif( $#d==2 ) {
				unshift @rtclick, "$d[2] Properties";				
				unshift @rtclick, "Right_Click_Num_2 $d[2]";
				unshift @rtclick, "Right_Click_Num_1 $d[2]";
			} elsif( $#d==3 ) {
				unshift @rtclick, "$d[3] Properties";
				unshift @rtclick, "Right_Click_Num_A2 $d[2]";
				unshift @rtclick, "Right_Click_Num_A1 $d[3] on $d[2]";
			}
		}
		push @rightclicks, \@rtclick;
	}
	return \@rightclicks;
}

#####################################################################
# STANDARD FUNCTIONS
#####################################################################
sub getPluginPanes { 		return $RFRM_Notebook_Tabname; }
sub getMenuItems {			return keys %MENUITEM_HASH;}
sub getExplorerButtons {		# This prints 2 buttons - page name $page1 & $page2 
	my($package)=shift;
	return %CONTEXT_HASH;
}

# PLUGIN INITIALIZATION
sub init {
	my($package)=@_;
	$package->debugmsg("Starting init()");
	
	# set up a reverse of the button hash
	foreach ( keys %CONTEXT_HASH ) { 		$CONTEXT_HASH_INV{$CONTEXT_HASH{$_}} = $_;	}
	
	$current_main_frame=$package->clear_tab($RFRM_Notebook_Tabname);

	$current_main_frame->Label(
		-text=>$RFRM_Title,-relief=>'ridge' , -bg=>'white'
		)->pack( -side=>'top', -fill=>'x', -anchor=>'n');

	my($butfrm)=$current_main_frame->Frame()->pack( -fill=>'x', -side=>'top', -anchor=>'n');
	foreach (sort keys %RFRM_BUTTONS) {
		my($helptext)="Help For $_";
		$helptext="Main Help Text" if /Help$/;
		my($i)=$_;
		$package->make_button(
				-widget	=>	$butfrm,
    			-help		=>	$helptext,
    			-text		=>	$i,
    			-command	=> sub {	
    				$package->debugmsg("Clicked On Button $i - $RFRM_BUTTONS{$i}");	
    				$package->handle_plugin_action($RFRM_BUTTONS{$i}); 
    			} 
    		)->pack( -side=>'left', -padx=>3, -pady=>8 );
	}
	
	my($save_button_frame)=$current_main_frame->Frame()->pack( -fill=>'x', -side=>'bottom');
	$save_button = $package->make_button(-widget=>$save_button_frame,
    			-help=>'Save Changes',
    			-text=>"Save Changes",
    			-command=> sub {
    				$package->handle_plugin_action("Save Changes");
    		} );   
    		
	$package->debugmsg("Completed init()");	
}

sub hide_save_button { $save_button->packForget(); }
sub show_save_button { $save_button->pack( -side=>'bottom', -padx=>3, -pady=>8 ); }
		
sub draw_an_area {
	my($package,$action,$item)=@_;
	$package->debugmsg("Started draw_an_area($action,$item)");	

	$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);
	$working_frame  = $current_main_frame->LabFrame(
			-label=>"Current Item = $item - Details Screen",
			-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
			
#	$current_plan="/" unless defined $current_plan;
#	$current_nb=main::plantab_paint_notebook($current_plan,$working_frame);
#	if( defined $current_nb_tab) {
#		$current_nb->raise($current_nb_tab);
#	}
	show_save_button();
	
#	$package->refresh_tree("Plans") if $current_TreeType ne "Plans";
#
}

sub handle_plugin_action {
	my($package,$user_action)=@_;
	$package->debugmsg("Started handle_plugin_action($user_action)");	

	$user_action=$current_UserAction unless defined $user_action;
		
	if( $user_action eq "ACTION_SHOW_FILE" ) {		# PROCESS $current_TreeItem TREE - ITS SELECTED
		$package->draw_an_area("SHOW",$current_TreeItem);
		# $package->unimplemented( "[$RFRM_Notebook_Tabname] handle_plugin_action($user_action,ITEM=$current_TreeItem)\n" );		

	} else {
		$package->unimplemented( "[$RFRM_Notebook_Tabname] handle_plugin_action($user_action)\n" );
	}

	$current_UserAction = $user_action;
	$package->debugmsg("Completed handle_plugin_action($user_action)");	

	
#		if( $args{-menuitem}=~/Define Maintenance Plans/ ) {
#			$package->handle_plugin_action("Define");
#		}elsif( $args{-menuitem}=~/Execute Maintenance Plans/ ) {
#			$package->handle_plugin_action("Execute");
#		}elsif( $args{-menuitem}=~/Execute Plan Operations/ ) {
#			$package->handle_plugin_action("Operations");
#		} else {
#			die "This Cant Happen Unless The Code Line is Screwed Up\n";
#		}
#

#	if( $user_action eq "Define" ) {
#		$package->user_action_define_plan();
#	} elsif( $user_action eq "Help" ) {
#		$package->user_action_help();
#	} elsif( $user_action eq "Logs" or $user_action eq "filetype" ) {
#		$package->user_action_logfiles($user_action);
#	} elsif( $user_action eq "Execute" ) {
#		$package->user_action_exec_plan();
#	} elsif( $user_action eq "Operations" ) {
#		$package->unimplemented("Operations");
#	} else {
#		die "handle_plugin_action($user_action) cant happen\n";
#	}

}

#sub user_action_define_plan {
#	my($package)=@_;
#	$package->statusmsg( "[$RFRM_Notebook_Tabname] user_action_define_plan(plan=$current_plan,svr=$current_server)\n" );
#	if( $current_UserAction eq "Define" and defined $current_nb_tab and Tk::Exists($current_nb_tab)) {
#		$package->statusmsg( "[$RFRM_Notebook_Tabname] refreshing plan tab but not recreating\n" );
#		main::update_planinfo($current_plan) if defined $current_plan;
#		return;
#	}
#
#	$save_button->packForget();
#	$current_nb_tab=undef;
#	$current_nb_tab=$current_nb->raised() if defined $current_nb;
#	$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);
#	$working_frame  = $current_main_frame->LabFrame(
#		-label=>"Current Plan = $current_plan Current Server = $current_server",
#		-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
#	$current_plan="/" unless defined $current_plan;
#	$current_nb=main::plantab_paint_notebook($current_plan,$working_frame);
#	if( defined $current_nb_tab) {
#		$current_nb->raise($current_nb_tab);
#	}
#	$save_button->pack( -side=>'bottom', -padx=>3, -pady=>8 );
#	$package->refresh_tree("Plans") if $current_TreeType ne "Plans";
#
#}
sub user_action_help {
	my($package)=@_;
	$package->statusmsg( "[$RFRM_Notebook_Tabname] user_action_help\n" );

#	$save_button->packForget();
#	$current_nb=undef;
	$package->show_html("http://www.edbarlow.com/document/sydump_readme.htm");

}
#sub user_action_exec_plan {
#	my($package)=@_;
#	$package->statusmsg( "[$RFRM_Notebook_Tabname] user_action_exec_plan()\n" );
#	$save_button->packForget();
#}

#sub user_action_logfiles  {
#	my($package,$user_action)=@_;
#	$package->statusmsg( "[$RFRM_Notebook_Tabname] user_action_logfiles()\n" );
#
#	$save_button->packForget();
#
#	$current_nb=undef;
#	$package->refresh_tree("Plans") if $current_TreeType ne "Plans";
#
#	$working_frame->destroy() if defined $working_frame and Tk::Exists($working_frame);
#	$working_frame=$current_main_frame->Frame()->pack( -fill=>'both', -side=>'top', -expand=>1);
#
#	my($log_frame)  = $working_frame->LabFrame(
#		-label=>"Current Plan = $current_plan Current Server = $current_server",
#		-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);
#
#	my($log_btns) = $log_frame->Frame()->pack( -fill=>'x', -side=>'top');
#
#	$package->make_button(-widget=>$log_btns,
#		-help=>'Errors',
#		-text=>"Errors",
#		-command=> sub {
#			$current_filetype = "errors";
#			$package->handle_plugin_action("filetype"); } )->pack( -side=>'left', -padx=>3);
#	$package->make_button(-widget=>$log_btns,
#		-help=>'Session Logs',
#		-text=>"Session Logs",
#		-command=> sub {
#			$current_filetype = "sessionlog";
#			$package->handle_plugin_action("filetype");
#			 } )->pack( -side=>'left' , -padx=>3);
#	$package->make_button(-widget=>$log_btns,
#		-help=>'DBCC',
#		-text=>"DBCC",
#		-command=> sub {
#			$current_filetype = "dbcc";
#			$package->handle_plugin_action("filetype") } )->pack( -side=>'left', -padx=>3 );
#	$package->make_button(-widget=>$log_btns,
#		-help=>'Audits',
#		-text=>"Audits",
#		-command=> sub {
#			$current_filetype = "audits";
#			$package->handle_plugin_action("filetype");
#		} )->pack( -side=>'left', -padx=>3 );
#	$package->make_button(-widget=>$log_btns,
#		-help=>'Refetch Plan Files',
#		-text=>"Refetch Plan Files",
#		-command=> sub {
#	#		@servers=undef;
#	#		%audits=undef;
#	#		%dbcc=undef;
#	#		%errors=undef;
#	#		%sessionlog=undef;
#			$package->handle_plugin_action("Execute");
#		 } )->pack( -side=>'left', -padx=>3 );
#
#	$log_btns->Checkbutton(
#		-variable=>\$show_all_files,
#		-text=>'Show All Files',
#		-command=>sub{ $package->handle_plugin_action("filetype"); }
#	)->pack( -side=>'left', -padx=>3 );
#
#	return if $user_action ne "filetype"; # or ! defined $current_server or ! defined $current_plan;
#
#	# prefetch if Server/Type not fetched
#
#	if( $current_filetype eq "audits"
#	or $current_filetype  eq "dbcc"
#	or $current_filetype  eq "errors"
#	or $current_filetype  eq "sessionlog" ) {
#		$package->do_dir( $current_filetype );
#		my($srchfrm)=$log_frame->Frame()->pack( -fill=>'x', -side=>'top', -padx=>5, -pady=>5);
#
#		$filename=undef;
#		my(@flist);
#		foreach ( sort keys %{$filedata{$current_filetype}} ) {
#			my(@d)=split(/\//,$_,3);
#			if( $show_all_files == 0 and defined $current_server ) { next if $d[0] ne $current_server; }
#			$filename=$_ unless defined $filename;
#			push @flist,$_;
#			#$be->insert("end",$_);
#			#$be->insert("end",$d[2]);
#		}
#
#		unshift @flist," "x64;
#		my($be)=$srchfrm->BrowseEntry(-variable=>\$filename, -choices=>\@flist,
#			-listwidth => 64, -width=>64,
#			#-autolistwidth=>1,
#			-browsecmd=> sub { $package->search() } )->pack(-side=>'left');
#		$be->bind("<Return>", sub { $package->search() } );
#
#		$be->focus();
#
#
#		#foreach ( keys %{$filedata{$current_filetype}} ) {
#		#	my($itemptr)= ${$filedata{$current_filetype}}{$_};
#		#	my(@d)=split(/\//,$_,3);
#		#	print $d[0]," ",$d[1]," ",$d[2]," ",$$itemptr{size}," ",$$itemptr{time},"\n";
#		#}
#
#		$package->make_button(-widget=>$srchfrm,
#   			-help=>'Fetch This Log File',
#			-text=>"Fetch",
#			-command=> sub { $package->search(); })->pack(-side=>'left');
#
#		$rotext=$log_frame->Scrolled('ROText',
#			-relief => "sunken",
#			-wrap=>'none',
#         		-bg=>'white',
#         		-font=>'code',
#         		-scrollbars => "osoe")->pack(-expand=>1,-fill=>'both');
#	}
#		$fetch_history{$current_server}=1;
#}



1;
