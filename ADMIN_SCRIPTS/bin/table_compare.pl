#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
my($curdir)=dirname($0);

sub usage
{
        print @_;
        print "table_compare.pl [-xh] -Ssrv [ -Uusr -Ppass -Ddb ] -ssrv [ -uusr -ppass -ddb ] -Ttbl -i1,2

                Compare Table on 2 servers

                -h htmlize output
                -i ignore columns (comma separated, starting at 1)
                -x debug mode \n";
        return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_i $opt_h $opt_D $opt_U $opt_S $opt_P $opt_d $opt_s $opt_u $opt_p $opt_x $opt_c $opt_T);
die usage("Bad Parameter List") unless getopts('xU:D:S:P:d:u:p:s:chi:T:');

my($NL)="\n";
$NL="<br>\n"            if defined $opt_h;

use DBIFunc;

die usage("")   unless defined $opt_U or $opt_P or $opt_S or $opt_D;
die usage("")   unless defined $opt_u or $opt_p or $opt_s or $opt_d;

$opt_d = $opt_D if( defined $opt_D and ! defined $opt_d );
$opt_D = $opt_d if( defined $opt_d and ! defined $opt_D );

$opt_s = $opt_S if( defined $opt_S and ! defined $opt_s );
$opt_S = $opt_s if( defined $opt_s and ! defined $opt_S );

$opt_p = $opt_P if( defined $opt_P and ! defined $opt_p );
$opt_P = $opt_p if( defined $opt_p and ! defined $opt_P );

$opt_u = $opt_U if( defined $opt_U and ! defined $opt_u );
$opt_U = $opt_u if( defined $opt_u and ! defined $opt_U );

die usage("Bad User Info -U\n")         unless defined $opt_U;
die usage("Bad Srv Info -S\n")  unless defined $opt_S;
die usage("Bad Pass Info -P\n")         unless defined $opt_P;
die usage("Bad User Info -u\n")         unless defined $opt_u;
die usage("Bad Srv Info -s\n")  unless defined $opt_s;
die usage("Bad Pass Info -p\n")         unless defined $opt_p;
die usage("Bad Db Info -D\n")   unless defined $opt_D;
die usage("Bad Db Info -d\n")   unless defined $opt_d;
die usage("Must Pass  Table With -T\n")         unless defined $opt_T;

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

print "<H1>" if defined $opt_h;
print "Table Comparison Utilitity v1.0\n";
print "</H1>" if defined $opt_h;
print "Comparing table $opt_T on $opt_s/$opt_d and $opt_S/$opt_d $NL";

dbi_set_web_page(1)      if defined $opt_h;

dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
                or die "Cant connect to $opt_S as $opt_U\n";

my(%ignore_cols);                                       # first column is 0 (data entered with 1)
my(@unique_key_colid)=();
my(@value_colid)=();
my(%unique_key_colid_h)=();
my(%colnames);                                                  # first column is 0
my($maxcolid)=0;

print "Getting Column Names",$NL if defined $opt_x;
foreach ( dbi_query(-db=>$opt_D, -query=>"select colid,name from syscolumns where id = object_id(\"$opt_T\")" )) {
                my($id,$nm)=dbi_decode_row($_);
                $maxcolid=$id if $id>$maxcolid;
                $id--;
                $colnames{$id}=$nm;
}

# Get columns to ignore
if( defined $opt_i ) {
        foreach ( split /,/, $opt_i ) {
                die "Ignore Columns a bad integer" if $_<1 or $_>200;
                $ignore_cols{$_-1}="Ignoring Column";
                print "Ignoring Column $_ : ",$colnames{$_-1},$NL
        }
}

print "Getting Unique Key Columns From $opt_S$NL" if defined $opt_x;

my($index_id)="";

foreach  ( db_query_orig($opt_D, "select indid from sysindexes i, sysobjects o where  o.id=i.id and o.name = \"$opt_T\" and o.uid=0 and i.status&2 = 2") ) {
        $index_id=$_;
}

die "ERROR: Table $opt_T in $opt_S / $opt_D has no unique index!\n"
        if $index_id eq "";

for (my $i=1 ; $i<=16; $i++ ) {
        foreach  ( dbi_query(-db=>$opt_D,
        		-query=>" select colid,name from syscolumns where  name = index_col(\"$opt_T\",$index_id,$i,0) and    id = object_id(\"$opt_T\")" )) {
                my($id,$nm)=dbi_decode_row($_);
                $id--;
                print "KEY COLUMN NUMBER: ",$id," Column Name = ",$colnames{$id},$NL;
                push @unique_key_colid,$id;
                $unique_key_colid_h{$id}=$nm;
        }
}

for (my $i=0 ; $i<$maxcolid; $i++ ) {
        push @value_colid,$i unless defined $ignore_cols{$i};
}


print "KEYS COLUMNS FOR THE SORT ARE (";
foreach ( @unique_key_colid ) {
        print $unique_key_colid_h{$_},",";
}
print ") ",$NL;

my %data_S = db_query_to_hash($opt_D,"select * from $opt_T"," ",\@unique_key_colid,\@value_colid);

dbi_disconnect();

dbi_connect(-srv=>$opt_s,-login=>$opt_u,-password=>$opt_p)
                or die "Cant connect to $opt_s as $opt_u\n";

my %data_s = db_query_to_hash($opt_d,"select * from $opt_T"," ",\@unique_key_colid,\@value_colid);

# ===================================
# Get differences in columns and mark
# ===================================
my($num_del)=0;
foreach (keys %data_s) {
        next unless defined $data_S{$_};
        if( $data_s{$_} eq $data_S{$_} ) {
                $num_del++;
                delete $data_s{$_};
                delete $data_S{$_};
        }
}
print "<h3>" if defined $opt_h;
print "Number of Identical Rows: $num_del\n";
print "</h3>" if defined $opt_h;
print "<h3>" if defined $opt_h;
print "Column Names:\n";
print "</h3>" if defined $opt_h;

print "<TABLE BORDER=1>\n" if defined $opt_h;
foreach ( keys %colnames ) {
        print "<TR><TD>Column Number ",($_+1),"</TD><TD>Name $colnames{$_}</TD><TD>",$ignore_cols{$_},"</TD></TR>\n";
}
print "</TABLE>\n" if defined $opt_h;

print "<h3>" if defined $opt_h;
print "ITEMS in $opt_s but not in $opt_S\n";
print "</h3>" if defined $opt_h;
print "<TABLE BORDER=1>\n" if defined $opt_h;
foreach (keys %data_s) {
        next if defined $data_S{$_};
        if( defined $opt_h ) {
                my @v=dbi_decode_row($data_s{$_});
                print "<TR><TD><b>$opt_s</b></TD><TD>",
                                join("</TD><TD>",@v),
                                "</TD></TR>\n";
        } else {
                print "\tKEY $_\n";
        }
}
print "</TABLE>\n" if defined $opt_h;
print "\n";

print "<h3>" if defined $opt_h;
print "ITEMS in $opt_S/$opt_D but not in $opt_s/$opt_d\n";
print "</h3>" if defined $opt_h;
print "<TABLE BORDER=1>\n" if defined $opt_h;
foreach (keys %data_S) {
        next if defined $data_s{$_};
        if( defined $opt_h ) {
                my @v=dbi_decode_row($data_S{$_});
                print "<TR><TD><b>$opt_S</b></TD><TD>",
                                join("</TD><TD>",@v),
                                "</TD></TR>\n";
        } else {
                print "\tKEY $_\n";
        }
}
print "</TABLE>\n" if defined $opt_h;
print "\n";

print "<h3>" if defined $opt_h;
print "ITEMS in Both Databases That Are Different\n";
print "</h3>" if defined $opt_h;
print "<TABLE BORDER=1>\n" if defined $opt_h;
foreach (keys %data_s) {
        next unless defined $data_S{$_};
        my @k=dbi_decode_row($_);
        my @v=dbi_decode_row($data_s{$_});
        my @V=dbi_decode_row($data_S{$_});

        if( defined $opt_h ) {
                # compute colors for items!

                print "<TR><TD COLSPAN=20><b>$_</b></TD></TR>\n";

                my($row1,$row2)=("<TR><TD></TD><TD>$opt_s</TD><TD>\n","<TR><TD></TD><TD>$opt_S</TD><TD>\n");
                for (my $i=0 ; $i<$maxcolid; $i++ ) {
                        if( $v[$i] ne $V[$i] ) {
                                $row1.="<FONT COLOR=BLUE><b>".$v[$i]."</TD><TD>\n";
                                $row2.="<FONT COLOR=BLUE><b>".$V[$i]."</TD><TD>\n";
                        } else {
                                $row1.=$v[$i]."</TD><TD>\n";
                                $row2.=$V[$i]."</TD><TD>\n";
                        }
                }
                print $row1,"</TD></TR>\n";
                print $row2,"</TD></TR>\n";
        } else {
                print "KEY $_\n";
                print "$opt_s>>> ",join(" ",@v),"\n";
                print "$opt_S>>> ",join(" ",@V),"\n";
        }
}
print "</TABLE>\n" if defined $opt_h;
