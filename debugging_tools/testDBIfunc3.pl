#!/usr/local/bin/perl-5.6.1

use lib qw(/apps/sybmon/dist/lib /apps/sybmon/dist/ADMIN_SCRIPTS/lib /apps/sybmon/dist/plugins /apps/sybmon/dist/cpan);

use strict;
use DBI;
use DBIFunc;
use MlpAlarm;
use Data::Dumper;

my($DEBUG);	# set to dbi trace level

my($server) 	= $MLPAlarm::server || "DEVNULL";
my($login) 	= $MLPAlarm::login || "gemalarms";
my($password) 	= $MLPAlarm::password || "gemalarms";
my($db) 	= $MLPAlarm::db || "gemalarms";
my($server_type)="Sybase";

print "Connecting to Alarm Server\n";

my($dbproc) = DBI->connect("dbi:$server_type:$server",$login,$password,{RaiseError=>0,PrintError=>0,AutoCommit=>1});
die "Cant Connect to Server $server as Login $login password $password db $db" unless defined $dbproc;
print "Connection Ok.\n";

$dbproc->{syb_err_handler}  = \&sybase_handler;
DBI->trace($DEBUG) if defined $DEBUG;

if(0) {
use vars qw(%GetInfoType);
use DBI::Const::GetInfoType;
print "THE FOLLOWING ARE DB INFO WITH GetInfoType\n";
foreach my $name (keys %GetInfoType) { 
	my($val) = $dbproc->get_info($GetInfoType{$name});
	print "$name ... $val\n";
}

print "THE FOLLOWING ARE DB INFO WITHOUT GetInfoType\n";
for my $i (1..300) { 
	my($val) = $dbproc->get_info($i);
	print "$i ... $val\n" if defined $val;
}

print "The Following are the Results of table_info\n";
my $sth = $dbproc->table_info('%','','');
my @rc;
push @rc,\@{$sth->{NAME}};
push @rc,@{$sth->fetchall_arrayref()};
foreach ( dbi_reformat_results(@rc) ) {
	print $_,"\n";
}

print "The Following are the Results of table_info(gemalarms)\n";
my $sth = $dbproc->table_info('gemalarms','%','%');
my @rc;
push @rc,\@{$sth->{NAME}};
push @rc,@{$sth->fetchall_arrayref()};
foreach ( dbi_reformat_results(@rc) ) {
	print $_,"\n";
}

print "The Following are the Results of column_info\n";
my $sth = $dbproc->column_info('gemalarms','%','%','%');
die "UNDEF RETURNED" unless defined $sth;
my @rc;
push @rc,\@{$sth->{NAME}};
push @rc,@{$sth->fetchall_arrayref()};
foreach ( dbi_reformat_results(@rc) ) {
	print $_,"\n";
}


my  $type_info_all = $dbproc->type_info_all;
print "Data Type Info\n";
print Dumper \$type_info_all;
my($hdr)=shift @$type_info_all;
my(%x)=reverse %$hdr;
my(@d);
foreach (sort { $a<=>$b } keys %x) { push @d,$x{$_}; }
unshift @$type_info_all, \@d;
foreach ( dbi_reformat_results(@$type_info_all) ) {
	print $_,"\n";
}
}
my(@type_info)=$dbproc->type_info();
print Dumper \@type_info;

print "The Following are the Results of tables\n";
my @tables = $dbproc->tables('%','','','');
print "Primary Key Info\n";
foreach my $tbl (@tables) {
	print "TABLES: ",$tbl,"\n";
	my $sth=$dbproc->primary_key_info( 'gemalarms','',$tbl );
	foreach (@{$sth->fetchall_arrayref()}) {
		print $_,"\n";
	}
}	

#my $sth = $dbproc->primary_key_info('gemalarms','dbo','sysobjects');
#my $sth = $dbproc->primary_key_info('gemalarms','','');
#foreach (@{$sth->fetchall_arrayref()}) {
	#print join(" ",@$_),"\n";
#}

