create procedure Event_proc (
	@monitor_program	varchar(30),
	@monitor_time	datetime 	=NULL,
	@system		varchar(30) 	,
	@subsystem	varchar(50) 	=null,
	@event_time	datetime 	=null,
	@severity	varchar(20)    	=null,
	@event_id	int		=null,
	@message_text	varchar(255)    =null,
	@message_value	int		=null,
	@internal_state	char(1)		=null
)
as
begin
	declare @my_severity int

	if exists ( select * from IgnoreList
		where isnull(monitor_program,@monitor_program)=@monitor_program
		and	isnull(system,'CANTHAPPENSTR')=isnull(@system  ,'CANTHAPPENSTR')
		and	isnull(subsystem,'CANTHAPPENSTR')=isnull(@subsystem  ,'CANTHAPPENSTR'))
	begin
		-- ignore this one
		return
	end

	if exists ( select * from GEM_Servers where blackedout_until is not null and blackedout_until>getdate() and system_name=isnull(@system,'CANTHAPPENSTR') )
	begin -- ignore this one
		return
	end

	if @event_time is null
		select @event_time=getdate()
	if @monitor_time is null
		select @monitor_time=getdate()
/*
	if exists ( select * from Notification
		where 	monitor_program=@monitor_program
		and	system=@system
		and	subsystem=@subsystem
		and     type='e' )
	begin

	end
*/
	if not exists ( select * from Event where
					monitor_program=@monitor_program
			and  	message_text=@message_text
			and	isnull(system,'CANTHAPPENSTR')=isnull(@system  ,'CANTHAPPENSTR')
			and	isnull(subsystem,'CANTHAPPENSTR')=isnull(@subsystem  ,'CANTHAPPENSTR')
			and   datediff(mi,event_time,@event_time)=0 )

      INSERT Event (
        	monitor_program, monitor_time, system, subsystem,
        	event_time, severity, event_id, message_text, message_value,
        	internal_state
      	) VALUES (
		@monitor_program, @monitor_time, @system, @subsystem,
		@event_time	, @severity, @event_id, @message_text, @message_value,
		@internal_state
      	)
end
go
