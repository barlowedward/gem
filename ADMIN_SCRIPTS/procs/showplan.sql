/* Procedure copyright(c) 2008 by Edward M Barlow */

/******************************************************************************
**
** Name        : sp__showplan
**
******************************************************************************/
:r database
go
:r dumpdb
go

if exists (select * from sysobjects
           where  name = "sp__showplan"
           and    type = "P")
   drop proc sp__showplan
go

create procedure sp__showplan
@spid smallint,
@batch_id int output,
@context_id int output,
@stmt_num int output
as
begin
    set role indirect_sa_role with passwd password on
    execute sp_showplan @spid,@batch_id,@context_id,@stmt_num
    set role indirect_sa_role off
end

return(0)

END
go
grant execute on sp__showplan  TO public
go
