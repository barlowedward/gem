#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use Repository;
use Sys::Hostname;
use CommonFunc;
use vars qw( $opt_S $opt_d $opt_A $MAXHOPS $MAXTIME);
use MlpAlarm;
use CommonHeader;

my($hostname)=hostname();

# Copyright (c) 2005 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

$| = 1;
sub usage {
	return "Usage: tracert.pl --MAXHOPS=hops --MAXTIME=secs --SYSTEM=server --BATCH_ID=key --DEBUG\n";
}
die usage("Bad Parameter List\n") unless GetOptions(
                "SYSTEM=s"      	=> \$opt_S,
                "BATCH_ID=s"    	=> \$opt_A,
                "MAXHOPS=s"     	=> \$MAXHOPS,
                "MAXTIME=s"     	=> \$MAXTIME,
                "DEBUG"     		=> \$opt_d );

$MAXHOPS=10 unless $MAXHOPS;
$MAXTIME=10 unless $MAXTIME;
my(@systemlist);

if( defined $opt_S ) {
	push @systemlist,$opt_S;
} else {
	@systemlist=get_password(-type=>"win32servers");
	push @systemlist,get_password(-type=>"unix");
}

print "tracert.pl: run at ".localtime(time)."\n";
print "Called by batch $opt_A\n" if $opt_A;
print "Run On $hostname as user=$ENV{USER}\n";
print "Run Time: ".localtime()."\n";

MlpBatchJobStart(-BATCH_ID=>$opt_A ) if defined $opt_A;

my(@dirs) = ("/usr/local/bin","/bin","/sbin","/usr/bin");
my($numok,$numfail)=(0,0);
foreach my $system (sort @systemlist) {
	chomp $system;
	my $cmd;
	if( $system =~ /\s/ ) {
		warn "ERROR - System $system contains a space!\n";
		next;
	}
	if( is_nt() ) {
		$cmd="tracert -h 10 -d -w 100 $system";
	} else {
		$cmd="traceroute $system 2>&1";
		foreach (@dirs) {
			next unless -x "$_/traceroute";
			$cmd="$_/traceroute $system 2>&1";
			last;
		}
	}

	print "executing $cmd ...\n" if $opt_d;
	printf "%20s ", $system unless defined $opt_d ;
	my($starttime)=time;
	my($numhops,$timetaken,$found_tc);
	open(CMD,$cmd." |") or die "FATAL ERROR Cant Run $cmd : $!\n"."Host name:".hostname()."\nTime=".localtime(time)."\nDirectories checked for traceroute: ".join(" ",@dirs)."\n";
	$timetaken=time-$starttime;
	print "TIME TAKEN 1 = $timetaken\n" if $opt_d;
	my(@rc);
	my($num_empty)=0;
	while(<CMD>) {
		chomp;chomp;
		print ">".$_ if $opt_d;
		push @rc,$_;
		if( /\* \* \*/ or /Request timed out/i ) { print "[no connect]\n"; last if $num_empty++ >= 3; }
	}
	close(CMD);
	print "TIME TAKEN 2 = ",(time-$starttime),"\n" if $opt_d;
	die "host ".hostname()." ".$rc[0] if $#rc==0 and $rc[0]=~/not found/;
	die "host ".hostname()." ".$rc[0] if $#rc==0 and $rc[0]=~/is not recognized as an inter/;
	printf "%20s ", $system if defined $opt_d ;
	my($ok)="TRUE";
	if( $#rc==0 and $rc[0]=~/unable to resolve target/i ) {
		$ok= "host ".hostname()." ".$rc[0];
	} else {
		foreach ( @rc ) {
			$found_tc="TRUE" if /Trace complete./;
			$ok=$_ if /Request timed out./i;
			$numhops++ if /^\s+\d+\s/;
		}
		print "TIME TAKEN 3 = ",(time-$starttime),"\n" if $opt_d;

		$ok="Request Timed Out\n".join("\n>>>",@rc) if $num_empty>=3;
		$ok="Trace Didnt Finish???\n".join("\n>>>",@rc) if is_nt() and ! $found_tc;
		$ok= "host ".hostname()." tracert $system had $numhops hops\n".join("\n>>>",@rc)
			if $ok eq "TRUE" and $numhops>$MAXHOPS;
		$ok= "host ".hostname()." tracert $system took $timetaken seconds\n".join("\n>>>",@rc)
			if $ok eq "TRUE" and $timetaken>$MAXTIME;
	}

	chomp $ok;
	chomp $ok;
	if( $ok eq "TRUE" ) {
		printf "OK!   hops=%3s time=%s\n",$numhops,$timetaken;
		$numok++;
		MlpHeartbeat(
			-monitor_program=>$opt_A,
			-system=>$system,
			-subsystem=>"ping",
			-message_text=>"ping succeeded in $numhops hops and $timetaken secs",
			-state=>'OK' ) if defined $opt_A;

	} else {
		printf "FAIL! %s\n",$ok;
		$numfail++;
		MlpHeartbeat(
			-monitor_program=>$opt_A,
			-system=>$system,
			-subsystem=>"ping",
			-message_text=>$ok,
			-state=>'ERROR' ) if defined $opt_A;

	}
}
print $numok," hosts are alive!\n";
print $numfail," hosts are dead!\n";

MlpBatchJobEnd() if defined $opt_A;

__END__

=head1 NAME

tracert.pl - trace route utility

=head2 USAGE

	Usage: tracert.pl --MAXHOPS=hops --MAXTIME=secs --SYSTEM=server --BATCH_ID=key --DEBUG

=head2 DESCRIPTION

Loops through your servers and prints the number of hops and the total time
it takes to get to them.  Handles unix and nt traceroute output.

Will alarm if there are problems.

It takes 5 seconds between attempts.   The trace aborts if it does not
get a response within 20 seconds.

=cut
