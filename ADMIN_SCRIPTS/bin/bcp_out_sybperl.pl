#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "bcp_out.pl [-dx] -UUSER -SSERVER -PPASS -eTBL,TBL -DDB_LIST -oOUTDIR Tbl Tbl Tbl

        Extracts data from a database

        -e exclude tables (separated by comma)
        -d debug mode

        DB_LIST may include wildcards.  If no TBLs defined, will bcp out
        all tables in the directory. OUTDIR is created if it does not exist.
";
        return "\n";
}

use strict;
no strict 'subs';
use File::Path;
use Getopt::Std;
use File::Basename;
use Sybase::DBlib;
require 'sybutil.pl';

$|=1;

use vars qw($opt_d $opt_U $opt_S $opt_P $opt_D $opt_o $opt_e);
die usage("Bad Parameter List") unless getopts('dU:D:o:S:P:e:');

# cd to the right directory
my($curdir)=dirname($0);

# IF ONLY HAVE opt_S get login & password from password file
if( defined $opt_S and ! defined $opt_U ) {
        use Repository;
   ($opt_U,$opt_P)=get_password(-type=>"sybase",-name=>$opt_S);
}

die usage("Must Pass Outdir with -o\n")
        unless defined $opt_o;
die usage("Outdir $opt_o not a directory\n")
        if -e $opt_o and ! -d $opt_o;
die usage("Must Pass Database\n")
        unless defined $opt_D;
die usage("Must pass sa password\n" )
        unless defined $opt_P;
die usage("Must pass server\n")
        unless defined $opt_S;
die usage("Must pass sa username\n" )
        unless defined $opt_U;

my(%exclude_file);
if( defined $opt_e ) {
        foreach (split /,/,$opt_e) {
                $exclude_file{$_}=1;
        }
}

#dblogin
# BCP_SETL
# dbopen
# bcp_init
#
# ---- bcp_sendrow
#      bcp_batch
# bcp_done
#
# host file
#   bcp_control
#   bcp_columns
#   bcp_colfmt
#   bcp_exec

#&dbmsghandle ("message_handler_mine");
#&dberrhandle ("error_handler_mine");
my $dbproc = new Sybase::DBlib $opt_U, $opt_P, $opt_S;
die "Can't connect to the $opt_S Sybase server as login $opt_U."
        unless defined $dbproc;

my($db)='master';
my($query)="select name from sysdatabases where name like \"$opt_D\"  and status & 0x1120 = 0 and status2&0x0030=0";
die "Cant Use Database $db" if $dbproc->dbuse($db) == 0;
$dbproc->dbcmd($query);
die "dbsqlexec() on $query failed\n" if $dbproc->dbsqlexec() == 0;

my(@dat)=();
my(@databaselist);
while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
        while ( @dat = $dbproc->dbnextrow()) {
                push @databaselist,@dat;
        }
}
die "No databases Found" if $#databaselist<0;

my($TABLE);
#  BCP_SETL(true);              -- only if want to bcp in
my($cnt)=1;
foreach $db (@databaselist) {
        print "WORKING ON DATABASE $db\n";

        mkpath( $opt_o, 1, 0777 ) unless -d $opt_o;

        #
        # GET PROCS
        #
        if( $#ARGV<0 ) {
                $query="select name from sysobjects where type = 'U' order by name";
        } else {
                $query="select name from sysobjects where type in ('U','V','S') and name in (";
                foreach (@ARGV) { $query.="\"$_\","; }
                chop $query;
                $query.=")";
        }
        # print "DBG: Db $db Proc $query\n";

        die "Cant Use Database $db" if $dbproc->dbuse($db) == 0;
        $dbproc->dbcmd($query);
        die "dbsqlexec() on $query failed\n" if $dbproc->dbsqlexec() == 0;

        my(@objlist)=();
        while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
        while ( @dat=$dbproc->dbnextrow()) {
                if( defined $exclude_file{$dat[0]} ) {
                        print "Excluding $_\n";
                        next;
                }
                push @objlist,$dat[0];
        }
        }

        foreach (@objlist) {

                if( defined $exclude_file{$_} ) {
                        print "Excluding $_\n";
                        next;
                }

                open(OUTFILE,">".$opt_o."/$_.bcp")
                        or die "Cant write to $opt_o/$_.bcp\n";
                my($count)=0;
                printf( "%7s %30s ",($cnt++)."/".($#objlist+1),$_ );

                $query="select * from $_\n";
                $dbproc->dbcmd($query);
                die "dbsqlexec() on $query failed\n" if $dbproc->dbsqlexec() == 0;

                while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
                while ( @dat=$dbproc->dbnextrow()) {
                        $count++;
                        s/\n/INSERT_RETURN_HERE/g;
                        print "." if $count%1000 == 0;
                        print OUTFILE join('~~',@dat),"\n";
                }
                }
                printf( " (%s rows saved)\n",$count );
                close OUTFILE;
        }
}

$dbproc->dbclose();
print "Bcp Completed Successfully\n";

__END__

=head1 NAME

bcp_out.pl - copy files out of a database

=head2 DESCRIPTION

Bcp all tables out of a database and into flat files
The files can be read with bcp_out.pl.
The files are in character mode with a delimeter of two tildas.

=head2 USAGE

bcp_out.pl [-dx] -UUSER -SSERVER -PPASS -DDB_LIST -oOUTDIR Tbl Tbl Tbl

      DB_LIST may include wildcards
      If no TBLs defined, will bcp out all tables in the directory
      OUTDIR is created if it does not exist
      -d debug mode

=cut
