: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

# CONFIGURATION UTILITY FOR EDS SYBASE TOOLS
# copyright (c) 1997-2011 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

use strict;
use Carp qw(confess);
use Cwd;
use Data::Dumper;

use Tk 8.0;
use Tk::NoteBook;
use Tk::DialogBox;
use Tk::Pane;
use Tk::Tree;
use Tk::LabFrame;
use Tk::ErrorDialog;
use Tk::ItemStyle;
use Sys::Hostname;

use CommonFunc;
use Repository;
use File::Basename;
use File::Copy;
use File::Find;
use File::Compare;
use Getopt::Long;
use MIME::Lite;
use Do_Time;
use RunCommand;
use Sybase::Release;
use DBIFunc;
use WebRegister;

my($application_starttime) = time;

my( %chklst_install_keys ) = (
	REGISTERED									=> "Registered",
	AGREE_WITH_LICENSE						=> "Agree With License",

	UNIX_CONFIGURE_MONITOR_SERVER			=> "Configure Unix Monitor",
	WIN32_CONFIGURE_MONITOR_SERVER		=> "Configure Win32 Monitor",
	WIN32_TEST_MAIL_INSTALL					=> "Verify Mail On Win32",
	UNIX_TEST_MAIL_INSTALL					=> "Verify Mail On Unix",
	INSTALL_ALARM_DATABASE					=> "Install Alarm Database",
	VALIDATE_ALARM_INSTALLATION			=> "Verify Alarm Database",

# 	POPULATE_LOCAL_CGI_BIN					=> "cgi-bin Reformat",
	COPY_CGI_BIN_TO_WEBSERVER				=> "Install cgi-bin",
	VERIFY_CGIURL_WORKS						=> "Auto Verify Web Server",

	INSTALLED									=> "Gem Is Installed",
) ;
#	VERIFIED										=> "Pass Verification Tests",

my( @q_chklst_install_keys ) = (
		"REGISTERED"								,
		"AGREE_WITH_LICENSE"						,

		"UNIX_CONFIGURE_MONITOR_SERVER"			,
		"WIN32_CONFIGURE_MONITOR_SERVER"		,
		"WIN32_TEST_MAIL_INSTALL"				,
		"UNIX_TEST_MAIL_INSTALL"				,
		"INSTALL_ALARM_DATABASE"				,
		"VALIDATE_ALARM_INSTALLATION"			,

# 		"POPULATE_LOCAL_CGI_BIN"				,
		"COPY_CGI_BIN_TO_WEBSERVER"			,
		"VERIFY_CGIURL_WORKS"						,
		"INSTALLED"									,
);

BEGIN {
	print "configure.pl: Generic Enterprise Manager Configuration Utility\n";
	print "Copyright (c) 2005-2011 by SQL Technologies\nAll Rights Reserved\n\n";

	print ("Testing Perl & Perl Library Setup\n");
	print ("   Your Perl version is $]\n");

	if( $] < 5.008001 and defined $ENV{PROCESSOR_LEVEL} ) {
		if( defined $^V and $^V!~/\s*$/) {
			die "Error : You must have perl 5.8.1 installed under Win32.  \nYou are running $^V \n";
		}
		my($x)=$];
		$x=~s/0+/\./g;
		$x=~s/\.\.+/\./g;
		die "Error : You must have perl 5.8.1 installed.  \nYou are running $x ($])\n";
	}

	if( ! defined $ENV{DISPLAY} and ! defined $ENV{PROCESSOR_LEVEL} ) {
		die "Error : DISPLAY environment variable not set!\n";
	}

	my(%badpackage)=();
	sub testit {
		my($pkg_arryref, $msgbad, $msgok)=@_;
		my $err;
		my $numbad=0;
		foreach my $str (@$pkg_arryref) {
			if ( eval "require $str" ) {
				$str->import();
			} else {
				my $err = $@;
				chomp $err;
				$numbad++;
				$badpackage{$str}=$err;
			}
		}

		if ($numbad ) {
			print $msgbad;
			foreach ( keys %badpackage) {
				print ("Package Did Not Load: $_ \n");
				if( defined $ENV{PROCESSOR_LEVEL} ) {
					print ("\tplease run: ppm install DBI\n")	if $_ eq "DBI";
					print ("\tplease run: ppm install DBD-ODBC\n") if $_ eq "DBD::ODBC";

					if(  $]<5.008 ) {
						print "\tplease run: ppm install Win32-TaskScheduler --location=http://taskscheduler.sourceforge.net/perl/\n"
							if $_ eq "Win32::TaskScheduler";
					} else {
						print "\tplease run: ppm install http://taskscheduler.sourceforge.net/perl58/Win32-TaskScheduler.ppd\n"
							if $_ eq "Win32::TaskScheduler";
					}
				}

				foreach ( split( /\n/,$badpackage{$_} )) {
					next if /^Can't locate /;
					print ("\t=> $_\n");
				}
			}

			print ("The package(s) listed above MUST be installed before you may proceed.\n\n");
			exit(1);
		} else {
			print ( $msgok );
		}
	}

	my @std_packages=qw(CGI Getopt::Std Carp DBM::Deep File::Copy File::Basename
			Sys::Hostname Data::Dumper File::Find File::Path LWP::Simple
			Socket Tk URI::Escape Log::Dispatch Log::Dispatch::Screen
			Log::Dispatch::File Fcntl Getopt::Long );
	my @dbi_packages=qw(DBI);
	if( defined $ENV{PROCESSOR_LEVEL}) {
		push @dbi_packages,"DBD::ODBC";
	} else {
		push @dbi_packages,"DBD::Sybase";
	}

	my @tk_packages=qw(Tk::HList Tk::Tree Tk::DialogBox Tk::ItemStyle Tk::NoteBook Tk::BrowseEntry Tk::Listbox
				Tk::ErrorDialog Tk::Menu Tk::ROText Tk::Scrollbar Tk::Pixmap Tk::Label Tk::Balloon );

	if( defined $ENV{PROCESSOR_LEVEL}) {
		push @dbi_packages,"Win32::TaskScheduler";
	}

	print ("   => Testing Perl Builtin Package Integrity\n      ");
	testit( \@std_packages,
		"\nERROR: At least one pre-requisite required package was not found!\nThese are required parts of perl and must be available\nContact your system administrator\n",
		"   OK: All Required Builtin Perl Packages Found!!\n" );

	print ("   => Testing Perl Database Connectivity Package Integrity\n      ");
	testit( \@dbi_packages,
		"\nERROR: Database Tests FAILED\n",
		"   OK: All Required Perl/DBI Packages Found!!\n" );

	if( ! defined $badpackage{DBI}) {
		if( $DBI::VERSION < 1.0 ) {
			print "   ERROR - DBI Version ($DBI::VERSION) Must be at 1.0 or later";
		} else {
			print "         OK: DBI Version is ($DBI::VERSION)\n";
		}
	}

	if( defined $ENV{PROCESSOR_LEVEL}) {
		print "   DBD::ODBC Version ($DBD::ODBC::VERSION) Should be at 1.0 or later"
			if $DBD::ODBC::VERSION < 1.0;
		print ("         OK: DBD::ODBC Version is ($DBD::ODBC::VERSION)\n");
	} else {
	   if( $DBD::Sybase::VERSION < 1.0 ) {
		   print ("   ***************************************\n");
		   print ("   * WARNING - DBD::Sybase Version ($DBD::Sybase::VERSION) Should Probably be at 1.0 or later\n");
		   print ("   * This will not stop your install but a more stable version is better\n");
		   print ("   ***************************************\n");
	   } else {
		   print ("         OK: DBD::Sybase Version is ($DBD::Sybase::VERSION)\n");
	   };
	}

	print ("   => Testing Perl/Tk Package Integrity\n   ");
	testit( \@tk_packages,
		"\nERROR: At least one Tk Package did not load found!\nThese are usually shipped with Tk - Is your version recent?\nYour Perl Tk Version is $Tk::VERSION",
		"   OK: All Required Perl/Tk Packages Found!!\n" );
	if( $Tk::VERSION < 800.000 ) {
		   print ("   ***************************************\n");
		   print ("   * ERROR - Tk Version ($Tk::VERSION) MUST be 8.24 or later\n");
		   print ("   ***************************************\n");
	} elsif( $Tk::VERSION < 800.024 ) {
		   print ("   ***************************************\n");
		   print ("   * WARNING - Tk Version ($Tk::VERSION) Should Probably be at 8.24 or later\n");
		   print ("   * This will not stop your install but a more stable version is better\n");
		   print ("   ***************************************\n");
	} else {
		   print ("   OK: Tk Version is ($Tk::VERSION)\n" );
	}
	print "\n";
}

our($status_bar_ready)="FALSE";
our($GemConfig,%CURRENT_CONFIG,$cur_hostname,$print_stdout,%server_info,
		$DEBUG,$statusmsgbar,$mf,		$DO_DUMP_XML,$gem_data,@perl_includes);
our(%graphics_object);			# graphics_objects - for colorization and show/hide
our($configure_tab_hosttype)="all";



my($welcome_tab_text);

my($real_scheduled_jobs);				# from your jobs scheduler
my($schedule_page)="Normal";			# "Normal" or "Backups"
# my($scheduler_act,$scheduler_pass);	# set it first time you do an account
my($connecttab_redraw_frame,$proctab_redraw_frame);
my($scheduler);							# this is the scheduler object - needed for page refreshes


#use vars qw/%BackupPlanNames/;

$print_stdout="TRUE";					# Print to stdout - this is diagnostic mode

$|=1;

sub usage
{
   print @_;
   print "configure.pl - Enterprise Data Manager Installer.\n";

   print "  --UPGRADE		(upgrades and exits)\n";
   print "  --UPGRADEALARMS	(upgrades and exits)\n";
   print "  --QUIET - dont print stuff to stdout\n";
   print "  --LOGFILE   specify a log file\n";
   print "  --NOQUIET - do print stuff to stdout (overrides --QUIET)\n";

   print "  --NOLOGFILE - dont print to log file\n";
   print "  --DEBUG \n";

   exit();
}

$CURRENT_CONFIG{program_name}="configure.pl";
$CURRENT_CONFIG{gem_root_directory}		= get_gem_root_dir();

if( ! -d "$CURRENT_CONFIG{gem_root_directory}/logs" ) {
	my($rc)=mkdir( "$CURRENT_CONFIG{gem_root_directory}/logs", 0755 );
	if( ! $rc ) {
			die "Cant mkdir $CURRENT_CONFIG{gem_root_directory}/logs : $!\n";
	}
}

die "Cant write into directory $CURRENT_CONFIG{gem_root_directory}/logs\n"
	unless -w "$CURRENT_CONFIG{gem_root_directory}/logs";

# write log record
my($lwrcmd)="> $CURRENT_CONFIG{gem_root_directory}/logs/".hostname()."_installhist.log";
$lwrcmd=">".$lwrcmd if -w "$CURRENT_CONFIG{gem_root_directory}/logs/installhist.log";
my($rc)=open( LOGREC,$lwrcmd);
if( ! $rc ) {
	warn "Cant write $CURRENT_CONFIG{gem_root_directory}/logs/installhist.log $!";
} else {
	print   LOGREC "GemInstall\t",	scalar(localtime(time)),"\t", 	hostname(),"\n";
	close(LOGREC);
}

print "Root Directory: $CURRENT_CONFIG{gem_root_directory}\n";
$CURRENT_CONFIG{gem_configuration_directory}   = $CURRENT_CONFIG{gem_root_directory}."/conf";

die "Directory Structure is invalid - no main subdirectory\n"	unless -d "$CURRENT_CONFIG{gem_root_directory}";
die "Directory Structure is invalid - no lib subdirectory\n"	unless -d "$CURRENT_CONFIG{gem_root_directory}/lib";
unlink $CURRENT_CONFIG{gem_root_directory}."/admin/ftp_it.ksh"	if -r $CURRENT_CONFIG{gem_root_directory}."/admin/ftp_it.ksh";

validate_directory_structure(0);

#die "Cant Use Gem Functions.pl : not found\n"
#	unless -r $CURRENT_CONFIG{gem_root_directory}."/lib/GemFunctions.pl";
#require $CURRENT_CONFIG{gem_root_directory}."/lib/GemFunctions.pl";

$CURRENT_CONFIG{log_file}   = $CURRENT_CONFIG{gem_root_directory}."/logs/".hostname()."_configure.log";

# do a basic sanity test
die "Directory Structure is invalid - no conf subdirectory\n"
	unless -d "$CURRENT_CONFIG{gem_root_directory}/conf";
die "Directory Structure is invalid - can not write conf subdirectory\n"
	unless -w "$CURRENT_CONFIG{gem_root_directory}/conf";
die "Directory Structure is invalid - no data subdirectory\n"
	unless -d "$CURRENT_CONFIG{gem_root_directory}/data";
die "Directory Structure is invalid - can not write data subdirectory\n"
	unless -w "$CURRENT_CONFIG{gem_root_directory}/data";

use vars qw( $opt_q $UPGRADE $UPGRADEALARMS $NOLOGFILE $NOQUIET $LOGFILE $REGISTER_AS_MONSRV $SHOWTIMINGS);
GetOptions(
	"debug" 				=> \$DEBUG,
	"quiet"				=>\$opt_q,
	"LOGFILE=s"			=>\$LOGFILE,
	"NOQUIET"			=>\$NOQUIET,
	"REGISTER_AS_MONSRV"=>\$REGISTER_AS_MONSRV,
	"upgrade"			=>\$UPGRADE,
	"timings"			=>\$SHOWTIMINGS,
	"NOLOGFILE"			=>\$NOLOGFILE,
	"UPGRADEALARMS"	=>\$UPGRADEALARMS)
		or usage( "syntax error\n" );
$REGISTER_AS_MONSRV=1;

undef $opt_q if $NOQUIET;
if( ! defined $NOLOGFILE ) {
	$CURRENT_CONFIG{log_file}=$LOGFILE if $LOGFILE;
	archive_file($CURRENT_CONFIG{log_file},5,1) if -r $CURRENT_CONFIG{log_file};
	print "Installation Log File: $CURRENT_CONFIG{log_file}\n";
	open( LOGFP,">$CURRENT_CONFIG{log_file}") or die "Cant Write Log File $CURRENT_CONFIG{log_file} : $!\n";
	print LOGFP "configure.pl started at ".localtime(time)."\n";
	print LOGFP " perl=$^X\n";
	print LOGFP " hostname=".hostname()."\n";
}

print "Running in Quiet Mode - Supressing Status Messages\n" if $opt_q;
read_all_configfiles("FULL");
_statusmsg( "[configure] \n");
_statusmsg( "[configure] GEM Configure Version: $CURRENT_CONFIG{VERSION}\n");
_statusmsg( "[configure] Start Time: ",scalar localtime(time),"\n");
_statusmsg( "[configure] Configuration Files Loaded - initializing\n");

#################
#
# Global Vars - TODO: Sadly v1 used a lot of these and i would need to rearchitect to get rid of em :
#
#################

my $GOD_MODE;			# well really developer mode - TODO: probably need to encrypt
our $NUM_STATUS_LINES=5; 	# number of lines on status line - constant
my($Bbase);

$print_stdout="TRUE"  if defined $DEBUG;
$print_stdout="FALSE" if defined $opt_q;

_debugmsg( "[configure] Creating MainWindow\n");
$CURRENT_CONFIG{mainWindow} = MainWindow->new(-title=>"GEM Configuration Utility : $CURRENT_CONFIG{VERSION}");
$CURRENT_CONFIG{mainWindow}->scaling(2) if $CURRENT_CONFIG{mainWindow}->scaling() > 2;

#
# Main Window is divided into 3 Frames
#
my $top   = $CURRENT_CONFIG{mainWindow}->Frame()->pack(qw/-side top -fill x/);
$mf    	  = $CURRENT_CONFIG{mainWindow}->NoteBook();
my $bot   = $CURRENT_CONFIG{mainWindow}->Frame()->pack(qw/-side bottom -fill x/);

_debugmsg( "[configure] Creating Fonts\n" );
my(@fontfamilies)=sort $CURRENT_CONFIG{mainWindow}->fontFamilies();
my($family);
foreach( @fontfamilies ) {	# remember this is alpha order
	if( lc($_) eq 'arial'     ) {	$family=$_; last;	}
	if( lc($_) eq 'helvetica' ) {	$family=$_; last;	}
	if( lc($_) eq 'symbol'    ) {	$family=$_; last;	}
}
if( ! defined $family ) {
 	warn "YOUR TERMINAL DOES NO SUPPORT APPROPRIATE FONTS - SETTING FONT TO * and HOPING IT WORKS\n";
 	$family="*";
}

$CURRENT_CONFIG{mainWindow}->fontCreate(	'header', 	-family=>$family, 	-size=>'12', -weight=>'bold');
$CURRENT_CONFIG{mainWindow}->fontCreate(	'large', 	-family=>$family, 	-size=>'12' );
$CURRENT_CONFIG{mainWindow}->fontCreate(	'bold', 		-family=>$family, 	-size=>'10', -weight=>'bold');
$CURRENT_CONFIG{mainWindow}->fontCreate(	'code', 		-family=>'Courier new', 	-size=>'10');
$CURRENT_CONFIG{mainWindow}->fontCreate(	'norm', 		-family=>$family, 	-size=>'10');
$CURRENT_CONFIG{mainWindow}->fontCreate(	'small', 	-family=>$family, 	-size=>'9');
$CURRENT_CONFIG{mainWindow}->fontCreate(	'smbold', 	-family=>$family, 	-size=>'9', -weight=>'bold');
$CURRENT_CONFIG{mainWindow}->fontCreate(	'tiny', 		-family=>$family, 	-size=>'6');

_debugmsg( "[configure] Done Creating Fonts - Scaling is ",$CURRENT_CONFIG{mainWindow}->scaling(),"\n" );

$top->Label(-font=>"header",-text=>=>"Generic Enterprise Manager $CURRENT_CONFIG{VERSION} Copyright (c) 1995-2011 By SQL Technologies", -relief=>"groove", -bg=>'beige')->pack(-side => "top", -pady=>2, -fill=>'x');

$statusmsgbar= $bot->Label(-height=>$NUM_STATUS_LINES,-justify=>'left',-text=>"")->pack(-side=>'left');
set_status_label( undef, "GEM Configuration Utility");

_debugmsg( "[configure] Past Initial Build\n" );

#dohelp(	$bot->Button(-text=>"Quit",-command=>\&exit_and_quit, -bg=>'white')->pack(-side => "right", -pady=>2, -padx=>2),
#	"Quit application without saving any of the configuration changes you have made");

dohelp(	$bot->Button(-text=>"Quit",-command=>sub {
				btn_save_config_changes(undef,"FALSE");
				exit_and_quit();
			},
			-bg=>'white')->pack(-side => "right", -pady=>2, -padx=>2),
	"Quit application and save configuration changes");

#$graphics_object{SAVE_CONFIGURATION_CHANGES}=$bot->Button(-text=>"Save Configuration Changes",
#		-command=>sub {
#			#return if is_busy();
#			btn_save_config_changes();
#		}, -bg=>'white')->pack(-side => "right", -pady=>2, -padx=>2);
#dohelp(	$graphics_object{SAVE_CONFIGURATION_CHANGES},
#	"Saving the configuration changes you have made.\nThis does not mean you have installed successfully\nYou need to complete the Install Software tab for that.");

dohelp( $bot->Button(-text=>"GEM Help",
		-command=>sub {
			_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
			return if is_busy();
			gemhelp();
		}, -bg=>"white")->pack(-side => "right", -pady=>2, -padx=>2),
	"Show GEM Online Help For the Current Page");

_debugmsg("[setup] Adding NoteBook Tabs\n");
notebook_tab_setup($mf);

_debugmsg( "[setup] Setting Interface Object Colors based on history\n" );
redraw_productset();
redraw_installtype();
redraw_alarms_svtype();
redraw_webserver();
# redraw_console_pub();

_statusmsg( " " );
_statusmsg( "Initialization Completed - Starting Main TK Loop\n" );
_statusmsg( " " );
$status_bar_ready="TRUE";

MainLoop;
chmod( 0777, $CURRENT_CONFIG{log_file} ) or die "Cant Chmod $CURRENT_CONFIG{log_file}\n";

die "The Installer Program Has Completed\n";

###################################################################################################################

sub gemhelp {
	my($curtab)=$mf->raised();
	my($optlen, $maxlen)=(75,85);

	my($file)=$CURRENT_CONFIG{gem_root_directory}."/doc/onlinehelp/";
	my $displaytab = $curtab;
	my($errmsg)='';
	if( $curtab eq 'Welcome' ) {				$file.="welcome.pod";
	} elsif( $curtab eq 'Register' ) {		$file.="register.pod";
	} elsif( $curtab eq 'Monitors' ) {		$file.="monitors.pod";
	} elsif( $curtab eq 'Paths' ) {			$file.="paths.pod";
	} elsif( $curtab eq 'Products' ){		$file.="mail.pod";
	} elsif( $curtab eq 'Database' ) {		$file.="alarms.pod";
	} elsif( $curtab eq 'Web Server' ) {	$file.="webserver.pod";
	} elsif( $curtab eq 'Vault' ) {			$file.="vault.pod";
	} elsif( $curtab eq 'Console' ) {		$file.="console.pod";   $displaytab = "Console Publishing";
	} elsif( $curtab eq 'Server' ) {			$file.="servers.pod";	$displaytab = "Servers";
	} elsif( $curtab eq 'nb_connections' ) {	$file.="connect.pod";	$displaytab = "Connections";
	} elsif( $curtab eq 'nb_ProcLibInstall' ) {	$file.="ProcLibInstall.pod";	$displaytab = "Procs";
	} elsif( $curtab eq 'File' ) {			$file.="files.pod";		$displaytab = "Files";

	} elsif( $curtab eq 'VerifySoftware' ) {		$file.="VerifySoftware.pod";
	} elsif( $curtab eq 'Install' ) {		$file.="InstallSoftware.pod";
	} elsif( $curtab eq 'Validate' ) {		$file.="PostInstall.pod";
	} elsif( $curtab eq 'Scheduler' ) {		$file.="scheduler.pod";
	# } elsif( $curtab eq 'Backups' ) {		$file.="backups.pod";
	# } elsif( $curtab eq 'Reports' ) {		$file.="reports.pod";
	# } elsif( $curtab eq 'BatchJobs' ) {		$file.="BatchJobs.pod";
	} elsif( $curtab eq 'Status' ) {		$file.="status.pod";

	} else {
			$errmsg="Unknown Tab $curtab For Online Help\n";
	}

	if( $errmsg ) {
	} elsif( ! -e $file ) {
		$errmsg = "ONLINE HELP FILE $file is not found\n" ;
	} elsif( ! -r $file ) {
		$errmsg = "ONLINE HELP FILE $file is not readable\n";
	}
	if( $errmsg ) {
		_statusmsg("[configure] Displaying message box - select a button to continue\n");
		$CURRENT_CONFIG{mainWindow}->messageBox( -title => "Validation Error", -message => $errmsg, -type => "OK" );
		_statusmsg("[ready] GEM Ready / Continuing...\n");
		return;
	}

	_statusmsg("[help] Help Message For The $displaytab Tab\n");

	my $d = $CURRENT_CONFIG{mainWindow}->DialogBox(-title=>"Help for the GEM $displaytab Tab", -buttons=>["OK"]);
	$d->add('Frame')->grid;
	my $frame = $d->Subwidget('frame');

	$frame->Label(-font=>"large",-text=>"Help For The $displaytab Tab", -justify=>'left', -bg=>'beige',
		-relief=>'groove')->pack(-side=>'top', -expand=>1, -fill=>'x');

	my($textObject) = $frame->Scrolled("ROText",
			-relief => "sunken",
			-wrap=>'none',
			-bg=>'white',
			-scrollbars => "osoe")->pack(-side=>'bottom');

	$textObject->tagConfigure('listitem', -font=>'norm', -foreground=>'blue', -font=>'bold');
	$textObject->tagConfigure('normal', -font=>'norm');
	$textObject->tagConfigure('bold', -font=>'bold');

	open(FILE92, $file) or l_die( "Cant open online help file $file $!\n" );
	my($toprint)="";
	my($lastwasblank)="FALSE";

	while(<FILE92>) {
		chomp;
		if( /^=head1/ ) {
			chomp;
			s/^=head1\s*//;
		#	$textObject->insert("end",$_."\n","header");
			$lastwasblank="FALSE";

		} elsif ( /^=over/ ) {
		} elsif ( /^=back/ ) {
		} elsif ( /^\s*$/ ) {

			my($v)=reword($toprint,$optlen, $maxlen);
			$v=~s/~//g;

			if( $v =~ /^PURPOSE:/ ) {
				$textObject->insert("end",$v."\n","bold") if $toprint ne "";
				$textObject->insert("end","\n") unless $lastwasblank eq "TRUE";
			} else {
				$textObject->insert("end",$v."\n","normal") if $toprint ne "";
				$textObject->insert("end","\n") unless $lastwasblank eq "TRUE";
			}
			$toprint="";
			$lastwasblank="TRUE";

		} elsif( /=item \*/ ) {
			s/=item \* /*/;
			$_.=" ";

			my($listitemlength)=length($_) * 1.1;

			$textObject->insert("end",$_,'listitem');
			# im using ~ as a proxy to trick reword so that it can allign the
			# stuff (including list item) correctly.
			for( 1..$listitemlength) { $toprint.="~"; }
			$lastwasblank="FALSE";
		} else {
			$toprint.=$_." ";
			$toprint=~s/\s+$/ /;
			$lastwasblank="FALSE";
		}
	}
	my($v)=reword($toprint,$optlen, $maxlen);
	$v=~s/~//g;
	$textObject->insert("end",$v."\n","normal");
	close(FILE92);
	$d->Show();
}

sub btn_save_config_changes {
	my($nomsgbox,$refresh_GemConfig)=@_;
	busy();
	
	die "WOAH - NO refresh_GemConfig\n" unless $refresh_GemConfig;

	if( ! defined $NOLOGFILE ) {
		print LOGFP "Saving Configuration Changes And Reopening This Log File\n";
		close(LOGFP);
		chmod( 0666, $CURRENT_CONFIG{log_file} );
		open( LOGFP,">> $CURRENT_CONFIG{log_file}") or die "Cant Open Log File $CURRENT_CONFIG{log_file} : $!\n";
	}

	# set_install_step("SAVE_CONFIGURATION_CHANGES");
	$GemConfig->{NT_PERL_LOCATION}		=~ s.\\.\/.g if defined $GemConfig->{NT_PERL_LOCATION};
	$GemConfig->{NT_CODE_LOCATION}		=~ s.\\.\/.g if defined $GemConfig->{NT_CODE_LOCATION};
	$GemConfig->{UNIX_PERL_LOCATION}		=~ s.\\.\/.g if defined $GemConfig->{UNIX_PERL_LOCATION};
	$GemConfig->{UNIX_CODE_LOCATION} 	=~ s.\\.\/.g if defined $GemConfig->{UNIX_CODE_LOCATION};
	$GemConfig->{CODE_LOCATION_ALT1} 	=~ s.\\.\/.g if defined $GemConfig->{CODE_LOCATION_ALT1};
	$GemConfig->{CODE_LOCATION_ALT2} 	=~ s.\\.\/.g if defined $GemConfig->{CODE_LOCATION_ALT2};

	for my $i (1..9) {
		next unless $GemConfig->{"SYBASE".$i};
		$GemConfig->{"SYBASE".$i} 		=~ s/\s+$//;
		$GemConfig->{"SYBASE".$i}		=~ s.\\.\/.g;
	}

	_statusmsg("[save] \n" );
	_statusmsg("[save] Saving All Configfiles That Have Changed\n" );
	_statusmsg("[save] \n" );

	$GemConfig->{GEM_VERSION}	   = $CURRENT_CONFIG{VERSION};
	$GemConfig->{GEM_SHT_VERSION} = $CURRENT_CONFIG{sht_version};

	update_begin_block("$CURRENT_CONFIG{gem_root_directory}/lib/CommonHeader.pm");
	update_begin_block("$CURRENT_CONFIG{gem_root_directory}/lib/DBIFunc.pm");

	$GemConfig = write_gemconfig(-refresh_GemConfig=>$refresh_GemConfig);
	write_password_vault();

	if( ! defined $nomsgbox ) {
		$CURRENT_CONFIG{mainWindow}->messageBox( -title => 'Done Configuration',
					-message => 'Configuration values saved.',
					-type => "OK" ) unless $UPGRADE;
		_statusmsg("[ready] GEM Ready / Continuing...\n");
	}

	_statusmsg( "[save] Configuration Values Saved\n" );

	# $graphics_object{SAVE_CONFIGURATION_CHANGES}->configure(-bg=>'white');
	unbusy();
}

sub exit_and_quit
{
	print "\nExiting Configuration Program\n";
	close_gemconfig();
	if( ! defined $NOLOGFILE ) {
		print LOGFP "\nExiting Configuration Program Without Saving Anything\n";
		close(LOGFP);
		chmod( 0666, $CURRENT_CONFIG{log_file} ) or l_die("Cant Chmod $CURRENT_CONFIG{log_file}\n");
	}
	exit(0);
}

#
# Set up The notebook Middle
#
sub notebook_tab_setup {
      my($mf)=@_;
      my($to_state)='disabled';
      $to_state="normal" if $GOD_MODE or get_install_variable("AGREE_WITH_LICENSE");

		my( $tabW,$tabR,$tabP,$tabB,$tabWebServer,$tabVerify,$tabInstallState,$tabD,$tabPr,$tabnb_ProcLibInstall,$tab_database,$tabS,$tabF,$tabV,$tabL,$tabMonitors,$tabI,$tabSc,$tabReports,$tabBatchJobs);

      $tabW = $mf->add('Welcome', -label=>'Welcome',  -underline=>0,
      		#-raisecmd=>sub {$next_btn->configure(-text=>'>>> NEXT >>>');	}
      		);
      $tabR = $mf->add('Register', -label=>'Register',  -underline=>0,      		-state=>$to_state);
      $tabL = $mf->add('Paths',-label=>'Paths', -underline=>0,     					-state=>$to_state);
      my($tabVault) = $mf->add('Vault',-label=>'Vault', -underline=>0,     		-state=>$to_state);
      $tabP = $mf->add('Products',-label=>'Mail', -underline=>0,		      		-state=>$to_state);
      $tab_database = $mf->add('Database',-label=>'Database', -underline=>0,						-state=>$to_state);
		$tabWebServer = $mf->add('Web Server',-label=>'Web Server', -underline=>0,	-state=>$to_state);
		# $tabD = $mf->add('Console',-label=>'Publishing', -underline=>0,					-state=>$to_state);

		$tabMonitors = $mf->add('Monitors',-label=>'Monitors', -underline=>0,  		-state=>$to_state);
      # $tabVerify = $mf->add('VerifySoftware',-label=>'Verify Software', -underline=>0, -state=>$to_state  );
      $tabI = $mf->add('Install',-label=>'Install Software', -underline=>0, -state=>$to_state     );
      
      $tabInstallState = $mf->add('Status',-label=>'Install Status', -underline=>0, -state=>'normal', -raisecmd => sub { repaint_installstatus_tab(); });

		_statusmsg( "[configure] Welcome Tab \n" );
      setup_welcome_tab($tabW);

      _statusmsg( "[configure] Register Tab\n" );
      setup_register_tab($tabR);

      _statusmsg( "[configure] Monitors Tab \n" );
      setup_monitors_tab($tabMonitors);

      _statusmsg( "[configure] Paths Tab \n" );
      setup_locations_tab($tabL);

      _statusmsg( "[configure] Password Vault Tab \n" );
      setup_password_vault_tab($tabVault);

      _statusmsg( "[configure] Mail Tab \n" );
      setup_mail_tab($tabP);

      #statusmsg( "[configure] Console Publishing Tab \n" );
      #setup_consolepublishing_tab($tabD);

      _statusmsg( "[configure] Database Tab \n" );
      setup_alarms_tab($tab_database);

      _statusmsg( "[configure] Web Server Tab \n" );
      setup_webserver_tab($tabWebServer);

      # _statusmsg( "[configure] Verify Install Tab \n" );
      # setup_verify_tab($tabVerify);

      _statusmsg( "[configure] Install Software Tab \n" );
      setup_install_software_tab($tabI);

      _statusmsg( "[configure] Install Status Tab \n" );
      setup_installstatus_tab($tabInstallState);

      set_install_major_step('SETUP');

      $mf->pack(-expand=>1, -fill=>'both');
      _statusmsg( "[configure] Completed Drawing Tabs \n" );
}

my($validate_is_initialized);
sub validate_directory_structure {
	($validate_is_initialized)=@_;

	umask 0000;

	#
	# Create Directories If Needed
	#
	# fix the directory structure if necessary - winzip apparently does not expand null files
	sub md {
		my($dirtomake)=@_;
		chomp($dirtomake);		# probably not necessary but once i had a data\r directory created on win32... bah
		if( -d $dirtomake ) {
			warn "WARNING: Directory $dirtomake is not readable\n" unless -r $dirtomake;
			warn "WARNING: Directory $dirtomake is not writable\n" unless -w $dirtomake;
			if( $dirtomake =~ /\/data\// ) {
				chmod 0777,$dirtomake;
			} else {
				chmod 0770,$dirtomake;
			}
			return;
		}
		if( $dirtomake =~ /\/data\// ) {
			my($rc)=mkdir( $dirtomake, 0777 );
			if( ! $rc ) {
				return( l_warning("Cant mkdir $dirtomake : $!\n") ) if $validate_is_initialized;
				die "Cant mkdir $dirtomake : $!\n";
			}
			chmod( 0777, $dirtomake );
		} else {
			my($rc)=mkdir( $dirtomake, 0755 );
			if( ! $rc ) {
				return l_warning("Cant mkdir $dirtomake : $!\n") if $validate_is_initialized;
				die "Cant mkdir $dirtomake : $!\n";
			}
			chmod( 0770, $dirtomake );
		}
	}

	my(@dirs)= qw?conf data ADMIN_SCRIPTS doc plugins Win32_perl_lib_5_8 bin logs win32_batch_scripts
			win32_batch_scripts/interactive win32_batch_scripts/batch win32_batch_scripts/plan_interactive debugging_tools working
			win32_batch_scripts/plan_batch data lib unix_batch_scripts unix_batch_scripts/interactive conf/saved_files
			unix_batch_scripts/batch unix_batch_scripts/plan_interactive unix_batch_scripts/plan_batch build_tools?;

	my(@data_dirs)=qw?BACKUP_LOGS server_audits cronlogs misc batchjob_logs logs CONSOLE_REPORTS depends patern_files
		html_output system_information_data misc CONSOLE_REPORTS/byserver CONSOLE_REPORTS/details
		CONSOLE_REPORTS/gifs CONSOLE_REPORTS/backup_errors CONSOLE_REPORTS/sessionlogs
		CONSOLE_REPORTS/GEM_BATCHJOB_LOGS CONSOLE_REPORTS/dbcclogs CONSOLE_REPORTS/details  CONSOLE_REPORTS/html_output
		system_information_data/schedlgu system_information_data/hosts server_audits
		lockfiles GEM_BATCHJOB_LOGS?;


	my($num_err)=0;
	foreach (@dirs) {
		chomp;
		md("$CURRENT_CONFIG{gem_root_directory}/$_");
		$num_err++ unless -r "$CURRENT_CONFIG{gem_root_directory}/$_";
		$num_err++ unless -w "$CURRENT_CONFIG{gem_root_directory}/$_";
	}

	die "Cant write into directory $CURRENT_CONFIG{gem_root_directory}/data\n"
		unless -w "$CURRENT_CONFIG{gem_root_directory}/data";

	foreach (@data_dirs) {
		chomp;
		md("$CURRENT_CONFIG{gem_root_directory}/data/$_");
		$num_err++ unless -r "$CURRENT_CONFIG{gem_root_directory}/data/$_";
		$num_err++ unless -w "$CURRENT_CONFIG{gem_root_directory}/data/$_";
	}

	if( $num_err ) {
		print "************************************************************************\n";
		print "* ONE OR MORE DIRECTORIES ARE NOT OK - HIT ANY KEY TO CONTINUE          \n";
		print "************************************************************************\n";
		<STDIN>;
	}

	$CURRENT_CONFIG{VALIDATE_FILE}	= time unless defined $CURRENT_CONFIG{VALIDATE_FILE};
	return "";
}

sub get_perl {
	my($perl)=$^X;
	if( $^X eq "perl" or $^X eq "perl.exe") {
			my($path)=$ENV{"PATH"};
			if( $path =~ /[CD]:/ )  {
				# NT
				foreach( split /;/,$path ) {
						next unless -x $_."/perl.exe";
						s.\\./.g;
						$perl=$_."/perl.exe";
						last;
				}
			} else {
				# UNIX
				foreach( split /:/,$path ) {
						next unless -x $_."/perl";
						$perl=$_."/perl";
						last;
				}
			}
			return l_warning("ERROR: perl was not found in your path\n") unless $perl ne "";
	}
	return $perl;
}

sub do_install_reformat {
	my($no_gui)=@_;

	_statusmsg(  "[do_install_reformat] Getting Full Path Name For Your Perl\n" );
	my($perl)=get_perl();
	_statusmsg(  "[do_install_reformat] Perl is $perl\n" );

	opendir(DIR,".") || die("reformat() Can't open directory . for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);

	# make alternate includes based on path - this is so that you have
	# both sets of use lib lines within code that will run on samba
	# cross mounted drives
	my($altpath)="";
	my($ppath)="";
	if( $GemConfig->{INSTALLTYPE} eq "SAMBA" ) {
		my($foundwin32)="FALSE";
		_statusmsg(  "[do_install_reformat] NT_CODE_LOCATION=",$GemConfig->{NT_CODE_LOCATION},"\n" );
		_statusmsg(  "[do_install_reformat] NT_CODE_FULLSPEC=",$GemConfig->{NT_CODE_FULLSPEC},"\n");
		_statusmsg(  "[do_install_reformat] UNIX_CODE_LOCATION=",$GemConfig->{UNIX_CODE_LOCATION},"\n");
		_statusmsg(  "[do_install_reformat] ALT1_CODE_LOCATION=",$GemConfig->{ALT1_CODE_LOCATION},"\n");
		_statusmsg(  "[do_install_reformat] ALT2_CODE_LOCATION=",$GemConfig->{ALT2_CODE_LOCATION},"\n");
		l_die("NO VALID LIBRARY PATHS\n") unless $#perl_includes>=0;

		# ok... lets figure this out -
		my($required_str)= basename($GemConfig->{NT_CODE_LOCATION}) || basename($GemConfig->{UNIX_CODE_LOCATION});
		print ">> [do_install_reformat] required_str = $required_str\n";
		foreach (@perl_includes) {
			if( ! /$required_str/ ) {
				print ">> [do_install_reformat] skipping $_\n";
			} else {
				$altpath .= ",".$_;
				$ppath   .= "-I$_ ";
				# print __LINE__," PI=$_\n";
			}
		}
		$altpath=~s/,$//;		
		$altpath=~s/^,//;		
		
#		foreach (@perl_includes) {
#			my($x)=$_;
#			if( /Win32_perl_lib/i ) {
#				$foundwin32="TRUE";
#				next;
#			}
#			$x =~ s/$CURRENT_CONFIG{gem_root_directory}//;
#			$x =~ s/^[\/\\]+//;
#			if( $CURRENT_CONFIG{isnt} ) {
#				$altpath.=",".$GemConfig->{UNIX_CODE_LOCATION}."/".$x;
#				$altpath.=",".$GemConfig->{ALT1_CODE_LOCATION}."/".$x if $GemConfig->{ALT1_CODE_LOCATION};
#				$altpath.=",".$GemConfig->{ALT2_CODE_LOCATION}."/".$x if $GemConfig->{ALT2_CODE_LOCATION};
#			} else {
#				my($dir)=$GemConfig->{NT_CODE_LOCATION};
#				$dir=~s.\\.\/.g;
#				$altpath.=",".$dir."/".$x;
#
#				$dir = $GemConfig->{ALT1_CODE_LOCATION};
#				$dir=~s.\\.\/.g;
#				$altpath.=",".$dir."/".$x if $dir;
#
#				$dir = $GemConfig->{ALT2_CODE_LOCATION};
#				$dir=~s.\\.\/.g;
#				$altpath.=",".$dir."/".$x if $dir;
#
#			}
#		}

#		if( $foundwin32 eq "FALSE" ) {
#			print "DBGDBG: $foundwin32 = FALSE\n";
#			# ERROR: THE FOLLOWING IS WRONG - THE LIB PATH COULD BE BACKWARDS - IM TESTING
#			# YOUR PERL BUT I NEED TO TEST THE WIN32 PERL...
#			$altpath.=",$GemConfig->{NT_CODE_LOCATION}/Win32_perl_lib_5_6" if $]<5.008;
#			$altpath.=",$GemConfig->{NT_CODE_LOCATION}/Win32_perl_lib_5_8"	if $]>=5.008;
#		}

		# _statusmsg(  "DBG: unix server - alt=$altpath\n" );
	#	$altpath=~s.\\./.g;
		#$altpath=~s.\/./.g;
	}

	# my($basecmd)="$^X -I".join(" -I",@perl_includes)." ";
	my($basecmd)="$^X $ppath ";
	my($cmd)=$basecmd."$CURRENT_CONFIG{admin_scripts_dir}/bin/rebuild.pl --LIBDIR=".
		$altpath. " --PERL=$perl --FILENAME=conf,ADMIN_SCRIPTS,debugging_tools";

	busy();
	RunCommand( -cmd=>$cmd,
			-mainwindow=>$CURRENT_CONFIG{mainWindow},
			-statusfunc=>\&_statusmsg,
			-printfunc =>\&_statusmsg,
			-showinwin => "Rebuild Codeline",
			-printhdr  => "[Reformatter] ",
			-debug=>$DEBUG
			);
	_statusmsg("[ready] GEM Ready / Continuing...\n");
	unbusy();
	chdir $CURRENT_CONFIG{gem_root_directory} or return l_warning("ERROR: Cant cd to $CURRENT_CONFIG{gem_root_directory}: $!\n");
	# set_install_step( "DO_INSTALL_REFORMAT" );
	return "";
}

# remove all stuff from the directories
#sub clear_directory {
#	my($dir)=@_;
#	opendir(DIR,$dir) || die( "reformat() Can't open directory $dir for reading\n" );
#	my(@dirlist) = grep(!/^\./,readdir(DIR));
#	foreach ( @dirlist ) {
#		l_warning("Found File $_ in directory $dir - This shouldnt happen - did you put that file there?\n") unless /\.bat$/ or /\.ksh$/;
#		unlink "$dir/$_" or print "Error unlinking $dir/$_\n";
#	}
#	closedir(DIR);
#}

sub repaint_installstatus_tab {
	_statusmsg("[repaint] repaint_installstatus_tab()\n");
	$graphics_object{INSTSTAT_INSTALLSTATE}->configure(-text=>$GemConfig->{INSTALLSTATE});
	# $graphics_object{INSTSTAT_INSTALLSTATE}->update();

	foreach ( @q_chklst_install_keys ) {
		my($used)='YES';
		$used='NO' if $GemConfig->{USE_SYBASE_ASE} 	eq "N" 			and /SYBASE/;
		$used='NO' if $GemConfig->{USE_SQLSERVER} 	eq "N" 			and /SQLSVR/;
		$used='NO' if $GemConfig->{USE_ORACLE} 		eq "N" 			and /ORACLE/;
		$used='NO' if $GemConfig->{INSTALLTYPE} 		eq "WINDOWS" 	and /UNIX/;
		$used='NO' if $GemConfig->{INSTALLTYPE} 		eq "UNIX" 		and /WIN32/;

		if( $used eq "NO" ) {
			$graphics_object{"INSTSTAT_".$_}->configure(-text=>'Not Needed',-bg=>'grey');
		} elsif( $GemConfig->{$_} ) {
			$graphics_object{"INSTSTAT_".$_}->configure(-text=>do_time(-fmt=>'mon dd @ hh:mi ',-time=>$GemConfig->{$_}),-bg=>'white');
		} else {
			$graphics_object{"INSTSTAT_".$_}->configure(-text=>"(Never Run)",-bg=>'yellow');
		}
	}
}

sub setup_installstatus_tab {
	my($current_tab)=@_;
	tab_header($current_tab,"INSTALL STATUS","Lists the status of your install");
	my $current_subfram  = $current_tab->LabFrame(-label=>"GEM MONITOR STATIONS",
   		-labelside=>'acrosstop')->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);

	$current_subfram->Label(-font=>"norm",	-width=>24, -text=>"INSTALLATION STATE", -relief=>'sunken',-bg=>'beige' )->grid(-column=>0, -row=>0,-padx=>2,-pady=>2,-sticky=>'e');
	$GemConfig->{INSTALLSTATE} = "NOT_INSTALLED" unless $GemConfig->{INSTALLSTATE};
	$graphics_object{INSTSTAT_INSTALLSTATE} =
		$current_subfram->Label(-font=>"norm",-width=>30,-text=>$GemConfig->{INSTALLSTATE},-relief=>'sunken',-bg=>'white' )->grid(-column=>1, -row=>0,-padx=>2,-pady=>2,-sticky=>'w', -columnspan=>2);
	$current_subfram->Label(-font=>"norm", -width=>15, -text=>"" )->grid(-column=>0, -row=>1,-padx=>2,-pady=>2,-sticky=>'e');

	$current_subfram->Button(	-width=>20,	-text=>'REFRESH SCREEN',	-bg=>'white', -command=> sub {
			repaint_installstatus_tab();
		} )->grid(-column=>4, -row=>0,-padx=>2,-pady=>2);

	my($row,$col)=(2,0);
	foreach my $lbl ( get_install_checklist_order() ) {
		#print $lbl," ",$row," ",$col,"\n";
		die "FATAL ERROR get_install_checklist_order() out of sync with get_install_checklist() for $lbl"
			unless $chklst_install_keys{$lbl};
		$current_subfram->Label(-font=>"norm",	-width=>24, -text=>$chklst_install_keys{$lbl}, -relief=>'sunken',-bg=>'beige' )->grid(-column=>$col, -row=>$row,-padx=>2,-pady=>2,-sticky=>'e');
		$graphics_object{"INSTSTAT_".$lbl}=$current_subfram->Label(-font=>"norm", -width=>15, -text=>'Not Set',-relief=>'sunken',-bg=>'white' )->grid(-column=>($col+1),   -row=>$row,-padx=>2,-pady=>2,-sticky=>'w');
		$row++;
		if( $row>6 ) { $row=2; $col+=2; }
	}

	repaint_installstatus_tab();
}

# set_install_major_step($key)		key=NOT_INSTALLED, AGREE_WITH_LICENSE, REGISTERED, INSTALLED
#	 if( $key ) set $GemConfig->{INSTALLSTATE} = $key if key > existing key
# 	 runs pageconfigure on the pages to disable/enable them
sub set_install_major_step {
	my($key)=@_;	#	NOT_INSTALLED, AGREE_WITH_LICENSE, REGISTERED, INSTALLED

	if( $key eq "SETUP" ) {
		_statusmsg("[configure] Setting Visibility Of Page Tabs\n");
		$mf->pageconfigure('Register',		-state=>'disabled');
		$mf->pageconfigure('Paths',			-state=>'disabled');
		$mf->pageconfigure('Monitors',		-state=>'disabled');
		$mf->pageconfigure('Products',		-state=>'disabled');
		$mf->pageconfigure('Server',			-state=>'disabled');
		$mf->pageconfigure('nb_connections',-state=>'disabled');
		$mf->pageconfigure('File',				-state=>'disabled');
		$mf->pageconfigure('Console',			-state=>'disabled');
		$mf->pageconfigure('Web Server',		-state=>'disabled');
		$mf->pageconfigure('Vault',			-state=>'disabled');
		$mf->pageconfigure('Database',		-state=>'disabled');
		$mf->pageconfigure('Install',			-state=>'disabled');
		$mf->pageconfigure('Validate',		-state=>'disabled');
		$mf->pageconfigure('VerifySoftware',-state=>'disabled');
		$mf->pageconfigure('nb_ProcLibInstall',-state=>'disabled');
		$mf->pageconfigure('Scheduler',		-state=>'disabled');
		$mf->raise("Welcome");
		return;
	}

	if( $key ) {	# IF YOU DONT PASS A KEY THEN JUST REDRAW THE GUI
		_statusmsg("[configure] set_install_major_step(key=$key)\n");

		# only set if you are progressing logically - the gui should prevent anything else
		if( $GemConfig->{INSTALLSTATE} eq "NOT_INSTALLED" and $key eq "AGREE_WITH_LICENSE" ) {
			$GemConfig->{INSTALLSTATE} = $key;
		} elsif( $GemConfig->{INSTALLSTATE} eq "AGREE_WITH_LICENSE" and $key eq "REGISTERED" ) {
			$GemConfig->{INSTALLSTATE} = $key;
		} elsif( $GemConfig->{INSTALLSTATE} eq "REGISTERED" and $key eq "INSTALLED" ) {
			$GemConfig->{INSTALLSTATE} = $key;
		#} elsif( $GemConfig->{INSTALLSTATE} eq "REGISTERED" and $key eq "VERIFIED" ) {
		#	$GemConfig->{INSTALLSTATE} = $key;
		#} elsif( $GemConfig->{INSTALLSTATE} eq "VERIFIED" and $key eq "INSTALLED" ) {
		#	$GemConfig->{INSTALLSTATE} = $key;
		} elsif( $GemConfig->{INSTALLSTATE} ne "AGREE_WITH_LICENSE" and
					$GemConfig->{INSTALLSTATE} ne "REGISTERED" and
				#	$GemConfig->{INSTALLSTATE} ne "VERIFIED" and
					$GemConfig->{INSTALLSTATE} ne "NOT_INSTALLED" and
					$GemConfig->{INSTALLSTATE} ne "INSTALLED" 			) {
			die("[configure] Invalid set_install_major_step() - key=$key state=$GemConfig->{INSTALLSTATE} \n");
		}
		set_install_step($key);
	} else {
		_statusmsg("[configure] set_install_major_step()\n");
	}

	if( $GemConfig->{INSTALLSTATE} eq "NOT_INSTALLED" ) {
      $mf->pageconfigure('Register',		-state=>'disabled');
      $mf->pageconfigure('Paths',			-state=>'disabled');
      $mf->pageconfigure('Monitors',		-state=>'disabled');
      $mf->pageconfigure('Products',		-state=>'disabled');
      $mf->pageconfigure('Server',			-state=>'disabled');
      $mf->pageconfigure('nb_connections',		-state=>'disabled');
      $mf->pageconfigure('File',				-state=>'disabled');
      $mf->pageconfigure('Console',			-state=>'disabled');
      $mf->pageconfigure('Web Server',		-state=>'disabled');
      $mf->pageconfigure('Vault',			-state=>'disabled');
      $mf->pageconfigure('Database',			-state=>'disabled');
      $mf->pageconfigure('Install',			-state=>'disabled');
      $mf->pageconfigure('Validate',		-state=>'disabled');
      $mf->pageconfigure('VerifySoftware',		-state=>'disabled');
      $mf->pageconfigure('nb_ProcLibInstall',-state=>'disabled');
		$mf->pageconfigure('Scheduler',		-state=>'disabled');
		$mf->raise("Welcome");
	} elsif( $GemConfig->{INSTALLSTATE} eq "AGREE_WITH_LICENSE" ) {
	   $mf->pageconfigure('Register',		-state=>'normal');
      $mf->pageconfigure('Paths',			-state=>'disabled');
      $mf->pageconfigure('Monitors',		-state=>'disabled');
      $mf->pageconfigure('Products',		-state=>'disabled');
      $mf->pageconfigure('Server',			-state=>'disabled');
      $mf->pageconfigure('nb_connections',		-state=>'disabled');
		$mf->pageconfigure('nb_ProcLibInstall',-state=>'disabled');
      $mf->pageconfigure('File',				-state=>'disabled');
      $mf->pageconfigure('Console',			-state=>'disabled');
      $mf->pageconfigure('Database',			-state=>'disabled');
      $mf->pageconfigure('Web Server',		-state=>'disabled');
      $mf->pageconfigure('Vault',			-state=>'disabled');
      $mf->pageconfigure('Install',			-state=>'disabled');
      $mf->pageconfigure('Validate',		-state=>'disabled');
      $mf->pageconfigure('VerifySoftware',		-state=>'disabled');
      $mf->pageconfigure('nb_ProcLibInstall',-state=>'disabled');
		$mf->pageconfigure('Scheduler',		-state=>'disabled');
		$mf->raise("Register");
	} elsif( $GemConfig->{INSTALLSTATE} eq "REGISTERED" ) {
	   $mf->pageconfigure('Register',		-state=>'normal');
	   $mf->pageconfigure('Paths',			-state=>'normal');
      $mf->pageconfigure('Monitors',		-state=>'normal');
      $mf->pageconfigure('Products',		-state=>'normal');
      $mf->pageconfigure('Server',			-state=>'normal');
      $mf->pageconfigure('nb_connections',		-state=>'normal');
		$mf->pageconfigure('nb_ProcLibInstall',-state=>'normal');
      $mf->pageconfigure('File',				-state=>'normal');
      $mf->pageconfigure('Console',			-state=>'normal');
      $mf->pageconfigure('Database',			-state=>'normal');
      $mf->pageconfigure('Web Server',		-state=>'normal');
      $mf->pageconfigure('Vault',			-state=>'normal');
      $mf->pageconfigure('Install',			-state=>'normal');
      $mf->pageconfigure('Validate',		-state=>'disabled');
      $mf->pageconfigure('VerifySoftware',-state=>'normal');
      $mf->pageconfigure('nb_ProcLibInstall',-state=>'normal');
		$mf->pageconfigure('Scheduler',		-state=>'disabled');
#	} elsif( $GemConfig->{INSTALLSTATE} eq "VERIFIED" ) {
#	   $mf->pageconfigure('Register',		-state=>'normal');
#	   $mf->pageconfigure('Paths',			-state=>'normal');
#      $mf->pageconfigure('Monitors',		-state=>'normal');
#      $mf->pageconfigure('Products',		-state=>'normal');
#      $mf->pageconfigure('Server',			-state=>'normal');
#      $mf->pageconfigure('nb_connections',		-state=>'normal');
#		$mf->pageconfigure('nb_ProcLibInstall',-state=>'normal');
#      $mf->pageconfigure('File',				-state=>'normal');
#      $mf->pageconfigure('Console',			-state=>'normal');
#      $mf->pageconfigure('Database',			-state=>'normal');
#      $mf->pageconfigure('Web Server',		-state=>'normal');
#      $mf->pageconfigure('Vault',		-state=>'normal');
#      $mf->pageconfigure('Install',			-state=>'normal');
#      $mf->pageconfigure('Validate',		-state=>'disabled');
#      $mf->pageconfigure('VerifySoftware',-state=>'normal');
#      $mf->pageconfigure('nb_ProcLibInstall',-state=>'normal');
#		$mf->pageconfigure('Scheduler',		-state=>'disabled');
	} elsif( $GemConfig->{INSTALLSTATE} eq "INSTALLED" ) {
	   $mf->pageconfigure('Register',		-state=>'normal');
	   $mf->pageconfigure('Monitors',		-state=>'normal');
	   $mf->pageconfigure('Paths',			-state=>'normal');
      $mf->pageconfigure('Products',		-state=>'normal');
      $mf->pageconfigure('Server',			-state=>'normal');
      $mf->pageconfigure('nb_connections',		-state=>'normal');
		$mf->pageconfigure('nb_ProcLibInstall',-state=>'normal');
      $mf->pageconfigure('File',				-state=>'normal');
      $mf->pageconfigure('Console',			-state=>'normal');
      $mf->pageconfigure('Database',			-state=>'normal');
      $mf->pageconfigure('Web Server',		-state=>'normal');
      $mf->pageconfigure('Vault',		-state=>'normal');
      $mf->pageconfigure('Install',			-state=>'normal');
      $mf->pageconfigure('Validate',		-state=>'normal');
      $mf->pageconfigure('VerifySoftware',		-state=>'normal');
      $mf->pageconfigure('nb_ProcLibInstall',-state=>'normal');
		$mf->pageconfigure('Scheduler',		-state=>'normal');
	}
	_statusmsg("[configure] Current install state=".$GemConfig->{INSTALLSTATE}."\n");
}

sub setup_welcome_tab {
	my($tabW)=@_;

   my($str)="GENERIC ENTERPRISE MANAGER
   $CURRENT_CONFIG{VERSION}
   Copyright (c) 1995-2011 by SQL Technologies.  All rights reserved.";

	tab_header($tabW,$str,"");
   #$tabW->Label(-font=>"bold",-text=>=>$str, -relief=>"groove", -bg=>'beige')->pack(-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w');

   my $tabW_m  = $tabW->LabFrame(-label=>"LICENSE ACCEPTANCE", -labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -pady=>15, -anchor=>'w', -expand=>1, -fill=>'both');
   $tabW_m->Label(-font=>"norm",-textvariable=>\$welcome_tab_text, -justify=>'left')->pack(-side => "top", -fill=>'x');

   my $bframe = $tabW_m->Frame()->pack(-side => "bottom", -fill=>'x');
   my($color)="yellow";		#get_install_color("AGREE_WITH_LICENSE");
   $graphics_object{AGREE_WITH_LICENSE}=$bframe->Button(
	-width=>25,
	-text=>'I AGREE WITH THE LICENSE',
	-command=> sub {
		_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
		return if is_busy();
		_statusmsg("[configure] \n");
		_statusmsg("[configure] You have Agreed to abide by the usage license\n");
		_statusmsg("[configure] \n");
		set_install_major_step( "AGREE_WITH_LICENSE" );
   	# $graphics_object{AGREE_WITH_LICENSE}->configure(-bg=>'white');
   	$mf->raise("Register");
   	},-bg=>$color )->pack( -side=>"right", -pady=>2, -padx=>2);
   dohelp($graphics_object{AGREE_WITH_LICENSE},"By clicking here you agree to respect the above license.");

    	dohelp($bframe->Button(
		-width=>25,
		-text=>'VIEW PRICING OPTIONS',
		-command=> \&click_pricing,
		-bg=>'white' )->pack( -side=>"right", -pady=>2, -padx=>2),
	"See detailed pricing options.");

	dohelp($bframe->Button(
	-width=>25,
	-text=>'LICENSE',
	-command=> \&click_rtu,
	-bg=>'white' )->pack( -side=>"right", -pady=>2, -padx=>2),
   	"See license details.");

   	dohelp($bframe->Button(
	-width=>25,
	-text=>'READ THIS',
	-command=> \&how_to_use,
	-bg=>'white' )->pack( -side=>"right", -pady=>2, -padx=>2),
   	"Instructions");

   	dohelp($bframe->Button(
	-width=>25,
	-text=>'INTRODUCTION',
	-command=> \&set_text_main,
	-bg=>'white' )->pack( -side=>"right", -pady=>2, -padx=>2),
   	"GEM Introduction.");

   	set_text_main();
}
sub set_text_main {
	  $welcome_tab_text=
"The Generic Enterprise Manager (GEM) is a solution to help Database Administrators (DBAs) manage enterprise databases.
Historically, SQL Technologies developed free Database Administration software and was supported through voluntary
contributions. GEM is a commercial version of this effort, providing a complete server management toolbox designed
specifically for DBAs.  GEM provides enterprise montitoring and alarming, infrastructure reporting, server
diagnostics, and plan based database backup and maintenance.  GEM provides Web Based and Graphical Screens that are
based on our perl command line utilities and our widely used extended stored procedure library.  Currently GEM is
targeted at Sybase and SQL Server systems but the architecture and design permits consolidated management of any
System or Database. GEM is a complete solution to help you automate the management of your database environment.

The home page for this package is http://www.edbarlow.com/gem.  Feedback should be directed to moreaboutgem\@gmail.com.




License, Pricing Information, and Package Requirements can be found by selecting the buttons listed below.
By continuing, you agree with the GEM terms of use.



";
}

sub click_pricing {
	$welcome_tab_text=

"Project GEM is a major effort designed to help manage business critical infrastructure at commercial 
organizations. This code is open source, but commercial users are expected to support this project financially. 

GEM is currently available at \$500 per database server for an initial license.  There is also a \$200 per 
server annual charge for maintenance, support, and software updates.

You are welcome to download and try the GEM software for evaluation purposes.
After a suitable period of evaluation, if you find GEM suitable for your production support, you are expected
to purchase a license.







";


}

sub how_to_use {
		  $welcome_tab_text=
"GEM is a toolkit for administrators and setup requires system administrator access to your database servers. On Unix,
you will need a login with access to system and database error log files.  On Windows, scheduled tasks run under an account
that needs direct access to remote Windows event logs and disks.  On a Windows GEM Installation, you are responsible for
setting up ODBC database entries and for installing perl 5.8 with the DBI and DBD::ODBC modules.  On Unix, you must install
perl with DBI and the appropriate Native DBD Database Drivers.  Linux and Unix systems are considered interchangeable.

If you have both Unix and Windows Dataservers, it is recommended you install GEM on a Samba share.  Samba allows the same
software to run seamlessly in both environments.  It is recommended that you use dedicated monitoring systems to schedule
GEM batch jobs.  One monitor system for Unix and one for Windows.  Monitoring servers have low system requirements. GEM *MUST*
be installed in a secure location - the configuration files contain administrator passwords. GEM is a small footprint tool,
adding only a useful, stand alone, set of System Stored Procedures to your Sybase and Microsoft SQL Servers.  Nothing else
is installed or changed on your servers. GEM requires a small (200-300MB) database for system monitoring.

This Program, the GEM Configuration Utility, sets up configuration files.  These files are stored in the conf subdirectory
and can be edited by hand for convenience. Data is saved when you select the Save Configuration Changes button.  The Quit
button exits without saving.  Operations you complete are permanent.

Complete the tabs from left to right.  The top of each page gives instructions.   Yellow backgrounds indicate important tasks you have
not yet performed. All tasks are rerunable so it is ok to start with one or two servers and add more later.
";
}

sub click_rtu {
	$welcome_tab_text=
"All GEM software and documentation are Copyright (c) 1995-2011 by SQL Technologies, inc.
All rights are reserved. GEM is currently under strict controls.
GEM may be downloaded from www.edbarlow.com for use, but neither the software nor any part of it
may not be redistributed in any manner without express consent.

NO WARANTY OF ANY SORT IS PROVIDED WITH THIS SOFTWARE

The terms of this license will change once the software goes general availability.

GEM is distributed as open source code, and it is ok for you to inspect and modify
the code line so long so long as you don't remove any of the copyright notices and do
not redistribute any part of the code without the written consent of SQL Technologies.






";
}

#sub do_an_install {
#
#	start_msg_logging( "Starting Full Install\n" );
#	_statusmsg("[configure] starting do_an_install()\n");
#
#	# check your configuration
#	my(%requiredsteps)=get_required_install_steps(-rc=>'values');
#	my(@errs);
#	if( ! $UPGRADE ) {
#		foreach ( keys %requiredsteps ) {
#			my($t)= get_install_variable( $_ );
#			if( ! defined $t ) {
#				push @errs, "\t",$requiredsteps{$_},"\n" unless $_ eq "INSTALLED";
#			}
#		}
#	}
#	if( $#errs >= 0 ) {
#		_statusmsg("[configure] starting confirm box\n");
#		unshift @errs,"This could mean that your installation will not be successful\n\n";
#		unshift @errs,"You have skipped the following steps\n";
#		push    @errs,"\nWould you like to continue?";
#		_statusmsg("[configure] Displaying message box - select a button to continue\n");
#		my($ans) = $CURRENT_CONFIG{mainWindow}->messageBox(
#				-title => "Validation Error",
#				-message => join("",@errs),
#			 	-type => "YesNo", -default=>'no' );
#		_statusmsg("[configure] done confirm box - answer = $ans\n");
#	 	return unless $ans =~ /yes/i;
#	}
#
#		_statusmsg("[configure] starting validation...\n");
#   my($rc)=validate_everything();
#
#   _statusmsg("[configure] validation results ($rc)...\n");
#   return if $rc ne "";
#   _statusmsg("[configure] Validation OK! Continuting installation...\n");
#
#	busy() unless $UPGRADE;
#
#	# Fix ctrl m and hashbangs
#	_debugmsg(  "[configure] beginning actual upgrade steps\n" );
#
#	do_install_reformat(1);
#	do_install_create_batchjobs();
#	#do_install_backup_scripts(); 	# 	if $GemConfig->{INSTALL_SYDUMP_BACKUP_SCRIPTS} eq "Y";
#	do_install_console(); 		# 	if $GemConfig->{INSTALL_SERVER_DOCUMENTER} eq "Y";
#
#	_statusmsg( "[setup] Install/Upgrade Alarm Database\n" );
#	set_install_step( "INSTALL_ALARM_DATABASE" ) if install_gemalarms(1);
#
#	_statusmsg( "[setup] Populate Local Cgi Bin\n" );
#	set_install_step( "POPULATE_LOCAL_CGI_BIN" ) if btn_populate_local_cgibin(1);
#
#	#_statusmsg( "[setup] Copy Cgi Bin to Webserver\n" );
#	#set_install_step( "COPY_CGI_BIN_TO_WEBSERVER" ) if btn_copy_local_cgi_to_websvr();
#
#	_statusmsg( "[setup] Installing Procedure Library\n" );
#	($rc)=do_installprocs();
#	if( $rc eq "" ) {
#		set_install_step("INSTALL_PROCEDURE_LIBRARY_SYBASE") if $GemConfig->{USE_SYBASE_ASE} eq "Y";
#		set_install_step("INSTALL_PROCEDURE_LIBRARY_SQLSVR") if is_nt() and $GemConfig->{USE_SQLSERVER} eq "Y";
#	}
#
#   $CURRENT_CONFIG{INSTALL}=time;
#	set_install_major_step( "INSTALLED" );
#
#	# colorize_install_buttons() unless $UPGRADE;
#	unbusy() unless $UPGRADE;
#
#	# print "DBG DBG: end_msg_logging()";
#	my(@rc)=end_msg_logging();
#
#	# foreach (@rc) { print "DBG DBG rc=$_\n"; }
#	if( ! $UPGRADE and $#rc>=0 ) {
#		_statusmsg("[configure] Please Select 'OK' on the pop-up Results display\n");
#		my_rotext_box(-title=>'Install GEM Software', -label=>'Install Software' , -text=>join("\n",@rc));
#		_statusmsg("[ready] GEM Ready / Continuing...\n");
#	}
#	_debugmsg("[configure] do_an_install() completed\n");
#}

sub setup_webserver_tab {
	my($tab_webserver)=@_;
	tab_header($tab_webserver,"WEB SERVER DEFINITION","A web server is required - with a virtual directory able to run perl scripts.  This page specifies how to deploy to this directory.
\n'copy $CURRENT_CONFIG{gem_root_directory}/ADMIN_SCRIPTS/cgi-bin //win32pathtocgibin'.");

	my $websvr_on_win32_labfrm  = $tab_webserver->LabFrame(
     		-label=>"IIS Web Server Directory",
     		-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -pady=>5, -anchor=>'w', -expand=>1, -fill=>'both');
	my $websvr_ops_labfrm  = $tab_webserver->LabFrame(
     		-label=>"Tasks",
     		-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -pady=>5, -anchor=>'w', -expand=>1, -fill=>'both');

   my $websvr_on_win32_subfrm  = $websvr_on_win32_labfrm->Frame()->pack(-side=>'top', -padx=>15, -pady=>15, -anchor=>'w');

	my($row_w)=1;

	my($pname_win32)="Win32 Full Path To Cgi-Bin";
	my($ie_win32)   ="i.e. \/\/hostname\/d\$\/path\/";
   my($pname_unix) ="Unix Destination Directory";
   my($ie_unix)    ="i.e. /apps/apache/hostname/gem/";
   my($wshtxt )    = "Web Server Hostname";

	$GemConfig->{MIMI_IS_WIN32}='Y';

	$websvr_on_win32_subfrm->Label( -padx=>15, -font=>"norm",-justify=>'left',-text=>$pname_win32)->grid(-column=>1,-row=>$row_w,-sticky=>'w');
	$websvr_on_win32_subfrm->Label( -padx=>15, -font=>"norm",-justify=>'left',-text=>$ie_win32)->grid(-column=>3,-row=>$row_w,-sticky=>'w');
	$graphics_object{MIMI_FILENAME_WIN32}=$websvr_on_win32_subfrm->Entry(-bg=>'white',-width=>50,-textvariable=>\$GemConfig->{MIMI_FILENAME_WIN32})->grid(-column=>2,-row=>$row_w++,-sticky=>'w'),
   dohelp( $graphics_object{MIMI_FILENAME_WIN32},	"File Name mapping to the above URL\nWe will copy/install to here.");

	$websvr_on_win32_subfrm->Label( -padx=>15, -font=>"norm",-justify=>'left',-text=>"URL Base For This Directory")->grid(-column=>1,-row=>$row_w,-sticky=>'w');
	$websvr_on_win32_subfrm->Label( -padx=>15, -font=>"norm",-justify=>'left',-text=>"i.e. http://hostname/gem")->grid(-column=>3,-row=>$row_w,-sticky=>'w');
   $graphics_object{MIMI_URL_WIN32}=$websvr_on_win32_subfrm->Entry(-bg=>'white',-width=>50,-textvariable=>\$GemConfig->{MIMI_URL_WIN32})->grid(-column=>2,-row=>$row_w++,-sticky=>'w');
   dohelp( $graphics_object{MIMI_URL_WIN32},
   	"Full Path Web URL That The Above CGI Script Enabled Directory Represents.");

	$websvr_on_win32_subfrm->Label( -padx=>15, -font=>"norm",-justify=>'left',-text=>"Perl Used By Your Win32 Web Server")->grid(-column=>1,-row=>$row_w,-sticky=>'w');
   $graphics_object{MIMI_CGIBIN_PERL_WIN32}=$websvr_on_win32_subfrm->Entry(-bg=>'white',-width=>50,-textvariable=>\$GemConfig->{MIMI_CGIBIN_PERL_WIN32})->grid(-column=>2,-row=>$row_w++,-sticky=>'w');
   dohelp( $graphics_object{MIMI_CGIBIN_PERL_WIN32},
   		"Perl you wish to use in the web scripts (ie. on your web server).");

	#$websvr_on_win32_subfrm->Label(-padx=>15, -font=>"norm",-justify=>'left',-text=>"Win32 Login For Scheduled Tasks")->grid(-column=>1,-row=>$row_w,-sticky=>'w');
	#$graphics_object{MIMI_LOGIN_WIN32}=$websvr_on_win32_subfrm->Entry(-bg=>'white',-width=>50,-textvariable=>\$GemConfig->{MIMI_LOGIN_WIN32})->grid(-column=>2,-row=>$row_w,-sticky=>'w');
   #dohelp( $graphics_object{MIMI_LOGIN_WIN32},	"Native Authentication Login For IIS.");
   #$websvr_on_win32_subfrm->Label( -padx=>15, -font=>"norm",-justify=>'left',-text=>"The run-as account for windows scheduled tasks")->grid(-column=>3,-row=>$row_w++,-sticky=>'w');

 	my($bcolor)=get_install_color("COPY_CGI_BIN_TO_WEBSERVER");
	$graphics_object{COPY_CGI_BIN_TO_WEBSERVER}=$websvr_ops_labfrm->Button(  -font=>"norm",
		-width=>30,
		-text=>'Copy Code to Web Server',
		-bg=>$bcolor,
		-command=>sub {
			btn_copy_local_cgi_to_websvr();
		 })->pack(  -padx=>15, -side=>"left" );	#grid(-column=>2,-row=>$row++,-sticky=>'w');
	dohelp($graphics_object{COPY_CGI_BIN_TO_WEBSERVER}, "Copy code to webserver");

   $graphics_object{COPY_CGI_BIN_TO_WEBSERVER}->configure(-state=>'disabled') unless is_nt();

	my(%alstate);
	$alstate{-state} = 'normal';
	$alstate{-state} = 'disabled' unless get_install_variable("COPY_CGI_BIN_TO_WEBSERVER");
	$graphics_object{VERIFY_CGIURL_WORKS}=$websvr_ops_labfrm->Button(  -font=>"norm",
		-width=>30,
		-text=>'Auto Verify Web Server',
		-bg=>get_install_color("VERIFY_CGIURL_WORKS"),
		-command=>sub {
			_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
			return if is_busy();

			btn_verify_web_url();

			# _statusmsg( "[setup] starting web browser to $GemConfig->{MIMI_URL_WIN32} \n" );
			# show_html( $GemConfig->{MIMI_URL} ) if defined $GemConfig->{MIMI_URL_WIN32} and $GemConfig->{MIMI_URL_WIN32} !~ /^\s*$/;
		 })->pack(  -padx=>15, -side=>"left" );	# -side=>"right", -pady=>2, -padx=>2);
	dohelp($graphics_object{VERIFY_CGIURL_WORKS}, "Verify URL Works A Web Browser To View The Alarm Viewer");
}

#sub ui_validate_webserver {
#	if( ! defined get_install_variable( 'POPULATE_LOCAL_CGI_BIN' ) ) {
#		_statusmsg("[configure] Displaying message box - select a button to continue\n");
#		$CURRENT_CONFIG{mainWindow}->messageBox(
#				-title => 'Warning Not Installed!',
#				-message => "Warning: You have not performed the step Populate local cgi-bin\nContinuing...",
#				-type => "OK" );
#		_statusmsg("[ready] GEM Ready / Continuing...\n");
#	}
#
#	if( ! defined get_install_variable( 'COPY_CGI_BIN_TO_WEBSERVER' ) ) {
#		_statusmsg("[configure] Displaying message box - select a button to continue\n");
#		$CURRENT_CONFIG{mainWindow}->messageBox(
#				-title => 'Warning Not Installed!',
#				-message => "Warning: You have not performed the step Copy local cgi-bin to Web Server\nContinuing...",
#				-type => "OK" );
#		_statusmsg("[ready] GEM Ready / Continuing...\n");
#	}
#
#	return if is_busy();
#	busy();
#	my($ok)="YES";
#	_statusmsg("[setup] Validating Web Server Setup\n");
#
#	my(@rc)=test_web_installation( $GemConfig );
#	my($vstate)="INCOMPLETE";
#	foreach (@rc) {
#		chomp;
#		_statusmsg("[verify] $_\n");
#		if( /ERROR:/ or /WARNING:/ ) {
#			$vstate="ERROR";
#			error_messagebox("Validation Error",$_);
#		} elsif( /SUCCESSFUL:/ ) {
#			$vstate="OK";
#		} elsif( /SKIPPED:/ ) {
#			$vstate="SKIPPED";
#		}
#	}
#	_statusmsg("[setup] Validation=$ok\n");
#	if(  $vstate eq "OK" ) {
#		_statusmsg( "[setup] web server validation succeeded\n" );
#		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
#				-title=>"Validate Web Server Installation",
#				-buttons=>["Ok"] );
#		$dialog->Label(-text=>"Web Server Successful Validation")->pack();
#		$dialog->Show;
#		set_install_step( "VERIFY_WEBSERVER" );
#		$graphics_object{VERIFY_CGIURL_WORKS}->configure(-state=>'normal');
#		$mf->raise("Web Server");
#	} else {
#		set_install_step("VALIDATE_ALARM_INSTALLATION",1);
#		_statusmsg( "[setup] validation failed\n" );
#
#		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
#			-title=>"Validate Web Server Installation",
#			-buttons=>["Ok"] );
#		$dialog->Label(-text=>"Web Server Validation FAILED: $rc")->pack();
#		$dialog->Show;
#	}
#	unbusy();
#}

sub redraw_alarms_svtype {
	if( $GemConfig->{ALARM_DB_TYPE} eq "sybase" ) {
		if( $GemConfig->{USE_SYBASE_ASE} ne "Y" ) {
				error_messagebox("Validation Error","ERROR - Cant Set Sybase Server As An Alarm Server as USE_SYBASE_ASE=N\n");
				$GemConfig->{ALARM_DB_TYPE} = "sqlsvr";
				return;
		}
		$graphics_object{ALARM_SA_PASSWORD}->configure(-state=>"normal");
	 	$graphics_object{ALARM_SA_LOGIN}->configure(-state=>"normal");
	 	$graphics_object{ALARM_SA_LOGIN_LBL2}->configure(-text=>"[ Login that is a dbo in the gamalarms database. ]");
	  	$graphics_object{ALARM_SA_PASSWORD_LBL2}->configure(-text=>"[ Login that is a dbo in the gamalarms database. ]");

	} else {
		if( $GemConfig->{USE_SQLSERVER} ne "Y" ) {
				error_messagebox("Validation Error","ERROR - Cant Set Sql Server As An Alarm Server as USE_SQLSERVER=N\n");
				$GemConfig->{ALARM_DB_TYPE} = "sybase";
				return;
		}
		$graphics_object{ALARM_SA_LOGIN_LBL2}->configure(-text=>"[ Native Authentication Used w/SQL Server.   ]");
	  	$graphics_object{ALARM_SA_LOGIN}->configure(-state=>"disabled");
		$graphics_object{ALARM_SA_PASSWORD_LBL2}->configure(-text=>"[ Native Authentication Used w/SQL Server.   ]");
	  	$graphics_object{ALARM_SA_PASSWORD}->configure(-state=>"disabled");
	}
}

sub setup_alarms_tab {
	my($tab_alarms)=@_;
	tab_header($tab_alarms,"DATABASE FOR ALARMS AND MONITORING",
"GEM uses a centralized Alarm database for monitoring.  This database must be on a registered server and
must be set up according to the installation directions.  To install objects inot the database, enter
Alarm Server Information and Select the 'Install/Upgrade Alarm Database' Button.");

	my $alarmstab_labfrm  = $tab_alarms->LabFrame(
     		-label=>"Setup Alarm and Monitoring System",
     		-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -pady=>15, -anchor=>'w', -expand=>1, -fill=>'both');

   my $alarms_subfrm  = $alarmstab_labfrm->Frame()->pack(-side=>'top', -padx=>15, -pady=>15, -anchor=>'w');

	my($bcolor)=get_install_color("INSTALL_ALARM_DATABASE");
	$alarms_subfrm->Label(	-font=>"norm",	-text=>"Alarm Database Server",
		-relief=>'sunken',-bg=>'beige')->grid(-column=>0,-row=>0, -columnspan=>3, -pady=>2, -padx=>2,-sticky=>'ew');

#	$alarms_subfrm->Label(	-font=>"norm",	-text=>"Web Server (for Alarm Monitoring)",
#		-relief=>'sunken',-bg=>'beige')->grid(-column=>3,-row=>0, -columnspan=>2, -pady=>2, -padx=>2,-sticky=>'ew');

		my($row)=1;
   	$alarms_subfrm->Label(-font=>"norm",-text=>"Server For Alarms")->grid(-column=>0,-row=>$row,-sticky=>'w');
   	$graphics_object{ALARM_SVR} =	$alarms_subfrm->Entry(-width=>30,-textvariable=>\$GemConfig->{ALARM_SERVER},-bg=>$bcolor)->grid(-column=>1,-row=>$row++,-sticky=>'w');
   	dohelp( $graphics_object{ALARM_SVR},	"SERVER_NAME of Server Hosting Alarms Database");

		$alarms_subfrm->Label(-font=>"norm",-text=>"Database Type")->grid(-column=>0,-row=>$row,-sticky=>'w');

   	if( ! $GemConfig->{ALARM_DB_TYPE} ) {
   		if( $GemConfig->{USE_SYBASE_ASE} eq "Y" ) {
   			 $GemConfig->{ALARM_DB_TYPE}="sybase";
   		} else {
   			 $GemConfig->{ALARM_DB_TYPE}="sqlsvr";
   		}
   	}

		my($frm_dbtouse) = $alarms_subfrm->Frame()->grid(-column=>1, -row=>$row++, -sticky=>'w');

		dohelp(
			$frm_dbtouse->Radiobutton(-font=>"norm",
	   		-text=>"Sybase",
	   		-command => \&redraw_alarms_svtype,
	   		-variable=>\$GemConfig->{ALARM_DB_TYPE},-value=>"sybase")->pack(-side=>'left'),
	   	"Select if you have a unix only or hybrid environment runinog on a samba share");
	   dohelp(
			$frm_dbtouse->Radiobutton(-font=>"norm",
	   		-text=>"Mysql",
	   		-command => \&redraw_alarms_svtype,
	   		-variable=>\$GemConfig->{ALARM_DB_TYPE},-value=>"mysql")->pack(-side=>'left'),
	   	"Select if you have a unix only or hybrid environment runinog on a samba share");
	   dohelp(
	   	$frm_dbtouse->Radiobutton(-font=>"norm",
	   		-text=>"Sql Server",
	   		-command => \&redraw_alarms_svtype,
	   		-variable=>\$GemConfig->{ALARM_DB_TYPE},-value=>"sqlsvr")->pack(-side=>'left'),
	   	"Select if you do NOT have any Unix servers or run unixODBC and are comfortable with it");

   	$alarms_subfrm->Label(-font=>"norm",-text=>"Database For Alarms")->grid(-column=>0,-row=>$row,-sticky=>'w');
   	# $alarms_subfrm->Label(-font=>"norm",-text=>"gemalarms")->grid(-column=>1,-row=>$row++,-sticky=>'w');
		$graphics_object{ALARM_DATABASE} = $alarms_subfrm->Entry(-width=>30,-textvariable=>\$GemConfig->{ALARM_DATABASE},-bg=>$bcolor)->grid(-column=>1,-row=>$row++,-sticky=>'w');

   	$alarms_subfrm->Label(-font=>"norm",-text=>"Dbo Login")->grid(-column=>0,-row=>$row,-sticky=>'w');
		$graphics_object{ALARM_SA_LOGIN_LBL2} = $alarms_subfrm->Label(-font=>"norm",-text=>"[ Native Authentication Used w/SQL Server.   ]")->grid(-column=>2,-row=>$row,-sticky=>'w');
   	$graphics_object{ALARM_SA_LOGIN} = $alarms_subfrm->Entry(-width=>30,-textvariable=>\$GemConfig->{ALARM_SA_LOGIN},-bg=>$bcolor)->grid(-column=>1,-row=>$row++,-sticky=>'w');
   	dohelp($graphics_object{ALARM_SA_LOGIN},	"Public Login To this Server (non DBO) that can be used for any programs to save alarms.\nThis must be a user in the database");

  		$alarms_subfrm->Label(-font=>"norm",-text=>"Dbo Password")->grid(-column=>0,-row=>$row,-sticky=>'w');
   	$graphics_object{ALARM_SA_PASSWORD_LBL2} = $alarms_subfrm->Label(-font=>"norm",-text=>"[ Native Authentication Used On SQL Server.   ]")->grid(-column=>2,-row=>$row,-sticky=>'w');
   	$graphics_object{ALARM_SA_PASSWORD} = $alarms_subfrm->Entry( -show=>'*',-width=>30,-textvariable=>\$GemConfig->{ALARM_SA_PASSWORD},-bg=>$bcolor)->grid(-column=>1,-row=>$row++,-sticky=>'w');
   	dohelp(	$graphics_object{ALARM_SA_PASSWORD}, "Password For the Public Login To this Server (non DBO)");

   	$alarms_subfrm->Label(-font=>"norm",-text=>"Public Login")->grid(-column=>0,-row=>$row,-sticky=>'w');
   	$graphics_object{ALARM_LOGIN} = $alarms_subfrm->Entry(-width=>30,-textvariable=>\$GemConfig->{ALARM_LOGIN},-bg=>$bcolor)->grid(-column=>1,-row=>$row++,-sticky=>'w');
   	dohelp($graphics_object{ALARM_LOGIN},	"Public Login To this Server (non DBO) that can be used for any programs to save alarms.\nThis must be a user in the database");

   	$alarms_subfrm->Label(-font=>"norm",-text=>"Public Password")->grid(-column=>0,-row=>$row,-sticky=>'w');
   	$graphics_object{ALARM_PASSWORD} = $alarms_subfrm->Entry( -width=>30,-textvariable=>\$GemConfig->{ALARM_PASSWORD},-bg=>$bcolor)->grid(-column=>1,-row=>$row++,-sticky=>'w');
   	dohelp(	$graphics_object{ALARM_PASSWORD}, "Password For the Public Login To this Server (non DBO)");

   	$alarms_subfrm->Label(-font=>"norm",-text=>"Alarm System Mail Address")->grid(-column=>0,-row=>$row,-sticky=>'w');
   	$alarms_subfrm->Label(-font=>"norm",-text=>"[ This is the source mail address for alerts ]")->grid(-column=>2,-row=>$row,-sticky=>'w');
   	$graphics_object{ALARM_MAIL} = $alarms_subfrm->Entry(-width=>30,-textvariable=>\$GemConfig->{ALARM_MAIL},
   		-bg=>$bcolor)->grid(-column=>1,-row=>$row++,-sticky=>'w');
   	dohelp( $graphics_object{ALARM_MAIL} ,	"From Mail Address for alarms");

	# this would qualify as a blank column
	$alarms_subfrm->Label(-width=>5,-text=>"")->grid(-column=>2,-row=>$row++,-sticky=>'ew');

	$bcolor=get_install_color("INSTALL_ALARM_DATABASE");
	$graphics_object{INSTALL_ALARM_DATABASE}=$alarms_subfrm->Button( -font=>"norm",
		-text=>"Install/Upgrade Alarm Database", -width=>30, -bg=>$bcolor,
		-command=>sub {
			btn_install_gemalarms();
		},		-bg=>$bcolor		 )->grid(-column=>1,-row=>$row,-sticky=>'ew');
	dohelp(	$graphics_object{INSTALL_ALARM_DATABASE},	"Install or Upgrade The Alarms Database");

	my $bframe = $alarmstab_labfrm->Frame()->pack(-side => "bottom", -fill=>'x');
	$bcolor=get_install_color("VALIDATE_ALARM_INSTALLATION");
   $graphics_object{VALIDATE_ALARM_INSTALLATION}=$bframe->Button(
		-font=>"norm",-width=>30,
		-text=>'Validate Alarm Installation',
		-command=> sub {
			btn_test_validate_alarm_installation();
		},	-bg=>$bcolor )->pack( -side=>"right", -pady=>2, -padx=>2);
   	dohelp($graphics_object{VALIDATE_ALARM_INSTALLATION},		"Validate Your Alarms Infrastructure");
}

sub btn_test_validate_alarm_installation {
	_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
	return if is_busy();

	# save the configuration
#	btn_save_config_changes(1,"TRUE");

	_statusmsg( "[setup] \n" );
	_statusmsg( "[setup] Validating Alarm Database Setup\n");
	_statusmsg( "[setup] \n" );

	if( ! defined get_install_variable( 'INSTALL_ALARM_DATABASE' ) ) {
		#print "DBGDBG: ",$GemConfig->{'INSTALL_ALARM_DATABASE'},"\n";
		#print Dumper $GemConfig;
		_statusmsg("[configure] Displaying message box - select a button to continue\n");
		$CURRENT_CONFIG{mainWindow}->messageBox(
				-title => 'Warning Not Installed!',
				-message => "Warning: You have not performed the step \"Install/Update Alarm Database\"\nContinuing...",
				-type => "OK" );
		_statusmsg("[ready] GEM Ready / Continuing...\n");
		return 0;
	}

	busy();
	my($ok)="YES";

	my($str)="$^X -I".join(" -I",@perl_includes)." $CURRENT_CONFIG{gem_root_directory}/bin/test_alarms.pl";	
		$str.= " -ALARM_SERVER=$GemConfig->{ALARM_SERVER} ";
		$str.= " -ALARM_DATABASE=$GemConfig->{ALARM_DATABASE} ";
		$str.= " -ALARM_LOGIN=$GemConfig->{ALARM_LOGIN} ";
		$str.= " -ALARM_PASSWORD=$GemConfig->{ALARM_PASSWORD} ";
		
	_statusmsg(  "(executing) ",$str );
	my(@rc);
	open(VAL90,$str." |" ) or die("ERROR CANT RUN $str\n");
	my($vstate)="INCOMPLETE";
	while( <VAL90> ) {
		push @rc, $_;
		_statusmsg("[rc] $_\n");
		if( /ERROR:/ or /WARNING:/ ) {
			$vstate="ERROR";
			error_messagebox("Validation Error",$_);
		} elsif( /SUCCESSFUL:/ ) {
			$vstate="OK";
		} elsif( /SKIPPED:/ ) {
			$vstate="SKIPPED";
		}
	}
	close(VAL90);

	if(  $vstate eq "OK" ) {
		_statusmsg( "[setup] Validation Succeeded\n" );
		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
					-title=>"Validate Alarm Installation",
					-buttons=>["Ok"] );
		$dialog->Label(-text=>"Alarm System Successful Validation")->pack();
		$dialog->Show;

		set_install_step( "VALIDATE_ALARM_INSTALLATION" );
		$graphics_object{VERIFY_CGIURL_WORKS}->configure(-state=>'normal');
		$mf->raise("Web Server");
		unbusy();
		return 1;
	} else {
		_statusmsg( "[setup] Validation Failed $vstate\n" );
		set_install_step("VALIDATE_ALARM_INSTALLATION",1);
		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
			-title=>"Validate Alarm Installation",
			-buttons=>["Ok"] );
		$dialog->Label(-text=>"Alarm System Validation FAILED: $rc")->pack();
		$dialog->Show;
		unbusy();
		return 0;
	}
}

# order to display the next items
sub get_install_checklist_order {	return @q_chklst_install_keys; };

sub get_required_install_steps {
	my(%args)=@_;
	my(@steps)=get_install_checklist_order();
	my(@rc);
	foreach ( @steps ) {
		next if $GemConfig->{INSTALLTYPE} 		eq "WINDOWS" and /UNIX/;
		next if $GemConfig->{INSTALLTYPE} 		eq "UNIX"    and /WIN32/;
		next if $GemConfig->{INSTALLTYPE} 		eq "UNIX"    and /SQLSVR/;
		next if $GemConfig->{USE_SYBASE_ASE} 	eq "N" 			and /SYBASE/;
		next if $GemConfig->{USE_SQLSERVER} 	eq "N" 			and /SQLSVR/;
		next if $GemConfig->{USE_ORACLE} 		eq "N" 			and /ORACLE/;
		#$rc{$_} = $chklst_install_keys{$_};
		push @rc, $_;
	}
	return @rc;
}

sub btn_verify_web_url {
	_statusmsg( "[verify] \n" );
	_statusmsg( "[verify] Auto Verify Web Server Button Pressed\n" );
	_statusmsg( "[verify] \n" );

	# set_install_step( "" );
	# foreach ( keys %$GemConfig ) { 	print "... ".$_." = ".$GemConfig->{$_}."\n" unless /^\_/; }
	#foreach ( sort( keys ( %{$GemConfig} ))) {		print "... ".$_." = ".$GemConfig->{$_}."\n";	}

	busy();
	my($str)="$^X -I".join(" -I",@perl_includes)." $CURRENT_CONFIG{gem_root_directory}/bin/test_verify_web_server.pl";
	_statusmsg(  "(executing) ",$str,"\n" );
	_statusmsg("[verify] \n");

	my(@rc);
	open(VAL90,$str." |" ) or die("ERROR CANT RUN $str\n");
	my($vstate)="INCOMPLETE";
	while( <VAL90> ) {
		push @rc, $_;
		_statusmsg("[rc] $_\n");
		if( /ERROR:/ or /WARNING:/ ) {
			$vstate="ERROR";
			error_messagebox("Validation Error",$_);
		} elsif( /SUCCESSFUL:/ ) {
			$vstate="OK";
		} elsif( /SKIPPED:/ ) {
			$vstate="SKIPPED";
		}
	}
	close(VAL90);
	
	if(  $vstate ne "OK" ) {
		_statusmsg( "[setup] Configuration Failed $vstate\n" );
		set_install_step("VERIFY_CGIURL_WORKS",1);
		$graphics_object{VERIFY_CGIURL_WORKS}->configure(-state=>'normal');
	
		unbusy();
		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
			-title=>"Fail - Configure Monitor Server",
			-buttons=>["Ok"] );
		$dialog->Label(-text=>"Configure Monitor Server FAILED: $rc")->pack();
		$dialog->Show;
		return 0;
	}

	set_install_step("VERIFY_CGIURL_WORKS");
	# $graphics_object{VERIFY_CGIURL_WORKS}->configure(-state=>'normal');
	unbusy();

	_statusmsg("[configure] Displaying message box - select a button to continue\n");
	$CURRENT_CONFIG{mainWindow}->messageBox( -title => "Validation OK", -message => "Monitor Station Looks Ok", -type => "OK" );
	_statusmsg("[ready] GEM Ready / Continuing...\n");


	return 1;
}

sub btn_configure_monitor_servers
{
	my($TYPE)=@_;
	if( is_nt() ) {
		if( $TYPE eq "UNIX" ) {
			validation_error("ERROR: Cant Validate Unix Monitor Station From Windows");
			return;
		}
	} else {
		if( $TYPE eq "WIN32" ) {
			validation_error("ERROR: Cant Validate Windows Monitor Station From Unix");
			return;
		}
	}

	_statusmsg("[setup] \n");
	_statusmsg("[setup] Validating $TYPE Monitor Station Setup\n");
	_statusmsg("[setup] \n");


	if( $TYPE eq "UNIX" ) { # check unix monitor station - must exist and be sane
		if( $GemConfig->{ INSTALLTYPE } ne "WINDOWS" ) {
			if( ! $GemConfig->{UNIX_MONITOR_SERVER} ) {
				validation_error("ERROR: NO UNIX MONITOR STATION DEFINED FOR ".$GemConfig->{ INSTALLTYPE }." INSTALL");
				return;
			}
		}
		set_install_step("UNIX_CONFIGURE_MONITOR_SERVER");
	}
	if( $TYPE eq "WIN32" ) {
		if( $GemConfig->{ INSTALLTYPE } ne "UNIX" ) {
			if( ! $GemConfig->{NT_MONITOR_SERVER} ) {
				validation_error("ERROR: NO UNIX MONITOR STATION DEFINED FOR ".$GemConfig->{ INSTALLTYPE }." INSTALL");
				return;
			}
		}
		set_install_step("WIN32_CONFIGURE_MONITOR_SERVER");
	}

	# what r ur variables
	my(%vars);
	if( $GemConfig->{ NT_MONITOR_SERVER } 	eq $cur_hostname ) {
		_statusmsg("[var] NT_MONITOR_SERVER \n");
		foreach ( keys %$GemConfig ) { if( /^NT_(.+)\s*=\s*(.+)$/ ) {		$vars{$1}=$2; print "Found $1 = $2\n";	}}
	} elsif ($GemConfig->{ UNIX_MONITOR_SERVER } eq $cur_hostname ) {
		_statusmsg("[var] UNIX_MONITOR_SERVER \n");
		foreach ( keys %$GemConfig ) { if( /^UNIX_(.+)\s*=\s*(.+)$/ ) {	$vars{$1}=$2; print "Found $1 = $2\n";	}}
	} elsif ($GemConfig->{ ALT1_MONITOR_SERVER } eq $cur_hostname ) {
		_statusmsg("[var] ALT1_MONITOR_SERVER \n");
		foreach ( keys %$GemConfig ) { if( /^ALT1_(.+)\s*=\s*(.+)$/ ) {	$vars{$1}=$2; print "Found $1 = $2\n";	}}
	} elsif ($GemConfig->{ ALT2_MONITOR_SERVER } eq $cur_hostname ) {
		_statusmsg("[var] ALT2_MONITOR_SERVER \n");
		foreach ( keys %$GemConfig ) { if( /^ALT2_(.+)\s*=\s*(.+)$/ ) {	$vars{$1}=$2; print "Found $1 = $2\n";	}}
	}

	# foreach ( sort keys %$GemConfig ) { print "... $_ = $GemConfig->{ $_ }\n" unless /^\_/;	}
	busy();
	my($str)="$^X -I".join(" -I",@perl_includes)." $CURRENT_CONFIG{gem_root_directory}/bin/test_configure_monitor_server.pl";
	_statusmsg(  "(executing) ",$str,"\n" );
	_statusmsg("[blank] \n");

	my($vstate)="INCOMPLETE";
	my($errmsg);

	my(@rc);
	open(VAL90,$str." |" ) or die("ERROR CANT RUN $str\n");
	while( <VAL90> ) {
		push @rc, $_;
		_statusmsg("[rc] $_\n");
		if( /ERROR:/ or /WARNING:/ ) {
			$vstate="ERROR";
			error_messagebox("Validation Error",$_);
		} elsif( /SUCCESSFUL:/ ) {
			$vstate="OK";
		} elsif( /SKIPPED:/ ) {
			$vstate="SKIPPED";
		}
	}
	close(VAL90);

	if(   $vstate eq "OK"  ) {
		$errmsg=validate_directory_structure(1);
		$vstate="ERROR" if $errmsg;
		_statusmsg("[msg] validate_directory_structure - state=$vstate $errmsg\n");
	}

	if(   $vstate eq "OK" and $TYPE eq "UNIX" ) {
		$errmsg=do_install_reformat();
		$vstate="ERROR" if $errmsg;
		_statusmsg("[msg] do_install_reformat - state=$vstate $errmsg\n");
	}

	if(  $vstate ne "OK" ) {
		_statusmsg( "[setup] Configuration Failed $vstate\n" );
		if( $TYPE eq "UNIX" ) {
			set_install_step("UNIX_CONFIGURE_MONITOR_SERVER",1);
		} elsif( $TYPE eq "WIN32" ) {
			set_install_step("WIN32_CONFIGURE_MONITOR_SERVER",1);
		}

		unbusy();
		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
			-title=>"Fail - Configure Monitor Server",
			-buttons=>["Ok"] );
		$dialog->Label(-text=>"Configure Monitor Server FAILED: $errmsg")->pack();
		$dialog->Show;
		return 0;
	}

	if( $TYPE eq "UNIX" ) { # check unix monitor station - must exist and be sane
		set_install_step("UNIX_CONFIGURE_MONITOR_SERVER");
		$graphics_object{VERIFY_CGIURL_WORKS}->configure(-state=>'normal');
	} elsif( $TYPE eq "WIN32" ) {
		set_install_step("WIN32_CONFIGURE_MONITOR_SERVER");
		$graphics_object{VERIFY_CGIURL_WORKS}->configure(-state=>'normal');
	}
	unbusy();

	_statusmsg("[configure] Displaying message box - select a button to continue\n");
	$CURRENT_CONFIG{mainWindow}->messageBox( -title => "Validation OK", -message => "Monitor Station Looks Ok", -type => "OK" );
	_statusmsg("[ready] GEM Ready / Continuing...\n");


	return 1;
}

#sub setup_verify_tab {
#	my($tabV)=@_;
#	tab_header($tabV,"FINAL GEM CONFIGURATION VERIFICATION","This tab includes all the Verify buttons for your setup.  When you are completed, you will see the Install Software tab.");
#
#	my(%verify_btn_opts)  = ( -width=>35, -padx=>3, -pady=>1, -font=>'small' );
#
#	my(%disable_on_win32,%disable_on_unix);
#	$disable_on_win32{-state}='disabled' 		if $CURRENT_CONFIG{isnt};
#	$disable_on_unix{-state}='disabled' 		if ! $CURRENT_CONFIG{isnt};
#
#	#$tabV->Label(	-font=>"norm",	-text=>"[ Hint: Now would be a good time to Save Configuration Changes! ]",
#	#	-relief=>'sunken',-bg=>'pink')->pack(qw/-side top -fill x -anchor n/);
#
#	my $verify_tab_labframe  = $tabV->LabFrame(
#     		-label=>"Validate Installation",
#     		-labelside=>'acrosstop')->pack(-side=>'left', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'e', -expand=>1);
#
#	my($row)=1;
#
#	my(%hdr_grid_opts) = ( -column=>0, -pady=>2, -padx=>2, -columnspan=>3, -sticky=>'ew' );
#   my(%bdy_grid_opts) = ( -column=>1, -pady=>2, -padx=>2, -columnspan=>1, -sticky=>'ew' );
#	my(%install_btn_opts)= ( -width=>40, -padx=>3, -pady=>1, -font=>'small' );
#
##	my $row=0;
##	my(%grid_options);
##	$grid_options{-columnspan}=3;
##	$grid_options{-pady}=2;
##	$grid_options{-padx}=2;
##	$grid_options{-column}=1;
##	$grid_options{-sticky}='ew';
#
#	$graphics_object{VERIFIED}=$verify_tab_labframe->Button(
#		-width=>40, -padx=>5, -pady=>5, -font=>'large',
#		-text=>'Manually Verify Configuration',
#		-command=> sub {
#		_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();return if is_busy();
#		set_install_major_step("VERIFIED");
#		$mf->raise('Install');
#	},
#	-bg=>'white')->grid(-row=>$row++, %hdr_grid_opts );
#   dohelp($graphics_object{VERIFIED},	"By clicking here you manually verify your configuration.");
#
#	$verify_tab_labframe->Label(	-font=>"norm",	-text=>" ")->grid(%hdr_grid_opts,-row=>$row++);
#	$verify_tab_labframe->Label(	-font=>"norm",	-text=>" ")->grid(%hdr_grid_opts,-row=>$row++);
#
#	$verify_tab_labframe->Label(	-font=>"norm",	-text=>"Validate GEM Software",
#		-relief=>'sunken',-bg=>'beige')->grid(-column=>0,-row=>$row++, -columnspan=>3, -pady=>2, -padx=>2,-sticky=>'ew');
#
##	$verify_tab_labframe->Label(	-font=>"norm",	-text=>"Validate GEM Software",
##		-relief=>'sunken',-bg=>'beige')->grid(-row=>$row++, %bdy_grid_opts);
#
#	my($bcolor)=get_install_color("UNIX_CONFIGURE_MONITOR_SERVER");
#	$bcolor='grey' if $CURRENT_CONFIG{isnt};
#	$graphics_object{I_UNIX_CONFIGURE_MONITOR_SERVER} =	$verify_tab_labframe->Button( %disable_on_win32, %verify_btn_opts,
#		-text=>'Configure Unix Monitor Station',
#		-command=> sub {
#			btn_configure_monitor_servers('UNIX');
#		}, -bg=>$bcolor  )->grid(-row=>$row++, %bdy_grid_opts);
#	dohelp(	$graphics_object{I_UNIX_CONFIGURE_MONITOR_SERVER},   		"Configure Monitor Station Set Up");
#
#	$bcolor =get_install_color("WIN32_CONFIGURE_MONITOR_SERVER");
#	$bcolor='grey' unless $CURRENT_CONFIG{isnt};
#	$graphics_object{I_WIN32_CONFIGURE_MONITOR_SERVER}
#		=	$verify_tab_labframe->Button( %disable_on_unix, %verify_btn_opts,
#		-text=>'Configure Win32 Monitor Station',
#		-command=> sub {
#			btn_configure_monitor_servers('WIN32');
#		}, -bg=>$bcolor  )->grid(-row=>$row++,%bdy_grid_opts);
#	dohelp(	$graphics_object{I_WIN32_CONFIGURE_MONITOR_SERVER},   		"Configure That Your Monitor Stations Are Set Up");
#
#	my($color)= get_install_color( "UNIX_TEST_MAIL_INSTALL" );
#	$color='white' if $GemConfig->{INSTALLTYPE} eq "UNIX";
#	$color='grey' if $CURRENT_CONFIG{isnt};
#	$graphics_object{"I_UNIX_TEST_MAIL_INSTALL"}=$verify_tab_labframe->Button( %disable_on_win32, %verify_btn_opts,
#		-text=>'Unix Test Mail Install',
#		-command=> sub {
#			btn_test_mail_install("UNIX_TEST_MAIL_INSTALL");
#		},		-bg=>$color )->grid(-row=>$row++,%bdy_grid_opts);
#	dohelp($graphics_object{"I_UNIX_TEST_MAIL_INSTALL"},
#		"By clicking here you will test Mime::Lite by sending mail to yourself $GemConfig->{ADMIN_EMAIL}.");
#
#	$color= get_install_color( "WIN32_TEST_MAIL_INSTALL" );
#	$color='white' if $GemConfig->{INSTALLTYPE} eq "UNIX";
#	$color='grey' unless $CURRENT_CONFIG{isnt};
#	$graphics_object{"I_WIN32_TEST_MAIL_INSTALL"}=$verify_tab_labframe->Button( %disable_on_unix,	%verify_btn_opts,
#		-text=>'Win32 Test Mail Install',
#		-command=> sub {
#			btn_test_mail_install("WIN32_TEST_MAIL_INSTALL");
#		},		-bg=>$color )->grid(-row=>$row++, %bdy_grid_opts);
#	dohelp($graphics_object{"I_WIN32_TEST_MAIL_INSTALL"},
#		"By clicking here you will test Mime::Lite by sending mail to yourself $GemConfig->{ADMIN_EMAIL}.");
#
#	$bcolor=get_install_color("VALIDATE_ALARM_INSTALLATION");
#   $graphics_object{I_VALIDATE_ALARM_INSTALLATION}=$verify_tab_labframe->Button(
#		%verify_btn_opts,
#		-text=>'Validate Alarm Installation',
#		-command=> sub {
#			btn_test_validate_alarm_installation();
#		},	-bg=>$bcolor )->grid(-row=>$row++, %bdy_grid_opts);
#   dohelp($graphics_object{I_VALIDATE_ALARM_INSTALLATION},		"Validate Your GEM Database");
#
##	$bcolor=get_install_color("VERIFY_WEBSERVER");
##	$graphics_object{VERIFY_WEBSERVER}=$verify_tab_labframe->Button(  -font=>"norm",
##		%verify_btn_opts,
##		-text=>'Configure Web Server Install',
##		-bg=>$bcolor,
##		-command=>sub {
##			ui_validate_webserver();
##		 })->grid(-column=>0,-row=>$row++,-sticky=>'ew');
##	dohelp($graphics_object{VERIFY_WEBSERVER}, "Test Your Web Server Installation");
#
#}

sub setup_install_software_tab {
	my($tabV)=@_;
	_debugmsg( "[setup] setup_install_software_tab()\n" );
	tab_header($tabV,"FINAL INSTALLATION STEPS","'Install Software' will run the steps listed below that button.  The other buttons are one-off installs.");

   my(%hdr_grid_opts) = ( -column=>0, -pady=>2, -padx=>2, -columnspan=>3, -sticky=>'ew' );
   my(%bdy_grid_opts) = ( -column=>1, -pady=>2, -padx=>2, -columnspan=>1, -sticky=>'ew' );
	my(%install_btn_opts)= ( -width=>40, -padx=>3, -pady=>1, -font=>'small' );

	# $tabV->Label(	-font=>"norm",	-text=>"[ Hint: Now would be a good time to Save Configuration Changes! ]",
	#	-relief=>'sunken',-bg=>'pink')->pack(qw/-side top -fill x -anchor n/);

	my $install_sw_labframe  = $tabV->LabFrame(
     		-label=>"Install The GEM Software",
     		-labelside=>'acrosstop')->pack(-side=>'right', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);

	my($row)=1;
	my($color)= get_install_color("INSTALLED");
	$graphics_object{INSTALLED}=$install_sw_labframe->Button(
		-width=>40, -padx=>5, -pady=>5, -font=>'large',
		-text=>"Manually Verify The \nInstallation Is Functioning",
		-command=> sub {
		_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();return if is_busy();
		set_install_major_step("INSTALLED");
		$mf->raise('Status');
	},	-bg=>$color)->grid(%hdr_grid_opts,-row=>$row++);
   dohelp($graphics_object{INSTALLED},	"By clicking here you manually verify your configuration.");

	$install_sw_labframe->Label(	-font=>"norm",	-text=>" ")->grid(%hdr_grid_opts,-row=>$row++);
	$install_sw_labframe->Label(	-font=>"norm",	-text=>" ")->grid(%hdr_grid_opts,-row=>$row++);

#	$install_sw_labframe->Label(	-font=>"norm",	-text=>"Required Installation Steps",
#		-relief=>'sunken',-bg=>'beige')->grid(-column=>0,-row=>$row++, -columnspan=>3, -pady=>2, -padx=>2,-sticky=>'ew');

#	my($color)= get_install_color("DO_INSTALL_REFORMAT");
#	$graphics_object{DO_INSTALL_REFORMAT}= $install_sw_labframe->Button( %install_btn_opts,
#				-text=>"Reformat The Code Line", -bg=>$color,
#				-command=>sub {
#			_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
#			return if is_busy();
#			my $rc=validate_directory_structure(1);
#			if( $rc ne "" ) {
#				$rc="Directory Structure Invalid: $rc";
#				set_status_label( undef, $rc );
#				error_messagebox("Validation Error",$rc);
#				return;
#			}
#			$rc=do_install_reformat();
#			if( $rc eq "" ) {
#				_statusmsg("[configure] Displaying message box - select a button to continue\n");
#				$CURRENT_CONFIG{mainWindow}->messageBox(
#					-title => 'Install Ok',
#					-message => 'Install Ok',
#					-type => "OK" );
#				_statusmsg("[ready] GEM Ready / Continuing...\n");
#			}
#		#} )->grid(-column=>0,-row=>$row++, -pady=>2, -padx=>2);
#		} )->grid(-row=>$row++, %bdy_grid_opts);
#
#	dohelp( $graphics_object{DO_INSTALL_REFORMAT}, "Reformat Code - Remove Ctrl-M's and Set Perl Hashbangs and Libraries");

#	my($color) = get_install_color("DO_INSTALL_CREATE_BATCHJOBS");
#	$graphics_object{DO_INSTALL_CREATE_BATCHJOBS}= $install_sw_labframe->Button( %install_btn_opts,
#		-text=>"Create GEM Batch Scripts",
#		-bg=>$color,
#		-command=>sub {
#			_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
#			return if is_busy();
#			busy();
#			do_install_create_batchjobs();
#			unbusy();
#			_statusmsg("[configure] Displaying message box - select a button to continue\n");
#			$CURRENT_CONFIG{mainWindow}->messageBox( -title =>"Batch Scripts Created" ,	-message => "BATCH SCRIPTS CREATED
#
#Windows Scripts Created in $CURRENT_CONFIG{gem_root_directory}/win32_batch_scripts.
#Unix Scripts Created in $CURRENT_CONFIG{gem_root_directory}/unix_batch_scripts.
#
#Subdirectories batch,interactive, plan_batch, plan_interactive created.
#Schedule jobs from the batch directories and use interactive to see stdout.
#The scheduler will call these pages.
#
#plan directories contain jobs required to manage your maintenance plans.",
#					-type => "OK" );
#			_statusmsg("[ready] GEM Ready / Continuing...\n");
#			}
#		 )->grid(-row=>$row++, %bdy_grid_opts);
#	dohelp(  $graphics_object{DO_INSTALL_CREATE_BATCHJOBS},
#		"Create GEM Batch Scripts\n");

	#$graphics_object{DO_CRONTAB}= $install_sw_labframe->Button( -font=>"norm",-text=>"Step 3) Create UNIX crontab files", -width=>40, -bg=>'white',
	#	-command=>sub {
	#		create_crontab();}
	#	 )->grid(-column=>0,-row=>$row++, -pady=>2, -padx=>2);
	#dohelp(  $graphics_object{DO_CRONTAB},"Create UNIX crontab files");

#	$graphics_object{FINAL_INSTALL_BACKUP_MGR}=$install_sw_labframe->Button( -font=>"norm",-text=>"Step 3) Backup Manager", -width=>40, -bg=>'white',
#		-command=>sub {
#			return if is_busy();
#			my $rc=validate_backup_scripts();
#			if( $rc ne "" ) {
#				$rc="Backup Manager Plugin Variables FAILED: $rc";
#				set_status_label( undef, $rc );
#				error_messagebox("Validation Error",$rc);
#				return ;
#			}
#			$rc = do_install_backup_scripts();
#			if( $rc eq "" ) {
#				_statusmsg("[configure] Displaying message box - select a button to continue\n");
#				$CURRENT_CONFIG{mainWindow}->messageBox(
#					-title => 'Install Ok',
#					-message => 'Install Ok',
#					-type => "OK" );
#				_statusmsg("[ready] GEM Ready / Continuing...\n");
#			}
#		},-bg=>$color
#		 )->grid(-column=>0,-row=>$row++, -pady=>2, -padx=>2);
#	dohelp($graphics_object{FINAL_INSTALL_BACKUP_MGR},"Install Backup Management Software");

#	$color= get_install_color("DO_INSTALL_CONSOLE");
#	$graphics_object{DO_INSTALL_CONSOLE} = $install_sw_labframe->Button(
#		%install_btn_opts,
#		-text=>"Install GEM Console",
#		-command=>sub {
#			_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
#			return if is_busy();
#			my $rc = validate_console();
#			if( $rc ne "" ) {
#				$rc="Console Plugin Variables FAILED: $rc";
#				set_status_label( undef, $rc );
#				error_messagebox("Validation Error",$rc);
#				return;
#			}
#			$rc = do_install_console();
#			if( $rc eq "" ) {
#				_statusmsg("[configure] Displaying message box - select a button to continue\n");
#				$CURRENT_CONFIG{mainWindow}->messageBox(
#					-title => 'Install Ok',
#					-message => 'Install Ok',
#					-type => "OK" );
#				_statusmsg("[ready] GEM Ready / Continuing...\n");
#			}
#		},-bg=>$color
#		 )->grid(-row=>$row++, %bdy_grid_opts);
#	dohelp($graphics_object{DO_INSTALL_CONSOLE}, "Modify Console Scripts As Appropriate");

#	my($color)=get_install_color("INSTALL_ALARM_DATABASE");
#	$graphics_object{I_INSTALL_ALARM_DATABASE}=$install_sw_labframe->Button( %install_btn_opts,
#		-text=>"Install/Upgrade Alarm Database",
#		-bg=>$color,
#		-command=>sub {
#			btn_install_gemalarms();
#		},		-bg=>$color		 )->grid(-row=>$row++, %bdy_grid_opts);
#	dohelp(	$graphics_object{I_INSTALL_ALARM_DATABASE},	"Install or Upgrade The GEM Database");
#
#   my($bcolor)=get_install_color("POPULATE_LOCAL_CGI_BIN");
#	$graphics_object{I_POPULATE_LOCAL_CGI_BIN}=$install_sw_labframe->Button(  %install_btn_opts,
#		-text=>'Populate local cgi-bin',
#		-bg=>$bcolor,
#		-command=>sub {
#			btn_populate_local_cgibin();
#		 })->grid(-row=>$row++, %bdy_grid_opts);
#	dohelp($graphics_object{I_POPULATE_LOCAL_CGI_BIN}, "Populate $CURRENT_CONFIG{gem_root_directory}/cgi-bin");
#
#	$bcolor=get_install_color("COPY_CGI_BIN_TO_WEBSERVER");
#	$graphics_object{I_COPY_CGI_BIN_TO_WEBSERVER}=$install_sw_labframe->Button( %install_btn_opts,
#		-text=>'Copy local cgi-bin to Web Server',
#		-bg=>$bcolor,
#		-command=>sub {
#			btn_copy_local_cgi_to_websvr();
#		 })->grid(-row=>$row++, %bdy_grid_opts);
#	dohelp($graphics_object{I_COPY_CGI_BIN_TO_WEBSERVER}, "Copy $CURRENT_CONFIG{gem_root_directory}/cgi-bin To Target");
}

sub do_item {
	my($current_subfram,$lbl,$valueref,$row,$col,$type)=@_;
	$current_subfram->Label(-font=>"norm",	-text=>$lbl   )->grid(-column=>$col++, -row=>$row,-padx=>2,-pady=>2,-sticky=>'e');
	if( defined $type and $type eq "Entry" ) {
		return $current_subfram->Entry(-font=>"norm",	-bg=>"white", -textvariable=>$valueref,-width=>40)->grid(-column=>$col,   -row=>$row,-padx=>2,-pady=>2,-sticky=>'w');
	} else {
		my($val)=$$valueref;
		$val="(Undefined)" unless $val;
		return $current_subfram->Label(-font=>"norm",	-text=>$val )->grid(-column=>$col,   -row=>$row,-padx=>2,-pady=>2,-sticky=>'w');
	}
}

sub setup_monitors_tab {
	my($current_tab)=@_;

	tab_header($current_tab,"GEM MONITORING STATIONS",
"Gem Monitor Stations can only be added by running configure.pl on them. Samba Installs require that you run configure.pl on ALL monitor stations before they will VERIFY.  On UNIX only installs, ignore the Win32 Monitor Station Section.  On Windows only Installs, ignore the UNIX section.");

	# FIRST - FIND CTHIS SERVER
	my($found_me)="FALSE";
	$found_me='TRUE' if 	$GemConfig->{ NT_MONITOR_SERVER } 	eq $cur_hostname
							or $GemConfig->{ UNIX_MONITOR_SERVER } eq $cur_hostname
							or $GemConfig->{ ALT1_MONITOR_SERVER } eq $cur_hostname
							or $GemConfig->{ ALT2_MONITOR_SERVER } eq $cur_hostname;

	_statusmsg( "[configure] This Monitor Server Has been Found = $found_me\n" );

	if( $found_me eq "FALSE" and $REGISTER_AS_MONSRV ) {
		_statusmsg( "[setup] Adding New Monitor Station $cur_hostname\n" );
		if( $CURRENT_CONFIG{isnt} ) {
			if( ! $GemConfig->{ NT_MONITOR_SERVER } ) {
				$GemConfig->{ NT_MONITOR_SERVER } = $cur_hostname;
				$GemConfig->{ NT_PERL_LOCATION }  = $^X;
				$GemConfig->{ NT_CODE_LOCATION }  = $CURRENT_CONFIG{gem_root_directory};
				$GemConfig->{ NT_CODE_FULLSPEC }	 = find_fullspec($GemConfig->{NT_CODE_LOCATION}) if $^O =~ /Win32/i;		
				$GemConfig->{ NT_FULLPATH } 		 = $CURRENT_CONFIG{gem_root_directory};
				$GemConfig->{ NT_FULLPATH } 		 =~ s/\//\\/g;
		   	$GemConfig->{ NT_FULLPATH } 		 =~ s/:/\$/g;
				$GemConfig->{ NT_FULLPATH } 		 = "\\\\".$cur_hostname."\\". $GemConfig->{ NT_FULLPATH };
			} elsif( ! $GemConfig->{ ALT1_MONITOR_SERVER } ) {
				$GemConfig->{ ALT1_MONITOR_SERVER } = $cur_hostname;
				$GemConfig->{ ALT1_PERL_LOCATION }  = $^X;
				$GemConfig->{ ALT1_CODE_LOCATION }  = $CURRENT_CONFIG{gem_root_directory};
				$GemConfig->{ ALT1_IS_NT }  = "YES";
				$GemConfig->{ ALT1_FULLPATH } 	= $CURRENT_CONFIG{gem_root_directory};
		   	$GemConfig->{ ALT1_FULLPATH } =~ s/\//\\/g;
				$GemConfig->{ ALT1_FULLPATH } =~ s/:/\$/g;
				$GemConfig->{ ALT1_FULLPATH } = "\\\\".$cur_hostname."\\". $GemConfig->{ ALT1_FULLPATH };
			} elsif( ! $GemConfig->{ ALT2_MONITOR_SERVER } ) {
				$GemConfig->{ ALT2_MONITOR_SERVER } = $cur_hostname;
				$GemConfig->{ ALT2_PERL_LOCATION }  = $^X;
				$GemConfig->{ ALT2_CODE_LOCATION }  = $CURRENT_CONFIG{gem_root_directory};
				$GemConfig->{ ALT2_IS_NT }  = "YES";
				$GemConfig->{ ALT2_FULLPATH } 	= $CURRENT_CONFIG{gem_root_directory};
				$GemConfig->{ ALT2_FULLPATH } =~ s/\//\\/g;
				$GemConfig->{ ALT2_FULLPATH } =~ s/:/\$/g;
				$GemConfig->{ ALT2_FULLPATH } = "\\\\".$cur_hostname."\\". $GemConfig->{ ALT2_FULLPATH };
			}
		} else {
			if( ! $GemConfig->{ UNIX_MONITOR_SERVER } ) {
				$GemConfig->{ UNIX_MONITOR_SERVER } = $cur_hostname;
				$GemConfig->{ UNIX_PERL_LOCATION }  = $^X;
				$GemConfig->{ UNIX_CODE_LOCATION }  = $CURRENT_CONFIG{gem_root_directory};
			} elsif( ! $GemConfig->{ ALT1_MONITOR_SERVER } ) {
				$GemConfig->{ ALT1_MONITOR_SERVER } = $cur_hostname;
				$GemConfig->{ ALT1_PERL_LOCATION }  = $^X;
				$GemConfig->{ ALT1_CODE_LOCATION }  = $CURRENT_CONFIG{gem_root_directory};
				$GemConfig->{ ALT1_IS_NT }  = "NO";
			} elsif( ! $GemConfig->{ ALT2_MONITOR_SERVER } ) {
				$GemConfig->{ ALT2_MONITOR_SERVER } = $cur_hostname;
				$GemConfig->{ ALT2_PERL_LOCATION }  = $^X;
				$GemConfig->{ ALT2_CODE_LOCATION }  = $CURRENT_CONFIG{gem_root_directory};
				$GemConfig->{ ALT2_IS_NT }  = "NO";
			}
		}
	}

	# OK NOW DISPLAY THE STUFF
	my $current_subfram  = $current_tab->LabFrame(-label=>"GEM MONITOR SYSTEMS",
   		-labelside=>'acrosstop')->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);

   my($counter)=0;
	my($row)=1;

	# LAYOUT    NT ... UNIX \n ALT1  ... ALT2
	# FIRST THE LABEL	THEN THE DETAILS

	# SEPARATOR BETWEEN THE ITEMS
	$current_subfram->Label(-font=>"norm",	-text=>"", -width=>10   )->grid(-column=>3, -row=>0,-padx=>2,-pady=>2);

	$graphics_object{WIN32_MONITOR_STATION_L} = $current_subfram->Label(-font=>"norm",	-text=>"WIN32 MONITOR STATION", -relief=>'sunken',-bg=>'beige'   )->grid(-column=>1, -row=>0,-padx=>2,-pady=>2,-sticky=>'ew',-columnspan=>2,);
	$graphics_object{WIN32_MONITOR_STATION_NAME} = do_item($current_subfram,"Monitor Station",\$GemConfig->{ NT_MONITOR_SERVER },1,1);
	$graphics_object{WIN32_MONITOR_STATION_PERL} = do_item($current_subfram,"Preferred Perl",\$GemConfig->{ NT_PERL_LOCATION },2,1);
	$graphics_object{WIN32_MONITOR_STATION_CODE} = do_item($current_subfram,"Code Location",\$GemConfig->{ NT_CODE_LOCATION },3,1);

	#if(  ! $GemConfig->{ NT_MONITOR_SERVER } ) {
	#	do_item('Full Path To Code (e.g. \\\\svr\c$\gem)',\$GemConfig->{ NT_FULLPATH },4,1 );
	#} else {
		$graphics_object{WIN32_MONITOR_STATION_FPATH} = do_item($current_subfram,'Full Path To Code (e.g. \\svr\c$\gem)',\$GemConfig->{ NT_FULLPATH },4,1,"Entry");
	#}

	$graphics_object{UNIX_MONITOR_STATION_L} = $current_subfram->Label(-font=>"norm",	-text=>"UNIX MONITOR STATION", -relief=>'sunken',-bg=>'beige'   )->grid(-column=>4, -row=>0,-padx=>2,-pady=>2,-sticky=>'ew',-columnspan=>2,);
	$graphics_object{UNIX_MONITOR_STATION_NAME} = do_item($current_subfram,"Monitor Station",\$GemConfig->{ UNIX_MONITOR_SERVER },1,4);
	$graphics_object{UNIX_MONITOR_STATION_PERL} = do_item($current_subfram,"Preferred Perl",\$GemConfig->{ UNIX_PERL_LOCATION },2,4);
	$graphics_object{UNIX_MONITOR_STATION_CODE} = do_item($current_subfram,"Code Location",\$GemConfig->{ UNIX_CODE_LOCATION },3,4);
	#if( ! $GemConfig->{ UNIX_MONITOR_SERVER } ) {
		#do_item("Path To Web Browser",\$GemConfig->{ UNIX_BROWSER_LOCATION },4,4 );
	#} else {
	#	$graphics_object{UNIX_MONITOR_STATION_FPATH} = do_item("Path To Web Browser",\$GemConfig->{ UNIX_BROWSER_LOCATION },4,4,"Entry");
	#}


# $graphics_object{LabelPrefUnixBrowser}	=	$current_subfram->Label(	-font=>"norm",	-text=>"Unix/Linux Web Browser",	-relief=>'sunken',-bg=>'beige');
# $graphics_object{PrefUnixBrowser}	=	$current_subfram->Entry(-font=>"norm",	-bg=>$color,-textvariable=>\$GemConfig->{UNIX_BROWSER_LOCATION},-width=>40);
#dohelp(	$graphics_object{PrefUnixBrowser},
#  		"Location of Web Browser Executable For Unix and Linux (/usr/local/bin/firefox)");

	my($BASE)=7;
	if( $GemConfig->{ NT_MONITOR_SERVER } ) {
		dohelp(
		$current_subfram->Button(	-width=>22, -text=>'Delete Win32 Station', -bg=>'white', -command=> sub {
				if( $GemConfig->{ NT_MONITOR_SERVER } eq hostname() ) {
					_statusmsg("Cant Delete The Currently Running Monitor");
					error_messagebox("Validation Error","Cant Delete The Monitor Station you are running on\n");
					return;
				}
				$GemConfig->{ NT_MONITOR_SERVER } = '';
				$GemConfig->{ NT_PERL_LOCATION } = '';
				$GemConfig->{ NT_CODE_LOCATION } = '';
				$GemConfig->{ NT_FULLPATH } = '';
				set_install_step("WIN32_CONFIGURE_MONITOR_SERVER",1);
			} )->grid(-column=>0, -row=>($BASE-1),-padx=>2,-pady=>2,-columnspan=>2),
			"Deletes The Monitoring Station From GEM");
	}
	if( $GemConfig->{ UNIX_MONITOR_SERVER } ) {
		dohelp(
		$current_subfram->Button(	-width=>22, -text=>'Delete Unix Station', -bg=>'white', -command=> sub {
				if( $GemConfig->{ NT_MONITOR_SERVER } eq hostname() ) {
					_statusmsg("Cant Delete The Currently Running Monitor");
					error_messagebox("Validation Error","Cant Delete The Monitor Station you are running on\n");
					return;
				}
				$GemConfig->{ UNIX_MONITOR_SERVER } = '';
				$GemConfig->{ UNIX_PERL_LOCATION } = '';
				$GemConfig->{ UNIX_CODE_LOCATION } = '';
		#		$GemConfig->{ UNIX_BROWSER_LOCATION } = '';
				set_install_step("UNIX_CONFIGURE_MONITOR_SERVER",1);
			} )->grid(-column=>4, -row=>($BASE-1),-padx=>2,-pady=>2),
			"Deletes The Monitoring Station From GEM");

		$current_subfram->Label(-font=>"norm",	-text=>"", -width=>10 )->grid(-column=>3, -row=>($BASE-1),-padx=>2,-pady=>2);
	}

	if( $GemConfig->{ ALT1_MONITOR_SERVER } ) {
		$current_subfram->Label(-font=>"norm",	-text=>"ALTERNATE MONITOR STATION", -relief=>'sunken',-bg=>'beige'   )->grid(-column=>1, -row=>$BASE,-padx=>2,-pady=>2,-sticky=>'ew',-columnspan=>2,);
		do_item($current_subfram,"Monitor Station",\$GemConfig->{ ALT1_MONITOR_SERVER },($BASE+1),1);
		do_item($current_subfram,"Preferred Perl",\$GemConfig->{ ALT1_PERL_LOCATION } ,($BASE+2),1);
		do_item($current_subfram,"Code Location",\$GemConfig->{ ALT1_CODE_LOCATION }  ,($BASE+3),1);
		do_item($current_subfram,"Is Win32",\$GemConfig->{ ALT1_IS_NT }               ,($BASE+4),1);
		do_item($current_subfram,"Full Path To Code",\$GemConfig->{ ALT1_FULLPATH }   ,($BASE+5),1,"Entry") if $GemConfig->{ ALT1_IS_NT } eq "YES";

		my($str);
		if( $GemConfig->{ ALT1_IS_NT } ) {
			$str="REPLACE ".$GemConfig->{ NT_MONITOR_SERVER }." WITH THIS SERVER";
		} else {
			$str="REPLACE ".$GemConfig->{ UNIX_MONITOR_SERVER }." WITH THIS SERVER";
		}
		$current_subfram->Button(	-width=>40,	-text=>$str, -bg=>"white", -command=> sub {
				if( $GemConfig->{ ALT1_IS_NT } ) {
					$GemConfig->{ NT_MONITOR_SERVER }	= $GemConfig->{ ALT1_MONITOR_SERVER };
					$GemConfig->{ NT_PERL_LOCATION } 	= $GemConfig->{ ALT1_PERL_LOCATION };
					$GemConfig->{ NT_CODE_LOCATION } 	= $GemConfig->{ ALT1_CODE_LOCATION };
					$GemConfig->{ NT_IS_NT } 			= $GemConfig->{ ALT1_IS_NT };
					$GemConfig->{ NT_FULLPATH } 		= $GemConfig->{ ALT1_FULLPATH };
				} else {
					$GemConfig->{ UNIX_MONITOR_SERVER }	= $GemConfig->{ ALT1_MONITOR_SERVER };
					$GemConfig->{ UNIX_PERL_LOCATION } 	= $GemConfig->{ ALT1_PERL_LOCATION };
					$GemConfig->{ UNIX_CODE_LOCATION } 	= $GemConfig->{ ALT1_CODE_LOCATION };
					$GemConfig->{ UNIX_IS_NT } 			= $GemConfig->{ ALT1_IS_NT };
					$GemConfig->{ UNIX_FULLPATH } 		= $GemConfig->{ ALT1_FULLPATH };
				}
				delete $GemConfig->{ ALT1_MONITOR_SERVER };
				delete $GemConfig->{ ALT1_PERL_LOCATION };
				delete $GemConfig->{ ALT1_CODE_LOCATION };
				delete $GemConfig->{ ALT1_IS_NT };
				delete $GemConfig->{ ALT1_FULLPATH };
				_statusmsg("[configure] Displaying message box - select a button to continue\n");
				$CURRENT_CONFIG{mainWindow}->messageBox( -title => "Server Replaced", -message => "Please Save Configuration Changes And Restart configure.pl", -type => "OK" );
				_statusmsg("[ready] GEM Ready / Continuing...\n");

			} )->grid(-column=>1, -row=>($BASE+6),-padx=>2,-pady=>2,-columnspan=>2,);
	}

	if( $GemConfig->{ ALT2_MONITOR_SERVER } ) {
		$current_subfram->Label(-font=>"norm",	-text=>"ALTERNATE MONITOR STATION", -relief=>'sunken',-bg=>'beige'   )->grid(-column=>4, -row=>$BASE,-padx=>2,-pady=>2,-sticky=>'ew',-columnspan=>2,);
		do_item($current_subfram,"Monitor Station",\$GemConfig->{ ALT2_MONITOR_SERVER },($BASE+1),4);
		do_item($current_subfram,"Preferred Perl",\$GemConfig->{ ALT2_PERL_LOCATION } ,($BASE+2),4);
		do_item($current_subfram,"Code Location",\$GemConfig->{ ALT2_CODE_LOCATION }  ,($BASE+3),4);
		do_item($current_subfram,"Is Win32",\$GemConfig->{ ALT2_IS_NT }               ,($BASE+4),4);
		dohelp(
			do_item($current_subfram,"Full Path To Code",\$GemConfig->{ ALT2_FULLPATH }   ,($BASE+5),4,"Entry"),
			'Enter Full Path For Code Directory ie.  \\hostname\share\directory')
			if $GemConfig->{ ALT2_IS_NT } eq "YES";
		my($str);
		if( $GemConfig->{ ALT2_IS_NT } ) {
			$str="REPLACE ".$GemConfig->{ NT_MONITOR_SERVER }." WITH THIS SERVER";
		} else {
			$str="REPLACE ".$GemConfig->{ UNIX_MONITOR_SERVER }." WITH THIS SERVER";
		}
		$current_subfram->Button(	-width=>40,	-text=>$str, -bg=>"white",	-command=> sub {
				if( $GemConfig->{ ALT2_IS_NT } ) {
					$GemConfig->{ NT_MONITOR_SERVER }	= $GemConfig->{ ALT2_MONITOR_SERVER };
					$GemConfig->{ NT_PERL_LOCATION } 	= $GemConfig->{ ALT2_PERL_LOCATION };
					$GemConfig->{ NT_CODE_LOCATION } 	= $GemConfig->{ ALT2_CODE_LOCATION };
					$GemConfig->{ NT_IS_NT } 			= $GemConfig->{ ALT2_IS_NT };
					$GemConfig->{ NT_FULLPATH } 		= $GemConfig->{ ALT2_FULLPATH };
				} else {
					$GemConfig->{ UNIX_MONITOR_SERVER }	= $GemConfig->{ ALT2_MONITOR_SERVER };
					$GemConfig->{ UNIX_PERL_LOCATION } 	= $GemConfig->{ ALT2_PERL_LOCATION };
					$GemConfig->{ UNIX_CODE_LOCATION } 	= $GemConfig->{ ALT2_CODE_LOCATION };
					$GemConfig->{ UNIX_IS_NT } 			= $GemConfig->{ ALT2_IS_NT };
					$GemConfig->{ UNIX_FULLPATH } 		= $GemConfig->{ ALT2_FULLPATH };
				}
				delete $GemConfig->{ ALT2_MONITOR_SERVER };
				delete $GemConfig->{ ALT2_PERL_LOCATION };
				delete $GemConfig->{ ALT2_CODE_LOCATION };
				delete $GemConfig->{ ALT2_IS_NT };
				delete $GemConfig->{ ALT2_FULLPATH };
				_statusmsg("[configure] Displaying message box - select a button to continue\n");
				$CURRENT_CONFIG{mainWindow}->messageBox( -title => "Server Replaced", -message => "Please Save Configuration Changes And Restart configure.pl", -type => "OK" );
				_statusmsg("[ready] GEM Ready / Continuing...\n");

			} )->grid(-column=>4, -row=>($BASE+6),-padx=>2,-pady=>2,-columnspan=>2,);
	}

	if( $GemConfig->{ INSTALLTYPE } eq "SAMBA" ) {
		$current_subfram->Label(-font=>"norm",	-text=>"CODE DIRECTORY ALIASES ALIASES (SAMBA INSTALLS)", -relief=>'sunken',-bg=>'beige'   )->grid(-column=>4, -row=>($BASE+7),-padx=>2,-pady=>2,-sticky=>'ew',-columnspan=>2,);
		dohelp(
			do_item($current_subfram,"Alt 1 For Code Directory",\$GemConfig->{ CODE_LOCATION_ALT1 },($BASE+8),4,"Entry"),
			"You are on a samba share - first alternate location for the code locations mentioned above.\nUse a full URL path name you didnt do it above.");
		dohelp(
			do_item($current_subfram,"Alt 2 For Code Directory",\$GemConfig->{ CODE_LOCATION_ALT2 },($BASE+9),4,"Entry"),
			"You are on a samba share - second alternate location for the code locations mentioned above.\nUse a full URL path name you didnt do it above.");
   }

   $current_subfram->Label(-font=>"norm",	-text=>"", -width=>10 )->grid(-column=>3, -row=>($BASE+10),-padx=>2,-pady=>2);

   my(%disable_on_win32,%disable_on_unix);
	$disable_on_win32{-state}='disabled' 		if $CURRENT_CONFIG{isnt};
	$disable_on_unix{-state}='disabled' 		if ! $CURRENT_CONFIG{isnt};

   my($bcolor)=get_install_color("WIN32_CONFIGURE_MONITOR_SERVER");
   $bcolor='grey' unless $CURRENT_CONFIG{isnt};
	$graphics_object{WIN32_CONFIGURE_MONITOR_SERVER}
		=	$current_subfram->Button(	%disable_on_unix, -width=>30,	-font=>'large', -text=>'Configure Win32 Monitor Station',	-bg=>$bcolor, -command=> sub {
			btn_configure_monitor_servers('WIN32');
		} )->grid(-column=>1, -row=>($BASE+11),-padx=>2,-pady=>2,-columnspan=>2);
	dohelp(	$graphics_object{WIN32_CONFIGURE_MONITOR_SERVER},   		"Configure Your Win32 Monitor Station $disable_on_unix{-state}");

   $bcolor=get_install_color("UNIX_CONFIGURE_MONITOR_SERVER");
   $bcolor='grey' if $CURRENT_CONFIG{isnt};
	$graphics_object{UNIX_CONFIGURE_MONITOR_SERVER}
		=	$current_subfram->Button(	%disable_on_win32, -width=>30,	-font=>'large', -text=>'Configure Unix Monitor Station',	-bg=>$bcolor, -command=> sub {
			btn_configure_monitor_servers('UNIX');
		} )->grid(-column=>4, -row=>($BASE+11),-padx=>2,-pady=>2,-columnspan=>2);
	dohelp(	$graphics_object{UNIX_CONFIGURE_MONITOR_SERVER},   		"Configure Unix Monitor Station $disable_on_win32{-state}");

#	my($bcolor)=get_install_color("VERIFY_MONITOR_SERVERS");
#	$graphics_object{VERIFY_MONITOR_SERVERS}
#		=	$current_subfram->Button(	-width=>30,	-text=>'Verify Monitor Station',	-bg=>$bcolor, -command=> sub {
#			my($ok)="YES";
#			_statusmsg("[setup] Validating Monitor Station Setup\n");
#			print "DBG DBG",__LINE__,$GemConfig->{ INSTALLTYPE }, "\n";
#
#			my(@rc)=test_verify_monitor_servers( $GemConfig );
#			my($vstate)="INCOMPLETE";
#			foreach (@rc) {
#				chomp;
#				_statusmsg("[verify] $_\n");
#				if( /ERROR:/ or /WARNING:/ ) {
#					$vstate="ERROR";
#					if( /ERROR: NO/ and /SERVER DEFINED FOR SAMBA INSTAL/ ) {
#						error_messagebox("Validation Error","YOU HAVE NOT RUN configure.pl ON ALL YOUR MONITORING SERVERS\n$_");
#					} else {
#						error_messagebox("Validation Error",$_);
#					}
#				} elsif( /SUCCESSFUL:/ ) {
#					$vstate="OK";
#				} elsif( /SKIPPED:/ ) {
#					$vstate="SKIPPED";
#				}
#			}
#			_statusmsg("[setup] Validation=$ok\n");
#			set_install_step("VERIFY_MONITOR_SERVERS",1)  unless $vstate eq "OK";
#			return unless $vstate eq "OK";
#			$CURRENT_CONFIG{mainWindow}->messageBox( -title => "Validation OK", -message => "Monitor Station Looks Ok", -type => "OK" );
#			set_install_step("VERIFY_MONITOR_SERVERS");

}

my( $password_vault_hashref );
sub read_password_vault {
	if( ! defined $password_vault_hashref ) {
		my(%x);
		$password_vault_hashref = \%x;
	}
	my($f)=$CURRENT_CONFIG{gem_configuration_directory}."/password_vault.dat";
	return unless -r $f;
	open(VLT,$f) or die "Cant Read $f";
	while(<VLT>) {
			chop;
			chomp;
			next if /^\s*\#/ or /^\s*$/;
			my(@data)=split(/;/,$_);
			if( $#data == 2 ) {			# two columns
				my($KEY) = uc($data[0]);
				# SAME SERVER
				if( defined $password_vault_hashref->{ "VAULT_".$KEY."1" }
				and         $password_vault_hashref->{ "VAULT_".$KEY."1" } eq $data[1] ) {
					if( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."1" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."1" } = $data[2];
					} elsif( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."2" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."2" } = $data[2];
					} elsif( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."3" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."3" } = $data[2];
					}
				# NO SERVERS
				} elsif( ! defined $password_vault_hashref->{ "VAULT_".$KEY."1" } ) {
					$password_vault_hashref->{ "VAULT_".$KEY."1" } = $data[1];
					if( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."1" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."1" } = $data[2];
					} elsif( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."2" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."2" } = $data[2];
					} elsif( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."3" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."3" } = $data[2];
					}
				# DIFF SERVERS

				} elsif( ! defined $password_vault_hashref->{ "VAULT_".$KEY."2" } ) {
					$password_vault_hashref->{ "VAULT_".$KEY."2" } = $data[1];
					if( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."4" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."4" } = $data[2];
					} elsif( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."5" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."5" } = $data[2];
					} elsif( ! defined $password_vault_hashref->{ "VAULTPASS_".$KEY."6" } ) {
						$password_vault_hashref->{ "VAULTPASS_".$KEY."6" } = $data[2];
					}
				} else {
					_statusmsg("[error] Third $KEY Server Found In Vault Ignoring\n");
				}

#
# no need for this here as the data is allready stored in gemconfig
#
#			} elsif( $#data == 1 and $data[0] =~ /^unix/ ) {
#				my($a)=$data[0];
#				$a=~s/^unix//;
#				$a=upper($a);
#				if( $GemConfig->{"UNIX_".$a."1"} ) {
#					$GemConfig->{"UNIX_".$a."1"} = $data[1];
#				} else {
#					$GemConfig->{"UNIX_".$a."2"} = $data[1];
#				}
#	   	} else {
#	   		die "FATAL ERROR - CANT PARSE $_\n";
	   	}
	}
	close(VLT);
}

sub write_password_vault {
	my($f)=$CURRENT_CONFIG{gem_configuration_directory}."/password_vault.dat";
	open(VLT,">".$f) or die "Cant Write $f";
	print VLT "#
# PASSWORD VAULT
#
# UNIX LOGINS
# HOSTTYPE;login
#
# GENERIC DB LOGINS FROM THE VAULT
# HOSTTYPE;SALOGIN;PASSWORD
#
# where HOSTTYPE ==> oracle|sybase|sqlsvr|unix
#\n";
	foreach my $KEY ("ORACLE","MYSQL","SYBASE") {
		if( defined $password_vault_hashref->{ "VAULT_".$KEY."1" } ){
			print VLT lc($KEY).";".$password_vault_hashref->{ "VAULT_".$KEY."1" }.";".$password_vault_hashref->{ "VAULTPASS_".$KEY."1" }."\n"
				if  $password_vault_hashref->{ "VAULTPASS_".$KEY."1" };
			print VLT lc($KEY).";".$password_vault_hashref->{ "VAULT_".$KEY."1" }.";".$password_vault_hashref->{ "VAULTPASS_".$KEY."2" }."\n"
				if  $password_vault_hashref->{ "VAULTPASS_".$KEY."2" };
			print VLT lc($KEY).";".$password_vault_hashref->{ "VAULT_".$KEY."1" }.";".$password_vault_hashref->{ "VAULTPASS_".$KEY."3" }."\n"
				if  $password_vault_hashref->{ "VAULTPASS_".$KEY."3" };
		}
		if( defined $password_vault_hashref->{ "VAULT_".$KEY."2" } ){
			print VLT lc($KEY).";".$password_vault_hashref->{ "VAULT_".$KEY."2" }.";".$password_vault_hashref->{ "VAULTPASS_".$KEY."4" }."\n"
				if  $password_vault_hashref->{ "VAULTPASS_".$KEY."4" };
			print VLT lc($KEY).";".$password_vault_hashref->{ "VAULT_".$KEY."2" }.";".$password_vault_hashref->{ "VAULTPASS_".$KEY."5" }."\n"
				if  $password_vault_hashref->{ "VAULTPASS_".$KEY."5" };
			print VLT lc($KEY).";".$password_vault_hashref->{ "VAULT_".$KEY."2" }.";".$password_vault_hashref->{ "VAULTPASS_".$KEY."6" }."\n"
				if  $password_vault_hashref->{ "VAULTPASS_".$KEY."6" };
		}
	}

	foreach my $a ( keys %$GemConfig ) {
		next unless $a =~ /^UNIX_(.+)(\d)$/;
		my($ty2,$id)=($1,$2);
		print VLT "unix",lc($ty2),";",$GemConfig->{$a},"\n";
	}

	close(VLT);
}


sub setup_password_vault_tab {
	my($tabL)=@_;

	tab_header($tabL,"Password Vault",
'Enter commonly used administration passwords here. These will be used by the discovery process.');

	my $sybase_subframe  = $tabL->LabFrame(-label=>"SYBASE VAULT",-labelside=>'acrosstop');
	my $mysql_subframe   = $tabL->LabFrame(-label=>"MYSQL VAULT",-labelside=>'acrosstop');
	my $oracle_subframe  = $tabL->LabFrame(-label=>"ORACLE VAULT",-labelside=>'acrosstop');

	my($logincolor)='beige';	# get_install_color("SAVE_CONFIGURATION_CHANGES");
	my($color)='white';	# get_install_color("SAVE_CONFIGURATION_CHANGES");

   for (1..2) {
   	$graphics_object{"LblVltSybase".$_}			=	$sybase_subframe->Label(	-font=>"norm",	-text=>"Sybase 'sa' Login #".$_,	-relief=>'sunken',-bg=>$logincolor);
		$graphics_object{"LblVltMysql".$_}			=	$mysql_subframe->Label(	   -font=>"norm",	-text=>"Mysql Login #".$_,	-relief=>'sunken',-bg=>$logincolor);
		$graphics_object{"LblVltOracle".$_}			=	$oracle_subframe->Label(	-font=>"norm",	-text=>"Oracle Login #".$_,	-relief=>'sunken',-bg=>$logincolor);

   	$graphics_object{"EntVltSybase".$_}			=	$sybase_subframe->Entry(-font=>"norm",	-bg=>$logincolor, -textvariable=>\$password_vault_hashref->{"VAULT_SYBASE".$_},-width=>40);
   	$graphics_object{"EntVltMysql".$_}			=	$mysql_subframe->Entry(-font=>"norm",	-bg=>$logincolor, -textvariable=>\$password_vault_hashref->{"VAULT_MYSQL".$_},-width=>40);
   	$graphics_object{"EntVltOracle".$_}			=	$oracle_subframe->Entry(-font=>"norm",	-bg=>$logincolor, -textvariable=>\$password_vault_hashref->{"VAULT_ORACLE".$_},-width=>40);

   	dohelp(	$graphics_object{"EntVltSybase".$_},  	"#".$_." Sybase Password Used For Connections");
   	dohelp(	$graphics_object{"EntVltMysql".$_},  	"#".$_." Mysql Password Used For Connections");
   	dohelp(	$graphics_object{"EntVltOracle".$_},	"#".$_." Oracle Password Used For Connections");
   }

   for (1..6) {
   	$graphics_object{"VltLblSybase".$_}			=	$sybase_subframe->Label(	-font=>"norm",	-text=>"Sybase Password #".$_,	-relief=>'sunken',-bg=>$color);
		$graphics_object{"VltLblMysql".$_}			=	$mysql_subframe->Label(	-font=>"norm",	-text=>"Mysql Password #".$_,	-relief=>'sunken',     -bg=>$color);
		$graphics_object{"VltLblOracleHome".$_}	=	$oracle_subframe->Label(	-font=>"norm",	-text=>"Orahome Password #".$_,	-relief=>'sunken',-bg=>$color);

   	$graphics_object{"VltPassSybase".$_}		=	$sybase_subframe->Entry(-font=>"norm",	-bg=>$color,-textvariable=>\$password_vault_hashref->{"VAULTPASS_SYBASE".$_},-width=>40);
   	$graphics_object{"VltPassMysql".$_}		 	=	$mysql_subframe->Entry(-font=>"norm",	-bg=>$color,-textvariable=>\$password_vault_hashref->{"VAULTPASS_MYSQL".$_},-width=>40);
   	$graphics_object{"VltPassOracleHome".$_}	=	$oracle_subframe->Entry(-font=>"norm",	-bg=>$color,-textvariable=>\$password_vault_hashref->{"VAULTPASS_ORACLE".$_},-width=>40);

   	dohelp(	$graphics_object{"VltPassSybase".$_},   		"#".$_." Sybase Directory To Check For Scripts");
   	dohelp(	$graphics_object{"VltPassMysql".$_},  			"#".$_." Mysql Directory To Check For Scripts");
   	dohelp(	$graphics_object{"VltPassOracleHome".$_},		"#".$_." Oracle Home Directory To Check For Scripts");
   }

	my($row)=1;

	$graphics_object{"LblVltSybase1"}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntVltSybase1"}->grid(-column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"LblVltSybase2"}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntVltSybase2"}->grid(-column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $row++;

	for ( 1..3 ) {
		my($i)=$_;
		$graphics_object{"VltLblSybase".$i}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"VltPassSybase".$i}->grid( -column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$i+=3;
   	$graphics_object{"VltLblSybase".$i}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"VltPassSybase".$i}->grid( -column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$row++;
   }

   $graphics_object{"LblVltMysql1"}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntVltMysql1"}->grid(-column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"LblVltMysql2"}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntVltMysql2"}->grid(-column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $row++;

	for ( 1..3 ) {
		my($i)=$_;
		$graphics_object{"VltLblMysql".$i}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"VltPassMysql".$i}->grid( -column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$i+=3;
   	$graphics_object{"VltLblMysql".$i}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"VltPassMysql".$i}->grid( -column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$row++;
   }

   $graphics_object{"LblVltOracle1"}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntVltOracle1"}->grid(-column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"LblVltOracle2"}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntVltOracle2"}->grid(-column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $row++;

	for ( 1..3 ) {
		my($i)=$_;
		$graphics_object{"VltLblOracleHome".$i}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"VltPassOracleHome".$i}->grid( -column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$i+=3;
   	$graphics_object{"VltLblOracleHome".$i}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"VltPassOracleHome".$i}->grid( -column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$row++;
   }

   $sybase_subframe->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);
	$mysql_subframe->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);
	$oracle_subframe->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);
}

sub setup_locations_tab {
	my($tabL)=@_;

	tab_header($tabL,"MAP DATABASE HOME DIRECTORIES",
'Enter all the $SYBASE and $ORAHOME directories you use in prefered order. This will be used by the discovery process. List all possible directories in their native OS format (C:\sybase D:\sybase /apps/sybase).');

	my $sybase_subframe  = $tabL->LabFrame(-label=>"SYBASE DISCOVERY DIRECTIVES",-labelside=>'acrosstop');
	my $mysql_subframe   = $tabL->LabFrame(-label=>"MYSQL DISCOVERY DIRECTIVES",-labelside=>'acrosstop');
	my $oracle_subframe  = $tabL->LabFrame(-label=>"ORACLE DISCOVERY DIRECTIVES",-labelside=>'acrosstop');

	my($color)='white';	# get_install_color("SAVE_CONFIGURATION_CHANGES");

   for (1..2) {
   	$graphics_object{"LblUnxSybase".$_}			=	$sybase_subframe->Label(	-font=>"norm",	-text=>"Sybase Unix Account #".$_,	-relief=>'sunken',-bg=>'beige');
		$graphics_object{"LblUnxMysql".$_}			=	$mysql_subframe->Label(	   -font=>"norm",	-text=>"Mysql Unix Account #".$_,	-relief=>'sunken',-bg=>'beige');
		$graphics_object{"LblUnxOracle".$_}			=	$oracle_subframe->Label(	-font=>"norm",	-text=>"Oracle Unix Account #".$_,	-relief=>'sunken',-bg=>'beige');

   	$graphics_object{"EntUnxSybase".$_}			=	$sybase_subframe->Entry(-font=>"norm",	-bg=>'beige',-textvariable=>\$GemConfig->{"UNIX_SYBASE".$_},-width=>40);
   	$graphics_object{"EntUnxMysql".$_}			=	$mysql_subframe->Entry(-font=>"norm",	-bg=>'beige',-textvariable=>\$GemConfig->{"UNIX_MYSQL".$_},-width=>40);
   	$graphics_object{"EntUnxOracle".$_}			=	$oracle_subframe->Entry(-font=>"norm",	-bg=>'beige',-textvariable=>\$GemConfig->{"UNIX_ORACLE".$_},-width=>40);

   	dohelp(	$graphics_object{"EntUnxSybase".$_},   		"#".$_." Sybase Unix Account Used For Connections");
   	dohelp(	$graphics_object{"EntUnxMysql".$_},  			"#".$_." Mysql Unix Account Used For Connections");
   	dohelp(	$graphics_object{"EntUnxOracle".$_},		   "#".$_." Oracle Unix Account Used For Connections");
   }

   for (1..6) {
   	$graphics_object{"LabelSybase".$_}			=	$sybase_subframe->Label(	-font=>"norm",	-text=>"Sybase Location #".$_,	-relief=>'sunken',-bg=>$color);
		$graphics_object{"LabelMysql".$_}			=	$mysql_subframe->Label(	-font=>"norm",	-text=>"Mysql Location #".$_,	-relief=>'sunken',     -bg=>$color);
		$graphics_object{"LabelOracleHome".$_}		=	$oracle_subframe->Label(	-font=>"norm",	-text=>"Orahome Location #".$_,	-relief=>'sunken',-bg=>$color);

   	$graphics_object{"PrefSybase".$_}			=	$sybase_subframe->Entry(-font=>"norm",	-bg=>$color,-textvariable=>\$GemConfig->{"SYBASE".$_},-width=>40);
   	$graphics_object{"PrefMysql".$_}		 		=	$mysql_subframe->Entry(-font=>"norm",	-bg=>$color,-textvariable=>\$GemConfig->{"MYSQL".$_},-width=>40);
   	$graphics_object{"PrefOracleHome".$_}		=	$oracle_subframe->Entry(-font=>"norm",	-bg=>$color,-textvariable=>\$GemConfig->{"ORAHOME".$_},-width=>40);

   	dohelp(	$graphics_object{"PrefSybase".$_},   		"#".$_." Sybase Directory To Check For Scripts");
   	dohelp(	$graphics_object{"PrefMysql".$_},  			"#".$_." Mysql Directory To Check For Scripts");
   	dohelp(	$graphics_object{"PrefOracleHome".$_},		"#".$_." Oracle Home Directory To Check For Scripts");
   }

	my($row)=1;

	$graphics_object{"LblUnxSybase1"}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntUnxSybase1"}->grid(-column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"LblUnxSybase2"}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntUnxSybase2"}->grid(-column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $row++;

	for ( 1..3 ) {
		my($i)=$_;
		$graphics_object{"LabelSybase".$i}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"PrefSybase".$i}->grid( -column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$i+=3;
   	$graphics_object{"LabelSybase".$i}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"PrefSybase".$i}->grid( -column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$row++;
   }

   $graphics_object{"LblUnxMysql1"}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntUnxMysql1"}->grid(-column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"LblUnxMysql2"}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntUnxMysql2"}->grid(-column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $row++;

	for ( 1..3 ) {
		my($i)=$_;
		$graphics_object{"LabelMysql".$i}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"PrefMysql".$i}->grid( -column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$i+=3;
   	$graphics_object{"LabelMysql".$i}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"PrefMysql".$i}->grid( -column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$row++;
   }

   $graphics_object{"LblUnxOracle1"}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntUnxOracle1"}->grid(-column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"LblUnxOracle2"}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{"EntUnxOracle2"}->grid(-column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   $row++;

	for ( 1..3 ) {
		my($i)=$_;
		$graphics_object{"LabelOracleHome".$i}->grid(-column=>1, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"PrefOracleHome".$i}->grid( -column=>2, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$i+=3;
   	$graphics_object{"LabelOracleHome".$i}->grid(-column=>3, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$graphics_object{"PrefOracleHome".$i}->grid( -column=>4, -row=>$row,-padx=>2,-pady=>2,-sticky=>'ew');
   	$row++;
   }

   $sybase_subframe->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);
	$mysql_subframe->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);
	$oracle_subframe->pack(	-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);
}

sub setup_register_tab {
	my($tabR)=@_;
	tab_header($tabR,"REGISTER YOUR INSTALLATION",
		"Registration is required. Information gathered will not be shared with outside parties. ");

   my $tabR_labfrm  = $tabR->LabFrame(-label=>"PRODUCT REGISTRATION",
   	-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);
   my $current_subfram = $tabR_labfrm->Frame()->pack(-side => "top", -fill=>'x');

   my($row)=0;

	$current_subfram->Label(-text=>" ")->grid(-column=>1,-row=>$row++);	# blank
   $current_subfram->Label(-font=>"norm",-text=>"GEM Administrator Name",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>$row++,-padx=>2,-pady=>2,-sticky=>'ew');
   $current_subfram->Label(-font=>"norm",-text=>"Company Name",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>$row++,-padx=>2,-pady=>2,-sticky=>'ew');
   $current_subfram->Label(-font=>"norm",-text=>"GEM Administrator Email",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>$row++,-padx=>2,-pady=>2,-sticky=>'ew');
   $current_subfram->Label(-text=>" ")->grid(-column=>1,-row=>$row++);	# blank
  # $current_subfram->Label(-font=>"norm",-text=>"GEM Administrator Phone   ",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>5,-padx=>2,-pady=>2,-sticky=>'ew');
   $current_subfram->Label(-font=>"norm",-text=>"Environment",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>$row++,-padx=>2,-pady=>2,-sticky=>'ew');
   $row++;$row++;
   $current_subfram->Label(-text=>" ")->grid(-column=>1,-row=>$row++);	# blank
   $current_subfram->Label(-font=>"norm",-text=>"Database Products",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>$row++,-padx=>2,-pady=>2,-sticky=>'ew');
   $current_subfram->Label(-text=>" ")->grid(-column=>1,-row=>$row++);	# blank
#   $current_subfram->Label(-font=>"norm",-text=>"GEM Configuration",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>$row++,-padx=>2,-pady=>2,-sticky=>'ew');

   $row=0;
   $current_subfram->Label(-text=>" ")->grid(-column=>2,-row=>$row++);	# blank
	my($color)=get_install_color("REGISTERED");
   $graphics_object{FLD_ADMIN_NAME}=$current_subfram->Entry(-font=>"norm",-bg=>$color, -textvariable=>\$GemConfig->{ADMIN_NAME},-width=>35)->grid(-column=>2,-row=>$row++,-sticky=>'w');
   dohelp( 	$graphics_object{FLD_ADMIN_NAME},
      	"Enter The Name Of the Tool Administrator (ie. your name)."   );
   $graphics_object{FLD_ADMIN_COMP}=$current_subfram->Entry(-font=>"norm",-bg=>$color, -textvariable=>\$GemConfig->{ADMIN_COMPANY},-width=>35)->grid(-column=>2,-row=>$row++,-sticky=>'w');
   dohelp( 	$graphics_object{FLD_ADMIN_COMP},
   	"Enter the name of your Company"   );
   $graphics_object{FLD_ADMIN_MAIL}=$current_subfram->Entry(-font=>"norm",-bg=>$color, -textvariable=>\$GemConfig->{ADMIN_EMAIL},-width=>35)->grid(-column=>2,-row=>$row++,-sticky=>'w');
   dohelp( 	$graphics_object{FLD_ADMIN_MAIL},
   	"Enter the name of the Email for the tool administrator"   );

	$current_subfram->Label(-text=>" ")->grid(-column=>2,-row=>$row++);	# blank
   dohelp( 	$current_subfram->Radiobutton(-font=>"norm",
   		-text=>"Linux or Unix Servers Only",
   		-command => \&redraw_installtype,
   		-variable=>\$GemConfig->{INSTALLTYPE},-value=>"UNIX")->grid(-column=>2,-row=>$row++,-sticky=>'w'),
   	"Select if you have all your servers on Unix");
   dohelp( 	$current_subfram->Radiobutton(-font=>"norm",
   		-text=>"Unix/Linux/Windows Hybrid With This Software on a Samba Share",
   		-command => \&redraw_installtype,
   		-variable=>\$GemConfig->{INSTALLTYPE},-value=>"SAMBA")->grid(-column=>2,-row=>$row++,-sticky=>'w'),
   	"Select if you have a hybrid environment and run off a samba share");
   dohelp( 	$current_subfram->Radiobutton(-font=>"norm",
   		-text=>"Windows Servers Only",-variable=>\$GemConfig->{INSTALLTYPE},
   		-command => \&redraw_installtype,
   		-value=>"WINDOWS")->grid(-column=>2,-row=>$row++,-sticky=>'w'),
   	"Select if you have only Windows Servers" );

	$current_subfram->Label(-text=>" ")->grid(-column=>2,-row=>$row++);	# blank
	my($frm_dbtouse) = $current_subfram->Frame()->grid(-column=>2, -row=>$row++, -sticky=>'w');
	$frm_dbtouse->Checkbutton(
		-font=>"norm",
		-variable=>\$GemConfig->{USE_SYBASE_ASE},
		-onvalue=>'Y',
		-command=> \&redraw_productset,
		-offvalue=>'N',
		-text=>"Sybase ASE")->pack(-side=>'left');

	$frm_dbtouse->Checkbutton(
		-font=>"norm",
		-variable=>\$GemConfig->{USE_SYBASEREP},
		-onvalue=>'Y',
		-offvalue=>'N',
		-command=> \&redraw_productset,
		-text=>"Sybase Replication")->pack(-side=>'left');

	$frm_dbtouse->Checkbutton(
		-font=>"norm",
		-variable=>\$GemConfig->{USE_ORACLE},
		-onvalue=>'Y',
		-command=> \&redraw_productset,
		-offvalue=>'N',
		-text=>"Oracle Database")->pack(-side=>'left');

	$frm_dbtouse->Checkbutton(
		-font=>"norm",
		-variable=>\$GemConfig->{USE_SQLSERVER},
		-onvalue=>'Y',
		-offvalue=>'N',
		-command=> \&redraw_productset,
		-text=>"Microsoft SQL Server")->pack(-side=>'left');

	$frm_dbtouse->Checkbutton(
		-font=>"norm",
		-variable=>\$GemConfig->{USE_MYSQL},
		-onvalue=>'Y',
		-offvalue=>'N',
		-command=> \&redraw_productset,
		-text=>"Mysql")->pack(-side=>'left');

	$current_subfram->Label(-text=>" ")->grid(-column=>2,-row=>$row++);	# blank

	verify_license_key(1);
	# Section Commented Out
	# Please REtain in case we want to re-add license verification
#	$graphics_object{GemRpt_TYPE} = $current_subfram->Frame()->grid(-column=>2, -row=>$row++, -sticky=>'w');

#   dohelp($graphics_object{GemRpt_TYPE}->Radiobutton(-font=>"norm",
#   		-text=>"Free GEM (4 Database Maximum)",
#   		-variable=>\$GemConfig->{IS_FREE_GEM},
#   		-command =>  \&verify_license_key,
#   		-value=>"YES")->pack(-side=>'left'),
#   	"Use FREE GEM or This is a Trial" );
#
#   dohelp($graphics_object{GemRpt_TYPE}->Radiobutton(-font=>"norm",
#   		-text=>"Full GEM (License Key Required)",
#   		-variable=>\$GemConfig->{IS_FREE_GEM},
#   		-command => \&verify_license_key,
#   		-value=>"NO")->pack(-side=>'left'),
#   	"Full Version of GEM - License Key Required" );

# hand set  by ed as this isnt used anymore 5/15/10
	$GemConfig->{UNIX_TO_UNIX_FTP}='N';
	$GemConfig->{UNIX_TO_UNIX_RSH}='N';
	$GemConfig->{UNIX_TO_UNIX_SSH}='Y';

   my $bframe = $tabR_labfrm->Frame()->pack(-side => "bottom", -fill=>'x');

   $graphics_object{REGISTERED}=$bframe->Button(
	-width=>25,
	-text=>'REGISTER YOUR VERSION',
	-command=> sub {
		_statusmsg("[button] the system is busy - this is probably an error\n") if is_busy();return if is_busy();

		_statusmsg("[configure] \n");
		_statusmsg("[configure] Register Your Version Button Pressed\n");
		_statusmsg("[configure] \n");

		if( $GemConfig->{ADMIN_NAME}  =~ /^\s*$/ ) {
			set_install_step("REGISTERED",1);
			error_messagebox ( "Validation Error","ERROR: Must Fill In GEM Administrator Name");
			return;
		}
		if( $GemConfig->{ADMIN_EMAIL}  =~ /^\s*$/ ) {
			set_install_step("REGISTERED",1);
			error_messagebox ( "Validation Error","ERROR: Must Fill In GEM Administrator Email");
			return;
		}
		if( $GemConfig->{ADMIN_EMAIL}  !~ /\@/ ) {
			set_install_step("REGISTERED",1);
			error_messagebox ( "Validation Error","ERROR: Must Fill In GEM Administrator Email Not Legit");
			return;
		}
		if( $GemConfig->{ADMIN_NAME}  =~ /^\s*$/ ) {
			set_install_step("REGISTERED",1);
			error_messagebox ( "Validation Error","ERROR: Must Fill In GEM Administrator Name");
			return;
		}

#		busy();
#		_statusmsg( "[button] Register With The Central Web Site\n" );
#		WebRegister::register_on_the_web(
#			$GemConfig->{ADMIN_NAME},
#			$GemConfig->{ADMIN_EMAIL},
#			"5551212",
#			$CURRENT_CONFIG{company} );
#		unbusy();

		set_install_major_step("REGISTERED");
		$mf->raise('Paths');
		$graphics_object{FLD_ADMIN_NAME}->configure(-bg=>'white');
		$graphics_object{FLD_ADMIN_COMP}->configure(-bg=>'white');;
		$graphics_object{FLD_ADMIN_MAIL}->configure(-bg=>'white');
	},
	-bg=>$color )->pack( -side=>"right", -pady=>2, -padx=>2);
#   dohelp($graphics_object{REGISTERED},"By clicking here you register with the store and get your download key.");
}

sub tab_header {
	my($tab,$title,$text)=@_;
	$text=reword($text,115,120);
	my $Hdr_Frame = $tab->Frame(-relief=>"groove", -bg=>'beige', -borderwidth=>1)->pack(-side => "top", -pady=>5, -padx=>10, -fill=>'x');
	$Hdr_Frame->Label(
				-font=>"header",
				-bg=>'beige',
				-text=>$title)->pack(-fill=>'x', -side=>'top');
	$Hdr_Frame->Label(
				-font=>'norm',
				-bg=>'beige',
				-justify=>'left',
				-text=>$text)->pack(-fill=>'x', -side=>'top') if $text ne "";
}

sub btn_test_mail_install
{
	my($BNAME,$no_ui)=@_;
	_statusmsg("[mail] the system is busy - this is probably an error\n");
	return if is_busy();

	_statusmsg("[mail] \n");
	_statusmsg("[mail] Validating Mail Setup\n");
	_statusmsg("[mail] \n");

	if( $CURRENT_CONFIG{isnt} and ! $GemConfig->{MAIL_HOST} ) {
		error_messagebox('Data Entry Error','ERROR: Must Fill In SMTP Mail Server');
		set_install_step( $BNAME,1 );
		return 0;
	}

	if( $GemConfig->{ADMIN_EMAIL} =~ /^\s*$/ or $GemConfig->{ADMIN_EMAIL} !~ /\@/ ) {
		error_messagebox('Data Entry Error','ERROR: Must Fill In Administrator Email');
		set_install_step( $BNAME,1 );
		return 0;
	}

	busy();
	_statusmsg( "[mail] Test - sending Mail to $GemConfig->{ADMIN_EMAIL}\n" );
	my($msg)=MIME::Lite->new(
				To=>	$GemConfig->{ADMIN_EMAIL},
				From=>$GemConfig->{ADMIN_EMAIL},
				Subject=>"gem install",
				Type=>'multipart/related' );
	if( ! $msg ) {
		error_messagebox('Data Entry Error','ERROR: Mail Test Fails');
		set_install_step( $BNAME,1 );
		unbusy();
		return 0;
	}

	$msg->attach(Type=>'text/html', Data=>"Gem Install: time=".scalar(localtime(time))."\n");
	my($rc);
	eval {
		if( $CURRENT_CONFIG{isnt} ) {
			_statusmsg( "[mail] Test - sending Mail via Mailhost '",$GemConfig->{MAIL_HOST},"'\n" );
			$rc = $msg->send("smtp",$GemConfig->{MAIL_HOST});
		} else {
			_statusmsg( "[mail] Test - sending Unix Mail\n" );
			$rc = $msg->send();
		}
	};
	my($err)=join("",$@) if $@;
	_statusmsg( "[mail] Mail Returned: ".$err ) if $err;
	if( $err ) {
		error_messagebox('Data Entry Error','ERROR: '.$err);
		set_install_step( $BNAME,1 );
		unbusy();
		return 0;
	}

	#if( $vstate ne "OK" ) {
	#	_statusmsg( "[configure] Testing Failed - Mail Not Ok\n" );
	#	set_install_step( $BNAME,1 );
	#	error_messagebox('Install GEM - Mail Check', 'ERROR : Mail was not sent to '.$GemConfig->{ADMIN_EMAIL});
		#$GemConfig->{is_mail_ok}="FALSE";
		#_statusmsg("[configure] Displaying message box - select a button to continue\n");
		#$CURRENT_CONFIG{mainWindow}->messageBox(
		#	-title => 'Install GEM - Mail Check',
		#	-message => 'ERROR : Mail was not sent to '.$GemConfig->{ADMIN_EMAIL},
		#	-type => "OK" );
		#_statusmsg("[ready] GEM Ready / Continuing...\n");
	#	return 0;
	#} else {
		# $GemConfig->{is_mail_ok}="TRUE";
		_statusmsg( "[mail] Testing Succeeded - Mail OK\n" );
		set_install_step($BNAME);
		$graphics_object{WIN32_SMTP_Server}->configure(-bg=>'white') if defined $graphics_object{WIN32_SMTP_Server};
		if( ! $no_ui ) {
			$mf->raise('Database');
			_statusmsg("[mail] Displaying message box - select a button to continue\n");
			$CURRENT_CONFIG{mainWindow}->messageBox(
				-title => 'Install GEM - Mail Check',
				-message => 'Mail sent to '.$GemConfig->{ADMIN_EMAIL},
				-type => "OK" );
			_statusmsg("[mail] GEM Ready / Continuing...\n");
		}
		unbusy();
		return 1;
	#}
}

sub setup_mail_tab {
	my($tabP)=@_;
	tab_header($tabP,"MAIL SETUP","Test Mail Facilities.  A SMTP Mail Server is required if you use a windows monitoring station.");

   my(%disable_on_win32,%disable_on_unix);
	$disable_on_win32{-state}='disabled' 		if $CURRENT_CONFIG{isnt};
	$disable_on_unix{-state}='disabled' 		if ! $CURRENT_CONFIG{isnt};

	#
	# WIN32 MAIL WIDGET
	#
 	my $tab_mail_labrame_win32  = $tabP->LabFrame(-label=>"Win32 Mail Server Setup",
	   	-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);

	my $mail_topframe1 = $tab_mail_labrame_win32->Frame()->pack( -side=>'top' ); # -side => "left", -fill=>'x', -anchor=>'w');
 	my $mail_topframe2 = $tab_mail_labrame_win32->Frame()->pack( -side=>'top' ); # -side => "left", -fill=>'x', -anchor=>'w');

 	$graphics_object{WIN32_SMTP_Server_L}=$mail_topframe2->Label(%disable_on_unix, -font=>"norm",-text=>"SMTP Mail Server (Windows only)",-relief=>'sunken',-bg=>'beige')->grid(-column=>1, -row=>1,-padx=>2,-pady=>2,-sticky=>'ew');
   $graphics_object{WIN32_SMTP_Server}  =$mail_topframe2->Entry(%disable_on_unix, -font=>"norm",-textvariable=>\$GemConfig->{MAIL_HOST},-width=>40	 )->grid(-column=>2,-row=>1,-sticky=>'w');
   dohelp($graphics_object{WIN32_SMTP_Server},"Enter the SMTP Mail Server (ie. mail1.xyz.com) "   );

   my($color) = get_install_color( "WIN32_TEST_MAIL_INSTALL" );
	$graphics_object{"WIN32_TEST_MAIL_INSTALL"}=$mail_topframe1->Button(
		%disable_on_unix,
		-width=>25,
		-text=>'Win32 Test Mail Install',
		-command=> sub {	btn_test_mail_install("WIN32_TEST_MAIL_INSTALL");	},
		-bg=>$color )->pack();	# grid(-column=>1,-row=>0);
		# pack( -side=>"right", -pady=>2, -padx=>2);
	dohelp($graphics_object{WIN32_TEST_MAIL_INSTALL},
		"By clicking here you will test Mime::Lite by sending mail to yourself $GemConfig->{ADMIN_EMAIL}.");

	#
	# UNIX MAIL WIDGET
	#
 	my $tab_mail_labrame_unix  = $tabP->LabFrame(-label=>"Unix Mail Server Setup",
	   	-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -fill=>'both', -pady=>15, -anchor=>'w', -expand=>1);

 	my $mail_botframe = $tab_mail_labrame_unix->Frame()->pack(); #-side => "left", -fill=>'x', -anchor=>'w');

	$color= get_install_color( "UNIX_TEST_MAIL_INSTALL" );
	$color='white' if $GemConfig->{INSTALLTYPE} eq "UNIX";
	$color='grey' if $CURRENT_CONFIG{isnt};
	$graphics_object{"UNIX_TEST_MAIL_INSTALL"}=$mail_botframe->Button(
		%disable_on_win32,
		-width=>25,
		-text=>'Unix Test Mail Install',
		-command=> sub { 	btn_test_mail_install("UNIX_TEST_MAIL_INSTALL");	},
		-bg=>$color )->pack();
		# grid(-column=>1,-row=>2,-sticky=>'w');
		#pack( -side=>"right", -pady=>2, -padx=>2);
	dohelp( $graphics_object{UNIX_TEST_MAIL_INSTALL},
	"By clicking here you will test Mime::Lite by sending mail to yourself $GemConfig->{ADMIN_EMAIL}.");
}

#sub btn_populate_local_cgibin {
#	my($no_gui)=@_;
#	_statusmsg("[populate] the system is busy - this is probably an error\n") if is_busy();
#	return if is_busy();
#
#	_statusmsg( "[populate] \n" );
#	_statusmsg( "[populate] Populate Local Cgi-Bin Button Pressed\n" );
#	_statusmsg( "[populate] \n" );
#
#	my($INSTALL_STEP_NAME)='POPULATE_LOCAL_CGI_BIN';
#
#	#
#	# PRE FLIGHT CHECKLIST
#	#
#	if( $GemConfig->{MIMI_IS_WIN32} eq "Y" ) {
#		if( ! defined $GemConfig->{MIMI_FILENAME_WIN32} or $GemConfig->{MIMI_FILENAME_WIN32} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill In Full Path Name');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( is_nt() and ! -d $GemConfig->{MIMI_FILENAME_WIN32} ) {
#			error_messagebox('Data Entry Error','ERROR: Directory Does Not Exist '.$GemConfig->{MIMI_FILENAME_WIN32});
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( is_nt() and ! -w $GemConfig->{MIMI_FILENAME_WIN32} ) {
#			error_messagebox('Data Entry Error','ERROR: Directory Is Not Writable '.$GemConfig->{MIMI_FILENAME_WIN32});
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( ! defined $GemConfig->{MIMI_URL_WIN32} or $GemConfig->{MIMI_URL_WIN32} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill In Url');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( ! defined $GemConfig->{MIMI_CGIBIN_PERL_WIN32} or $GemConfig->{MIMI_CGIBIN_PERL_WIN32} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill In Perl To Use');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( ! defined $GemConfig->{MIMI_LOGIN_WIN32} or $GemConfig->{MIMI_LOGIN_WIN32} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill A Windows Id For A Login');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#
#		if( ! is_nt() ) {
#			error_messagebox('Data Entry Error','ERROR: Your Web Server Is On Win32 so you must Populate From Win32.');
#			# set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#	} elsif( $GemConfig->{MIMI_IS_WIN32} eq "N" ) {
#		if( ! defined $GemConfig->{MIMI_HOSTNAME_UNIX} or $GemConfig->{MIMI_HOSTNAME_UNIX} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill In Web Server Hostname');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( ! defined $GemConfig->{MIMI_FILENAME_UNIX} or $GemConfig->{MIMI_FILENAME_UNIX} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill Unix File To Use');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( ! defined $GemConfig->{MIMI_URL_UNIX} or $GemConfig->{MIMI_URL_UNIX} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill In Unix URL To Use');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( ! defined $GemConfig->{MIMI_CGIBIN_PERL_UNIX} or $GemConfig->{MIMI_CGIBIN_PERL_UNIX} =~ /^\s*$/ ) {
#			error_messagebox('Data Entry Error','ERROR: Must Fill In Perl To Use');
#			set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#		if( is_nt() ) {
#			error_messagebox('Data Entry Error','ERROR: Your Web Server Is On Unix so you must Populate From Unix.');
#			# set_install_step( $INSTALL_STEP_NAME,1 );
#			return 0;
#		}
#	} else {
#		error_messagebox('Data Entry Error','ERROR: Unknown - Is Web server On Win32');
#		set_install_step( $INSTALL_STEP_NAME,1 );
#		return 0;
#	}
#
#	# COPY STUFF TO LOCAL CGI BIN
#	my($CGI_DIR)="$CURRENT_CONFIG{gem_root_directory}/cgi-bin";
#
#	# rebuild the alarm file with approriate variables
#	rebuild_alarm_file("$CURRENT_CONFIG{gem_root_directory}/lib/MlpAlarm.pm");
#
#	# copy all necessary files to the cgi-bin
#	_statusmsg("[populate] Populating $CGI_DIR\n");
#	busy();
#
#	if( blind_dircopy($CURRENT_CONFIG{gem_root_directory}."/ADMIN_SCRIPTS/cgi-bin",$CGI_DIR,			1,undef) == 0	) {
#		set_install_step( $INSTALL_STEP_NAME,1  );
#		unbusy(1);
#		return 0;
#	}
#
#	if( blind_dircopy($CURRENT_CONFIG{gem_root_directory}."/lib",			$CGI_DIR,			0,".pm") == 0 ) {
#		set_install_step( $INSTALL_STEP_NAME,1  );
#		unbusy(1);
#		return 0;
#	}
#
#	if( blind_dircopy($CURRENT_CONFIG{gem_root_directory}."/lib/MIME",	$CGI_DIR."/MIME",	1,".pm") == 0 ){
#		set_install_step( $INSTALL_STEP_NAME,1  );
#		unbusy(1);
#		return 0;
#	}
#
#	if( blind_dircopy($CURRENT_CONFIG{gem_root_directory}."/lib/Tie",					$CGI_DIR."/Tie", 	1,".pm") == 0 ) {
#		set_install_step( $INSTALL_STEP_NAME,1  );
#		unbusy(1);
#		return 0;
#	}
#
#	# remove hostname if its local
#	#$GemConfig->{MIMI_HOSTNAME_UNIX}='' if $GemConfig->{MIMI_IS_WIN32} eq "N" and $GemConfig->{MIMI_HOSTNAME_UNIX} eq hostname();
#
#	my($perl);
#	if( $GemConfig->{MIMI_IS_WIN32} eq "Y" ) {
#		$perl=$GemConfig->{MIMI_CGIBIN_PERL_WIN32};
#	} else {
#		$perl=$GemConfig->{MIMI_CGIBIN_PERL_UNIX};
#	}
#
#	for my $i (1..9) {
#		$GemConfig->{"SYBASE".$i} =~ s/\s+$//;
#		$GemConfig->{"SYBASE".$i}	=~ s.\\.\/.g;
#	}
#
#	my($c)=$GemConfig->{ADMIN_COMPANY};
#	$c=~s/\"/\'/g;
#
#	_statusmsg("[populate] perl=$perl company=$c\n");
#
#	# edit .pl files from target dir
#	_statusmsg("[populate] Rebuilding $CGI_DIR\n");
#	opendir(DIR,$CGI_DIR) || die("copy_cgibin() Can't open directory ".$CGI_DIR."/cgi-bin for reading\n");
#	my(@from_perl_scripts) = grep(/\.pl$/,readdir(DIR));
#	closedir(DIR);
#	if( $#from_perl_scripts < 3 ) {
#		error_messagebox('Error',"ERROR: Could not find enough perl scripts files in $CGI_DIR");
#		unbusy(1);
#		return 0;
#	}
#
#	#foreach my $x qw( mimi.pl webgem.pl printenv.pl web_login_changer.pl ) {
#	foreach my $x ( @from_perl_scripts ) {
#
#	my($file)="$CGI_DIR/$x";
#	_statusmsg("[populate] ... Rebuilding $file\n");
#	open(A79,"$file")	   or return l_warning("Cant Read $file : $!");
#	open(B78,">$file.tmp") or return l_warning("Cant Write $file.tmp : $!");
#	print B78 "#!".$perl."
#use lib qw(.);
#my(\$COMPANYNAME)=\"".$GemConfig->{ADMIN_COMPANY}."\";
#
#BEGIN {
#	if( ! defined \$ENV{SYBASE} or ! -d \$ENV{SYBASE} ) {
#		if( 1==2 ) { print 'a silly way to save coding time'; \n";
#
#	for my $i (1..9) {
#		if( defined $GemConfig->{"SYBASE".$i} and $GemConfig->{"SYBASE".$i} =~ /\// ) {
#				$GemConfig->{"SYBASE".$i} =~ s/\s+$//;
#				print B78 "\t\t} elsif( -d \"".$GemConfig->{"SYBASE".$i}."\" ) {\n\t\t\t\$ENV{SYBASE}=\"".$GemConfig->{"SYBASE".$i}."\";\n";
#		}
#	}
#	print B78 "\t\t}\n\t}\n}\n# done header";
#
#	#my($found)=0;
#	foreach (<A79>) {
#		print B78 $_ unless /^#!/ or /^use lib/;
#		#$found=1 if /^}/;
#		#print B78 $_ if $found;
#	}
#	close(A79);
#	close(B78);
#	rename($file.".tmp",$file) or return l_warning("Cant rename $file.tmp to $file : $!");
#	chmod( 0555, $file) or warn "Cant Chmod $file $! \n";
#	}
#
#	unbusy(1);
#	if( ! $no_gui ) {
#		_statusmsg("[populate] Starting messageBox()\n");
#		$CURRENT_CONFIG{mainWindow}->messageBox(
#					-title => 'Directory Populated',
#					-message => "Populated $CGI_DIR\n\nCopy this directory to your web server with the \"Copy local cgi-bin to Web Server\" button.",
#					-type => "OK" );
#		_statusmsg("[populate] GEM Ready / Continuing...\n");
#	}
#	set_install_step( $INSTALL_STEP_NAME );
#	_statusmsg("[populate] btn_populate_local_cgibin() succeeded - returning 1\n");
#	return 1;
#}

sub btn_install_gemalarms {
	_statusmsg("[setup] the system is busy - this is probably an error\n") if is_busy();
	return if is_busy();
	_statusmsg( "[setup] \n" );
	_statusmsg( "[setup] Install/Upgrade Alarm Database Button Pressed\n" );
	_statusmsg( "[setup] \n" );
	busy();
	my($rc)=install_gemalarms();
	if( $rc ) {
		_statusmsg( "[setup] Install/Upgrade Alarm Database Succeeded\n" );
		set_install_step( "INSTALL_ALARM_DATABASE" );
	} else {
		_statusmsg( "[setup] Install/Upgrade Alarm Database Failed???\n" );
		set_install_step( "INSTALL_ALARM_DATABASE",1 );
	}
	unbusy();
}

sub btn_copy_local_cgi_to_websvr
{
	
	foreach ( keys %$GemConfig ) {
		print "DBGDBG", $_," ",$GemConfig->{$_},"\n" if /^MIMI/;
	}
	_statusmsg(  "Rollout Directory = $GemConfig->{MIMI_FILENAME_WIN32} \n" );
	
	if( ! $GemConfig->{MIMI_FILENAME_WIN32} ) {
		error_messagebox("Validation Error","No Rollout Directory");
		return;
	}
	if( ! -w $GemConfig->{MIMI_FILENAME_WIN32} ) {
		error_messagebox("Validation Error","Rollout Directory Not Writable");
		return;
	}
	
	my($str)="$^X -I".join(" -I",@perl_includes)." $CURRENT_CONFIG{gem_root_directory}/bin//rollout_web_server.pl --ROLLOUT_DIRECTORY=$GemConfig->{MIMI_FILENAME_WIN32} 2>&1 \n";
	_statusmsg(  "(executing) ",$str );
	my(@rc);
	open(VAL90,$str." |" ) or die("ERROR CANT RUN $str\n");
	my($vstate)="INCOMPLETE";
	while( <VAL90> ) {
		push @rc, $_;
		_statusmsg("[rollout] $_\n");
		if( /ERROR:/ or /WARNING:/ ) {
			$vstate="ERROR";
			error_messagebox("Validation Error",$_);
		} elsif( /SUCCESSFUL:/ ) {
			$vstate="OK";
		} elsif( /SKIPPED:/ ) {
			$vstate="SKIPPED";
		}
	}
	close(VAL90);
	
	if(  $vstate eq "OK" ) {
		_statusmsg( "[setup] Copy Succeeded\n" );
		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
					-title=>"Copy Web Installation",
					-buttons=>["Ok"] );
		$dialog->Label(-text=>"Copy Web Successful")->pack();
		$dialog->Show;

		set_install_step( "COPY_CGI_BIN_TO_WEBSERVER" );
		$graphics_object{COPY_CGI_BIN_TO_WEBSERVER}->configure(-state=>'normal');
		unbusy();
		return 1;
	} else {
		_statusmsg( "[setup] Validation Failed $vstate\n" );
		set_install_step("COPY_CGI_BIN_TO_WEBSERVER",1);
		my $dialog =  $CURRENT_CONFIG{mainWindow}->DialogBox(
			-title=>"Copy Web nstallation",
			-buttons=>["Ok"] );
		$dialog->Label(-text=>"Copy To Web Server FAILED: $rc")->pack();
		$dialog->Show;
		unbusy();
		return 0;
	}
}
#{
#	_statusmsg("[copy_to_web] the system is busy - this is probably an error\n") if is_busy();
#	return if is_busy();
#
##	if( ! get_install_variable("POPULATE_LOCAL_CGI_BIN") ) {
##		error_messagebox('Data Entry Error','ERROR: Must Populate Local Cgi Bin First');
##		set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
##		unbusy(1);
##		return 0;
##	}
#
#	busy();
#	_statusmsg( "[copy_to_web] \n" );
#	_statusmsg( "[copy_to_web] Copy cgi-bin to Web Server Button Pressed\n" );
#	_statusmsg( "[copy_to_web] \n" );
#
#	my($IS_WIN32)=$GemConfig->{MIMI_IS_WIN32};
#
#	if( $GemConfig->{INSTALLTYPE} eq "UNIX" and $IS_WIN32 eq "Y" ) {
#		error_messagebox('Data Entry Error','ERROR: Installtype=UNIX does not support Win32 Web Servers');
#		set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#		unbusy(1);
#		return 0;
#	} elsif( $GemConfig->{INSTALLTYPE} eq "WINDOWS" and $IS_WIN32 eq "N" ) {
#		error_messagebox('Data Entry Error','ERROR: Installtype=WINDOWS does not support Unix Web Servers');
#		set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#		unbusy(1);
#		return 0;
#	} elsif( ! is_nt() and $IS_WIN32 eq "Y") {
#		error_messagebox('Data Entry Error','ERROR: Cant copy to Win32 Web Server From Unix');
#		set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#		unbusy(1);
#		return 0;
#	} elsif( is_nt() and $IS_WIN32 ne "Y") {
#		error_messagebox('Data Entry Error','ERROR: Cant copy to Unix Web Server From Win32');
#		set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#		unbusy(1);
#		return 0;
#	}
#
#	_statusmsg("[copy_to_web] initial validation ok\n");
#	if( $IS_WIN32 eq "Y" ) {
#		_statusmsg( "[copy_to_web] Copying The Windows Way\n" );
#		my($dirname)=$GemConfig->{MIMI_FILENAME_WIN32};
#		if( ! -d $dirname ) {
#			error_messagebox('Data Entry Error','ERROR: Directory '.$dirname.' is not a directory.');
#			set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#			unbusy(1);
#			return 0;
#		}
#
#		my($fromdir)=$CURRENT_CONFIG{gem_root_directory}."/cgi-bin";
#		_statusmsg( "[copy_to_web] fromdir=$fromdir\n" );
#		_statusmsg( "[copy_to_web] todir=$dirname\n" );
#
#		if( blind_dircopy($CURRENT_CONFIG{gem_root_directory}."/cgi-bin", 			$dirname,1 ) == 0 ) {
#				error_messagebox('Data Entry Error','ERROR: Cant Copy Directory');
#				set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#				unbusy(1);
#				return 0;
#		}
#		_statusmsg( "[copy_to_web] File Copies Completed\n" );
#	} 	elsif( $GemConfig->{MIMI_HOSTNAME_UNIX} eq hostname()
#		or $GemConfig->{MIMI_HOSTNAME_UNIX} eq "127.0.0.1"
#		or $GemConfig->{MIMI_HOSTNAME_UNIX} eq "localhost"
#		) {		# LOCAL COPY
#
#			_statusmsg( "[copy_to_web] Web Server Is Local/Unix\n" );
#			my($dirname)=$GemConfig->{MIMI_FILENAME_UNIX};
#			if( ! -d $dirname ) {
#				error_messagebox('Data Entry Error','ERROR: Directory '.$dirname.' is not a directory.');
#				set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#				unbusy(1);
#				return 0;
#			}
#
#			my($fromdir)=$CURRENT_CONFIG{gem_root_directory}."/cgi-bin";
#			_statusmsg( "[copy_to_web] fromdir=$fromdir\n" );
#			_statusmsg( "[copy_to_web] todir=$dirname\n" );
#
#			if( blind_dircopy($CURRENT_CONFIG{gem_root_directory}."/cgi-bin", $dirname,1 )  == 0 ) {
#				error_messagebox('Data Entry Error','ERROR: Cant Copy Directory');
#				set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#				unbusy(1);
#				return 0;
#			}
#			_statusmsg( "[copy_to_web] Local File Copies Completed\n" );
#
#	} else {
#		my($hostname,$tgtdir)=( $GemConfig->{MIMI_HOSTNAME_UNIX}, $GemConfig->{MIMI_FILENAME_UNIX} );
#		_statusmsg( "[copy_to_web] Test Native SSH To $hostname\n");
#
#		sub runc {
#			my($cmd)=@_;
#			my(@rc);
#			open(C,"$cmd 2>&1 | ") or die "Cant run $cmd $!\n";
#			while(<C>) { chop;chomp;push @rc, $_; }
#			close(C);
#			return @rc;
#		}
#
#		# TOUCH A FILE
#		my(@r)=runc("ssh $hostname rm -f $tgtdir/tmpo");
#		if( $#r>=0 ) {
#			_statusmsg("[copy_to_web] Failure to Run Command ssh $hostname rm -f $tgtdir/tmpo");
#			error_messagebox('Data Entry Error','ERROR: Cant copy to Unix Web Server From Win32'.join(" ",@r));
#			set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#			unbusy(1);
#			return 0;
#		}
#
#		(@r)=runc("ssh $hostname touch $tgtdir/tmpo");
#		if( $#r>=0 ) {
#			_statusmsg("[copy_to_web] Failure to Run Command ssh $hostname touch $tgtdir/tmpo");
#			error_messagebox('Data Entry Error','ERROR: Cant copy to Unix Web Server From Win32'.join(" ",@r));
#			set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#			unbusy(1);
#			return 0;
#		}
#
#		(@r)=runc("ssh $hostname ls  $tgtdir/tmpo");
#		if( $#r>0 ) {		# 1 row & only 1 row
#			_statusmsg("[copy_to_web] Failure to Run Command ssh $hostname ls $tgtdir/tmpo");
#			error_messagebox('Data Entry Error','ERROR: Cant copy to Unix Web Server From Win32'.join(" ",@r));
#			set_install_step( "COPY_CGI_BIN_TO_WEBSERVER",1 );
#			unbusy(1);
#			return 0;
#		}
#
#		my($fromdir)=$CURRENT_CONFIG{gem_root_directory}."/cgi-bin";
#		_statusmsg( "[copy_to_web] fromdir=$fromdir\n" );
#		_statusmsg( "[copy_to_web] tohost=$hostname\n" );
#		_statusmsg( "[copy_to_web] todir=$tgtdir\n" );
#
#		(@r)=runc("scp -r $fromdir $hostname:$tgtdir");
#		_statusmsg( "[copy_to_web] scp -r $fromdir $hostname:$tgtdir\n" );
#		foreach (@r) {	_statusmsg( "[copy_to_web] $_\n" ); }
#	}
#
#	set_install_step( "COPY_CGI_BIN_TO_WEBSERVER" );
#	unbusy(1);
#	_statusmsg("[copy_to_web] Copy Completed\n");
#	return 1;
#}

sub blind_dircopy {
	my($fromdir,$todir,$recurse,$extension)=@_;
	return l_die("$fromdir is not a directory" ) unless -d $fromdir;
	return l_die("$fromdir is not readable" )    unless -r $fromdir;

	if( ! -d $todir ) {
		my($rc)=mkdir( $todir, 0755 );
		if( ! $rc ) {	die "Cant mkdir $todir : $!\n";	}
	}

	return l_die("$todir is not a directory" )   unless -d $todir;
	return l_die("$todir is not writable" )      unless -w $todir;

	opendir(DIR,$fromdir) || die("blind_dircopy() Can't open directory ".$fromdir." for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);

	_statusmsg( "[dircopy] ... From $fromdir/\*".$extension."\n" );
	foreach (@dirlist) {
		next if /200\d\d\d\d\d/ or /201\d\d\d\d\d/ or /^save/ or /^admin$/;
		next if $extension and ! /$extension$/;

		if( -d "$fromdir/$_" and $recurse ) {
			if( ! -d "$todir/$_" ) {
				mkdir( "$todir/$_", 0755 ) or die "Cant mkdir $todir/$_ : $!\n";
			}
			_statusmsg( "[dircopy] To directory $_\n" );
			my($r)=blind_dircopy("$fromdir/$_","$todir/$_",1,$extension);
			_statusmsg( "[dircopy] To directory $_ completed\n" );
			return 0 if $r==0;
			next;
		}
		_statusmsg( "[dircopy] ... copy file $_\n" );
		next if -d "$fromdir/$_";

		my($fromfile)=$fromdir."/".$_;
		my($tofile)=$todir."/".$_;

		if( ! -e $fromfile ) {
			error_messagebox('Data Entry Error',"ERROR: File $fromfile Does Not Exist");
			return 0;
		}
		if( ! -r $fromfile ) {
			error_messagebox('Data Entry Error',"ERROR: File $fromfile Is Not Writable");
			return 0;
		}

		unlink($tofile) if -e $tofile;
		if( -e $tofile and ! -w $tofile ) {
			error_messagebox('Data Entry Error',"ERROR: File $tofile exists and is not writeable");
			return 0;
		}

		my($rc)=File::Copy::copy($fromfile,$tofile);
		if( $rc == 0 ) {
			error_messagebox('Data Entry Error',"ERROR: can not copy $fromfile to $tofile\n$!");
			return 0;
		}
	}
	return 1;
}

#sub setup_consolepublishing_tab {
# 	my($tabD)=@_;
#   tab_header($tabD,"CONSOLE PUBLISHING",
#		"The GEM Console Is a set of static web pages in ".$CURRENT_CONFIG{gem_root_directory}."/data/CONSOLE_REPORTS.\nYou can published them to another system/directory.\n Skip this step unless you have specific requirements.");
#
#	my $console_publish_labfrm  = $tabD->LabFrame(
#  		-label=>"Do You Want to Publish Your Console Reports?",
#  		-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -pady=>5, -anchor=>'w', -expand=>1, -fill=>'both');
#	my $console_publish_subfrm  = $console_publish_labfrm->Frame()->pack(-side=>'top', -padx=>15, -pady=>15, -anchor=>'w');
#	my $console_unix_labfrm  = $tabD->LabFrame(
#  		-label=>"Publishing From Linux/Unix Monitor Station",
#  		-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -pady=>5, -anchor=>'w', -expand=>1, -fill=>'both');
#	my $console_win32_labfrm  = $tabD->LabFrame(
#  		-label=>"Publishing From Win32 Monitor Station",
#  		-labelside=>'acrosstop')->pack(-side=>'top', -padx=>15, -pady=>5, -anchor=>'w', -expand=>1, -fill=>'both');
#
#	my($row)=0;
#	$console_publish_subfrm->Label(-font=>'norm', -text=>'Console Creation Directory: ' )->grid(-column=>1,-row=>$row,-sticky=>'w');
#	$console_publish_subfrm->Label(-font=>'norm', -text=>$CURRENT_CONFIG{gem_root_directory}."/data/CONSOLE_REPORTS" )->grid(-column=>2,-row=>$row++,-sticky=>'w');
#
#	$console_publish_subfrm->Label(-font=>'norm', -text=>' ' )->grid(-column=>1,-row=>$row++,-sticky=>'w');
#
#	$GemConfig->{documenter_do_copy}='N' unless $GemConfig->{documenter_do_copy};
#	$console_publish_subfrm->Label(-font=>'norm', -text=>'Do You Wish to Publish The Console' )->grid(-column=>1,-row=>$row,-sticky=>'w');
#	$console_publish_subfrm->Radiobutton(-font=>"norm",
#   		-text=>"Do Not Publish The Console",
#   		-command => \&redraw_console_pub,
#   		-variable=>\$GemConfig->{documenter_do_copy},-value=>"N")->grid(-column=>2,-row=>$row++,-sticky=>'w'),
#	$console_publish_subfrm->Radiobutton(-font=>"norm",
#   		-text=>"Publish the console by copying to another system/directory",
#   		-command => \&redraw_console_pub,
#   		-variable=>\$GemConfig->{documenter_do_copy},-value=>"Y")->grid(-column=>2,-row=>$row++,-sticky=>'w'),
#
#	$console_publish_subfrm->Label(-font=>"norm",-text=>"Monitoring Server To Publish From\n(Samba installs only)")->grid(-column=>1,-row=>$row,-sticky=>'w');
#	$console_publish_subfrm->Radiobutton(-font=>"norm",
#   		-text=>"Publish from your Linux or Unix Monitoring Server",
#   		-command => \&redraw_console_pub,
#   		-variable=>\$GemConfig->{publishing_monitor_server},-value=>"UNIX")->grid(-column=>2,-row=>$row++,-sticky=>'w'),
# 	$console_publish_subfrm->Radiobutton(-font=>"norm",
#   		-text=>"Publish from your Windows Monitoring Server",
#   		-command => \&redraw_console_pub,
#   		-variable=>\$GemConfig->{publishing_monitor_server},-value=>"WIN32")->grid(-column=>2,-row=>$row++,-sticky=>'w'),
#
#	$graphics_object{UNIX_Pub_L} 				= $console_unix_labfrm->Label(-font=>"large",-relief=>'sunken',-bg=>'beige',-text=>"If Publishing From A Linux/Unix Monitoring Server")->grid(-column=>1,-row=>$row++,-sticky=>'ew',-columnspan=>2);
#   $graphics_object{UNIX_Pub_Hostname_L} 	= $console_unix_labfrm->Label(-font=>"norm",-text=>"Unix Destination Hostname")->grid(-column=>1,-row=>$row,-sticky=>'w');
#   $graphics_object{UNIX_Pub_Hostname} 	= $console_unix_labfrm->Entry(-bg=>'white',-width=>50,-textvariable=>\$GemConfig->{EXTERNAL_WEB_SERVER_HOSTNAME})->grid(-column=>2,-row=>$row++,-sticky=>'w'),;;
#   $graphics_object{UNIX_Pub_Dest_L}		= $console_unix_labfrm->Label(-font=>"norm",-text=>"Unix Destination Directory")->grid(-column=>1,-row=>$row,-sticky=>'w');
#   $graphics_object{UNIX_Pub_Dest} 			= $console_unix_labfrm->Entry(-bg=>'white',-width=>50,-textvariable=>\$GemConfig->{EXTERNAL_WEB_SERVER_DIRECTORY})->grid(-column=>2,-row=>$row++,-sticky=>'w');
#   $graphics_object{WIN32_Pub_L} 			= $console_win32_labfrm->Label(-font=>"large",-relief=>'sunken',-bg=>'beige',-text=>"If Publishing From A Windows Monitoring Server")->grid(-column=>1,-row=>$row++,-sticky=>'ew',-columnspan=>2);
#   $graphics_object{WIN32_Pub_Dest_L1} 	= $console_win32_labfrm->Label(-font=>"norm",-text=>'Win32 Destination Path (\\\\hostname\x$\path)')->grid(-column=>1,-row=>$row,-sticky=>'w');
#   $graphics_object{WIN32_Pub_Dest_L2} 	= $console_win32_labfrm->Entry(-bg=>'white',-width=>50,-textvariable=>\$GemConfig->{EXTERNAL_WEB_SERVER_DIRECTORY})->grid(-column=>2,-row=>$row++,-sticky=>'w');
#}
#
#sub redraw_console_pub {
#	my($state_ux) 		= ($GemConfig->{INSTALLTYPE} eq "WINDOWS") ? "disabled" : "normal";
#	my($state_win32) 	= ($GemConfig->{INSTALLTYPE} eq "UNIX") ? "disabled" : "normal";
#	$state_win32=$state_ux='disabled' if $GemConfig->{documenter_do_copy} eq 'N';
#	$state_ux='disabled' 	if $GemConfig->{publishing_monitor_server} eq "WIN32";
#	$state_win32='disabled' if $GemConfig->{publishing_monitor_server} eq "UNIX";
#
#	$graphics_object{UNIX_Pub_L}->configure(-state=>$state_ux);
#   $graphics_object{UNIX_Pub_Hostname_L}->configure(-state=>$state_ux);
#   $graphics_object{UNIX_Pub_Hostname}->configure(-state=>$state_ux);
#   $graphics_object{UNIX_Pub_Dest_L}->configure(-state=>$state_ux);
#   $graphics_object{UNIX_Pub_Dest}->configure(-state=>$state_ux);
#
#   $graphics_object{WIN32_Pub_L}->configure(-state=>$state_win32);
#   $graphics_object{WIN32_Pub_Dest_L1}->configure(-state=>$state_win32);
#   $graphics_object{WIN32_Pub_Dest_L2}->configure(-state=>$state_win32);
#}

#
# redraw_installtype() is called when you change the installtype on the product page
#
sub redraw_installtype {
	my($nomsg)=@_;		# if set its part of normal startup, if not set its a change to INSTALLTYPE

#	if( $GemConfig->{INSTALLTYPE} ne "SAMBA" ) {
#		# $graphics_object{LBL_WIN32_USE_FTP1}->gridForget();
#
#   	if( ! $nomsg and is_nt() and $GemConfig->{INSTALLTYPE} eq "UNIX" ) {
#   		_statusmsg("[configure] Displaying message box - select a button to continue\n");
#	    	$CURRENT_CONFIG{mainWindow}->messageBox(
#				-title 	=> 'Change Environment',
#				-message => 'NOTE: You have set INSTALLTYPE=UNIX but are on windows. Please save, exit, and restart configure.pl on UNIX/Linux.',
#				-type => "OK" );
#			_statusmsg("[configure] GEM Ready / Continuing...\n");
#		}
#
#		if( ! $nomsg and ! is_nt() and $GemConfig->{INSTALLTYPE} eq "WINDOWS" ) {
#   		_statusmsg("[configure] Displaying message box - select a button to continue\n");
#	    	$CURRENT_CONFIG{mainWindow}->messageBox(
#				-title 	=> 'Change Environment',
#				-message => 'NOTE: You have set INSTALLTYPE=SAMBA but are on windows. Please save, exit, and restart configure.pl on UNIX/Linux.',
#				-type => "OK" );
#			_statusmsg("[configure] GEM Ready / Continuing...\n");
#		}
#   }

#	if( $GemConfig->{INSTALLTYPE} ne "UNIX" ) {
#   	my($found)="FALSE";
#   	for (1..10) { if( $GemConfig->{"SYBASE".$_} eq "C:/sybase" ) { $found="TRUE"; last; } }
#   	$GemConfig->{"SYBASE2"}="C:/sybase" if $found eq "FALSE"  and $GemConfig->{"SYBASE2"} eq "";
#		$found="FALSE";
#   	for (1..10) { if( $GemConfig->{"SYBASE".$_} eq "D:/sybase" ) { $found="TRUE"; last; } }
#   	$GemConfig->{"SYBASE3"}="D:/sybase" if $found eq "FALSE"  and $GemConfig->{"SYBASE3"} eq "";
#	}

	my($state) = ($GemConfig->{INSTALLTYPE} eq "UNIX") ? "disabled" : "normal";
	# if INSTALLTYPE=unix then turn off win32 stuff...
	foreach ( keys %graphics_object ) {
		next unless /WIN32/i;
		next if /^LBL/i or /^Label/i or /_USE_FTP/ or /TEST_MAIL_INSTALL/ or /CONFIGURE_MONITOR_SERVER/;
		$graphics_object{$_}->configure(-state=>$state);
	}

	$state = ($GemConfig->{INSTALLTYPE} eq "WINDOWS") ? "disabled" : "normal";
	# if INSTALLTYPE=WINDOWS then turn off UNIX stuff...
	foreach ( keys %graphics_object ) {
		next unless /UNIX/i;
		next if /^LBL/i or /^Label/i or /_USE_FTP/ or /TEST_MAIL_INSTALL/ or /CONFIGURE_MONITOR_SERVER/;
		$graphics_object{$_}->configure(-state=>$state);
	}

	# redraw_console_pub();
}

#
# redraw_webserver() is called from the WebServer -> Type Button
#
sub redraw_webserver {
	if( $GemConfig->{MIMI_IS_WIN32} eq "Y" ) {
		_statusmsg("[setup] Win32 Web Server - Disabling unix widgets\n");
		foreach( keys %graphics_object ) {
			next unless /^MIMI/;
			if( /UNIX/ ) {
				$graphics_object{$_}->configure(-state=>'disabled');
			} else {
				$graphics_object{$_}->configure(-state=>'normal');
			}
		}
	} else {
		_statusmsg("[setup] Unix Web Server - Disabling win32 widgets\n");
		foreach( keys %graphics_object ) {
			next unless /^MIMI/;
			if( /WIN32/ ) {
				$graphics_object{$_}->configure(-state=>'disabled');
			} else {
				$graphics_object{$_}->configure(-state=>'normal');
			}
		}
	}
	my($state);
	if( $GemConfig->{MIMI_IS_WIN32} eq "Y" ) {
		if( is_nt() ) {
			$state='normal';
		} else {
			$state='disabled';
		}
	} else {
		if( is_nt() ) {
			$state='disabled';
		} else {
			$state='normal';
		}
	}
	$graphics_object{COPY_CGI_BIN_TO_WEBSERVER}->configure(-state=>$state);# unless is_nt();
}

# redraw_productset() is called when you change teh product list on the registration page
sub redraw_productset {
	my($state) = ($GemConfig->{USE_SYBASE_ASE} eq "Y") ? "normal" : "disabled";
	foreach ( keys %graphics_object ) {
		next unless /SYBASE/i;
		$graphics_object{$_}->configure(-state=>$state);
	}
	$state = ($GemConfig->{USE_SQLSERVER} eq "Y") ? "normal" : "disabled";
	foreach ( keys %graphics_object ) {
		next unless /SQLSVR/i;
		$graphics_object{$_}->configure(-state=>$state);
	}
	$state = ($GemConfig->{USE_MYSQL} eq "Y") ? "normal" : "disabled";
	foreach ( keys %graphics_object ) {
		next unless /Mysql/i;
		$graphics_object{$_}->configure(-state=>$state);
	}
	$state = ($GemConfig->{USE_ORACLE} eq "Y") ? "normal" : "disabled";
	foreach ( keys %graphics_object ) {
		next unless /ORACLE/i;
		$graphics_object{$_}->configure(-state=>$state);
	}
}

# rebuild_alarm_file($file) rebuilds file
# replaces my($server,$login,$password,$db)= with appropriate variables
sub rebuild_alarm_file {
	my($file)=@_;
	_statusmsg("[configure] Rebuilding Alarm File $file\n");
	open(A77,"$file")	  		or return l_warning("Cant Read $file : $!");
	open(B76,">$file.tmp") 	or return l_warning("Cant Write $file.tmp : $!");
	my($foundstr)='FALSE';
	foreach (<A77>) {
		if( /^my\(\$server,\$login,\$pass/ ) {
			$foundstr='TRUE';
			print B76 'my($server,$login,$password,$db)=("'.$GemConfig->{ALARM_SERVER}.'","'.
					$GemConfig->{ALARM_LOGIN}.'","'.
					$GemConfig->{ALARM_PASSWORD}.'","'.
					$GemConfig->{ALARM_DATABASE}.'");'."\n";
		} else {
			print B76 $_;
		}
	}
	close(A77);
	close(B76);
	l_die( "ERROR - COULD NOT FIND KEY STRING IN $file\n" ) if $foundstr eq "FALSE";
	archive_file($file,5);
	rename($file.".tmp",$file) or return l_warning("Cant rename $file.tmp to $file : $!");
	_statusmsg("[configure] Rebuild completed\n");
}

#sub validation_error {
#	my($msg)=@_;
#	return if $UPGRADE;
#	_statusmsg("[configure] Displaying message box - select a button to continue\n");
#	_statusmsg("[configure] $msg\n");
#	my($dlg) = $CURRENT_CONFIG{mainWindow}->messageBox( -title => "Validation Error",-message => $msg, -type => "OK" );
#	_statusmsg("[ready] GEM Ready / Continuing...\n");
#	#$dlg->Show();
#	return;
#}

sub error_messagebox {
	my($title,$message)=@_;
	_statusmsg("[msgbox] Displaying message box - select a button to continue\n");
	die "FATAL ERROR\n$title\n$message" if $UPGRADE;
	$CURRENT_CONFIG{mainWindow}->messageBox(	-title => $title,	-message => $message, -type => "OK" );
	_statusmsg("[msgbox] GEM Ready / Continuing...\n");
}

sub install_gemalarms {
	_statusmsg("[InstallDb] Install/Upgrade Gem Alarm Server\n");
	_statusmsg("[InstallDb]    Server Name=$GemConfig->{ALARM_SERVER}\n");
	_statusmsg("[InstallDb]    Type=$GemConfig->{ALARM_DB_TYPE}\n");

	# verify data entry
	if( $GemConfig->{ALARM_SERVER}=~/^\s*$/ or
	    $GemConfig->{ALARM_LOGIN}=~/^\s*$/ or
	    $GemConfig->{ALARM_PASSWORD}=~/^\s*$/ or
	    $GemConfig->{ALARM_DB_TYPE}=~/^\s*$/ or
	    $GemConfig->{ALARM_MAIL}=~/^\s*$/ or
	    $GemConfig->{ALARM_DATABASE}=~/^\s*$/ ) {
	    	error_messagebox('Data Entry Error','ERROR: not all alarm information entered');
	    	return 0;
	}

	# if sql server, you must be on win32
	if( ! is_nt() and $GemConfig->{ALARM_DB_TYPE} eq "sqlsvr" ) {
		error_messagebox('Data Entry Error','ERROR: cat install alarm db on a sql server - you are running configure.pl unix');
		return 0;
	}

	# set sa password = xxx if sql server and no sa password
	if( $GemConfig->{ALARM_DB_TYPE} eq "sqlsvr" ) {
			_statusmsg("[InstallDb] Setting sa password = xxx (native authentication)\n") unless $GemConfig->{ALARM_SA_PASSWORD};
			$GemConfig->{ALARM_SA_LOGIN}='sa' 	  unless $GemConfig->{ALARM_SA_LOGIN};
			$GemConfig->{ALARM_SA_PASSWORD}='xxx' unless $GemConfig->{ALARM_SA_PASSWORD};
	} else {
		if( ! $GemConfig->{ALARM_SA_LOGIN} or ! $GemConfig->{ALARM_SA_PASSWORD} ) {
			error_messagebox('Data Entry Error','Login/Password must be set for sybase servers');
			return 0;
		}
	}

   my($ctype)=$GemConfig->{ALARM_DB_TYPE};
  	$ctype="ODBC" if is_nt();
  	_statusmsg( "[InstallDb] creating dbi $ctype connection to $GemConfig->{ALARM_SERVER}\n" );
  	my($c)=dbi_connect(	-srv=>$GemConfig->{ALARM_SERVER},
   				-login=>$GemConfig->{ALARM_SA_LOGIN},
   				-PrintError=>1,
   				-maxConnect=>100,
   				-password=>$GemConfig->{ALARM_SA_PASSWORD},
   				-type=>$ctype,
   				-connection=>1 );

   if( ! $c ) {
   	_statusmsg( "[InstallDb] connection failed\n" );
   	die "Failed To Connect to $GemConfig->{ALARM_SERVER}" if $UPGRADE;

    #  if( defined $avail_dsns{$args{-name}} ) {
      #	_statusmsg( "[InstallDb] Dsn found but unable to connect\n" )
     # } else {
      	if( is_nt() ) {
      		_statusmsg( "[InstallDb] Unable to find ODBC settings for server $GemConfig->{ALARM_SERVER}\n" );
   		} else {
      		_statusmsg("[InstallDb] WARNING: Unable to connect to server $GemConfig->{ALARM_SERVER} as login $GemConfig->{ALARM_SA_LOGIN} : no dsn available\n");
      	}
     # }
      error_messagebox('Connection Failed',"Failed To Connect to $GemConfig->{ALARM_SERVER}");
      return 0;
   } else {
   	_statusmsg( "[InstallDb] connected\n" );
   }
	my($is_sqlsvr)='NO';
   if( $GemConfig->{ALARM_DB_TYPE} eq "sybase" or $GemConfig->{ALARM_DB_TYPE} eq "sqlsvr" ) {
 		# test connection to see if it really is an ase server...
		my(@d)=dbi_query(-connection=>$c, -db=>"master",
			-query=>"select name from sysobjects where name='sysobjects'");
		foreach (@d) {
			my($n)=dbi_decode_row($_);
			last if $n eq "sysobjects";

			if( $n=~/user api layer: external error: This routine cannot be called because another command struct/ ) {
				print "Unable to Connect -  Server ",$GemConfig->{ALARM_SERVER}," is not an ASE or SQL server";
			}

			print "[ERROR] Woah... select returned $n!!!\n";
			print "[ERROR] this is part of ",join(",",dbi_decode_row($_)),"\n";
			error_messagebox('Connection Failed',"Unable to Connect -  Server $GemConfig->{ALARM_SERVER} Returned Abnormal Query Results\n");
			return 0;
		}
		dbi_set_mode("INLINE");

		@d=dbi_query(-connection=>$c, -db=>"master",-query=>"select 'YES' from sysdatabases where name='msdb'");

		foreach (@d) {
			($is_sqlsvr)=dbi_decode_row($_);
		}
		if( $is_sqlsvr ne "YES" and $is_sqlsvr ne "NO" ) {
			error_messagebox('Query Failed',"Query Returned $is_sqlsvr\n");
			return 0;
		}
		if( $GemConfig->{ALARM_DB_TYPE} eq "sybase" and $is_sqlsvr eq "YES" ) {
			error_messagebox('Query Failed',"msdb exists on sybase?\n");
			return 0;
		}
		if( $GemConfig->{ALARM_DB_TYPE} ne "sybase" and $is_sqlsvr eq "NO" ) {
			error_messagebox('Query Failed',"msdb doesnt exist on sqlsvr?\n");
			return 0;
		}
	}

	my(@d)=dbi_query(-connection=>$c, -db=>"master",-query=>"select status&12 from sysdatabases where name='".$GemConfig->{ALARM_DATABASE}."'");
	my($status);
	foreach (@d) {
			($status)=dbi_decode_row($_);
	}
	_statusmsg("[InstallDb] Status&12 from sysdatabases=$status\n");
	_statusmsg("[InstallDb] Connection Ok\n");

	if( ! defined $status ) {
		_statusmsg("[InstallDb] gemalarms db does not exist ...\n");
		die "FATAL ERROR" if $UPGRADE;
		error_messagebox('Query Failed',"gemalarms db does not exist?\n");
		#_statusmsg("[InstallDb] GEM Ready / Continuing...\n");
		return 0;
	} elsif( $status == 12 or $is_sqlsvr eq "YES" ) {
		if( $is_sqlsvr eq "YES" ) {
			_statusmsg("[InstallDb] SQL Server Found As Alarm Server.\n");
		} else {
			_statusmsg("[InstallDb] Select Into & Truncate Log On Chkpt Set - Continuing...\n");
		}
		my(@d)=dbi_query(-connection=>$c, -db=>"master",-query=>"select name from sysdatabases where name like '%_BAK'");
		my(@tbls);
		foreach (@d) {
			push @tbls, dbi_decode_row($_);
		}
		if( $#tbls>=0 ) {	# ERROR BACKUP TABLES EXIST
			die "_BAK exist - this may be from a failed upgrade: ".join(" ",@tbls)."\n" if $UPGRADE;
			_statusmsg( "[InstallDb] _BAK exist - this may be from a failed upgrade: ".join(" ",@tbls)."\n" );
			error_messagebox('Query Failed',"_BAK exist - this may be from a failed upgrade: ".join(" ",@tbls)."\n",);
			return 0;
		}
	} else {
		_statusmsg("[InstallDb] Status&12 from sysdatabases=$status - should be 12\n");
		_statusmsg( "[InstallDb] Select Into & Bcp Are NOT Set For this database\n" );
		error_messagebox('Query Failed',"Select Into & Bcp Are NOT Set For this database\n",);
		return 0;
	}

	rebuild_alarm_file("$CURRENT_CONFIG{gem_root_directory}/lib/MlpAlarm.pm");
	# rebuild_alarm_file("$CURRENT_CONFIG{gem_root_directory}/cgi-bin/MlpAlarm.pm")
	#	if -r "$CURRENT_CONFIG{gem_root_directory}/cgi-bin/MlpAlarm.pm";

#	MlpSetVars(
#			-server		=>	$GemConfig->{ALARM_SERVER},
#			-login		=>	$GemConfig->{ALARM_LOGIN},
#			-password	=>	$GemConfig->{ALARM_PASSWORD},
#			-db			=>	$GemConfig->{ALARM_DATABASE} );
#	_statusmsg("[InstallDb]   Done Rebuilding Mlpalarm.pm\n");

	my($objects)="";
	opendir(DIR,"$CURRENT_CONFIG{admin_scripts_dir}/gemalarms/") || die("Can't open directory $CURRENT_CONFIG{admin_scripts_dir}/gemalarms for reading");
	my(@dirlist) = grep((!/^\./ and !/depricated/),readdir(DIR));
	closedir(DIR);

	foreach ( @dirlist ) { $objects.= "$CURRENT_CONFIG{admin_scripts_dir}/gemalarms/$_ " if /.tbl$/; }
	foreach ( @dirlist ) { $objects.= "$CURRENT_CONFIG{admin_scripts_dir}/gemalarms/$_ " if /.trg$/; }
	foreach ( @dirlist ) { $objects.= "$CURRENT_CONFIG{admin_scripts_dir}/gemalarms/$_ " if /.vie$/; }
	foreach ( @dirlist ) { $objects.= "$CURRENT_CONFIG{admin_scripts_dir}/gemalarms/$_ " if /.prc$/; }
	foreach ( @dirlist ) { $objects.= "$CURRENT_CONFIG{admin_scripts_dir}/gemalarms/$_ " if /.ins$/; }
	foreach ( @dirlist ) { $objects.= "$CURRENT_CONFIG{admin_scripts_dir}/gemalarms/$_ " if /.sql$/; }

	$objects=~s/\s$//;

	_statusmsg("[InstallDb] Calling do_release()\n");
	do_release(    connection=>$c,
					#	debug=>1,
						servername => $GemConfig->{ALARM_SERVER},
						dbname => $GemConfig->{ALARM_DATABASE},
						batch_nodie => 1,
						objects => $objects	);

			#batch_die => $opt_x,
			#username => $opt_U,
			#servername => $opt_S,
			#password => $opt_P,
			#ctlfile => $opt_c,
			#dir => $opt_V,
			#start_at_fileno => $opt_s,
			#end_at_fileno => $opt_e,
			#noexec => $opt_n,
			#norules => $opt_r,
			#nosave => $opt_s,
			#procs_only => $opt_T,
			#new_obj_only => $opt_N,

	_statusmsg("[InstallDb] done do_release() Completed\n");

	# part 2 load some web site constant data
	my($basecmd)="$^X -I".join(" -I",@perl_includes)." ";
	my($cmd)=$basecmd."$CURRENT_CONFIG{admin_scripts_dir}/cgi-bin/LoadMimiReportData.pl ";
	_statusmsg("[ready] LoadMimiReportData started...\n");
	_statusmsg("[ReportData] $cmd\n");
	busy();
	open( CMD, $cmd." |" ) or die "Cant Run $cmd\n";
	my($ok)=0;
	while(<CMD>) {
		chomp;
		if( /^INV\_/ or /^GEM\_/ or /^DEFAULT\_ARGS/ or /^DCM\_/ ) {
			$ok++;
		} else {
			_statusmsg("[ReportData] $_\n");
		}
	}
	_statusmsg("[ReportData] Loaded $ok Reports\n");
	
	close(CMD);
#	RunCommand( -cmd=>$cmd,
#			-mainwindow=>$CURRENT_CONFIG{mainWindow},
#			-statusfunc=>\&_statusmsg,
#			-printfunc =>\&_statusmsg,
#			-showinwin => "Rebuild Codeline",
#			-printhdr  => "[Reformatter] ",
#			-debug=>$DEBUG
#			);
	_statusmsg("[ready] LoadMimiReportData completed...\n");
	unbusy();

	my($cmd)=$basecmd."$CURRENT_CONFIG{admin_scripts_dir}/cgi-bin/LoadConstantData.pl ";
	_statusmsg("[ready] LoadConstantData started...\n");
	_statusmsg("[ConstantData] $cmd\n");
	busy();
	open( CMD, $cmd." |" ) or die "Cant Run $cmd\n";
	my($ok)=0;
	while(<CMD>) {
		chomp;
		if( /^insert INV_/ ) { 
			$ok++;
		} else {
			_statusmsg("[ConstantData] $_\n");
		}
	}
	close(CMD);
	_statusmsg("[ConstantData] Loaded $ok Rows\n");
	
	_statusmsg("[ready] LoadMimiReportData completed...\n");
	unbusy();

#	busy();
#	RunCommand( -cmd=>$cmd,
#			-mainwindow=>$CURRENT_CONFIG{mainWindow},
#			-statusfunc=>\&_statusmsg,
#			-printfunc =>\&_statusmsg,
#			-showinwin => "Rebuild Codeline",
#			-printhdr  => "[Reformatter] ",
#			-debug=>$DEBUG
#			);
#	_statusmsg("[ready] LoadConstantData completed...\n");
#	unbusy();

	_statusmsg("[InstallDb] Displaying message box - select a button to continue\n") unless $UPGRADE;
	$CURRENT_CONFIG{mainWindow}->messageBox(
			-title => 'Success',
			-message => "Completed Alarm Database Installation\n",
			-type => "OK" ) unless $UPGRADE;
	_statusmsg( "[InstallDb] GEM Ready / Continuing...\n");
	_statusmsg( "[InstallDb] Alarm Install Completed\n" );
	return 1;
}

sub archive_file {
	my($from, $versions,$do_statusmsg)=@_;
	my($curdate)=do_time(-fmt=>'yyyymmdd_hhmiss');
	my($tofile)=$from.".".$curdate;
	$tofile =~ s.\/conf\/.\/conf\/saved_files\/.;		# saved into saved_files directory
	if( ! $do_statusmsg ) {
		_statusmsg("[save] ... Archived $from\n" );
		_statusmsg("[save]           to $tofile\n" )if defined $DEBUG;
	}
	rename( $from , $tofile )
		or l_die("Cant rename $from to $tofile : $!\n");

	if( defined $versions ) {
		my($dir)=dirname($from);
		my($file)=basename($from);
		opendir(DIR,$dir)
			|| die("Cant open directory $dir for reading\n");
		my(@dirlist) = sort grep(/^$file\.\d\d\d\d\d\d\d\d/,readdir(DIR));
		closedir(DIR);

		if( $#dirlist>$versions ) {
			$#dirlist-=$versions;
			foreach (sort @dirlist) {
				_statusmsg("[save] ... Removing $dir/$_\n" ) unless $do_statusmsg;
				unlink $dir."/".$_;
			}
		}
		#print "DBG ARCHIVER - versions=$versions dir=$dir file=$file\n\t==>",join("\n\t==>",@dirlist),"\n";
	}
	if( ! $do_statusmsg ) {
		_statusmsg("[save] ... Archive Completed\n" );
	}

	return $tofile;
}

# LOGGING FUNCTIONS
my(@message_log);
sub start_msg_logging {
	my($msg)=@_;
	@message_log=();
	push @message_log,$msg;
	push @message_log,"started at ".localtime(time)."\n";
}

sub end_msg_logging {
	my(@rc);
	push @rc,@message_log;
	@message_log=();
	return @rc;
}

sub _debugmsg {
	my($msg)=join("",@_);
	if( defined $DEBUG ) {
		$msg=~s/\] /\]\t\t\<dbg\> /;
		_statusmsg($msg);
	} else {
		if( ! defined $NOLOGFILE ) {
			foreach( split(/\n/,$msg) ) { chomp; print LOGFP "\t\t<dbg> $_\n"; }
		}
	}
}

sub _statusmsg {
    my $cmd = shift;
    my $msg = join("",@_);
    if( $cmd ne "noprint" ) {
    	$msg = $cmd.$msg;
    	$cmd = "";
    }

    chomp $msg;
    if( $msg =~ /\[/ ) {
    	my($p,$m)=split(/\s/,$msg,2);
    	if( $SHOWTIMINGS ) {
    		$msg=sprintf("%-12.12s|%d|%s",$p,(time-$application_starttime)%1000,$m);
    	} else {
    		$msg=sprintf("%-12.12s| %s",$p,$m);
    	}
    }

    if( $cmd eq "" ) {
    	if( $print_stdout eq "TRUE" ) {
    		foreach( split(/\n/,$msg) ) {
    			print ">> $_\n" unless $opt_q;
    			print LOGFP ">> $_\n" unless defined $NOLOGFILE;
    		}
    	} else {
	    	foreach( split(/\n/,$msg) ) {
	    		print LOGFP ">> $_\n" unless $NOLOGFILE;
	    	}
    	}
    }

    if ( $status_bar_ready eq "TRUE" and defined $statusmsgbar and $msg !~ /^\#\#\#\#/ ) {
    	if( ! $msg ) {
    		$msg = "Ready";
    	} else {
    		push @message_log, $msg if $#message_log != -1;
    		$msg = reword($msg,50, 90);
    	}

    	#if( length($msg)>100 ) {	# split it on white spaces
    	#	my(@m)=split(/\n/,$msg);
    	#	my($mout)="";
    	#	foreach (@m){
    	#		if( length < 80 ) {
    	#			$mout.=$_."\n";
    	#		} else {
    	#			my(
    	#		}
    	#	}
    	#}

      $statusmsgbar->configure( -text => "$msg" );
      $statusmsgbar->update;
    }
}
# END LOGGING FUNCTIONS

sub read_all_configfiles {
	my($readconfig,$debug)=@_;
	$cur_hostname=hostname();

	_statusmsg("[cfgfiles] Reading Configuration Files - $readconfig\n" );

	my($AM_I_INITIALIZED)=copy_sample_files() if $readconfig eq "FULL";
	print "... Reading configuration data\n" if defined $debug;
	print LOGFP "... Reading configuration data\n"
		if defined $debug and ! defined $NOLOGFILE;
	_statusmsg("[cfgfiles] Parsing Key Values\n" );

	$CURRENT_CONFIG{current_hostname}			=	$cur_hostname;
	$CURRENT_CONFIG{admin_scripts_dir}			=	$CURRENT_CONFIG{gem_root_directory}."/ADMIN_SCRIPTS";
	($CURRENT_CONFIG{VERSION},$CURRENT_CONFIG{sht_version})	=	get_version();
	$CURRENT_CONFIG{isnt}  				= 	is_nt();

	# $CURRENT_CONFIG{sqlserver_list}  = qs_run_osql() if is_nt();

	#$GemConfig->{documenter_do_copy} = "N" unless $GemConfig->{documenter_do_copy};

	# gem_repository_initialize();

	print "... Reading configfile\n" if defined $debug;
	print LOGFP "... Reading configfile\n" unless defined $NOLOGFILE;
	# %CONFIG=read_configfile(-nodie=>1, -debug=>$debug);
	$GemConfig=read_gemconfig( -debug=>$debug, -nodie=>1);
	if( $GemConfig->{ERRORS} ) {
		foreach ( @$GemConfig->{ERRORS} ) {
			_statusmsg("[cfgfiles] $_ \n" );
		}
	}

	# set any dynamic thing ies
	if( $AM_I_INITIALIZED eq "FALSE" ) {
		if( ! $GemConfig->{INSTALLTYPE}  ) {
			if( is_nt() ) {
				$GemConfig->{INSTALLTYPE} = "WINDOWS";
			} else {
				$GemConfig->{INSTALLTYPE} = "UNIX";
			}
		}

#		if( ! is_nt() and ! $GemConfig->{UNIX_BROWSER_LOCATION} ) {
#			$GemConfig->{UNIX_BROWSER_LOCATION}="/usr/local/bin/firefox"
#				if ! $GemConfig->{UNIX_BROWSER_LOCATION} and -x "/usr/local/bin/firefox";
#			$GemConfig->{UNIX_BROWSER_LOCATION}="/usr/bin/firefox"
#				if ! $GemConfig->{UNIX_BROWSER_LOCATION} and -x "/usr/local/bin/firefox";
#			$GemConfig->{UNIX_BROWSER_LOCATION}="/usr/local/bin/mozilla"
#				if ! $GemConfig->{UNIX_BROWSER_LOCATION} and -x "/usr/local/bin/firefox";
#			$GemConfig->{UNIX_BROWSER_LOCATION}="/usr/bin/mozilla"
#				if ! $GemConfig->{UNIX_BROWSER_LOCATION} and -x "/usr/local/bin/firefox";
#		}
	}

	read_password_vault();

	verify_license_key(1);

	$GemConfig->{MIMI_IS_WIN32}="N" unless $GemConfig->{MIMI_IS_WIN32};
	#$GemConfig->{MIMI_LOGIN_WIN32}=$ENV{USERDOMAIN}."\\".$ENV{USERNAME}
	#	if ! $GemConfig->{MIMI_LOGIN_WIN32} and $ENV{USERDOMAIN} and $ENV{USERNAME} and is_nt();

	$GemConfig->{MIMI_CGIBIN_PERL_WIN32} = "C:/perl/bin/perl.exe"  unless $GemConfig->{MIMI_CGIBIN_PERL_WIN32};
	$GemConfig->{MIMI_CGIBIN_PERL_UNIX}  = "/usr/local/bin/perl"   unless $GemConfig->{MIMI_CGIBIN_PERL_UNIX};

	$GemConfig->{UNIX_SYBASE1} = "sybase"  unless $GemConfig->{UNIX_SYBASE1};
   $GemConfig->{UNIX_MYSQL1}  = "mysql" 	unless $GemConfig->{UNIX_MYSQL1};
   $GemConfig->{UNIX_ORACLE1} = "oracle"  unless $GemConfig->{UNIX_ORACLE1};

	if( $GemConfig->{INSTALLTYPE} eq "NA" ) {
		if( is_nt() ) {
			$GemConfig->{INSTALLTYPE}="WINDOWS";
			$GemConfig->{USE_SQLSERVER}="Y";
		} else {
			$GemConfig->{INSTALLTYPE}="UNIX";
			$GemConfig->{USE_SQLSERVER}="N";
		}
	}

	if( $ENV{SYBASE} ) {
		my($found);
		my($firstblank);
		for (1..9) {
			$found="YES" if $GemConfig->{"SYBASE".$_} eq $ENV{SYBASE};
			$firstblank=$_ if ! $firstblank and ! $GemConfig->{"SYBASE".$_};
		}
		$GemConfig->{"SYBASE".$firstblank}=$ENV{SYBASE} if ! $found and $firstblank;
	}

	$CURRENT_CONFIG{downarrow}=Tk->findINC('cbxarrow.xbm');
	
	my(%newincludes);
	foreach (@INC) {
		if( $_ !~ /$CURRENT_CONFIG{gem_root_directory}/ ){
				last;
		}
		s?\\?\/?g;
		$newincludes{ $_ }=1;
		_debugmsg("[cfgfiles] Adding Perl Include Lib : $_\n" );
		print LOGFP "  Adding Perl Include Lib : $_\n" unless defined $NOLOGFILE;
	}
	
	$newincludes{ $GemConfig->{NT_CODE_FULLSPEC}."/lib" } = 1;
	$newincludes{ $GemConfig->{NT_CODE_FULLSPEC}."/lib/Win32_perl_lib_5_8" } = 1;	
	
#	if( $GemConfig->{ALT1_FULLPATH} ) {
#		$newincludes{ $GemConfig->{ALT1_FULLPATH}."/lib" } = 1;
#		$newincludes{ $GemConfig->{ALT1_FULLPATH}."/lib/Win32_perl_lib_5_8" } = 1;
#	}
#	
#	if( $GemConfig->{ALT2_FULLPATH} ) {
#		$newincludes{ $GemConfig->{ALT2_FULLPATH}."/lib" } = 1;
#		$newincludes{ $GemConfig->{ALT2_FULLPATH}."/lib/Win32_perl_lib_5_8" } = 1;
#	}
#	
	$newincludes{ $GemConfig->{UNIX_CODE_LOCATION}."/lib" } = 1;
	$newincludes{ $GemConfig->{ALT1_CODE_LOCATION}."/lib" } = 1;
	$newincludes{ $GemConfig->{ALT1_CODE_LOCATION}."/lib/Win32_perl_lib_5_8" } = 1;
	$newincludes{ $GemConfig->{ALT2_CODE_LOCATION}."/lib" } = 1;
	$newincludes{ $CURRENT_CONFIG{gem_root_directory}."/lib" } = 1;
		
	if( defined $ENV{perlargs} ) {
		my($x)=$ENV{perlargs};
		$x=~s/^\s+//;
		$x=~s/\s+$//;
		$x=~s/^-I//;
		
		$newincludes{ $x } = 1;
	}
	
	my(%newincludes2);
	foreach (keys %newincludes) { s?\\?\/?g; $newincludes2{$_}=1; }
	@perl_includes = keys( %newincludes2 );
	
	#foreach (@perl_includes) {
	#	print __LINE__," PI=$_\n";
	#}
		
	print "Set perl includes to ",	join(" ",@perl_includes) ,"\n";
	print "Completed reading files\n" if defined $debug;
	print LOGFP "Completed reading files\n" if defined $debug and ! defined $NOLOGFILE;

}

sub copy_sample_files {
	opendir(DIR,$CURRENT_CONFIG{gem_configuration_directory})
		or l_die("copy_sample_files() Cant open directory $CURRENT_CONFIG{gem_configuration_directory} for reading : $!\n");
	my(@dirlist)=grep(!/^\./,readdir(DIR));
	closedir(DIR);
	my(%conf_file);
	my($num_copies)=0;
	foreach( @dirlist ) { $conf_file{$_}=1;	}
	foreach( @dirlist ) {
		next unless /.sample$/;
		next if /crontab.sample/;
		my($f)=$_;
		$f =~ s/.sample$//;
		next if $conf_file{$f};
		_statusmsg( "[func] creating conf/$f from sample file\n" );
		print LOGFP "creating conf/$f from sample file\n"
			 unless defined $NOLOGFILE;
		$num_copies++;
		copy("$CURRENT_CONFIG{gem_configuration_directory}/$f.sample","$CURRENT_CONFIG{gem_configuration_directory}/$f") or l_die("cant copy $f : $!");
		chmod 0660, "$CURRENT_CONFIG{gem_configuration_directory}/$f";
	}

	return "TRUE" if $num_copies>0;
	return "FALSE";
}

# verify_license_key() is called when you select free/full gem
# my($lk)='Gemr0cks!';
sub verify_license_key {
	
	$GemConfig->{IS_FREE_GEM}="NO";
	return 1;
	
#	my($passed_in_key)=@_;
#	if( $passed_in_key ) {
#		 if( $GemConfig->{GEM_LICENSE_KEY} eq $lk ) {
#		 	$GemConfig->{IS_FREE_GEM}="NO";
#		} else {
#			$GemConfig->{IS_FREE_GEM}="YES";
#		}
#		return;
#	}
#	return if $GemConfig->{IS_FREE_GEM} eq "YES";	# value is changed before getting here
#	return unless $CURRENT_CONFIG{mainWindow};
#
#	my $dlg= $CURRENT_CONFIG{mainWindow}->DialogBox(
#		-title => 'Enter License Key',
#		-buttons => [ "Ok", "Cancel" ] );
#	my $key="ENTER KEY HERE";
#
#   $dlg->add("Label",-font=>"norm",-text=>"ENTER LICENSE KEY\nContact SQL TECHNOLOGIES\nat moreaboutgem\@gmail.com\nfor a key.", -justify=>'center',
#		-bg=>'beige', -relief=>'sunken')->pack(-side=>'top',-fill=>'x') ;
#
#	my($r)=$dlg->add("Entry",-bg=>'white',-font=>"large",-textvariable=>\$key,-width=>30, -relief=>'groove')->pack(-side=>'top',-fill=>'x');
#	$r->focus();
#
#	my($rc)=$dlg->Show();
#	if( $rc eq "Ok" ){
#		if( $key eq $lk ) {	# ok ok - its not very secure at all...
#			_statusmsg("[keygen] LICENSE KEY OK\n");
#			$GemConfig->{GEM_LICENSE_KEY}=$key;
#			return;
#		}
#		_statusmsg("[keygen] LICENSE KEY INCORRECT\n");
#		_statusmsg("[configure] Displaying message box - select a button to continue\n");
#		$CURRENT_CONFIG{mainWindow}->messageBox(
#					-title => 'License Incorrect',
#					-message => "ERROR:  License Key Incorrect K=$key, lk=$lk\n",
#					-type => "OK" );
#		
#		_statusmsg("[ready] GEM Ready / Continuing...\n");
#	}
#	$GemConfig->{IS_FREE_GEM}="YES";
}


my($busy_count)=0;
sub is_busy
{
	_statusmsg("[func] skipping button click as application is busy\n" ) if $busy_count;
	return $busy_count;
}

sub busy
{
	$busy_count++;
	_debugmsg("[func] busy() busy_count=$busy_count\n" );
	$CURRENT_CONFIG{mainWindow}->configure(-cursor=>'watch');
}

sub unbusy
{
	my($clear)=@_;
	$busy_count-- if $busy_count>0;
	$busy_count=0 if $clear;
	_debugmsg("[func] unbusy() busy_count=$busy_count\n" );
	$CURRENT_CONFIG{mainWindow}->configure(-cursor=>'arrow') if $busy_count==0;
}

sub set_status_label {
	my($widget,$txt)=@_;
	chomp $txt;
	$statusmsgbar->configure(-text=>$txt);
}
my $balloon;
sub dohelp {
	my($widget,$help)=@_;

	if( ! defined $CURRENT_CONFIG{mainWindow} ) {
		foreach (keys %CURRENT_CONFIG) {			print "$_ -> $CURRENT_CONFIG{$_}\n";		}
		l_die("mainWindow undefined\n");
	}
	if( ! defined $widget or ! Tk::Exists($widget) ) {
		confess("Fatal error in dohelp($help) at ",__FILE__," ",__LINE__,"\n");
		l_die("widget $widget not a tk widget \n");
	}
	$balloon = $CURRENT_CONFIG{mainWindow}->Balloon()	#-statusbar=>$statusmsgbar)
		unless defined $balloon;

	$balloon->attach($widget,-msg=>$help);

	my(@txt);

#	foreach ( split(/\n/,$help) ) {
#		if( length > 100 ) {
#			my($msg) = reword($_,50, 90);
#			foreach ( split(/\n/,$msg) ) { push @txt,$_; }
#		} else {
#			push @txt, $_;
#		}
#	}

	if( $#txt >= $main::NUM_STATUS_LINES ){
		$help="";
		for my $i (0..$main::NUM_STATUS_LINES){
			$help.=$txt[$i]."\n";
		}
		chomp $help;
	}

	#if( ! $msg ) {
    	#	$msg = $_statusmsg_idle;
    	#} else {
    	#	$msg = reword($msg,50, 90);
    	#}

	$widget->bind('<Enter>',[\&set_status_label, $help]);
	$widget->bind('<Leave>', [\&set_status_label, ""]);

	return $widget;
}

#sub get_install_key {
#	my($key,$host)=@_;
#	return $key;
#}

# get_install_color($key) returns white if set and yellow if not set
sub get_install_color {
	my($key)=@_;
	my($rc)=get_install_variable($key);
	return "white" if $rc;
	return "yellow";
}

# set_install_variable($key,$unsetflag)
#  - key = get_install_key($key)
#  - set GemConfig->{$key}=time (or delete it)
#  - turns on/off Graphics Objects $key and I_$key
sub set_install_variable {
	my($key,$unsetflag)=@_;
	die "set_install_variable() NO HOSTNAME DEFINED TO GemData->new()" unless $cur_hostname;
	# my($fullkey)=get_install_key($key);
	if( ! $graphics_object{$key} ) {
		die "ERROR - set_install_variable() NO GRAPHICS OBJECT NAMED $key\n";
	}
	if( $unsetflag ) {
		$graphics_object{$key}->configure(-bg=>'yellow');
		$graphics_object{"I_".$key}->configure(-bg=>'yellow') if $graphics_object{"I_".$key};
		delete $GemConfig->{$key};
	} else {
		die "ERROR NO GRAPHICS OBJECT NAMED $key\n" unless $graphics_object{$key};
		$graphics_object{$key}->configure(-bg=>'white');
		$graphics_object{"I_".$key}->configure(-bg=>'white') if $graphics_object{"I_".$key};
		$GemConfig->{$key}=time();
	}
}

# get_install_variable($key,$host) : return GemConfig->{get_install_key($key,$host)}
sub get_install_variable {
	my($key,$host)=@_;
	die "get_install_variable() NO HOSTNAME DEFINED TO GemData->new()" unless $cur_hostname or $host;
	# my($fullkey)=get_install_key($key,$host);
	return $GemConfig->{$key};
}

sub set_install_step {
	my($key,$unsetflag)=@_;
	if( $unsetflag ) {
		_statusmsg( "[func] set_install_step($key,$unsetflag)\n" );
	} else {
		_statusmsg( "[func] set_install_step($key)\n" );
	}
	my($t) = set_install_variable($key,$unsetflag);
	return $t if $unsetflag;

	# my(%requiredsteps)=get_required_install_steps();
	my(@errs);
	if( ! $UPGRADE and ! $unsetflag and $key ne "INSTALLED" ) {
		my($badcnt)=0;
		# foreach ( sort (keys %requiredsteps) ) {
		_statusmsg( "[func] \n" );
		_statusmsg( "[func] *** SYSTEM STATE DUMP ***\n" );
		foreach ( sort ( get_required_install_steps() )) {
			my($t)= get_install_variable( $_ );
			my($llt);
			$llt= localtime($t) if $t != 0;
			my($s)=sprintf( "%-33s %s\n",$_ , $llt);
			_statusmsg( "[func] $s" );
			$badcnt++ if ! $t and $_ ne "INSTALLED" and $_ ne "VERIFY";
		}
		_statusmsg( "[func] \n" );
		if( $badcnt==0 ) {
			if( $GemConfig->{INSTALLSTATE} ne "INSTALLED"  ) {
				_statusmsg( "[func] MARKING INSTALL COMPLETED\n" );
				set_install_step("INSTALLED");
				set_install_major_step("INSTALLED");
			} else {
				_statusmsg( "[func] INSTALL STATE = $GemConfig->{INSTALLSTATE}\n" );
			}
		} else {
			_statusmsg( "[func] THIS INSTALL IS NOT COMPLETED\n" );
		}
	}

	if( ! $UPGRADE ) {
		# might not be defined if u are doing an upgrade
		#if( $key eq "CONNECT_SURVEY" ) {
		#	l_die("NO CONNECT SURVEY") unless defined $graphics_object{CONNECT_SURVEY};
		#	$graphics_object{FINAL_SURVEY}->configure(-bg=>"white");
		#} elsif( $key eq "CONNECT_TO_ALL" ) {
		#	l_die("NO CONNECT SURVEY") unless defined $graphics_object{CONNECT_TO_ALL};
		#	$graphics_object{CONNECT_TO_ALL}->configure(-bg=>"white");
		#} elsif( $key eq "INSTALLED" ) {
			# set_install_variable("DO_INSTALL_REFORMAT",$unsetflag);
			# set_install_variable("DO_INSTALL_CREATE_BATCHJOBS",$unsetflag);
		#	set_install_variable("DO_INSTALL_CONSOLE",$unsetflag);
		#} else {
		#	if( 	! get_install_variable('INSTALLED')
			# and 	get_install_variable('DO_INSTALL_REFORMAT')
			# and 	get_install_variable('DO_INSTALL_CREATE_BATCHJOBS')
		#	and 	get_install_variable('DO_INSTALL_CONSOLE')) {
		#		set_install_variable("INSTALLED",$unsetflag);
		#		$graphics_object{INSTALLED}->configure(-bg=>"white");
		#	}

		#	if( defined $graphics_object{INSTALLED} ) {
		#		$graphics_object{INSTALLED}->configure(-bg=>"white")
		#			if $key eq "INSTALLED";
				#$graphics_object{DO_INSTALL_REFORMAT}->configure(-bg=>"white")
				#	if $key eq "DO_INSTALL_REFORMAT"
				#	or $key eq "INSTALLED";
				#$graphics_object{DO_INSTALL_CREATE_BATCHJOBS}->configure(-bg=>"white")
				#	if $key eq "DO_INSTALL_CREATE_BATCHJOBS"
				#	or $key eq "INSTALLED";
				#$graphics_object{DO_INSTALL_CONSOLE}->configure(-bg=>"white")
				#	if $key eq "DO_INSTALL_CONSOLE"
				#	or $key eq "INSTALLED";
		#	}
		#}
		_debugmsg( "[func] checkpoint 1\n" );

		if( $key eq "INSTALL_ALARM_DATABASE" ) {
			_debugmsg( "[func] repainting alarm objects()\n" );
			foreach ( keys %graphics_object ) {
				$graphics_object{$_}->configure(-bg=>"white") if /^ALARM/ and $_ ne "VALIDATE_ALARM_INSTALLATION";
			}
		}
		_debugmsg( "[func] checkpoint 2\n" );

		$graphics_object{$key}->configure(-bg=>"white") if defined $graphics_object{$key};# and $key=~/^Console/;
		# $graphics_object{SAVE_CONFIGURATION_CHANGES}->configure(-bg=>"pink")	if defined $graphics_object{$key};

		# turn off Final_ matching Console_
		if( $key =~ /Console/ ) {
			$key=~s/Console/Final/;
			$graphics_object{$key}->configure(-bg=>"white") if defined $graphics_object{$key};;
		}
		_debugmsg( "[func] checkpoint 3\n" );
	}
	_debugmsg( "[func] done set_install_step()\n" );
	return $t;
}

#sub test_mail_install {
#	my($GemConfig,$IsNt)=@_;
#
#	_statusmsg( "[test_mail] Calling test_mail_install()\n" );
#	return ("ERROR: Must Fill In SMTP Mail Server")	if $IsNt and ! $GemConfig->{MAIL_HOST};
#	return ("ERROR: Must Fill In Administrator Email")
#		if $GemConfig->{ADMIN_EMAIL} =~ /^\s*$/ or $GemConfig->{ADMIN_EMAIL} !~ /\@/;
#	_statusmsg( "[test_mail] Test - sending Mail to $GemConfig->{ADMIN_EMAIL}\n" );
#	my($msg)=MIME::Lite->new(
#				To=>	$GemConfig->{ADMIN_EMAIL},
#				From=>$GemConfig->{ADMIN_EMAIL},
#				Subject=>"gem install",
#				Type=>'multipart/related' );
#	return ("ERROR: Mail Test Fails") unless $msg;
#	$msg->attach(Type=>'text/html', Data=>"Gem Install: time=".scalar(localtime(time))."\n");
#	my($rc);
#	eval {
#		if( $IsNt ) {
#			_statusmsg( "[test_mail] Test - sending Mail via Mailhost '",$GemConfig->{MAIL_HOST},"'\n" );
#			$rc = $msg->send("smtp",$GemConfig->{MAIL_HOST});
#		} else {
#			_statusmsg( "[test_mail] Test - sending Unix Mail\n" );
#			$rc = $msg->send();
#		}
#	};
#	my($err)=join("",$@) if $@;
#	_statusmsg( "[test_mail] Mail Returned: ".$err ) if $err;
#	return ("ERROR: $err\n") if $err;
#	return "SUCCESSFUL: Passed Mail Tests\n";
#}

#sub test_validate_alarm_installation {
#	my($str)="$^X -I".join(" -I",@perl_includes)." $CURRENT_CONFIG{gem_root_directory}/bin/test_alarms.pl";
#	_statusmsg(  "(executing) ",$str );
#	my($rc)="";
#	open(VAL90,$str." |" ) or die("ERROR CANT RUN $str\n");
#	while( <VAL90> ) {
#		$rc.=$_;
#		_statusmsg(  "returned: ",$_ );
#	}
#	close(VAL90);
#	#$rc="" if $rc eq "SUCCESSFUL: Alarm System Ok\n";
#	return $rc;
#}

#sub test_register {
#	my($GemConfig)=@_;
#	return ( "ERROR: Must Fill In GEM Administrator Name")
#		if $GemConfig->{ADMIN_NAME}  =~ /^\s*$/;
#	return ("ERROR: Must Fill In GEM Administrator Email")
#		if $GemConfig->{ADMIN_EMAIL} =~ /^\s*$/;
#	return ("ERROR: GEM Administrator Email not legit")
#		if $GemConfig->{ADMIN_EMAIL} !~ /\@/;
##	return ("ERROR: Must Fill In GEM Administrator Phone")
##		if $GemConfig->{ADMIN_PHONENUM} =~ /^\s*$/;
#	return ("ERROR: Must Fill Environment Type")
#		if $GemConfig->{INSTALLTYPE} =~ /^\s*$/;
#	return ("SUCCESSFUL: Passed Registration Tests\n");
#}

sub update_begin_block {	# edit CommonHeader.pm	or DBIFunc.pm Begin Block For $SYBASE contents
	my($cfg_file)=@_;
	_statusmsg("[func] Update_Begin_Block( $cfg_file )\n" );
	open(ICFG1,     $cfg_file ) 			or return l_warning("Cant Open $cfg_file : $!");
	open(OCFG1, ">".$cfg_file.".tmp" ) 	or return l_warning("Cant Open $cfg_file.tmp");
	my($state)=0;
	while(<ICFG1>) {
		chomp;
		s/\s+$//;
		$_ .= "\n";
		if( $state==2 ) {
			# WE ARE DONE MODDING BLOCK... PRINT EVERYTHING
			print OCFG1 $_;
		} elsif( $state ) {
			# NOT DONE BUT ALSO NOT JUST STARTING
			if( /\s*\# WARNING - THANK YOU FOR NOT REMOVING THIS EITHER/ ) {
				$state=2;
				print OCFG1 $_;
			}
			next;
		} elsif( /\s*\# WARNING - DO NOT REMOVE THIS COMMENT/ ) {
			# NOT DONE AND STARTING AND WE FOUND OUR START PATERN
			$state=1;
			print OCFG1 $_,"\t#******************\n";
			for my $i (1..9) {
				next unless $GemConfig->{"SYBASE".$i};
				$GemConfig->{"SYBASE".$i} =~ s/\s+$//;
				if( defined $GemConfig->{"SYBASE".$i} and $GemConfig->{"SYBASE".$i} =~ /\// ) {
						print OCFG1 "\t\t} elsif( -d \"".$GemConfig->{"SYBASE".$i}."\" ) {\n\t\t\t\$ENV{SYBASE}=\"".$GemConfig->{"SYBASE".$i}."\";\n";
				}
			}
			print OCFG1 "\t#******************\n"
		}	else {
			# NOT DONE AND STARTING AND NO START PATERN YET
				print OCFG1 $_;
		}
	}
	close(ICFG1);
	close(OCFG1);
	if( compare($cfg_file.".tmp",$cfg_file) != 0 ) {
		_statusmsg("[func] ... Archiving & OverWriting $cfg_file\n" );
		archive_file($cfg_file,2);
		rename($cfg_file.".tmp",$cfg_file)
			or return l_warning("Cant rename $cfg_file.tmp to $cfg_file : $!");
	} else {
		_statusmsg( "[func] $cfg_file Unchanged (not saving)\n" );
		print LOGFP "Identical Files Found\n" unless defined $NOLOGFILE;
		unlink $cfg_file.".tmp";
	}
	chmod( 0666, $cfg_file ) or warn "Cant Chmod $cfg_file $!\n";
}

#sub unimplemented {
#	my($string)=@_;
#	_statusmsg("[func]  (not) $string");
#   my $dialog = $CURRENT_CONFIG{mainWindow}->DialogBox(-title=>"Unimplemented", -buttons=>["Ok"]);
#   $dialog->Label(-text=>"<To Do> $string")->pack();
#   $dialog->Show;
#}

1;