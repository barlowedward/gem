#!/apps/perl/linux/perl-5.8.2/bin/perl

# copyright (c) 2005-2008 by SQL Technologies.  All Rights Reserved.
# Explicit right to use can be found at www.edbarlow.com

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use MlpAlarm;
use Getopt::Long;
use CommonFunc;
use Repository;
use DBIFunc;

use vars qw( $TYPE $BATCH_ID $DEBUG $SVR );

my($heartbeat_debug);		# set to 1 to just to debug mlpheartbeats

sub usage
{
   print @_;
   print "BkpChk_UpdateStaticInfo.pl --BATCH_ID=id [--TYPE=sqlsvr|sybase] --DEBUG\n";
   return "\n";
}

$| =1;

die usage("Bad Parameter List\n") unless GetOptions(
		"DEBUG"			=> \$DEBUG ,
		"BATCH_ID=s"	=> \$BATCH_ID ,
		"SVR=s"			=> \$SVR ,
		"TYPE=s"			=> \$TYPE ,
	);

$BATCH_ID="UpdateBackupStateStaticInfo" unless $BATCH_ID;

MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -SUBKEY => $TYPE );

print("Reading crosscheck.dat\n");
my(@crosscheckdat)=get_conf_file_dat('crosscheck');
my(%server_aliases);
my(%ignore_server,%ignore_server_db);
foreach (@crosscheckdat) {
	chomp;
	if( /^SERVER_ALIAS/i ) {
		my($key,@d)=split;
		foreach my $t (@d) {
			$server_aliases{$t} = \@d;
		}
	}
	#BACKUP_ALARM_THRESHOLD ADSRV090 ReportServerTempDB  200
	#BACKUP_ALARM_THRESHOLD ADSRV088 -1
	if( /^BACKUP_ALARM_THRESHOLD/i ) {
		my(@x)=split;
		if( $#x == 2 ) {		# ignore server?
			$ignore_server{$x[1]} = 1 if $x[2] == -1;
		} elsif( $#x == 3 ) {
			$ignore_server_db{$x[1].":".$x[2]} = 2;
			$ignore_server_db{$x[1].":".$x[2]} = 1 if $x[3] == -1;
		}
	}
}

my(@rc)=MlpGetBackupState();
my(%keylist);	# by $SERVER/$db
foreach ( @rc ) {
	my(@d)=dbi_decode_row($_);
	my($server)=shift @d;
	my($db)=shift @d;
	my($key)=$server."/".$db;
	$keylist{$key}="EXISTS_NOMORE";		# vs NEW or UPDATED
}

if( is_nt() and ( $TYPE eq "sqlsvr" or ! defined $TYPE )) {
	my @servers =  get_password(-type=>'sqlsvr');

	my($status1bits) = 32+64+128+256+512+1024+4096+32768;
	my($query)='select name,has_dbaccess(name), tl=(status&8) / 8, si=(status&4)/4,
		ro=(status&1024)/1024, su=(status&4096)/4096, bu=(status&'.$status1bits.')
	from master..sysdatabases'."\n";

	print $query if $DEBUG;

	foreach my $srv (@servers) {
		next if defined $SVR and $srv ne $SVR;
		print $srv,"\n";
		my($login,$pass,$ctype)=get_password(-type=>'sqlsvr',-name=>$srv);
		my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
		if( ! $rcx ) {
		      	print "***CONNECT FAILURE TO $srv\n";
			next;
		}
		my(@rc)=dbi_query(-db=>"master",-query=>$query);
		foreach ( @rc ) {
			my($nm,$is_useable,$is_tran_truncated,$is_select_into,$is_readonly,$is_singleuser,$is_backupable)=dbi_decode_row($_);

			next if $nm eq "tempdb" or $nm eq "model";

			my($is_systemdb)=0;
			$is_systemdb=1 if $nm eq "master" or $nm eq "model" or $nm =~ /tempdb/
				or $nm eq 'syblogins' or $nm eq 'pubs'  or $nm eq 'pubs2' or $nm eq 'msdb' or $nm eq 'Northwind' or $nm eq "dbccdb"
				or $nm eq 'sybsystemdb' or $nm eq 'sybsecurity' or $nm eq 'sybsystemprocs' or $nm eq 'ReportServer'  or $nm eq "sybmgmtdb"
				or $nm eq 'ReportServerTempDB';
			my($is_backupable)=$is_useable;
			$is_backupable=0 if $is_singleuser;
			$is_backupable=0 if $is_systemdb==1 and $nm!='master' and $nm!='msdb';

			if( defined $ignore_server_db{$srv.":".$nm} ) {
				$is_backupable=$ignore_server_db{$srv.":".$nm} -1;
			} elsif( $ignore_server{$srv} ) {
				$is_backupable=0;
			}

			my($key)=$srv."/".$nm;
			print " => MlpSetBackupState(srv=$srv db=$nm usable=$is_useable trunc=$is_tran_truncated key=$key si=$is_singleuser)\n";

			if( defined $keylist{$key} ) {	# ok it exists
				$keylist{$key}="UPDATED";
			} else {
				$keylist{$key}="NEW";
			}

			MlpSetBackupState(
					-system					=> $srv,
					-dbname					=> $nm,
					-is_tran_truncated	=> $is_tran_truncated,
					-debug					=> $DEBUG,
					-is_readonly			=> $is_readonly,
					-is_select_into		=> $is_select_into,
					-is_singleuser			=> $is_singleuser,
					-is_backupable			=> $is_backupable,
					-is_system_db			=> $is_systemdb,
					-is_useable				=> $is_useable
				);

				if( $server_aliases{$srv} ) {
					my($aryref)=$server_aliases{$srv};
					foreach (@$aryref ) {
						next if $_ eq $srv;
						print " => (ALIAS FOUND FOR $srv OF $_)\n";
						print " => MlpSetBackupState(srv=$_ db=$nm usable=$is_useable trunc=$is_tran_truncated key=$key si=$is_singleuser)\n";
						MlpSetBackupState(
								-system					=> $_,
								-dbname					=> $nm,
								-is_tran_truncated	=> $is_tran_truncated,
								-is_useable			=> $is_useable,
								-debug					=> $DEBUG,
								-is_readonly			=> $is_readonly,
								-is_select_into		=> $is_select_into,
								-is_singleuser=> $is_singleuser,
								-is_backupable=> $is_backupable,
								-is_system_db			=> $is_systemdb,
							);
					}
				}
		}

if( $SVR ) {		# for debugging only
	print "\n";
	foreach ( sort keys %keylist ) { print "DBG DBG: $_ $keylist{$_}\n" if /^$SVR/ and $keylist{$_} ne "UPDATED"; }
}

		# if we actually got on the server... we can delete any db's that no longer exist...
		foreach my $k ( keys %keylist ) {
			next unless $keylist{$k} eq "EXISTS_NOMORE";
			next unless $k=~/^$srv\//;
			print "\n   Checking $k - Database may not exist\n";

			# Database is in the server but is not found locally!  The update did, however, hit above due to the proc
			my($skipit)="";
			foreach ( keys %keylist ) {
				next unless lc($_) eq lc($k) and $_ ne $k;
				$skipit=$_;
				last;
			}
			if( $skipit ) {
				print "   Case Mismatch $k and $skipit - not deleting\n";
				next;
			}

			my($s,$d)=split("/",$k);
			die "WOAH: $s != $srv" unless $s eq $srv;
			print " => MlpSetBackupState( -system=>$s, -dbname=>$d, -delete=>1 );\n";
			MlpSetBackupState( -system	=> $s, -dbname	=> $d,-delete => 1, -debug					=> $DEBUG, );
		}
		dbi_disconnect();
		print " done\n";
	}
}

if( $TYPE eq "sybase" or ! defined $TYPE ) {
	my @servers = get_password(-type=>'sybase');
	# 1120= 1000 (single usr) + 100 (suspect) + 20 (for load)
   #   30= 20 (offline until recovery) + 10 (offline)
	my($status1bits) = 0x1120;
	my($status2bits) = 0x0030;
	my($query)="select name, (status&$status1bits) + (status2&$status2bits),
		tl=(status&8) / 8, 	si=(status&4)/4, ro=(status&1024)/1024, su=(status&4096)/4096
		from master..sysdatabases";
	my($query2)='select srvnetname from master..sysservers where srvname="SYB_BACKUP"';

	foreach my $srv (@servers) {
		next if defined $SVR and $srv ne $SVR;
		print "$srv\n";
		my($login,$pass,$ctype)=get_password(-type=>'sybase',-name=>$srv);
		my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
		if( ! $rcx ) {
		   print "***CONNECT FAILURE TO $srv\n";
			next;
		}

		my(@rc2)=dbi_query(-db=>"master",-query=>$query2);
		my($srvnetname);
		foreach ( @rc2 ) { ($srvnetname)=dbi_decode_row($_); }
		if (! $srvnetname ) {
			warn "Warning server $srv does not have SYB_BACKUP sysserver\n";
			$srvnetname="ERROR";
		}
		printf " => BkpServerName=%s\n",$srvnetname;

		my(@rc)=dbi_query(-db=>"master",-query=>$query);
		foreach ( @rc ) {
			my($nm,$is_useable,$is_tran_truncated,$is_select_into,$is_readonly,$is_singleuser)=dbi_decode_row($_);
			next if $nm eq "tempdb" or $nm eq "model";
			if( $is_useable ) {
				$is_useable = 0;
			} else {
				$is_useable = 1;
			}
			my($is_systemdb)=0;
			$is_systemdb=1 if $nm eq "master" or $nm eq "model" or $nm =~ /tempdb/
				or $nm eq 'syblogins' or $nm eq 'pubs'  or $nm eq 'pubs2' or $nm eq 'msdb' or $nm eq 'Northwind' or $nm eq "dbccdb"
				or $nm eq 'sybsystemdb' or $nm eq 'sybsecurity' or $nm eq 'sybsystemprocs' or $nm eq 'ReportServer' or $nm eq "sybmgmtdb"
				or $nm eq 'ReportServerTempDB';
			my($is_backupable)=$is_useable;
			$is_backupable=0 if $is_singleuser;
			$is_backupable=0 if $is_systemdb==1 and $nm!='master' and $nm!='msdb';

			if( defined $ignore_server_db{$srv.":".$nm} ) {
				$is_backupable=$ignore_server_db{$srv.":".$nm} -1;
			} elsif( $ignore_server{$srv} ) {
				$is_backupable=0;
			}

			my($key)=$srvnetname."/".$nm;

#			if( $DEBUG ) {
				print " => MlpSetBackupState(srv=$srv -system=$srvnetname db=$nm usable=$is_useable trunc=$is_tran_truncated key=$key si=$is_singleuser)\n";
#			} else {
#				print ".";
#			}
			if( defined $keylist{$key} ) {	# ok it exists
				$keylist{$key}="UPDATED";
			} else {
				$keylist{$key}="NEW";
			}

			MlpSetBackupState(
					-debug					=> $DEBUG,
					-system					=> $srv,
					-dbname					=> $nm,
					-is_tran_truncated	=> $is_tran_truncated,
					-is_useable				=> $is_useable,
					-is_readonly			=> $is_readonly,
					-is_select_into		=> $is_select_into,
					-is_singleuser			=> $is_singleuser,
					-is_backupable			=> $is_useable,
					-is_system_db			=> $is_systemdb,
				);

#	is_useable					char(1)			null,
#	is_backupable           char(1)        null,
#	requires_backup			char(1)			null,

			if( $server_aliases{$srv} ) {
				my($aryref)=$server_aliases{$srv};
				foreach (@$aryref ) {
					next if $_ eq $srv;
					print " => (ALIAS FOUND FOR $srv OF $_)\n";
					print " => MlpSetBackupState(srv=$_ db=$nm usable=$is_useable trunc=$is_tran_truncated key=$key si=$is_singleuser)\n";
					MlpSetBackupState(
						-debug					=> $DEBUG,
						-system					=> $srv,
						-dbname					=> $nm,
						-is_tran_truncated	=> $is_tran_truncated,
						-is_useable				=> $is_useable,
						-is_readonly			=> $is_readonly,
						-is_select_into		=> $is_select_into,
						-is_singleuser			=> $is_singleuser,
						-is_backupable			=> $is_backupable,
						-is_system_db			=> $is_systemdb,
					);
				}
			}
		}

		foreach my $k ( keys %keylist ) {
			next unless $keylist{$k} eq "EXISTS_NOMORE";
			next unless $k=~/^$srvnetname\//;

			print "\n  Checking $k - Database does not exist\n";

			# Database is in the server but is not found locally!  The update did, however, hit above due to the proc
			my($skipit)="";
			foreach ( keys %keylist ) {
				next unless lc($_) eq lc($k) and $_ ne $k;
				$skipit=$_;
				last;
			}
			if( $skipit ) {
				print "   Case Mismatch $k and $skipit - not deleting\n";
				next;
			}

			my($s,$d)=split("/",$k);
			die "WOAH: $s != $srvnetname" unless $s eq $srvnetname;
			print " => MlpSetBackupState( -system=>$s, -dbname=>$d, -delete=>1 );\n";
			MlpSetBackupState( -system	=> $s, -dbname	=> $d,-delete => 1, -debug	=> $DEBUG, );
		}
		dbi_disconnect();
		print " done $srv\n";
	}
}

MlpOldBackupState();
MlpBatchJobEnd();
print "\nCompleted processing at ".localtime(time)."\n";
exit(0);


=head1 NAME

BkpChk_UpdateStaticInfo.pl - Update Backup Configuration Info For Reports

=head2 USAGE

	mssql_msdb_report.pl

=head2 DESCRIPTION

This program interrogates the msdb tables in order to create variables that can be used by the GEM
backup scripts or whatever.  Basically, it dumps the msdb tables in a reasonable ascii format that you
can read.

=cut

exit(0);

__END__

=head1 NAME

UPdateBackupStateStaticInfo.pl - Get Backup times And Save Them

=head2 USAGE

	UPdateBackupStateStaticInfo.pl -UUSER -SSERVER -PPASS -nmax_num_users

=head2 DESCRIPTION

Updates the static state information in the BackupState table for reporting purposes.
Specifically, this program will
  a) rectify the rows by removing rows for databases that no longer exist
  b) update the is_tran_truncated, is_useable, and from_internal_system_name fields

This program needs be run rarely (weekly?) and will be spawned by the Weekly Console Rebuild.

=cut

