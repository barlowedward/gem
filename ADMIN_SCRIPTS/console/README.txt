NOTES ON CONSOLE BUILD

# disadvantage of an xml database is that there is no implicit file locking and that is
# on the to-do list.
#
# The Survey scripts need to be run daily
#  survey_windows  - only will run on windows
#  survey_sqlserver - only will run on windows
#  survey_oracle
#  survey_sybase
#  survey_unix - works both windows and unix#
#
# BuildConsoleXXX jobs build the documentation tree (important) not that XXX
#   indicates the level of the rebuild (weekly>nightly>hourly).
#############################################
7,17,27,37,47,57 * * * 0-6   /apps/sybmon/dev/unix_batch_scripts/batch/ConsoleBuildAndFtp.ksh

	/usr/local/bin/perl-5.8.2  /apps/sybmon/dev/ADMIN_SCRIPTS/console/console_build_and_ftp.pl $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleBuildAndFtp.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleBuildAndFtp.err

		document_all.pl --BATCHID=ConsoleBuildAndFtp
		ftp_to_website.pl

6 * * * *                /apps/sybmon/dev/unix_batch_scripts/batch/ConsoleUnixHourly.ksh

	/usr/local/bin/perl-5.8.2  /apps/sybmon/dev/ADMIN_SCRIPTS/console/console_hourly.pl $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleUnixHourly.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleUnixHourly.err

56 0 * * *               /apps/sybmon/dev/unix_batch_scripts/batch/ConsoleUnixNightly.ksh

	/usr/local/bin/perl-5.8.2  /apps/sybmon/dev/ADMIN_SCRIPTS/console/console_nightly.pl $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleUnixNightly.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleUnixNightly.err

33 13 * * 6              /apps/sybmon/dev/unix_batch_scripts/batch/ConsoleUnixWeekly.ksh

	/usr/local/bin/perl-5.8.2  /apps/sybmon/dev/ADMIN_SCRIPTS/console/console_weekly.pl $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleUnixWeekly.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleUnixWeekly.err

21 1 * * *               /apps/sybmon/dev/unix_batch_scripts/batch/OraSurvey.ksh
	/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/console/discover.pl -Toracle -BOraSurvey $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_OraSurvey.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_OraSurvey.err
52 2 * * *               /apps/sybmon/dev/unix_batch_scripts/batch/SybSurvey.ksh
	/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/console/discover.pl -Tsybase -BSybSurvey $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_SybSurvey.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_SybSurvey.err
53 2 * * *               /apps/sybmon/dev/unix_batch_scripts/batch/UnixSurvey.ksh
	/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/console/discover.pl -Tunix -BUnixSurvey $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_UnixSurvey.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_UnixSurvey.err

27 10 * * 6              /apps/sybmon/dev/unix_batch_scripts/batch/SybAuditor.ksh

	/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/dbi_backup_scripts/config_report.pl
	--BATCHID=SybAuditor
	--TYPE=sybase
	--OUTDIR=/apps/sybmon/dev/data/server_audits $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_SybAuditor.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_SybAuditor.err

42 3 * * *               /apps/sybmon/dev/unix_batch_scripts/batch/UnixBackupStateStaticInfo.ksh

	/usr/local/bin/perl-5.8.2  /apps/sybmon/dev/ADMIN_SCRIPTS/bin/UpdateBackupStateStaticInfo.pl
	--BATCH_ID=UnixBackupStateStaticInfo $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_UnixBackupStateStaticInfo.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_UnixBackupStateStaticInfo.err

30 4 * * 6               /apps/sybmon/dev/unix_batch_scripts/batch/ConsoleArchiver.ksh

	/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/bin/ConsoleArchiver.pl
	--BATCHID=ConsoleArchiver $*
		> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_Cons
oleArchiver.log
		2> /apps/sybmon/dev/data/GEM_BATCHJOB_LOGS/Unix_ConsoleArchiver.err


#6 * * * *                /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_AgentReport.ksh

	/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_AgentReport
	--REPORT=GemRpt_AgentReport
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_AgentReport.html $*

#24 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_BackupReport.ksh
	/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiRepor
t.pl
	--BATCHID=GemRpt_BackupReport
	--REPORT=GemRpt_BackupReport
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_BackupReport.html $*

#56 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_Backups.ksh
#55 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_BlackoutReport.ksh
#44 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_ProductionErrors.ksh
#46 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_ProductionList.ksh
#25 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_ProductionWarnings.ksh
#38 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_Sybase_ASE.ksh
#30 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_Sybase_BackupServer.ksh
#27 * * * *               /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_Sybase_RepServer.ksh
#6 * * * *                /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_Unix_Errors.ksh
#6 * * * *                /apps/sybmon/dev/unix_batch_scripts/batch/GemRpt_Unix_Warnings.ksh
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_AgentReport
	--REPORT=GemRpt_AgentReport
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_AgentReport.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_BackupReport
	--REPORT=GemRpt_BackupReport
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_BackupReport.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_Backups
	--REPORT=GemRpt_Backups
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_Backups.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_BlackoutReport
	--REPORT=GemRpt_BlackoutReport
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_BlackoutReport.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_ProductionErrors
	--REPORT=GemRpt_ProductionErrors
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_ProductionErrors.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_ProductionList
	--REPORT=GemRpt_ProductionList
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_ProductionList.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_ProductionWarnings
	--REPORT=GemRpt_ProductionWarnings
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_ProductionWarnings.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_Sybase_ASE
	--REPORT=GemRpt_Sybase_ASE
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_Sybase_ASE.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_Sybase_BackupServer
	--REPORT=GemRpt_Sybase_BackupServer
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_Sybase_BackupServer.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_Sybase_RepServer
	--REPORT=GemRpt_Sybase_RepServer
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_Sybase_RepServer.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_Unix_Errors
	--REPORT=GemRpt_Unix_Errors
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_Unix_Errors.html $*
/usr/local/bin/perl-5.8.2 /apps/sybmon/dev/ADMIN_SCRIPTS/monitoring/RunMimiReport.pl
	--BATCHID=GemRpt_Unix_Warnings
	--REPORT=GemRpt_Unix_Warnings
	--OUTFILE=/apps/sybmon/dev/data/html_output/GemRpt_Unix_Warnings.html $*

