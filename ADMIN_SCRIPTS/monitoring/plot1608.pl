#!/apps/perl/linux/perl-5.8.2/bin/perl
# a silly program to count 1608 messages from the error file
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use DBIFunc;
use MlpAlarm;
use CommonFunc;
use Getopt::Long;
use vars qw( $FILENAME );

$|=1;

sub usage {
	print @_,"\n";
 	return "Usage: $0 --FILENAME=file \n";
}

die usage() if $#ARGV<0;
die usage("Bad Parameter List $!\n") unless
    GetOptions( "FILENAME=s"	=>	\$FILENAME);

open(FILE,$FILENAME ) or die "Cant Read $FILENAME\n";
my($row);
my($curid)="";
my($curidcount)=0;
while(<FILE>) {
	$row++;
	# print "." if $row%1000 == 0;
	next unless /Error: 1608/;
	my $msg=substr($_,20,8);
	if( $msg eq $curid ) {
		$curidcount++;
	} else {
		print $curid," count=",$curidcount,"\n";
		$curid=$msg;
		$curidcount=0;
	}
	# print $msg,"\n";
}
close(FILE);

