#!/bin/sh

set -x
if [ -z "$1" -o -z "$2" ]
then
	echo "Usage $0 SERVER PASSWORD"
	exit
fi

SERVER=$1
PASSWORD=$2
if [ ! -r $SYBASE/$SYBASE_ASE/scripts/installmontables ]
then
	echo "installmontables not found in $SYBASE/$SYBASE_ASA/scripts"
	exit 0
fi

isql -Usa -S$SERVER -P$PASSWORD -iinstall1.sql  -e
# isql -Usa -S$SERVER -P$PASSWORD  -e <<EOF
#sp_configure "enable cis",1
#go
#use master
#go
#if( not exists select * from sysservers where srvname='loopback' )
	#exec sp_addserver loopback,null,@@servername
#go
isql -Usa -P$PASSWORD -S$SERVER -i $SYBASE/$SYBASE_ASE/scripts/installmontables

isql -Usa -S$SERVER -P$PASSWORD -iinstall2.sql  -e
