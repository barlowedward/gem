/* Procedure copyright(c) 1996 by Edward Barlow */

use master
go
dump tran master with no_log
go

if exists (select *
           from   sysobjects
           where  type = 'P'
           and    name = 'sp__lock')
begin
    drop procedure sp__lock
end
go

create procedure sp__lock( @dbname varchar(255)=null,@spid smallint=null,@dont_format char(1)=null )
as
begin

declare @dbnm sysname
declare @query varchar(255)

select distinct	convert (smallint, req_spid) 	As spid,
		rsc_dbid 			As dbid,
		rsc_objid 			As ObjId,
--		rsc_indid 			As IndId,
		substring (v.name, 1, 4) 	As Type,
--		substring (rsc_text, 1, 16) 	As Resource,
		substring (u.name, 1,12) 	As Mode,
		substring (x.name, 1, 5) 	As Status,
		convert(sysname,'') 		As Login,
		db_name(rsc_dbid) 		As DbName,
		convert(varchar(255),'') 	As ObjNm,
		convert(nchar(16),'')		As Cmd
	into 	#locks
	from 	master.dbo.syslockinfo,
		master.dbo.spt_values v,
		master.dbo.spt_values x,
		master.dbo.spt_values u
	where   master.dbo.syslockinfo.rsc_type = v.number
			and v.type = 'LR'
			and master.dbo.syslockinfo.req_status = x.number
			and x.type = 'LS'
			and master.dbo.syslockinfo.req_mode + 1 = u.number
			and u.type = 'L'
			and rsc_dbid != 2
			and v.name!='DB'
			and req_spid!=@@spid
order by spid

if @dbname is not null
	delete #locks where DbName!=@dbname
if @spid is not null
	delete #locks where spid!=@spid
update #locks set Login=loginame, Cmd=p.cmd
	from master..sysprocesses p
	where p.spid=#locks.spid

select distinct DbName into #dblist from #locks where ObjId !=0

declare db_curs cursor for select DbName from #dblist
open    db_curs
fetch   db_curs into @dbnm
while @@fetch_status>=0
begin
	if( has_dbaccess(@dbnm) != 1 )
		select 'UNABLE TO ACCESS DATABASE '+@dbnm
	else
	begin
		--select @query = N'use ['+@dbnm+'] insert #sysfiles select '''+@dbnm+''',* from sysfiles'
		--execute (@query)
		select @query= N'use ['+@dbnm+'] update #locks set ObjNm=object_name(ObjId) where DbName='''+@dbnm+''''
		execute (@query)
	end
	fetch   db_curs into @dbnm
end
deallocate db_curs

update #locks set ObjNm='..'+rtrim(ObjNm) where ObjNm != ''

if @dont_format is null
select distinct
Type,
Login=rtrim(Login)+' (pid='+convert(varchar,spid)+')',
[Table] = convert(varchar(30),rtrim(DbName)+ObjNm),
Cmd,
Mode= case when Mode='S' then 'Shared'
	when Mode='U' then 'Update'
	when Mode='X' then 'Exclusive'
	when Mode='IS' then 'Intent Shrd'
	when Mode='IU' then 'Intent Upd'
	when Mode='IX' then 'Intent Excl'
	when Mode='SIU' then 'ShIntntUpd'
	when Mode='SIX' then 'ShIntntEx'
	when Mode='UIX' then 'UpdIntntEx'
	when Mode='BU' then 'Bulk Op'
	when Mode='Sch-S' then 'Schema St'
	when Mode='Sch-M' then 'Schema Mod'
	else 'Other' end,
Status
from #locks
else
select distinct
Type,
Login=    rtrim(Login)+' (pid='+convert(varchar,spid)+')',
[Table] = rtrim(DbName)+ObjNm,
Cmd,
Mode= case when Mode='S' then 'Shared'
	when Mode='U' then 'Update'
	when Mode='X' then 'Exclusive'
	when Mode='IS' then 'Intent Shrd'
	when Mode='IU' then 'Intent Upd'
	when Mode='IX' then 'Intent Excl'
	when Mode='SIU' then 'ShIntntUpd'
	when Mode='SIX' then 'ShIntntEx'
	when Mode='UIX' then 'UpdIntntEx'
	when Mode='BU' then 'Bulk Op'
	when Mode='Sch-S' then 'Schema St'
	when Mode='Sch-M' then 'Schema Mod'
	else 'Other' end,
Status
from #locks


drop table #locks
drop table #dblist

        return 0
end
go

grant execute on sp__lock to public
go

use master
go
EXEC sp_MS_marksystemobject 'sp__lock'
go

