#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use MlpAlarm;
MlpLoadRepository();

__END__

=head1 NAME

INV_load_configfiles

=head1 SYNOPSIS

INV_load_configfiles.pl

=head1 DESCRIPTION

Loads the repository data into the system

- INV_policy_defn loaded from AuditMessage.dat - 4 default policies
- set INV_system_policy to default policy setting if not allready set

- Load INV_system_attribute and INV_system
  set INV_system.is_deleted and INV_system.production_state INV_system.is_monitored appropriately

-- reset INV_constant
-- reset INV_system_poss_attribute

=cut
