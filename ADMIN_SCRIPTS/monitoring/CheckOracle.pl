#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBI;
use Sys::Hostname;
use Repository;
use MlpAlarm;
use Logger;
use File::Basename;
use CommonFunc;


use vars qw( $BATCH_ID $TIME $NOSYSLOGSHOLD $PERSIST $SLEEPTIME $NOBLOCKMONITOR $USER
$PERSIST_MAIL_DELAY $MAILTO $SERVER $PRODUCTION $NOPRODUCTION $PASSWORD $OUTFILE
$opt_d $NOLOCK $QUICKMONITOR $IGNORESERVER );

my(%current_connection);
our($current_server);
my(%PERSIST_INITIALIZED);

my(%LAST_EMAIL);			# key=servername, value=time
my(%LAST_UPTIMETIME);	# key=servername, value=time
my($PERSIST_ERROR_FOUND);
my($num_errors_found);
my(%MONITOR_THIS_SERVER); 	# comes from passfile NOT_MONITORED=1 - key = servername


sub usage
{
	print @_;
	print "CheckOracle.pl -BATCH_ID=id -TIME=secs [-debug] --MAILTO=a,b,c

Servers are not monitored if NOT_MONITORED=1 in the config file

--NOLOCK	(run even if you are rerunning the program)
--SLEEPTIME represents the loop delay if you use --PERSIST
with --PERSIST, the post error report delay is --PERSIST_MAIL_DELAY secs (to prevent spam) (3 minute dflt)
You may identify -SERVER/-PASSWORD/-USER or --PRODUCTION or --NOPRODUCTION (default = all oracle servers)
--TIME is the time threshold for blocks (ie they must be older than -TIME arg) before a CRITICAL event is generated\n";
   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

#die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"SYSTEM=s"		=> \$SERVER,
		"OUTFILE=s"		=> \$OUTFILE ,
		"USER=s"			=> \$USER ,
		"PRODUCTION"	=> \$PRODUCTION ,
		"NOPRODUCTION"	=> \$NOPRODUCTION ,
		"NOLOCK"			=> \$NOLOCK ,
		"BATCH_ID=s"	=> \$BATCH_ID ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"MAILTO=s" 		=> \$MAILTO,
		"SLEEPTIME=s" 		=> \$SLEEPTIME,
		"PERSIST_MAIL_DELAY=s" 		=> \$PERSIST_MAIL_DELAY,
		"PERSIST"		=> \$PERSIST,
		"DEBUG"      	=> \$opt_d );

$PERSIST_MAIL_DELAY = 300 unless $PERSIST_MAIL_DELAY;

cd_home();
if( ! $NOLOCK ) {
	print "NOLOCK IS NOT DEFINED!\n" if $opt_d;
	if( Am_I_Up(-debug=>$opt_d,-prefix=>$BATCH_ID) ) {
		my($fname,$secs,$thresh)=GetLockfile(-prefix=>$BATCH_ID);
		print "Cant Start CheckServer.pl - it is apparently allready running\nUse --NOLOCK to ignore this, --DEBUG to see details\nFile=$fname secs=$secs thresh=$thresh secs\n";
		exit(0);
	}
} else {
	print "NOLOCK IS DEFINED!\n" if $opt_d;
}

unlink($OUTFILE) if defined $OUTFILE;

I_Am_Up(-prefix=>$BATCH_ID) unless $NOLOCK;
my $GemConfig=read_gemconfig( -debug=>$opt_d, -nodie=>1, -READONLY=>1 );
logger_init(
	-debug			=> $opt_d,
	-logfile 		=> $OUTFILE,
	-command_run 	=> $0,
	-mail_host		=>	$GemConfig->{MAIL_HOST},
	-mail_to 		=>	$MAILTO,
	-fail_subject	=>	"Blocks Exist on Server $SERVER"  );

$TIME=30 unless $TIME;

$SLEEPTIME=120 if $PERSIST and ! $SLEEPTIME;		# default 1 minute

my($Driver)="Oracle";
$Driver="ODBC" if is_nt();

if( defined $opt_d ) {
	print "CheckServer.pl Called at ".localtime(time)." in Debug Mode\n";
	print "TIME    =$TIME \n";
	print "USER    =$USER \n";
	print "MAILTO	=$MAILTO \n";
	print "SERVER	=$SERVER \n";
	print "PASSWORD=$PASSWORD \n";
	print "OUTFILE	=$OUTFILE \n";
}

my(@server_list);
if( ! $SERVER ) {
  	if( $PRODUCTION ) {
 		MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-SUBKEY=>"PRODUCTION" ) if $BATCH_ID;
 	} elsif( $NOPRODUCTION ) {
		MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-SUBKEY=>"NOPRODUCTION" ) if $BATCH_ID;
 	} else {
 		MlpBatchJobStart(-BATCH_ID=>$BATCH_ID,-SUBKEY=>"ALL" ) if $BATCH_ID;
 	}

 	if( $PRODUCTION ) {
 		push @server_list, get_password(-type=>"oracle",PRODUCTION=>1, MONITORED=>1);
 	} elsif( $NOPRODUCTION ) {
 		push @server_list, get_password(-type=>"oracle",NOPRODUCTION=>1, MONITORED=>1);
 	} else {
		push @server_list, get_password(-type=>"oracle", MONITORED=>1);
	}
} elsif( defined $SERVER ) {
	MlpBatchJobStart(-BATCH_ID=>$BATCH_ID, -SUBKEY=>$SERVER ) if $BATCH_ID;
	push @server_list, $SERVER;
}
print "Checkserver.pl servers: ".join("\n",@server_list),"\n" if $opt_d;
DBI->trace(8) if $opt_d;

sub l_oracle_error_handler {
	my(@args)=@_;
	die "Not quite sure about format of the oracle error handler - so here goes\n",join("\t",@args),"\n";
}

sub l_ms_error_handler {
	my ($state, $message, $num) = @_;
	$num_errors_found++;
	print "***************\nl_ms_error_handler():",join("|",@_),"\n***************\n" if $opt_d;
	$message =~ s/^(\[[\w\s]*\])+//;
	info( "l_ms_error_handler()\n\tError Num: $state Arg=$num\n\tError Txt: $message\n\tTarget Server=$current_server\n");

	my($msg,$title);
	if( $PERSIST and $PERSIST_INITIALIZED{$current_server} ) {
		$title="$current_server: CONNECTION ERROR\n";
		$msg="The Persistent Monitor has found an error on $current_server\n$message\nThe text of this message may not matter.";
		$PERSIST_ERROR_FOUND = 1;
	} elsif( $PERSIST ) {
		# multiple errors from the same thingie
		info( "skipped: l_ms_error_handler($message)\n" );
		return;
	} else {
		$title="$current_server: ERROR $message\n";
		$msg="CheckServer($current_server) Error: $message\n";
	}

	if( $PERSIST and ( ! $LAST_EMAIL{$current_server} or time > $LAST_EMAIL{$current_server} + $PERSIST_MAIL_DELAY )) {
		MlpHeartbeat(
				-state=>"CRITICAL",
				# -debug=>$opt_d,
				-subsystem=>"CheckServer",
				-system=>$current_server,
				-monitor_program=>$BATCH_ID,
				-message_text=>"GEM_009: ".$msg);

		info("**** CheckServer.pl Mail ****\n* TITLE=",$title,"* MESSAGE=$msg\n********************\n");
		logger_init(
				-debug			=> $opt_d,
				-logfile 		=> $OUTFILE,
				-command_run 	=> $0,
				-mail_to 		=>	$MAILTO,
				-mail_host		=>	$GemConfig->{MAIL_HOST},
				-fail_subject	=>	$title  );

		logger_send_mail($msg);
		$LAST_EMAIL{$current_server}=time;
	}

	if( $PERSIST ) {
		info( "Connection Error $message\n" );
		info( "All connection errors are considered Fatal when --PERSIST is set\n" );
		$PERSIST_INITIALIZED{$current_server} = undef;
	} else {
		info("Program Completed - Fatal Error $message\n");
		exit();
	}
	#DBIFunc::ms_error_handler(@_);
}

while(1) {
	print "MAIN LOOP CALLED AT ".localtime(time)."\n" if $PERSIST;
	$num_errors_found=0;
	I_Am_Up(-prefix=>$BATCH_ID) unless $NOLOCK;

	foreach $current_server (@server_list ) {

		# This program must run at least 1ce per hour
		MlpHeartbeat(
			-state=>"CRITICAL",
			-heartbeat=>60,
			-monitor_program=>"HEARTBEAT",
			-system=>$BATCH_ID,
			# -subsystem=>$current_server,
			-message_text=>"BATCH_ID may be stalled") if $BATCH_ID;

		info("*******************************************************************************\n");
	   info("* $BATCH_ID Checking Oracle $current_server at ".localtime(time)."\n");
	   info("*******************************************************************************\n");
	   $PERSIST_ERROR_FOUND=0;
	   if( ! $PERSIST_INITIALIZED{$current_server} or ! $current_connection{$current_server} ) {
	   	print "[$current_server] Initializing Connection \n";
		   ($USER,$PASSWORD)=get_password(-type=>"oracle", -name=>$current_server)
		   	if ! defined $USER or ! defined $PASSWORD;

		   if( ! $USER and ! $PASSWORD ) {
		   	warn "ERROR: Cant retrieve login/password for server $current_server\n";
				MlpHeartbeat(
					-state=>"CRITICAL",
					-debug=>$opt_d,
					-subsystem=>"CheckServer",
					-system=>$current_server,
					-monitor_program=>$BATCH_ID,
					-message_text=>"GEM_009: ERROR: Cant retrieve login/password for server $current_server.  Check Config Files.");
				logger_send_mail("FATAL ERROR - CANT RETRIEVE login/password for server $current_server.  Check Config Files.");
				next;
		   }

			#
			# CONNECT
			#
			$current_connection{$current_server} = DBI->connect("dbi:$Driver:$current_server", $USER, $PASSWORD);

			if( ! $current_connection{$current_server} ) {
				info("[$current_server] Unable To Connect To dbi:$Driver:$current_server as $USER... Trying Again in 20 Seconds\n");
				die "DONE";
				sleep(20);
				info("[$current_server] Connecting to $current_server as $USER\n");
				$current_connection{$current_server} = DBI->connect("dbi:$Driver:$current_server", $USER, $PASSWORD);

				# print __LINE__,' ',__FILE__," DBG DBG connection= ",$current_connection{$current_server},"\n";
				if( ! $current_connection{$current_server} ) {
					my($msg)="Cant connect to $current_server as $USER from ".hostname()." ".$DBI::errstr."\n";

					MlpHeartbeat(
						-state=>"CRITICAL",
						-debug=>$opt_d,
						-subsystem=>"CheckServer",
						-system=>$current_server,
						-monitor_program=>$BATCH_ID,
						-message_text=>"GEM_009: ".$msg) if $BATCH_ID;

					info("**** CheckServer.pl Mail ****\n* TITLE=Can Not Connect To $current_server\n* MESSAGE=FATAL ERROR - Can Not Connect To $current_server\n********************\n");
					logger_init( -debug=> $opt_d, -logfile => $OUTFILE,
						-command_run => $0, -mail_to =>$MAILTO,
						-mail_host=>$GemConfig->{MAIL_HOST},
						-fail_subject=>"Can Not Connect To $current_server"  );
					logger_send_mail("FATAL ERROR - Can Not Connect To $current_server");
					next;
				}
			} else {
				debug("[$current_server] Connected\n");
			}

			if( $PERSIST ) {
				if( ! is_nt() ) {
					info("[$current_server] Resetting Oracle Error Handler\n");
					$current_connection{$current_server}->{syb_err_handler}  = \&l_oracle_error;
				} else {
					$current_connection{$current_server}->{odbc_err_handler} = \&l_ms_error_handler;
					info("[$current_server] Resetting ODBC Handler\n");
				}
			}

			info("[$current_server] Connected \n");
			$PERSIST_INITIALIZED{$current_server}=1;

		} else {
			info("[$current_server] Using Persistent Connection To $current_server\n");
		}

		print "[$current_server] Connection Ok\n" if $opt_d;

		my $sql="select USERNAME,MACHINE,PROGRAM,OSUSER from v\$session where STATUS='ACTIVE' and PROGRAM!='ORACLE.EXE'";

		info( "[$current_server] Checking Process Table\n" );
		debug( "$sql\n" );

		my(@rc)=run_oracle_query($current_connection{$current_server}, $sql);

		foreach (@rc) {
			print "===",join(" ",@$_),"\n";
		}

	#	use Data::Dumper;
	#	print Dumper \@rc;

		if( $PERSIST_ERROR_FOUND ) { debug("PERSIST ERROR FOUND - RESTARTING LOOP\n"); next; }
		$LAST_UPTIMETIME{$current_server}=time;
#		if( $#block_data<0 ) {
#			info( "[$current_server] No Blocks Found\n") if ! $NOBLOCKMONITOR;
#			info( "[$current_server] Server Looks Ok....\n" );
#			MlpHeartbeat( -state=>"OK", -system=>$current_server,
#				-subsystem=>"CheckServer", -monitor_program=>$BATCH_ID, -message_text=>"GEM_009: Server Has No Blocked Processes...");
#			MlpBatchJobEnd() unless $PERSIST;
#			dbi_disconnect(-connection=>$current_connection{$current_server}) unless $PERSIST;
#			next;
#		}

		if( $PERSIST_ERROR_FOUND ) { debug("PERSIST ERROR FOUND - RESTARTING LOOP\n"); next; }

#		if( $#str>=0 ) {
#			push @str, "RESULTS OF sp__lock \@dont_format='Y'. ";
#			debug( "running: exec sp__lock\n" );
#			my(@x)=dbi_query(-db=>"master",-query=>"exec sp__lock \@dont_format='Y'", -print_hdr=>1, -connection=>$current_connection{$current_server});
#			if( $PERSIST_ERROR_FOUND ) { debug("PERSIST ERROR FOUND - RESTARTING LOOP\n"); next; }
#			push @str,dbi_reformat_results(@x);
#
#			my($header_str);
#			$header_str.="$num_suspend Processes Suspended\n" 	if $num_suspend>0;
#			$header_str.="$num_infected Processes Infected\n" 	if $num_infected>0;
#			$header_str.="$num_stopped Processes Stopped\n" 	if $num_stopped>0;
#			$header_str.="$num_badstatus Processes Stopped\n" 	if $num_badstatus>0;
#
#			if( $num_blocked>0 ) {
#				$header_str.="BLOCKS FOUND: $current_server\n";
#				$header_str.="Block Time: ".to_timestr($longest_block)."\n" if $longest_block;
#				foreach ( keys %blocked_count ) {
#					$header_str.="Spid $_ Blocking $blocked_count{$_} jobs\n";
#				}
#			}
#
#			my($body_str)="============================\n";
#			my($found)="FALSE";
#			foreach ( @str ) {
#				chomp; chomp;
#				if( /^Blocker spid=/ ) {
#					s/^Blocker spid=/Spid=/;
#					$header_str.=$_."\n";
#					next;
#				}
#				$body_str.=$_."\n";
#			 }
#
#			$header_str="CODING ERROR - LOCKS APPEAR GONE???\n".join("\n",@str) unless $header_str;
#
#			info( "[$current_server] Writing ERROR to Alarm Database: text=$header_str" );
#			info( "[$current_server] MlpHeartbeat( -state=>ERROR, -debug=>$opt_d, -system=>$current_server, -subsystem=>Blocks,	-monitor_program=>$BATCH_ID, -message_text=>GEM_009: $header_str) ");
#			MlpHeartbeat( -state=>"ERROR", -debug=>$opt_d, -system=>$current_server, -subsystem=>"CheckServer",
#		 		 -monitor_program=>$BATCH_ID, -message_text=>"GEM-009: ".$header_str);
#			if( defined $OUTFILE ) {
#				open(OUT,">> $OUTFILE") or logdie("Cant Write to $OUTFILE: $!");
#				print OUT $header_str."\n".$body_str;
#				close OUT;
#			}
#			bad_email($header_str."\n".$body_str);
#		} else {
#			debug("[$current_server] Process Finished With No Errors...\n");
#			MlpHeartbeat( -state=>"OK", -debug=>$opt_d, -system=>$current_server, -subsystem=>"CheckServer", -monitor_program=>$BATCH_ID, -message_text=>"GEM-009: Server Has No Blocked Processes...");
#		}
		$current_connection{$current_server}->disconnect unless $PERSIST;
	}
	last unless $PERSIST;

	info( "*******************************************************************************\n");
   info( "* SLEEPING $SLEEPTIME SECONDS as --PERSIST FLAG WAS SET\n" );
	info( "*******************************************************************************\n");
   sleep($SLEEPTIME);
}

MlpBatchJobEnd();

info( "Program completed successfully.\n" );
$current_connection{$current_server}->disconnect if $current_connection{$current_server};
exit(0);

sub debug {
	print @_ if defined $opt_d;
}

sub to_timestr
{
	my($secs)=@_;
	my $hrs= int($secs/3600);
	$secs -= 3600*$hrs;
	$hrs .= " hour " if $hrs==1;
	$hrs .= " hours " if $hrs>1;
	$hrs = "" if $hrs < 1;

	my $min= int($secs/60);
	$secs -= 60*$min;

	my($outstr)="$hrs $min Minutes $secs Secs";
	$outstr=~s/\s+/ /g;
	return $outstr;
}

sub run_oracle_query {
	my($dbh,$opt_q)=@_;
	my $sth;
	#my $dat;

	unless ($sth = $dbh->prepare ($opt_q)) {
		print("SQL command $opt_q prepare failed ");
		$dbh->disconnect;
		exit;
	}

	# excute the query
	unless ( $sth->execute ) {
		print("SQL command $opt_q execute failed ");
		$dbh->disconnect;
		exit;
	}

	my(@rc);
	while ( my($aref) = $sth->fetchrow_arrayref() ) {
		last if ! defined $aref or $aref==0 or $#$aref<0 ;
		print "RC=>",join(" ",@{$aref}),"\n" if $opt_d;
		my(@d);
		foreach( @$aref) { push @d,$_; }
		push @rc, \@d;
	}

	$sth ->finish;
#	$dbh->disconnect;
	return @rc;
}


sub is_nt {
	return 1 if $^O =~ /MSWin32/;
	return 0;
}
