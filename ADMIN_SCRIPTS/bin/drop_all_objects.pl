#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
        print @_;
        print "USAGE: $0 -UUSER -SSERVER -PPASS -DDB -B\n
-B Batch - no question";
        return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_d $opt_U $opt_S $opt_P $opt_D $DEBUG $opt_B);
die usage("Bad Parameter List\n") unless getopts('dU:D:S:P:B');

use File::Basename;
use Sybase::SybFunc;

die usage("Must Pass DB\n")                                     unless defined $opt_D;
die usage("Must pass sa password\n" )   unless defined $opt_P;
die usage("Must pass server\n")                         unless defined $opt_S;
die usage("Must pass sa username\n" )   unless defined $opt_U;
$DEBUG=1 if defined $opt_d;

# Ignore cant drop due to ri messages because we will re-drop tables
db_msg_exclude(3712);
db_set_web_page(0);
db_connect($opt_S,$opt_U,$opt_P)
        or die "Cant connect to $opt_S as $opt_U\n";

# GET DB INFO
my(@databaselist)=db_query("master","select name from sysdatabases where name like \"$opt_D\"  and status & 0x1120 = 0 and status2&0x0030=0");

die "No databases found\n" unless $#databaselist>=0;
my($db);
my(%ANSWER);
foreach $db (@databaselist) {
        next if $db eq "master"
                or $db eq "model"
                or $db eq "sybsystemprocs"
                or $db eq "sybsecurity"
                or $db eq "tempdb";

        print "Database $db\n";
        foreach (db_query($db,"select type,count(*) from sysobjects group by type")){
                my($typ,$cnt)=db_decode_row($_);
                $typ=~s/\s//;
                if( $typ eq 'P' ) {
                        printf( "\t%10d Procedures\n",$cnt);
                } elsif( $typ eq 'U' ) {
                        printf( "\t%10d Tables\n",$cnt);
                } elsif( $typ eq 'TR' ) {
                        printf( "\t%10d Triggers\n",$cnt);
                } elsif( $typ eq 'D' ) {
                        printf( "\t%10d Defaults\n",$cnt);
                } elsif( $typ eq 'R' ) {
                        printf( "\t%10d Rules\n",$cnt);
                } elsif( $typ eq 'V' ) {
                        printf( "\t%10d Views\n",$cnt);
                } elsif( $typ eq 'S' ) {
                        next;
                } else {
                        printf( "\t%10d %s\n",$cnt,$typ);
                }
        }

        if( ! defined $opt_B )  {
                print "Confirm Deletion of Server $opt_S Database $db (y or n):";
                my($ans);
                $ans=<STDIN>;
                chop $ans;
                $ANSWER{$db}=$ans;
                next unless $ans eq "Y" or $ans eq "y";
        }

        print "Dropping Procedures\n";
        db_eachobject($db,"drop procedure","P",$opt_d,0);
        print "Dropping Triggers\n";
        db_eachobject($db,"drop trigger","TR",$opt_d,0);
        print "Dropping Tables\n";
        db_eachobject($db,"drop table","U",$opt_d,0);
        print "Dropping Views\n";
        db_eachobject($db,"drop view","V",$opt_d,0);

        # drop tables again to handle ri blowups
        print "Dropping Tables\n";
        db_eachobject($db,"drop table","U",$opt_d,0);

        print "Dropping Types\n";

        # Drop Types
        my(@types)=db_query($db,"select name from systypes where name not in (select name from model..systypes)");
        foreach (@types) {
                db_query($db,"exec sp_droptype $_",0);
        }

        db_eachobject($db,"drop rule","R",$opt_d,0);
        db_eachobject($db,"drop default","D",$opt_d,0);
}

foreach $db (@databaselist) {
        if( defined $opt_B or $ANSWER{$db} eq "Y" or $ANSWER{$db} eq "y" ) {
                foreach (db_query($db,"select db_name(),type,name from sysobjects where type!=\"S\"")) {
                        my($d,$t,$n)=db_decode_row($_);
                        print "REMAINING OBJECT $n of type $t in $d \n";
                }
        } else {
                print "Not Deleting $db Because You answered ",$ANSWER{$db}," To Interactive Query\n";
        }
}

db_close_sybase();

__END__

=head1 NAME

drop_all_objects.pl - drop all objects in the database

=head2 DESCRIPTION

drop all objects from a database

=head2 USAGE


        USAGE: ./drop_all_objects.pl -UUSER -SSERVER -PPASS -DDB

=head2 NOTES

does not handle failure well.  RI issues?

=cut

