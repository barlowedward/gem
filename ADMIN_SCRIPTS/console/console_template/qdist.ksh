
#
# This is an admin script used to copy the .pl programs to my web server
#
if [ -n "$1" ]
then
cp  $* //adsrv115/c$/cgi-bin-dev
exit
fi

cp *.pl //adsrv115/c$/cgi-bin-dev/console_mysql
cp *.pl //adsrv115/c$/cgi-bin-dev/console_sqlsvr
cp *.pl //adsrv115/c$/cgi-bin-dev/console_sybase
cp *.pl //adsrv115/c$/cgi-bin-dev/console_oracle
cp *.pl //adsrv115/c$/cgi-bin-dev/console_unix
cp *.pl //adsrv115/c$/cgi-bin-dev/console_win
