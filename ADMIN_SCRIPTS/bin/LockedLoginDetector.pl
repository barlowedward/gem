#!/apps/perl/linux/perl-5.8.2/bin/perl

# Copyright (c) 2009 by Edward Barlow. All rights reserved.

use strict;
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use Carp qw/confess/;
use Getopt::Long;
use RepositoryRun;
use File::Basename;
use DBIFunc;
use MlpAlarm;

my $VERSION = 1.0;
my($PROGRAM_NAME)=basename($0);

use vars qw( $DOALL $BATCH_ID $USER $SERVER $PRODUCTION $DATABASE $NOSTDOUT $NOPRODUCTION $PASSWORD $OUTFILE $DEBUG $NOEXEC);

sub usage
{
	print @_;

print "$PROGRAM_NAME Version $VERSION\n\n";
print @_;
print "SYNTAX:\n";
print $PROGRAM_NAME." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
-or- \n".
basename($0)." -DOALL=sybase
-or- \n".
basename($0)." -DOALL=sqlsvr

Additional Arguments
   --BATCH_ID=id     [ if set - will log via alerts system ]
   --PRODUCTION|--NOPRODUCTION
   --DEBUG
   --NOSTDOUT			[ no console messages ]
   --OUTFILE=Outfile
   --NOEXEC\n";
  return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"			=> \$SERVER,
		"OUTFILE=s"			=> \$OUTFILE ,
		"USER=s"				=> \$USER ,
		"DATABASE=s"		=> \$DATABASE ,
		"PRODUCTION"		=> \$PRODUCTION ,
		"NOPRODUCTION"		=> \$NOPRODUCTION ,
		"NOSTDOUT"			=> \$NOSTDOUT ,
		"NOEXEC"				=> \$NOEXEC ,
		"DOALL=s"			=> \$DOALL ,
		"BATCH_ID=s"		=> \$BATCH_ID ,
		"PASSWORD=s" 		=> \$PASSWORD ,
		"DEBUG"      		=> \$DEBUG );

die "HMMM WHY ARE U RUNNING --NOEXEC without --DEBUG... what are you trying to proove?" if $NOEXEC and ! $DEBUG;

my($STARTTIME)=time;
repository_parse_and_run(
	SERVER			=> $SERVER,
	PROGRAM_NAME	=> $PROGRAM_NAME,
	OUTFILE			=> $OUTFILE ,
	USER				=> $USER ,
	AGENT_TYPE		=> 'Gem Monitor',
	PRODUCTION		=> $PRODUCTION ,
	NOPRODUCTION	=> $NOPRODUCTION ,
	NOSTDOUT			=> $NOSTDOUT ,
	DOALL				=> $DOALL ,
	BATCH_ID			=> $BATCH_ID ,
	PASSWORD 		=> $PASSWORD ,
	DEBUG      		=> $DEBUG,
	init 				=> \&init,
	server_command => \&server_command
);

exit(0);

###############################
# USER DEFINED init() FUNCTION!
###############################
sub init {
	#warn "INIT!\n";
}

###############################
# USER DEFINED server_command() FUNCTION!
###############################
sub server_command {
	my($cursvr)=@_;
	sub myformat {
		my(@x)=@_;
		my($str)=join("||",@x);
		$str=~ s/\s*\|\|\s*/\|\|/g;
		$str =~ s/\s+$//;
		return $str;
	}

	#
	# Get Database Type & Level
	#
	my($DBTYPE,$DBVERSION,$PATCHLEVEL)=dbi_db_version();
	if( $DBTYPE eq "SQL SERVER" ) {
		#
		# locked/expired irrelevant on sql server
		#
		warn "Cant Look For Locked Logins On SQL Server";
		return;
	} else {
		#
		# lock info saved in credential attribute
		# compare and handle
		#
		my($typ)="SYBASE";					# $typ=SYBASE or SQLSERVER

		# get current locked/expired status from the repository
		my($q)="select credential_name, attribute_key, attribute_value
					from INV_credential_attribute
					where credential_type='sybase_login'
					and system_name=".dbi_quote(-text=>$cursvr)." and system_type='SYBASE'
					and attribute_key like 'tmp_%'";
		my(%lockedlogins,%expiredlogins);
		my(@lastresults)=MlpAlarm::_querywithresults($q,1);
		foreach (@lastresults) {
				my(@vals) = dbi_decode_row($_);
				$lockedlogins{$vals[0]} = $vals[2] if $vals[1] eq "tmp_is_locked";
				$expiredlogins{$vals[0]}= $vals[2] if $vals[1] eq "tmp_is_expired";
		}

		# foreach ( keys %lockedlogins ) { print "DBGDBG~".$_."~".$lockedlogins{$_}."~".$expiredlogins{$_}."~\n"; }

		# get current locked/expired status from the target database
		my($q)="exec sp__helplogin \@dont_format='Y'";
		print "--> $q\n" if $DEBUG;
		foreach( dbi_query(-db=>"master",-query=>$q)){
			my(@vals) = dbi_decode_row($_);

			my($base)=			dbi_quote(-text=>$vals[1]).",".			# login
									"'sybase_login',".
									dbi_quote(-text=>$vals[0]).",".			# sid
									dbi_quote(-text=>$cursvr)	.",".			# system
									dbi_quote(-text=>$typ);						# systype

			my($col)=4;
			$vals[$col] =~ s/\s//g;
			$vals[$col] = "N" if $vals[$col] =~ /^\s*$/;
			$q=undef;

			if( ! defined $lockedlogins{$vals[1]} ) {

				print "INITIALIZING LOCK  FLAG FOR $vals[1]\n";
				$q="insert INV_credential_attribute values ( $base , 'tmp_is_locked',". dbi_quote(-text=>$vals[$col]).")";
#				if( $vals[$col] =~ /Y/ ) {
#					MlpHeartbeat(
#						-state		=>"ERROR",
#						-subsystem	=>"is_locked(".$vals[1].")",
#						-system		=>$cursvr,
#						-monitor_program=>$BATCH_ID,
#						-message_text=>"Login $vals[1] is Locked") if $BATCH_ID;
#				}


			} elsif( $lockedlogins{$vals[1]} ne $vals[$col] ) {

				my($msg)= "LOCK FLAG HAS CHANGED: was ".$lockedlogins{$vals[1]}." now is ".$vals[$col]."\n";

				print $msg;

#and	credential_id	=".dbi_quote(-text=>$vals[0])."
				# its defined so its an update
				$q="update INV_credential_attribute
set 	attribute_value=".dbi_quote(-text=>$vals[$col])."
where	credential_name=".dbi_quote(-text=>$vals[1])."
and	credential_type='sybase_login'
and	system_name		=".dbi_quote(-text=>$cursvr)."
and 	system_type		=".dbi_quote(-text=>$typ)."
and 	attribute_key	='tmp_is_locked'";

			$msg="Login $vals[1] is LOCKED" if $vals[$col] =~ /Y/;
			my($state)='OK';
			$state='ERROR' if $vals[$col] =~ /Y/;
			MlpHeartbeat(
				-state		=>$state,
				-subsystem	=>"is_locked(".$vals[1].")",
				-system		=>$cursvr,
				-monitor_program=>$BATCH_ID,
				-message_text=>$msg) if $BATCH_ID;

			} else {
				print "OK - $vals[1] IS_LOCKED = $lockedlogins{$vals[1]} \n";
			}
			if( defined $q ) {
				print "(noexec)" if $NOEXEC and $DEBUG;
				print $q,"\n" if $DEBUG;
				MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
			}

			my($col)=5;
			$vals[$col] =~ s/\s//g;
			$vals[$col] = "N" if $vals[$col] =~ /^\s*$/;
			$q=undef;
			if( ! defined $expiredlogins{$vals[1]} ) {

				print "INITIALIZING EXPIRED FLAG FOR $vals[1]\n";

				$q="insert INV_credential_attribute values ( $base , 'tmp_is_expired',". dbi_quote(-text=>$vals[$col]).")";

#				if( $vals[$col] =~ /Y/ ) {
#					MlpHeartbeat(
#						-state		=>"ERROR",
#						-subsystem	=>"is_expired(".$vals[1].")",
#						-system		=>$cursvr,
#						-monitor_program=>$BATCH_ID,
#						-message_text=>"Login $vals[1] is Expired") if $BATCH_ID;
#				}
			} elsif( $expiredlogins{$vals[1]} ne $vals[$col] ) {

				my($msg)="EXPIRED FLAG HAS CHANGED: was $expiredlogins{$vals[1]} now is $vals[$col]\n";
				print $msg;

#and	credential_id	=".dbi_quote(-text=>$vals[0])."

				# its defined so its an update
				$q="update INV_credential_attribute
set 	attribute_value=".dbi_quote(-text=>$vals[$col])."
where	credential_name=".dbi_quote(-text=>$vals[1])."
and	credential_type='sybase_login'
and	system_name		=".dbi_quote(-text=>$cursvr)."
and 	system_type		=".dbi_quote(-text=>$typ)."
and 	attribute_key	='tmp_is_expired'";

				$msg="Login $vals[1] is EXPIRED" if $vals[$col] =~ /Y/;
				my($state)='OK';
				$state='ERROR' if $vals[$col] =~ /Y/;
				MlpHeartbeat(
					-state		=>$state,
					-subsystem	=>"is_expired(".$vals[1].")",
					-system		=>$cursvr,
					-monitor_program=>$BATCH_ID,
					-message_text=>$msg) if $BATCH_ID;

			} else {
				print "OK - $vals[1] IS_EXPIRE = $expiredlogins{$vals[1]} \n";
			}

			if( defined $q ) {
				print "(noexec)" if $NOEXEC and $DEBUG;
				print $q,"\n" if $DEBUG;
				MlpAlarm::_querynoresults($q,1) unless $NOEXEC;
			}
			print "\n" if $DEBUG;
		}
		print "\n" if $DEBUG;
	}
}


__END__

=head1 NAME

LockedLoginDetector.pl

=head1 SYNOPSIS

Detects locked logins and alerts appropriately
