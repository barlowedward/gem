#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# generic cd to the right directory
use File::Basename;
my($curdir)=dirname($0);

# Analyzes Audit Of Command Text
use strict;
use Sybase::DBlib;
use integer;

sub usage
{
   print @_;
   print "$0 [-d] -USA_USER -SSERVER -PSA_PASS

      optional parms
         -t              optional table name (default=sysaudits)
         -h              htmlize output
         -p              parse/summarize the sql by removing quotes etc
         -DDB            database name to use in selectivity criterion
         -lLOGINNAME     login name to use in selectivity criterion
         -sSTARTTIME     start time to use in selectivity criterion
         -eENDTIME       end time to use in selectivity criterion
         -d              DEBUG MODE";

   return "\n";
}

use strict;
use Getopt::Std;

use vars qw($opt_t $opt_U $opt_S $opt_P $opt_d $opt_l $opt_s $opt_e $opt_D $opt_h $opt_p $opt_o);
die usage("Bad Parameter List") unless getopts('hU:S:P:D:l:s:e:dqopt:');

$opt_t="sysaudits" unless defined $opt_t;
my($basetime)=time;

# IF ONLY HAVE opt_S get login & password from password file
if( defined $opt_S and ! defined $opt_U ) {
        use Repository;
   ($opt_U,$opt_P)=get_password(-type=>"sybase",-name=>$opt_S);
}

die usage("Must pass sso password\n" ) unless defined $opt_P;
die usage("Must pass server\n" )                unless defined $opt_S;
die usage("Must pass sso username\n" ) unless defined $opt_U;

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

&dbmsghandle ("message_handler_mine");
&dberrhandle ("error_handler_mine");
my($dbproc);
($dbproc = new Sybase::DBlib $opt_U, $opt_P, $opt_S);
if( ! defined $dbproc ) {
      die("Can't connect to the $opt_S Sybase server as login $opt_U.\n")
}

my($cmd)="select spid,sequence,suid,convert(char(30),eventtime,9),extrainfo from $opt_t where event=107";
$cmd.=" and dbname=$opt_D" if defined $opt_D;
$cmd.=" and loginname=\"$opt_l\"" if defined $opt_l;
$cmd.=" and eventtime > \"$opt_s\"" if defined $opt_s;
$cmd.=" and eventtime < \"$opt_e\"" if defined $opt_e;

print "Analysis of Table $opt_t on Server $opt_S $NL $NL";
print "<em>" if defined $opt_h;
print "Retrieval Command=$cmd $NL";
print "</em>" if defined $opt_h;

print "Starting Retrieval At Time=",time-$basetime," Seconds$NL";

my($rc)=$dbproc->dbuse("sybsecurity");
die "Cant Use sybsecurity" if $rc==0;

$dbproc->dbcmd($cmd);
$dbproc->dbsucceed(1);

print "Completed Retrieval At Time=",time-$basetime," Seconds$NL";

my($suid,$spid,$time,$seq,$txt);                                        # the columns selected
my(%vals);                      # our output
my($count)=0;                                                                   # The row being processed
# maxnorm is ten minutes - no queries should exceed

print "Each Period Represents 100 Rows Processed $NL" if defined $opt_d;

# fill in our associative arrays
my(%DATA);
while (($spid,$seq,$suid,$time,$txt) = $dbproc->dbnextrow()) {
        $count++;
        print "."        if $count%200==0 and defined $opt_d;
        print "$NL"      if $count%14000==0 and defined $opt_d;

        if( $seq==1 ) {
                $DATA{$spid.$suid.$time} = $txt;
        } else {
                $DATA{$spid.$suid.$time} .= $txt
                        if defined $DATA{$spid.$suid.$time};
        }
}

foreach ( values %DATA ) {

        if( defined $opt_p ) {

        # justify string
        s/^\s+//;
        s/\s+$//;

        # multiple spaces down to one space
        s/\s+/ /g;

        # remove everything after first word
        if( /^exec /i or /^execute /i ) {
                s/exec[ute]*\s*//;

                # remove to end of line if stored proc
                s/\s.+//;
        }

        # remove stuff in quotes
        s/["`'][\w\s]+["`']/''/g;

        # parse down " = "
        s/\s*=\s*/=/g;

        # remove integer parms
        s/=\d+/=\?/g;

        }

        # make insert statemnts look the same
        s/values\s*\(.*\)//;

        # now make string only 100 characters long
        # substr($_,100)="" if defined $opt_p and length($_)>100;

        if( defined $vals{$_} ) {
                $vals{$_}++;
        } else {
                $vals{$_}=1;
        }
}
$dbproc->dbclose();

print "$NL";
print "Done Parsing $count rows At Time=",time-$basetime," Seconds",$NL;
print $NL,"All times are In MilliSeconds.  Warning:  Times are extracted from the audit logs, which only keep start times.  This means that these times may be REALLY off depending on other processing on the system.  Use them if you think they might help, but keep this in mind.  Frequency numbers are useful.$NL";

print "<TABLE>\n" if defined $opt_h;
print "<TR><TH>Frequency</TH><TH ALIGN=LEFT>Parsed Query</TH></TH>\n" if defined $opt_h;

if( ! defined $opt_o ) {
        foreach (sort keys %vals) {
                print_row( $vals{$_},$_ );
        }
}

print "</TABLE>\n" if defined $opt_h;

print "Program Done At Time=",time-$basetime," Seconds$NL";

# take array, print as a tbl row or not
sub print_row
{
        if( defined $opt_h ) {
                print "<TR>";
                foreach (@_) {
                        print "<TD>$_</TD>";
                }
                print "</TR>\n";
        } else {
                foreach (@_) {
                        print $_,"\t";
                }
                print "\n";
        }
}

=head1 NAME

audit_fetch.pl - analyze your sysaudits information

=head2 DESCRIPTION

Parses through sysaudits and gives you pretty output.  This relieves the signifcant pain in the rear end to look at audit info.  Since sybase auditing is brain dead in its timings (who decided to not audit end times of batches), the summary timings that this program calculates are really nearly useless - they are based on the difference between the statement and the time the next statement was run.

Of use however, is the parser, which does things like parse down strings (in quotes) to nothing and remove parameters from stored proc text, which makes your output times summarized better.

=head2 USAGE

        ./audit_fetch.pl [-d] -USA_USER -SSERVER -PSA_PASS

        optional parms
        -t              optional table name (default=sysaudits)
        -h              htmlize output
        -p              parse/summarize the sql by removing quotes etc
        -DDB            database name to use in selectivity criterion
        -lLOGINNAME     login name to use in selectivity criterion
        -sSTARTTIME     start time to use in selectivity criterion
        -eENDTIME       end time to use in selectivity criterion
        -d              DEBUG MODE

        sort options  (default is to sort text)
        -o n  num hits, b  besttime, w  worsttime, a  avg time

=head2 NOTES

html output is the preferred method of viewing the logs as tables are perfect for this kind of thing.

=cut

