#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use File::Basename;
use Sys::Hostname;
use Net::myFTP;
use strict;
use Repository;
use CommonFunc;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

my $curhostname = hostname();
my(@hosts)=get_password(-type=>"unix");
foreach my $host (@hosts) {

   my($login,$password)=get_password(-type=>"unix",-name=>$host);
   my(%args)=get_password_info(-type=>"unix",-name=>$host);
	next if $host eq $curhostname;

	my($method);
	if( is_nt() ) {
		$method=$args{WIN32_COMM_METHOD} if is_nt();
	} else {
		$method=$args{UNIX_COMM_METHOD};
	}
	die "COMM_METHOD NOT DEFINED for SERVER $host\n" unless $method;

	if( $method eq "NONE" ) {	# hmm cant talk
		print "host $host: METHOD=NONE - skipping\n";
		next;
	}

	print $host," ",$args{SERVER_TYPE},"\n";
	if( $args{SERVER_TYPE} ne "PRODUCTION"
	and $args{SERVER_TYPE} ne "CRITICAL"
	and $args{SERVER_TYPE} ne "DEVELOPMENT"
	and $args{SERVER_TYPE} ne "QA" ) {
		print "ERROR:  SERVER TYPE ($args{SERVER_TYPE} MUST BE PRODUCTON or DEVELOPMENT or QA\n";
		next;
	}

	my(@sybase_dirs)=split(/\s/,$args{SYBASE});
	my(@more_sybase_dirs)=split(/\s/,$args{SYBASE_CLIENT})
		if defined $args{SYBASE_CLIENT} and $args{SYBASE_CLIENT} !~ /^\s*$/;
	push @sybase_dirs,@more_sybase_dirs;

   my($ftp) = Net::myFTP->new($host,METHOD=>$method,Debug=>undef)
         or die "Failed To Create FTP Object $@";
   if( ! $ftp )  {
      warn "Failed To Connect To $host\n";
      next;
   }

   my($rc,$err)=	$ftp->login($login,$password);
	if( ! $rc )  {
      warn "Failed To Connect To $host : $err\n";
      next;
   }

   $ftp->ascii();

	foreach my $file_dir (@sybase_dirs) {
		next if $file_dir =~ /^\s*$/;
   	if( ! $ftp->cwd($file_dir) ) {
      	warn("WARNING: Cant Chdir To $file_dir on machine $host");
      	next;
   	}
   	my(@files)=$ftp->ls();
		my $found = "NO";

		foreach (@files) { if( $_ eq "interfaces" ) { $found="OK"; last; } }
		printf "... %40.40s %s ",$file_dir,$found;

		print "Ignored (no interfaces file found)...\n" unless $found eq "OK";

		next unless $found eq "OK";

		my($file);
		if( $args{SERVER_TYPE} eq "PRODUCTION" or $args{SERVER_TYPE} eq "CRITICAL"  ) {
			$file = "interfaces_production";
		} elsif( $args{SERVER_TYPE} eq "DEVELOPMENT" ) {
			$file = "interfaces_development";
		} elsif( $args{SERVER_TYPE} eq "QA" ) {
			$file = "interfaces_qa";
		} else {
			die "ERROR:  SERVER TYPE ($args{SERVER_TYPE} MUST BE PRODUCTON or DEVELOPMENT or QA\n";
		}

   	next unless -e "$file";
		$ftp->rename("interfaces","interfaces.".today());
   	if( ! $ftp->put($file,"interfaces") ) {
      	warn("WARNING: Couldnt Copy File interfaces file to server: $!\n");
   	}
		$ftp->quot("chmod 664 interfaces")
			or warn "Cant chmod 664 interfaces : $\n";
		print "Copied $file...\n";
	}
}

my($today_date)="";
sub today
{
	return $today_date if $today_date ne "";
   my(@t)=localtime(time);
   $t[4]++;
   $t[5]+=1900;
   substr($t[3],0,0)="0" if length($t[3])==1;
   substr($t[4],0,0)="0" if length($t[4])==1;
   substr($t[5],0,0)="0" if length($t[5])==1;
	substr($t[2],0,0)="0" if length($t[2])==1;
	substr($t[1],0,0)="0" if length($t[1])==1;
	substr($t[0],0,0)="0" if length($t[0])==1;
   $today_date= $t[5].$t[4].$t[3]."_".$t[2].$t[1].$t[0];
   return $today_date;
}
