#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use MlpAlarm;
use Getopt::Long;
use Repository;
use DBIFunc;

use vars qw( $BATCHID $DAYS $DEBUG $SYSTEM);

$|=1;
my($VERSION)="1.0";

sub usage {
	print @_,"\n";
	return "Usage: mssql_cycle_eventlog.pl --DAYS=days --SYSTEM=system --DEBUG --BATCHID";
}

sub error_out {
   my($msg)=join("",@_);
   MlpBatchJobEnd(-EXIT_CODE=>"ERROR", -EXIT_NOTES=>$msg) if $BATCHID;

   MlpEvent(
      -monitor_program=> $BATCHID,
      -system=> $BATCHID,
      -message_text=> $msg,
      -severity => "ERROR"
   ) if $BATCHID;
   die $msg;
}

die usage("Bad Parameter List $!\n") unless
	GetOptions(	"DAYS=s"=>\$DAYS,
			"SYSTEM"=>\$SYSTEM,
			"BATCHID=s"=>\$BATCHID,
			"DEBUG"=>\$DEBUG 	 );

die usage("Error: must define days\n") unless $DAYS;

MlpBatchJobStart(-BATCH_ID=>$BATCHID,-AGENT_TYPE=>'Gem Monitor') if $BATCHID;
print "Program $BATCHID started at ".localtime(time)."\n";

my(@servers);
if( ! defined $SYSTEM or $SYSTEM eq "ALL" ) {
	@servers=get_password(-type=>'sqlsvr');
} else {
	@servers=split(/,/,$SYSTEM);
}

print "\nServer       DATE             SIZE                    DAYS\n";
my($login,$pass,$ctype,$srv,$cmd);
foreach $srv ( sort @servers) {
	($login,$pass,$ctype)=get_password(-type=>'sqlsvr',-name=>$srv);
   	print "Connecting to $srv\n" if $DEBUG;
   	my($rcx)=dbi_connect(-srv=>$srv,-login=>$login,-password=>$pass,-type=>'ODBC');
	if( ! $rcx ) {
      		print "	$srv: Cant connect to $srv as $login\n";
		next;
	}
	my(@rc)=dbi_query(-db=>"master",-query=>"select name from master..sysobjects where name='sp_enumerrorlogs'", -debug=>$DEBUG);
	foreach ( @rc ) {
		my($nm)=dbi_decode_row($_);
		if( $nm ne "sp_enumerrorlogs" ) {
			error_out("Server $srv does not have sp_enumerrorlogs procedure in master\n");
		}
	}

	# its the date of the second row that matters
	my(@rc)=dbi_query(-db=>"master",-query=>"exec sp_enumerrorlogs", -debug=>$DEBUG);
	my($num,$date,$filesize);
	my($rowid)=0;
	foreach ( @rc ) {
		my(@x)=dbi_decode_row($_);
		if( $rowid == 0 ) {
			$num		= $x[0];
			if( $num !~ /^\s*0\s*$/ ) {
				warn("$srv: Warning for some reason - sp_enumerrorlogs did not return id=0 as the first row\nIt returned: ".join(" ",@x));
				last;
			}

			$filesize	= $x[2];
			$rowid++;
		} elsif( $rowid == 1 ) {
			$date	= $x[1];
			last;
		}
	}
	if( $rowid==0 ) {
		foreach ( @rc ) {
			my(@x)=dbi_decode_row($_);
			print join(" ",@x),"\n";
		}
		print "executing sp__id\n";
		@rc = dbi_query(-db=>"master",-query=>"exec sp__id", -debug=>$DEBUG);
		foreach ( @rc ) {
			my(@x)=dbi_decode_row($_);
			print join(" ",@x),"\n";
		}
		next;
	}

	my(@rc)=dbi_query(-db=>"master",-query=>"select datediff(dd,'".$date."',getdate())", -debug=>$DEBUG);
	my($num_days);
	foreach ( @rc ) {
		($num_days)=dbi_decode_row($_);
	}

	printf( "%-12s DATE=$date SIZE=%-12d DAYS=%d\n",$srv,$filesize,$num_days );

	if( $num_days>$DAYS ) {	# purge
		dbi_msg_exclude(24000);
		dbi_msg_ok('01000');
		print "*** Executing sp_cycle_errorlog on $srv\n";
		my(@rc)=dbi_query(-db=>"master",-query=>"exec sp_cycle_errorlog", -debug=>$DEBUG);
		foreach ( @rc ) {
			my($str)=join(" ",dbi_decode_row($_));
			next if $str=~/^\s*$/;
			print "$srv: WARNING: ",$str,"\n";
		}
	}
};
print "\nCompleted processing at ".localtime(time)."\n";
MlpBatchJobEnd() if $BATCHID;
exit(0);

__END__

=head1 NAME

mssql_cycle_eventlog.pl - Cycle SQL Server Error Log Files

=head2 USAGE

	Usage: mssql_cycle_eventlog.pl --DAYS=days --SYSTEM=system --DEBUG --BATCHID

=cut
