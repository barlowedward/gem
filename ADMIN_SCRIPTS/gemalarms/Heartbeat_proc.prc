create procedure Heartbeat_proc (
	@monitor_program 		varchar(30)	=NULL,
	@system          		varchar(30)	=NULL,
	@subsystem       		varchar(50)	=NULL,
	@state 		 			varchar(30)	=NULL,
	@message_text 	 		varchar(255)=NULL,
	@batchjob 	 			varchar(20)	=NULL,
	@document_url 	 		varchar(255)=NULL,
	@heartbeat_time		datetime		=NULL,

	@reviewed_by			varchar(255)=  null,
	@reviewed_time			datetime 	=	null,
	@reviewed_until		datetime 	=	null,
	@reviewed_severity	varchar(30) =	null
)
as
begin
	declare @last_ok_time datetime

if @batchjob is null
begin
	if exists ( select * from IgnoreList
		where isnull(monitor_program,@monitor_program)=@monitor_program
		and 	isnull(system,isnull(@system,'CANTHAPPENSTR'))
					=isnull(@system,'CANTHAPPENSTR')
		and	isnull(subsystem,isnull(@subsystem,'CANTHAPPENSTR'))
					=isnull(@subsystem,'CANTHAPPENSTR')  )
		--and 	isnull(system,'CANTHAPPENSTR')=isnull(@system,'CANTHAPPENSTR')
		--and	isnull(subsystem,'CANTHAPPENSTR')=isnull(@subsystem,'CANTHAPPENSTR')  )
	begin
		-- ignore this one
		select 	@reviewed_by='IgnoreList',
					@reviewed_time=getdate(),
					@reviewed_until='Jan 1 2200',
					@message_text='(on ignore list) '+@message_text
		-- return
	end

	if exists ( select * from GEM_Servers
					where 	blackedout_until is not null
					and		blackedout_until > getdate()
					and 		system_name=isnull(@system,'CANTHAPPENSTR') )
	begin
		-- ignore this one
		select 	@reviewed_by='BlackedOut',
					@reviewed_time=getdate(),
					@reviewed_until='Jan 1 2200',
					@message_text='(on blackout list) '+@message_text
		-- return
	end
end

	-- declare @last_state varchar(20), @last_time datetime
	if @heartbeat_time is null
		select @heartbeat_time=getdate()

	-- First Message U R Inserting
	-- if ( @last_state is null )
	if not exists (
			select 1 from Heartbeat
			where monitor_program=@monitor_program
			and 	isnull(system,'CANTHAPPENSTR')   = isnull(@system ,'CANTHAPPENSTR')
			and	isnull(subsystem,'CANTHAPPENSTR')= isnull(@subsystem,'CANTHAPPENSTR')  )
	begin
		if ( @state = 'OK' or @state = 'COMPLETED' or @state='RUNNING' )
			select @last_ok_time = @heartbeat_time

		INSERT Heartbeat
		VALUES ( @monitor_program , @heartbeat_time, @last_ok_time,
			@system, @subsystem, @state,
			@message_text, @document_url,
			@reviewed_by			,
			@reviewed_time			,
			@reviewed_until		,
			@reviewed_severity	,
			--null,null, null, 'I',
			@batchjob,'I' )
	end
	-- It was good
	else if ( @state = 'OK' or   @state = 'COMPLETED' or @state='RUNNING' )
		UPDATE 	Heartbeat
		set	last_ok_time=@heartbeat_time,
			state=@state,
			message_text=@message_text,
			monitor_time=@heartbeat_time,
			internal_state='I'	,

			-- added this 10/10/07
			reviewed_by			=	isnull(@reviewed_by		,	reviewed_by),
			reviewed_time		=	isnull(@reviewed_time	,	reviewed_time),
			reviewed_until		=	isnull(@reviewed_until	,	reviewed_until),
			reviewed_severity	=	isnull(@reviewed_severity,	reviewed_severity)

		where monitor_program=@monitor_program
		and	system=@system
		and	isnull(subsystem,'CANTHAPPENSTR')=isnull(@subsystem,'CANTHAPPENSTR')
		and   monitor_time<@heartbeat_time
	else
		UPDATE 	Heartbeat
		set	state=@state,
			message_text=@message_text,
			monitor_time=@heartbeat_time,
			internal_state='I',

			reviewed_by			=	isnull(@reviewed_by		,	reviewed_by),
			reviewed_time		=	isnull(@reviewed_time	,	reviewed_time),
			reviewed_until		=	isnull(@reviewed_until	,	reviewed_until),
			reviewed_severity	=	isnull(@reviewed_severity,	reviewed_severity)

		where monitor_program=@monitor_program
		and	system=@system
		and	isnull(subsystem,'CANTHAPPENSTR')=isnull(@subsystem,'CANTHAPPENSTR')
		and   monitor_time<@heartbeat_time
end
go

