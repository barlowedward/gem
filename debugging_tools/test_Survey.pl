use lib qw(/apps/sybmon/dev/lib);
#use RosettaStone;
use CommonFunc;
#use DBIFunc;
use Repository;
use GemData;

my(%CONFIG)=read_configfile(-nodie=>1);
foreach ( keys %CONFIG ) {
	print $_," => ",$CONFIG{$_},"\n"
		if /^ALARM/;
}

#dbi_set_mode("INLINE");

my($gd)=new GemData();
my($gem_root_dir)=get_gem_root_dir();
my(@rc)=$gd->survey(
			-name=>$CONFIG{ALARM_SERVER},
			-login=>$CONFIG{ALARM_LOGIN},
			-password=>$CONFIG{ALARM_PASSWORD},
			-type=>'Sybase', 
			-local_directory=>$gem_root_dir."/data/system_information_data");
foreach (@rc) { 
	print $_;
}

print "Getting Info\n";
my($d)=$gd->get_set_data(-class=>sybase, -name=>$CONFIG{ALARM_SERVER}, -debug=>1);
print $d;
