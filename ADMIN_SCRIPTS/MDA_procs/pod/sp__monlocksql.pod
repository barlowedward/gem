=head1 NAME

sp__monlocksql - show lock information and the sql locking it

=head2 AUTHOR

Edward Barlow (SQL Technologies)

=head2 DESCRIPTION

Show information about locks AND the sql that causes them.  This can
be rather useful, although you need to run this from some form of windowed
isql or it will scroll off and be totally unusable.

This procedure uses vmstat/iostat format - you can run it either once
or in a loop (@num_iter times with @num_sec_delay seconds between runs).

=head2 USAGE

sp__monlocksql  @dont_format

  - @dont_format   : char(1): no output formating if its not null

=head2 SAMPLE OUTPUT

1> sp__monlocksql

 SPID	ObjName	LockState	LockType	LockLevel	WaitTime	SQL
 481	tempdb.#L___________01004810009078207   	Granted	exclusive table	TABLE		sp__monlocksql
 833	clientmlp.hts_profit_loss               	Granted	shared intent	TABLE		select hts_profit_loss.* from hts_profit_loss
 905	clientmlp.hts_profit_loss               	Granted	shared intent	TABLE		EXECUTE imagvue..swap_report_sp
 681	clientmlp.hts_profit_loss               	Granted	shared intent	TABLE		select * from hts_profit_loss where pl_date=20060407
 596	shared1.hts_yc_sp                       	Granted	shared intent	TABLE		select * from hts_yc_sp where ycsp_ycsph_id = 10001685
 596	clientmlp.hts_yc_sp                     	Granted	shared intent	TABLE		select * from hts_yc_sp where ycsp_ycsph_id = 10001685
