#!/apps/perl/linux/perl-5.8.2/bin/perl

use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);

use strict;
use Getopt::Long;
use DBIFunc;
use Data::Dumper;
use File::Basename;

use vars qw( $USER $RUNMINUTES $SERVER $PASSWORD $TRUNCATETIME $DEBUG);

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

usage ("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"		=> \$SERVER,
		"USER=s"			=> \$USER,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"RUNMINUTES=s"	=> \$RUNMINUTES,
		"TRUNCATETIME=s"     	=> \$TRUNCATETIME,
		"DEBUG"      	=> \$DEBUG );

sub usage {
	print @_;
	die "GetSybMDASqlText.pl --SERVER=xxx --USER=xxx --PASSWORD=xxx --RUNMINUTES=s [--TRUNCATETIME=ms]\n";
}
usage("Must pass server\n" ) 		   unless defined $SERVER;
usage("Must pass username\n" )	  	unless defined $USER;
usage("Must pass password\n" )	  	unless defined $PASSWORD;
usage("Must pass RUNMINUTES\n" )	  	unless defined $RUNMINUTES;

$RUNMINUTES = time + 60*$RUNMINUTES;
dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD)
	or die "Cant connect to $SERVER as $USER\n";

dbi_set_mode("INLINE");

my(@m2Tbl,@m3Tbl);
my(%m2ColAry);		# key=colid val=colname
my($query1_key_1,$query1_key_2,$query1_key_3,$query1_sqltext,$query1_dat_2);
my($query2_key_1,$query2_key_2,$query2_key_3);
my($query2_key_4,$query2_key_5,$query2_key_6,$query2_key_7,$query2_key_8);
my($query2_ElapsedTimeColid)=0;
my($query3_key_1,$query3_key_2,$query3_key_3);
my($query3_key_4,$query3_key_5,$query3_key_6);
my($m3PlanText);

my(%objectnames);		# DBID:OBJECTID
sub get_objectname {
	my($DBID,$OBJID)=@_;
	return $objectnames{$DBID.":".$OBJID} if $objectnames{$DBID.":".$OBJID};
	return "" if $OBJID==0;
	my(@rc)=dbi_query( -db=>"", -query=>"select object_name($OBJID,$DBID)");
	foreach(@rc) {
		my(@d)=dbi_decode_row($_);
		$objectnames{$DBID.":".$OBJID} = $d[0];
		return $d[0];
	}
}

# GET MY SPID
my($spid);
my(@rc)=dbi_query(-db=>"master",-query=>"select \@\@spid");
foreach(@rc) { my(@d)=dbi_decode_row($_); $spid=$d[0]; }

my($q)="select TableName,ColumnName,ColumnID from master..monTableColumns  where TableName in ('monSysSQLText','monSysStatement','monSysPlanText') order by TableID, ColumnID";
@rc=dbi_query(-db=>"",-query=>$q);
foreach( @rc) {
		my @dat=dbi_decode_row($_);
		if( $dat[0] eq "monSysSQLText" ) {
			$query1_sqltext = $dat[2] if $dat[1] eq "SQLText";
			$query1_dat_2 = $dat[2] if $dat[1] eq "ServerUserID";
			$query1_key_1 = $dat[2] if $dat[1] eq "SPID";
			$query1_key_2 = $dat[2] if $dat[1] eq "KPID";
			$query1_key_3 = $dat[2] if $dat[1] eq "BatchID";
		} elsif( $dat[0] eq "monSysStatement" ) {
			push @m2Tbl,$dat[1] unless $dat[1] eq "ContextID" or $dat[1] eq "LineNumber" or $dat[1] eq "PlanID";
			$m2ColAry{$dat[2]} = $dat[1] if $dat[1] eq "CpuTime" or
														$dat[1] eq "WaitTime" or
														$dat[1] eq "PhysicalReads" or
														$dat[1] eq "LogicalReads" or
														$dat[1] eq "PagesModified" or
														$dat[1] eq "PacketsSent" or
														$dat[1] eq "PacketsReceived";
			$query2_ElapsedTimeColid = $dat[2] if $query2_ElapsedTimeColid < $dat[2];
			$query2_key_1 = $dat[2] if $dat[1] eq "SPID";
			$query2_key_2 = $dat[2] if $dat[1] eq "KPID";
			$query2_key_3 = $dat[2] if $dat[1] eq "BatchID";
			$query2_key_4 = $dat[2] if $dat[1] eq "ContextID";
			$query2_key_5 = $dat[2] if $dat[1] eq "PlanID";
			$query2_key_6 = $dat[2] if $dat[1] eq "DBID";
			$query2_key_7 = $dat[2] if $dat[1] eq "ProcedureID";
			$query2_key_8 = $dat[2] if $dat[1] eq "LineNumber";
		} elsif( $dat[0] eq "monSysPlanText" ) {
			push @m3Tbl,$dat[1];
			$query3_key_1 = $dat[2] if $dat[1] eq "SPID";
			$query3_key_2 = $dat[2] if $dat[1] eq "KPID";
			$query3_key_3 = $dat[2] if $dat[1] eq "BatchID";
			$query3_key_4 = $dat[2] if $dat[1] eq "ContextID";
			$query3_key_5 = $dat[2] if $dat[1] eq "PlanID";
			$query3_key_6 = $dat[2] if $dat[1] eq "SequenceNumber";
			$m3PlanText 	= $dat[2] if $dat[1] eq "PlanText";
		} else {
			die "UNKNOWN KEY $dat[0]\n";
		}
}

die "INVALID Q1 KEY\n" 		unless $query1_key_1 or $query1_key_2 or $query1_key_3;
die "INVALID Q2 KEY\n" 		unless $query2_key_1 or $query2_key_2 or $query2_key_3;
die "INVALID Q2 SUBKEY\n" 	unless $query2_key_4 or $query2_key_5 or $query2_key_6;
die "INVALID Q3 KEY\n" 		unless $query3_key_1 or $query3_key_2 or $query3_key_3;
die "INVALID Q3 SUBKEY\n" 	unless $query3_key_4 or $query3_key_5 or $query3_key_6;
die "INVALID Q3 VAL\n" 		unless $m3PlanText;

$query2_ElapsedTimeColid++;		# its the last column

# GET LOGINS
my(%loginnames);
if($query1_dat_2) {
	my(@rc)=dbi_query(-db=>"",-query=>"select suid,name from syslogins");
	foreach(@rc) { my(@d)=dbi_decode_row($_); $loginnames{$d[0]} = $d[1]; }
}

my($query1_sql)="select * from monSysSQLText where SPID!=$spid order by SPID,KPID,BatchID,SequenceInBatch ";
my($query2_sql)="select *,datediff(ms,StartTime,EndTime) from monSysStatement where SPID!=$spid order by SPID,KPID,BatchID,PlanID,ContextID,LineNumber";
my($query3_sql)="select * from monSysPlanText where SPID!=$spid order by SPID,KPID,BatchID,PlanID,ContextID,SequenceNumber";

my(%monSysSQLText,%monSysStatement,%monSysPlanText);	# hashes are keyed on SPID,KPID,BatchID
my(%BatchLogin);

sub init_sample_set {
		my(@rc)=dbi_query(-db=>"",-query=>$query1_sql);
		@rc=dbi_query(-db=>"",	-query=>$query2_sql);
		@rc=dbi_query(-db=>"",	-query=>$query3_sql);
}
init_sample_set();

sub collect_sample_set {
	my(@rc)=dbi_query(-db=>"",-query=>$query1_sql);
	foreach(@rc) {
			my @dat=dbi_decode_row($_);
			die "NO COL $query1_sqltext in data ".join("|",@dat) unless $dat[$query1_sqltext];
			$dat[$query1_sqltext] =~ s/\n/ /g;
		   $dat[$query1_sqltext] =~ s/\s+/ /g;
		   $dat[$query1_sqltext] =~ s/^\s+//;
		   $dat[$query1_sqltext] =~ s/\s+$//;
		   my($key)=$dat[$query1_key_1].":".$dat[$query1_key_2].":".$dat[$query1_key_3];
		   if( $DEBUG ) {
		   	print "DBG: SQLText: ",join("|",@dat),"\n";
			   print "DBG: KEY=".$key." SQL= $dat[$query1_sqltext]\n";
			}
			if( ! $monSysSQLText{$key} ) {
				$monSysSQLText{$key} = $dat[$query1_sqltext];
			} else {
				$monSysSQLText{$key} .= " ".$dat[$query1_sqltext];
			}
			$BatchLogin{$key} = $dat[$query1_dat_2] if $query1_dat_2;
	}

	@rc=dbi_query(-db=>"",-query=>$query2_sql);
	foreach(@rc) {
			my @dat=dbi_decode_row($_);
			my($key)=$dat[$query2_key_1].":".$dat[$query2_key_2].":".$dat[$query2_key_3];
			my($subkey)=$dat[$query2_key_4].":".$dat[$query2_key_5].":".$dat[$query2_key_6].":".$dat[$query2_key_7].": Line ".$dat[$query2_key_8];

		   if( $DEBUG ) {
		   	print "DBG: STMT: ",join("|",@dat),"\n";
			   print "DBG: KEY=".$key."\n";
			}
			if( ! $monSysStatement{$key} ) {
		   	my(%dat2);
		   	$dat2{$subkey} = \@dat;
				$monSysStatement{$key} = \%dat2;
			} else {
				my($dat2) = $monSysStatement{$key};
				if( $$dat2{$subkey} ) {
					my($ref)=$$dat2{$subkey};
					my($e)=join("|",@$ref);
					my($n)=join("|",@dat);
					next if $e eq $n;					# are they identical - deal with dups from buggy mda monitoring tbls
					print "2 WARNING: Subkey $subkey Exists\n";
					print "@ EXISTING VALUE:  ",$e,"\n";
					print "@ NEW VALUE:       ",$n ,"\n";
				}
				$$dat2{$subkey} = \@dat;
				#print Dumper \$dat2;
			}
	}

	@rc=dbi_query(-db=>"",-query=>$query3_sql);
	foreach(@rc) {
			my @dat=dbi_decode_row($_);
			die "NO COL $query3_key_1 in data ".join("|",@dat) unless $dat[$query3_key_1];
			die "NO COL $query3_key_2 in data ".join("|",@dat) unless $dat[$query3_key_2];
			die "NO COL $query3_key_3 in data ".join("|",@dat) unless $dat[$query3_key_3];

			my($key)=$dat[$query3_key_1].":".$dat[$query3_key_2].":".$dat[$query3_key_3];
		   if( $DEBUG ) {
		   	print "DBG: PLAN: ",join("|",@dat),"\n";
			   print "DBG: KEY=".$key."\n";
			}
			$dat[$m3PlanText] =~ s/\s+$//;
			if( ! $monSysPlanText{$key} ) {
		   	my(%dat2);
		   	$dat2{$dat[$query3_key_4].":".$dat[$query3_key_5].":".$dat[$query3_key_6]} = $dat[$m3PlanText];
		   	#print "KEY: $key SUBKEY:".$dat[$query3_key_4].":".$dat[$query3_key_5].":".$dat[$query3_key_6]." = ",$dat[$m3PlanText],"\n";
				$monSysPlanText{$key} = \%dat2;
			} else {
				my($dat2) = $monSysPlanText{$key};
				#print "KEY: $key SUBKEY:".$dat[$query3_key_4].":".$dat[$query3_key_5].":".$dat[$query3_key_6]." = ",$dat[$m3PlanText],"\n";
				if( $$dat2{$dat[$query3_key_4].":".$dat[$query3_key_5].":".$dat[$query3_key_6]} ) {
					print "3 WARNING: Subkey $dat[$query3_key_4]:$dat[$query3_key_5]:$dat[$query3_key_6] Exists\n";
					print "@ EXISTING VALUE:  ",$$dat2{$dat[$query3_key_4].":".$dat[$query3_key_5].":".$dat[$query3_key_6]},"\n";
					print "@ NEW VALUE:  ", $dat[$m3PlanText],"\n";
				}
				$$dat2{$dat[$query3_key_4].":".$dat[$query3_key_5].":".$dat[$query3_key_6]} = $dat[$m3PlanText];
			}
	}
}

sub show_sample_set {
	my(%summary_nruns,%summary_totaltime);	# keyed on sqltext
	foreach my $key ( sort keys %monSysSQLText) {
		next if $monSysSQLText{$key} =~ /^use \w+$/;
		my($headertext)="";
		my($statementtext)="";
		my($batchtime)=0;
		my(@keydat)=split(":",$key);
		$headertext .= "SPID=$keydat[0] KPID=$keydat[1] BATCHID=$keydat[2] ";
		$headertext .=  "LOGIN: ".$loginnames{$BatchLogin{$key}}."\t" if $query1_dat_2;
		$headertext .=  "\n";
		$headertext .=  "SQL TEXT:".$monSysSQLText{$key}."\n";
		my($parsedtext)=$monSysSQLText{$key};
		$parsedtext =~ s/\'\w+\'/STR/g;
		$parsedtext =~ s/\"\w+\"/STR/g;
		$parsedtext =~ s/\b\d+\b/NUM/g;
		$summary_nruns{$parsedtext}++;
		$summary_totaltime{$parsedtext}=0;
		my($dat2) = $monSysStatement{$key};
		$statementtext .=  Dumper \$dat2 if $DEBUG;
		if( $dat2 ) {
			$statementtext .=  "SUBKEY=ContextID/PlanID,DBID/ProcedureID\n";

# 			THE OLD WAY OF PRINTING IT
#			foreach my $arykeys ( sort keys %$dat2 ) {
#				my $aryptr = $$dat2{$arykeys};
#				foreach my $colid ( keys %m2ColAry ) {
#					printf( "\t%-24s => %s\n", " [$arykeys]".$m2ColAry{$colid},$$aryptr[$colid] );
#				}
#				printf( "\t%-24s => %s miliseconds\n", " [$arykeys]"."Elapsed Time",$$aryptr[$query2_ElapsedTimeColid] );
#			}

			# get a list of real subkeys (no line #)
			my(%real_subkeys);
			foreach my $arykeys ( sort keys %$dat2 ) {
				my $k = $arykeys;
				$k =~ s/: Line \d+$//;
				$real_subkeys{$k}=1;
			}

			foreach my $rsubkey ( sort keys %real_subkeys) {
				my(@xx)=split(/:/,$rsubkey);
				if( $xx[3] ) {
					my($objname)=get_objectname($xx[2], $xx[3]);
					$statementtext .=  "\tMonSysStatement: DBID=$xx[2] OBJECTID=$xx[3] IMPLIES OBJECTNAME=$objname\n";
				}
				my(@outlines);
				foreach my $colid ( keys %m2ColAry ) {
					#print "DBG WORKING ON COLUMN $colid\n";
					my(@dat);
					my(@hdr)=("SUBKEY","ITEM");
					foreach my $arykeys ( sort keys %$dat2 ) {
						next unless $arykeys =~ /^$rsubkey/;
						my $aryptr = $$dat2{$arykeys};
						#print "DBG WORKING ON ARYKEY $arykeys - value = $$aryptr[$colid] \n";

						my $k = $arykeys;
						if( $#outlines < 0) {
							$k=~m/(\d+)$/;
							push @hdr,"Line ".$1;
						}

						if( $#dat < 0 ) {
							push @dat, $rsubkey;
							push @dat, $m2ColAry{$colid};
						}
						push @dat, $$aryptr[$colid];
						#printf( "\t%-24s => %s\n", " [$k]".$m2ColAry{$colid},$$aryptr[$colid] );
					}
					push @outlines,\@hdr if $#outlines<0;
					push @outlines,\@dat;
				}

				my(@elapsed);
				foreach my $arykeys ( sort keys %$dat2 ) {
					next unless $arykeys =~ /^$rsubkey/;
					my $aryptr = $$dat2{$arykeys};
					push @elapsed, ($rsubkey,"Elapsed Time") if $#elapsed<0;
					push @elapsed, $$aryptr[$query2_ElapsedTimeColid];
					$batchtime += $$aryptr[$query2_ElapsedTimeColid];
					#printf( "DBG DBG \t%-24s => %s miliseconds\n", " [$arykeys]"."Elapsed Time",$$aryptr[$query2_ElapsedTimeColid] );
				}
				push @outlines,\@elapsed;
				$statementtext .=  "\t".join("\n\t",dbi_reformat_results(@outlines))."\n\n";
			}

			# Again - Old Way
			#foreach my $arykeys ( sort keys %$dat2 ) {
			#	my $aryptr = $$dat2{$arykeys};
			#	printf( "\t%-24s => %s miliseconds\n", " [$arykeys]"."Elapsed Time",$$aryptr[$query2_ElapsedTimeColid] );
			#}
		}

		my($dat3) = $monSysPlanText{$key};
		$statementtext .=  Dumper \$dat3 if $DEBUG;
		my($showplantext)="";
		if( $dat3 ) {
			$showplantext .=  "\tSHOWPLAN KEYS = ContextID:PlanID:SequenceNumber\n";
			my($numlines)=0;
			foreach my $k ( sort keys %$dat3 ) {
				$numlines++;
				$showplantext .= sprintf( "\tSHOWPLAN [%s] => %s \n", $k,$$dat3{$k} );
			}
			$showplantext = "[Not Printing Showplan As Only $numlines Lines]\n" if $numlines<=4;
		}
		$summary_totaltime{$parsedtext}+=$batchtime;
		if( $batchtime < $TRUNCATETIME ) {
			print $headertext;
		} else {
			print $headertext.$statementtext.$showplantext;
		}
		print "===================\n";
	}
	print "XXXXXXXXXXXXXXX  SUMMARY XXXXXXXXXXXXXXX\n";
	printf "%4s %7s %7s %s\n", "nruns", "TotSecs", "AvgSecs", "SQL";
	foreach( sort keys %summary_totaltime) {
		printf "%4d %7f %7f %s\n",
			$summary_nruns{$_},
			int($summary_totaltime{$_}/100)/10,
			int($summary_totaltime{$_}/($summary_nruns{$_}*100))/10,
			$_;
	}
	print "XXXXXXXXXXXXXXX  SUMMARY XXXXXXXXXXXXXXX\n";
}

# FOR TESTING
#sleep(10);
#collect_sample_set();
#show_sample_set();
#exit(0);

while (time < $RUNMINUTES ) {
	print "Collecting...\n";
	collect_sample_set();
	#print Dumper \%monSysSQLText 		if $DEBUG;
	#print Dumper \%monSysStatement 	if $DEBUG;
	sleep(20);
}
print "===================\n";
show_sample_set();

exit(0);

dbi_disconnect();
exit(0);

__END__

=head1 NAME

GetSybMDASqlText.pl - get running sql

=head2 USAGE

GetSybMDASqlText.pl --SERVER=xxx --USER=xxx --PASSWORD=xxx --RUNMINUTES=s [--DELETE] [--TABULATE] [--REPORT] [--CLEAR]
	--TABULATE will print the output formatted by columns (spaces)

=head2 DESCRIPTION

Uses the MDA tables to collect info on sql running on your servers

Creates a table MDAhistory in db with output

--REPORT allows you to see existing data

--CLEAR clears out the table and exits

--DELETE clears out the table and continues

--RUNMINUTES allows you to schedule this via cron - it will run that number of minutes

--TABULATE pretty prints the output which is probably what you want.  If its not passed, a tab is used as a delimeter.
if it is passed, we remove all multi-whitespace so the query report looks ok

this handles the differences between the 2 versions of the MDA table

REQUIRES MDA TABLES TO BE INSTALLED & SET UP APPROPRIATELY

SYBASE ONLY OF COURSE

=head2 MDA Tables

sp_help monSysSQLText
	NUMBER  : sql text pipe max messages
	REQUIRES: enable monitoring, max SQL text monitored, SQL batch capture, sql text pipe max messages, and statement pipe active
	*SPID
	*KPID
	*BatchID
	ServerUserID   		=> suser_name()
	SequenceInBatch		=> CAN JOIN THESE FIELDS TOGETHER - THEY ARE ONLY 255 LONG

	SQLText
sp_help monSysPlanText
	NUMBER  : plan text pipe max messages
	REQUIRES: enable monitoring, plan text pipe max messages, and plan text pipe active
	*SPID
	*KPID
	*BatchID
	**ContextID
	**PlanID
	SequenceNumber
	DBID
	ProcedureID
	PlanText

sp_help monSysStatement
	NUMBER  : statement pipe max messages
	REQUIRES: enable monitoring, statement statistics active, per object statistics active, statement pipe max messages, and statement pipe active
	*SPID
	*KPID
	*BatchID
	DBID
	ProcedureID
	**PlanID
	**ContextID
	LineNumber
	CpuTime
	WaitTime
	MemUsageKB
	PhysicalReads
	LogicalReads
	PagesModified
	PacketsSent
	PacketsReceived
	NetworkPacketSize
	PlansAltered
	StartTime				-> VERY USEFUL
	EndTime					-> VERY USEFUL
monOpenObjectActivity

Ideas - Batch Count By SPID/KPID/BatchId
=cut
