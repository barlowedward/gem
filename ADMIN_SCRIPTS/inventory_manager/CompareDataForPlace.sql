
-- Compare Two For A City

declare @plid int, @plid2 int, @wasp_site varchar(30)
-- select  @plid=14,  @plid2=3, @wasp_site='Greenwich'
select  @plid=25,  @plid2=19, @wasp_site='Ridgefield Park'

select * from barlow_place_map
where (dcm_placeid = @plid or dcm_placeid = @plid2 or wasp_site=@wasp_site )
order by wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet

select distinct cntrycitybldgnum,floor,row,cabinet,num=count(*)
from machines where (cntrycitybldgnum=@plid or cntrycitybldgnum=@plid2)
group by cntrycitybldgnum,floor,row,cabinet
order by cabinet,cntrycitybldgnum,floor,row

select distinct
wasp_site = @wasp_site,
wasp_location = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_floor = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Floor'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_row = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Row'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_cabinet = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Cabinet'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type )
--into 	 #barlow_place_map
from 	gemalarms..INV_system_attribute s
where 	s.system_type ='WASP' and attribute_name='Site' and attribute_value=@wasp_site
order by wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet
