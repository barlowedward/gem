# run this on a peecee

use strict;
use lib qw(G:/dev/lib
	   G:/dev/apps/sybmon/lib
	   G:/dev/Win32_perl_lib_5_8);

use Carp qw/confess/;
use Getopt::Long;
use MlpAlarm;
use File::Basename;
use DBIFunc;
my $VERSION = 1.0;

our @Field =
(
# "dn",
# "desc",
  "samid",
# "sid",
# "upn",
# "fn",
# "mi",
# "ln",
  "display",
# "empid",
# "office",
# "tel",
# "email",
# "hometel",
# "pager",
# "mobile",
# "fax",
# "iptel",
# "webpg",
# "title",
# "dept",
# "company",
# "hmdir",
# "hmdrv",
# "profile",
# "loscr",
# "mustchpwd",
# "canchpwd",
# "reversiblepwd",
# "pwdneverexpires",
# "acctexpires",
# "disabled",
);

our %Field;

for (my $n = 0; $n < @Field; ++$n)
{
  $Field{$Field[$n]} = $n;
}

our $SAMID   = $Field{"samid"};
our $DISPLAY = $Field{"display"};

our $PROGRAM_NAME = basename($0);

our $BATCH_ID;
our $DBGMAX;

sub usage
{
  print @_;

  print "$PROGRAM_NAME Version $VERSION\n\n";
  print @_;
  print "SYNTAX:\n";
  print $PROGRAM_NAME." [--BATCH_ID=id] [--DBGMAX=maxusers]
  where:
    --DBGMAX=max          number of rows for diagnostics
\n";
  exit;
}

$| = 1;	# flush stdout all the time

my $curdir = dirname($0);
chdir $curdir or die "Error $! on 'cd $curdir'\n";

die usage("Bad Parameter List\n")
unless GetOptions
(
  "DBGMAX:n"	=> \$DBGMAX,
  "BATCH_ID:s"	=> \$BATCH_ID
);

$DBGMAX  = 0 unless $DBGMAX;	# limit the number of users for testing

our $CONNECTION = MlpGetDbh();

my $query;
my $cmd;

# remove the old list of users from the table

$query = "\tdelete INV_principal where created_from = 'ldapimporter'";
MlpAlarm::_querynoresults($query);

# insert the complete list of users

$cmd = "dsquery user -limit $DBGMAX | dsget user";

foreach (@Field)
{
  $cmd .= " -$_";
}

$cmd .= " -c -l |";

print "$cmd\n";

my $linenum = -1;

open(CMD, $cmd) or die "Error $! on $cmd\n";	# $! = errno

our $Now = get_now();

print "$Now\n";

our @field;

while (<CMD>)
{
  chomp;

  if ($_ eq "dsget succeeded") {exit;}

  my @a = split /:/, $_, 2;

  if (@a == 0)
  {
    my $samid   = $field[$SAMID];
    my $display = $field[$DISPLAY];
    print "$samid $display\n";
    if ($display =~ /^SystemMailbox/
     || $display =~ /^Enterprise Vault System Mailbox/)
    {
      print "OMITTED\n";
    }
    else
    {
      print "INCLUDED\n";
      my $Samid   = quote($samid);
      $query = "INSERT INV_principal
       (principal_name,   created_from, created_date)
values (        $Samid, 'ldapimporter',       '$Now')";
      MlpAlarm::_querynoresults($query);
    }
    @field = ();
  }
  elsif (@a == 2)
  {
    my $f;
    my $key = $a[0];
    if (exists($Field{$key}))
    {
      $f = $Field{$key};
      $field[$f] = substr($a[1], 1);
    }
    else
    {
      print __LINE__, " $key Huh?\n";
      exit;
    }
  }
  else
  {
    print __LINE__, " Huh?\n";
    exit;
  }
}
close(CMD);

sub get_now
{
  my ($S, $M, $H, $d, $m, $y) = localtime(time());
  my $now = sprintf("%04d%02d%02d %02d:%02d:%02d",
						  $y+1900, $m+1, $d, $H, $M, $S);
  return $now;
}

sub quote
{
  my $text = $_[0];
  return dbi_quote(-connection =>$CONNECTION, -text =>$text);
}

__END__

=pod

=head1 NAME

INV_principal - load samid's of users into INV_principal

=head1 SYNOPSIS

INV_principal.pl [--BATCH_ID=s] [--DBGMAX=n]

=head1 DESCRIPTION

Remove from INV_principal all records with
created_from = 'ldapimporter'

Using dsquery/dsget, get user samid's from ldap, and
insert them into INV_principal

=cut
