#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

use strict;
use Getopt::Std;
use File::Basename;
$|=1;

use vars qw($opt_d $opt_U $opt_S $opt_P $opt_D $opt_i);
die usage("Bad Parameter List") unless getopts('dU:D:S:P:i:');

# cd to the right directory
my($curdir)=dirname($0);
#chdir $curdir or die "Cant cd to $curdir: $!\n";

use Sybase::SybFunc;
#require("$curdir/syb_func.pl");

sub usage
{
        print @_;
        print "bcp_in.pl [-d] -UUSER -SSERVER -PPASS -DDB [-iDIR] File File File\n";
        return "\n";
}

my($num_files_loaded)=1;
die usage("Bad Parameter List") unless getopts('dU:D:i:S:P:');
die usage("Must Pass Database\n")
        unless defined $opt_D;
die usage("Must pass sa password\n" )
        unless defined $opt_P;
die usage("Must pass server\n")
        unless defined $opt_S;
die usage("Must pass sa username\n" )
        unless defined $opt_U;

my( $DBPROC_write );

print "Connecting To Server $opt_S\n";
db_connect($opt_S,$opt_U,$opt_P,1)
        or die "Cant connect to $opt_S as $opt_U\n";

print "Using $opt_D\n" if defined $opt_d;
db_use_database($opt_D);

print "Deciphering select into for db\n" if defined $opt_d;
my $query = "select 1 from master..sysdatabases where name=\"$opt_D\" and status&4=4  and status & 0x1120 = 0 and status2&0x0030=0\n";
print $query if defined $opt_d;
my($si)="False";
foreach (db_query("master",$query)) {
                $si="True";
}
print "Select Into = $si\n";

my(%tables);

$query="Select name from sysobjects where type=\"U\"\n";
print $query if defined $opt_d;
foreach (db_query($opt_D,$query)) {
                $tables{$_}=1;
}

my(@files)=@ARGV;
push @files, dirlist($opt_i) if defined $opt_i;

foreach (@files) {
	print "File=$_\n" if defined $opt_d;
        my($table)=basename($_);

        my($filename)=$_ if -r $_;
   	$filename = $opt_i."/".$_ if -r $opt_i."/".$_;

        die "FILE $_ not found\n" unless defined $filename;

        $table=~ s/\.\w+//g;

        if( ! defined $tables{$table} ) {
                print "Ignoring $filename as no table found in target\n";
                next;
        } else {
                printf( "%4s ",$num_files_loaded );
                db_bcp_table($table,$filename,"~~",1,100,"INSERT_RETURN_HERE");
                $num_files_loaded++;
        }
}

$num_files_loaded--;
print "Command Completed: Copy $num_files_loaded Files In\n";

db_close_sybase();

exit(0);

# ==========================
# dirlist(): Return All Files In A Directory
# ==========================
sub dirlist
{
        my($dir) = @_;

        opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
        my(@dirlist) = grep(!/^\./,readdir(DIR));
        closedir(DIR);

        return @dirlist;
}


__END__

=head1 NAME

bcp_in.pl - utility to copy files into a server

=head2 DESCRIPTION

Bcp files into a server.  The files should be created with bcp_out.pl.
The files are in character mode with a delimeter of two tildas.

=head2 USAGE
        bcp_in.pl [-d] -UUSER -SSERVER -PPASS -DDB [-iDIR] File File File

=cut
