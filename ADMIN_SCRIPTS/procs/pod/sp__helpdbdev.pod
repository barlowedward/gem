
=head1 NAME

sp__helpdbdev  - Show how database uses devices

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


Show device to database breakdown. Which devices are used by database.

=head2 SEE ALSO


The following procedures are especially useful while creating new databases
and attempting to perform optimal allocation of space: sp__helpdb,
sp__helpdevice, sp__helpdbdev.

=head2 USAGE


sp__helpdbdev [ @dbname ]

if @dbname parameter is passed, only show information for given database

=head2 SAMPLE OUTPUT


 1> sp__helpdbdev

 Database Name Device Name Size Usage
 --------------- --------------- -------------------- ---------------
 master master 2.000000 data and log
 master master 2.000000 data and log
 master master 3.000000 data and log
 migrator datadevice 10.000000 data and log
 model master 2.000000 data and log
 pubs2 master 2.000000 data and log
 tempdb master 2.000000 data and log

 1> exec sp__helpdbdev
  Database Name   Device Name     Size                 Usage
  --------------- --------------- -------------------- ---------------
  master          master                      1.500000 data and log
  master          master                      3.000000 data and log
  mis             datadev2                    5.000000 data only
