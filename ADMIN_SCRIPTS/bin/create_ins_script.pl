#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

# CREATE INSERT SCRIPT FROM DATA IN A SERVER

use strict;
use Getopt::Std;
sub usage
{
	my($str)=@_;
   return $str."USAGE: create_ins_script.pl -Ssrv -Uusr -Ppass -Ddb -ttable -qquery [-eexclcol1,exclcol2] -iintcol1,intcol2

	-d debug mode
	-i no quote columns (used for user defined types)
	-e exclude from report
	-r random data set
	-k num rows of random data set

   Output is placed in tablename.ins
   If no query passed with -q it will do a select \*\n";

}

use vars qw($opt_d $opt_e $opt_t $opt_U $opt_S $opt_P $opt_D $opt_q $opt_i $opt_r $opt_k);
die usage("Bad Parameter List\n") unless getopts('U:D:S:P:t:q:e:i:drk:');

use File::Basename;
my($curdir)=dirname($0);
use DBIFunc;

die usage("Must pass table\n")		  unless defined $opt_t;
die usage("Must pass database\n")	       unless defined $opt_D;
die usage("Must pass sa password\n" )	   unless defined $opt_P;
die usage("Must pass server\n")		 unless defined $opt_S;
die usage("Must pass sa username\n" )	   unless defined $opt_U;

#
# CONNECT TO DB
#
print "Creating Insert Script For $opt_D..$opt_t\n";

dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
	or die "Cant connect to $opt_S as $opt_U\n";

print "Successful login to $opt_S\n";

my(%NO_QUOTE_OVERRIDES);
if( defined $opt_i ) {
   foreach ( split /,/,$opt_i ) {
      s/\s//g;
      $NO_QUOTE_OVERRIDES{$_} = "TRUE";
   }
}

my(%EXCLUDE_COLS);
if( defined $opt_e ) {
	foreach ( split /,/,$opt_e ) {
		s/\s//g;
		$EXCLUDE_COLS{$_} = "TRUE";
	}
}

#
# BUILD STRING FOR INSERT
#     helpcolumn returns ordered list of column names & types
#     @do_quote is YES/NO if column must have quotes
#
my($field_name_list)="";
my($query_fields)="";
my(@col_nm,@do_quote,@allow_null,@data_type);
foreach ( dbi_query(-db=>$opt_D,-query=>"exec sp__helpcolumn \@objname=$opt_t, \@dont_format='Y'") ) {
	s/\s//g;
	my(@rc)=dbi_decode_row($_);

	if( defined $EXCLUDE_COLS{ $rc[0] } ) {
		print "Excluding Column $rc[0]\n";
		next;
	}
	$field_name_list.= "," unless $field_name_list eq "";
	$query_fields   .= "," unless $field_name_list eq "";

	$field_name_list.= $rc[0];
	$query_fields   .= $rc[0] if $rc[3] =~ /^\s*No\s*$/;

	push @col_nm,$rc[0];
	push @data_type,$rc[1];

	if( defined $NO_QUOTE_OVERRIDES{ $rc[0] } ) {
		$query_fields   .= "isnull(".$rc[0].",-1234)" if $rc[3] =~ /^\s*Yes\s*$/;
		push @do_quote,"NO";
	} elsif( $rc[1] =~ /date/  ) {
				# date and datetime conversion
	    $query_fields   .= "isnull(".$rc[0].",'Nov  9 1962  7:42:00:000AM')"
				if $rc[3] =~ /^\s*Yes\s*$/;
	    push @do_quote,"YES";
	} elsif( $rc[1] !~ /int/
		and $rc[1] !~ /float/
		and $rc[1] !~ /decimal/
		and $rc[1] !~ /numeric/
		and $rc[1] !~ /money/
		and $rc[1] !~ /double/ ) {

		$query_fields   .= "isnull(".$rc[0].",'-1234')" if $rc[3] =~ /^\s*Yes\s*$/;
		push @do_quote,"YES";
	} else {
		# $query_fields   .= "isnull(".$rc[0].",-1234)" if $rc[3] =~ /^\s*Yes\s*$/;
		$query_fields   .= $rc[0] if $rc[3] =~ /^\s*Yes\s*$/;
		push @do_quote,"NO";
	}
	push @allow_null, $rc[3];
};

$field_name_list =~ s/\s//g;
#$query_fields    =~ s/\s//g;

$opt_q = "select $query_fields from $opt_t"
	unless defined $opt_q;

# SELECT OUR RESULTS INTO @RESULTS
my( @RESULTS )=dbi_query(-db=>$opt_D,-query=>$opt_q);
print "Returned ",( $#RESULTS+1 )," rows from query\n";
print "QUERY: $opt_q\n" if defined $opt_d;

# WRITE @RESULTS TO FILE
open( OUTFILE,"> $opt_t.ins") or die "CANT OPEN $opt_t.ins: $!\n";
print OUTFILE "use $opt_D\ngo\n";
print OUTFILE "set nocount on\ngo\n";
print OUTFILE "truncate table $opt_t\ngo\n" unless defined $opt_r;

if( defined $opt_d ) {
	my($cnt)=0;
	foreach (@col_nm) {
		print "Col $_ Quote=$do_quote[$cnt] Null=$allow_null[$cnt]\n"
				if defined $opt_d;
		$cnt++;
	}
}

#
# Put Together The Insert String
#
foreach my $rc ( @RESULTS ) {
	my(@cols)=dbi_decode_row($rc);
	my($values_str) = "";
	my($colcnt)=0;
	foreach (@cols) {
		$values_str.="\t," if $values_str ne "";

		# ok here are the nulity parameters for this side
		#
		#  if /date/  then 'Nov  9 1962 7:42:00:000AM'
		#  else if numerical type then ''
		#  else if string then -1234
		my($cellvalue);
		if( $allow_null[$colcnt]=~/^\s*Yes\s*$/ ) {
			if( defined $NO_QUOTE_OVERRIDES{ $col_nm[$colcnt] } ) {
				$cellvalue = "NULL\n" if $_ eq "-1234";
			} elsif( $data_type[$colcnt] =~ /date/  ) {
			 	$cellvalue = "NULL\n" if $_ eq "Nov  9 1962  7:42:00:000AM";
			} elsif( $data_type[$colcnt] !~ /int/
				and 	$data_type[$colcnt] !~ /float/
				and 	$data_type[$colcnt] !~ /decimal/
				and 	$data_type[$colcnt] !~ /numeric/
				and 	$data_type[$colcnt] !~ /money/
				and 	$data_type[$colcnt] !~ /double/ ) {
			 		$cellvalue = "NULL\n" if $_ eq "-1234";
			} else {
			 	$cellvalue = "NULL\n"
					if( $do_quote[$colcnt] eq "NO" and $_ eq "" );
			}
		}
		if( ! defined $cellvalue ) {
			 $values_str .= "\"" if $do_quote[$colcnt] eq "YES";
			 $values_str .= "$_";
			 $values_str .= "\"" if $do_quote[$colcnt] eq "YES";
			 $values_str .= "\n";
		} else {
			 $values_str .= $cellvalue;
		}
		$colcnt++;
	}
	print OUTFILE "insert $opt_t ( \n\t$field_name_list\n) values (\n\t$values_str\n)\ngo\n";
}

close OUTFILE;
print "Output file is $opt_t.ins\n";

__END__

=head1 NAME

create_ins_script.pl - create insert script utility

=head2 DESCRIPTION

Output a set of insert statements that will rebuild the data in a server.

=head2 USAGE

	USAGE: create_ins_script.pl -Ssrv -Uusr -Ppass -Ddb -ttable -qquery
	  [-eexclcol1,exclcol2] -iintcol1,intcol2

	-i no quote columns (used for user defined types)
	-e exclude from report

   Output is placed in tablename.ins

   If no query passed with -q it will do a select *

=cut
