use racks
go
drop table barlow_precompute
go
select distinct
system_name,
system_type,
wasp_site = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Site'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_location = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_floor = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Floor'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_row = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Row'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
wasp_cabinet = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Location - Cabinet'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type ),
asset_tag = ( select attribute_value from gemalarms..INV_system_attribute a
		  		 where 	a.system_type ='WASP'
		  		 and 	a.attribute_name like 'Asset Tag'
				 and 	a.system_name=s.system_name
				 and 	a.system_type=s.system_type )
into    barlow_precompute
from 	gemalarms..INV_system s
where 	s.system_type ='WASP'
go
create index xxx on barlow_precompute ( wasp_site,wasp_location,wasp_floor,wasp_row,wasp_cabinet )
go
create unique index xxxa on barlow_precompute ( asset_tag )
go
--select * from barlow_precompute
--order by system_name,system_type

-- dump database racks to 'compress::3::/export/home/sybase-dump/racks.dmp'

--select * from barlow_precompute where asset_tag='12311
