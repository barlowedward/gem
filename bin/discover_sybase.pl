#!/apps/perl/linux/perl-5.8.2/bin/perl

# copyright (c) 2009-2011 by Edward Barlow

use strict;
use Socket;
use Getopt::Long;
use DBIFunc;
use DBI_discovery;
use Carp qw/confess/;
use Repository;

use vars qw( $DEBUG );
die "Bad Parameter List $!\nAllowed arguments to are --DEBUG" unless
   GetOptions(  "DEBUG"		=> \$DEBUG 	);

my(@interfaces)=dbi_get_interfaces(undef,undef,1);			# the 1 is for nodie

my(%registered_hosts);
foreach (get_password(-type=>"unix") ) 		  { $registered_hosts{$_}=1; }
foreach (get_password(-type=>"win32servers") ) { $registered_hosts{$_}=1; }

my(@svrs)=get_password(-type=>"sybase");
printf( "-- Found %4s registered servers of type sybase \n",($#svrs+1) );

my(%registered_servers);
my(%registered_ip_ports);
foreach my $s (@svrs) {
	my(%hsh) = get_password_info(-type=>"sybase",-name=>$s);
	if( $hsh{HOSTNAME} and ! $registered_hosts{$hsh{HOSTNAME}} ) {
		print "WARNING: HOSTNAME $hsh{HOSTNAME} for $s is not in Unix/Win32 password file.\n";			
	}
	if( $hsh{HOSTNAME} and $hsh{PORTNUMBER} ) {
		$registered_servers{$s} = $hsh{HOSTNAME}.":".$hsh{PORTNUMBER};
		my($ip) = GetIpAddress( $hsh{HOSTNAME} );
			if( $ip eq "" ) {
				my($correct_host);
				foreach (@interfaces) {
					my($server, $host,$port)=( $_->[0], $_->[1], $_->[2] );
					next unless $s eq $server;
					if( $host =~ /\+/ ) {
						my(@x)=split(/\+/,$host);
						$host=$x[0];
					}
					$correct_host=$host;
					last;
				}
				if( ! $correct_host ) {
					print "WARNING: Bad HOSTNAME $hsh{HOSTNAME} for $s\n";
				} else {
					print "WARNING: HOSTNAME should be $correct_host for $hsh{HOSTNAME} for $s\n";
				}
			} else {
				$registered_ip_ports{$ip.":".$hsh{PORTNUMBER}}=1;
			}
	} else {
		$registered_servers{$s}="NA";
		my($correct_host);
		foreach (@interfaces) {
			my($server, $host,$port)=( $_->[0], $_->[1], $_->[2] );
			next unless $s eq $server;
			if( $host=~/\+/ ) {
				my(@x)=split(/\+/,$host);
				$host=$x[0];
			}
			if( $port=~/\+/ ) {
				my(@x)=split(/\+/,$host);
				$port=$x[0];
			}				
			print "WARNING: HOSTNAME=$host PORT=$port for $s\n";
			$registered_servers{$s} = $host.":".$port;
			last;
		}
	}
}	

sub debugmsg { print @_ if $DEBUG; }

my(%registered_hostports);
my(%SYB_INTERFACES_HOSTS,%SYB_INTERFACES_PORTS,%SYB_INTERFACES_IP);	
my(@interfaces)=dbi_get_interfaces(undef,undef,1);			# the 1 is for nodie
foreach (@interfaces) {
	my($server, $host,$port)=( $_->[0], $_->[1], $_->[2] );
	debugmsg( "TESTING INTERFACES FILE SERVER $server\n" );
	my($rc)= tcp_portping($host,$port);
	debugmsg( "\tNot Pingable\n" ) unless $rc;
	next unless $rc;	
	debugmsg( "\tSql Server\n" ) if $port==1433 or $port==1434;
	next if $port==1433 or $port==1434;
	my($ip)=GetIpAddress($host);
	debugmsg( "\tNo Ip\n" ) if $ip eq "";
	next if $ip eq "";
	if( $server=~/_RS$/ or $server=~/_BS$/ or $server=~/_BACK$/ or $server=~/_RSSD$/ ) {
		debugmsg( "\t$server is probably not Sybase ASE\n" );
		next;
	}		
	
	if( $registered_servers{$server} ) {
		debugmsg( "\tName Match to Registered Gem Server $server\n" );
		if( $registered_servers{$server} eq "NA" ) {
			print "\tERROR: Must Write Host=$host Port=$port For Server $server\n";
			$registered_ip_ports{$ip.":".$port}=1;
		} else {
			my($h,$p) = split(/:/,$registered_servers{$server},2);
			if( $h eq $host and $p eq $port ) {
				debugmsg( "\tOK\n" );
			} elsif(  $p eq $port and $ip eq GetIpAddress($h) ) {
				debugmsg( "\tOK\n" );
			} else {
				print "ERROR: Host=$host!=$h Port=$port!=$p For Server $server\n";
			}
		}
	} else {
		# server not registered by exact name... is it registered under another name
		if( $registered_ip_ports{$ip.":".$port} ) {
			debugmsg( "\tOK - Registered under another name\n" );
		} else {
			printf "NEW SERVER:  %14s ==> Host %14s Port %5s Ip %s\n",$server,$host,$port,$ip;
		}
	}
	#$SYB_INTERFACES_HOSTS{$server} = $host;
	#$SYB_INTERFACES_IP{$server} = $ip;
	#$SYB_INTERFACES_PORTS{$server} = $port;
	#printf "Pinging  %14s ==> Host %14s Port %5s Ip %s: %s\n",$server,$host,$port,$ip,$rc;
}

my(%ipcache);	# ip by svr
sub GetIpAddress {
	my($svr)=@_;
	return $ipcache{$svr} if $ipcache{$svr};
	my(@address)=gethostbyname($svr);# or confess "Cant resolve ip for $svr $!\n";
	# print "DBGDBG $#address FOR imagsyb2\n" if $svr eq "imagsyb2";
	return "" if $#address < 0;
	@address= map { inet_ntoa($_) } @address[4..$#address];
	print "  ==> GetIpAddress($svr) = ",join(" ",@address),"\n" if $DEBUG;
	print "FAILED FOR $svr\n" if $#address<0;
	$ipcache{$svr} = join(/\|/,@address);
	return  join(/\|/,@address);
}