#!/apps/perl/linux/perl-5.8.2/bin/perl
use lib qw(G:/gem/lib G:/gem/Win32_perl_lib_5_8 //samba/sybmon/gem/lib //samba/sybmon/gem/Win32_perl_lib_5_8 /apps/sybmon/gem/lib);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage {

        my($str)=@_;
        chomp $str;
        print "===========================================\n$str\n===========================================\n\n" if defined $str;

        die "Usage: check_app_cksum.pl -DDirectory [-oOUTFILE|-iINFILE] -RPrefix -fm

-m shows missing files

Checks application checksums against a file.
Normally reports against the file.  If -f is passed, it will show differences
both ways.

Note that the prefix is removed from the file names to ease comparison.  If
no prefix is sent, it is assumed to be same as Directory.
\n";
}

$|=1;

my($num_out_lines)=0;
my(@outputlines);

use strict;
use Getopt::Std;

use vars qw( $opt_m $opt_D $opt_o $opt_i $opt_R $opt_f);

usage() if $#ARGV<0;
usage("Invalid Arguments") unless getopts('D:o:i:R:fm');
usage("Must Pass Data Directory") unless  defined $opt_D ;
usage("Prefix From -R must be in -D Directory") if defined $opt_R and $opt_D !~ /$opt_R/;
usage("Must Pass -i or -o filename") unless defined $opt_o or defined $opt_i;

$opt_R = $opt_D unless defined $opt_R;
$opt_D .= "/" unless $opt_D =~ /\/$/;
$opt_R .= "/" unless $opt_R =~ /\/$/;

print "check_app_cksum.pl - application checksum comparison program\n\n";
print "The following programs differ from the baseline checksum file.  These file may be out of date.\n\n" if defined $opt_i;

# Check that cksum exists
my($cksum)="";
my($path)=$ENV{"PATH"};
if( $path =~ /[CD]:/ )  {
        # NT
        foreach( split /;/,$path ) {
                next unless -x $_."/cksum.exe";
                s.\\./.g;
                $cksum = $_."/cksum.exe";
                last;
        }
} else {
        # UNIX
        foreach( split /:/,$path ) {
                next unless -x $_."/cksum";
                $cksum = $_."/cksum";
                last;
        }
}
die "cksum program not found in your path\n" unless $cksum ne "";
#print "Found cksum program at $cksum\n";

die "ERROR: Base Directory ($opt_D) Not Found\n" unless -d $opt_D;

if( defined $opt_o ) {

        my(@dirlist)=get_subdirectories($opt_D);
        push @dirlist,$opt_D;

        #
        # Create an output file...
        #
        unlink $opt_o if -e $opt_o;
        open(OUTFILE,">".$opt_o) or die "Cant write to file $opt_o : $!\n";
        foreach (sort @dirlist) {
                my(@rc) = run_cksum($cksum." ".$_."/*");

                print OUTFILE "\n# DIRECTORY $_\n";
                print OUTFILE join("\n",@rc),"\n";
        }
        print "\n";
        close OUTFILE;

} else {

        die "Cant find input file $opt_i\n" unless -e $opt_i;
        my(%CKSUM,%SIZE);
        open(INFILE,$opt_i) or die "Cant read file $opt_i : $!\n";
        while (<INFILE>) {
                next if /^\s*$/ or /^#/;
                chop;
                my($cksum,$size,$fname)=split;
                $fname =~ s.^\/..;
                next if $fname=~/^\s*$/;
                $CKSUM{$fname} = $cksum;
                $SIZE{$fname}  = $size;
        }

        my(@dirlist)=get_subdirectories($opt_D);
        push @dirlist,$opt_D;

        my(%FOUND);
        foreach (sort @dirlist) {
                my(@rc) = run_cksum($cksum." ".$_."/*");
                foreach (@rc) {
                        next if /^\s*$/ or /^#/;
                        chomp;
                        my($cksum,$size,$fname)=split;
                        $fname =~ s.^\/..;
                        next if $fname=~/^\s*$/;
                        $FOUND{$fname}=1;
                        if( $cksum == $CKSUM{$fname}    and $size == $SIZE{$fname} ) {
                                next;
                        } elsif( $fname =~ /\.err$/
                          or $fname =~ /\.log$/
                          or $fname =~ /\.old$/
                          or $fname =~ /\.env$/
                          or $fname =~ /\.cfg$/
                          or $fname =~ /\.out$/
                          or $fname =~ /\.dat$/
                          or $fname =~ /\.sample$/
                          or $fname =~ /\.\d+$/ ) {
                                next;
                        } elsif( !defined $CKSUM{$fname}        or !defined $SIZE{$fname} ) {
                                prline($fname,"Missing in File") if defined $opt_f and defined $opt_m;
                                next;
                        } elsif( $cksum != $CKSUM{$fname}       and $size == $SIZE{$fname} ) {
                                prline($fname,"Different Cksum");
                        } elsif( $cksum == $CKSUM{$fname}       and $size != $SIZE{$fname} ) {
                                prline($fname,"Different Size");
                        } elsif( $cksum != $CKSUM{$fname}       and $size != $SIZE{$fname} ) {
                                prline($fname,"Different");
                        }
                }
        }
        close INFILE;

        foreach (sort keys %CKSUM) {
                next if defined $FOUND{$_};
                next if /\.err$/ or /\.old$/ or /\.log$/ or /\.\d+$/ or /\.dat/ or /\.out/ or /\.cfg/ or /\.env$/ or /\.sample$/;
                prline($_,"Missing") if defined $opt_m;
        }
}

foreach (sort @outputlines) { print $_; }

if( $num_out_lines==0 and defined $opt_i )  {
        print "No Differences Found\n";
}

sub get_subdirectories
{
        my($dir)=@_;
        opendir(DIR,$dir) || die("Cant open directory $dir for reading\n");
        $dir.="/";
        my(@d)=grep((!/^\./ and -d $dir.$_), readdir(DIR));
        closedir DIR;

        my(@out);
        foreach (@d) {
                push @out,$dir.$_;
                push @out,get_subdirectories($dir.$_);
        }
        return @out;
}

sub run_cksum
{
        my($string)=join(" ",@_);
        my(@rc);
        $string .= " 2>&1 |";
        open( SYS2STD,$string ) || s_quit("Unable To Run $string");
        while ( <SYS2STD> ) {
                chop;
                next if /Access is denied./;
                next if /cannot find the file specified/;
                s.$opt_R..;
                push @rc,$_;
        }
        close(SYS2STD);
        return @rc;
}

sub prline
{
        my($fname,$state)=@_;
        push @outputlines,sprintf("%-50.50s %s\n",$fname,$state);
        $num_out_lines++;
}

__END__

=head1 NAME

check_app_cksum.pl - Check an application checksum (requires cksum command)

=head2 DESCRIPTION

Uses cksum program to compare checksums in a directory (and its subdirectories)
with a static data file.  A great way to identify programs that are bad or
out of date between a release and an output directory.  Ignores files with
extensions .err, .log, .sample, .env, .cfg, or with digits as their extensions.

Create a 'gold' area of your software and run the checksummer on it to create
an output file.  You can then compare the output file to other directories to
find what is different.   eg:

=head2 EXAMPLE

        # create my_file
        check_app_cksum.pl -D/a/b/c -omy_file

        # compare using my_file
        check_app_cksum.pl -D/d/b/c -imy_file

=head2 USAGE

Usage: check_app_cksum.pl -DDirectory [-oOUTFILE|-iINFILE] -RPrefix -f

Checks application checksums against a file.
Use -f to show files not in both file and directory.

Note that the prefix is removed from the file names to ease comparison.
If no prefix is sent, it is assumed to be same as Directory.

=cut

