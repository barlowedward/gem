use lib "G:/dev/Win32_perl_lib_5_6";
use Win32::PerfMon;
use strict;
use Data::Dumper;

my $err = undef;

my(@counters)= (
#"System|System Up Time",
#"Network Interface|Bytes Received/sec|Intel[R] PRO Adapter",
#"Network Interface|Bytes Sent/sec|Intel[R] PRO Adapter",
#"Network Interface|Bytes Received/sec|MS TCP Loopback interface",
#"Network Interface|Bytes Sent/sec|MS TCP Loopback interface",
#"PhysicalDisk|% Disk Read Time|_Total",
#"PhysicalDisk|% Disk Write Time|_Total",
#"PhysicalDisk|Avg. Disk Bytes/Read|_Total",
#"PhysicalDisk|Avg. Disk Bytes/Transfer|_Total",
#"PhysicalDisk|Avg. Disk Bytes/Write|_Total",
#"PhysicalDisk|Avg. Disk Queue Length|_Total",
#"PhysicalDisk|Avg. Disk Read Queue Length|_Total",
#"PhysicalDisk|Avg. Disk sec/Read|_Total",
#"PhysicalDisk|Avg. Disk sec/Transfer|_Total",
#"PhysicalDisk|% Disk Time|0 C:",
#"Processor(_Total)|% Processor Time"
);

my $PerfObj = Win32::PerfMon->new("\\\\adsrv222");

my(@counters);
my $PerfObjectPtr = $PerfObj->ListObjects();
foreach my $obj (@$PerfObjectPtr) {
#	next unless 	$obj eq "Paging File"
#		or 	$obj eq "PhysicalDisk"
#		or 	$obj eq "LogicalDisk"
#		or	$obj eq "Processor"
#		or	$obj eq "Memory"
#		or	$obj eq "System"
#		or	$obj =~ /^SQLServer/
#		or	$obj eq "Network Interface"
#		or	$obj eq "Paging File";
	print "Object $obj\n";
	my($CounterPtr)=$PerfObj->ListCounters($obj);
	if( $CounterPtr == -1 ) {
		print "Error No Counters for $obj\n";
	} else {
		foreach my $i (@$CounterPtr) {
			print "Counter $obj - ",$i,"\n";
		}
	}
	
	#if( $CounterPtr == -1 ) {
	#	print "Error No Counters for $obj\n";
	#} else {
	#	print Dumper $CounterPtr;
	#	foreach ( @$CounterPtr ) {
	#		push @counters,$obj."|".$_;
	#	}
	#}
	my($Instances)=$PerfObj->ListInstances($obj);
	if( $Instances == -1 ) {
		print "Error No Instances for $obj\n";
	} else {
		foreach my $i (@$Instances) {
			print "Instance obj - ",$i,"\n";
		}
	}
	#if( $Instances != -1 ) {
	#	print "Instances for $obj\n";
	#	print Dumper $Instances;
	#}
}
die "Done";
my(@c_objs);
if($PerfObj != undef) {
	my($cntr)=-1;
	foreach (@counters) {
		$cntr++;
		my(@c)=split(/\|/,$_);
		$c[2]=-1 unless $c[2];
		$c_objs[$cntr] = $PerfObj->AddCounter(@c);
		print "Adding Counter $c[0], $c[1]\n";
  		if($c_objs[$cntr] != 0) {
  			my($val) = $PerfObj->CollectData();
  			if($val  != 0) {
  				my $secs = $PerfObj->GetCounterValue(@c);
  				if($secs > -1) {
  					print "$c[0] $c[1] = [$secs]\n";
  				} else {
  					$err = $PerfObj->GetErrorText();
	  				print "Failed to get the counter data ", $err, "\n";
  				}
  			} else {
  				$err = $PerfObj->GetErrorText();
  				print "Failed to collect the perf data ", $err, "\n";
  			}
  		} else {
  			$err = $PerfObj->GetErrorText();
  			print "Failed to add the counter ", $err, "\n";
  		}
    	}
} else {
  	print "Failed to greate the perf object\n";
}
